package org.nuclos.test.rest

import org.apache.http.HttpException
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.HttpResponseInterceptor
import org.apache.http.protocol.HttpContext

class ResponseSqlInterceptor implements HttpResponseInterceptor {
	@Override
	void process(final HttpResponse response, final HttpContext context) throws HttpException, IOException {
		RestResponse restResponse = new RestResponse(response)

		// There must be no INSERT/UPDATE/DELETE statements for GET requests. See NUCLOS-6610
		HttpRequest request = context.getAttribute('http.request')
		if (request.getRequestLine().getMethod() == 'GET') {
			assert !restResponse.sqlInsertUpdateDeleteCount
		}
	}
}
