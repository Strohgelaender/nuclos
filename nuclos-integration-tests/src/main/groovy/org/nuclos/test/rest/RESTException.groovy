package org.nuclos.test.rest

import javax.ws.rs.core.Response

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RESTException extends IOException {
	private final Response.Status status
	private final String errorText

	RESTException(
			final int statusCode,
			final String errorText,
			final Exception cause
	) {
		super(cause)

		this.status = Response.Status.fromStatusCode(statusCode)
		this.errorText = errorText
	}

	RESTException(
			final int statusCode,
			final String errorText
	) {
		this.status = Response.Status.fromStatusCode(statusCode)
		this.errorText = errorText
	}

	Response.Status getStatus() {
		status
	}

	int getStatusCode() {
		status.statusCode
	}

	String getErrorText() {
		errorText
	}

	@Override
	String getMessage() {
		return "Request failed with status $status.statusCode ($status)"
	}
}
