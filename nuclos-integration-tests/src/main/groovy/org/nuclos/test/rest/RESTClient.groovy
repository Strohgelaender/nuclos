package org.nuclos.test.rest


import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.json.JSONObject
import org.nuclos.common.UID
import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject
import org.nuclos.test.TestEntities

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RESTClient {
	final String username
	final String password

	String datalanguage

	CloseableHttpClient httpClient
	String sessionId

	RESTClient(final String user, final String password) {
		this.username = user
		this.password = password


		RequestConfig requestConfig= RequestConfig.copy(RequestConfig.DEFAULT)
				.setCookieSpec(CookieSpecs.STANDARD)
				.build()

		this.httpClient = HttpClients.custom()
				.setDefaultRequestConfig(requestConfig)
				.addInterceptorFirst(new ResponseSqlInterceptor())
				.build()
	}

	RESTClient login() {
		sessionId = RESTHelper.login(this)
		return this
	}

	public <PK> void save(EntityObject<PK> eo) {
		RESTHelper.save(eo, this)
	}

	public <PK> boolean delete(EntityObject<PK> eo) {
		return RESTHelper.delete(eo, this)
	}

	public <PK> void deleteAll(Collection<EntityObject<PK>> eos) {
		RESTHelper.deleteAll(eos, this)
	}

	public <PK> EntityObject<PK> getEntityObject(
			EntityClass<PK> entityClass,
			PK id
	) {
		RESTHelper.getEntityObject(entityClass, id, this)
	}

	public <PK> List<EntityObject<PK>> getEntityObjects(
			EntityClass<PK> entityClass,
			QueryOptions options = null
	) {
		RESTHelper.getEntityObjects(entityClass, this, options)
	}

	public <PK, SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			String referenceAttributeId
	) {
		RESTHelper.loadDependents(eo, subEoClass, referenceAttributeId, this)
	}

	public <PK, SubPK> List<EntityObject<SubPK>> loadDependentsRecursively(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			// TODO: Use proper type like org.nuclos.server.rest.services.helper.RecursiveDependency
			String... referenceAttributeIdsAndEoIds
	) {
		RESTHelper.loadDependentsRecursively(this, eo, subEoClass, referenceAttributeIdsAndEoIds)
	}

	public <PK> void executeCustomRule(
			EntityObject<PK> eo,
			String ruleName
	) {
		RESTHelper.executeCustomRule(eo, ruleName, this)
	}

	JSONObject getSessionData() {
		RESTHelper.getSessionData(this)
	}

	void saveAll(final List<EntityObject> entityObjects) {
		RESTHelper.saveAll(entityObjects, this)
	}

	public <PK> void changeState(EntityObject<PK> eo, int statusNumeral) {
		RESTHelper.changeState(eo, statusNumeral, this)
	}

	public void expectSearchResults(String search, int hits, int wait) {
		RESTHelper.expectSearchResults(search, hits, this, wait)
	}

	public void expectSearchResults(String search, int hits) {
		RESTHelper.expectSearchResults(search, hits, this, 0)
	}

	public <PK, PK2> EntityObject<PK2> generateObject(
			final EntityObject<PK> eo,
			final UID generator,
			final EntityClass<PK2> resultClass
	) {
		return RESTHelper.generateObject(eo, generator, resultClass, this)
	}

	File exportResultList(
			final TestEntities entity,
			final String outputFormat
	) {
		return RESTHelper.exportResultList(entity, outputFormat, this)
	}

	byte[] exportNuclet(String nucletName) {
		return RESTHelper.exportNuclet(nucletName, this)
	}

}
