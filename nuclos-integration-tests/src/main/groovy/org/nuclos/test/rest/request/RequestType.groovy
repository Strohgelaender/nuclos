package org.nuclos.test.rest.request

import java.util.regex.Pattern

import groovy.transform.CompileStatic;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum RequestType {

	EO_CREATE(Pattern.compile('^(?i)(POST).*/rest/bos/(\\w+)($|\\?)')),
	EO_READ(Pattern.compile('^(?i)(GET).*/rest/bos/(\\w+)/(\\w+)($|\\?)')),
	EO_UPDATE(Pattern.compile('^(?i)(PUT).*/rest/bos/(\\w+)/(\\w+)($|\\?)')),
	EO_DELETE(Pattern.compile('^(?i)(DELETE).*/rest/bos/(\\w+)/(\\w+)($|\\?)')),

	EO_READ_LIST(Pattern.compile('^(?i)(GET|POST) .*/rest/bos/\\w+($|\\?|/query)')),

	EO_SUBFORM(Pattern.compile('^(?i)(GET) .*/rest/bos/\\w+/\\w+/subBos/(?:recursive/)?\\w+(/\\w+)+($|\\?)')),
	EO_GENERATION(Pattern.compile('^(?i)(POST).*/rest/boGenerations/(\\w+)/(\\w+)/(\\d+)/generate/(\\w+)($|\\?)')),

	TREE_ROOT(Pattern.compile('^(?i)(GET) .*/rest/data/tree/.*')),
	TREE_SUBTREE(Pattern.compile('^(?i)(GET) .*/rest/data/subtree/.*')),

	// Any request related to preferences
	PREFERENCE_ALL(Pattern.compile('^(?i)(GET|POST|PUT|DELETE).*/rest/preferences.*')),

	PREFERENCE_CREATE(Pattern.compile('^(?i)(POST).*/rest/preferences.*')),
	PREFERENCE_READ(Pattern.compile('^(?i)(GET).*/rest/preferences.*')),
	PREFERENCE_UPDATE(Pattern.compile('^(?i)(PUT).*/rest/preferences.*')),
	PREFERENCE_DELETE(Pattern.compile('^(?i)(DELETE).*/rest/preferences(?!.*/select)')),
	// SAVE = CREATE or UPDATE
	PREFERENCE_SAVE(Pattern.compile('^(?i)(POST|PUT).*/rest/preferences.*')),
	PREFERENCE_SELECT(Pattern.compile('^(?i)(PUT).*/rest/preferences/.*/select.*')),
	PREFERENCE_DESELECT(Pattern.compile('^(?i)(DELETE).*/rest/preferences/.*/select.*')),

	// All request methods except OPTIONS
	ALL(Pattern.compile('.*'))

	private final Pattern pattern

	RequestType(Pattern pattern) {
		this.pattern = pattern
	}

	boolean matches(String url) {
		return url.find(pattern)
	}
}
