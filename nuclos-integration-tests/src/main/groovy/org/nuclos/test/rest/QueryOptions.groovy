package org.nuclos.test.rest

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class QueryOptions {
	String queryWhere
	String search
}
