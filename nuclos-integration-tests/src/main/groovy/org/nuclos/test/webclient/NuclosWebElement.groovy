package org.nuclos.test.webclient

import org.nuclos.test.log.Log
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.*
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

/**
 * A Wrapper class for WebElements that provides additional and more reliable methods.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class NuclosWebElement implements WebElement {
	/**
	 * The wrapped element, to which most method calls are simply delegated.
	 */
	final WebElement element

	NuclosWebElement(final WebElement element) {
		this.element = element
	}

	NuclosWebElement $(String selector) {
		AbstractWebclientTest.$(element, selector)
	}

	List<NuclosWebElement> $$(String selector) {
		AbstractWebclientTest.$$(element, selector)
	}

	/**
	 * Waits for a matching child element for the given selector to appear.
	 *
	 * @param selector
	 * @return
	 */
	NuclosWebElement waitForElement(String selector) {
		NuclosWebElement result = null
		AbstractWebclientTest.waitFor {
			result = $(selector)
			return result?.displayed && result?.enabled
		}
		return result
	}

	/**
	 * Waits for a child element matching the given selector to appear and then
	 * tries to click it.
	 * Fails only if the element does not appear within the default timeout.
	 */
	NuclosWebElement click(String selector) {
		NuclosWebElement result = null

		AbstractWebclientTest.waitFor {
			try {
				result = waitForElement(selector)
				result.click()
				return true
			} catch (WebDriverException ex) {
				Log.debug("Could not click element '$selector': $ex.message")
				return false
			}
		}

		return result
	}

	void highlight() {
		AbstractWebclientTest.highlight(this.element)
	}

	void blur() {
		AbstractPageObject.executeScript("arguments[0].blur()", element)
		AbstractWebclientTest.tryToWaitForAngular(10)
	}

	/**
	 * Tries to click on this element and possibly retries on exceptions.
	 * Fails only if the element does not become clickable within the default timeout.
	 */
	@Override
	void click() {
		AbstractWebclientTest.waitFor {
			try {
				clickWithoutWait()
				AbstractWebclientTest.tryToWaitForAngular(10)
				return true
			} catch (Exception ex) {
				Log.warn("Could not click element '$this': $ex.message")
				return false
			}
		}
	}

	void clickWithoutWait() {
		element.click()
	}

	@Override
	void submit() {
		element.submit()
	}

	/**
	 * This sends keys directly to the element, even if it does not have the focus.
	 * To better simulate real user behavior, use {@link AbstractWebclientTest#sendKeys(java.lang.CharSequence [ ])}.
	 *
	 * @param keysToSend
	 * @deprecated Use {@link AbstractWebclientTest#sendKeys(java.lang.CharSequence [ ])}
	 */
	@Override
	@Deprecated
	void sendKeys(final CharSequence... keysToSend) {
		element.sendKeys(keysToSend)
	}

	@Override
	void clear() {
		element.clear()
	}

	@Override
	String getTagName() {
		element.tagName
	}

	@Override
	String getAttribute(final String name) {
		element.getAttribute(name)
	}

	@Override
	boolean isSelected() {
		element.selected
	}

	@Override
	boolean isEnabled() {
		element.enabled
	}

	@Override
	String getText() {
		element.text
	}

	@Override
	List<WebElement> findElements(final By by) {
		element.findElements(by)
	}

	@Override
	WebElement findElement(final By by) {
		element.findElement(by)
	}

	@Override
	boolean isDisplayed() {
		element.displayed
	}

	@Override
	Point getLocation() {
		element.location
	}

	@Override
	Dimension getSize() {
		element.size
	}

	@Override
	Rectangle getRect() {
		element.rect
	}

	@Override
	String getCssValue(final String propertyName) {
		element.getCssValue(propertyName)
	}

	@Override
	<X> X getScreenshotAs(final OutputType<X> target) throws WebDriverException {
		element.getScreenshotAs(target)
	}

	boolean hasClass(final String className) {
		return this.getAttribute('class').split(' ').find { it.trim() == className }
	}

	void doubleClick() {
		WebDriver driver = AbstractWebclientTest.getDriver()
		Actions builder = new Actions(driver)
		builder.doubleClick(element).perform()
		AbstractWebclientTest.tryToWaitForAngular(10)
	}

	private int getScrollWidth() {
		getAttribute('scrollWidth') as Integer
	}

	private int getScrollHeight() {
		getAttribute('scrollHeight') as Integer
	}

	private int getClientWidth() {
		getAttribute('clientWidth') as Integer
	}

	private int getClientHeight() {
		getAttribute('clientHeight') as Integer
	}

	private int getScrollLeft() {
		getAttribute('scrollLeft') as Integer
	}

	private int getScrollTop() {
		getAttribute('scrollTop') as Integer
	}

	private void scrollHorizontally(int pixels) {
		AbstractPageObject.executeScript("arguments[0].scrollLeft += $pixels", element)
	}

	private void scrollVertically(int pixels) {
		AbstractPageObject.executeScript("arguments[0].scrollTop += $pixels", element)
	}

	void scrollToLeftEnd() {
		while (canScrollLeft()) {
			scrollLeft()
		}
	}

	void scrollLeft() {
		scrollHorizontally(-clientWidth)
	}

	void scrollRight() {
		scrollHorizontally(clientWidth)
	}

	void scrollTop() {
		scrollVertically(-clientHeight)
	}

	void scrollBottom() {
		scrollVertically(clientHeight)
	}

	boolean isHorizontalScrollbarPresent() {
		scrollWidth > clientWidth
	}

	boolean isVerticalScrollbarPresent() {
		scrollHeight > clientHeight
	}

	boolean canScrollRight() {
		scrollLeft + clientWidth < scrollWidth
	}

	boolean canScrollLeft() {
		scrollLeft > 0
	}

	boolean canScrollBottom() {
		scrollTop + clientHeight < scrollHeight
	}

	boolean canScrollTop() {
		scrollTop > 0
	}

	NuclosWebElement getParent() {
		WebElement parent = element.findElement(By.xpath('..'))
		new NuclosWebElement(parent)
	}

	void mouseover() {
		new Actions(AbstractWebclientTest.driver)
				.moveToElement(this.element)
				.build()
				.perform()
	}

	void setValue(String value) {
		// Does not trigger Angular's change detection
		AbstractWebclientTest.executeScript('$(arguments[0]).val(arguments[1])', this.element, value)

		// Workaround to trigger change detection
		sendKeys(Keys.SPACE)
		sendKeys(Keys.BACK_SPACE)
	}

	String getValue() {
		AbstractWebclientTest.executeScript('return $(arguments[0]).val()', this.element)
	}

	String getId() {
		getAttribute('id')
	}
}
