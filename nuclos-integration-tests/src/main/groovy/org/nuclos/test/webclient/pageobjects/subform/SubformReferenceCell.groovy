package org.nuclos.test.webclient.pageobjects.subform

import static org.nuclos.test.webclient.AbstractWebclientTest.$$

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.openqa.selenium.By
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@CompileStatic
class SubformReferenceCell {
	NuclosWebElement subformReference
	String entityObjectClass
	String attributeName

	SubformReferenceCell(String entityObjectClass, String attributeName) {
		this.entityObjectClass = entityObjectClass
		this.attributeName = attributeName
		subformReference = $$('[col-id=' + entityObjectClass + '_' + attributeName + ']')[1]
		new Actions(AbstractWebclientTest.driver).moveToElement(subformReference.element).build().perform()
		sleep(2000) // lov icons are shown delayed
		AbstractWebclientTest.waitForAngularRequestsToFinish()
	}

	void searchReference() {
		subformReference.findElements(By.cssSelector(".search-reference"))[0].click()
	}

	void addReference() {
		subformReference.findElements(By.cssSelector(".add-reference"))[0].click()
	}

	void openReference() {
		subformReference.findElements(By.cssSelector(".edit-reference"))[0].click()
	}

	boolean isSearchVisible() {
		return subformReference.findElements(By.cssSelector(".search-reference")).size() == 1
	}

	boolean isAddVisible() {
		return subformReference.findElements(By.cssSelector(".add-reference")).size() == 1
	}

	boolean isOpenVisible() {
		return subformReference.findElements(By.cssSelector(".edit-reference")).size() == 1
	}

	void setValue(String value) {
		subformReference.click()
		ListOfValues lov = ListOfValues.findByAttribute(this.attributeName)
		lov.selectEntry(value)
	}
}
