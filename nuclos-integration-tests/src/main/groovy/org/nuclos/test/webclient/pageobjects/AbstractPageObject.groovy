package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import java.text.NumberFormat

import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.Browser
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@CompileStatic
class AbstractPageObject {
	static List<NuclosWebElement> getErrorMessages() {
		$$('.error-dialog div[ng-bind-html="dialog.messageTitle"]')
	}

	static List<NuclosWebElement> getErrorStacktraces() {
		return $$('.error-dialog div[ng-bind-html="dialog.stacktrace"]')
	}

	/**
	 * Returns the trimmed text of an open modal alert dialog.
	 *
	 * @return
	 */
	static String getAlertText() {
		$('nuc-alert-modal-component .modal-body')?.text?.trim()
	}

	static void clickButtonOk() {
		$('#button-ok').click()
	}

	static void clickButtonCancel() {
		$('#button-cancel').click()
	}

	static void clickButtonYes() {
		$('#button-yes').click()
	}

	static void clickButtonNo() {
		$('#button-no').click()
	}

	static void refresh() {
		Log.info "Refreshing: $currentUrl"

		// getDriver().navigate().refresh() does not always work with PhantomJS
		if (AbstractWebclientTest.context.browser != Browser.PHANTOMJS) {
			AbstractWebclientTest.getDriver().navigate().refresh()
		}
		else {
			getUrl(currentUrl)
		}

		waitForAngularRequestsToFinish()
	}

	static String formatValue(value) {
		if (value instanceof Date) {
			return AbstractWebclientTest.context.dateFormat.format(value)
		}
		else if (value instanceof BigDecimal ) {
			return NumberFormat.getNumberInstance(AbstractWebclientTest.context.locale).format(value)
		}

		return "$value"
	}
	
	static WebElement findElementContainingText(String cssSelector, String text) {
		return $$(cssSelector).find { it.text == text }
	}

	static def executeScript(String script, Object ...args) {
		((org.openqa.selenium.JavascriptExecutor) AbstractWebclientTest.getDriver()).executeScript(script, args)
	}
}
