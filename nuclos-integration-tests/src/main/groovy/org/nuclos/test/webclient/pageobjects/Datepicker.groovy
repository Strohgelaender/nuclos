package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

@CompileStatic
class Datepicker {
	final NuclosWebElement containerElement

	Datepicker(final NuclosWebElement containerElement) {
		this.containerElement = containerElement
	}

	NuclosWebElement getDatepicker() {
		return containerElement
	}

	void open() {
		if (!open) {
			containerElement.click()
		}
	}

	boolean isOpen() {
		datepicker?.$('ngb-datepicker')
	}

	String getMonth() {
		NuclosWebElement select = datepicker.$$('select')[0]
		new Select(select.element).firstSelectedOption.text.trim()
	}

	int getYear() {
		NuclosWebElement select = datepicker.$$('select')[1]
		new Select(select.element).firstSelectedOption.text.trim().toInteger()
	}

	void clickDayOfCurrentMonth(int day) {
		for (NuclosWebElement dayElement : allDays) {
			if (!dayElement.hasClass('outside') && dayElement.text.trim().toInteger() == day) {
				dayElement.click()
				break
			}
		}
	}

	private List<NuclosWebElement> getAllDays() {
		datepicker.$$('.custom-day')
	}

	private NuclosWebElement getDatepickerElement() {
		datepicker.$('ngb-datepicker')
	}
}
