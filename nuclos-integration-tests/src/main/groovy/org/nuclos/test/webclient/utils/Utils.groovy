package org.nuclos.test.webclient.utils

import org.apache.logging.log4j.Level
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.codehaus.groovy.reflection.ReflectionUtils
import org.nuclos.test.webclient.AbstractWebclientTest
import org.openqa.selenium.WebDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.DesiredCapabilities

import net.anthavio.phanbedder.Phanbedder

class Utils {

	static String formatDate(Date date) {
		date.format("yyyy-MM-dd_HH-mm-ss.SSS")
	}
//
//	getRandomNumber: function(numberLength) {
//		var randomNumber = "";
//		var possible = "0123456789";
//		for (var i = 0; i < numberLength; i++)
//			randomNumber += possible.charAt(Math.floor(Math.random() * possible.length));
//		return randomNumber;
//	},
//
//	fillRequiredFields: function(value) {
//		element.all(protractor.By.css('[required]')).then(function(elements) {
//			for (var i in elements) {
//				elements[i].sendKeys(value);
//			}
//		});
//	},

	// mkdir -p
	static void mkdirP(String path) {
		mkdirP(new File(path))
	}

	static void mkdirP(File f) {
		if (!f.exists()) {
			f.mkdirs()
		}
	}

	// remove directory
	static void rmDir(String path) {
		rmDir(new File(path))
	}

	static void rmDir(File f) {
		if (f.exists()) {
			f.deleteDir()
		}
	}

	static WebDriver createPhantomJSDriver() {
		File phantomjs = Phanbedder.unpack()
		DesiredCapabilities capabilities = DesiredCapabilities.phantomjs()
		capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantomjs.getAbsolutePath())

		PhantomJSDriverService service = PhantomJSDriverService.createDefaultService(capabilities)
		PhantomJSDriver driver = new PhantomJSDriver(service, capabilities)

		return driver
	}

	static Logger getLogger() {
		final Logger log = LogManager.getLogger(ReflectionUtils.callingClass.simpleName)
		Level logLevel = Level.INFO;
		if (System.getProperty('log')) {
			try {
				logLevel = Level.valueOf(System.getProperty('log').toUpperCase())
			}
			catch (Exception ex) {

			}
		}
		log.level = logLevel

		return log
	}

	/**
	 * Checks if the call stack contains the given caller.
	 *
	 * @param caller
	 * @return
	 */
	public static boolean isCalledFrom(String caller) {
		isCalledFrom(caller, 1)
	}

	/**
	 * Checks if the call stack contains the given caller at least callCount times.
	 *
	 * @param caller
	 * @param callCount
	 * @return
	 */
	public static boolean isCalledFrom(String caller, int callCount) {
		int count = Thread.currentThread().stackTrace.findAll {
			"$it".contains(caller)
		}.size()

		return count >= callCount
	}

	/**
	 * Tries to extract the source code of the given closure.
	 *
	 * @param closure
	 * @return
	 */
	public static String closureToString(Closure closure) {
		try {
			return closure.metaClass.classNode.getDeclaredMethods("doCall")[0].code.text
		}
		catch (Exception ex) {
			try {
				return closure?.toString()
			}
			catch (Exception ex2) {
			}
		}
		return "Unknown source"
	}
}
