package org.nuclos.test.webclient.util

import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat

import org.nuclos.test.webclient.AbstractWebclientTest

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class ValueParser {

	static Date parseDate(String dateString) {
		if (!dateString) {
			return null
		}

		List<String> dateFormats = ['dd.MM.yyyy', 'MM/dd/yyyy']
		for (String format : dateFormats) {
			try {
				return new SimpleDateFormat(format).parse(dateString)
			} catch (ParseException) {
			}
		}
		return null
	}

	static BigDecimal parseBigDecimal(final String text) {
		try {
			DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(AbstractWebclientTest.context.locale)
			df.setParseBigDecimal(true)
			return df.parseObject(text) as BigDecimal
		} catch (ParseException e) {
			// Ignore
		}

		return null
	}
}
