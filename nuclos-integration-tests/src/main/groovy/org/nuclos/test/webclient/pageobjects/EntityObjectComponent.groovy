package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.log.CallTrace.trace
import static org.nuclos.test.webclient.AbstractWebclientTest.*

import java.text.SimpleDateFormat

import org.nuclos.schema.layout.web.WebInputComponent
import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject
import org.nuclos.test.log.Log
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.util.ValueParser
import org.nuclos.test.webclient.validation.ValidationStatus
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * Represents the entity-object component.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObjectComponent extends AbstractPageObject {

	static <PK> EntityObjectComponent open(EntityObject<PK> eo) {
		open(eo.entityClass, eo.id)
	}

	static <PK> EntityObjectComponent open(EntityClass<PK> entityClass, PK entityObjectId = null) {
		String url = "/view/$entityClass.fqn"
		if (entityObjectId) {
			url += "/$entityObjectId"
		}
		getUrlHash(url)
		waitForAngularRequestsToFinish()

		forDetail()
	}

	static EntityObjectComponent forDetail() {
		new EntityObjectComponent()
	}

	static EntityObjectModal forModal() {
		new EntityObjectModal()
	}

	String getSelectorPrefix() {
		return 'nuc-detail '
	}

	/**
	 * Clicks the "new" Button to create a new entity object.
	 * Should only be available if the current EO is not new or dirty.
	 */
	EntityObjectComponent addNew() {
		trace("Add new EO") {
			newButton.click()
			return this
		}
	}

	/**
	 * Clicks the "save" Button to save the current entity object.
	 * Should only be available if the current EO is new or dirty.
	 */
	EntityObjectComponent save() {
		trace("Save EO") {
			saveButton.click()
			return this
		}
	}

	/**
	 * Clicks the "cancel" Button to cancel modifying the current entity object.
	 * Should only be available if the current EO is marked as new or dirty.
	 */
	EntityObjectComponent cancel() {
		cancelButton.click()
		return this
	}

	/**
	 * Clicks the "delete" Button to delete the current entity object.
	 * Should only be available if the current EO is not new and not dirty.
	 */
	EntityObjectComponent delete() {
		getDeleteButton().click()
		clickButtonOk()
		return this
	}

	/**
	 * Closes this window and selects the current EO in the opener window.
	 * This should be only available if this window was opened via the search function
	 * of a reference field.
	 */
	static void selectInOtherWindow() {
		$('#selectEntryInOtherWindow').clickWithoutWait()
	}

	/**
	 * Finds the corresponding WebElement for the given attribute name.
	 *
	 * Warning: Theoretically there could be multiple input fields for the same attribute.
	 */
	NuclosWebElement getAttributeElement(
			String attributeName,
			Class<? extends WebInputComponent> componentClass = null
	) {
		String selector = "[name=\"attribute-$attributeName\"]"

		if (componentClass) {
			selector = 'nuc-' + dashSeparated(componentClass.simpleName) + ' ' + selector
		}

		selector = selectorPrefix + selector

		$(selector)
	}

	static String dashSeparated(String s) {
		return s.replaceAll(/\B[A-Z]/, '-$0').toLowerCase()
	}

	/**
	 * Checks if the corresponding input field for the given attribute name is displayed.
	 *
	 * @param attributeName
	 * @return
	 */
	boolean isFieldVisible(String attributeName) {
		getAttributeElement(attributeName)?.isDisplayed()
	}

	boolean isAttributeReadonly(String attributeName) {
		getAttributeElement(attributeName).$('input').getAttribute('disabled')
	}

	/**
	 * Fills the corresponding input element for attributeName with the given value.
	 *
	 * @param attributeName
	 * @param value
	 */
	void setAttribute(String attributeName, Object value) {
		if (value instanceof Date) {
			value = new SimpleDateFormat('yyyy-MM-dd').format(value)
		}

		NuclosWebElement element = getAttributeElement(attributeName)

		if (value instanceof File) {
			setFileAttribute(attributeName, value as File)
			return
		}

		if (element.$('.ui-autocomplete')) {
			ListOfValues lov = ListOfValues.findByAttribute(attributeName)
			boolean hasEntry = lov.hasEntry("$value")
			if (!hasEntry && value['id'] && value['name']) {
				value = value['name']
				hasEntry = lov.hasEntry("$value")
			}
			if (hasEntry) {
				lov.selectEntry("$value")
			} else if (value != null) {
				throw new NoSuchElementException("LOV option not found: " + attributeName + "=" + value)
			}
			return
		}

		String inputType = element.getAttribute('type')
		if (inputType == 'checkbox') {
			String checked = element.getAttribute('checked')
			if (checked as boolean != value as boolean) {
				element.click()
			}
		} else {
			enterText(attributeName, "$value", element)
		}

		blur()
		waitForAngularRequestsToFinish()
	}

	void setFileAttribute(String attributeName, File file) {
		FileComponent component = getFileComponent(attributeName)
		component.setFile(file)
	}

	void enterText(String attributeName, String value, NuclosWebElement element = null) {
		if (!element) {
			element = getAttributeElement(attributeName)
		}

		element.clear()
		element.sendKeys(value)
	}

	/**
	 * Gets the value for the given attribute from the corresponding input field.
	 *
	 * The optional parameter "componentClass" can be used to distinguish between the input components
	 * if there are multiple different components for the same attribute on the layout.
	 */
	def <T> T getAttribute(
			String attributeName,
			Class<? extends WebInputComponent> componentClass = null,
			Class<T> valueClass = String.class
	) {
		trace("getAttribute('$attributeName')") {
			T result

			NuclosWebElement field = getAttributeElement(attributeName, componentClass)

			// checkbox
			if (field.parent.getTagName() == 'nuc-web-checkbox') {
				return field.isSelected()
			}

			// lov
			if (isLovElement(field)) {
				field = field.$('p-autocomplete input')
			}

			String value
			if (field.tagName == 'a') {
				value = field.text?.trim()
			} else {
				value = field.getAttribute('value')
			}

			if (valueClass == String.class) {
				result = value as T
			} else if (valueClass == Date.class) {
				result = ValueParser.parseDate(value) as T
			} else {
				throw new IllegalArgumentException('Can not get attribute value for class ' + valueClass)
			}

			return result
		} as T
	}

	static String getLabelText(String attributeName) {
		$('nuc-web-label label[data-name="' + attributeName + '"]')?.text
	}

	String getText(
			String attributeName,
			Class<? extends WebInputComponent> componentClass = null
	) {
		getAttribute(attributeName, componentClass, String.class)
	}

	/**
	 * Gets the LoV page object for the given attribute name.
	 *
	 * TODO: The same attribute could be present in the layout multiple times and in different input components.
	 *
	 * @param attributeName
	 * @param value
	 */
	ListOfValues getLOV(String attributeName) {
		ListOfValues.findByAttribute(attributeName)
	}

	FileComponent getFileComponent(String attributeName) {
		NuclosWebElement element = getAttributeElement(attributeName)

		if (!element) {
			return null
		}

		new FileComponent(element)
	}

	Datepicker getDatepicker(String attributeName) {
		NuclosWebElement element = getAttributeElement(attributeName).parent
		new Datepicker(element)
	}

	/**
	 * Determines if the currently selected EO is marked as dirty.
	 *
	 * @return
	 */
	static boolean isDirty() {
		$('nuc-detail-buttons [name="isDirty"]').getAttribute('value') == 'true'
	}

	static Long getId() {
		$('nuc-detail-buttons [name="id"]').getAttribute('value')?.toLong()
	}

	static Subform getSubform(EntityClass entityClass, String referenceAttributeName) {
		final String fqn = entityClass.fqn + '_' + referenceAttributeName
		return getSubform(fqn)
	}

	/**
	 * @deprecated Use {@link #getSubform(EntityClass, String)}
	 */
	@Deprecated()
	static Subform getSubform(String refAttrFqn) {
		NuclosWebElement subformElement = $('[ref-attr-id="' + refAttrFqn + '"]')

		if (!subformElement) {
			return null
		}

		new Subform(refAttrFqn: refAttrFqn, subformElement: subformElement)
	}

	static void clickButton(final String text) {
		trace("Click Button \"$text\"") {
			getButton(text).click()
		}
	}

	static NuclosWebElement getButton(String buttonText) {
		$$('button').find { it.text.trim() == buttonText.trim() }
	}

	static WebElement checkButton(String buttonText, boolean exists, boolean enabled) {
		WebElement we = getButton(buttonText)

		if (!exists) {
			assert we == null
			return null
		}

		assert we != null
		assert enabled ? we.getAttribute('disabled') != 'true' : we.getAttribute('disabled') == 'true'

		return we
	}

	NuclosWebElement checkField(String attributeName, boolean exists, boolean enabled) {
		NuclosWebElement we = getAttributeElement(attributeName)

		if (!exists) {
			assert we == null
			return null
		}

		assert we != null

		if (isLovElement(we)) {
			// Dropdown / LOV
			String disabled = we.$$('input')[0].getAttribute('disabled')
			assert enabled ? disabled != 'true' : disabled == 'true'
		} else if ('div'.equals(we.getTagName())) {
			String disabled = we.getAttribute('disabled')
			assert enabled ? disabled != 'true' : disabled == 'true'
		} else {
			assert we.enabled == enabled
		}

		return we
	}

	static Set<String> getLabels() {
		waitForAngularRequestsToFinish()
		Set<String> ret = new HashSet<String>()

		List<NuclosWebElement> labels = $$('label')
		Log.info("labels size: " + labels.size())
		for (WebElement webElement : labels) {
			String text = webElement.getText()
			ret.add(text)
		}

		return ret
	}

	static NuclosWebElement getTab(String tabName) {
		$('.tab-title[title="' + tabName + '"]')
	}

	static boolean hasTab(String tabName) {
		getTab(tabName) as boolean
	}

	static void selectTab(String tabName) {
		getTab(tabName).click()
	}

	/**
	 * Creates a new EO of the given entity class, fills in the given data, and saves.
	 */
	static <PK> EntityObjectComponent createEO(final EntityClass<PK> entityClass, final Map<String, ?> eoData, String mandator = null) {
		EntityObjectComponent eo = open(entityClass)
		eo.addNew()

		if (mandator) {
			chooseMandator(mandator)
		}

		eoData.each { key, value ->
			eo.setAttribute(key, value)
		}

		eo.save()
	}

	static NuclosWebElement getNewButton() {
		$('#newEO')
	}

	static NuclosWebElement getSaveButton() {
		$('#saveEO')
	}

	static NuclosWebElement getDeleteButton() {
		$('#deleteEO')
	}

	static NuclosWebElement getCancelButton() {
		$('#cancelEO')
	}

	private static boolean isLovElement(NuclosWebElement element) {
		element.getAttribute('class').indexOf('lov-container') != -1
	}

	private static void chooseMandator(String mandator) {
		List<WebElement> options = $('#mandators').findElements(By.xpath("//*[text()='" + mandator + "']"))
		if (options.size() == 0) {
			throw new NoSuchElementException("not found")
		}
		options.get(0).click()
	}

	/**
	 * select the process if a process dropdown is defined in layout
	 */
	void selectProcess(String processName) {
		setAttribute('nuclosProcess', processName)
	}

	static int getListEntryCount() {
		$$('nuc-sidebar ag-grid-angular .ag-body-container [row-index]').size()
	}

	static boolean hasNextStateButton() {
		return $('.nuclos-next-state') != null
	}

	static boolean datePickerIsOpen() {
		return $('ngb-datepicker') != null
	}

	ValidationStatus getValidationStatus(String attributeName) {
		NuclosWebElement element = getAttributeElement(attributeName)
		String cssClasses = element.getAttribute('class')

		ValidationStatus.fromCssClasses(cssClasses)
	}

	static void openCharts() {
		$('#openCharts').click()
	}
}
