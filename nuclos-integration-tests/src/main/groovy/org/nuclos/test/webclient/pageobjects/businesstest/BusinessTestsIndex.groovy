package org.nuclos.test.webclient.pageobjects.businesstest

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

/**
 * Represents the business test start page.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class BusinessTestsIndex extends AbstractPageObject {
	static void open() {
		getUrlHash('#/businesstests')
	}

	static boolean isStarted() {
		!$('#generateAndRun').enabled
	}

	static void generateAndRun() {
		$('#generateAndRun').click()
		waitForAngularRequestsToFinish()
		waitFor(120) {
			!BusinessTestsIndex.started
		}
	}

	static String getLog() {
		$('#output').text
	}

	static int getTestsTotal() {
		$('#overview_testsTotal').text.toInteger()
	}

	static String getState() {
		$('#overview_state').text
	}

	static int getTestsGreen() {
		$('#overview_testsGreen').text.toInteger()
	}

	static int getTestsYellow() {
		$('#overview_testsYellow').text.toInteger()
	}

	static int getTestsRed() {
		$('#overview_testsRed').text.toInteger()
	}

	static int getDuration() {
		$('#overview_duration').text.replace(' ms', '').toInteger()
	}
}