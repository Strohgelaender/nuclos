package org.nuclos.test.webclient.pageobjects.subform

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.util.ValueParser
import org.nuclos.test.webclient.validation.ValidationStatus
import org.openqa.selenium.Keys
import org.openqa.selenium.StaleElementReferenceException

import groovy.transform.CompileStatic

@CompileStatic
class Subform extends AbstractPageObject {
	String refAttrFqn
	NuclosWebElement subformElement

	private final static String CHECKBOX_CHECKED_SELECTOR = '.fa-check-square-o'

	Row newRow() {
		subformElement.$('.new-subbo').click()
		new Row(this, 0)
	}

	boolean isNewVisible() {
		subformElement?.$('.new-subbo')?.displayed
	}

	boolean isNewEnabled() {
		subformElement?.$('.new-subbo')?.enabled
	}

	boolean isDeleteVisible() {
		subformElement?.$('.delete-selected-subbos')?.displayed
	}

	boolean isDeleteEnabled() {
		subformElement?.$('.delete-selected-subbos')?.enabled
	}

	void deleteSelectedRows() {
		$(subformElement, '.delete-selected-subbos').click()
		waitForAngularRequestsToFinish()
	}

	boolean isCloneVisible() {
		subformElement?.$('.clone-selected-subbos')?.displayed
	}

	boolean isCloneEnabled() {
		subformElement?.$('.clone-selected-subbos')?.enabled
	}

	void cloneSelectedRows() {
		$(subformElement, '.clone-selected-subbos').click()
	}

	int getRowCount() {
		subformElement.$$('.ag-body-container .ag-row').size()
	}

	int getDeletedRowCount() {
		subformElement.$$('div.ag-row .row-deleted .ag-selection-checkbox').size()
	}

	Row getRow(int index) {
		new Row(this, index)
	}

	void scrollToRow(int rowIndex) {
		executeScript("arguments[0].scrollIntoView();", $$(subformElement, '.ui-grid-row')[rowIndex])
	}

	void toggleSelection(int index) {
		// the HTML elements are reorder in DOM depending on sorting position of the row - so use row attribute
		subformElement.$$('[row-index="' + index + '"] [col-id="0"]').get(0).click()
	}

	NuclosWebElement getEditInModalLink(int index) {
		getEditCell(index).$('a.edit-in-modal-link')
	}

	NuclosWebElement getEditCell(int index) {
		subformElement.$('.ag-row[row-index="' + index + '"] nuc-subform-edit-row-renderer')
	}

	void editInModal(int index) {
		getEditInModalLink(index).click()
	}

	NuclosWebElement getEditInWindowLink(int index) {
		getEditCell(index).$('a.edit-in-window-link')
	}

	void editInWindow(int index) {
		getEditInWindowLink(index).click()
	}

	void selectAllRows() {
		NuclosWebElement selectAll = selectAllElement

		// If "ag-checkbox-checked" is hidden, not all rows are selected
		if (selectAll.$('span.ag-checkbox-checked.ag-hidden')) {
			selectAll.click()
		}
	}

	void unselectAllRows() {
		NuclosWebElement selectAll = selectAllElement

		// While "ag-checkbox-unchecked" is hidden, some rows (or all) are selected
		while (selectAll.$('span.ag-checkbox-unchecked.ag-hidden')) {
			selectAll.click()
		}
	}

	private NuclosWebElement getSelectAllElement() {
		subformElement.$('.ag-header-select-all')
	}

	private List<NuclosWebElement> getHeaderElements() {
		subformElement.$$('.ag-header-container .ag-header-cell')
	}

	private NuclosWebElement getHeaderElementByAttributeName(String name) {
		subformElement.$('.ag-header-container .ag-header-cell[col-id$="' + name + '"]')
	}

	NuclosWebElement getViewPort() {
		subformElement.$('.ag-body-viewport')
	}

	void openViewConfiguration() {
		subformElement.$('.view-preferences-button').click()
	}

	/**
	 * get visible column headers
	 * TODO scroll to get all column headers
	 */
	List<String> getColumnHeaders() {
		headerElements*.text*.trim()
	}

	void clickColumnHeader(String attributeName) {
		getHeaderElementByAttributeName(attributeName).click()
	}

	List<String> getColumnValues(String attributeName) {
		getColumnValues(attributeName, String.class)
	}

	public <T> List<T> getColumnValues(String attributeName, Class<T> clazz) {
		(0..rowCount - 1).collect {
			getRow(it).getValue(attributeName, clazz)
		}
	}

	NuclosWebElement getPopupEditor(NuclosWebElement cell) {
		String compId = cell.getAttribute('comp-id')
		subformElement.$(
				'.ag-popup-editor [for-cell="' + compId + '"] input,' +
				'.ag-popup-editor  [for-cell="' + compId + '"] textarea'
		)
	}

	List getSortModel() {
		List<Map> sortModel = []

		headerElements.each{ header ->
			NuclosWebElement sortIndicator = header.$('.sort-indicator')
			if (sortIndicator?.displayed) {
				String colId = header.getAttribute('col-id')
				String sort = sortIndicator.hasClass('sort-asc') ? 'asc' : 'desc'

				String indicatorText = sortIndicator.text.trim()
				int priority = indicatorText.isInteger() ? indicatorText.toInteger() : 0

				sortModel << [
						colId: colId,
						sort: sort,
						priority: priority
				]
			}
		}

		sortModel.sort { it.priority }

		return sortModel
	}

	String getOverlayText() {
		subformElement.$('.ag-overlay-panel')?.text
	}

	boolean isMoveUpEnabled() {
		moveUpButton?.enabled
	}

	NuclosWebElement getMoveUpButton() {
		subformElement.$('.autonumber-move-up')
	}

	void moveSelectedRowsUp() {
		moveUpButton.click()
	}

	boolean isMoveDownEnabled() {
		moveDownButton?.enabled
	}

	NuclosWebElement getMoveDownButton() {
		subformElement.$('.autonumber-move-down')
	}

	void moveSelectedRowsDown() {
		moveDownButton.click()
	}

	class Row {
		private final Subform subform
		NuclosWebElement bodyRow
		final int index

		Row(Subform subform, int index) {
			this.subform = subform
			this.index = index

			bodyRow = this.fetchRowElement()
		}

		private NuclosWebElement fetchRowElement() {
			// TODO why is 'index'-attribute of grid row not available after deleting a subform entry
			def element = subform.subformElement.$('.ag-body-container .ag-row[index="' + index + '"]')
			if (element != null)
				return element
			else
				return subform.subformElement.$('.ag-body-container .ag-row[row-index="' + index + '"]')
		}

		ListOfValues getLOV(String attributeName) {
			NuclosWebElement lovElement = getFieldElement(attributeName)
			return lovElement ? new SubformListOfValues(
					attributeName: attributeName,
					lov: lovElement,
					subform: subform
			) : null
		}

		FileComponent getFileComponent(String attributeName) {
			NuclosWebElement element = getFieldElement(attributeName)
			element ? new SubformFileComponent(element) : null
		}

		/**
		 * Gets the field element whose "col-id" ends with the given attribute name.
		 *
		 * @param attributeName
		 * @return
		 */
		NuclosWebElement getFieldElement(String attributeName) {
			waitForAngularRequestsToFinish()

			NuclosWebElement element = getFieldElementOrScrollHorizontally(attributeName)

			if (!element) {
				throw new IllegalArgumentException("Could not find subform column for attribute '$attributeName'")
			}

			return element
		}

		void clickCell(String attributeName) {
			getFieldElement(attributeName).click()
		}

		private NuclosWebElement getFieldElementOrScrollHorizontally(String attributeName) {
			NuclosWebElement element = getFieldElementOrNull(attributeName)
			if (element && element.isDisplayed()) {
				return element
			}

			NuclosWebElement view = subform.viewPort

			if (view.canScrollLeft()) {
				view.scrollToLeftEnd()
				element = getFieldElementOrNull(attributeName)
			}

			while (!element && view.canScrollRight()) {
				view.scrollRight()
				sleep(100)
				element = getFieldElementOrNull(attributeName)
			}

			return element
		}

		private NuclosWebElement getFieldElementOrNull(String attributeName) {
			retryOnStaleElement {
				bodyRow.$('div[col-id$="' + attributeName + '"]')
			}
		}

		/**
		 * Checks or unchecks a boolean attribute.
		 */
		void setChecked(final String attributeName, final boolean check) {
			NuclosWebElement cell = getFieldElement(attributeName)

			boolean checked = cell.$(CHECKBOX_CHECKED_SELECTOR) as boolean

			if (checked != check) {
				cell.click()
			}
		}

		NuclosWebElement openEditor(NuclosWebElement cell) {
			NuclosWebElement input = getEditor(cell)

			// press enter to edit cell
			if (!input) {
				// Sometimes the cell is hidden by another big cell editor (date editor)
				// Close it first by sending ESC
				sendKeys(Keys.ESCAPE)

				cell.click()
				input = getEditor(cell)
			}

			return input
		}

		NuclosWebElement getEditor(NuclosWebElement cell) {
			getPopupEditor(cell)
		}

		int enterValue(String attributeName, String value) {
			int nrChoices = 0
			NuclosWebElement cell = getFieldElement(attributeName)

			NuclosWebElement subformReference = cell.$('nuc-subform-reference')
			if (subformReference) {
				ListOfValues lov = getLOV(attributeName)
				lov.open()
				nrChoices = lov.getChoices().size()
				lov.selectEntry(value)
			} else {
				NuclosWebElement input = openEditor(cell)

				if (!input) {
					// TODO: Should it ever be necessary to send text to the cell itself?
					input = cell
				}

				// Textarea must be cleared first, because the text is not pre-selected like in other components
				if (input.tagName == 'textarea') {
					input.clear()
				}

				sendKeys(value)
				sendKeys(Keys.TAB)
				sendKeys(Keys.ESCAPE)
			}

			waitForAngularRequestsToFinish()
			return nrChoices
		}

		void enterValues(Map<String, String> data) {
			data.each { String fieldName, String value ->
				enterValue(fieldName, value)
			}
		}

		public <T> T getValue(String attributeName, Class<T> clazz = String.class) {
			T result

			NuclosWebElement element = getFieldElement(attributeName)
			String text = element?.text
			if (clazz == String.class) {
					result = text as T
			} else if (clazz == Date.class) {
				result = ValueParser.parseDate(text) as T
			} else if (clazz == Boolean.class) {
				result = !!element.$(CHECKBOX_CHECKED_SELECTOR) as T
			} else if (clazz == Integer.class) {
				if (text.isNumber()) {
					result = text.toInteger() as T
				}
			} else if (clazz == BigDecimal.class) {
				result = ValueParser.parseBigDecimal(text) as T
			} else {
				throw new IllegalArgumentException('Can not get subform values for class ' + clazz)
			}

			return result
		}

		ValidationStatus getValidationStatus(String attributeName) {
			NuclosWebElement element = getFieldElement(attributeName)
			String cssClasses = element.getAttribute('class')

			ValidationStatus.fromCssClasses(cssClasses)
		}

		boolean isDirty() {
			hasCssClass('row-modified')
		}

		boolean isNew() {
			hasCssClass('row-new')
		}

		boolean isDeleted() {
			hasCssClass('row-deleted')
		}

		boolean isSelected() {
			hasCssClass('ag-row-selected')
		}

		private boolean hasCssClass(String className) {
			retryOnStaleElement {
				bodyRow.getAttribute('class').contains(className) ||
						bodyRow.$('.ag-cell')?.getAttribute('class')?.contains(className)
			}
		}

		void setSelected(boolean selected) {
			// TODO: Workaround for selection bug with autonumbers...
			while (this.selected != selected) {
				Subform.this.toggleSelection(index)
			}
		}

		/**
		 * Tries to re-fetch the row element once, in case it became stale.
		 * Happens when subform rows are re-rendered.
		 *
		 * @param c
		 * @return
		 */
		private <T> T retryOnStaleElement(Closure<T> c) {
			try {
				return c()
			} catch (StaleElementReferenceException e) {
				bodyRow = fetchRowElement()
			}

			return c()
		}

		boolean canEditInModal() {
			Subform.this.getEditInModalLink(index)
		}

		void editInModal() {
			Subform.this.editInModal(index)
		}

		boolean canEditInWindow() {
			Subform.this.getEditInWindowLink(index)
		}

		void editInWindow() {
			Subform.this.editInWindow(index)
		}

		Datepicker getDatepicker(final String attributeName) {
			NuclosWebElement element = getFieldElement(attributeName)
			new Datepicker(element) {
				/**
				 * Subform datepicker is not a child of the cell (containerElement), but is opened in the popup
				 * editor area of the subform.
				 */
				@Override
				NuclosWebElement getDatepicker() {
					return getPopupEditor(element)?.parent
				}
			}
		}

		int getHeight() {
			bodyRow.size.height
		}
	}
}
