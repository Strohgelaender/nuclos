package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * Represents the state component,
 * should be visible on the detail page of an entity object with state model.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class StateComponent extends AbstractPageObject {

	/**
	 * Returns the trimmed text of the 'current state' element.
	 *
	 * @return
	 */
	static String getCurrentState() {
		currentStateElement?.text?.trim()
	}

	/**
	 * Returns the number of the 'current state' element.
	 *
	 * @return
	 */
	static Integer getCurrentStateNumber() {
		currentStateElement?.getAttribute('state-number')?.toInteger()
	}

	/**
	 * Returns the trimmed texts of all 'next state' elements.
	 *
	 * TODO: If a stat button label is configured, the label is returned here instead of the state name.
	 * It would be probably better to always return the state name here.
	 *
	 * @return
	 */
	static List<String> getNextStates() {
		nextStateElements*.text*.trim()
	}

	/**
	 * Returns the state numbers of all 'next state' elements.
	 *
	 * @return
	 */
	static List<Integer> getNextStateNumbers() {
		nextStateElements.collect { it.getAttribute('state-number').toInteger() }
	}

	/**
	 * Tries to click a 'state change' element that contains the given text.
	 *
	 * Warning: Label might be i18nized!
	 *
	 * @param label
	 */
	static void changeState(String label) {
		nextStateElements.find { it.text.contains(label) }.click()
		waitForAngularRequestsToFinish()
	}

	/**
	 * Returns all 'next state' elements,
	 * i.e. the state change buttons in the nuc-state component.
	 *
	 * @return
	 */
	private static List<NuclosWebElement> getNextStateElements() {
		$$('nuc-state button:enabled')
	}

	/**
	 * Returns all state change buttons of the layout.
	 *
	 * TODO: Those buttons are not exactly part of the state component, so this
	 * method should not be part of the state component.
	 */
	static List<StateButton> getStateButtons() {
		$$('nuc-web-button-change-state button').collect {
			new StateButton(
					element: it,
					label: it.text?.trim(),
					enabled: it.enabled
			)
		}
	}

	/**
	 * FIXME Doesn't work yet
	 *
	 * @return
	 */
	static List<StateButton> getActiveChangeStateButtons() {
		List<StateButton> ret = new ArrayList<>()
		for (StateButton sb : getStateButtons()) {
			if (sb.enabled) {
				ret.add(sb);
			}
		}
		return ret;
	}

	/**
	 * Clicks the 'OK' button of the state change confirmation dialog.
	 */
	static void confirmStateChange() {
		$('#button-ok').click()
		waitForAngularRequestsToFinish()
	}

	/**
	 * Clicks the 'Cancel' button of the state change confirmation dialog.
	 */
	static void cancelStateChange() {
		$('#button-cancel').click()
		waitForAngularRequestsToFinish()
	}

	static void changeStateByNumeral(final int stateNumber) {
		nextStateElements.find { it.getAttribute('state-number').toInteger() == stateNumber }.click()
		waitForAngularRequestsToFinish()
	}

	static class StateButton {
		NuclosWebElement element
		String label
		boolean enabled

		@Override
		String toString() {
			label
		}

		void click() {
			element.click()
		}
	}

	private static NuclosWebElement getCurrentStateElement() {
		$('#nuclosCurrentState')
	}
}
