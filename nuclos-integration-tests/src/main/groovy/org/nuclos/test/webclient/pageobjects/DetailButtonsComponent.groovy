package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class DetailButtonsComponent extends AbstractPageObject {
	static String getPopoverTitle() {
		$('div.popover .popover-title')?.text?.trim()
	}

	static String getPopoverText() {
		$('div.popover .popover-content')?.text?.trim()
	}
}
