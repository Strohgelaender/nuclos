package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * Represents the menu component, should always be visible.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class MenuComponent extends AbstractPageObject {
	/**
	 * Returns the trimmed text of preference menu item.
	 * The user menu must be toggled before the link text can be read.
	 *
	 * @return
	 */
	static String getPreferenceLinkText() {
		$('a[href="#/preferences"]').text.trim()
	}

	/**
	 * Clicks on the user menu dropdown to toggle it.
	 */
	static void toggleUserMenu() {
		$('#user-menu a.dropdown-toggle').click()
	}

	static void clickChangePasswordLink() {
		$('a[href="#/account/changePassword"]').click()
	}

	static void clickOpenPreferencesResetModal() {
		$('#open-preferences-reset-modal').click()
	}

	static void clickOpenPreferencesManage() {
		getOpenPreferencesManage().click()
	}

	static NuclosWebElement getOpenPreferencesManage() {
		$('#open-preferences-manage')
	}
}
