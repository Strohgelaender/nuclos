package org.nuclos.test.webclient.pageobjects.perspective

import static AbstractWebclientTest.$

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic
import groovy.transform.Immutable

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Immutable
@CompileStatic
class Perspective extends AbstractPageObject {
	private NuclosWebElement element

	/**
	 * The preference ID for this perspective.
	 */
	String id

	String name

	boolean isSelected() {
		element.hasClass('btn-info')
	}

	/**
	 * Edit this perspective.
	 * Performs a mouse move to the perspective button (to toggle the edit/delete overlay)
	 * and clicks the edit button (which opens the edit modal).
	 *
	 * @return
	 */
	PerspectiveModal edit() {
		showHoverMenu()

		$("#editPerspective_$id").click()

		new PerspectiveModal()
	}

	private void showHoverMenu() {
		Actions builder = new Actions(AbstractWebclientTest.driver)
		builder.moveToElement($("#perspective_$id").element).perform()
	}

	void delete() {
		showHoverMenu()
		$("#deletePerspective_$id").click()
	}

	void toggle() {
		$("#perspective_$id").click()
	}
}
