package org.nuclos.test.webclient.pageobjects.preference

import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic
import groovy.transform.Immutable

/**
 * Represents a user group with which a selected {@link PreferenceItem} can be shared.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Immutable
@CompileStatic
class PreferenceGroup {
	private WebElement element
	String name
	boolean selected

	void toggle() {
		element.findElement(By.tagName('input')).click()
	}
}
