package org.nuclos.test.webclient.pageobjects.dashboard


import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class DashboardItem extends AbstractPageObject {
	private final String type

	private final int x
	private final int y
	private final Integer rows
	private final Integer cols
}