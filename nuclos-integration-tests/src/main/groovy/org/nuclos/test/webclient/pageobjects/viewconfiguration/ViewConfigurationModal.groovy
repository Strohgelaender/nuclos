package org.nuclos.test.webclient.pageobjects.viewconfiguration

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.waitFor

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

@CompileStatic
class ViewConfigurationModal extends AbstractPageObject {

	static void open() {
		if ($$('nuc-view-preferences-modal').empty) {
			$$('.view-preferences-button-mainbar')[0].click()
		}
	}

	static void close() {
		$('#button-ok').click()
	}

	static void clickIfCollapsed(final String selector) {
		NuclosWebElement element = $(selector)
		if (element?.hasClass('collapsed')) {
			element.click()

			// wait until CSS transition of bootstrap accordion is completed
			waitFor{ !$('.collapsing') }
		}
	}
}