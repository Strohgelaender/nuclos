package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.openqa.selenium.WebElement

import groovy.transform.Immutable

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Immutable
class MessageModal {
	String title
	String message

	void confirm() {
		WebElement we = $('.modal-dialog #button-ok')

		we.click()
	}
}
