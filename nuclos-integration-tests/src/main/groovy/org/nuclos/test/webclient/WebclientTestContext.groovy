package org.nuclos.test.webclient

import java.text.DateFormat
import java.text.SimpleDateFormat

import org.nuclos.test.log.Log
import org.openqa.selenium.Dimension
import org.openqa.selenium.remote.RemoteWebDriver

import com.browserstack.local.Local

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class WebclientTestContext {
	/**
	 * The default timeout used by waitForAngular etc.
	 */
	final int DEFAULT_TMEOUT = 60

	RemoteWebDriver driver

	// The following settings can be overriden with System Properties
	String nuclosWebclientProtocol = initFromSystemProperty('nuclos.webclient.protocol', 'http')
	String nuclosWebclientHost = initFromSystemProperty('nuclos.webclient.host', '127.0.0.1')
	String nuclosWebclientPort = initFromSystemProperty('nuclos.webclient.port', '4200')
	String nuclosServerProtocol = initFromSystemProperty('nuclos.server.protocol', 'http')
	String nuclosServerHost = initFromSystemProperty('nuclos.server.host', nuclosWebclientHost)
	String nuclosServerPort = initFromSystemProperty('nuclos.server.port', '8080')
	String nuclosServerContext = initFromSystemProperty('nuclos.server.context', 'nuclos-war')
	String seleniumServer = initFromSystemProperty('selenium.server', 'http://127.0.0.1:4444/wd/hub')
	Browser browser = initFromSystemProperty('browser', { String name ->
		Browser.find {
			"$it".toLowerCase() == name.toLowerCase()
		}
	}, Browser.FIREFOX)
	Locale locale = initFromSystemProperty('locale', { String name ->
		DateFormat.getAvailableLocales().find {
			it.toString() == name
		}
	}, Locale.GERMANY)
	DateFormat dateFormat = locale.language == 'en' ? new SimpleDateFormat('MM/dd/yyyy') : new SimpleDateFormat('dd.MM.yyyy')

	String nuclosServer = "$nuclosServerProtocol://$nuclosServerHost:$nuclosServerPort/$nuclosServerContext"
	String nuclosWebclientServer = "$nuclosWebclientProtocol://$nuclosWebclientHost:$nuclosWebclientPort"
	String baseUrl = "$nuclosWebclientServer/index.html"
	String jscoverageClearStorageUrl = "$nuclosWebclientServer/jscoverage-clear-local-storage.html"

	Dimension preferredWindowSize = new Dimension(1024, 768)

	private <T> T initFromSystemProperty(String propertyName, T defaultValue) {
		initFromSystemProperty(propertyName, null, defaultValue)
	}

	String nuclosWebclientBaseHref = initFromSystemProperty('nuclos.webclient.basehref', '')
	String nuclosWebclientBaseURL = "$nuclosWebclientServer$nuclosWebclientBaseHref/index.html"

	boolean takeScreenshots = initFromSystemProperty('nuclos.webclient.screenshots', false)

	BrowserProxy browserProxy

	String browserstackBuild = initFromSystemProperty('browserstack.build', null)
	String browserstackProject = initFromSystemProperty('browserstack.project', null)
	String browserstackUser = initFromSystemProperty('browserstack.user', null)
	String browserstackKey = initFromSystemProperty('browserstack.key', null)
	Map<String, String> browserstackArgs
	Local browserstackLocal


	/**
	 * Tries to read the system property with the given name.
	 *
	 * @param propertyName
	 * @param initClosure Optional, will be applied to the system property value if not null
	 * @param defaultValue
	 * @return
	 */
	private <T> T initFromSystemProperty(String propertyName, Closure initClosure, def defaultValue) {
		String property = System.getProperty(propertyName)
		def result

		if (initClosure) {
			if (property) {
				result = initClosure(property)
			}
		} else {
			result = property
		}

		if (property == null) {
			result = defaultValue
		}

		Log.info "$propertyName = $result"

		return result
	}
}
