package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.context

import org.openqa.selenium.By

import groovy.transform.CompileStatic

/**
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LoadingIndicatorComponent extends AbstractPageObject {
	static boolean isLoading() {
		// Not using "$" here, because it waits for angular
		String src = context.driver.findElement(By.cssSelector('nuc-loading-indicator img')).getAttribute('src')
		println "src = $src"
		src.contains('rotating')
	}
}
