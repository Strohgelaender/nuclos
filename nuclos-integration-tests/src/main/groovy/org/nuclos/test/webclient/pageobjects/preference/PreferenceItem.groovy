package org.nuclos.test.webclient.pageobjects.preference

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.AbstractWebclientTest
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic
import groovy.transform.Immutable

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Immutable
@CompileStatic
class PreferenceItem {
	private WebElement element
	String name
	PreferenceType type
	boolean shared
	boolean customized
	String businessObject
	int rowIndex // starting from 1

	/**
	 * Selects this {@link PreferenceItem}.
	 * This must happen before it can be shared with users groups.
	 */
	void select() {
		element.click()
		waitForAngularRequestsToFinish()
	}

	/**
	 * Searches for a PreferenceGroup with the given name and shares
	 * this {@link PreferenceItem} with it.
	 * This only works correctly if the item is selected.
	 *
	 * @param groupName
	 */
	void shareWith(String groupName) {
		PreferenceGroup group = groups.find { it.name == groupName }
		if (!group.selected) {
			group.toggle()
			waitForAngularRequestsToFinish()
		}
	}

	void unshareFrom(String groupName) {
		PreferenceGroup group = groups.find { it.name == groupName }
		if (group.selected) {
			group.toggle()
			waitForAngularRequestsToFinish()
		}
	}

	void publishChanges() {
		getShareIcon(rowIndex).click()
		waitForAngularRequestsToFinish()
	}

	void discardChanges() {
		getDiscardIcon(rowIndex).click()
		waitForAngularRequestsToFinish()
		AbstractWebclientTest.getMessageModal().confirm()
	}

	void resetLayoutAssignment() {
		$('#reset-layout-assignment').click()
		waitForAngularRequestsToFinish()
	}

	static WebElement getShareIcon(rowIndex) {
		def items = $('.ag-pinned-right-cols-container .ag-row:nth-child(' + rowIndex + ')')
				.findElements(By.cssSelector('.update-preference-share'))
		items.size() == 1 ? items[0] : null
	}

	static WebElement getDiscardIcon(rowIndex) {
		def items = $('.ag-pinned-right-cols-container .ag-row:nth-child(' + rowIndex + ')')
				.findElements(By.cssSelector('.delete-customized-preference-item'))
		items.size() == 1 ? items[0] : null
	}


	/**
	 * Fetches all available user groups with which this item can be shared.
	 * This only works after the item is selected.
	 *
	 * @return
	 */
	List<PreferenceGroup> getGroups() {
		$$('.list-group .list-group-item').collect {
			String name = it.text.trim()
			boolean selected = !!it.findElement(By.cssSelector('input')).getAttribute('checked')

			new PreferenceGroup(
					element: it,
					name: name,
					selected: selected
			)
		}
	}
}
