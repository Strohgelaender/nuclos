package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.$$
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

/**
 * Represents the sidebar component.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Sidebar extends AbstractPageObject {

	static List<String> getListEntries() {
		entryElements*.text
	}

	/**
	 * Gets the count of entries (EntityObjects) that are currently shown in the Sidebar.
	 */
	static int getListEntryCount() {
		waitForAngularRequestsToFinish()
		entryElements.size()
	}

	static void selectEntry(int index) {
		entryElements[index].click()
	}

	static void selectEntryByText(String text) {
		findEntryByText(text).click()
	}

	static NuclosWebElement findEntryByText(String text) {
		entryElements.find {
			it.$$('.ag-cell').find {
				it.text?.trim() == text
			}
		}
	}

	static int getSelectedIndex() {
		entryElements.findIndexOf { NuclosWebElement it ->
			it.hasClass('ag-row-selected')
		}
	}

	static String getValue(int rowIndex, int colIndex) {
		$$('nuc-sidebar ag-grid-angular .ag-body-container [row-index="' + rowIndex + '"] .ag-cell')[colIndex].text
	}

	static String getValueByColumnLabel(int rowIndex, String colName) {
		def colIndex = $$('nuc-sidebar ag-grid-angular .ag-header-container .nuclos-grid-header-text').findIndexOf {
			WebElement option -> option.text?.trim() == colName
		}

		getValue(rowIndex, colIndex)
	}

	static String getValue(int rowIndex, String attr) {
		$$('nuc-sidebar ag-grid-angular [row-index=' + rowIndex + '] [col-id="' + attr + '"]').text
	}

	private static List<NuclosWebElement> getEntryElements() {
		$$('nuc-sidebar ag-grid-angular .ag-body-container [row-index]')
	}


	static void addColumn(String boMetaId, String name) {
		SideviewConfiguration.open()

		String attributeFqn = boMetaId + '_' + name
		SideviewConfiguration.selectColumn(attributeFqn)

		SideviewConfiguration.close()
	}

	static void moveColumnLeft(String boMetaId, String name) {
		moveColumn(boMetaId, name, false)
	}

	static void moveColumnRight(String boMetaId, String name) {
		moveColumn(boMetaId, name, true)
	}

	private static void moveColumn(String boMetaId, String name, boolean right) {
		SideviewConfiguration.open()

		String attributeFqn = boMetaId + '_' + name
		SideviewConfiguration.moveColumn(attributeFqn, right)

		SideviewConfiguration.close()
	}

	/**
	 * WARNING returns only visible columns
	 */
	static List<String> selectedColumns() {
		def result = []
		$$('nuc-sidebar ag-grid-angular .ag-header-row .nuclos-grid-header-text').each {
			result.add(it.text)
		}
		return result
	}

	static List<String> getColumnHeaders() {
		$$('nuc-sidebar ag-grid-angular .ag-header-row .nuclos-grid-header-text')*.text*.trim().findAll{ it }
	}

	static void resizeSidebarComponent(int newWidth) {
		NuclosWebElement e = getResizeBar()
		int x = e.getLocation().getX()

		new Actions(AbstractWebclientTest.getDriver())
				.dragAndDropBy(e.element, newWidth - x, 0)
				.build()
				.perform()

		AbstractWebclientTest.waitFor {
			getResizeBar().getLocation().getX() == newWidth
		}

		// wait for ag-grid in sidebar to get ready
		sleep(500)
	}

	static NuclosWebElement getResizeBar() {
		$('#sidebar .resize-handle')
	}

	static List<NuclosWebElement> getTreeRows() {
		WebElement we = $('#sidetree')
		assert we != null

		WebElement we2 = $(we, '.ag-body-container')
		assert we2 != null

		List<NuclosWebElement> lstWe = $$(we2, '.ag-row')
		return lstWe
	}

	static boolean isEntryMarkedDirty(int index) {
		// check if background is yellow
		entryElements[index].element.getCssValue('background-color').toString().equals('rgba(255, 255, 170, 1)')
	}
}
