package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.schema.layout.web.WebCombobox
import org.nuclos.schema.layout.web.WebListofvalues
import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

@CompileStatic
class ListOfValues {

	String attributeName
	NuclosWebElement lov

	static ListOfValues findByAttribute(String attributeName) {
		NuclosWebElement element = EntityObjectComponent.forDetail().getAttributeElement(attributeName, WebListofvalues)
		if (!element) {
			element = element = EntityObjectComponent.forDetail().getAttributeElement(attributeName, WebCombobox)
		}
		fromElement(
				attributeName,
				element
		)
	}

	static ListOfValues findByAttributeInModal(final String attributeName
	) {
		NuclosWebElement element = EntityObjectComponent.forModal().getAttributeElement(attributeName)
		fromElement(
				attributeName,
				element
		)
	}

	static ListOfValues fromElement(
			String attributeName,
			NuclosWebElement element
	) {
		if (!element) {
			return null
		}

		new ListOfValues(
				attributeName: attributeName,
				lov: element
		)
	}

	ListOfValues() {}

	void open() {
		if (!open) {
			NuclosWebElement button = dropdownButton
			if (button) {
				button.click()
			} else {
				lov.click()
			}
			waitFor {
				if (open) {
					return true
				}
				// FIXME: This is a workaround for NUCLOS-7243 - a second click should not be necessary.
				lov.click()
			}
		}
	}

	void clickDropdownButton() {
		dropdownButton.click()
	}

	void close() {
		if (open) {
			lovWidget.$('input').click()
		}
	}

	void closeViaEscape() {
		if (open) {
			lovWidget.$('input').sendKeys(Keys.ESCAPE)
		}
	}

	void selectEntry(String text) {
		if (!open) {
			open()
		}

		def choices = getChoices()
		for (int i = 0; i < choices.size(); i++) {
			if (choices[i] == text) {
				NuclosWebElement element = choiceElements[i];
				NuclosWebElement lovPanel = getLovPanel();

				while (!element.isDisplayed() && lovPanel.canScrollBottom()) {
					lovPanel.scrollBottom()
					sleep(100)
				}

				if (element.isDisplayed()) {
					element.click();
					try {
						// Sometimes a fragment remains ... close it
						if (open) {
							close()
						}
					} catch(Exception e) {
					}
				} else {
					// It is possible that the component is not completely displayed in the window
					// and extends beyond the bottom edge.
					// TODO find a solution
					throw new NoSuchElementException("No such visible option in dropdown: $text")
				}
				return
			}
		}

		throw new NoSuchElementException("No such option in dropdown: $text")
	}

	boolean hasEntry(String text) {
		open()
		def choices = getChoices()
		for (int i = 0; i < choices.size(); i++) {
			if (choices[i] == text) {
				return true
			}
		}
		return false
	}

	/**
	 * Returns the text of the currently highlighted entry (if the dropdown is visible).
	 *
	 * @return
	 */
	String getHighlightedEntry() {
		trimText(lovPanel.$('li.ui-state-highlight')?.text)
	}

	String getSelectedEntryFromDropdown() {
		trimText(lovPanel.$('li > div.selected')?.text)
	}

	/**
	 * Returns the current textual value of this LOV.
	 *
	 * @return
	 */
	String getTextValue() {
		trimText(inputElement.getAttribute('value'))
	}

	/**
	 * Enters the given search text into the input field.
	 */
	String search(String text) {
		inputElement.clear()
		inputElement.sendKeys(text)
		waitForAngularRequestsToFinish()
	}

	NuclosWebElement getInputElement() {
		lovContainer.$('input')
	}

	List<NuclosWebElement> getChoiceElements() {
		NuclosWebElement ul = lovPanel.$$('ul').find { it.displayed }

		if (!ul) {
			return []
		}

		// TODO: This is not very performant. Unify the dropdown component (NUCLOS-6571) and use a class selector here.
		// The reason for not using simply a "li" selector is that there can be an additional li element for
		// the empty/loading message, which is just plain text.
		ul.$$('.dd-item')*.parent
	}

	List<String> getChoices() {
		List<String> result = [];
		List<NuclosWebElement> choiceElements = getChoiceElements();
		for (int i = 0; i < choiceElements.size(); i++) {
			NuclosWebElement element = choiceElements[i]
			String s = element.getAttribute('innerText');
			s = s.replace('\u200B','')
			s = s.trim()
			result.add(s);
		}
		result
	}

	boolean isOpen() {
		lovPanel?.displayed
	}

	NuclosWebElement getLovContainer() {
		lov
	}

	String getComponentId() {
		lovContainer?.getAttribute('for-id')
	}

	NuclosWebElement getLovPanel() {
		$('.ui-autocomplete-panel[for-id="' + componentId + '"]')
	}

	NuclosWebElement getLovWidget() {
		lovContainer?.$('.ui-autocomplete.ui-widget')
	}

	NuclosWebElement getDropdownButton() {
		lovContainer?.$('.ui-autocomplete-dropdown')
	}

	boolean isSearchVisible() {
		searchElement
	}

	NuclosWebElement getSearchElement() {
		lov.$(".search-reference")
	}

	boolean isAddVisible() {
		addElement
	}

	NuclosWebElement getAddElement() {
		lov.$(".add-reference")
	}

	boolean isOpenVisible() {
		openElement
	}

	NuclosWebElement getOpenElement() {
		lov.$(".edit-reference")
	}

	void searchReference() {
		hoverLovOverlay()
		searchElement.click()
	}

	void addReference() {
		hoverLovOverlay()
		addElement.click()
	}

	void openReference() {
		hoverLovOverlay()
		openElement.click()
	}

	private void hoverLovOverlay() {
		new Actions(driver).moveToElement(lov.$('input').element).build().perform()

		// lov icons are shown delayed
		sleep(2000)
	}

	void blur() {
		inputElement.blur()
	}

	private String trimText(String text) {
		return text?.replace('\u200B', '')?.trim()
	}

	void sendKeys(final CharSequence... keysToSend) {
		inputElement.sendKeys(keysToSend)
		waitForAngularRequestsToFinish()
	}
}
