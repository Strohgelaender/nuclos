package org.nuclos.test.webclient.pageobjects.chart

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.Modal
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class ChartModal extends Modal {
	static List<NuclosWebElement> getTabs() {
		$$(modalSelector + ' .chart-tab-title')
	}

	static List<String> getTabTitles() {
		tabs*.text
	}

	static int getChartCount() {
		tabs.size()
	}

	static void newChart() {
		$(modalSelector + ' #newChart').click()
	}

	static int getSelectedChartIndex() {
		$(modalSelector + ' .tab-title.active > .chart-tab-title')?.getAttribute('index')?.toInteger()
	}

	private static NuclosWebElement getSaveButton() {
		$(modalSelector + ' #saveChart')
	}

	static void saveChart() {
		saveButton.click()
	}

	static boolean canSave() {
		saveButton?.displayed
	}

	static void deleteChart() {
		$(modalSelector + ' #deleteChart').click()
	}

	static void setChartName(String name) {
		clickTab('chart-tab-options')
		$(modalSelector + ' #chartName').with {
			it.clear()
			it.sendKeys(name)
		}
	}

	static String getChartName() {
		clickTab('chart-tab-options')
		$(modalSelector + ' #chartName').getAttribute('value')
	}

	static void setChartOptions(String chartOptions) {
		clickTab('chart-tab-extended')
		$(modalSelector + ' #chartOptions').value = chartOptions
	}

	static boolean isChartVisible() {
		// A chart element might be visible, but invalid configuration prevents the chart from rendering properly.
		// Assuming every chart contains some labels, check if we can find any text.
		$(modalSelector + ' #chartView')?.text?.trim()
	}

	private static void clickTab(String tabId) {
		$("$modalSelector #$tabId").click()
	}

	static boolean isMultibarChartVisible() {
		$(modalSelector + ' svg .nv-multiBarWithLegend')
	}

	static boolean isConfigurationValid() {
		!$(modalSelector + ' .alert.alert-danger')
	}

	static String getReferenceAttributeId() {
		clickTab('chart-tab-options')
		new Select($(modalSelector + ' #referenceAttributeId')).firstSelectedOption.text?.trim()
	}

	static void setReferenceAttributeId(String attributeId) {
		clickTab('chart-tab-options')
		new Select($(modalSelector + ' #referenceAttributeId')).selectByValue(attributeId)
	}

	static String getCategoryAttributeId() {
		clickTab('chart-tab-options')
		new Select($(modalSelector + ' #categoryAttributeId')).firstSelectedOption.text?.trim()
	}

	static void setCategoryAttributeId(String attributeId) {
		clickTab('chart-tab-options')
		new Select($(modalSelector + ' #categoryAttributeId')).selectByValue(attributeId)
	}

	static String getChartType() {
		$(modalSelector + ' .chart-type.active')?.id
	}

	static void setChartType(final String chartType) {
		$("$modalSelector .chart-type[id=\"$chartType\"]").click()
	}

	static void newSeries() {
		$(modalSelector + ' #newSeries').click()
	}

	static int getChartSeriesCount() {
		$(modalSelector + ' #chartSeriesCount').value?.toInteger()
	}

	static void removeSeries() {
		$(modalSelector + ' #removeSeries').click()
	}

	static String getSeriesAttributeId() {
		new Select($(modalSelector + ' #seriesAttributeId')).firstSelectedOption.text?.trim()
	}

	static void setSeriesAttributeId(String attributeId) {
		new Select($(modalSelector + ' #seriesAttributeId')).selectByValue(attributeId)
	}

	static int getChartDataCount() {
		$(modalSelector + ' #chartDataCount').value?.toInteger()
	}

	static void setFilter(final int index, final String value) {
		$$(modalSelector + " .chart-filter-item input").get(index).with {
			it.clear()
			it.click()
		}
		sendKeys(value)
	}

	static void applyFilter() {
		$(modalSelector + ' #buttonChartSearch').click()
	}

	static boolean isExportAvailable() {
		$(modalSelector + ' #exportChart')?.displayed
	}

	static void exportChart() {
		$(modalSelector + ' #exportChart').click()
	}
}
