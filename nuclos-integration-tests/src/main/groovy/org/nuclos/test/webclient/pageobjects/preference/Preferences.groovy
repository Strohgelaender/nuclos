package org.nuclos.test.webclient.pageobjects.preference

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.nuclos.test.webclient.pageobjects.AbstractPageObject
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * Represents the Preferences page and provides methods for accessing and manipulating existing preferences.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Preferences extends AbstractPageObject {

	static void open() {
		getUrlHash('#preferences')
	}

	/**
	 * Extracts all {@link PreferenceItem}s from the current page.
	 *
	 * TODO: After every modification the corresponding WebElements become stale and have to be fetched again,
	 * because the view is completely rebuilt after every modification...
	 *
	 * @return
	 */
	static List<PreferenceItem> getPreferenceItems() {
		def index = 0
		$$('.ag-body-container .ag-row').collect {//index, WebElement it ->
			index ++

			boolean shared = !it.findElements(By.cssSelector('.fa-globe')).empty

			PreferenceType type = null

			String typeName = it.findElement(By.cssSelector('nuc-type-renderer i')).getAttribute('data-type')?.toUpperCase()
			if (typeName) {
				type = PreferenceType.valueOf(typeName.replace('-', ''))
			}

			String businessObject = it.findElement(By.cssSelector('.ag-row [col-id="boName"]'))?.text.trim()

			String name = it.findElement(By.cssSelector('.ag-row [col-id="name"]')).text.trim()
			def pref = new PreferenceItem(
					element: it,
					shared: shared,
					customized: PreferenceItem.getShareIcon(index) != null,
					type: type,
					businessObject: businessObject,
					name: name,
					rowIndex: index
			)
			return pref
		}
	}

	/**
	 * Searches for a {@link PreferenceItem} with the given name and type,
	 * selects it and shares it with the given user group.
	 *
	 * @param type
	 * @param name
	 * @param group
	 */
	static void shareItem(PreferenceType type, String name, String group) {

		// filter by name - needed for large grid data to get requested item to display
		Preferences.filter('name', name)

		PreferenceItem item = preferenceItems.find {
			it.type == type && it.name == name
		}
		item.select()
		item.shareWith(group)
	}

	static void filter(String columnName, String value) {
		def header = $('.ag-header-container')
		def children = header.findElements(By.cssSelector('[col-id]'))
		def headerCols = header.findElements(By.cssSelector('[col-id="name"]'))
		if (headerCols.size() == 1) {
			def index = children.indexOf(headerCols[0])
			WebElement input = $$('.ag-header-container .ag-header-row:nth-child(2) .ag-header-cell input')[index]
			input.clear()
			input.sendKeys(value)
		}
		waitForAngularRequestsToFinish()
	}
}
