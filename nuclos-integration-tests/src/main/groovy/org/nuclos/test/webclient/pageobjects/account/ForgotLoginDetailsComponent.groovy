package org.nuclos.test.webclient.pageobjects.account

import static org.nuclos.test.webclient.AbstractWebclientTest.$

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.AbstractPageObject

import groovy.transform.CompileStatic

@CompileStatic
class ForgotLoginDetailsComponent extends AbstractPageObject {

	static enum TYPE {
		FORGOT_USERNAME,
		FORGOT_PASSWORD
	}

	static TYPE getType() {
		radioUsername.selected ? TYPE.FORGOT_USERNAME : TYPE.FORGOT_PASSWORD
	}

	static void setType(TYPE type) {
		if (type == TYPE.FORGOT_USERNAME) {
			radioUsername.click()
		} else {
			radioPassword.click()
		}
	}

	static NuclosWebElement getRadioUsername() {
		$('#radioUsername')
	}

	static NuclosWebElement getRadioPassword() {
		$('#radioPassword')
	}

	static String getUsername() {
		usernameField.text
	}

	static void setUsername(String username) {
		usernameField.value = username
	}

	static NuclosWebElement getUsernameField() {
		$('#username')
	}

	static String getEmail() {
		emailField.text
	}

	static void setEmail(String email) {
		emailField.value = email
	}

	static NuclosWebElement getEmailField() {
		$('#email')
	}

	static void submit() {
		submitButton.click()
	}

	static NuclosWebElement getSubmitButton() {
		$('button[type="submit"]')
	}

	static String getErrorMessage() {
		errorMessageElement?.text
	}

	static NuclosWebElement getErrorMessageElement() {
		$('div.alert.alert-danger')
	}

	static String getSuccessMessage() {
		successMessageElement?.text
	}

	static NuclosWebElement getSuccessMessageElement() {
		$('div.alert.alert-success')
	}

	static void submitAndAssertError(List<String> expectedMessage = null) {
		submit()

		String errorMessage = errorMessage
		assert errorMessage

		if (expectedMessage) {
			assert errorMessage && expectedMessage.find { errorMessage.contains(it) }
		}
	}

	static void clickLoginLink() {
		$('a[href$="/login"]').click()
	}
}
