package org.nuclos.test.webclient


import java.util.concurrent.TimeUnit

import org.codehaus.groovy.reflection.ReflectionUtils
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.request.RequestCounts
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.MessageModal
import org.nuclos.test.webclient.util.Screenshot
import org.nuclos.test.webclient.utils.Utils
import org.openqa.selenium.*
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.logging.LogEntries
import org.openqa.selenium.logging.LogEntry
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.Select

import com.browserstack.local.Local

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.Har
import net.lightbody.bmp.proxy.CaptureType

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractWebclientTest extends AbstractNuclosTest {

	static WebclientTestContext context = new WebclientTestContext()

	/**
	 * Callback script for Selenium that waits for the Angular 2 app to become stable
	 * and have no pending macro tasks (e.g. asynchronous requests).
	 */
	static final String WAIT_FOR_ANGULAR_SCRIPT = '''
// Check if we really are on a Nuclos Webclient page
if (document.getElementsByTagName('nuc-root').length == 0) {
	return true;
}

// NgZone may not be available yet, we have to retry later
if (!window['NgZone']) {
	console.error('No NgZone!');
	return false;
}

if (NgZone.isStable && !NgZone.hasPendingMacrotasks){
	return true;
} else {
	console.log('waitForAngular: has macro tasks');
	return false;
}
'''

	// TODO: Adjust for new Webclient!
	static final String WEBCLIENT_CSS = '''
#detailblock #sideview-statusbar-content:before {
	opacity: 0.3;
	content: 'statusbar with timestamp disabled for screenshot diffs';
}
'''

	@BeforeClass
	static void setup() {
		Runtime.addShutdownHook {
			shutdown()
		}

		setup(true, true)
	}

	static void setup(boolean withTestUserLogin, boolean setCookieAccepted) {
		Log.debug 'Setup'

		AbstractNuclosTest.setup()

		try {
			setupSystemParameters()

			startProxy()
			startBrowserstackLocal()
			setupDriver:
			{
				Proxy proxy = context.browserProxy.getSeleniumProxy(context.nuclosWebclientHost)
				context.driver = context.browser.createDriver(proxy)
				context.driver.setFileDetector(new LocalFileDetector())
				context.driver.manage().timeouts().setScriptTimeout(context.DEFAULT_TMEOUT, TimeUnit.SECONDS)
			}

			resizeBrowserWindow()
			getUrl(context.jscoverageClearStorageUrl)
			openStartpage()
			waitForAngularRequestsToFinish()

			// clear localStorage - needed for phantomjs
			executeScript('localStorage.clear();')

			if (setCookieAccepted) {
				WebElement we = $('#acceptcookies')
				if (we != null && we.isDisplayed()) {
					we.click()
				}
			}

			nuclosSession.managementConsole('resetCompleteMandatorSetup')

			if (withTestUserLogin) {
				RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
				login('test', 'test')
			}

			if (setCookieAccepted) {
				assert $('#acceptcookies') == null
			}

		}

		catch (Throwable ex) {
			fail(ex.message, ex)
		}

		Log.debug 'Setup done'
	}

	@AfterClass
	static void teardown() {
		waitForAngularRequestsToFinish()
		try {
			executeScript("jscoverage_report();")
			executeAsyncScript('var callback = arguments[arguments.length - 1];callback();')
		}
		catch (Exception ignore) {
			Log.warn "Could not create jscoverage report"
		}

		shutdown()
	}

	static NuclosWebElement $(String selector) {
		Log.info("Selecting: \"$selector\"")
		waitForAngularRequestsToFinish()
		List<WebElement> elements = context.getDriver().findElements(By.cssSelector(selector))
		elements.empty ? null : new NuclosWebElement(elements.get(0))
	}

	static NuclosWebElement $(WebElement e, String selector) {
		List<WebElement> elements = e.findElements(By.cssSelector(selector))
		elements.empty ? null : new NuclosWebElement(elements.get(0))
	}

	static List<NuclosWebElement> $$(String selector) {
		Log.info("Selecting: \"$selector\"")
		context.getDriver().findElements(By.cssSelector(selector)).collect { new NuclosWebElement(it) }
	}

	static List<NuclosWebElement> $$(WebElement e, String selector) {
		e.findElements(By.cssSelector(selector)).collect { new NuclosWebElement(it) }
	}

	static void openStartpage() {
		getUrl(context.nuclosWebclientBaseURL)
	}

	static boolean login(String username = 'nuclos', boolean autologin = false) {
		return login(username, '', autologin)
	}

	private static void startProxy() {
		if (context.browserProxy?.started) {
			return
		}

		context.browserProxy = new BrowserProxy(context)
		context.browserProxy.start()
	}

	/**
	 * Counts all browser requests.
	 */
	static RequestCounts countRequests(Closure c) {
		context.browserProxy.countRequests(c)
	}

	/**
	 * Gets the HAR using default capture settings.
	 * Does not capture response content.
	 */
	static Har getHar(Closure c) {
		context.browserProxy.getHar([], c)
	}

	/**
	 * Gets the HAR using the given capture settings.
	 * Here you can capture resopnse content via {@link net.lightbody.bmp.proxy.CaptureType#RESPONSE_CONTENT}
	 */
	static Har getHar(List<CaptureType> captureTypes, Closure c) {
		context.browserProxy.getHar(captureTypes, c)
	}

	/**
	 * Starts the BrowserStack Local binary, which opens a tunnel to BrowserStack.
	 * This must be done for every test because of possibly different proxy settings.
	 */
	private static void startBrowserstackLocal() {
		if (getBrowserstackArgs()) {
			context.browserstackLocal = new Local()
			context.browserstackLocal.start(browserstackArgs)
		}
	}

	protected static Map<String, String> getBrowserstackArgs() {
		if (context.browserstackArgs) {
			return context.browserstackArgs
		}

		final String key = System.getProperty('browserstack.key')
		if (key) {
			context.browserstackArgs = [
					'key'         : key,
					'v'           : 'true',
					'force'       : 'true',
					'onlyAutomate': 'true',
			]

			if (context.browserProxy) {
				context.browserstackArgs['-local-proxy-host'] = context.browserProxy.hostname
				context.browserstackArgs['-local-proxy-port'] = context.browserProxy.port.toString()
				context.browserstackArgs['-force-proxy'] = 'true'
			}
		}

		return context.browserstackArgs
	}

	/**
	 * Performs a Login with the given credentials and the current locale, if not logged in already.
	 */
	static boolean login(String username, String password, boolean autologin = false, String mandator = null) {
		if (isPresent('#logout')) {
			return false
		}

		loginUnsafe(username, password, autologin, mandator)

		assert $('#logout')
		return true
	}

	static boolean isLoggedIn() {
		isPresent('#logout')
	}

	/**
	 * Performs a Login with the given credentials and the current locale.
	 * Without any checks for login success etc.
	 */
	static void loginUnsafe(String username, String password, boolean autologin = false, String mandator = null) {
		LocaleChooser.locale = context.locale

		AuthenticationComponent.username = username
		AuthenticationComponent.password = password
		AuthenticationComponent.autologin = autologin

		AuthenticationComponent.submit()

		if (mandator) {
			if (mandator == 'null') {
				// superuser login without any mandator selection
				$('#mandatorless-session').click()
			} else {
				WebElement chooseMandator = $('#choose-mandator')
				Select droplist = new Select(chooseMandator)
				droplist.selectByVisibleText(mandator)
				$('#choose-mandator-button').click()
			}
		}
	}

	/**
	 * Does not fail the build, if "waitForAngular" did not succeed.
	 */
	static void tryToWaitForAngular(int timeout) {
		Closure waitClosure = {
			windowClosed || executeScript(WAIT_FOR_ANGULAR_SCRIPT)
		}
		if (!doWaitFor(timeout, waitClosure)) {
			Log.warn("Failed to wait for angular requests to finish")
		}
	}

	static void waitForAngularRequestsToFinish() {
		waitForAngularRequestsToFinish(context.DEFAULT_TMEOUT)
	}

	/**
	 * Waits for Angular-2 to finish loading by waiting for the "onStable" event of NgZone (if not already stable).
	 * NgZone is set as a global Javascript variable by the Webclient2 app component.
	 *
	 * @param timeout
	 */
	static void waitForAngularRequestsToFinish(int timeout) {
		Log.debug "Waiting for Angular requests to finish"
		waitFor(timeout) {
			windowClosed || executeScript(WAIT_FOR_ANGULAR_SCRIPT)
		}
	}

	/**
	 * Waits for the given Closure to return true.
	 * Fails the build if the condition is not met within the given timeout.
	 */
	static boolean waitFor(int timeoutInSeconds, Closure condition) {
		if (!doWaitFor(timeoutInSeconds, condition)) {
			String source = Utils.closureToString(condition)
			fail('Failed to wait for condition: ' + source, new Throwable(source))
		}

		return true
	}

	static boolean waitFor(Closure condition) {
		waitFor(context.DEFAULT_TMEOUT, condition)
	}

	static String getCurrentUrl() {
		try {
			waitForAngularRequestsToFinish()
			return context.driver.getCurrentUrl()
		}
		catch (Throwable ex) {
			Log.error 'Could not get current URL', ex
		}

		return null
	}

	/**
	 * Takes a screenshot and saves it under the given name,
	 * if screenshots are enabled or "force" is true.
	 */
	static void screenshot(String name, boolean force = false) {
		if (!context.takeScreenshots && !force) {
			Log.debug("Skipping screenshot '$name'")
			return
		}

		waitForAngularRequestsToFinish()
		String caller = ReflectionUtils.callingClass.simpleName
		// get caller - ignore superclass in stacktrace
		for (def l = 0; l < 10; l++) {
			def callerName = ReflectionUtils.getCallingClass(l).name
			if (callerName.indexOf("AbstractWebclient") == -1) {
				caller = callerName
				break
			}
		}
		Screenshot.take(caller, name)
	}

	/**
	 * Forcefully fails the build immediately.
	 */
	static void fail(String failure, Throwable t = null) {
		FailureHandler.fail(failure, t)
	}

	static boolean logout() {
		if (!isPresent('#logout')) {
			return false
		}

		waitForAngularRequestsToFinish()

		$('#user-menu .dropdown').click()
		$('#logout').click()

		waitForAngularRequestsToFinish()

		assert !$('#logout')

		return true
	}

	static boolean isPresent(String selector) {
		try {
			def element = $(selector)
			return !!element
		}
		catch (Exception ignore) {
			return false
		}
	}

	/**
	 * Reloads the current page in the browser.
	 */
	static void refresh() {
		Log.info "Refreshing: $currentUrl"

		// getDriver().navigate().refresh() does not always work with PhantomJS
		if (context.getBrowser() != Browser.PHANTOMJS) {
			context.driver.navigate().refresh()
		} else {
			getUrl(currentUrl)
		}

		waitForAngularRequestsToFinish()
	}

	static void getUrl(String url) {
		Log.debug "Get: $url"
		context.getDriver().get(url)

		// Give the browser and app some time to start loading/redirecting
		sleep(1000)
	}

	/**
	 * @param hash The URL hash without leading '#'
	 */
	static void getUrlHash(String hash) {
		Log.debug "Get hash: $hash"
		try {
			executeScript("window.location.hash='$hash'")
			waitForAngularRequestsToFinish()
		}
		catch (Exception ex) {
			Log.warn "Could not set URL hash $hash", ex
		}
	}

	static void assertNotFound() {
		assertError('404')
	}

	static void assertAccessDenied() {
		assert context.driver.currentUrl.contains('/error/403')
		assertError('403')
	}

	static void assertError(String s = '') {
		String error = errorMessage

		assert error
		assert !error.empty
		assert error.contains(s)
	}

	static String getErrorMessage() {
		return $('nuc-error .card-header')?.text
	}

	/**
	 * Returns a currently displayed modal message dialog.
	 */
	static MessageModal getMessageModal() {
		if ($('.modal-dialog') == null) {
			return null
		}

		String title = $('.modal-dialog .modal-title').text.trim()
		String message = $('.modal-dialog .modal-body').text.trim()

		assert !(title ==~ /.*\{\d*}.*/), 'Message parameters were not resolved'
		assert !(message ==~ /.*\{\d*}.*/), 'Message parameters were not resolved'

		new MessageModal(title: title, message: message)
	}

	static void assertNoMessageModel() {
		MessageModal messageModal = getMessageModal()

		if (messageModal != null) {
			// show message in test result
			assert messageModal.message == null;
		}

		assert messageModal == null
	}

	static void assertMessageModalAndConfirm(String title, String partOfMessage) {
		MessageModal messageModal = getMessageModal()
		assert messageModal != null

		assert messageModal.title == title

		if (partOfMessage != null) {
			assert messageModal.message.contains(partOfMessage)
		}

		messageModal.confirm()

		assert $('.modal-dialog') == null
	}

	/**
	 * @return The handle of the newly opened window.
	 */
	static String duplicateCurrentWindow() {
		Set<String> handles = context.driver.windowHandles

		String url = currentUrl
		executeScript("window.open('$url', '_blank')")

		Set<String> newHandles = context.driver.windowHandles
		assert newHandles.size() == handles.size() + 1

		String newWindowHandle = (newHandles - handles).first()
		context.driver.switchTo().window(newWindowHandle)
		getUrl(url)

		return newWindowHandle
	}

	static boolean isWindowClosed() {
		try {
			!context.driver.windowHandles.contains(context.driver.windowHandle)
		} catch (NoSuchWindowException ignore) {
			return true
		}
	}

	static List<WebElement> luceneSearch(String s) {

		WebElement fullTextSearch = $('#full-text-search')
		assert fullTextSearch != null

		WebElement input = fullTextSearch.findElement(By.className('full-text-search-input'))
		assert input != null

		input.clear()
		input.sendKeys(s)

		screenshot('afterLuceneSearch')

		return fullTextSearch.findElements(By.className('full-text-search-result-item'))
	}

	static void luceneTest(String s, int expected) {
		waitForAngularRequestsToFinish()

		try {
			waitFor(10) {
				luceneSearch(s).size() == expected
			}
		} catch (Exception ignore) {
			def size = luceneSearch(s).size()
			screenshot('beforeExceptionInLuceneTest')
			assert size == expected
		}

		screenshot('afterLuceneSearchAndWait')
	}

	static void sendKeysAndWaitForAngular(CharSequence... keys) {
		sendKeys(keys)
		waitForAngularRequestsToFinish()
	}

	/**
	 * Sends the keys to the active element, how a real user would do it.
	 */
	static void sendKeys(CharSequence... keys) {
		new Actions(context.driver)
				.sendKeys(keys)
				.build()
				.perform()
	}

	/**
	 * Performs a general click at the location of the given element,
	 * like a real user would to it.
	 */
	static void click(NuclosWebElement e) {
		new Actions(context.driver)
				.click(e.element)
				.build()
				.perform()
	}

	private static boolean initialSetupExecuted = false

	static void setupSystemParameters() {
		if (!initialSetupExecuted) {
			// will be called once for all tests
			overrideCssForTests:
			{
				Map params = [WEBCLIENT_CSS: WEBCLIENT_CSS]
				getNuclosSession().setSystemParameters(params)
			}

			initialSetupExecuted = true
		}
	}

	static void shutdown() {
		try {
			context.driver?.quit()
		}
		catch (Throwable t) {
			Log.warn "Could not quit selenium driver", t
		}

		try {
			context.browserstackLocal?.stop()
		} catch (Exception e) {
			Log.warn 'Failed to stop BrowserStack Local', e
		}

		if (context.browserProxy?.started) {
			context.browserProxy?.stop()
		}
	}

	static void resizeBrowserWindow() {
		context.driver.manage().window().setSize(context.preferredWindowSize)
	}

	static void printBrowserDebugInfos() {
		if (!context.driver) {
			Log.warn 'Could not print browser debug infos - browser not started'
			return
		}

		printBrowserLog()
		printCookies()

		Log.error "Last URL: $currentUrl"
		screenshot('failure', true)
	}

	private static void printBrowserLog() {
		try {
			LogEntries logEntries = context.driver.manage().logs().get(LogType.BROWSER)
			for (LogEntry entry : logEntries) {
				Log.info "[$context.browser] " + new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage()
			}
		}
		catch (Exception ex) {
			Log.warn "Failed to get browser logs for $context.browser: $ex.message"
		}
	}

	private static void printCookies() {
		try {
			Log.info "Browser Cookies:"
			context.driver.manage().cookies.each {
				Log.info it.toString()
			}
		}
		catch (Exception ex) {
			Log.warn "Failed to get browser cookies for $context.browser: $ex.message"
		}
	}

	/**
	 * Deprecated: Use {@link #waitFor(Closure)} or a custom WebDriverWait instead.
	 *
	 * @param millis
	 */
	@Deprecated()
	static void sleep(long millis) {
		try {
			Thread.sleep(millis)
		} catch (InterruptedException e) {
			Log.error(e.getMessage(), e)
		}
	}

	/**
	 * Switches to another browser window, if more than 1 window exists.
	 *
	 * @return
	 */
	static String switchToOtherWindow() {
		def currentWindow = context.driver.windowHandle
		def otherWindow = context.driver.windowHandles.find { it != currentWindow }

		if (otherWindow) {
			context.driver.switchTo().window(otherWindow)
			resizeBrowserWindow()
			waitForAngularRequestsToFinish()
			return otherWindow
		}

		return null
	}

	static void closeOtherWindows() {
		def currentWindow = context.driver.windowHandle

		context.driver.windowHandles.each {
			if (it != currentWindow) {
				context.driver.switchTo().window(it)
				context.driver.close()
			}
		}

		context.driver.switchTo().window(currentWindow)
	}

	/**
	 * Waits for the given Closure to return true.
	 *
	 * @param timeoutInSeconds
	 * @param condition
	 * @return true , if the condition was met within the timeout
	 */
	static boolean doWaitFor(int timeoutInSeconds, Closure condition) {
		try {
			Thread t = new Thread() {
				@Override
				void run() {
					def result = condition()
					while (!result && !isInterrupted()) {
						Log.info "Waiting for condition ${condition.toString()}..."
						sleep(500)
						result = condition()
					}
				}
			}
			t.start()
			try {
				t.join(timeoutInSeconds * 1000)
			} catch (InterruptedException ex) {
				Log.warn "Thread interrupted", ex
			}

			if (t.alive) {
				t.interrupt()
				return false
			}
			return true
		} catch (Exception ex) {
			Log.warn("Failed to wait for condition", ex)
			return false
		}
	}

	static void blur() {
		executeScript('document.activeElement.blur();')
	}

	static Object executeScript(String script, Object... args) {
		((JavascriptExecutor) context.driver).executeScript(script, args)
	}

	static Object executeAsyncScript(String script, Object... args) {
		((JavascriptExecutor) context.driver).executeAsyncScript(script, args)
	}

	static boolean isHorizontalScrollbarPresent() {
		executeScript('return document.documentElement.scrollWidth > document.documentElement.clientWidth;')
	}

	static boolean isVerticalScrollbarPresent() {
		executeScript('return document.documentElement.scrollHeight > document.documentElement.clientHeight;')
	}

	static highlight(WebElement element) {
		executeScript('arguments[0].style.border="3px solid red"', element)
	}

	static RemoteWebDriver getDriver() {
		context.driver
	}

	static Locale getLocale() {
		context.locale
	}
}
