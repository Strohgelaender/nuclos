package org.nuclos.test.webclient.pageobjects.preference

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
enum PreferenceType {
	TABLE,
	SUBFORMTABLE,
	SEARCHTEMPLATE,
	CHART,
	PERSPECTIVE
}
