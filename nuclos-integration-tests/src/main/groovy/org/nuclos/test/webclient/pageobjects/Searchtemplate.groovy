package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.*

import org.apache.commons.lang.NotImplementedException
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SearchtemplateConfiguration
import org.nuclos.test.webclient.pageobjects.viewconfiguration.ViewConfigurationModal
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

/**
 * TODO: Remove this completely.
 */
@CompileStatic
@Deprecated
class Searchtemplate extends AbstractPageObject {

	static WebElement getSearchfilterValueTextInput(String attributeFqn) { $('#search-value-' + attributeFqn) }

	static WebElement getSearchfilterValueDatepickerInput() {
		def elems = $$('.searchfilter-popover-content [ngbdatepicker]')
		return elems.size() > 0 ? elems.get(0) : null
	}

	static class SearchTemplateItem {
		String name
		String operator
		String value
	}

	static void setSearchCondition(String attributeFqn, SearchTemplateItem searchTemplateItem) {
		// select operator
		if (searchTemplateItem.operator == null) { // boolean
			throw new NotImplementedException("boolean operator is not implemented");
			// TODO implement boolean operator
//			if (searchTemplateItem.value == 'true') {
//				$('.searchfilter-popover-content input[value="true"]').click()
//			}
		} else {
			// $('#search-operator-' + attributeFqn).findElement(By.id('operator-' + searchTemplateItem.operator)).click()
			def searchOp = context.getDriver().findElement(By.id('search-operator-' + attributeFqn))
			// def input = getSearchfilterValueTextInput(attributeFqn);
			def input = context.getDriver().findElement(By.id('search-value-' + attributeFqn));
			if (input != null) {
				input.clear()
				input.sendKeys('' + searchTemplateItem.value)
				searchOp.findElement(By.id('operator-' + searchTemplateItem.operator)).click()
			} else {
				throw new NotImplementedException("dropdown and datepicker values are not implemented")
			}

			// TODO implement dropdown and datepicker
//			List<NuclosWebElement> dropdowns = $$('.searchfilter-attribute-popover .dropdown')
//			if (dropdowns.size() > 0) {
//				// reference
//
//				ListOfValues lov = new ListOfValues()
//				lov.lov = dropdowns[0]
//				if (!lov.open) {
//					lov.open()
//				}
//				lov.selectEntry(searchTemplateItem.value)
//			} else {
//				def datepickerInput = getSearchfilterValueDatepickerInput()
//				if (datepickerInput != null) {
//					// datepicker
//					// TODO sendKeys will not update the model ????
//					datepickerInput.clear()
//					datepickerInput.sendKeys('' + searchTemplateItem.value + Keys.TAB)
//				}
//			}
		}
		waitForAngularRequestsToFinish()
	}
}