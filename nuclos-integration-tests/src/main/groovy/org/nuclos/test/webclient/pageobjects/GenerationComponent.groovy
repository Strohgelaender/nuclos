package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * Represents the 'Generator' component which is typically shown in the toolbar above an EO.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class GenerationComponent extends AbstractPageObject {

	/**
	 * Executes the object generator with the given name.
	 *
	 * @param name
	 */
	static int generateObjectAndConfirm(final String name) {
		int generatorCount

		NuclosWebElement container = $('nuc-generation')
		container.$('.dropdown-toggle').click()

		List<NuclosWebElement> lstWe = container.$$('.dropdown-item')
		generatorCount = lstWe.size()

		lstWe.find { it.text?.trim() == name }.click()

		waitForAngularRequestsToFinish()

		AbstractWebclientTest.messageModal.confirm()

		return generatorCount
	}

	/**
	 * If there is a result dialog, clicks on the result link.
	 */
	static void openResult() {
		$('#generation-result-dialog a')?.click()
	}
}
