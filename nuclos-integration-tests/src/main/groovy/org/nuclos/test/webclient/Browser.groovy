package org.nuclos.test.webclient

import org.nuclos.test.webclient.utils.WebDriverFactory
import org.openqa.selenium.Proxy
import org.openqa.selenium.remote.RemoteWebDriver

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum Browser {
	FIREFOX(WebDriverFactory.remoteFirefox),
	CHROME(WebDriverFactory.remoteChrome),
	IE(WebDriverFactory.remoteIE),
	PHANTOMJS(WebDriverFactory.remotePhantomJS),
	SAFARI(WebDriverFactory.remoteSafari)

	Closure<RemoteWebDriver> driverFactory

	Browser(Closure driverFactory) {
		this.driverFactory = driverFactory
	}

	RemoteWebDriver createDriver(Proxy proxy) {
		return driverFactory(proxy)
	}
}
