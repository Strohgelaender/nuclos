package org.nuclos.test.webclient.pageobjects.search

import static org.nuclos.test.webclient.AbstractWebclientTest.$
import static org.nuclos.test.webclient.AbstractWebclientTest.sendKeys
import static org.nuclos.test.webclient.AbstractWebclientTest.waitForAngularRequestsToFinish

import org.nuclos.test.EntityClass
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Searchbar {
	static void create() {
		openSearchEditor()
		$('#searchfilter-create').click()
	}

	static void editName(String name) {
		$('#meta-edit').click()
		$('#meta-editor-name-input').clear()
		sendKeys(name)
		$('#meta-edit').click()
	}

	static void delete() {
		$('#meta-edit').click()
		$('#meta-editor-delete-searchfilter').click()
		EntityObjectComponent.clickButtonOk()
	}

	static String getSelectedFilter() {
		searchfilterLov.textValue
	}

	static List<String> getSelectedSearchItems() {
	}

	static void selectSearchfilter(String name) {
		searchfilterLov.selectEntry(name)
	}

	static void selectAttribute(String attribute) {
		attributeLov.selectEntry(attribute)
	}

	static List<String> getFilters() {
		searchfilterLov.open()
		def choices = searchfilterLov.getChoices()
		searchfilterLov.close()
		return choices
	}

	/**
	 * TODO: Select the corresponding automatically when setting a search condition.
	 */
	static def setCondition(final EntityClass<?> entityClass, final Searchtemplate.SearchTemplateItem item) {
		openSearchEditor()
		Searchtemplate.setSearchCondition(entityClass.fqn + '_' + item.name, item)
	}

	static void clearSearchfilter() {
		searchfilterLov.selectEntry('')
	}

	static ListOfValues getSearchfilterLov() {
		openSearchEditor()
		ListOfValues.fromElement(
				null,
				$('nuc-search-filter-selector nuc-dropdown')
		)
	}

	static openSearchEditor() {
		if ($('#search-editor') == null) {
			$('.search-editor-button').click()
		}
	}

	static closeSearchEditor() {
		if ($('#search-editor') != null) {
			$('.search-editor-button').click()
		}
	}

	static ListOfValues getAttributeLov() {
		ListOfValues.fromElement(
				null,
				$('nuc-search-attribute-selector nuc-dropdown')
		)
	}

	static void removeAttribute(final String fqn) {
		openSearchEditor()
		$('#search-remove-' + fqn).click()
	}

	static WebElement getSearchfilter() {
		$('#text-search-input')
	}

	static void search(String searchString) {
		clearTextSearchfilter()
		searchfilter.sendKeys(searchString)

		waitForAngularRequestsToFinish()
	}

	static void clearTextSearchfilter() {
		if ($('.clear-searchfilter') != null) {
			$('.clear-searchfilter').click()
		}
	}
}
