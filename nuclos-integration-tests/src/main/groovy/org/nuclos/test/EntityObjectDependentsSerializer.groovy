package org.nuclos.test

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObjectDependentsSerializer extends StdSerializer<Map<String, List<EntityObject<?>>>> {
	protected EntityObjectDependentsSerializer() {
		super(Map.class)
	}

	@Override
	void serialize(
			final Map<String, List<EntityObject<?>>> dependentMap,
			final JsonGenerator gen,
			final SerializerProvider provider
	) throws IOException {
		gen.writeStartObject()

		writeSubBos:
		{
			insert: {
				gen.writeObjectFieldStart('insert')
				dependentMap.each { key, value ->
					gen.writeArrayFieldStart(key)
					value.findAll { it.new }.each {
						gen.writeObject(it)
					}
					gen.writeEndArray()
				}
				gen.writeEndObject()
			}

			update: {
				gen.writeObjectFieldStart('update')
				dependentMap.each { key, value ->
					gen.writeArrayFieldStart(key)
					value.findAll { it.update }.each {
						gen.writeObject(it)
					}
					gen.writeEndArray()
				}
				gen.writeEndObject()
			}

			delete: {
				gen.writeObjectFieldStart('delete')
				dependentMap.each { key, value ->
					gen.writeArrayFieldStart(key)
					value.findAll { it.delete }.each {
						gen.writeObject(it.id)
					}
					gen.writeEndArray()
				}
				gen.writeEndObject()
			}
		}

		gen.writeEndObject()
	}
}
