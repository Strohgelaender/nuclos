package org.nuclos.test

import static org.nuclos.test.log.CallTrace.trace

import javax.ws.rs.core.Response

import org.apache.logging.log4j.Level
import org.json.JSONObject
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.experimental.categories.Category
import org.junit.rules.TestRule
import org.junit.runners.MethodSorters
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.ServerLogger
import org.nuclos.test.rest.SuperuserRESTClient

import groovy.transform.CompileStatic
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractNuclosTest {
	static SuperuserRESTClient nuclosSession

	@Rule
	public final TestRule testRule = new NuclosTestRule()

	@BeforeClass
	static void setup() {
		Log.debug 'Setup'

		truncateTablesAndCaches()
		nuclosSession.managementConsole('disableIndexer')

		nuclosSession.setServerLogLevel(ServerLogger.SQL_TIMER, Level.DEBUG)
	}

	static SuperuserRESTClient getNuclosSession() {
		if (!nuclosSession) {
			trace('Login as "nuclos"') {
				nuclosSession = new SuperuserRESTClient('nuclos', '').login()
			}
		}

		return nuclosSession
	}

	static void truncateTablesAndCaches() {
		trace('Truncate tables and Caches') {
			Map truncateTable =
					[
							boMetaId  : 'nuclet_test_utils_TruncateTables',
							attributes: [
									'name': 'Truncate User Tables' + new Date().getTime()
							]
					]
			JSONObject bo = RESTHelper.createBo(truncateTable, getNuclosSession())
			bo.getJSONObject('attributes').put('nuclosState',  [id: 'nuclet_test_utils_TruncateTablesSM_State_20'])
			RESTHelper.updateBo(bo, getNuclosSession())

			Log.info getNuclosSession().invalidateServerCaches()
		}
	}

	/**
	 * Works only for requests done directly via RESTClient,
	 * does not capture Webclient requests.
	 */
	static void expectErrorStatus(
			Response.Status expectedStatus,
			Closure<?> c
	) {
		expectRestException(c) {
			assert it.status == expectedStatus
		}
	}

	/**
	 * Catches any RestException that occurs while executing the given closure.
	 * Hands the RestException to the given exceptionHandler.
	 */
	static void expectRestException(
			Closure<?> c,

			@ClosureParams(value = SimpleType, options = ['org.nuclos.test.rest.RESTException'])
					Closure<?> exceptionHandler
	) {
		Exception e = null

		try {
			c()
		} catch (RESTException e2) {
			e = e2
		}

		if (e) {
			exceptionHandler(e)
		} else {
			throw new IllegalStateException("Expected exception did not occur")
		}
	}
}
