package org.nuclos.test

import java.text.SimpleDateFormat

import org.json.JSONObject
import org.nuclos.api.businessobject.Flag
import org.nuclos.test.rest.RESTClient

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.module.SimpleModule

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObject<PK> {

	private static final ObjectMapper mapper

	static {
		mapper = new ObjectMapper()

		SimpleModule module = new SimpleModule();
		module.addSerializer(JSONObject.class, new JSONObjectSerializer())

		Class<?> clazz = Class.forName('org.json.JSONObject$Null')
		module.addSerializer(clazz, new NullSerializer())

		mapper.registerModule(module)

		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
		mapper.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true)
	}

	PK boId

	int version = 0
	String executeCustomRule

//	@JsonSerialize(using = EntityObjectAttributesSerializer.class)
	Map<String, Object> attributes = new HashMap<>()

	@JsonIgnore
	Flag flag = Flag.INSERT

	@JsonIgnore
	final EntityClass<PK> entityClass

	@JsonProperty('subBos')
	@JsonSerialize(using = EntityObjectDependentsSerializer)
	Map<String, List<EntityObject<?>>> dependents = new HashMap<>()

	/**
	 * The RESTClient by which this EO was (re-)loaded.
	 */
	@JsonIgnore
	RESTClient client

	static <PK> EntityObject<PK> fromJson(
			final EntityClass<PK> entityClass,
			final JSONObject json
	) {
		final EntityObject<PK> eo = new EntityObject<PK>(entityClass)
		eo.updateFromJson(json)
		return eo
	}

	EntityObject(EntityClass<PK> entityClass) {
		this.entityClass = entityClass
	}

	PK getId() {
		return boId
	}

	String toJson() {
		mapper.writeValueAsString(this)
	}

	void updateFromJson(JSONObject json) {
		def id = (PK) json['boId']

		// Convert Integer IDs to Long
		if (id instanceof Integer) {
			id = Long.valueOf(id as long)
		}

		boId = id as PK

		attributes.clear()
		json.getJSONObject('attributes').with {
			for (String key : it.keySet()) {
				attributes.put(key, it.get(key))
			}
		}

		version = json['version'] as Integer
	}

	@JsonIgnore
	boolean isNew() {
		return !id
	}

	@JsonIgnore
	boolean isUpdate() {
		return id && flag == Flag.UPDATE
	}

	@JsonIgnore
	boolean isDelete() {
		return id && flag == Flag.DELETE
	}

	Object getAttribute(String attributeName) {
		this.attributes.get(attributeName)
	}

	EntityReference<?> getReference(String attributeName) {
		JSONObject json = this.attributes.get(attributeName) as JSONObject

		return new EntityReference<?>(
				id: json.get('id'),
				name: json.getString('name')
		)
	}

	void setAttribute(String attributeName, Object value) {
		if (value instanceof Date) {
			value = new SimpleDateFormat('yyyy-MM-dd').format(value)
		} else if (value instanceof GString) {
			value = value.toString()
		}

		this.attributes.put(attributeName, value)

		flag = this.new ? Flag.INSERT : Flag.UPDATE
	}

	String getBoMetaId() {
		entityClass.fqn
	}

	@JsonIgnore
	public <SubPK> List<EntityObject<SubPK>> getDependents(
			EntityClass<SubPK> subEntityClass,
			String referenceAttributeName
	) {
		String referenceAttributeFqn = subEntityClass.fqn + '_' + referenceAttributeName

		if (!this.dependents.containsKey(referenceAttributeFqn)) {
			this.dependents.put(referenceAttributeFqn, [])
		}

		return this.dependents.get(referenceAttributeFqn) as List<EntityObject<SubPK>>
	}

	public <SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityClass<SubPK> subEntityClass,
			String referenceAttributeName
	) {
		String referenceAttributeFqn = subEntityClass.fqn + '_' + referenceAttributeName

		List<EntityObject<SubPK>> dependents = client.loadDependents(
				this,
				subEntityClass,
				referenceAttributeFqn
		)

		this.dependents.put(referenceAttributeFqn, dependents as List<EntityObject<?>>)

		return dependents
	}

	void save() {
		checkClient()
		client.save(this)
	}

	private void checkClient() {
		if (!client) {
			throw new IllegalStateException('This EO is not associated with any REST client.')
		}
	}

	boolean delete() {
		checkClient()
		return client.delete(this)
	}

	void flagDeleted() {
		this.flag = Flag.DELETE
	}

	void executeCustomRule(String ruleName) {
		checkClient()
		client.executeCustomRule(this, ruleName)
	}

	void changeState(int statusNumeral) {
		checkClient()
		client.changeState(this, statusNumeral)
	}
}
