package org.nuclos.test

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum SystemEntities implements EntityClass<String> {
	PARAMETER,
	USER,
	ROLE,
	ROLEUSER,
	NUCLET,
	nucletParameter

	@Override
	String getFqn() {
		String nameAppendix = name().toLowerCase()
		if (!name().equals(name().toUpperCase())) {
			nameAppendix = name()
		}
		return 'org_nuclos_businessentity_nuclos' + nameAppendix
	}
}
