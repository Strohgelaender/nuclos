package org.nuclos.test.server

import org.json.JSONArray
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DataLanguageTest extends AbstractNuclosTest {

	static RESTClient clientDe
	static RESTClient clientEn

	static Long auftragstypSchnellId
	static Long artikeltypSpielzeugId
	static Long artikelTeddybaerId
	static Long artikelMatchboxAutoId
	static Long auftragNachbarschaftId
	static Long posNachbarschaft1Id

	@Test
	void _00_setup() {
		clientDe = new RESTClient('nuclos', '')
		clientDe.datalanguage = 'de_DE'
		clientDe.login()
		clientEn = new RESTClient('nuclos', '')
		clientEn.datalanguage = 'en_GB'
		clientEn.login()

		EntityObject<Long> auftragstyp = new EntityObject(TestEntities.NUCLET_TEST_I18N_AUFTRAGSTYP)
		auftragstyp.setAttribute('auftragstyp', 'Schnell')
		clientDe.save(auftragstyp);
		auftragstypSchnellId = auftragstyp.getId()
		auftragstyp = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_AUFTRAGSTYP, auftragstypSchnellId);
		auftragstyp.setAttribute('auftragstyp', 'Fast')
		clientEn.save(auftragstyp);

		EntityObject<Long> artikeltyp = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKELTYP)
		artikeltyp.setAttribute('artikeltyp', 'Spielzeug')
		clientDe.save(artikeltyp);
		artikeltypSpielzeugId = artikeltyp.getId()
		artikeltyp = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKELTYP, artikeltypSpielzeugId)
		artikeltyp.setAttribute('artikeltyp', 'Toys')
		clientEn.save(artikeltyp);

		EntityObject<Long> artikelTeddybaer = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		artikelTeddybaer.setAttribute('artikeltyp', [id: artikeltypSpielzeugId])
		artikelTeddybaer.setAttribute('artikelname', 'Teddybär')
		clientDe.save(artikelTeddybaer);
		artikelTeddybaerId = artikelTeddybaer.getId()
		artikelTeddybaer = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL, artikelTeddybaerId)
		artikelTeddybaer.setAttribute('artikelname', 'Teddy bear')
		clientEn.save(artikelTeddybaer);

		EntityObject<Long> artikelMatchboxAuto = new EntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL)
		artikelMatchboxAuto.setAttribute('artikeltyp', [id: artikeltypSpielzeugId])
		artikelMatchboxAuto.setAttribute('artikelname', 'Matchbox Auto')
		clientDe.save(artikelMatchboxAuto);
		artikelMatchboxAutoId = artikelMatchboxAuto.getId()
		artikelMatchboxAuto = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_ARTIKEL, artikelMatchboxAutoId)
		artikelMatchboxAuto.setAttribute('artikelname', 'Matchbox car')
		clientEn.save(artikelMatchboxAuto);

		EntityObject<Long> auftragNachbarschaft = new EntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG)
		auftragNachbarschaft.setAttribute('auftragstyp', [id: auftragstypSchnellId])
		auftragNachbarschaft.setAttribute('hauptartikel', [id: artikelMatchboxAutoId])
		auftragNachbarschaft.setAttribute('auftragsbezeichnung', 'Nachbarschaft')
		clientDe.save(auftragNachbarschaft);
		auftragNachbarschaftId = auftragNachbarschaft.getId()
		auftragNachbarschaft = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		auftragNachbarschaft.setAttribute('auftragsbezeichnung', 'Neighbourhood')
		clientEn.save(auftragNachbarschaft);


		EntityObject<Long> posNachbarschaft1 = new EntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGPOS)
		posNachbarschaft1.setAttribute('datalangauftrag', [id: auftragNachbarschaftId])
		posNachbarschaft1.setAttribute('artikel', [id: artikelMatchboxAutoId])
		posNachbarschaft1.setAttribute('menge', 1)
		clientDe.save(posNachbarschaft1);
		posNachbarschaft1Id = posNachbarschaft1.getId()
	}

	@Test
	void _01_testResultlist_de() {
		List<EntityObject<Long>> datalangAuftragList = clientDe.getEntityObjects(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG)
		assert datalangAuftragList.size() == 1
		assert datalangAuftragList.getAt(0).getAttribute("auftragsbezeichnung") == 'Nachbarschaft'
		assert datalangAuftragList.getAt(0).getAttribute("auftragstyp").getAt("name") == 'Schnell'
		assert datalangAuftragList.getAt(0).getAttribute("hauptartikel").getAt("name") == 'Spielzeug: Matchbox Auto'
	}

	@Test
	void _02_testResultlist_en() {
		List<EntityObject<Long>> datalangAuftragList = clientEn.getEntityObjects(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG)
		assert datalangAuftragList.size() == 1
		assert datalangAuftragList.getAt(0).getAttribute("auftragsbezeichnung") == 'Neighbourhood'
		assert datalangAuftragList.getAt(0).getAttribute("auftragstyp").getAt("name") == 'Fast'
		assert datalangAuftragList.getAt(0).getAttribute("hauptartikel").getAt("name") == 'Toys: Matchbox car'
	}

	@Test
	void _03_testDetails_de() {
		EntityObject<Long> datalangAuftrag = clientDe.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		assert datalangAuftrag.getAttribute("auftragsbezeichnung") == 'Nachbarschaft'
		assert datalangAuftrag.getAttribute("auftragstyp").getAt("name") == 'Schnell'
		assert datalangAuftrag.getAttribute("hauptartikel").getAt("name") == 'Spielzeug: Matchbox Auto'
	}

	@Test
	void _04_testDetails_de() {
		EntityObject<Long> datalangAuftrag = clientEn.getEntityObject(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG, auftragNachbarschaftId)
		assert datalangAuftrag.getAttribute("auftragsbezeichnung") == 'Neighbourhood'
		assert datalangAuftrag.getAttribute("auftragstyp").getAt("name") == 'Fast'
		assert datalangAuftrag.getAttribute("hauptartikel").getAt("name") == 'Toys: Matchbox car'
	}

	@Test
	void _05_testReferenceList_de() {
		JSONArray reflist = RESTHelper.getReferenceList(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG.fqn + '_hauptartikel', clientDe);
		assert reflist.size() == 2
		assert reflist.getAt(0).getAt("name") == 'Spielzeug: Matchbox Auto'
		assert reflist.getAt(1).getAt("name") == 'Spielzeug: Teddybär'
	}

	@Test
	void _06_testReferenceList_en() {
		JSONArray reflist = RESTHelper.getReferenceList(TestEntities.NUCLET_TEST_I18N_DATALANGAUFTRAG.fqn + '_hauptartikel', clientEn);
		assert reflist.size() == 2
		assert reflist.getAt(0).getAt("name") == 'Toys: Matchbox car'
		assert reflist.getAt(1).getAt("name") == 'Toys: Teddy bear'
	}

	@Test
	void _07_testVlpData_de() {
		JSONArray vlpdata = RESTHelper.getVlpData(TestEntities.NUCLET_TEST_I18N_DATALANGPOS.fqn + '_artikel', '', 'D7ChzSO8q0pw9abMRIRd', 'intid', 'vlpconcat', auftragNachbarschaftId.toString(), clientDe)
		assert vlpdata.size() == 2
		// Sortierung in Datenquelle ist DESC (ACHTUNG: funktioniert nur im Restservice! Der Desktop wrapped das VLP Statement noch mit einer Standardlogik NUCLOS-5545)
		assert vlpdata.getAt(0).getAt("name") == 'Spielzeug: Teddybär'
		assert vlpdata.getAt(1).getAt("name") == 'Spielzeug: Matchbox Auto'
	}

	@Test
	void _08_testVlpData_en() {
		JSONArray vlpdata = RESTHelper.getVlpData(TestEntities.NUCLET_TEST_I18N_DATALANGPOS.fqn + '_artikel', '', 'D7ChzSO8q0pw9abMRIRd', 'intid', 'vlpconcat', auftragNachbarschaftId.toString(), clientEn)
		assert vlpdata.size() == 2
		// Sortierung in Datenquelle ist DESC (ACHTUNG: funktioniert nur im Restservice! Der Desktop wrapped das VLP Statement noch mit einer Standardlogik NUCLOS-5545)
		assert vlpdata.getAt(0).getAt("name") == 'Toys: Teddy bear'
		assert vlpdata.getAt(1).getAt("name") == 'Toys: Matchbox car'
	}


	/*@Test
	void _01_testNucletIntegrationInsertFinalRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4711)
		client.save(order) // die erste Lagerbuchung (InsertFinal)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 1
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4711 '
	}

	@Test
	void _02_testNucletIntegrationUpdateRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4712)
		client.save(order) // eine weitere Lagerbuchung (InsertFinal)
		order.setAttribute('orderNumber', 4713)
		client.save(order); // eine weitere Lagerbuchung (Update)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 3
		// ORDER BY t.INTID DESC ....
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4713 '
		assert lagerbuchungen.getAt(1).getAttribute("artikel").getAt("name") == '4713 '
	}

	@Test
	void _03_testNucletIntegrationStateChangeRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4714)
		order.setAttribute('customer', [id: customerId])
		client.save(order) // eine weitere Lagerbuchung (InsertFinal)
		client.changeState(order, 80) // // eine weitere Lagerbuchung (StateChange)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 5
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4714 Mr. Integrationspunkt'
	}*/

}
