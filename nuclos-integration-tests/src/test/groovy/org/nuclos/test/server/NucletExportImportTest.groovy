package org.nuclos.test.server

import java.util.zip.ZipInputStream

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NucletExportImportTest extends AbstractNuclosTest {

	static RESTClient client

	static String nucletName = 'nucb'

	@Test
	void _00_setup() {
		client = new RESTClient('nuclos', '')
		client.login()

	}

	@Test
	void _01_writeNucletParams() {
		QueryOptions opt = new QueryOptions(
				search: nucletName
		)
		List<EntityObject<String>> eos = client.getEntityObjects(SystemEntities.NUCLET, opt);

		assert eos.size() == 1

		EntityObject<String> eo = eos.get(0)

		List<EntityObject<String>> lstDep = eo.loadDependents(SystemEntities.nucletParameter, 'nuclet')

		setNucletParamAttributes(lstDep, '01', true)
		setNucletParamAttributes(lstDep, '02', false)

		eo.save()

	}

	static void setNucletParamAttributes(List<EntityObject<String>> lstNp, String nr, Boolean export) {
		String param = 'ParamX' + nr
		EntityObject<String> nucletParam = null
		for (EntityObject<String> np : lstNp) {
			if (np.getAttribute('name') == param) {
				nucletParam = np;
				break;
			}
		}
		if (nucletParam == null) {
			nucletParam = new EntityObject<String>(SystemEntities.nucletParameter)
			lstNp.add(nucletParam)
		}
		nucletParam.setAttribute('name', param)
		nucletParam.setAttribute('description', 'Desc' + nr)
		nucletParam.setAttribute('value', 'ValueY' + nr)
		nucletParam.setAttribute('exportvalue', export)
	}

	@Test
	void _02_exportNuclet() {
		byte[] nucletContent = client.exportNuclet(nucletName);

		assert nucletContent

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(nucletContent));

		while ((zis.getNextEntry()) != null) {
			int size;
			byte[] buffer = new byte[2048];

			while ((size = zis.read(buffer, 0, buffer.length)) != -1) {
				baos.write(buffer, 0, size);
			}
			baos.flush();
		}
		zis.close();
		baos.close();

		String s = baos.toString();

		assert s;
		assert s.contains('ParamX01')
		assert s.contains('ValueY01')
		assert s.contains('ParamX02')
		// This is the very test: It may not contain the value with exportvalue == false
		assert !s.contains('ValueY02')

	}

	@Ignore('Not implemented yet')
	@Test
	void _03_importNuclet() {
		// TODO String response = client.importNuclet(nucletContent)
		// TODO assert response
	}
}
