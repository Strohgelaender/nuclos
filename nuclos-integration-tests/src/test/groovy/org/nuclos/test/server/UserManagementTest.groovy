package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Ignore('Temporarily disabled because it server can become instable, see NUCLOS-6814')
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class UserManagementTest extends AbstractNuclosTest {
	static String testRoleName = 'TestRole'

	@Test
	void _01setup() {
		deleteTestRoles()
	}

	@Test
	void _10createRole() {
		EntityObject<Long> eo = new EntityObject(TestEntities.NUCLOS_ROLES)
		eo.setAttribute('name', testRoleName)

		assert !eo.id
		nuclosSession.save(eo)

		assert eo.id

	}

	@Test
	void _15createSimilarRoleNotPossible() {
		EntityObject<Long> eo = new EntityObject(TestEntities.NUCLOS_ROLES)
		eo.setAttribute('name', testRoleName + "()")

		assert !eo.id
		expectErrorStatus(Response.Status.INTERNAL_SERVER_ERROR) {
			nuclosSession.save(eo)
		}

		assert !eo.id

	}

	@Test
	void _20createSimilarRolePossible() {
		EntityObject<Long> eo = new EntityObject(TestEntities.NUCLOS_ROLES)
		eo.setAttribute('name', testRoleName + '(1)')

		assert !eo.id
		nuclosSession.save(eo)

		assert eo.id

		// Rename it
		eo.setAttribute('name', testRoleName + ' 2')
		nuclosSession.save(eo)

		//Try to rename to not a name that is not possible
		eo.setAttribute('name', testRoleName + '+')

		expectErrorStatus(Response.Status.INTERNAL_SERVER_ERROR) {
			nuclosSession.save(eo)
		}

	}

	@Test
	void _30createSimilarRoleNotPossible2() {
		EntityObject<Long> eo = new EntityObject(TestEntities.NUCLOS_ROLES)
		eo.setAttribute('name', testRoleName + '2')

		assert !eo.id
		expectErrorStatus(Response.Status.INTERNAL_SERVER_ERROR) {
			nuclosSession.save(eo)
		}

		assert !eo.id

	}

	@Test
	void _90cleanUp() {
		assert deleteTestRoles() == 2
	}

	static int deleteTestRoles() {
		QueryOptions opt = new QueryOptions(
				search: testRoleName
		)

		List<EntityObject<Long>> eos = nuclosSession.getEntityObjects(TestEntities.NUCLOS_ROLES, opt)
		nuclosSession.deleteAll(eos)

		return eos.size()
	}

}
