package org.nuclos.test.server

import org.json.JSONObject
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ServerIntegrationTest extends AbstractNuclosTest {

	/* 
	 * documentFile_ - Tests
	 * Siehe auch NUCLOS-5233 "Refactoring Dokumentenanhänge" 
	 */
	@Test
	void documentFile_00disableIndexer() {
		nuclosSession.managementConsole('disableIndexer')
	}

	/* 
	 * Neuen Datensatz gleich mit neuer Datei speichern. Link muss mit erzeugt werden.
	 */

	@Test
	void documentFile_01_StoreFileInsertGO() { runServerTest("documentFileStoreFileInsertGO") }

	@Test
	void documentFile_01_StoreFileInsertMD() { runServerTest("documentFileStoreFileInsertMD") }

	/*
	 * Datensatz ohne Datei erzeugen. Erst mit einem Update wird eine Datei angehängt. Link muss mit dem Update 
	 * erzeugt werden.
	 */

	@Test
	void documentFile_02_StoreFileUpdateGO() { runServerTest("documentFileStoreFileUpdateGO") }

	@Test
	void documentFile_02_StoreFileUpdateMD() { runServerTest("documentFileStoreFileUpdateMD") }

	/*
	 * Ausgangssituation: Ein Datensatz mit einer Datei existiert.
	 * Getestet wird das Verlinken der vorhandenen Datei. Ein zusätzlicher Link muss erzeugt werden, eine 
	 * weitere Datei im Dokumentenordner NICHT. 
	 */

	@Test
	void documentFile_03_LinkFileInsertGO() { runServerTest("documentFileLinkFileInsertGO") }

	@Test
	void documentFile_03_LinkFileInsertMD() { runServerTest("documentFileLinkFileInsertMD") }

	/*
	 * Ähnlich dem LinkFileInsert. Unterschied:
	 * Erst mit einem Update wird der Link erzeugt. Auch hier darf keine neue Datei im Dokumentenordner
	 * erzeugt werden.
	 */

	@Test
	void documentFile_04_LinkFileUpdateGO() { runServerTest("documentFileLinkFileUpdateGO") }

	@Test
	void documentFile_04_LinkFileUpdateMD() { runServerTest("documentFileLinkFileUpdateMD") }

	/*
	 * Überschreibt einen vorhandenen Link mit einem neuen File. Damit testen wir die Aktualisierung aller vorhanden Links.
	 * Achtung, ein neues File kann nicht als Link gespeichert werden, erst wenn es existiert kann man es verlinken.
	 */

	@Test
	void documentFile_04_LinkNewFileGO() { runServerTest("documentFileLinkNewFileGO") }

	@Test
	void documentFile_04_LinkNewFileMD() { runServerTest("documentFileLinkNewFileMD") }

	/*
	 * Löschen eines Datensatzes muss auch alle Links und Dateien entfernen, sofern die Datei nicht noch von einem anderen Datensatz
	 * referenziert wird. 
	 */

	@Test
	void documentFile_05_DeleteFileDeleteGO() { runServerTest("documentFileDeleteFileDeleteGO") }

	@Test
	void documentFile_05_DeleteFileDeleteMD() { runServerTest("documentFileDeleteFileDeleteMD") }

	/*
	 * Datei fehlt aus unbekannten Gründen (denkbar durch einen Dumprestore). Das sollte jedoch das Löschen des Datensatzes nicht berühren.
	 */

	@Test
	void documentFile_05_DeleteFileDeleteGOWithMissingFile() {
		runServerTest("documentFileDeleteFileDeleteGOWithMissingFile")
	}

	@Test
	void documentFile_05_DeleteFileDeleteMDWithMissingFile() {
		runServerTest("documentFileDeleteFileDeleteMDWithMissingFile")
	}

	/*
	 * Löschen der Datei durch Zurücksetzen/Nullen. Link und Datei müssen entfernt werden, sofern nicht anderweitig referenziert.
	 */

	@Test
	void documentFile_06_DeleteFileSetNullGO() { runServerTest("documentFileDeleteFileSetNullGO") }

	@Test
	void documentFile_06_DeleteFileSetNullMD() { runServerTest("documentFileDeleteFileSetNullMD") }

	/*
	 * Löscht eine Link auf eine vorhandene Datei, die allerdings noch durch andere Datensätze referenziert wird. 
	 * Hierfür wird das BO mit dem Link gelöscht. Die Datei darf aber NICHT gelöscht werden.
	 */

	@Test
	void documentFile_07_DeleteLinkDeleteGO() { runServerTest("documentFileDeleteLinkDeleteGO") }

	@Test
	void documentFile_07_DeleteLinkDeleteMD() { runServerTest("documentFileDeleteLinkDeleteMD") }

	/*
	 * Löscht eine Link auf eine vorhandene Datei, die allerdings noch durch andere Datensätze referenziert wird. 
	 * Hierfür wird die Referenz zurückgesetzt bzw. genullt. Die Datei darf aber NICHT gelöscht werden.
	 */

	@Test
	void documentFile_08_DeleteLinkSetNullGO() { runServerTest("documentFileDeleteLinkSetNullGO") }

	@Test
	void documentFile_08_DeleteLinkSetNullMD() { runServerTest("documentFileDeleteLinkSetNullMD") }

	/*
	 * Gleicher Test wie "DeleteFileDelete...". Geprüft wird hier die transaktionale Abhängigkeit. Die Datei darf erst mit dem Commit im 
	 * Dateisystem gelöscht werden. Innerhalb der Transaktion, nach erfolgreichem Löschen des BO muss die Datei noch vorhanden sein, 
	 * falls es doch noch zum Rollback kommt.
	 */

	@Test
	void documentFile_09_DeleteFileDeleteGOCheckBeforeCommit() {
		runServerTest("documentFileDeleteFileDeleteGOCheckBeforeCommit")
	}

	@Test
	void documentFile_09_DeleteFileDeleteMDCheckBeforeCommit() {
		runServerTest("documentFileDeleteFileDeleteMDCheckBeforeCommit")
	}

	/*
	 * Gleicher Test wie "DeleteFileSetNull...". Geprüft wird hier die transaktionale Abhängigkeit. Die Datei darf erst mit dem Commit im
	 * Dateisystem gelöscht werden. Innerhalb der Transaktion, nach erfolgreichem Löschen des BO muss die Datei noch vorhanden sein,
	 * falls es doch noch zum Rollback kommt.
	 */

	@Test
	void documentFile_10_DeleteFileSetNullGOCheckBeforeCommit() {
		runServerTest("documentFileDeleteFileSetNullGOCheckBeforeCommit")
	}

	@Test
	void documentFile_10_DeleteFileSetNullMDCheckBeforeCommit() {
		runServerTest("documentFileDeleteFileSetNullMDCheckBeforeCommit")
	}

	/*
	 * Wird ein Dokument (byte[]) von Dateisystem gelesen, obwohl es gar nicht angefordert wurde?
	 * Hierfür löschen wir das Dokument aus dem Dokumentenordner. Bei Zugriff wird automatisch ein Fehler geworfen, wenn die Datei nicht mehr 
	 * gefunden wird. In diesem Fall haben wir auch ein Performanceproblem!
	 */

	@Test
	void documentFile_11_CheckUnnecessaryFileAccessGO() { runServerTest("documentFileCheckUnnecessaryFileAccessGO") }

	@Test
	void documentFile_11_CheckUnnecessaryFileAccessMD() { runServerTest("documentFileCheckUnnecessaryFileAccessMD") }

	/*
	 * Wie geht das System damit um, wenn eine erwartete Datei im Dateisystem nicht gefunden wurde (z.B. Aufgrund einer Inkonsistenz eines 
	 * Dumprestore o.ä.). Erwartung: Es wird eine Fatale / FileNotFoundException geworfen.
	 */

	@Test
	void documentFile_12_CheckMissingFileAccessGO() { runServerTest("documentFileCheckMissingFileAccessGO") }

	@Test
	void documentFile_12_CheckMissingFileAccessMD() { runServerTest("documentFileCheckMissingFileAccessMD") }

	/*
	 * Test mit Unterformular und einem Dateianhangsattribut, wobei das UF (+Datei) gleich mit dem Insert angelegt wird. 
	 */

	@Test
	void documentFile_13_StoreFileInDependentGOWithInsert() {
		runServerTest("documentFileStoreFileInDependentGOWithInsert")
	}

	@Test
	void documentFile_13_StoreFileInDependentMDWithInsert() {
		runServerTest("documentFileStoreFileInDependentMDWithInsert")
	}

	/*
	 * Test mit Unterformular und einem Dateianhangsattribut, wobei der UF-Datensatz schon ohne Datei existiert, und durch ein 
	 * Update erst eine Datei hinzugefügt wird.
	 */

	@Test
	void documentFile_14_StoreFileInDependentGOWithUpdate() {
		runServerTest("documentFileStoreFileInDependentGOWithUpdate")
	}

	@Test
	void documentFile_14_StoreFileInDependentMDWithUpdate() {
		runServerTest("documentFileStoreFileInDependentMDWithUpdate")
	}

	/*
	 * Test des Standard Dokumentenanhangsunterformulares von Nuclos. Kann nur bei BOs mit Statusmodell eingesetzt werden.
	 * Fügt ein neue Datei hinzu. Links sind hier nicht vorgesehen.
	 */

	@Test
	void documentFile_15_StoreAttachmentWithInsert() { runServerTest("documentFileStoreAttachmentWithInsert") }

	@Test
	void documentFile_16_StoreAttachmentWithUpdate() { runServerTest("documentFileStoreAttachmentWithUpdate") }

	/*
	 * Eine vorhandene Datei wird in einem UF verlinkt.
	 */

	@Test
	void documentFile_17_LinkFileInDependentGOWithInsert() {
		runServerTest("documentFileLinkFileInDependentGOWithInsert")
	}

	@Test
	void documentFile_17_LinkFileInDependentMDWithInsert() {
		runServerTest("documentFileLinkFileInDependentMDWithInsert")
	}

	/*
	 * Eine vorhandene Datei wird mit einem Update in einem vorhandenen UF-Datensatz verlinkt.
	 */

	@Test
	void documentFile_18_LinkFileInDependentGOWithUpdate() {
		runServerTest("documentFileLinkFileInDependentGOWithUpdate")
	}

	@Test
	void documentFile_18_LinkFileInDependentMDWithUpdate() {
		runServerTest("documentFileLinkFileInDependentMDWithUpdate")
	}

	/*
	 * Löscht einen UF-Datensatz mit Datei, ohne den Hauptdatensatz zu löschen. Link und Datei müssen entfernt werden
	 */

	@Test
	void documentFile_19_DeleteFileDeleteDependentGO() { runServerTest("documentFileDeleteFileDeleteDependentGO") }

	@Test
	void documentFile_19_DeleteFileDeleteDependentMD() { runServerTest("documentFileDeleteFileDeleteDependentMD") }

	/*
	 * Setzt eine Datei in einem vorhandenen UF-Datensatz zurück (nullt ihn). Mit dem Update müssen Link und Datei entfernt worden sein.
	 */

	@Test
	void documentFile_20_DeleteFileSetNullDependentGO() { runServerTest("documentFileDeleteFileSetNullDependentGO") }

	@Test
	void documentFile_20_DeleteFileSetNullDependentMD() { runServerTest("documentFileDeleteFileSetNullDependentMD") }

	/*
	 * Löscht einen UF-Datensatz mit Datei. Die Datei wird jedoch noch an anderer Stelle referenziert. Link muss gelöscht werden, 
	 * Datei NICHT.
	 */

	@Test
	void documentFile_21_DeleteLinkDeleteDependentGO() { runServerTest("documentFileDeleteLinkDeleteDependentGO") }

	@Test
	void documentFile_21_DeleteLinkDeleteDependentMD() { runServerTest("documentFileDeleteLinkDeleteDependentMD") }

	/*
	 * Setzt Datei in einem vorhandenen UF-Datensatz zurück (nullt ihn). Die Datei wird jedoch noch an anderer Stelle referenziert. 
	 * Link muss gelöscht werden, Datei NICHT.
	 */

	@Test
	void documentFile_22_DeleteLinkSetNullDependentGO() { runServerTest("documentFileDeleteLinkSetNullDependentGO") }

	@Test
	void documentFile_22_DeleteLinkSetNullDependentMD() { runServerTest("documentFileDeleteLinkSetNullDependentMD") }

	/*
	 * Test des Standard Dokumentenanhangsunterformulares von Nuclos. Kann nur bei BOs mit Statusmodell eingesetzt werden.
	 * Löscht einen vorhandenen Eintrag. Datei muss entfernt werden. Links sind hier nicht vorgesehen.
	 */

	@Test
	void documentFile_23_DeleteAttachmentWithDelete() { runServerTest("documentFileDeleteAttachmentWithDelete") }

	@Test
	void documentFile_24_DeleteAttachmentWithUpdate() { runServerTest("documentFileDeleteAttachmentWithUpdate") }

	/*
	 * Beim klonen eines Datensatzes müssen auch die Dateien geklont werden. Keine Verlinkung (siehe NUCLOS-5464)
	 */

	@Test
	void documentFile_25_CloneFileGO() { runServerTest("documentFileCloneFileGO") }

	@Test
	void documentFile_26_CloneFileMD() { runServerTest("documentFileCloneFileMD") }

	@Test
	void genericBusinessObject_01_withDependents() { runServerTest("genericBusinessObjectWithDependents") }

	@Test
	void tablePreferencesManager_01_columnOrder() { runServerTest("tablePreferencesManagerColumnOrder") }

	static void runServerTest(String sTestcase) {

		def serverTest =
				[
						boMetaId  : 'nuclet_test_utils_ServerTest',
						attributes: [
								'testcase': sTestcase
						]
				]

		serverTest = RESTHelper.createBo(serverTest, nuclosSession)
		JSONObject attributes = serverTest.getJSONObject('attributes')
		if (attributes.has('exception')) {
			throw new Exception(attributes.getString('exception'))
		}
		assert attributes.get('ok')
	}

}
