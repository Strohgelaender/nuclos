package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper
import org.springframework.http.HttpMethod

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class CustomRestRuleTest extends AbstractNuclosTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static RESTClient client

	final static String BASE_PATH = '/execute/example.rest.CustomRestTestRule/'
	final static String BASE_PATH_INACTIVE = '/execute/example.rest.CustomRestInactiveRule/'

	@Test
	void _00_setupUpAccounts() {
		RESTHelper.createUser('testct', 'testct', ['Example controlling', 'Example readonly'], nuclosSession)
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
	}

	@Test
	void _01_noPermission() {
		client = new RESTClient('testct', 'testct').login()
		exception.expect(RESTException)
			RESTHelper.requestString(BASE_PATH + 'getTest', HttpMethod.GET, client.sessionId)
	}

	@Test
	void _03_getTest() {
		client = new RESTClient('test', 'test').login()

		String response = RESTHelper.requestString(BASE_PATH + 'getTest', HttpMethod.GET, client.sessionId)
		assert response == 'user-last-name:Lastname'
	}

	@Test
	void _04_getTestWithRequestParams() {
		String response = RESTHelper.requestString(BASE_PATH + 'getTestWithRequestParams?param1=ABC&param2=XYZ', HttpMethod.GET, client.sessionId)
		assert response == 'ABCXYZ'
	}

	@Test
	void _05_postTest() {
		String input = '{"a":1}'
		String response = RESTHelper.postJson(BASE_PATH + 'postTest', input, client)
		assert response == input
	}

	@Test
	void _06_putTest() {
		String input = '{"a":1}'
		String response = RESTHelper.putJson(BASE_PATH + 'putTest', input, client)
		assert response == input
	}

	@Test
	void _07_deleteTest() {
		String input = '{"a":1}'
		String response = RESTHelper.requestString(BASE_PATH + 'deleteTest', HttpMethod.DELETE, client.sessionId, input)
		assert response == 'ok'
	}

	@Test
	void _08_methodNotAllowedTest() {
		// try a GET request on a method which only accepts POST
		exception.expect(RESTException)
			RESTHelper.requestString(BASE_PATH + 'postTest', HttpMethod.GET, client.sessionId)
	}

	@Test
	void _10_inactiveRule() {
		exception.expect(RESTException)
			RESTHelper.requestString(BASE_PATH_INACTIVE + 'getTest', HttpMethod.GET, client.sessionId)
	}

}