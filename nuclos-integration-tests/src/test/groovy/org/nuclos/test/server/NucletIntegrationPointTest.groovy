package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class NucletIntegrationPointTest extends AbstractNuclosTest {

	static RESTClient client

	static Long customerId

	@Test
	void _00_setup() {
		client = new RESTClient('nuclos', '')
		client.login()
		EntityObject<Long> cutomer = new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMER)
		cutomer.setAttribute('customerNumber', 1)
		cutomer.setAttribute('name', 'Mr. Integrationspunkt')
		client.save(cutomer)
		customerId = cutomer.getId()
	}

	@Test
	void _01_testNucletIntegrationInsertFinalRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4711)
		client.save(order) // die erste Lagerbuchung (InsertFinal)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 1
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4711 '
	}

	@Test
	void _02_testNucletIntegrationUpdateRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4712)
		client.save(order) // eine weitere Lagerbuchung (InsertFinal)
		order.setAttribute('orderNumber', 4713)
		client.save(order); // eine weitere Lagerbuchung (Update)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 3
		// ORDER BY t.INTID DESC ....
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4713 '
		assert lagerbuchungen.getAt(1).getAttribute("artikel").getAt("name") == '4713 '
	}

	@Test
	void _03_testNucletIntegrationStateChangeRuleExecution() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4714)
		order.setAttribute('customer', [id: customerId])
		client.save(order) // eine weitere Lagerbuchung (InsertFinal)
		client.changeState(order, 80) // // eine weitere Lagerbuchung (StateChange)
		List<EntityObject<Long>> lagerbuchungen = client.getEntityObjects(TestEntities.NUCLET_TEST_OTHER_LAGERBUCHUNG)
		assert lagerbuchungen.size() == 5
		assert lagerbuchungen.getAt(0).getAttribute("artikel").getAt("name") == '4714 Mr. Integrationspunkt'
	}

	@Test
	void _04_testDefaultRuleExecutionBeforeIntegrationPointRule() {
		EntityObject<Long> order = new EntityObject(TestEntities.EXAMPLE_REST_ORDER)
		order.setAttribute('orderNumber', 4715)
		client.save(order);

		// UpdateRule test here only...
		order.setAttribute('note', 'Hello default rule!')
		client.save(order);

		assert order.getAttribute('note') == 'Hello default rule! --- Hi NucletIntegrationPointTest :)'
	}

}
