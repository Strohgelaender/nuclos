package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ResultListTest extends AbstractNuclosTest {
	Map wordStart = [text: 'Word Start', times: 150, agent: 'test']
	static RESTClient client

	@Test
	void _01setup() {
		nuclosSession.managementConsole('enableIndexerSynchronously')
	}

	@Test
	void _02_createTestUserAndLogin() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
		client.login()
	}

	@Test
	void _03createNewEntries() {
		EntityObject<Long> eo = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		eo.setAttributes(new HashMap<String, Object>(wordStart))

		assert !eo.id
		client.save(eo)

		assert eo.id

		Object cobj = eo.getAttribute('countletters')
		assert cobj instanceof Integer
		assert ((Integer)cobj) > 0

		client.executeCustomRule(eo, 'example.rest.RegisterWords')

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD)
		assert eos.size() >= 40
	}

	@Test
	void _04textSearch() {
		QueryOptions opt = new QueryOptions(
				search: 'Medizin'
		)

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		EntityObject<Long> eo = eos.get(0)

		Object cobj = eo.getAttribute('countletters')
		assert cobj instanceof Integer
		assert ((Integer)cobj) > 0

		opt = new QueryOptions(
				search: 'her'
		)

		List<EntityObject<Long>> eos2 = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos2.size() == 7


		QueryOptions opt150 = new QueryOptions(
				search: '150'
		)

		List<EntityObject<Long>> eos3 = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt150)
		assert eos3.size() == 1

		eo.setAttribute('parent', [id: eos3.get(0).id])
		client.save(eo)
	}

	// NUCLOS-6375
	@Test
	void _05textReferencesDisplayingNumber() {
		// The search for "150" yields two results: One for a direct number, other for reference to a number
		QueryOptions opt150 = new QueryOptions(
				search: '150'
		)

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt150)
		assert eos.size() == 2


		// The search for "6" should not yield any result, as the referencing value is a number
		QueryOptions opt6 = new QueryOptions(
				search: '50'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt6)
		assert eos.size() == 0

	}

	@Test
	void _6MultiWordsInOneColumnsTest() {
		// NUCLOS-6256 Word and Star are in one column

		QueryOptions opt = new QueryOptions(
				search: 'Wor Star'
		)

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Wor*Star'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Star*Wor'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

		opt = new QueryOptions(
				search: '"Wor Star"'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0


		opt = new QueryOptions(
				search: '"Word Star"'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1
	}

	@Test
	void _7MultiWordsInSeparateColumnsTest() {
		// NUCLOS-6256  and test are in different columns

		QueryOptions opt = new QueryOptions(
				search: 'Med test'
		)

		List<EntityObject<Long>> eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'test Medizin'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Medizin*test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

		opt = new QueryOptions(
				search: '"Medizin test"'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

		opt = new QueryOptions(
				search: 'Wor xyz'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

		opt = new QueryOptions(
				search: 'Wor test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 2

		opt = new QueryOptions(
				search: 'Wor Sta test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Wor*Sta test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 1

		opt = new QueryOptions(
				search: 'Wor Sta*test'
		)

		eos = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, opt)
		assert eos.size() == 0

	}

	@Ignore
	@Test
	void _9luceneTest() {
		client.expectSearchResults("doktor", 2, 300)
	}

	@Test
	void _99disableIndexer() {
		nuclosSession.managementConsole('disableIndexer')
	}

}
