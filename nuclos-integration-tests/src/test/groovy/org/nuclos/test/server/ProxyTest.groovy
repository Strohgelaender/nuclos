package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ProxyTest extends AbstractNuclosTest {

	/*
	 * 
	 */

	@Test
	void _00_setup() {
		RESTHelper.createUser('ProxyTest', 'ProxyTest', ['Example user'], nuclosSession)
	}


	@Test
	void _01_testResultForBasicBO() {
		RESTClient client = new RESTClient('ProxyTest', 'ProxyTest').login()
		def result = RESTHelper.getBos('nuclet_test_other_TestProxyContainer', client) as Map
		def bos = result.get('bos') as List
		assert bos.size() == 2
	}
}