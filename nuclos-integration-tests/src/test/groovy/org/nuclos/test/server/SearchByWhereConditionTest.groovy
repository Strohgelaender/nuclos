package org.nuclos.test.server


import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

import javax.ws.rs.core.Response

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SearchByWhereConditionTest extends AbstractNuclosTest {
	static RESTClient client

	static EntityObject testEo

	static String entityFqn = TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS.fqn
	static String subformFqn = TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO.fqn
	static String userFqn = 'org_nuclos_businessentity_nuclosuser'

	@BeforeClass
	static void setup() {
		AbstractNuclosTest.setup()

		1.times {
			testEo = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
			testEo.setAttribute('text', "test $it")
			nuclosSession.save(testEo)
		}
		2.times {
			EntityObject eo = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
			eo.setAttribute('text', "nuclos $it")
			eo.setAttribute('reference', [id: 'nuclos1000'])

			List<EntityObject<Long>> memos = eo.getDependents(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO, 'parent')
			EntityObject memo = new EntityObject<>(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO)
			memo.setAttribute('memo', 'Test')
			memos << memo

			nuclosSession.save(eo)
		}

		client = new RESTClient('test', 'test')
		client.login()
	}

	@Test
	void _05_uidReferenceIsNull() {
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "${entityFqn}_reference IS NULL")
		)

		assert result.size() == 1
	}

	@Test
	void _10_uidReferenceEqualsValue() {
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "${entityFqn}_reference = 'nuclos1000'")
		)

		assert result.size() == 2
	}

	@Test
	void _15_uidReferenceSubSelect() {
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: """
${entityFqn}_reference = (
	select ${userFqn}.id from $userFqn where ${userFqn}_name = 'nuclos'
)
"""
				)
		)

		assert result.size() == 2
	}

	@Test
	void _17_uidReferenceSubSelectIN() {
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: """
${entityFqn}_reference IN (
	select ${userFqn}.id from $userFqn where ${userFqn}_name = 'nuclos'
)
"""
				)
		)

		assert result.size() == 2
	}

	@Test
	void _20_intidReference() {
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "${entityFqn}.id = $testEo.id")
		)

		assert result.size() == 1
	}

	@Test
	void _22_intidReferenceSubSelectIN() {
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: """${entityFqn}.id IN (
	SELECT ${subformFqn}_parent FROM $subformFqn WHERE ${subformFqn}_memo = 'Test'
)""")
		)

		assert result.size() == 2
	}

	@Test
	void _23_intidReferenceSubSelectIN2() {
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO,
				new QueryOptions(queryWhere: """${subformFqn}_parent IN (
	SELECT ${subformFqn}_parent FROM $subformFqn WHERE ${subformFqn}_memo = 'Test'
)""")
		)

		assert result.size() == 2
	}

	@Test
	void _24_intidReferenceSubSelectINInvalid() {
		expectErrorStatus(Response.Status.EXPECTATION_FAILED) {
			client.getEntityObjects(
					TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSMEMO,
					new QueryOptions(queryWhere: """${subformFqn}_parent IN (
	SELECT ${subformFqn}_parent FROM $entityFqn WHERE ${entityFqn}_text = 'Test'
)""")
			)
		}
	}

	@Test
	void _25_conditionInBraces() {
		// Additional brackets in the condition should not cause any Exceptions
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: '(1 = 1)')
		)

		assert !result.empty
	}

	@Test
	void _30_timestampSearch() {
		String createdAt = "${TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS.fqn}_createdAt"

		String now = Instant.now().toString()
		String oneHourAgo = Instant.now().minus(1, ChronoUnit.HOURS).toString()

		// There can be no record that was created later than NOW
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "$createdAt > '$now'")
		)
		assert result.empty

		// All records where created earlier than NOW
		result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "$createdAt <= '$now'")
		)
		assert !result.empty

		// No records are older than 1 hour
		result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "$createdAt < '$oneHourAgo'")
		)
		assert result.empty

		// All records where created within the last hour
		result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "$createdAt > '$oneHourAgo'")
		)
		assert !result.empty
	}

	@Test
	void _35_dateSearch() {
		String createdAt = "${TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS.fqn}_createdAt"

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")

		String today = sdf.format(new Date())
		String yesterday = DateTimeFormatter.ISO_LOCAL_DATE
				.withZone(ZoneId.systemDefault())
				.toFormat()
				.format(
					Instant.now().minus(1l, ChronoUnit.DAYS)
				)

		// All records are from today
		List<EntityObject<Long>> result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "$createdAt = '$today'")
		)
		assert !result.empty

		// No record is from tomorrow
		result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "$createdAt > '$today'")
		)
		assert result.empty

		// No records are created after yesterday
		result = client.getEntityObjects(
				TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS,
				new QueryOptions(queryWhere: "$createdAt > '$yesterday'")
		)
		assert !result.empty
	}
}