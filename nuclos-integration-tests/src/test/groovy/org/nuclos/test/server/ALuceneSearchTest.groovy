package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 * The "A" at the beginning is for this test to start the series of ServerTests
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ALuceneSearchTest extends AbstractNuclosTest {
	static RESTClient suclient = new RESTClient('nuclos', '')
	static RESTClient client


	@Test
	void _00_setup() {
		nuclosSession.managementConsole('enableIndexerSynchronously')
		nuclosSession.managementConsole('rebuildLuceneIndex')
	}

	@Test
	void _01_createTestUser() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
	}

	@Test
	void _02createTestData() {
		TestDataHelper.createLuceneTestData(nuclosSession)
	}

	@Test
	void _04_login() {
		client.login()
		assert client.sessionId
	}

	@Test
	void _06luceneTest() {
		// After data change give lucene some time to cleanly close the indexing, here 200ms

		client.expectSearchResults(TestDataHelper.catName, 4, 200) //Note: The three words + the category itself.

		client.expectSearchResults(TestDataHelper.catReName, 0) //Note: No hit until now.
	}

	@Test
	void _08renameCategory() {

		EntityObject<Long> eo = client.getEntityObject(TestEntities.EXAMPLE_REST_CATEGORY, Long.valueOf((String)TestDataHelper.cat1['id']))

		eo.setAttribute('name', TestDataHelper.catReName)

		eo.save()

	}

	@Test
	void _10testReference() {

		client.expectSearchResults(TestDataHelper.catName, 1, 200) //Note: catName is still the originalName of word3

		client.expectSearchResults(TestDataHelper.catReName, 4) //Note: The three words + the category itself.

	}

	@Test
	void _12testRecordGrant() {

		//word4 and word5 must not appear for user test, but still the category for which there are not record grants

		client.expectSearchResults(TestDataHelper.cat2Name, 1)

		suclient.login()
		assert suclient.sessionId
		// while the super user see them all
		suclient.expectSearchResults(TestDataHelper.cat2Name, 3)

	}

	@Test
	void _14testRollback() {

		EntityObject<Long> newWord = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		newWord.setClient(client)
		newWord.setAttributes(new HashMap<>(TestDataHelper.word6))

		// An error from the final rule must have been
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			newWord.save()
		}

		client.expectSearchResults(TestDataHelper.word6.text as String, 0, 200) // It must not be indexed (rollback)

		newWord.setAttribute('times', 37)

		newWord.save()

		assert newWord.id // Saved!

		client.expectSearchResults(TestDataHelper.word6.text as String, 1, 200) // Now it must be indexed

	}

	@Test
	void _16testComplexSearches() {
		client.expectSearchResults('Fl', 2) // Fleisch and Flugzeuge

		client.expectSearchResults('Fle', 1) // Only Fleisch

		client.expectSearchResults('fl%20sc', 1) // "Flugzeuge Schiffe"

		client.expectSearchResults('Sch', 1) // Schiffe

		suclient.expectSearchResults('Sch', 2) // Schiffe and Schraubenzieher
	}


	@Test
	void _18testDeleteRecord() {

		EntityObject<Long> eo = client.getEntityObject(TestEntities.EXAMPLE_REST_CATEGORY, Long.valueOf((String)TestDataHelper.cat1['id']))

		// Should not be able to delete (existing dependent data)
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			assert !eo.delete()
		}

		assert eo.id

		client.expectSearchResults(TestDataHelper.catReName, 4, 200) //Note: Still three words + the category itself.

		eo = client.getEntityObject(TestEntities.EXAMPLE_REST_WORD, (Long) TestDataHelper.word1['id'])
		assert eo.delete()
		assert !eo.id // Delete successfull

		client.expectSearchResults(TestDataHelper.catReName, 3, 200) //Note: One delete, one hit less

		client.expectSearchResults('Fl', 1) // Only Flugzeuge now
	}

	@Ignore('DocumentFileStore does not work with lucene yet')
	@Test
	void _20_testDocumentFile() {
		client.expectSearchResults('Blind', 0)

		ServerIntegrationTest.runServerTest("documentFileStoreFileInsertMD")

		client.expectSearchResults('Blind', 1) // Multiple hits of "Blind" in the document, but one document

	}

	@Test
	void _99disableIndexer() {
		nuclosSession.managementConsole('disableIndexer')
	}

}