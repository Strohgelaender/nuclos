package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTException
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class VirtualEntityTest extends AbstractNuclosTest {
	static RESTClient client

	static Map<String, Object> word1 = new HashMap<>([text: 'Haus', times: 3, agent: 'test'])
	static Map<String, Object> word3 = new HashMap<>([text: 'Pferd', times: 2, agent: 'test'])
	static Map<String, Object> word4 = new HashMap<>([text: 'Esel', times: 2, agent: 'test'])

	@Test
	void _00_setup() {
	}

		@Test
	void _01_createTestUser() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
	}

	@Test
	void _10_createRecord() {
		client.login()

		EntityObject<Long> word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		word.setAttributes(new HashMap<String, Object>(word1))
		assert !word.id

		client.save(word)

		assert word.id // Saving was successful

	}

	@Test
	void _20_testReadOnlyVirtualEntity() {
		List<EntityObject<Long>> lstEO = client.getEntityObjects(TestEntities.EXAMPLE_REST_VIRTUALROWORD, null)

		assert lstEO.size() == 1

		EntityObject<Long> eo = lstEO.get(0)

		assert eo.getAttribute('text') == word1.get('text')
		assert eo.getAttribute('times') == word1.get('times')

		eo.setAttribute('times', 174)

		boolean error = false
		try {
			client.save(eo)
		} catch (RESTException e) {
			error = true
		}

		// Readonly Virtual Entity cannot be updated, so an error must yield
		assert error

		eo = client.getEntityObject(TestEntities.EXAMPLE_REST_VIRTUALROWORD, eo.getId())

		// The values must be the same as before:
		assert eo.getAttribute('times') == word1.get('times')

	}

	@Test
	void _30_testWritableVirtualEntity() {
		List<EntityObject<Long>> lstEO = client.getEntityObjects(TestEntities.EXAMPLE_REST_VIRTUALRWWORD, null)

		assert lstEO.size() == 1

		EntityObject<Long> eo = lstEO.get(0)

		assert eo.getAttribute('text') == word1.get('text')
		assert eo.getAttribute('times') == word1.get('times')

		eo.setAttribute('times', 174)

		boolean error = false
		try {
			client.save(eo)
		} catch (RESTException e) {
			error = true
		}

		// TODO: H2 cannot update views directly, so an "instead of" trigger must be used
		if (true) {
			return;
		}

		// Writable Virtual Entity can be updated, so there must not be an error
		assert !error

		eo = client.getEntityObject(TestEntities.EXAMPLE_REST_VIRTUALRWWORD, eo.getId())

		// The values must not be the same as before:
		assert eo.getAttribute('times') != word1.get('times')
		assert eo.getAttribute('times') == 174

	}

}