package org.nuclos.test.server.rest.request

import org.junit.Test
import org.nuclos.test.rest.request.RequestCounts
import org.nuclos.test.rest.request.RequestType

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RequestCountsTest {
	List<String> testRequests = [
			'GET http://172.17.0.1:8080/nuclos-war/rest/boMetas/example_rest_Order',
			'GET http://172.17.0.1:8080/nuclos-war/rest/meta/sideviewmenuselector',
			'GET http://172.17.0.1:8080/nuclos-war/rest/boMetas/example_rest_Order/layouts',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order&type=perspective',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order&type=searchtemplate,sideviewmenu,subform-table',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order&returnSubBo=true&type=subform-table',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order',
			'OPTIONS http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/query?offset=0&gettotal=true&chunksize=40&countTotal=true&search=&withTitleAndInfo=false&attributes=orderNumber,customer&sort=',
			'POST http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/query?offset=0&gettotal=true&chunksize=40&countTotal=true&search=&withTitleAndInfo=false&attributes=orderNumber,customer&sort=',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/40055986',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order',
			'GET http://172.17.0.1:8080/nuclos-war/rest/meta/sideviewmenuselector',
			'GET http://172.17.0.1:8080/nuclos-war/rest/layout/example_rest_ExampleorderLO/calculated',
			'GET http://172.17.0.1:8080/nuclos-war/rest/resources/stateIcons/example_rest_ExampleorderSM_State_10',
			'GET http://172.17.0.1:8080/nuclos-war/rest/resources/stateIcons/example_rest_ExampleorderSM_State_80',
			'GET http://172.17.0.1:8080/nuclos-war/rest/resources/stateIcons/example_rest_ExampleorderSM_State_90',
			'GET http://172.17.0.1:8080/nuclos-war/rest/layout/example_rest_ExampleorderLO/rules',
			'GET http://172.17.0.1:8080/nuclos-war/rest/boMetas/example_rest_Order/subBos/example_rest_OrderPosition_order',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos',
			'GET http://172.17.0.1:4200/roboto-v15-latin-regular.7e367be02cd17a96d513.woff2',
			'GET http://172.17.0.1:8080/nuclos-war/rest/meta/tableviewlayout/example_rest_Order/example_rest_OrderPosition',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_OrderPosition&layoutId=example_rest_ExampleorderLO&type=subform-table',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/40055986/subBos/example_rest_OrderPosition_order',
			'GET http://172.17.0.1:8080/nuclos-war/rest/boMetas/example_rest_OrderPosition',
			'GET http://172.17.0.1:8080/nuclos-war/rest/layout/example_rest_OrderPositionLO/calculated',
			'GET http://172.17.0.1:4200/assets/nuclos-rotating-48.gif',
			'POST http://clients2.google.com/service/update2?cup2key=6:2131701652&cup2hreq=97f1ff695b6358a40bad6024ca1bdb44415f1a3f9eb1ac8cb2f74f47ea758146',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Customer/40121015/subBos/example_rest_Order_customer/40121023',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos/nuclet_test_subform_Parent/40018892/subBos/recursive/nuclet_test_subform_Subform_parent/40018919/nuclet_test_subform_Subsubform_subform/40018928/nuclet_test_subform_Subsubsubform_subsubform',
			'POST http://172.17.0.1:8080/nuclos-war/rest/boGenerations/nuclet_test_other_TestObjektgenerator/40058615/2/generate/nuclet_test_other_TestObjektgeneratorGEN',
			'GET https://redirector.gvt1.com/edgedl/chrome/dict/en-us-7-1.bdic',
			'GET https://r5---sn-hoxu-h0je.gvt1.com/edgedl/chrome/dict/en-us-7-1.bdic?cms_redirect=yes&expire=1510944340&ip=88.217.255.58&ipbits=0&mm=28&mn=sn-hoxu-h0je&ms=nvh&mt=1510929881&mv=m&pl=24&shardbypass=yes&sparams=expire,ip,ipbits,mm,mn,ms,mv,pl,shardbypass&signature=3D786EF3955639959897F0B8DF4449CCC51B7EC0.1F7A4519D6FA5CFF0FF0D8C3D6B4D2584717A43B&key=cms1',
			'PUT http://172.17.0.1:8080/nuclos-war/rest/preferences/xalLzQtHMkB3DYmKWW3e/select',
			'DELETE http://172.17.0.1:8080/nuclos-war/rest/preferences/xalLzQtHMkB3DYmKWW3e/select'
	]

	@Test
	void testRequestAnalysis() {
		RequestCounts requests = new RequestCounts(testRequests)

		assert requests.getRequestCount(RequestType.EO_READ) == 1
		assert requests.getRequestCount(RequestType.EO_READ_LIST) == 1
		assert requests.getRequestCount(RequestType.EO_SUBFORM) == 2
		assert requests.getRequestCount(RequestType.EO_GENERATION) == 1

		assert requests.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
		assert requests.getRequestCount(RequestType.PREFERENCE_DESELECT) == 1

		// Should count all requests except the 2 blacklisted
		assert requests.requestCount == testRequests.size() - 3
	}

	@Test
	void blacklist() {
		assert RequestCounts.isBlacklisted('OPTIONS http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/query?offset=0&gettotal=true&chunksize=40&countTotal=true&search=&withTitleAndInfo=false&attributes=orderNumber,customer&sort=')
		assert RequestCounts.isBlacklisted('GET https://redirector.gvt1.com/edgedl/chrome/dict/en-us-7-1.bdic')
	}

	@Test
	void requestCountsToString() {
		RequestCounts requests = new RequestCounts(testRequests)
		String str = requests.toString()

		testRequests.each {
			if (!RequestCounts.isBlacklisted(it)) {
				assert str.contains(it)
			} else {
				assert !str.contains(it)
			}
		}
	}
}