package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import com.google.common.collect.Iterators
import com.xlson.groovycsv.CsvParser

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RecordGrantTest extends AbstractNuclosTest {
	static RESTClient suclient = new RESTClient('nuclos', '')
	static RESTClient client

	static Map<String, Object> word1 = new HashMap<>([text: 'Haus', times: 3, agent: 'test'])
	static Map<String, Object> word2 = new HashMap<>([text: 'Pferd', times: 2, agent: 'nuclos'])
	static Map<String, Object> word3 = new HashMap<>([text: 'Pferd', times: 2, agent: 'test'])
	static Map<String, Object> word4 = new HashMap<>([text: 'Esel', times: 2, agent: 'test'])
	static Long word2Id;

	@Test
	void _00_setup() {
	}

		@Test
	void _01_createTestUser() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
		client = new RESTClient('test', 'test')
	}

	@Test
	void _10_testRecordGrantDenied() {
		client.login()

		EntityObject<Long> word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		word.setAttributes(new HashMap<String, Object>(word1))
		assert !word.id

		client.save(word)

		assert word.id // Saving was successful

		List<EntityObject<Long>> lstEO = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, null)
		assert lstEO.size() == 1

		word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		word.setAttributes(new HashMap<String, Object>(word2))
		assert !word.id

		// Cannot save because of missing permission because of Record Grant
		expectErrorStatus(Response.Status.FORBIDDEN) {
			client.save(word)
		}

		assert !word.id

		lstEO = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, null)
		assert lstEO.size() == 1

	}

	@Test
	void _15_testUniquenessViolation() {
		// Super User in order to create word2
		suclient.login()

		EntityObject<Long> word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		word.setAttributes(new HashMap<String, Object>(word2))
		assert !word.id

		suclient.save(word) /// TODO Request failed with status 401 (Unauthorized) on jenkins

		assert word.id // Saving was successful
		word2Id = (Long)word.id

		List<EntityObject<Long>> lstEO = suclient.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, null)
		assert lstEO.size() == 2 // Super-User can see two records (word1, word2)

		// From Here Test User again

		word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		word.setAttributes(new HashMap<String, Object>(word3))
		assert !word.id

		// Cannot save because of unique violation
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			client.save(word)
		}

		assert !word.id

		lstEO = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, null)
		assert lstEO.size() == 1 // Test-User can only see one record (word1)

	}

	@Test
	void _20_testCreationSuccessful() {
		EntityObject<Long> word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)
		word.setAttributes(new HashMap<String, Object>(word4))
		assert !word.id

		client.save(word)

		assert word.id // Saving was successful

		List<EntityObject<Long>> lstEO = client.getEntityObjects(TestEntities.EXAMPLE_REST_WORD, null)
		assert lstEO.size() == 2
	}

	/**
	 * See NUCLOS-6041
	 */
	@Test
	void _25_testReadNotAllowed() {
		// No record grant to this record
		expectErrorStatus(Response.Status.FORBIDDEN) {
			client.getEntityObject(TestEntities.EXAMPLE_REST_WORD, word2Id)
		}
	}

	@Test
	void _30_exportCsv() {
		// Test user can see only 2 results via record grant
		exportResultsAsCSVAndCheckCount(client, 2)
	}

	@Test
	void _31_exportCsvWithSuperuser() {
		// Superuser can see all 3 results
		exportResultsAsCSVAndCheckCount(nuclosSession, 3)
	}

	void exportResultsAsCSVAndCheckCount(RESTClient client, int expectedResultCount) {
		final File csv = client.exportResultList(
				TestEntities.EXAMPLE_REST_WORD,
				"csv"
		)

		Iterator iter = CsvParser.parseCsv(new FileReader(csv), separator: ';')
		int size = Iterators.size(iter)

		assert size == expectedResultCount
	}
}