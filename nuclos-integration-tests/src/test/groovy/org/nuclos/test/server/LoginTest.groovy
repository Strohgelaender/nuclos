package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.rest.QueryOptions
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LoginTest extends AbstractNuclosTest {

	@AfterClass
	static void cleanup() {
		nuclosSession.stopMaintenance()
	}

	@Test
	void _00_setup() {
		deleteUserIfExists('logintest')

		RESTHelper.createUser('logintest', 'logintest', ['Example user'], nuclosSession)
		RESTHelper.createUser('logintest2', 'logintest2', [], nuclosSession)
	}

	@Test
	void _10_unknownUser() {
		expectErrorStatus(Response.Status.UNAUTHORIZED) {
			new RESTClient('unknownuser', '').login()
		}
	}

	@Test
	void _20_knownUserWrongPassword() {
		EntityObject<String> user = getUser('logintest')
		assert !user.getAttribute('loginattempts')
		assert !user.getAttribute('lastlogin')

		3.times {
			expectErrorStatus(Response.Status.UNAUTHORIZED) {
				new RESTClient('logintest', '').login()
			}

			user = getUser('logintest')
			assert user.getAttribute('loginattempts') == it + 1
			assert !user.getAttribute('lastlogin')
		}
	}

	@Test
	void _30_correctLogin() {
		new RESTClient('logintest', 'logintest').login()

		EntityObject<String> user = getUser('logintest')
		assert !user.getAttribute('loginattempts')
		assert user.getAttribute('lastlogin')
	}

	@Test
	void _40_correctLoginNoRestApi() {
		expectErrorStatus(Response.Status.FORBIDDEN) {
			new RESTClient('logintest2', 'logintest2').login()
		}
	}

	@Test
	void _90_maintenance() {
		nuclosSession.startMaintenance()

		assert nuclosSession.maintenance

		// A normal user should get a "service unavailable" error
		expectErrorStatus(Response.Status.SERVICE_UNAVAILABLE) {
			new RESTClient('test', 'test').login()
		}

		// Superuser should be able to login
		new RESTClient('nuclos', '').login()
	}

	private static void deleteUserIfExists(String username) {
		try {
			getUser(username).delete()
		} catch (Exception e) {
			// ignore
		}
	}

	private static EntityObject getUser(String username) {
		nuclosSession.getEntityObjects(
				SystemEntities.USER,
				new QueryOptions(queryWhere: "org_nuclos_businessentity_nuclosuser_name='$username'")
		).first()
	}

	private EntityObject getRole(String name) {
		nuclosSession.getEntityObjects(
				SystemEntities.ROLE,
				new QueryOptions(queryWhere: "org_nuclos_businessentity_nuclosrole_name='$name'")
		).first()
	}
}