package org.nuclos.test.server

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.UID
import org.nuclos.test.*

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class GeneratorWithProcessTest extends AbstractNuclosTest {

	@Test
	void _05_exceptionForWrongProcess() {
		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_RULES_TESTGENERATORWITHPROCESS)
		eo.setAttribute("name", "generated")
		expectErrorStatus(Response.Status.PRECONDITION_FAILED) {
			nuclosSession.save(eo)
		}
	}

	@Test
	void _10_insertAndGenerate() {
		EntityObject eo = new EntityObject(TestEntities.NUCLET_TEST_RULES_TESTGENERATORWITHPROCESS)
		eo.setAttribute("name", "test")
		nuclosSession.save(eo)

		EntityObject<Long> resultEo = nuclosSession.generateObject(
				eo,
				new UID("2KopopfnoVgOSIhuTjfR"),    // "Generator With Process"
				TestEntities.NUCLET_TEST_RULES_TESTGENERATORWITHPROCESS
		)

		EntityReference<?> reference = resultEo.getReference('nuclosProcess')
		assert reference.id == 'nuclet_test_rules_TestGeneratorWithProcess_GeneratorProcess2'
		assert reference.name == 'Generator Process 2'
	}
}
