package org.nuclos.test.webclient.sidebar

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class AttributeNameTest extends AbstractWebclientTest {

	@Test
	void _01setupBos() {
		assert $('#logout')
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_ATTRIBUGTENAMETEST)
		eo.addNew()
		eo.setAttribute('zahl', 10)
		eo.setAttribute('2zahl', 20)
		eo.save()
	}

	@Test
	void showCollumnStartingWithNumber() {
		Sidebar.addColumn(TestEntities.NUCLET_TEST_OTHER_ATTRIBUGTENAMETEST.fqn, '2zahl')
		Sidebar.resizeSidebarComponent(500)
		assert Sidebar.getValue(0, 0) == "10"
		assert Sidebar.getValue(0, 1) == "20"
	}
}
