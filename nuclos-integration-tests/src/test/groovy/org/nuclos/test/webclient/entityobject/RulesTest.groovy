package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.GenerationComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.search.Searchbar

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RulesTest extends AbstractWebclientTest {
	Map<String, String> testRulesData = [name: 'Test Rules']
	String error = context.getLocale().language == 'de' ? 'Fehler' : 'Error'

	@Test()
	void _00_createEO() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_RULES_TESTRULES)

		def entryCountBefore = Sidebar.listEntryCount

		eo.addNew()

		// Custom rule buttons should be disabled for new EOs
		assert !eo.getButton('Benutzeraktion').enabled

		eo.setAttribute('name', testRulesData['name'])
		eo.setAttribute('zahl', 12)
		eo.save()

		assertMessageModalAndConfirm(error, 'Zahl') // An error must occur because an update rule is checking for a number between 1 and 9

		eo.setAttribute('zahl', 5)

		eo.save()

		assert getMessageModal() == null // This time no error must occur

		// FIXME first added entry is not shown in sidebar
		Sidebar.refresh()

		def entryCountAfter = Sidebar.listEntryCount

		assert entryCountAfter == entryCountBefore + 1

		// Insert-Rule with Java 8 syntax
		assert eo.getAttribute('java8') == 'ok'

		assert eo.getAttribute('anlegen') == 'ok'
		assert eo.getAttribute('anlegen2') == 'ok'
		assert eo.getAttribute('anlegen3') == 'ok'

		assert eo.getAttribute('anlegenimanschluss') == 'ok'
		assert eo.getAttribute('anlegenimanschluss2') == 'ok'
		assert eo.getAttribute('anlegenimanschluss3') == 'ok'

		// InsertFinalRules update the BO
		assert eo.getAttribute('aktualisieren') == 'ok'
		assert eo.getAttribute('aktualisieren2') == 'ok'
		assert eo.getAttribute('aktualisieren3') == 'ok'
		assert eo.getAttribute('aktualisierenimanschluss') == 'ok'
		assert eo.getAttribute('aktualisierenimanschluss2') == 'ok'
		assert eo.getAttribute('aktualisierenimanschluss3') == 'ok'

		assert eo.getAttribute('regelndirektamuf') == 'ok'

		assert eo.getAttribute('loeschen') == ''
		assert eo.getAttribute('loeschen2') == ''
		assert eo.getAttribute('loeschen3') == ''
		assert eo.getAttribute('loeschenimanschluss') == ''
		assert eo.getAttribute('loeschenimanschluss2') == ''
		assert eo.getAttribute('loeschenimanschluss3') == ''
		assert eo.getAttribute('statuswechsel') == ''
		assert eo.getAttribute('statuswechsel2') == ''
		assert eo.getAttribute('statuswechsel3') == ''
		assert eo.getAttribute('statuswechselimanschluss') == ''
		assert eo.getAttribute('statuswechselimanschluss2') == ''
		assert eo.getAttribute('statuswechselimanschluss3') == ''
		assert eo.getAttribute('benutzeraktion') == ''
		assert eo.getAttribute('objektgenerator') == ''
		assert eo.getAttribute('objektgenerator2') == ''
		assert eo.getAttribute('objektgenerator3') == ''
		assert eo.getAttribute('objektgeneratorimanschluss') == ''
		assert eo.getAttribute('objektgeneratorimanschluss2') == ''
		assert eo.getAttribute('objektgeneratorimanschluss3') == ''
		assert eo.getAttribute('drucken') == ''
		assert eo.getAttribute('drucken2') == ''
		assert eo.getAttribute('drucken3') == ''
		assert eo.getAttribute('druckenimanschluss') == ''
		assert eo.getAttribute('druckenimanschluss2') == ''
		assert eo.getAttribute('druckenimanschluss3') == ''
		assert eo.getAttribute('job') == ''
		assert eo.getAttribute('statuswechselautomatisch') == ''
		assert eo.getAttribute('statusabhaengigespflichtf') == ''

		assert eo.getAttribute('inputrequiredexceptions') == ''
		assert eo.getAttribute('inputrequiredgeneraterule') == ''
		assert eo.getAttribute('inputrequiredstatechange') == ''
		assert eo.getAttribute('inputrequiredstatechangefin') == ''
	}

	@Test
	void _05_updateEO() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('name', testRulesData['name'])
		eo.save()

		assert eo.getAttribute('aktualisieren') == 'ok'
		assert eo.getAttribute('aktualisieren2') == 'ok'
		assert eo.getAttribute('aktualisieren3') == 'ok'
		assert eo.getAttribute('aktualisierenimanschluss') == 'ok'
		assert eo.getAttribute('aktualisierenimanschluss2') == 'ok'
		assert eo.getAttribute('aktualisierenimanschluss3') == 'ok'
	}

	@Test
	void _10_changeStateViaMenu() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert StateComponent.currentStateNumber == 10
		assert StateComponent.nextStateNumbers == [20]

		StateComponent.changeStateByNumeral(20)
		StateComponent.confirmStateChange()

		assert eo.getAttribute('statuswechsel') == 'ok'
		assert eo.getAttribute('statuswechsel2') == 'ok'
		assert eo.getAttribute('statuswechsel3') == 'ok'
		assert eo.getAttribute('statuswechselimanschluss') == 'ok'
		assert eo.getAttribute('statuswechselimanschluss2') == 'ok'
		assert eo.getAttribute('statuswechselimanschluss3') == 'ok'
		assert eo.getAttribute('statuswechselautomatisch') == ''
		assert eo.getAttribute('statusabhaengigespflichtf') == 'ok'

		assert StateComponent.nextStateNumbers == [30, 40, 50]
	}

	@Test
	void _11_changeStateMissingRule() {

		StateComponent.changeStateByNumeral(50)
		StateComponent.confirmStateChange()

		assertMessageModalAndConfirm(error, 'found for class=')

		assert StateComponent.currentStateNumber == 20
		assert StateComponent.nextStateNumbers == [30, 40, 50]
	}

	@Test
	void _12_changeStateInputRequired() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert StateComponent.currentStateNumber == 20
		assert StateComponent.nextStateNumbers == [30, 40, 50]

		StateComponent.changeStateByNumeral(40)
		StateComponent.confirmStateChange()

		// InputRequired from StateChangeRule
		// TODO: NUCLOS-5487

		// InputRequired from StateChangeFinalRule
		eo.clickButtonYes()
		eo.clickButtonNo()

		assert StateComponent.currentStateNumber == 20
		assert StateComponent.nextStateNumbers == [30, 40, 50]

		// TODO: NUCLOS-5487
//		assert eo.getAttribute('inputrequiredstatechange') == 'ok'
		assert eo.getAttribute('inputrequiredstatechangefin') == 'ok'
	}

	@Test
	void _15_changeStateViaButton() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot('before-change-state-button')
		eo.clickButton('Statuswechsel -> 30')
		screenshot('after-change-state-button')

		screenshot('after-change-state-button-refresh')

		assert StateComponent.currentStateNumber == 10
		assert StateComponent.nextStateNumbers == [20]

		assert eo.getAttribute('statuswechselautomatisch') == 'ok'
	}

	@Test
	void _20_generateEO() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot('before-generate-object')

		GenerationComponent.generateObjectAndConfirm('Test Rules')

		// InputRequired via GenerateRule
		GenerationComponent.clickButtonYes()
		GenerationComponent.clickButtonNo()

		// InputRequired via GenerateFinalRule
		GenerationComponent.clickButtonYes()
		GenerationComponent.clickButtonNo()

		screenshot('after-generate-object')

		assert eo.getAttribute('objektgenerator') == 'ok'
		assert eo.getAttribute('objektgenerator2') == 'ok'
		assert eo.getAttribute('objektgenerator3') == 'ok'
		assert eo.getAttribute('objektgeneratorimanschluss') == 'ok'
		assert eo.getAttribute('objektgeneratorimanschluss2') == 'ok'
		assert eo.getAttribute('objektgeneratorimanschluss3') == 'ok'

		assert eo.getAttribute('inputrequiredgeneraterule') == 'ok'
		assert eo.getAttribute('inputrequiredgeneratefinal') == 'ok'
	}

	@Test
	void _25_deleteGeneratedEO() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Sidebar.addColumn(TestEntities.NUCLET_TEST_RULES_TESTRULES.fqn, 'name')

		Searchbar.search(testRulesData.name + '_2')
		Sidebar.selectEntry(0)
		eo.delete()

		Searchbar.search(testRulesData.name)
		Sidebar.selectEntry(0)

		assert eo.getAttribute('loeschen') == 'ok'
		assert eo.getAttribute('loeschen2') == 'ok'
		assert eo.getAttribute('loeschen3') == 'ok'
		assert eo.getAttribute('loeschenimanschluss') == 'ok'
		assert eo.getAttribute('loeschenimanschluss2') == 'ok'
		assert eo.getAttribute('loeschenimanschluss3') == 'ok'
	}

	/**
	 * If the EO ist not dirty, a POST request is executed, which does not take the EO data from the client,
	 * but loads it from the DB.
	 * If the context flag "UpdateAfterExecution" is not explicitly set, the EO must be updated after custom-rule execution
	 */
	@Test
	void _30_executeCustomRule() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !eo.dirty
		eo.clickButton('Benutzeraktion')

		assert eo.getAttribute('benutzeraktion') == 'ok'
	}

	/**
	 * If the EO ist dirty, a PUT request is executed, which executes the rule and saves the eo afterwards (NUCLOS-5994).
	 * If the context flag "UpdateAfterExecution" is not explicitly set, the EO must be updated after custom-rule execution
	 */
	@Test
	void _32_executeCustomRuleWithModification() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		def name2Enter = eo.getText('name') + ' 2'

		eo.setAttribute('name', name2Enter)
		eo.setAttribute('zahl', 13)
		assert eo.dirty

		eo.clickButton('Benutzeraktion')

		screenshot("AfterBenutzerAktion")

		assert getMessageModal() == null // NUCLOS-5994 Customrule changed the value 13 to 9 before update, no error occurs

		assert eo.getAttribute('benutzeraktion') == 'ok'
		assert eo.getAttribute('name') == name2Enter
		assert eo.getAttribute('zahl') == '9' // Zahl has been updated within the CostumRule "Benutzeraktion", *before* Update (NUCLOS-5994)
		assert !eo.dirty
	}
	
	/**
	 * If the context flag "UpdateAfterExecution" is set to false, the EO must be unchanged after custom-rule execution
	 */
	@Test
	void _33_executeCustomRuleWithoutUpdate() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		def nameBefore = eo.getText('name')
		def zahlBefore = eo.getAttribute('zahl')
		def name2Enter = nameBefore + ' 3'
		def benutzeraktionBefore = eo.getAttribute('benutzeraktion')
		
		eo.setAttribute('name', name2Enter)
		eo.setAttribute('zahl', 26)
		assert eo.dirty

		eo.clickButton('BenutzeraktionKeinUpdate')

		screenshot("AfterBenutzerAktionKeinUpdate")

		assert getMessageModal() == null // No update thus no error from the update rule even though 26 is not allowed

		assert eo.getAttribute('benutzeraktion') == benutzeraktionBefore
		assert eo.getAttribute('name') == nameBefore
		assert eo.getAttribute('zahl') == zahlBefore // Zahl has not been updated within rule, but reverted because no update
		assert !eo.dirty
	}
	
	/**
	 * If the context flag "UpdateAfterExecution" is explicitly set to true, the EO must be updated after custom-rule execution
	 */
	@Test
	void _34_executeCustomRuleWithExplUpdate() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		def name2Enter = eo.getText('name') + ' 4'

		eo.setAttribute('name', name2Enter)
		eo.setAttribute('zahl', 71)
		assert eo.dirty

		eo.clickButton('BenutzeraktionMitExplUpdate')

		assertMessageModalAndConfirm(error, 'Zahl') // An error must occur because an update rule is checking for a number between 1 and 9

		eo.setAttribute('zahl', 7)

		eo.clickButton('BenutzeraktionMitExplUpdate')

		assert eo.getAttribute('benutzeraktion') == 'ok'
		assert eo.getAttribute('name') == name2Enter
		assert eo.getAttribute('zahl') == '7' // Zahl has been updated
		assert !eo.dirty
	}

//	@Test
//	void _35_print() {
//		// TODO
//	}
//
	@Test
	void _40_inputRequiredException() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.clickButton('InputRequiredExceptions')

		eo.clickButtonYes()
		eo.clickButtonNo()
		eo.clickButtonCancel()

		// Nothing should be changed after cancelling
		assert eo.getAttribute('inputrequiredexceptions') != 'ok'

		eo.clickButton('InputRequiredExceptions')

		eo.clickButtonYes()
		eo.clickButtonNo()
		eo.clickButtonOk()
		eo.clickButtonOk()

		NuclosWebElement option = $$('#input-required-dialog select option').find { it.text.contains('Option 3') }
		option.click()
		waitForAngularRequestsToFinish()

		eo.clickButtonOk()

		$('#input-required-dialog input').sendKeys('Test 123')

		eo.clickButtonOk()

		$('#input-required-dialog textarea').sendKeys('Test mit Zeilen-\numbruch')

		eo.clickButtonOk()

		assert eo.getAttribute('inputrequiredexceptions') == 'ok'
	}

//	@Test
//	void _45_job() {
//		// TODO: Start the job
//
////		Sideview.refresh()
//
////		assert eo.getAttribute('job') == 'ok'
//	}

	@Test
	void _50_messagebox() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.clickButton('Messagebox')

		String message = eo.alertText
		assert message == "HELLO"

		eo.clickButtonOk()
	}

	/**
	 * Creates a new BO and opens it in a new window/tab.
	 *
	 * Note that Selenium does not automatically update driver.windowHandle to the current window.
	 */
	@Test
	void _55_navigate() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		def windowHandle1 = driver.windowHandle

		eo.clickButton('Navigate-to-BO')

		// Assert a new window/tab was opened and is selected
		assert driver.windowHandles.size() == 2

		switchToOtherWindow()

		assert driver.windowHandle != windowHandle1
		assert currentUrl.contains('/view/nuclet_test_rules_TestRules')
	}

	@Test
	void _60_navigateLogout() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		def windowHandle1 = driver.windowHandle

		eo.clickButton('Navigate-to-Logout')

		// Logout redirects back to Login
		assert currentUrl.contains('/login')
		assert !loggedIn
	}

	@Test
	void _65_navigateLogin() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		login('test', 'test')

		assert loggedIn
		assert !currentUrl.contains('/login')

		eo.clickButton('Navigate-to-Login')

		assert loggedIn
		assert currentUrl.contains('/login')
	}

	/**
	 * A close command should be sent and the new window/tab, that was opened
	 * by the test {@link #_55_navigate()}, should be closed.
	 *
	 */
	@Test
	void _70_closeCommand() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert driver.windowHandles.size() == 2
		String handle = driver.windowHandle

		eo.open(TestEntities.NUCLET_TEST_RULES_TESTRULES)

		// Try to close the current window
		eo.clickButton('Close')

		assert driver.windowHandles.size() == 1

		driver.switchTo().window(driver.windowHandles.iterator().next())

		assert driver.windowHandle != handle
	}

	@Test
	void _75_stateDependentCustomRule() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_RULES_TESTRULES)
		eo.addNew()

		assert !stateDependentButton.enabled

		eo.setAttribute('name', 'State dependent CustomRule test')
		eo.save()

		assert !stateDependentButton.enabled

		StateComponent.changeStateByNumeral(20)
		StateComponent.confirmStateChange()

		assert stateDependentButton.enabled
	}

	private NuclosWebElement getStateDependentButton() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.getButton('State dependent CustomRule')
	}
}
