package org.nuclos.test.webclient.search

import java.text.SimpleDateFormat

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common2.DateUtils
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.search.Searchbar

import groovy.transform.CompileStatic

/**
 * TODO: Merge with org.nuclos.test.webclient.search.SearchTest2
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SearchTest extends AbstractWebclientTest {

	static Integer statusbarCount() {
		waitForAngularRequestsToFinish()
		def countString = $('#sideview-statusbar-count').text
		countString != null && countString.length() > 0 ? Integer.parseInt(countString) : null
	}

	static String customerFQN = TestEntities.EXAMPLE_REST_CUSTOMER.fqn
	static String orderFQN = TestEntities.EXAMPLE_REST_ORDER.fqn

	static final Map customer100 = [
			boMetaId  : customerFQN,
			attributes: [
					customerNumber: 100,
					name          : 'Customer 100',
					discount      : 50,
					// FIXME: Date is saved incorrectly as 1998-12-31
					birthday      : new SimpleDateFormat('yyyy-MM-dd').parse('1999-01-01')
			]
	]

	static final Map customer101 = [
			boMetaId  : customerFQN,
			attributes: [
					customerNumber: 101,
					name          : 'Customer 101',
					discount      : 70,
					// FIXME: Date is saved incorrectly as 1999-12-31
					birthday      : new SimpleDateFormat('yyyy-MM-dd').parse('2000-01-01')
			]
	]

	static final Map customer102 = [
			boMetaId  : customerFQN,
			attributes: [
					customerNumber: 102,
					name          : 'Customer 102 ÄÖÜ',
					discount      : 30,
					// FIXME: Date is saved incorrectly as 1999-12-31
					birthday      : new SimpleDateFormat('yyyy-MM-dd').parse('2000-01-01')
			]
	]

	static void insertTestData() {
		RESTHelper.createBo(customer100, nuclosSession)
		RESTHelper.createBo(customer101, nuclosSession)
	}

	@Test
	void _01setup() {
		assert $('#logout')

		insertTestData()
	}

		@Test
	void _02findCustomerViaUrl() {
		def equalSignEncoded = '%3D'
		getUrlHash("/view/$TestEntities.EXAMPLE_REST_CUSTOMER.fqn/search/customerNumber${equalSignEncoded}100")

		assert EntityObjectComponent.forDetail().getAttribute('customerNumber') == '100'

		/*
		// name like %10
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*10")
		assert Sidebar.listEntryCount == 0

		// name like %10%
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*10*")
		assert Sidebar.listEntryCount == 2

		// name like %10_
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*10%3F")
		assert Sidebar.listEntryCount == 2

		// name like %1
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*1")
		assert Sidebar.listEntryCount == 1

		// name like ___________1
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}%3F%3F%3F%3F%3F%3F%3F%3F%3F%3F%3F1")
		assert Sidebar.listEntryCount == 1

		// name like %_1_1
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*%3F1%3F1")
		assert Sidebar.listEntryCount == 1
		*/
	}

	@Test
	void _03findCustomerViaStandardSearch() {

		refresh()
		assert Sidebar.listEntries.size() == 2

		Searchbar.search('Customer 101')
		screenshot('searched-customer101')

		assert Sidebar.listEntries.size() == 1
		assert Sidebar.getValueByColumnLabel(0, 'Name').contains('Customer 101')
	}

	@Test
	void _08findCustomerViaTextSearch() {
		Searchbar.clearSearchfilter()
		Searchbar.clearTextSearchfilter()

		assert Sidebar.listEntries.size() == 2
		assert statusbarCount() == 2

		Searchbar.search('Customer 101')
		assert Sidebar.listEntries.size() == 1
		assert statusbarCount() == 1

		searchByDate:
		{
			Date date = new Date(98, 11, 31)	// 31.12.1998
			String formattedDate = DateUtils.formatDateWithFullYear(date, context.locale)
			Searchbar.search(formattedDate)

			assert Sidebar.listEntries.size() == 1
			assert Sidebar.findEntryByText('Customer 100')
		}
	}

	@Test
	void _09findCustomerWithUmlautViaTextSearch() {
		RESTHelper.createBo(customer102, nuclosSession)

		Searchbar.clearSearchfilter()
		Searchbar.clearTextSearchfilter()

		assert Sidebar.listEntries.size() == 3
		assert statusbarCount() == 3
		Searchbar.search('ÄÖÜ')
		assert Sidebar.listEntries.size() == 1
		assert statusbarCount() == 1
	}

}

