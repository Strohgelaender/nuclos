package org.nuclos.test.webclient.chart

import org.apache.commons.io.IOUtils
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.chart.ChartModal

import groovy.transform.CompileStatic

/**
 * Charts mit Daten aus Diagramm Datenquelle
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ChartDiagramSourceTest extends AbstractWebclientTest {

	static String chartOptionsString = IOUtils.toString(
			ChartTest.class.getResourceAsStream('chart-config-for-diagram-source.json')
	)

	@Test
	void _01setupBos() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void _02configureChart() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		// select 2nd entry
		Sidebar.selectEntryByText('2001')

		eo.openCharts()

		ChartModal.newChart()
		ChartModal.chartOptions = chartOptionsString

		ChartModal.saveChart()

		// number of bars
		waitFor(3) { $$('.modal-dialog nvd3 .nv-bar').size() == 12 }

		screenshot('first-chart')

		assert $$('.modal-dialog svg .nv-multiBarWithLegend').size() == 1
	}

	@Test
	void _03filterChartData() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ChartModal.setFilter(0, 'Hardware')
		ChartModal.applyFilter()

		waitFor(3) { $$('.modal-dialog nvd3 .nv-bar').size() == 6 }

		screenshot('chart-filter-1')

		ChartModal.setFilter(1, '2014.06')
		ChartModal.applyFilter()

		waitFor(3) { $$('.modal-dialog nvd3 .nv-bar').size() == 4 }

		screenshot('chart-filter-2')

		ChartModal.setFilter(2, '2015.04')
		ChartModal.applyFilter()

		waitFor(3) { $$('.modal-dialog nvd3 .nv-bar').size() == 2 }

		screenshot('chart-filter-3')
	}

}
