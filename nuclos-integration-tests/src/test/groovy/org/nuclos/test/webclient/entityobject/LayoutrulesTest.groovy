package org.nuclos.test.webclient.entityobject

import java.text.SimpleDateFormat

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LayoutrulesTest extends AbstractWebclientTest {
	String layoutRulesSubformFqn = 'nuclet_test_other_TestLayoutRulesSubform_testlayoutrules'

	@Test
	void _00_setup() {
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTRULES)
		eo.addNew()

		eo.setAttribute('name', 'test')
		eo.save()
	}

	@Test
	void _05_vlpWithDateParam() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues users = eo.getLOV('userlov2')

		setDateToFuture:
		{
			eo.setAttribute('date', new Date() + 1)
			users.open()

			// The VLP should return no entries for future dates
			assert users.choices == ['']

			users.close()
		}

		setDateToPast:
		{
			eo.setAttribute('date', new Date(0, 0, 1))
			users.open()

			// The VLP should return no entries for future dates
			assert users.choices.containsAll(['', 'nuclos', 'test'])
		}

		eo.cancel()
	}

	/**
	 * Tests if layout rules are executed on a new, dirty EO.
	 */
	@Test
	void _10_testNewEO() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()

		assert eo.getAttribute('clear1') == 'Clear me'

		eo.setAttribute('textvaluechanged', 'test')
		assert eo.getAttribute('clear1') == ''

		eo.cancel()
	}

	@Test
	void _20_testTextValueChanged() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.getAttribute('clear1') == 'Clear me'
		assertInitialLOVValues:
		{
			ListOfValues lov = eo.getLOV('userlov1')
			assert !lov.textValue

			lov.open()
			assert lov.choices.containsAll(['', 'nuclos', 'test'])

			lov.close()
		}

		eo.setAttribute('textvaluechanged', 'nuclos')
		assert eo.getAttribute('clear1') == ''

		assertVLPWasRefreshed:
		{
			ListOfValues lov = eo.getLOV('userlov1')

			assert !lov.textValue || !lov.textValue == ' '

			lov.open()
			assert lov.choices.containsAll(['', 'nuclos'])

			lov.selectEntry('nuclos')
			lov.close()
			assert lov.textValue == 'nuclos'
		}
	}

	@Test
	void _30_testRefValueChanged() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.getAttribute('clear2') == 'Clear me'

		ListOfValues lov = eo.getLOV('refvaluechanged')
		lov.selectEntry('nuclos')

		assert lov.textValue == 'nuclos'
		assert eo.getAttribute('clear2') == ''
		assert eo.getAttribute('transfervalue2') == 'nuclos'
	}

	@Test
	void _40_testSubformRules() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(layoutRulesSubformFqn)
		Subform.Row row = subform.newRow()

		eo.clickButtonOk()

		assert row.getValue('clear1') == 'Clear me'

		row.enterValue('textvaluechanged', 'nuclos')

		assert row.getValue('clear1') == ''
		assert row.getValue('userlov1') == ''

		// Selecting a new value in the LOV should transfer the username
		row.getLOV('userlov1').selectEntry('nuclos')

		assert row.getValue('username') == 'nuclos'
	}

	@Test
	void _50_testSubformReinitialization() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('refvaluechanged')

		lov.selectEntry('')

		// Subform should have been re-initialized after a new value was selected in the LOV
		Subform subform = eo.getSubform(layoutRulesSubformFqn)
		assert subform.rowCount == 0

		eo.cancel()
	}


	@Test
	void _51_testSubformVlpParamFromMainEo() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(layoutRulesSubformFqn)

		Subform.Row row = subform.newRow()

		// Close the edit modal
		eo.clickButtonOk()

		eo.setAttribute('textvaluechanged', 'nonexistentuser')

		ListOfValues lov = row.getLOV('userlov2')
		lov.open()
		assert lov.choices == ['']

		// TODO: lov.close() does not work, because the subform lov editor
		// is sometimes in the wrong place
		lov.closeViaEscape()

		eo.setAttribute('textvaluechanged', 'nuclos')
		lov.open()
		assert lov.choices == ['', 'nuclos']
		lov.selectEntry('nuclos')

		eo.cancel()
	}

	@Test
	void _52_testVlpParamInModal() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(layoutRulesSubformFqn)

		// Opens a new subform entry via modal
		subform.newRow()

		EntityObjectComponent eoModal = EntityObjectComponent.forModal()

		assert eoModal.getAttribute('clear1') != ''
		assert eoModal.getAttribute('username') == ''

		eoModal.setAttribute('textvaluechanged', 'nonexistentuser')

		ListOfValues lov = eoModal.getLOV('userlov1')

		lov.open()
		assert lov.choices == ['']
		lov.close()
		assert eoModal.getAttribute('clear1') == ''
		assert eoModal.getAttribute('username') == ''

		eoModal.setAttribute('textvaluechanged', 'nuclos')
		assert eoModal.getAttribute('clear1') == ''

		lov.open()
		assert lov.choices == ['', 'nuclos']
		lov.selectEntry('nuclos')
		assert eoModal.getAttribute('username') == 'nuclos'

		eoModal.clickButtonOk()
		eo.cancel()
	}

	@Test
	void _53_testSubsubformVlpParamFromSubformEoInModal() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(layoutRulesSubformFqn)
		// Opens a new subform entry via modal
		subform.newRow()

		editInModal:
		{
			EntityObjectComponent eoModal = EntityObjectComponent.forModal()

			Subform subsubform = eoModal.getSubform('nuclet_test_other_TestLayoutRulesSubSubform_parent')
			Subform.Row row = subsubform.newRow()
			ListOfValues lov = row.getLOV('userlov1')
			lov.open()
			assert lov.choices.containsAll(['', 'nuclos', 'test'])
			lov.close()

			eoModal.setAttribute('textvaluechanged', 'nuclos')

			lov.open()
			assert lov.choices == ['', 'nuclos']
			lov.selectEntry('nuclos')

			eoModal.clickButtonOk()
		}

		eo.cancel()
	}

	/**
	 * Changes of the "nuclos process" attribute can trigger layout changes.
	 *
	 * Order can have no nuclos-process selected, or "Priority order".
	 * Both have a different layout, which should be dynamically loaded whenever the nuclos-process is changed.
	 */
	@Test
	void _55_testNuclosProcessChange() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		eo.addNew()

		assertLayoutDefault(eo)

		eo.setAttribute('orderNumber', 123)
		eo.getLOV('nuclosProcess').selectEntry('Priority order')

		assertLayoutPriority(eo)

		eo.save()
		assertLayoutPriority(eo)

		eo.getLOV('nuclosProcess').selectEntry('')
		assertLayoutDefault(eo)

		// Reset should restore the previous nuclos-process and therefore load the first layout again
		eo.cancel()
		assertLayoutPriority(eo)

		eo.getLOV('nuclosProcess').selectEntry('')
		assertLayoutDefault(eo)

		eo.save()
		assertLayoutDefault(eo)
	}

	/**
	 * Check if the correct layout is used, when a subform record with nuclosProcess and process-dependent layout
	 * is opened in a modal.
	 */
	@Test
	void _56_nuclosProcessInModal() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		Sidebar.selectEntryByText('Test-Customer 2')

		eo.selectTab('Orders')

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer')
		Subform.Row row = subform.newRow()

		row.enterValue('orderNumber', '382938')
		row.enterValue('nuclosProcess', 'Priority order')
		row.enterValue('deliveryDate', '2100-01-01')

		eo.save()

		row.editInModal()

		EntityObjectModal modal = EntityObjectComponent.forModal()

		// Delivery date is not visible in the default Order layout,
		// but is visible in the Priority Order layout.
		Date date = modal.getAttribute('deliveryDate', null, Date.class)
		String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(date)

		assert formattedDate == '2100-01-01'
	}

	@Test
	void _60_testLayoutFitsIntoWindow() {
		assert !horizontalScrollbarPresent
		assert !verticalScrollbarPresent
		assert !LayoutComponent.horizontalScrollbarPresent
		assert !LayoutComponent.verticalScrollbarPresent
	}

	/**
	 * Asserts the default Order layout ist currently shown.
	 */
	private void assertLayoutDefault(EntityObjectComponent eo) {
		// deliveryDate is only visible in priority order layout.
		assert !eo.isFieldVisible('deliveryDate')
	}

	/**
	 * Asserts the Priority Order layout ist currently shown.
	 */
	private void assertLayoutPriority(EntityObjectComponent eo) {
		// deliveryDate is only visible in priority order layout.
		assert eo.isFieldVisible('deliveryDate')
	}
}
