package org.nuclos.test.webclient.businesstest

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.businesstest.BusinessTestsAdvanced
import org.nuclos.test.webclient.pageobjects.businesstest.BusinessTestsIndex
import org.openqa.selenium.By

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class BusinessTestsIndexTest extends AbstractWebclientTest {
	@Test
	void _00_setup() {
		logout()
		login('nuclos')
	}

	@Test
	void _01_generateAndRun() {
		BusinessTestsIndex.open()
		BusinessTestsIndex.generateAndRun()

		String log = BusinessTestsAdvanced.log
		assert log
		assert log.contains('Running')

		BusinessTestsAdvanced.tests.each {
			// Assert there are no empty table cells now
			assert !$$(it, 'td').find {
				!it.text && it.findElements(By.xpath('child::*')).empty
			}, 'Found empty table cell in the business tests overview'

			final String testName =  $(it, 'td').text
			assert log.contains(testName)
		}

		assert BusinessTestsAdvanced.testsTotal > 0
		assert ['OK', 'WARNING', 'ERROR'].contains(BusinessTestsAdvanced.state)

		assert BusinessTestsAdvanced.testsGreen + BusinessTestsAdvanced.testsYellow + BusinessTestsAdvanced.testsRed == BusinessTestsAdvanced.testsTotal
		assert BusinessTestsAdvanced.duration > 0
	}
}
