package org.nuclos.test.webclient.entityobject

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectModal
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.openqa.selenium.Dimension
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformTest extends AbstractWebclientTest {

	String tabAuftragsPosition = 'tab_' + TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION.fqn + '_auftrag'
	String auftragNo1stTransition = TestEntities.EXAMPLE_REST_AUFTRAG.fqn
	String customerBoMetaId = TestEntities.EXAMPLE_REST_CUSTOMER.fqn

	Map customer = [customerNumber: 123, name: 'Customer 123']
	String subBoMetaId = TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer'
	String orderSubBoMetaId = TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer'


	@BeforeClass
	static void setup() {
		setup(true, true)
		RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)
	}

	@Test
	void _00_createEo() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		countRequests {
			eo.addNew()

			screenshot('createnewentry-b')

			def testEntryName = '42'

			eo.setAttribute('customerNumber', testEntryName)
			eo.setAttribute('name', testEntryName)

			screenshot('createnewentry-c')
			eo.save()
			screenshot('createnewentry-d')
		}.with {
			assert it.getRequestCount(RequestType.EO_CREATE) == 1
			assert it.getRequestCount(RequestType.EO_SUBFORM) == 1
			assert it.getSqlCount(RequestType.EO_CREATE) < 5

			assert it.sqlCount < 10
			assert it.sqlTime < 100
		}
	}

	@Test
	void _01_editSmallSubform() {

		EntityObject article = new EntityObject(TestEntities.EXAMPLE_REST_ARTICLE)
		article.setAttribute('articleNumber', 2000)
		article.setAttribute('price', 2000)
		article.setAttribute('name', 'Test-Article')
		nuclosSession.save(article)

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		Subform.Row subformRow = subform.newRow()
		subformRow.enterValue('article', '2000 Test-Article')
		assert subformRow.getValue('article') == '2000 Test-Article'

		// resize window
		driver.manage().window().setSize(new Dimension(1024, 550))

		subformRow = subform.newRow()
		subformRow.enterValue('article', '2000 Test-Article')
		assert subformRow.getValue('article') == '2000 Test-Article'

		screenshot('edit-small-subform')

		eo.cancel()

		resizeBrowserWindow()
	}

	@Test
	void _02_overlayStatusText() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		eo.addNew()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')
		assertOverlayTextForEmpty(subform)

		subform.newRow()
		assert !subform.overlayText

		subform.selectAllRows()
		subform.deleteSelectedRows()

		assertOverlayTextForEmpty(subform)

		eo.cancel()

	}

	@Test
	void _03_checkboxTest() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')

		Subform.Row firstRow = subform.newRow()

		// go to catgory
		firstRow.getFieldElement('category').click()

		// tab to versandt
		sendKeys(Keys.TAB)

		assert !firstRow.getValue('shipped', Boolean.class)
		// toggle checkbox -> true
		sendKeys(Keys.SPACE)
		assert firstRow.getValue('shipped', Boolean.class)

		// toggle checkbox -> false
		sendKeys(Keys.SPACE)
		assert !firstRow.getValue('shipped', Boolean.class)

		// toggle checkbox via click -> true
		firstRow.getFieldElement('shipped').click()
		assert firstRow.getValue('shipped', Boolean.class)

		// toggle checkbox via click -> false
		firstRow.getFieldElement('shipped').click()
		assert !firstRow.getValue('shipped', Boolean.class)

		eo.cancel()
	}

	@Test
	void _05_insertSubformEntryInModal() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		eo.getTab('Orders').click()

		Subform subform = eo.getSubform(orderSubBoMetaId)
		subform.newRow()

		EntityObjectModal eoModal = EntityObjectComponent.forModal()
		parentRefFieldShouldBeDisabled:
		{
			assert eoModal.getAttribute('customer') == '42'
			assert eoModal.isAttributeReadonly('customer')
		}

		eoModal.setAttribute('orderNumber', 111)
		eoModal.clickButtonOk()

		eo.save()

		assert subform.rowCount == 1

	}

	static Map<String, Boolean> fieldsInSubOrder = [
			'orderDate'          : true,
			'deliveryDate'       : true,
			'discount'           : true,
			'approvedcreditlimit': true,
			'confirmation'       : false,
			'order'              : false,

			// Textarea
			'note'               : true,

			'sendDate'           : true,

			// LOVs
			'rechnung'           : true,
			'nuclosState'        : true,
	]

	@Test
	void _06_testEnteringAndTabbing() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Subform subform = eo.getSubform(orderSubBoMetaId)

		Subform.Row row = subform.getRow(0)

		boolean first = true

		// NUCLOS-6489 q) Test ENTER and ARROW_RIGHT
		fieldsInSubOrder.each { String field, boolean editable ->
			NuclosWebElement cell = row.getFieldElement(field)

			if (first) {
				// Get focus once
				cell.click()
				sendKeys(Keys.ESCAPE)
				first = false
			}

			assert cell.hasClass('ag-cell-focus'): "$field does not have focus class"

			if (editable) {
				sendKeys(Keys.ENTER) // Edit

				NuclosWebElement editor = row.getEditor(cell)
				assert editor: "No editor for $field"

				// Unedit
				if (editor.tagName == 'input' && !editor.hasClass('ui-autocomplete-input')) {
					sendKeys(Keys.ENTER)
				} else {
					// Textarea is not closed by Enter
					sendKeys(Keys.ESCAPE)
				}

				cell = row.getFieldElement(field)

				editor = row.getEditor(cell)

				assert !editor: "$field still has an editor"

				assert cell.hasClass('ag-cell-focus'): "$field does not have focus class"
			}

			sendKeys(Keys.ARROW_RIGHT)
			if (!field.toString().equals("nuclosState")) { // except last column (nuclosState)
				assert cell.hasClass('ag-cell-no-focus'): "$field still has the focus class"
				// Focus lost => Navigation works.
			}
		}

		first = true
		// NUCLOS-6489 l) Test TAB
		fieldsInSubOrder.findAll { it.value }.each { String field, boolean editable ->
			NuclosWebElement cell = row.getFieldElement(field)
			if (first) {
				cell.click()
				first = false
			}

			NuclosWebElement editor = subform.getPopupEditor(cell)
			assert editor: "No editor for $field"

			// TODO: Also do some editing

			sendKeys(Keys.TAB)
		}
	}

	@Test
	void _07_saveTextarea() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(orderSubBoMetaId)
		Subform.Row row = subform.getRow(0)
		assert !row.getValue('note')
		row.getFieldElement('note').click()
		sendKeys('Test')

		eo.save()
		row = subform.getRow(0)
		assert row.getValue('note') == 'Test'
	}

	@Test
	void _10_checkDirty() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Subform subform = eo.getSubform(orderSubBoMetaId)

		Subform.Row row = subform.getRow(0)
		assert row.getValue('orderNumber') == '111'

		// TODO: Row becomes already dirty now, when the first '1' is entered
//		row.enterValue('orderNumber', '111')
		assert !row.isDirty()

		row.enterValue('orderNumber', '112')
		assert row.isDirty()

		eo.cancel()

		assert !row.getValue('discount')
		row.enterValue('discount', '')

		assert !row.isDirty()

		assert !row.getValue('orderDate')
		row.enterValue('orderDate', '')

		assert !row.isDirty()

		row.enterValue('discount', '3')
		assert row.isDirty()

		eo.cancel()
	}

	@Test
	void _12_editSubformEntryInModal() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(orderSubBoMetaId)
		countRequests {
			subform.getRow(0).editInModal()
		}.with {
			assert it.getRequestCount(RequestType.EO_READ) == 0

			// 1 request for the details of the subform record
			// + 1 request for the subform in the modal
			assert it.getRequestCount(RequestType.EO_SUBFORM) == 2
		}

		EntityObjectModal eoModal = EntityObjectComponent.forModal()
		parentRefFieldShouldBeDisabled:
		{
			assert eoModal.getAttribute('customer') == '42'
			assert eoModal.isAttributeReadonly('customer')
		}

		assert eoModal.getAttribute('orderNumber') == '111'
		eoModal.setAttribute('orderNumber', 222)

		eoModal.clickButtonOk()

		eo.save()

		assert subform.rowCount == 1

		Subform.Row row = subform.getRow(0)
		assert row.getValue('orderNumber') == '222'
	}

	@Test
	void _15_testDynamicButtonActivation() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTSUBFORMBUTTONS)
		eo.addNew()

		Subform subform = eo.getSubform('nuclet_test_other_TestSubformButtonsDependent_reference')
		checkEmptySubform:
		{
			assert subform.newEnabled
			assert !subform.deleteEnabled
			assert !subform.cloneEnabled
		}

		Subform.Row row = subform.newRow()
		assert !row.selected

		checkSubformWithoutSelection:
		{
			assert subform.newEnabled
			assert !subform.deleteEnabled
			assert !subform.cloneEnabled
		}

		row.selected = true
		assert row.selected

		checkSubformWithSelection:
		{
			assert subform.newEnabled
			assert subform.deleteEnabled
			assert subform.cloneEnabled
		}

		eo.cancel()
	}

	@Test
	void _30_createBos() {
		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_CUSTOMER, customer)
	}

	@Test
	void _32_testEditLinksForIgnoredSublayout() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subBoMetaId)
		Subform.Row subformRow = subform.newRow()
		assert subformRow.new
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		def data = [city: 'Test', zipCode: '123', street: 'Test']
		subformRow.enterValues(data)

		eo.save()
		assert !subformRow.canEditInModal()
		assert subformRow.canEditInWindow()

		subformRow.enterValue('city', 'Test 2')
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.cancel()
		assert !subformRow.canEditInModal()
		assert subformRow.canEditInWindow()

		subformRow.setSelected(true)
		subform.deleteSelectedRows()
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.save()
	}

	@Test
	void _34_testEditLinksForEnabledSublayout() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		eo.selectTab('Orders')

		Subform subform = eo.getSubform(orderSubBoMetaId)
		Subform.Row subformRow = subform.newRow()
		assert subformRow.new
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		EntityObjectModal eoModal = EntityObjectComponent.forModal()
		eoModal.setAttribute('orderNumber', '123')
		eoModal.clickButtonOk()
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.save()
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		subformRow.enterValue('orderNumber', '1234')
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.cancel()
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		subformRow.editInModal()
		eoModal = EntityObjectComponent.forModal()
		eoModal.setAttribute('orderNumber', '1234')
		eoModal.clickButtonOk()
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.save()
		assert subformRow.getValue('orderNumber') == '1234'

		subformRow.setSelected(true)
		subform.deleteSelectedRows()
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.save()
	}

	@Test
	void _35_createEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.selectTab('Address')

		Subform subform = eo.getSubform(subBoMetaId)
		Subform.Row subformRow = subform.newRow()
		assert subformRow.new
		// Editing should start automatically for first cell of new row - check if there is a cell editor
		assert subformRow.getEditor(subformRow.getFieldElement('city'))

		// Expect validation error for empty subform row
		eo.save()

		// FIXME: Create proper error dialog
//		assert EntityObject.getErrorMessages().size() == 1
//		assert EntityObject.getErrorStacktraces().first().getAttribute('innerHTML').contains('common.exception.novabitvalidationexception')
		eo.clickButtonOk()

		assert EntityObjectComponent.getErrorMessages().size() == 0

		// Enter valid data
		def data = [city: 'Sauerlach', zipCode: '82054', street: 'Mühlweg 2']
		subformRow.enterValues(data)

		assert subformRow.getValue('city') == data.city
		assert subformRow.getValue('zipCode') == data.zipCode
		assert subformRow.getValue('street') == data.street

		eo.save()

		assert subformRow.getValue('city') == data.city
		assert subformRow.getValue('zipCode') == data.zipCode
		assert subformRow.getValue('street') == data.street

		subformRow = subform.newRow()
		data = [city: 'Sauerlach_2', zipCode: '82054_2', street: 'Mühlweg 2_A']
		subformRow.enterValues(data)

		subformRow = subform.newRow()
		data = [city: 'Sauerlach_3', zipCode: '82054_3', street: 'Mühlweg 2_B']
		subformRow.enterValues(data)

		subformRow = subform.newRow()
		data = [city: 'Sauerlach_4', zipCode: '82054_4', street: 'Mühlweg 2_C']
		subformRow.enterValues(data)

		assert subform.getRowCount() == 4

		// FIXME
//		assert EntityObject.getTab('tab_' + subBoMetaId + '_customer').getText() == 'Address (4)'

		assert subformRow.new

		countRequests {
			eo.save()
		}.with {
			assert it.getRequestCount(RequestType.EO_UPDATE) == 1
			assert it.getSqlCount(RequestType.EO_UPDATE) < 5

			// All initialized subforms are reloaded (Address, Orders)
			assert it.getRequestCount(RequestType.EO_SUBFORM) == 2

			assert it.requestCount < 10
			assert it.sqlCount < 10
			assert it.sqlTime < 100
		}

		assert !subformRow.new
		assert !subformRow.dirty
		assert !subformRow.deleted

		assert subform.getRowCount() == 4
	}

	/**
	 * The "edit in new window" link should not be visible,
	 * if the user has no rights on the target entity.
	 */
	@Test
	void _37_editInWindowWithoutRights() {
		logout()
		login('readonly', 'readonly')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS, 'customer')
		Subform.Row row = subform.getRow(0)

		assert !row.canEditInWindow()

		logout()
		login('test', 'test')
	}

	@Test
	void _40_updateEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subBoMetaId)
		Subform.Row subformRow = subform.getRow(1)
		def data = [city: '', zipCode: '', street: '']

		// TODO: Uncomment the following code block when NUCLOS-4220 is fixed
		/*
subformRow.enterValues(data);


// Expect validation error for empty subform row
EntityObject.save();
expect(Sideview.getErrorMessages().count()).toBe(1);
expect(Sideview.getErrorStacktraces().first().getInnerHtml()).toContain('common.exception.novabitvalidationexception');
Sideview.clickOkButton();
expect(Sideview.getErrorMessages().count()).toBe(0);
*/
		data = [city: 'Sauerlach_5', zipCode: '82054_5', street: 'Mühlweg 2_5']

		assert !subformRow.dirty
		assert !eo.dirty
		subformRow.enterValues(data)
		assert subformRow.dirty
		assert eo.dirty

		eo.save()
		// Neu Selektieren damit die Maske g'scheit ausschaut (NUCLOS-5479)
		Sidebar.selectEntry(0)
		subform = eo.getSubform(subBoMetaId)

		for (int i = 0; i < subform.rowCount; i++) {
			if (subform.getRow(i).getValue('city') == data.city) {
				Log.debug 'Matching row: ' + i
				subformRow = subform.getRow(i)

				assert subformRow.getValue('city') == data.city
				assert subformRow.getValue('zipCode') == data.zipCode
				assert subformRow.getValue('street') == data.street
			}
		}
	}

	@Test
	void _42_checkInputIsNotLost() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subBoMetaId)
		Subform.Row subformRow = subform.getRow(0)

		assert !subformRow.dirty
		assert !eo.dirty
		String oldStreet = subformRow.getValue('street')
		String newStreet = "$oldStreet 2"

		enterValueWithoutLeavingEditor:
		{
			NuclosWebElement cell = subformRow.getFieldElement('street')
			cell.click()
			sendKeys(newStreet)
		}

		assert eo.dirty
		assert subformRow.dirty

		eo.save()

		assert subformRow.getValue('street') == newStreet

		subformRow.enterValue('street', oldStreet)
		eo.save()
	}

	@Test
	void _45_testCloning() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subBoMetaId)
		subform.selectAllRows()
		assert subform.rowCount == 4

		assert !eo.dirty
		subform.cloneSelectedRows()
		assert subform.getRow(0).new
		assert eo.dirty

		eo.save()

		// uncheck "select all rows" checkbox
		subform.unselectAllRows()
		assert subform.rowCount == 8

		// FIXME
//		assert EntityObject.getTab('tab_' + subBoMetaId + '_customer').getText() == 'Address (8)'

	}

	@Test
	void _52_testDirtyState() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.getTab('Orders').click()

		Subform orderSubform = eo.getSubform(orderSubBoMetaId)
		Subform.Row row = orderSubform.newRow()

		// Close the edit modal
		EntityObjectComponent.clickButtonOk()

		row.enterValues([orderNumber: '123'])

		eo.save()

		assert orderSubform.rowCount == 1

		rowShouldBecomeDirtyWhileEditing:
		{
			assert !row.new
			assert !row.dirty
			assert !row.deleted

			NuclosWebElement cell = row.getFieldElement('orderNumber')
			cell.click()
			assert !row.dirty
			sendKeys('1234')
			assert row.dirty
			sendKeys(Keys.ENTER)
			assert row.dirty

			eo.save()
		}

		// Reference field
		assertDirtyStateAndReset {
			orderSubform.getRow(0).enterValue('customerAddress', 'Sauerlach, 82054, Mühlweg 2')
		}

		// Date chooser
		assertDirtyStateAndReset {
			orderSubform.getRow(0).enterValue('sendDate', '2017-12-31')
		}
	}

	private static void assertDirtyStateAndReset(Closure c) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !eo.dirty
		c()
		assert eo.dirty
		eo.cancel()
	}

	@Test
	void _55_deleteEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.getTab('Address').click()
		Subform subform = eo.getSubform(subBoMetaId)

		int rowCount = subform.rowCount

		subform.getRow(1).selected = true
		subform.getRow(3).selected = true

		screenshot('before-deletion')
		assert !eo.dirty
		assert !subform.getRow(1).deleted
		subform.deleteSelectedRows()
		assert subform.getRow(1).deleted
		assert eo.dirty
		screenshot('after-deletion')

		assert subform.rowCount == rowCount
		assert subform.deletedRowCount == 2

		eo.save()

		assert subform.getRowCount() == rowCount - 2

		subform.selectAllRows()
		for (int i = 0; i < subform.rowCount; i++) {
			assert subform.getRow(i).selected
		}

		subform.deleteSelectedRows()

		assert subform.deletedRowCount == subform.rowCount

		eo.save()

		assert subform.rowCount == 0
	}

	@Test
	void _60_testSubsubform() {
		// TODO
	}


	def goDocument = "org_nuclos_businessentity_nuclosgeneralsearchdocument"
	def tabGoDocument = "tab_" + goDocument + "_genericObject"
	def tabNotiz = "tab_Notiz"

	@Test
	@Ignore('TODO: Paging is not implemented in Webclient2 yet')
	void _75_testSubformPaging() {
//		assert logout()
//		assert login('nuclos', '')
//
//		def anzahl = 250
//
//		Sideview.createBo(auftragNo1stTransition, [name: 'Auftrag 3', anzahl: anzahl])
//		WebElement tab = Sideview.getTab(tabAuftragsPosition)
//
//		assert tab != null
//		assert tab.getText() == "Position (0)"
//
//		Subform subform = EntityObject.getSubform(auftragsPosition)
//
//		assert subform != null
//		assert subform.rowCount == 0
//
//		Sideview.clickButton('Regel 1') //Start Rule that adds 'anzahl' Words to the subform
//
//		screenshot('afterClickRegelButton1')
//
//		List<WebElement> lstRows = $$('.ui-grid-row')
//
//		assert lstRows.size() >= 30 && lstRows.size() <= 50 // TODO: lstRows.size() should be exactly 40?
//		assert tab.getText() == "Position (40/" + anzahl + ")"
//
//		assert Sideview.getTab(tabGoDocument).getText() == "Dokumente (0)"
//		assert Sideview.getTab(tabNotiz).getText() == "Notiz"
//
//		// scroll down subform list //TODO: Doesn't Work yet
//		new Actions(driver).moveToElement($$('.ui-grid-row')[30]).build().perform();
//
//		lstRows = $$('.ui-grid-row')
//
//		//TODO: Doesn't work because of scrolling didn't work
//		//assert lstRows.size() >= 60 && lstRows.size() <= 80
	}

	@Test
	@Ignore('TODO: Paging is not implemented in Webclient2 yet')
	void _80_testSortingWithPaging() {
//		// TODO change sort direction doesn't work in phantomjs
//		if (AbstractWebclientTest.browser != AbstractWebclientTest.Browser.PHANTOMJS) {
//
//			Subform subform = EntityObject.getSubform(auftragsPosition)
//
//			Map<String, Integer> mp = countStartingCharacters(subform, 10)
//
//			//Unsorted, neither lot of 'A' or 'z'
//
//			Integer countA = mp.get('a')
//			assert countA == null || countA < 2
//
//			Integer countz = mp.get('z')
//			assert countz == null || countz < 2
//
//			subform.sort(1)
//
//			screenshot("SortAsc")
//
//			mp = countStartingCharacters(subform, 10)
//
//			//Sorted Asc, lot of 'a' but no 'z'
//
//			countA = mp.get('a')
//			assert countA >= 5
//
//			countz = mp.get('z')
//			assert countz == null
//
//			subform.sort(1)
//
//			screenshot("SortDesc")
//
//			mp = countStartingCharacters(subform, 10)
//
//			//Sorted Desc, lot of 'z' but no 'a'
//
//			countA = mp.get('a')
//			assert countA == null
//
//			countz = mp.get('z')
//			assert countz >= 5
//
//			// new entry - sort should apply first after saving
//
//			subform.newRow()
//			subform.getRow(0).enterValues([name: 'Nuclos'])
//
//			assert subform.getRow(0).getValue('name') == 'Nuclos'
//
//			subform.sort(1) //Delete Sorting
//			subform.sort(1) //SortingAsc
//
//			//Must still be at top
//			assert subform.getRow(0).getValue('name') == 'Nuclos'
//
//			mp = countStartingCharacters(subform, 10)
//			//The 'z' also as sorting did not change while editing
//			countz = mp.get('z')
//			assert countz >= 5
//
//			// Sortierung wieder auf den Ausgangszustand setzen, da sonst ein weiterer Testlauf scheitert
//			// TODO Reset über Rest-Methode
//			subform.sort(1) //SortingDesc
//			subform.sort(1) //Delete Sorting
//		}
	}


	@Test
	@Ignore('TODO: Paging is not implemented in Webclient2 yet')
	void _85_testDynamicLoading() {
//		def anzahl = 250
//		Subform subform = EntityObject.getSubform(auftragsPosition)
//
//		WebElement tab = Sideview.getTab(tabAuftragsPosition)
//
//		// scroll down subform list
//		subform.scrollToRow(30)
//		sleep(1000)
//		waitForAngularRequestsToFinish()
//
//		List<WebElement> lstRows = $$('.ui-grid-row')
//
//		assert lstRows.size() > 40
	}

	@Test
	void _88_openDynamicSubformEntity() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPROXYCONTAINER)
		screenshot('After Open Dynamic Subform Entity')

		assert eo.getAttribute('name') == 'Test 2'

		eo.selectTab('Dynamic Proxy')
		Subform subform = eo.getSubform('nuclet_test_other_DynamicProxyContainerDYN_INTIDTUDGENERICOBJECT')

		subform.getRow(0).editInWindow()

		checkOtherWindow:
		{
			switchToOtherWindow()

			EntityObjectComponent eo2 = EntityObjectComponent.forDetail()
			assert eo2.id == eo.id
			assert currentUrl.contains('/' + TestEntities.NUCLET_TEST_OTHER_TESTPROXYCONTAINER.fqn + '/')

			switchToOtherWindow()
		}
	}

	@Test
	void _90_tabKeyTest() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Sidebar.refresh()
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION, 'order')


		Subform.Row firstRow = subform.newRow()
		subform.newRow()

		NuclosWebElement priceWe = firstRow.getFieldElement('price')

		priceWe.click()
		sendKeys('33')

		// Tab to quantity
		sendKeys(Keys.TAB)
		sendKeys('222')

		//FIXME: I have to leave the editor with Keys.DOWN as with Keys.TAB the focus goes to the next Cell,
		//FIXME: which is a lov and the subsequential behavior is not as predicted.
		sendKeys(Keys.DOWN)

		def price = firstRow.getValue('price')
		assert price == '33.00' || price == '33,00'
		def quantity = firstRow.getValue('quantity')
		assert quantity == '222.00' || quantity == '222,00'

		// NUCLOS-6489 j) Don't leave editing cell by left-right arrow keys

		priceWe.click()
		sendKeys(Keys.ARROW_RIGHT)
		sendKeys(Keys.ARROW_LEFT)
		sendKeys(Keys.BACK_SPACE)
		sendKeys('6')
		sendKeys(Keys.TAB)

		price = firstRow.getValue('price')
		assert price == '33.60' || price == '33,60'

		// Test escape key
		priceWe.click()
		sendKeys('75')
		sendKeys(Keys.ESCAPE)

		price = firstRow.getValue('price')
		assert price == '33.60' || price == '33,60'

		// NUCLOS-6489 i) Send directly without pressing Enter

		priceWe.sendKeys('1')
		sendKeys('01')
		sendKeys(Keys.TAB)

		price = firstRow.getValue('price')
		assert price == '101.00' || price == '101,00'

		eo.cancel()
	}

	private void assertOverlayTextForEmpty(Subform subform) {
		assert ['Keine Datensätze', 'No Rows To Show'].contains(subform.overlayText)
	}

	private void assertOverlayTextForLoading(Subform subform) {
		assert ['Lade...', 'Loading...'].contains(subform.overlayText)
	}
}
