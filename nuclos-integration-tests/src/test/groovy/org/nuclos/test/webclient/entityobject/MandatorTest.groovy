package org.nuclos.test.webclient.entityobject

import static org.junit.Assert.assertThat

import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.experimental.categories.Category
import org.junit.rules.ExpectedException
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.springframework.http.HttpMethod

/**
 * TODO: Use @CompileStatic on this class.
 */
@Ignore("Fix - no succss on test-server - worked locally")
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class MandatorTest extends AbstractWebclientTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none()

	static String mandatorMuc = 'Munich'
	static String mandatorMucDev = 'Dev'
	static String mandatorMucConf = 'Conf'
	static String mandatorBer = 'Berlin'

	static String mandatorMucId
	static String mandatorMucConfId
	static String mandatorBerId

	def userMuc = 'userMuc'

	static int testArticleCounter = 0

	@BeforeClass
	static void setup() {
		setup(false, true)
	}

	@Test
	void _00_setup() {
		def sessionId = RESTHelper.login('nuclos', '')

		def mandatorMucBo = RESTHelper.createBo('org_nuclos_businessentity_nuclosmandator', [name: mandatorMuc], sessionId)
		def mandatorBerBo = RESTHelper.createBo('org_nuclos_businessentity_nuclosmandator', [name: mandatorBer], sessionId)
		mandatorMucId = mandatorMucBo.boId
		mandatorBerId = mandatorBerBo.boId

		def mandatorMucDevBo = [name: mandatorMucDev, parentMandator: [id: mandatorMucId]]
		def mandatorMucConfBo = [name: mandatorMucConf, parentMandator: [id: mandatorMucId]]
		RESTHelper.createBo('org_nuclos_businessentity_nuclosmandator', mandatorMucDevBo, sessionId)
		mandatorMucConfId = RESTHelper.createBo('org_nuclos_businessentity_nuclosmandator', mandatorMucConfBo, sessionId).boId

		// we need the path for login...
		mandatorMucDev = mandatorMuc + '/' + mandatorMucDev
		mandatorMucConf = mandatorMuc + '/' + mandatorMucConf

		RESTHelper.managementConsole('setMandatorLevel', '-package example.rest -bo Article -level 2 -package example.rest -bo Category -level 1', sessionId)

		def userMucId = RESTHelper.createUser(userMuc, userMuc, ['Example user'], sessionId).id.string

		def userMucAssignment = [mandator: [id: mandatorMucId], user: [id: userMucId]]
		RESTHelper.createBo('org_nuclos_businessentity_nuclosmandatorUser', userMucAssignment, sessionId)
	}

	@Test
	void _01_testInsertByRest() {
		// Berlin data created by nuclos (superuser), no 'chooseMandator' necessary
		def sessionId = RESTHelper.login('nuclos', '')

		RESTHelper.createBo(TestEntities.EXAMPLE_REST_CATEGORY.fqn, [name: 'berCategory1', nuclosMandator: [id: mandatorBerId]], sessionId)
		RESTHelper.createBo(TestEntities.EXAMPLE_REST_CATEGORY.fqn, [name: 'berCategory2', nuclosMandator: [id: mandatorBerId]], sessionId)
		RESTHelper.createBo(TestEntities.EXAMPLE_REST_CATEGORY.fqn, [name: 'berCategory3', nuclosMandator: [id: mandatorBerId]], sessionId)

		// Munich data created by userMuc
		sessionId = RESTHelper.login(userMuc, userMuc)
		def sessionBo = RESTHelper.getSession(sessionId)
		for (mandator in sessionBo.mandators) {
			if (mandator.mandatorId == mandatorMucId) {
				def url = mandator.links.chooseMandator.href
				RESTHelper.requestJson(url, HttpMethod.POST, sessionId, null)
			}
		}

		// mandator 'Munich' is the only available and automatically set by server
		RESTHelper.createBo(TestEntities.EXAMPLE_REST_CATEGORY.fqn, [name: 'mucCategory1'], sessionId)
		RESTHelper.createBo(TestEntities.EXAMPLE_REST_CATEGORY.fqn, [name: 'mucCategory2'], sessionId)
	}

	@Test
	void _02_testResultListSize() {
		logout()
		login(userMuc, userMuc, false, mandatorMuc)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)
		waitForAngularRequestsToFinish(1000)

		assert Sidebar.listEntryCount == 2

		logout()
		login('nuclos', '', false, mandatorBer)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)

		assert Sidebar.listEntryCount == 3
	}

	@Test
	void _03_testInsertMandatorWithWrongLevel() {
		logout()
		login(userMuc, userMuc, false, mandatorMuc)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		int sizeBefore = Sidebar.listEntryCount

		exception.expect(java.util.NoSuchElementException);
		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, getTestArticle(), mandatorMuc)
	}

	@Test
	void _04_testInsertMandatorNewNotPossible() {
		logout()
		login('nuclos', '', false, mandatorBer)

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

		assert eo.getNewButton() == null
	}


	@Test
	void _05_testInsertMandatorNotAccessible() {

		def sessionId = RESTHelper.login('nuclos', '')

		def mandatorBerConfBo = [name: 'conf', parentMandator: [id: mandatorBerId]]
		RESTHelper.createBo('org_nuclos_businessentity_nuclosmandator', mandatorBerConfBo, sessionId)
		def mandatorBerDevBo = [name: 'dev', parentMandator: [id: mandatorBerId]]
		RESTHelper.createBo('org_nuclos_businessentity_nuclosmandator', mandatorBerDevBo, sessionId)


		logout()
		login('nuclos', '', false, mandatorBer)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		int sizeBefore = Sidebar.listEntryCount

		exception.expect(java.util.NoSuchElementException);
		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, getTestArticle(), mandatorMucConf)
	}


	@Test
	void _06_testInsertMandatorNotAccessibleAndWithWrongLevel() {
		logout()
		login(userMuc, userMuc, false, mandatorMuc)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		int sizeBefore = Sidebar.listEntryCount

		exception.expect(java.util.NoSuchElementException);
		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, getTestArticle(), mandatorBer)
	}

	@Test
	void _07_testInsertWithMandatorSelection() {
		logout()
		login(userMuc, userMuc, false, mandatorMuc)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		int sizeBefore = Sidebar.listEntryCount
		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, getTestArticle(), mandatorMucDev)

		EntityObjectComponent.refresh()
		assert sizeBefore + 1 == Sidebar.listEntryCount
	}

	/**
	 * Only one mandator accessible? No mandator selection is required
	 */
	@Test
	void _08_testInsertWithoutMandatorSelection() {
		logout()
		login(userMuc, userMuc, false, mandatorMucConf)

		// es wird versucht die letzte URL wieder zu öffnen, der Datensatz kann von diesem User allerdings nicht gelesen werden -> Fehlermeldung
		//EntityObject.closeErrorMessage()

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		int sizeBefore = Sidebar.listEntryCount

		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, getTestArticle())

		EntityObjectComponent.refresh()
		assert sizeBefore + 1 == Sidebar.listEntryCount
	}

	/**
	 * Reference fields to mandator dependent bos must be auto-restricted by login
	 */
	@Test
	void _09_testInsertWithRestrictedReferenceFieldByLogin() {
		logout()
		login(userMuc, userMuc, false, mandatorMuc)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		int sizeBefore = Sidebar.listEntryCount

		// exception.expect(org.openqa.selenium.NoSuchElementException);
		def article = getTestArticle();
		article.category = 'berCategory1'
		exception.expect(java.util.NoSuchElementException);
		def eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, article, mandatorMucConf)

		eo.getCancelButton().click()
	}

	/**
	 * Reference fields to mandator dependent bos must be auto-restricted by the selected mandator for the new bo
	 */
	@Test
	void _10_testInsertWithRestrictedReferenceFieldByBoMandator() {
		$('#cancelEO').click()

		logout()
		login('nuclos', '', false, 'null')

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)
		int sizeBefore = Sidebar.listEntryCount

		exception.expect(java.util.NoSuchElementException);
		def article = getTestArticle();
		article.category = 'berCategory1'
		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, article, mandatorMucDev)
	}

	@Test
	void _11_testUpdateWithRestrictedReferenceFieldByLogin() {
		$('#cancelEO').click()
		logout()
		login(userMuc, userMuc, false, mandatorMuc)

		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, getTestArticle(), mandatorMucConf)
		exception.expect(java.util.NoSuchElementException);
		eo.setAttribute('category', 'berCategory1')
	}

	@Test
	void _12_testUpdateWithRestrictedReferenceFieldByBoMandator() {
		logout()
		login('nuclos', '', false, 'null')

		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_ARTICLE, getTestArticle(), mandatorMucDev)
		exception.expect(java.util.NoSuchElementException);
		eo.setAttribute('category', 'berCategory1')
	}

	@Test
	void _13_testUpdateWithMandatorChangeByRest() {
		def sessionId = RESTHelper.login('nuclos', '')

		def boCreated = RESTHelper.createBo(TestEntities.EXAMPLE_REST_CATEGORY.fqn, [name: 'berCategoryX', nuclosMandator: [id: mandatorBerId]], sessionId)
		boCreated.attributes.nuclosMandator = [id: mandatorMucId]

		//exception.expect(java.io.IOException);
		try {
			RESTHelper.updateBo(boCreated, sessionId)
		} catch (IOException ioex) {
			def message = ioex.getMessage()
			assertThat(message, CoreMatchers.containsString("Server returned HTTP response code: 406"))
		}
	}

	@Test
	void _14_testWrongMandatorLevelByRest() {
		def sessionId = RESTHelper.login('nuclos', '')

		try {
			RESTHelper.createBo(TestEntities.EXAMPLE_REST_CATEGORY.fqn, [name: 'berCategoryXXX', nuclosMandator: [id: mandatorMucConfId]], sessionId)
		} catch (IOException ioex) {
			def message = ioex.getMessage()
			assertThat(message, CoreMatchers.containsString("Server returned HTTP response code: 406"))
		}
	}


	private Object getTestArticle() {
		testArticleCounter++;
		def result = [articleNumber: testArticleCounter, name: 'Article ' + testArticleCounter, price: '0', category: null]
		return result
	}
}
