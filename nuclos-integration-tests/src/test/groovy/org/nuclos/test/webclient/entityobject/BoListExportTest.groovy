package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration

import groovy.transform.CompileStatic

@Ignore
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class BoListExportTest extends AbstractWebclientTest {
	
	static String testEntryName = null
	static String sessionId
	
	void checkExportResponse(String url, String sessionId, String containsText) {
		HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Cookie","JSESSIONID=" + sessionId);
		connection.connect();
		
		InputStreamReader isr = new InputStreamReader((InputStream) connection.getContent());
		BufferedReader buff = new BufferedReader(isr);
		String line;
		String text;
		while (true) {
		  line = buff.readLine();
		  text += line + '\n';
			if (line == null) {
				break
			}
		}
		
		assert connection.getResponseCode() == 200 // HTTP status ok
		
		assert text.size() > 0
		
		if (containsText != null) {
			assert text.indexOf(containsText) != -1
		}
	}
	
	@Test
	void _00_setup() {
		assert $('#logout')
		TestDataHelper.insertTestData(nuclosSession)
		assert $('#logout')
	}
	
	@Test
	void _01_checkSecurity() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

		// user 'test' has not the required permission
		assert $('#e2e-test-export-link') == null
	}

	@Test
	void _02_openPrintDialog() {

		logout()
		login('nuclos')

		RESTClient client = new RESTClient('nuclos', '').login()
		sessionId = client.sessionId

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ARTICLE)

		SideviewConfiguration.newSideviewConfiguration('Sideviewconfig1')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_name')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_active')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ARTICLE.fqn + '_price')
		SideviewConfiguration.saveSideviewConfiguration()
		SideviewConfiguration.close()
		Sidebar.resizeSidebarComponent(800)

		assert Sidebar.selectedColumns().size() == 3

		// open export dialog
		$('#export-bo-list').click()
		waitForAngularRequestsToFinish()
	}
	
	@Test
	void _03_executePrintoutPDF() {
		testExecuteDownload('pdf')
	}

	@Test
	void _04_executePrintoutCSV() {
		// assert that export contains selcted columns
		testExecuteDownload('csv', '"Name";"Active";"Price"\n"Mouse"')
	}

	@Test
	void _05_executePrintoutXLSX() {
		testExecuteDownload('xlsx')
	}

	@Test
	void _06_executePrintoutXLS() {
		testExecuteDownload('xls')
	}

	@Test
	void _07_prepareSubformExport() {
		// close modal
		closeModal()

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		subform.openViewConfiguration()
		SideviewConfiguration.newSideviewConfiguration('SubformColumnConfig1')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_quantity')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_name')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_price')
		SideviewConfiguration.clickButtonOk()

		$('.export-subbos').click()
	}

	@Test
	void _08_executeSubformPrintoutPDF() {
		testExecuteDownload('pdf')
	}

	@Test
	void _09_executeSubformPrintoutCSV() {
		// assert that export contains selcted columns
		testExecuteDownload('csv', '"Quantity";"Name";"Price"')
	}

	@Test
	void _10_executeSubformPrintoutXLSX() {
		testExecuteDownload('xlsx')
	}

	@Test
	void _11_executeSubformPrintoutXLS() {
		testExecuteDownload('xls')
	}
	
	
	
	/*
	TODO: this does not work at the moment
	
	@Test
	void _12_prepareSubformExport2() {
		closeModal()
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		// select second entry
		Sidebar.selectEntry(2)

		eo.selectTab('Orders')

		$('.export-subbos').click()
	}

	@Test
	void _13_executeSubformPrintoutXLS() {
		testExecuteDownload('xls')
	}
	*/

	private void testExecuteDownload(String outputType) {
		testExecuteDownload(outputType, null)
	}
	private void testExecuteDownload(String outputType, String containsText) {
		// select outputType
		$('#list-export-modal input[value="' + outputType + '"]').click()

		// execute export
		$('#list-export-modal #execute-listexport-button').click()

		String exportHref = $('#e2e-test-export-link').getAttribute("href")

		checkExportResponse(exportHref, sessionId, containsText)
		screenshot('export-' + outputType + '-done')
	}

	private void closeModal() {
		// close modal
		$('.close').click()
	}

}
