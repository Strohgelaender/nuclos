package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic


@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ModifiableTest extends AbstractWebclientTest {
	private Long customerId

	@Test
	void _10_insertCustomer() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		eo.addNew()
		eo.setAttribute('customerNumber', 123)
		eo.setAttribute('name', 123)
		eo.save()

		customerId = eo.id
	}

	@Test
	void _20_checkIsModifiableBO() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Log.debug 'checkIsModifiableBO'

		eo.addNew()
		screenshot('checkIsModifiableBO-a')
		eo.checkField('notmodifiable', true, false)
		eo.checkField('nuclosState', true, true)
		eo.checkField('nuclosProcess', true, true)

		eo.setAttribute('orderNumber', 42)
		eo.getLOV('customer').selectEntry('VLPText: 123')

		eo.save()
		screenshot('checkIsModifiableBO-b')
		eo.checkField('notmodifiable', true, false)
	}

	@Test
	void _25_checkIsModifiableSubBO() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER, customerId)

		eo.selectTab('Orders')
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDER, 'customer')

		// TODO: The Webclient should set a marker CSS class on disabled cells, which can be checked here
		// TODO: The background-image url should be "assets/lighten.png" for disabled fields - seems to be buggy!
		assert subform.getRow(0).getFieldElement('notmodifiable').getCssValue('background-image')?.contains('url')
	}
}
