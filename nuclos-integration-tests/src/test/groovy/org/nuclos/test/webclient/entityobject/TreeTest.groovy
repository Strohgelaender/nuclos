package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class TreeTest extends AbstractWebclientTest {
	String world = TestEntities.EXAMPLE_REST_WORLD.fqn
	String country = TestEntities.EXAMPLE_REST_COUNTRY.fqn
	String city = TestEntities.EXAMPLE_REST_CITY.fqn

	@Test
	void _01_setup() {

		Map country1 = [name: 'Australien', world: [id: 4711]]
		Map country2 = [name: 'Deutschland', world: [id: 4711]]
		Map country3 = [name: 'Schweden', world: [id: 4711]]

		def country1id = RESTHelper.createBo(country, country1, nuclosSession)['boId']
		def country2id = RESTHelper.createBo(country, country2, nuclosSession)['boId']
		def country3id = RESTHelper.createBo(country, country3, nuclosSession)['boId']

		Map city11 = [name: 'Sydney', einwohner: 5005000, land: [id: country1id]]
		RESTHelper.createBo(city, city11, nuclosSession)

		Map city12 = [name: 'Perth', einwohner: 2066000, land: [id: country1id]]
		RESTHelper.createBo(city, city12, nuclosSession)

		Map city21 = [name: 'Berlin', einwohner: 3671000, land: [id: country2id]]
		RESTHelper.createBo(city, city21, nuclosSession)

		Map city22 = [name: 'München', einwohner: 1450000, land: [id: country2id]]
		RESTHelper.createBo(city, city22, nuclosSession)

		Map city23 = [name: 'Frankfurt', einwohner: 733000, land: [id: country2id]]
		RESTHelper.createBo(city, city23, nuclosSession)

	}

	@Test
	void _05_selectWorld() {
		EntityObjectComponent eo

		countRequests {
			eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORLD)
		}.with {
			// Sidebar is opened first, there should be no tree requests yet
			assert it.getRequestCount(RequestType.TREE_ROOT) == 0
			assert it.getRequestCount(RequestType.TREE_SUBTREE) == 0
		}

		eo.checkField('name', true, false).getAttribute('value') == 'World'

		WebElement we = $('.explorerbutton')
		assert we != null && we.getText() == 'World'
		countRequests {
			we.click()
		}.with {
			assert it.getRequestCount(RequestType.TREE_ROOT) == 1
			assert it.getRequestCount(RequestType.TREE_SUBTREE) == 0
		}

		screenshot('AfterClickOnExplorerButton')

	}

	@Test
	void _07_checkTree() {

		List<NuclosWebElement> lstWe = Sidebar.getTreeRows()
		assert lstWe.size() == 3

		assert lstWe[0].getText() == 'Australien'
		assert lstWe[1].getText() == 'Deutschland'
		assert lstWe[2].getText() == 'Schweden'

		NuclosWebElement value = $(lstWe[0], '.ag-group-value')
		assert value != null

		countRequests {
			value.click()
		}.with {
			assert it.getRequestCount(RequestType.TREE_ROOT) == 0
			assert it.getRequestCount(RequestType.TREE_SUBTREE) == 0
		}

		screenshot('AustralienClicked')

		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.checkField('name', true, false).getAttribute('value') == 'Australien'
		eo.checkField('world', true, false)

		assert eo.getAttribute('world') == 'World'

	}

	@Test
	void _08_checkExpandedTree() {

		List<NuclosWebElement> lstWe = Sidebar.getTreeRows()
		NuclosWebElement contracted = $(lstWe[1], '.ag-group-contracted')
		assert contracted != null

		countRequests {
			contracted.click()
		}.with {
			assert it.getRequestCount(RequestType.TREE_ROOT) == 0
			assert it.getRequestCount(RequestType.TREE_SUBTREE) == 1
		}

		screenshot('DeutschlandExpanded')

		lstWe = Sidebar.getTreeRows()
		assert lstWe.size() == 6

		assert lstWe[0].getText() == 'Australien'
		assert lstWe[1].getText() == 'Deutschland'
		assert lstWe[2].getText() == 'Berlin'
		assert lstWe[3].getText() == 'Frankfurt'
		assert lstWe[4].getText() == 'München'
		assert lstWe[5].getText() == 'Schweden'

		NuclosWebElement value = $(lstWe[3], '.ag-group-value')
		assert value != null

		countRequests {
			value.click()
		}.with {
			assert it.getRequestCount(RequestType.TREE_ROOT) == 0
			assert it.getRequestCount(RequestType.TREE_SUBTREE) == 0
		}

		screenshot('FrankfurtClicked')

		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.checkField('name', true, true).getAttribute('value') == 'Frankfurt'
		eo.checkField('einwohner', true, true)
		eo.checkField('land', true, true)

		assert eo.getAttribute('einwohner') == '733000'
		assert eo.getAttribute('land') == 'Deutschland'

	}

	@Test
	void _09_addTreeNode() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()

		// NUCLOS-6039 Check if it is really a new record
		assert eo.getAttribute('name') == ''

		eo.setAttribute('name', 'Stockholm')
		eo.setAttribute('einwohner', 936000)
		eo.setAttribute('land', 'Schweden')

		assert eo.dirty


		eo.save()

		List<NuclosWebElement> lstWe = Sidebar.getTreeRows()

		NuclosWebElement contracted = $(lstWe[5], '.ag-group-contracted')
		assert contracted != null

		countRequests {
			contracted.click()
		}.with {
			assert it.getRequestCount(RequestType.TREE_ROOT) == 0
			assert it.getRequestCount(RequestType.TREE_SUBTREE) == 1
		}

		screenshot('SchwedenExpanded')

		lstWe = Sidebar.getTreeRows()
		assert lstWe.size() == 7
		assert lstWe[6].getText() == 'Stockholm'

	}


	@Test
	void _10_leaveTreeView() {
		WebElement we = $('.explorerbutton')
		assert we != null && we.getText() == 'World'

		countRequests {
			we.click()
		}.with {
			assert it.getRequestCount(RequestType.TREE_ROOT) == 0
			assert it.getRequestCount(RequestType.TREE_SUBTREE) == 0
		}

		screenshot('AfterClickOnExplorerButton2')

		assert  $('#sidetree') == null
	}

}