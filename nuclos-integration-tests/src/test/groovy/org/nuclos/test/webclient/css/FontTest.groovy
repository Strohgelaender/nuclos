package org.nuclos.test.webclient.css

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class FontTest extends AbstractWebclientTest {

	static final String FONT_FAMILY_DEFAULT = 'sans-serif'

	static final String FONT_SIZE_DEFAULT = '11px'
	static final String FONT_SIZE_SIDEBAR = '11px'
	static final String FONT_SIZE_BUTTON = '13px'
	static final String FONT_SIZE_MENU = '16px'
	static final String FONT_SIZE_STATUSBAR = '12px'
	static final String FONT_SIZE_SEARCH_INPUT = '12px'

	@Test
	void _00_checkCssViaSystemparameter() {
		assert $$('head > style').find {
			it.getAttribute('innerHTML')?.trim() == AbstractWebclientTest.WEBCLIENT_CSS.trim()
		}
	}

	@Test
	void _01setupBos() {
		assert $('#logout')

		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}

	@Test
	void _02detail() {

		$$('nuc-web-component input, nuc-web-component textarea, nuc-web-component span:not(.fa), nuc-web-component label').each {
			assert it.getCssValue('font-size') == FONT_SIZE_DEFAULT
			assertFontFamily(it)
		}

		$$('nuc-web-component button').each {
			def cssClass = it.getAttribute("class")
			if (it.text.length() > 0) {
				if (cssClass.indexOf("ui-autocomplete-dropdown") != -1) {
					// lov/combobox button
				} else {
					assert it.getCssValue('font-size') == FONT_SIZE_BUTTON
					assertFontFamily(it)
				}
			}
		}
	}

	@Test
	void _03sidebar() {
		String orderNumberInDetailBlock = EntityObjectComponent.forDetail().getAttribute('orderNumber')
		def orderNumberInSidebar = Sidebar.findElementContainingText('.ag-cell', orderNumberInDetailBlock)
		assert orderNumberInSidebar != null
		assert orderNumberInSidebar.getCssValue('font-size') == FONT_SIZE_SIDEBAR
		assertFontFamily(orderNumberInSidebar)
	}

	@Test
	void _04subform() {

		// subform headers and subform cells
		$$('nuc-web-subform .ag-cell, nuc-web-subform .ag-header-cell-text').each {
			it.findElements(By.cssSelector('*')).each {
				assert it.getCssValue('font-size') == FONT_SIZE_DEFAULT
				assertFontFamily(it)
			}
		}
	}

	@Test
	void _05searchInput() {
		$$('.search-input input').each {
			assert it.getCssValue('font-size') == FONT_SIZE_SEARCH_INPUT
			assertFontFamily(it)
		}
	}

	@Test
	void _06statusbar() {
		$$('nuc-statusbar sideview-statusbar-content *').each {
			assert it.getCssValue('font-size') == FONT_SIZE_STATUSBAR
			assertFontFamily(it)
		}
	}

	@Test
	void _07menu() {
		$$('nuc-menu .nav-link').each {
			assert it.getCssValue('font-size') == FONT_SIZE_MENU
			assertFontFamily(it)
		}
	}

	private void assertFontFamily(WebElement element) {
		def fontFamily = element.getCssValue('font-family')
		if (fontFamily.length() > 0
				&& element.getAttribute('class').indexOf('fa ') == -1
				&& element.getAttribute('class').indexOf('pi ') == -1) {
			assert fontFamily.indexOf(FONT_FAMILY_DEFAULT) != -1
		}
	}
}
