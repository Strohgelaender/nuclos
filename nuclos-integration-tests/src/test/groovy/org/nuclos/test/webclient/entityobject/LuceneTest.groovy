package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
@Ignore
class LuceneTest extends AbstractWebclientTest {

	@Test
	void _0setup() {
		nuclosSession.managementConsole('enableIndexerSynchronously')
		nuclosSession.managementConsole('rebuildLuceneIndex')
		// Keep this test asynchronous in order to test asynchronous indexing (compared to LuceneSearchTest)
		nuclosSession.managementConsole('enableIndexer')
	}

	@Test
	void _2createTestData() {
		TestDataHelper.createLuceneTestData(nuclosSession)
	}

	@Test
	void _4luceneTest() {
		screenshot('beforeLuceneTest')
		luceneTest(TestDataHelper.catName, 4) //Note: The three words + the category itself.
	}

	@Test
	void _5renameCategory() {

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)

		Sidebar.addColumn(TestEntities.EXAMPLE_REST_CATEGORY.fqn, 'name')

		Searchbar.search(TestDataHelper.catName)

		Sidebar.selectEntry(0)

		screenshot('afterCategorySearch')

		assert Sidebar.listEntryCount == 1

		Sidebar.selectEntry(0)

		waitForAngularRequestsToFinish()

		def inputs = $$('nuc-layout input[type="text"]')

		inputs*.clear()
		waitForAngularRequestsToFinish()
		inputs*.sendKeys(TestDataHelper.catReName)

		screenshot('afterChangeName')

		eo.save()

		screenshot('afterSaveEntry')
	}

	@Test
	void _6testReference() {

		screenshot('beforeLuceneTestReference')

		luceneTest(TestDataHelper.catName, 1) //Note: catName is still the originalName of word3

		luceneTest(TestDataHelper.catReName, 4) //Note: The trhee words + the category itself.

	}

	@Test
	void _7testRecordGrant() {

		//word4 and word5 must not appear for user test, but still the category for which there are not record grants

		luceneTest(TestDataHelper.cat2Name, 1)

	}

	@Test
	void _8testRollback() {

		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_WORD, TestDataHelper.word6)

		screenshot('errorFinalRule')

		assumeError("Times muss größer oder gleich 1 sein!")

		luceneTest(TestDataHelper.word6.text as String, 0) //A error was thrown so it must not be indexed (rollbacked)


		def inputs = $$('nuc-layout input[type="text"]')

		assert inputs.size() >= 2

		inputs[1].clear()
		inputs[1].sendKeys("37") //Correct input

		screenshot('afterInput37')

		eo.save()

		screenshot('afterSave')

		luceneTest(TestDataHelper.word6.text as String, 1) //Now it must be indexed

	}

	@Test
	void _90testDocuments() {
		luceneTest('hello', 0) // No hello until now

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_AUFTRAG)

		FileComponent fileComponent = eo.getFileComponent('dokument')
		fileComponent.setFile(nuclosDocx)

		screenshot('afterSettingDocument')

		eo.save()

		screenshot('afterSave')

		luceneTest('hello', 1) // Expect to find hello within the file

		// NUCLOS-6260 3) Do no find 'remark', because there are no rights on this attribute
		luceneTest('remark', 0)

	}

	static File nuclosDocx = new File(LuceneTest.getResource('nuclos.docx').toURI())

	@Test
	void _92testDocumentInSubform() {

		StateComponent.changeStateByNumeral(20)

		screenshot ('after ChangeState')

		luceneTest('remark', 1) // Now find remark, because there are rights in this state

		luceneTest('world', 1) // Still one time "hello world"

		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.getTab('Dokumente').click()

		Subform subform = eo.getSubform('org_nuclos_businessentity_nuclosgeneralsearchdocument_genericObject');

		subform.newRow()

		screenshot('after-new-row')

		Subform.Row row = subform.getRow(0)

		FileComponent fileComponent = row.getFileComponent('documentfile')
		fileComponent.setFile(nuclosDocx)

		eo.save()

		screenshot('after saving document into subform')

		assert subform.getRowCount() == 1 // This is also a test for NUCLOS-6265

		row = subform.getRow(0)
		Object value = row.getValue('documentfile')

		assert value == nuclosDocx.getName()

		// FIXME NUCLOS-6260 (2b)
		// luceneTest('world', 2) // Second one appeared

	}

	@Test
	void _94testDocumentOtherLayout() {

		StateComponent.changeStateByNumeral(50)

		screenshot ('after ChangeState')

		luceneTest('remark', 1) // Still find remark, because there are rights in this state

		// FIXME NUCLOS-6260 (2b) Should be 2
		luceneTest('world', 1) // Still one time "hello world", if though it is not in the layout

	}

	@Test
	void _96testDocumentNoPermissions() {

		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('anzahl', 1)
		eo.save() // Automatic state change to 60

		StateComponent.changeStateByNumeral(80)
		StateComponent.confirmStateChange()

		screenshot ('after ChangeState')

		luceneTest('remark', 0) // Find nothing, no rights on the attribute in this state

		luceneTest('world', 0) // Find nothing, no rights on the subform in this state

	}

	@Test
	void _98testRemoveDocument() {

		StateComponent.changeStateByNumeral(60)
		StateComponent.confirmStateChange()

		screenshot ('after ChangeState')

		luceneTest('remark', 1) // Again find remark, because there are rights in this state

		// FIXME NUCLOS-6260 (2b) Should be 2
		luceneTest('world', 1) // Again one time "hello world"

		if (true) {
			// TODO: Remove Document and check Lucene
			return
		}
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		FileComponent fileComponent = eo.getFileComponent('dokument')
		// FIXME: Doesn't work
		fileComponent.clearFile()

		eo.save()

		screenshot ('after Save')

		luceneTest('world', 1) // One hit for the document within subform

		Subform subform = eo.getSubform('org_nuclos_businessentity_nuclosgeneralsearchdocument_genericObject');

		Subform.Row row = subform.getRow(0)
		row.setSelected(true)

		subform.deleteSelectedRows()

		eo.save()

		screenshot ('after Save2')

		luceneTest('world', 0) // Nothing left
	}

	@Test
	void _99disableIndexer() {
		nuclosSession.managementConsole('disableIndexer')
	}

	static void assumeError(String s) {
		screenshot("errorassumed")

		String error = getErrorMessage()

		assert error
		assert !error.empty
		assert error.indexOf(s) != -1

		Dialog.close()
	}

	static String getErrorMessage() {
		return $('.modal-body')?.text
	}
}

