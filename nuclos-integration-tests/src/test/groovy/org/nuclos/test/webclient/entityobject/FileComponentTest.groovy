package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.FileComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class FileComponentTest extends AbstractWebclientTest {
	private static File nuclosImage = new File(FileComponentTest.getResource('nuclos.png').toURI())

	@Test
	void _05_uploadImage() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		eo.setAttribute('text', 'Test File Component')

		FileComponent fileComponent = eo.getFileComponent('image')
		fileComponent.setFile(nuclosImage)

		// Temporary upload URL
		waitFor {
			fileComponent.imageUrl.contains('/boImages/temp/')
		}

		eo.save()

		// Persistent image URL
		waitFor {
			fileComponent.imageUrl.contains('/boImages/' + TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS.fqn)
		}

	}

/*
TODO set Firefox profile to save files without opening file dialog
does not work when browser opens the save file dialog
	@Test
	void _06_downloadImage() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		FileComponent fileComponent = eo.getFileComponent('image')

		countRequests {
			fileComponent.downloadFile()
		}.with {
			assert it.getRequestCount(RequestType.ALL) == 1
		}
	}

	@Test(expected = Exception.class)
	void _07_deleteImage() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		FileComponent fileComponent = eo.getFileComponent('image')
		fileComponent.deleteFile()
		eo.save()

		// download of deleted file should fail
		fileComponent.downloadFile()
	}
 */

	@Test
	void _10_uploadSubformImages() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.selectTab('Subform Images')
		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTSUBFORMIMAGES.fqn + '_parent')

		3.times {
			subform.newRow()
		}

		FileComponent fileComponent = subform.getRow(1).getFileComponent('image')
		fileComponent.setFile(nuclosImage)

		// FIXME: Does not work for subforms:
		// Temporary upload URL
//		assert fileComponent.imageUrl.contains('/boImages/temp/')

		eo.save()

		fileComponent = subform.getRow(1).getFileComponent('image')

		// Persistent image URL
		waitFor {
			fileComponent.imageUrl.contains(
					'/boImages/' + TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS.fqn
							+ '/' + eo.id
							+ '/subBos/' + TestEntities.NUCLET_TEST_OTHER_TESTSUBFORMIMAGES.fqn + '_parent'
							+ '/'
			)
		}
	}

/*
	TODO set Firefox profile to save files without opening file dialog
	does not work when browser opens the save file dialog
	@Test
	void _11_downloadSubformImage() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTSUBFORMIMAGES.fqn + '_parent')
		FileComponent fileComponent = subform.getRow(1).getFileComponent('image')

		countRequests {
			fileComponent.downloadFile()
		}.with {
// TODO download count is not recognized
			println " request count: " + it.getRequestCount(RequestType.ALL)
//			assert it.getRequestCount(RequestType.ALL) == 1
		}
	}

	@Test(expected = Exception.class)
	void _12_deleteSubformImage() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTSUBFORMIMAGES.fqn + '_parent')
		FileComponent fileComponent = subform.getRow(1).getFileComponent('image')

		fileComponent.deleteFile()
		eo.save()

		// download of deleted file should fail
		fileComponent.downloadFile()
	}
*/
}
