package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.schema.layout.web.WebTextfield
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.Datepicker
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectModal
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LayoutComponentTest extends AbstractWebclientTest {

	@Test
	void _00_textareaInSubform() {
		final int standardRowHeight = 25
		final String multilineText = "Multi\nline\ntext"

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		eo.selectTab('Subform Memo')

		Subform subform = eo.getSubform('nuclet_test_other_TestLayoutComponentsMemo_parent')

		multilineViaModal:
		{
			Subform.Row row = subform.newRow()
			EntityObjectModal modal = EntityObjectComponent.forModal()
			modal.setAttribute('memo', multilineText)

			// Close the modal
			EntityObjectComponent.clickButtonOk()

			assert row.height > standardRowHeight
			assert row.getValue('memo') == multilineText
		}

		multilineInlineEditing:
		{
			Subform.Row row = subform.newRow()

			// Close the modal
			EntityObjectComponent.clickButtonOk()

			assert row.height == standardRowHeight

			row.enterValue('memo', "Single line text")
			assert row.height == standardRowHeight

			row.enterValue('memo', multilineText)
			assert row.height > standardRowHeight
			assert row.getValue('memo') == multilineText
		}

		eo.cancel()
	}

	@Test
	void _05_comboboxOnTextfield() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		Object test = eo.getAttribute('radio')
		assert test == 3 || test == '3' // Default Value für OptionGroup "RadioTest" is 3

		assert !eo.getAttribute('text', WebTextfield)

		ListOfValues lov = eo.getLOV('text')
		assert !lov.textValue
		lov.open()
		List<String> choices = lov.choices
		assert choices.containsAll(['nuclos', 'test'])
		assert !choices.contains('')

		changeValueViaTextfield:
		{
			String nonexistentuser = 'nonexistentuser'
			lov.sendKeys(nonexistentuser)

			assert eo.getAttribute('text', WebTextfield) == nonexistentuser
			choices = lov.choices
			assert choices.containsAll(['nuclos', 'test'])
			assert lov.textValue == nonexistentuser
		}

		changeValueViaLov:
		{
			lov.selectEntry('test')
			assert eo.getAttribute('text', WebTextfield) == 'test'
			assert lov.textValue == 'test'
		}

		checkValuesAfterSave:
		{
			eo.save()
			assert eo.getAttribute('text', WebTextfield) == 'test'
			assert lov.textValue == 'test'
		}

		checkValuesAfterRest:
		{
			eo.setAttribute('text', 'asdf')
			eo.cancel()
			assert eo.getAttribute('text', WebTextfield) == 'test'
			assert lov.textValue == 'test'
		}
	}

	@Test
	void _07_testSeparators() {
		NuclosWebElement nbw = $('.titled-separator')
		assert nbw
		assert nbw.isDisplayed()
		assert nbw.getText() == 'Separator with Title'
		assert nbw.getCssValue('border-bottom').startsWith('1px solid')

		nbw = $('.separator-horizontal')
		assert nbw
		assert nbw.isDisplayed()
		assert nbw.getCssValue('border-bottom').startsWith('1px solid')
		assert nbw.getCssValue('border-left').startsWith('0px none')

		nbw = $('.separator-vertical')
		assert nbw
		assert nbw.isDisplayed()
		assert nbw.getCssValue('border-bottom').startsWith('0px none')
		assert nbw.getCssValue('border-left').startsWith('1px solid')

	}

	@Test
	void _10_selectDateByDatepickerDropdown() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Date date = new Date(117, 0, 1)   // 2017-01-01

		eo.setAttribute('date', date)

		Datepicker datepicker = eo.getDatepicker('date')

		datepicker.open()
		assert datepicker.getMonth() == 'Jan'
		assert datepicker.getYear() == 2017

		changeDate:
		{
			datepicker.clickDayOfCurrentMonth(31)

			Date expectedDate = new Date(117, 0, 31)    // 2017-01-31
			assert eo.getAttribute('date', null, Date.class) == expectedDate

			assert eo.dirty
			eo.save()
		}
	}

	@Test
	void _11_selectInvalidDate() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('date', 'ABC')
		sendKeys(Keys.TAB)

		assert eo.getAttribute('date') == ''

		eo.cancel()
	}

	@Test
	void _12_selectDateByDatepickerDropdownInSubform() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.selectTab('Subform Date')
		Subform subform = eo.getSubform('nuclet_test_other_TestLayoutComponentsSubform_parent')

		subform.newRow()
		Subform.Row row = subform.newRow()
		Datepicker datepicker = row.getDatepicker('subdate')

		row.enterValue('subdate', '2017-01-01')

		datepicker.open()
		assert datepicker.getMonth() == 'Jan'
		assert datepicker.getYear() == 2017

		changeDate:
		{
			datepicker.clickDayOfCurrentMonth(31)

			// NUCLOS-6498 r)
			Subform.Row rowEmpty = subform.getRow(1)
			NuclosWebElement column = row.getFieldElement('subdate')
			NuclosWebElement columnEmpty = rowEmpty.getFieldElement('subdate')

			assert column.hasClass('ag-cell-focus')
			assert columnEmpty.hasClass('ag-cell-no-focus')
			column.sendKeys(Keys.ARROW_DOWN)

			assert column.hasClass('ag-cell-no-focus')
			assert columnEmpty.hasClass('ag-cell-focus')

			rowEmpty.setSelected(true)
			subform.deleteSelectedRows()

			Date expectedDate = new Date(117, 0, 31)    // 2017-01-31
			assert row.getValue('subdate', Date.class) == expectedDate

			assert eo.dirty
			eo.save()
			assert !eo.dirty

 			assert row.getValue('subdate', Date.class) == expectedDate
		}
	}

	@Test
	void _13_enterInvalidDateInSubform() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.selectTab('Subform Date')
		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTSSUBFORM, 'parent')

		subform.newRow()
		Subform.Row row = subform.newRow()

		row.enterValue('subdate', 'ABC')
		assert row.getValue('subdate') == ''

		NuclosWebElement cell = row.getFieldElement('subdate')
		cell.click()
		sendKeys('XXX')
		sendKeys(Keys.ESCAPE)

		assert row.getValue('subdate') == ''

		// NUCLOS-7059 Browser freeze occurred when trying to edit this cell again:
		cell.click()

		sendKeys(Keys.ESCAPE)

		eo.cancel()
	}

	@Test
	void _15_testOptionGroup() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !eo.dirty

		WebElement we1 = $('#Test-1')
		WebElement we3 = $('#Test-3')
		WebElement we59 = $('#Test-59')

		assert we1 != null
		assert we3 != null
		assert we59 != null

		assert !we1.isSelected()
		assert we3.isSelected()
		assert !we59.isSelected()

		we1.click()

		Object test = eo.getAttribute('radio')
		assert test == 1 || test == '1' // Now value für OptionGroup "RadioTest" is 1

		assert eo.dirty

		assert we1.isSelected()
		assert !we3.isSelected()

		eo.setAttribute('radio', 20)

		assert !we1.isSelected()
		assert !we59.isSelected()

		eo.setAttribute('radio', 59)

		assert we59.isSelected()

		test = eo.getAttribute('radio')
		assert test == 59 || test == '59' // Now value für OptionGroup "RadioTest" is 59

		eo.cancel()

		assert !eo.dirty

		test = eo.getAttribute('radio')
		assert test == 3 || test == '3' // Now value für OptionGroup "RadioTest" is 3 again

		assert !we1.isSelected()
		assert we3.isSelected()
		assert !we59.isSelected()

	}

	@Test
	void _20_nonInsertableDropdown() {
		logout()
		login('nuclos')

		EntityObjectComponent eo = EntityObjectComponent.open(SystemEntities.PARAMETER)
		eo.addNew()
		eo.setAttribute('name', 'THUMBNAIL_SIZE')
		eo.setAttribute('description', 'test')
		eo.save()

		assert eo.getAttribute('name') == 'THUMBNAIL_SIZE'

		ListOfValues lov = eo.getLOV('name')
		lov.open()

		// Entering some text should not set the record dirty for non-insertable inputs
		sendKeysAndWaitForAngular('asdf')
		assert !lov.choices
		// TODO: This behaves a bit differently since NUCLOS-6153. Evaluate if the current behavior is ok
//		assert !eo.dirty

		// Only after an existing dropdown entry is selected should the record be dirty
		lov.inputElement.clear()
		sendKeysAndWaitForAngular('CLIENT_READ_TIMEOUT')
		sendKeysAndWaitForAngular(Keys.TAB)
		assert eo.dirty
		assert eo.getAttribute('name') == 'CLIENT_READ_TIMEOUT'

		eo.save()
		assert eo.getAttribute('name') == 'CLIENT_READ_TIMEOUT'
	}

}
