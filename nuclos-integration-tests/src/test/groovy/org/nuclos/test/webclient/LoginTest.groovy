package org.nuclos.test.webclient

import org.json.JSONObject
import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.SystemEntities
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.webclient.pageobjects.*
import org.nuclos.test.webclient.pageobjects.account.PasswordChangeComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LoginTest extends AbstractWebclientTest {
	String sessionId

	@AfterClass
	static void cleanup() {
		nuclosSession.deleteUserByEmail('test@nuclos.de')
	}

	void assertLoggedOut() {
		assert !$('#logout')
		assert currentUrl.contains('login')
	}

	@Test()
	void _05_normalLogin() {
		logout()
		login('nuclos', '')

		Log.info 'Login test...'

		assert $('#logout')

		getUrlHash('/businesstests')
		assert currentUrl.endsWith('/businesstests')

		AbstractWebclientTest.refresh()
		assert currentUrl.endsWith('/businesstests')

		assert $('#logout')

		logout()
		assertLoggedOut()

		openStartpage()
		assertLoggedOut()
		assert !AuthenticationComponent.errorMessage

		// TODO: Restart the browser and assert that we are still logged out (i.e. no autologin happened).
		// Unfortunately if we restart the Selenium session, all cookies are deleted, so we
		// cannot relyably test this.
	}

	@Test
	void _10_autoLogin() {
		assertLoggedOut()

		login('nuclos', true)
		assert $('#logout')

		logout()
		assertLoggedOut()

		getHar {
			openStartpage()
		}.with {
			// There should be no HTTP error codes, except 401 because we are logged out
			Set<Integer> responseCodes = it.log.entries*.response*.status*.toInteger().unique().toSet()
			assert [200, 304, 401].containsAll(responseCodes)
		}
		assertLoggedOut()

		// TODO: Login with autologin checked, restart the browser and assert that we are automatically
		// logged in again (i.e. autologin happened).
		// Unfortunately if we restart the Selenium session, all cookies are deleted, so we cannot test this.
	}

	@Test
	void _15_testRedirect() {
		logout()
		assertLoggedOut()
		String newUrl = context.nuclosWebclientBaseURL.replace('index.html', '///////index.html')
		assert newUrl.contains('/////')
		assert !newUrl.startsWith(context.nuclosWebclientBaseURL)

		getUrl(newUrl)

		assert !driver.currentUrl.contains('///'), 'Redirect because of multiple slashes did not work'

	}

	/**
	 * Tests if login is still possible after a failed login attempt.
	 */
	@Test
	void _20_testFailedLogin() {
		assertLoggedOut()

		loginUnsafe('123', '')

		assertLoggedOut()

		String errorMessage = AuthenticationComponent.errorMessage
		assert errorMessage.contains('Wrong username/password') || errorMessage.contains('Falscher Benutzer/Passwort')

		login('nuclos', '')

	}

	/**
	 * Tests if we are automatically redirected to the Login page, if we have no valid session.
	 */
	@Test
	void _25_testUnauthorizedRedirect() {
		logout()
		assertLoggedOut()

		getUrlHash('/businesstests')
		assertLoggedOut()
		assert currentUrl.endsWith('/login')

		login('nuclos', '')

		assert currentUrl.endsWith('/businesstests')
	}

	/**
	 * Tests if we are automatically redirected to the last remembered location.
	 */
	@Test
	void _30_testRedirectToLastLocation() {
		getUrlHash('/businesstests/advanced')

		logout()
		login('nuclos', '')

		assert currentUrl.endsWith('/businesstests/advanced')
	}

	/**
	 * Tests if locale changes are applied and remembered.
	 */
	@Test
	void _35_testLocaleChange() {
		logout()

		LocaleChooser.locale = Locale.ENGLISH
		assert LocaleChooser.locale == 'en'

		assert AuthenticationComponent.usernameLabel == 'Username'
		assert AuthenticationComponent.passwordLabel == 'Password'

		LocaleChooser.locale = Locale.GERMANY
		assert LocaleChooser.locale == 'de'

		assert AuthenticationComponent.usernameLabel == 'Benutzer'
		assert AuthenticationComponent.passwordLabel == 'Passwort'

		LocaleChooser.locale = Locale.ENGLISH
		assert LocaleChooser.locale == 'en'

		AbstractWebclientTest.context.locale = Locale.ENGLISH
		login('nuclos')

		readSessionId()
		assertServerLocale(Locale.ENGLISH)

		getUrlHash('/dashboard')

		// Check if a menu item and a text in the dashboard are in english now
		MenuComponent.toggleUserMenu()

		assert MenuComponent.preferenceLinkText.contains('Manage preferences')

		refresh()

		assert LocaleChooser.locale == 'en'
		assertServerLocale(Locale.ENGLISH)

		// Check if a menu item and a text in the dashboard are in english now
		MenuComponent.toggleUserMenu()

		assert MenuComponent.preferenceLinkText.contains('Manage preferences')

		LocaleChooser.locale = Locale.GERMANY
		assertServerLocale(Locale.GERMANY)
		// TODO: The menu component is not updated yet after the locale is changed
		//assert MenuComponent.preferenceLinkText.contains('Manage preferences')
	}

	@Test
	void _40_testOpenPasswordChange() {
		logout()
		login('test', 'test')

		MenuComponent.toggleUserMenu()
		MenuComponent.clickChangePasswordLink()

		assert currentUrl.contains('/account/changePassword')

		assert !PasswordChangeComponent.errorMessage
		assert !PasswordChangeComponent.successMessage

	}

	@Test
	void _42_testChangePassword() {
		testEmptyOldPassword:
		{
			PasswordChangeComponent.submit()
			assert PasswordChangeComponent.errorMessage
		}

		PasswordChangeComponent.oldPassword = 'test'

		testEmptyNewPassword:
		{
			PasswordChangeComponent.submit()
			assert PasswordChangeComponent.errorMessage
		}

		PasswordChangeComponent.newPassword = 'test2'

		testEmptyConfirmNewPassword:
		{
			PasswordChangeComponent.submit()
			assert PasswordChangeComponent.errorMessage
		}

		PasswordChangeComponent.confirmNewPassword = 'test2'

		testSuccessfulChange:
		{
			PasswordChangeComponent.submit()
			assert PasswordChangeComponent.successMessage
		}
	}

	@Test
	void _45_testLoginWithChangedPassword() {
		logout()
		login('test', 'test2')
	}

	@Test
	void _50_testChangePasswordBack() {
		MenuComponent.toggleUserMenu()
		MenuComponent.clickChangePasswordLink()
		assert !PasswordChangeComponent.errorMessage
		assert !PasswordChangeComponent.successMessage

		PasswordChangeComponent.oldPassword = 'test2'
		PasswordChangeComponent.newPassword = 'test'
		PasswordChangeComponent.confirmNewPassword = 'test'

		PasswordChangeComponent.submit()
		assert PasswordChangeComponent.successMessage
	}

	@Test
	void _55_testRequestsWhenLoggedIn() {
		getHar {
			openStartpage()
		}.with {
			// There should be no HTTP error codes
			Set<Integer> responseCodes = it.log.entries*.response*.status*.toInteger().unique().toSet()
			assert [200, 304].containsAll(responseCodes)
		}
	}

	@Test
	void _60_loginWithExpiredCredentials() {
		// Set the "requirepasswordchange" flag of the User test, this should cause a logout
		EntityObject<String> testUser = nuclosSession.getEntityObjects(SystemEntities.USER).find {
			it.getAttribute('name') == 'test'
		}
		testUser.setAttribute('requirepasswordchange', true)
		testUser.save()

		// Should cause a redirect to the login page, since the session was invalidated
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		assertLoggedOut()

		// Should redirect to the password change page
		loginUnsafe('test', 'test')
		assert !$('#logout')
	}

	@Test
	void _62_setNewPassword() {
		PasswordChangeComponent.newPassword = 'test3'
		PasswordChangeComponent.confirmNewPassword = 'test3'
		PasswordChangeComponent.submit()

		assert PasswordChangeComponent.successMessage

		PasswordChangeComponent.clickLoginLink()
	}

	@Test
	void _64_loginWithNewPassword() {
		assert login('test', 'test3')
	}

	private readSessionId() {
		SessionInfo.open()
		sessionId = SessionInfo.getSessionId()
	}

	private assertServerLocale(Locale locale) {
		RESTClient client = new RESTClient('test', 'test')
		client.sessionId = sessionId

		JSONObject session = client.getSessionData()
		assert session.get('locale') == locale.getLanguage()
	}
}
