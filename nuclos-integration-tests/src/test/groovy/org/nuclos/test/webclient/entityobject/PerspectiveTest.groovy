package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.perspective.Perspective
import org.nuclos.test.webclient.pageobjects.perspective.PerspectiveComponent
import org.nuclos.test.webclient.pageobjects.perspective.PerspectiveModal
import org.nuclos.test.webclient.pageobjects.preference.PreferenceType
import org.nuclos.test.webclient.pageobjects.preference.Preferences
import org.nuclos.test.webclient.pageobjects.search.Searchbar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PerspectiveTest extends AbstractWebclientTest {
	String orderFQN = TestEntities.EXAMPLE_REST_ORDER.fqn
	String orderPositionFQN = TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn
	String subformOrderPositionFQN = orderPositionFQN + '_order'

	List<LinkedHashMap<String,Serializable>> orders = [
			[
					boMetaId  : orderFQN,
					attributes: [
							orderNumber: 100
					]
			],
			[
					boMetaId  : orderFQN,
					attributes: [
							orderNumber  : 101,
							nuclosProcess: [
									id: 'example_rest_Order_Priorityorder'
							],
							deliveryDate : '2016-11-01'
					]
			],
			[
					boMetaId  : orderFQN,
					attributes: [
							'orderNumber': 102,
							nuclosProcess: [
									id: 'example_rest_Order_Priorityorder'
							],
							deliveryDate : '2016-11-01'
					]
			],
	]

	String searchtemplateName1 = 'orderNumber <= 100'
	String searchtemplateName2 = 'orderNumber > 100'

	String perspectiveName1 = 'Normal, <= 100'
	String perspectiveName2 = 'Prio, > 100'

	String sideviewMenuName1 = 'Order number'
	String sideviewMenuName2 = 'Order number, Customer'

	String subformTableName1 = 'Subform table 1'
	String subformTableName2 = 'Subform table 2'

	@Test
	void _01_setup() {
		RESTHelper.createUser('test2', 'test2', ['Example user', 'Example controlling'], nuclosSession)

		RESTClient client = new RESTClient('test', 'test').login()
		orders.each {
			RESTHelper.createBo(it, client)
		}
	}

	/**
	 * Creates search templates as user 'test2'.
	 */
	@Test
	void _02_createSearchTemplates() {
		logout()
		login('test2', 'test2')
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Searchbar.clearSearchfilter()

		assert Sidebar.listEntryCount == 3

		Searchbar.create()
		Searchbar.selectAttribute('Order number')
		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'orderNumber',
						operator: '<=',
						value: '100'
				)
		)
		Searchbar.editName(searchtemplateName1)

		assert eo.listEntryCount == 1

		Searchbar.create()
		Searchbar.selectAttribute('Order number')
		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'orderNumber',
						operator: '>',
						value: '100'
				)
		)
		Searchbar.editName(searchtemplateName2)
		Searchbar.closeSearchEditor()

		assert eo.listEntryCount == 2
	}

	/**
	 * Creates sideview menu configs as user 'test2'.
	 */
	@Test
	void _03_createSideviewMenus() {
		SideviewConfiguration.open()

		orderNumber:
		{
			SideviewConfiguration.newSideviewConfiguration()

			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber')

			SideviewConfiguration.saveSideviewConfiguration()

			assert SideviewConfiguration.selectedConfigurationName == sideviewMenuName1
		}

		customer:
		{
			SideviewConfiguration.newSideviewConfiguration()

			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_orderNumber')
			SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer')

			SideviewConfiguration.saveSideviewConfiguration()

			assert SideviewConfiguration.selectedConfigurationName == sideviewMenuName2
		}

		SideviewConfiguration.close()
	}

	/**
	 * Creates sideview menu configs as user 'test2'.
	 */
	@Test
	void _04_createSubformTablePreferences() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subformOrderPositionFQN)
		subform.openViewConfiguration()

		SideviewConfiguration.newSideviewConfiguration(subformTableName1)
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_article')
		SideviewConfiguration.saveSideviewConfiguration()

		SideviewConfiguration.newSideviewConfiguration(subformTableName2)
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_article')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_price')
		SideviewConfiguration.selectColumn(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_quantity')
		SideviewConfiguration.saveSideviewConfiguration()

		SideviewConfiguration.close()

		// reset layout assignment
		Preferences.open()
		Preferences.filter('name', subformTableName1)
		def pref = Preferences.preferenceItems.get(0)
		pref.select()
		pref.resetLayoutAssignment()

		Preferences.filter('name', subformTableName2)
		pref = Preferences.preferenceItems.get(0)
		pref.select()
		pref.resetLayoutAssignment()

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}

	/**
	 * Creates some perspectives as user 'test2'.
	 */
	@Test
	void _07_createPerspectives() {
		perspective1:
		{
			PerspectiveModal modal = PerspectiveComponent.newPerspective()

			assert modal.layoutForNew

			modal.name = perspectiveName1
			// TODO: Test if Layout is changed on the fly
			modal.selectLayout('Example order')
			// TODO: Test if search template is changed on the fly
			modal.selectSearchtemplate(searchtemplateName1)
			modal.selectSideviewMenu(sideviewMenuName1)
			modal.selectSubformTablePreference(orderPositionFQN, subformTableName1)
			modal.save()
		}

		perspective2:
		{
			PerspectiveModal modal = PerspectiveComponent.newPerspective()
			modal.name = perspectiveName2
			modal.selectLayout('Example priority order')
			modal.layoutForNew = false
			modal.selectSearchtemplate(searchtemplateName2)
			modal.selectSideviewMenu(sideviewMenuName2)
			modal.selectSubformTablePreference(orderPositionFQN, subformTableName2)
			modal.save()
		}

		def perspectives = PerspectiveComponent.perspectives

		assert perspectives.size() == 2
		assert perspectives[0].id
		assert perspectives[0].name == perspectiveName1

		assert perspectives[1].id
		assert perspectives[1].name == perspectiveName2
	}

	/**
	 * Tries to edit and save an existing perspective.
	 */
	@Test
	void _08_editPerspective() {
		List<Perspective> perspectives = PerspectiveComponent.perspectives
		PerspectiveModal edit = perspectives[0].edit()

		assert edit.name == perspectiveName1
		assert edit.searchtemplate == searchtemplateName1
		assert edit.layout == 'Example order'
		assert edit.layoutForNew

		// Make some changes
		edit.name = edit.name + ' (edited)'
		edit.layoutForNew = false
		// TODO: Also edit the selected layout etc.
		edit.save()

		perspectives = PerspectiveComponent.perspectives
		assert perspectives.size() == 2
		assert perspectives[0].name == perspectiveName1 + ' (edited)'

		// Undo changes
		edit = perspectives[0].edit()
		assert edit.name == perspectiveName1 + ' (edited)'
		assert edit.searchtemplate == searchtemplateName1
		assert edit.layout == 'Example order'
		assert !edit.layoutForNew

		edit.name = perspectiveName1
		edit.layoutForNew = true
		edit.save()
	}

	/**
	 * Shares the previously created perspectives with the 'Example user' group.
	 */
	@Test
	void _09_sharePerspectives() {
		Preferences.open()

		Preferences.shareItem(PreferenceType.SEARCHTEMPLATE, searchtemplateName1, 'Example user')
		Preferences.shareItem(PreferenceType.SEARCHTEMPLATE, searchtemplateName2, 'Example user')

		Preferences.shareItem(PreferenceType.PERSPECTIVE, perspectiveName1, 'Example user')
		Preferences.shareItem(PreferenceType.PERSPECTIVE, perspectiveName2, 'Example user')

		Preferences.shareItem(PreferenceType.TABLE, sideviewMenuName1, 'Example user')
		Preferences.shareItem(PreferenceType.TABLE, sideviewMenuName2, 'Example user')

		Preferences.shareItem(PreferenceType.SUBFORMTABLE, subformTableName1, 'Example user')
		Preferences.shareItem(PreferenceType.SUBFORMTABLE, subformTableName2, 'Example user')
	}

	/**
	 * Logs in as other user 'test' with whom the perspectives are shared.
	 */
	@Test
	void _10_testPerspectives() {
		logout()
		login('test', 'test')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.resizeSidebarComponent(500)
		Sidebar.refresh()

		List<Perspective> perspectives = PerspectiveComponent.perspectives

		assert perspectives.size() == 2

		// Select the first perspective
		Perspective perspective1 = PerspectiveComponent.getPerspective(perspectiveName1)

		// By default no perspective is selected
		assert perspectives.findAll { it.selected }.empty

		perspective1.toggle()
		assertPerspective1(eo)

		Perspective perspective2 = PerspectiveComponent.getPerspective(perspectiveName2)
		assert !perspective2.selected
		perspective2.toggle()

		assertPerspective2(eo)
	}

	@Test
	void _11_testPerspectiveSelection() {
		refresh()

		List<Perspective> perspectives = PerspectiveComponent.perspectives
		Perspective perspective1 = perspectives.find {
			it.name == perspectiveName1
		}
		Perspective perspective2 = perspectives.find {
			it.name == perspectiveName2
		}

		assert !perspective1.selected
		assert perspective2.selected

		assertPerspective2(EntityObjectComponent.forDetail())
	}

	/**
	 * Perspective 2 should still be selected and has the flag "layoutForNew" deactivated.
	 * Test if layout2 is really not used for a new entry.
	 */
	@Test
	void _12_testPerspectiveNotForNew() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()

		assertLayout1(eo)

		eo.setAttribute('orderNumber', '666')
		eo.save()

		// After saving the perspective layout should be applied
		assertPerspective2(eo)
	}

	@Test
	void _13_testPerspectiveForNew() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Perspective perspective1 = PerspectiveComponent.perspectives.find {
			it.name == perspectiveName1
		}

		perspective1.toggle()

		eo.addNew()

		// TODO: In order to really test this, a third layout should be configured for new entries
		assertLayout1(eo)

		eo.setAttribute('orderNumber', '667')
		eo.setAttribute('nuclosProcess', 'Priority order')

		assertPerspective1(eo)

		eo.save()

		assertPerspective1(eo)
	}

	/**
	 * Tests if the sideview menu preference of a perspective is still selected
	 * and applied after the sideview menu preference is customized.
	 */
	@Test
	void _14_testSideviewMenuCustomization() {
		logout()
		login('test2', 'test2')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Sidebar.resizeSidebarComponent(500)

		Perspective perspective1 = PerspectiveComponent.perspectives.find {
			it.name == perspectiveName1
		}
		assert !perspective1.selected

		PerspectiveModal modal = perspective1.edit()

		assert modal.name == perspectiveName1
		assert modal.sideviewMenu == sideviewMenuName1

		modal.cancel()

		perspective1.toggle()

		refresh()

		assertPerspective1(eo)
	}

	/**
	 * Tests if the searchfilter preference of a perspective is still selected
	 * after the searchfilter preference is customized.
	 */
	@Test
	void _15_testSearchTemplateCustomization() {
		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'orderNumber',
						operator: '>',
						value: '100'
				)
		)
		Searchbar.closeSearchEditor()

		Perspective perspective1 = PerspectiveComponent.perspectives.find {
			it.name == perspectiveName1
		}

		PerspectiveModal modal = perspective1.edit()

		assert modal.name == perspectiveName1
		assert modal.searchtemplate == searchtemplateName1

		modal.cancel()

		refresh()

		assertPerspective1(EntityObjectComponent.forDetail(), true)
	}

	@Test
	@Ignore('TODO fix sidebar')
	void _16_deleteSharedAndSelectedPerspective() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Perspective perspective = PerspectiveComponent.perspectives.find {
			it.name == perspectiveName2
		}
		perspective.toggle()

		Sidebar.selectEntryByText('666')
		assert !eo.getAttribute('nuclosProcess')
		assertLayout2(eo)

		perspective
		perspective.delete()
		perspective.clickButtonOk()

		// Deleted perspective should have been removed
		assert PerspectiveComponent.perspectives.size() == 1

		// The default layout of the EO should be applied again
		assertLayout1(eo)
	}

	/**
	 * Asserts that perspective1 is selected and applied.
	 */
	private void assertPerspective1(EntityObjectComponent eo) {
		assertPerspective1(eo, false);
	}

	private void assertPerspective1(EntityObjectComponent eo, boolean afterCustomization) {
		/**
		 * Assert the perspectives are correctly (de-)selected.
		 */
		assertPerspectiveSelection:
		{
			List<Perspective> perspectives = PerspectiveComponent.perspectives
			Perspective perspective1 = perspectives.find {
				it.name == perspectiveName1
			}
			Perspective perspective2 = perspectives.find {
				it.name == perspectiveName2
			}

			assert perspective1.selected
			assert !perspective2.selected
		}

		/**
		 * Assert the list entries are correct for the perspective search template.
		 */
		assertListEntries:
		{
			List<String> entries = Sidebar.listEntries

			assert entries.size() >= 1
			if (afterCustomization) {
				assert entries.contains('101')
			} else {
				assert entries.contains('100')
			}
		}

		/**
		 * Assert the correct layout is used.
		 */
		assertLayout:
		{
			assertLayout1(eo)
		}

		assertSubformTablePreferences:
		{
			Subform subform = eo.getSubform(subformOrderPositionFQN)
			assert subform.columnHeaders == ['Article']
		}

		/**
		 * Asserts the correct sideview menu config is used.
		 */
		assertSideviewMenu:
		{
			assert Sidebar.columnHeaders == ['Order number']
		}
	}

	/**
	 * Asserts that perspective2 is selected and applied.
	 */
	private void assertPerspective2(EntityObjectComponent eo) {
		/**
		 * Assert the perspectives are correctly (de-)selected.
		 */
		assertPerspectiveSelection:
		{
			List<Perspective> perspectives = PerspectiveComponent.perspectives
			Perspective perspective1 = perspectives.find {
				it.name == perspectiveName1
			}
			Perspective perspective2 = perspectives.find {
				it.name == perspectiveName2
			}

			assert !perspective1.selected
			assert perspective2.selected
		}

		/**
		 * Assert the list entries are correct for the perspective search template.
		 */
		assertListEntries:
		{
			List<String> entries = Sidebar.listEntries

			assert entries.size() >= 2
			assert entries.contains('101')
			assert entries.contains('102')
		}

		/**
		 * Assert the correct layout is used.
		 */
		assertLayout2(eo)

		assertSubformTablePreferences:
		{
			Subform subform = eo.getSubform(subformOrderPositionFQN)
			assert subform.columnHeaders == ['Article', 'Price', 'Quantity']
		}

		/**
		 * Asserts the correct sideview menu config is used.
		 */
		assertSideviewMenu:
		{
			assert Sidebar.columnHeaders == ['Order number', 'Customer']
		}
	}

	/**
	 * Asserts layout1 is currently applied.
	 */
	private void assertLayout1(EntityObjectComponent eo) {
		// deliveryDate is only visible in priority order layout.
		assert !eo.isFieldVisible('deliveryDate')
	}

	/**
	 * Asserts layout2 is currently applied.
	 */
	private void assertLayout2(EntityObjectComponent eo) {
		// deliveryDate is only visible in priority order layout.
		assert eo.isFieldVisible('deliveryDate')
	}
}
