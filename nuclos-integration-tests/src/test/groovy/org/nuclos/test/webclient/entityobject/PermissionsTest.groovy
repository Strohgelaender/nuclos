package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PermissionsTest extends AbstractWebclientTest {
	String auftragsPositionSub = TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION.fqn + "_auftrag"
	String attrPositionName = TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION.fqn + "_name"
	String auftragNo1stTransition = TestEntities.EXAMPLE_REST_AUFTRAG.fqn
	String rechnungReadOnly = TestEntities.EXAMPLE_REST_RECHNUNG.fqn
	String goDocument = "org_nuclos_businessentity_nuclosgeneralsearchdocument_genericObject"

	String tabAuftragsPosition = "Position"
	String tabGoDocument = "Dokumente"
	String tabNotiz = "Notiz"

	String memoText = "Enter some text\nwith mutliple\nlines"

	@Test
	void _01_setup() {

		Map auftrag1 = [name: 'Auftrag 1', bemerkung: 'Bemerkung', nogroup: 'NoGroup1'] as Map<String, Serializable>
		RESTHelper.createBo(auftragNo1stTransition, auftrag1, nuclosSession)

		Map auftrag2 = [name: 'Auftrag 2', nogroup: 'NoGroup2'] as Map<String, Serializable>
		RESTHelper.createBo(auftragNo1stTransition, auftrag2, nuclosSession)

		Map rechnung = [name: 'Rechnung 1'] as Map<String, Serializable>
		RESTHelper.createBo(rechnungReadOnly, rechnung, nuclosSession)

	}

	@Test
	void _03_noEntityPermissionAtAll() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION)
		assertAccessDenied()
	}

	@Test
	void _04_checkFirstState() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_AUFTRAG)

		waitForAngularRequestsToFinish()

		waitFor(20, { //Wait for the angularjs to finish opening this eo
			$('label') != null
		});

		checkFields1st4th5thState(true, 10)

		checkTabsFirstState(true, "Pos1")
	}

	@Test
	void _05_changeState() {
		StateComponent.changeStateByNumeral(20) // Bearbeitung
	}

	@Test
	void _06_checkSecondState() {

		checkFieldsSecondState("Mein Auftrag")
		checkTabsSecondState()
	}


	@Test
	void _07_changeState2() {
		StateComponent.changeStateByNumeral(50) // Fertig
	}

	@Test
	void _08_checkThirdState() {

		checkFieldsThirdState()
		checkTabsThirdState("Pos1")
	}

	@Test
	void _09_checkLayoutChange() {
		Sidebar.refresh()
		Sidebar.selectEntry(1)

		checkFields1st4th5thState(true, 10)
		checkTabsFirstState(true, "Pos2")

		// FIXME selectEntry doesn't work reliable here - click in browser works
		// Sidebar.selectEntry(0) //Click on the BO with the third state and the standard layout
		Sidebar.selectEntryByText('Mein Auftrag')

		checkFieldsThirdState()
		checkTabsThirdState("Pos1")

		changeFromFertigToAenderung() //to 4th state

		checkFields1st4th5thState(false, 60)
		checkTabs4thState("Pos1", true)

		Sidebar.selectEntry(1) //Click on the BO with the first state and the standard layout

		checkFields1st4th5thState(false, 10)
		checkTabsFirstState(false, "Pos2")


		StateComponent.changeStateByNumeral(20)
		StateComponent.changeStateByNumeral(50)

		changeFromFertigToAenderung() //to 4th state

		checkTabs4thState("Pos2", false)

		// FIXME selectEntry doesn't work reliable here - click in browser works
		// Sidebar.selectEntry(0) //Click on the BO with the third state and the standard layout
		Sidebar.selectEntryByText('Mein Auftrag')

		checkTabs4thState("Pos1", false)

	}

	@Test
	void _10_testStateChangeFailure() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		String error = getLocale().language == 'de' ? 'Fehler' : 'Error'

		StateComponent.changeStateByNumeral(90) //to 5th state (Abbruch)
		assertMessageModalAndConfirm(error, 'Bemerkung')
		//There must be an error, because Bemerkung must not be empty in the next state

		assert !eo.isDirty()
		assert eo.getCancelButton() == null

		eo.checkField('anzahl', true, true).sendKeys('8')

		assert eo.isDirty()
		assert eo.getCancelButton() != null

		StateComponent.changeStateByNumeral(90) //to 5th state (Abbruch)
		assertMessageModalAndConfirm(error, 'Bemerkung')
		//There must be an error, because Bemerkung must not be empty in the next state

		assert eo.isDirty()
		assert eo.getCancelButton() != null

		eo.checkField("bemerkung", true, true).sendKeys('Ungültig')

		StateComponent.changeStateByNumeral(90) //to 5th state (Abbruch) //This time should it should work well

		assert getMessageModal() == null
		assert getErrorMessage() == null

		eo.checkField('anzahl', true, true).getAttribute('value') == '8'

		checkTabs5thState()

		Sidebar.selectEntry(1) //Click on the BO with the 4th state and the standard layout

		checkTabs4thState("Pos2", true)

		Sidebar.selectEntry(0) //Click on the BO with the 5th state and the standard layout

		checkFields1st4th5thState(false, 90)
		checkTabs5thState()

	}

	@Test
	void _11_checkPermissionsForSideview() {

		Sidebar.selectEntry(1) //Click on the BO with the 4th state and the standard layout

		waitForAngularRequestsToFinish()

		Sidebar.addColumn(TestEntities.EXAMPLE_REST_AUFTRAG.fqn, 'bemerkung')

		screenshot('afterAddingBemerkungColumn')

		assert Sidebar.selectedColumns().size() == 3 // Status, name, bemerkung

		assert Sidebar.getValue(0, 1) == "Mein Auftrag"
		assert Sidebar.getValue(0, 2) == "" // This one should not be visible by statemodel
		assert Sidebar.getValue(1, 1) == "Auftrag 1"
		assert Sidebar.getValue(1, 2) == "Bemerkung"

		Sidebar.resizeSidebarComponent(1000)

		// FIXME selectEntry doesn't work reliable here - click in browser works
		// Sidebar.selectEntry(0) //Click on the BO with the third state and the standard layout
		Sidebar.selectEntryByText('Mein Auftrag')

		screenshot('afterClickOnFirstRecord')

		assert Sidebar.getValue(0, 1) == "Mein Auftrag"
		assert Sidebar.getValue(0, 2) == "" // This one should not be visible by statemodel
		assert Sidebar.getValue(1, 1) == "Auftrag 1"
		assert Sidebar.getValue(1, 2) == "Bemerkung"
	}

	@Test
	void _12_checkRegel1() {

		Sidebar.selectEntry(1) //Click on the BO with the 4th state and the standard layout

		waitForAngularRequestsToFinish()

		// FIXME: Regel doesn't work
		if (false) {

			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.clickButton('Regel 1') //Start Rule that adds 5 Words to the subform

			screenshot('afterClickRegelButton1')

			// FIXME: NUCLOS-5596 Missing: Number of Lines in Subforms
			if (false) {
				assert eo.getTab(tabAuftragsPosition).getText() == "Position (6)"
			}
		}

	}

	@Test
	void _20_readOnlyEntities() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_RECHNUNG)

		screenshot("forbidden4")

		assert getErrorMessage() == null

		assert eo.getNewButton() == null
		assert eo.getDeleteButton() == null

		//Here we cannot save
		eo.checkField('name', true, false)

		//FIXME NUCLOS-4791 Next state should not be possible even if state-model says yes.

	}

	void changeFromFertigToAenderung() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !eo.hasNextStateButton() //Automatic State Transition may not appear

		eo.checkButton('Änderung', true, false) //State Change Button exists, but is disabled (automatic)

		WebElement we = eo.checkField('passwort', true, true)

		we.sendKeys("abc")

		assert we.getText() != "abc" //Password Field does not give away its content

		assert eo.isDirty()

		eo.save()

		screenshot("AfterChangeFinalRuleWithStateChange")

		assert eo.hasNextStateButton() //A new state, new state transitions

	}

	void checkFields1st4th5thState(boolean enterValues, int stateNumeral) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot("permissionsFirstState")

		assert getErrorMessage() == null

		//New Button must not be displayed, because this first state tranisiton it not allowed
		assert eo.getNewButton() == null

		//State-model prohibits changes

		Set<String> labels = eo.getLabels()
		eo.checkField('name', true, false)

		assert labels.contains("Name")

		if (stateNumeral == 60) { // Only Bemerkung is different in 4th State (60 Änderung). Writable
			eo.checkField('bemerkung', true, true)
			assert labels.contains("Bemerkung")
		}
		//FIXME NUCLOS-5595
		else {

			// FIXME NUCLOS-5595 Bemerkung must not exist, restriction == 'hidden'
			// The equality check to empty string is important, because there are no rights to read
			eo.checkField('bemerkung', true, false).getAttribute('value') == ''

			//eo.checkField('bemerkung', false, false)
			//assert !labels.contains("Bemerkung")
		}

		eo.checkField('rechnung', true, false)
		eo.checkField('kategorie', true, false)
		eo.checkField('nogroup', true, false).getAttribute('value') == ''

		eo.checkButton('Generation', true, false) //This generation is not allowed for this state
		eo.checkButton('Status Fertig', true, false)  //This state is not within the subsequent states
		eo.checkButton('Regel 1', true, true)
		eo.checkButton('Regel 2', true, false) //Disabled in layout

		WebElement weAnzahl = eo.checkField('anzahl', true, true)
		WebElement wePreis = eo.checkField('preis', true, true)

		if (enterValues) {
			weAnzahl.sendKeys("7")
			if (getLocale().language == 'de') {
				wePreis.sendKeys("5,9")
			} else {
				wePreis.sendKeys("5.9")
			}
			//Regel1, disabled during edit
			eo.checkButton('Regel 1', true, false)
		}

		eo.checkField('wichtig', true, true)
		eo.checkField('wann', true, false).click()

		assert !eo.datePickerIsOpen() //Disabled, so no datepicker must appear

		WebElement we = $('.nuc-legend')
		assert we != null
		assert we.getText() == 'Extra'

		WebElement weEmail = eo.checkField('email', true, true)
		WebElement weLink = eo.checkField('link', true, true)

		if (enterValues) {
			weEmail.sendKeys("test@nuclos.de")
			weLink.sendKeys("www.google.de")
		}

		eo.checkField('dokument', true, true)

	}

	void checkTabsFirstState(boolean newValues, String subValue) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.hasTab(tabAuftragsPosition)
		assert eo.hasTab(tabGoDocument)

		//FIXME: Notiz must be disabled and tab disappear
		if (false) {
			assert !eo.hasTab(tabNotiz)
		}

		Subform subform = eo.getSubform(auftragsPositionSub);

		assert subform.newVisible
		assert subform.cloneVisible

		//Groovy Rule in Layout forbids delete
		assert !subform.deleteVisible

		if (newValues) {
			subform.newRow()

			screenshot('after-new-row')

			Subform.Row row = subform.getRow(0)
			row.enterValue(attrPositionName, subValue)
		}

		eo.getTab(tabGoDocument).click()

		subform = eo.getSubform(goDocument) //Check if document subform is readonly

		assert !subform.newVisible
		assert !subform.cloneVisible
		assert !subform.deleteVisible

		if (newValues) {
			eo.save()
		}

		eo.getTab(tabAuftragsPosition).click()

		screenshot('after-click-back')

		subform = eo.getSubform(auftragsPositionSub);

		assert subform.rowCount == 1

		Subform.Row row = subform.getRow(0)
		assert row.getValue(attrPositionName) == subValue

		row.enterValue(attrPositionName, 'Pos99')

		screenshot('afterRowEnterValue')

		assert row.getValue(attrPositionName) == 'Pos99'

		assert eo.isDirty()
		assert eo.getCancelButton() != null;
		assert eo.getSaveButton() != null;

		eo.cancel()

		row = subform.getRow(0)
		assert row.getValue(attrPositionName) == subValue
	}

	void checkFieldsSecondState(String newName) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		//Now we can save

		WebElement weName = eo.checkField('name', true, true)
		eo.checkField('bemerkung', true, true)

		Set<String> labels = eo.getLabels();
		assert labels.contains("Name")
		assert labels.contains("Bemerkung")

		eo.checkButton('Generation', true, true)
		eo.checkButton('Status Fertig', true, true)
		eo.checkButton('Regel 1', true, true)
		eo.checkButton('Regel 2', true, false) //Disabled in layout

		eo.checkField('rechnung', true, true)
		eo.checkField('kategorie', true, true)

		eo.getLOV('rechnung').selectEntry('Rechnung 1')

		assert eo.checkField('anzahl', true, false).getAttribute('value') == '7'
		def preis = eo.checkField('preis', true, false).getAttribute('value')
		assert preis == '5.90' || preis == '5,90' || preis == '5,9'

		assert !eo.datePickerIsOpen()

		eo.checkField('wichtig', true, false)
		eo.checkField('wann', true, true).click()

		assert eo.datePickerIsOpen()

		assert eo.checkField('email', true, false).getAttribute('value') == "test@nuclos.de"

		// deactivated links should be clickable but not editable
		assert eo.checkField('link', true, true).text == "www.google.de"

		eo.checkField('dokument', true, false)

		weName.clear()
		waitForAngularRequestsToFinish()

		//FIXME: EO has not been set dirty with the clearing of the name!
		if (false) {
			assert eo.getSaveButton() != null
			assert eo.getCancelButton() != null

			//NEW FEATURE: Dirty EO does not remove Delete Button, but delete is not possibl here
			assert eo.getDeleteButton() == null
		}

		weName.sendKeys(newName)

		screenshot('afterChangeName')

		//Generation and Regel 1 disable during edit
		eo.checkButton('Generation', true, false)
		eo.checkButton('Regel 1', true, false)

		eo.checkButton('Status Fertig', true, true)
		eo.checkButton('Regel 2', true, false) //Disabled in layout

		eo.save()

		screenshot('afterSaveEntry')

		assert eo.checkField('name', true, true).getAttribute('value') == newName
	}

	void checkTabsSecondState() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		//FIXME Position Tab should not appear here.
		if (false) {
			assert !eo.hasTab(tabAuftragsPosition)
		} else {
			//Thus Dokument-Tab has to be clicked.
			eo.getTab(tabGoDocument).click()
			waitForAngularRequestsToFinish()
		}

		assert eo.hasTab(tabGoDocument)
		assert eo.hasTab(tabNotiz)

		Subform subform = eo.getSubform(goDocument);

		waitForAngularRequestsToFinish()

		assert subform.newVisible
		assert subform.cloneVisible
		assert subform.deleteVisible

		eo.getTab(tabNotiz).click()

		WebElement we = eo.checkField('notiz', true, true)

		waitFor(10, {
			we.isDisplayed()
		})

		we.sendKeys(memoText)

		eo.getTab(tabGoDocument).click()

		eo.save()

		eo.getTab(tabNotiz).click()

		waitForAngularRequestsToFinish()

		assert eo.checkField('notiz', true, true).getAttribute('value') == memoText

	}

	void checkFieldsThirdState() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		//Another Layout is used now for this state

		eo.checkField('name', true, false) //Layout Component not active
		eo.checkField('bemerkung', true, false) //Attribute Group read only in state model

		eo.checkField('rechnung', true, false) //Layout Component not active
		eo.checkField('kategorie', false, false) //Layout Component not visible

		Set<String> labels = eo.getLabels();
		assert !labels.contains("Rechnung") //Layout Label not visible
		assert labels.contains("Kategorie") //Layout Label visible (vice versa to the field)

		assert eo.getAttribute('rechnung') == 'Rechnung 1'

		eo.checkField('anzahl', true, true) //Groovy script: active="return true;"

		//FIXME: Trivial Groovy scripts does not work
		if (false) {
			eo.checkField('preis', true, false) //Groovy script: active="return false;"
		}

		eo.checkField('wichtig', false, false) //Invisible in this layout
		eo.checkField('wann', false, false) //Does not exist in this layout

		assert labels.contains("Anzahl")
		assert labels.contains("Preis")
		assert !labels.contains("Wichtig")
		assert !labels.contains("Wann")

		assert eo.checkField('passwort', true, true).getAttribute('type') == 'password'

		WebElement we = $('.nuc-legend')
		assert we == null

		eo.checkField('email', false, false) //The complete panel does not exist in this layout
		eo.checkField('link', false, false)
		eo.checkField('dokument', false, false)

		assert eo.getSaveButton() == null
		assert eo.getCancelButton() == null

		//NEW FEATURE: Dirty EO does not remove Delete Button, but delete is not possible here
		assert eo.getDeleteButton() == null

	}

	void checkTabsThirdState(String subValue) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.hasTab(tabAuftragsPosition)
		assert eo.hasTab(tabGoDocument)
		assert eo.hasTab(tabNotiz)

		Subform subform = eo.getSubform(auftragsPositionSub);

		assert !subform.newVisible //Subform is not active in layout
		assert !subform.cloneVisible
		assert !subform.deleteVisible

		assert subform.rowCount == 1
		Subform.Row row = subform.getRow(0)
		assert row.getValue(attrPositionName) == subValue

		try {
			row.enterValue(attrPositionName, 'Pos99')
		} catch (IndexOutOfBoundsException e) {
			//Do nothing here because this is expected
		}
		assert row.getValue(attrPositionName) == subValue //No change here, because subform should be readonly

		assert eo.getCancelButton() == null
		assert eo.getSaveButton() == null

		eo.getTab(tabGoDocument).click()

		subform = eo.getSubform(goDocument);

		assert !subform.newVisible //Groovy Script false
		assert !subform.cloneVisible //Groovy Script false
		assert subform.deleteVisible //Groovy Script true

		eo.getTab(tabNotiz).click()

		eo.checkField('notiz', true, false).getAttribute('value') == memoText

	}

	void checkTabs4thState(String subValue, boolean checkGoDocument) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.hasTab(tabAuftragsPosition)
		assert eo.hasTab(tabGoDocument)
		assert eo.hasTab(tabNotiz)

		Subform subform = eo.getSubform(auftragsPositionSub)

		if (!subform) {
			eo.getTab(tabAuftragsPosition).click()
			subform = eo.getSubform(auftragsPositionSub)
		}

		assert subform.rowCount == 1

		assert !subform.newVisible
		assert !subform.cloneVisible
		assert !subform.deleteVisible

		Subform.Row row = subform.getRow(0)
		try {
			row.enterValue(attrPositionName, 'Pos99')
		} catch (IndexOutOfBoundsException e) {
			//Do nothing here because this is expected
		}

		assert row.getValue(attrPositionName) == subValue //Subform must be readonly

		if (checkGoDocument) {
			eo.getTab(tabGoDocument).click()

			subform = eo.getSubform(goDocument) //Check if document subform is writeable

			waitFor(5, {
				subform.newVisible
			})

			assert subform.cloneVisible
			assert subform.deleteVisible

			eo.getTab(tabAuftragsPosition).click()
		}

	}

	void checkTabs5thState() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		//FIXME: NUCLOS-5560 Compete Tab "disappering" does not works yet

		if (false) {
			assert !eo.hasTab(tabAuftragsPosition)
			assert eo.hasTab(tabGoDocument)
			assert !eo.hasTab(tabNotiz)
		} else {
			eo.getTab(tabGoDocument).click()
		}

		Subform subform = eo.getSubform(goDocument)

		assert !subform.newVisible
		assert !subform.cloneVisible
		assert !subform.deleteVisible

	}

}
