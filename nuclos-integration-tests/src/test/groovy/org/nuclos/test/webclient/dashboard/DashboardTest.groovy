package org.nuclos.test.webclient.dashboard

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.dashboard.Dashboard

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DashboardTest extends AbstractWebclientTest {
	@Test
	void _00_setup() {
		LocaleChooser.locale = Locale.ENGLISH
	}

	@Test
	void _05_newDashboard() {
		assert !Dashboard.configMode

		Dashboard.addNew()

		assert Dashboard.configMode

		assert Dashboard.name == 'New dashboard'
		Dashboard.name = 'Dashboard 1'
		assert Dashboard.name == 'Dashboard 1'
		assert Dashboard.tabTitles == ['Dashboard 1']

		assert Dashboard.items.empty
		Dashboard.addTaskListItem('Order-And-Customer-Dyn-Tasks')
		assert Dashboard.items.size() == 1
	}

	@Test
	void _10_reloadDashboard() {
		refresh()
		assert Dashboard.tabTitles == ['Dashboard 1']
		assert Dashboard.items.size() == 1
	}

	@Test
	void _15_clickTasklist() {
		// TODO: Click on tasklist item
		// TODO: Check if it is opened
	}

	@Test
	void _20_shareDashboard() {
		// TODO: Share dashboard
		// TODO: Switch user and check
	}

	@Test
	void _25_customizeDashboard() {
		// TODO: Customize the shared dashboard
	}

	@Test
	void _30_addSecondDashboard() {
		// TODO: Add some new dashboard
	}

	@Test
	void _35_deleteDashboard() {
		// TODO: Delete the newly created dashboard
		// TODO: Check that the shared dashboard is not deletable
	}
}
