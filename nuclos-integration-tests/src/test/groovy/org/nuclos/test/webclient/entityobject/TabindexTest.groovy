package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Ignore // TODO implement tabindex in webclient 2

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class TabindexTest extends AbstractWebclientTest {

	@Test()
	void _2open() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_TABINDEX_TESTTABINDEX)
		screenshot('tabindex-opened')
	}

	@Test
	void _3createNewEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot('createnewentry-a')
		eo.addNew()
		screenshot('createnewentry-b')


		$('#attribute-a').sendKeys('input 1') // write text in first input field
		driver.switchTo().activeElement().sendKeys(Keys.TAB)    // tab to next input field

		driver.switchTo().activeElement().sendKeys('input 2') // write text in 2. input field
		driver.switchTo().activeElement().sendKeys(Keys.TAB)    // tab to next input field

		driver.switchTo().activeElement().sendKeys('input 3') // write text in 3. input field
		driver.switchTo().activeElement().sendKeys('\t')    // tab to next input field

		driver.switchTo().activeElement().sendKeys('input 4') // write text in 4. input field
		driver.switchTo().activeElement().sendKeys('\t')    // tab to next input field

		driver.switchTo().activeElement().sendKeys('input 5') // write text in 5. input field
		driver.switchTo().activeElement().sendKeys('\t')    // tab to next input field


		screenshot('createnewentry-c')

		assert $('#attribute-a').getAttribute('value') == 'input 1'
		assert $('#attribute-b').getAttribute('value') == 'input 5'
		assert $('#attribute-c').getAttribute('value') == 'input 2'
		assert $('#attribute-d').getAttribute('value') == 'input 4'
		assert $('#attribute-e').getAttribute('value') == 'input 3'
	}
}
