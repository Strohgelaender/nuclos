package org.nuclos.test.webclient.entityobject

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.GenerationComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.MessageModal
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class GenerationTest extends AbstractWebclientTest {
	static File nuclosDocx = new File(GenerationTest.getResource('nuclos.docx').toURI())
	static File nuclosPng = new File(GenerationTest.getResource('nuclos.png').toURI())

	static long eoId

	Map<String, ?> eoData = [
			name              : 'test',
			mandatorytext     : 'test',
			exceptionbeiinsert: false,
			document          : nuclosDocx,
			image             : nuclosPng,
	]
	Map<String, ?> eoData2 = [
			name              : 'test2',
			mandatorytext     : 'test2',
			exceptionbeiinsert: false
	]

	@Test
	void _00_createEO() {
		LocaleChooser.locale = Locale.ENGLISH
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData)
		eoId = eo.id
	}

	@Test()
	void _05_generateEO() {
		String oldWindow

		generateEo:
		{
			screenshot('before-calling-generator')
			countRequests {
				int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')
				assert generatorCount == 4

				GenerationComponent.openResult()

				assert driver.getWindowHandles().size() == 2

				oldWindow = driver.windowHandle
				String newWindow = switchToOtherWindow()

				assert oldWindow != newWindow
			}.with {
				// There should be no update, because the EO is not dirty
				assert it.getRequestCount(RequestType.EO_UPDATE) == 0

				assert it.getRequestCount(RequestType.EO_GENERATION) == 1

				// There is 1 request to load the generated EO (new browser tab)
				assert it.getRequestCount(RequestType.EO_READ) == 1
			}
			screenshot('after-calling-generator')

		}

		checkGeneratedObject:
		{
			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			assert eo.getAttribute('name') == 'test test test'
			assert eo.getFileComponent('document').text == nuclosDocx.name
			assert eo.getFileComponent('image').imageUrl.contains("/$eo.id/")
		}

		deleteFileInSourceEo:
		{
			switchToOtherWindow()

			EntityObjectComponent eo = EntityObjectComponent.forDetail()
			eo.getFileComponent('document').clearFile()

			// FIXME: Can not be cleared due to problems with the file component
//			eo.getFileComponent('image').clearFile()

			eo.save()
		}

		/**
		 * Files should still be available in the generated EO.
		 */
		checkFileInGeneratedEo:
		{
			switchToOtherWindow()

			EntityObjectComponent eo = EntityObjectComponent.forDetail()

			eo.refresh()
			assert !eo.dirty
			assert Sidebar.listEntryCount == 2

			assert eo.getFileComponent('document').text == nuclosDocx.name
			assert eo.getFileComponent('image').imageUrl.contains("/$eo.id/")

			// TODO: Download files, check content

			driver.close()
			driver.switchTo().window(oldWindow)
		}

		screenshot('generate-1-window-1')
	}

	@Test
	void _07_staleVersion() {
		EntityObject<Long> eo = nuclosSession.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoId)
		EntityObject<Long> eo2 = nuclosSession.getEntityObject(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoId)

		eo.setAttribute('name', eo.getAttribute('name').toString() + ' 2')
		eo.save()

		checkRestStatusCode:
		{
			eo2.setAttribute('name', '...')
			expectErrorStatus(Response.Status.CONFLICT) {
				eo2.save()
			}
		}

		checkConflictInWebclient:
		{
			EntityObjectComponent eoComponent = EntityObjectComponent.forDetail()

			eoComponent.clickButton('Objekt generieren')
			messageModal.confirm()

			MessageModal modal = messageModal
			assert modal.message.toLowerCase().contains('version')
			modal.confirm()

			refresh()
		}
	}

	@Test()
	void _10_generateEOWithException() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot('with-exception-before-update')
		eo.setAttribute('exceptionbeiinsert', true)
		eo.save()

		screenshot('with-exception-before-calling-generator-2')
		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')

		assert generatorCount == 4

		GenerationComponent.openResult()
		screenshot('with-exception-after-calling-generator-2')

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert eo.dirty
		assert eo.alertText == 'Test Exception'

		eo.clickButtonOk()

		eo.setAttribute('exceptionbeiinsert', false)
		eo.save()

		eo.refresh()

		assert Sidebar.listEntryCount == 3
		assert !eo.dirty

		screenshot('with-exception-generate-1-window-2-after-save')

		driver.close()
		driver.switchTo().window(oldWindow)

		screenshot('with-exception-generate-2-window-1')
	}

	@Test()
	void _15_generateEOViaButton() {
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData2)

		eo.checkButton('Intern generieren', true, true)
		eo.clickButton('Objekt generieren')
		messageModal.confirm()

		GenerationComponent.openResult()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert !eo.dirty

		driver.close()
		driver.switchTo().window(oldWindow)
	}

	@Test()
	void _17_generateEOViaButtonWithSave() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		generateEo:
		{
			eo.setAttribute('name', 'test 2')

			assert eo.dirty

			countRequests {
				eo.clickButton('Objekt generieren')
				messageModal.confirm()
				GenerationComponent.openResult()

				assert driver.getWindowHandles().size() == 2

				String oldWindow = driver.windowHandle
				String newWindow = switchToOtherWindow()

				assert oldWindow != newWindow

				assert !eo.dirty

				driver.close()
				driver.switchTo().window(oldWindow)
			}.with {
				// 1 request to save the dirty EO
				assert it.getRequestCount(RequestType.EO_UPDATE) == 1

				assert it.getRequestCount(RequestType.EO_GENERATION) == 1

				// 1 request to read the generated EO
				assert it.getRequestCount(RequestType.EO_READ) == 1
			}

			assert !eo.dirty
		}
	}

	@Test()
	void _20_generateDependent() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform('nuclet_test_other_TestObjektgeneratorDependent_testobjektgen')
		assert subform.rowCount == 0

		eo.clickButton('Dependent generieren')
		messageModal.confirm()

		assert subform.rowCount == 1
	}

	@Test()
	void _25_closeOnException() {
		GenerationComponent.generateObjectAndConfirm('Generate with close on exception')
		MessageModal modal = getMessageModal()

		assert modal.message == 'Exception from GenerateRule'

		modal.confirm()
	}

	@Test()
	void _30_generateWithDependentException() {
		GenerationComponent.generateObjectAndConfirm('Test Objektgenerator With Incomplete Dependents')
		MessageModal modal = getMessageModal()

		assert modal.message.contains('Validation')
		assert modal.message.contains('Mandatory Text')

		modal.confirm()
	}
}
