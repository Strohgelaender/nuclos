package org.nuclos.test.webclient.chart

import java.text.SimpleDateFormat

import org.apache.commons.io.IOUtils
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Modal
import org.nuclos.test.webclient.pageobjects.chart.ChartModal

import groovy.transform.CompileStatic

/**
 * testing charts by using subform data
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ChartTest extends AbstractWebclientTest {
	static EntityObject company
	static String chartOptionsString = IOUtils.toString(
			ChartTest.class.getResourceAsStream('chart-config.json')
	)
	static String referenceAttributeId = TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES.fqn + '_company'
	static String categoryAttributeId = TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES.fqn + '_umsatzjahr'
	static String series1AttributeId = TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES.fqn + '_umsatz'
	static String series2AttributeId = TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES.fqn + '_gewinn'

	static List<String> chartTypes = [
			'multiBarChart',
			'multiBarHorizontalChart',
			'lineChart',
			'stackedAreaChart',
			'scatterChart',
			'pieChart',
			'linePlusBarChart'
	]

	@Test
	void _00_createTestData() {
		company = new EntityObject(TestEntities.NUCLET_TEST_CHARTS_COMPANY)
		company.setAttribute('name', 'Company 123')

		List<LinkedHashMap<String, Integer>> testData = [
				[umsatzjahr: 2006, umsatz: 10000, gewinn: 5000],
				[umsatzjahr: 2007, umsatz: 12000, gewinn: 7000],
				[umsatzjahr: 2008, umsatz: 15000, gewinn: 7000],
				[umsatzjahr: 2009, umsatz: 17000, gewinn: 9000],
				[umsatzjahr: 2010, umsatz: 18000, gewinn: 10000],
				[umsatzjahr: 2011, umsatz: 20000, gewinn: 11000],
				[umsatzjahr: 2012, umsatz: 21000, gewinn: 10000],
				[umsatzjahr: 2013, umsatz: 18000, gewinn: 16000],
				[umsatzjahr: 2014, umsatz: 22000, gewinn: 17000],
				[umsatzjahr: 2015, umsatz: 24000, gewinn: 15000],
		]

		List<EntityObject<Long>> dependents = company.getDependents(
				TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES,
				'company'
		)

		testData.each {
			EntityObject subEo = new EntityObject<>(TestEntities.NUCLET_TEST_CHARTS_FINANCIALFIGURES)
			subEo.setAttribute('umsatzjahr', it['umsatzjahr'])
			subEo.setAttribute('umsatz', it['umsatz'])
			subEo.setAttribute('gewinn', it['gewinn'])
			dependents << subEo
		}

		nuclosSession.save(company)
	}

	@Test
	void _05_openChartModal() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_CHARTS_COMPANY)

		eo.openCharts()

		assert Modal.title == 'Charts' || Modal.title == 'Diagramme'
		assert Modal.body

		Modal.close()
		assert !Modal.visible
	}

	@Test
	void _10_createChart() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_CHARTS_COMPANY)

		eo.openCharts()

		assert ChartModal.chartCount == 0

		ChartModal.newChart()

		assert !ChartModal.configurationValid
		assert !ChartModal.referenceAttributeId
		assert !ChartModal.exportAvailable
		assert ChartModal.canSave()
		assert ChartModal.chartCount == 1
		assert ChartModal.tabTitles == ['new chart'] || ChartModal.tabTitles == ['neues Diagramm']

		ChartModal.close()
	}

	@Test
	void _15_openAndAddChart() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_CHARTS_COMPANY)

		eo.openCharts()

//		assert ChartModal.tabTitles == ['new chart'] || ChartModal.tabTitles == ['neues Diagramm']
//
//		assert ChartModal.selectedChartIndex == 0
//
//		// Was not saved before - should still be dirty after re-opening the modal
//		assert ChartModal.canSave()

		ChartModal.newChart()
		assert ChartModal.selectedChartIndex == 0
		assert !ChartModal.exportAvailable
	}

	@Test
	void _20_saveChart() {
		ChartModal.newChart()

		countRequests {
			ChartModal.saveChart()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_CREATE) == 1
			assert it.getRequestCount(RequestType.PREFERENCE_UPDATE) == 0

			assert it.requestCount == 1
		}

		assert !ChartModal.canSave()

		ChartModal.setChartName('Test-Chart 2')
		assert ChartModal.canSave()

		countRequests {
			ChartModal.saveChart()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_CREATE) == 0
			assert it.getRequestCount(RequestType.PREFERENCE_UPDATE) == 1

			assert it.requestCount == 1
		}
	}

	@Test
	void _25_deleteChart() {
		assert ChartModal.chartCount == 2

		countRequests {
			ChartModal.deleteChart()
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_DELETE) == 1
			assert it.requestCount == 1
		}

		assert ChartModal.chartCount == 1
	}

	@Test
	void _30_configureChart() {
		ChartModal.chartName = 'Test-Chart'

		ChartModal.saveChart()

		assert ChartModal.chartName == 'Test-Chart'

		assert !ChartModal.chartVisible

		ChartModal.chartOptions = chartOptionsString

		assert ChartModal.configurationValid
		assert ChartModal.referenceAttributeId == referenceAttributeId
		assert ChartModal.chartVisible
		assert ChartModal.multibarChartVisible
	}

	@Test
	void _40_chartTypes() {
		assert ChartModal.chartType == chartTypes[0]

		chartTypes.each {
			ChartModal.chartType = it
			assert ChartModal.chartType == it
			assert ChartModal.chartVisible
		}
	}

	@Test
	void _45_filterChartData() {
		assert ChartModal.chartDataCount == 10
		assert ChartModal.exportAvailable

		ChartModal.setFilter(0, '1970-1-1')
		ChartModal.applyFilter()
		assert ChartModal.chartDataCount == 0
		assert !ChartModal.exportAvailable

		String today = new SimpleDateFormat('yyyy-MM-dd').format(new Date())
		ChartModal.setFilter(0, today)
		ChartModal.applyFilter()
		assert ChartModal.chartDataCount == 10

		ChartModal.setFilter(1, '2008')
		ChartModal.applyFilter()
		assert ChartModal.chartDataCount == 8

		ChartModal.setFilter(2, '2012')
		ChartModal.applyFilter()
		assert ChartModal.chartDataCount == 5
	}

	@Test
	void _47_exportChartAsCsv() {
		ChartModal.exportChart()

		// TODO: Check content of the downloaded file
	}

	@Test
	void _50_createChartViaGuiOnly() {
		ChartModal.newChart()

		assert ChartModal.chartSeriesCount == 0

		ChartModal.chartName = 'Configured via GUI'
		ChartModal.referenceAttributeId = referenceAttributeId
		ChartModal.categoryAttributeId = categoryAttributeId
		ChartModal.newSeries()
		assert ChartModal.chartSeriesCount == 1

		assert !ChartModal.chartVisible
		ChartModal.seriesAttributeId = series1AttributeId
		assert ChartModal.chartVisible

		ChartModal.newSeries()
		assert ChartModal.chartSeriesCount == 2
		assert ChartModal.chartVisible
		ChartModal.seriesAttributeId = series2AttributeId
		assert ChartModal.chartVisible

		ChartModal.newSeries()
		assert ChartModal.chartSeriesCount == 3
		ChartModal.removeSeries()
		assert ChartModal.chartSeriesCount == 2
	}
}

