package org.nuclos.test.webclient.entityobject

import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.HttpPost
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.RestResponse
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DropdownPerformanceTest extends AbstractWebclientTest {

	private static List<String> choicesForZwa = ['Zwar', 'abgezwackt', 'vierundzwanzig']
	private static List<String> choicesForKzert = ['Konzert']
	private static String kzert = 'k*zert'

	@Test
	void _00_setup() {
		InputStream stream = getClass().getResourceAsStream('exampletext.txt')
		String exampleText = IOUtils.toString(stream, 'UTF-8')

		EntityObject word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)

		word.setAttribute('text', 'jweoriwejori')
		word.setAttribute('times', "30000")
		word.setAttribute('exampletext', exampleText)

		nuclosSession.save(word)
		word.executeCustomRule('example.rest.RegisterWords')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)

		eo.addNew()
	}

	@Test
	void _01_searchForDate() {
		HttpPost post = new HttpPost(RESTHelper.REST_BASE_URL +
				'/bos/example_rest_Word/query?' +
				'offset=0&' +
				'gettotal=true&' +
				'chunksize=100&' +
				'countTotal=true&' +
				'search=1972&' +
				'withTitleAndInfo=false&' +
				'attributes=text,times,agent,kategorie,parent,date,countletters&' +
				'sort=example_rest_Word_date+asc&' +
				'skipStatesAndGenerations=true'
		)
		RestResponse response = RESTHelper.requestResponse(nuclosSession, post)

		// TODO: Normally this should be exactly 1 query
		assert response.sqlCount > 0 && response.sqlCount <= 5

		assert response.sqlTime > 0 && response.sqlTime < 1000
	}

	@Test
	void _05_testLov() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('lov')

		lov.inputElement.click()
		inputZ(lov)
		inputAddWa(lov)
		inputKzert(lov)
		lov.close()
	}

	@Test
	void _10_testLovWithVlp() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('lovwithvlp')

		lov.inputElement.click()
		inputZ(lov)
		inputAddWa(lov)
		inputKzert(lov)
		lov.close()
	}

	@Test
	void _15_testCombobox() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('combobox')

		lov.inputElement.click()
		inputZ(lov)
		inputAddWa(lov)
		inputKzert(lov)
		lov.close()
	}

	@Test
	void _20_testComboboxWithVlp() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('comboboxwithvlp')

		lov.inputElement.click()
		inputZ(lov)
		inputAddWa(lov)
		inputKzert(lov)
		lov.close()
	}

	private static void inputZ(ListOfValues lov) {
		sendKeys('z')
		assert lov.open
		assert lov.choiceElements.size() == 100
	}

	private static void inputAddWa(ListOfValues lov) {
		sendKeys('wa')
		assert lov.choices == choicesForZwa
	}

	private static void inputKzert(ListOfValues lov) {
		lov.inputElement.clear()
		sendKeys(kzert)
		assert lov.choices == choicesForKzert
	}
}
