package org.nuclos.test.webclient.businesstest

import static org.nuclos.test.webclient.pageobjects.businesstest.BusinessTestsDetails.*

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractWebclientTest

import groovy.transform.CompileStatic

/**
 * TODO: Some of the tests do not involve the GUI directly and could be run directly via REST for faster execution.
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class BusinessTestsDetailsTest extends AbstractWebclientTest {
	final String insert10Articles = '''import example.rest.Article

10.times {
	new Article(
			articleNumber: it,
			name: "Article $it".toString(),
			price: it * 10 + 0.1,
			active: true
	).save()
}
'''

	/**
	 * Runs the given script.
	 *
	 * @param script
	 */
	private static void runScript(final String script) {
		setTestScript(script)
		executeTest()
	}

	/**
	 * Runs the given script and asserts that there were no errors.
	 *
	 * @param script
	 */
	private static void testScript(final String script) {
		runScript(script)

		String result = getTestResult()
		assert result == 'PASSED' : "Test did not pass - result = '$result'"

		String state = getTestState()
		assert state == 'OK' : "Test state not ok - state = '$state'"
	}

	@Test
	void _00_setup() {
		logout()
		login('nuclos')
	}

	@Test
	void _03_createTest() {
		createNewTest('Test', 'println 123')

		waitFor { !currentUrl.endsWith('/new') } // Saving the test should have caused a redirect
		assert getTestScript().contains('context')
	}

	@Test
	void _04_updateTest() {
		def newScript = '''println 'Test...'
print 123'''
		setTestScript(newScript)
		saveTest()
		refresh()
		waitFor() {
			getTestScript() == newScript
		}
	}


	@Test
	void _05_executeTest() {
		executeTest()

		assert getTestResult() == 'PASSED'
		assert getTestState() == 'OK'

		assert getTestLog().contains('Test...\n123')
	}

	@Test
	void _07_scriptAttempt() {
		final String script = '''import example.rest.Article

attempt {
	assert false
}

attempt {
	1/0
}

3.times {
	attempt {
		new Article().save()
	}
}

attempt {
	println "Last attempt"
}'''

		setTestScript(script)
		executeTest()

		assert getTestResult() == 'PASSED with errors'
		assert getWarningCount() == 3
		assert getErrorCount() == 2
		assert getTestLog().contains('Last attempt')

		assert getTestLog().contains('Assertion failed'): 'First error should have been logged'
	}

	@Test
	void _09_scriptWhere() {
		final String script = insert10Articles + '''

final String query = 'example_rest_Article_articleNumber >= 5 AND example_rest_Article_price <= 80.1\'

assert Article.query(query).size() == 4
assert Article.query(query, -1).size() == 4
assert Article.query(query, 0).size() == 0
assert Article.query(query, 1).size() == 1
assert Article.query(query, 2).size() == 2
assert Article.query(query, 3).size() == 3
assert Article.query(query, 4).size() == 4
assert Article.query(query, 5).size() == 4
'''

		this.testScript(script)
	}

	@Test
	void _10_scriptStateChange() {
		// TODO: Create sample records, find by state, change state
	}

	@Test
	void _11_scriptFailedWarning() {
		runScript('new example.rest.Article().save()')

		assert getTestState() == 'WARNING'
		assert getWarningCount() == 1
		assert getErrorCount() == 0

		// Assert validation errors for mandatory fields show up in the result message
		assert getTestResult().contains('"Name"')
		assert getTestResult().contains('"Article number"')
		assert getTestResult().contains('"Price"')
		assert getTestResult().contains('"Active"')

		assert getTestLog().contains('common.exception.novabitvalidationexception')
	}

	@Test
	void _12_scriptFailedError() {
		runScript('1/0')

		assert getTestState() == 'ERROR'
		assert getWarningCount() == 0
		assert getErrorCount() == 1

		assert getTestResult() == 'Division by zero'

		assert getTestLog().contains('java.lang.ArithmeticException: Division by zero')
	}

	@Test
	void _13_scriptCRUD() {
		final String script = insert10Articles + '''
assert Article.list().size() == 10

Article article = Article.list(1).get(0)

assert article.articleNumber == 0

article.name = "$article.name - updated"
article.save()

Article article2 = Article.get(article.id)

assert article2.name == "Article 0 - updated".toString()

article2.delete()

assert Article.get(article.id) == null
assert Article.list().size() == 9
'''

		this.testScript(script)
	}

	@Test
	void _15_scriptSubform() {
		final String script = getClass().getResource('Multiref Test.groovy').text

		this.testScript(script)
	}

	@Test
	void _16_scriptCustomRules() {
		final String script = '''import nuclet.test.rules.TestRules

TestRules testRules = new TestRules(name: 'Test CustomRule')
testRules.executeBenutzeraktion()
assert testRules.benutzeraktion == 'ok\'
'''

		this.testScript(script)
	}

	@Test
	void _17_scriptList() {
		final String script = insert10Articles + '''

assert Article.list().size() == 10
assert Article.list(2).size() == 2
assert Article.list(0).size() == 0
assert Article.list(-1).size() == 10
'''

		this.testScript(script)
	}

	@Test
	void _18_editorHighlightError() {
		final String script = '''

1/0

'''

		runScript(script)

		waitFor {
			List<Marker> markers = highlights

			markers.size() == 1 && markers.find {
				Marker m -> m.type == Marker.TYPE.ERROR && m.line == 3
			}
		}
	}

	@Test
	void _19_editorHighlightWarning() {
		final String script = '''

new example.rest.Order().save()

'''

		runScript(script)

		waitFor {
			List<Marker> markers = highlights

			markers.size() == 1 && markers.find { Marker m ->
				m.type == Marker.TYPE.WARNING && m.line == 3
			}
		}
	}

	@Test
	void _20_editorImportError() {
		final String script = '''
import asdfasdfasdf

'''

		runScript(script)

		waitFor {
			List<Marker> markers = highlights
			markers.size() == 1 && markers.find { Marker m ->
				m.type == Marker.TYPE.ERROR && m.line == 2
			}
		}
	}

	@Test
	void _21_editorAttemptError() {
		final String script = '''
1/0
'''

		runScript(script)

		waitFor {
			List<Marker> markers = highlights
			markers.size() == 1 && markers.find { Marker m ->
				m.type == Marker.TYPE.ERROR && m.line == 2
			}
		}
	}

	@Test
	void _22_editorAttemptWarning() {
		final String script = '''
attempt {
	new example.rest.Order().save()
}

'''

		runScript(script)


		waitFor {
			List<Marker> markers = highlights
			markers.size() == 1 && markers.find { Marker m ->
				m.type == Marker.TYPE.WARNING && m.line == 3
			}
		}
	}

	@Test
	void _23_editorAttemptWarningAndError() {
		final String script = '''
attempt {
	new example.rest.Order().save()
}

attempt {
	1/0
}
'''

		runScript(script)

		waitFor {
			List<Marker> markers = highlights
			markers.size() == 1 && markers.find { Marker m ->
				m.type == Marker.TYPE.ERROR && m.line == 7
			}
		}
	}

	@Test
	void _24_editorScrolling() {
		assert isRowVisible(0)

		final String script = '\n' * 100 + '1/0'
		runScript(script)

		waitFor {
			List<Marker> markers = highlights
			return markers.size() == 1 &&
					markers.find { Marker m -> m.type == Marker.TYPE.ERROR && m.line == 101 } &&
					!isRowVisible(0) &&
					isRowVisible(100)
		}
	}

	@Test
	void _25_scriptChangeProcess() {
		final String script = '''import example.rest.Order

Order o = new Order(orderNumber: 123)

assert !o.nuclosProcess
assert !o.nuclosProcessId

o.save()

assert !o.nuclosProcess
assert !o.nuclosProcessId

o.changeProcessPriorityorder()

assert o.nuclosProcessId

o.save()

assert o.nuclosProcess
assert o.nuclosProcessId

o.unsetProcess()

assert !o.nuclosProcessId

o.save()

assert !o.nuclosProcess
assert !o.nuclosProcessId
'''

		this.testScript(script)
	}

	@Test
	void _26_scriptTestUIDReference() {
		final String script = '''import org.nuclos.common.UID
import nuclet.test.rules.TestAPI

TestAPI test = new TestAPI(test: 'test')

assert test.refuser == null
assert test.refuserId == null

test.refuserId = new UID('nuclos1000')
assert test.refuserId == new UID('nuclos1000')

test.save()

assert test.refuserId == new UID('nuclos1000')
assert test.refuser == 'nuclos\'

test.refuserId = null
assert test.refuserId == null

test.save()

assert test.refuser == null
assert test.refuserId == null
'''

		this.testScript(script)
	}

	/**
	 * Tests the inclusion (via "evaluate()") of one business test into another.
	 */
	@Test
	void _27_scriptEvaluateOtherTest() {
		createNewTest('A', '''import example.rest.Article

Article a = new Article(articleNumber: 1, name: 'Article A', price: 0.01, active: true).save()
println 'Saved Article A'

return a.id
''')
		final String uidA

		waitFor {
			uidA = getTestUID()
		}

		createNewTest('B', """import example.rest.Article

Long articleID = evaluate(new org.nuclos.common.UID('$uidA'))
Article a = Article.get(articleID)

assert a.name == 'Article A'

println 'OK'
""".toString())

		executeTest()

		waitFor {
			def log = getTestLog()
			log.contains('Saved Article A') && log.contains('OK')
		}
	}

	@Test
	void _28_scriptEntityToString() {
		this.testScript('''import example.rest.Order

Order o = new Order(orderNumber: 123)

assert o.toString() == 'Order null\'

Long id = o.save().id
assert id

assert o.toString() == "Order $id"''')
	}

	@Test
	void _30_withUser() {
		this.testScript('''
import org.nuclos.server.security.NuclosLocalServerSession

// Try nested User changes and check if the closured where really
// executed as the respective user
boolean executedAsTest = false
boolean executedAsNuclos = false

assert NuclosLocalServerSession.getInstance().getCurrentUser() == 'nuclos'
withUser('test') {
	assert NuclosLocalServerSession.getInstance().getCurrentUser() == 'test'
	executedAsTest = true
	withUser('nuclos') {
		assert NuclosLocalServerSession.getInstance().getCurrentUser() == 'nuclos'
		executedAsNuclos = true
	}
}

assert executedAsTest
assert executedAsNuclos


// Try to change to a non-existent user and check the expected Exception
Exception caughtException
try {
	withUser('nonExistentUser') {
		// Should never be reached
		assert false
	}
} catch (Exception e) {
	caughtException = e
}

assert caughtException
assert caughtException.toString().contains("UsernameNotFoundException")
''')
	}

	@Test
	void _31_withUserRecordGrant() {
		this.testScript('''
import org.nuclos.server.businesstest.execution.BusinessTestScript

import example.rest.Order
import example.rest.Word
import groovy.transform.BaseScript

@BaseScript
BusinessTestScript context


assert Order.list().empty

new Word(
		text: 'a',
		times: 1,
		agent: 'nuclos'
).save()

new Word(
		text: 'b',
		times: 1,
		agent: 'test'
).save()

// Nuclos can see all words
assert Word.list().collect{ it.text }.toSorted() == ['a', 'b']

withUser('test') {
	// test can see only 1 Word via record grant
	assert Word.list().collect{ it.text }.toSorted() == ['b']
}

// Nuclos can see all words
assert Word.list().collect{ it.text }.toSorted() == ['a', 'b']
''')
	}

	@Test
	void _32_withUserRollback() {
		this.testScript('''
import example.rest.Order
import groovy.transform.BaseScript
import org.nuclos.server.businesstest.execution.BusinessTestScript

@BaseScript
BusinessTestScript context


assert Order.list().empty

withUser('test') {
	// This closure runs on a new Thread
	// A rollback can only happen immediately afterwards, before the Thread is destroyed
	new Order(orderNumber: 123).save()
	assert Order.list().size() == 1
}

// Order created by test should still be there
assert Order.list().size() == 1
assert Order.list().first().createdBy == 'test'
''')
	}

	@Test
	void _35_messageContextSendMessage() {
		this.testScript('''
import groovy.transform.BaseScript
import org.nuclos.server.businesstest.execution.BusinessTestScript

import nuclet.test.rules.TestRules

@BaseScript
BusinessTestScript context


withUser('nuclos') {
	TestRules rules = new TestRules(name: 'Test').save()
	rules.executeMessagebox()
}
''')
	}

	@Test
	void _90_deleteTest() {
		deleteTest()

		// Deleting the test should have caused a redirect to the overview
		waitFor {
			currentUrl.endsWith('/businesstests/advanced')
		}
	}
}
