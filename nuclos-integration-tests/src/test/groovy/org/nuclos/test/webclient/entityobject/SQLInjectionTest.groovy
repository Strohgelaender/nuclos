package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SQLInjectionTest extends AbstractWebclientTest {

	static RESTClient client

	@Test
	void _00_setup() {
		client = new RESTClient('nuclos', '')
		client.login()

		EntityObject<Long> sqlsnippet = new EntityObject(TestEntities.NUCLET_TEST_OTHER_SQLSNIPPET)
		sqlsnippet.setAttribute('sql', 'T1.INTARTICLENUMBER > 4000')
		client.save(sqlsnippet)

		EntityObject<Long> article0 = new EntityObject(TestEntities.EXAMPLE_REST_ARTICLE)
		article0.setAttribute('articleNumber', 0)
		article0.setAttribute('name', 'Armer Ritter')
		article0.setAttribute('price', 0.01)
		client.save(article0)

		EntityObject<Long> article4711 = new EntityObject(TestEntities.EXAMPLE_REST_ARTICLE)
		article4711.setAttribute('articleNumber', 4711)
		article4711.setAttribute('name', '4711')
		article4711.setAttribute('price', 47.11)
		client.save(article4711)
	}

	@Test()
	void _01_testVlpWithSqlParamForbidden() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_SQLINJECTIONTEST)
		eo.addNew()
		eo.setAttribute('paramvalue', 'true = true')

		eo = EntityObjectComponent.forDetail()
		ListOfValues lov = eo.getLOV('vlpwithsqlparam')
		lov.close()
		lov.clickDropdownButton()
		// no values
		assert lov.choices.empty
		lov.sendKeys('*')
		assert lov.choices.empty

		eo.cancel()
	}

	@Test()
	void _02_testVlpWithSqlParamOnWhitelist() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_SQLINJECTIONTEST)
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()
		eo.setAttribute('paramvalue', 'T1.INTARTICLENUMBER > 4000')
		ListOfValues lov = eo.getLOV('vlpwithsqlparam')
		lov.close()
		lov.clickDropdownButton()

		assert lov.open
		assert lov.choices == ['','4711']

		eo.cancel()
	}

	@Test()
	void _03_testVlpWithTextParamForbidden() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_SQLINJECTIONTEST)
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()
		eo.setAttribute('paramvalue', 'X%\' OR TRUE; --')
		ListOfValues lov = eo.getLOV('vlpwithtextparam')
		lov.close()
		lov.clickDropdownButton()

		assert lov.open
		assert lov.choices == ['']

		eo.cancel()
	}

	@Test()
	void _04_testVlpWithTextParamAllowed() {
		EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_SQLINJECTIONTEST)
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.addNew()
		eo.setAttribute('paramvalue', 'tt')
		ListOfValues lov = eo.getLOV('vlpwithtextparam')
		lov.close()
		lov.clickDropdownButton()

		assert lov.open
		assert lov.choices == ['', 'Armer Ritter']

		eo.cancel()
	}

	@Test()
	void _05_testDatasourceWithListOfString() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_SQLINJECTIONTEST)
		eo.addNew()
		eo.setAttribute('paramvalue', 'datasource list of string test')
		eo.setAttribute('testlistofstrings', true)

		eo.save()

		assert eo.getAttribute('resultlistofstring') == true
	}

	@Test()
	void _06_testDatasourceWithListOfInteger() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_SQLINJECTIONTEST)
		eo.addNew()
		eo.setAttribute('paramvalue', 'datasource list of integer test')
		eo.setAttribute('testlistofinteger', true)

		eo.save()

		assert eo.getAttribute('resultlistofinteger') == true
	}

	@Test()
	void _07_testDatasourceWithSqlParam() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_SQLINJECTIONTEST)
		eo.addNew()
		eo.setAttribute('paramvalue', 'datasource sql param test')
		eo.setAttribute('testsqlinreport', true)

		eo.save()

		assert eo.getAttribute('resultsqlinreport') == true
	}
}
