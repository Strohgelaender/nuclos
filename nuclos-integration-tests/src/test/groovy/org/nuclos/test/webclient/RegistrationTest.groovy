package org.nuclos.test.webclient

import javax.mail.MessagingException
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

import org.apache.commons.mail.util.MimeMessageParser
import org.jsoup.Jsoup
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.PortAllocator
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.pageobjects.ActivationComponent
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.Busy
import org.nuclos.test.webclient.pageobjects.RegistrationComponent
import org.nuclos.test.webclient.pageobjects.account.ForgotLoginDetailsComponent
import org.nuclos.test.webclient.pageobjects.account.PasswordResetComponent

import com.icegreen.greenmail.user.GreenMailUser
import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RegistrationTest extends AbstractWebclientTest {
	static ServerSetup serverSetup
	static GreenMail greenMail
	static GreenMailUser mailUser

	static Map<String, String> oldSystemParameters

	static String registrationEmail = 'testregistration@nuclos.de'

	static String activationUrl

	@BeforeClass
	static void setup() throws IOException, URISyntaxException, MessagingException {
		AbstractWebclientTest.setup()

		oldSystemParameters = nuclosSession.simpleSystemParameters

		setupMailServer:
		{
			// Copy the default GreenMail setups and make some adjustments
			final int port = PortAllocator.allocate()
			serverSetup = new ServerSetup(port, ServerSetup.SMTP.bindAddress, ServerSetup.SMTP.protocol)
			serverSetup.serverStartupTimeout = 10000
			greenMail = new GreenMail(serverSetup)

			//Start all email servers using non-default ports.
			greenMail.start()
			mailUser = greenMail.setUser("test-user@nuclos.de", "test", "test")
		}

		nuclosSession.deleteUserByEmail(registrationEmail)
		nuclosSession.deleteUserByEmail('2' + registrationEmail)
		nuclosSession.deleteUserByEmail(registrationEmail + '2')
		RESTHelper.createUser('anonymous', 'anonymous', ['Example user'], nuclosSession)

		Map<String, String> params = [
				'SMTP Server'                   : greenMail.smtp.bindTo,
				'SMTP Port'                     : greenMail.smtp.port as String,
				'SMTP Username'                 : mailUser.email,
				'SMTP Password'                 : mailUser.password,
				'SMTP Authentication'           : 'Y',
				'SMTP Sender'                   : 'registration@nuclos.de',

				// Anonymous login and self registration
				'ANONYMOUS_USER_ACCESS_ENABLED' : 'Y',
				'ROLE_FOR_SELF_REGISTERED_USERS': 'Example user',
				'ACTIVATION_EMAIL_SUBJECT'      : 'Nuclos account registration',
				'ACTIVATION_EMAIL_MESSAGE'      : activationMessage,
				'EMAIL_SIGNATURE'               : '<b>TODO: signature</b>',
				'INITIAL_ENTRY'                 : 'anonymous:' + TestEntities.EXAMPLE_REST_ORDER.fqn
		]

		nuclosSession.patchSystemParameters(params)
	}

	static String getActivationMessage() {
		"""Hello {0} {1},
<br/><br/>
to activate your account <a href="$context.nuclosWebclientBaseURL#/account/activate/{2}/{3}">click here</a>.
<br/><br/>
		Bye
"""
	}

	@AfterClass
	static void teardown() {
		greenMail.stop()

		nuclosSession.setSystemParameters(oldSystemParameters)
		nuclosSession.deleteUserByEmail('test@nuclos.de')

		AbstractWebclientTest.teardown()
	}

	@Test
	void _00_loginAsAnonymous() {
		logout()
		refresh()

		// Should be logged in as anonymous and redirected to the "initial entry" Order
		assert currentUrl.contains('example_rest_Order')
	}

	@Test
	void _05_register() {
		getUrlHash('/login')

		AuthenticationComponent.clickSelfRegistration()

		RegistrationComponent.submitAndAssertError(['Datenschutzerklärung', 'privacy consent'])
		RegistrationComponent.togglePrivacyConsent()

		RegistrationComponent.submitAndAssertError(['Benutzername', 'User name'])

		RegistrationComponent.username = 'testregistration'
		RegistrationComponent.submitAndAssertError(['Kennwort', 'Password'])

		RegistrationComponent.password = 'test'
		RegistrationComponent.submitAndAssertError(['Vorname', 'First name'])

		RegistrationComponent.firstname = 'Firstname'
		RegistrationComponent.submitAndAssertError(['Nachname', 'Last name'])

		RegistrationComponent.lastname = 'Lastname'
		RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])

		// Not a valid email format
		RegistrationComponent.email = 'test'
		// TODO: Check if the message really equals 'webclient.account.email.invalid'
		RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])


		RegistrationComponent.email = registrationEmail
		// TODO: Check if the message really equals 'webclient.account.email.confirmation.unequal'
		RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])

		// Unequal email and confirmation
		RegistrationComponent.email2 = registrationEmail + '2'
		// TODO: Check if the message really equals 'webclient.account.email.confirmation.unequal'
		RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])

		RegistrationComponent.email2 = registrationEmail

		assert !Busy.busy

		RegistrationComponent.submit()
		assert RegistrationComponent.successMessage
	}

	@Test
	void _10_loginBeforeActivation() {
		getUrlHash('/login')

		loginUnsafe('testregistration', 'test')

		// TODO: Check message content
		assert AuthenticationComponent.errorMessage
	}

	@Test
	void _15_activationEmail() {
		greenMail.waitForIncomingEmail(1)
		MimeMessage activationMail = greenMail.receivedMessages.last()
		greenMail.purgeEmailFromAllMailboxes()

		assert activationMail.from.first() == new InternetAddress('registration@nuclos.de')

		MimeMessageParser parser = new MimeMessageParser(activationMail)
		parser.parse()
		String htmlContent = parser.getHtmlContent()
		String plainContent = parser.getPlainContent()

		assert !plainContent
		assert htmlContent

		activationUrl = Jsoup.parse(htmlContent).select('a[href]').first().attr('href')
		assert activationUrl
	}

	@Test
	void _20_activateViaLink() {
		getUrl(activationUrl + '_wrong')
		assert ActivationComponent.errorMessage

		getUrl(activationUrl)
		assert ActivationComponent.successMessage
	}

	@Test
	void _25_loginAfterActivation() {
		getUrlHash('/login')
		assert login('testregistration', 'test')
	}

	@Test
	void _30_uniqueValues() {
		logout()
		AuthenticationComponent.clickSelfRegistration()

		RegistrationComponent.togglePrivacyConsent()
		RegistrationComponent.username = 'testregistration'
		RegistrationComponent.password = 'test'
		RegistrationComponent.firstname = 'Firstname'
		RegistrationComponent.lastname = 'Lastname'
		RegistrationComponent.email = registrationEmail
		RegistrationComponent.email2 = registrationEmail

		username: {
			RegistrationComponent.submitAndAssertError(['Benutzername', 'User name'])
			RegistrationComponent.username = 'testregistration2'
		}

		email: {
			RegistrationComponent.submitAndAssertError(['E-Mail', 'Email'])
			RegistrationComponent.email = '2' + registrationEmail
			RegistrationComponent.email2 = '2' + registrationEmail
		}

		RegistrationComponent.submit()
		assert RegistrationComponent.successMessage
	}

	@Test
	void _35_forgotLoginDetails() {
		if (!logout()) {
			getUrlHash('/login')
		}

		// Link should only be visible after all necessary system parameters are configured
		assert !AuthenticationComponent.forgotLoginDetailsLink

		nuclosSession.patchSystemParameters([
				'USERNAME_EMAIL_SUBJECT': 'Forgot username',
				'USERNAME_EMAIL_MESSAGE': '''Hello {0} {1},
<br/><br/>
you are registered with the following detalis:
<br/><br/>
Username: {2}<br/>
First name: {0}<br/>
Last name: {1}<br/>
<br/><br/>
		Bye
''',
				'RESET_PW_EMAIL_SUBJECT': 'Forgot username',
				'RESET_PW_EMAIL_MESSAGE': """Hello {0} {1},
<br/><br/>
you are registered with the following details:
<br/><br/>
Username: {2}<br/>
First name: {0}<br/>
Last name: {1}<br/>
Password reset code: {3}<br/><br/>

to reset your password <a href="$context.nuclosWebclientBaseURL#/account/resetPassword/{2}/{3}">click here</a>.
<br/><br/>
		Bye
""".toString()
		])

		refresh()
	}

	@Test
	void _40_forgotUsername() {
		if (!logout()) {
			getUrlHash('/login')
		}

		AuthenticationComponent.forgotLoginDetailsLink.click()

		assert ForgotLoginDetailsComponent.type == ForgotLoginDetailsComponent.TYPE.FORGOT_USERNAME
		assert !ForgotLoginDetailsComponent.submitButton.enabled
		assert !ForgotLoginDetailsComponent.usernameField?.displayed
		assert ForgotLoginDetailsComponent.emailField.displayed

		ForgotLoginDetailsComponent.email = 'non_existent_email@nuclos.de'

		ForgotLoginDetailsComponent.submitAndAssertError(['exist', 'There is no'])

		ForgotLoginDetailsComponent.email = 'test@nuclos.de'
		ForgotLoginDetailsComponent.submit()
		assert ForgotLoginDetailsComponent.successMessage

		greenMail.waitForIncomingEmail(1)
		MimeMessage activationMail = greenMail.receivedMessages.last()
		greenMail.purgeEmailFromAllMailboxes()

		assert activationMail.from.first() == new InternetAddress('registration@nuclos.de')

		MimeMessageParser parser = new MimeMessageParser(activationMail)
		parser.parse()
		String htmlContent = parser.getHtmlContent()

		assert htmlContent.contains('Username: test')
		assert htmlContent.contains('First name: Firstname')
		assert htmlContent.contains('Last name: Lastname')

		ForgotLoginDetailsComponent.clickLoginLink()
	}

	@Test
	void _45_forgotPassword() {
		AuthenticationComponent.forgotLoginDetailsLink.click()
		ForgotLoginDetailsComponent.type = ForgotLoginDetailsComponent.TYPE.FORGOT_PASSWORD

		assert ForgotLoginDetailsComponent.type == ForgotLoginDetailsComponent.TYPE.FORGOT_PASSWORD
		assert !ForgotLoginDetailsComponent.submitButton.enabled
		assert ForgotLoginDetailsComponent.usernameField.displayed
		assert !ForgotLoginDetailsComponent.emailField?.displayed

		ForgotLoginDetailsComponent.username = 'non_existent_user'

		ForgotLoginDetailsComponent.submitAndAssertError(['exist', 'There is no'])

		ForgotLoginDetailsComponent.username = 'test'
		ForgotLoginDetailsComponent.submit()
		assert ForgotLoginDetailsComponent.successMessage

		greenMail.waitForIncomingEmail(1)
		MimeMessage activationMail = greenMail.receivedMessages.last()
		greenMail.purgeEmailFromAllMailboxes()

		assert activationMail.from.first() == new InternetAddress('registration@nuclos.de')

		MimeMessageParser parser = new MimeMessageParser(activationMail)
		parser.parse()
		String htmlContent = parser.getHtmlContent()

		assert htmlContent.contains('Username: test')
		assert htmlContent.contains('First name: Firstname')
		assert htmlContent.contains('Last name: Lastname')
		assert htmlContent.contains('Password reset code: ')

		activationUrl = Jsoup.parse(htmlContent).select('a[href]').first().attr('href')
		assert activationUrl
	}

	@Test
	void _50_resetPassword() {
		getUrl(activationUrl)

		assert !PasswordResetComponent.errorMessage
		assert !PasswordResetComponent.successMessage

		PasswordResetComponent.submitAndAssertError(['Passwort', 'Password'])

		PasswordResetComponent.newPassword = 'test'
		PasswordResetComponent.submitAndAssertError(['Passwörter', 'Passwords'])

		PasswordResetComponent.confirmNewPassword = 'foo'
		PasswordResetComponent.submitAndAssertError(['Passwörter', 'Passwords'])

		// Password in the DB is already "test" - expect an error when trying to set it to "test" again
		PasswordResetComponent.confirmNewPassword = 'test'
		PasswordResetComponent.submitAndAssertError(['Valid'])

		PasswordResetComponent.newPassword = 'foo'
		PasswordResetComponent.confirmNewPassword = 'foo'

		PasswordResetComponent.submit()
		assert PasswordResetComponent.successMessage
	}

	@Test
	void _55_loginWithNewPassword() {
		getUrlHash('/login')
		assert login('test', 'foo')
	}
}
