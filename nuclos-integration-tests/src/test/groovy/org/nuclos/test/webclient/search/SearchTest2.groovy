package org.nuclos.test.webclient.search

import static org.nuclos.test.webclient.search.SearchTest.statusbarCount

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.search.Searchbar

import groovy.transform.CompileStatic

/**
 * TODO: Merge with org.nuclos.test.webclient.search.SearchTest
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SearchTest2 extends AbstractWebclientTest {
	static String filter1 = 'Filter 1'
	static String filter2 = 'Filter 2'

	@Test
	void _00_setup() {
		TestDataHelper.insertTestData(nuclosSession)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}

	@Test
	void _05_createFirstSearchfilter() {

		// TODO: Attributes should always be available, without the need to explicitly create a filter first
		assert !Searchbar.attributeLov

		countRequests {
			Searchbar.create()

			assert ['Searchfilter 1', 'Suchfilter 1'].contains(Searchbar.selectedFilter)
			assert Searchbar.filters == ['', Searchbar.selectedFilter]

			Searchbar.selectAttribute('Note')

			Searchbar.setCondition(
					TestEntities.EXAMPLE_REST_ORDER,
					new Searchtemplate.SearchTemplateItem(
							name: 'note',
							operator: '=',
							value: 'A'
					)
			)
		}.with {
			assert it.getRequestCount(RequestType.PREFERENCE_ALL) == 3
			assert it.getRequestCount(RequestType.EO_READ_LIST) >= 1
			assert it.getRequestCount(RequestType.EO_READ_LIST) <= 3
		}

		assert Sidebar.listEntries.size() == 2
	}

	@Test
	void _06_renameFirstSearchfilter() {
		countRequests {
			Searchbar.editName(filter1)
		}.with {
			assert it.getRequestCount(RequestType.ALL) <= 2
			assert it.getRequestCount(RequestType.PREFERENCE_SAVE) == 1
		}
	}

	@Test
	void _07_removeSearchItem() {
		Searchbar.removeAttribute(TestEntities.EXAMPLE_REST_ORDER.fqn + '_note')
		assert Sidebar.listEntries.size() == 6
	}

	@Test
	void _08_addOtherSearchCondition() {
		Searchbar.selectAttribute('Note')

		Searchbar.setCondition(
				TestEntities.EXAMPLE_REST_ORDER,
				new Searchtemplate.SearchTemplateItem(
						name: 'note',
						operator: '=',
						value: 'B'
				)
		)

		assert Sidebar.listEntries.size() == 4
		assert statusbarCount() == 4
	}

	@Test
	void _10_createSecondSearchfilter() {
		countRequests {
			Searchbar.create()
			assert ['Searchfilter 1', 'Suchfilter 1'].contains(Searchbar.selectedFilter)
			assert Searchbar.filters == ['', filter1, Searchbar.selectedFilter]

			Searchbar.selectAttribute('Order number')
			new Searchtemplate.SearchTemplateItem(
					name: 'orderNumber',
					operator: '>',
					value: '100'
			)
		}.with {
			assert it.getRequestCount(RequestType.EO_READ_LIST) == 1
			assert it.getRequestCount(RequestType.ALL) == 2
		}

		countRequests {
			Searchbar.editName(filter2)
		}.with {
			assert it.getRequestCount(RequestType.ALL) <= 2
			assert it.getRequestCount(RequestType.PREFERENCE_SAVE) == 1
		}
	}

	@Test
	void _15_selectEmpty() {
		countRequests {
			Searchbar.clearSearchfilter()
		}.with {
			assert it.getRequestCount(RequestType.ALL) <= 2
			it.getRequestCount(RequestType.PREFERENCE_DESELECT) == 1
		}
	}

	@Test
	void _20_selectFirstSearchfilter() {
		countRequests {
			Searchbar.selectSearchfilter(filter1)
		}.with {
			assert it.getRequestCount(RequestType.ALL) <= 2
			it.getRequestCount(RequestType.PREFERENCE_SELECT) == 1
		}
	}

	@Test
	void _22_reload() {
		countRequests {
			refresh()
		}.with {
			it.getRequestCount(RequestType.PREFERENCE_SELECT) == 0
		}

		assert Searchbar.selectedFilter == filter1
	}

	@Test
	void _25_editSearchfilter() {
		countRequests {
			Searchbar.selectAttribute('Order number')
			new Searchtemplate.SearchTemplateItem(
					name: 'orderNumber',
					operator: '<=',
					value: '100'
			)
		}.with {
			assert it.getRequestCount(RequestType.ALL) <= 2
		}
	}

	@Test
	void _30_deleteSearchfilter() {
		countRequests {
			Searchbar.delete()
		}.with {
			assert it.getRequestCount(RequestType.ALL) <= 5 // 3 + (optional save: 2)
			assert it.getRequestCount(RequestType.PREFERENCE_DELETE) == 1
		}

		assert !Searchbar.attributeLov
	}

	@Test
	void _35_deleteLastSearchfilter() {
		Searchbar.selectSearchfilter(filter2)
		countRequests {
			Searchbar.delete()
		}.with {
			assert it.getRequestCount(RequestType.ALL) <= 3
			assert it.getRequestCount(RequestType.PREFERENCE_DELETE) == 1
		}
	}
}
