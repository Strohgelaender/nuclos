package org.nuclos.test.webclient.entityobject

import javax.ws.rs.core.Response

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.request.RequestCounts
import org.nuclos.test.rest.request.RequestType
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubsubformTest extends AbstractWebclientTest {

	@Test
	void _00_setup() {
		LocaleChooser.locale = Locale.ENGLISH
		RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)

		EntityObject<Long> parent = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		parent.setAttribute('text', 'parent')

		List<EntityObject<Long>> subform = parent.getDependents(
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
				'parent'
		)

		3.times { subCount ->
			EntityObject subEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM)
			subEo.setAttribute('text', subCount.toString())
			subform << subEo

			List<EntityObject<Long>> subsubform = subEo.getDependents(
					TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
					'subform'
			)
			3.times { subsubCount ->
				EntityObject subsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM)
				subsubEo.setAttribute('text', "$subCount-$subsubCount".toString())
				subsubform << subsubEo

				List<EntityObject<Long>> subsubsubform = subsubEo.getDependents(
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM,
						'subsubform'
				)
				3.times { subsubsubCount ->
					EntityObject subsubsubEo = new EntityObject(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM)
					subsubsubEo.setAttribute('text', "$subCount-$subsubCount-$subsubsubCount".toString())
					subsubsubform << subsubsubEo
				}
			}
		}

		nuclosSession.save(parent)

		subform = nuclosSession.loadDependents(
				parent,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
				TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent'
		)

		List<EntityObject<Long>> subsubform = nuclosSession.loadDependents(
				subform.first(),
				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM,
				TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_subform'
		)

		assert subsubform.size() == 3

		testPermissions:
		{
			RESTClient readonlyClient = new RESTClient('readonly', 'readonly')
			readonlyClient.login()

			expectErrorStatus(Response.Status.FORBIDDEN) {
				readonlyClient.getEntityObject(
						TestEntities.NUCLET_TEST_SUBFORM_PARENT,
						parent.id
				)
			}

			expectErrorStatus(Response.Status.FORBIDDEN) {
				readonlyClient.loadDependents(
						parent,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent'
				)
			}

			expectErrorStatus(Response.Status.FORBIDDEN) {
				readonlyClient.loadDependentsRecursively(
						parent,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
				)
			}

			expectErrorStatus(Response.Status.FORBIDDEN) {
				readonlyClient.loadDependentsRecursively(
						parent,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
						subform.first().id.toString()
				)
			}

			expectErrorStatus(Response.Status.FORBIDDEN) {
				readonlyClient.loadDependentsRecursively(
						parent,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
						subform.first().id.toString(),
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_subform'
				)
			}
		}

		RESTClient testUser = new RESTClient('test', 'test')
		testUser.login()

		/**
		 * Only parent and subform are readable for Example User in status 20.
		 * Subsubform is forbidden.
		 * Subsubsubform should be transitively forbidden.
		 */
		testPermissionsStatus20:
		{
			parent.changeState(20)

			testUser.getEntityObject(
					TestEntities.NUCLET_TEST_SUBFORM_PARENT,
					parent.id
			)

			testUser.loadDependents(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent'
			)

			testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
			)

			assert testUser.loadDependentsRecursively(
					parent,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
					TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
					subform.first().id.toString()
			).first().entityClass == TestEntities.NUCLET_TEST_SUBFORM_SUBFORM

			// Directly forbidden via state model
			expectErrorStatus(Response.Status.FORBIDDEN) {
				testUser.loadDependentsRecursively(
						parent,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
						subform.first().id.toString(),
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_subform'
				)
			}

			// Directly forbidden via state model
			expectErrorStatus(Response.Status.FORBIDDEN) {
				testUser.loadDependentsRecursively(
						parent,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
						subform.first().id.toString(),
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_subform',
						subsubform.first().id.toString()
				)
			}

			// Forbidden, because parent subform is forbidden via state model
			expectErrorStatus(Response.Status.FORBIDDEN) {
				testUser.loadDependentsRecursively(
						parent,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM,
						TestEntities.NUCLET_TEST_SUBFORM_SUBFORM.fqn + '_parent',
						subform.first().id.toString(),
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM.fqn + '_subform',
						subsubform.first().id.toString(),
						TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM.fqn + '_subsubform'
				)
			}
		}

		parent.changeState(10)
	}

	@Test
	void _10_editSubsubform() {
		EntityObjectComponent eo
		assert1SubformRequest {
			eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_SUBFORM_PARENT)
		}

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBFORM, 'parent')
		Subform sub2form = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBFORM, 'subform')
		Subform sub3form = eo.getSubform(TestEntities.NUCLET_TEST_SUBFORM_SUBSUBSUBFORM, 'subsubform')

		assert subform.rowCount == 3
		assert sub2form.rowCount == 0
		assert sub3form.rowCount == 0

		assert !sub2form.newEnabled
		assert !sub3form.newEnabled

		assert1SubformRequest {
			subform.toggleSelection(0)
		}

		assert sub2form.rowCount == 3

		assertNoMultiEditing:
		{
			assertNoSubformRequest {
				// No multi-editing supported yet
				subform.toggleSelection(1)
				assert sub2form.rowCount == 0
				subform.toggleSelection(1)
			}
		}

		update:
		{
			// TODO
		}

		delete:
		{
			assert1SubformRequest {
				sub2form.toggleSelection(0)
			}
			sub2form.deleteSelectedRows()

			assertNoSubformRequest {
				// Highest level subform will be loaded
				eo.save()
			}

			// Foreign key violation, because recursive deletion does not work (yet)
			assertMessageModalAndConfirm('Error', 'Subsubform')

			// Cancel to reset the delete-flag for sub2form
			assertNoSubformRequest {
				eo.cancel()
			}

			deleteSub3formEntries:
			{
				assertNoSubformRequest {
					subform.getRow(0).selected = true
				}
				assertNoSubformRequest {
					sub2form.getRow(0).selected = true
				}

				assertNoSubformRequest {
					(0..sub3form.rowCount - 1).each {
						sub3form.getRow(it).selected = true
					}
					sub3form.deleteSelectedRows()
				}

				assert1SubformRequest {
					eo.save()
				}
			}

			deleteSub2formEntries:
			{
				assert1SubformRequest {
					subform.getRow(0).selected = true
				}
				assert1SubformRequest {
					sub2form.getRow(0).selected = true
					sub2form.deleteSelectedRows()
				}

				assert1SubformRequest {
					eo.save()
				}
			}

			assert !eo.dirty

			assert1SubformRequest {
				subform.getRow(0).selected = true
			}

			// Selection is reset after saving
			assert sub2form.rowCount == 2
		}

		create:
		{
			Subform.Row row = subform.newRow()
			row.enterValue('text', '3')
			eo.save()

			assert subform.rowCount == 4
		}

		createWithDependents:
		{
			assertNoSubformRequest {
				Subform.Row row = subform.newRow()
				row.enterValue('text', '4')
				row.selected = true

				assert sub2form.rowCount == 0
				assert sub2form.newEnabled
				assert !sub3form.newEnabled

				Subform.Row sub2row = sub2form.newRow()
				sub2row.enterValue('text', '4-0')
				sub2row.selected = true

				assert sub3form.rowCount == 0
				assert sub2form.newEnabled
				assert sub3form.newEnabled

				Subform.Row sub3row = sub3form.newRow()
				sub3row.enterValue('text', '4-0-0')
			}

			assert1SubformRequest {
				eo.save()
			}

			assert1SubformRequest {
				subform.getRow(0).selected = true
			}
			assert1SubformRequest {
				sub2form.getRow(0).selected = true
			}

			Subform.Row sub3row = sub3form.getRow(0)

			assert sub3row.getValue('text') == '4-0-0'
		}
	}

	private void assertNoSubformRequest(Closure<?> c) {
		assertSubformRequests(c, 0)
	}

	private void assert1SubformRequest(Closure<?> c) {
		assertSubformRequests(c, 1)
	}

	private void assertSubformRequests(Closure<?> c, int count) {
		RequestCounts counts = countRequests(c)
		assert counts.getRequestCount(RequestType.EO_SUBFORM) == count
	}

}
