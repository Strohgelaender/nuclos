package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PrintoutTest extends AbstractWebclientTest {

	static WebElement findElementContainingText(String cssSelector, String text) {
		return $$(cssSelector).find { it.text == text }
	}
	
	static void selectDropdownValue(String value) {
		$('.modal .ui-autocomplete-dropdown').click()
		findElementContainingText('.modal .ui-autocomplete-list li span', value).click()
	}

	
	@Test
	void _0setupBos() {
		
		assert $('#logout')
		TestDataHelper.insertTestData(nuclosSession)

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
	}
	
	
	@Test
	void _1executePrintout() {
		
		// open printout dialog
		$('nuc-printout button').click()
		waitForAngularRequestsToFinish()
		
		// select first printout
		$$('.output-format-checkbox')[0].click()
		waitForAngularRequestsToFinish()
		
		// set datasource parameter
		selectDropdownValue('Software')
		screenshot('printout-selected')

		// execute printout
		$('#execute-printout-button').click()		
		screenshot('printout-executed')

		assert $$('.printout-downoload-link').size() == 1
		
		// click download link
		$$('.printout-downoload-link')[0].click()
		screenshot('printout-download')

	}

}
