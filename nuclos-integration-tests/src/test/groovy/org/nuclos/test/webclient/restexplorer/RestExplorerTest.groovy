package org.nuclos.test.webclient.restexplorer

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.webclient.AbstractWebclientTest

import groovy.transform.CompileStatic

@Ignore
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RestExplorerTest extends AbstractWebclientTest {
	@Test
	void _00_setup() {
		logout()
		login('nuclos')
	}

	@Test
	void _01_navigateToRestExplorer() {

		getUrlHash('#/restexplorer')

		// assert redirect to first entry
		assert driver.currentUrl.indexOf('#/restexplorer/0') != -1

		// execute first REST service
		$('#execute-rest-service').click()

		// check if response contains username attribute
		assert $$('.key').find { it.text != null && it.text.indexOf("username") != -1 }.text == "\"username\":"
	}
}
