package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * search references in new window
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LockTest extends AbstractWebclientTest {

	@Test
	void _00createTestUser() {
		RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
	}

	@Test
	void _02setupData() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	/**
	 * Test correct subform behavior when data is reloaded
	 */
	@Test
	void _04testSubform() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)
		assert row.getValue('article') == '1004 Mouse'

		row.clickCell('category')
		ListOfValues lov = row.getLOV('category')
		assert lov.open
		assert !lov.textValue
		assert lov.choices.size() > 1

		lov.selectEntry('Hardware')

		row.clickCell('category')
		lov = row.getLOV('category')
		assert lov.open
		assert lov.textValue == 'Hardware'

		assert row.getValue('article') == ''

		eo.cancel()

		subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		row = subform.getRow(0)

		assert row.getValue('article') == '1004 Mouse'
	}

	@Test
	void _06lock() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		checkForEnabled(eo, true, false)

		eo.clickButton('Lock')

		screenshot('afterLock')

		assert $('#lock-info')
	}

	@Test
	void _08checkLock() {

		assert logout()

		screenshot('afterLogOut')

		assert login('nuclos', '')

		screenshot('afterLogInAsNuclos')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		screenshot('afterOpenOrder')

		// This a fallback to a webclient bug: Sometimes the entity just doesn't open and you have to refresh
		if (!($('#lock-info'))) {
			Sidebar.refresh()
			screenshot('afterRefresh')
			eo = EntityObjectComponent.forDetail()
		}

		assert $('#lock-info')

		eo.checkField('orderNumber', true, false)

		String orderNumber = eo.getAttribute('orderNumber')
		assert orderNumber != null && !orderNumber.empty

		boolean exception = false
		try {
			eo.setAttribute('orderNumber', '99999')
		} catch (Exception e) {
			exception = true
		}
		assert exception

		assert !eo.dirty

		// no change
		assert orderNumber == eo.getAttribute('orderNumber')

		// no delete button
		assert eo.getDeleteButton() == null

		// no state change
		assert !eo.hasNextStateButton()

	}

	@Test
	void _10checkLockedUnlockedToggle() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		checkForEnabled(eo, false, true)

		Sidebar.selectEntry(1)

		checkForEnabled(eo, true, true)

		// FIXME selectEntry doesn't work reliable here - click in browser works
		// Sidebar.selectEntry(0)
		Sidebar.selectEntryByText('10020158')

		checkForEnabled(eo, false, true)

	}

	@Test
	void _12checkUnLock() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		WebElement we = $('#unlock-button')
		assert we != null
		assert we.isEnabled()

		we.click()

		screenshot('afterUnlocking')

		assert !$('#lock-info')

		// delete button
		assert eo.getDeleteButton() != null

		// state change
		assert eo.hasNextStateButton()

		def newOrderNumber = '999'
		eo.setAttribute('orderNumber', newOrderNumber)

		// change
		assert newOrderNumber == eo.getAttribute('orderNumber')

		Subform subform = eo.getSubform(position)
		Subform.Row row = subform.getRow(0)
		String newArticle = '1001 Nuclos'

		row.clickCell('article')
		ListOfValues lov = row.getLOV('article')
		assert lov.open

		lov.selectEntry(newArticle)

		eo.save()

		assert newOrderNumber == eo.getAttribute('orderNumber')

		assert row.getValue('article') == '1001 Nuclos'

	}

	String goDocumentBoMetaId = 'org_nuclos_businessentity_nuclosgeneralsearchdocument_genericObject'
	String tabGoDocument = "Documents"

	String position = 'example_rest_OrderPosition_order';
	String tabPosition = "Position"

	void checkForEnabled(EntityObjectComponent eo, boolean enabled, boolean superUser) {

		screenshot('beforeCheckForEnabled')

		assert eo.getNewButton() != null

		//Delete and Next State Button
		if (enabled) {
			assert eo.getDeleteButton() != null
			assert eo.hasNextStateButton()

		} else {
			assert eo.getDeleteButton() == null
			assert !eo.hasNextStateButton()

		}

		eo.checkField('nuclosProcess', true, enabled)
		eo.checkField('customer', true, enabled)

		eo.checkField('orderDate', true, enabled)
		eo.checkField('discount', true, enabled)

		eo.checkField('confirmation', true, enabled) // Dokument
		eo.checkField('note', true, enabled)
		eo.checkField('approvedcreditlimit', true, enabled && superUser)

		assert eo.hasTab(tabPosition)
		assert eo.hasTab(tabGoDocument)

		Subform subform = eo.getSubform(position)
		assert subform != null

		assert subform.newVisible == enabled
		assert subform.cloneVisible == enabled
		assert subform.deleteVisible == enabled

		Subform.Row row = subform.getRow(0)

		assert !row.isDirty()

		String newArticle = '1001 Nuclos'
		def oldArticle = row.getValue('article')

		row.clickCell('article')
		ListOfValues lov = row.getLOV('article')
		assert lov.open == enabled

		if (enabled) {
			lov.selectEntry(newArticle)
		}

		def oldQuantity = row.getValue('quantity')
		row.enterValue('quantity', '99')

		screenshot('changedQuantity')

		if (enabled) {
			//cancel and save button
			assert eo.getCancelButton() != null
			assert eo.getSaveButton() != null

			assert row.isDirty()
			assert row.getValue('article') == newArticle

			String val = row.getValue('quantity')
			assert val == '99,00' || val == '99.00' || val == '99'

			eo.getTab(tabGoDocument).click()
			screenshot('changedToDocumentTab')
			if (superUser) {
				assert eo.getSubform(goDocumentBoMetaId) != null
			} else {
				assert eo.getSubform(goDocumentBoMetaId) == null
			}

			eo.getTab(tabPosition).click()
			screenshot('changedBackToPositionTab')

			subform = eo.getSubform(position);
			assert subform.newVisible == enabled

			row = subform.getRow(0)

			screenshot('gotFirstRowFromPositionTab')

			assert row.isDirty()
			assert row.getValue('article') == newArticle

			val = row.getValue('quantity')
			assert val == '99,00' || val == '99.00' || val == '99'

			eo.cancel()
		}

		row = subform.getRow(0)

		assert !row.isDirty()

		assert row.getValue('article') == oldArticle
		assert row.getValue('quantity') == oldQuantity

	}

}
