package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.StateComponent

import groovy.transform.CompileStatic

/**
 * Tests state changes via the state-component.
 *
 * TODO: Also test numerals
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class StateChangeTest extends AbstractWebclientTest {


	@Test
	void _01_setup() {
		LocaleChooser.locale = Locale.GERMANY

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTSTATECHANGE)
		eo.addNew()

		assert !StateComponent.currentState
		assert !StateComponent.nextStates

		// Assert all state change buttons in the layout are disabled
		def buttons = StateComponent.stateButtons
		assert buttons*.label.toSet() == ['Status A', 'Status B', 'Status C', 'Status ☠', 'Status D', 'Status E'].toSet()
		assert !buttons.find { it.enabled }

		eo.setAttribute('name', 'test')
		eo.save()
	}

	@Test
	void _05_assertStateA() {
		// Should be in state A initially
		assertStateA()
	}

	@Test
	void _07_changeToStateX() {
		StateComponent.changeState('Status ☠')
		StateComponent.confirmStateChange()

		assertStateX()
	}

	@Test
	void _08_changeToStateA() {
		StateComponent.changeState('Auf Initialstatus setzen')
		StateComponent.confirmStateChange()

		assertStateA()
	}

	@Test
	void _10_changeToStateB() {
		StateComponent.changeState('Status B')
		StateComponent.confirmStateChange()

		assertStateB()
	}

	@Test
	void _15_changeToStateC() {
		StateComponent.changeState('Status C')
		StateComponent.confirmStateChange()

		// State is automatically changed from 30 to 40 (by a server rule)
		assertStateD()
	}

	@Test
	void _25_changeToStateA() {
		// Perform the state change to state 10 (this is a "nonstop" state change)
		StateComponent.changeState('Auf Initialstatus setzen')

		_05_assertStateA()
	}

	@Test
	void _30_changeToStateB() {
		_10_changeToStateB()
	}

	@Test
	void _35_changeToStateD() {
		StateComponent.changeState('Status D')
		StateComponent.confirmStateChange()

		assertStateD()
	}

	/**
	 * Does another cycle of state changes only via layout buttons.
	 * State changes via layout button do not show confirmation dialogs.
	 */
	@Test
	void _40_changeToStateViaButtons() {
		StateComponent.stateButtons.find { it.label == 'Status A' }.click()

		assertStateA()

		StateComponent.stateButtons.find { it.label == 'Status B' }.click()

		assertStateB()

		StateComponent.stateButtons.find { it.label == 'Status C' }.click()

		assertStateD()
	}

	@Test
	void _45_cancelStateChange() {
		StateComponent.changeState('Status E')
		StateComponent.cancelStateChange()

		assertStateD()
	}

	@Test
	void _99_changeToStateE() {
		StateComponent.changeState('Status E')
		StateComponent.confirmStateChange()

		assertStateE()
	}

	// Some helper methods which contain only assertions:

	private void assertStateA() {
		assert StateComponent.currentState == 'Status A'
		assert StateComponent.nextStates == ['Status ☠', 'Status B']    // Sorted by numeral

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert buttons.findAll { it.enabled }*.label.toSet() == ['Status B', 'Status ☠'].toSet()
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status A', 'Status C', 'Status D', 'Status E'].toSet()
		assert getActiveTabName() == 'Reiter A'
		assert getActiveTabContent() == 'Pane A'
	}

	private void assertStateB() {
		assert StateComponent.currentState == 'Status B'
		assert StateComponent.nextStates == ['Status C', 'Status D']

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert buttons.findAll { it.enabled }*.label.toSet() == ['Status C', 'Status D'].toSet()
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status A', 'Status B', 'Status ☠', 'Status E'].toSet()
		assert $('.tab-title.active').text == 'Reiter B'
		assert getActiveTabContent() == 'Pane B'
	}

	private void assertStateD() {
		assert StateComponent.currentState == 'Status D'
		assert StateComponent.nextStates == ['Auf Initialstatus setzen', 'Status E']

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert buttons.findAll { it.enabled }*.label.toSet() == ['Status A', 'Status E'].toSet()
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status B', 'Status C', 'Status ☠', 'Status D'].toSet()
		assert $('.tab-title.active').text == 'Reiter D'
		assert getActiveTabContent() == 'Pane D'
	}

	private void assertStateE() {
		assert StateComponent.currentState == 'Status E'
		assert !StateComponent.nextStates

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert !buttons.find { it.enabled }
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status A', 'Status B', 'Status C', 'Status ☠', 'Status D', 'Status E'].toSet()
		assert $('.tab-title.active').text == 'Reiter E'
		assert getActiveTabContent() == 'Pane E'
	}

	private void assertStateX() {
		assert StateComponent.currentState == 'Status ☠'
		assert StateComponent.nextStates == ['Auf Initialstatus setzen']

		// Assert the state change buttons correctly enabled/disabled
		def buttons = StateComponent.stateButtons
		assert buttons.findAll { it.enabled }*.label == ['Status A']
		assert buttons.findAll { !it.enabled }*.label.toSet() == ['Status B', 'Status C', 'Status ☠', 'Status D', 'Status E'].toSet()
	}

	private String getActiveTabContent() {
		$('nuc-web-tabcontainer .tab-content').text
	}

	private String getActiveTabName() {
		$('.tab-title.active').text
	}
}
