package org.nuclos.test

import org.junit.Test

import groovy.json.JsonSlurper
import groovy.transform.CompileStatic;


/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObjectTest {

	@Test
	void serializeWithDependents() {
		EntityObject<Long> eo = new EntityObject<Long>(TestEntities.EXAMPLE_REST_CUSTOMER)
		List<EntityObject<?>> dependents = eo.getDependents(
				TestEntities.EXAMPLE_REST_CUSTOMERADDRESS ,
				'customer'
		)

		dependents.add(
				new EntityObject(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS).with {
					setAttribute('street', 'Street 1')
					setAttribute('city', 'City 2')
					setAttribute('zipCode', 'Zip Code 3')
					return it
				}
		)

		String json = eo.toJson()


		def jsonSlurper = new JsonSlurper()
		def object = jsonSlurper.parseText(json)

		assert object instanceof Map
		assert object['subBos'] instanceof Map
		assert object['subBos']['insert']
		assert object['subBos']['update']
		assert object['subBos']['delete']

		println json
	}
}