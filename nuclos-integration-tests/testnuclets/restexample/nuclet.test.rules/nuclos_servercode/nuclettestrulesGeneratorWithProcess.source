package nuclet.test.rules;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.GenerateContext;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;

/**
 * @name
 * @description
 * @usage
 * @change
 */
@Rule(name = "Generator With Process", description = "Generator With Process")
public class GeneratorWithProcess implements GenerateRule, GenerateFinalRule, InsertRule, InsertFinalRule {


	@Override
	public void insert(final InsertContext context) throws BusinessException {
		checkProcessAfterGeneration(context.getBusinessObject(TestGeneratorWithProcess.class));
	}

	@Override
	public void insertFinal(final InsertContext context) throws BusinessException {
		checkProcessAfterGeneration(context.getBusinessObject(TestGeneratorWithProcess.class));
	}

	public void generate(GenerateContext context) throws BusinessException {
		TestGeneratorWithProcess target = context.getTargetObject(TestGeneratorWithProcess.class);
		target.setName("generated " + System.currentTimeMillis());

		checkProcessAfterGeneration(target);
	}

	@Override
	public void generateFinal(final GenerateContext context) throws BusinessException {
		checkProcessAfterGeneration(context.getTargetObject(TestGeneratorWithProcess.class));
	}

	private void checkProcessAfterGeneration(final TestGeneratorWithProcess eo) throws BusinessException {
		if (StringUtils.contains(eo.getName(), "generated")
				&& !isGeneratorProcess2(eo)) {
			throw new BusinessException("Wrong process - the object generator should have set 'Generator Process 2'");
		}
	}

	private boolean isGeneratorProcess2(final TestGeneratorWithProcess eo) {
		return TestGeneratorWithProcess.GeneratorProcess2.getId().equals(eo.getNuclosProcessId())
				&& "Generator Process 2".equals(eo.getNuclosProcess());
	}
}
