import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestAddonService } from './test-addon.service';
import { TestAddonComponent } from './test-addon.component';

export function TestAddonServiceFactory() {
	return new TestAddonService();
}

@NgModule({
	imports: [
		CommonModule
	],
	providers: [
		{ provide: TestAddonService, useFactory: TestAddonServiceFactory }
	],
	declarations: [
		TestAddonComponent
	],
	exports: [
		TestAddonComponent
	],
	entryComponents: [
		TestAddonComponent
	]
})
export class TestAddonModule { }
