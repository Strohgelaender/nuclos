import { Component, Inject, Injectable, Input, OnDestroy, OnInit } from '@angular/core';

import { TestAddonService } from './test-addon.service';
import { IEntityObject, ResultlistContext, RESULTLIST_CONTEXT, LayoutContext, LAYOUT_CONTEXT } from '@nuclos/nuclos-addon-api';
import { Subscription } from 'rxjs/Subscription';

@Component({
	selector: 'nuc-addon-test-addon',
	styles: [`
        :host {
			display: block;
			width: 100%;
			height: 100%;
			border: 1px dashed lightskyblue;
			text-align: center;
        }
	`],
	template: `
		<div>
			Addon TestAddon works.
			<br>
			<div *ngIf="eo">
				selected item: {{eo.getId()}}
			</div>
		</div>
	`
})
export class TestAddonComponent implements OnInit, OnDestroy {


	private subscriptions: Subscription[] = [];

	private eo: IEntityObject;

	constructor(
		// @Inject(LAYOUT_CONTEXT) private layoutContext: LayoutContext,
		// @Inject(RESULTLIST_CONTEXT) private resultlistContext: ResultlistContext,
		private service: TestAddonService
	) {
	}

	ngOnInit() {
		// get addon property for layout adddon
		// let someAddonProperty = this.layoutContext.getAddonProperty('someAddonProperty');

		/*
		// applies for layout addons
		if (this.layoutContext) {
			this.subscriptions.push(
				this.layoutContext.onEoSelection().subscribe(
					(eo: IEntityObject) => {
						if (eo) {
							this.eo = eo;
						}
					}
				)
			);
		}
		*/
	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

}
