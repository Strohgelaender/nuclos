#!/bin/bash
# imports all nuclets in directory testnuclets into DB


export NUCLOS_URL=${1:-'http://127.0.0.1:8080/nuclos-war'}
export NUCLOS_USER=nuclos
export NUCLOS_PASSWORD=

echo "Importing test-nuclets  to $NUCLOS_URL ..."

# directory where this script is stored
export DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

curl --cookie-jar $DIR/cookies.txt $NUCLOS_URL/rest -X POST -H "Accept:application/json" -H "Content-Type: application/json" -d '{"username":"'$NUCLOS_USER'", "password":"'$NUCLOS_PASSWORD'"}'

# set data languages
curl --fail --cookie $DIR/cookies.txt -X POST "$NUCLOS_URL/rest/maintenance/managementconsole/setDataLanguage/de_DE(primary)%20en_GB"

# import all nuclets inside 'testnuclets'
for dir in $DIR/*/
do
    nucletdir=${dir}
    printf "\n\nImporting nuclet ${nucletdir}"
    
	# zip nuclet
	rm $DIR/nucletimport.nuclet
	cd ${nucletdir}
	zip -r $DIR/nucletimport.nuclet .
	
	pwd
	# send zipped nuclet to server
	curl --fail --cookie $DIR/cookies.txt -F "file=@$DIR/nucletimport.nuclet" "$NUCLOS_URL/rest/maintenance/nucletimport"
	
	
	# exit if curl was not successful
	if [ $? == "0" ]
	then
		echo "Import was successful"
	else
		echo "ERROR Import was not successful: $?"
		exit 1
	fi

	echo "Check server ready state..."
	# initial wait time:
	sleep 20

	while [ "$SERVERSTATUS" != "{\"ready\":true}" ]; do
		SERVERSTATUS=$(curl --silent --fail --cookie $DIR/cookies.txt "$NUCLOS_URL/rest/serverstatus" -H 'Content-Type: application/json' -H 'Accept: application/json, text/plain, */*')
		sleep 5
	done

	echo "Server is ready now"
	
done
