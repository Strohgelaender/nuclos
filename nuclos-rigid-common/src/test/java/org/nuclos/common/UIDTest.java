package org.nuclos.common;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class UIDTest {
	@Test
	public void testParseAllUIDs() {
		assert UID.parseAllUIDs(null).isEmpty();
		assert UID.parseAllUIDs("").isEmpty();
		assert UID.parseAllUIDs("asd{jkds f} uid $ksdfo").isEmpty();
		assert UID.parseAllUIDs("uid{ZPCHtBMWVQ5tPkeTn5q9}").equals(Arrays.asList(new UID("ZPCHtBMWVQ5tPkeTn5q9")));
		assert UID.parseAllUIDs("uid{ZPCHtBMWVQ5tPkeTn5q9} - uid{YqLXE6Zgm0ytG1IRAFmH} uid{ltl8heqh4zMQVeQlojpv}")
				.equals(Arrays.asList(new UID("ZPCHtBMWVQ5tPkeTn5q9"), new UID("YqLXE6Zgm0ytG1IRAFmH"), new UID("ltl8heqh4zMQVeQlojpv")));

		assert UID.parseAllUIDs("uid{Khi5.SrOmcSSkwbY-04ujXeBC} uid{Khi5.YxQwf-cgZ1j2XGAOo43QwW1ii9}")
				.equals(Arrays.asList(new UID("SrOmcSSkwbY-04ujXeBC"), new UID("YxQwf-cgZ1j2XGAOo43QwW1ii9")));
	}
}