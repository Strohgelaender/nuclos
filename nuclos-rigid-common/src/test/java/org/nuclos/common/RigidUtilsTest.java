package org.nuclos.common;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class RigidUtilsTest {
	@Test
	public void isNuclosStackFrame() {
		assert RigidUtils.isNuclosStackFrame("org.nuclos.Test");
		assert RigidUtils.isNuclosStackFrame("at org.nuclos.Test");
		assert RigidUtils.isNuclosStackFrame("\tat org.nuclos.Test");

		assert !RigidUtils.isNuclosStackFrame("sun.something");
		assert !RigidUtils.isNuclosStackFrame("at sun.something");
		assert !RigidUtils.isNuclosStackFrame("\tat sun.something");
	}
}