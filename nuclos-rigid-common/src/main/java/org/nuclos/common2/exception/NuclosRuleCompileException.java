//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.exception;

/**
 * A more specific variant of {@link NuclosCompileException} indicating
 * that a (user provided) business rule has failed to compile.
 * <p>
 * This is in contrast to compilation problems in the (auto generated)
 * BO classes.
 * <p>
 * Formerly, this kind of problem has been indicated with (the now 
 * more specific) org.nuclos.server.ruleengine.NuclosBusinessRuleException.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.3.0
 */
public class NuclosRuleCompileException extends NuclosCompileException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7924035469070607369L;

	public NuclosRuleCompileException(Throwable exception) {
		super(exception);
	}

}
