//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

import java.util.Iterator;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.FieldRefIterator;
import org.nuclos.common.dblayer.IFieldRef;

/**
 * Parser for the expression language used in field references.
 *  
 * @author Thomas Pasch
 * @since Nuclos 3.2.01
 * @see org.nuclos.common.FieldMeta#getForeignEntityField()
 * @deprecated use ForeignEntityFieldUIDParser
 */
@Deprecated
public class ForeignEntityFieldParser implements Iterable<IFieldRef> {
	
	private static final Logger LOG = Logger.getLogger(ForeignEntityFieldParser.class);
	
	private static final Pattern OLD_SIMPLE_REF_PATTERN = Pattern.compile("\\p{Alpha}[\\p{Alnum}_]*");
	
	private static final String DEFAULT_FOREIGN_ENTITY_FIELD = "${name}";
	
	private static final Pattern REF_PATTERN = Pattern.compile("\\$\\{(\\p{Alpha}[\\p{Alnum}_]*)\\}", Pattern.MULTILINE);
	
	// 
	
	private String ffieldname;
	
	public ForeignEntityFieldParser(FieldMeta<?> foreignEntityField, final boolean bSearch) {
		if (foreignEntityField == null) {
			throw new NullPointerException();
		}
		// this.foreignEntityField = foreignEntityField;
		
		String fef = null;
		if (bSearch && !RigidUtils.isNullOrEmpty(foreignEntityField.getSearchField()))
			fef = foreignEntityField.getSearchField();
			
		if (fef == null) {
			if (foreignEntityField.getForeignEntity() != null)
				fef = foreignEntityField.getForeignEntityField();
			else if (foreignEntityField.getLookupEntity() != null)
				fef = foreignEntityField.getLookupEntityField();
			else if (foreignEntityField.getUnreferencedForeignEntity() != null)
				fef = foreignEntityField.getUnreferencedForeignEntityField();
		}
		
		init(fef);
	}
	
	public ForeignEntityFieldParser(String sRefDefinition) {
		init(sRefDefinition);
	}
	
	private void init(String sRefDefinition) {
		if (sRefDefinition == null) {
			LOG.debug("Null/empty foreignEntityField in expression in " + sRefDefinition 
					+ " is deprecated, use " + DEFAULT_FOREIGN_ENTITY_FIELD + " instead!");
			this.ffieldname = DEFAULT_FOREIGN_ENTITY_FIELD;
		}
		else if (OLD_SIMPLE_REF_PATTERN.matcher(sRefDefinition).matches()) {
			LOG.debug("Old style foreignEntityField expression '" + sRefDefinition + "' in "
					+ sRefDefinition + " is deprecated, please enclose it: ${" + sRefDefinition + "}");
			this.ffieldname = "${" + sRefDefinition + "}";
		}
		else {
			this.ffieldname = sRefDefinition;
		}
	}

	@Override
	public Iterator<IFieldRef> iterator() {
		return new FieldRefIterator(REF_PATTERN, ffieldname);
	}

	public static void main(String[] args) {
		// final String test = "prefix${a}${test} -\n ${main} hiho ${error} ->";
		// final String test = "${a}$b{";
		final String test = "test";
		System.out.println("Parse: '" + test + "'");
		final FieldMetaVO<?> field = new FieldMetaVO();
		field.setForeignEntityField(test);
		field.setForeignEntity(new UID("foreignEntity"));
		field.setFieldName("fieldRefToForeignEntityField");
		for (IFieldRef r: new ForeignEntityFieldParser(field, false)) {
			System.out.println(r);
		}
		System.out.println("ForeignEntityFieldParser main ended");
	}

}
