package org.nuclos.common2;

import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLOutputFactory;

import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.mapper.Mapper;
import com.thoughtworks.xstream.mapper.MapperWrapper;

/**
 * Singleton for retrieving instances of XStream.
 * <p>
 * Nuclos uses a XStream factory due to the following reasons:
 * <ul>
 * 		<li>Central place for configuration of XStream. (E.g. this ensures that
 * 			all our XStream uses the StaxDriver.)
 * 		<li>Central place for re-mapping interface classes back to default 
 * 			implementations (see {@link #defaultImplementation} for details).
 * 		<li>Using a XStream instance pool for maximal performance
 * 		<li>Ensure that output is indented (pretty-printing) even if we use
 * 			the StaxDriver (see http://osdir.com/ml/java.xstream.user/2006-04/msg00091.html
 * 			for details).
 * </ul>
 * 
 * @author Thomas Pasch
 */
public class XStreamSupport {
	
	private static XStreamSupport INSTANCE;
	
	private static Map<Class<?>, Class<?>> defaultImplementation;
	
	private GenericObjectPool<XStream> pool;
	
	public XStreamSupport() {
		init();
		INSTANCE = this;
	}
	
	private final void init() {		
		defaultImplementation = new HashMap<Class<?>, Class<?>>();
		
		// pool
		pool = new GenericObjectPool<XStream>(new PoolableXStreamFactory());
		pool.setTestOnBorrow(false);
		pool.setTestOnReturn(false);
		pool.setTestWhileIdle(false);
		pool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_FAIL);
		pool.setMaxActive(10);
		pool.setMaxIdle(10);
		pool.setMinIdle(0);
		pool.setSoftMinEvictableIdleTimeMillis(-1);
		pool.setMinEvictableIdleTimeMillis(-1);
	}
	
	public static XStreamSupport getInstance() {
		return INSTANCE;
	}
	
	public static void addDefaultImplementation(Class<?> interfaceClass, Class<?> implementationClass) {
		defaultImplementation.put(interfaceClass, implementationClass);
	}

	/**
	 * Attention: The stream must be closed again. If you use the returned Closeable in a try-with-resources statement,
	 * it will be closed automatically.
	 */
	public CloseableXStream getCloseableStream() {
		return new CloseableXStream(pool);
	}

	/**
	 * This performs <em>very</em> badly!
	 * <p>
	 * This method is <em>never</em> needed if you (de)serialize to/from a String.
	 * </p>
	 * @deprecated Read http://stackoverflow.com/questions/1001899/xstream-fromxml-exception for alternatives.
	 */
	public XStream getXStreamUtf8() {
		// ???
		final XStream result = new XStream(new DomDriver("UTF-8")) {
		    protected MapperWrapper wrapMapper(MapperWrapper next) {
		        return new NuclosMapper(next);
		    }			
		};
		return result;
	}
	
	private class NuclosMapper extends MapperWrapper {
		
		public NuclosMapper(Mapper wrapped) {
			super(wrapped);
		}

		@Override
	    public Class<?> defaultImplementationOf(Class type) {
			Class<?> result = defaultImplementation.get(type);
			if (result == null) {
				result = super.defaultImplementationOf(type);
			}
			return result;
	    }

	}
	
	private class PoolableXStreamFactory implements PoolableObjectFactory<XStream> {
		
		private PoolableXStreamFactory() {
		}

		@Override
		public XStream makeObject() throws Exception {
			final XStream result = new XStream(new MyStaxDriver()) {
			    protected MapperWrapper wrapMapper(MapperWrapper next) {
			        return new NuclosMapper(next);
			    }
			};
			return result;
		}

		@Override
		public void destroyObject(XStream obj) throws Exception {
			// do nothing
		}

		@Override
		public boolean validateObject(XStream obj) {
			return true;
		}

		@Override
		public void activateObject(XStream obj) throws Exception {
			// do nothing
		}

		@Override
		public void passivateObject(XStream obj) throws Exception {
			// do nothing
		}
		
	}
	
	private class MyStaxDriver extends StaxDriver {

		@Override
	    protected XMLOutputFactory createOutputFactory() {
	        return new IndentingXMLOutputFactory();
	    }
	}

}
