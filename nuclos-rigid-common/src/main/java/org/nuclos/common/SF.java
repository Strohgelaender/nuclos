//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * @see org.nuclos.common.dal.vo.SystemFields
 */
public abstract class SF<T> implements DbField<T>, SFConstants, Serializable {
	
	private static final long serialVersionUID = -7343606680974137378L;

	private static final Logger LOG = Logger.getLogger(SF.class);

	public static final String LOCK_TABLE_NAME = "T_UD_LOCK";
	public static final String LOCK_OWNER_COLUMN_NAME = "STRUID_OWNER";
	public static final String LOCK_ENTITY_COLUMN_NAME = "STRUID_ENTITY";
	public static final String LOCK_ENTITYOBJECT_COLUMN_NAME = "INTID_ENTITYOBJECT";
	private static final String OWNER_CALC_FUNCTION = "(SELECT lock." + LOCK_OWNER_COLUMN_NAME +
													  " FROM " + LOCK_TABLE_NAME + " lock " + 
													  " WHERE lock." + LOCK_ENTITYOBJECT_COLUMN_NAME + " = {0}.INTID" + 
													  " AND lock." + LOCK_ENTITY_COLUMN_NAME + " = ''{1}'')";
	
	static {
		// Needed for NUCLOS-3021.
		try {
			final Class<?> e = Thread.currentThread().getContextClassLoader().loadClass("org.nuclos.common.E");
		} catch (ClassNotFoundException e) {
			LOG.warn("Unable to load org.nuclos.common.E: " + e);
		}
	}
	
	private static final UID UID_PROCESS = new UID("ui9l");
	private static final UID UID_STATE = new UID("U89N");
	private static final UID UID_RECORDGRANT = new UID("kvhh");
	private static final UID UID_MANDATOR = new UID("AExm");
	private static final UID UID_USER = new UID("dRxj");

	public static final Integer INTID_SCALE = 20;
	public static final Integer UID_SCALE = 128;
	public static final Integer NAME_SCALE = 255;
	public static final Integer VERSION_SCALE = 9;
	public static final Integer CANWRITE_SCALE = 1, CANSTATECHANGE_SCALE = 1, CANDELETE_SCALE = 1;
	
	private static final int ORDER_PK = -1;
	private static final int ORDER_OWNER = 65499;
	private static final int ORDER_MANDATOR = 65500;
	private static final int ORDER_STATEICON = 65501;
	private static final int ORDER_STATENUMBER = 65502;
	private static final int ORDER_STATE = 65503;
	private static final int ORDER_PROCESS = 65504;
	private static final int ORDER_SYSTEMIDENTIFIER = 65505;
	private static final int ORDER_LOGICALDELETED = 65506;
	private static final int ORDER_ORIGIN = 65507;
	private static final int ORDER_ORIGINUID = 65508;
	private static final int ORDER_IMPORTVERSION = 65509;
	private static final int ORDER_CREATEDAT = 65510;
	private static final int ORDER_CREATEDBY = 65511;
	private static final int ORDER_CHANGEDAT = 65512;
	private static final int ORDER_CHANGEDBY = 65513;
	private static final int ORDER_VERSION = 65514;
	private static final int ORDER_CANWRITE = 65515;
	private static final int ORDER_CANSTATECHANGE = 65516;
	private static final int ORDER_CANDELETE = 65517;
	
	public static final Integer PK_ID_POSTFIX = 0;
	
	public static final SF<UID> PK_UID = new SF<UID>(PK_ID_POSTFIX, "primaryKey", "STRUID", false, UID.class, UID_SCALE, ORDER_PK) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createPrimaryKey();}
	};
	public static final SF<Long> PK_ID = new SF<Long>(PK_ID_POSTFIX, "primaryKey", "INTID", false, Long.class, INTID_SCALE, ORDER_PK) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createPrimaryKey();}
	};
	public static final SFValueable<InternalTimestamp> CREATEDAT = new SFValueable<InternalTimestamp>(1, "createdAt", "DATCREATED", false, InternalTimestamp.class, null, ORDER_CREATEDAT) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createCreatedAt();}
	};
	public static final SFValueable<String> CREATEDBY = new SFValueable<String>(2, "createdBy", "STRCREATED", false, String.class, NAME_SCALE, ORDER_CREATEDBY) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createCreatedBy();}
	};
	public static final SFValueable<InternalTimestamp> CHANGEDAT = new SFValueable<InternalTimestamp>(3, "changedAt", "DATCHANGED", false, InternalTimestamp.class, null, ORDER_CHANGEDAT) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createChangedAt();}
	};
	public static final SFValueable<String> CHANGEDBY = new SFValueable<String>(4, "changedBy", "STRCHANGED", false, String.class, NAME_SCALE, ORDER_CHANGEDBY) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createChangedBy();}
	};
	public static final SFValueable<Integer> VERSION = new SFValueable<Integer>(5, "version", "INTVERSION", false, Integer.class, VERSION_SCALE, ORDER_VERSION) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createVersion();}
	};
	public static final SFValueable<String> STATE = new SFValueable<String>(6, "nuclosState", "STRVALUE_NUCLOSSTATE", true, String.class, 255, ORDER_STATE) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createState();}
	};
	public static final SFValueable<Integer> STATENUMBER = new SFValueable<Integer>(7, "nuclosStateNumber", "INTVALUE_NUCLOSSTATE", true, Integer.class, 3, ORDER_STATENUMBER) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createStateNumber();}
	};
	public static final SFValueable<NuclosImage> STATEICON = new SFValueable<NuclosImage>(8, "nuclosStateIcon", "OBJVALUE_NUCLOSSTATE", false, NuclosImage.class, 255, ORDER_STATEICON) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createStateIcon();}
	};
	public static final SFValueable<String> SYSTEMIDENTIFIER = new SFValueable<String>(9, "nuclosSystemId", "STRNUCLOSSYSTEMID", false, String.class, 255, ORDER_SYSTEMIDENTIFIER) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createSystemIdentifier();}
	};
	public static final SFValueable<String> PROCESS = new SFValueable<String>(10, "nuclosProcess", "STRVALUE_NUCLOSPROCESS", false, String.class, 255, ORDER_PROCESS) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createProcess();}
	};
	public static final SFValueable<String> ORIGIN = new SFValueable<String>(11, "nuclosOrigin", "STRNUCLOSORIGIN", false, String.class, 255, ORDER_ORIGIN) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createOrigin();}
	};
	public static final SFValueable<Boolean> LOGICALDELETED = new SFValueable<Boolean>(12, "nuclosDeleted", "BLNNUCLOSDELETED", false, Boolean.class, null, ORDER_LOGICALDELETED) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createLogicalDeleted();}
	};
	public static final SFValueable<Integer> IMPORTVERSION = new SFValueable<Integer>(13, "nuclosImportVersion", "INTNUCLOSIMPORTVERSION", false, Integer.class, 9, ORDER_IMPORTVERSION) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createImportVersion();}
	};
	public static final SF<UID> ORIGINUID = new SF<UID>(14, "nuclosOriginUID", "STRNUCLOSORIGINUID", false, UID.class, UID_SCALE, ORDER_ORIGINUID) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createOriginUID();}
	};
	public static final SFValueable<Object> CANWRITE = new SFValueable<Object>(15, "canWrite", "CANWRITE", false, Object.class, CANWRITE_SCALE, ORDER_CANWRITE) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createCanwrite();}
	};
	public static final SFValueable<Object> CANDELETE = new SFValueable<Object>(16, "canDelete", "CANDELETE", false, Object.class, CANDELETE_SCALE, ORDER_CANWRITE) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createCandelete();}	
	};
	public static final SFValueable<String> MANDATOR = new SFValueable<String>(17, "nuclosMandator", "STRVALUE_NUCLOSMANDATOR", true, String.class, 255, ORDER_MANDATOR) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createMandator(entityUID);}
	};
	public static final SFValueable<Object> CANSTATECHANGE = new SFValueable<Object>(18, "canStateChange", "CANSTATECHANGE", false, Object.class, CANSTATECHANGE_SCALE, ORDER_CANSTATECHANGE) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createCanstatechange();}
	};
	public static final SFValueable<String> OWNER = new SFValueable<String>(19, "nuclosOwner", "STRVALUE_NUCLOSOWNER", false, String.class, 255, ORDER_OWNER) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createOwner();}
	};
	
	public static final SF<UID> STATE_UID = new SF<UID>(6, "nuclosState", "STRUID_NUCLOSSTATE", true, UID.class, UID_SCALE, ORDER_STATE) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createState();}
	};
	public static final SF<UID> PROCESS_UID = new SF<UID>(10, "nuclosProcess", "STRUID_NUCLOSPROCESS", false, UID.class, UID_SCALE, ORDER_PROCESS) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createProcess();}
	};
	public static final SF<UID> MANDATOR_UID = new SF<UID>(17, "nuclosMandator", "STRUID_NUCLOSMANDATOR", true, UID.class, UID_SCALE, ORDER_MANDATOR) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createMandator(entityUID);}
	};
	public static final SF<UID> OWNER_UID = new SF<UID>(19, "nuclosOwner", "STRUID_NUCLOSOWNER", false, UID.class, UID_SCALE, ORDER_OWNER) {
		private static final long serialVersionUID = 1L;
		@Override FieldMeta<?> createStaticMeta(UID entityUID) {return createOwner();}
	};
	
	public static final String RESOURCE_LABEL_MANDATOR_PREFIX = "mandator.eofield.label.";
	
	public static final UID STATE_CHANGED_ACTION = new UID("nuclosStateChangedAction");

	private static Collection<SF<?>> allFields = null;
	
	private final Integer postfix;

	private final String field;
	
	private final String dbField;
	
	// ???
	private transient final MetaDataCache metaCache;

	private final boolean forceValueSearch;
	
	private final Class<T> cls;
	
	private final Integer scale;
	
	private final Integer order;

	SF(Integer postfix, String field, String dbField, boolean forceValueSearch, Class<T> cls, Integer scale, Integer order) {
		this.postfix = postfix;
		this.field = field;
		this.dbField = dbField;
		this.forceValueSearch = forceValueSearch;
		this.cls = cls;
		this.scale = scale;
		this.metaCache = new MetaDataCache(this);
		this.order = order;
	}

	public FieldMeta<T> getMetaData(EntityMeta<?> entityMeta) {
		return (FieldMeta<T>) getMetaData(entityMeta.getUID());
	}
	
	public FieldMeta<?> getMetaData(UID entityUID) {
		return metaCache.getMetaData(entityUID);
	}
	
	public UID getUID(EntityMeta<?> entityMeta) {
		return getUID(entityMeta.getUID());
	}
	
	public UID getUID(UID entityUID) {
		return metaCache.getMetaData(entityUID).getUID();
	}

	public String getFieldName() {
		return field;
	}
	
	public String getDbColumn() {
		return dbField;
	}
	
	public Integer getOrder() {
		return order;
	}

	public boolean isForceValueSearch() {
		return forceValueSearch;
	}
	
	public Class<T> getJavaClass() {
		return cls;
	}

	public static Collection<SF<?>> getAllFields() {
		if (allFields == null) {
			Collection<SF<?>> initAllFields = new ArrayList<SF<?>>();
			Field[] fl = SF.class.getDeclaredFields();
			for (Field f : fl) {
				try {
					f.setAccessible(true);
					Object o = f.get(null);
					if (o instanceof SF<?>) {
						initAllFields.add((SF<?>) f.get(null));
					}
				} catch (NullPointerException e) {
					// ignore
				} catch (Exception e) {
					throw new CommonFatalException(e);
				} 
			}
			allFields = initAllFields;
		}
		return allFields;
	}

	static boolean isSystemField(FieldMeta<?> field) {
		final UID entityUID = field.getEntity();
		final UID fieldUID = field.getUID();

		for (SF<?> sField : getAllFields()) {
			if (sField.getUID(entityUID).equals(fieldUID)) {
				return true;
			}
		}

		return false;
	}

	public static boolean isVersionField(FieldMeta<?> field) {
		final UID entityUID = field.getEntity();
		final UID fieldUID = field.getUID();

		return VERSION.checkField(entityUID, fieldUID)
				|| CREATEDAT.checkField(entityUID, fieldUID)
				|| CREATEDBY.checkField(entityUID, fieldUID)
				|| CHANGEDAT.checkField(entityUID, fieldUID)
				|| CHANGEDBY.checkField(entityUID, fieldUID);
	}

	public static boolean isEOFieldWithForceValueSearch(UID entity, UID field) {
		if (entity == null) {
			throw new IllegalArgumentException("entity must not be null");
		}
		for (SF<?> eoField: getAllFields()) {
			if (eoField.getUID(entity).equals(field)) {
				return eoField.isForceValueSearch();
			}
		}
		return false;
	}

	public static boolean isEOFieldWithForceValueSearch(String field) {
		SF<?> eoField = getByField(field);
		if (eoField != null) {
			return eoField.isForceValueSearch();
		}
		return false;
	}
	
	public static boolean isEOField(UID entity, UID field) {
		if (entity == null) {
			throw new IllegalArgumentException("entity must not be null");
		}
		for (SF<?> eoField: getAllFields()) {
			if (eoField.checkField(entity, field)) {
				return true;
			}
		}
		return false;
	}

	public static SF<?> getEOField(UID entity, UID field) {
		if (entity == null) {
			throw new IllegalArgumentException("entity must not be null");
		}
		for (SF<?> eoField: getAllFields()) {
			if (eoField.checkField(entity, field)) {
				return eoField;
			}
		}
		return null;
	}

	public static boolean isEOField(String field) {
		SF<?> eoField = getByField(field);
		if (eoField != null) {
			return true;
		}
		return false;
	}

	public static SF<?> getByField(String field) {
		for (SF<?> eoField : getAllFields()) {
			if (eoField.checkField(field))
				return eoField;
		}
		return null;
	}

	public boolean checkField(String field) {
		return (field != null) && field.equals(getFieldName());
	}
	
	public boolean checkField(UID entity, UID field) {
		return (field != null) && field.equals(getUID(entity));
	}
	
	abstract FieldMeta<?> createStaticMeta(UID entityUID);
	
	@Override
	public int hashCode() {
		return field.hashCode();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that instanceof SF<?>) {
			return false;
		} else {
			throw new IllegalArgumentException("Something went wrong: SF.equal(" + that.getClass().getName() + ")! SF.this[" + this + "] that[" + that + "]");
		}
	}

	@Override
	public String toString() {
		return "SF["+field+"]";
	}

	FieldMeta<?> createMeta(UID entityUID) {
		FieldMetaVO<?> meta = (FieldMetaVO<?>) createStaticMeta(entityUID);
		
		meta.setUID(new UID(entityUID.getString() + postfix));
		meta.getUID().setDebugInfo(getFieldName());
		meta.setEntity(entityUID);
		meta.setFieldName(getFieldName());
		meta.setDbColumn(getDbColumn());
		meta.setDataType(getJavaClass().getName());
		meta.setScale(scale);
		return meta;
	}

	private static class MetaDataCache {
		
		private final Map<UID, FieldMeta<?>> entityFields = new HashMap<UID, FieldMeta<?>>();
		
		private final SF<?> sf;
		
		public MetaDataCache(SF<?> sf) {
			this.sf = sf;
		}
		
		FieldMeta<?> getMetaData(UID entityUID) {
			FieldMeta<?> result = entityFields.get(entityUID);
			if (result != null) {
				return result;
			}
			result = sf.createMeta(entityUID);
			entityFields.put(entityUID, result);
			return result;
		}
		
	}
	
	static FieldMeta<?> createPrimaryKey() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(false);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(false);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("primarykey");
		
		result.setOrder(-1);

		return result;
	}
	
	private static FieldMeta<?> createCreatedAt() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.createdat.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.createdat.description");
		
		result.setOrder(ORDER_CREATEDAT);

		return result;
	}

	private static FieldMeta<?> createCreatedBy() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.createdby.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.createdby.description");
		
		result.setOrder(ORDER_CREATEDBY);

		return result;
	}

	private static FieldMeta<?> createChangedAt() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.changedat.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.changedat.description");
		
		result.setOrder(ORDER_CHANGEDAT);

		return result;
	}

	private static FieldMeta<?> createChangedBy() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setScale(255);
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.changedby.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.changedby.description");
		
		result.setOrder(ORDER_CHANGEDBY);

		return result;
	}

	private static FieldMeta<?> createVersion() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.version.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.version.description");
		
		result.setOrder(ORDER_VERSION);

		return result;
	}

	private static FieldMeta<?> createCanwrite() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(true);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);
		
		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.canwrite.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.canwrite.description");
		
		result.setOrder(ORDER_CANWRITE);

		return result;
	}
	
	private static FieldMeta<?> createCanstatechange() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(true);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);
		
		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.canstatechange.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.canstatechange.description");
		
		result.setOrder(ORDER_CANSTATECHANGE);

		return result;
	}
	
	private static FieldMeta<?> createCandelete() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);
		
		result.setReadonly(true);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.candelete.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.candelete.description");
		
		result.setOrder(ORDER_CANDELETE);

		return result;
	}
	
	private static FieldMeta<?> createMandator(UID entityUID) {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_WRITE);

		result.setReadonly(false);
		result.setNullable(false);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(false);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setForeignEntity(UID_MANDATOR);
		result.setForeignEntityField("uid{AExmd}");

		result.setLocaleResourceIdForLabel(RESOURCE_LABEL_MANDATOR_PREFIX+entityUID.getString());
		result.setLocaleResourceIdForDescription(RESOURCE_LABEL_MANDATOR_PREFIX+entityUID.getString());
		
		result.setOrder(ORDER_MANDATOR);

		return result;
	}

	private static FieldMeta<?> createState() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setForeignEntity(UID_STATE);
		result.setForeignEntityField("uid{U89Na}");

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.state.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.state.description");
		
		result.setOrder(ORDER_STATE);

		return result;
	}

	private static FieldMeta<?> createStateNumber() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setColumnMaster(false);
		result.setReadonly(true);
		result.setNullable(true);
		result.setModifiable(false);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setForeignEntity(UID_STATE);
		result.setForeignEntityField("uid{U89Nd}");

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.statenumeral.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.statenumeral.description");
		
		result.setOrder(ORDER_STATENUMBER);

		return result;
	}

	private static FieldMeta<?> createStateIcon() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setColumnMaster(false);
		result.setReadonly(true);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(false);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setForeignEntity(UID_STATE);
		result.setForeignEntityField("uid{U89Nh}");

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.stateicon.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.stateicon.description");
		
		result.setOrder(ORDER_STATEICON);

		return result;
	}

	private static FieldMeta<?> createSystemIdentifier() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.systemidentifier.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.systemidentifier.description");
		
		result.setOrder(ORDER_SYSTEMIDENTIFIER);

		return result;
	}

	private static FieldMeta<?> createProcess() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_WRITE);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setForeignEntity(UID_PROCESS);
		result.setForeignEntityField("uid{ui9lb}");

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.process.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.process.description");
		
		result.setOrder(ORDER_PROCESS);

		return result;
	}

	private static FieldMeta<?> createOrigin() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.origin.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.origin.description");
		
		result.setOrder(ORDER_ORIGIN);

		return result;
	}

	private static FieldMeta<?> createLogicalDeleted() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(false);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);
		result.setDefaultMandatory("false");

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.logicaldeleted.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.logicaldeleted.description");
		
		result.setOrder(ORDER_LOGICALDELETED);

		return result;
	}
	
	private static FieldMeta<?> createImportVersion() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(false);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.importversion.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.importversion.description");
		
		result.setOrder(ORDER_IMPORTVERSION);

		return result;
	}
	
	private static FieldMeta<?> createOriginUID() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setReadonly(false);
		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(false);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);

		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.originuid.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.originuid.description");
		
		result.setOrder(ORDER_ORIGINUID);

		return result;
	}
	
	private static FieldMeta<?> createOwner() {
		FieldMetaVO<?> result = new FieldMetaVO<Object>();
		result.setFieldGroup(GROUP_UID_READ);

		result.setNullable(true);
		result.setModifiable(true);
		result.setSearchable(false);
		result.setLogBookTracking(true);
		result.setShowMnemonic(false);
		result.setUnique(false);
		result.setInsertable(false);
		result.setReadonly(true);
		
		result.setCalcFunction(OWNER_CALC_FUNCTION);		
		result.setForeignEntity(UID_USER);
		result.setForeignEntityField("uid{dRxja}");
		
		result.setLocaleResourceIdForLabel("nuclos.entityfield.eo.owner.label");
		result.setLocaleResourceIdForDescription("nuclos.entityfield.eo.owner.description");
		
		result.setOrder(ORDER_OWNER);

		return result;
	}
}
