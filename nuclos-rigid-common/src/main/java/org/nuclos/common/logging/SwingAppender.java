package org.nuclos.common.logging;

import javax.swing.*;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 * @deprecated For moving forwards to kick log4j out of the project. (tp)
 */
public class SwingAppender extends AbstractAppender {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2191173766263885953L;
	//private final PatternLayout layout = new PatternLayout("%d{ISO8601} %-5p [%c] %m%n");
	
	private final IWriteComponent view;
	
	public SwingAppender(IWriteComponent view) {
		super("SwingAppender", null, createLayout());
		this.view = view;
	}
	
	private static PatternLayout createLayout() {
		return PatternLayout.newBuilder().withPattern("%d{ISO8601} %-5p [%c] %m%n").build();
	}
	
	@Override
	public PatternLayout getLayout() {
		return (PatternLayout) super.getLayout();
	}

	@Override
	public void append(final LogEvent event) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				StringBuilder sb = new StringBuilder();
				sb.append(getLayout().toSerializable(event));
				
				// TODO Log4j2 ... Migrate
//				if (getLayout().ignoresThrowable()) {
//					String[] s = event.getThrowableStrRep();
//					if (s != null) {
//						int len = s.length;
//						for (int i = 0; i < len; i++) {
//							sb.append(s[i]);
//							sb.append(Layout.LINE_SEP);
//						}
//					}
//				}

				view.write(sb.toString());
			}
		});
	}

}
