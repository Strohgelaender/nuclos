//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * An implementation of IMetaProvider for defining a desired state of the DB
 * meta data.
 * <p>
 * You use this class to define desired <em>changes</em> to your DB (meta) data
 * model.
 * </p>
 */
public class StaticMetaDataProvider implements IRigidMetaProvider {
	
	private final SysEntities sysEntities;

	public StaticMetaDataProvider(SysEntities sysEntities) {
		this.sysEntities = sysEntities;
	}

	private Map<UID, EntityMeta<?>> mapMetaData = new LinkedHashMap<UID, EntityMeta<?>>();
	private Map<UID, FieldMeta<?>> mapFieldMetaData = new LinkedHashMap<UID, FieldMeta<?>>();

	@Override
	public Collection<EntityMeta<?>> getAllEntities() {
		return mapMetaData.values();
	}

	public boolean checkEntity(UID uid) {
		final EntityMeta<?> result = mapMetaData.get(uid);
		return result != null;
	}

	@Override
	public EntityMeta<?> getEntity(UID uid) {
		final EntityMeta<?> result = mapMetaData.get(uid);
		if (result == null) {
			throw new CommonFatalException("entity with id " + uid
					+ " does not exist.");
		}
		return result;
	}

	@Override
	public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entity) {
		return transformFieldMap(mapMetaData.get(entity).getFields());
	}

	@Override
	public FieldMeta<?> getEntityField(UID field) {
		FieldMeta<?> result = mapFieldMetaData.get(field);
		if (result != null) {
			return result;
		}
		throw new CommonFatalException("entity field with uid=" + field
				+ " does not exist.");
	}

	public void add(EntityMeta<?> entityMeta, boolean withFields) {
		mapMetaData.put(entityMeta.getUID(), entityMeta);
		if (withFields) {
			mapFieldMetaData.putAll(transformFieldMap(entityMeta.getFields()));
		}
	}

	public EntityMetaVO<?> addClone(EntityMeta<?> entityMeta, boolean withFields) {
		EntityMetaVO<?> clone = new EntityMetaVO(entityMeta, withFields);
		this.add(clone, withFields);
		return clone;
	}

	public FieldMetaVO<?> addClone(FieldMeta<?> fieldMeta) {
		EntityMeta<?> entityMeta = mapMetaData.get(fieldMeta.getEntity());
		if (entityMeta == null) {
			throw new NuclosFatalException(String.format("Metadata not found for entity with %s", fieldMeta.getEntity()));
		}
		Collection<FieldMeta<?>> fields;
		try {
			fields = entityMeta.getFields();
		} catch (Exception ex) {
			fields = new ArrayList<FieldMeta<?>>();
			((EntityMetaVO<?>) entityMeta).setFields(fields);
		}
		if (!fields.contains(fieldMeta)) {
			FieldMetaVO<?> clone = new FieldMetaVO(fieldMeta);
			fields.add(clone);
			mapFieldMetaData.put(clone.getUID(), clone);
			return clone;
		} 
		return null;
	}

	public void addClones(Collection<? extends FieldMeta<?>> fieldsMeta) {
		for (FieldMeta<?> fieldMeta : fieldsMeta) {
			addClone(fieldMeta);
		}
	}

	@Override
	public EntityMeta<?> getByTablename(String sTableName) {
		throw new NotImplementedException();
	}

	@Override
	public boolean isNuclosEntity(UID entityUID) {
		return sysEntities._isNuclosEntity(entityUID);
	}
	
	public static Map<UID, FieldMeta<?>> transformFieldMap(Collection<FieldMeta<?>> fields) {
		if (fields == null || fields.isEmpty()) {
			return Collections.emptyMap();
		}
		Map<UID, FieldMeta<?>> result = new HashMap<UID, FieldMeta<?>>();
		for (FieldMeta<?> fieldMeta : fields) {
			result.put(fieldMeta.getUID(), fieldMeta);
		}
		return result;
	}
}
