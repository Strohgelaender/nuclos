//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dblayer;

import org.nuclos.common.UID;

/**
 * The AST result element from parsing the expression language used in field references.
 *  
 * @author Maik Stueker
 * @since Nuclos 4.0.00
 * 
 * @see org.nuclos.common2.ForeignEntityFieldParser
 */
public interface IFieldUIDRef {
	
	boolean isConstant();
	
	String getConstant();
	
	boolean isUID();
	
	UID getUID();

}
