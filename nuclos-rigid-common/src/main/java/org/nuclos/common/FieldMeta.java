package org.nuclos.common;

import java.io.Serializable;

public abstract class FieldMeta<T> implements DbField<T>, Serializable, Comparable<FieldMeta<?>> {

	private static final long serialVersionUID = -85933747620750276L;
	private static final String ENTITY_LANGUAGE_DB_COLUMN_SUFFIX = "_LANG";
	public static final Integer DATA_LANG_DB_COLUMN_NAME_MAX_LENGTH = 30;
	
	public static final FieldMeta<?> NULL = new FieldMeta() {
		private static final long serialVersionUID = 1L;
		@Override public UID getUID() {return null;}
		@Override public UID getEntity() {return null;}
		@Override public Class<?> getJavaClass() {return null;}
		@Override public String getDbColumn() {return "";}
		@Override public String getFieldName() {return "";}
	};
	
	@Override
	public int compareTo(FieldMeta<?> that) {
		return RigidUtils.compare(getOrder(), that.getOrder());
	}

	public abstract static class Valueable<T> extends FieldMeta<T> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3819785859369486105L;
	}
	
	public abstract UID getUID();
	
	public abstract UID getEntity();
	
	public abstract String getFieldName();

	public abstract String getDbColumn();

	public abstract Class<T> getJavaClass();
	
	public String getDataType() {
		return getJavaClass().getName();
	}
	
	public boolean isFileDataType() {
		return "org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile".equals(getDataType());
	}
	
	public Integer getScale() {
		return null;
	}

	public Integer getPrecision() {
		return null;
	}

	public UID getForeignIntegrationPoint() {
		return null;
	}

	public UID getForeignEntity() {
		return null;
	}
	
	/**
	 * Return a 'stringified' reference definition that could be parsed 
	 * by {@link org.nuclos.common2.ForeignEntityFieldUIDParser}.
	 */
	public String getForeignEntityField() {
		return null;
	}
	
	public UID getUnreferencedForeignEntity() {
		return null;
	}
	
	public String getUnreferencedForeignEntityField() {
		return null;
	}
	
	public UID getLookupEntity() {
		return null;
	}
	
	public String getLookupEntityField() {
		return null;
	}
	
	public UID getFieldGroup() {
		return null;
	}
	
	public String getFallbackLabel() {
    	return null;
    }

	public String getFormatInput() {
		return null;
	}

	public String getFormatOutput() {
		return null;
	}

	public Long getDefaultForeignId() {
		return null;
	}

	public String getDataLangDBColumn() {
		return null;	
	}
	
	public UID getDefaultForeignUid() {
		return null;
	}

	public String getDefaultValue() {
		return null;
	}
	
	public String getLocaleResourceIdForDescription() {
		return null;
	}
	
	public String getLocaleResourceIdForLabel() {
		return null;
	}
	
	public String getCalcFunction() {
		return null;
	}
	
	public UID getCalcAttributeDS() {
		return null;
	}
	
	public UID getCalcBaseFieldUID() {
		return null;
	}
	
	public final boolean isCalculated() {
		return !RigidUtils.isNullOrEmpty(getCalcFunction()) || getCalcAttributeDS() != null; 
	}
	
	public String getCalcAttributeParamValues() {
		return null;
	}

	public boolean isCalcOndemand() {
		return false;
	}
	
	public boolean isCalcAttributeAllowCustomization() {
		return false;
	}
	
	public NuclosScript getCalculationScript() {
		return null;
	}
	
	public String getSortorderASC() {
		return null;
	}

	public String getSortorderDESC() {
		return null;
	}
	
	public String getDefaultMandatory() {
		return null;
	}
	
	public boolean isReadonly() {
		return false;
	}

	public final boolean isWritable() {
		return !isReadonly();
	}

	public boolean isUnique() {
		return false;
	}

	public boolean isHidden() {
		return false;
	}

	public boolean isNullable() {
		return true;
	}

	public boolean isSearchable() {
		return false;
	}

	public boolean isModifiable() {
		return true;
	}

	public boolean isInsertable() {
		return true;
	}

	public boolean isLogBookTracking() {
		return false;
	}

	public boolean isShowMnemonic() {
		return false;
	}
	
	//TODO: What is this for? Seems always to be false. Is there some confusion with dynamic entities?
	public boolean isDynamic() {
    	return false;
    }
	
	public boolean isIndexed() {
		return false;
	}
	
	public boolean isPermissionTransfer() {
		return false;
	}
	
	public boolean isOnDeleteCascade() {
		return false;
	}
	
	public boolean isResourceField() {
		return false;
	}

	public boolean isInvariant() {
		return false;
	}

	public boolean isIntegrationField() {
		return false;
	}
	
	public Integer getOrder() {
		return null;
	}
	
	public String getDefaultComponentType() {
		return null;
	}
	
	public String getSearchField() {
		return null;
	}
	
	public NuclosScript getBackgroundColorScript() {
		return null;
	}
	
	public UID getAutonumberEntity() {
		return null;
	}
	
	public boolean isColumnMaster() {
		return true;
	}

	public final boolean isIntegrationComplete() {
		if (getForeignIntegrationPoint() != null && getForeignEntity() == null) {
			// integration is not completed. We ignore such attributes in column creation...
			return false;
		}
		return true;
	}
	
	public Boolean isLocalized() {
		return false;
	}

	public UID getDocumentFileDbBackupContentField() {
		return null;
	}

	public UID getFirstNonNullForeignEntity() {
		return RigidUtils.firstNonNull(getForeignEntity(), getUnreferencedForeignEntity(), getLookupEntity());
	}
	
	public boolean hasAnyForeignEntity() {
		return getForeignEntity() != null || getUnreferencedForeignEntity() != null || getLookupEntity() != null;
	}

	@Override
	public String toString() {
		return getFieldName();
	}
	
	public String getComment() {
		return null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof FieldMeta)) {
			return false;
		}

		final FieldMeta<?> that = (FieldMeta<?>) obj;
		return RigidUtils.equal(getUID(), that.getUID());
	}

	@Override
    public int hashCode() {
		int result = getFieldName().hashCode();
		final UID uid = getUID();
		if (uid != null) {
			result += 3 * uid.hashCode();
		}
	    return result;
    }

	public static String getLanguageFieldDBColumName(FieldMeta meta) {
		return getLanguageFieldDBColumName(meta, false);
	}
	
	public static String getLanguageFieldDBColumName(FieldMeta meta, boolean createModificationFlag) {
		String columnName = meta.getDbColumn() + ENTITY_LANGUAGE_DB_COLUMN_SUFFIX;
		
		if (createModificationFlag) {
			columnName = "BLN" + columnName + "MODIFIED";
		}
		
		if (columnName.length() > DATA_LANG_DB_COLUMN_NAME_MAX_LENGTH) {
			if (createModificationFlag) {
				return columnName.substring(0, DATA_LANG_DB_COLUMN_NAME_MAX_LENGTH - ENTITY_LANGUAGE_DB_COLUMN_SUFFIX.length() - 3) + "MODIFIED";
			} else {
				return meta.getDbColumn().substring(0, DATA_LANG_DB_COLUMN_NAME_MAX_LENGTH - ENTITY_LANGUAGE_DB_COLUMN_SUFFIX.length()) + ENTITY_LANGUAGE_DB_COLUMN_SUFFIX;							
			}
		} else {
			return columnName;
		}
	}

	public boolean isSystemField() {
		return SF.isSystemField(this);
	}

	public boolean isVersionField() {
		return SF.isVersionField(this);
	}

	public boolean isNuclosRowColor() {
		return "nuclosRowColor".equalsIgnoreCase(getFieldName());
	}
}
