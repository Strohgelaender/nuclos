//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class NuclosImage implements Serializable, NuclosAttributeExternalValue, org.nuclos.api.NuclosImage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1793727770228849127L;

	private static final Logger LOG = Logger.getLogger(NuclosImage.class);

	private final String name;
	private final byte[] content;
	private final INuclosImageProducer producer;
	
	private byte[] thumbnail;
	private int width = -1;
	private int height = -1;
	private boolean bProduce;
	
	private transient BufferedImage image;

	public NuclosImage() {
		this("", null);
		bProduce = false;
	}
	
	public NuclosImage(String fileName, byte[] content) {
		this(fileName, content, null);
	}

	public NuclosImage(String fileName, byte[] content, Dimension d) {
		this(fileName, content, d, ImageProducerFactory.createImageProducer(content));
	}

	public NuclosImage(String fileName, byte[] content, Dimension d, INuclosImageProducer producer) {
		this.name = fileName;
		if (d != null) {
			try {
				content = producer.getScaledImageBytes(content, d);
			} catch (IOException io) {
				LOG.warn(io.getMessage(), io);
			}
		}
			
		this.content = content;
		this.producer = producer;
	}

	@Override
	public String getName() {
		return name;
	}

	public void produceThumbnail() {
		try {
			setThumbnail(producer.getThumbNail(getImage()));
		}
		catch(Exception e) {
			// thumbnail can't be set
			LOG.warn("produceThumbnail failed: " + e);
		}
	}

	@Override
	public byte[] getContent() {
		return content;
	}

	public byte[] getThumbnail() {
		if (thumbnail == null || thumbnail.length == 0 || bProduce == false) {
			produceThumbnail();
		}
		return thumbnail;
	}
	
	public void setThumbnail(byte[] thumbnail) {
		this.thumbnail = thumbnail;
		this.bProduce = true;
	}
	
	public BufferedImage getImage() {
		if (image == null) {
			try {
				image = producer.getBufferedImage(this.content);				
			} catch (Exception e) {
				LOG.warn(e.getMessage(), e);
			}
		}
		
		return image;
	}

	public int getWidth() {
		if (width == -1) {
			BufferedImage image = getImage();
			if (image != null) {
				width = image.getWidth();
			}			
		}
		
		return width;
	}

	public int getHeight()  {
		if (height == -1) {
			BufferedImage image = getImage();
			if (image != null) {
				height = image.getWidth();
			}			
		}
		
		return height;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof NuclosImage) {
			NuclosImage that = (NuclosImage) obj;
			return RigidUtils.equal(name, that.name)
				&& Arrays.equals(content, that.content)
				&& RigidUtils.equal(bProduce, that.bProduce);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		int code0 = RigidUtils.hashCode(name);
		int code1 = Arrays.hashCode(content);
		int code2 = RigidUtils.hashCode(bProduce);
		return code0 ^ code1 ^ code2;
	}

	@Override
	public String toString() {
		return name;
	}
	
	public String getBASE64Data(boolean bFullImage) {
		byte[] imageBytes = null;
		if (!bFullImage) {
			imageBytes = getThumbnail();
		}
		
		if (imageBytes == null) {
			imageBytes = getContent();
		}
		
		return imageBytes != null ? new String(Base64.encodeBase64(imageBytes)) : null;
	}
	
}
