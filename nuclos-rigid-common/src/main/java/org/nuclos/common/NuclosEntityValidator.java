package org.nuclos.common;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Helper class to check the validity of entities.
 * Checks if names are allowed and constraints are met.
 *
 * TODO: Throw only checked Exceptions, instead of RuntimeExceptions
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 * @see <a href="http://support.nuclos.de/browse/NUCLOS-2790">NUCLOS-2790</a>
 */
public class NuclosEntityValidator {

	public static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.businessentity";
	public static final String DEFAULT_ENTITY_PREFIX = "BO";

	private static final Set<String> RESERVED_NAMES;
	private static final Set<String> RESERVED_DB_COLUMN_NAMES;

	static {
		final Set<String> names = new HashSet<>();
		names.add("class");
		names.add("entity");
		names.add("genericobject");
		names.add("attachment");
		names.add("position");
		names.add("businessobject");

		final Set<String> dbColumnNames = new HashSet<>();
		dbColumnNames.add("strsystemid");
		dbColumnNames.add("intid_t_md_process");
		dbColumnNames.add("strorigin");
		dbColumnNames.add("intid_t_md_state");

		for (SF sf : SF.getAllFields()) {
			if (StringUtils.isNotBlank(sf.getFieldName())) {
				names.add(StringUtils.lowerCase(sf.getFieldName()));
				dbColumnNames.add(StringUtils.lowerCase(sf.getDbColumn()));
			}
		}

		RESERVED_NAMES = names;
		RESERVED_DB_COLUMN_NAMES = dbColumnNames;
	}

	/**
	 * Checks if the given entityName is valid, i.e. it is not a reserved word and does not collide with another
	 * entity name.
	 * Also there must be no other class in the given imports with the same name.
	 *
	 * @param entityUID (null for new entity)
	 */
	public static boolean isValidEntityName(
			final String entityName,
			final UID nucletUID,
			final UID entityUID,
			final Collection<EntityMeta<?>> colMetas,
			final Collection<? extends INuclosEntityImport> imports
	) {
		final String escapedEntityName = escapeJavaIdentifier(entityName);
		return _isValidEntityName(escapedEntityName, nucletUID, entityUID, colMetas, imports);
	}

	private static boolean _isValidEntityName(
			final String escapedEntityName,
			final UID nucletUID,
			final UID entityUID,
			final Collection<EntityMeta<?>> colMetas,
			final Collection<? extends INuclosEntityImport> imports
	) {
		if (isReservedName(escapedEntityName)) {
			return false;
		}

		if (_isImported(escapedEntityName, imports)) {
			return false;
		}

		return !_isDuplicate(escapedEntityName, nucletUID, entityUID, colMetas);
	}

	/**
	 * Checks if the given entityName is already in use.
	 *
	 * @param entityUID (null for new entity)
	 */
	public static boolean isDuplicate(
			final String entityName,
			final UID nucletUID,
			final UID entityUID,
			final Collection<EntityMeta<?>> colMetas
	) {
		final String escapedEntityName = escapeJavaIdentifier(entityName);
		return _isDuplicate(escapedEntityName, nucletUID, entityUID, colMetas);
	}

	private static boolean _isDuplicate(
			final String escapedEntityName,
			final UID nucletUID,
			final UID entityUID,
			final Collection<EntityMeta<?>> colMetas
	) {
		for (EntityMeta<?> entityMeta : colMetas) {
			if (RigidUtils.equal(entityUID, entityMeta.getUID())) {
				continue;
			}
			if (RigidUtils.equal(nucletUID, entityMeta.getNuclet())) {
				final String otherEscapedEntityName = escapeJavaIdentifier(entityMeta.getEntityName());
				if (StringUtils.equalsIgnoreCase(escapedEntityName, otherEscapedEntityName)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Checks if the given entity name is contained in the given imports.
	 */
	private static boolean _isImported(
			final String entityName,
			final Collection<? extends INuclosEntityImport> imports
	) {
		if (imports == null || imports.isEmpty()) {
			return false;
		}

		for (INuclosEntityImport imp : imports) {
			if (StringUtils.equalsIgnoreCase(entityName, imp.getImportClass().getSimpleName())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks if the given attribute name is valid for the given entity,
	 * i.e. is not reserved and does not lead to any collisions.
	 */
	public static boolean isValidAttributeName(final String name, final UID entityUid, final IRigidMetaProvider metaProvider) {
		EntityMeta<?> entityMeta = null;
		try {
			// TODO: There's no method to check the existence of an entity or to fetch it failsafe.
			entityMeta = metaProvider.getEntity(entityUid);
		} catch (CommonFatalException e) {
			// Ignore
		}

		return isValidAttributeName(name, entityMeta, metaProvider);
	}

	/**
	 * @see #isValidAttributeName(String, UID, IRigidMetaProvider)
	 */
	static boolean isValidAttributeName(final String name, final EntityMeta<?> entityMeta, final IRigidMetaProvider metaProvider) {
		try {
			final String escapedName = escapeJavaIdentifierPart(name);
			return _isValidAttributeName(escapedName, entityMeta, metaProvider);
		} catch (Exception ex) {
			return false;
		}
	}

	private static boolean _isValidAttributeName(final String escapedName, final EntityMeta<?> entityMeta, final IRigidMetaProvider metaProvider) {
		if (_isReservedName(escapedName) || StringUtils.isBlank(escapedName)) {
			return false;
		}

		// Check for references to this entity, if the EntityMeta already exists
		// TODO: Also consider multi-depedencies, the generated methods for those are currently simply numerated (see NUCLOS-4932)
		if (entityMeta != null) {
			final Set<String> referencingEntityNames = _findReferencingEntityNames(entityMeta, metaProvider);
			for (String referencingEntity : referencingEntityNames) {
				final String escapedRefName = escapeJavaIdentifier(referencingEntity);
				if (StringUtils.equalsIgnoreCase(escapedRefName, escapedName)) {
					return false;
				}
			}
		}

		// Check for duplicate method names
		// TODO: EntityMeta is needed here, but new BOs (for which there is no EntityMeta yet) should also be checked.
		if (entityMeta != null) {
			final Set<String> methodNames = new HashSet<>();
			for (FieldMeta<?> field : entityMeta.getFields()) {
				methodNames.add(getGetterName(field.getFieldName()));
				methodNames.add(getSetterName(field.getFieldName()));
			}
			return !methodNames.contains(getGetterName(escapedName))
					&& !methodNames.contains(getSetterName(escapedName));
		}

		return true;
	}

	/**
	 * Finds all fields that reference the given entity.
	 */
	private static Set<FieldMeta<?>> _findReferencesTo(final EntityMeta<?> entity, final IRigidMetaProvider metaProvider) {
		final Set<FieldMeta<?>> result = new HashSet<>();
		for (EntityMeta<?> meta : metaProvider.getAllEntities()) {
			for (FieldMeta<?> field : meta.getFields()) {
				if (entity.getUID().equals(field.getForeignEntity())) {
					result.add(field);
				}
			}
		}
		return result;
	}

	/**
	 * Finds the names of all entities with at least one reference to the given entity.
	 */
	private static Set<String> _findReferencingEntityNames(final EntityMeta<?> entity, final IRigidMetaProvider metaProvider) {
		final Set<String> result = new HashSet<>();
		for (EntityMeta<?> referencingEntity : metaProvider.getAllEntities()) {
			for (FieldMeta<?> field : referencingEntity.getFields()) {
				if (entity.getUID().equals(field.getForeignEntity())) {
					result.add(referencingEntity.getEntityName());
				}
			}
		}
		return result;
	}

	/**
	 * Checks if the a reference from an entity with the given entityName is allowed to the entity
	 * corresponding to the given foreignEntityMeta.
	 * A reference is not allowed, if the foreign entity already contains a field with the same name as
	 * the entity name, as this would lead to duplicate getter methods in the generated class for the
	 * foreign entity (one getter for the field and one for the referencing entity records).
	 *
	 * @see <a href="http://support.nuclos.de/browse/NUCLOS-2790">NUCLOS-2790</a>
	 * @see <a href="http://support.nuclos.de/browse/NUCLOS-4932">NUCLOS-4932</a>
	 */
	public static boolean isValidReference(final String entityName, final EntityMeta<?> foreignEntityMeta) {
		final String escapedEntityName = escapeJavaIdentifier(entityName);
		return _isValidReference(escapedEntityName, foreignEntityMeta);
	}

	private static boolean _isValidReference(final String escapedEntityName, final EntityMeta<?> foreignEntityMeta) {
		for (FieldMeta<?> fieldMeta : foreignEntityMeta.getFields()) {
			final String fieldName = fieldMeta.getFieldName();
			if (StringUtils.equalsIgnoreCase(fieldName, escapedEntityName)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * System field names and names ending with "Id" and "BO" are reserved.
	 *
	 * @see <a href="http://support.nuclos.de/browse/NUCLOS-2790">NUCLOS-2790</a>
	 */
	public static boolean isReservedName(final String name) {
		final String escapedName = escapeJavaIdentifier(name);
		return _isReservedName(escapedName);
	}

	private static boolean _isReservedName(final String name) {
		return RESERVED_NAMES.contains(StringUtils.lowerCase(name))
				|| StringUtils.endsWithIgnoreCase(name, "id")
				|| StringUtils.endsWithIgnoreCase(name, "bo")
				|| (StringUtils.startsWithIgnoreCase(name, "nuclos") && !StringUtils.equalsIgnoreCase(name, "nuclosRowColor"));
	}

	public static boolean isReservedDbColumnName(String name) {
		return RESERVED_DB_COLUMN_NAMES.contains(StringUtils.lowerCase(name));
	}

	/**
	 * Returns a valid Java identifier for the given identifier, e.g. a Java class name for an entity name.
	 */
	public static String escapeJavaIdentifier(final String identifier) {
		if (isEntirelyInvalidIdentifier(identifier)) {
			throw new IllegalArgumentException("The given identifier is entirely invalid");
		}
		return escapeJavaIdentifier(identifier, "_");
	}

	/**
	 * Returns a valid Java identifier part for the given identifier part, e.g. an attribute name used in a getter method.
	 * Unlike a complete identifier, an identifier part may also start with numbers.
	 */
	public static String escapeJavaIdentifierPart(final String identifier) {
		if (isEntirelyInvalidIdentifier(identifier)) {
			throw new IllegalArgumentException("The given identifier is entirely invalid");
		}

		return _escapeIdentifierPart(identifier);
	}

	/**
	 * Returns a valid Java identifier for the given identifier, e.g. a Java class name for an entity name.
	 * If the identifier starts with an invalid character (e.g. a number), the default prefix is prepended.
	 */
	public static String escapeJavaIdentifier(final String identifier, final String defaultPrefix) {
		if (!isValidJavaIdentifier(defaultPrefix)) {
			throw new IllegalArgumentException("The given prefix is not a valid Java identifier");
		}

		String retVal = _escapeIdentifierPart(identifier);

		if (StringUtils.isEmpty(retVal) || !Character.isJavaIdentifierStart(retVal.charAt(0)))
			retVal = defaultPrefix + retVal;

		return retVal;
	}

	/**
	 * Does the normal preprocessing (replaces invalid characters), but instead of prefixing the result it is
	 * put in quotes, if it still starts with an illegal character (e.g. a number).
	 */
	public static String escapeIdentifierAndQuote(final String identifier) {
		if (isEntirelyInvalidIdentifier(identifier)) {
			throw new IllegalArgumentException("The given identifier is entirely invalid");
		}

		String retVal = _escapeIdentifierPart(identifier);

		if (StringUtils.isEmpty(retVal)) {
			throw new IllegalArgumentException("The given identifier is entirely invalid");
		}

		if (!Character.isJavaIdentifierStart(retVal.charAt(0)))
			retVal = "'" + retVal + "'";

		return retVal;
	}

	/**
	 * Replaces mutated vowels and strips any non-alphanumeric chars from the given identifier.
	 */
	private static String _escapeIdentifierPart(final String identifier) {
		String retVal = identifier;

		retVal = escapeMutatedVowel(retVal);
		retVal = retVal.replaceAll("[^a-zA-Z0-9]+", "");

		return retVal;
	}

	public static String escapeMutatedVowel(String toValidate) {
		String retVal = toValidate;

		retVal = retVal.replace("ä", "ae");
		retVal = retVal.replace("ü", "ue");
		retVal = retVal.replace("ö", "oe");
		retVal = retVal.replace("Ä", "Ae");
		retVal = retVal.replace("Ü", "Ue");
		retVal = retVal.replace("Ö", "Oe");
		retVal = retVal.replace("ß", "ss");

		return retVal;
	}

	/**
	 * Generates the getter method name for the given attribute name;
	 */
	public static String getGetterName(final String attributeName) {
		final String escapedName = escapeJavaIdentifierPart(attributeName);
		return "get" + StringUtils.capitalize(escapedName);
	}

	/**
	 * Generates the setter method name for the given attribute name;
	 */
	public static String getSetterName(final String attributeName) {
		final String escapedName = escapeJavaIdentifierPart(attributeName);
		return "set" + StringUtils.capitalize(escapedName);
	}

	/**
	 * Checks if the given identifier is a valid Java identifier.
	 */
	static boolean isValidJavaIdentifier(final String identifier) {
		if (StringUtils.isEmpty(identifier)) {
			return false;
		}

		if (!Character.isJavaIdentifierStart(identifier.charAt(0))) {
			return false;
		}

		for (int i = 1; i < identifier.length(); i++) {
			if (!Character.isJavaIdentifierPart(identifier.charAt(i))) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Checks if the given identifier is entirely invalid, i.e. it does not contain at least one char that can be used
	 * for a Java identifier.
	 */
	private static boolean isEntirelyInvalidIdentifier(final String identifier) {
		if (StringUtils.isBlank(identifier)) {
			return true;
		}

		for (int i = 0; i < identifier.length(); i++) {
			if (Character.isJavaIdentifierPart(identifier.charAt(i))) {
				return false;
			}
		}

		return true;
	}

	public static String getProxyInterfaceName(EntityMeta<?> emdVO, boolean bForSystemRulesOnly) {
		return getProxyInterfaceName(emdVO.getBusinessObjectClassName(), bForSystemRulesOnly);
	}

	private static String getProxyInterfaceName(String sEntityName, boolean bForSystemRulesOnly) {
		String formatEntity = escapeJavaIdentifier(
				sEntityName,
				DEFAULT_ENTITY_PREFIX
		);
		String formatEntityProxy = formatEntity + "Proxy";
		if (bForSystemRulesOnly) {
			return getFormattedEntityNameForSystemRulesOnly(formatEntityProxy);
		}
		return formatEntityProxy;
	}

	public static String getFormattedEntityNameForSystemRulesOnly(String sEntityName) {
		// default is something like this 'nuclosnucletIntegrationPoint'. This sucks.
		if (sEntityName.startsWith("nuclos")) {
			sEntityName = sEntityName.substring(6);
		}
		if (sEntityName.startsWith("_")) {
			sEntityName = sEntityName.substring(1);
		}
		sEntityName = StringUtils.capitalize(sEntityName);
		return sEntityName;
	}

	public static String getEntityProxyInterfaceQualifiedName(String sPackage, EntityMeta<?> emdVO, boolean bForInternalUseOnly) {
		final String sProxyInterfaceName = getProxyInterfaceName(emdVO, bForInternalUseOnly);
		return getEntityProxyInterfaceQualifiedName(sPackage, sProxyInterfaceName);
	}

	public static String getEntityProxyInterfaceQualifiedName(String sPackage, String sEntityName, boolean bForInternalUseOnly) {
		final String sProxyInterfaceName = getProxyInterfaceName(sEntityName, bForInternalUseOnly);
		return getEntityProxyInterfaceQualifiedName(sPackage, sProxyInterfaceName);
	}

	private static String getEntityProxyInterfaceQualifiedName(String sPackage, String sProxyInterfaceName) {
		return (StringUtils.isNotBlank(sPackage) ? sPackage : DEFFAULT_PACKAGE_NUCLET) + "." + sProxyInterfaceName;
	}
}
