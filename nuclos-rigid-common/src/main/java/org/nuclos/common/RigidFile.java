package org.nuclos.common;

import org.nuclos.server.common.valueobject.DocumentFileBase;

public class RigidFile extends DocumentFileBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4865826565815170207L;

	public RigidFile(String sFileName, Object documentFilePk) {
		super(sFileName, documentFilePk);
	}

	protected RigidFile(String sFileName, Object documentFilePk, byte[] abContents) {
		super(sFileName, documentFilePk, abContents);
	}
	
	protected RigidFile(RigidFile file) {
		super(file.getFilename(), file.getDocumentFilePk(), file.getContents());
	}

	@Override
	protected byte[] getStoredContents() {
		return getContents();
	}

	public void setContents(byte[] contents) {
		this.contents = contents;
	}
	
	protected void contentsChanged() {	
	}

}
