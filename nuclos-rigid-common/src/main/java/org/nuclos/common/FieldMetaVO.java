//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;


public class FieldMetaVO<T> extends FieldMeta<T> {

	private static final long serialVersionUID = 8515924380531732345L;
	
	private UID primaryKey;
	
	private UID entity;
	private UID fieldGroup;

	private String field;

	private String dbColumn;

	private UID foreignIntegrationPoint;
	private UID foreignEntity;
	private String foreignEntityField;
	private UID unreferencedForeignEntity;
	private String unreferencedForeignEntityField;
	private UID lookupEntity;
	private String lookupEntityField;
	private String searchField;
	private UID autonumberEntity;

	private Class<T> javaClass;
	private String defaultComponentType;
	private Integer scale;
	private Integer precision;
	private String formatInput;
	private String formatOutput;

	private Long defaultForeignId;
	private String defaultValue;

	private boolean readonly;
	private boolean unique;
	private boolean hidden;
	private boolean nullable;
	private boolean searchable;
	private boolean modifiable;
	private boolean insertable;
	private boolean logBookTracking;
	private boolean showMnemonic;
	private boolean permissiontransfer;
	private boolean indexed;
	private boolean dynamic;

	private Boolean localized;
	
	private String calcFunction;
	private UID calcAttributeDS;
	private String calcAttributeParamValues;
	private boolean calcAttributeAllowCustomization;
	private boolean calcOndemand;
	private UID calcBaseFieldUID;
	private String sortorderASC;
	private String sortorderDESC;

	private String fallbacklabel;
	private String localeResourceIdForLabel;
	private String localeResourceIdForDescription;

	private String defaultMandatory;

	private boolean onDeleteCascade;
	private Integer order;
	private String comment;
	private NuclosScript calculationScript;
	private NuclosScript backgroundColorScript;
	
	private Boolean columnMaster; 
	
	private String dataLangDBColumn;
	
	public FieldMetaVO() {
	}
	
	public FieldMetaVO(FieldMeta<T> other) {
		primaryKey = other.getUID();
		columnMaster = other.isColumnMaster();
		
		entity = other.getEntity();
		fieldGroup = other.getFieldGroup();

		field = other.getFieldName();

		dbColumn = other.getDbColumn();

		foreignIntegrationPoint = other.getForeignIntegrationPoint();
		foreignEntity = other.getForeignEntity();
		foreignEntityField = other.getForeignEntityField();
		unreferencedForeignEntity = other.getUnreferencedForeignEntity();
		unreferencedForeignEntityField = other.getUnreferencedForeignEntityField();
		lookupEntity = other.getLookupEntity();
		lookupEntityField = other.getLookupEntityField();
		searchField = other.getSearchField();
		autonumberEntity = other.getAutonumberEntity();

		javaClass = other.getJavaClass();
		defaultComponentType = other.getDefaultComponentType();
		scale = other.getScale();
		precision = other.getPrecision();
		formatInput = other.getFormatInput();
		formatOutput = other.getFormatOutput();

		defaultForeignId = other.getDefaultForeignId();
		defaultValue = other.getDefaultValue();

		readonly = other.isReadonly();
		unique = other.isUnique();
		hidden = other.isHidden();
		nullable = other.isNullable();
		searchable = other.isSearchable();
		modifiable = other.isModifiable();
		insertable = other.isInsertable();
		logBookTracking = other.isLogBookTracking();
		showMnemonic = other.isShowMnemonic();
		permissiontransfer = other.isPermissionTransfer();
		indexed = other.isIndexed();
		dynamic = other.isDynamic();

		calcBaseFieldUID = other.getCalcBaseFieldUID();
		calcFunction = other.getCalcFunction();
		calcAttributeDS = other.getCalcAttributeDS();
		calcAttributeParamValues = other.getCalcAttributeParamValues();
		calcOndemand = other.isCalcOndemand();
		calcAttributeAllowCustomization = other.isCalcAttributeAllowCustomization();
		sortorderASC = other.getSortorderASC();
		sortorderDESC = other.getSortorderDESC();

		fallbacklabel = other.getFallbackLabel();
		localeResourceIdForLabel = other.getLocaleResourceIdForLabel();
		localeResourceIdForDescription = other.getLocaleResourceIdForDescription();

		defaultMandatory = other.getDefaultMandatory();

		onDeleteCascade = other.isOnDeleteCascade();
		order = other.getOrder();

		calculationScript = other.getCalculationScript();
		backgroundColorScript = other.getBackgroundColorScript();
		
		localized = other.isLocalized();
		
		dataLangDBColumn = other.getDataLangDBColumn();
	}
	
	public boolean equalAll(FieldMeta<?> that) {
		return 
			RigidUtils.equal(this.getUID(), that.getUID()) &&
			RigidUtils.equal(this.isColumnMaster(), that.isColumnMaster()) &&
			RigidUtils.equal(this.getEntity(), that.getEntity()) &&
			RigidUtils.equal(this.getFieldGroup(), that.getFieldGroup()) &&
			RigidUtils.equal(this.getDbColumn(), that.getDbColumn()) &&
			RigidUtils.equal(this.getForeignIntegrationPoint(), that.getForeignIntegrationPoint()) &&
			RigidUtils.equal(this.getForeignEntity(), that.getForeignEntity()) &&
			RigidUtils.equal(this.getForeignEntityField(), that.getForeignEntityField()) &&
			RigidUtils.equal(this.getUnreferencedForeignEntity(), that.getUnreferencedForeignEntity()) &&
			RigidUtils.equal(this.getUnreferencedForeignEntityField(), that.getUnreferencedForeignEntityField()) &&
			RigidUtils.equal(this.getLookupEntity(), that.getLookupEntity()) &&
			RigidUtils.equal(this.getLookupEntityField(), that.getLookupEntityField()) &&
			RigidUtils.equal(this.getSearchField(), that.getSearchField()) &&
			RigidUtils.equal(this.getAutonumberEntity(), that.getAutonumberEntity()) &&
			RigidUtils.equal(this.getJavaClass(), that.getJavaClass()) &&
			RigidUtils.equal(this.getDefaultComponentType(), that.getDefaultComponentType()) &&
			RigidUtils.equal(this.getScale(), that.getScale()) &&
			RigidUtils.equal(this.getPrecision(), that.getPrecision()) &&
			RigidUtils.equal(this.getFormatInput(), that.getFormatInput()) &&
			RigidUtils.equal(this.getFormatOutput(), that.getFormatOutput()) &&
			RigidUtils.equal(this.getDefaultValue(), that.getDefaultValue()) &&
			RigidUtils.equal(this.getDefaultForeignId(), that.getDefaultForeignId()) &&
			RigidUtils.equal(this.isReadonly(), that.isReadonly()) &&
			RigidUtils.equal(this.isUnique(), that.isUnique()) &&
			RigidUtils.equal(this.isNullable(), that.isNullable()) &&
			RigidUtils.equal(this.isSearchable(), that.isSearchable()) &&
			RigidUtils.equal(this.isModifiable(), that.isModifiable()) &&
			RigidUtils.equal(this.isInsertable(), that.isInsertable()) &&
			RigidUtils.equal(this.isLogBookTracking(), that.isLogBookTracking()) &&
			RigidUtils.equal(this.isShowMnemonic(), that.isShowMnemonic()) &&
			RigidUtils.equal(this.isPermissionTransfer(), that.isPermissionTransfer()) &&
			RigidUtils.equal(this.isIndexed(), that.isIndexed()) &&
			RigidUtils.equal(this.isDynamic(), that.isDynamic()) &&
			RigidUtils.equal(this.getCalcBaseFieldUID(), that.getCalcBaseFieldUID()) &&
			RigidUtils.equal(this.getCalcFunction(), that.getCalcFunction()) &&
			RigidUtils.equal(this.getCalcAttributeDS(), that.getCalcAttributeDS()) &&
			RigidUtils.equal(this.getCalcAttributeParamValues(), that.getCalcAttributeParamValues()) &&
			RigidUtils.equal(this.isCalcOndemand(), that.isCalcOndemand()) &&
			RigidUtils.equal(this.isCalcAttributeAllowCustomization(), that.isCalcAttributeAllowCustomization()) &&
			RigidUtils.equal(this.getSortorderASC(), that.getSortorderASC()) &&
			RigidUtils.equal(this.getSortorderDESC(), that.getSortorderDESC()) &&
			RigidUtils.equal(this.getFallbackLabel(), that.getFallbackLabel()) &&
			RigidUtils.equal(this.getLocaleResourceIdForLabel(), that.getLocaleResourceIdForLabel()) &&
			RigidUtils.equal(this.getLocaleResourceIdForDescription(), that.getLocaleResourceIdForDescription()) &&
			RigidUtils.equal(this.getDefaultMandatory(), that.getDefaultMandatory()) &&
			RigidUtils.equal(this.isOnDeleteCascade(), that.isOnDeleteCascade()) &&
			RigidUtils.equal(this.getOrder(), that.getOrder()) &&
			RigidUtils.equal(this.getCalculationScript(), that.getCalculationScript()) &&
			RigidUtils.equal(this.getBackgroundColorScript(), that.getBackgroundColorScript()) &&
			RigidUtils.equal(this.getDataLangDBColumn(), that.getDataLangDBColumn()) &&
			RigidUtils.equal(this.isLocalized(), that.isLocalized());
	}
	
	public UID getUID() {
		return primaryKey;
	}
	
	public void setUID(UID primaryKey) {
		this.primaryKey = primaryKey;
	}

	@Override
	public String getFieldName() {
		return field;
	}

	@Override
	public String getDbColumn() {
		return dbColumn;
	}
	
	@Override
	public String getDataLangDBColumn() {
		return this.dataLangDBColumn;	
	}
	
	public void setDataLangDBColumn(String column) {
		this.dataLangDBColumn = column;
	}
	
	@Override
	public Class<T> getJavaClass() {
		return javaClass;
	}

	@Override
	public UID getEntity() {
		return entity;
	}	
	
	public void setEntity(UID entity) {
		this.entity = entity;
	}

	public void setFieldGroup(UID fieldGroup) {
		this.fieldGroup = fieldGroup;
	}

	public void setFieldName(String field) {
		this.field = field;
	}

	public void setDbColumn(String dbColumn) {
		this.dbColumn = dbColumn;
	}

	@SuppressWarnings("unchecked")
	public void setDataType(String dataType) {
		try {
			this.javaClass = (Class<T>) RigidUtils.getClassLoaderThatWorksForWebStart().loadClass(dataType);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public String getDefaultComponentType() {
		if (defaultComponentType == null) {
			if (getForeignEntity() != null && !isFileDataType())
				return !isSearchable() ? DefaultComponentTypes.COMBOBOX : DefaultComponentTypes.LISTOFVALUES;
			if (getLookupEntity() != null)
				return !isSearchable() ? DefaultComponentTypes.COMBOBOX : DefaultComponentTypes.LISTOFVALUES;
		}
		return defaultComponentType;
	}

	public void setDefaultComponentType(String defaultComponentType) {
		this.defaultComponentType = defaultComponentType;
	}

	public void setScale(Integer scale) {
		this.scale = scale;
	}

	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	public void setFormatInput(String formatInput) {
		this.formatInput = formatInput;
	}

	public void setFormatOutput(String formatOutput) {
		this.formatOutput = formatOutput;
	}

	public void setDefaultForeignId(Long defaultForeignId) {
		this.defaultForeignId = defaultForeignId;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setReadonly(Boolean readonly) {
		this.readonly = Boolean.TRUE.equals(readonly);
	}

	public void setUnique(Boolean unique) {
		this.unique = Boolean.TRUE.equals(unique);
	}

	public void setHidden(Boolean hidden) {
		this.hidden = Boolean.TRUE.equals(hidden);
	}

	public void setNullable(Boolean nullable) {
		this.nullable = Boolean.TRUE.equals(nullable);
	}

	public void setSearchable(Boolean searchable) {
		this.searchable = Boolean.TRUE.equals(searchable);
	}

	public void setModifiable(Boolean modifiable) {
		this.modifiable = Boolean.TRUE.equals(modifiable);
	}

	public void setInsertable(Boolean insertable) {
		this.insertable = Boolean.TRUE.equals(insertable);
	}

	public void setLogBookTracking(Boolean logBookTracking) {
		this.logBookTracking = Boolean.TRUE.equals(logBookTracking);
	}

	public void setShowMnemonic(Boolean showMnemonic) {
		this.showMnemonic = Boolean.TRUE.equals(showMnemonic);
	}
	
	public void setCalcBaseFieldUID(UID calcBaseFieldUID) {
		this.calcBaseFieldUID = calcBaseFieldUID;
	}

	public void setCalcFunction(String calcFunction) {
		this.calcFunction = calcFunction;
	}
	
	public void setCalcAttributeDS(UID calcAttributeDS) {
		this.calcAttributeDS = calcAttributeDS;
	}
	
	public void setCalcAttributeParamValues(String calcAttributeParamValues) {
		this.calcAttributeParamValues = calcAttributeParamValues;
	}
	
	public void setCalcOndemand(Boolean calcOndemand) {
		this.calcOndemand = Boolean.TRUE.equals(calcOndemand);
	}
	
	public void setCalcAttributeAllowCustomization(Boolean calcAttributeAllowCustomization) {
		this.calcAttributeAllowCustomization = Boolean.TRUE.equals(calcAttributeAllowCustomization);
	}

	public void setSortorderASC(String sortorderASC) {
		this.sortorderASC = sortorderASC;
	}

	public void setSortorderDESC(String sortorderDESC) {
		this.sortorderDESC = sortorderDESC;
	}

	public void setLocaleResourceIdForLabel(String localeResourceIdForLabel) {
		this.localeResourceIdForLabel = localeResourceIdForLabel;
	}

	public void setLocaleResourceIdForDescription(
		String localeResourceIdForDescription) {
		this.localeResourceIdForDescription = localeResourceIdForDescription;
	}

	@Override
	public UID getFieldGroup() {
		return fieldGroup;
	}

	@Override
	public Integer getScale() {
		return scale;
	}

	@Override
	public Integer getPrecision() {
		return precision;
	}

	@Override
	public String getFormatInput() {
		return formatInput;
	}

	@Override
	public String getFormatOutput() {
		return formatOutput;
	}

	@Override
	public Long getDefaultForeignId() {
		return defaultForeignId;
	}

	@Override
	public String getDefaultValue() {
		return defaultValue;
	}

	@Override
	public boolean isReadonly() {
		return readonly;
	}

	@Override
	public boolean isUnique() {
		return unique;
	}

	@Override
	public boolean isHidden() {
		return hidden;
	}

	@Override
	public boolean isNullable() {
		return nullable;
	}

	@Override
	public boolean isSearchable() {
		return searchable;
	}

	@Override
	public boolean isModifiable() {
		return modifiable;
	}

	@Override
	public boolean isInsertable() {
		return insertable;
	}

	@Override
	public boolean isLogBookTracking() {
		return logBookTracking;
	}

	@Override
	public boolean isShowMnemonic() {
		return showMnemonic;
	}
	
	@Override
	public UID getCalcBaseFieldUID() {
		return calcBaseFieldUID;
	}

	@Override
	public String getCalcFunction() {
		return calcFunction;
	}

	@Override
	public String getCalcAttributeParamValues() {
		return calcAttributeParamValues;
	}
	
	@Override
	public UID getCalcAttributeDS() {
		return calcAttributeDS;
	}
	
	@Override
	public boolean isCalcOndemand() {
		return calcOndemand;
	}
	
	@Override
	public boolean isCalcAttributeAllowCustomization() {
		return calcAttributeAllowCustomization;
	}

	@Override
	public String getSortorderASC() {
		return sortorderASC;
	}

	@Override
	public String getSortorderDESC() {
		return sortorderDESC;
	}

	@Override
	public String getLocaleResourceIdForLabel() {
		return localeResourceIdForLabel;
	}

	@Override
	public String getLocaleResourceIdForDescription() {
		return localeResourceIdForDescription;
	}

	@Override
	public UID getForeignIntegrationPoint() {
		return foreignIntegrationPoint;
	}

	public void setForeignIntegrationPoint(final UID foreignIntegrationPoint) {
		this.foreignIntegrationPoint = foreignIntegrationPoint;
	}

	@Override
	public UID getForeignEntity() {
		return foreignEntity;
	}

	public void setForeignEntity(UID foreignEntity) {
		this.foreignEntity = foreignEntity;
	}

	@Override
	public String getForeignEntityField() {
		return foreignEntityField;
	}

	public void setForeignEntityField(String foreignEntityField) {
		this.foreignEntityField = foreignEntityField;
	}

	@Override
	public UID getUnreferencedForeignEntity() {
		return unreferencedForeignEntity;
	}

	public void setUnreferencedForeignEntity(UID unreferencedForeignEntity) {
		this.unreferencedForeignEntity = unreferencedForeignEntity;
	}

	@Override
	public String getUnreferencedForeignEntityField() {
		return unreferencedForeignEntityField;
	}

	public void setUnreferencedForeignEntityField(String unreferencedForeignEntityField) {
		this.unreferencedForeignEntityField = unreferencedForeignEntityField;
	}

	@Override
	public UID getLookupEntity() {
		return lookupEntity;
	}

	public void setLookupEntity(UID lookupEntity) {
		this.lookupEntity = lookupEntity;
	}

	@Override
	public String getLookupEntityField() {
		return lookupEntityField;
	}

	public void setLookupEntityField(String lookupEntityField) {
		this.lookupEntityField = lookupEntityField;
	}

	@Override
	public final String getSearchField() {
		return searchField;
	}

	public final void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	@Override
	public final UID getAutonumberEntity() {
		return autonumberEntity;
	}

	public final void setAutonumberEntity(UID autonumberEntity) {
		this.autonumberEntity = autonumberEntity;
	}

	@Override
	public boolean isPermissionTransfer() {
		return permissiontransfer;
	}

	public void setPermissionTransfer(Boolean permissiontransfer) {
		this.permissiontransfer = Boolean.TRUE.equals(permissiontransfer);
	}

	@Override
	public String getFallbackLabel() {
    	return fallbacklabel;
    }

	public void setFallbackLabel(String fallbacklabel) {
    	this.fallbacklabel = fallbacklabel;
    }

	@Override
	public boolean isDynamic() {
    	return dynamic;
    }

	public void setDynamic(boolean dynamic) {
    	this.dynamic = dynamic;
    }

	@Override
	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(Boolean indexed) {
		this.indexed = Boolean.TRUE.equals(indexed);
	}

	@Override
	public String getDefaultMandatory() {
		return defaultMandatory;
	}

	public void setDefaultMandatory(String defaultMandatory) {
		this.defaultMandatory = defaultMandatory;
	}

	@Override
	public boolean isOnDeleteCascade() {
		return onDeleteCascade;
	}

	public void setOnDeleteCascade(Boolean onDeleteCascade) {
		this.onDeleteCascade = Boolean.TRUE.equals(onDeleteCascade);
	}

	@Override
	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	@Override
	public NuclosScript getCalculationScript() {
		return calculationScript;
	}

	public void setCalculationScript(NuclosScript calculationScript) {
		this.calculationScript = calculationScript;
	}

	@Override
	public final NuclosScript getBackgroundColorScript() {
		return backgroundColorScript;
	}

	public final void setBackgroundColorScript(NuclosScript backgroundColorScript) {
		this.backgroundColorScript = backgroundColorScript;
	}

	@Override
	public boolean isColumnMaster() {
		return RigidUtils.defaultIfNull(this.columnMaster, super.isColumnMaster());
	}
	
	public void setColumnMaster(Boolean columnMaster) {
		this.columnMaster = columnMaster;
	}

	@Override
	public Boolean isLocalized() {
		return Boolean.TRUE.equals(this.localized);
	}

	public void setLocalized(Boolean isLocalized) {
		this.localized = Boolean.TRUE.equals(isLocalized);
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String getComment() {
		return this.comment;
	}
	
}
