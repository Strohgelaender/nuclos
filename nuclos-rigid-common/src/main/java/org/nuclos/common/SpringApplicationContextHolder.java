//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Singleton for getting spring beans <em>from the main spring context</em> 
 * if there no way to do that 'the spring way'. Works on client and server
 * site.
 * <p>
 * Before using this class you should <em>always</em> consider dependency
 * injection. On the server site, using @Configurable in <em>always</em> an option
 * to archive the same result, even if you are in a non-spring object.
 * <p>
 * TODO: This should be a real SINGLETON, i.e. only ONE static method (getInstance()).
 * All other static method should be turned into methods of the SINGLETON.
 * 
 * @author Thomas Pasch
 */
public class SpringApplicationContextHolder implements ApplicationContextAware {

	private static final Logger LOG = Logger.getLogger(SpringApplicationContextHolder.class);
	
	private static SpringApplicationContextHolder INSTANCE;

	private static AtomicInteger springReady = new AtomicInteger();
	
	private static List<SpringReadyListener> listeners = new ArrayList<SpringReadyListener>();
	
	// 
	
	private ApplicationContext applicationContext;
	
	private volatile boolean destroyed = false;

	/**
	 * private Constructor which
	 * initialize Spring ApplicationContext
	 */
	SpringApplicationContextHolder() {
		INSTANCE = this;
	}

	@PostConstruct
	void init() {
		final int i = springReady.incrementAndGet();
		LOG.info("Spring main context init: " + i);
	}
	
	@PreDestroy
	void destroy() {
		destroyed = true;
	}
	
	/**
	 * HACK
	 */
	public static void setSpringReady() {
		final int i = springReady.incrementAndGet();
		LOG.info("Spring main context setSpringReady: " + i);
		synchronized (listeners) {
			if (!listeners.isEmpty()) {
				Iterator<SpringReadyListener> it = listeners.iterator();
				while (it.hasNext()) {
					final SpringReadyListener l = it.next();
					final int springReadyState = springReady.intValue();
					if (springReadyState >= l.getMinReadyState()) {
						notifyListener(l);
						it.remove();
					}
				}
			}
		}
	}
	
	private static void notifyListener(SpringReadyListener l) {
		try {
			l.springIsReady();
		} catch (Exception ex) {
			LOG.error("Notify 'spring is ready' failed: " + ex.getMessage(), ex);
		}
	}
	
	private static SpringApplicationContextHolder getInstance()	{
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	/*
	 * loads the Spring ApplicationContext from config-File
	 *
	 * @return ApplicationContext
	 */
	public static ApplicationContext getApplicationContext() {
		return getInstance().applicationContext;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext context) {
		if(applicationContext != null)
			return;
		LOG.info("ApplicationContext set to " + context);
		applicationContext = context;
	}	
	
		
	/*
	 * try to find a Bean in the Spring ApplicationContext
	 * 
	 * @param strBean String
	 * @return Object
	 */
	public static Object getBean(String strBean) {		
		Object bean = null;
		try {
			bean = getApplicationContext().getBean(strBean);
		} catch (BeansException e) {
			throw new NuclosFatalException(e);
		} 
		return bean;
	}
	
	/*
	 * try to find a Bean in the Spring ApplicationContext
	 * 
	 * @param strBean String
	 * @return Object
	 */
	public static <T> T getBean(Class<T> c) {		
		T bean = null;
		try{
			bean = getApplicationContext().getBean(c);
		} catch (BeansException e) {
			throw new NuclosFatalException(e);
		} 
		return bean;
	}
	
	public static boolean containsBean(Class<?> c) {
		return !getApplicationContext().getBeansOfType(c).isEmpty();
	}

	public static boolean isSpringReady() {
		return isSpringReady(2, false);
	}
	
	public static boolean isSpringReady(boolean silent) {
		return isSpringReady(2, silent);
	}

	/**
	 * Determines if the Server startup is complete, i.e. login is possible and all (REST-)services can be used.
	 * 
	 * @return true, if the Server startup is complete
	 */
	public static boolean isNuclosReady() {
		return isSpringReady(3, false) && !isDestroyed();
	}

	public static void resetSpringReadyState() {
		LOG.info("Spring main context setSpringReady: RESET (0)");
		springReady.set(0);
	}

	private static boolean isSpringReady(final int minReadyState, boolean silent) {
		final int i = springReady.intValue();
		final boolean result = i >= minReadyState;
		if (!result && !silent) {
			LOG.warn("Spring main context not initialized, waiting ... : " + i);
		}
		return result;
	}
	
	public static boolean isDestroyed() {
		return getInstance().destroyed;
	}
	
	public static void addSpringReadyListener(SpringReadyListener l) {
		synchronized (listeners) {
			final int springReadyState = springReady.intValue();
			if (springReadyState >= l.getMinReadyState()) {
				notifyListener(l);
			} else {
				listeners.add(l);
			}
		}
	}
	
	public interface SpringReadyListener {
		void springIsReady();
		int getMinReadyState();
	}
	
}
