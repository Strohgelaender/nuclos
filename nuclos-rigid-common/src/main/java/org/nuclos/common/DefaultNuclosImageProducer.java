package org.nuclos.common;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

public class DefaultNuclosImageProducer implements INuclosImageProducer, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2978076790499789080L;
	protected static final Logger LOG = Logger.getLogger(DefaultNuclosImageProducer.class);

	@Override
	public BufferedImage getBufferedImage(byte[] b) throws IOException {
		if (b == null || b.length == 0) {
			return null;
		}
		
		return ImageIO.read(new ByteArrayInputStream(b));
	}
	
	@Override
	public byte[] getScaledImageBytes(byte[] b, Dimension d) throws IOException {
		return getScaledImageBytesFromBufferdImage(getBufferedImage(b), d);
	}

	@Override
	public byte[] getThumbNail(BufferedImage buff) throws IOException {
		return getScaledImageBytesFromBufferdImage(buff, getThumbnailSize());
	}

	protected byte[] getScaledImageBytesFromBufferdImage(BufferedImage buff, Dimension d) throws IOException {
		if (buff == null) {
			return new byte[0];
		}
		
		int iWidth = d.width;
		int iHeight = d.height;
		
		if (buff.getHeight() < iHeight) {
			iHeight = buff.getHeight();
			iWidth = buff.getWidth();
		}

		BufferedImage bdest = new BufferedImage(iWidth, iHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bdest.createGraphics();
	    AffineTransform at = AffineTransform.getScaleInstance((double)iWidth/buff.getWidth(), (double)iHeight/buff.getHeight());
	    g.drawRenderedImage(buff, at);
	    					
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ImageIO.write(bdest, "PNG", baos);
	    baos.flush();
	    byte[] bscaled = baos.toByteArray();
	    baos.close();
	    
	    return bscaled;
   }

	protected Dimension getThumbnailSize() {
		int iHeight = DEFAULTTHUMBSIZE;
		int iWidth = DEFAULTTHUMBSIZE;
		
		Object o = SpringApplicationContextHolder.getBean("parameterProvider");
		if (o instanceof ParameterProvider) {
			ParameterProvider pp = (ParameterProvider) o;
			String sThumbnailsize = pp.getValue(ParameterProvider.KEY_THUMBAIL_SIZE);
		
			try {
				String s[] = sThumbnailsize.split("\\*");
				iWidth = Integer.parseInt(s[0]);
				iHeight = Integer.parseInt(s[1]);
			}
			catch(Exception e) {
				LOG.debug("produceThumbnail: " + e);
			}
		}
		
		return new Dimension(iWidth, iHeight);
	}
	
}
