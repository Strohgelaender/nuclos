package org.nuclos.common;

import java.io.Serializable;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * class Version
 */
public class Version implements Serializable, Comparable<Version> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3996077843536827040L;

	private static final Logger LOG = Logger.getLogger(Version.class);
	
	private static final Pattern SIMPLE_VERSION_PAT = Pattern.compile("(\\D*\\d+\\.\\d+)(\\.\\d+)*(.*)");

	private static final Pattern MAJOR_MINOR_VERSION_PAT = Pattern.compile("(\\D*\\d+\\.\\d+)(.*)");
	
	private static final Pattern PREFIX_PAT = Pattern.compile("\\D*(.*)");

	private final String appId;
	private final String appName;
	private final String versionNumber;
	private final String versionDate;
	
	private transient VersionNumber vn;
	private transient String simpleVersion;
	private transient String majorMinorVersion;
	private transient boolean snapshot;

	public Version(String appId, String appName, String sVersionNumber, String sVersionDate) {
		this.appId = appId;
		this.appName = appName;
		this.versionNumber = sVersionNumber;
		this.versionDate = sVersionDate;
		this.vn = new VersionNumber(stripPrefix(sVersionNumber));
	}
	
	private static String stripPrefix(String s) {
		final Matcher m = PREFIX_PAT.matcher(s);
		if (m.matches()) {
			final String result = m.group(1);
			if (!s.equals(result)) {
				LOG.info("real version number " + s + " striped to " + result);
			}
			return result;
		}
		LOG.warn("Can't strip prefix on version number " + s);
		return s;
	}

	public String getAppId() {
		return appId;
	}

	public String getAppName() {
		return appName;
	}

	/**
	 * Returns the original (Maven) version number.
	 *
	 * @return
	 */
	public String getVersionNumber() {
		return this.versionNumber;
	}

	public String getMajorMinorVersionNumber() {
		if (majorMinorVersion == null) {
			final Matcher m = MAJOR_MINOR_VERSION_PAT.matcher(versionNumber);
			if (!m.matches()) {
				throw new IllegalArgumentException(versionNumber);
			}
			majorMinorVersion = m.group(1);
		}
		return majorMinorVersion;
	}
	
	public String getSimpleVersionNumber() {
		if (simpleVersion == null) {
			if (!versionNumber.isEmpty()) {
				final Matcher m = SIMPLE_VERSION_PAT.matcher(versionNumber);
				if (!m.matches()) {
					throw new IllegalArgumentException(versionNumber);
				}
				final StringBuilder result = new StringBuilder(m.group(1));
				if (m.group(2) != null) {
					result.append(m.group(2));
				}
				if (m.group(3).indexOf("SNAPSHOT") >= 0) {
					result.append(" BETA");
					snapshot = true;
				} else {
					snapshot = false;
				}
				simpleVersion = result.toString();
			} else {
				simpleVersion = "";
			}
		}
		return simpleVersion;
	}
	
	public boolean isSnapshot() {
		// implizit initialize snaphot
		getSimpleVersionNumber();
		
		return snapshot;
	}

	public Date getVersionDate() {
		if ("${maven.build.timestamp}".equals(versionDate)) {
			// We are on eclipse development
			return null;
		}
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(versionDate);
		}
		catch (ParseException e) {
			throw new NuclosFatalException(e);
		}
	}

	public String getVersionDateString() {
		return this.versionDate;
	}
	
	private VersionNumber getVN() {
		if (vn == null) {
			vn = new VersionNumber(stripPrefix(versionNumber));
		}
		return vn;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if ((o == null) || !(o instanceof Version)) {
			return false;
		}
		final Version that = (Version) o;
		boolean result = getVN().equals(that.getVN());
		// We added bugfix to SNAPSHOTS e.g. v4.3.6-SNAPSHOT.
		// No need to compare with version date any more...
		/*
		if (result && isSnapshot()) {
			if (getVersionDate() != null && that.getVersionDate() != null) {
				result = getVersionDate().equals(that.getVersionDate());
			}
		}
		*/
		return result;
	}
	
	@Override
	public int hashCode() {
		int result = 3 * versionNumber.hashCode() + 9371;
		// We added bugfix to SNAPSHOTS e.g. v4.3.6-SNAPSHOT.
		// No need to compare with version date any more...
		/*
		if (isSnapshot()) {
			final Date date = getVersionDate();
			if (date == null) {
				result += 5 * versionDate.hashCode();
			} else {
				result += 7 * date.hashCode();
			}
		}
		*/
		return result;
	}

	@Override
	public int compareTo(Version o) {
		if (o == null) return 1;
		int result = getVN().compareTo(o.getVN());
		// We added bugfix to SNAPSHOTS e.g. v4.3.6-SNAPSHOT.
		// No need to compare with version date any more...
		/*
		if (result == 0) {
			final Date date = getVersionDate();
			if (date != null && o.getVersionDate() != null) {
				result = date.compareTo(o.getVersionDate());
			}
		}
		*/
		return result;
	}

	/**
	 * @return application name and version.
	 */
	public String getShortName() {
		return getAppName() + " V" + getSimpleVersionNumber();
	}

	/**
	 * @return application name and version, including version date.
	 */
	public String getLongName() {
		return getAppName() + " V" + getSimpleVersionNumber() + " (" + getVersionDateString() + ")";
	}

	@Override
	public String toString() {
		return getAppName() + " V" + getSimpleVersionNumber() + " (" + getVersionDateString() + ")";
	}

	static Version fromProperties(Properties props, String prefix) {
		try
		{
			String id = props.getProperty(prefix + ".id");
			String name = props.getProperty(prefix + ".name");
			if (StringUtils.isEmpty(id) && StringUtils.isEmpty(name)) {
				throw new Exception("id and name empty");
			}
			String version = props.getProperty(prefix + ".version.number");
			String bugfixVersion = props.getProperty(prefix + ".version.number.bugfix");
			if (bugfixVersion != null && !bugfixVersion.isEmpty()) {
				version = version + "." + bugfixVersion;
			}
			String versionDate = props.getProperty(prefix + ".version.date");
			return new Version(id != null ? id : name, name, version, versionDate);
		}catch(Exception e)
		{
			LOG.warn("Some fields of version properties are missing. Nuclos version is used instead.");
			return null;
		}
	}

	public static Version readNuclosVersion(ClassLoader cl) {
		final Properties props = new Properties();
		URL url;
		try {
			url = cl.getResource("nuclos-version.properties");
			LOG.info("nuclos-version.properties loaded from " + url);
			if (url != null) {
				props.load(url.openStream());
			}
			return fromProperties(props, "nuclos");
		} catch (Exception ex) {
			throw new NuclosFatalException("Error reading nuclos-version.properties", ex);
		}
	}

}	// inner class Version