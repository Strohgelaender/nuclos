package org.nuclos.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.TranscodingHints;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.util.SVGConstants;
import org.apache.commons.io.FileUtils;

public class SVGImageProducer extends DefaultNuclosImageProducer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4437966319498191335L;

	@Override
	public BufferedImage getBufferedImage(byte[] b) throws IOException {
		if (b == null || b.length == 0) {
			return null;
		}
		
		return getSVGImage(b);
	}

	private BufferedImage getSVGImage(byte []b) throws IOException {
		final BufferedImage[] imagePointer = new BufferedImage[1];
		
		String css = "svg {" +
	            "shape-rendering: geometricPrecision;" +
	            "text-rendering:  geometricPrecision;" +
	            "color-rendering: optimizeQuality;" +
	            "image-rendering: optimizeQuality;" +
	            "}";
	    File cssFile = File.createTempFile("batik-default-override-", ".css");
	    FileUtils.writeStringToFile(cssFile, css);

	    TranscodingHints transcoderHints = new TranscodingHints();
	    transcoderHints.put(ImageTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE);
	    transcoderHints.put(ImageTranscoder.KEY_DOM_IMPLEMENTATION,
	            SVGDOMImplementation.getDOMImplementation());
	    transcoderHints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT_NAMESPACE_URI,
	            SVGConstants.SVG_NAMESPACE_URI);
	    transcoderHints.put(ImageTranscoder.KEY_DOCUMENT_ELEMENT, "svg");
	    transcoderHints.put(PNGTranscoder.KEY_MAX_WIDTH, new Float(800));
	    transcoderHints.put(PNGTranscoder.KEY_MAX_HEIGHT, new Float(500));
	    transcoderHints.put(ImageTranscoder.KEY_USER_STYLESHEET_URI, cssFile.toURI().toString());

		try {
			
			TranscoderInput input = new TranscoderInput(new ByteArrayInputStream(b));
			ImageTranscoder t = new ImageTranscoder() {

	            @Override
	            public BufferedImage createImage(int w, int h) {
	                return new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
	            }

	            @Override
	            public void writeImage(BufferedImage image, TranscoderOutput out)
	                    throws TranscoderException {
	                imagePointer[0] = image;
	            }
	        };
	        t.setTranscodingHints(transcoderHints);
	        t.transcode(input, null);
		}	        
        catch (TranscoderException ex) {
        	LOG.debug(ex.getMessage());
            return null;
        }
		finally {
			cssFile.delete();
		}
        
        return imagePointer[0];			
	}
	
}
