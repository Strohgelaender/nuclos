export { ${ADDON_NAME_CAMEL_CASE}Module } from './${ADDON_FILE_NAME}.module';
export { ${ADDON_NAME_CAMEL_CASE}Service } from './${ADDON_FILE_NAME}.service';
export { ${ADDON_NAME_CAMEL_CASE}Component } from './${ADDON_FILE_NAME}.component';
