import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObjectEventListener } from '../../entity-object-data/shared/entity-object-event-listener';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { FqnService } from '../../shared/fqn.service';
import { ActionExecutorFactory } from '../action/action-executor-factory';
import { RefreshValuelistActionExecutor } from '../action/refresh-valuelist-action-executor';

export class RuleExecutor implements EntityObjectEventListener {

	private static ruleAppliesTo(
		rule: Rule,
		meta: EntityMeta,
		attributeName: string
	): boolean {
		return FqnService.toFqn(meta, attributeName) === rule.event.sourcecomponent;
	}

	private static getExecutor(rule: Rule, action) {
		return ActionExecutorFactory.newExecutor(rule.event, action);
	}


	constructor(
		private rules: Rules,
		private dataService: DataService
	) {
	}

	afterAttributeChange(
		entityObject: EntityObject,
		attributeName: string,
		oldValue: any,
		newValue: any
	) {
		Logger.instance.debug('attributeChanged: %o, %o, %o, %o', entityObject, attributeName, oldValue, newValue);
		entityObject.getMeta().subscribe(meta => {
			this.executeRules(meta, entityObject, attributeName);
		});
	}

	afterReload(entityObject: EntityObject) {
		this.initializeVlps(entityObject);
	}

	afterCancel(entityObject: EntityObject) {
		this.initializeVlps(entityObject);
	}

	initializeVlps(entityObject: EntityObject) {
		if (this.rules.rules) {
			for (let rule of this.rules.rules) {
				for (let action of rule.actions) {
					// TODO: Make this type safe
					if (action['_type'] === 'RuleActionRefreshValuelist') {
						let executor = RuleExecutor.getExecutor(rule, action);
						(executor as RefreshValuelistActionExecutor).setVlpParameters(
							entityObject
						);
					}
				}
			}
		}

		Logger.instance.debug('VLP parameters initialized on %o: %o', entityObject.getTitle() || entityObject, entityObject.getVlpContext());
	}

	private executeRules(
		meta: EntityMeta,
		entityObject: EntityObject,
		attributeName: string
	) {
		if (!this.rules.rules) {
			return;
		}

		for (let rule of this.rules.rules) {
			if (RuleExecutor.ruleAppliesTo(rule, meta, attributeName)) {
				this.executeActions(rule, entityObject);
			}
		}
	}

	/**
	 * Executes all actions of the given Rule on the given EO.
	 */
	private executeActions(rule: Rule, eo: EntityObject) {
		for (let action of rule.actions) {
			this.executeAction(rule, action, eo);
		}
	}

	private executeAction(rule: Rule, action, eo: EntityObject) {
		let executor = RuleExecutor.getExecutor(rule, action);
		executor.execute(eo, this.dataService);
	}
}

