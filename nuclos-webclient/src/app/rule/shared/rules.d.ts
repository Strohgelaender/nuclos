
interface ObjectFactory {
}

interface Rule extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	event: RuleEvent;
	actions: RuleAction[];
	name: string;
}

interface RuleAction extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	targetcomponent: string;
}

interface RuleActionClear extends RuleAction, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	entity: string;
}

interface RuleActionEnable extends RuleAction, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	invertable: boolean;
}

interface RuleActionRefreshValuelist extends RuleAction, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	entity: string;
	parameterForSourcecomponent: string;
}

interface RuleActionReinitSubform extends RuleAction, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	entity: string;
}

interface RuleActionTransferLookedupValue extends RuleAction, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	sourcefield: string;
}

interface RuleEvent extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	type: string;
	entity: string;
	sourcecomponent: string;
}

interface Rules extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	rules: Rule[];
}

interface Serializable {
}

interface Cloneable {
}

interface CopyTo {
}

interface Equals {
}

interface HashCode {
}

interface ToString {
}
