import { Injectable } from '@angular/core';
import {
	ISecurityService,
	UserAction
} from '@nuclos/nuclos-addon-api';
import { AuthenticationService } from '../../authentication';
import { SecurityModule } from '../security.module';

@Injectable({
	providedIn: SecurityModule
})
export class SecurityService implements ISecurityService {

	constructor(
		private authService: AuthenticationService,
	) {
	}

	isUserActionAllowed(action: UserAction): boolean {
		return this.authService.isActionAllowed(action);
	}
}
