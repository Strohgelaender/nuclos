import { Injectable } from '@angular/core';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { WindowTitleService } from '../../entity-object/shared/window-title.service';
import { EntityObject } from './entity-object.class';

@Injectable()
export class EntityObjectEventService {
	static instance: EntityObjectEventService;

	private selectedEo: BehaviorSubject<IEntityObject | undefined>;
	private deletedEo: Subject<IEntityObject>;
	private savedEo: Subject<IEntityObject>;
	private addEo: Subject<IEntityObject>;
	private createdEo: Subject<IEntityObject>;
	private resetEo: Subject<IEntityObject>;
	private modifiedEo = new Subject<IEntityObject>();
	private stateChanged = new Subject<IEntityObject>();

	constructor(
		private windowTitleService: WindowTitleService,
	) {
		EntityObjectEventService.instance = this;
		this.selectedEo = new BehaviorSubject<IEntityObject | undefined>(undefined);
		this.deletedEo = new Subject<IEntityObject>();
		this.savedEo = new Subject<IEntityObject>();
		this.addEo = new Subject<IEntityObject>();
		this.createdEo = new Subject<IEntityObject>();
		this.resetEo = new Subject<IEntityObject>();

		this.selectedEo.subscribe(
			(eo: EntityObject) => {
				if (eo) {
					this.setWindowTitleFromEo(eo);
				} else {
					this.windowTitleService.setDefaultWindowTitle();
				}
			}
		);
	}

	/**
	 * fires when another EO gets selected
	 */
	observeSelectedEo() {
		return this.selectedEo.pipe(distinctUntilChanged());
	}

	/**
	 * Emits the deleted EOs.
	 */
	observeDeletedEo() {
		return this.deletedEo.pipe(distinctUntilChanged());
	}

	/**
	 * Emits the saved EOs.
	 */
	observeSavedEo() {
		return this.savedEo.pipe(distinctUntilChanged());
	}

	observeEoModification() {
		return this.modifiedEo;
	}

	observeStateChanges() {
		return this.stateChanged;
	}

	/**
	 * Emits new added but unsaved EOs.
	 */
	observeAddEo(): Observable<IEntityObject> {
		return this.addEo;
	}

	/**
	 * Emits new created EOs.
	 */
	observeCreatedEo(): Observable<IEntityObject> {
		return this.createdEo;
	}

	/**
	 * Emits the IDs of reset EOs.
	 */
	observeResetEo() {
		return this.resetEo;
	}

	emitSelectedEo(eo: IEntityObject | undefined) {
		this.selectedEo.next(eo);
	}

	emitModifiedEo(eo: IEntityObject) {
		this.modifiedEo.next(eo);
	}

	emitStateChange(eo: IEntityObject) {
		this.stateChanged.next(eo);
	}

	emitSavedEo(eo: IEntityObject) {
		this.savedEo.next(eo);
	}

	emitDeletedEo(eo: IEntityObject) {
		this.deletedEo.next(eo);
	}

	emitCreatedEo(eo: IEntityObject) {
		this.createdEo.next(eo);
	}

	emitAddEo(eo: IEntityObject) {
		this.addEo.next(eo);
	}

	emitResetEo(eo: IEntityObject) {
		this.resetEo.next(eo);
	}

	/**
	 * TODO: The EO title is not always available:
	 * If the EO is initially selected from the result list, it must be reloaded first.
	 * Only then will the title be available.
	 */
	private setWindowTitleFromEo(eo: EntityObject) {
		let title = eo.getTitle();
		if (title) {
			this.windowTitleService.setWindowTitle(title);
		} else {
			this.windowTitleService.setDefaultWindowTitle();
		}
	}
}
