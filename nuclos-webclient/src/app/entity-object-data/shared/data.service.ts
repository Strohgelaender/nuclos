import { Injectable } from '@angular/core';
import { Headers, Response, URLSearchParams } from '@angular/http';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AddonService } from '../../addons/addon.service';
import { WsTab } from '../../explorertrees/explorertrees.model';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import {
	ColumnAttribute,
	Preference,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { SearchService } from '../../search/shared/search.service';
import { SearchfilterService } from '../../search/shared/searchfilter.service';
import { DatetimeService } from '../../shared/datetime.service';
import { FqnService } from '../../shared/fqn.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { BoAttr, BoViewModel, EntityAttrMeta, EntityMeta, InputType } from './bo-view.model';
import { EntityObject } from './entity-object.class';
import { ResultParams } from './result-params';

export interface Column {
	boAttrId: string;
}
interface Alias {
	[key: string]: Column
}
export interface WhereCondition {
	aliases: Alias;
	clause: string;
}

/**
 * TODO: Split this into more meaningful separate services, instead of one monolith for everything.
 * TODO: Methods are too complex and have too many parameters.
 */
@Injectable()
export class DataService {

	/**
	 * Validates and formats the search input value according to the "input type" of the attribute.
	 */
	private static formatSearchTemplateValue(
		value: any,
		attribute: BoAttr,
		useTicks: boolean
	): string {

		// if (attribute.inputType === 'reference') {
		if (attribute.reference) {
			value = (value && value.id) ? value.id : value;
		}

		if (value !== undefined
			&& value.length === undefined
			&& (
				attribute.inputType === InputType.NUMBER
				|| attribute.inputType === InputType.REFERENCE
				|| attribute.inputType === InputType.DATE
			)
			|| value !== undefined && value.length > 0) {

			if (useTicks) {
				if (attribute.inputType === InputType.DATE) {
					value = moment(value).format('YYYY-MM-DD');
				}
				if (typeof value !== 'number') {
					value = value.replace(/'/g, ''); // remove '
				}
			}
			if (useTicks) {
				value = '\'' + value + '\'';
			}
		} else {
			value = null;
		}

		return value;
	}

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService,
		private fqnService: FqnService,
		private searchService: SearchService,
		private searchfilterService: SearchfilterService,
		private addonService: AddonService,
		private i18n: NuclosI18nService,
		private dateTimeService: DatetimeService,
		private $log: Logger
	) {
	}


	/**
	 * TODO: Too many parameters!
	 */
	loadList(
		meta: EntityMeta,
		vlpId: string | undefined,
		vlpParams: URLSearchParams | undefined,
		sideviewmenuPreference?: Preference<SideviewmenuPreferenceContent>,
		searchtemplatePreference?: Preference<SearchtemplatePreferenceContent>,
		resultParams = ResultParams.DEFAULT
	): Observable<BoViewModel> {

		let columns: (ColumnAttribute|undefined)[] = sideviewmenuPreference ? sideviewmenuPreference.content.columns.filter(c => c.selected) : [];
		let sort = '';
		if (sideviewmenuPreference && sideviewmenuPreference.content.columns) {
			sort = this.getSortString(sideviewmenuPreference.content.columns);
		}

		let searchInputText = this.searchService.getCurrentSearchInputText();

		let searchFilter = undefined;

		// add attributes for title and info
		let attributeFqns: (string|undefined)[] = columns.map(a => a ? a.boAttrId : undefined);
		let titleAttributeFqns: string[] = this.fqnService.parseFqns(meta.getTitlePattern());
		let infoAttributeFqns: string[] = this.fqnService.parseFqns(meta.getInfoPattern());

		// add attributes required by addons
		let requiredAddonAttributeFqns: string[] = this.addonService.getRequiredResultlistAttributeNames(meta.getEntityClassId())
			.map(attrName => meta.getEntityClassId() + '_' + attrName);

		columns = Array
			.from(new Set<string|undefined>([...attributeFqns, ...titleAttributeFqns, ...infoAttributeFqns, ...requiredAddonAttributeFqns]))

			// filter out columns from other EO's (at the moment only supported by the Java-client result list)
			.filter(fqn => fqn && fqn.startsWith(meta.getEntityClassId() + '_'))

			.map(fqn => {
					if (fqn) {
						let attr = meta.getAttributeMetaByFqn(fqn);
						if (attr) {
							return {
								boAttrId: attr.getAttributeID(),
							} as ColumnAttribute
						}
					}
					return undefined;
				}
			)
			.filter(fqn => fqn !== undefined);

		let filter = this.buildFilter(
			meta,
			searchInputText,
			searchFilter,
			vlpId,
			vlpParams,
			columns,
			sort
		);

		_.forOwn(resultParams, (value, key) => filter.append(key, value as string));

		let queryObject: {where: WhereCondition | undefined} = {
			where: undefined
		};

		let cols: SearchtemplateAttribute[] = [];
		if (searchtemplatePreference && searchtemplatePreference.content.columns) {
			cols = searchtemplatePreference.content.columns;
		}
		queryObject.where = this.buildWhereCondition(
			cols,
			meta,
			this.searchfilterService.getAdditionalWhereConditions(meta.getEntityClassId())
		);

		if (meta.getEmptyResultListIfNoSearchCondition()
			&& queryObject.where !== undefined && queryObject.where.clause.length === 0  && searchInputText.length === 0) {
			this.$log.info('Search will be executed only if a search critera is set.');
			// actual a result list query call shouldn't be necessary here
			// but the result list response contains the 'canCreateBo' flag which will be evaluated for displaying the new-button
			// so just request an empty list
			// TODO create a REST service for retrieving 'canCreateBo' flag
			queryObject.where = { clause: meta.getEntityClassId() + '.id is null', aliases: {} };
		}

		return this.loadEoData(meta, filter, queryObject);
	}


	loadEoData(
		meta: EntityMeta,
		queryParams,
		queryObject: { where: WhereCondition | undefined } = { where: undefined }
	): Observable<BoViewModel> {
		return this.http.post(
			this.nuclosConfig.getRestHost() + '/bos/' + meta.getEntityClassId() + '/query',
			queryObject,
			{params: queryParams}
		).pipe(map((response: Response) => {
			let result: BoViewModel = response.json();
			let entityObjects: EntityObject[] = [];
			for (let bo of result.bos) {
				entityObjects.push(new EntityObject(<any>bo));
			}
			result.bos = entityObjects;
			return result;
		}));
	}

	/**
	 * Builds the 'AND'-concatenated WHERE condition based on the given search object.
	 *
	 * TODO: searchObject type
	 */
	private buildWhereCondition(attributes: SearchtemplateAttribute[],
								meta: EntityMeta,
								additionalWhereConditions: string[]): WhereCondition {
		let result: WhereCondition = {
			aliases: {},
			clause: ''
		};
		let index = 0;
		let andArray: string[] = [];

		// // TODO: Get rid of this workaround
		// searchObject = this.synchronizeAttributes(searchObject);

		for (let searchElement of attributes) {

			if (!searchElement.operator || searchElement.enableSearch === false || searchElement.selected === false) {
				continue;
			}

			let attribute = this.fqnService.getAttributeByFqn(meta, searchElement.boAttrId);
			attribute.inputType = this.getInputType(attribute);
			let fqn = searchElement.boAttrId;
			let alias = 'col' + index;
			let useTicks = this.needsTicks(attribute, searchElement.operator);
			let andPart = alias + ' ' + searchElement.operator + ' ';
			if (!this.isUnaryOperator(searchElement.operator)) {
				if (this.hasMultipleValues(searchElement) && searchElement.operator === '=') {
					let values = searchElement.values;
					if (values) {
						andPart = '('
							+ values.map(
								value => alias + ' ' + searchElement.operator + ' ' + DataService.formatSearchTemplateValue(value, attribute, useTicks)
							).join(' OR ')
							+ ')';
					}
				} else {
					let value = DataService.formatSearchTemplateValue(searchElement.value, attribute, useTicks);
					if (value === null) {
						this.$log.warn('No value given for non-unary operator \'%o\' on attribute \'%o\', ignoring', searchElement.operator, attribute);
						continue;
					}

					andPart += value;
				}
			}

			andArray.push(andPart);
			result.aliases[alias] = {boAttrId: fqn};
			index++;
		}

		andArray.push(...additionalWhereConditions);

		result.clause = andArray.join(' AND ');

		return result;
	}

	getInputType(attribute): string {
		if (attribute.reference) {
			return InputType.REFERENCE;
		} else if (attribute.type === 'Decimal' || attribute.type === 'Integer') {
			return InputType.NUMBER;
		} else if (attribute.type === 'String') {
			return InputType.STRING;
		} else if (attribute.type === 'Date' || attribute.type === 'Timestamp') {
			return InputType.DATE;
		} else if (attribute.type === 'Boolean') {
			return InputType.BOOLEAN;
		}
		return InputType.STRING;
	}

	/**
	 * Determines if the ticks should be used to escape the value for the given attribute in a WHERE-query.
	 *
	 * @param attribute
	 * @param operator
	 * @returns {boolean}
	 */
	private needsTicks(attribute: any, operator: string): boolean {
		if (attribute.system) {
			return true;
		} else if (attribute.inputType === InputType.NUMBER) {
			return false;
		} else if (attribute.inputType === InputType.REFERENCE && operator === '=') {
			return false;
		}

		return true;
	}

	public formatAttribute(value, attrMeta: EntityAttrMeta): string {
		let result = value;
		if (value.name !== undefined) {
			result = value.name !== null ? value.name : '';
		} else if (attrMeta.isBoolean()) {
			result = value ? this.i18n.getI18n('common.yes') : this.i18n.getI18n('common.no');
		} else if (attrMeta.isDate()) {
			result = this.dateTimeService.formatDate(value);
		} else if (attrMeta.isTimestamp()) {
			result = this.dateTimeService.formatTimestamp(value);
		}
		return result;
	}

	/**
	 * Determines if the given operator is unary.
	 *
	 * TODO: Available operators and their types should be defined somewhere centrally.
	 */
	private isUnaryOperator(operator: string): boolean {
		return operator === 'is null' || operator === 'is not null';
	}

	/**
	 * TODO
	 *    - subform column selection
	 *    - searchfilter
	 *    - search (text)
	 */
	exportBoList(
		boId: number | undefined,
		refAttrId: string | undefined,
		meta: EntityMeta,
		columnPreference: Preference<SideviewmenuPreferenceContent>,
		searchtemplatePreference: Preference<SearchtemplatePreferenceContent>,
		format: string,
		pageOrientationLandscape: boolean,
		isColumnScaled: boolean
	): Observable<string> {

		let isSubform: boolean = boId !== undefined && refAttrId !== undefined;

		let offset = undefined;
		let chunkSize = undefined;

		let columns: ColumnAttribute[] = columnPreference ? columnPreference.content.columns.filter(c => c.selected) : [];

		let sort = '';
		if (columnPreference && columnPreference.content.columns) {
			sort = this.getSortString(columnPreference.content.columns);
		}

		let filter = {
			offset: offset,
			chunksize: chunkSize,
			gettotal: false,
			countTotal: true,
			search: this.searchService.getCurrentSearchInputText(),
			searchFilter: undefined, // TODO
			withTitleAndInfo: false,
			attributes: '',
			sort: '',
			searchCondition: '',
			where: {}
		};
		// request only given attributes
		filter.attributes =
			columns
			// use short name instead of fqn to avoid problems with to long query param
				.map(attribute => this.fqnService.getShortAttributeName(meta.getEntityClassId(), attribute.boAttrId))
				.join(',');

		filter.sort = this.getSortString(columns);
		let searchCondition = this.getSearchCondition(meta);
		if (searchCondition) {
			filter.searchCondition = searchCondition;
		}

		if (searchtemplatePreference && searchtemplatePreference.content.columns) {
			filter.where = this.buildWhereCondition(
				searchtemplatePreference.content.columns,
				meta,
				this.searchfilterService.getAdditionalWhereConditions(meta.getEntityClassId())
			);
		} else {
			delete filter.where;
		}

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');

		let url;
		if (!isSubform) {
			url = this.nuclosConfig.getRestHost()
				+ '/bos/' + meta.getEntityClassId()
				+ '/boListExport/' + format + '/' + pageOrientationLandscape + '/' + isColumnScaled;
		} else {
			url = this.nuclosConfig.getRestHost()
				+ '/bos/' + meta.getEntityClassId() + '/'
				+ boId
				+ '/subBos/'
				+ refAttrId
				+ '/export/' + format + '/' + pageOrientationLandscape + '/' + isColumnScaled;
		}
		url
			+= '?countTotal=true&gettotal=false&offset=0&search='
			+ '&sort=' + filter.sort
			+ '&withTitleAndInfo=false';
		return this.http.post(
			url,
			filter,
			{
				headers: headers
			}
		).pipe(
			map((response: Response) => response.json()),
			map(data => data['links'].export.href),
		);
	}

	private getSortString(columns: ColumnAttribute[]) {
		return columns
			.filter(col => col.sort && col.sort.direction)
			.sort(
				(a, b) => {
					if (!a.sort || !b.sort || a.sort.prio === undefined || b.sort.prio === undefined) {
						return 0;
					}
					if (a.sort.prio < b.sort.prio) {
						return -1;
					}
					if (a.sort.prio > b.sort.prio) {
						return 1;
					}
					return 0;
				}
			)
			.map(column => column.boAttrId + (column.sort ? '+' + column.sort.direction : ''))
			.join(',');
	}


	/**
	 * TODO: Rename 'BO' to 'entity object', 'BO meta' to 'entity class'.
	 */
	getBo(boMetaId: string, boId: number): Observable<EntityObject> {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/bos/' + boMetaId + '/' + boId
		).pipe(
			map((response: Response) => new EntityObject(response.json())));
	}

	/**
	 * Fetches the corresponding EO for the given attribute (with only this one attribute filled).
	 * attributeId must be a fully quallified name!
	 */
	fetchByAttribute(
		sourceAttributeId: string,
		sourceEOId: number,
		targetAttributeId: string,
		targetEOId?: number
	): Observable<EntityObject> {
		// TODO: Splitting by underscore is potentially unsafe, the FQN format is flawed.
		let index = sourceAttributeId.lastIndexOf('_');
		if (index <= 0) {
			throw 'Attribute ID must be a fully qualified name! Illegal value: ' + sourceAttributeId;
		}
		let url = this.nuclosConfig.getRestHost() + '/data/fieldget/'
			+ sourceAttributeId + '/'
			+ sourceEOId + '/'
			+ targetAttributeId + '/'
			+ targetEOId;

		return this.http.get(url).pipe(
			map((response: Response) => new EntityObject(response.json())));
	}


	/**
	 * TODO: Too many parameters!
	 */
	private buildFilter(
		meta: EntityMeta,
		search: string,
		searchFilter: string | undefined,
		vlpId: string | undefined,
		vlpParams: URLSearchParams | undefined,
		attributes,
		sort
	): URLSearchParams {

		let searchParams: URLSearchParams = new URLSearchParams();
		if (vlpParams) {
			searchParams.appendAll(vlpParams);
		}

		searchParams.append('search', search);
		if (searchFilter) {
			searchParams.append('searchFilter', searchFilter);
		}
		if (vlpId) {
			searchParams.append('vlpId', vlpId);
		}
		searchParams.append('withTitleAndInfo', 'false');


		if (attributes === undefined) {
			attributes = [];
		}

		// request only given attributes
		if (attributes !== undefined) {
			searchParams.append('attributes',
				attributes
				// use short name instead of fqn to avoid problems with to long query param
					.map(attribute => this.fqnService.getShortAttributeName(meta.getEntityClassId(), attribute.boAttrId))
					.join(',')
			);
		}

		if (sort !== undefined) {
			searchParams.append('sort', sort);
		}

		let searchCondition = this.getSearchCondition(meta);
		if (searchCondition) {
			searchParams.append('searchCondition', searchCondition);
		}

		// NUCLOS-6311 4)
		searchParams.append('skipStatesAndGenerations', 'true');
		return searchParams;
	}


	private getSearchCondition(metaData: EntityMeta) {
		if (!metaData || !metaData.getAttributes()) {
			return undefined;
		}

		let hasFieldFilter = false;
		let fieldFilter = 'CompositeCondition:AND:[';

		for (let attrKey of Object.keys(metaData.getAttributes())) {
			let attribute = <any>metaData.getAttribute(attrKey);

			if (!attribute || attribute.ticked) {
				continue;
			}

			let fieldName = this.fqnService.getShortAttributeName(metaData.getBoMetaId(), attribute.boAttrId);
			if (attribute.search) {
				if (hasFieldFilter) {
					fieldFilter += ',';
				}

				fieldFilter += 'LikeCondition:LIKE:';
				fieldFilter += fieldName;
				fieldFilter += ':*' + attribute.search + '*';

				hasFieldFilter = true;
			}

			if (attribute.valuelist && attribute.valuelist.length > 0) {
				let hasInCondition = false;
				let inCondition = '';

				for (let key of Object.keys(attribute.valuelist)) {
					let value = attribute.valuelist[key];
					if (value.ticked && value.name) {
						if (hasInCondition) {
							inCondition += ',';
						}

						// NUCLOS-4111 CollectableInCondition works with the String Name of a Reference Field
						// TODO: It would be better if it worked with the IDs
						inCondition += '\'' + value.name + '\'';
						hasInCondition = true;
					}
				}

				if (hasInCondition) {
					if (hasFieldFilter) {
						fieldFilter += ',';
					}

					fieldFilter += 'InCondition:IN:';
					fieldFilter += fieldName;
					fieldFilter += ':[';
					fieldFilter += inCondition;
					fieldFilter += ']';

					hasFieldFilter = true;
				}
			}
		}

		if (hasFieldFilter) {
			return fieldFilter + ']';
		}

		return undefined;
	}

	loadMatrixData(webMatrix: WebMatrix, eo: EntityObject): Observable<any> {
		let link = this.nuclosConfig.getRestHost() + '/data/data/matrix/';

		let postData: any = webMatrix;
		postData.entity = eo.getEntityClassId();
		postData.boId = eo.getId();

		return this.http.post(link, JSON.stringify(postData)
		).pipe(map((response: Response) => response.json()));
	}

	loadTreeData(tree: WsTab): Observable<any[]> {
		let link = this.nuclosConfig.getRestHost() + '/data/tree/' + tree.boMetaId + '/' + tree.boId;

		return this.http.get(link).pipe(map((response: Response) => response.json()));
	}

	loadSubTreeData(node_id: string): Observable<any[]> {
		let link = this.nuclosConfig.getRestHost() + '/data/subtree/' + node_id;

		return this.http.get(link).pipe(map((response: Response) => response.json()));
	}

	private hasMultipleValues(searchElement: SearchtemplateAttribute) {
		return !searchElement.value && searchElement.values && searchElement.values.length > 0;
	}
}
