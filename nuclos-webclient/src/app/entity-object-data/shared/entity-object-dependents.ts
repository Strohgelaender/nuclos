import { IEntityObjectDependents, ISortModel, ISubEntityObject } from '@nuclos/nuclos-addon-api';
import { BehaviorSubject, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AutonumberService } from './autonumber.service';
import { EntityObjectDependency } from './entity-object-dependency';
import { EntityObjectSearchConfig } from './entity-object-search-config';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { EntityObjectService } from './entity-object.service';

export class EntityObjectDependents implements IEntityObjectDependents {
	private subject = new BehaviorSubject<ISubEntityObject[] | undefined>(undefined);
	private loading = false;

	constructor(
		private parent: EntityObject,
		private referenceAttributeId: string,
	) {
	}

	asObservable(): Observable<ISubEntityObject[] | undefined> {
		return this.subject;
	}

	current(): ISubEntityObject[] | undefined {
		return this.subject.value;
	}

	set(subEos: ISubEntityObject[]) {
		this.subject.next(subEos);
		AutonumberService.instance.updateAutonumbers(this);
	}

	reload(sortModel?: ISortModel) {
		this.clear();
		this.loadIfEmpty(sortModel);
	}

	/**
	 * Loads all dependents for the given attribute FQN.
	 */
	loadIfEmpty(sortModel?: ISortModel): void {
		if (this.loading) {
			return;
		}

		let currentData = this.subject.getValue();
		if (currentData) {
			this.subject.next(currentData);
			return;
		}

		if (this.parent.isNew()) {
			this.subject.next([]);
		} else {
			this.subject.next(undefined);

			this.loading = true;
			this.getService()
				.loadDependents(
					this.parent,
					this.getDependency(),
					{
						sort: sortModel && sortModel.toString()
					}
				).pipe(
				finalize(() => this.loading = false))
				.subscribe(
					(dependents: ISubEntityObject[]) => {
						this.subject.next(dependents);
					}
				);
		}
	}

	private getDependency(): EntityObjectDependency {
		let result = new EntityObjectDependency(this.referenceAttributeId);
		let parent = this.parent;
		while (parent instanceof SubEntityObject) {
			let parentDependency = new EntityObjectDependency(
				parent.getReferenceAttributeId(),
				undefined,
				'' + parent.getId()
			);
			result.getRootDependency().setParent(parentDependency);
			parent = parent.getParent();
		}
		return result;
	}

	addAll(dependents: ISubEntityObject[]) {
		let currentDependents = this.subject.value || [];
		if (dependents.length > 0) {
			this.parent.setDirty(true);
		}
		currentDependents.unshift(...dependents);
		AutonumberService.instance.updateAutonumbers(this);

		this.subject.next(currentDependents);
	}

	removeAll(dependents: ISubEntityObject[]) {
		if (dependents.length === 0) {
			return;
		}

		let currentDependents = this.subject.value || [];
		dependents.forEach(dependent => {
			let index = currentDependents.indexOf(dependent);
			if (index >= 0) {
				currentDependents.splice(index, 1);
				this.parent.setDirty(true);
			}
		});

		AutonumberService.instance.updateAutonumbers(this);

		this.subject.next(currentDependents);
	}

	isEmpty(): boolean {
		let current = this.current();
		return current === undefined || current.length === 0;
	}

	isLoading(): boolean {
		return this.loading;
	}

	private getService() {
		return EntityObjectService.instance;
	}

	getSelectedSubEos(): ISubEntityObject[] {
		let currentSubEos = this.current();

		if (!currentSubEos || currentSubEos.length === 0) {
			return [];
		}

		return currentSubEos.filter(
			subEo => subEo.isSelected()
		);
	}

	clear() {
		this.subject.next(undefined);
	}

	fetchBySearchConfig(searchConfig: EntityObjectSearchConfig) {
		return this.getService()
			.loadDependents(
				this.parent,
				this.getDependency(),
				searchConfig
			);
	}

	fetchValueList(attributeId: string) {
		return this.getService().loadValueList(
			this.parent,
			this.getDependency(),
			attributeId
		);
	}
}
