import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { Observable, of as observableOf, Subject, throwError as observableThrowError } from 'rxjs';

import { catchError, filter, finalize, map, mergeMap, retryWhen, tap } from 'rxjs/operators';
import { MandatorData } from '../../authentication';
import { CommandService } from '../../command/shared/command.service';
import { InputRequiredService } from '../../input-required/shared/input-required.service';
import { LayoutService } from '../../layout/shared/layout.service';
import { Logger } from '../../log/shared/logger';
import { RuleService } from '../../rule/shared/rule.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { ObservableUtils } from '../../shared/observable-utils';
import { NuclosStateService } from '../../state/shared/nuclos-state.service';
import { State } from '../../state/shared/state';
import { LocalStorageService } from '../../storage/shared/local-storage.service';
import { NuclosValidationService } from '../../validation/nuclos-validation.service';
import { ValidationStatus } from '../../validation/validation-status.enum';
import { AutonumberService } from './autonumber.service';
import { BoViewModel, EntityAttrMeta, EntityObjectData, LovEntry } from './bo-view.model';
import { EntityObjectDependency } from './entity-object-dependency';
import { EntityObjectErrorService } from './entity-object-error.service';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObjectSearchConfig } from './entity-object-search-config';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { LovDataService } from './lov-data.service';
import { LovSearchConfig } from './lov-search-config';
import { MetaService } from './meta.service';

/**
 * Provides access to the currently selected EntityObject.
 */
@Injectable()
export class EntityObjectService {
	static instance: EntityObjectService;

	static readonly EO_LOCALSTORAGE_KEY_PREFIX = 'EntityObject_';

	// TODO: Mandator stuff should be in its own module and service.
	/* an EntityObject could be assigned to a certain mandator */
	private selectedMandator: MandatorData;

	constructor(
		private nuclosConfig: NuclosConfigService,
		private metaService: MetaService,
		private http: NuclosHttpService,
		private $log: Logger,
		private ruleService: RuleService,
		private inputRequiredService: InputRequiredService,
		private lovDataService: LovDataService,
		private validationService: NuclosValidationService,
		private commandService: CommandService,
		private layoutService: LayoutService,
		private eoEventService: EntityObjectEventService,
		private localStorageService: LocalStorageService,
		private autonumberService: AutonumberService,
		private nuclosStateService: NuclosStateService,
		private eoErrorService: EntityObjectErrorService,
	) {
		EntityObjectService.instance = this;
	}

	selectMandator(mandator: MandatorData) {
		this.selectedMandator = mandator;
	}

	getSelectedMandator(): MandatorData {
		return this.selectedMandator;
	}

	listenForExternalChanges(eo: EntityObject) {
		if (eo.isNew()) {
			return;
		}

		let key = EntityObjectService.EO_LOCALSTORAGE_KEY_PREFIX + eo.getId();
		this.localStorageService.observeItem(key).subscribe((data: EntityObjectData) => {
			eo.mergeData(data);
		});
	}

	publishChanges(eo: EntityObject) {
		if (eo.isNew()) {
			return;
		}

		let key = EntityObjectService.EO_LOCALSTORAGE_KEY_PREFIX + eo.getId();
		this.localStorageService.setItem(key, eo.getData());
		this.localStorageService.removeItem(key);
	}

	/**
	 * Loads the EO for the given entity and ID.
	 */
	loadEO(entityClassId: string, entityObjectId: number): Observable<EntityObject> {
		this.$log.debug('Loading EO %o (%o)...', entityClassId, entityObjectId);
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/bos/' + entityClassId + '/' + entityObjectId
		).pipe(
			map((response: Response) => new EntityObject(response.json())),
			tap((eo: EntityObject) => this.ruleService.updateRuleExecutor(eo).subscribe()),
		);
	}

	reloadEo(eo: EntityObject): Observable<EntityObject> {
		let url = eo.getSelfURL();

		if (!url) {
			return observableThrowError('Could not reload EO %o - no self URL');
		}

		return this.http.get(url).pipe(
			map((response: Response) => response.json()),
			tap(data => eo.setData(data)),
			map(() => eo),
		);
	}

	changeState(eo: EntityObject, state: State): Observable<EntityObject> {
		let result = new Subject<EntityObject>();
		this._changeState(eo, state).subscribe(
			entityObject => {
					this.eoEventService.emitStateChange(entityObject);
					result.next(entityObject);
			},
			error => {
				result.error(error);
			}
		);
		return result;
	}

	/**
	 * Changes the state of the given EO, without any confirmation dialogs etc.
	 * If the EO is dirty, it is saved. Else the state is changed directly.
	 */
	private _changeState(eo: EntityObject, state: State): Observable<any> {
		if (eo.isDirty()) {
			// NUCLOS-4661: Backup data in case the state change fails
			let wasDirty = eo.isDirty();
			let oldState = eo.getAttribute('nuclosState');

			eo.setAttribute('nuclosState', {id: state.nuclosStateId});

			return eo.save().pipe(catchError(error => {
				eo.setAttribute('nuclosState', oldState);
				eo.setDirty(wasDirty);
				return observableThrowError(error);
			}));
		} else {
			let body = {};
			return this.handleInputRequired(
				eo,
				body,
				this.nuclosStateService.changeState(eo, state, body).pipe(
					mergeMap(() => eo.reload()),
					map(reloadedEo => reloadedEo.getData()),
				)
			).pipe(map(eoData => new EntityObject(eoData)));
		}
	}

	/**
	 * Saves the given EO.
	 * If it is new, it is inserted via a POST request.
	 * If it is not new, it is updated via a PUT request on its self-url.
	 *
	 * @param eo
	 * @returns {Observable<R>}
	 */
	save(eo: EntityObject, customRule?: string): Observable<EntityObject> {
		this.$log.debug('Saving: %o', eo);

		let postData: EntityObjectData = eo.serialize();

		if (customRule) {
			// TODO: Do not manipulate EO data for this!
			// Use the rule execution REST service instead and pass the rule name as parameter.
			postData.executeCustomRule = customRule;
		}

		let url = eo.isNew() ? eo.getInsertURL() : eo.getSelfURL();

		// force update of title / info
		delete eo.title;
		delete eo.info;

		// No self-link yet, if this EO is new
		if (!url) {
			// TODO: Get the INSERT-Link via HATEOAS
			url = this.nuclosConfig.getRestHost() + '/bos/' + eo.getEntityClassId();
		}

		let isNew = eo.isNew();
		let result = isNew
			? this.http.post(url, postData)
			: this.http.put(url, postData);

		return this.handleSaveRequest(
			eo,
			postData,
			result,
		).pipe(tap(() => {
			if (isNew) {
				this.eoEventService.emitCreatedEo(eo);
			}
		}));
	}

	executeCustomRule(eo: EntityObject, rule: string) {
		if (eo.isNew()) {
			this.$log.error('Cannot execute custom rule on unsaved EO.');
			return observableOf(eo);
		}

		if (eo.isDirty()) {
			// TODO: Use the same REST service here
			return this.save(eo, rule);
		} else {
			let url = eo.getSelfURL() + '/execute/' + rule;

			let postData = {};
			let result = this.http.post(url, postData);

			return this.handleSaveRequest(
				eo,
				postData,
				result
			);
		}
	}

	private handleSaveRequest(
		eo: EntityObject,
		postData: any,
		saveRequest: Observable<any>
	) {
		saveRequest = ObservableUtils.onSubscribe(
			saveRequest,
			() => eo.saving(true)
		).pipe(map(response => {
			this.$log.debug('Saving complete: %o', eo);
			let newData = response.json();
			eo.saved(newData);
			return eo;
		}));

		return this.handleInputRequired(
			eo, postData, saveRequest
		).pipe(tap(
			savedEO => this.commandService.executeCommands(savedEO)
		));
	}

	private handleInputRequired(
		eo: EntityObject,
		body: any,
		httpRequest: Observable<any>
	) {
		return httpRequest.pipe(
			retryWhen(
				// Retry until all InputRequired exceptions are handled
				errors => {
					eo.saving(false);
					return errors.pipe(mergeMap(
						error => {
							eo.saving(false);
							return this.inputRequiredService.handleError(error, body, eo);
						}
					));
				}
			),
			catchError(
				error => {
					eo.saving(false);
					return this.eoErrorService.handleError(error, eo);
				}
			),
			finalize(
				() => {
					eo.saving(false);
					this.eoEventService.emitSavedEo(eo);
				}
			),
		);
	}

	/**
	 * Deletes the given EO.
	 * If it is new, it only a "delete" event is emitted.
	 * If it is not new, it is deleted from the server first.
	 *
	 * @param eo
	 * @returns {any}
	 */
	delete(eo: EntityObject): Observable<EntityObject> {
		// If the EO is new, just emit a "deleted" event and return
		if (eo.isNew()) {
			this.eoEventService.emitDeletedEo(eo);
			return observableOf(eo);
		}

		// If the EO is not new, delete it on the server first
		let url = eo.getSelfURL();
		if (!url) {
			this.$log.error('EO has no self-URL: %o', eo);
			return observableOf(eo);
		}

		return this.http.delete(url).pipe(
			tap(() => {
					eo.deleted();
					this.eoEventService.emitDeletedEo(eo);
				}
			),
			map(
				() => eo
			),
			catchError(
				error => this.eoErrorService.handleError(error, eo)
			),
		);
	}

	/**
	 * Unlocks the given EO.
	 */
	unlock(eo: EntityObject): Observable<EntityObject> {

		let links = eo.getLinks();

		if (!links || !links.lock) {
			this.$log.error('EO has no lock-URL: %o', eo);
			return observableOf(eo);
		}

		return this.http.delete(links.lock.href).pipe(
			map(
				() => {
					eo.clearOwner();
					return eo;
				}
			),
			catchError(
				error => this.eoErrorService.handleError(error, eo)
			),
		);
	}

	/**
	 * Creates a new empty EO for the currently selected entity class,
	 * which can be used for inserts.
	 *
	 * @param entityMetaId Optional parent EO, if a subform EO is being created.
	 */
	createNew(
		entityMetaId: string,
		parentEO?: EntityObject
	): Observable<IEntityObject> {

		return this.metaService.getBoMeta(entityMetaId).pipe(
			map(meta => meta.getLinks().defaultGeneration),
			filter(defaultGenerationLink => !!defaultGenerationLink),
			mergeMap(defaultGenerationLink =>
				this.http.get(defaultGenerationLink!.href).pipe(
					map((response: Response) => new EntityObject(response.json()) as IEntityObject),
					tap((eo: EntityObject) => this.ruleService.updateRuleExecutor(eo, parentEO).subscribe()),
					tap((eo: EntityObject) => {
							eo.getData()._flag = 'insert';

							if (this.selectedMandator) {
								eo.setMandator(this.selectedMandator);
							}

							this.eoEventService.emitAddEo(eo);
						}
					),
				)
			),
		);
	}

	loadDependents(
		parent: EntityObject,
		dependency: EntityObjectDependency,
		searchConfig?: EntityObjectSearchConfig
	) {
		this.$log.debug('Loading dependents for dependency %o...', dependency);

		let url = this.nuclosConfig.getRestHost()
			+ '/bos/' + parent.getRootEo().getEntityClassId() + '/' + parent.getRootEo().getId()
			+ '/subBos/recursive/' + dependency.toPath();

		if (searchConfig) {
			let first = true;
			for (let x in searchConfig) {
				if (searchConfig[x]) {
					if (first) {
						url += '?';
						first = false;
					} else {
						url += '&';
					}
					url += encodeURI(x) + '=' + encodeURI(searchConfig[x]);
				}
			}
		}

		return this.http.get(url).pipe(
			map((response: Response) => {
				let result: SubEntityObject[] = [];
				let eoData: EntityObjectData[] = response.json().bos;
				for (let data of eoData) {
					let subEO = new SubEntityObject(
						parent,
						dependency.getReferenceAttributeFqn(),
						data
					);
					result.push(subEO);
					this.ruleService.updateRuleExecutor(subEO, parent.getRootEo()).subscribe();
				}
				this.$log.debug('Successfully loaded %o dependents for %o', result.length, dependency);
				return result;
			}),
			catchError(error => {
				this.$log.error('Failed to load depenents for %o: %o', dependency, error);
				return observableThrowError(error);
			}),
		);
	}

	loadValueList(
		parent: EntityObject,
		dependency: EntityObjectDependency,
		attributeId: string
	): Observable<{name: string, id: any}[]> {
		this.$log.debug('Loading dependents for dependency %o...', dependency);

		let url = this.nuclosConfig.getRestHost()
			+ '/bos/' + parent.getRootEo().getEntityClassId() + '/' + parent.getRootEo().getId()
			+ '/subBos/' + dependency.getReferenceAttributeFqn()
			+ '/valuelist/' + attributeId;

		return this.http.get(url).pipe(
			map((response: Response) => response.json()),
			catchError(error => {
				this.$log.error('Failed to load depenents for %o: %o', dependency, error);
				return observableThrowError(error);
			}),
		);
	}

	loadLovEntries(search: LovSearchConfig): Observable<LovEntry[]> {
		return this.lovDataService.loadLovEntries(search);
	}

	/**
	 * Checks for a layout change (because of changed nuclos-process).
	 * Sets the new layout URL on the given EO.
	 *
	 * @param eo
	 */
	nuclosProcessChanged(eo: EntityObject): void {
		let nuclosProcess = eo.getNuclosProcess();
		let nuclosProcessId = nuclosProcess && nuclosProcess.id;

		// TODO: Get From HATEOAS
		let url = this.nuclosConfig.getRestHost() + '/meta/processlayout/' + nuclosProcessId + '/' + eo.getEntityClassId();
		if (!eo.isNew()) {
			url += '/' + eo.getId();
		}

		this.http.getCachedJSON(url)
			.subscribe(data => {
				let layoutURL = data.links && data.links.layout && data.links.layout.href;
				if (layoutURL) {
					eo.setLayoutURL(layoutURL);
				}
			});
	}

	validateAttributeValue(value: any, meta: EntityAttrMeta) {
		return this.validationService.validate(value, meta);
	}

	getValidationStatus(attributeMeta: EntityAttrMeta, value: any): ValidationStatus {
		return this.validationService.getValidationStatus(attributeMeta, value);
	}

	getLayoutURLDynamically(eo: EntityObject): Observable<string | undefined> {
		return this.layoutService.getWebLayoutURLDynamically(eo);
	}

	loadEmptyList(metaId: string): Observable<BoViewModel> {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/bos/' + metaId + '/query?offset=0&chunkSize=0&countTotal=false&gettotal=false'
		).pipe(map((response: Response) => {
			let result: BoViewModel = response.json();
			return result;
		}));
	}
}
