import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import {
	BehaviorSubject,
	EMPTY,
	Observable,
	of as observableOf,
	Subject,
	throwError as observableThrowError
} from 'rxjs';

import { distinctUntilChanged, finalize, take, tap } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { Logger } from '../../log/shared/logger';
import { EntityMeta } from './bo-view.model';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObjectNavigationService } from './entity-object-navigation.service';
import { EntityObject } from './entity-object.class';
import { EntityObjectService } from './entity-object.service';
import { MetaService } from './meta.service';

/**
 * Holds currently selected entity object(s).
 */
@Injectable()
export class EntityObjectResultService {
	static instance: EntityObjectResultService;

	/**
	 * Currently selected entity class ID (aka "boMetaId").
	 */
	private selectedEntityClassId = new BehaviorSubject<string | undefined>(undefined);

	private selectedMeta: EntityMeta | undefined;

	/**
	 * Currently selected entity object ID (aka "boId").
	 */
	private selectedEoId: number | string | undefined;

	/**
	 * Currenty selected entity object, should be always consistent with selected EO class and ID.
	 */
	private selectedEo: IEntityObject | undefined;

	private entityObjects: IEntityObject[] = [];

	/**
	 * Determines if there already was result data for the currently selected entity class.
	 * This is important to decide about auto-selection of the first result, when new results are loaded.
	 * Auto-selection should not occur initially if an EO was directly selected (via deep link).
	 * But it should occur if new results are loaded after search filter changes etc.
	 */
	private pristine = true;

	private totalEoCount: number | undefined;

	private eoSelectionInProgress = false;

	canCreateBo: boolean | undefined;

	private resultListUpdateSubject = new Subject<boolean>();

	private eoSelectionSubject = new Subject<void>();

	vlpId: string | undefined;
	vlpParams: URLSearchParams | undefined;

	constructor(
		private eoEventService: EntityObjectEventService,
		private eoNavigationService: EntityObjectNavigationService,
		private authenticationService: AuthenticationService,
		private metaService: MetaService,
		private $log: Logger,
	) {
		EntityObjectResultService.instance = this;

		this.eoEventService.observeDeletedEo().subscribe(
			(eo) => {
				this.removeEo(eo);

				// TODO: Check if the deleted EO really belonged to the currently shown EOs
				if (this.totalEoCount) {
					this.totalEoCount--;
				}
			}
		);

		this.eoEventService.observeCreatedEo().subscribe(
			() => {
				// TODO: Check if the created EO really belongs to the currently shown EOs
				if (this.totalEoCount !== undefined) {
					this.totalEoCount++;
				}
			}
		);

		this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
			if (!loggedIn) {
				this.selectEo(undefined);
			}
		});

		this.observeSelectedEntityClassId().subscribe(
			() => {
				this.clear();
				this.updateSelectedMeta().subscribe();
				this.authenticationService.setLastEntityCookie(this.getSelectedEntityClassId());
			}
		);
	}

	private getEoService() {
		return EntityObjectService.instance;
	}


	getSelectedEntityClassId() {
		return this.selectedEntityClassId.getValue();
	}

	getSelectedMeta() {
		return this.selectedMeta;
	}

	updateSelectedMeta(): Observable<EntityMeta> {
		let entityClassId = this.getSelectedEntityClassId();

		if (!entityClassId) {
			this.selectedMeta = undefined;
			return EMPTY;
		}

		return this.metaService.getEntityMeta(entityClassId).pipe(tap(
			meta => this.selectedMeta = meta
		));
	}

	getSelectedEoId(): number | string | undefined {
		return this.selectedEoId;
	}

	getSelectedEo(): IEntityObject | undefined {
		return this.selectedEo;
	}

	isSelected(eo: EntityObject) {
		if (eo.getEntityClassId() !== this.getSelectedEntityClassId()) {
			return false;
		}

		if (eo.isNew()) {
			if (eo.getTemporaryId()) {
				return this.getSelectedEoId() === eo.getTemporaryId();
			}
			return this.getSelectedEoId() === 'new';
		}

		return eo.getId() === this.getSelectedEoId();
	}

	selectEo(eo: IEntityObject | undefined) {
		this.$log.warn('Selecting EO: %o (%o)', eo, eo && (eo as EntityObject).getTitle());
		this.selectedEo = eo;

		this.selectedEoId = undefined;
		if (eo) {
			this.selectEntityClassId(eo.getEntityClassId());
			this.selectedEoId = eo.getId() || eo.getTemporaryId() || 'new';
		}

		this.eoSelectionInProgress = false;

		this.eoEventService.emitSelectedEo(eo);
		if (eo) {
			this.eoNavigationService.navigateToEo(eo);
		}
	}

	waitForEoSelection(): Observable<void> {
		if (!this.eoSelectionInProgress) {
			return observableOf(undefined);
		}

		return this.eoSelectionSubject.pipe(take(1));
	}

	/**
	 * Loads the EO for the given entity and ID
	 * and sets it as the selected EO.
	 * The ID could also be a temporary ID, starting with 'temp_'.
	 */
	selectEoByClassAndId(entityClassId: string, entityObjectId: number | string) {
		let selectedEO = this.getSelectedEo();

		if (this.eoSelectionInProgress) {
			this.$log.warn('EO selection is already in progress, ignoring');
			if (selectedEO) {
				return observableOf(selectedEO);
			} else {
				return EMPTY;
			}
		}

		if (selectedEO && selectedEO.getEntityClassId() === entityClassId && '' + selectedEO.getId() === entityObjectId) {
			this.selectedEoId = selectedEO.getId();
			return observableOf(selectedEO);
		}

		this.eoSelectionInProgress = true;

		let isTemp = ('' + entityObjectId).startsWith('temp_');
		let result;

		if (isTemp) {
			let eoString = localStorage.getItem('' + entityObjectId);
			if (eoString) {
				let eo = new EntityObject(JSON.parse(eoString));
				eo.setDirty(true);
				eo.select();
				result = observableOf(eo);
			} else {
				result = observableThrowError('No entity object found for ID: ' + entityObjectId);
			}
		} else {
			result = this.getEoService().loadEO(entityClassId, <number>entityObjectId).pipe(tap(
				eo => eo.select()
			));
		}

		return result.pipe(
			finalize(() => {
				this.eoSelectionInProgress = false;
				this.eoSelectionSubject.next();
			})
		);
	}

	selectEntityClassId(entityClassId: string | undefined) {
		this.$log.warn('Selecting entity class: %o', entityClassId);
		this.selectedEntityClassId.next(entityClassId);
	}


	observeSelectedEntityClassId(): Observable<string | undefined> {
		return this.selectedEntityClassId.pipe(distinctUntilChanged());
	}

	observeResultListUpdate(): Observable<boolean> {
		return this.resultListUpdateSubject;
	}

	resetEo(eo: EntityObject) {
		if (eo) {
			eo.reset();
		}
		this.eoEventService.emitResetEo(eo);
	}

	navigateToNew() {
		let entityClassId = this.getSelectedEntityClassId();
		if (entityClassId) {
			this.eoNavigationService.navigateToNew(entityClassId);
		}
	}

	createNew(): Observable<IEntityObject> {
		let entityClassId = this.getSelectedEntityClassId();
		if (!entityClassId) {
			return EMPTY;
		}

		return this.getEoService().createNew(
			entityClassId
		).pipe(tap(
			eo => this.entityObjects.unshift(eo)
		));
	}

	/**
	 * TODO: Make private.
	 */
	setSelectedMeta(meta: EntityMeta) {
		this.selectedMeta = meta;
	}

	getResults() {
		return this.entityObjects;
	}

	/**
	 * Removes the EO with the given ID from the list.
	 */
	private removeEo(eo: IEntityObject): void {
		let eoId = eo.getId();

		let entityObjects = this.getResults();

		let wasSelected = eo === this.selectedEo;
		if (wasSelected) {
			this.selectEo(undefined);
		}

		// May not have been loaded yet
		if (!this.entityObjects) {
			return;
		}

		let index = this.entityObjects.findIndex(e => e.getId() === eo.getId());
		this.$log.debug('removeEO(%o): index = %o', eoId, index);
		if (index > -1) {
			entityObjects.splice(index, 1);
			// If the removed EO was selected, select the previous EO in the list
			if (wasSelected) {
				if (index > 0) {
					index--;
				}
				let nextEO = entityObjects[index];
				if (nextEO) {
					this.eoNavigationService.navigateToEo(nextEO);
				}
			}
		} else {
			this.$log.warn('Not found: %o', eo);
		}
	}

	addNewData(boViewModel: any) {
		this.entityObjects.push(...boViewModel.bos);
		this.totalEoCount = boViewModel.total;
		this.canCreateBo = boViewModel.canCreateBo;
		this.resultsUpdated();
	}

	private resultsUpdated() {
		this.resultListUpdateSubject.next(this.pristine);
		this.pristine = false;
	}

	private findEoInResults(eo: IEntityObject) {
		let index = this.findEoIndexInResults(eo);

		if (index < 0) {
			return undefined;
		}

		return this.entityObjects[index];
	}

	private findEoIndexInResults(eo: IEntityObject): number {
		if (eo.isNew()) {
			return this.entityObjects.findIndex(
				resultEo => resultEo === eo || resultEo.getTemporaryId() === eo.getTemporaryId()
			);
		} else {
			return this.entityObjects.findIndex(
				resultEo => resultEo.getId() === eo.getId()
			);
		}
	}

	selectFirstResult() {
		this.$log.warn('Selecting first result...');
		if (this.entityObjects && this.entityObjects.length > 0) {
			if (this.selectedEo && this.findEoInResults(this.selectedEo)) {
				this.$log.warn('Already selected a result from the result list');
				return;
			}
			let firstResult = this.entityObjects[0];
			firstResult.select();
			firstResult.reload().subscribe();
		}
	}

	getTotalResultCount() {
		return this.totalEoCount;
	}

	/**
	 * The number of results loaded from the server.
	 * Might be smaller than the current number of EOs, as the user can add new EOs.
	 */
	getLoadedResultCount() {
		return this.entityObjects.length;
	}


	reloadCanCreateBoIfNotSet(boMetaId: string) {
		if (this.canCreateBo === undefined) {
			this.getEoService().loadEmptyList(boMetaId).subscribe(model => {
				this.canCreateBo = model.canCreateBo;
			});
		}
	}

	clear() {
		this.selectedMeta = undefined;
		this.selectEo(undefined);
		this.entityObjects = [];
		this.totalEoCount = undefined;
		this.canCreateBo = undefined;
		this.pristine = true;
	}

	resetResults() {
		this.entityObjects = [];
	}

	addSelectedEoToList() {
		if (this.selectedEo && !this.findEoInResults(this.selectedEo)) {
			this.entityObjects.unshift(this.selectedEo);
			this.resultsUpdated();
		} else {
			this.$log.warn('No EO selected - can not add to result list');
		}
	}
}
