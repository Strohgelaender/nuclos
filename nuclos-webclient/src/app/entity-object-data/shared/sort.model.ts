import { ISortModel } from '@nuclos/nuclos-addon-api';

export type SortDirection = 'asc' | 'desc';

export interface SortAttribute {
	colId: string;
	sort: SortDirection;
}

export class SortModel implements ISortModel {

	private columns: SortAttribute[];

	constructor(columns: SortAttribute[]) {
		this.columns = columns;
	}

	public getColumns() {
		return this.columns;
	}

	public toString() {
		return this.columns
			.map(column => column.colId + '+' + (column.sort ? column.sort : 'asc'))
			.join(',');
	}
}
