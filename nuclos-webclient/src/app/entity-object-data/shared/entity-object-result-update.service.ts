import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, Observable, Subject } from 'rxjs';

import { finalize, mergeMap, tap } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { LoadMoreResultsEvent } from '../../entity-object/entity-object.component';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { Logger } from '../../log/shared/logger';
import { ObservableUtils } from '../../shared/observable-utils';
import { BoViewModel } from './bo-view.model';
import { DataService } from './data.service';
import { EntityObjectResultService } from './entity-object-result.service';
import { EntityObjectSearchfilterService } from './entity-object-searchfilter.service';

@Injectable()
export class EntityObjectResultUpdateService {

	private loading = false;

	/**
	 * Triggered when the EO list should be reloaded.
	 */
	private mustLoadListSubject = new Subject<any>();

	private searchFilterId;

	constructor(
		private eoResultService: EntityObjectResultService,
		private eoPreferenceService: EntityObjectPreferenceService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private dataService: DataService,
		private authenticationService: AuthenticationService,
		private route: ActivatedRoute,
		private $log: Logger,
	) {
		this.authenticationService.observeLoginStatus().subscribe(
			loggedIn => {
				if (!loggedIn) {
					this.eoResultService.clear();
				}
			}
		);

		this.eoSearchfilterService.observeSelectedSearchfilter().subscribe(
			() => this.triggerLoadList()
		);

		this.route.queryParams.subscribe(params => {
			this.searchFilterId = params['searchFilterId'];
		});
	}

	observeMustLoadList(): Observable<any> {
		return this.mustLoadListSubject;
	}

	triggerLoadList() {
		if (!this.authenticationService.isLoggedIn()) {
			this.$log.warn('Skipping result list update - requires login');
			return;
		}

		this.$log.warn('Trigger loading of eo list');

		let selectedEntityClassId = this.eoResultService.getSelectedEntityClassId();
		if (selectedEntityClassId) {
			let entityMeta = this.eoResultService.getSelectedMeta();
			if (entityMeta && entityMeta.getBoMetaId() === selectedEntityClassId) {
				this.mustLoadListSubject.next();
			} else {
				this.eoResultService.updateSelectedMeta().subscribe(
					() => this.mustLoadListSubject.next()
				);
			}
		} else {
			this.$log.warn('Cannot load result list - no entity class selected');
		}
	}

	/**
	 * load data - will be called subsequently when scrolling down in sidebar
	 */
	loadData(event: LoadMoreResultsEvent): Observable<BoViewModel> {
		return this.loadResultsForSelectedEntityClass(event).pipe(
			tap(
				(boViewModel) => {
					this.eoResultService.addNewData(boViewModel);
				}
			)
		);
	}

	isLoading() {
		return this.loading;
	}

	setLoading(loading) {
		this.loading = loading;
	}

	private loadResultsForSelectedEntityClass(event: LoadMoreResultsEvent): Observable<BoViewModel> {
		return this.eoResultService.updateSelectedMeta().pipe(
			mergeMap(() => {
				let observable: Observable<BoViewModel>;
				const meta = this.eoResultService.getSelectedMeta();
				const vlpId = this.eoResultService.vlpId;
				const vlpParams: URLSearchParams | undefined = this.eoResultService.vlpParams;
				if (!meta) {
					observable = EMPTY;
				} else {
					observable = this.dataService.loadList(
						meta,
						vlpId,
						vlpParams,
						this.eoPreferenceService.getSelectedTablePreference(),
						this.eoSearchfilterService.getSelectedSearchfilter(),
						{
							offset: event.offset,
							chunkSize: event.limit,
							countTotal: event.count,
							searchFilterId: this.searchFilterId
						}
					).pipe(
						tap(boViewModel => boViewModel.boMetaId = meta.getBoMetaId()),
					);
				}

				return ObservableUtils.onSubscribe(
					observable,
					() => this.setLoading(true)
				).pipe(
					finalize(
						() => this.setLoading(false)
					),
				);
			})
		);
	}
}
