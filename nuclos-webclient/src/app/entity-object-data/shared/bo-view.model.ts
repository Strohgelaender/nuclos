import { Command } from '../../command/shared/command';
import { Generation } from '../../generation/shared/generation';
import { InputRequired } from '../../input-required/shared/model';
import { FqnService } from '../../shared/fqn.service';
import { Link, LinkContainer, SubEOLinkContainer } from '../../shared/link.model';
import { StateInfo } from '../../state/shared/state';
import { EntityObject } from './entity-object.class';

export interface BoViewModel {

	boMetaId: string; // same as meta.boMetaId
	bos: Array<EntityObject>; // list of BOs in sideview
	selectedBo: EntityObject;
	bo?: EntityObjectData;

	meta: EntityMetaData;

	search?: string; // text search filter for bos
	searchfilter?: string; // search filter id   // TODO rename

	all?: boolean; // true if all data is loaded

	total?: number; // total number of bos
	canCreateBo?: boolean; // true if it is allowed to create a BO

	setSelectedBo?: Function;
}

export interface BoAttr {
	boAttrId: string;
	boAttrName?: string;
	calculated?: boolean;
	hidden?: boolean;
	nullable?: boolean;
	unique?: boolean;
	reference?: boolean;
	system?: boolean;

	precision?: number;
	scale?: number;

	name?: string;
	type?: AttributeType;
	inputType?: any;
	referencingBoMetaId?: any;
	readonly?: boolean;
	defcomptype?: 'ListOfValues' | 'Combobox' | 'Autonummer';
}

export type AttributeType = 'Boolean' | 'Date' | 'Decimal' | 'Document' | 'Image' | 'Integer' | 'String' | 'Timestamp';

export type AttributeRestriction = 'readonly' | 'hidden' | 'disabled';

// TODO: Use a map instead of hacks like "nocreate,nodelete"!
export type EntityRestriction = 'readonly' | 'nocreate' | 'nodelete' | 'nocreate,nodelete';

export class EntityObjectData {
	boId?: number;

	/**
	 * The EO can have a temporary ID, if it could not be saved yet
	 * (e.g. because of incomplete data for object generation)
	 */
	temporaryId?: string;

	boMetaId: string;

	attributes: Map<string, object>;
	attrImages?: { links: { image: Link } };

	_flag?: Flag; // TODO rename
	dirty?: boolean;
	version?: number;

	title?: string; // TODO
	info?: string; // TODO

	rowcolor?: string;

	// TODO suBos and bos ?
	subBos?: Map<EntityObjectData, any> | any; // TODO
	bos?: Array<EntityObjectData>;

	links: LinkContainer;
	attrRestrictions?: { [key: string]: AttributeRestriction; };
	restriction?: EntityRestriction;
	canWrite?: boolean;
	canDelete?: boolean;
	nextStates?: StateInfo[];

	inputrequired?: InputRequired;
	executeCustomRule?: any; // TODO

	generations?: Generation[];

	/**
	 * default selected tab, defined in status model
	 */
	defaultTabbedPaneName: string | undefined;

	/**
	 * There can be a business error on the EO (e.g. after a failed generation attempt)
	 */
	businessError?: string;

	commands?: { list: Command<any>[] };
}

export class EntityObjectSubEoInfo {
	links: SubEOLinkContainer;
	restriction?: string;		// TODO: This should be a Map or a Set
	readonlyattributes?: any;	// TODO: Define type
}

export class EntityMetaData {
	boMetaId: string;
	name?: string;

	refAttrId?: string; // reference to main eo if this is a subform

	/* There might be a "detail"-boMetaId if this is a dynamic BO */
	detailBoMetaId?: string;

	mandatorLevelId?: string;

	/**
	 * TODO: This should be of type BoAttr[], must be changed in REST.
	 * Currently it is an object which is used like a Map.
	 */
	attributes: Object;

	links: LinkContainer;

	readonly?: boolean;

	stateModel?: boolean;

	titlePattern?: string;
	infoPattern?: string;

	// TODO: Rename to searchFilterId
	searchfilter?: string; // TODO correct place?
	taskMetaId?: string;

	/**
	 * Name of the field which contains the actual entity for dynamic entities (e.g. a dynamic task list).
	 */
	dynamicEntityFieldName?: string;
	/**
	 * They underlying entity, if this is a task list.
	 */
	taskEntity?: string;

	lafParameter: LookAndFeelParameter;
}

export class EntityMeta {
	constructor(private emData: EntityMetaData) {
	}

	/**
	 * @deprecated use getter/setter on EntityMeta
	 * @returns {EntityMetaData}
	 */
	getMetaData() {
		return this.emData;
	}

	getAttributeLabel(attributeName: string): string | undefined {
		let attribute = this.getAttributeMetaByFqn(attributeName);
		return attribute && attribute.getName();
	}

	getLinks(): LinkContainer {
		return this.emData.links;
	}

	/**
	 * @deprecated use getAttributeMeta
	 * @param attrKey (this might not be the same as boAttrName - but the same as last part of FQN, attrKey contains no '_')
	 * @returns {any}
	 */
	getAttribute(attrKey: string): BoAttr {
		return this.emData.attributes[attrKey];
	}

	/**
	 * @deprecated use getAttributeMetaByFqn
	 * @param attributeName might contain '_'
	 * @return {EntityAttrMeta}
	 */
	getAttributeMeta(attributeName: string): EntityAttrMeta | undefined {
		let result = this.findAttributeMetaData(attributeName);

		if (result === undefined) {
			console.warn('Could not find attribute %o in meta %o', attributeName, this);
		}

		return result;
	}

	getAttributeMetaByFqn(fqn: string): EntityAttrMeta | undefined {
		let attributeName = FqnService.getShortAttributeNameFailsafe(this.getBoMetaId(), fqn);
		let boAttr = this.getAttribute(attributeName);
		return boAttr ? new EntityAttrMeta(boAttr) : undefined;
	}

	private findAttributeMetaData(attributeNameOrFqn: string) {
		let attributeName = FqnService.getShortAttributeNameFailsafe(this.getBoMetaId(), attributeNameOrFqn);
		return this.getAttributes().get(attributeName);
	}

	getAttributes(): Map<string, EntityAttrMeta> {
		let result = new Map<string, EntityAttrMeta>();

		for (let attributeId in this.emData.attributes) {
			if (this.emData.attributes.hasOwnProperty(attributeId)) {
				let attributeMeta = new EntityAttrMeta(this.emData.attributes[attributeId]);

				result.set(attributeId, attributeMeta);
			}
		}

		return result;
	}

	/**
	 * Returns the simple name of this entity class.
	 *
	 * @returns {string}
	 */
	getEntityName(): string | undefined {
		return this.getMetaData().name;
	}

	/**
	 * TODO: Rename from 'BO meta' to 'entity class'
	 *
	 * @returns {string}
	 * @deprecated use getEntityClassId()
	 */
	getBoMetaId(): string {
		return this.emData.boMetaId;
	}

	/**
	 * @returns {string}
	 */
	getEntityClassId(): string {
		return this.emData.boMetaId;
	}

	getDetailEntityClassId(): string | undefined {
		return this.emData.detailBoMetaId;
	}

	getMandatorLevelId(): string | undefined {
		return this.emData.mandatorLevelId;
	}

	getMandatator(): EntityAttrMeta | undefined {
		return this.getAttributeMeta('nuclosMandator');
	}

	getRefAttrId(): string | undefined {
		return this.emData.refAttrId;
	}

	hasStateModel(): boolean {
		return !!this.emData.stateModel;
	}

	isSubform(): boolean {
		return !!this.getRefAttrId();
	}

	getTitlePattern(): string | undefined {
		return this.emData.titlePattern;
	}

	getInfoPattern(): string | undefined {
		return this.emData.infoPattern;
	}

	getPopupParameter(): string | undefined {
		return this.emData.lafParameter ? this.emData.lafParameter.nuclos_LAF_Webclient_Popup : undefined;
	}

	/**
	 * if set true search will be executed only if a search condition is applied
	 * @returns {boolean | undefined}
	 */
	getEmptyResultListIfNoSearchCondition(): boolean | undefined {
		return this.emData.lafParameter ? this.emData.lafParameter.nuclos_LAF_Empty_Result_List_If_No_Search_Condition : undefined;
	}

	getDynamicEntityFieldName() {
		return this.emData.dynamicEntityFieldName;
	}

	getTaskEntity() {
		return this.emData.taskEntity;
	}

	getTaskMetaId() {
		return this.emData.taskMetaId;
	}
}

export class EntityAttrMeta {

	constructor(private attrMetaData: BoAttr) {
		if (!this.attrMetaData) {
			throw('Cannot instantiate EntityAttrMeta from undefined data');
		}
	}

	getAttributeID() {
		return this.attrMetaData.boAttrId;
	}

	getName() {
		return this.attrMetaData.name;
	}

	/*
	 * @deprecated use getAttributeID()
	 */
	getAttributeName() {
		return this.attrMetaData.boAttrName;
	}

	/**
	 * @deprecated use getter/setter on EntityAttrMeta
	 * @returns {BoAttr}
	 */
	getMetaData() {
		return this.attrMetaData;
	}

	getType(): AttributeType {
		return (this.attrMetaData && this.attrMetaData.type) || 'String';
	}

	isNumber() {
		let type = this.getType();

		if (!type) {
			return false;
		}

		return type === 'Decimal' || type === 'Integer';
	}

	isDecimal() {
		return this.getType() === 'Decimal';
	}

	getPrecision() {
		let result = this.attrMetaData.precision;

		if (this.getType() === 'Integer') {
			result = 0;
		}

		return result;
	}

	isBoolean(): boolean {
		return this.getType() === 'Boolean';
	}

	isDate() {
		return this.getType() === 'Date';
	}

	isTimestamp(): boolean {
		return this.getType() === 'Timestamp';
	}

	isDocument(): boolean {
		return this.getType() === 'Document';
	}

	isImage(): boolean {
		return this.getType() === 'Image' && this.getAttributeName() !== 'nuclosStateIcon';
	}

	isStateIcon(): boolean {
		return this.getType() === 'Image' && this.getAttributeName() === 'nuclosStateIcon';
	}

	isState(): boolean {
		return this.getAttributeName() === 'nuclosState';
	}

	isProcess(): boolean {
		return this.getAttributeName() === 'nuclosProcess';
	}

	isStateNumber(): boolean {
		return this.getAttributeName() === 'nuclosStateNumber';
	}

	isDeletedFlag(): boolean {
		return this.getAttributeName() === 'nuclosDeleted';
	}

	isNullable(): boolean | undefined {
		return this.attrMetaData.nullable;
	}

	isAutonumber(): boolean {
		return this.getComponentType() === 'Autonummer';
	}

	getReferencedEntityClassId() {
		return this.attrMetaData.referencingBoMetaId;
	}

	isHidden() {
		return this.attrMetaData.hidden;
	}

	isReadonly() {
		return this.attrMetaData.readonly;
	}

	isReference(): boolean {
		return !!this.attrMetaData.reference;
	}

	getComponentType() {
		return this.attrMetaData.defcomptype;
	}

	isSystemAttribute() {
		return this.attrMetaData.system;
	}

	isCalculated(): boolean {
		return !!this.attrMetaData.calculated;
	}

	getInputType() {
		return this.attrMetaData.inputType;
	}

	getScale(): number | undefined {
		return this.attrMetaData.scale;
	}
}

export type Flag = 'insert' | 'update' | 'delete';

export class InputType {
	static REFERENCE = 'REFERENCE';
	static NUMBER = 'NUMBER';
	static DATE = 'DATE';
	static STRING = 'STRING';
	static BOOLEAN = 'BOOLEAN';
}

export class AttrType {
	static IMAGE = 'Image';
}

export class LovEntry {
	static EMPTY: LovEntry = {
		get id() {
			return null;
		},
		get name() {
			return '\u200b';
		}
	};

	id: number | null;
	name: string;
	selected?: boolean;
}

export interface DocumentAttribute {
	id: string;
	name: string;
	fileType: string;
}

export class LookAndFeelParameter {
	nuclos_LAF_Webclient_Popup?: string;
	nuclos_LAF_Empty_Result_List_If_No_Search_Condition: boolean;
}
