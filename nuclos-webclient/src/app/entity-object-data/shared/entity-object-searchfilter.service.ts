import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, tap } from 'rxjs/operators';
import { Logger } from '../../log/shared/logger';
import { PerspectiveService } from '../../perspective/shared/perspective.service';
import {
	Preference,
	PreferenceType,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent
} from '../../preferences/preferences.model';
import { SearchService } from '../../search/shared/search.service';
import { SearchfilterService } from '../../search/shared/searchfilter.service';
import { FqnService } from '../../shared/fqn.service';
import { EntityMeta } from './bo-view.model';
import { EntityObjectResultService } from './entity-object-result.service';
import { MetaService } from './meta.service';
import { SelectableService } from './selectable.service';

/**
 * Provides access to all available search filter for the currently selected entity.
 * And to the currently selected searchfilter.
 */
@Injectable()
export class EntityObjectSearchfilterService {
	static instance: EntityObjectSearchfilterService;

	private selectedSearchfilter: BehaviorSubject<Preference<SearchtemplatePreferenceContent> | undefined>
		= new BehaviorSubject(undefined);
	private allSearchfilters: BehaviorSubject<Preference<SearchtemplatePreferenceContent>[]>
		= new BehaviorSubject([]);

	constructor(
		private selectableService: SelectableService,
		private perspectiveService: PerspectiveService,
		private searchService: SearchService,
		private searchfilterService: SearchfilterService,
		private eoResultService: EntityObjectResultService,
		private fqnService: FqnService,
		private metaService: MetaService,
		private $log: Logger,
	) {
		EntityObjectSearchfilterService.instance = this;
		this.perspectiveService.observeSelectedSearchtemplatePreference().subscribe(
			pref => this.selectSearchfilter(pref)
		);

		this.eoResultService.observeSelectedEntityClassId().subscribe(
			entityClassId => this.loadSearchfilters(entityClassId)
		);

		this.selectedSearchfilter.pipe(distinctUntilChanged()).subscribe(
			() => this.searchService.initiateDataLoad()
		);
	}

	private loadSearchfilters(entityClassId: string | undefined) {
		this.searchfilterService.getSearchfiltersForEntity(entityClassId)
		.subscribe(
			preferences => this.setSearchfilters(preferences as Preference<SearchtemplatePreferenceContent>[])
		);
	}

	getSelectedSearchfilter(): Preference<SearchtemplatePreferenceContent> | undefined {
		return this.selectedSearchfilter.getValue();
	}

	sortSearchfilters() {
		let searchfilters = this.getAllSearchfilters();
		searchfilters = this._sortSearchfilters(searchfilters);
		searchfilters = _.clone(searchfilters);
		this.allSearchfilters.next(searchfilters);
	}

	selectSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		this.updateColumns(searchfilter);

		let searchfilters = this.getAllSearchfilters();

		if (searchfilter) {
			this.searchfilterService.select(searchfilter);
			let found = searchfilters.find(
				it => !!searchfilter && SearchtemplatePreferenceContent.equals(it.content, searchfilter.content) && it.name === searchfilter.name
			);
			if (found) {
				searchfilter = found;
			} else {
				searchfilters.push(searchfilter);
				searchfilters = this._sortSearchfilters(searchfilters);
				searchfilters = _.clone(searchfilters);
				this.allSearchfilters.next(searchfilters);
			}
		} else {
			// Deselect the last searchfilter, if none is to be selected now
			this.searchfilterService.deselect(this.getSelectedSearchfilter());
		}

		this.selectedSearchfilter.next(searchfilter);
	}

	getAllSearchfilters(): Preference<SearchtemplatePreferenceContent>[] {
		return this.allSearchfilters.getValue();
	}

	observeSelectedSearchfilter(): Observable<Preference<SearchtemplatePreferenceContent> | undefined> {
		return this.selectedSearchfilter.pipe(tap(it => this.updateColumns(it)));
	}

	observeAllSearchfilters(): Observable<Preference<SearchtemplatePreferenceContent>[]> {
		return this.allSearchfilters.pipe(distinctUntilChanged());
	}

	private _sortSearchfilters(preferences: Preference<SearchtemplatePreferenceContent>[]): Preference<SearchtemplatePreferenceContent>[] {
		let sorted = preferences.sort((a, b) => {
			if (a.name && b.name) {
				if (a.name > b.name) {
					return 1;
				}
				if (a.name < b.name) {
					return -1;
				}
			}
			return 0;
		});
		return sorted;
	}

	private setSearchfilters(preferences: Preference<SearchtemplatePreferenceContent>[]) {
		let sorted = this._sortSearchfilters(preferences);
		this.allSearchfilters.next(sorted);
		this.selectSearchfilter(
			sorted.find(it => it.selected === true)
		);
	}

	deleteSearchfilter(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		if (searchfilter) {
			this.searchfilterService.deleteSearchfilter(searchfilter)
				.subscribe(() => {
					let searchfilters = this.getAllSearchfilters();
					let index = searchfilters.indexOf(searchfilter);
					if (index >= 0) {
						searchfilters.splice(index, 1);
						searchfilters = _.clone(searchfilters);
						this.allSearchfilters.next(searchfilters);
					}
					if (this.getSelectedSearchfilter() === searchfilter) {
						this.selectSearchfilter(undefined);
					}
				});
		}
	}

	applySearchtemplateIfNoPerspective(
		preferences,
		meta: EntityMeta,
		urlQuery: string | any,
	) {
		let selectedPerspective = this.perspectiveService.getSelectedPerspective();
		let searchtemplatePreference;

		if (selectedPerspective && selectedPerspective.searchTemplatePrefId) {
			this.$log.debug('Perspective is overriding default user-selected search template, ignoring.')
			return;
		}

		if (urlQuery) {
			searchtemplatePreference = this.searchfilterService.createFromUrlSearch(meta, urlQuery);
		} else {
			searchtemplatePreference = preferences
				.filter(pref => pref.type === PreferenceType.searchtemplate.name)
				.find(pref => pref.selected as boolean);

			// No auto-selection!
			// if (!searchtemplatePreference) {
			// 	searchtemplatePreference = preferences
			// 		.filter(pref => pref.type === PreferenceType.searchtemplate.name)
			// 		.shift() as Preference<SearchtemplatePreferenceContent>;
			// }
		}

		// add meta data to columns
		if (searchtemplatePreference) {
			this.selectableService.addMetaDataToColumns(searchtemplatePreference, meta);
		}
		this.$log.warn('selected searchtemplate: %o', searchtemplatePreference);

		this.selectSearchfilter(searchtemplatePreference);
	}

	updateColumns(searchfilter: Preference<SearchtemplatePreferenceContent> | undefined) {
		if (searchfilter && searchfilter.content && searchfilter.content.columns) {
			this.metaService.getEntityMeta(searchfilter.boMetaId).subscribe(meta => {
				this.selectableService.addMetaDataToColumns(searchfilter, meta);
				searchfilter.content.columns.forEach(
					column => {
						try {
							meta.getAttributes().forEach((_attributeMeta, attributeKey) => {
								let shortAttributeName = this.fqnService.getShortAttributeName(meta.getBoMetaId(), column.boAttrId);
								if (attributeKey === shortAttributeName) {
									column.inputType = SearchfilterService.getInputType(<SearchtemplateAttribute>meta.getAttribute(attributeKey));
									let operator = this.searchfilterService.getOperatorDefinition(column);
									if (operator) {
										this.searchfilterService.formatValue(column, operator);
									}
									throw 'break';
								}
							});
						} catch (e) {
							// Ignore
						}
					}
				);
			});
			searchfilter.content.columns.filter(column => column.operator === undefined).forEach(
				column => {
					try {
						let operators = this.searchfilterService.getOperatorDefinitions()[column.inputType];
						column.operator = operators[0].operator;
					} catch (e) {
						// Ignore
					}
				}
			);
		}
	}
}
