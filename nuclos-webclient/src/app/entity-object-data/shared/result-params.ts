export class ResultParams {
	static DEFAULT: ResultParams = {
		offset: 0,
		chunkSize: 40,
		countTotal: true
	};

	offset: number;
	chunkSize: number;
	countTotal: boolean;

	searchFilterId?: string;
}
