import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { HttpMethod } from '../../rest-explorer/shared/rest-service-info';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { EntityMeta } from './bo-view.model';

@Injectable()
export class MetaService {
	static instance: MetaService;

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService
	) {
		MetaService.instance = this;
	}

	getBoMeta(boMetaId: string): Observable<EntityMeta> {
		return this.http.getCachedJSON(
			this.nuclosConfig.getRestHost() + '/boMetas/' + boMetaId,
			json => new EntityMeta(json)
		);
	}

	getEntityMeta(entityClassId: string): Observable<EntityMeta> {
		return this.getBoMeta(entityClassId);
	}

	getSubBoMeta(href: string): Observable<EntityMeta> {
		return this.http.getCachedJSON(
			href,
			json => new EntityMeta(json)
		);
	}

	// Rest-Service actually gives away information "canCreateEo" in another way, e.g:
	// /rest/bos/example_rest_Auftrag
	// { "all": true,  "bos": [], "canCreateBo": true }

	canCreateEo(entityClassId: string): Observable<boolean> {
		return this.checkEntityForHttpMethod(entityClassId, HttpMethod.POST);
	}

	canReadEo(entityClassId: string): Observable<boolean> {
		return this.checkEntityForHttpMethod(entityClassId, HttpMethod.GET);
	}

	checkEntityForHttpMethod(entityClassId: string, httpMethod: HttpMethod): Observable<boolean> {
		return new Observable<boolean>(observer => {
			this.http.getCachedJSON(
				this.nuclosConfig.getRestHost() + '/bos',
				json => json.map(m => new EntityMeta(m))
			).pipe(
				take(1),
				tap(entityMetaArray => {
					let meta: EntityMeta = entityMetaArray.filter(
						entityMeta => entityMeta.getBoMetaId() === entityClassId).shift();
					if (meta && meta.getLinks().bos) {
						let links = meta.getLinks();
						if (links && links.bos && links.bos.methods) {
							observer.next(links.bos.methods.indexOf(httpMethod) !== -1);
						}
					}
				}),
			).subscribe();
		});
	}
}
