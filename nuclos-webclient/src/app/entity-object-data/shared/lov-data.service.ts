import { Injectable } from '@angular/core';
import { Response, ResponseContentType, URLSearchParams } from '@angular/http';
import { Observable, of as observableOf } from 'rxjs';

import { map, tap } from 'rxjs/operators';
import { Logger } from '../../log/shared/logger';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { EntityAttrMeta, LovEntry } from './bo-view.model';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { LovSearchConfig } from './lov-search-config';
import { MetaService } from './meta.service';

@Injectable()
export class LovDataService {

	constructor(
		private nuclosConfig: NuclosConfigService,
		private http: NuclosHttpService,
		private metaService: MetaService,
		private $log: Logger
	) {
	}

	loadSearchLovEntries(
		entityAttributeMeta: EntityAttrMeta,
		query: string
	): Observable<LovEntry[]> {

		let select = {
			reffield: entityAttributeMeta.getAttributeID(),
			vlp: (entityAttributeMeta.isState() || entityAttributeMeta.isStateNumber()) ? {
				type: 'named',
				value: 'status',
				params: {searchmode: false}
			} : undefined
		};

		return this.deprecatedLoadLovEntriesForVlp(
			select,
			null,
			query
		).pipe(tap(
			data => this.addEmptyOption(entityAttributeMeta, data)
		));
	}

	getLovParams(search: LovSearchConfig): URLSearchParams {
		let params = new URLSearchParams();

		if (search.vlp) {
			let referenceField = search.attributeMeta.getAttributeID();
			params.set('reffield', referenceField);
			this.addVlpParams(params, search);
		}

		if (search.mandatorId) {
			params.set('mandatorId', search.mandatorId);
		}

		if (search.quickSearchInput) {
			params.set('quickSearchInput', search.quickSearchInput);
		}

		if (search.resultLimit && search.resultLimit > 0) {
			params.set('chunkSize', '' + search.resultLimit);
		}

		return params;
	}

	loadLovEntries(
		search: LovSearchConfig
	): Observable<LovEntry[]> {
		const params = this.getLovParams(search);

		let link;
		if (search.vlp) {
			link = this.nuclosConfig.getRestHost() + '/data/vlpdata';
		} else if (search.attributeMeta.isReference()) {
			let referenceField = search.attributeMeta.getAttributeID();
			link = this.nuclosConfig.getRestHost() + '/data/referencelist/' + referenceField;
		} else {
			this.$log.error('Can not load value-list for non-reference attribute without VLP: %o', search);
			return observableOf([]);
		}

		return this.http.get(link, {
				responseType: ResponseContentType.Json,
				search: params
			}
		).pipe(
			map((response: Response) => response.json()),
			tap(data => this.addEmptyOption(search.attributeMeta, data)),
		);
	}

	private addEmptyOption(entityAttributeMeta: EntityAttrMeta, data) {
		if (entityAttributeMeta.isNullable()) {
			data.unshift(LovEntry.EMPTY);
		}
	}

	/**
	 * @deprecated Use {@link loadLovEntriesForVlp}
	 */
	deprecatedLoadLovEntriesForVlp(
		select: { reffield: string, vlp: any },
		eoId: number | null,
		quickSearchInput?: string,
		mandatorId?
	): Observable<LovEntry[]> {
		let link = this.nuclosConfig.getRestHost() + '/data/referencelist/' + select.reffield;
		let params = new URLSearchParams();

		if (select.vlp) {
			link = this.nuclosConfig.getRestHost() + '/data/vlpdata';

			let vlpparams = {
				vlptype: select.vlp.type,
				vlpvalue: select.vlp.value,
				intid: eoId,
				mandatorId: mandatorId,
				reffield: select.reffield
			};

			for (let i in select.vlp.params) {
				if (select.vlp.params.hasOwnProperty(i)) {
					vlpparams[i] = select.vlp.params[i];
				}
			}

			for (let key in vlpparams) {
				if (vlpparams[key] !== undefined) {
					params.set(key, vlpparams[key]);
				}
			}
		}

		if (mandatorId) {
			params.set('mandatorId', mandatorId);
		}

		if (quickSearchInput) {
			params.set('quickSearchInput', quickSearchInput);
		}

		return this.http.get(link, {
				responseType: ResponseContentType.Json,
				search: params
			}
		).pipe(map((response: Response) => response.json()));
	}

	// NUCLOS-6499: Note that actually there has to be a check done, if there is a layout for this particular eo.
	// For the sake of simplicity we suppose that an eo which has explicit read rights, has a layout, too

	canOpenReference(attributeMeta: EntityAttrMeta): Observable<boolean> {
		return new Observable<boolean>(observer => {
			this.metaService.canReadEo(attributeMeta.getReferencedEntityClassId())
				.subscribe(canCreateReferencedEo => {
						observer.next(canCreateReferencedEo);
					}
				);
		});
	}

	/**
	 * the user is allowed to create a new EO and select it as reference
	 * if the referenced EO can be created and the referencing EO is writable
	 */
	canAddReference(
		eo: EntityObject,
		attributeMeta: EntityAttrMeta
	): Observable<boolean> {
		return new Observable<boolean>(observer => {
			let result = eo instanceof SubEntityObject || eo.canWrite(); // for main eo check 'canWrite' flag
			if (!result) {
				observer.next(false);
				return;
			}
			this.metaService.canCreateEo(attributeMeta.getReferencedEntityClassId())
				.subscribe(canCreateReferencedEo => {
						observer.next(canCreateReferencedEo);
					}
				);
		});
	}

	private addVlpParams(params: URLSearchParams, search: LovSearchConfig) {
		let vlp = search.vlp;

		if (!vlp) {
			return;
		}

		let vlpparams = {
			vlptype: vlp.type,
			vlpvalue: vlp.value,
			intid: search.eo.getRootEo().getId(),
			mandatorId: search.mandatorId,
			reffield: search.attributeMeta.getAttributeID()
		};

		if (vlp.parameter) {
			for (let param of vlp.parameter) {
				vlpparams[param.name] = param.value;
			}
		}

		let vlpParametersForAttribute = search.eo.getVlpParameters(search.attributeMeta.getAttributeID());

		this.$log.debug('VLP parameters: %o', vlpParametersForAttribute);

		if (vlpParametersForAttribute) {
			vlpParametersForAttribute.forEach((value, key) => {
				vlpparams[key] = value.getValue();
			});
		}

		for (let key in vlpparams) {
			if (vlpparams[key] !== undefined && vlpparams[key] !== null) {
				let value = vlpparams[key];
				if (value && value.hasOwnProperty('id')) {
					value = value.id;
				}
				params.set(key, value);
			} else {
				params.set(key, '');
			}
		}
	}
}
