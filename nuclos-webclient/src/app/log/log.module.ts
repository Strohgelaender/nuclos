import { NgModule } from '@angular/core';
import { Logger } from '@nuclos/nuclos-addon-api';
import { ConsoleLogService } from './shared/console-log.service';
import { Logger as DeprecatedLogger } from './shared/logger';

@NgModule({
	imports: [],
	declarations: [],
	providers: [
		{
			provide: Logger,
			useClass: ConsoleLogService
		},
		{
			// TODO replace usages with Logger from api
			provide: DeprecatedLogger,
			useClass: ConsoleLogService
		}
	]
})
export class LogModule {
}
