import { Injectable } from '@angular/core';
import { ILogger } from '@nuclos/nuclos-addon-api';
import { Logger } from './logger';

// Declare the console as an ambient value so that TypeScript doesn't complain.
declare var console: any;

/* tslint:disable:no-console */
@Injectable()
export class ConsoleLogService implements ILogger {

	constructor() {
		Logger.instance = this;
	}

	assert(...args: any[]): void {
		if (console && console.assert) {
			this.addCaller(args);
			console.assert(...args);
		}
	}

	error(...args: any[]): void {
		if (console && console.error) {
			this.addCaller(args);
			console.error(...args);
		}
	}

	group(...args: any[]): void {
		if (console && console.group) {
			console.group(...args);
		}
	}

	groupEnd(...args: any[]): void {
		if (console && console.groupEnd) {
			console.groupEnd(...args);
		}
	}

	info(...args: any[]): void {
		if (console && console.info) {
			this.addCaller(args);
			console.info(...args);
		}
	}

	log(...args: any[]): void {
		if (console && console.log) {
			this.addCaller(args);
			console.log(...args);
		}
	}

	warn(...args: any[]): void {
		if (console && console.warn) {
			this.addCaller(args);
			console.warn(...args);
		}
	}

	debug(...args: any[]): void {
		if (console && console.debug) {
			this.addCaller(args);
			console.debug(...args);
		}
	}

	trace(...args: any[]): void {
		if (console && console.trace) {
			this.addCaller(args);
			console.trace(...args);
		}
	}

	private addCaller(args: any[]) {
		let callerName = this.getCallerName();

		if (callerName) {
			args[0] = '[' + callerName + ']   ' + args[0];
		}
	}

	/**
	 * Ugly workaround because arguments.callee is not available in strict mode.
	 *
	 * @returns {any}
	 */
	private getCallerName() {
		let result;

		let regex = /(.*?)(?:\..*)*@|at (\w+).*? \(/g;
		let stack = new Error().stack;

		let nextName = () => {
			let foundName;
			if (stack) {
				let regexResult = regex.exec(stack);
				if (regexResult) {
					foundName = regexResult[1] || regexResult[2];
				}
			}
			return foundName;
		};

		result = this.findExternalCaller(nextName);

		return result;
	}

	private findExternalCaller(nextName: () => string) {
		let result;

		// TODO: Following lines are commented out, because it kills all performance for "ng serve" in debug mode?!
		// do {
		// 	result = nextName();
		// } while (result === 'ConsoleLogService');

		return result;
	}
}
