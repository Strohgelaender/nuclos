import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { EntityObjectEventListener } from '../entity-object-data/shared/entity-object-event-listener';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { Logger } from '../log/shared/logger';
import { RuleService } from '../rule/shared/rule.service';
import { BusyService } from '../shared/busy.service';
import { LayoutService } from './shared/layout.service';

@Component({
	selector: 'nuc-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit, OnChanges, OnDestroy {
	@Input() eo: EntityObject | undefined;
	@Output() onSubmit = new EventEmitter<any>();

	webLayout: WebLayout | undefined;
	rules: Rules | undefined;

	private eoListener: EntityObjectEventListener;
	private subscriptions: Subscription[] = [];

	constructor(
		private layoutService: LayoutService,
		private busyService: BusyService,
		private ruleService: RuleService
	) {
	}

	private createListener() {
		// Create an Observable for the busy service
		let subscriber: Subscriber<any>;
		let saving: Observable<any> = new Observable(internalSubscriber => {
			subscriber = internalSubscriber;
		});

		this.eoListener = {
			afterLayoutChange: (
				entityObject: EntityObject,
				_layoutURL: string
			) => {
				this.subscriptions.push(
					this.busyService.busy(
						this.updateLayout(entityObject)
					).subscribe()
				);
			},
			isSaving: (_eo, inProgress) => {
				if (inProgress) {
					this.busyService.busy(saving).subscribe();
					subscriber.next(true);
				} else {
					subscriber.next(false);
					subscriber.complete();
				}
				Logger.instance.debug('Saving: %o', inProgress);
			}
		};
	}

	ngOnInit() {
	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChanges = changes['eo'];
		if (eoChanges) {
			let previousEO = changes['eo'].previousValue;

			if (previousEO && this.eoListener) {
				previousEO.removeListener(this.eoListener);
			}

			let eo: EntityObject = changes['eo'].currentValue;
			if (eo) {
				this.createListener();
				eo.addListener(this.eoListener);

				this.subscriptions.push(
					this.busyService.busy(
						this.updateLayout(eo)
					).subscribe()
				);
			}
		}
	}

	private updateLayout(eo: EntityObject): Observable<any> {
		return this.layoutService.getWebLayout(eo).pipe(tap(
			weblayout => {
				this.webLayout = weblayout;
				this.subscriptions.push(
					this.ruleService.updateRuleExecutor(eo).subscribe()
				);
			},
		));
	}

	submit() {
		// TODO: Submitting the form somehow also triggers adding of a new row in the subform
		this.onSubmit.next();
	}
}
