import { Component, Injector, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractWebComponent } from '../shared/abstract-web-component';

@Component({
	selector: 'nuc-web-container',
	templateUrl: './web-container.component.html',
	styleUrls: ['./web-container.component.css']
})
export class WebContainerComponent extends AbstractWebComponent<WebContainer> implements OnInit, OnChanges {
	contentClasses;

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
		this.updateContentClasses();
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.updateContentClasses();
	}

	toggle() {
		// TODO
	}

	private updateContentClasses() {
		this.contentClasses = {
			'tab-content': true,
			opaque: this.webComponent.opaque
		};
	}

	getGrid() {
		return this.webComponent.grid;
	}

	getTable() {
		return this.webComponent.table;
	}

	getCalculated() {
		return this.webComponent.calculated;
	}

	getComponents() {
		return this.webComponent.components;
	}
}
