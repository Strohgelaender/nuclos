import {
	Component,
	ComponentFactoryResolver,
	ComponentRef,
	Input,
	OnChanges,
	OnInit,
	ReflectiveInjector,
	SimpleChanges,
	ViewChild,
	ViewContainerRef
} from '@angular/core';
import * as components from '../';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';

/**
 * Dynamically creates a concrete child component based on the '_type' of the given WebComponent.
 */
@Component({
	selector: 'nuc-web-component',
	templateUrl: './web-component.component.html',
	styleUrls: ['./web-component.component.css']
})
export class WebComponentComponent implements OnInit, OnChanges {
	@Input() eo: EntityObject;

	/**
	 * Reference to the dynamically created component.
	 */
	componentRef: ComponentRef<any>;

	@ViewChild('dynamicComponentContainer', {read: ViewContainerRef})
	dynamicComponentContainer: ViewContainerRef;

	constructor(
		private resolver: ComponentFactoryResolver,
		private $log: Logger
	) {
	}

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes['eo']) {
			this.componentRef.instance.eo = this.eo;
			if (this.componentRef.instance.ngOnChanges) {
				this.componentRef.instance.ngOnChanges(changes);
			}
		}
	}

	/**
	 * TODO: Handle unknown component types
	 *
	 * @param component
	 */
	@Input() set component(component: WebComponent) {
		if (!component) {
			return;
		}

		// Resolve the actual component type
		// The '_type' property is automatically added on server side by the Jackson mapper
		let typeName = component['_type'];
		let componentType = components[typeName + 'Component'];

		if (!componentType) {
			this.$log.error('Unknown component type: %o', typeName);
			return;
		}

		this.createComponent(component, componentType);
	}

	/**
	 * Instantiates the actual component and inserts it into the container.
	 *
	 * @param component
	 * @param componentType
	 */
	private createComponent(component: WebComponent, componentType: any) {
		// Data to be injected in the dynamically created component:
		let inputProviders = [
			{provide: 'eo', useValue: this.eo},
			{provide: 'webComponent', useValue: component}
		];
		let resolvedInputs = ReflectiveInjector.resolve(inputProviders);

		// injector provides the input data to the new component
		let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.dynamicComponentContainer.parentInjector);
		let factory = this.resolver.resolveComponentFactory(componentType);
		let componentRef = factory.create(injector);

		this.dynamicComponentContainer.insert(componentRef.hostView);

		if (this.componentRef) {
			this.componentRef.destroy();
		}

		this.componentRef = componentRef;
	}
}
