/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebPhonenumberComponent } from './web-phonenumber.component';

xdescribe('WebPhonenumberComponent', () => {
	let component: WebPhonenumberComponent;
	let fixture: ComponentFixture<WebPhonenumberComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebPhonenumberComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebPhonenumberComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
