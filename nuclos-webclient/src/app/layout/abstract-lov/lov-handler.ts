import { URLSearchParams } from '@angular/http';
import { Observable, of as observableOf } from 'rxjs';
import { LovEntry } from '../../entity-object-data/shared/bo-view.model';

export abstract class LovHandler {
	static DUMMY: LovHandler = {
		loadEntries: () => observableOf([]),
		loadFilteredEntries: () => observableOf([]),
		getValue: () => undefined,
	};

	abstract loadEntries(): Observable<LovEntry[]>;

	abstract loadFilteredEntries(search: string): Observable<LovEntry[]>;

	abstract getValue(): any;

	abstract clearValue?(): any;

	abstract refreshEntries?(): any;

	abstract entrySelected?(value?: any);

	abstract isEnabled?(): boolean;

	abstract getWidth?();

	abstract getHeight?();

	abstract searchReferenceClick?();

	abstract addReferenceClick?();

	abstract stopEditing?();

	abstract keydownHandler?(event);

	abstract getVlpId?(): string | undefined;

	abstract getVlpParams?(): URLSearchParams;
}
