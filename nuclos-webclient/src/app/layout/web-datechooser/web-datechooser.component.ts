import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DatechooserComponent } from '../../ui-components/datechooser/datechooser.component';
import { DatepickerPosition } from '../../ui-components/datechooser/datepicker-position';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-datechooser',
	templateUrl: './web-datechooser.component.html',
	styleUrls: ['./web-datechooser.component.css']
})
export class WebDatechooserComponent extends AbstractInputComponent<WebDatechooser> implements OnInit {

	@Input() datepickerPosition: DatepickerPosition = 'bottom-left';
	@Output() onValueChange = new EventEmitter();

	@ViewChild('nucDatechooser') datechooser: DatechooserComponent;

	constructor(
		injector: Injector
	) {
		super(injector);
	}

	ngOnInit() {
	}

	setAttributeValue(value?: string) {
		// Bootstrap calls this too early (while the input is still incomplete)
		if (this.datechooser.isInputFocused()) {
			this.eo.setAttribute(this.webComponent.name, value);

			// TODO: Avoid manually setting the EO dirty
			this.eo.setDirty(true);

			return;
		}

		this.eo.setAttribute(this.webComponent.name, value);
		this.onValueChange.next();
	}

	focusInput() {
		this.datechooser.focusInput();
	}

	selectInputText() {
		this.datechooser.selectInputText();
	}

	toggleDatepicker() {
		this.datechooser.toggleDatepicker();
	}

	setInputText(value: string) {
		this.datechooser.setInputText(value);
	}

	commitValue() {
		this.datechooser.commitValue();
	}

	getModel() {
		return this.getModelValue();
	}

	isValid() {
		return this.datechooser && this.datechooser.isValid();
	}

	getRawInput() {
		return this.datechooser.getRawInput();
	}

	getValidationCssClasses(): any {
		return super.getValidationCssClasses();
	}
}
