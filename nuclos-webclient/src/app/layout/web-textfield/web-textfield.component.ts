import { Component, Injector, OnChanges, OnInit } from '@angular/core';
import { EntityAttrMeta } from '../../entity-object-data/shared/bo-view.model';
import { NumberService } from '../../shared/number.service';
import { NuclosValidationService } from '../../validation/nuclos-validation.service';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-textfield',
	templateUrl: './web-textfield.component.html',
	styleUrls: ['./web-textfield.component.css']
})
export class WebTextfieldComponent extends AbstractInputComponent<WebTextfield> implements OnInit, OnChanges {

	private attributeMeta: EntityAttrMeta;
	private valueString;
	isNumber: boolean;

	constructor(
		injector: Injector,
		private numberService: NumberService,
		private validationService: NuclosValidationService
	) {
		super(injector);
	}

	ngOnInit() {
		this.fetchMeta();
	}

	ngOnChanges() {
		if (!this.attributeMeta) {
			this.fetchMeta();
		}
	}

	private fetchMeta() {
		if (this.eo) {
			this.eo.getAttributeMeta(this.webComponent.name).subscribe(
				meta => {
					this.attributeMeta = meta;
					this.isNumber = meta && meta.isNumber();
				}
			);
		}
	}

	getValueString(): string {
		if (this.valueString) {
			return this.valueString;
		} else {
			let value = this.eo && this.eo.getAttribute(this.webComponent.name);
			return this.format(value);
		}
	}

	format(value) {
		if (this.attributeMeta) {
			if (this.attributeMeta.isNumber()) {
				value = this.numberService.format(value, this.attributeMeta.getPrecision());
			} else if (this.attributeMeta.isReference()) {
				value = value.name;
			}
		}

		return value;
	}

	setValueString(value) {
		this.valueString = value;
		this.updateModel();
	}

	updateModel() {
		let value = this.valueString;

		if (this.attributeMeta && this.attributeMeta.isNumber()) {
			value = this.numberService.parseNumber(value);
		}

		this.eo.setAttribute(this.webComponent.name, value);
	}

	stopEditing() {
		this.valueString = undefined;
	}

	isValid(): boolean {
		let result = true;

		if (this.attributeMeta && this.attributeMeta.isNumber()) {
			let value = this.getModelValue();
			result = this.validationService.isValidForInput(value, 'number');
		}

		return result;
	}

	getCssClasses() {
		let css = this.getValidationCssClasses() || {};
		css.number = this.isNumber;
		return css;
	}
}
