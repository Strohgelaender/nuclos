import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { TinymceComponent } from 'angular2-tinymce/dist/angular2-tinymce.component';
import { AbstractInputComponent } from '../shared/abstract-input-component';
// import * as CKEDITOR from 'ckeditor';

declare var tinymce;

@Component({
	selector: 'nuc-web-html-editor',
	templateUrl: './web-html-editor.component.html',
	styleUrls: ['./web-html-editor.component.css']
})
export class WebHtmlEditorComponent extends AbstractInputComponent<WebHtmlEditor> implements OnInit {

	@ViewChild('tinymce') tinymce: TinymceComponent;

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

	onChangeCallback($event) {
		if (this.hasParsedValueChanged($event)) {
			this.setValue($event);
		}
	}

	private hasParsedValueChanged(newValue: string) {
		let htmlFromModel = this.parseModelValue();
		return htmlFromModel !== newValue;
	}

	private parseModelValue() {
		let modelValue = this.getValue();
		let parsedNode = new this.tinymce.editor.parser.parse(modelValue);
		let result = new tinymce.html.Serializer().serialize(parsedNode);

		return result;
	}
}
