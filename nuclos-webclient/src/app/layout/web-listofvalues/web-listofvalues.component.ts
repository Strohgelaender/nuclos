import { Component, ElementRef, Injector, OnInit } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { LovDataService } from '../../entity-object-data/shared/lov-data.service';
import { FqnService } from '../../shared/fqn.service';
import { AbstractLovComponent } from '../abstract-lov/abstract-lov.component';

@Component({
	selector: 'nuc-web-listofvalues',
	templateUrl: '../abstract-lov/abstract-lov.component.html',
	styleUrls: ['../abstract-lov/abstract-lov.component.scss']
})
export class WebListofvaluesComponent extends AbstractLovComponent<WebListofvalues> implements OnInit {

	constructor(
		private lovDataService: LovDataService,
		injector: Injector,
		fqnService: FqnService,
		elementRef: ElementRef
	) {
		super(injector, fqnService, elementRef);

		this.handler = {
			loadEntries: () => this.loadEntries(),
			loadFilteredEntries: search => this.loadFilteredEntries(search),
			getValue: () => this.getValue(),
			getVlpId: () => this.getVlpId(),
			getVlpParams: () => this.getVlpParams()
		};
	}

	ngOnInit() {
		super.ngOnInit();
	}

	loadEntries() {
		return this.loadFilteredEntries('');
	}

	loadFilteredEntries(search: string) {
		return this.lovDataService.loadLovEntries(this.getLovSearchConfig(search));
	}

	getVlpParams(): URLSearchParams {
		return this.lovDataService.getLovParams(this.getLovSearchConfig(''))
	}
}
