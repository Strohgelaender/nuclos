/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebListofvaluesComponent } from './web-listofvalues.component';

xdescribe('WebListofvaluesComponent', () => {
	let component: WebListofvaluesComponent;
	let fixture: ComponentFixture<WebListofvaluesComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebListofvaluesComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebListofvaluesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
