import { Component } from '@angular/core';
import { LovDataService } from '../../../entity-object-data/shared/lov-data.service';
import { BrowserRefreshService } from '../../../shared/browser-refresh.service';
import { FqnService } from '../../../shared/fqn.service';
import { AbstractReferenceTargetComponent } from '../abstract-reference-target/abstract-reference-target.component';

@Component({
	selector: 'nuc-search-reference-target',
	templateUrl: './search-reference-target.component.html',
	styleUrls: ['./search-reference-target.component.css']
})
export class SearchReferenceTargetComponent extends AbstractReferenceTargetComponent {

	constructor(
		protected browserRefreshService: BrowserRefreshService,
		protected fqnService: FqnService,
		protected lovDataService: LovDataService
	) {
		super(browserRefreshService, fqnService, lovDataService);
	}

	searchReference($event: MouseEvent) {
		this.targetReference(undefined, false, false, this.getVlpId(), this.getVlpParams());
		$event.stopPropagation();
	}
}
