import { Input, OnChanges, OnInit } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { EntityAttrMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { LovDataService } from '../../../entity-object-data/shared/lov-data.service';
import { BrowserRefreshService, SelectReferenceInOtherWindowEvent } from '../../../shared/browser-refresh.service';
import { FqnService } from '../../../shared/fqn.service';
import { LovHandler } from '../../abstract-lov/lov-handler';

/**
 * Created by Oliver Brausch on 21.10.17.
 */
export abstract class AbstractReferenceTargetComponent implements OnChanges, OnInit {
	@Input() protected eo: EntityObject| undefined;
	@Input() protected attributeMeta: EntityAttrMeta | undefined;
	@Input() protected popupParameter;
	@Input() protected lovHandler: LovHandler = LovHandler.DUMMY;

	isVisible = false;

	constructor(
		protected browserRefreshService: BrowserRefreshService,
		protected fqnService: FqnService,
		protected lovDataService: LovDataService
	) {
	}

	ngOnInit() {
		this.updateVisibility();
	}

	ngOnChanges() {
		this.updateVisibility();
	}

	getLovHandler(): LovHandler {
		return this.lovHandler;
	}

	protected getVlpId(): string | undefined {
		if (this.lovHandler.getVlpId) {
			return this.lovHandler.getVlpId();
		}
		return undefined;
	}

	protected getVlpParams(): URLSearchParams | undefined {
		if (this.lovHandler.getVlpParams) {
			return this.lovHandler.getVlpParams();
		}
		return undefined;
	}

	protected updateVisibility() {
		// This !this.isVisible is with purpose: Priviliges by attributeMeta will not be revoked on-the-fly
		if (!this.isVisible && this.attributeMeta && this.attributeMeta.isReference()) {
			this.lovDataService.canOpenReference(this.attributeMeta).subscribe(
				canOpen => this.isVisible = canOpen
			);
		}
	}

	protected targetReference(
		boId: string | number | undefined,
		withSaving: boolean,
		setReferenceUnsafe: boolean,
		vlpId: string | undefined,
		vlpParams: URLSearchParams | undefined) {

		let refAttrFqn = this.attributeMeta && this.attributeMeta.getReferencedEntityClassId();
		if (!refAttrFqn) {
			return;
		}

		this.browserRefreshService.openEoInNewTab(
			refAttrFqn,
			boId,
			withSaving,
			!withSaving,
			withSaving,
			this.popupParameter,
			vlpId,
			vlpParams
		).subscribe(
			(message: SelectReferenceInOtherWindowEvent) => {
				if (this.eo && this.attributeMeta !== undefined) {
					let fieldName = this.fqnService.getShortAttributeName(this.eo.getEntityClassId(), this.attributeMeta.getAttributeID());
					if (fieldName !== undefined) {
						let value = {
							id: message.eoId,
							name: message.name
						};
						if (setReferenceUnsafe) {
							this.eo.setAttributeUnsafe(fieldName, value);
						} else {
							this.eo.setAttribute(fieldName, value);
						}
						this.eo.setDirty(true);
					}
				}
			}
		);
	}
}
