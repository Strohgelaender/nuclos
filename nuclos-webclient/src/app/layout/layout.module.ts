import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { TinymceModule } from 'angular2-tinymce';
import { NgUploaderModule } from 'ngx-uploader';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { ViewPreferencesConfigModule } from '../entity-object/view-preferences-config/view-preferences-config.module';
import { GridModule } from '../grid/grid.module';
import { I18nModule } from '../i18n/i18n.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import { GridLayoutComponent } from './grid-layout/grid-layout.component';
import { LayoutComponent } from './layout.component';
import { ImageService } from './shared/image.service';
import { LayoutService } from './shared/layout.service';
import { WebAddonComponent } from './web-addon/web-addon.component';
import { WebButtonChangeStateComponent } from './web-button/web-button-change-state/web-button-change-state.component';
import { WebButtonDummyComponent } from './web-button/web-button-dummy/web-button-dummy.component';
import { WebButtonExecuteRuleComponent } from './web-button/web-button-execute-rule/web-button-execute-rule.component';
import { WebButtonGenerateObjectComponent } from './web-button/web-button-generate-object/web-button-generate-object.component';
import { WebButtonHyperlinkComponent } from './web-button/web-button-hyperlink/web-button-hyperlink.component';
import { WebCheckboxComponent } from './web-checkbox/web-checkbox.component';
import { WebComboboxComponent } from './web-combobox/web-combobox.component';
import { WebComponentComponent } from './web-component/web-component.component';
import { WebContainerComponent } from './web-container/web-container.component';
import { WebDatechooserComponent } from './web-datechooser/web-datechooser.component';
import { WebEmailComponent } from './web-email/web-email.component';
import { WebFileComponent } from './web-file/web-file.component';
import { WebGridCalculatedComponent } from './web-grid-calculated/web-grid-calculated.component';
import { WebHtmlEditorComponent } from './web-html-editor/web-html-editor.component';
import { WebHyperlinkComponent } from './web-hyperlink/web-hyperlink.component';
import { WebLabelStaticComponent } from './web-label-static/web-label-static.component';
import { WebLabelComponent } from './web-label/web-label.component';
import { AddReferenceTargetComponent } from './web-listofvalues/add-reference-target/add-reference-target.component';
import { EditReferenceTargetComponent } from './web-listofvalues/edit-reference-target/edit-reference-target.component';
import { SearchReferenceTargetComponent } from './web-listofvalues/search-reference-target/search-reference-target.component';
import { WebListofvaluesComponent } from './web-listofvalues/web-listofvalues.component';
import { WebMatrixComponent } from './web-matrix/web-matrix.component';
import { WebOptiongroupComponent } from './web-optiongroup/web-optiongroup.component';
import { WebPanelComponent } from './web-panel/web-panel.component';
import { WebPasswordComponent } from './web-password/web-password.component';
import { WebPhonenumberComponent } from './web-phonenumber/web-phonenumber.component';
import { WebSeparatorComponent } from './web-separator/web-separator.component';
import { SubformBooleanEditorComponent } from './web-subform/cell-editors/subform-boolean-editor/subform-boolean-editor.component';
import { SubformComboboxEditorComponent } from './web-subform/cell-editors/subform-combobox-editor/subform-combobox-editor.component';
import { SubformDateEditorComponent } from './web-subform/cell-editors/subform-date-editor/subform-date-editor.component';
import { SubformLovEditorComponent } from './web-subform/cell-editors/subform-lov-editor/subform-lov-editor.component';
import { SubformMultilineEditorComponent } from './web-subform/cell-editors/subform-multiline-editor/subform-multiline-editor.component';
import { SubformNumberEditorComponent } from './web-subform/cell-editors/subform-number-editor/subform-number-editor.component';
import { SubformStringEditorComponent } from './web-subform/cell-editors/subform-string-editor/subform-string-editor.component';
import {
	SubformBooleanRendererComponent,
	SubformDateRendererComponent,
	SubformDocumentRendererComponent,
	SubformEditRowRendererComponent,
	SubformHiddenRendererComponent,
	SubformNumberRendererComponent,
	SubformReferenceRendererComponent,
	SubformStateIconRendererComponent
} from './web-subform/cell-renderer';
import { SubformButtonsComponent } from './web-subform/subform-buttons/subform-buttons.component';
import { WebSubformComponent } from './web-subform/web-subform.component';
import { WebSubformService } from './web-subform/web-subform.service';
import { WebTabcontainerComponent } from './web-tabcontainer/web-tabcontainer.component';
import { WebTableComponent } from './web-table/web-table.component';
import { WebTextareaComponent } from './web-textarea/web-textarea.component';
import { WebTextfieldComponent } from './web-textfield/web-textfield.component';
import { WebTitledSeparatorComponent } from './web-titled-separator/web-titled-separator.component';

@NgModule({
	imports: [
		AgGridModule,
		AutoCompleteModule,
		CommonModule,
		FormsModule,

		AgGridModule.withComponents([
			SubformBooleanRendererComponent,
			SubformDateRendererComponent,
			SubformDocumentRendererComponent,
			SubformStateIconRendererComponent,
			SubformNumberRendererComponent,
			SubformReferenceRendererComponent,
			SubformEditRowRendererComponent,
			SubformHiddenRendererComponent,

			SubformComboboxEditorComponent,
			SubformDateEditorComponent,
			SubformLovEditorComponent,
			SubformNumberEditorComponent,
			SubformBooleanEditorComponent,
			SubformStringEditorComponent,
			SubformMultilineEditorComponent,
		]),

		I18nModule,
		ClickOutsideModule,
		UiComponentsModule,

		NgbModule,
		NgUploaderModule,
		ViewPreferencesConfigModule,
		GridModule,

		TinymceModule.withConfig({
			selector: 'textarea',
			skin_url: 'assets/tinymce/skins/lightgray',
			plugins: ['autoheight', 'code', 'link', 'paste', 'table']
		})
	],
	declarations: [
		AddReferenceTargetComponent,
		EditReferenceTargetComponent,
		GridLayoutComponent,
		LayoutComponent,
		SearchReferenceTargetComponent,
		SubformBooleanRendererComponent,
		SubformBooleanEditorComponent,
		SubformButtonsComponent,
		SubformComboboxEditorComponent,
		SubformDateEditorComponent,
		SubformDateRendererComponent,
		SubformDocumentRendererComponent,
		SubformLovEditorComponent,
		SubformMultilineEditorComponent,
		SubformNumberRendererComponent,
		SubformReferenceRendererComponent,
		WebAddonComponent,
		SubformStateIconRendererComponent,
		SubformStringEditorComponent,
		WebButtonChangeStateComponent,
		WebButtonDummyComponent,
		WebButtonExecuteRuleComponent,
		WebButtonGenerateObjectComponent,
		WebButtonHyperlinkComponent,
		WebCheckboxComponent,
		WebComboboxComponent,
		WebComponentComponent,
		WebContainerComponent,
		WebDatechooserComponent,
		WebEmailComponent,
		WebFileComponent,
		WebGridCalculatedComponent,
		WebHyperlinkComponent,
		WebLabelComponent,
		WebLabelStaticComponent,
		WebListofvaluesComponent,
		WebMatrixComponent,
		WebOptiongroupComponent,
		WebPasswordComponent,
		WebPhonenumberComponent,
		WebSeparatorComponent,
		WebSubformComponent,
		WebTabcontainerComponent,
		WebTableComponent,
		WebTextareaComponent,
		WebTextfieldComponent,
		WebTitledSeparatorComponent,
		SubformNumberEditorComponent,
		SubformStringEditorComponent,
		SubformEditRowRendererComponent,
		SubformHiddenRendererComponent,
		WebHtmlEditorComponent,
		WebPanelComponent,
		SubformStateIconRendererComponent,
		SubformButtonsComponent,
	],
	providers: [
		LayoutService,
		WebSubformService,
		ImageService,
	],
	exports: [
		LayoutComponent
	],
	// Add all dynamic WebLayout components here and in index.ts
	entryComponents: [
		WebAddonComponent,
		WebButtonChangeStateComponent,
		WebButtonDummyComponent,
		WebButtonExecuteRuleComponent,
		WebButtonGenerateObjectComponent,
		WebButtonHyperlinkComponent,
		WebCheckboxComponent,
		WebComboboxComponent,
		WebComponentComponent,
		WebContainerComponent,
		WebDatechooserComponent,
		WebEmailComponent,
		WebFileComponent,
		WebHtmlEditorComponent,
		WebHyperlinkComponent,
		WebLabelComponent,
		WebLabelStaticComponent,
		WebListofvaluesComponent,
		WebMatrixComponent,
		WebOptiongroupComponent,
		WebPanelComponent,
		WebPasswordComponent,
		WebPhonenumberComponent,
		WebSeparatorComponent,
		WebSubformComponent,
		WebTabcontainerComponent,
		WebTableComponent,
		WebTextareaComponent,
		WebTextfieldComponent,
		WebTitledSeparatorComponent
	]
})
export class LayoutModule {
}
