import { Component, EventEmitter, Inject, Injector, NgZone, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UploaderOptions, UploadFile, UploadInput, UploadOutput } from 'ngx-uploader';
import { Observable } from 'rxjs';
import { AttrType, DocumentAttribute } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventListener } from '../../entity-object-data/shared/entity-object-event-listener';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { DialogService } from '../../popup/dialog/dialog.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { AbstractInputComponent } from '../shared/abstract-input-component';
import { ImageService } from '../shared/image.service';

@Component({
	selector: 'nuc-web-file',
	templateUrl: './web-file.component.html',
	styleUrls: ['./web-file.component.scss']
})
export class WebFileComponent extends AbstractInputComponent<WebFile> implements OnInit, OnChanges {


	private uploadToken: string;
	dragOver: boolean;
	isImage: boolean;
	isUploaded: boolean;
	options: UploaderOptions;
	uploadInput: EventEmitter<UploadInput> = new EventEmitter();
	documentIsSelected = false;
	showUploadLabel: boolean;

	fileName: string;
	fileIconUrl: string | undefined;
	uploadedFile: UploadFile | undefined;

	private eoListener: EntityObjectEventListener = {
		afterSave: () => this.initDocument(),
		afterCancel: () => this.initDocument(),
		afterReload: () => this.initDocument()
	};

	constructor(
		injector: Injector,
		protected nuclosConfigService: NuclosConfigService,
		private dialogService: DialogService,
		private nuclosI18nService: NuclosI18nService,
		private imageService: ImageService,
		@Inject(NgZone) private zone: NgZone
	) {
		super(injector);
	}

	ngOnInit() {
		if (this.eo) {
			this.eo.addListener(this.eoListener);
		}

		this.initDocument();
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChange = changes['eo'];
		if (eoChange) {
			let previousEo = eoChange.previousValue;
			if (previousEo) {
				previousEo.removeListener(this.eoListener);
			}
			let currentEo = eoChange.currentValue;
			if (currentEo) {
				currentEo.addListener(this.eoListener);
			}
			this.initDocument();
		}
	}


	private initDocument() {
		this.documentIsSelected = false;
		delete this.fileName;
		delete this.uploadToken;

		this.showUploadLabel = true;
		let documentAttribute = this.getDocumentAttribute();
		if (documentAttribute) {
			this.fileName = documentAttribute.name;
			this.documentIsSelected = !!documentAttribute.id;

			this.fileIconUrl = this.getFileIconUrl();

			if (documentAttribute.id || this.getImageUrl()) {
				this.isUploaded = true;
				this.showUploadLabel = false;
			}
		}
		this.getIsImage().subscribe(isImage => {
			this.isImage = isImage;
		});

		if (this.hasImageHref()) {
			this.documentIsSelected = true;
		}
	}

	handleUpload(uploadedOutput: UploadOutput) {

		if (uploadedOutput.type === 'allAddedToQueue') { // when all files added in queue
			// auto upload files when added
			this.uploadInput.emit({
				type: 'uploadAll',
				url: this.nuclosConfigService.getRestHost() + '/boDocuments/upload',
				withCredentials: true
			});
		} else if (uploadedOutput.type === 'dragOver') {
			this.dragOver = true;
			return;
		}

		this.dragOver = false;

		let uploadedFile = uploadedOutput.file;

		if (uploadedFile && uploadedFile.responseStatus && uploadedFile.responseStatus !== 200) {
			this.dialogService.alert(
				{
					title: this.nuclosI18nService.getI18n('webclient.error.title'),
					message: uploadedFile.response
				}
			);
		}

		if (uploadedOutput.type === 'start') {
			setTimeout(() => {
				this.uploadedFile = uploadedFile;
				if (this.uploadedFile) {
					this.uploadToken = this.uploadedFile.response;
					if (this.uploadToken === undefined) {
						// the ngx-uploader calls the handleUpload function before the upload response is retrieved completely
						// sometimes the response is not already available (happened with Firefox), so try again
						this.handleUpload(uploadedOutput);
						return;
					}
				}

				if (this.uploadedFile) {
					this.fileName = this.uploadedFile.name;
					let fileExtension = this.uploadedFile.name.split('.').pop();
					if (fileExtension) {
						this.fileIconUrl = this.getFileIconUrlForFileType(fileExtension);
					}
				}
				this.documentIsSelected = true;
				this.setDocumentAttribute(
					{
						'uploadToken': this.uploadToken,
						'name': this.fileName
					}
				);

				this.isUploaded = false;
				this.showUploadLabel = false;
			});
		}

	}

	hasDocument(): boolean {
		let result = false;

		if (this.isImage) {
			result = this.hasImageHref();
		} else {
			let attribute = this.getDocumentAttribute();
			result = !!(attribute && attribute.id);
		}
		return result;
	}

	private hasImageHref() {
		if (this.getImageUrl()) {
			return true;
		}
		let href = this.imageService.getImageHref(
			this.getEntityObject(),
			this.webComponent.name
		);
		return !!href;
	}

	openImageModal() {
		let imageUrl = this.getImageUrl();

		if (!imageUrl) {
			Logger.instance.warn('Can not open empty image URL');
			return;
		}

		this.imageService.openImage(imageUrl);
	}

	clickOnFile() {
		if (this.documentIsSelected) {
			if (this.isImage) {
				this.openImageModal();
			} else {
				if (this.hasDocument()) {
					this.openDocument();
				}
			}
		}
	}


	getFileIconUrl(): string | undefined {
		let documentAttribute = this.getDocumentAttribute();
		if (!documentAttribute || !documentAttribute.id) {
			return undefined;
		}
		let fileType = 'generic';
		if (documentAttribute && documentAttribute.fileType) {
			fileType = documentAttribute.fileType;
		}
		return this.getFileIconUrlForFileType(fileType.toLowerCase());
	}

	private guessFileTypeFromFileExtension(extension: string | undefined): string | undefined {
		switch (extension) {
			case 'generic':
				return 'generic';
			case 'pdf':
				return 'pdf';
			case 'xls':
			case 'xlsx':
				return 'excel';
			case 'ppt':
			case 'pptx':
				return 'powerpoint';
			case 'doc':
			case 'docx':
				return 'word';
			case 'txt':
				return 'text';
			default:
				return undefined;
		}
	}

	private getFileIconUrlForFileType(fileType: string): string | undefined {
		let fileTypePrefix = this.guessFileTypeFromFileExtension(fileType);
		return fileTypePrefix ? 'assets/file/' + fileTypePrefix.toLowerCase() + '-file.png' : undefined;
	}


	protected openDocument() {
		let docAttrId = this.eo.getEntityClassId() + '_' + this.webComponent.name;
		// TODO: HATEOAS
		let selfUrl = this.eo.getSelfURL();
		if (selfUrl) {
			let link;
			if (this.isImage) {
				link = selfUrl.replace('/bos/', '/boImages/') + '/images/' + docAttrId + '/' + this.eo.getAttribute('version');
			} else {
				link = selfUrl.replace('/bos/', '/boDocuments/') + '/documents/' + docAttrId;
			}
			window.open(link);
		}
	}

	removeDocument(): void {
		this.resetDocument();
		delete this.uploadToken;
		delete this.fileIconUrl;
	}

	protected resetDocument(): void {
		this.eo.setAttribute(this.webComponent.name, null);
		this.resetModel(this.eo.getData().attrImages);
	}

	protected resetModel(attrImages): void {
		delete this.fileName;
		delete this.uploadedFile;
		this.showUploadLabel = true;
		this.documentIsSelected = false;

		if (attrImages && attrImages.links) {
			if (attrImages.links.image && this.webComponent.name === 'image') {
				delete attrImages.links.image;
			}
			if (attrImages.links[this.webComponent.name]) {
				delete attrImages.links[this.webComponent.name];
			}
		}
	}

	private getIsImage(): Observable<boolean> {
		// TODO
		return new Observable(
			observer => {
				this.eo.getMeta().subscribe(
					meta => {
						let attribute = meta.getAttribute(this.webComponent.name);
						if (attribute) {
							observer.next(attribute.type === AttrType.IMAGE);
							observer.complete();
						}
					}
				);
			}
		);
	}

	/**
	 * @return undefined if attribute is image
	 */
	getDocumentAttribute(): DocumentAttribute | undefined {
		return this.eo.getAttribute(this.webComponent.name);
	}

	setDocumentAttribute(value): void {
		return this.eo.setAttribute(this.webComponent.name, value);
	}

	getImageUrl() {
		if (!this.documentIsSelected) {
			return;
		}

		if (this.getEntityObject().isDirty() && this.uploadToken) {
			// temporarily uploaded file
			return this.nuclosConfigService.getRestHost() + '/boImages/temp/' + this.uploadToken;
		}

		return this.imageService.getImageHref(this.getEntityObject(), this.getImageAttributeId());
	}

	protected getImageAttributeId() {
		return this.webComponent.name;
	}
}
