/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebButtonDummyComponent } from './web-button-dummy.component';

xdescribe('WebButtonDummyComponent', () => {
	let component: WebButtonDummyComponent;
	let fixture: ComponentFixture<WebButtonDummyComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebButtonDummyComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebButtonDummyComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
