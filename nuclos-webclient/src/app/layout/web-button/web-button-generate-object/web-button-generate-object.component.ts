import { Component, Injector } from '@angular/core';
import { NuclosGenerationService } from '../../../generation/shared/nuclos-generation.service';
import { Logger } from '../../../log/shared/logger';
import { WebButtonComponent } from '../web-button.component';

@Component({
	selector: 'nuc-web-button-generate-object',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonGenerateObjectComponent extends WebButtonComponent<WebButtonGenerateObject> {

	constructor(
		injector: Injector,
		private generationService: NuclosGenerationService,
		private $log: Logger
	) {
		super(injector);
	}

	getGeneration() {
		return this.eo.getGenerations().find(
			it => it.generationId === this.webComponent.objectGenerator
		);
	}

	buttonClicked() {
		let generation = this.getGeneration();

		if (!generation) {
			this.$log.error('Unknown generation: %o', this.webComponent.objectGenerator);
			return;
		}

		this.generationService.confirmAndGenerate(
			this.eo,
			generation
		);
	}

	getCssClass(): string {
		return 'nuc-button-generate-object';
	}

	isEnabled() {
		return super.isEnabled() && this.getGeneration() !== undefined;
	}
}
