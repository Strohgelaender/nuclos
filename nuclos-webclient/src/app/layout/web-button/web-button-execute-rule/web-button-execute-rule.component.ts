import { Component, Injector } from '@angular/core';
import { WebButtonComponent } from '../web-button.component';

@Component({
	selector: 'nuc-web-button-execute-rule',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonExecuteRuleComponent extends WebButtonComponent<WebButtonExecuteRule> {

	constructor(
		injector: Injector
	) {
		super(injector);
	}

	buttonClicked() {
		this.eo.executeRule(this.webComponent.rule).subscribe(
			eo => eo.select()
		);
	}

	getCssClass(): string {
		return 'nuc-button-execute-rule';
	}

	isEnabled(): boolean {
		return super.isEnabled() && this.eo && !this.eo.isNew()
			&& this.isButtonEnabled();
	}

	private isButtonEnabled() {
		return this.eo.isAttributeWritable(this.webComponent.name);
	}
}
