/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebTextareaComponent } from './web-textarea.component';

xdescribe('WebTextareaComponent', () => {
	let component: WebTextareaComponent;
	let fixture: ComponentFixture<WebTextareaComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebTextareaComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebTextareaComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
