import { Component, ElementRef, Injector, OnInit } from '@angular/core';
import { ColDef, GridApi, RowNode } from 'ag-grid';
import { Subscription } from 'rxjs';
import { EntityObjectData } from '../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { SortModel } from '../../entity-object-data/shared/sort.model';
import { EntityObjectGridService } from '../../entity-object/entity-object-grid/entity-object-grid.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { StringUtils } from '../../shared/string-utils';
import { AbstractGridComponent } from '../shared/abstract-grid-component';
import { SubformComboboxEditorComponent } from '../web-subform/cell-editors/subform-combobox-editor/subform-combobox-editor.component';
import { SubformStringEditorComponent } from '../web-subform/cell-editors/subform-string-editor/subform-string-editor.component';
import { SubformBooleanRendererComponent, SubformReferenceRendererComponent } from '../web-subform/cell-renderer';
import { NuclosCellEditorParams, NuclosCellRendererParams } from '../web-subform/web-subform.model';

@Component({
	selector: 'nuc-web-matrix',
	templateUrl: './web-matrix.component.html',
	styleUrls: ['./web-matrix.component.scss']
})
export class WebMatrixComponent extends AbstractGridComponent<WebMatrix> implements OnInit {

	private loadMatrixSubscription: Subscription;
	private loadSubDependentSubscription: Subscription;

	constructor(
		injector: Injector,
		private eoGridService: EntityObjectGridService,
		private dataService: DataService,
		private $log: Logger,
		i18nService: NuclosI18nService,
		elementRef: ElementRef
	) {
		super(injector, i18nService, elementRef);

		this.eoChangeListener = {
			afterAttributeChange: (
				entityObject: SubEntityObject,
				attributeName: string,
				oldValue: any,
				newValue: any
			) => this.attributeChanged(entityObject, attributeName, oldValue, newValue)
		};

		this.gridOptions.enableColResize = true;
	}

	protected onGridApiReady() {
	}

	ngOnInit() {
		this.init();
	}

	protected init() {
		this.$log.debug('Matrix init...');
		this.dependents = this.eo.getDependents(this.webComponent.entityYParentField);
		this.initColumns({xAxisBoList: []}, 12);
		this.hardRefresh();
		this.loadData();
	}

	loadData() {
		this.loadMatrixSubscription = this.dataService.loadMatrixData(this.webComponent, this.eo).subscribe(data => {
			this.initColumns(data, 108);

			if (this.dependents) {
				this.dependents.asObservable().subscribe(
					(dependents: SubEntityObject[]) => {
						this.initCellData(data, dependents);
						if (dependents) {
							dependents.forEach(eo => eo.addListener(this.eoChangeListener));
						}
						this.updateGridData();
						this.hardRefresh();
					}
				);
				this.dependents.loadIfEmpty();
			}
		});

	}

	private initCellData(data, dependents: SubEntityObject[] | undefined) {

		let dropDownMap = {};
		if (data.matrixValues) {
			for (let value of data.matrixValues) {
				dropDownMap[value.id] = value;
			}
		}

		if (dependents) {
			dependents.length = 0;
		}

		for (let row of data.yAxisBoList) {

			let valueMap = {};
			valueMap['0'] = row.evaluatedHeaderTitle;
			valueMap[this.webComponent.entityYParentField] = {id: this.eo.getId()};

			let subEo = new SubEntityObject(
				this.eo,
				this.webComponent.entityYParentField,
				new EntityObjectData()
			);
			subEo.init(this.webComponent.entityY, row.boId, valueMap);

			let subsubEos: SubEntityObject[] = [];

			for (let column of data.xAxisBoList) {
				let key = column.boId + '_' + row.boId;
				let dict = data.entityMatrixBoDict[key];

				if (dict !== undefined) {
					let attrValue = dropDownMap[dict.value] ? dropDownMap[dict.value] : dict.value;
					subEo.setAttributeUnsafe(column.boId + '', attrValue);

					let sseo = this.createSubSubDependent(column.boId, row.boId, dict.value, dict.boId, subEo);

					subsubEos.push(sseo);
				}
			}

			subEo.getDependents(this.webComponent.entityFieldMatrixParent).set(subsubEos);
			subEo.setDirty(false);

			if (dependents) {
				dependents.push(subEo);
			}

		}

		// set header height / width to fit containing content
		setTimeout(() => {
			// determine the maximum height of the header
			if (this.gridOptions.api) {
				let maxHeaderHeight = $($('.ag-header-cell-label').toArray().reduce((prev, current) => {
					return ($(prev).height() > $(current).height()) ? prev : current;
				})).height();
				this.gridOptions.api.setHeaderHeight(maxHeaderHeight);

				if (this.gridOptions.columnApi && this.gridOptions.columnDefs && this.gridOptions.columnDefs.length > 0) {
					let colDef: ColDef = this.gridOptions.columnDefs[0];
					if (colDef.field) {
						this.gridOptions.columnApi.autoSizeColumn(colDef.field, 'autosizeColumns');
					}
				}
			}
		}, 100);

	}

	private createSubSubDependent(
		columnId: number,
		rowId: number,
		value: any,
		boId: number | undefined,
		parentEo: EntityObject
	): SubEntityObject {
		let valueMap = {};
		valueMap[this.webComponent.entityMatrixValueField] = value;
		valueMap[this.webComponent.entityFieldMatrixXRefField] = {id: columnId};
		valueMap[this.webComponent.entityFieldMatrixParent] = {id: rowId};

		let sseo = new SubEntityObject(
			parentEo,
			this.webComponent.entityFieldMatrixXRefField,
			new EntityObjectData()
		);
		sseo.init(this.webComponent.entityMatrix, boId, valueMap);

		return sseo;
	}

	private initColumns(data: any, width: number) {
		let colDefs = this.createColumnDefinitions(width, data);
		this.setColumns(colDefs);
	}

	private createColumnDefinitions(width: number, data: any) {
		let firstColDef: ColDef = {
			headerName: '',
			width: width * 2,
			colId: '0',
			field: '0',
			editable: false,
			valueGetter: this.eoGridService.valueGetter,
			pinned: true
		};

		let columnDefinitions = [firstColDef];

		for (let column of data.xAxisBoList) {

			let editable = this.isEnabled();

			let id: string = String(column.boId);

			let colDef: ColDef = {
				headerName: column._title,
				field: id,
				colId: id,
				width: width,
				editable: editable,
				valueGetter: this.eoGridService.valueGetter,
				newValueHandler: this.newValueHandler
			};

			this.$log.debug('%o: %o, colDef = %o', id, editable, colDef);

			if (this.webComponent.cellInputType === 'checkicon') {
				colDef.cellRendererFramework = SubformBooleanRendererComponent;
				colDef.editable = false;
				colDef.cellRendererParams = {
					nuclosCellRenderParams: {
						editable: editable
					} as NuclosCellRendererParams
				};

			} else if (this.webComponent.cellInputType === 'combobox') {

				colDef.cellRendererFramework = SubformReferenceRendererComponent;
				colDef.cellEditorFramework = SubformComboboxEditorComponent;

				if (data.matrixValues && data.matrixValues[0] && data.matrixValues[0].id !== null) {
					data.matrixValues.unshift({id: null, name: ' '});
				}

				// provide data for render component
				colDef.cellEditorParams = {
					// TODO: Load Meta object properly instead of faking it here
					staticList: data.matrixValues
				} as NuclosCellEditorParams;

			} else {
				colDef.cellEditorFramework = SubformStringEditorComponent;
			}

			columnDefinitions.push(colDef);
		}

		return columnDefinitions;
	}

	protected cancel() {
		if (this.loadSubDependentSubscription) {
			this.loadSubDependentSubscription.unsubscribe();
		}
	}

	/**
	 *
	 * @param entityObject
	 * @param attributeName
	 * @param oldValue
	 * @param newValue
	 */
	private attributeChanged(
		entityObject: SubEntityObject,
		attributeName: string,
		_oldValue: any,
		newValue: any
	) {
		attributeName = StringUtils.replaceAll(attributeName, '_', '');

		this.$log.debug('attribute changed: %o = %o', attributeName, newValue);
		this.loadSubDependentSubscription = entityObject.getDependentsAndLoad(
			this.webComponent.entityFieldMatrixParent, new SortModel([])
		).asObservable().subscribe(subdependents => {
			if (!subdependents) {
				return;
			}

			// First find the sub-sub-entity-obejct if it exists.
			let existing: SubEntityObject | undefined;
			for (let subdependent of subdependents) {
				let xId = subdependent.getAttribute(this.webComponent.entityFieldMatrixXRefField).id;
				if (String(xId) === attributeName) {
					existing = subdependent as SubEntityObject;
					break;
				}
			}

			let value = this.getMatrixValue(newValue);

			if (existing) {
				existing.setAttributeUnsafe(this.webComponent.entityMatrixValueField, value);

				if (value) {
					existing.getData()._flag = 'update';

				} else {
					if (existing.getData()._flag === 'insert') {
						// It's not persistent, so just remove it from the list
						let index = subdependents.indexOf(existing);
						subdependents.splice(index, 1);
					} else {
						// It's in the DB, so mark it for deletion
						existing.getData()._flag = 'delete';
					}
				}

			} else if (value) {
				// First time this cell gets a value, thus create the sub-sub-eo.
				let sseo = this.createSubSubDependent(
					Number(attributeName),
					Number(entityObject.getId()),
					value,
					undefined,
					entityObject
				);
				subdependents.push(sseo);
			}

		});
	}

	getMatrixValue(newValue): any {
		if (newValue === true) {
			return 1;
		} else if (newValue === false) {
			return 0;
		} else if (newValue.id !== undefined) {
			return newValue.id;
		}
		return newValue;
	}

	protected getLogger(): Logger {
		return this.$log;
	}

	/**
	 * @override set attribute without async validation to fix NUCLOS-6199
	 */
	protected newValueHandler(params: {
		node: RowNode,
		data: SubEntityObject,
		oldValue: any,
		newValue: any,
		colDef: ColDef,
		api: GridApi,
		context: any
	}): boolean {

		if (!params.colDef.field) {
			this.getLogger().warn('Field missing: %o', params.colDef);
			return false;
		}

		// TODO params.colDef.field is a data record id not an attributeName
		params.node.data.setAttributeValidated(params.colDef.field, params.newValue);

		return false;
	}
}
