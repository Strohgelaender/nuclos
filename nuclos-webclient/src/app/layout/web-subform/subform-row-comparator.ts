import { RowNode } from 'ag-grid';

declare var require: any;
let naturalCompare = require('string-natural-compare');

/**
 * Does "natural comparison", but sorts empty Strings to first/last place (depending on sort order).
 * The naturalCompare would put empty Strings somewhere in between.
 */
let nuclosTextCompare = (textA, textB) => {
	if (!textA && textB) {
		return -1;
	} else if (textA && !textB) {
		return 1;
	} else {
		return naturalCompare(textA, textB);
	}
};

const compareSelectionFirst = (valueComparator: (valueA?, valueB?) => any) => {
	return (valueA, valueB, nodeA: RowNode, nodeB: RowNode, isInverted) => {
		let result;
		if (!!nodeA.data.isNew() === !!nodeB.data.isNew()) {
			return valueComparator(valueA, valueB);
		} else if (nodeA.data.isNew()) {
			result = -1;
		} else {
			result = 1;
		}
		if (isInverted) {
			result = -result;
		}
		return result;
	};
};

const nullAndUndefinedFirst = (valueComparator: (valueA?, valueB?) => any) => {
	return (valueA, valueB) => {
		let undefinedA = valueA === null || typeof valueA === 'undefined';
		let undefinedB = valueB === null || typeof valueB === 'undefined';

		if (undefinedA && undefinedB) {
			return 0;
		} else if (undefinedA && !undefinedB) {
			return -1;
		} else if (!undefinedA && undefinedB) {
			return 1;
		} else {
			return valueComparator(valueA, valueB);
		}
	};
};

export class SubformRowComparator {
	static nameComparator = compareSelectionFirst(
		(valueA?: { name: string }, valueB?: { name: string }) => {
			let nameA = valueA && valueA.name;
			let nameB = valueB && valueB.name;
			return nuclosTextCompare(nameA, nameB);
		}
	);

	static textComparator = compareSelectionFirst(nuclosTextCompare);

	static booleanComparator = compareSelectionFirst(
		nullAndUndefinedFirst(
			(valueA, valueB) => valueA - valueB
		)
	);

	static numberComparator = compareSelectionFirst(
		nullAndUndefinedFirst(
			(valueA, valueB) => valueA - valueB
		)
	);
}
