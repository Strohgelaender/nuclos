import { Component } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { SubEntityObject } from '../../../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { DetailModalService } from '../../../../entity-object/shared/detail-modal.service';
import { Logger } from '../../../../log/shared/logger';
import { EXPAND_SIDEVIEW_PARAM, SHOW_CLOSE_ON_SAVE_BUTTON_PARAM } from '../../../../shared/browser-refresh.service';
import { LayoutService } from '../../../shared/layout.service';
import { RowEditCellParams } from '../../web-subform.model';
import { AbstractRendererComponent } from '../abstract-renderer-component';

@Component({
	selector: 'nuc-subform-edit-row-renderer',
	templateUrl: './subform-edit-row-renderer.component.html',
	styleUrls: ['./subform-edit-row-renderer.component.css']
})
export class SubformEditRowRendererComponent extends AbstractRendererComponent {

	showEditRowInNewTab = false;
	showEditRowInPopup = false;

	private cellRenderParams: RowEditCellParams;
	private popupParameter: string | undefined;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private layoutService: LayoutService,
		private detailModalService: DetailModalService
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.agInit(params);

		this.cellRenderParams = params;
		this.getSubEntityObject().getMeta().subscribe(meta => {

			this.popupParameter = this.getPopupParameter(meta);

			let showEditRowInNewTabOrPopup: boolean = !!this.getSubEntityObject().getDetailLink();

			this.showEditRowInNewTab = showEditRowInNewTabOrPopup && this.popupParameter === undefined;
			this.showEditRowInPopup = showEditRowInNewTabOrPopup && this.popupParameter !== undefined;
		});
	}


	private baseUrl() {
		return window.location.href.substring(0, window.location.href.indexOf('#/') + 2);
	}

	openInModal() {
		let subEo = this.getSubEntityObject();
		if (!subEo.isComplete()) {
			this.loadCompleteSubEO(subEo);
		}
		this.detailModalService.openEoInModal(subEo).subscribe();
	}

	private loadCompleteSubEO(subEo: SubEntityObject) {
		let id = subEo.getId();
		if (id) {
			subEo.reload().subscribe(
				() => subEo.setComplete(true)
			);
		}
	}

	openInNewTab() {
		this.openInNewTabOrPopup(undefined);
	}

	openInPopup() {
		this.openInNewTabOrPopup(this.popupParameter);
	}

	private getPopupParameter(meta): string | undefined {


		// popupaprameter from advanced properties
		if (this.cellRenderParams instanceof RowEditCellParams) { // TODO this is never true ??
			return this.cellRenderParams.getAdvancedProperty('popupparameter');
		} else {
			let advancedProperties = new RowEditCellParams(this.cellRenderParams['advancedProperties']);
			let popupParameter = advancedProperties.getAdvancedProperty('popupparameter');
			if (popupParameter) {
				return popupParameter;
			}
		}

		// popupparameter from look and feel parameters
		return meta.getPopupParameter();
	}

	private openInNewTabOrPopup(popupParameter) {
		let link = this.getSubEntityObject().getDetailLink();
		if (!link) {
			Logger.instance.error('Can not open Sub-EO without detail link: %o', this.getSubEntityObject());
			return;
		}

		this.getSubEntityObject().getMeta().subscribe(meta => {
			// NUCLOS-4435: Details of dynamik BO
			let entityClassId = meta.getDetailEntityClassId() || meta.getBoMetaId();
			if (!entityClassId) {
				entityClassId = this.getSubEntityObject().getEntityClassId();
			}

			let href =
				this.baseUrl() +
				(popupParameter ? 'popup/' : 'view/') +
				entityClassId +
				'/' +
				this.getSubEntityObject().getId() +
				'?' + EXPAND_SIDEVIEW_PARAM +
				'&' + SHOW_CLOSE_ON_SAVE_BUTTON_PARAM;

			window.open(href, '_blank', popupParameter);
		});
	}

	canOpenWindow() {
		if (this.canOpenModal() && !this.popupParameter) {
			return false;
		}
		let subEO = this.getSubEntityObject();
		return !subEO.isDirty();
	}

	canOpenModal() {
		if (!this.cellRenderParams.canOpenModal) {
			return false;
		}
		return this.isCompleteOrUntouched();
	}

	isCompleteOrUntouched() {
		let subEO = this.getSubEntityObject();
		return subEO.isComplete() || !subEO.isDirty();
	}
}
