import * as _ from 'lodash';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

export class SubformEoStatusToRowClass {

	static cellClassRules = {
		'row-new': params => params.data.isNew(),
		'row-modified': params => params.data.isDirty(),
		'row-deleted': params => params.data.isMarkedAsDeleted(),
	};

	static getCurrentRowClasses(eo: EntityObject): string[] {
		let result: string[] = [];
		_.forOwn(SubformEoStatusToRowClass.cellClassRules, (className: any, evaluator: any) => {
			if (evaluator({data: eo})) {
				result.push(className);
			}
		});
		return result;
	}

	static addCellClassRulesTo(existingClassRules: any) {
		_.assign(existingClassRules, this.cellClassRules);
	}
}
