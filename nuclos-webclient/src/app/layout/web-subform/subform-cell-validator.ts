import { ValidationStatus } from '../../validation/validation-status.enum';

export class SubformCellValidator {
	private cssClasses;

	constructor(
		private params,
		private validator
	) {
		this.cssClasses = this.getCssClasses();
	}

	hasClass(name: string) {
		return this.cssClasses && this.cssClasses[name];
	}

	private getCssClasses(): any {
		let status = this.getValidationStatus();
		if (this.params.validationStatus === status) {
			return this.params.cssClasses || {};
		}

		this.params.validationStatus = status;
		let result: any = {
			'nuc-validation': true
		};

		if (status !== undefined) {
			let statusName = ValidationStatus[status].toLowerCase();
			result['nuc-validation-' + statusName] = true;
		}

		this.params.cssClasses = result;

		return result;
	}

	/**
	 * Gets the validation status based on the model value.
	 *
	 * @returns {ValidationStatus|undefined}
	 */
	private getValidationStatus() {
		if (!this.params.data) {
			return undefined;
		}

		let colId = this.params.colDef.colId;
		if (!colId) {
			return undefined;
		}

		let status = this.params.data.getValidationStatus(colId);

		if (this.hasInput() && !this.isValid()) {
			status = ValidationStatus.INVALID;
		}

		return status;
	}

	/**
	 * Determines if the current input value is valid.
	 * Additional constraints might be applied here (e.g. email format validation, etc.).
	 * Subclasses which do actual validation themselves should override this method.
	 *
	 * @returns {ValidationStatus|undefined}
	 */
	private isValid(): boolean {
		if (!this.validator) {
			return true;
		}
		return this.validator(this.params);
	}

	private hasInput() {
		let value = this.params.value;

		if (value === null || value === undefined) {
			return false;
		}

		// Reference attribute must have an ID
		if (typeof value === 'object') {
			if (!value.id) {
				return false;
			}
		}

		return true;
	}
}
