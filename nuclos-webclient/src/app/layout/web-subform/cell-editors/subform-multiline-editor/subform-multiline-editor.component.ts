import { Component, ElementRef, ViewChild } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { Logger } from '../../../../log/shared/logger';
import { NuclosCellEditorParams } from '../../web-subform.model';
import { WebSubformService } from '../../web-subform.service';
import { AbstractEditorComponent } from '../abstract-editor-component';

@Component({
	selector: 'nuc-subform-multiline-editor',
	templateUrl: './subform-multiline-editor.component.html',
	styleUrls: ['./subform-multiline-editor.component.css']
})
export class SubformMultilineEditorComponent extends AbstractEditorComponent {

	@ViewChild('input') input: ElementRef;

	private initialHeight;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private $log: Logger,
		ref: ElementRef
	) {
		super(entityObjectService, eoEventService, ref);
	}

	agInit(params: NuclosCellEditorParams): any {
		super.agInit(params);

		setTimeout(() => {
			if (this.input) {
				this.input.nativeElement.focus();
			}
		});

		if (params.charPress) {
			super.setValue(params.charPress);
			return;
		}

		super.setValue(params.value);
	}

	setStringValue(value) {
		this.setEntityObjectDirty();
		super.setValue(value);
	}

	isPopup() {
		// Edit textarea in a popup to allow dynamic size changes
		return true;
	}

	getHeight() {
		let newHeight = WebSubformService.calculateCellHeightForText(
			this.getValue(),
			['form-control', 'form-control-sm']
		).height;

		// Editor should have at least the row height
		newHeight = Math.max(newHeight, super.getHeight());

		if (!this.initialHeight) {
			this.initialHeight = newHeight;
			return newHeight;
		}

		return Math.max(this.initialHeight, newHeight);
	}

	keydown(event: KeyboardEvent): void {
		// Enter
		if (event.keyCode === 13) {
			// Do not propagate to grid, or editing will stop
			event.stopPropagation();
			return;
		}
		super.keydown(event);
	}
}
