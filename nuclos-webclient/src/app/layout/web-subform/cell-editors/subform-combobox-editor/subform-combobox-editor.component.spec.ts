import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SubformComboboxEditorComponent } from './subform-combobox-editor.component';

xdescribe('ComboboxEditorComponent', () => {
	let component: SubformComboboxEditorComponent;
	let fixture: ComponentFixture<SubformComboboxEditorComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SubformComboboxEditorComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SubformComboboxEditorComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
