import { Component, ElementRef, ViewChild } from '@angular/core';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { Logger } from '../../../../log/shared/logger';
import { NumberService } from '../../../../shared/number.service';
import { AbstractEditorComponent } from '../abstract-editor-component';

@Component({
	selector: 'nuc-subform-number-editor',
	templateUrl: './subform-number-editor.component.html',
	styleUrls: ['./subform-number-editor.component.css']
})
export class SubformNumberEditorComponent extends AbstractEditorComponent {

	@ViewChild('input') input;

	private stringValue = '';

	constructor(
		private numberService: NumberService,
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private $log: Logger,
		ref: ElementRef
	) {
		super(entityObjectService, eoEventService, ref);
	}

	agInit(params: any): any {
		super.agInit(params);

		if (params.charPress) {
			this.stringValue = params.charPress;
			return;
		}

		let value = +super.getValue();
		if (!isNaN(value)) {
			this.stringValue = this.numberService.format(value, this.attributeMeta.getPrecision());
		}
	}

	getValue(): any {
		let result = this.numberService.parseNumber(this.stringValue, this.attributeMeta.getPrecision());
		this.$log.debug('getValue() = %o', result);
		return result;
	}

	getStringValue() {
		return this.stringValue;
	}

	setStringValue(value) {
		this.$log.debug('setStringValue(%o)', value);
		this.stringValue = value;
		super.setValue(value);
	}
}
