import { Component, ElementRef, ViewChild } from '@angular/core';
import { WebDatechooserComponent } from '../../..';
import { EntityObjectEventService } from '../../../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object-data/shared/entity-object.service';
import { FqnService } from '../../../../shared/fqn.service';
import { DatepickerPosition } from '../../../../ui-components/datechooser/datepicker-position';
import { AbstractEditorComponent } from '../abstract-editor-component';

@Component({
	selector: 'nuc-subform-date-editor',
	templateUrl: 'subform-date-editor.component.html',
	styleUrls: ['subform-date-editor.component.css']
})
export class SubformDateEditorComponent extends AbstractEditorComponent {

	datepickerPosition: DatepickerPosition = 'bottom-left';

	@ViewChild('nuclosDatechooser') nuclosDatechooser: WebDatechooserComponent;

	constructor(
		private fqnService: FqnService,
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private elementRef: ElementRef
	) {
		super(entityObjectService, eoEventService, elementRef);
	}

	agInit(params: any) {
		super.agInit(params);

		setTimeout(() => {
			this.nuclosDatechooser.focusInput();
			this.nuclosDatechooser.selectInputText();
			if (params.charPress) {
				this.nuclosDatechooser.setInputText(params.charPress);
			}
			this.nuclosDatechooser.toggleDatepicker();

			this.datepickerPosition = this.determineDatepickerPosition();
		});
	}

	getName() {
		if (!this.attributeMeta) {
			return undefined;
		}

		let eo = this.getSubEntityObject();
		if (!eo) {
			return;
		}
		let fieldName = this.fqnService.getShortAttributeName(eo.getEntityClassId(), this.attributeMeta.getAttributeID());
		return fieldName;
	}

	valueChanged() {
		this.updateStringValue();
		this.getGridApi().stopEditing();
		super.reFocusCurrentCell();
	}

	private updateStringValue() {
		let value = this.nuclosDatechooser.getValue();
		let stringValue: string | null = null;

		let m = moment(value);
		if (m.isValid()) {
			stringValue = m.format('YYYY-MM-DD');
		}

		this.setValue(stringValue);
	}

	private determineDatepickerPosition(): DatepickerPosition {
		let subformElement = $(this.elementRef.nativeElement).closest('nuc-web-subform');
		let subformHeight = subformElement.parent().height();
		let subformOffset = subformElement.offset();
		if (subformOffset) {
			let subformOffsetTop = subformOffset.top;
			let dateEditorOffsetTop = $(this.elementRef.nativeElement).offset().top;
			let offset = dateEditorOffsetTop - subformOffsetTop;
			if (offset > subformHeight) {
				return 'top-left';
			}
		}
		return 'bottom-left';
	}

	keydown(event: KeyboardEvent) {
		if (event.code === 'Enter' || event.code === 'Tab' || event.code === 'Escape') {
			this.nuclosDatechooser.commitValue();
		}

		if (event.code === 'Enter') {
			this.reFocusCurrentCell();
		}

		super.keydown(event);
	}
}
