import { Component, Injector, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractLabelComponent } from '../shared/abstract-label-component';

@Component({
	selector: 'nuc-web-label',
	templateUrl: './web-label.component.html',
	styleUrls: ['./web-label.component.css']
})
export class WebLabelComponent extends AbstractLabelComponent<WebLabel> implements OnInit, OnChanges {

	private label: string;

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
		if (this.eo && this.webComponent) {
			this.eo.getAttributeLabel(this.webComponent.name).subscribe(
				label => this.label = label
			);
		}
	}

	ngOnChanges(_changes: SimpleChanges): void {
		if (this.eo && this.webComponent) {
			this.eo.getAttributeLabel(this.webComponent.name).subscribe(
				label => this.label = label
			);
		}
	}

	getText() {
		return this.label;
	}

	getForId() {
		if (this.webComponent.forId) {
			return 'component-' + this.webComponent.forId;
		}

		return undefined;
	}
}
