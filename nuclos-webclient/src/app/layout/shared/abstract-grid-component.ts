import { DoCheck, ElementRef, Injector } from '@angular/core';
import { IEntityObjectDependents } from '@nuclos/nuclos-addon-api';
import { ColDef, GridApi, GridOptions, RowNode } from 'ag-grid';
import { Subscription } from 'rxjs';
import { EntityObjectEventListener } from '../../entity-object-data/shared/entity-object-event-listener';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { AbstractInputComponent } from './abstract-input-component';

/**
 * TODO: Consolidate with GridComponent and EntityObjectGridComponent
 */
export abstract class AbstractGridComponent<T extends WebInputComponent> extends AbstractInputComponent<T> implements DoCheck {

	gridOptions: GridOptions = <GridOptions>{};

	/**
	 * The dependents (Sub-EOs).
	 * "undefined" means "loading".
	 */
	protected dependents?: IEntityObjectDependents;

	private _eo: EntityObject;

	protected elementRef: ElementRef;

	/**
	 * Listens for changes in the Sub-EOs.
	 * Changes might e.g. be made by Layout-Rules. The grid must then be updated.
	 */
	private eoListener: EntityObjectEventListener;

	protected eoChangeListener: EntityObjectEventListener;

	protected dependentSubscription: Subscription;

	constructor(
		injector: Injector,
		i18nService: NuclosI18nService,
		elementRef: ElementRef
	) {
		super(injector);

		this.elementRef = elementRef;

		this.gridOptions.localeTextFunc = (key, defaultValue) => {
			let gridKey = 'grid.' + key;
			return i18nService.getI18nOrUndefined(gridKey) || defaultValue;
		};

		this.eoListener = {
			afterSave: () => this.loadData(),
			beforeCancel: () => this.cancel(),
			afterReload: () => this.init(),
		};
	}

	protected abstract loadData();

	protected abstract init();

	/**
	 * will be called when the ag-grid api is available
	 */
	protected abstract onGridApiReady(api: GridApi);

	protected cancel() {

	}

	isVisible(): boolean {
		return !!this.gridOptions.columnDefs;
	}

	/**
	 * TODO: DoCheck is called very often. Maybe there is a better way to reliably detect changes of the EO.
	 */
	ngDoCheck(): void {
		if (this._eo !== this.eo) {
			// ngDoCheck is called several times when changing eo, once without subEos and then with subEos.
			// One of them with subEos is the one we want
			if (!this.eo.getSubEoInfos()) {
				return;
			}
			// Same issue applies here, only one of nearly identical eos is needed
			if (this._eo && this._eo.getId() === this.eo.getId()) {
				return;
			}
			this.getLogger().debug('EO changed');
			if (this._eo) {
				this._eo.removeListener(this.eoListener);
			}
			this._eo = this.eo;
			this._eo.addListener(this.eoListener);

			this.updateGridStatusOverlay(true);

			this.init();
		}
	}

	protected updateGridData() {
		this.getLogger().debug('Update grid data: %o', this.dependents);
		if (this.gridOptions.api) {
			let rowData;

			if (this.dependents) {
				// this.dependents.current() may return undefined, if it is not yet loaded.
				// In this case, the grid data should not be touched.
				// If we set an empty array here, the grid would show the "no rows" overlay.
				// See NUCLOS-6953.
				rowData = this.dependents.current();
			} else {
				// this.dependents is sometimes undefined, e.g. when multiple parents are selected for a sub-subform.
				// In this case, we must set an empty list to clear the subform.
				rowData = [];
			}

			if (rowData) {
				this.gridOptions.api.setRowData(rowData);
				this.updateRowSelection();
			}
		}
		this.getLogger().debug('Update grid data done');
	}

	/**
	 * Does a hard refresh of the view.
	 * Row DOM elements may be recreated.
	 */
	protected hardRefresh() {
		if (this.gridOptions.api) {

			this.updateRowSelection();
			this.softRefresh();
		}
	}

	protected updateRowSelection(): void {
		if (this.gridOptions.api) {
			this.gridOptions.api.forEachNode((rowNode: RowNode) => {
				rowNode.setSelected(rowNode.data.isSelected(), false);
			});
		}
	}

	/**
	 * Does a soft refresh of the view.
	 * Row DOM elements are not recreated. Only changed, volatile cells are redrawn.
	 */
	protected softRefresh() {
		if (this.gridOptions.api) {
			this.updateGridStatusOverlay();

			this.gridOptions.api.refreshCells(
				{
					force: true
				}
			);
		}
	}

	updateGridStatusOverlay(forceLoading = false) {
		setTimeout(() => {
			if (this.gridOptions.api) {
				if (forceLoading) {
					this.gridOptions.api.showLoadingOverlay();
					return;
				}

				if (this.dependents) {
					if (this.dependents.isLoading()) {
						this.gridOptions.api.showLoadingOverlay();
					} else if (this.dependents.isEmpty()) {
						this.gridOptions.api.showNoRowsOverlay();
					} else {
						this.gridOptions.api.hideOverlay();
					}
				} else {
					this.gridOptions.api.hideOverlay();
				}
			}
		});
	}

	/**
	 * called from the component
	 */
	gridReady() {
		if (this.gridOptions.api) {
			this.onGridApiReady(this.gridOptions.api);
		}

		if (this.dependents && !this.dependents.isEmpty()) {
			this.updateGridData();
		}
		this.hardRefresh();
	}

	/**
	 * Transfers new value to the underlying Sub-EO via the Setter method.
	 */
	protected newValueHandler(params: {
		node: RowNode,
		data: SubEntityObject,
		oldValue: any,
		newValue: any,
		colDef: ColDef,
		api: GridApi,
		context: any
	}): boolean {

		if (!params.colDef.field) {
			this.getLogger().warn('Field missing: %o', params.colDef);
			return false;
		}

		params.node.data.setAttributeObserved(
			params.colDef.field,
			params.newValue
		).subscribe(changed => {
			if (changed) {
				if (this.gridOptions.api) {

					// this leads to loosing the cell focus
					// (needed for row colors - otherwise gridOptions.getRowClass() is not called):
					// this.gridOptions.api.refreshRows([params.node]);

					// Textarea cells might have to be resized
					this.resetRowHeight();

					params.node.setSelected(params.node.data.isSelected());
				}
			}
		});
		return false;
	}

	protected abstract getLogger(): Logger;

	isEmpty() {
		return !this.dependents || this.dependents.isEmpty();
	}

	protected setColumns(colDefs: ColDef[]) {
		let api = this.gridOptions.api;

		// Use the API when available
		if (api) {
			api.setColumnDefs(colDefs);
		} else {
			this.gridOptions.columnDefs = colDefs;
		}
	}

	/**
	 * E.g. textarea heights can change when the content changes.
	 */
	protected resetRowHeight() {
		if (this.gridOptions.api) {
			this.gridOptions.api.resetRowHeights();
		}
	}

	// Called by click-outside directive
	stopEditing() {
		if (this.gridOptions.api) {
			this.gridOptions.api.stopEditing();
		}
	}

	getDependents() {
		return this.dependents;
	}
}
