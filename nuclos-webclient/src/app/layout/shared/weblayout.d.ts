
interface ObjectFactory {
}

interface WebAddon extends WebComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebAdvancedProperty extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name: string;
	value: string;
}

interface WebButton extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	label: string;
	icon: string;
	disableDuringEdit: boolean;
}

interface WebButtonChangeState extends WebButton, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	targetState: string;
}

interface WebButtonDummy extends WebButton, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebButtonExecuteRule extends WebButton, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	rule: string;
}

interface WebButtonGenerateObject extends WebButton, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	objectGenerator: string;
}

interface WebButtonHyperlink extends WebButton, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	hyperlinkField: string;
}

interface WebCalcCell extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	components: WebComponent[];
	left: string;
	top: string;
	width: string;
	height: string;
}

interface WebCell extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	components: WebComponent[];
	colspan: number;
	rowspan: number;
}

interface WebCheckbox extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebCombobox extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebComponent extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	advancedProperties: WebAdvancedProperty[];
	id: string;
	name: string;
	column: number;
	row: number;
	colspan: number;
	rowspan: number;
	fontSize: string;
	textColor: string;
	bold: boolean;
	italic: boolean;
	underline: boolean;
}

interface WebContainer extends WebComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	grid: WebGrid;
	table: WebTable;
	calculated: WebGridCalculated;
	components: WebComponent[];
	opaque: boolean;
	backgroundColor: string;
}

interface WebDatechooser extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebEmail extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebFile extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebGrid extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	rows: WebRow[];
}

interface WebGridCalculated extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	cells: WebCalcCell[];
	minWidth: number;
	minHeight: number;
}

interface WebHtmlEditor extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebHyperlink extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebInputComponent extends WebComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	valuelistProvider: WebValuelistProvider;
	enabled: boolean;
	editable: boolean;
	insertable: boolean;
	columns: string;
	nextfocuscomponent: string;
	nextfocusfield: string;
}

interface WebLabel extends WebComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	forId: string;
}

interface WebLabelStatic extends WebComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	text: string;
}

interface WebLayout extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	grid: WebGrid;
	table: WebTable;
	calculated: WebGridCalculated;
}

interface WebListofvalues extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebMatrix extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	matrixColumns: WebMatrixColumn[];
	matrixPreferencesField: string;
	entityX: string;
	entityY: string;
	entityMatrix: string;
	entityFieldMatrixParent: string;
	entityFieldMatrixXRefField: string;
	entityMatrixValueField: string;
	entityMatrixNumberState: string;
	entityFieldCategorie: string;
	entityFieldX: string;
	entityYParentField: string;
	entityFieldY: string;
	entityXSortingFields: string;
	entityYSortingFields: string;
	entityXHeader: string;
	entityYHeader: string;
	entityMatrixReferenceField: string;
	entityMatrixValueType: string;
	entityXVlpId: string;
	entityXVlpIdfieldname: string;
	entityXVlpFieldname: string;
	entityXVlpReferenceParamName: string;
	cellInputType: string;
}

interface WebMatrixColumn extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name: string;
	label: string;
	controltype: string;
	controltypeclass: string;
	enabled: string;
	notcloneable: string;
	insertable: string;
	rows: string;
	columns: string;
	resourceId: string;
	width: string;
	nextfocuscomponent: string;
	nextfocusfield: string;
}

interface WebOption extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name: string;
	value: string;
	label: string;
}

interface WebOptiongroup extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	options: WebOptions;
}

interface WebOptions extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	option: WebOption[];
	name: string;
	orientation: string;
	default: string;
}

interface WebPanel extends WebContainer, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	title: string;
	borderWidth: string;
	borderColor: string;
}

interface WebPassword extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebPhonenumber extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebRow extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	cells: WebCell[];
}

interface WebSeparator extends WebComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	orientation: string;
}

interface WebSubform extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	subformColumns: WebSubformColumn[];
	controllerType: WebComponent;
	boAttrId: string;
	entity: string;
	autonumberSorting: boolean;
	notCloneable: boolean;
	multiEditable: boolean;
	toolbarOrientation: WebToolbarOrientation;
	opaque: boolean;
	uniqueMastercolumn: string;
	foreignkeyfieldToParent: string;
	parentSubform: string;
	dynamicCellHeightsDefault: boolean;
	ignoreSubLayout: boolean;
	newEnabled: boolean;
	editEnabled: boolean;
	deleteEnabled: boolean;
	cloneEnabled: boolean;
}

interface WebSubformColumn extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	valuelistProvider: WebValuelistProvider;
	advancedProperties: WebAdvancedProperty[];
	name: string;
	label: string;
	controlType: SubformControlType;
	controlTypeClass: string;
	enabled: boolean;
	notCloneable: boolean;
	insertable: boolean;
	rows: string;
	columns: string;
	resourceId: string;
	width: string;
	nextFocusComponent: string;
	nextFocusField: string;
	customUsageSearch: string;
	visible: boolean;
}

interface WebTab extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	title: string;
	content: WebComponent;
}

interface WebTabcontainer extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	tabs: WebTab[];
	selectedIndex: number;
}

interface WebTable extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	rows: WebRow[];
	columnSizes: number[];
	rowSizes: number[];
}

interface WebTextarea extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	rows: number;
}

interface WebTextfield extends WebInputComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
}

interface WebTitledSeparator extends WebComponent, Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	title: string;
}

interface WebValuelistProvider extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	parameter: Parameter[];
	name: string;
	type: string;
	value: string;
	idFieldname: string;
	fieldname: string;
}

interface Parameter extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name: string;
	value: string;
}

interface Serializable {
}

interface Cloneable {
}

interface CopyTo {
}

interface Equals {
}

interface HashCode {
}

interface ToString {
}

type SubformControlType = 'TEXTFIELD' | 'TEXTAREA' | 'EMAIL' | 'HYPERLINK';

type WebButtonAction = 'DUMMY' | 'CHANGE_STATE' | 'EXECUTE_RULE' | 'GENERATE_OBJECT' | 'HYPERLINK';

type WebToolbarOrientation = 'HORIZONTAL' | 'VERTICAL' | 'HIDE';
