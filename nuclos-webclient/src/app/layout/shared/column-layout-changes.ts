import { ColDef, ColumnEvent, ColumnMovedEvent, ColumnResizedEvent, GridOptions } from 'ag-grid';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ArrayUtils } from '../../shared/array-utils';

/**
 * Mixin for handling column-layout changes
 */
export class ColumnLayoutChanges {

	/**
	 * provides column layout changes
	 */
	columnLayoutChanged: Subject<Date> = new Subject<Date>();
	columnLayoutChangedSubscription: Subscription;

	/**
	 * subscribe to column changes like column order, width or sort order
	 * @param {GridOptions} gridOptions
	 * @return {Observable<Date>}
	 */
	onColumnChangesDebounced(gridOptions: GridOptions): Observable<Date> {

		gridOptions.onColumnResized = (event: ColumnResizedEvent) => {
			// update column model
			let colDef = this.getColDef(gridOptions, event);
			if (colDef) {
				colDef.width = event.column.getActualWidth();
			}
			this.columnLayoutChanged.next(new Date());
		};

		gridOptions.onColumnMoved = (event: ColumnMovedEvent) => {
			// update column model
			let colDef = this.getColDef(gridOptions, event);
			if (gridOptions.columnDefs && colDef) {
				let fromIndex = gridOptions.columnDefs.indexOf(colDef);
				ArrayUtils.move(gridOptions.columnDefs, fromIndex, event.toIndex);
			}
			this.columnLayoutChanged.next(new Date());
		};

		gridOptions.onSortChanged = () => {
			this.columnLayoutChanged.next(new Date());
		};

		return this.columnLayoutChanged
			.asObservable().pipe(
				debounceTime(500),
				distinctUntilChanged(),
			);
	}

	getColDef(gridOptions: GridOptions, event: ColumnEvent): ColDef | undefined {
		let columnDefs = gridOptions.columnDefs as ColDef[];
		if (columnDefs && event.column) {
			let colDef: ColDef = columnDefs.filter(col => col.field === event.column.getColDef().field).shift() as ColDef;
			return colDef;
		}
		return undefined;
	}

}
