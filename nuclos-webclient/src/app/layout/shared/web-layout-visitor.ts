export abstract class WebLayoutVisitor {

	public visitLayout(item: WebLayout) {
		this.exploreGrid(item.calculated);
	}

	private exploreGrid(grid: WebGridCalculated) {
		if (grid.cells) {
			grid.cells.forEach(
				cell => this.exploreCell(cell)
			);
		}
	}

	private exploreCell(cell: WebCalcCell) {
		if (cell.components) {
			cell.components.forEach(
				component => this.visitComponent(component)
			);
		}
	}

	private exploreTabcontainer(container: WebTabcontainer) {
		if (container.tabs) {
			container.tabs.forEach(
				tab => this.exploreTab(tab)
			);
		}
	}

	private exploreTab(tab: WebTab) {
		if (tab.content) {
			this.visitComponent(tab.content);
		}
	}

	private exploreContainer(container: WebContainer) {
		if (container.calculated) {
			this.exploreGrid(container.calculated);
		}

		if (container.components) {
			container.components.forEach(
				component => this.visitComponent(component)
			);
		}
	}

	private explorePanel(panel: WebPanel) {
		if (panel.components) {
			panel.components.forEach(
				component => this.visitComponent(component)
			);
		}
		if (panel.calculated) {
			this.exploreGrid(panel.calculated);
		}
	}

	visitComponent(item: WebComponent) {
		let type: string = item['_type'];
		switch (type) {
			case 'WebAddon':
				if (this.visitAddon) {
					this.visitAddon(<WebAddon>item);
				}
				break;
			case 'WebButtonChangeState':
				if (this.visitButtonChangeState) {
					this.visitButtonChangeState(<WebButtonChangeState>item);
				}
				break;
			case 'WebButtonDummy':
				if (this.visitButtonDummy) {
					this.visitButtonDummy(<WebButtonDummy>item);
				}
				break;
			case 'WebButtonExecuteRule':
				if (this.visitButtonExecuteRule) {
					this.visitButtonExecuteRule(<WebButtonExecuteRule>item);
				}
				break;
			case 'WebButtonGenerateObject':
				if (this.visitButtonGenerateObject) {
					this.visitButtonGenerateObject(<WebButtonGenerateObject>item);
				}
				break;
			case 'WebButtonHyperlink':
				if (this.visitButtonHyperlink) {
					this.visitButtonHyperlink(<WebButtonHyperlink>item);
				}
				break;
			case 'WebCheckbox':
				if (this.visitCheckbox) {
					this.visitCheckbox(<WebCheckbox>item);
				}
				break;
			case 'WebCombobox':
				if (this.visitCombobox) {
					this.visitCombobox(<WebCombobox>item);
				}
				break;
			case 'WebContainer':
				if (this.visitContainer) {
					this.visitContainer(<WebContainer>item);
				}
				this.exploreContainer(<WebContainer>item);
				break;
			case 'WebDatechooser':
				if (this.visitDatechooser) {
					this.visitDatechooser(<WebDatechooser>item);
				}
				break;
			case 'WebEmail':
				if (this.visitEmail) {
					this.visitEmail(<WebEmail>item);
				}
				break;
			case 'WebFile':
				if (this.visitFile) {
					this.visitFile(<WebFile>item);
				}
				break;
			case 'WebHtmlEditor':
				if (this.visitHtmlEditor) {
					this.visitHtmlEditor(<WebHtmlEditor>item);
				}
				break;
			case 'WebHyperlink':
				if (this.visitHyperlink) {
					this.visitHyperlink(<WebHyperlink>item);
				}
				break;
			case 'WebLabel':
				if (this.visitLabel) {
					this.visitLabel(<WebLabel>item);
				}
				break;
			case 'WebLabelStatic':
				if (this.visitLabelStatic) {
					this.visitLabelStatic(<WebLabelStatic>item);
				}
				break;
			case 'WebListofvalues':
				if (this.visitListofvalues) {
					this.visitListofvalues(<WebListofvalues>item);
				}
				break;
			case 'WebMatrix':
				if (this.visitMatrix) {
					this.visitMatrix(<WebMatrix>item);
				}
				break;
			case 'WebOptiongroup':
				if (this.visitOptiongroup) {
					this.visitOptiongroup(<WebOptiongroup>item);
				}
				break;
			case 'WebPanel':
				if (this.visitPanel) {
					this.visitPanel(<WebPanel>item);
				}
				this.explorePanel(<WebPanel>item);
				break;
			case 'WebPassword':
				if (this.visitPassword) {
					this.visitPassword(<WebPassword>item);
				}
				break;
			case 'WebPhonenumber':
				if (this.visitPhonenumber) {
					this.visitPhonenumber(<WebPhonenumber>item);
				}
				break;
			case 'WebSeparator':
				if (this.visitSeparator) {
					this.visitSeparator(<WebSeparator>item);
				}
				break;
			case 'WebSubform':
				if (this.visitSubform) {
					this.visitSubform(<WebSubform>item);
				}
				break;
			case 'WebTabcontainer':
				if (this.visitTabcontainer) {
					this.visitTabcontainer(<WebTabcontainer>item);
				}
				this.exploreTabcontainer(<WebTabcontainer>item);
				break;
			case 'WebTextarea':
				if (this.visitTextarea) {
					this.visitTextarea(<WebTextarea>item);
				}
				break;
			case 'WebTextfield':
				if (this.visitTextfield) {
					this.visitTextfield(<WebTextfield>item);
				}
				break;
			case 'WebTitledSeparator':
				if (this.visitTitledSeparator) {
					this.visitTitledSeparator(<WebTitledSeparator>item);
				}
				break;
		}
	}

	visitTabcontainer?(item: WebTabcontainer);

	visitPanel?(item: WebPanel);

	visitContainer?(item: WebContainer);

	visitAddon?(item: WebAddon);

	visitButtonChangeState?(item: WebButtonChangeState);

	visitButtonDummy?(item: WebButtonDummy);

	visitButtonExecuteRule?(item: WebButtonExecuteRule);

	visitButtonGenerateObject?(item: WebButtonGenerateObject);

	visitButtonHyperlink?(item: WebButtonHyperlink);

	visitCheckbox?(item: WebCheckbox);

	visitCombobox?(item: WebCombobox);

	visitDatechooser?(item: WebDatechooser);

	visitEmail?(item: WebEmail);

	visitFile?(item: WebFile);

	visitHtmlEditor?(item: WebHtmlEditor);

	visitHyperlink?(item: WebHyperlink);

	visitLabel?(item: WebLabel);

	visitLabelStatic?(item: WebLabelStatic);

	visitListofvalues?(item: WebListofvalues);

	visitMatrix?(item: WebMatrix);

	visitOptiongroup?(item: WebOptiongroup);

	visitPassword?(item: WebPassword);

	visitPhonenumber?(item: WebPhonenumber);

	visitSeparator?(item: WebSeparator);

	visitSubform?(item: WebSubform);

	visitTextarea?(item: WebTextarea);

	visitTextfield?(item: WebTextfield);

	visitTitledSeparator?(item: WebTitledSeparator);
}
