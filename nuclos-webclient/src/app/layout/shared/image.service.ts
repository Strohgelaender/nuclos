import { Injectable } from '@angular/core';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { FqnService } from '../../shared/fqn.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';

@Injectable()
export class ImageService {

	constructor(
		private config: NuclosConfigService,
		private $log: Logger
	) {
	}

	openImage(imageUrl: string) {
		$['magnificPopup'].open({
			items: {
				src: imageUrl
			},
			type: 'image'
		});
	}

	openGalleryForSubform(subEo: SubEntityObject, imageAttributeId: string) {
		let subEos = subEo.getParent().getDependentsAndLoad(
			subEo.getReferenceAttributeId()
		).current() as SubEntityObject[];

		if (!subEos || subEos.length === 0) {
			throw new Error('Failed to load sub-EOs');
		}

		let {items, index} = this.prepareItems(subEos, imageAttributeId, subEo);

		if (items.length === 0) {
			throw new Error('No image items');
		} else if (index < 0) {
			throw new Error('Selected sub-EO not found in items');
		}

		this.openGallery(items, index);
	}

	private prepareItems(
		subEos: SubEntityObject[],
		attributeId: string,
		subEo: SubEntityObject
	) {
		let items: { src: string }[] = [];
		let index = -1;
		let i = 0;
		for (let eo of subEos) {
			let href = this.getImageHref(eo, attributeId);

			if (href) {
				items.push({src: href});
				if (eo === subEo) {
					index = i;
				}
				i++;
			}
		}
		return {items, index};
	}

	private openGallery(items: { src: string }[], index: number) {
		this.$log.warn('open %o @ %o', items, index);
		$['magnificPopup'].open({
			items: items,
			gallery: {
				enabled: true
			},
			type: 'image'
		}, index);
	}

	getImageHref(eo: EntityObject, attributeId: string): string | undefined {
		let result;

		let attributeName = FqnService.getShortAttributeNameFailsafe(
			eo.getEntityClassId(),
			attributeId
		);

		// status icon
		if (attributeName === 'nuclosStateIcon') {
			let stateIconLink = eo.getStateIconLink();
			if (stateIconLink) {
				result = stateIconLink.href;
			}
		} else {
			let attrImages = eo.getData().attrImages;
			if (attrImages && attrImages.links && attrImages.links[attributeName]) {
				result = attrImages.links[attributeName].href;
			}
		}

		// If it was just uploaded, the uploadToken is only written to the attributes
		if (!result) {
			let attribute = eo.getAttribute(attributeId);
			let uploadToken = attribute && attribute.uploadToken;
			if (uploadToken) {
				result = this.config.getRestHost() + '/boImages/temp/' + uploadToken;
			}
		}

		return result;
	}
}
