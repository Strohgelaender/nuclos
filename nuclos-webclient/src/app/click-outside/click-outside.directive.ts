import { Directive, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { ClickOutsideService } from './click-outside.service';
import { EvaluateClickOutside } from './evaluate-click-outside.model';

/**
 * fires an event if a mouse-click is registered outside the enclosing HTML element
 */
@Directive({
	selector: '[nucClickOutside]'
})
export class ClickOutsideDirective implements OnInit {

	@Output() clickOutside = new EventEmitter();

	/**
	 * evaluate if click was outside
	 * needed e.g. for popups
	 */
	@Input() evaluateClickOutside: EvaluateClickOutside;

	private clickOutsideEvents: Subject<string> = new Subject<string>();

	constructor(
		private clickOutsideService: ClickOutsideService,
		private element: ElementRef
	) {

		this.clickOutsideEvents.asObservable().subscribe(
			(event) => {
				// send click outside event
				this.clickOutside.emit(event);
			}
		);
	}

	ngOnInit(): void {
		this.clickOutsideService.registerCallback(
			this.clickOutsideEvents,
			this.element,
			this.evaluateClickOutside
		);
	}
}
