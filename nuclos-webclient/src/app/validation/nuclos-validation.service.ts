import { Injectable } from '@angular/core';
import { AttributeType, EntityAttrMeta } from '../entity-object-data/shared/bo-view.model';
import { Logger } from '../log/shared/logger';
import { NumberService } from '../shared/number.service';
import { ValidationStatus } from './validation-status.enum';
import {
	BooleanValidator,
	DateValidator,
	DocumentValidator,
	DummyValidator,
	NumberValidator,
	StringValidator,
	TimestampValidator,
	Validator
} from './validators';

@Injectable()
export class NuclosValidationService {

	private validators: Map<AttributeType, Validator<AttributeType>> = new Map();

	// tslint:disable-next-line
	private emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

	constructor(
		private numberService: NumberService,
		private $log: Logger
	) {

		let numberValidator = new NumberValidator(numberService);

		this.validators.set('Integer', numberValidator);
		this.validators.set('Decimal', numberValidator);

		this.validators.set('Boolean', new BooleanValidator());
		this.validators.set('Date', new DateValidator());
		this.validators.set('Timestamp', new TimestampValidator());
		this.validators.set('Document', new DocumentValidator());
		this.validators.set('String', new StringValidator());
	}

	isValidForInput(value: any, inputType: 'number' | 'email'): boolean {
		let result;

		if (!inputType) {
			return true;
		}

		if (value === null || value === undefined) {
			return false;
		}

		if (inputType === 'number') {
			value = this.numberService.parseNumber(value);
			result = !isNaN(value);
		} else if (inputType === 'email') {
			result = this.emailPattern.test(value);
		}

		return result;
	}

	validate(value: any, meta: EntityAttrMeta) {
		if (!meta) {
			return value;
		}

		if (value === null || value === undefined) {
			// The attribute might not be nullable, but clearing the field must be possible
			return value;
		}

		let validator = this.getValidator(meta.getType());

		try {
			value = validator.validate(value, meta);
		} catch (e) {
			this.$log.error('Invalid attribute "' + meta.getName() + '": ' + e);
		}

		return value;
	}

	isRequiredAttributeMissing(meta: EntityAttrMeta, value: any) {
		if (!meta) {
			return undefined;
		}

		if (meta.isNullable()) {
			return false;
		}

		let missing = value === null || value === undefined || value === '';
		if (meta.isNumber()) {
			missing = missing || isNaN(value);
		}

		if (meta.isReference()) {
			missing = missing || (!value || value.id === null || value.id === undefined);
		}

		return missing;
	}

	isConstraintViolated(_meta: EntityAttrMeta, _value: any) {
		// TODO
		return false;
	}

	private getValidator(type: AttributeType) {
		let validator = this.validators.get(type);

		if (!validator) {
			this.$log.warn('No validator for attribute type %o', type);
			return new DummyValidator();
		}

		return validator;
	}

	getValidationStatus(attributeMeta: EntityAttrMeta, value: any): ValidationStatus {
		if (this.isRequiredAttributeMissing(attributeMeta, value)) {
			return ValidationStatus.MISSING;
		} else if (this.isConstraintViolated(attributeMeta, value)) {
			return ValidationStatus.INVALID;
		} else {
			return ValidationStatus.VALID;
		}
	}
}
