export enum ValidationStatus {
	VALIDATING,	// Validation in progress
	VALID,
	MISSING,	// Violates Not-Null constraint
	INVALID,	// Violates any other constraints
}
