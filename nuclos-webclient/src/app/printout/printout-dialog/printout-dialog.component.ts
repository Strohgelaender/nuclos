import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AutoComplete } from 'primeng/primeng';
import { AuthenticationService } from '../../authentication';
import { LovEntry } from '../../entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { LovDataService } from '../../entity-object-data/shared/lov-data.service';
import { AUTOCOMPLETE_PANEL_SELECTOR } from '../../layout/web-subform/cell-editors/abstract-lov-editor.component';
import { BusyService } from '../../shared/busy.service';
import { Printout, PrintoutOutputFormatParameter } from '../shared/printout.model';
import { PrintoutService } from '../shared/printout.service';

@Component({
	selector: 'nuc-printout-dialog',
	templateUrl: './printout-dialog.component.html',
	styleUrls: ['./printout-dialog.component.css']
})
export class PrintoutDialogComponent implements OnInit {

	// TODO: Injecting this service via constructor does not work
	private printoutService: PrintoutService;

	@Input() private eo: EntityObject;

	printoutExecuted = false;

	@ViewChild('autoComplete') autoComplete: AutoComplete;

	printouts: Printout[];
	lovEntries: LovEntry[];

	constructor(
		private activeModal: NgbActiveModal,
		private lovDataService: LovDataService,
		protected elementRef: ElementRef,
		protected busyService: BusyService,
		private authenticationService: AuthenticationService
	) {
	}

	ngOnInit() {
		this.printoutService.getPrintoutData(this.eo).subscribe(printouts => {
			this.printouts = printouts;
		});
	}

	private autocompletePanel() {
		return $(this.elementRef.nativeElement.querySelector(AUTOCOMPLETE_PANEL_SELECTOR));
	}

	private isAutocompletePanelVisible() {
		return this.autocompletePanel().is(':visible');
	}

	/**
	 * open autocomplete panel if not already
	 */
	openLovPanel(param: PrintoutOutputFormatParameter) {
		if (!this.isAutocompletePanelVisible()) {
			this.autoComplete.focus = true;
			this.autocompletePanel().show();
			this.autocomplete(param, '');
		} else {
			this.autocompletePanel().hide();
		}
	}

	autocomplete(param: PrintoutOutputFormatParameter, search: string) {
		this.lovDataService.deprecatedLoadLovEntriesForVlp(
			{reffield: '', vlp: param.vlp},
			null,
			search,
			this.authenticationService.getCurrentMandatorId()
		).subscribe(
			lovEntries => {
				this.lovEntries = lovEntries;
			}
		);

	}

	/**
	 * execute button will be enabled when at least 1 printout is selected
	 * and all required fields are filled
	 */
	executeButtonEnabled(printoutsData: Printout[]) {
		let nrOfSelectedPrintouts = 0;
		let requiredFieldsFilled = true;
		if (printoutsData === undefined) {
			return false;
		}
		for (let item of printoutsData) {
			if (item.outputFormats) {
				for (let j = item.outputFormats.length - 1; j >= 0; j--) {
					let outputFormat = item.outputFormats[j];
					if (outputFormat.selected) {
						nrOfSelectedPrintouts++;
						if (outputFormat.parameters) {
							for (let param of outputFormat.parameters) {
								if (!param.nullable && !param.value) {
									requiredFieldsFilled = false;
								}
							}
						}
					}
				}
			}
		}
		return nrOfSelectedPrintouts !== 0 && requiredFieldsFilled;
	}

	executePrintout() {
		this.busyService.busy(
			this.printoutService.executePrintout(this.eo, this.printouts)
		).subscribe(printouts => {
				if (printouts !== undefined) {
					this.printoutExecuted = true;

					for (let printout of printouts) {
						for (let outputFormat of printout.outputFormats) {
							for (let _printout of this.printouts) {
								if (_printout.printoutId === printout.printoutId) {
									for (let _outputFormat of _printout.outputFormats) {
										if (_outputFormat.outputFormatId === outputFormat.outputFormatId) {
											_outputFormat.links = outputFormat.links;
											_outputFormat.fileName = outputFormat.fileName;
										}
									}
								}
							}
						}
					}
				}
			}
		);
	}


	ok() {
		this.activeModal.close();
	}

	cancel() {
		this.activeModal.dismiss();
	}

}
