import { Link } from '../../shared/link.model';

export interface VlpParams {
	fieldname: string;
	'id-fieldname': string;
	valuelistProvider: string;
}
export interface VLP {
	type: string;
	value: string;
	params: VlpParams;
}
export interface PrintoutOutputFormatParameter {
	parameter: string;
	name: string;
	value?: any;
	type: string;
	nullable: boolean;
	vlp: VLP;
}

export interface PrintoutOutputFormat {
	outputFormatId: string;
	name: string;
	parameters: PrintoutOutputFormatParameter[];
	selected?: boolean;
	links?: {
		file: Link
	};
	fileName?: string;
}

export interface Printout {
	printoutId: string;
	name: string;
	outputFormats: PrintoutOutputFormat[];
}
