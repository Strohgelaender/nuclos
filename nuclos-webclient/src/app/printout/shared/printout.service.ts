import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from as observableFrom, Observable, of as observableOf } from 'rxjs';

import { map } from 'rxjs/operators';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { PrintoutDialogComponent } from '../printout-dialog/printout-dialog.component';
import { Printout } from './printout.model';

@Injectable()
export class PrintoutService {

	constructor(
		private modalService: NgbModal,
		private http: NuclosHttpService
	) {
	}

	/**
	 * Displays printout modal dialog.
	 *
	 */
	openPrintoutDialog(eo: EntityObject): Observable<any> {
		let ngbModalRef = this.modalService.open(
			PrintoutDialogComponent
		);

		// TODO: Injecting this service via constructor does not work
		ngbModalRef.componentInstance.printoutService = this;
		ngbModalRef.componentInstance.eo = eo;

		return observableFrom(ngbModalRef.result);
	}

	getPrintoutData(eo: EntityObject): Observable<any | undefined> {
		let printoutsLink = eo.getPrintoutURL();
		if (printoutsLink !== undefined) {
			return this.http
				.get(printoutsLink).pipe(
				map(response => response.json()));
		}
		return observableOf(undefined);
	}

	executePrintout(
		eo: EntityObject,
		printoutsData: Printout[]
	): Observable<any | undefined> {
		// submit selected outputFormat to the server - remove not selected
		let postData = JSON.parse(JSON.stringify(printoutsData));
		for (let item of postData) {
			for (let j = item.outputFormats.length - 1; j >= 0; j--) {
				let outputFormat = item.outputFormats[j];

				// TODO: ui-select component in printout-dialog.html stores the value in parameter.value.name.name
				// maybe there is a more elegant way to configure where to store the model value
				if (outputFormat.parameters) {
					for (let parameter of outputFormat.parameters) {
						if (parameter.value && parameter.value.name && parameter.value.name.name) {
							parameter.value = parameter.value.name.name;
						}
					}
				}

				if (!outputFormat.selected) {
					item.outputFormats.splice(j, 1);
				}
			}

		}

		let printoutsLink = eo.getPrintoutURL();
		if (printoutsLink) {
			return this.http
				.post(printoutsLink, postData).pipe(
				map(response => response.json()));
		}

		return observableOf(undefined);
	}
}
