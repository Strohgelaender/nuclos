import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { EntityObjectEventListener } from '../entity-object-data/shared/entity-object-event-listener';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { PrintoutService } from './shared/printout.service';

@Component({
	selector: 'nuc-printout',
	templateUrl: './printout.component.html',
	styleUrls: ['./printout.component.css']
})
export class PrintoutComponent implements OnInit, OnChanges {

	@Input() eo: EntityObject;

	saveInProgress: boolean;

	private eoListener: EntityObjectEventListener = {
		isSaving: (entityObject: EntityObject, inProgress: boolean) => this.saveInProgress = inProgress
	};

	constructor(private printoutService: PrintoutService) {
	}

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				let previousEo: EntityObject = eoChange.previousValue;
				previousEo.removeListener(this.eoListener);
			}
			this.eo.addListener(this.eoListener);
		}
	}

	/**
	 * open printout dialog
	 */
	openPrintoutDialog() {
		this.printoutService.openPrintoutDialog(this.eo).subscribe();
	}

}
