import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Logger } from '../../log/shared/logger';
import { NuclosStateService } from '../shared/nuclos-state.service';
import { OldStateInfo, StateInfo } from '../shared/state';

@Component({
	selector: 'nuc-confirm-state-change',
	templateUrl: './confirm-state-change.component.html',
	styleUrls: ['./confirm-state-change.component.css']
})
export class ConfirmStateChangeComponent implements OnInit {
	@Input() targetState: StateInfo;
	@Input() currentStateId: string;
	@Input() targetStateId: string;

	targetStateInfo: OldStateInfo;
	currentStateInfo: OldStateInfo;

	// TODO: Injecting this service via constructor does not work
	stateService: NuclosStateService;

	constructor(
		private $log: Logger,
		private activeModal: NgbActiveModal
	) {
		this.$log.debug('Init: %o', this);
		this.$log.debug('active modal: %o', this.activeModal);
	}

	ngOnInit() {
		this.stateService.fetchInfo(this.currentStateId).subscribe(info => {
			this.$log.debug('State info: %o', info);
			this.currentStateInfo = info;
		});
		this.stateService.fetchInfo(this.targetStateId).subscribe(info => {
			this.$log.debug('State info: %o', info);
			this.targetStateInfo = info;
		});
	}

	ok() {
		this.activeModal.close();
	}

	cancel() {
		this.activeModal.dismiss();
	}

	/**
	 * Returns a gradient background style if current state and target state both have a color.
	 *
	 * TODO: IE support:
	 * "background: -webkit-gradient(linear, left top, left bottom,
	 *            from(" + $scope.dialog.currentStateInfo.color + "),
	 *            to(" + $scope.dialog.newStateInfo.color + "));" +
	 * "background-image: linear-gradient(to right, " + $scope.dialog.currentStateInfo.color + " 0%,
	 * "        + $scope.dialog.newStateInfo.color + " 100%);"
	 *
	 * @returns {any}
	 */
	getGradientStyle(): any {
		let color1 = this.currentStateInfo && this.currentStateInfo.color;
		let color2 = this.targetStateInfo && this.targetStateInfo.color;

		if (!color1 || !color2) {
			return {'display': 'none'};
		}

		return {
			'background': 'linear-gradient(to right, ' + color1 + ', ' + color2 + ')'
		};
	}
}
