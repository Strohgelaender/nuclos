/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfirmStateChangeComponent } from './confirm-state-change.component';

xdescribe('ConfirmStateChangeComponent', () => {
	let component: ConfirmStateChangeComponent;
	let fixture: ComponentFixture<ConfirmStateChangeComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ConfirmStateChangeComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ConfirmStateChangeComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
