import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { I18nModule } from '../i18n/i18n.module';
import { ConfirmStateChangeComponent } from './confirm-state-change/confirm-state-change.component';
import { CurrentStateComponent } from './current-state/current-state.component';
import { NextStateComponent } from './next-state/next-state.component';
import { NuclosStateService } from './shared/nuclos-state.service';
import { StateComponent } from './state.component';

@NgModule({
	imports: [
		CommonModule,
		I18nModule,
	],
	declarations: [
		StateComponent,
		ConfirmStateChangeComponent,
		CurrentStateComponent,
		NextStateComponent
	],
	providers: [
		NuclosStateService
	],
	exports: [
		StateComponent
	],
	entryComponents: [
		ConfirmStateChangeComponent
	]
})
export class StateModule {
}
