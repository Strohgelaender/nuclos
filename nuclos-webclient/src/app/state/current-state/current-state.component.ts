import { Component, Input, OnInit } from '@angular/core';
import { StateInfo } from '../shared/state';

@Component({
	selector: 'nuc-current-state',
	templateUrl: './current-state.component.html',
	styleUrls: ['./current-state.component.css']
})
export class CurrentStateComponent implements OnInit {
	@Input() state: StateInfo;

	constructor() {
	}

	ngOnInit() {
	}

}
