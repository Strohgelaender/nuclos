import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from as observableFrom, Observable, of as observableOf } from 'rxjs';

import { map, tap } from 'rxjs/operators';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { ConfirmStateChangeComponent } from '../confirm-state-change/confirm-state-change.component';
import { OldStateInfo, State, StateInfo } from './state';

@Injectable()
export class NuclosStateService {

	constructor(
		private http: NuclosHttpService,
		private config: NuclosConfigService,
		private modalService: NgbModal
	) {
	}

	/**
	 * Fetches informations (description, color, ...) about the state with the given ID.
	 */
	fetchInfo(stateId: string): Observable<OldStateInfo> {
		return this.http
			.get(this.config.getRestHost() + '/data/statusinfo/' + stateId)
			.pipe(
				map(response => response.json()),
				tap((info: OldStateInfo) => {
					if (info.buttonIcon) {
						info.buttonIconResourceURL = this.config.getResourceURL(info.buttonIcon);
					}
				}),
			);
	}

	/**
	 * Sets the given state on the current EO and saves it.
	 *
	 * @param eo
	 * @param state
	 * @returns Observable<EntityObject>
	 */
	changeState(eo: EntityObject, state: State, body: any): Observable<any> {
		eo.saving(true);
		return this.http.put(
			this.config.getRestHost()
			+ '/boStateChanges/' + eo.getEntityClassId()
			+ '/' + eo.getId()
			+ '/' + state.nuclosStateId,
			body
		);
	}

	/**
	 * Displays a confirmation dialog for changing to the given state.
	 * Does not actually change the state.
	 *
	 * TODO: Show a proper modal dialog instead of a simple alert
	 *
	 * @param eo
	 * @param state
	 */
	confirmStateChange(eo: EntityObject, state: StateInfo): Observable<any> {
		if (state.nonstop) {
			return observableOf(eo);
		}

		let currentState = eo.getState();
		let stateId = currentState && currentState.nuclosStateId;

		let ngbModalRef = this.modalService.open(
			ConfirmStateChangeComponent
		);

		// TODO: Injecting this service via constructor does not work
		ngbModalRef.componentInstance.stateService = this;

		ngbModalRef.componentInstance.currentStateId = stateId;
		ngbModalRef.componentInstance.targetStateId = state.nuclosStateId;

		return observableFrom(ngbModalRef.result);
	}
}
