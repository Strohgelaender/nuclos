import { Component, OnInit } from '@angular/core';
import { IPreference } from '@nuclos/nuclos-addon-api';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { Logger } from '../log/shared/logger';
import { DialogService } from '../popup/dialog/dialog.service';
import { Preference } from '../preferences/preferences.model';
import { DashboardPreferenceContent } from './shared/dashboard-preference-content';
import { DashboardService } from './shared/dashboard.service';

@Component({
	selector: 'nuc-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	dashboard: Preference<DashboardPreferenceContent>;
	dashboards: Preference<DashboardPreferenceContent>[];

	configMode = false;
	itemsChanged = new Date();

	constructor(
		private $log: Logger,
		private dashboardService: DashboardService,
		private dialogService: DialogService,
		private i18n: NuclosI18nService,
	) {
	}

	ngOnInit() {
		this.dashboardService.getDashboardPrefs().subscribe(
			dashboards => this.setDashboards(dashboards)
		);

		this.dashboardService.resetNotificationsSince();
	}

	/**
	 * TODO: Is called too often, needs debouncing
	 */
	saveDashboard() {
		this.itemsChanged = new Date();
		this.dashboardService.saveDashboard(this.dashboard).subscribe(
			(dashboard: Preference<DashboardPreferenceContent>) => {
				this.$log.info('Dashboard saved: %o', dashboard);

				// TODO: Completely replace the old dashboard object
				this.dashboard.customized = dashboard.customized;
			}
		);
	}

	private setDashboards(dashboards: Preference<DashboardPreferenceContent>[]) {
		this.dashboards = dashboards;

		let selected = this.dashboards.find(pref => pref.selected as boolean);
		if (selected) {
			this.dashboard = selected;
		} else if (this.dashboards.length > 0) {
			this.dashboard = this.dashboards[0];
		}
	}

	createNew() {
		let newDashboard = this.dashboardService.createNew();
		this.dashboards.push(newDashboard);
		this.configMode = true;
		this.selectDashboard(newDashboard);
	}

	selectDashboard(dashboard) {
		this.dashboard = dashboard;
		this.dashboardService.selectDashboard(this.dashboard).subscribe();
	}

	toggleConfig() {
		this.configMode = !this.configMode;
	}

	deleteDashboard() {
		this.dialogService.confirm({
			title: this.i18n.getI18n('webclient.dashboard.delete'),
			message:  this.i18n.getI18n('webclient.dashboard.delete.confirm', this.dashboard.name)
		}).then(() => {
			this.dashboardService.deleteDashboard(this.dashboard).subscribe(() => {
				let index = this.dashboards.indexOf(this.dashboard);
				this.dashboards.splice(index, 1);
				if (index > 0) {
					index--;
				}
				let nextDashboard = this.dashboards[index];
				if (nextDashboard) {
					this.selectDashboard(nextDashboard);
				}
			});
		});
	}

	resetDashboard() {
		this.dashboardService.resetDashboard(this.dashboard).subscribe(
			uncustomizedDashboard => {
				this.replaceDashboard(uncustomizedDashboard);
			}
		);
	}

	replaceDashboard(dashboard: IPreference<DashboardPreferenceContent>) {
		let index = this.dashboards.findIndex(pref => pref.prefId === dashboard.prefId);
		if (index >= 0) {
			this.dashboards[index] = dashboard;
		}
		if (this.dashboard.prefId === dashboard.prefId) {
			this.dashboard = dashboard;
		}
	}
}
