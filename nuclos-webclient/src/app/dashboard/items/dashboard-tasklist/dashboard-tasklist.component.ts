import { Component, Input, OnInit } from '@angular/core';
import { GridOptions, RowNode } from 'ag-grid';
import { EMPTY, Subscription } from 'rxjs';

import { catchError, tap } from 'rxjs/operators';
import { BoViewModel, EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectNavigationService } from '../../../entity-object-data/shared/entity-object-navigation.service';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { MetaService } from '../../../entity-object-data/shared/meta.service';
import { EntityObjectGridColumn } from '../../../entity-object/entity-object-grid/entity-object-grid-column';
import { EntityObjectGridService } from '../../../entity-object/entity-object-grid/entity-object-grid.service';
import { SideviewmenuService } from '../../../entity-object/shared/sideviewmenu.service';
import { Logger } from '../../../log/shared/logger';
import {
	ColumnAttribute,
	Preference,
	PreferenceType,
	SideviewmenuPreferenceContent
} from '../../../preferences/preferences.model';
import { PreferencesService } from '../../../preferences/preferences.service';
import { TaskService } from '../../../task/shared/task.service';
import { DashboardTasklistItem } from '../../preference-items/dashboard-tasklist-item';
import { TaskListPreferenceContent } from '../../shared/task-list-preference-content';

@Component({
	selector: 'nuc-dashboard-tasklist',
	templateUrl: './dashboard-tasklist.component.html',
	styleUrls: ['./dashboard-tasklist.component.css']
})
export class DashboardTasklistComponent implements OnInit {
	@Input() item: DashboardTasklistItem;

	bos: EntityObject[] = [];
	visible = false;

	private subscriptions: Subscription[] = [];

	gridOptions: GridOptions = <GridOptions>{};
	gridColumns: EntityObjectGridColumn[] = [];

	private taskListTablePreference?: Preference<any>;
	private taskListMeta;

	// The full entity meta (including attributes) for the current task list
	// TODO: It is very ugly that we must fetch 2 meta definitions here.
	private taskListEntityMeta: EntityMeta;

	constructor(
		private taskService: TaskService,
		private $log: Logger,
		private eoGridService: EntityObjectGridService,
		private eoNavigationService: EntityObjectNavigationService,
		private preferenceService: PreferencesService,
		private sideviewmenuService: SideviewmenuService,
		private metaService: MetaService,
	) {
	}

	ngOnInit() {
		// TODO: Auto-refresh, even in background (via service)
		this.taskService.getTaskData(this.item.taskList).pipe(
			catchError(e => {
				this.$log.warn(e);

				// Prevents redirect to error page
				return EMPTY;
			}))
			.subscribe((result: BoViewModel) => {
				this.bos = result.bos;
				this.visible = true;
			});

		this.taskService.getTaskListDefinition(this.item.taskList.entityClassId).pipe(
			tap(taskListMeta => this.metaService.getEntityMeta(taskListMeta.getEntityClassId()).subscribe(
				// Get a full entity meta from the meta service
				entityMeta => this.taskListEntityMeta = entityMeta
			)),
			catchError(e => {
				this.$log.debug(e);
				// Does not work, if we have a search filter based task list here.
				// In this case try to load a normal entity meta instead.
				return this.metaService.getEntityMeta(this.item.taskList.entityClassId).pipe(tap(
					entityMeta => this.taskListEntityMeta = entityMeta
				));
			}),
		).subscribe(
			taskListMeta => {
				this.taskListMeta = taskListMeta;
				this.loadPreference();
			}
		);
	}

	private loadPreference() {
		this.preferenceService.getPreferences({
			type: [PreferenceType.tasklistTable.name]
		}).subscribe((prefs: Array<Preference<TaskListPreferenceContent>>) => {
			if (prefs) {
				this.taskListTablePreference = prefs.find(
					pref => pref.content.taskListId === this.taskListMeta.getTaskMetaId()
				);

				if (this.taskListTablePreference) {
					this.sortAndSetColumns(
						(this.taskListTablePreference as Preference<SideviewmenuPreferenceContent>).content.columns
					);
				} else {
					// No task list table preference found -> try to load normal sidebar column preference.
					// Sidebar prefs are all we have for search filter based task lists.
					this.sideviewmenuService.loadSideviewmenuPreference(
						this.taskListMeta.getEntityClassId()
					).subscribe(pref => {
						if (!pref) {
							// Creating default table preference if nothing else is available
							pref = this.sideviewmenuService.emptySideviewmenuPreference(this.taskListEntityMeta, true);
						}
						this.sortAndSetColumns(
							(pref as Preference<SideviewmenuPreferenceContent>).content.columns
						);
					});
				}
			}
		});
	}
	private sortAndSetColumns(columns: ColumnAttribute[]) {
		// TODO: Refactor - do not use SideviewmenuService here
		let sortedColumns = this.sideviewmenuService.sortColumns(columns);
		this.setColumns(sortedColumns);
	}

	private setColumns(columns: ColumnAttribute[]) {
		if (!this.taskListEntityMeta) {
			this.$log.warn('No task list entity meta');
			return;
		}

		let newColumns = columns.map(col => {
			let attributeMeta;

			if (col.boAttrId) {
				attributeMeta = this.taskListEntityMeta.getAttributeMetaByFqn(col.boAttrId);
			} else if (col.name) {
				// Try to get the attribute meta by name as a fallback
				attributeMeta = this.taskListEntityMeta.getAttributeMeta(col.name);
			} else {
				this.$log.warn('Could not find attribute meta for column %o', col);
				return {};
			}

			let column = this.eoGridService.createColumnDefinition(
				attributeMeta!
			);

			column.width = col.width;

			return column;
		}).filter(col => col.colId);

		if (newColumns.length > 0) {
			this.gridColumns = newColumns;
		}
	}

	gridReady() {
		this.gridOptions.api!.setRowData(this.bos);
	}

	rowClicked(params: { node: RowNode }) {
		let eo: EntityObject = params.node.data;

		let entityClassId = eo.getEntityClassId();

		this.eoNavigationService.navigateToEoById(
			entityClassId,
			eo.getId()!,
			this.item.taskList.searchfilter
		);
	}
}
