import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardService } from '../shared/dashboard.service';

import { DashboardConfigComponent } from './dashboard-config.component';

xdescribe('DashboardConfigComponent', () => {
	let component: DashboardConfigComponent;
	let fixture: ComponentFixture<DashboardConfigComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DashboardConfigComponent],
			providers: [DashboardService]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DashboardConfigComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
