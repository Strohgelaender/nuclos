import { PreferenceContent } from '../../preferences/preferences.model';

export class TaskListPreferenceContent extends PreferenceContent {
	taskListId: string;
}
