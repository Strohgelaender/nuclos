import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { GridsterConfig } from 'angular-gridster2';
import { Logger } from '../../log/shared/logger';
import { Preference } from '../../preferences/preferences.model';
import { DashboardPreferenceContent } from '../shared/dashboard-preference-content';
import { DashboardGridItemWrapper } from './dashboard-grid-item-wrapper';

@Component({
	selector: 'nuc-dashboard-grid',
	templateUrl: './dashboard-grid.component.html',
	styleUrls: ['./dashboard-grid.component.css']
})
export class DashboardGridComponent implements OnInit, OnChanges {
	options: GridsterConfig;

	@Input() dashboard: Preference<DashboardPreferenceContent>;
	@Input() configMode: boolean;

	@Output() configurationChange = new EventEmitter();

	items?: DashboardGridItemWrapper[];

	constructor(
		private $log: Logger
	) {
	}

	ngOnInit() {
		this.options = {
			minRows: 2,
			minCols: 2,
			itemChangeCallback: (item, itemComponent) => {
				this.$log.debug('itemChanged', item, itemComponent);
				this.configurationChange.emit();
			},
		};
	}

	removeItem(item: DashboardGridItemWrapper) {
		let index = this.dashboard.content.items.indexOf(item.wrappedItem);

		if (index < 0) {
			this.$log.warn('Could not find item %o', item);
			return;
		}

		this.dashboard.content.items.splice(index, 1);
		this.configurationChange.emit(this.dashboard);
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.items = undefined;
	}

	getItems() {
		if (this.items === undefined || this.items.length !== this.dashboard.content.items.length) {
			this.items = this.dashboard.content.items.map(
				item => new DashboardGridItemWrapper(item, this.configMode)
			);
		}
		return this.items;
	}
}
