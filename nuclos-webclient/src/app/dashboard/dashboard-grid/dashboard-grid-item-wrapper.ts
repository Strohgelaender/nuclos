import { DashboardGridItem, DashboardGridItemType } from '../shared/dashboard-grid-item';

/**
 * Wraps an actual DashboardGridItem.
 *
 * Adds some additional properties which are only relevant for the grid component,
 * but should not be persistet es preference content.
 */
export class DashboardGridItemWrapper implements DashboardGridItem {
	constructor(
		public wrappedItem: DashboardGridItem,
		private configMode: boolean
	) {
	}

	get cols(): number | undefined {
		return this.wrappedItem.cols;
	};
	set cols(value) {
		this.wrappedItem.cols = value;
	}

	get rows(): number | undefined {
		return this.wrappedItem.rows;
	};
	set rows(value) {
		this.wrappedItem.rows = value;
	}

	get type(): DashboardGridItemType {
		return this.wrappedItem.type;
	}

	get x(): number {
		return this.wrappedItem.x;
	};
	set x(value) {
		this.wrappedItem.x = value;
	}

	get y(): number {
		return this.wrappedItem.y;
	}
	set y(value) {
		this.wrappedItem.y = value;
	}

	get dragEnabled(): boolean {
		return this.configMode;
	}

	get resizeEnabled(): boolean {
		return this.configMode;
	}
}
