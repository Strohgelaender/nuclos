import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from as observableFrom, Observable } from 'rxjs';

@Injectable()
export class ModalService {

	constructor(
		private modalService: NgbModal
	) {
	}

	open(templateRefOrType: any, context?: any, beforeDismiss?: () => boolean): Observable<any> {
		let ngbModalRef = this.modalService.open(
			templateRefOrType,
			{
				size: 'lg',
				windowClass: 'fullsize-modal-window',
				beforeDismiss: beforeDismiss
			}
		);

		for (let x in context) {
			if (context.hasOwnProperty(x)) {
				ngbModalRef.componentInstance[x] = context[x];
			}
		}

		return observableFrom(ngbModalRef.result);
	}
}
