import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalComponent } from './modal/modal.component';
import { ModalService } from './shared/modal.service';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		ModalComponent
	],
	providers: [
		ModalService,
	],
	exports: [
		ModalComponent
	],
})
export class ModalModule {
}
