import { Component } from '@angular/core';
import { DialogOptions } from '../dialog.model';
import { DialogState } from '../dialog.service';
import { DialogButton } from '@nuclos/nuclos-addon-api';

@Component({
	selector: 'nuc-confirm-modal-component',
	templateUrl: './confirm.component.html'
})
export class ConfirmComponent {

	options: DialogOptions;
	buttonOptions: DialogButton[];

	constructor(private state: DialogState) {
		this.options = state.options;
		this.buttonOptions = state.buttonOptions;
	}

	close() {
		this.state.modalRef.close();
	}

}
