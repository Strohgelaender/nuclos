export class DialogOptions {
	/**
	 * The title of the confirmation modalRef
	 */
	title: string;

	/**
	 * The message in the confirmation modalRef
	 */
	message: string;
}
