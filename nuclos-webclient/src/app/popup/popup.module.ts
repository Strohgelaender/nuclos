import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DialogModule } from './dialog/dialog.module';
import { ModalModule } from './modal/modal.module';

@NgModule({
	imports: [
		CommonModule,

		DialogModule,
		ModalModule
	],
	declarations: []
})
export class PopupModule {
}
