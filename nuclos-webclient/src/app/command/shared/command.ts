import { CommandType } from './command-type.enum';

export interface Command<T extends CommandType> {
	commandType: T;
}

export interface MessageCommand extends Command<'MESSAGEBOX'> {
	title: string;
	message: string;
}

export interface NavigationCommand extends Command<'NAVIGATE'> {
	newTab: boolean;

	id?: number;
	path?: string;

	additionalNavigation?: 'LOGIN' | 'LOGOUT';
}

export interface CloseCommand extends Command<'CLOSE'> {
}
