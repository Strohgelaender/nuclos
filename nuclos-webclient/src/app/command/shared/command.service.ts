import { Injectable } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { DialogService } from '../../popup/dialog/dialog.service';
import { CommandExecutor } from '../command-executor';

@Injectable()
export class CommandService {
	private commandExecutor: CommandExecutor;

	constructor(
		private dialogService: DialogService,
		private $log: Logger
	) {
		this.commandExecutor = new CommandExecutor(this, $log);
	}

	executeCommands(eo: EntityObject) {
		let commands = eo.getCommands();

		if (!commands) {
			return;
		}

		this.$log.debug('Executing %o commands...', commands.length);
		for (let command of commands) {
			this.commandExecutor.execute(command);
		}
	}

	showMessage(title: string, message: string) {
		this.dialogService.alert({
			title: title,
			message: message
		});
	}
}
