import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotLoginDetailsComponent } from './forgot-login-details.component';

xdescribe('ForgotLoginDetailsComponent', () => {
	let component: ForgotLoginDetailsComponent;
	let fixture: ComponentFixture<ForgotLoginDetailsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ForgotLoginDetailsComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ForgotLoginDetailsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
