/**
 * TODO: Move to nuclos-schema, use same definition in the REST service.
 */
export interface UserPassword {
	userName: string;
	oldPassword?: string;
	newPassword?: string;
	confirmNewPassword?: string;
	passwordResetCode?: string;
}
