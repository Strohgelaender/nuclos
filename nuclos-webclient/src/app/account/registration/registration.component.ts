import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DisclaimerService } from '../../disclaimer/shared/disclaimer.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { BusyService } from '../../shared/busy.service';
import { HasMessage } from '../../shared/has-message';
import { AccountData } from '../shared/account-data';
import { NuclosAccountService } from '../shared/nuclos-account.service';

@Component({
	selector: 'nuc-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.css']
})
export class RegistrationComponent extends HasMessage implements OnInit {

	account: AccountData;

	showForm = true;

	private privacyDisclaimer: LegalDisclaimer | undefined;

	constructor(
		private accountService: NuclosAccountService,
		private disclaimerService: DisclaimerService,
		private busyService: BusyService,
		private location: Location,
		i18n: NuclosI18nService
	) {
		super(i18n);

		this.account = {};
	}

	ngOnInit() {
		this.disclaimerService.getPrivacyDisclaimer().subscribe(
			disclaimer => this.privacyDisclaimer = disclaimer
		);
	}

	onSubmit() {
		if (this.showForm && this.hasDisclaimer() && !this.account.privacyconsent) {
			this.setMessage(
				'danger',
				'webclient.account.privacyconsent2',
				'webclient.account.noprivacyconsent'
			);
			return;
		}

		this.busyService.busy(
			this.accountService.create(this.account)
		).subscribe(
				() => {
					this.showSuccessMessage();
					this.showForm = false;
				},
				error => this.showErrorFromResponse('webclient.account.error', error)
			);
	}

	cancel() {
		this.showForm = false;
		this.location.back();
	}

	hasDisclaimer() {
		return this.privacyDisclaimer;
	}

	showDisclaimer() {
		if (this.privacyDisclaimer) {
			this.disclaimerService.showDisclaimer(this.privacyDisclaimer);
		}
	}

	private showSuccessMessage() {
		this.setMessage(
			'success',
			'webclient.account.successful.registered',
			'webclient.account.you.get.an.email.to.activate.your.account'
		);
	}
}
