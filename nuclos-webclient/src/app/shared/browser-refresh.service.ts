import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { EMPTY, Observable } from 'rxjs';
import { Logger } from '../log/shared/logger';
import { Preference } from '../preferences/preferences.model';


/*
 this service will be used to communicate across multiple browser instances on the users machine through localStorage Events

 it is used in these scenarios:
 - reference in LOV is selected via a second tab (main and subform)
 --> localStorage event from the second tab containing the selected reference id

 - referenced entry will be opened for editing in a second tab (main and subform)
 --> localStorage event from the second tab which causes the first tab to reload data (containing the recent changes)

 - a new referencing entry is created in a second tab and then selected in the first tab (main and subform)

 - an existing subform entry will be opened for editing in a second tab
 --> localStorage event from the second tab which causes the first tab to reload data (containing the recent changes)

 */

export const SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM = 'selectEntryInOtherWindowToken';
export const SHOW_CLOSE_ON_SAVE_BUTTON_PARAM = 'showCloseOnSave';
export const SHOW_SELECT_BUTTON_PARAM = 'showSelectButton';
export const EXPAND_SIDEVIEW_PARAM = 'expand';
export const VLP_ID_PARAM = 'vlpId';

export const SHOW_CLOSE_ON_SAVE_BUTTON_LOCAL_STORAGE_KEY_NAME = 'showCloseOnSave';

const LocalStorageKeys = {
	RefreshBrowserWindow: 'refreshbrowserwindow',
	PreferenceChange: 'preferencechange',
	SelectEntryInOtherWindow: 'selectEntryInOtherWindow',
};

export class SelectReferenceInOtherWindowEvent {
	entityClassId: string;
	eoId: number;
	name: string;
	token: number;
}


@Injectable()
export class BrowserRefreshService {

	private static readonly REFRESH_ALL_PREFS = 'refreshAll';

	constructor(private $log: Logger) {
	}

	onEoChanges(): Observable<void> {
		return new Observable<void>(
			observer => {
				window.addEventListener('storage', (ev) => {
					if (ev.key !== LocalStorageKeys.RefreshBrowserWindow) {
						// ignore other keys
						return;
					}
					if (ev.newValue !== null) {
						this.$log.debug('refresh window data');
						observer.next();
					}
				});
			}
		);
	}


	/**
	 * notify other browser-tabs and popups to reload data via storage events
	 */
	refreshOtherBrowserInstances(): void {
		localStorage.setItem(LocalStorageKeys.RefreshBrowserWindow, 'refresh');
		localStorage.removeItem(LocalStorageKeys.RefreshBrowserWindow);
	}


	/**
	 * notify other browser instances when a preference was changed
	 */
	public emitPreferenceChange(preference: Preference<any>): void {
		localStorage.setItem(LocalStorageKeys.PreferenceChange, JSON.stringify(preference));
		localStorage.removeItem(LocalStorageKeys.PreferenceChange);
	}

	public emitPreferencesChange(): void {
		localStorage.setItem(LocalStorageKeys.PreferenceChange, BrowserRefreshService.REFRESH_ALL_PREFS);
		localStorage.removeItem(LocalStorageKeys.PreferenceChange);
	}


	/**
	 * receive preference change events from other browser instances
	 */
	public onPreferenceChange(): Observable<Preference<any>> {
		return new Observable<Preference<any>>(
			observer => {

				let eventListener = (event) => {
					if (event.key !== LocalStorageKeys.PreferenceChange) {
						// ignore other keys
						return;
					}
					if (event.newValue !== null) {

						let localStorageEvent: Preference<any> | undefined =
							event.newValue !== BrowserRefreshService.REFRESH_ALL_PREFS ? JSON.parse(event.newValue) : undefined;

						this.$log.debug('local storage event received:', localStorageEvent);


						// update selected reference in main window
						observer.next(localStorageEvent);
					}
				};
				window.addEventListener('storage', eventListener);
			}
		);
	}

	/**
	 * this function is called on the main window to select a reference in another browser instance
	 */
	private onReferenceSelection(token: number): Observable<SelectReferenceInOtherWindowEvent> {
		return new Observable<SelectReferenceInOtherWindowEvent>(
			observer => {

				// add an eventListener to get selection from other tab
				// let  eventListener = function(ev) {
				let eventListener = (event) => {
					if (event.key !== LocalStorageKeys.SelectEntryInOtherWindow) {
						// ignore other keys
						return;
					}
					if (event.newValue !== null) {

						let searchEvent: SelectReferenceInOtherWindowEvent = JSON.parse(event.newValue);

						this.$log.debug('local storage event received:', searchEvent);

						if (searchEvent.token !== token) {
							return;
						}

						// update selected reference in main window
						observer.next(searchEvent);

						window.removeEventListener('storage', eventListener);
					}
				};
				window.addEventListener('storage', eventListener);
			}
		);
	}


	/**
	 * this function is called in the second window
	 * fires the event listeners in the main window
	 */
	selectEntryInOtherWindow(message: SelectReferenceInOtherWindowEvent): void {
		// send message
		this.$log.debug('send local storage event:', message);
		localStorage.setItem(
			LocalStorageKeys.SelectEntryInOtherWindow,
			JSON.stringify(message)
		);
		localStorage.removeItem(LocalStorageKeys.SelectEntryInOtherWindow);
	}

	openEoInNewTab(
		referencingBoMetaId: string,
		boId: string | number | undefined,
		showSaveButton: boolean,
		showSelectButton: boolean,
		expandSideview: boolean,
		popupParameter: any,
		vlpId: string | undefined,
		vlpParams: URLSearchParams | undefined,
		addSelectionHandler = true
	): Observable<SelectReferenceInOtherWindowEvent> {

		// open new tab to edit/select reference
		if (addSelectionHandler) {
			let token = this.createToken();
			this.openInNewTab(referencingBoMetaId, boId, popupParameter, showSaveButton, showSelectButton, expandSideview,
				vlpId, vlpParams, token);
			return this.onReferenceSelection(token);
		} else {
			this.openInNewTab(referencingBoMetaId, boId, popupParameter, showSaveButton, showSelectButton, expandSideview,
				vlpId, vlpParams, undefined);
			return EMPTY;
		}
	}

	private openInNewTab(
		boMetaId: string,
		boId: string | number | undefined,
		popupparameter: any,
		showSaveButton: boolean,
		showSelectButton: boolean,
		expandSideview: boolean,
		vlpId: string | undefined,
		vlpParams: URLSearchParams | undefined,
		token?: number,
	) {
		let href =
			(expandSideview ? '#/popup/' : '#/view/')
			+ boMetaId
			+ (boId ? '/' + boId : '')
			+ '?'
			+ (showSaveButton ? '&' + SHOW_CLOSE_ON_SAVE_BUTTON_PARAM + '=true' : '')
			+ (showSelectButton ? '&' + SHOW_SELECT_BUTTON_PARAM + '=true' : '')
			+ (expandSideview ? '&' + EXPAND_SIDEVIEW_PARAM + '=true' : '')
			+ (vlpId ? ('&' + VLP_ID_PARAM + '=' + vlpId) : '')
			+ (token ? '&' + SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM + '=' + token : '')
		;

		if (vlpParams) {
			vlpParams.paramsMap.forEach((values: string[], key: string) => {
				if (values) {
					values.forEach((value: string) => {
						href = href + '&' + key + '=' + value;
					});
				} else {
					href = href + '&' + key;
				}
			});
		}

		if (popupparameter) {
			window.open(href, '', popupparameter);
		} else {
			window.open(href);
		}
	}

	private createToken(): number {
		return new Date().getTime();
	}

}
