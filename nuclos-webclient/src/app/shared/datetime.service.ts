import { Injectable } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import Moment = moment.Moment;

@Injectable()
export class DatetimeService {

	constructor(private nuclosI18n: NuclosI18nService) {
	}

	getDatePattern() {
		return this.getLocale().datePattern;
	}

	getJavaDatePattern() {
		return this.getLocale().javaDatePattern;
	}

	formatDate(date) {
		if (!date || date.length < 10) {
			return '';
		}

		return moment(date).format(this.getDatePattern());
	}

	parseDate(dateString: string): Date | undefined {
		let m = moment(dateString);

		if (!m.isValid()) {
			return undefined;
		}

		return m.toDate();
	}

	formatTimestamp(timestamp: Date): string {
		if (timestamp === undefined) {
			return '';
		}
		return moment(timestamp).format(this.getLocale().dateTimePattern);
	}

	/**
	 * NgbDateStruct is used by ng-bootstrap datepicker
	 * @param date
	 * @return {{year: number, month: number, day: number}}
	 */
	buildNgbDateStruct(date: any, format?): NgbDateStruct {
		let dateMoment: Moment;
		if (date instanceof Date) {
			dateMoment = moment(date);
		} else  {
			dateMoment = moment(date, format);
		}
		return {
			year: dateMoment.year(),
			month: dateMoment.month() + 1,
			day: dateMoment.date()
		};
	}

	private getLocale() {
		return this.nuclosI18n.getCurrentLocale();
	}
}
