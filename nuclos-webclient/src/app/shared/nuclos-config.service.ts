import { Injectable, Injector } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { map } from 'rxjs/operators';
import { Logger } from '../log/shared/logger';
import { NuclosHttpService } from './nuclos-http.service';
import { SystemParameters } from './system-parameters';

/**
 * Possible config values, exactly written like in the actual JSON file.
 */
enum Config {
	nuclosURL
}

@Injectable()
export class NuclosConfigService {

	private http: NuclosHttpService;
	private config: any;

	constructor(
		private injector: Injector,
		private $log: Logger
	) {

		// Http must be loaded via injector to prevent dependency cycles
		this.http = this.injector.get(NuclosHttpService);
	}

	/**
	 * Loads the config file.
	 */
	load(): Promise<boolean> {
		this.$log.info('Loading config...');
		return new Observable<boolean>((observer: Observer<boolean>) => {
			this.http.get('assets/config.json').pipe(map(response => response.json())).subscribe(config => {
				this.$log.debug('Config: %o', config);
				this.config = config;
				observer.next(true);
				observer.complete();
			});
		}).toPromise();
	}

	/**
	 * Returns the system parameters, possibly loading them from the server first.
	 */
	getSystemParameters(): Observable<SystemParameters> {
		return this.http.getCachedJSON(
			this.getRestHost() + '/meta/systemparameters',
			json => new SystemParameters(json)
		);
	}

	private getString(config: Config): string {
		let key: string = Config[config];
		let value = this.config[key];

		// this.$log.debug('Config: %o = %o', key, value);

		return value;
	}

	/**
	 * Returns the Nuclos URL, possibly replacing a "<host>" placeholder with the current location host.
	 *
	 * @returns {string}
	 */
	getNuclosURL(): string {
		return this.getString(Config.nuclosURL)
			.replace('<host>', window.location.hostname)
			.replace('<port>', window.location.port);
	}

	getRestHost(): string {
		return this.getNuclosURL() + '/rest';
	}

	getServerLogWebsocketURL(): string {
		return this.getNuclosURL() + '/websocket/serverlog';
	}

	/**
	 * Returns the full URL for the given resource (e.g. a button icon).
	 *
	 * @param resourceId
	 * @returns {string}
	 */
	getResourceURL(resourceId: string) {
		return this.getRestHost() + '/data/resource/' + resourceId;
	}
}
