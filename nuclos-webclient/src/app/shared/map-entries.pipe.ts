import { Pipe, PipeTransform } from '@angular/core';

/**
 * Transforms a map into an array of key-value-entries.
 */
@Pipe({name: 'entries', pure: false})
export class MapEntriesPipe implements PipeTransform {
	transform(map: Map<any, any>): any {
		let result: { key, value }[] = [];
		map.forEach((value, key) => result.push({key: key, value: value}));
		return result;
	}
}
