import { Injectable, Injector } from '@angular/core';
import { ConnectionBackend, Headers, Http, Request, RequestOptions, RequestOptionsArgs, Response } from '@angular/http';
import { UNAUTHORIZED } from 'http-status-codes';
import { Observable, Subject, throwError as observableThrowError } from 'rxjs';

import { catchError, finalize, map } from 'rxjs/operators';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { Logger } from '../log/shared/logger';
import { ObservableUtils } from './observable-utils';

@Injectable()
export class NuclosHttpService extends Http {

	private pendingRequests = new Map<number, string>();
	private requestsPendingSince: Date | undefined;
	private lastRequestID = 0;
	// TODO: Observable for error 401, register to it from authentication service
	private unauthorizedRequests = new Subject();

	constructor(
		protected _backend: ConnectionBackend,
		protected _defaultOptions: RequestOptions,
		protected $log: Logger,
		protected cacheService: NuclosCacheService,
		protected injector: Injector
	) {
		super(_backend, _defaultOptions);
	}

	private setCustomHeaders(
		url: string | Request,
		options?: RequestOptionsArgs,
		additionalHeaders?: Map<string, string>
	): RequestOptionsArgs {

		if (!options) {
			options = new RequestOptions({});
		}

		if (!options.headers) {
			options.headers = new Headers();
		}


		if (typeof url === 'string') {
			options.withCredentials = true;
			this.addHeaders(options.headers, additionalHeaders);
		} else {
			url.withCredentials = true;
			this.addHeaders(url.headers, additionalHeaders);
		}

		return options;
	}

	private addHeaders(headers: Headers, additionalHeaders: Map<string, string> | undefined): void {
		if (additionalHeaders) {
			additionalHeaders.forEach(
				(value, key) => {
					headers.set(key, value);
				}
			);
		}
	}

	request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
		options = this.setCustomHeaders(url, options);

		let urlString: any = url;
		if (url instanceof Request) {
			urlString = url.url;
		}

		const requestID = this.nextRequestID();

		let request = super.request(url, options);

		return ObservableUtils.onSubscribe(
			request,
			() => this.requestStarted(requestID, urlString)
		).pipe(
			catchError(err => {
				this.handleError(err);
				return observableThrowError(err);
			}),
			finalize(
				() => this.requestFinished(requestID)
			),
		);
	}

	private handleError(err) {
		if (err.status === UNAUTHORIZED) {
			this.unauthorizedRequests.next();
		}
	}

	nextRequestID() {
		return ++this.lastRequestID;
	}

	requestStarted(requestID: number, url: string) {
		if (!this.requestsPendingSince) {
			this.requestsPendingSince = new Date();
		}

		this.pendingRequests.set(requestID, url);
	}

	requestFinished(requestID: number) {
		this.pendingRequests.delete(requestID);

		if (!this.hasPendingRequest()) {
			this.requestsPendingSince = undefined;
		}
	}

	/**
	 * Performs a POST request.
	 *
	 * It is assumed we always send JSON data, so the Content-Type header is set to 'application/json'.
	 */
	post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
		options = this.setCustomHeaders(
			url,
			options,
			new Map<string, string>().set('Content-Type', 'application/json')
		);
		return super.post(url, body, options);
	}

	/**
	 * Same as {@link Http#get}, but caches the result.
	 *
	 * If an additional mapper is provided, the JSON is mapped again before caching.
	 */
	getCachedJSON(
		url: string,
		mapper?: (json: any) => any,
		options?: RequestOptionsArgs
	): Observable<any> {
		let cache = this.cacheService.getCache('http.GET');

		let observable = this.get(url, options).pipe(map(response => response.json()));
		if (mapper) {
			observable = observable.pipe(map(mapper));
		}

		return cache.get(
			url,
			observable
		);
	}

	hasPendingRequest() {
		return this.pendingRequests.size > 0;
	}

	/**
	 * How long since there were no pending requests (in ms).
	 */
	getPendingRequestTime() {
		if (!this.requestsPendingSince) {
			return -1;
		}

		return Date.now() - this.requestsPendingSince.getTime();
	}

	getUnauthorizedRequests(): Observable<any> {
		return this.unauthorizedRequests;
	}
}
