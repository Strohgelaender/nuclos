import { Injectable } from '@angular/core';
import { Logger } from '../log/shared/logger';

@Injectable()
export class HyperlinkService {
	constructor(
		private $log: Logger
	) {
	}

	open(url: string, target?: string, features?: string) {
		if (this.isRelative(url)) {
			target = '_self';
		}

		this.openInWindow(url, target, features);
	}

	openInWindow(url: string, target?: string, features?: string) {
		url = this.validateURL(url);

		if (url.startsWith('data:image')) {
			// Workaround for security restrictions in newer browsers, which prevent data URLs from being opened directly:
			// Open a new blank window and write the image to the document
			let windowHandle = window.open('about:blank', target, features);
			let iframe = '<img src="' + url + '"></img>';
			if (windowHandle) {
				windowHandle.document.open();
				windowHandle.document.write(iframe);
				windowHandle.document.close();
			} else {
				this.$log.warn('Could not open new window');
			}
		} else {
			window.open(url, target, features);
		}
	}

	validateURL(url: string): string {
		if (!this.hasProtocol(url) && !this.isRelative(url) && !this.isSpecial(url)) {
			url = 'http://' + url;
		}

		return url;
	}

	private hasProtocol(url: string) {
		return url.indexOf('://') >= 0;
	}

	private isRelative(url: string) {
		return url.startsWith('#')
			|| url.startsWith('/')
			|| url.startsWith('./')
			|| url.startsWith('../');
	}

	private isSpecial(url: string) {
		return url.startsWith('about:')
			|| url.startsWith('data:');
	}
}
