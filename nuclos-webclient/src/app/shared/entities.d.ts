
interface LegalDisclaimer extends Serializable, Cloneable, CopyTo, Equals, HashCode, ToString {
	name: string;
	text: string;
}

interface ObjectFactory {
}

interface Serializable {
}

interface Cloneable {
}

interface CopyTo {
}

interface Equals {
}

interface HashCode {
}

interface ToString {
}
