import { Injectable } from '@angular/core';
import { Response, ResponseContentType } from '@angular/http';
import { DatasourceParams } from '@nuclos/nuclos-addon-api';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NuclosConfigService } from './nuclos-config.service';
import { NuclosHttpService } from './nuclos-http.service';

@Injectable()
export class DatasourceService {

	constructor(private http: NuclosHttpService, private nuclosConfig: NuclosConfigService) {
	}

	executeDatasource(datasourceId: string, datasourceParams: DatasourceParams, maxRowCount?: number): Observable<object[]> {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/data/datasource/' + datasourceId + (maxRowCount ? '/' + maxRowCount : ''),
			{
				responseType: ResponseContentType.Json,
				search: datasourceParams
			}
		).pipe(map(
			(response: Response) => response.json()
		));
	}
}
