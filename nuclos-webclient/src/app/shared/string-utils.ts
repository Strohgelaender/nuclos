export class StringUtils {
	static regexReplaceAll(subject: string, search: string, replace: string): string {
		if (typeof subject !== 'string') {
			return subject;
		}
		let regExp = new RegExp(search, 'g');
		return subject.replace(regExp, replace);
	}

	static replaceAll(subject: string, search: string, replace: string): string {
		let regex = this.escapeRegExp(search);
		return this.regexReplaceAll(subject, regex, replace);
	}

	static escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
	}

	static regexFromSearchString(search: string): RegExp {
		let parts = search.trim().split('*');

		let pattern = '';
		for (let i = 0; i < parts.length; i++) {
			let part = parts[i];
			pattern += StringUtils.escapeRegExp(part);
			if (i < parts.length - 1) {
				pattern += '.*';
			}
		}

		let regex = new RegExp(pattern, 'i');

		return regex;
	}

	static countLines(value: any): number {
		let result = 0;

		if (typeof value === 'string') {
			result = (value.match(/\n/g) || []).length + 1;
		}

		return result;
	}

	static trim(value: string) {
		return value && value.trim();
	}

	static isBlank(value: string) {
		return !StringUtils.trim(value);
	}

	static equalsIgnoreCase(string1: string | undefined, string2: string | undefined) {
		if (string1 !== undefined && string2 !== undefined) {
			return string1.toUpperCase() === string2.toUpperCase();
		}

		return false;
	}
}
