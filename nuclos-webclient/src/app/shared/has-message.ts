import { Response } from '@angular/http';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';

/**
 * Represents components which can contain a user message.
 *
 * TODO: This class can currently only be used via inheritance.
 * With Typescript 2.2 it could be done via mixins.
 *
 */
export abstract class HasMessage {
	messageType: 'success' | 'danger';
	messageTitle;
	messageText;

	constructor(private i18n: NuclosI18nService) {
	}

	setMessage(
		type,
		titleKey: string,
		textKey: string
	) {
		this.messageType = type;
		this.messageTitle = this.i18n.getI18n(titleKey);
		this.messageText = this.i18n.getI18n(textKey);
	}

	clearMessage() {
		this.messageTitle = undefined;
		this.messageText = undefined;
	}

	showErrorFromResponse(
		titleKey: string,
		error: Response
	) {
		let message = '';
		try {
			message = error.json().message;
		} catch (e) {
			// Ignore
		}

		this.setMessage(
			'danger',
			titleKey,
			message || error.text()
		);
	}

	showError(titleKey: string, messageKey: string) {
		this.setMessage('danger', titleKey, messageKey);
	}

	hasMessage() {
		return this.messageType && (this.messageTitle || this.messageText);
	}
}
