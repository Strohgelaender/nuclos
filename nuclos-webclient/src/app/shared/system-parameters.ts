/**
 * Created by alaemmlein on 12/21/16.
 */

/**
 * TODO: Generate automatically from Java sources
 */
export enum SystemParameter {
	ENVIRONMENT_DEVELOPMENT,
	FULLTEXT_SEARCH_ENABLED,
	WEBCLIENT_CSS,
	ANONYMOUS_USER_ACCESS_ENABLED,
	USER_REGISTRATION_ENABLED,
	FORGOT_LOGIN_DETAILS_ENABLED
}

export class SystemParameters {
	private systemparameters: Map<string, any>;

	constructor(systemparameters: Map<string, any>) {
		this.systemparameters = systemparameters;
	}

	/**
	 * Returns the value for the given SystemParameter.
	 *
	 * @param param
	 * @returns {any}
	 */
	get(param: SystemParameter): any {
		let key = SystemParameter[param];
		return this.systemparameters[key];
	}

	/**
	 * Determines if the given SystemParameter is enabled.
	 *
	 * @param param
	 */
	is(param: SystemParameter): boolean {
		let value = this.get(param);

		if (typeof value === 'boolean') {
			return value;
		} else if (typeof value === 'string') {
			value = value.toLowerCase();
			return value === 'true'
				|| value === 'yes'
				|| value === 'y'
				|| value === '1'
				|| value === 'enable'
				|| value === 'enabled';
		}

		return false;
	}
}
