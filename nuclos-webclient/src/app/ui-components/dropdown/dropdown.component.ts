import {
	Component,
	ElementRef,
	EventEmitter,
	forwardRef,
	HostBinding,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import * as _ from 'lodash';
import { AutoComplete } from 'primeng/components/autocomplete/autocomplete';
import { ClickOutsideService } from '../../click-outside/click-outside.service';
import { IdFactoryService } from '../../shared/id-factory.service';
import { StringUtils } from '../../shared/string-utils';

@Component({
	selector: 'nuc-dropdown',
	templateUrl: './dropdown.component.html',
	styleUrls: ['./dropdown.component.scss'],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => DropdownComponent),
			multi: true
		}
	]
})
export class DropdownComponent implements OnInit, ControlValueAccessor, OnChanges {

	@HostBinding('attr.for-id')
	@Input() id;

	@Input() dropdown = false;
	@Input() suggestions: any[] = [];
	@Input() field;
	@Input() placeholder;
	@Input() appendTo = undefined;
	@Input() componentId;
	@Input() multiple = false;
	@Input() optional = false;

	@Output() onSelect = new EventEmitter();
	@Output() completeMethod = new EventEmitter();

	@ViewChild('autoComplete') autoComplete: AutoComplete;

	value: any;
	_onChange: any;
	_onTouched: any;
	disabled = false;

	/**
	 * If the Input "optional" is set, this value will be initialised and added as first, empty option.
	 */
	emptyValue;

	filterString;
	filteredSuggestions: any[] = [];

	constructor(
		private elementRef: ElementRef,
		private clickOutsideService: ClickOutsideService,
		private idFactory: IdFactoryService,
	) {
	}

	ngOnInit() {
		if (!this.id) {
			this.id = this.idFactory.getNextId();
		}
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes['suggestions']) {
			this.updateFilteredSuggestions();
		}
	}

	getId() {
		return this.id;
	}

	private updateFilteredSuggestions() {
		this.filteredSuggestions = this.filterSuggestions(this.suggestions);
		if (this.optional && !this.filterString) {
			this.emptyValue = {};
			if (this.field) {
				this.emptyValue[this.field] = '\u200b';
			} else {
				this.emptyValue = '';
			}
			this.filteredSuggestions.unshift(this.emptyValue);
		}
	}

	private filterSuggestions(suggestions: any[]) {
		if (!this.filterString) {
			return _.clone(suggestions);
		}

		let regex = StringUtils.regexFromSearchString(this.filterString);

		return suggestions.filter(it => {
				if (typeof it === 'string') {
					return it.match(regex);
				} else if (it && this.field) {
					let value = it[this.field];
					return ('' + value).match(regex);
				} else {
					return ('' + it).match(regex);
				}
			}
		);
	}

	writeValue(obj: any): void {
		this.setValue(obj);
	}

	registerOnChange(fn: (_: any) => void): void {
		this._onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this._onTouched = fn;
	}

	setDisabledState(isDisabled: boolean): void {
		this.disabled = isDisabled;
	}

	selectEntry(value) {
		if (value === this.emptyValue) {
			value = undefined;
		}
		this.onSelect.emit(value);
	}

	setValue(value) {
		if (value === this.emptyValue) {
			value = undefined;
		}
		let oldValue = this.value;
		this.value = value;
		if (this.value !== oldValue && this._onChange) {
			this._onChange(value);
		}
		return value;
	}

	onComplete(filter: string) {
		this.filterString = filter;
		this.updateFilteredSuggestions();

		this.completeMethod.emit(filter);
	}

	keyup($event: KeyboardEvent) {
		// The autocomplete component does not trigger a search, if there is no input
		if ($(this.autoComplete.inputEL.nativeElement).val() === '') {
			this.showAllEntries();
		}
	}

	/**
	 * fix for https://github.com/primefaces/primeng/issues/745
	 */
	showAllEntries() {
		if (this.disabled) {
			return;
		}

		this.onComplete('');

		this.showResultPanel();
	}

	private showResultPanel() {
		this.autoComplete.show();
		this.setupPanelAndInput();

		this.clickOutsideService.outsideClick(this.elementRef.nativeElement);
	}

	openLovPanel($event) {
		if ($event) {
			// We handle the dropdown panel. Prevent the event from reaching the autocomplete component:
			let eventObject = $event;
			if (typeof eventObject.originalEvent === 'object') {
				eventObject = eventObject.originalEvent;
			}
			if (typeof eventObject.preventDefault === 'function') {
				eventObject.preventDefault();
			}
			if (typeof eventObject.stopPropagation === 'function') {
				eventObject.stopPropagation();
			}
		}

		if (this.autoComplete.panelVisible) {
			this.autoComplete.hide();
		} else {
			this.showAllEntries();
		}

		// select lov input text
		let textInput = this.autoComplete.inputEL.nativeElement;
		if (textInput.selectionEnd - textInput.selectionStart === 0) {
			textInput.setSelectionRange(0, textInput.value.length);
		}
	}

	private setupPanelAndInput() {
		let element = this.autoComplete.panelEL.nativeElement;
		element.setAttribute('for-id', this.getId());

		let inputEl = this.autoComplete.inputEL.nativeElement;
		let inputWidth = $(inputEl).outerWidth();
		$(element).outerWidth(inputWidth);

		// if (!this.keydownEventRegistered) {
		// 	this.keydownEventRegistered = true;
		//
		// 	inputEl.addEventListener('keydown', event => {
		// 		if (this.handler.keydownHandler) {
		// 			this.handler.keydownHandler(event);
		// 		}
		// 	});
		// }
	}

	dropdownArrowClick($event) {
		if (this.autoComplete.panelVisible) {
			this.autoComplete.hide();
			return;
		}

		this.openLovPanel($event);
		this.autoComplete.handleDropdownClick($event);
	}

	onBlur() {
		if (this._onTouched) {
			this._onTouched();
		}
	}

	clearInput() {
		this.setInput('');
	}

	setInput(input: string) {
		$(this.autoComplete.inputEL.nativeElement).val(input);
	}

	resolveFieldData(option: any, field: any): any {
		return option[field];
	}
}
