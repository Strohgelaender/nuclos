import { Component, OnInit } from '@angular/core';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { SystemParameter } from '../../shared/system-parameters';

@Component({
	selector: 'nuc-css-from-systemparameter',
	templateUrl: './css-from-systemparameter.component.html',
	styleUrls: ['./css-from-systemparameter.component.css']
})
export class CssFromSystemparameterComponent implements OnInit {

	webclientCss: string;

	constructor(
		private config: NuclosConfigService
	) {
	}

	ngOnInit() {
		this.config.getSystemParameters().subscribe(params => {
			this.webclientCss = params.get(SystemParameter.WEBCLIENT_CSS);
			$('<style>')
				.attr('type', 'text/css')
				.text(this.webclientCss)
				.appendTo('head');
		});
	}

}
