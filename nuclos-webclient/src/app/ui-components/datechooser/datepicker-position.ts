export type DatepickerPosition =
	'top' |
	'top-left' |
	'top-right' |
	'bottom' |
	'bottom-left' |
	'bottom-right' |
	'left' |
	'left-top' |
	'left-bottom' |
	'right' |
	'right-top' |
	'right-bottom'
