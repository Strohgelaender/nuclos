import { Injectable } from '@angular/core';
import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';


const I18N_VALUES = {
	en: {
		weekdays: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
		months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
	},
	de: {
		weekdays: ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'],
		months: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
	},
	fr: {
		weekdays: ['Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa', 'Di'],
		months: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Déc'],
	}
};

@Injectable()
export class NuclosDatepickerI18n extends NgbDatepickerI18n {

	constructor(private nuclosI18nService: NuclosI18nService) {
		super();
	}

	getWeekdayShortName(weekday: number): string {
		return I18N_VALUES[this.nuclosI18nService.getCurrentLocale().key].weekdays[weekday - 1];
	}

	getMonthShortName(month: number): string {
		return I18N_VALUES[this.nuclosI18nService.getCurrentLocale().key].months[month - 1];
	}

	getMonthFullName(month: number): string {
		return this.getMonthShortName(month);
	}
}
