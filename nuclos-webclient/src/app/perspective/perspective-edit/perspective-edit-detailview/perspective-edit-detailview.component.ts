import { Component } from '@angular/core';
import { AbstractPerspectiveEditComponent } from '../abstract-perspective-edit.component';

@Component({
	selector: 'nuc-perspective-edit-detailview',
	templateUrl: './perspective-edit-detailview.component.html',
	styleUrls: ['./perspective-edit-detailview.component.css']
})
export class PerspectiveEditDetailviewComponent extends AbstractPerspectiveEditComponent {

}
