import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerspectiveEditDetailviewComponent } from './perspective-edit-detailview.component';

xdescribe('PerspectiveEditDetailviewComponent', () => {
	let component: PerspectiveEditDetailviewComponent;
	let fixture: ComponentFixture<PerspectiveEditDetailviewComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PerspectiveEditDetailviewComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PerspectiveEditDetailviewComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
