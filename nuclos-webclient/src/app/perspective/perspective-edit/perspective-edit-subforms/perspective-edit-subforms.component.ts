import { Component, OnInit } from '@angular/core';
import { AbstractPerspectiveEditComponent } from '../abstract-perspective-edit.component';

@Component({
	selector: 'nuc-perspective-edit-subforms',
	templateUrl: './perspective-edit-subforms.component.html',
	styleUrls: ['./perspective-edit-subforms.component.css']
})
export class PerspectiveEditSubformsComponent extends AbstractPerspectiveEditComponent implements OnInit {

	ngOnInit(): void {
	}

	getPreferenceEntries() {
		return this.model.getSubformPreferencesForSelectedLayout();
	}

	getSelectedColumnPref(subEntityClassId: string) {
		let subformPrefs = this.perspective.subformTablePrefIds;
		return subformPrefs && subformPrefs[subEntityClassId];
	}

	selectSubformColumnPref(key, value) {
		this.perspective.subformTablePrefIds[key] = value;
	}

}
