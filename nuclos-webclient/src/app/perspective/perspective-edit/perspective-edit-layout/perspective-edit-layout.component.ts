import { Component } from '@angular/core';
import { AbstractPerspectiveEditComponent } from '../abstract-perspective-edit.component';
import { PerspectiveService } from '../../shared/perspective.service';

@Component({
	selector: 'nuc-perspective-edit-layout',
	templateUrl: './perspective-edit-layout.component.html',
	styleUrls: ['./perspective-edit-layout.component.css']
})
export class PerspectiveEditLayoutComponent extends AbstractPerspectiveEditComponent {

	constructor(
		private perspectiveService: PerspectiveService
	) {
		super();
	}

	onChange() {
		this.change.next();
		this.perspectiveService.updateSubformPreferencesForSelectedLayout();
	}
}
