import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerspectiveEditLayoutComponent } from './perspective-edit-layout.component';

xdescribe('PerspectiveEditLayoutComponent', () => {
	let component: PerspectiveEditLayoutComponent;
	let fixture: ComponentFixture<PerspectiveEditLayoutComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PerspectiveEditLayoutComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PerspectiveEditLayoutComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
