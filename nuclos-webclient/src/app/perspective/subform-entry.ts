import { Preference, SideviewmenuPreferenceContent } from '../preferences/preferences.model';

export interface SubformEntry {
	subEntityClassId: string;
	subEntityName?: string;
	columnPreferences: Preference<SideviewmenuPreferenceContent>[]
}
