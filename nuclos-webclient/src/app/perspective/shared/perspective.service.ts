import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IPreferenceFilter, UserAction } from '@nuclos/nuclos-addon-api';
import { forkJoin as observableForkJoin, from as observableFrom, Observable, of as observableOf, Subject } from 'rxjs';

import { concat, filter, finalize, map, mergeMap, take, tap } from 'rxjs/operators';
import { AuthenticationService } from '../../authentication';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { MetaService } from '../../entity-object-data/shared/meta.service';
import { SelectableService } from '../../entity-object-data/shared/selectable.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { LayoutService } from '../../layout/shared/layout.service';
import { Logger } from '../../log/shared/logger';
import { DialogService } from '../../popup/dialog/dialog.service';
import {
	Preference,
	PreferenceType,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { ILayoutInfo } from '../layout-info';
import { IPerspective } from '../perspective';
import { PerspectiveEditComponent } from '../perspective-edit/perspective-edit.component';
import { PerspectiveModel } from '../perspective-model';
import { PerspectiveModelImpl } from '../perspective-model-impl';
import { SubformEntry } from '../subform-entry';
import { SubformTablePreferenceSelection } from './subform-table-preference-selection';

@Injectable()
export class PerspectiveService {
	private model: PerspectiveModel;

	private selectedEO;
	private meta: EntityMeta | undefined;

	private tablePreferenceSubject = new Subject<Preference<SideviewmenuPreferenceContent> | undefined>();
	private searchtemplatePreferenceSubject = new Subject<Preference<SearchtemplatePreferenceContent> | undefined>();
	private subformTablePreferenceSubject = new Subject<SubformTablePreferenceSelection>();

	/**
	 * Triggered when a perspective requires that a displayed result list is reloaded.
	 */
	private updateResultsSubject = new Subject<any>();

	private loading = true;
	private loadingFinishedSubject = new Subject<any>();

	constructor(
		private metaService: MetaService,
		private preferences: PreferencesService,
		private authService: AuthenticationService,
		private selectableService: SelectableService,
		private layoutService: LayoutService,
		private dialogService: DialogService,
		private modalService: NgbModal,
		private i18nService: NuclosI18nService,
		private $log: Logger
	) {
		this.model = new PerspectiveModelImpl($log);

		this.authService.observeLoginStatus().subscribe(() => {
			this.clear();
		});
	}

	private clear() {
		this.selectedEO = undefined;
		this.meta = undefined;
		this.model = new PerspectiveModelImpl(this.$log);
		this.tablePreferenceSubject.next(undefined);
		this.searchtemplatePreferenceSubject.next(undefined);
		this.subformTablePreferenceSubject.next(undefined);
		this.setLoading(false);
	}

	selectEntityClass(meta: EntityMeta) {
		if (meta) {
			if (meta !== this.meta) {
				this.meta = meta;
				this.init().subscribe();
			}
		} else {
			this.clear();
		}
	}

	selectEo(eo: EntityObject | undefined) {
		this.selectedEO = eo;
		if (eo) {
			eo.getMeta().subscribe(meta => {
				if (this.meta !== meta) {
					this.meta = meta;
					this.init().subscribe();
				} else {
					this.applyPerspective(this.getSelectedPerspective());
				}
			});
		}
	}

	isLoading() {
		return this.loading;
	}

	private setLoading(loading: boolean) {
		this.loading = loading;

		if (!loading) {
			this.loadingFinishedSubject.next(loading);
		}
	}

	waitForLoadingFinished() {
		if (!this.isLoading()) {
			return observableOf(true);
		}

		this.$log.debug('Waiting for perspectives...');
		return this.loadingFinishedSubject.pipe(take(1));
	}

	init(): Observable<any> {
		this.$log.debug('Initializing perspectives...');
		this.setLoading(true);
		this.model.selectedPerspective = undefined;
		this.layoutService.setPerspectiveModel(this.model);

		return observableForkJoin(
			this.fetchLayouts(),
			this.fetchAllPreferences(),
			this.fetchPerspectives(),
		).pipe(finalize(() => {
			this.applyPerspective(this.model.selectedPerspective);
			this.setLoading(false);
		}));
	}

	getEntityClassId() {
		return this.meta && this.meta.getBoMetaId();
	}

	private getEntityObject(): EntityObject | undefined {
		return this.selectedEO;
	}

	getModel(): PerspectiveModel {
		return this.model;
	}

	/**
	 * Fetches all available layouts for this BO.
	 */
	fetchLayouts(): Observable<ILayoutInfo[]> {
		let entityClassId = this.getEntityClassId();

		if (!entityClassId) {
			return observableOf([]);
		}

		return this.fetchLayoutsAsync(entityClassId).pipe(tap(
			layouts => this.model.layoutInfos = layouts
		));
	};

	// TODO: Instead of loading complete preferences, load only necessary infos (UID, name)
	fetchAllPreferences(): Observable<Preference<any>[]> {
		let entityClassId = this.getEntityClassId();

		if (!entityClassId) {
			return observableOf([]);
		}

		return this.fetchAllPreferencesAsync(entityClassId).pipe(
			tap(
				(prefs: Array<Preference<any>>) => {
					this.model.sideviewMenuPrefs = prefs.filter(function (pref: Preference<any>) {
						let prefType: PreferenceType = 'table';
						return pref.type === prefType;
					});
					this.model.searchTemplatePrefs = prefs.filter(function (pref: Preference<any>) {
						let prefType: PreferenceType = 'searchtemplate';
						return pref.type === prefType;
					});
				}
			),
			concat(
				this.fetchSubformTablePreferences()
			),
		);
	};

	fetchSubformTablePreferences(): Observable<Preference<any>[]> {
		let entityClassId = this.getEntityClassId();

		if (!entityClassId) {
			return observableOf([]);
		}

		let prefFilter: IPreferenceFilter = {
			boMetaId: entityClassId,
			type: ['subform-table'],
			returnSubBoPreferences: true
		};

		return this.preferences.getPreferences(prefFilter).pipe(
			tap(
				(prefs: Array<Preference<SideviewmenuPreferenceContent>>) => {
					this.model.subformTablePrefs = prefs;
					this.$log.debug('subform table prefs: %o', prefs);
					this.updateSubformPreferencesForSelectedLayout();
				}
			)
		);
	};

	/**
	 * Fetches the available perspectives for this BO.
	 */
	private fetchPerspectives(): Observable<Preference<IPerspective>[]> {
		let entityClassId = this.getEntityClassId();

		if (!entityClassId) {
			return observableOf([]);
		}

		return this.fetchPerspectivesAsync(entityClassId).pipe(tap(
			perspectives => {
				this.$log.debug('Perspectives: %o', perspectives);
				this.model.setPerspectives(perspectives);
			}
		));
	}

	private fetchLayoutsAsync(boMetaId: string): Observable<ILayoutInfo[]> {
		return this.layoutService.getLayoutsForEntity(boMetaId);
	}

	/**
	 * Retrieves all preferences for the given BO that are relevant for perspectives.
	 *
	 * @param boMetaId
	 * @returns {IPromise<T>}
	 */
	private fetchAllPreferencesAsync(boMetaId: string): Observable<Preference<any>[]> {
		let prefFilter: IPreferenceFilter = {
			boMetaId: boMetaId,
			type: [
				PreferenceType.searchtemplate.name,
				PreferenceType.table.name,
				PreferenceType.subformTable.name
			]
		};

		return this.preferences.getPreferences(prefFilter);
	};

	/**
	 * Retrieves all available perspectives for the given BO.
	 */
	fetchPerspectivesAsync(boMetaId: string): Observable<Preference<IPerspective>[]> {
		let prefFilter: IPreferenceFilter = {
			boMetaId: boMetaId,
			type: ['perspective']
		};

		return this.preferences.getPreferences(prefFilter) as Observable<Preference<IPerspective>[]>;
	}

	/**
	 * Inserts or updates the given perspective preferences.
	 */
	save(perspectivePref: Preference<IPerspective>): Observable<Preference<IPerspective>> {
		this.model.setDefaultValues(perspectivePref.content);

		if (!this.model.isValidPerspective(perspectivePref.content)) {
			this.$log.warn('Invalid perspective');
			return observableOf(perspectivePref);
		}

		perspectivePref.name = perspectivePref.content.name;

		return this.preferences.savePreferenceItem(perspectivePref).pipe(tap(
			savedPerspectivePrefs => {
				this.$log.debug('pref id: %o, saved prefs: %o', perspectivePref.prefId, savedPerspectivePrefs);
				if (!perspectivePref.prefId && savedPerspectivePrefs) {
					perspectivePref.prefId = savedPerspectivePrefs.prefId;
				}
				this.getModel().addPerspective(perspectivePref);
			}
		));
	}

	/**
	 * Deletes the given perspective preferences and removes it from the model.
	 * Unsharing must be confirmed first.
	 */
	unshareAndDelete(perspectivePref: Preference<IPerspective>): Observable<boolean> {
		if (perspectivePref.shared) {
			return observableFrom(
				this.dialogService.confirm(
					{
						title: this.i18nService.getI18n('webclient.perspective.delete.confirm.title'),
						message: this.i18nService.getI18n('webclient.perspective.delete.confirm.message'),
					}
				)
			).pipe(mergeMap(
				() => observableFrom(this.preferences.unsharePreferenceItem(perspectivePref)).pipe(
					mergeMap(
						() => this.delete(perspectivePref)
					))
			));
		} else {
			return this.delete(perspectivePref);
		}
	}

	private delete(perspectivePref: Preference<IPerspective>): Observable<boolean> {
		return this.preferences.deletePreferenceItem(perspectivePref).pipe(tap(
			() => {
				if (perspectivePref.selected) {
					this.togglePerspective(perspectivePref);
				}
				this.model.removePerspective(perspectivePref);
			}
		));
	}

	applySelectedLayout() {
		let currentEO = this.getEntityObject();

		if (!currentEO) {
			this.$log.debug('No BO selected');
			return;
		}

		if (currentEO.getEntityClassId() !== this.getEntityClassId()) {
			this.$log.debug('BO meta ID does not match entity meta ID');
			return;
		}

		currentEO.checkLayoutChange();
	};

	applySelectedSideviewmenu() {
		let perspective = this.getSelectedPerspective();
		if (!perspective) {
			return;
		}

		let prefId = perspective.sideviewMenuPrefId;
		if (!prefId) {
			return;
		}

		this.preferences.getPreference(prefId).subscribe((pref: Preference<SideviewmenuPreferenceContent>) => {
			if (!pref) {
				return;
			}

			this.metaService.getBoMeta(pref.boMetaId).subscribe(meta => {
				this.selectableService.addMetaDataToColumns(pref, meta);
				this.tablePreferenceSubject.next(pref);
			});
		});
	};

	applySelectedSearchtemplate() {
		const pref = this.getSelectedSearchTemplate();
		if (!pref) {
			return;
		}

		this.metaService.getBoMeta(pref.boMetaId).subscribe(meta => {
			this.selectableService.addMetaDataToColumns(pref, meta);
			this.searchtemplatePreferenceSubject.next(pref);
			this.updateResultsSubject.next();
		});
	};

	applySelectedSubformConfigurations() {
		// TODO: Apply all subform configurations
		let subformPrefIds = this.model.selectedPerspective && this.model.selectedPerspective.subformTablePrefIds;
		this.$log.debug('subform prefs: %o', subformPrefIds);
		if (subformPrefIds) {
			for (let subformEntityClassId in subformPrefIds) {
				if (!subformPrefIds.hasOwnProperty(subformEntityClassId)) {
					continue;
				}
				let selectedPrefId = subformPrefIds[subformEntityClassId];
				if (selectedPrefId) {
					this.preferences.getPreference(selectedPrefId).subscribe(
						(pref: Preference<SideviewmenuPreferenceContent>) => {
							let selection: SubformTablePreferenceSelection = {
								entityClassId: subformEntityClassId,
								subformTablePreference: pref
							};
							this.subformTablePreferenceSubject.next(selection);
						}
					);
				}
			}
		}
	};

	/**
	 * Sets the given perspective as the selected perspective.
	 *
	 * @param perspective
	 */
	selectPerspective(perspective: IPerspective) {
		this.model.selectedPerspective = perspective;
	}

	/**
	 * Returns the selected perspective, or null if no perspective is selected.
	 *
	 * @returns {IPerspective}
	 */
	getSelectedPerspective(): IPerspective | undefined {
		return this.model.selectedPerspective;
	}

	/**
	 * Returns the search template preference for the currently selected perspective, or null.
	 *
	 * @returns {IPerspective}
	 */
	getSelectedSearchTemplate(): Preference<SearchtemplatePreferenceContent> | null {
		let selectedPerspective = this.getSelectedPerspective();
		if (selectedPerspective && selectedPerspective.searchTemplatePrefId) {
			return this.model.getSearchTemplate(selectedPerspective.searchTemplatePrefId);
		}

		return null;
	}

	/**
	 * Returns the selected sideview menu preference ID, or null if no perspective is selected.
	 *
	 * @returns {string}
	 */
	getSelectedSideviewMenuPrefId(): string | null {
		if (!this.model.selectedPerspective) {
			return null;
		}

		return this.model.selectedPerspective.sideviewMenuPrefId;
	}

	isNewAllowed() {
		return this.authService.isActionAllowed(UserAction.ConfigurePerspectives);
	}

	isEditAllowed() {
		return this.authService.isActionAllowed(UserAction.ConfigurePerspectives);
	}

	togglePerspective(perspectivePref: Preference<IPerspective>) {
		this.getModel().togglePerspective(perspectivePref.content);
		if (perspectivePref.selected) {
			this.applyPerspective(perspectivePref.content);
			this.preferences.selectPreference(perspectivePref).subscribe();
		} else {
			this.applyPerspective(undefined);
			this.preferences.deselectPreference(perspectivePref).subscribe();
		}
	}

	editPerspective(perspectivePref: Preference<IPerspective>) {
		this.editInModal(perspectivePref.content, false).subscribe(
			() => this.save(perspectivePref).subscribe()
		);
	}

	editInModal(perspective: IPerspective, isNew: boolean) {
		// Reload preferences, or we will not see newly added searchtemplate configurations etc.
		this.fetchAllPreferences().subscribe();

		let previousSelected = this.getModel().selectedPerspective;
		this.applyPerspective(perspective);

		let ngbModalRef = this.modalService.open(
			PerspectiveEditComponent
		);

		ngbModalRef.componentInstance.perspective = perspective;
		ngbModalRef.componentInstance.model = this.getModel();
		ngbModalRef.componentInstance.isNew = isNew;
		ngbModalRef.componentInstance.apply.subscribe(() => this.applyPerspective(perspective));

		return observableFrom(ngbModalRef.result).pipe(
			finalize(() => this.applyPerspective(previousSelected)));
	}

	newPerspective() {
		const entityClassId = this.getEntityClassId();
		if (!entityClassId) {
			this.$log.warn('Can not create new perspective: no entity class selected');
			return;
		}

		let newPerspective: IPerspective = {
			name: '',
			boMetaId: entityClassId,
			layoutId: '',
			searchTemplatePrefId: '',
			sideviewMenuPrefId: '',
			subformTablePrefIds: {},
			layoutForNew: true
		};

		this.editInModal(newPerspective, true).subscribe(
			(perspective: IPerspective) => this.saveNewPerspective(perspective)
		);
	}

	private saveNewPerspective(perspective: IPerspective) {
		let perspectivePrefs: Preference<IPerspective> = {
			boMetaId: perspective.boMetaId,
			type: 'perspective',
			name: perspective.name,
			content: perspective
		};

		this.save(perspectivePrefs).subscribe(
			() => this.$log.debug('Perspective saved')
		);
	}

	applyPerspective(perspective: IPerspective | undefined) {
		this.$log.debug('Apply perspective: %o', perspective);

		this.getModel().selectedPerspective = perspective;

		this.applySelectedLayout();
		this.applySelectedSideviewmenu();
		this.applySelectedSearchtemplate();
		this.applySelectedSubformConfigurations();
	}

	updateSubformPreferencesForSelectedLayout(): void {
		let entries = new Map<string, SubformEntry>();
		let layoutId = this.model.selectedPerspective && this.model.selectedPerspective.layoutId;

		this.model.subformTablePrefs.filter(
			pref => !pref.layoutId || pref.layoutId === this.model.getLayoutFqn(layoutId)
		).forEach(
			pref => {
				let entry = entries.get(pref.boMetaId);
				if (!entry) {
					entry = {
						subEntityClassId: pref.boMetaId,
						subEntityName: '',
						columnPreferences: []
					};
					entries.set(pref.boMetaId, entry);
				}
				entry.columnPreferences.push(pref);
			}
		);

		let result: SubformEntry[] = [];
		entries.forEach(entry => {
			result.push(entry);
			this.metaService.getEntityMeta(entry.subEntityClassId).subscribe(
				meta => entry.subEntityName = meta.getEntityName()
			);
		});

		this.$log.debug('Updated subform table prefs: %o', result);

		this.model.subformTablePrefsBySubEntity = result;
	}

	observeSelectedTablePreference() {
		return this.tablePreferenceSubject;
	}

	observeSelectedSearchtemplatePreference() {
		return this.searchtemplatePreferenceSubject;
	}

	observeSelectedSubformTablePreference(subformEntityClassId) {
		return this.subformTablePreferenceSubject.pipe(
			filter(
				selection => (selection.entityClassId === subformEntityClassId) as boolean
			),
			map(
				selection => selection.subformTablePreference
			),
		);
	}

	observeSelectedSubformTablePreferences() {
		return this.subformTablePreferenceSubject;
	}

	getSelectedSubformTablePreference(
		subformMeta: EntityMeta
	): Observable<Preference<SideviewmenuPreferenceContent> | undefined> {
		let perspective = this.model.selectedPerspective;

		if (!perspective || !perspective.subformTablePrefIds) {
			return observableOf(undefined);
		}

		let selectedPrefId = perspective.subformTablePrefIds[subformMeta.getBoMetaId()];
		if (!selectedPrefId) {
			return observableOf(undefined);
		}

		return this.preferences.getPreference(selectedPrefId) as Observable<Preference<SideviewmenuPreferenceContent> | undefined>;
	}

	observeUpdateResults() {
		return this.updateResultsSubject;
	}
}
