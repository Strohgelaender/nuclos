import { Component, DoCheck, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GridOptions } from 'ag-grid';
import { DataService } from '../../entity-object-data/shared/data.service';
import { WsTab } from '../../explorertrees/explorertrees.model';
import { Logger } from '../../log/shared/logger';
import { NuclosConfigService } from '../../shared/nuclos-config.service';

@Component({
	selector: 'nuc-sidetree',
	templateUrl: './sidetree.component.html',
	styleUrls: ['./sidetree.component.scss']
})
export class SideTreeComponent implements OnInit, DoCheck {

	private static imagePath: string;

	@Input()
	tree: WsTab;

	@Output()
	selectedNode = new EventEmitter<any>();

	private _tree: WsTab;

	treeData: any[];

	gridOptions: GridOptions = <GridOptions>{};


	private openingNode: any;

	constructor(
		private dataService: DataService,
		private nuclosConfig: NuclosConfigService,
		private $log: Logger
	) {
		SideTreeComponent.imagePath = this.nuclosConfig.getRestHost() + '/meta/icon/';
	}

	ngOnInit() {
		this.initTreeInGrid();
	}

	ngDoCheck () {
		if (this._tree !== this.tree) {
			this._tree = this.tree;
			this.loadTreeData();
		}
	}

	private loadTreeData() {
		this.dataService.loadTreeData(this.tree).subscribe(data => {
			this.treeData = data;
			this.reloadRowData(true);
		});
	}

	private reloadRowData = (full: boolean) => {
		if (this.gridOptions.api) {
			if (full) {
				this.gridOptions.api.setColumnDefs(this.getColumnDefs());
			}
			this.gridOptions.api.setRowData(this.treeData);
		}
	}

	private rowGroupOpened = (params) => {
		let node = params.node;
		this.openingNode = node;
		if (node.data.open || node.data.leaf) {
			node.data.open = false;
			return;
		}

		if (node.data.children !== undefined) {
			node.data.open = true;
			return;
		}

		this.dataService.loadSubTreeData(node.data.node_id).subscribe(data => {
			node.data.children = data;
			node.data.open = true;
			node.data.leaf = data.length === 0;
			this.reloadRowData(false);
		});
	}

	private rowClicked = (params) => {
		// This prevents that an eo is selected when just the node is opened to expand the tree
		if (params.node === this.openingNode) {
			this.openingNode = undefined;
			return;
		}
		this.selectedNode.emit(params.data);
	}

	private labelCellStyle() {
		return {'text-align': 'left'};
	}

	private innerCellRenderer = (params) => {
		let imageLink = SideTreeComponent.imagePath + params.data.icon;
		let padding = params.data.leaf ? 11 : 4;
		return '<img src="' + imageLink + '" style="padding-left: ' + padding + 'px;" /> ' + params.data.title;
	}

	private getColumnDefs() {
		return [
			{ headerName: this.tree.label, field: 'title', width: 250,
				cellRenderer: 'group',
				cellRendererParams: {
					innerRenderer: this.innerCellRenderer,
					suppressCount: true
				},
				cellStyle: this.labelCellStyle
			}
		];
	}

	private initTreeInGrid() {

		this.gridOptions.enableColResize = true;
		this.gridOptions.enableSorting = true;
		this.gridOptions.animateRows = true;
		this.gridOptions.rowHeight = 28;
		this.gridOptions.onRowClicked = this.rowClicked;
		this.gridOptions.onRowGroupOpened = this.rowGroupOpened;

		this.gridOptions.getNodeChildDetails = <any>function(node) {
			if (!node.leaf) {
				return {
					group: true,
					children: node.children || [],
					expanded: node.open
				};
			} else {
				return null;
			}
		};

		this.gridOptions.columnDefs = this.getColumnDefs();
	}

	gridReady() {
	}

}
