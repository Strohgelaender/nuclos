import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClickOutsideService } from '../../click-outside/click-outside.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-modal',
	templateUrl: './detail-modal.component.html',
	styleUrls: ['./detail-modal.component.css']
})
export class DetailModalComponent implements OnInit {
	@Input() eo: EntityObject;

	constructor(
		private clickoutside: ClickOutsideService,
		private activeModal: NgbActiveModal,
		private ref: ElementRef
	) {
	}

	ngOnInit() {
	}

	close() {
		// To notify open subform cell editors etc.
		this.clickoutside.outsideClick(this.ref.nativeElement);

		this.activeModal.close();
	}
}
