import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../authentication';
import { EntityMeta } from '../entity-object-data/shared/bo-view.model';
import { EntityObjectErrorService } from '../entity-object-data/shared/entity-object-error.service';
import { EntityObjectEventService } from '../entity-object-data/shared/entity-object-event.service';
import { EntityObjectResultUpdateService } from '../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { EntityObjectSearchfilterService } from '../entity-object-data/shared/entity-object-searchfilter.service';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../entity-object-data/shared/entity-object.service';
import { MetaService } from '../entity-object-data/shared/meta.service';
import { WsTab } from '../explorertrees/explorertrees.model';
import { Logger } from '../log/shared/logger';
import { PerspectiveService } from '../perspective/shared/perspective.service';
import { BrowserRefreshService } from '../shared/browser-refresh.service';
import { TaskList } from '../task/shared/task-list';
import { EntityObjectRouteService } from './entity-object-route.service';
import { EntityObjectPreferenceService } from './shared/entity-object-preference.service';

export class LoadMoreResultsEvent {

	constructor(
		public offset: number,
		public limit: number,
		public count: boolean,
		public successCallback: Function,
		public failCallback: Function,
	) {
	}
}

export type SidebarMode = 'sidebar' | 'tree' | 'task-list';

@Component({
	selector: 'nuc-entity-object',
	templateUrl: './entity-object.component.html',
	styleUrls: ['./entity-object.component.css']
})
export class EntityObjectComponent implements OnInit, OnDestroy {

	private urlQuery: string | undefined;

	triggerUnsavedChangesPopover: Date;
	private compId = Math.random();

	private explorerTree: WsTab | undefined;
	private taskList?: TaskList;

	private subscriptions: Subscription[] = [];

	constructor(
		private authenticationService: AuthenticationService,
		private route: ActivatedRoute,
		private metaService: MetaService,
		private eoEventService: EntityObjectEventService,
		private eoService: EntityObjectService,
		private eoRouteService: EntityObjectRouteService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
		private $log: Logger,
		private browserRefreshService: BrowserRefreshService,
		private perspectiveService: PerspectiveService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		// TODO: Only injected here in order to be instantiated by angular
		private eoErrorService: EntityObjectErrorService,
	) {
	}

	ngOnInit() {
		this.subscriptions.push(
			this.eoEventService.observeSelectedEo().subscribe(
				(eo: EntityObject) => {
					// TODO: Find a better place for this
					this.perspectiveService.selectEo(eo);
				}
			)
		);

		this.subscriptions.push(
			this.route.params.subscribe(
				(params: Params) => {
					this.urlQuery = params['query'];
					this.$log.warn('Params from %o: %o', this.compId, params);
					this.eoRouteService.handleParams(params);
				}
			)
		);

		this.subscriptions.push(
			this.browserRefreshService.onEoChanges().subscribe(
				() => {
					// reload eo data
					let selectedEo = this.eoResultService.getSelectedEo();
					if (selectedEo && !selectedEo.isDirty()) {
						selectedEo.reload();
					}
				}
			)
		);

		this.subscriptions.push(
			this.eoResultService.observeSelectedEntityClassId().subscribe(
				(entityClassId: string) => {
					this.metaService.getEntityMeta(entityClassId).subscribe(
						meta => {
							this.perspectiveService.selectEntityClass(meta);
							this.loadPrefsAndEoListAfterPerspective(entityClassId);
						}
					);
				}
			)
		);

		this.subscriptions.push(
			this.eoResultService.observeResultListUpdate().subscribe(
				pristine => {
					this.eoResultService.waitForEoSelection().subscribe(() => {
						let selectedEo = this.eoResultService.getSelectedEo();
						if (pristine && selectedEo) {
							this.eoResultService.addSelectedEoToList();
						} else {
							this.eoResultService.selectFirstResult();
						}
					});
				}
			)
		);

	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

	getSideviewmenuPref() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$.getValue();
	}

	getSearchtemplatePref() {
		return this.eoSearchfilterService.getSelectedSearchfilter();
	}

	getSideviewmenuPrefSubject() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$;
	}

	getSearchtemplatePrefSubject() {
		return this.eoSearchfilterService.observeSelectedSearchfilter();
	}

	/**
	 * TODO: Prefs and EO list are now loaded twice in some cases, because this component is
	 * not a singleton, but instantiated at least twice by angular (because of different routes).
	 *
	 * @param entityClassId
	 */
	private loadPrefsAndEoListAfterPerspective(entityClassId: string) {
		this.subscriptions.push(
			this.perspectiveService.waitForLoadingFinished().subscribe(
				() => this.loadPrefsAndEoList(entityClassId)
			)
		);
	}


	private loadPrefsAndEoList(entityClassId: string) {
		console.warn('Load searchtemplate Prefs and EO list for entity class %o', entityClassId);

		let entityMeta = this.metaService.getBoMeta(entityClassId);
		if (!entityMeta) {
			return;
		}

		this.subscriptions.push(
			entityMeta.subscribe(
				meta => {
					this.eoResultService.setSelectedMeta(meta);

					this.subscriptions.push(
						this.entityObjectPreferenceService.loadPreferences(meta, this.urlQuery).subscribe(() => {
							this.$log.warn('prefs loaded');
						})
					);
				}
			)
		);
	}

	private hasResults() {
		return this.eoResultService.getLoadedResultCount() > 0;
	}

	warnAboutUnsavedChanges() {
		this.triggerUnsavedChangesPopover = new Date();
	}

	getMeta(): EntityMeta | undefined {
		return this.eoResultService.getSelectedMeta();
	}

	getSelectedEo() {
		return this.eoResultService.getSelectedEo();
	}

	canCreateBo() {
		return this.eoResultService.canCreateBo;
	}

	selectExplorerTree(tree: WsTab) {
		if (this.explorerTree === tree) {
			this.explorerTree = undefined;
			return;
		}

		this.explorerTree = tree;
	}

	getExplorerTree() {
		return this.explorerTree;
	}

	selectedTreeNode(tnode: any) {
		// NUCLOS-3783 Display of the following eo:
		this.$log.info('Selected EO, Entity:' + tnode.boMetaId + ' PK:' + tnode.boId);

		this.eoService.loadEO(tnode.boMetaId, tnode.boId).subscribe(eo => {
			this.eoResultService.selectEo(eo);
			// NUCLOS-6039
			this.eoResultService.reloadCanCreateBoIfNotSet(tnode.boMetaId);
		});
	}

	getSidebarMode(): SidebarMode {
		if (this.getExplorerTree()) {
			return 'tree';
		} else if (this.taskList) {
			return 'task-list';
		}
		return 'sidebar';
	}

}


/**
 * view eo in popup
 * no sidebar/toolbar/header
 */
@Component({
	selector: 'nuc-entity-object',
	templateUrl: './entity-object-popup.component.html',
	styleUrls: ['./entity-object.component.css']
})
export class EntityObjectPopupComponent extends EntityObjectComponent {
	ngOnInit() {
		super.ngOnInit();
		$('header').hide();
	}
}
