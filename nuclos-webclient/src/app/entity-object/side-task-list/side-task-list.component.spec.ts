import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideTaskListComponent } from './side-task-list.component';

xdescribe('SideTaskListComponent', () => {
	let component: SideTaskListComponent;
	let fixture: ComponentFixture<SideTaskListComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [SideTaskListComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SideTaskListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
