import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { from as observableFrom, Observable } from 'rxjs';

import { finalize } from 'rxjs/operators';
import { EntityObject, SubEntityObject } from '../../entity-object-data/shared/entity-object.class';
import { RuleService } from '../../rule/shared/rule.service';
import { DetailModalComponent } from '../detail-modal/detail-modal.component';
import { IEntityObject } from '@nuclos/nuclos-addon-api';

@Injectable()
export class DetailModalService {

	constructor(
		private modalService: NgbModal,
		private ruleService: RuleService
	) {
	}

	openEoInModal(eo: IEntityObject): Observable<any> {
		let ngbModalRef = this.modalService.open(
			DetailModalComponent,
			{size: 'lg', windowClass: 'fullsize-modal-window'}
		);

		let result = observableFrom(ngbModalRef.result);

		/**
		 * A SubEntityObject must be modified to behave like a main EO if it is opened via modal.
		 */
		if (eo instanceof SubEntityObject) {
			const subEo = eo;

			// After the modal is closed, layout rules may change again
			result.pipe(finalize(() => this.ruleService.updateRuleExecutor(subEo))).subscribe();

			eo = subEo.toEntityObject();
		}

		ngbModalRef.componentInstance.eo = eo;

		return result;
	}
}
