///<reference path="../../../../node_modules/angular-resizable-element/dist/esm/src/interfaces/boundingRectangle.interface.d.ts"/>
import {
	Component,
	ElementRef,
	EventEmitter,
	HostListener,
	Input,
	NgZone,
	OnDestroy,
	OnInit,
	Output,
	ViewChild
} from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { ActivatedRoute, Params } from '@angular/router';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { ColDef, ColumnEvent, GridOptions, RowNode } from 'ag-grid';
import { ResizeEvent } from 'angular-resizable-element';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectNavigationService } from '../../entity-object-data/shared/entity-object-navigation.service';
import { EntityObjectResultUpdateService } from '../../entity-object-data/shared/entity-object-result-update.service';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { SortModel } from '../../entity-object-data/shared/sort.model';
import { ColumnLayoutChanges } from '../../layout/shared/column-layout-changes';
import { Logger } from '../../log/shared/logger';
import {
	ColumnAttribute,
	Preference,
	SearchtemplatePreferenceContent,
	SidebarLayoutType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { SearchService } from '../../search/shared/search.service';
import { VLP_ID_PARAM } from '../../shared/browser-refresh.service';
import { FqnService } from '../../shared/fqn.service';
import { Mixin } from '../../shared/mixin';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { EntityObjectGridColumn } from '../entity-object-grid/entity-object-grid-column';
import { EntityObjectComponent } from '../entity-object.component';
import { EntityObjectPreferenceService } from '../shared/entity-object-preference.service';
import { SideviewmenuService } from '../shared/sideviewmenu.service';
import { EntityObjectDatasource, EntityObjectTitleInfoDatasource } from './grid/entity-object-datasource';
import { SidebarCardLayoutViewItem, SidebarViewItem } from './view/sidebar-view.model';
import { SearchEditorButtonComponent } from '../../search/search-editor-button/search-editor-button.component';
import { SearchEditorComponent } from '../../search/search-editor/search-editor.component';


@Component({
	selector: 'nuc-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss']
})
@Mixin([ColumnLayoutChanges])
export class SidebarComponent implements ColumnLayoutChanges, OnInit, OnDestroy {

	@Input() meta: EntityMeta;
	@Input() searchtemplatePreference: Preference<SearchtemplatePreferenceContent>;

	@Output() onColumnChange: EventEmitter<Preference<SideviewmenuPreferenceContent>> =
		new EventEmitter<Preference<SideviewmenuPreferenceContent>>();

	@ViewChild('listContainer') private listContainerElement: ElementRef;
	@ViewChild('sidebar') private sidebar: ElementRef;

	sidebarLayoutType: SidebarLayoutType;
	selectedColumns: ColumnAttribute[] = [];
	gridColumns: EntityObjectGridColumn[] = [];
	sortModel: SortModel;

	// TODO: Do not access GridOptions directly here
	gridOptions: GridOptions = <GridOptions>{
		rowModelType: 'infinite'
	};

	private _sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>;

	private selectedEoDirtyState = false;
	private initialized = false;

	private lastKeyboardNavigationTime = 0;

	private subscriptions: Subscription[] = [];

	private dataReloadSubject = new Subject<any>();

	/* mixins */
	// tslint:disable-next-line
	columnLayoutChanged: Subject<Date> = new Subject<Date>();
	// tslint:disable-next-line
	columnLayoutChangedSubscription: Subscription;

	private searchEditorVisibilityObserver: MutationObserver;

	onColumnChangesDebounced(gridOptions: GridOptions): Observable<Date> {
		return {} as Observable<Date>;
	};

	getColDef(gridOptions: GridOptions, event: ColumnEvent): ColDef | undefined {
		return;
	};

	/**
	 * TODO: Too many dependencies!
	 */
	constructor(
		private route: ActivatedRoute,
		private entityObjectComponent: EntityObjectComponent,
		private sideviewmenuService: SideviewmenuService,
		private entityObjectService: EntityObjectService,
		private eoNavigationService: EntityObjectNavigationService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		private eoEventService: EntityObjectEventService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private searchService: SearchService,
		private nuclosConfigService: NuclosConfigService,
		private fqnService: FqnService,
		private dataService: DataService,
		private activatedRoute: ActivatedRoute,
		private ngZone: NgZone,
		private $log: Logger
	) {
	}

	ngOnInit() {
		this.searchEditorVisibilityObserver = new MutationObserver((mutations: MutationRecord[]) => {
			let relayoutWorkaround = false;
			for (let mutation of mutations) {
				let addedNodes = mutation.addedNodes;
				let addedCount = addedNodes.length;
				for (let i = 0; i < addedCount; i++) {
					if (this.isFirefox() &&
						addedNodes[i].attributes['id'] &&
						addedNodes[i].attributes['id'].value === 'search-editor-container') {
						relayoutWorkaround = true;
					}
					if (addedNodes[i].attributes['id'] &&
						addedNodes[i].attributes['id'].value === 'sidebar-statusbar') {
						relayoutWorkaround = true;
					}
				}
			}
			if (relayoutWorkaround) {
				this.relayoutWorkaround();
			}
		});
		this.searchEditorVisibilityObserver.observe(this.sidebar.nativeElement, {
			childList: true
		});

		// use VLP restrictions (NUCLOS-5330)
		this.route.queryParams.subscribe((params: Params) => {
			let vlpId = params[VLP_ID_PARAM];
			if (vlpId) {
				this.eoResultService.vlpId = vlpId;
				let vlpParams: URLSearchParams = new URLSearchParams();
				for (const param of Object.keys(params)) {
					vlpParams.append(param, params[param]);
				}
				this.eoResultService.vlpParams = vlpParams;
			}
		});

		this.subscriptions.push(
			this.entityObjectPreferenceService.selectedSideviewmenuPref$.subscribe(pref => {
				if (pref) {

					// FIXME hack to ignore preference change events from old view - avoid emitting them
					// this is caused by the 'redirect' /view/eoClassId --> /view/eoClassId/eoId
					let href = window.location.href + '/';
					if (href.indexOf(pref.boMetaId + '/') === -1 && href.indexOf(pref.boMetaId + '?') === -1) {
						return;
					}
					this.sideviewmenuPreference = pref;


					// TODO: Handle missing column names.
					this.selectedColumns = this.selectedSortedColumns();

					this.$log.debug('Selected columns: %o', this.selectedColumns);

					this.initOrUpdateGridColumns();
				}
			})
		);

		this.subscriptions.push(
			this.sideviewmenuService.onSideviewmenuPrefChange().subscribe(
				() => {
					this.onColumnChange.emit(this.sideviewmenuPreference);
				}
			)
		);

		this.subscriptions.push(
			this.sideviewmenuService.getViewType().subscribe(
				type => this.sidebarLayoutType = type
			)
		);

		this.subscriptions.push(this.eoResultService.observeResultListUpdate().subscribe(data => {

			if (this.initialized) {
				return;
			}
			this.initialized = true;

			// TODO select selected eo in grid initially - after rows are rendered - is there no callback on ag-grid ?
			setTimeout(() => {
				this.markCurrentEoAsSelectedInGrid();
			}, 1000);

			// save column layout when column order, width or sort order was changed (debounced)
			if (this.sideviewmenuService.getViewType().getValue() !== 'card') {
				this.subscriptions.push(
					this.onColumnChangesDebounced(this.gridOptions).subscribe(() => {

						this.sideviewmenuPreference.content.columns =
							this.sideviewmenuService.getSelectedAttributesFromGrid(this.gridOptions, this.meta);

						this.saveSideviewmenuPreference();
					})
				);
			}

			// sort columns immediately - not debounced
			this.gridOptions.onSortChanged = () => {

				// prevent saving a preference which was just deleted
				if (this.sideviewmenuPreference.prefId === undefined) {
					return;
				}

				// emit columnLayoutChanged event for debounced saving
				/*
				TODO
				this results in saving prefs all the time because onSortChange is not only called when user changes sort
				but also when grid is initialized:
				this.columnLayoutChanged.next(new Date());
				*/

				if (this.gridOptions.api) {

					// update preference
					this.sideviewmenuPreference.content.columns.forEach(col => delete col.sort);
					this.gridOptions.api.getSortModel().forEach((sortModelColumn, index) => {
						let column = this.sideviewmenuPreference.content.columns.filter(
							co => co.boAttrId === sortModelColumn.colId).shift();
						if (column) {
							column.sort = {
								direction: sortModelColumn.sort,
								enabled: true,
								prio: index
							};
						}
					});

					// select current EO in sidebar after data was loaded
					this.gridOptions.onModelUpdated = event => {
						if (this.gridOptions.api && this.gridOptions.api.paginationGetRowCount() > 0) {
							let rowFound = this.markCurrentEoAsSelectedInGrid();
							if (rowFound) {
								delete this.gridOptions.onModelUpdated;
							}
						}
					};
				}

				this.saveSideviewmenuPreference(); // TODO save debounced - see TODO above

				// load data again after sorting was changed
				this.initiateDataReload();
			};

		}));

		// load data when search conditions were changed
		this.subscriptions.push(
			this.searchService.subscribeDataLoad().subscribe(() => {
				this.initiateDataReload();
			})
		);

		// select eo in sidebar when url has changed
		this.subscriptions.push(
			this.activatedRoute.params.subscribe((params: Params) => {
				let selectedEo = this.eoResultService.getSelectedEo();
				let sid = selectedEo ? selectedEo.getId() : undefined;
				// ignore route change events during cursor up/down navigation
				if (String(sid) !== String(params.entityObjectId) && (new Date().getTime() - this.lastKeyboardNavigationTime) > 1000) {
					this.markEoAsSelectedInGrid(params.entityObjectId);
				}
			})
		);

		this.subscriptions.push(
			this.dataReloadSubject.pipe(debounceTime(250)).subscribe(
				() => this.reloadData()
			)
		);

		this.subscriptions.push(
			this.eoResultService.observeSelectedEntityClassId().subscribe(
				() => {
					this.initiateDataReload()
				}
			)
		);
	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
		this.eoResultService.resetResults();
		this.searchEditorVisibilityObserver.disconnect();
	}

	initiateDataReload() {
		this.dataReloadSubject.next();
	}

	/**
	 * TODO: Reload happens too often, when opening an entity but not a specific record:
	 * - Result list is loaded for the entity
	 * - First record is auto-selected which causes a route change
	 * - Route change causes the EntityObject component (and Sidebar) to be initialized again
	 * - Newly initialized Sidebar triggers loading of the result list again (this should not happen)
	 */
	private reloadData() {
		if (this.gridOptions.api) {
			this.eoResultService.resetResults();
			if (this.sidebarLayoutType === 'card') {

				// layout type 'card'

				this.gridOptions.api.setDatasource(new EntityObjectTitleInfoDatasource(
					this.eoResultUpdateService,
					this.eoResultService,
					this.nuclosConfigService,
					this.fqnService,
					this.dataService
				));
			} else {

				// layout type 'table'

				this.gridOptions.api.setDatasource(
					new EntityObjectDatasource(
						this.eoResultUpdateService,
						this.eoResultService,
						this.nuclosConfigService,
					)
				);
			}
		} else {
			// If grid is not ready yet, try again
			this.initiateDataReload();
		}
	}

	@Input()
	set sideviewmenuPreference(sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>) {
		this._sideviewmenuPreference = sideviewmenuPreference;
		this.setSearchEditorAttributesContainerHeight();
	}

	get sideviewmenuPreference(): Preference<SideviewmenuPreferenceContent> {
		return this._sideviewmenuPreference;
	}

	private setSearchEditorAttributesContainerHeight(): void {
		$('search-editor-attributes-container').css('height', this.getSearchAttributesContainerHeight() + 'px')
	}

	isSearchEditorFixedAndShowing(): boolean {
		if (this._sideviewmenuPreference) {
			return this._sideviewmenuPreference.content.sideviewSearchEditorFixed &&
				this._sideviewmenuPreference.content.sideviewSearchEditorFixedAndShowing;
		}
		return false;
	}

	/**
	 * init or update grid columns
	 */
	private initOrUpdateGridColumns(): void {
		this.gridColumns = this.getGridColumns();
		this.sortModel = new SortModel(
				this.sideviewmenuService.getAgGridSortModel(this.selectedColumns.filter(
					col => col.sort && col.sort.direction))
			);
	}

	/**
	 * is called when a row was selected
	 */
	rowSelected(params: { node: RowNode }) {
		if (params.node.isSelected()) { // ignore deselection event
			if (params.node.id !== null) {
				// TODO: If this is a dynamic task list, get the entity class from the EO data (attribute "ENTITY")
				this.selectEo(this.getMeta()!.getBoMetaId(), params.node.id);
			}
		}
	}

	protected getGridColumns(): EntityObjectGridColumn[] {
		let columns: EntityObjectGridColumn[] = [];
		let cellStyle = {'border-right': '1px dotted #888888'};

		if (this.sidebarLayoutType === 'card') {

			// layout type 'card'
			// TODO cellrenderer
			let titleColDef = {
				headerName: 'Title',
				field: 'title',
				cellStyle: cellStyle
			} as EntityObjectGridColumn;
			columns.push(titleColDef);

			let infoColDef = {
				headerName: 'Info',
				field: 'info',
				cellStyle: cellStyle
			} as EntityObjectGridColumn;
			columns.push(infoColDef);
		} else {

			// layout type 'table'
			columns = this.selectedColumns.map(col => {
				// TODO: Use EntityObjectGridService to create column definition
				let column = {
					headerName: col.name,
					field: col.boAttrId, // FQN
					width: col.width,
					cellStyle: cellStyle
				} as EntityObjectGridColumn;
				if (this.meta) {
					let attributeMeta = this.meta.getAttributeMetaByFqn(col.boAttrId);
					column.attributeMeta = attributeMeta;
					if (attributeMeta) {
						column.cellEditorParams = {
							attrMeta: attributeMeta,
						};
					}
				}

				return column;
			});
		}
		return columns;
	}

	/**
	 * disables row selection if current eo is dirty
	 */
	private updateRowSelectionAdmission(eo: EntityObject) {
		this.selectedEoDirtyState = eo.isDirty();
		this.gridOptions.rowSelection = eo.isDirty() ? 'none' : 'single';
	}

	/**
	 * display dirty popover when trying to navigate away
	 */
	checkDirtyState() {
		if (this.selectedEoDirtyState) {
			this.entityObjectComponent.warnAboutUnsavedChanges();
		}
	}

	gridReady() {

		// update changed item (from detail form) in list
		this.subscriptions.push(
			this.eoEventService.observeEoModification().pipe(filter(
				eo => eo.getEntityClassId() === this.eoResultService.getSelectedEntityClassId()
			)).subscribe((eo: EntityObject) => {
				// update data in sidebar
				this.updateViewData(eo);
				this.updateRowSelectionAdmission(eo);
			})
		);

		// update item reset (from detail form) in list
		this.subscriptions.push(
			this.eoEventService.observeResetEo().pipe(filter(
				eo => eo.getEntityClassId() === this.eoResultService.getSelectedEntityClassId()
			)).subscribe((eo: EntityObject) => {

				// resetting a new unsaved eo removes it from list
				if (eo.isNew()) {
					this.eoNavigationService.navigateToView(this.getMeta()!.getBoMetaId());
				}

				// update data in sidebar
				this.updateViewData(eo);
				this.updateRowSelectionAdmission(eo);
			})
		);

		this.subscribeSavedEo();

		this.subscriptions.push(
			this.eoEventService.observeDeletedEo().pipe(filter(
				eo => eo.getEntityClassId() === this.eoResultService.getSelectedEntityClassId()
			)).subscribe(eo => {
				this.initiateDataReload();
			})
		);

		this.subscriptions.push(
			this.eoEventService.observeAddEo().pipe(filter(
				eo => eo.getEntityClassId() === this.eoResultService.getSelectedEntityClassId()
			)).subscribe(() => {
				if (this.gridOptions.api) {
					this.gridOptions.api.deselectAll();
					this.gridOptions.api.refreshInfiniteCache();
				}
			})
		);
	}


	private subscribeSavedEo() {
		// update sidebar after saving the eo to remove dirty row color
		this.subscriptions.push(
			this.eoEventService.observeSavedEo().subscribe((eo: EntityObject) => {
				// update data in sidebar
				this.updateViewData(eo);

				// select new eo if it was created
				this.markCurrentEoAsSelectedInGrid();

				// if the first eo is created on an empty list it will not show up in grid for whatever reason - so refresh list after save
				if (this.gridOptions.api!.getRenderedNodes().length === 0) {
					this.initiateDataReload();
				}

				this.updateRowSelectionAdmission(eo);

				// TODO who is closing this subscription ??
				this.subscribeSavedEo();
			})
		);
	}

	/**
	 * mark row of selectedEo in grid
	 * @return {boolean} true if row was found
	 */
	private markCurrentEoAsSelectedInGrid(): boolean {
		let selectedEo = this.eoResultService.getSelectedEo();
		if (selectedEo) {
			let selectedEoId = selectedEo.getId();
			return this.markEoAsSelectedInGrid(selectedEoId);
		}
		return false;
	}

	/**
	 * mark row as selected in grid
	 * @return {boolean} true if row was found
	 */
	private markEoAsSelectedInGrid(selectedEoId): boolean {
		let rowFound = false;
		if (this.gridOptions.api) {
			this.gridOptions.api.deselectAll();

			if (selectedEoId !== undefined) {
				this.gridOptions.api.forEachNode(rowNode => {
					if (
						selectedEoId && rowNode.id && rowNode.id.toString() === selectedEoId.toString() ||
						selectedEoId === null && rowNode.id === null
					) {
						rowNode.setSelected(true);
						rowFound = true;
						return;
					}
				});
			}
		}
		return rowFound;
	}

	private updateViewData(eo: EntityObject): void {
		this.gridOptions!.api!.forEachNode(rowNode => {
			let eoId = eo.getId();
			if (rowNode.data) {
				rowNode.id = rowNode.data.getId();
			}

			if (
				eoId && rowNode.id && rowNode.id.toString() === eoId.toString()
				||
				eo.isNew() && rowNode.id === null
			) {
				if (this.sidebarLayoutType === 'card') {
					rowNode.setData(
						new SidebarCardLayoutViewItem(
							eo,
							this.nuclosConfigService,
							this.fqnService,
							this.dataService
						).build()
					);
				} else {
					rowNode.setData(
						new SidebarViewItem(
							eo,
							this.nuclosConfigService
						).build()
					);
				}

				// update rowClass to display correct row color
				this.gridOptions.api!.redrawRows({rowNodes: [rowNode]});

				return;
			}
		});
	}

	getMeta() {
		return this.eoResultService.getSelectedMeta();
	}

	selectedSortedColumns(): ColumnAttribute[] {
		return this.sideviewmenuService.getSelectedColumnsSorted(this.sideviewmenuPreference);
	}

	onResize(event: ResizeEvent): void {
		this.sideviewmenuPreference.content.sideviewMenuWidth = event.rectangle.width;
		$(document.body).addClass('disable-text-selection');
	}

	onResizeEnd(event: ResizeEvent): void {
		let width = event.rectangle.width;
		this.sideviewmenuPreference.content.sideviewMenuWidth = width;
		this.sidebarWidthChanged();
		setTimeout(() => {
			$(document.body).removeClass('disable-text-selection');
			this.fixRowBackgroundColor();
		});
	}

	getSearchContainerHeight(): number {
		return this.getSearchAttributesContainerHeight() + this.getSearchConstantsHeight();
	}

	getSearchAttributesContainerHeight(): number {
		let result = this.isSearchEditorFixedAndShowing() ? 154 : 0;
		if (this.sideviewmenuPreference && this.sideviewmenuPreference.content.sideviewSearchAttributesContainerHeight) {
			result = this.sideviewmenuPreference.content.sideviewSearchAttributesContainerHeight;
		}
		return result;
	}

	getSearchConstantsHeight(): number {
		if (this.isSearchEditorFixedAndShowing()) {
			return $('#search-editor-toolbar').height() + $('#search-editor-resize-handle').height();
		} else {
			return 0;
		}
	}

	onResizeSearch(event: ResizeEvent): void {
		if (event.rectangle.height) {
			this.sideviewmenuPreference.content.sideviewSearchAttributesContainerHeight = event.rectangle.height - this.getSearchConstantsHeight();
		}
	}

	onResizeEndSearch(event: ResizeEvent): void {
		if (event.rectangle.height) {
			this.sideviewmenuPreference.content.sideviewSearchAttributesContainerHeight = event.rectangle.height - this.getSearchConstantsHeight();
		}
		this.saveSideviewmenuPreference();
		if (this.isFirefox()) {
			this.relayoutWorkaround();
		}
	}

	sidebarWidthChanged(): void {
		this.saveSideviewmenuPreference();
	}

	@HostListener('window:keydown', ['$event'])
	handleKeyboardEvent(event) {

		// don't navigate if focus is outside sidebar
		if ($(event.target).closest('nuc-sidebar').length === 0) {
			return;
		}

		let selectedEo = this.eoResultService.getSelectedEo();
		if (selectedEo) {
			// TODO check object identity - indexOf should work
			// let boIndex = this.bos.indexOf(this.selectedBo);
			let entityObjects = this.eoResultService.getResults();
			let boIndex = entityObjects
				.map(eo => eo.getId())
				.indexOf(selectedEo.getId());
			let boId = selectedEo.getId();
			if (boId && boIndex !== -1) {
				let nextEO: IEntityObject | undefined = undefined;
				if (event.key === 'ArrowUp') {
					if (boIndex > 0) {
						nextEO = entityObjects[boIndex - 1];
					}
				} else if (event.key === 'ArrowDown') {
					if (boIndex + 1 < entityObjects.length) {
						nextEO = entityObjects[boIndex + 1];
					}
				}

				if (nextEO) {
					this.gridOptions!.api!.forEachNode(rowNode => {
						if (rowNode.id!.toString() === nextEO!.getId()!.toString()) {
							rowNode.setSelected(true);
							this.lastKeyboardNavigationTime = new Date().getTime();
						}
						if (rowNode.id!.toString() === boId!.toString()) {
							rowNode.setSelected(false);
							this.gridOptions!.api!.refreshView();
							this.lastKeyboardNavigationTime = new Date().getTime();
						}
					});
				}
			}
		}
	}

	selectEo(entityClassId: string, eoId: string) {
		this.eoNavigationService.navigateToEoById(entityClassId, eoId);
	}

	set width(width: string) {
		this.sidebar.nativeElement.style.width = width;
	}

	get width() {
		return this.sidebar.nativeElement.style.width;
	}

	scrollLeft() {
		$(this.listContainerElement.nativeElement).animate({
			scrollLeft: 0
		}, 500);
	}

	private saveSideviewmenuPreference(): void {
		this.sideviewmenuService.saveSideviewmenuPreference(this.sideviewmenuPreference).subscribe(
			(preferenceItem) => {
				if (preferenceItem) {
					// preference was just created
					this.sideviewmenuPreference = preferenceItem;
				}
			}
		);
	}

	autoSizeColumnsForCardLayout() {
		if (this.sidebarLayoutType === 'card') {
			this.gridOptions!.columnApi!.autoSizeAllColumns('autosizeColumns');
		}
	}

	/**
	 * FIXME: Does not seem to work anymore.
	 * TODO: Move to GridComponent.
	 *
	 * if columns in ag-grid don't use the complete width of the grid then there is a white gap on the right side
	 * which is not wanted if the row background color is anything but white
	 */
	private fixRowBackgroundColor() {
		// it would be desirable to implement this via CSS
		(<any>document['styleSheets'][0]).addRule(
			'.ag-row::after',
			'width: ' + $('#sidebar-grid ag-grid-angular').width() + 'px!important'
		);
	}

	/**
	 * Workaround for Firefox and wrong height calculation after showing the search editor
	 */
	private relayoutWorkaround() {
		let sidebarGrid = $('#sidebar-grid');
		sidebarGrid.css('flex', '');
		setTimeout(() => {
			sidebarGrid.css('flex', '1');
		});
	}

	private isFirefox(): boolean {
		if (navigator.userAgent.indexOf('Firefox') !== -1 ) {
			return true;
		}
		return false;
	}
}
