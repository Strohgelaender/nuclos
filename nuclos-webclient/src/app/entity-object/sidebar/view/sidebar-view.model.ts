import { EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../../entity-object-data/shared/data.service';
import { EntityObject } from '../../../entity-object-data/shared/entity-object.class';
import { FqnService } from '../../../shared/fqn.service';
import { NuclosConfigService } from '../../../shared/nuclos-config.service';

/**
 * prepare view only if needed for performance reasons
 */
export class SidebarViewItem {

	id;
	nuclosStateIconUrl: string | undefined;
	entityObject: EntityObject;
	nuclosConfigService: NuclosConfigService;
	rowcolor: string | undefined;
	rowclass: string | undefined;

	constructor(
		entityObject: EntityObject,
		nuclosConfigService: NuclosConfigService
	) {
		this.entityObject = entityObject;
		this.nuclosConfigService = nuclosConfigService;
		this.id = this.getId();
	}

	build(): SidebarViewItem {
		this.buildViewAttributes(this.entityObject);
		return this;
	}

	protected buildViewAttributes(entityObject: EntityObject) {
		for (let attrKey of Object.keys(entityObject.getAttributes())) {
			let value = entityObject.getAttribute(attrKey);
			if (attrKey === 'nuclosState' && entityObject.getState() && entityObject.getState()!.nuclosStateId) {
				this.nuclosStateIconUrl = this.nuclosConfigService.getRestHost() + '/resources/stateIcons/' + entityObject.getState()!.nuclosStateId;
			}
			if (value && value.name !== undefined) {
				value = value.name;
			}
			let attrFqnKey = entityObject.getEntityClassId() + '_' + attrKey;
			this[attrFqnKey] = value;

			this.rowcolor = entityObject.getRowColor();

			if (entityObject.isDirty()) {
				this.rowclass = 'dirty-eo'
			}
			if (entityObject.isNew()) {
				this.rowclass = 'new-eo'
			}
		}
	}

	public getId() {
		return this.entityObject.getId();
	}

	public getRowColor() {
		return this.rowcolor;
	}
}


export class SidebarCardLayoutViewItem extends SidebarViewItem {

	title: string;
	info: string;

	constructor(entityObject: EntityObject,
				nuclosConfigService: NuclosConfigService,
				private fqnService: FqnService,
				private dataService: DataService) {
		super(entityObject, nuclosConfigService);
	}

	protected buildViewAttributes(eo: EntityObject) {
		this.entityObject.getMeta().subscribe(meta => {
			if (meta) {
				this.title = this.formatTitleOrInfo(meta.getTitlePattern(), eo, meta);
				this.info = this.formatTitleOrInfo(meta.getInfoPattern(), eo, meta);
			}
		});
	}

	private formatTitleOrInfo(inputString: string | undefined, eo: EntityObject, meta: EntityMeta): string {

		if (inputString === undefined) {
			return '';
		}

		return this.fqnService.formatFqnString(inputString, (fqn) => {
			let attribute = eo.getAttribute(fqn);
			let attributeMeta = meta.getAttributeMetaByFqn(fqn);
			if (attribute && attributeMeta) {
				return this.dataService.formatAttribute(attribute, attributeMeta);
			}
			return '';
		});
	}

}
