import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailToolbarComponent } from './detail-toolbar.component';

xdescribe('DetailToolbarComponent', () => {
	let component: DetailToolbarComponent;
	let fixture: ComponentFixture<DetailToolbarComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetailToolbarComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailToolbarComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
