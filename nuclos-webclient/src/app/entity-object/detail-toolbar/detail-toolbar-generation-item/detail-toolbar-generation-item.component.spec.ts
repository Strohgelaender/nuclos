import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailToolbarGenerationItemComponent } from './detail-toolbar-generation-item.component';

xdescribe('DetailToolbarGenerationItemComponent', () => {
	let component: DetailToolbarGenerationItemComponent;
	let fixture: ComponentFixture<DetailToolbarGenerationItemComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetailToolbarGenerationItemComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailToolbarGenerationItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
