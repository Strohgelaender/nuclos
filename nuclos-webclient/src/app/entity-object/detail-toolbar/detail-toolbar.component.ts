import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';

@Component({
	selector: 'nuc-detail-toolbar',
	templateUrl: './detail-toolbar.component.html',
	styleUrls: ['./detail-toolbar.component.css']
})
export class DetailToolbarComponent implements OnInit {

	@Input() eo: EntityObject;
	@Input() canCreateBo: boolean;
	@Input() triggerUnsavedChangesPopover: Date;

	constructor() {
	}

	ngOnInit() {
	}

}
