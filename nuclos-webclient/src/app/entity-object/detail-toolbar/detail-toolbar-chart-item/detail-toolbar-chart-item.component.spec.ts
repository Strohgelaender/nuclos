import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailToolbarChartItemComponent } from './detail-toolbar-chart-item.component';

xdescribe('DetailToolbarChartItemComponent', () => {
	let component: DetailToolbarChartItemComponent;
	let fixture: ComponentFixture<DetailToolbarChartItemComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetailToolbarChartItemComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailToolbarChartItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
