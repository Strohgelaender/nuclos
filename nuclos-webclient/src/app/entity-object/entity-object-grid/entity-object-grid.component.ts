import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { GridOptions } from 'ag-grid';
import { Observable } from 'rxjs';
import { SortModel } from '../../entity-object-data/shared/sort.model';
import {
	BooleanRendererComponent,
	DateRendererComponent,
	NumberRendererComponent,
	StateIconRendererComponent
} from '../../grid/grid/cell-renderer';
import { NuclosCellEditorParams, NuclosCellRendererParams } from '../../layout/web-subform/web-subform.model';
import { SidebarViewItem } from '../sidebar/view/sidebar-view.model';
import { ReferenceRendererComponent } from './cell-renderer';
import { EntityObjectGridColumn } from './entity-object-grid-column';
import { EntityObjectGridService } from './entity-object-grid.service';

/**
 * A Grid component for EntityObject data that handles column preferences and NuclosRowColor.
 *
 * TODO: Init columns
 * TODO: Init sorting
 * TODO: Handle NuclosRowColor
 */
@Component({
	selector: 'nuc-entity-object-grid',
	templateUrl: './entity-object-grid.component.html',
	styleUrls: ['./entity-object-grid.component.css']
})
export class EntityObjectGridComponent implements OnInit, OnChanges {
	@Input() columns: EntityObjectGridColumn[];
	@Input() sortModel: SortModel;

	// TODO: Should not be used externally
	@Input() gridOptions: GridOptions = <GridOptions>{};

	@Output() rowSelected = new EventEmitter();
	@Output() gridReady = new EventEmitter();
	@Output() rowClicked = new EventEmitter();
	@Output() modelUpdated = new EventEmitter();

	constructor(
		private eoGridService: EntityObjectGridService,
	) {
	}

	ngOnInit() {
		this.initGrid().subscribe(
			() => this.applyColumns()
		);
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes['sortModel']) {
			if (this.gridOptions.api && this.sortModel) {
				this.gridOptions.api.setSortModel(this.sortModel.getColumns());
			}
		}

		if (changes['columns']) {
			this.applyColumns();
		}
	}

	private applyColumns() {
		if (!this.gridOptions.api || !this.columns) {
			return;
		}

		this.columns.forEach(column => {
			let attributeMeta = column.attributeMeta;
			if (attributeMeta) {
				column.headerClass = this.eoGridService.getTextAlignmentClass(attributeMeta);
				column.cellClass = this.eoGridService.getTextAlignmentClass(attributeMeta);

				// provide data for render component
				column.cellRendererParams = {
					nuclosCellRenderParams: {
						editable: false,
						attrMeta: attributeMeta,
					} as NuclosCellRendererParams
				};

				// provide data for editor component
				column.cellEditorParams = {
					attrMeta: attributeMeta
				} as NuclosCellEditorParams;

				if (attributeMeta.isBoolean()) {
					column.cellRendererFramework = BooleanRendererComponent;
				} else if (attributeMeta.isDate() || attributeMeta.isTimestamp()) {
					column.cellRendererFramework = DateRendererComponent;
				} else if (attributeMeta.isNumber()) {
					column.cellRendererFramework = NumberRendererComponent;
				} else if (attributeMeta.isStateIcon()) {
					column.cellRendererFramework = StateIconRendererComponent;
				} else if (attributeMeta.isReference()) {
					column.cellRendererFramework = ReferenceRendererComponent;
				}
			}
		});

		this.gridOptions.api.setColumnDefs(this.columns);
	}

	private initGrid(): Observable<any> {
		this.gridOptions.enableColResize = true;
		this.gridOptions.enableSorting = true;
		this.gridOptions.cacheBlockSize = 100;

		this.gridOptions.columnDefs = [{}];

		this.gridOptions.getRowNodeId = (item) => {
			return item.getId();
		};

		this.gridOptions.rowSelection = 'single';

		// NuclosRowColor
		this.gridOptions.getRowClass = (params: { data: SidebarViewItem }) => {
			if (params.data) {
				return params.data.rowclass || '';
			}
			return '';
		};
		this.gridOptions.getRowStyle = (params: { data: SidebarViewItem }) => {
			if (params.data) {
				let rowColor = params.data.getRowColor();
				return rowColor !== undefined ? {
					'background-color': rowColor
				} : undefined;
			}
			return undefined;
		};

		return new Observable(observer => {
			if (this.gridOptions) {
				this.gridOptions.onGridReady = () => {
					observer.next();
					observer.complete();
				};
			} else {
				observer.complete();
			}
		});
	}
}
