export interface EntityObjectRouteParams {
	entityClassId: string;
	entityObjectId?: string;
	search?: string;
}
