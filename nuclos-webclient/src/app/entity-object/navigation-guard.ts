import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EntityObjectResultService } from '../entity-object-data/shared/entity-object-result.service';
import { EntityObject } from '../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../entity-object-data/shared/entity-object.service';
import { EntityObjectComponent } from './entity-object.component';

export class NavigationGuard implements CanDeactivate<EntityObjectComponent> {
	constructor(
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService
	) {
	}

	canDeactivate(
		component: EntityObjectComponent,
		route: ActivatedRouteSnapshot,
		_state: RouterStateSnapshot
	): Observable<boolean>
		| Promise<boolean>
		| boolean {
		let result = true;
		let selectedEO = this.eoResultService.getSelectedEo() as EntityObject;

		if (selectedEO) {
			// allow navigation if new eo was not modified
			if (selectedEO.isNew() && !selectedEO.isDirty()) {
				// TODO: This class is a guard. It should not actively interfere with EO services.
				this.eoService.delete(selectedEO).subscribe(
					() => {
						this.eoResultService.selectEo(undefined);
					}
				);
			}
		}

		if (!selectedEO) {
			result = true;
		} else if (selectedEO.isNew() && route.params['entityObjectId'] !== 'new') {
			result = true;
		} else if (selectedEO.isDirty()) {
			component.warnAboutUnsavedChanges();
			result = false;
		}

		return result;
	}
}
