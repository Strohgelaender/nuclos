import { RouterModule, Routes } from '@angular/router';
import { EntityObjectComponent, EntityObjectPopupComponent } from './entity-object.component';
import { LegacyRouteComponent } from './legacy-route/legacy-route.component';
import { NavigationGuard } from './navigation-guard';

export const ROUTE_CONFIG: Routes = [
	// TODO: Navigation from 'view/:entityClassId/:entityObjectId' to 'view/:entityClassId'
	// and vice-versa instantiates a new EntityObjectComponent, which causes additional
	// EO list queries to the server. Make sure only one component instance is created.
	// TODO: Define auxiliary routes for the sidebar (EntityObjectComponent)
	{
		path: 'view/:entityClassId/:entityObjectId',
		component: EntityObjectComponent,
		canDeactivate: [
			NavigationGuard
		]
	},
	{
		path: 'view/:entityClassId',
		component: EntityObjectComponent
	},
	{
		path: 'view/:entityClassId/search/:query',
		component: EntityObjectComponent,
		canDeactivate: [
			NavigationGuard
		]
	},
	{
		path: 'popup/:entityClassId/:entityObjectId',
		component: EntityObjectPopupComponent,
		canDeactivate: [
			NavigationGuard
		]
	},

	// For backwards compatibility to Webclient1 routes,
	// which might still be saved in location cookies:
	{
		path: 'sideview/:entityClassId/:entityObjectId',
		component: LegacyRouteComponent
	},
	{
		path: 'sideview/:entityClassId',
		component: LegacyRouteComponent
	},
	{
		path: 'sideview/:entityClassId/search/:query',
		component: LegacyRouteComponent
	},
];

export const EntityObjectRoutes = RouterModule.forChild(ROUTE_CONFIG);
