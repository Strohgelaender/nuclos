import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { Logger } from '../../log/shared/logger';
import { FqnService } from '../../shared/fqn.service';
import { StringUtils } from '../../shared/string-utils';
import { TaskService } from '../../task/shared/task.service';

@Component({
	selector: 'nuc-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, OnDestroy {
	@Input()
	meta: EntityMeta;

	@Input()
	boId: number;

	@Input()
	eo: EntityObject | undefined;

	@Input()
	canCreateBo: boolean;

	@Input()
	triggerUnsavedChangesPopover: Date;

	private subscriptions: Subscription[] = [];

	constructor(
		private eoEventService: EntityObjectEventService,
		private eoService: EntityObjectService,
		private $log: Logger,
		private taskService: TaskService,
		private fqnService: FqnService,
	) {
	}

	ngOnInit() {
		this.eoEventService.observeSelectedEo().subscribe(
			eo => this.setEo(eo as EntityObject)
		);
	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

	private setEo(eo: EntityObject | undefined) {
		// TODO: There are more kinds of dynamic entities (dynamic BOs, virtual BOs), not only dynamic task lists - handle this more generic
		if (eo && eo.isDynamicTaskList()) {
			this.taskService.getTaskListDefinition(eo.getEntityClassId()).subscribe( taskListMeta => {
				let realEntity = this.getRealEntity(taskListMeta, eo);

				if (!realEntity) {
					this.$log.warn('%o is a dynamic task list and the real entity could not be determined -> Could not load the real entity object!');
					return;
				}

				let realId = eo.getId();
				if (realEntity && realId) {
					this.$log.info(
						'Loading real EO %o:%o for dynamic EO %o:%o...',
						realEntity,
						realId,
						eo.getEntityClassId(),
						eo.getId()
					);

					this.eoService.loadEO(realEntity, realId).subscribe(
						realEo => this.setEo(realEo)
					);
					return;
				}
			});
		}

		this.eo = eo;
	}

	private getRealEntity(taskListMeta: EntityMeta, eo: EntityObject) {
		let dynamicEntityFieldName = taskListMeta.getDynamicEntityFieldName();
		let realEntity;

		if (dynamicEntityFieldName) {
			dynamicEntityFieldName = StringUtils.replaceAll(dynamicEntityFieldName, '_', '');
			realEntity = eo.getAttribute(dynamicEntityFieldName);
		}

		if (!realEntity) {
			realEntity = taskListMeta.getTaskEntity();
		}

		return realEntity;
	}
}
