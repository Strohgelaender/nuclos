import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { SystemParameter } from '../shared/system-parameters';
import { MenuService } from './menu.service';
import { UserAction } from '@nuclos/nuclos-addon-api';

@Component({
	selector: 'nuc-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

	menuStructure;
	username: string | undefined;
	loggedIn = false;
	private isDevMode = false;

	constructor(
		private menuService: MenuService,
		private nuclosConfig: NuclosConfigService,
		private authenticationService: AuthenticationService
	) {
		this.authenticationService.observeLoginStatus().subscribe(
			loggedIn => {
				this.loggedIn = loggedIn;
				if (this.loggedIn) {
					this.updateMenu();
					this.authenticationService.getMandatorSelection().subscribe(
						() => this.updateMenu()
					);

					this.username = this.authenticationService.getUsername();
				} else {
					this.menuStructure = undefined;
					this.username = undefined;
				}
			}
		);

		this.nuclosConfig.getSystemParameters().subscribe(
			params => {
				this.isDevMode = params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT);
			}
		);

	}

	isAnonymous() {
		return this.authenticationService.isAnonymous();
	}

	isSuperUser() {
		return this.authenticationService.isSuperUser();
	}

	canSharePreferences() {
		return this.authenticationService.isActionAllowed(UserAction.SharePreferences);
	}

	ngOnInit() {
		let user = this.authenticationService.getCurrentUser();
		if (user) {
			this.username = user.username;
		}
	}

	private updateMenu(): void {
		this.menuService.getMenuStructure().subscribe(
			menuStructure => {
				this.menuStructure = menuStructure;
			}
		);
	}
}
