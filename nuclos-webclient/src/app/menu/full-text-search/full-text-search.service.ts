import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { SystemParameter } from '../../shared/system-parameters';
import { FullTextSearchResultItem } from './full-text-search.model';

@Injectable()
export class FullTextSearchService {

	private searchUrl: string;

	constructor(
		private nuclosConfigService: NuclosConfigService,
		private http: NuclosHttpService
	) {
		this.searchUrl = nuclosConfigService.getRestHost() + '/data/search/{text}';
	}

	isFullTextSearchEnabled(): Observable<boolean> {
		return this.nuclosConfigService.getSystemParameters().pipe(map(
			params => params.is(SystemParameter.FULLTEXT_SEARCH_ENABLED)));
	}

	search(searchtext: string): Observable<FullTextSearchResultItem[]> {
		let fullTextSearchUrl = this.searchUrl.replace('{text}', searchtext);
		return this.http.get(fullTextSearchUrl).pipe(
			map(response => response.json()),
			map(result => {
				result.forEach(
					(resultItem: FullTextSearchResultItem) => {
						resultItem.url = '#/view/' + resultItem.uid + '/' + resultItem.pk;
					}
				);
				return result;
			}),
		);
	}
}
