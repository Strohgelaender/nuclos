import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { MenuItem } from './shared/menu.model';

@Injectable()
export class MenuService {

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService
	) {
	}

	getMenuStructure(): Observable<MenuItem> {

		return this.http
			.get(this.nuclosConfig.getRestHost() + '/meta/menustructure').pipe(
			map((response: Response) => response.json()));
	}
}
