import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PreferenceTypeName } from '@nuclos/nuclos-addon-api';
import { EntityObjectResultService } from '../../../entity-object-data/shared/entity-object-result.service';
import { NuclosI18nService } from '../../../i18n/shared/nuclos-i18n.service';
import { PreferencesService } from '../../../preferences/preferences.service';

@Component({
	selector: 'nuc-preferences-reset-modal',
	templateUrl: './preferences-reset-modal.component.html',
	styleUrls: ['./preferences-reset-modal.component.css']
})
export class PreferencesResetModalComponent implements OnInit {

	static readonly RESET_TYPES: PreferenceTypeName[] = ['table', 'subform-table', 'searchtemplate', 'perspective'];

	showResetPreferencesForEoButton = false;

	constructor(private activeModal: NgbActiveModal,
				private i18n: NuclosI18nService,
				private preferencesService: PreferencesService,
				private entityObjectResultService: EntityObjectResultService) {
	}

	ngOnInit() {
		// show "reset prefs for eo button" if there are any customized prefs for this eo class
		let eoClassId = this.entityObjectResultService.getSelectedEntityClassId();
		if (eoClassId !== undefined) {
			this.preferencesService.getPreferences(
				{
					type: PreferencesResetModalComponent.RESET_TYPES,
					boMetaId: eoClassId
				}
			).subscribe(prefs => {
				this.showResetPreferencesForEoButton = prefs.filter(pref => pref.customized).length > 0;
			});
		}
	}

	getResetPreferencesForEoButtonLabel() {
		let eoClassId = this.entityObjectResultService.getSelectedEntityClassId();
		let eoName = eoClassId ? eoClassId.substring(eoClassId.lastIndexOf('_') + 1) : ' - ';
		return this.i18n.getI18n('webclient.preferences.resetModal.resetPreferencesForEo', eoName);
	}

	resetPrefsForCurrentEntityClass() {
		this.resetPreferences(this.entityObjectResultService.getSelectedEntityClassId());
	}

	resetAllPrefs() {
		this.resetPreferences();
	}

	closeModal() {
		this.activeModal.dismiss();
	}

	private resetPreferences(eoMeataId?: string) {
		this.preferencesService.resetCustomizedPreferenceItems(PreferencesResetModalComponent.RESET_TYPES, eoMeataId).subscribe(() => {
			window.location.reload();
		});
	}
}
