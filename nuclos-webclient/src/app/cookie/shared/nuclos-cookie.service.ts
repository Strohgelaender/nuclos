import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { StringUtils } from '../../shared/string-utils';

@Injectable()
export class NuclosCookieService {
	private cookieService: CookieService;

	constructor(
		@Inject(DOCUMENT) private document
	) {
		this.cookieService = new CookieService(document);
	}

	get(key: string) {
		return this.cookieService.get(key);
	}

	getObject(key: string) {
		return JSON.parse(
			this.get(key)
		);
	}

	put(key: string, value: string, expires?: Date) {
		this.cookieService.set(
			key,
			value,
			expires,
			this.getPathDirectory()
		);
	}

	putObject(key: string, value: object) {
		return this.put(
			key,
			JSON.stringify(value)
		);
	}

	remove(key: string) {
		this.cookieService.delete(key, this.getPathDirectory());
	}

	private getPathDirectory() {
		let path = window.location.pathname;
		let dir = path.substring(0, path.lastIndexOf('/')) + '/';

		dir = StringUtils.regexReplaceAll(dir, '/{2,}', '/');

		return dir;
	}
}
