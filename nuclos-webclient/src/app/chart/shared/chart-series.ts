export interface ChartSeries {
	boAttrId: string;
	color?: string;
	bar?: string;
	label: {
		de: string
	}
}
