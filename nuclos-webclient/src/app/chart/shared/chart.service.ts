import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { AuthenticationService } from '../../authentication';
import { EntityAttrMeta, EntityMeta, LovEntry } from '../../entity-object-data/shared/bo-view.model';
import { EntityObjectDependents } from '../../entity-object-data/shared/entity-object-dependents';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { MetaService } from '../../entity-object-data/shared/meta.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { ChartPreferenceContent, Preference, PreferenceType } from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { FqnService } from '../../shared/fqn.service';
import { ObjectUtils } from '../../shared/object-utils';
import { ChartOptions } from './chart-options';
import { ChartSeries } from './chart-series';
import { EoChartWrapper } from './eo-chart-wrapper';
import { SearchTemplate } from './search-template';
import { UserAction } from '@nuclos/nuclos-addon-api';

@Injectable()
export class ChartService {

	static chartTypes = [
		'multiBarChart',
		'multiBarHorizontalChart',
		'lineChart',
		'stackedAreaChart',
		'scatterChart',
		'pieChart',
		'linePlusBarChart'
	];

	private defaultChartConfig: ChartPreferenceContent = {
		chart: {
			type: ChartService.chartTypes[0],
			primaryChart: {
				boMetaId: '',
				refBoAttrId: '',
				categoryBoAttrId: '',
				series: [],
			}
		},
		name: {
			de: ''
		}
	};

	private showConfig = false;

	constructor(
		private preferenceService: PreferencesService,
		private authenticationService: AuthenticationService,
		private i18nService: NuclosI18nService,
		private $log: Logger
	) {
	}

	getCharts(eo: EntityObject): Observable<Preference<ChartPreferenceContent>[]> {
		return this.preferenceService.getPreferences(
			{
				type: [PreferenceType.chart.name],
				boMetaId: eo.getEntityClassId()
			}
		) as Observable<Preference<ChartPreferenceContent>[]>;
	}

	newChart(eo: EntityObject): EoChartWrapper {
		let chart = new Preference<ChartPreferenceContent>('chart', eo.getEntityClassId());
		chart.name = this.i18nService.getI18n('webclient.charts.newChart');
		chart.dirty = true;
		chart.content = ObjectUtils.cloneDeep(this.defaultChartConfig);
		chart.content.chart.primaryChart.boMetaId = eo.getEntityClassId();
		return new EoChartWrapper(eo, chart);
	}

	saveChart(eoChart: EoChartWrapper) {
		return this.preferenceService.savePreferenceItem(eoChart.chartPreference);
	}

	canConfigureCharts() {
		return this.authenticationService.isActionAllowed(UserAction.ConfigureCharts);
	}

	deleteChart(chart: Preference<ChartPreferenceContent>) {
		return this.preferenceService.deletePreferenceItem(chart);
	}

	prepareChartOptions(chartOptions: ChartPreferenceContent): any {
		chartOptions.chart.x = this.evalOrDefault(
			chartOptions.chart.x,
			(dataSet: any) => dataSet.categoryValue
		);

		chartOptions.chart.y = this.evalOrDefault(
			chartOptions.chart.y,
			(dataSet: any) => dataSet.seriesValue
		);

		if (!chartOptions.chart.yAxis) {
			chartOptions.chart.yAxis = {};
		}
		chartOptions.chart.yAxis.tickFormat = this.evalOrDefault(
			chartOptions.chart.yAxis.tickFormat,
			value => value
		);

		if (!chartOptions.chart.xAxis) {
			chartOptions.chart.xAxis = {};
		}
		chartOptions.chart.xAxis.tickFormat = this.evalOrDefault(
			chartOptions.chart.xAxis.tickFormat,
			value => value
		);
	}

	/**
	 * Tries to eval the given function definition,
	 * or returns the default function.
	 */
	private evalOrDefault(
		functionDefinition: string | Function | undefined,
		defaultFunction: Function
	) {
		let result = functionDefinition || defaultFunction;

		if (typeof functionDefinition === 'string') {
			try {
				// tslint:disable-next-line
				result = eval('var f = (' + functionDefinition + '); f;');
			} catch (e) {
				this.$log.warn(e);
			}
		}

		return result;
	}

	isValidChartPreference(chart: ChartPreferenceContent) {
		return chart && this.isValidChartOptions(chart.chart);
	}

	isValidChartOptions(chart: ChartOptions) {
		return chart
			&& chart.primaryChart
			&& chart.primaryChart.boMetaId
			&& chart.primaryChart.categoryBoAttrId
			&& chart.primaryChart.refBoAttrId
			&& this.containsValidSeries(chart.primaryChart.series);
	}

	private containsValidSeries(series?: ChartSeries[]) {
		if (series) {
			for (let serie of series) {
				if (this.isValidSeries(serie)) {
					return true;
				}
			}
		}
		return false;
	}

	isValidSeries(series: ChartSeries) {
		return series && series.boAttrId;
	}

	findReferenceAttributes(eo: EntityObject | undefined) {
		let result: { id: string, name: string }[] = [];

		if (eo) {
			let subEoInfos = eo.getSubEoInfos();
			if (subEoInfos) {
				let keys = subEoInfos.keys();
				let next = keys.next();
				while (true) {
					let referenceAttributeId = next.value;
					if (referenceAttributeId) {
						result.push({
							id: referenceAttributeId,
							// TODO: Get the name
							name: referenceAttributeId
						});
					}
					if (next.done) {
						break;
					}
					next = keys.next();
				}
			}
		}

		return result;
	}

	getSubEntityMeta(
		eo: EntityObject,
		referenceAttributeId: string
	): Observable<EntityMeta> {
		let href = this.getSubEntityMetaHref(eo, referenceAttributeId);

		if (!href) {
			return observableThrowError('Could not determine EntityMeta-URL for reference ' + referenceAttributeId);
		}

		// TODO: MetaService should be refactored into separate module and injected here
		return MetaService.instance.getSubBoMeta(href);
	}

	private getSubEntityMetaHref(
		eo: EntityObject | undefined,
		referenceAttributeId: string
	) {
		if (eo) {
			let subEoInfos = eo.getSubEoInfos();
			if (subEoInfos) {
				let info = subEoInfos.get(referenceAttributeId);
				if (info) {
					return info.links.boMeta.href;
				}
			}
		}

		return undefined;
	}

	isShowConfig() {
		return this.showConfig;
	}

	toggleConfig() {
		this.showConfig = !this.showConfig;
	}

	search(
		eoChart: EoChartWrapper,
	) {
		if (!this.isValidChartOptions(eoChart.chartOptions)) {
			return;
		}

		let entityClassId = eoChart.chartOptions.primaryChart.boMetaId;
		let searchCondition = this.buildSearchCondition(
			entityClassId,
			eoChart.searchItems
		);

		let sortString = this.toSortString(eoChart.chartOptions);

		(eoChart.eo.getDependents(eoChart.chartOptions.primaryChart.refBoAttrId) as EntityObjectDependents)
			.fetchBySearchConfig({
				searchCondition: searchCondition,
				sort: sortString
			})
			.subscribe(
				subEos => eoChart.setChartData(subEos)
			);
	}

	private toSortString(chart: ChartOptions) {
		let result;

		let sort = chart.primaryChart.sort;
		let sortBy;
		let sortDirection = 'asc';
		if (sort !== undefined && sort.sortByBoAttrId) {
			sortBy = sort.sortByBoAttrId;
		} else {
			// sort by category
			sortBy = chart.primaryChart.categoryBoAttrId;
		}

		if (sort && sort.sortDirection) {
			sortDirection = sort.sortDirection;
		}

		if (sortBy) {
			result = sortBy + ' ' + sortDirection;
		}

		return result;
	}

	private buildSearchCondition(
		entityClassId: string,
		searchItems: SearchTemplate[]
	): string | undefined {
		if (!searchItems) {
			return;
		}

		// build something like: 'CompositeCondition:AND:[Comparison:EQUAL:categoryName:Hardware,Comparison:GTE:orderMonth:2015.01]';
		let conditions: string[] = [];
		for (let item of searchItems) {
			if (!item.value && item.mandatory) {
				this.$log.warn('Mandatory search filter item is not set: ' + item.boAttrId);
				return;
			}

			if (item.value) {
				let value = item.value.name ? item.value.name : item.value;
				if (value) {
					// TODO use FQN
					let fieldName = FqnService.getShortAttributeNameFailsafe(entityClassId, item.boAttrId);
					if (item.inputType === 'datepicker') {
						value = moment(value).format('YYYY-MM-DD');
					}
					let condition = 'Comparison:' + item.comparisonOperator + ':' + fieldName + ':' + value;
					conditions.push(condition);
				}
			}
		}

		let searchCondition;
		if (conditions.length > 0) {
			searchCondition = 'CompositeCondition:AND:[' + conditions.join(',') + ']';
		}

		return searchCondition;
	}

	buildSearchItems(eoChart: EoChartWrapper) {
		eoChart.searchItems = [];

		if (!eoChart.chartOptions.searchTemplate || !this.isValidChartOptions(eoChart.chartOptions)) {
			return;
		}

		for (let searchTemplateItem of eoChart.chartOptions.searchTemplate) {
			let item = ObjectUtils.cloneDeep(searchTemplateItem);
			// let attributeName = FqnService.getShortAttributeNameFailsafe(this.chart.primaryChart.boMetaId, item.boAttrId);
			MetaService.instance.getEntityMeta(eoChart.chartOptions.primaryChart.boMetaId).subscribe(entityMeta => {
				let attributeMeta = entityMeta.getAttributeMetaByFqn(item.boAttrId);
				if (item.dropdown) {
					item.inputType = 'dropdown';

					// TODO: Use a proper LOV component with caching of results and filtering
					if (attributeMeta) {
						this.getDropdownOptions(
							eoChart,
							attributeMeta
						).subscribe(
							result => item.suggestions = result
						);
					} else {
						this.$log.warn('No attribute meta for %o', item.boAttrId);
					}

				} else {
					if (attributeMeta) {
						if (attributeMeta.getType() === 'Timestamp' || attributeMeta.getType() === 'Date') {
							item.inputType = 'datepicker';
						} else {
							item.inputType = 'string';
						}
					}
				}
				eoChart.searchItems.push(item);
			});
		}
	}

	private getDropdownOptions(
		eoChart: EoChartWrapper,
		attributeMeta: EntityAttrMeta
	): Observable<LovEntry[]> {
		return (eoChart.getEoDependents() as EntityObjectDependents)
			.fetchValueList(attributeMeta.getAttributeID());
	}

	exportAsCsv(eoChart: EoChartWrapper) {
		let preferenceContent = eoChart.chartPreference.content;

		let columnSeparator = ';';
		let lineSeparator = '\n';
		let fileName = preferenceContent.name.de + '.csv';

		let subEntityClassId = eoChart.chartOptions.primaryChart.boMetaId;

		// TODO: Refactor meta to separate module and inject it as a service here
		MetaService.instance.getEntityMeta(subEntityClassId).subscribe(meta => {
			let csvExport: string[] = [];

			let columnAttributes: string[] = [];
			let categoryAttributeName = FqnService.getShortAttributeNameFailsafe(
				eoChart.chartOptions.primaryChart.boMetaId,
				preferenceContent.chart.primaryChart.categoryBoAttrId
			);
			columnAttributes.push(categoryAttributeName);
			for (let serie of preferenceContent.chart.primaryChart.series) {
				let seriesAttributeName = FqnService.getShortAttributeNameFailsafe(preferenceContent.chart.primaryChart.boMetaId, serie.boAttrId);
				columnAttributes.push(seriesAttributeName);
			}

			let csvHeader: string[] = [];
			for (let columnAttribute of columnAttributes) {
				let attributeMeta = meta.getAttributeMetaByFqn(columnAttribute);
				let columnAttributeName = attributeMeta && attributeMeta.getName() || columnAttribute;
				csvHeader.push(columnAttributeName);
			}

			csvExport.push(csvHeader.join(columnSeparator) + lineSeparator);

			let subEos = eoChart.getCurrentChartData();
			if (subEos) {
				for (let subEo of subEos) {
					let csvLine: string[] = [];
					for (let columnAttribute of columnAttributes) {
						let value = '' + subEo.getAttribute(columnAttribute);
						value = value.replace('.', this.i18nService.getCurrentLocale().decimalSeparator);
						csvLine.push(value);
					}

					csvExport.push(csvLine.join(columnSeparator) + lineSeparator);
				}
			}

			let blob = new Blob(csvExport, {type: 'text/plain;charset=utf-8'});
			saveAs(blob, fileName);
		});
	}
}
