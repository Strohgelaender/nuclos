import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { NvD3Module } from 'ng2-nvd3';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { ModalModule } from '../popup/modal/modal.module';
import { UiComponentsModule } from '../ui-components/ui-components.module';
import { ChartButtonComponent } from './chart-button/chart-button.component';
import { ChartContainerComponent } from './chart-container/chart-container.component';
import { ChartFilterComponent } from './chart-filter/chart-filter.component';
import { ChartModalComponent } from './chart-modal/chart-modal.component';
import { ChartTabsComponent } from './chart-modal/chart-tabs/chart-tabs.component';
import { ChartViewComponent } from './chart-modal/detail/chart-view/chart-view.component';
import { ConfigComponent } from './chart-modal/detail/config/config.component';
import { ExtendedComponent } from './chart-modal/detail/config/extended/extended.component';
import { OptionsComponent } from './chart-modal/detail/config/options/options.component';
import { SeriesComponent } from './chart-modal/detail/config/options/series/series.component';
import { DetailComponent } from './chart-modal/detail/detail.component';
import { ToolbarComponent } from './chart-modal/detail/toolbar/toolbar.component';
import { CsvExportComponent } from './csv-export/csv-export.component';
import { ChartService } from './shared/chart.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		I18nModule,
		LogModule,
		ModalModule,
		UiComponentsModule,

		// TODO: Should not be necessary here
		ClickOutsideModule,

		AutoCompleteModule,
		NgbModule,
		PrettyJsonModule,
		NvD3Module,
	],
	declarations: [
		ChartButtonComponent,
		ChartContainerComponent,
		ChartFilterComponent,
		ChartModalComponent,
		ConfigComponent,
		CsvExportComponent,
		DetailComponent,
		ChartViewComponent,
		OptionsComponent,
		ToolbarComponent,
		ExtendedComponent,
		SeriesComponent,
		ChartTabsComponent,
	],
	exports: [
		ChartButtonComponent,
	],
	entryComponents: [
		ChartModalComponent,
	],
	providers: [
		ChartService,
	]
})
export class ChartModule {
}
