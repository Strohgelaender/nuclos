import { Component, Input, OnInit } from '@angular/core';
import { ChartService } from '../../../shared/chart.service';
import { EoChartWrapper } from '../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

	@Input() eoChart: EoChartWrapper
	@Input() eoCharts: EoChartWrapper[];

	constructor(
		private chartService: ChartService
	) {
	}

	ngOnInit() {
	}

	canConfigureCharts() {
		return this.chartService.canConfigureCharts();
	}

	save() {
		this.chartService.saveChart(this.eoChart).subscribe();
	}

	delete() {
		if (this.eoChart.chartPreference.prefId) {
			this.chartService.deleteChart(this.eoChart.chartPreference).subscribe(() => {
				this.removeChart()
			});
		} else {
			this.removeChart()
		}
	}

	private removeChart() {
		let index = this.eoCharts.indexOf(this.eoChart);
		if (index >= 0) {
			this.eoCharts.splice(index, 1);
		}
	}

	toggleConfig() {
		this.chartService.toggleConfig();
	}

	exportChart() {
		this.chartService.exportAsCsv(this.eoChart);
	}

	isExportAvailable() {
		let configurationValid = this.chartService.isValidChartPreference(this.eoChart.chartPreference.content);
		let dataCount = this.eoChart.getCurrentDataCount();
		return configurationValid && dataCount > 0;
	}
}
