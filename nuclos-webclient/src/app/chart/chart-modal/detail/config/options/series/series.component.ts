import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChartSeries } from '../../../../../shared/chart-series';
import { EoChartWrapper } from '../../../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-series',
	templateUrl: './series.component.html',
	styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {

	@Input() eoChart: EoChartWrapper;
	@Input() referenceAttributeId: string;
	@Input() entityAttributes: { id: string, name: string }[] = [];

	@Output() seriesChanged = new EventEmitter();

	selectedSeries: ChartSeries | undefined;

	constructor() {
	}

	ngOnInit() {
	}

	addSeries() {
		let series: ChartSeries = {
			boAttrId: '',
			label: {
				de: ''
			}
		};
		this.eoChart.chartOptions.primaryChart.series.push(series);
		this.selectedSeries = series;
	}

	setAttributeId(series: ChartSeries, attributeId: string) {
		let attribute = this.entityAttributes.find(it => it.id === attributeId);

		if (!attribute) {
			return;
		}

		series.boAttrId = attribute.id;
		series.label.de = attribute.name;

		this.seriesChanged.emit();
	}

	removeSeries(series: ChartSeries) {
		let allSeries = this.eoChart.chartOptions.primaryChart.series;
		let index = allSeries.indexOf(series);
		if (index >= 0) {
			allSeries.splice(index, 1);
			this.selectPreviousSeries(index);
			this.seriesChanged.emit();
		}
	}

	private selectPreviousSeries(index: number) {
		let allSeries = this.eoChart.chartOptions.primaryChart.series;
		if (index > 0) {
			this.selectedSeries = allSeries[index - 1];
		} else if (allSeries.length > 0) {
			this.selectedSeries = allSeries[0];
		} else {
			this.selectedSeries = undefined;
		}
	}
}
