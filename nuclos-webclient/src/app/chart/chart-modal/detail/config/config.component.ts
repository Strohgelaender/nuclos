import { Component, Input, OnInit } from '@angular/core';
import { EoChartWrapper } from '../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-config',
	templateUrl: './config.component.html',
	styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {

	@Input() eo: EoChartWrapper;


	constructor() {
	}

	ngOnInit() {
	}

}
