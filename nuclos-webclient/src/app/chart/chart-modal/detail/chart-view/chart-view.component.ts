import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { EntityAttrMeta, EntityMeta } from '../../../../entity-object-data/shared/bo-view.model';
import { SubEntityObject } from '../../../../entity-object-data/shared/entity-object.class';
import { Logger } from '../../../../log/shared/logger';
import { FqnService } from '../../../../shared/fqn.service';
import { ObjectUtils } from '../../../../shared/object-utils';
import { ChartService } from '../../../shared/chart.service';
import { EoChartWrapper } from '../../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-view',
	templateUrl: './chart-view.component.html',
	styleUrls: ['./chart-view.component.css']
})
export class ChartViewComponent implements AfterViewInit, OnInit {

	@Input() eoChart: EoChartWrapper;

	chartOptions: any;
	chartData: { key: string, values: any[] }[] = [];

	subMeta: EntityMeta;

	private subscription: Subscription;

	constructor(
		private chartService: ChartService,
		private $log: Logger
	) {
	}

	ngOnInit() {
		this.chartOptions = this.getChartOptions();
		this.getDependents();

		this.eoChart.chartOptionsChanged.subscribe(() => this.updateChartOptions());
	}

	ngAfterViewInit(): void {
		// Workaround to make nvd3 resize properly
		setTimeout(() => window.dispatchEvent(new Event('resize')));
	}

	private updateChartOptions() {
		this.chartOptions = this.getChartOptions();
		this.getDependents();
	}

	private getChartOptions() {
		if (this.isConfigurationValid()) {
			try {
				let clone = ObjectUtils.cloneDeep(this.eoChart.chartPreference.content);
				this.chartService.prepareChartOptions(clone);
				return clone;
			} catch (e) {
				this.$log.warn(e);
			}
		}
		return undefined;
	}

	private getDependents() {
		if (!this.isConfigurationValid()) {
			return;
		}

		this.updateSubMeta().subscribe(
			() => this.subscribeToDependents()
		);
	}

	private subscribeToDependents() {
		if (this.subscription) {
			this.subscription.unsubscribe();
		}

		if (this.eoChart) {
			this.subscription = this.eoChart.dataSubject.pipe(
				map(subEos => {
					let chartData: any[] = [];

					if (subEos) {
						chartData = this.toChartData(subEos);
					}

					return chartData;
				})).subscribe(
					data => {
						this.chartData = data;
						this.$log.debug('chartData = %o', data);
					}
				);

			// Initially load subform data
			if (this.chartData.length === 0) {
				this.chartService.search(this.eoChart);
			}

		} else {
			this.$log.warn('No EO to load chartData');
		}
	}

	private updateSubMeta() {
		let referenceAttributeId = this.eoChart.chartOptions.primaryChart.refBoAttrId;
		return this.chartService.getSubEntityMeta(this.eoChart.eo, referenceAttributeId).pipe(
			tap(meta => this.subMeta = meta)
		);
	}

	private toChartData(subEos: SubEntityObject[]): { key: string, values: any[] }[] {
		let result: any[] = [];
		let chartContent = this.eoChart.chartPreference.content;
		let series = chartContent.chart.primaryChart.series;
		// let scaleLabel = preferenceContent.chart.primaryChart.scaleLabel ? ' ' + preferenceContent.chart.primaryChart.scaleLabel.de.label : '';

		let subEntityClassId;
		if (subEos.length > 0) {
			subEntityClassId = subEos[0].getEntityClassId();
		} else {
			return result;
		}

		let categoryAttributeName = FqnService.getShortAttributeNameFailsafe(
			subEntityClassId,
			chartContent.chart.primaryChart.categoryBoAttrId
		);
		// let categoryIsDateType = subformMetaData.attributes[categoryAttributeName].type == 'Timestamp'
		// || subformMetaData.attributes[categoryAttributeName].type == 'Date';

		for (let i = 0; i < series.length; i++) {
			let serie = series[i];
			if (!this.chartService.isValidSeries(serie)) {
				continue;
			}
			let label = serie.label.de;
			let seriesAttributeName = FqnService.getShortAttributeName(subEntityClassId, serie.boAttrId);
			let seriesData = this.getSeriesData(subEos, seriesAttributeName, categoryAttributeName);

			result.push({
				// TODO use localized string: key: this.localizedString(serie.label),
				key: label,
				values: seriesData,
				color: serie.color,
				// crosshair: crosshairData,
				bar: serie.bar
			});
		}
		return result;
	}

	private getSeriesData(
		subEos: SubEntityObject[],
		seriesAttributeName: string,
		categoryAttributeName: string
	) {
		let result: any[] = [];

		let categoryAttributeMeta = this.subMeta.getAttributeMeta(categoryAttributeName);
		let seriesAttributeMeta = this.subMeta.getAttributeMeta(seriesAttributeName);

		subEos.forEach(subEo => {
			let value = subEo.getAttribute(seriesAttributeName);
			let categoryValue = subEo.getAttribute(categoryAttributeName);

			if (categoryAttributeMeta) {
				categoryValue = this.deserializeValue(categoryAttributeMeta, categoryValue);
			}

			if (seriesAttributeMeta) {
				value = this.deserializeValue(seriesAttributeMeta, value);
			}

			result.push({
				categoryValue: categoryValue,
				seriesValue: value,
				tooltip: 'TODO: tooltip'
			});
		});

		return result;
	}

	private deserializeValue(meta: EntityAttrMeta, value: any) {
		let result = value;

		if (meta) {
			if (meta.isDate() || meta.isTimestamp()) {
				result = new Date(value);
			} else if (meta.isReference()) {
				result = value && value.name;
			}
		}

		return result;
	}

	isConfigurationValid() {
		return this.chartService.isValidChartPreference(this.eoChart.chartPreference.content);
	}

	getChartData() {
		if (this.isPieChart()) {
			// TODO: Pie-charts can't display multiple series. Should we display multiple pie charts in this case?
			return this.chartData[0].values;
		} else {
			return this.chartData;
		}
	}

	getChartDataCount() {
		return this.eoChart.getCurrentDataCount();
	}

	getChartSeriesCount() {
		return this.eoChart.getChartSeriesCount();
	}

	private isPieChart() {
		return this.eoChart.chartOptions.type === 'pieChart';
	}
}
