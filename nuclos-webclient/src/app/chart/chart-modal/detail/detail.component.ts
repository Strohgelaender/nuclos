import { Component, Input, OnInit } from '@angular/core';
import { ChartService } from '../../shared/chart.service';
import { EoChartWrapper } from '../../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

	@Input() eoChart: EoChartWrapper;
	@Input() allCharts: EoChartWrapper[];

	constructor(
		private chartService: ChartService
	) {
	}

	ngOnInit() {
	}

	isShowConfig() {
		return this.chartService.isShowConfig();
	}
}
