import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'nuc-chart-container',
	templateUrl: './chart-container.component.html',
	styleUrls: ['./chart-container.component.css']
})
export class ChartContainerComponent implements OnInit {

	constructor() {
	}

	ngOnInit() {
	}

}
