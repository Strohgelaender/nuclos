import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AutoComplete } from 'primeng/primeng';
import { ChartService } from '../shared/chart.service';
import { EoChartWrapper } from '../shared/eo-chart-wrapper';

@Component({
	selector: 'nuc-chart-filter',
	templateUrl: './chart-filter.component.html',
	styleUrls: ['./chart-filter.component.css']
})
export class ChartFilterComponent implements OnInit {
	@Input() eoChart: EoChartWrapper;

	/**
	 * TODO: Extract a re-usable Autocomplete component, do not re-implement it here.
	 */
	@ViewChild('autoComplete2') autoComplete: AutoComplete;

	constructor(
		private chartService: ChartService
	) {
	}

	ngOnInit() {
		if (!this.eoChart.searchItems) {
			this.chartService.buildSearchItems(this.eoChart);
		}
	}

	search() {
		this.chartService.search(this.eoChart);
	}

	/**
	 * TODO: Extract a re-usable Autocomplete component, do not re-implement it here.
	 */
	togglePanel() {
		if (this.autoComplete.panelVisible) {
			this.hidePanel();
		} else {
			this.showPanel();
		}
	}

	showPanel() {
		this.autoComplete.panelVisible = true;
		this.autoComplete.show();

		let element = this.autoComplete.panelEL.nativeElement;
		let inputEl = this.autoComplete.inputEL.nativeElement;
		let inputWidth = $(inputEl).outerWidth();
		$(element).outerWidth(inputWidth);
	}

	hidePanel() {
		this.autoComplete.hide();
	}
}
