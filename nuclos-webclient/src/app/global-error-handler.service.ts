import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { FORBIDDEN, NOT_FOUND } from 'http-status-codes';
import { Logger } from './log/shared/logger';

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {

	private static IGNORE_ERRORS_FOR = ['/subBos/', '/user/forgot', '/forgotPassword'];

	constructor(
		private injector: Injector,
		private $log: Logger,
	) {
	}

	handleError(error: any): void {
		this._handleError(error);
	}

	private _handleError(err) {
		if (this.isErrorIgnoredForUrl(err.url)) {
			return;
		}

		if (err.status === FORBIDDEN) {
			this.router.navigate(['/error', FORBIDDEN]);
		} else if (err.status === NOT_FOUND) {
			this.router.navigate(['/error', NOT_FOUND]);
		} else {
			this.$log.error(err);
		}
	}

	/**
	 * TODO: Find a better way than a hard-coded list to ignore errors for certain URLs.
	 */
	private isErrorIgnoredForUrl(url: string) {
		return url && GlobalErrorHandlerService.IGNORE_ERRORS_FOR.find(
			value => url.indexOf(value) >= 0
		) !== undefined;
	}

	public get router(): Router {
		return this.injector.get(Router);
	}
}
