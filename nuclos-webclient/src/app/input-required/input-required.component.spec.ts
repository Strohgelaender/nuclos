/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InputRequiredComponent } from './input-required.component';

xdescribe('InputRequiredComponent', () => {
	let component: InputRequiredComponent;
	let fixture: ComponentFixture<InputRequiredComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [InputRequiredComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(InputRequiredComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
