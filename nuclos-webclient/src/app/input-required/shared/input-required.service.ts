import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { from as observableFrom, Observable, of as observableOf, throwError as observableThrowError } from 'rxjs';

import { tap } from 'rxjs/operators';
import { AddonService } from '../../addons/addon.service';
import { Logger } from '../../log/shared/logger';
import { InputRequiredDialogComponent } from '../input-required-dialog/input-required-dialog.component';
import { InputRequired } from './model';

/**
 * Handles "Input Required" which can occur whenever an EO is saved.
 * This service does not save the EO itself or some strange callback magic.
 */
@Injectable()
export class InputRequiredService {

	constructor(private modalService: NgbModal,
				private addonService: AddonService,
				private $log: Logger
	) {
	}

	/**
	 * Handles possible InputRequired "errors".
	 * Returns an Observable that handles the InputRequired specification
	 * or just an Observable containing the given error if no InputRequired was found.
	 *
	 * @param error The error that occurred while sending the data. Could contain an InputRequired specification.
	 * @param data The original request data.
	 * @returns {any}
	 */
	handleError(
		error: Response | any,
		data: any,
		eo: IEntityObject
	): Observable<any> {
		if (error instanceof Response) {
			const json = this.responseToJson(error);
			if (json && json.inputrequired) {
				let inputRequired = <InputRequired>json.inputrequired;
				return this.handleInputRequired(inputRequired, eo).pipe(
					tap(result => {
						// data might have a "inputrequired" attribute from a previous InputRequired error.
						// The previous result must be merged with the current one and sent again.
						let nextInputRequired = data.inputrequired || inputRequired;
						if (!nextInputRequired.result) {
							nextInputRequired.result = {};
						}
						nextInputRequired.result[inputRequired.specification.key] = result;
						data.inputrequired = nextInputRequired;
					})
				);
			}
		}

		// If no InputRequired was handled, just re-throw the error
		return observableThrowError(error);
	}

	private responseToJson(response: Response) {
		let result;

		try {
			result = response.json();
		} catch (e) {
		}

		return result;
	}

	/**
	 * Handles a concrete InputRequired object.
	 */
	private handleInputRequired(
		inputRequired: InputRequired,
		eo: IEntityObject
	): Observable<any> {
		if (!inputRequired || !inputRequired.specification) {
			return observableOf(undefined);
		}

		// if the InputRequiredException is called with InputDelegateSpecification an addon will be instantiated (counterpart to client extension)
		let inputRequiredDialogComponent = InputRequiredDialogComponent;
		const delegateComponentPackageName = inputRequired.specification.delegateComponent;
		if (delegateComponentPackageName) {
			// use 'class-name' for instantiating addon, package will be ignored
			// this makes it possible to use InputRequiredExceptions with InputDelegateSpecification for Java-Extensions and WebAddons
			const delegateComponentName = delegateComponentPackageName.split('.').pop();
			if (delegateComponentName) {
				const delegateComponent = this.addonService.getComponentFactoryClass(delegateComponentName);
				if (!delegateComponent) {
					this.$log.error('Unable to find addon component ' + delegateComponentPackageName + ' for input required exception.');
				} else {
					inputRequiredDialogComponent = <any>delegateComponent;
				}
			}
		}

		const ngbModalRef = this.modalService.open(inputRequiredDialogComponent);
		ngbModalRef.componentInstance.specification = inputRequired.specification;
		ngbModalRef.componentInstance.eo = eo;


		return observableFrom(ngbModalRef.result);
	}
}
