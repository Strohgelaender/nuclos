import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { publish, refCount } from 'rxjs/operators';

@Injectable()
export class LocalStorageService {

	private itemSubjects = new Map<string, Subject<any>>();

	constructor() {
		window.addEventListener('storage', (event: StorageEvent) => this.handleEvent(event));
	}

	private handleEvent(event: StorageEvent) {
		if (event.key && event.newValue) {
			this.publishItem(event.key, event.newValue);
		}
	}

	private publishItem(key: string, stringValue: string | undefined) {
		let subject = this.itemSubjects.get(key);
		if (subject) {
			let data;

			if (stringValue) {
				data = JSON.parse(stringValue);
			}

			subject.next(data);
		}
	}

	getItem(key: string) {
		return localStorage.getItem(key);
	}

	setItem(key: string, data: any) {
		let stringValue = JSON.stringify(data);
		localStorage.setItem(key, stringValue);
	}

	removeItem(key: string) {
		localStorage.removeItem(key);
	}

	observeItem(key: string): Observable<any> {
		let subject = this.itemSubjects.get(key);

		if (!subject) {
			subject = new Subject<any>();
			this.itemSubjects.set(key, subject);
		}

		return subject.pipe(
			publish(),
			refCount(),
		);
	}
}
