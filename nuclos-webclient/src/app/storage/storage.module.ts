import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LocalStorageService } from './shared/local-storage.service';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	providers: [
		LocalStorageService
	]
})
export class StorageModule {
}
