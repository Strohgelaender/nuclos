/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { NuclosCacheService } from './nuclos-cache.service';

describe('NuclosCacheService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [NuclosCacheService]
		});
	});

	it('should ...', inject([NuclosCacheService], (service: NuclosCacheService) => {
		expect(service).toBeTruthy();
	}));
});
