import { Component, OnInit } from '@angular/core';
import { MapEntriesPipe } from '../../shared/map-entries.pipe';
import { NuclosCacheService } from '../shared/nuclos-cache.service';

@Component({
	selector: 'nuc-cache-overview',
	templateUrl: './cache-overview.component.html',
	styleUrls: ['./cache-overview.component.css']
})
export class CacheOverviewComponent implements OnInit {

	private caches: { key, value }[] = [];

	constructor(private cacheService: NuclosCacheService) {
	}

	ngOnInit() {
		this.caches = new MapEntriesPipe().transform(this.cacheService.getAllCaches());
	}

	getCaches(): { key, value }[] {
		return this.caches;
	}
}
