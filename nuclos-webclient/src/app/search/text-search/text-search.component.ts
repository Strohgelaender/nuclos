import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { EntityObject } from '../../entity-object-data/shared/entity-object.class';
import { SearchService } from '../shared/search.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';

@Component({
	selector: 'nuc-text-search',
	templateUrl: './text-search.component.html',
	styleUrls: ['./text-search.component.css']
})
export class TextSearchComponent implements OnInit {

	@Input() eo: EntityObject | undefined;
	@Input() meta: EntityMeta;

	textSearch: string;

	/**
	 * provides text input changes for debounced search
	 */
	private textSearchUpdated: Subject<string> = new Subject<string>();

	constructor(
		private i18n: NuclosI18nService,
		private searchService: SearchService
	) {
	}

	ngOnInit() {
		// debounce search input
		this.textSearchUpdated.asObservable()
			.debounceTime(400)
			.distinctUntilChanged().subscribe(
			(searchInputText) => {
				this.searchService.updateSearchInputText(searchInputText);
				this.searchService.initiateDataLoad();
				this.scrollSidebarToTop();
			}
		);
	}

	getPlaceholder(): string {
		return (this.meta ? (this.meta.getEntityName() + ' ') : '') + this.i18n.getI18n('webclient.search.quickfind');
	}

	doTextSearch(): void {
		this.textSearchUpdated.next(this.textSearch);
	}

	isDisabled() {
		return this.eo && this.eo.isDirty();
	}

	/**
	 * scroll sidebar to top
	 * @deprecated: TODO: Sidebar should do this by itself, instead of being manipulated here
	 */
	private scrollSidebarToTop(): void {
		$('#sidebar-list-container').scrollTop(0);
	}
}
