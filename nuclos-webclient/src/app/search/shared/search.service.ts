import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

/**
 * Triggers a search based on the current searchfilter.
 */
@Injectable()
export class SearchService {

	private searchInputText: BehaviorSubject<string>;
	private initateDataLoadSubject: Subject<Date>;

	constructor() {
		this.searchInputText = new BehaviorSubject<string>('');
		this.initateDataLoadSubject = new Subject<Date>();
	}

	updateSearchInputText(text: string) {
		this.searchInputText.next(text);
	}

	getCurrentSearchInputText(): string {
		return this.searchInputText.getValue();
	}

	initiateDataLoad(): void {
		this.initateDataLoadSubject.next(new Date());
	}

	subscribeDataLoad(): Subject<Date> {
		return this.initateDataLoadSubject;
	}
}
