import { Component, Input, OnInit } from '@angular/core';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { IEntityObject } from '@nuclos/nuclos-addon-api';

@Component({
	selector: 'nuc-search-bar',
	templateUrl: './search-bar.component.html',
	styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
	@Input() meta: EntityMeta;

	eo: IEntityObject | undefined;

	constructor(
		private eoEventService: EntityObjectEventService
	) {
	}

	ngOnInit() {
		this.eoEventService.observeSelectedEo().subscribe(
			eo => this.eo = eo
		);
	}

}
