import { Component, Input, OnInit } from '@angular/core';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { Preference, SearchtemplatePreferenceContent } from '../../preferences/preferences.model';
import { EntityObjectSearchfilterService } from '../../entity-object-data/shared/entity-object-searchfilter.service';
import { SideviewmenuPreferenceContent } from '../../preferences/preferences.model';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { SideviewmenuService } from '../../entity-object/shared/sideviewmenu.service';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import { SearchEditorComponent } from '../search-editor/search-editor.component';
import { OnDestroy } from '@angular/core';

@Component({
	selector: 'nuc-search-editor-button',
	templateUrl: './search-editor-button.component.html',
	styleUrls: ['./search-editor-button.component.css']
})
export class SearchEditorButtonComponent implements OnInit {

	@Input() meta: EntityMeta;

	constructor(
		private sideviewmenuService: SideviewmenuService,
		private entityObjectPreferenceService: EntityObjectPreferenceService
	) {}

	ngOnInit() {}

	toggleSearchEditor() {
		if (this.isEditorFixed()) {
			this.setEditorFixedAndShowing(!this.isEditorFixedAndShowing());
			this.setPopoverShowing(false);
		} else {
			this.setPopoverShowing(!this.isPopoverShowing());
		}
	}

	/*adjustPopover() {
		let button = $('#search-editor-button');
		if (button.offset() !== undefined) {
			let popover = $('#search-editor-popover');
			let toolbar = $('#toolbar');
			if (toolbar.offset() !== undefined) {
				popover.css('top', (toolbar.offset().top + toolbar.outerHeight()) + 'px');
			}
		}
	}*/

	isSelectedSearchfilterEnabled(): boolean {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			for (let searchElement of searchfilter.content.columns) {
				if (searchElement.operator && searchElement.enableSearch !== false && searchElement.selected !== false) {
					return true;
				}
			}
		}
		return false;
	}

	getSelectedSearchfilter(): Preference<SearchtemplatePreferenceContent> | undefined {
		return EntityObjectSearchfilterService.instance.getSelectedSearchfilter();
	}

	getSideviewmenuPref() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$.getValue();
	}

	isPopoverShowing(): boolean {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			return pref.content.searchEditorPopoverShowing;
		}
		return false;
	}

	setPopoverShowing(popoverShowing: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			pref.content.searchEditorPopoverShowing = popoverShowing;
		}
	}

	isEditorFixed(): boolean {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			return pref.content.sideviewSearchEditorFixed;
		}
		return false;
	}

	isEditorFixedAndShowing() {
		let pref = this.getSideviewmenuPref();
		if (pref && pref.content.sideviewSearchEditorFixed) {
			return pref.content.sideviewSearchEditorFixedAndShowing;
		}
		return false;
	}

	private setEditorFixedAndShowing(value: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref && pref.content.sideviewSearchEditorFixed) {
			pref.content.sideviewSearchEditorFixedAndShowing = value;
			this.saveSideviewmenuPreference();
		}
	}

	private saveSideviewmenuPreference(): void {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			this.sideviewmenuService.saveSideviewmenuPreference(pref).subscribe();
		}
	}

}
