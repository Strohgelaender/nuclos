import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../../entity-object-data/shared/entity-object-searchfilter.service';
import {
	Preference,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent
} from '../../../preferences/preferences.model';
import { Selectable } from '../../../two-side-multi-select/two-side-multi-select.model';
import { SearchfilterService } from '../../shared/searchfilter.service';

@Component({
	selector: 'nuc-search-attribute-selector',
	templateUrl: './search-attribute-selector.component.html',
	styleUrls: ['./search-attribute-selector.component.css']
})
export class SearchAttributeSelectorComponent implements OnInit, OnChanges {

	@Input() meta: EntityMeta;
	@Input() searchfilter: Preference<SearchtemplatePreferenceContent>;

	filter = '';
	selectedValue;
	availableAttributes: Selectable[] = [];
	selectedAttributes: Selectable[] = [];

	constructor(
		private searchfilterService: SearchfilterService,
		private eoSearchfilterService: EntityObjectSearchfilterService,
	) {
	}

	ngOnInit() {
		this.updateAttributes('');
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.updateAttributes('');
	}

	private updateAttributes(query: string) {
		this.updateSelectedAttributes();
		this.updateAvailableAttributes(query);
	}

	private updateAvailableAttributes(query: string) {
		if (this.meta) {
			let emptySearchPref = this.searchfilterService.createSearchfilter(this.meta);
			if (!this.searchfilter) {
				this.searchfilter = emptySearchPref;
			}

			this.availableAttributes = (emptySearchPref.content.columns as Selectable[]).filter(
				column => column.name.toLowerCase().indexOf(query.toLowerCase()) >= 0
					&& !this.isSelected(column.name)
					// && !
			);
		} else {
			this.availableAttributes = [];
		}
	}

	private updateSelectedAttributes() {
		if (this.searchfilter) {
			this.selectedAttributes = (this.searchfilter.content.columns as Selectable[])
				.filter(it => it.selected);
		} else {
			this.selectedAttributes = [];
		}
	}

	selectAttribute(attribute: Selectable) {
		let fromSearchfilter = this.findAttributeInSearchfilter(attribute.name);
		if (fromSearchfilter) {
			fromSearchfilter.selected = true;
			fromSearchfilter.enableSearch = true;
			let index = this.searchfilter.content.columns.indexOf(fromSearchfilter, 0);
			if (index > -1) {
				this.searchfilter.content.columns.splice(index, 1);
			}
			this.searchfilter.content.columns.push(fromSearchfilter);
		} else {
			attribute.selected = true;
			let newAttribute = attribute as SearchtemplateAttribute;
			newAttribute.enableSearch = true;
			this.searchfilter.content.columns.push(newAttribute);
		}

		this.filter = '';
		this.selectedValue = {name: ''};

		this.eoSearchfilterService.updateColumns(this.searchfilter);
		this.searchfilterService.saveAndSelectSearchfilter(this.searchfilter).subscribe();
	}

	loadAttributes(query: string) {
		this.updateAttributes(query);
	}

	private isSelected(name: string) {
		return this.selectedAttributes.find(it => it.name === name);
	}

	private findAttributeInSearchfilter(name: string) {
		let result;
		if (this.searchfilter) {
			result = (this.searchfilter.content.columns as Selectable[]).find(
				it => it.name === name
			);
		}
		return result;
	}

	modelChange($event) {
		if (typeof $event === 'string') {
			this.filter = $event;
		}
	}
}
