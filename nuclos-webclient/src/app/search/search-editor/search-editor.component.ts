import { Component, Input, OnInit } from '@angular/core';
import { EntityMeta } from '../../entity-object-data/shared/bo-view.model';
import { Preference, SearchtemplatePreferenceContent } from '../../preferences/preferences.model';
import { InputType } from '../../entity-object-data/shared/bo-view.model';
import { SearchtemplateAttribute } from '../../preferences/preferences.model';
import { SearchfilterService } from '../shared/searchfilter.service';
import { FqnService } from '../../shared/fqn.service';
import { DatetimeService } from '../../shared/datetime.service';
import { Operator } from '../operator';
import { Logger } from '../../log/shared/logger';
import { Subject } from '../../../../node_modules/rxjs/index';
import { SearchService } from '../shared/search.service';
import { EntityObjectEventService } from '../../entity-object-data/shared/entity-object-event.service';
import { EntityObjectSearchfilterService } from '../../entity-object-data/shared/entity-object-searchfilter.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { ElementRef } from '@angular/core';
import { IEntityObject } from '@nuclos/nuclos-addon-api';
import { debounceTime } from 'rxjs/operators';
import { distinctUntilChanged } from 'rxjs/operators';
import { SideviewmenuService } from '../../entity-object/shared/sideviewmenu.service';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { Selectable } from '../../two-side-multi-select/two-side-multi-select.model';
import { DialogService } from '../../popup/dialog/dialog.service';
import { ViewChild } from '@angular/core';
import { DropdownComponent } from '../../ui-components/dropdown/dropdown.component';
import { SearchFilterSelectorComponent } from './search-filter-selector/search-filter-selector.component';

@Component({
	selector: 'nuc-search-editor',
	templateUrl: './search-editor.component.html',
	styleUrls: ['./search-editor.component.css']
})
export class SearchEditorComponent implements OnInit {

	@Input() meta: EntityMeta;

	@Input() searchAttributesContainerHeight: number;

	@ViewChild('searchfilterSelector') searchfilterSelector: SearchFilterSelectorComponent;

	private eo: IEntityObject | undefined;

	customSearchfilterName;

	metaEditingActive = false;

	itemOrderingActive = false;

	itemSelectables: Selectable[] = [];

	/**
	 * provides changes of searchtemplate for debounced search / pref save
	 */
	private searchRequestTimer: Subject<number> = new Subject<number>();
	private searchFilterUpdateTimer: Subject<number> = new Subject<number>();

	datePattern: string;

	constructor(
		private fqnService: FqnService,
		private datetimeService: DatetimeService,
		private searchService: SearchService,
		private searchfilterService: SearchfilterService,
		private sideviewmenuService: SideviewmenuService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private eoEventService: EntityObjectEventService,
		private dialogService: DialogService,
		private i18n: NuclosI18nService,
		private ref: ElementRef,
		private $log: Logger
	) {
		this.datePattern = this.datetimeService.getDatePattern();
	}

	ngOnInit() {

		this.eoEventService.observeSelectedEo().subscribe(
			eo => this.eo = eo
		);

		this.searchRequestTimer.asObservable().pipe(
			debounceTime(400),
			distinctUntilChanged()
		).subscribe(
			() => {
				this.searchService.initiateDataLoad();

				this.scrollSidebarToTop();
			}
		);

		this.searchFilterUpdateTimer.asObservable().pipe(
			debounceTime(400),
			distinctUntilChanged()
		).subscribe(
			() => {
				this.saveSelectedSearchfilter();
			}
		);
	}

	/**
	 * define which search input component will be used for which attribute
	 */
	getSearchComponent(attribute: SearchtemplateAttribute): string {
		let inputType = this.getInputType(attribute);
		if (inputType === InputType.BOOLEAN) {
			return 'boolean';
		} else if (inputType === InputType.REFERENCE && (!attribute.operator || attribute.operator === '=')) {
			return 'lov';
		} else if (inputType === InputType.DATE) {
			return 'datepicker';
		}
		return 'string';
	}

	private getInputType(attribute: SearchtemplateAttribute): string {
		let attrName = this.fqnService.getShortAttributeName(this.meta.getBoMetaId(), attribute.boAttrId);
		return SearchfilterService.getInputType(<SearchtemplateAttribute>this.meta.getAttribute(attrName));
	}

	getOperatorDefinitions(inputType: any) {
		return this.searchfilterService.getOperatorDefinitions()[inputType];
	}

	getOperatorDefinition(attribute: SearchtemplateAttribute): Operator | undefined {
		return this.searchfilterService.getOperatorDefinition(attribute);
	}

	showOperator(attribute: SearchtemplateAttribute, operatorDef: Operator): boolean {
		return !(!attribute.nullable && operatorDef.isUnary)
			&& !(attribute.boAttrName === 'nuclosStateNumber' || attribute.boAttrName === 'nuclosState');
	}

	inputSearchValue(attribute: SearchtemplateAttribute): void {
		// TODO
		// if (attribute.value !== undefined && attribute.value.id !== undefined) {
		// 	// empty dropdown selection
		// 	attribute.value = undefined;
		// }

		attribute.enableSearch = true;

		let operatorDef = this.searchfilterService.getOperatorDefinition(attribute);
		if (operatorDef) {
			if (operatorDef.isUnary) {
				attribute.value = '';
			} else {
				// TODO: Hack
				window.setTimeout(() => {
					$('#searchfilter-value').focus();
				});
			}
		}

		if (operatorDef) {
			this.searchfilterService.formatValue(attribute, operatorDef);
		}

		// clear input if reference was selected before in lov and then a 'like' search was called
		if (attribute.reference && operatorDef && operatorDef.operator === 'like'
			&& attribute && attribute.value && attribute && attribute.value.id) {
			delete attribute.value;
		}

		this.searchFilterUpdateTimer.next(new Date().getTime());

		this.doSearch(true, true);
	}

	private saveSelectedSearchfilter() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.searchfilterService.saveAndSelectSearchfilter(searchfilter).subscribe();
		}
	}

	searchItemCheckClicked() {
		this.doSearch(true, true);
		this.saveSelectedSearchfilter();
	}

	doSearch(doSearch: boolean, selectFirstFoundEntry: boolean, validate = true): void {

		this.$log.debug('doSearch(%o, %o, %o)', doSearch, selectFirstFoundEntry, validate);

		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			if (validate) {
				this.validateSearchForm(searchfilter.content);
			}

			if (doSearch) {
				this.searchRequestTimer.next(new Date().getTime());
			}
		}
	}

	private validateSearchForm(searchtemplate: SearchtemplatePreferenceContent): void {
		if (!searchtemplate) {
			this.$log.warn('No searchtemplate given');
			return;
		}

		this.$log.debug('Validating searchtemplate: %o with %o attributes', searchtemplate, searchtemplate.columns.length);

		let searchFormIsValid = true;
		searchtemplate.columns
			.forEach((attribute) => {
				let operatorDef = this.searchfilterService.getOperatorDefinition(attribute);
				if (operatorDef) {
					if (operatorDef.isUnary) {
						attribute.value = '';
					}

					attribute.isValid =
						operatorDef.isUnary
						||
						(
							attribute.value !== undefined && attribute.value.length === undefined && attribute.inputType === InputType.NUMBER
							|| attribute.value !== undefined && operatorDef.operator === '='
							&& attribute.value.id && attribute.inputType === InputType.REFERENCE // equal search reference with dropdown
							|| attribute.value !== undefined && operatorDef.operator === 'like'
							&& attribute.value.length > 0 && attribute.inputType === InputType.REFERENCE // equal search reference with dropdown
							|| attribute.value !== undefined && attribute.value.length === undefined && attribute.inputType === InputType.DATE
							|| attribute.value !== undefined && attribute.value.length > 0
							|| operatorDef.operator === '=' && attribute.values && attribute.values.length > 0	// IN condition
						);

					if (!attribute.isValid && attribute.enableSearch) {
						searchFormIsValid = false;
					}
				} else {
					if (attribute.enableSearch) {
						searchFormIsValid = false;
					}
				}
				searchtemplate.isValid = searchFormIsValid;
			});
	}

	removeSearchItem(attribute: SearchtemplateAttribute) {
		attribute.selected = false;

		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			let index = searchfilter.content.columns.findIndex(
				it => it.boAttrId === attribute.boAttrId
			);
			if (index >= 0) {
				searchfilter.content.columns.splice(index, 1);
				this.doSearch(true, true);
			}
			this.saveSelectedSearchfilter();
		}
	}

	selectSearchfilterButtonClicked(searchfilter: Preference<SearchtemplatePreferenceContent>) {
		EntityObjectSearchfilterService.instance.selectSearchfilter(searchfilter);
	}

	createSearchfilter() {
		this.metaEditingActive = false;
		this.itemOrderingActive = false;

		let searchfilter = this.searchfilterService.createSearchfilter(this.meta);
		searchfilter.name = this.i18n.getI18n('webclient.searchfilter.new');
		const searchfilterNew = this.i18n.getI18n('webclient.searchfilter.new');
		const regexp = new RegExp('^' + searchfilterNew + ' [0-9]* *$');
		let searchfilterNumber = 0;
		for (const sf of this.getAllSearchfilters()) {
			if (sf.name && regexp.test(sf.name)) {
				let usedNumber = +sf.name.substr(sf.name.indexOf(' ') + 1).trim();
				if (usedNumber > searchfilterNumber) {
					searchfilterNumber = usedNumber;
				}
			}
		}
		searchfilterNumber++;
		searchfilter.name = searchfilterNew + ' ' + searchfilterNumber;
		searchfilter.content.userdefinedName = true;

		EntityObjectSearchfilterService.instance.selectSearchfilter(searchfilter);
		setTimeout(() => {
			if (this.ref) {
				let inputs = $(this.ref.nativeElement).find('input');
				if (inputs && inputs.length === 2) {
					inputs[1].focus();
					inputs[1].click();
				}
			}
		});
	}

	/**
	 * @deprecated: TODO: Sidebar should do this by itself, instead of being manipulated here
	 */
	private scrollSidebarToTop(): void {
		$('#sidebar-list-container').scrollTop(0);
	}

	isDisabled() {
		return !!this.eo && this.eo.isDirty();
	}

	getSelectedSearchfilter(): Preference<SearchtemplatePreferenceContent> | undefined {
		return EntityObjectSearchfilterService.instance.getSelectedSearchfilter();
	}

	getSelectableColumns(): Selectable[] {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			return searchfilter.content.columns as Selectable[];
		}
		return [];
	}

	getAllSearchfilters(): Preference<SearchtemplatePreferenceContent>[] {
		return EntityObjectSearchfilterService.instance.getAllSearchfilters();
	}

	getSideviewmenuPref() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$.getValue();
	}

	isEditorFixed(): boolean {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			return pref.content.sideviewSearchEditorFixed;
		}
		return false;
	}

	setEditorFixed(editorFixed: boolean) {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			if (!editorFixed) {
				// show popover
				pref.content.searchEditorPopoverShowing = true;
			}
			pref.content.sideviewSearchEditorFixed = editorFixed;
			pref.content.sideviewSearchEditorFixedAndShowing = editorFixed;
			this.saveSideviewmenuPreference();
		}
	}

	private saveSideviewmenuPreference(): void {
		let pref = this.getSideviewmenuPref();
		if (pref) {
			this.sideviewmenuService.saveSideviewmenuPreference(pref).subscribe();
		}
	}

	showSearchItems(): boolean {
		return !this.metaEditingActive && !this.itemOrderingActive;
	}

	editMeta() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.itemOrderingActive = false;
			this.metaEditingActive = !this.metaEditingActive;
			if (this.metaEditingActive) {
				// activated ... init
				this.customSearchfilterName = searchfilter.name;
			} else {
				// save
				if (searchfilter.name !== this.customSearchfilterName) {
					searchfilter.content.userdefinedName = true;
					searchfilter.name = this.customSearchfilterName;
					this.saveSelectedSearchfilter();
					EntityObjectSearchfilterService.instance.sortSearchfilters();
					this.searchfilterSelector.setSearchfilterDropdownInput(this.customSearchfilterName);
				}
			}
		}
	}

	orderItems() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.metaEditingActive = false;
			this.itemOrderingActive = !this.itemOrderingActive;
			if (this.itemOrderingActive) {
				// activated ... init
				this.itemSelectables = searchfilter.content.columns as Selectable[];
			} else {
				// save
				searchfilter.content.columns.sort((a, b) => {
					let indexA = this.itemSelectables.findIndex(value => value.name === a.name);
					let indexB = this.itemSelectables.findIndex(value => value.name === b.name);
					return indexA < indexB ? -1 : 1;
				});
				this.saveSelectedSearchfilter();
			}
		}
	}

	deleteSearchfilter() {
		let searchfilter = this.getSelectedSearchfilter();
		if (searchfilter) {
			this.dialogService.confirm({
					title: this.i18n.getI18n('webclient.searcheditor.searchfilter.delete'),
					message: this.i18n.getI18n('webclient.searcheditor.searchfilter.delete.confirm')
				}
			).then(
				() => {
					// ok, delete
					this.metaEditingActive = false;
					EntityObjectSearchfilterService.instance.deleteSearchfilter(searchfilter);
				},
				() => {
					// cancel
				}
			);
		}
	}

}
