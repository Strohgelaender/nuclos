import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EntityMeta } from '../../../entity-object-data/shared/bo-view.model';
import { EntityObjectSearchfilterService } from '../../../entity-object-data/shared/entity-object-searchfilter.service';
import { NuclosI18nService } from '../../../i18n/shared/nuclos-i18n.service';
import { Preference, SearchtemplatePreferenceContent } from '../../../preferences/preferences.model';
import { SearchService } from '../../shared/search.service';
import { SearchfilterService } from '../../shared/searchfilter.service';
import { ViewChild } from '@angular/core';
import { DropdownComponent } from '../../../ui-components/dropdown/dropdown.component';

@Component({
	selector: 'nuc-search-filter-selector',
	templateUrl: './search-filter-selector.component.html',
	styleUrls: ['./search-filter-selector.component.css']
})
export class SearchFilterSelectorComponent implements OnInit {

	@Input() meta: EntityMeta;
	@Input() searchfilter: Preference<SearchtemplatePreferenceContent> | undefined;

	@ViewChild('searchfilterSelectorDropdown') searchfilterSelectorDropdown: DropdownComponent;

	@Output() createSearchfilter = new EventEmitter<any>();

	constructor(
	) {
	}

	ngOnInit() {
	}

	getSearchfilters() {
		return EntityObjectSearchfilterService.instance.getAllSearchfilters();
	}

	doCreateSearchfilter() {
		if (this.createSearchfilter) {
			this.createSearchfilter.emit();
		}
	}

	selectSearchfilter(searchfilter) {
		EntityObjectSearchfilterService.instance.selectSearchfilter(searchfilter);
	}

	setSearchfilterDropdownInput(value: string) {
		this.searchfilterSelectorDropdown.setInput(value);
	}
}
