import { Component, Input, OnInit } from '@angular/core';
import { BoAttr, EntityMeta, LovEntry } from '../../../entity-object-data/shared/bo-view.model';
import { LovDataService } from '../../../entity-object-data/shared/lov-data.service';
import { MetaService } from '../../../entity-object-data/shared/meta.service';
import { FqnService } from '../../../shared/fqn.service';
import { AbstractSearchItem } from '../abstract-search-item';
import { Renderer2 } from '@angular/core';
import { ElementRef } from '@angular/core';

@Component({
	selector: 'nuc-search-item-lov',
	templateUrl: './search-item-lov.component.html',
	styleUrls: ['./search-item-lov.component.css']
})
export class SearchItemLovComponent extends AbstractSearchItem implements OnInit {

	@Input() meta: EntityMeta;
	@Input() suggestions: LovEntry[];
	@Input() attribute: any;

	constructor(
		private fqnService: FqnService,
		private metaService: MetaService,
		private lovDataService: LovDataService,
		private _renderer: Renderer2,
		private _elemRef: ElementRef
	) {
		super();
	}

	ngOnInit() {
		this.modelChanged.subscribe(
			() => this.filterSuggestations()
		);
	}
	private filterSuggestations() {
		if (this.suggestions && this.model) {
			this.suggestions = this.suggestions.filter(
				suggestionEntry => suggestionEntry.id != null && !this.model.find(
					selectedEntry => suggestionEntry.id === selectedEntry.id
				)
			);
		}
	}

	loadSearchLovDropDownOptions(query, attribute: BoAttr): void {
		let attrName = this.fqnService.getShortAttributeName(this.meta.getBoMetaId(), attribute.boAttrId);
		this.metaService.getEntityMeta(this.meta.getBoMetaId()).subscribe(entityMeta => {
			let attributeMeta = entityMeta.getAttributeMeta(attrName);
			if (attributeMeta) {
				this.lovDataService.loadSearchLovEntries(attributeMeta, query).subscribe(
					data => {
						if (data) {
							this.suggestions = data;
							this.filterSuggestations();
						}
					}
				);
			}
		});
	}
}
