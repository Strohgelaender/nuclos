import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PRECONDITION_FAILED, UNAUTHORIZED } from 'http-status-codes';
import { throwError as observableThrowError } from 'rxjs';

import { catchError } from 'rxjs/operators';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { SystemParameter } from '../shared/system-parameters';
import { AuthenticationData, AuthenticationService } from './authentication.service';

@Component({
	selector: 'nuc-authentication',
	templateUrl: './authentication.component.html',
	styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

	@Input()
	username = '';

	@Input()
	password: string;

	@Input()
	autologin: boolean;

	@ViewChild('usernameInput') usernameInput;
	@ViewChild('passwordInput') passwordInput;

	showRegistrationLink = false;
	showForgotLoginDetailsLink = false;

	loginFormEnabled = true;
	showMandatorForm = false;
	showMandatorlessSessionButton = false;
	selectedMandatorId;
	loginData: AuthenticationData;

	/**
	 * Should be set to an i18n key, e.g. 'webclient.nosession.error.wrongpass'.
	 */
	errorMessage: string | undefined;

	constructor(
		private route: ActivatedRoute,
		private authenticationService: AuthenticationService,
		private i18nService: NuclosI18nService,
		private configService: NuclosConfigService
	) {
	}

	ngOnInit() {

		// initialize username input with username from last login
		let lastLoginUsername = this.authenticationService.getUsername();
		if (lastLoginUsername) {
			this.username = lastLoginUsername;
		}

		this.route.params.subscribe(
			params => this.i18nService.setLocaleString(params['locale'])
		);

		this.configService.getSystemParameters()
			.subscribe(params => {
				this.showRegistrationLink = params.is(SystemParameter.USER_REGISTRATION_ENABLED);
				this.showForgotLoginDetailsLink = params.is(SystemParameter.FORGOT_LOGIN_DETAILS_ENABLED);
			});
		setTimeout(() => {
			if (this.usernameInput) {
				// focus username respectively password input
				if (this.username.length === 0) {
					this.usernameInput.nativeElement.focus();
				} else {
					this.passwordInput.nativeElement.focus();
				}
			}
		});
	}

	login() {
		this.errorMessage = undefined;
		let loginData = {
			username: this.username,
			password: this.password,
			locale: this.i18nService.getCurrentLocale().key,
			autologin: this.autologin
		};

		this.authenticationService.login(loginData).pipe(catchError(error => {
			if (error.status === UNAUTHORIZED) {
				this.errorMessage = 'webclient.nosession.error.wrongpass';
			} else if (this.isCredentialsExpired(error)) {
				this.authenticationService.setCredentialsExpired({
					userName: loginData.username,
					oldPassword: loginData.password
				});
			} else {
				this.errorMessage = 'webclient.nosession.error.nocode';
			}
			return observableThrowError(error);
		})).subscribe((authenticationData) => {

			// show mandator selection dialog if configured
			this.showMandatorlessSessionButton = authenticationData.superUser;
			if (authenticationData.mandators) {
				this.loginData = authenticationData;

				// check if the last selected mandator (from cookie) still exists
				let lastMandatorLoggedInId = this.authenticationService.getLastMandatorIdCookie();

				if (!lastMandatorLoggedInId) {
					this.selectedMandatorId = authenticationData.mandators[0].mandatorId;
				} else {
					this.selectedMandatorId = lastMandatorLoggedInId;
				}

				if (authenticationData.mandators.length === 1) {
					// only one mandator configured no need for mandator selection
					this.authenticationService.selectMandator(authenticationData.mandators[0]).subscribe(() => {
						$('body').css('cursor', 'progress');
						this.authenticationService.autoNavigation().then(() => {
							window.location.reload();
						});
					});
				} else {
					this.loginFormEnabled = false;
					this.showMandatorForm = true;
				}
			}
		});
	}

	private isCredentialsExpired(error: Response) {
		return error.status === PRECONDITION_FAILED
			&& error.json()
			&& error.json()['message'] === 'nuclos.security.authentication.credentialsexpired';
	}

	selectMandator() {
		this.authenticationService.onAuthenticationDataAvailable().subscribe(() => {
			if (this.loginData.mandators) {
				let mandator = this.loginData.mandators.filter(m => m.mandatorId === this.selectedMandatorId).shift();
				this.authenticationService.selectMandator(mandator).subscribe(
					() => {
						this.loginData.mandator = mandator;
						this.navigateHome();
					}
				);
			}
		});
	}

	mandatorlessSession() {
		this.navigateHome();
	}

	private navigateHome() {
		$('body').css('cursor', 'progress');
		this.authenticationService.navigateHome();

		if (window.location.href.indexOf('/login') === -1) {
			window.location.reload();
		}
	}
}
