import { Component, OnInit } from '@angular/core';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { SystemParameter } from '../../shared/system-parameters';
import { AuthenticationService } from '../authentication.service';

@Component({
	selector: 'nuc-session-info',
	templateUrl: './session-info.component.html',
	styleUrls: ['./session-info.component.css']
})
export class SessionInfoComponent implements OnInit {

	displayData: Array<{ key, value }>;

	constructor(
		private configService: NuclosConfigService,
		private authenticationService: AuthenticationService
	) {
	}

	ngOnInit() {
		this.configService.getSystemParameters().subscribe(params => {
			if (params.is(SystemParameter.ENVIRONMENT_DEVELOPMENT)) {
				let authentication = this.authenticationService.getAuthentication();
				this.displayData = [];

				if (!authentication) {
					return;
				}

				for (let i in authentication) {
					if (authentication.hasOwnProperty(i)) {
						let value = JSON.stringify(authentication[i]);
						this.displayData.push({key: i, value: value});
					}
				}
			}
		});
	}

}
