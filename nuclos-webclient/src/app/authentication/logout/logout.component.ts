import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';

@Component({
	selector: 'nuc-logout',
	templateUrl: './logout.component.html',
	styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

	constructor(private authenticationService: AuthenticationService) {
	}

	ngOnInit() {
		this.authenticationService.logout().subscribe();
	}
}
