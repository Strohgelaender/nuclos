import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { WsTab } from './explorertrees.model';

@Injectable()
export class ExplorerTreesService {

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService
	) {
	}

	getMenuSelector(): Observable<WsTab[]> {

		return this.http
			.get(this.nuclosConfig.getRestHost() + '/meta/sideviewmenuselector', {withCredentials: true}).pipe(
			map((response: Response) => response.json()));
	}

}
