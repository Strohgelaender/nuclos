import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WsTab } from './explorertrees.model';
import { ExplorerTreesService } from './explorertrees.service';

@Component({
	selector: 'nuc-explorertrees',
	templateUrl: './explorertrees.component.html',
	styleUrls: ['./explorertrees.component.css']
})
export class ExplorerTreesComponent implements OnInit {

	@Input()
	selected: WsTab;

	@Output()
	onSelect: EventEmitter<WsTab> = new EventEmitter<WsTab>();

	private tabs: WsTab[];


	/**
	 */

	constructor(
		private explorerTreesService: ExplorerTreesService,
	) {
	}

	ngOnInit() {
		this.explorerTreesService.getMenuSelector().subscribe(tabs => this.tabs = tabs);
	}

	getWsTabs() {
		return this.tabs;
	}

	getExplorerTabs() {
		return this.tabs && this.tabs.filter(tab => tab.type === 'explorer');
	}

	selectTab(tab: WsTab) {
		this.onSelect.emit(tab);
	}

}
