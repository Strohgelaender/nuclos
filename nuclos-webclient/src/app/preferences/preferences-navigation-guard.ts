import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication';
import { UserAction } from '@nuclos/nuclos-addon-api';

export class PreferencesNavigationGuard implements CanActivate {

	constructor(private authenticationService: AuthenticationService) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		return new Observable(observer => {
			this.authenticationService.onAuthenticationDataAvailable().subscribe(loginSuccessful => {
				// NUCLOS-6689
				let sharingAllowed = loginSuccessful && this.authenticationService.isActionAllowed(UserAction.SharePreferences);
				observer.next(sharingAllowed);
			});
		});
	}
}
