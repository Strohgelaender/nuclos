import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { AuthenticationService } from '../authentication';
import { GridModule } from '../grid/grid.module';
import { I18nModule } from '../i18n/i18n.module';
import { ListModule } from '../list/list.module';
import { EditRowRendererComponent } from './cell-renderer/edit-row-renderer/edit-row-renderer.component';
import { SharedRendererComponent } from './cell-renderer/shared-renderer/shared-renderer.component';
import { TypeRendererComponent } from './cell-renderer/type-renderer/type-renderer.component';
import { SharedFilterComponent } from './filter/shared-filter/shared-filter.component';
import { PreferencesAdminService } from './preferences-admin.service';
import { PreferencesNavigationGuard } from './preferences-navigation-guard';
import { PreferencesComponent } from './preferences.component';
import { PreferencesRoutes } from './preferences.routes';
import { PreferencesService } from './preferences.service';

export function preferencesNavGuardFactory(authenticationService: AuthenticationService) {
	return new PreferencesNavigationGuard(authenticationService);
}


@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		PrettyJsonModule,

		AgGridModule.withComponents([
			SharedRendererComponent,
			TypeRendererComponent,
			EditRowRendererComponent
		]),

		I18nModule,
		ListModule,
		GridModule,

		PreferencesRoutes
	],
	declarations: [
		SharedRendererComponent,
		TypeRendererComponent,
		EditRowRendererComponent,
		PreferencesComponent,
		SharedFilterComponent
	],
	providers: [
		PreferencesAdminService,
		PreferencesService,
		{
			provide: PreferencesNavigationGuard,
			useFactory: preferencesNavGuardFactory,
			deps: [
				AuthenticationService
			]
		},
	],
	entryComponents: [
		SharedFilterComponent
	]
})
export class PreferencesModule {
}
