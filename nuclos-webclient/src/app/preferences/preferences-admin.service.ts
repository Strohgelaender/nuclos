import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LayoutService } from '../layout/shared/layout.service';
import { Logger } from '../log/shared/logger';
import { Preference } from './preferences.model';
import { PreferencesService } from './preferences.service';

@Injectable()
export class PreferencesAdminService {

	preferencesItems: BehaviorSubject<Preference<any>[]> = new BehaviorSubject([]);
	selectedPreferenceItem: Preference<any> | undefined;

	constructor(private preferences: PreferencesService,
				private layoutService: LayoutService,
				private $log: Logger) {
	}

	deleteCustomizedPreferenceItem(preferenceItem: Preference<any>): void {
		this.preferences.deleteCustomizedPreferenceItem(preferenceItem).subscribe(
			() => {
				if (preferenceItem.prefId) {
					this.preferences.getPreference(preferenceItem.prefId).subscribe(
						pref => {
							// update pref in list
							let prefInList = this.preferencesItems.getValue().filter(p => p.prefId === preferenceItem.prefId).shift();
							if (prefInList) {
								prefInList.shared = pref.shared;
								prefInList.customized = pref.customized;
							}
						}
					);
				}
			}
		);
	}

	updatePreferenceShare(preferenceItem: Preference<any>): void {
		this.preferences.updatePreferenceShare(preferenceItem).subscribe(
			() => {
				this.preferencesItems.getValue().filter(p => p.prefId === preferenceItem.prefId)[0].customized = false;
			}
		);
	}

	loadPreferences(): BehaviorSubject<Preference<any>[]> {

		this.preferences.getPreferences({}).subscribe(
			preferences => {
				let promises: Promise<any>[] = [];
				preferences.map((item: Preference<any>) => {
					if (item.boMetaId) {
						item.boName = this.getBoName(item);
					}
					if (this.selectedPreferenceItem && item.prefId === this.selectedPreferenceItem.prefId) {
						this.selectedPreferenceItem = item;
					}
					if (item.layoutId) {
						let promise = this.layoutService.getLayoutInfo(item.layoutId).toPromise().then(
							layoutInfo => {
								item.layoutName = layoutInfo.name;
							},
							error => {
								let message = 'Unable to get layout info for layout id \'' + item.layoutId + '\'.';
								this.$log.warn(message, error);
								item.layoutName = message;
							});
						promises.push(promise);
					}
				});

				Promise.all(promises).then(() => {
					this.preferencesItems.next(preferences)
				});
			}
		);
		return this.preferencesItems;
	}

	private getBoName(item: Preference<any>): string {
		return item.boMetaId.substring(item.boMetaId.lastIndexOf('_') + 1);
	}

}
