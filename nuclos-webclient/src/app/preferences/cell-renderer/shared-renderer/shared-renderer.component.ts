import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { Preference } from '../../preferences.model';

@Component({
	selector: 'nuc-shared-renderer',
	templateUrl: 'shared-renderer.component.html',
	styleUrls: ['shared-renderer.component.css']
})
export class SharedRendererComponent implements AgRendererComponent {

	preferenceItem: Preference<any>;

	constructor() {
	}

	agInit(params: any) {
		this.preferenceItem = params.value;
	}

	refresh(params: any): boolean {
		return false;
	}
}
