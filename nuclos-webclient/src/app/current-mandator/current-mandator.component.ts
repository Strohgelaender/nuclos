import { Component, OnInit } from '@angular/core';
import { AuthenticationService, MandatorData } from '../authentication/authentication.service';

@Component({
	selector: 'nuc-current-mandator',
	templateUrl: './current-mandator.component.html',
	styleUrls: ['./current-mandator.component.css']
})
export class CurrentMandatorComponent implements OnInit {

	currentMandator: MandatorData | undefined;

	constructor(private authenticationService: AuthenticationService) {

	}

	ngOnInit() {
		this.authenticationService.observeLoginStatus().subscribe(() => {
			let authData = this.authenticationService.getAuthentication();
			if (authData) {
				if (authData.mandator) {
					this.currentMandator = authData.mandator;
				}
			}

		});
		this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
			if (!loggedIn) {
				this.currentMandator = undefined;
			}
		});
	}
}
