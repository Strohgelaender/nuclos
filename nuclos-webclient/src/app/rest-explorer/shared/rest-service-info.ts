export class HttpMethod {
	static GET = 'GET';
	static PUT = 'PUT';
	static POST = 'POST';
	static DELETE = 'DELETE';
}

export class RestReturnType {
	static JsonObject = 'JsonObject';
	static Response = 'Response';
}

export class RestServiceInfo {
	index: number;
	description: string;
	finalized: boolean;
	validateSession: boolean;
	method: HttpMethod;
	path: string;
	returnType: RestReturnType;
	examplePostData: string;
	pathParams?: string[];
}

export class RestServiceCall {
	path: string;
	postData: string;
	restServiceInfo: RestServiceInfo;
}

export interface RestServiceInfoCombined {
	path: string;
	restServiceInfos: RestServiceInfo[];
}
