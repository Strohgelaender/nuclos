import { Injectable } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { RestServiceCall, RestServiceInfo, RestServiceInfoCombined } from './rest-service-info';

export class RestCall extends RestServiceInfo {
	date;
	pathParams;
	postData;
	requestParams;
}

@Injectable()
export class RestExplorerService {

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService
	) {
	}


	restServices(): Observable<RestServiceInfo[]> {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/meta/restservices'
		).pipe(
			map((response: Response) => response.json()),

			// get only the list - ignore wrapper
			map((json) => json.services),

			// calculate path params
			map((restServiceList) => {
				for (let service of restServiceList) {
					service.basePath = service.path;
					if (service.path != null && service.path.indexOf('{') !== -1) {
						service.basePath = service.path.substring(0, service.path.indexOf('{'));
						// pathParams
						let tokens = service.path.split(/({.*?})/);
						service.pathParams = [];
						if (tokens != null) {
							for (let token of tokens) {
								if (token && token.indexOf('{') === 0) {
									service.pathParams.push(token.substring(1, token.length - 1));
								}
							}
						}
					}
				}
				return restServiceList;
			}),
		);
	}


	/**
	 * aggregates restservices with same path but different HTTP methods
	 */
	restructure(restServiceList: RestServiceInfo[]): RestServiceInfoCombined[] {
		let items: Map<string, RestServiceInfoCombined> = new Map();

		for (let restService of restServiceList) {
			let key = restService.path;
			if (items.has(key)) {
				let item = items.get(key);
				if (item) {
					item.restServiceInfos.push(restService);
				}
			} else {
				let restServiceInfoCombined: RestServiceInfoCombined = {
					path: restService.path,
					restServiceInfos: [restService]
				};
				items.set(restService.path, restServiceInfoCombined);
			}
		}

		return Array.from(items.values());
	}


	executeRestService(restServiceCall: RestServiceCall)/*: Observable<RestServiceInfo[]> */ {
		let httpMethod = restServiceCall.restServiceInfo.method.toString().toLowerCase();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');

		let restCall: Observable<Response>;
		if (httpMethod === 'get' || httpMethod === 'delete') {
			restCall = this.http[httpMethod](this.nuclosConfig.getRestHost() + restServiceCall.path, {
				headers: headers
			});
		} else {
			restCall = this.http[httpMethod](this.nuclosConfig.getRestHost() + restServiceCall.path, restServiceCall.postData, {
				headers: headers
			});
		}

		return restCall.pipe(
			map((response: Response) => {
				let result;
				try {
					result = response.json();
				} catch (error) {
					// response is not in JSON format
					result = response.text();
				}
				return result;
			}));
	}

	getPathRegex(path) {
		return path
				.replace(/\{boMetaId\}/g, '[a-zA-Z_]*')
				.replace(/\{boAttrId\}/g, '[a-zA-Z_]*')
				.replace(/\{boId\}/g, '[0-9]*')
				.replace(/\{pk\}/g, '[0-9]*')
				.replace(/\{stateId\}/g, '[0-9]*')
				.replace(new RegExp('{[a-zA-Z0-9_]*}'), '[a-zA-Z0-9_]*')
			+ '$'
			;
	}

	findRestCall(url: string, restServices: RestServiceInfo[]): RestCall | undefined {

		// let restHost = this.nuclosConfig.getRestHost();
		let method = 'GET';
		let postData = undefined;
		let requestParams = undefined;

		let path = url.substring(url.indexOf('/rest') +  5);
		if (restServices != null) {
			let matchedCallEntries: any[] = [];
			for (let service of restServices) {
				let serviceBasePath = service.path;
				if (service.path != null && service.path.indexOf('{') !== -1) {
					serviceBasePath = service.path.substring(0, service.path.indexOf('{'));
				}

				if (
					path.indexOf(serviceBasePath) !== -1 // same path without parameters
					&& path.split('/').length === service.path.split('/').length // same number of parameters
					&& new RegExp(this.getPathRegex(service.path)).test(path) // match regex
					&& ('' + service.method).toLocaleLowerCase() === method.toLocaleLowerCase() // same HTTP method
				) { // matched
					let paramString = path.substring(serviceBasePath.length);
					let pathParams: any[] = [];
					let paramStringToken = paramString.split('\/');
					let paramIndex = 0;
					for (let p in paramStringToken) {
						// check if this token is a parameter or something like dependence in /rest/bo/{boMetaId}/{boId}/dependence/{reffield}
						if (this.getNonParamPathTokens(service.path).indexOf(paramStringToken[p]) === -1 && service.pathParams != null) {
							let param = {
								name: service.pathParams[paramIndex],
								value: paramStringToken[p]
							};
							pathParams.push(param);
							paramIndex++;
						}
					}

					if (requestParams === null) {
						requestParams = undefined;
					}

					let callEntry = new RestCall();
					callEntry.path = service.path;
					callEntry.date = new Date();
					callEntry.pathParams = pathParams;
					callEntry.postData = postData;
					callEntry.method = method;
					callEntry.requestParams = requestParams;

					matchedCallEntries.push(callEntry);
				}
			}
			if (matchedCallEntries.length === 1) {
				return matchedCallEntries[0];
			} else {
				let subPath = path;
				while (subPath.length > 0) {
					subPath = subPath.substring(0, subPath.lastIndexOf('/'));
					for (let matchedCallEntry of matchedCallEntries) {
						if (matchedCallEntry.path.indexOf(subPath) === 0) {
							return matchedCallEntry;
						}
					}
				}

				console.warn('Found %i REST calls.', matchedCallEntries.length);
				console.warn(matchedCallEntries, url);
			}
		}
		return undefined;
	}

	getNonParamPathTokens(path) {
		let result: string[] = [];
		let pathTokens = path.split('/');
		for (let pathToken of pathTokens) {
			if (pathToken.length !== 0 && pathToken.indexOf('{') !== 0) {
				result.push(pathToken);
			}
		}
		return result;
	}

}

