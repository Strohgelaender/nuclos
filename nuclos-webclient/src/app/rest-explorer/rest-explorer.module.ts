import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ResizableModule } from 'angular-resizable-element';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { I18nModule } from '../i18n/i18n.module';
import { ListModule } from '../list/list.module';
import { LogModule } from '../log/log.module';
import { MenuModule } from '../menu/menu.module';
import { RestExplorerDetailComponent } from './rest-explorer-detail/rest-explorer-detail.component';
import { RestExplorerNavigationComponent } from './rest-explorer-navigation/rest-explorer-navigation.component';
import { RestExplorerComponent } from './rest-explorer.component';
import { RestExplorerRoutes } from './rest-explorer.routes';
import { RestExplorerService } from './shared/rest-explorer.service';
import { UrlPipe } from './shared/url.pipe';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
		ResizableModule,
		RestExplorerRoutes,
		MenuModule,
		ListModule,
		PrettyJsonModule,
		I18nModule,
		LogModule
	],
	declarations: [
		UrlPipe,
		RestExplorerComponent,
		RestExplorerNavigationComponent,
		RestExplorerDetailComponent
	],
	exports: [
		UrlPipe
	],
	providers: [
		RestExplorerService
	]
})
export class RestExplorerModule {
}
