import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ResizeEvent } from 'angular-resizable-element';
import { RestServiceInfoCombined } from '../shared/rest-service-info';

@Component({
	selector: 'nuc-rest-explorer-navigation',
	templateUrl: './rest-explorer-navigation.component.html',
	styleUrls: ['../rest-explorer.component.css', './rest-explorer-navigation.component.css']
})
export class RestExplorerNavigationComponent {

	@Input() restServicesCombined: RestServiceInfoCombined[];

	@Output() widthChanged = new EventEmitter();
	style: Object = {};

	constructor() {
	}

	onResize(event: ResizeEvent): void {
		this.widthChanged.emit(event.rectangle.width);
	}

	onResizeEnd(event: ResizeEvent): void {
		this.widthChanged.emit(event.rectangle.width);
	}

}
