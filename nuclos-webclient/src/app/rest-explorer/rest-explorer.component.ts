import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { merge as observableMerge } from 'rxjs';
import { Logger } from '../log/shared/logger';
import { RestExplorerService } from './shared/rest-explorer.service';
import { RestServiceInfo, RestServiceInfoCombined } from './shared/rest-service-info';

@Component({
	selector: 'nuc-rest-explorer',
	templateUrl: './rest-explorer.component.html',
	styleUrls: ['./rest-explorer.component.css']
})
export class RestExplorerComponent implements OnInit {

	restServicesCombined: RestServiceInfoCombined[];
	selectedRestServiceCombined: RestServiceInfoCombined;

	selectedRestService: RestServiceInfo;
	previewUrl: string;

	// used for resizing sidebar
	navigationStyle: CSSStyleDeclaration = {maxWidth: '300px'} as CSSStyleDeclaration;
	wrapperStyle: CSSStyleDeclaration = {paddingLeft: '300px'} as CSSStyleDeclaration;

	constructor(
		@Inject(DOCUMENT) private document: any,
		private route: ActivatedRoute,
		private restExplorerService: RestExplorerService,
		private $log: Logger
	) {
	}

	ngOnInit() {
		this.restExplorerService.restServices().subscribe(
			(restServices) => {

				restServices = restServices.map((item, index) => {
					// add index to each entry
					item.index = index;
					return item;
				});

				this.restServicesCombined = this.restExplorerService.restructure(restServices);

				/*
				 * restservice can be selected via
				 * - index path parm (click on sidebar / url)
				 * 	e.g. /restexplorer/3
				 * - url query param
				 *  e.g. /restexplorer/preview?url=http://.../rest/bos
				 */

				observableMerge(this.route.params, this.route.queryParams).subscribe(param => {
					let restServiceInfo: RestServiceInfo | undefined;
					if (param['url']) {
						restServiceInfo = this.restExplorerService.findRestCall(param['url'], restServices);
						this.previewUrl = param['url'];
						this.$log.debug('Selecting restservice via url', param, restServiceInfo);
					} else if (param['index']) {
						restServiceInfo = restServices[param['index']];
						this.$log.debug('Selecting restservice via index', param, restServiceInfo);
					}
					if (restServiceInfo) {
						this.selectRestService(restServiceInfo);
					}
				});
			}
		);
	}

	selectRestService(restService: RestServiceInfo): void {
		this.selectedRestServiceCombined = this.restServicesCombined.filter(rs => rs.path === restService.path)[0];
		this.selectedRestService = restService;

		// for mobile devices: scroll to content
		this.document.getElementsByTagName('body')[0].scrollLeft = 1000;
		this.document.getElementsByTagName('body')[0].scrollTop = 0;
	}

	widthChanged(width: number): void {
		this.navigationStyle.maxWidth = width + 'px';
		this.navigationStyle.width = width + 'px';
		this.wrapperStyle.paddingLeft = width + 'px';
	}

}
