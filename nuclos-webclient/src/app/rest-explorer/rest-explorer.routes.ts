import { RouterModule, Routes } from '@angular/router';
import { RestExplorerComponent } from './rest-explorer.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: '',
		redirectTo: '0',
		pathMatch: 'full'
	},
	{
		path: ':index',
		component: RestExplorerComponent
	},
	{
		path: 'preview',
		component: RestExplorerComponent
	}
];

export const RestExplorerRoutes = RouterModule.forChild(ROUTE_CONFIG);
