import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { RestServiceInfo } from '../shared/rest-service-info';
import { RestExplorerService } from '../shared/rest-explorer.service';

interface Param {
	name: string;
	value: string;
	placeholder?: string;
}

/**
 * contains param or label
 */
interface PathElement {
	param?: Param;
	name?: string;
	isSeparator?: boolean;
}

@Component({
	selector: 'nuc-rest-explorer-detail',
	templateUrl: './rest-explorer-detail.component.html',
	styleUrls: ['../rest-explorer.component.css', './rest-explorer-detail.component.css']
})
export class RestExplorerDetailComponent implements OnInit, OnChanges {

	@Input() restService: RestServiceInfo;
	@Input() previewUrl: string;

	pathElements: PathElement[];
	pathParams: Param[];
	postData: string;
	response: string;
	error;
	validate;

	constructor(private restExplorerService: RestExplorerService) {
	}

	ngOnInit() {
	}


	ngOnChanges() {
		if (this.restService) {
			delete this.error;
			delete this.response;
			this.pathParams = this.extractPathParamsFromRestService(this.restService);
			this.pathElements = this.getPathElementsFromPath(
				'/rest' + this.restService.path,
				this.previewUrl ? this.restService.pathParams : this.pathParams
			);
			this.postData = this.restService.examplePostData;
		}
	}


	private getTranslatedPath(path: string, pathParams: PathElement[]): string {
		let translatedPath = path;
		for (let pathElement of pathParams) {
			if (pathElement.param) {
				translatedPath = translatedPath.replace('{' + pathElement.param.name + '}', pathElement.param.value);
			}
		}
		return translatedPath;
	}

	executeRestService() {
		delete this.response;
		let translatedPath = this.getTranslatedPath(this.restService.path, this.pathElements);
		this.restExplorerService.executeRestService(
			{
				path: translatedPath,
				postData: this.postData,
				restServiceInfo: this.restService
			}
		).subscribe(
			response => {
				this.response = response;
			},
			error => {
				this.error = error;
			}
		);
	}



	private getPathElementsFromPath(s: string, pathParams): PathElement[] {
		let tokens = s.match(/{.+?}/g);
		let tokenArray: PathElement[] = [];
		if (tokens) {
			for (let i = 0; i < tokens.length; ++i) {
				let token = tokens[i].substring(1).substring(0, tokens[i].length - 2);
				let index = s.indexOf(tokens[i]);
				let staticPartOfPath = s.substring(0, index);
				tokenArray.push({name: staticPartOfPath, isSeparator: staticPartOfPath.length === 1});
				s = s.substring(index + tokens[i].length);

				let val;
				for (let param of pathParams) {
					param.placeholder = '{' + param.name + '}';
					if (param.name === token) {
						val = param;
						break;
					}
				}
				if (val != null && val.length !== 0) {
					tokenArray.push({param: val});
				}
			}
			if (s.length !== 0) {
				tokenArray.push({name: s});
			}
		} else {
			tokenArray.push({name: s});
		}
		return tokenArray;
	}

	private extractPathParamsFromRestService(restService: RestServiceInfo) {
		let tokens = restService.path.match(/{.+?}/g);
		let pathParams: any[] = [];
		if (tokens) {
			for (let i = 0; i < tokens.length; ++i) {
				let token = tokens[i].substring(1).substring(0, tokens[i].length - 2);
				let tokenExists = false;
				for (let j in pathParams) {
					if (pathParams[j].name === token) {
						tokenExists = true;
						break;
					}
				}
				if (!tokenExists) {
					let elem = {name: token, value: ''};
					pathParams.push(elem);
				}
			}
		}
		return pathParams;
	}
}
