import { Injectable } from '@angular/core';
import { EMPTY, Observable, of as observableOf } from 'rxjs';

import { catchError, map, mergeMap } from 'rxjs/operators';
import { EntityMeta, EntityMetaData } from '../../entity-object-data/shared/bo-view.model';
import { DataService } from '../../entity-object-data/shared/data.service';
import { EntityObjectService } from '../../entity-object-data/shared/entity-object.service';
import { MetaService } from '../../entity-object-data/shared/meta.service';
import { ResultParams } from '../../entity-object-data/shared/result-params';
import { Logger } from '../../log/shared/logger';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { TaskList } from './task-list';

@Injectable()
export class TaskService {

	constructor(
		private config: NuclosConfigService,
		private httpService: NuclosHttpService,
		private $log: Logger,
		private metaService: MetaService,
		// TODO: Refactor to some EO-data service
		private dataService: DataService,
		// TODO: Only injected here to make sure it is instantiated
		private eoService: EntityObjectService
	) {
	}

	/**
	 * Does not return complete entity meta data!
	 */
	getTaskListDefinitions(): Observable<EntityMetaData[]> {
		return this.httpService.getCachedJSON(this.config.getRestHost() + '/meta/tasklists');
	}

	getTaskListDefinition(taskListFqn: string): Observable<EntityMeta> {
		return this.getTaskListDefinitions().pipe(map(
			metas => {
				let taskListMeta = metas.find(
					meta => meta.boMetaId === taskListFqn
				);

				if (!taskListMeta) {
					throw new Error('Could not find task list meta for ' + taskListFqn);
				}

				return new EntityMeta(taskListMeta);
			}
		));
	}

	getTaskData(taskList: TaskList) {
		return this.getTaskListMeta(taskList).pipe(mergeMap(
			meta => {
				let params: ResultParams = {
					offset: 0,
					chunkSize: 10,
					countTotal: false
				};

				// Only search filter based task lists have a search filter
				if (taskList.searchfilter) {
					params.searchFilterId = taskList.searchfilter;
				}

				return this.dataService.loadEoData(
					meta,
					params
				);
			}
		));
	}

	/**
	 * Returns a complete entity meta for the task list entity.
	 */
	getTaskListMeta(taskList: TaskList) {
		return this.getTaskListMetaByFqn(taskList.entityClassId);
	}

	getTaskListMetaByFqn(taskListFqn: string) {
		return this.metaService.getBoMeta(taskListFqn);
	}

	/**
	 * TODO: Should be refactored later to some kind of push notification from the server (Websocket).
	 */
	getTaskCountSince(taskList: TaskList, since: Date) {
		return this.getTaskListMeta(taskList).pipe(
			mergeMap(meta => {
				// TODO: This method only works for search filter based task lists at the moment
				if (!taskList.searchfilter) {
					return observableOf(0);
				}

				let params: ResultParams = {
					offset: 0,
					chunkSize: 0,
					countTotal: true,
					searchFilterId: taskList.searchfilter
				};

				return this.metaService.getEntityMeta(meta.getEntityClassId()).pipe(
					mergeMap(entityMeta =>
						this.dataService.loadEoData(
							entityMeta,
							params,
							{
								where: {
									aliases: {},
									clause: entityMeta.getAttribute('changedAt').boAttrId + ' > \'' + since.toISOString() + '\''
								}
							}
						).pipe(
							map(
								data => data.total
							),
							catchError(e => {
								this.$log.warn('Failed to get count for task list: %o', e);
								return EMPTY;
							}),
						)
					)
				);
			})
		);
	}
}
