import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EntityObjectDataModule } from '../entity-object-data/entity-object-data.module';
import { HttpModule } from '../http/http.module';
import { TaskService } from './shared/task.service';

@NgModule({
	imports: [
		CommonModule,

		EntityObjectDataModule,
		HttpModule,
	],
	declarations: [],
	providers: [
		TaskService
	]
})
export class TaskModule {
}
