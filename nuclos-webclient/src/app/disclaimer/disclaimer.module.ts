import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { I18nModule } from '../i18n/i18n.module';
import { CookieDisclaimerComponent } from './cookie-disclaimer/cookie-disclaimer.component';
import { DisclaimerMenuComponent } from './disclaimer-menu/disclaimer-menu.component';
import { DisclaimerRouteComponent } from './disclaimer-route/disclaimer-route.component';
import { DisclaimerRoutes } from './disclaimer.routes';
import { DisclaimerService } from './shared/disclaimer.service';

@NgModule({
	imports: [
		CommonModule,

		I18nModule,

		DisclaimerRoutes
	],
	declarations: [
		DisclaimerMenuComponent,
		CookieDisclaimerComponent,
		DisclaimerRouteComponent
	],
	exports: [
		DisclaimerMenuComponent,
		CookieDisclaimerComponent,
		DisclaimerRouteComponent
	],
	providers: [
		DisclaimerService
	]
})
export class DisclaimerModule {
}
