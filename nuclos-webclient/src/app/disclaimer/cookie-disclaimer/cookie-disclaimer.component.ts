import { Component, OnInit } from '@angular/core';
import { NuclosCookieService } from '../../cookie/shared/nuclos-cookie.service';
import { DisclaimerService } from '../shared/disclaimer.service';

@Component({
	selector: 'nuc-cookie-disclaimer',
	templateUrl: './cookie-disclaimer.component.html',
	styleUrls: ['./cookie-disclaimer.component.css']
})
export class CookieDisclaimerComponent implements OnInit {
	private cookiesAccepted = false;
	private privacyDisclaimer: LegalDisclaimer | undefined;

	constructor(
		private disclaimerService: DisclaimerService,
		private cookieService: NuclosCookieService
	) {
	}

	ngOnInit() {
		this.cookiesAccepted = this.cookieService.get('cookiesaccepted') === 'true';
		this.disclaimerService.getPrivacyDisclaimer().subscribe(
			disclaimer => this.privacyDisclaimer = disclaimer
		);
	}

	displayCookieDisclaimer() {
		return this.privacyDisclaimer && !this.cookiesAccepted;
	}

	showPrivacyContent() {
		if (this.privacyDisclaimer) {
			this.disclaimerService.showDisclaimer(this.privacyDisclaimer);
		}
	}

	acceptCookies() {
		this.cookiesAccepted = true;
		this.cookieService.put('cookiesaccepted', 'true');
	}
}
