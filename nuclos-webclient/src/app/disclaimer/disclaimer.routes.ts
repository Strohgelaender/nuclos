import { RouterModule, Routes } from '@angular/router';
import { CanNeverActivateGuard } from '../shared/can-never-activate-guard';
import { DisclaimerRouteComponent } from './disclaimer-route/disclaimer-route.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'disclaimer/:name',
		component: DisclaimerRouteComponent,
		canActivate: [
			CanNeverActivateGuard
		]
	},
];
//
export const DisclaimerRoutes = RouterModule.forChild(ROUTE_CONFIG);
