import { inject, TestBed } from '@angular/core/testing';
import { DisclaimerService } from './disclaimer.service';

xdescribe('DisclaimerService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [DisclaimerService]
		});
	});

	it('should ...', inject([DisclaimerService], (service: DisclaimerService) => {
		expect(service).toBeTruthy();
	}));
});
