import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisclaimerRouteComponent } from './disclaimer-route.component';

xdescribe('DisclaimerRouteComponent', () => {
	let component: DisclaimerRouteComponent;
	let fixture: ComponentFixture<DisclaimerRouteComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DisclaimerRouteComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DisclaimerRouteComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
