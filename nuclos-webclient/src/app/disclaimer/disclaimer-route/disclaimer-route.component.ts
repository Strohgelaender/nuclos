import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { DisclaimerService } from '../shared/disclaimer.service';

@Component({
	selector: 'nuc-disclaimer-route',
	templateUrl: './disclaimer-route.component.html',
	styleUrls: ['./disclaimer-route.component.css']
})
export class DisclaimerRouteComponent implements OnInit {

	constructor(
		private disclaimerService: DisclaimerService,
		private router: Router
	) {
	}

	ngOnInit() {
		this.router.events.pipe(
			filter(e => e instanceof NavigationStart))
			.subscribe((event: NavigationStart) => this.showDisclaimer(event.url));
	}

	private showDisclaimer(url: string) {
		let disclaimerName = this.getDisclaimerName(url);

		if (disclaimerName) {
			this.disclaimerService.showDisclaimerByName(disclaimerName);
		}
	}

	private getDisclaimerName(url: string) {
		let result: string | undefined = undefined;
		let regex = /\/disclaimer\/(.+?)(?:\/|$)/;

		let regexResult = regex.exec(url);
		if (regexResult) {
			result = regexResult[1];
		}

		return result;
	}
}
