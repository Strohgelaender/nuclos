import { Component, OnInit } from '@angular/core';
import { DisclaimerService } from '../shared/disclaimer.service';

@Component({
	selector: 'nuc-disclaimer-menu',
	templateUrl: './disclaimer-menu.component.html',
	styleUrls: ['./disclaimer-menu.component.css']
})
export class DisclaimerMenuComponent implements OnInit {
	legalDisclaimers: LegalDisclaimer[];

	constructor(private disclaimerService: DisclaimerService) {
	}

	ngOnInit() {
		this.disclaimerService.getLegalDisclaimers().subscribe(
			data => {
				if (data.disclaimers && data.disclaimers.length > 0) {
					this.legalDisclaimers = data.disclaimers;
				}
			}
		);
	}

	showDisclaimer(disclaimer) {
		this.disclaimerService.showDisclaimer(disclaimer);
	}
}
