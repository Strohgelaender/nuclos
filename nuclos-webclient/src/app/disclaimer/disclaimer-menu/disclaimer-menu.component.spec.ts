import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DisclaimerMenuComponent } from './disclaimer-menu.component';

xdescribe('DisclaimerMenuComponent', () => {
	let component: DisclaimerMenuComponent;
	let fixture: ComponentFixture<DisclaimerMenuComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DisclaimerMenuComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DisclaimerMenuComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
