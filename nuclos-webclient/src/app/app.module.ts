import { LOCATION_INITIALIZED } from '@angular/common';
import { APP_INITIALIZER, ErrorHandler, Injector, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DndModule } from 'ng2-dnd';
import { LoadingModule } from 'ngx-loading';
import { AccountModule } from './account/account.module';
import { AddonModules } from './addon.modules';
import { AppComponent } from './app.component';
import { AppRoutesModule } from './app.routes';
import { AuthenticationModule } from './authentication/authentication.module';
import { BusinesstestModule } from './businesstest/businesstest.module';
import { CacheModule } from './cache/cache.module';
import { ChartModule } from './chart/chart.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { DisclaimerModule } from './disclaimer/disclaimer.module';
import { EntityObjectModule } from './entity-object/entity-object.module';
import { WindowTitleService } from './entity-object/shared/window-title.service';
import { ErrorModule } from './error/error.module';
import { GlobalErrorHandlerService } from './global-error-handler.service';
import { HttpModule } from './http/http.module';
import { I18nModule } from './i18n/i18n.module';
import { LogModule } from './log/log.module';
import { MenuModule } from './menu/menu.module';
import { DialogModule } from './popup/dialog/dialog.module';
import { PopupModule } from './popup/popup.module';
import { PreferencesModule } from './preferences/preferences.module';
import { ServerInfoModule } from './server-info/server-info.module';
import { BrowserDetectionService } from './shared/browser-detection.service';
import { BrowserRefreshService } from './shared/browser-refresh.service';
import { BusyService } from './shared/busy.service';
import { CanNeverActivateGuard } from './shared/can-never-activate-guard';
import { DatasourceService } from './shared/datasource.service';
import { DatetimeService } from './shared/datetime.service';
import { FqnService } from './shared/fqn.service';
import { HyperlinkService } from './shared/hyperlink.service';
import { IdFactoryService } from './shared/id-factory.service';
import { MapEntriesPipe } from './shared/map-entries.pipe';
import { NuclosConfigService } from './shared/nuclos-config.service';
import { NumberService } from './shared/number.service';
import { UiComponentsModule } from './ui-components/ui-components.module';
import { ValidationModule } from './validation/validation.module';

export function configFactory(config: NuclosConfigService, injector: Injector) {
	// fix for https://github.com/angular/angular-cli/issues/5762
	return () => new Promise<any>((resolve: any) => {
		const locationInitialized = injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
		locationInitialized.then(() => {
			config.load().then(() => resolve(null));
		});
	});
}

@NgModule({
	declarations: [
		AppComponent,
		MapEntriesPipe
	],
	imports: [
		AddonModules,

		// Angular modules
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,

		DndModule.forRoot(),

		// Nuclos modules
		AccountModule,
		AuthenticationModule,
		BusinesstestModule,
		CacheModule,
		ChartModule,
		DashboardModule,
		DialogModule,
		DisclaimerModule,
		EntityObjectModule,
		ErrorModule,
		HttpModule,
		I18nModule,
		LoadingModule,
		LogModule,
		MenuModule,
		PopupModule,
		PreferencesModule,
		ServerInfoModule,
		UiComponentsModule,
		ValidationModule,

		// App Routes (contains Wildcard-Routes and must therefor be defined last)
		AppRoutesModule
	],
	providers: [
		BrowserRefreshService,
		BrowserDetectionService,
		BusyService,
		DatasourceService,
		FqnService,
		DatetimeService,
		NumberService,
		NuclosConfigService,
		HyperlinkService,
		{
			provide: APP_INITIALIZER,
			useFactory: configFactory,
			deps: [NuclosConfigService, Injector],
			multi: true
		},
		CanNeverActivateGuard,
		IdFactoryService,
		WindowTitleService,
		{
			provide: ErrorHandler,
			useClass: GlobalErrorHandlerService
		}
	],
	exports: [
		AppComponent
	],
	bootstrap: [
		AppComponent
	]
})
export class AppModule {
}
