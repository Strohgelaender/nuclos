import { Injectable } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { Observable, Subject, timer as observableTimer, zip as observableZip } from 'rxjs';

import { map } from 'rxjs/operators';
import { Logger } from '../../log/shared/logger';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { ServerLogMessage } from './server-log-message';

export interface LogConfig {
	level: SqlLoggingMode;
}

export interface DebugSql {
	debugSQL: string;
	minExecTime: number;
}

export class SqlLoggingMode {
	static readonly Disabled = 'Disabled';
	static readonly SQLLogger = 'SQLLogger';
	static readonly SQLTimerLogger = 'SQLTimer';
	static readonly SQLUpdateLogger = 'SQLUpdate';
}

@Injectable()
export class ServerLogService {
	private request: Atmosphere.Request;
	private serverLogMessage: Subject<ServerLogMessage>;

	private messageBuffer: Array<ServerLogMessage>;
	private serverLogMessages: Subject<Array<ServerLogMessage>>;

	private subscribed = false;

	constructor(
		private configService: NuclosConfigService,
		private http: NuclosHttpService,
		private $log: Logger
	) {
		this.serverLogMessage = new Subject<ServerLogMessage>();

		this.messageBuffer = [];
		this.serverLogMessages = new Subject<Array<ServerLogMessage>>();

		// We are now ready to cut the request
		this.request = {
			url: this.configService.getServerLogWebsocketURL(),
			contentType: 'application/json',
			trackMessageLength: false,
			shared: true,
			transport: 'websocket',
			fallbackTransport: 'long-polling',
			reconnect: false,
			maxReconnectOnClose: 3
		};

		this.request.onOpen = (response: Atmosphere.Response) => {
			this.$log.debug('onOpen: %o', response);
		};

		this.request.onMessage = (response: Atmosphere.Response) => {
			if (!response.responseBody) {
				return;
			}
			let message: ServerLogMessage = JSON.parse(response.responseBody, (_key, value) => {
				let reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
				let reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;
				// first, just make sure the property is a string:
				if (typeof value === 'string') {
					// then, use regex to see if it's an ISO-formatted string
					let a = reISO.exec(value);
					if (a) {
						// if so, Date() can parse it:
						return new Date(value);
					}
					// otherwise, see if it's a wacky Microsoft-format string:
					a = reMsAjax.exec(value);
					if (a) {
						// and perform some jujitsu to make use of it:
						let b = a[1].split(/[-+,.]/);
						return new Date(b[0] ? +b[0] : 0 - +b[1]);
					}
					// here, you could insert any additional tests and parse instructions you like, for other date syntaxes...
				}
				// important: you need to return any values you're not parsing, or they die...
				return value;
			});
			this.messageBuffer.push(message);
			this.serverLogMessage.next(message);
		};

		this.request.onReconnect = (req: Atmosphere.Request, response: Atmosphere.Response) => {
			this.$log.debug('onReconnect: %o, %o', req, response);
		};

		observableTimer(500, 1000).subscribe(
			() => {
				this.serverLogMessages.next(this.messageBuffer);
				this.messageBuffer = [];
			}
		);
	}

	/**
	 * Emits every received ServerLogMessage immediately.
	 *
	 * @returns {Subject<ServerLogMessage>}
	 */
	getServerLog(): Observable<ServerLogMessage> {
		this.subscribeToWebsocket();

		return this.serverLogMessage;
	}

	/**
	 * Buffers the messages and emits the buffer only once per second.
	 *
	 * @returns {Subject<Array<ServerLogMessage>>}
	 */
	getServerLogBuffered(): Observable<Array<ServerLogMessage>> {
		this.subscribeToWebsocket();

		return this.serverLogMessages;
	}

	/**
	 * Subscribes to the atmosphere server log websocket.
	 */
	private subscribeToWebsocket() {
		if (!this.subscribed) {
			(<any>atmosphere).subscribe(
				this.configService.getServerLogWebsocketURL(),
				() => {
				},
				this.request
			);
			this.subscribed = true;
		}
	}

	setLoggingMode(sqlLoggingMode): void {
		switch (sqlLoggingMode) {
			case (SqlLoggingMode.Disabled):
				this.setSqlLogging(SqlLoggingMode.SQLLogger, false).subscribe();
				this.setSqlLogging(SqlLoggingMode.SQLTimerLogger, false).subscribe();
				this.setSqlLogging(SqlLoggingMode.SQLUpdateLogger, false).subscribe();
				break;
			case (SqlLoggingMode.SQLLogger):
				this.setSqlLogging(SqlLoggingMode.SQLLogger, true).subscribe();
				this.setSqlLogging(SqlLoggingMode.SQLTimerLogger, false).subscribe();
				this.setSqlLogging(SqlLoggingMode.SQLUpdateLogger, false).subscribe();
				break;
			case (SqlLoggingMode.SQLTimerLogger):
				this.setSqlLogging(SqlLoggingMode.SQLTimerLogger, true).subscribe();
				this.setSqlLogging(SqlLoggingMode.SQLLogger, false).subscribe();
				this.setSqlLogging(SqlLoggingMode.SQLUpdateLogger, false).subscribe();
				break;
			case (SqlLoggingMode.SQLUpdateLogger):
				this.setSqlLogging(SqlLoggingMode.SQLUpdateLogger, true).subscribe();
				this.setSqlLogging(SqlLoggingMode.SQLTimerLogger, false).subscribe();
				this.setSqlLogging(SqlLoggingMode.SQLLogger, false).subscribe();
				break;
		}
	}

	getSqlLoggingMode(): Observable<SqlLoggingMode> {
		return observableZip(
			this.getSqlLogConfig(SqlLoggingMode.SQLLogger),
			this.getSqlLogConfig(SqlLoggingMode.SQLTimerLogger),
			this.getSqlLogConfig(SqlLoggingMode.SQLUpdateLogger)
		).pipe(
			map(configs => {
				if (configs[0].level === 'DEBUG') {
					return SqlLoggingMode.SQLLogger;
				} else if (configs[1].level === 'DEBUG') {
					return SqlLoggingMode.SQLTimerLogger;
				} else if (configs[2].level === 'DEBUG') {
					return SqlLoggingMode.SQLUpdateLogger;
				}
				return SqlLoggingMode.Disabled;
			}));
	}

	private getSqlLogConfig(logger): Observable<LogConfig> {
		return this.http
			.get(
				this.configService.getRestHost() + '/maintenance/logging/' + logger + '/level'
			).pipe(
			map((response: Response) => response.json()));
	}

	private setSqlLogging(logger: SqlLoggingMode, enabled: boolean): Observable<any> {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http
			.put(
				this.configService.getRestHost() + '/maintenance/logging/' + logger + '/level',
				JSON.stringify(
					{'level': enabled ? 'DEBUG' : 'INFO'}
				),
				{
					headers: headers
				}
			).pipe(
			map((response: Response) => response.json()));
	}

	getDebugSql(): Observable<DebugSql> {
		return this.http
			.get(
				this.configService.getRestHost() + '/maintenance/logging/debugSQL'
			).pipe(
			map((response: Response) => response.json()));
	}

	setDebugSqlString(debugSqlString: string, minExecTime: string) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		let postData: DebugSql = {
			debugSQL: debugSqlString,
			minExecTime: 0
		};
		if (minExecTime.length > 0) {
			postData.minExecTime = Number(minExecTime);
		}
		return this.http
			.put(
				this.configService.getRestHost() + '/maintenance/logging/debugSQL',
				JSON.stringify(postData),
				{
					headers: headers
				}
			).pipe(
			map((response: Response) => response.json()))
			.subscribe();

	}

	/**
	 * Returns the download URL for the complete server.log file.
	 *
	 * @returns {string}
	 */
	getDownloadLink() {
		return this.configService.getRestHost() + '/maintenance/logging/server.log';
	}
}
