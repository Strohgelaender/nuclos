import { Pipe, PipeTransform } from '@angular/core';

/**
 * filters ServerLogMessages by level and message
 */
@Pipe({
	name: 'serverLogFilter',
	pure: false
})
export class ServerLogFilterPipe implements PipeTransform {

	static readonly levels = [
		'ALL',
		'TRACE',
		'DEBUG',
		'INFO',
		'WARN',
		'ERROR',
		'FATAL',
		'OFF'
	];

	transform(value: any, args: any[]): any {
		let items = value;
		let filter = args;
		let filterLevelNumber: number = ServerLogFilterPipe.levels.indexOf(filter['level']);
		let filterMessage = filter['message'];

		return items.filter(item => {
			let itemLevelNumber: number = ServerLogFilterPipe.levels.indexOf(item.level);
			return itemLevelNumber >= filterLevelNumber && item.message.toLowerCase().indexOf(filterMessage) !== -1;
		});
	}
}



