import { Component, OnInit } from '@angular/core';
import { BusinesstestOverallResult } from '../../../../nuclos_typings/nuclos/businesstest';
import { BusinesstestService } from '../shared/businesstest.service';

@Component({
	selector: 'nuc-businesstest-overview',
	templateUrl: './businesstest-overview.component.html',
	styleUrls: ['./businesstest-overview.component.css']
})
export class BusinesstestOverviewComponent implements OnInit {
	overallResult: BusinesstestOverallResult | null;

	constructor(private businesstestService: BusinesstestService) {
	}

	ngOnInit() {
		this.businesstestService.getOverallResult().subscribe(result => this.overallResult = result);
		this.businesstestService.updateOverallResult().subscribe();
	}
}
