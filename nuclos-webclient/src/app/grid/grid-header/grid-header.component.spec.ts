import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridHeaderComponent } from './grid-header.component';

xdescribe('GridHeaderComponent', () => {
	let component: GridHeaderComponent;
	let fixture: ComponentFixture<GridHeaderComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GridHeaderComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GridHeaderComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
