import { Component } from '@angular/core';
import { IHeaderParams } from 'ag-grid';
import { IHeaderAngularComp } from 'ag-grid-angular';

@Component({
	selector: 'nuc-grid-header',
	templateUrl: './grid-header.component.html',
	styleUrls: ['./grid-header.component.css']
})
export class GridHeaderComponent implements IHeaderAngularComp {

	params: IHeaderParams;

	constructor() {
	}

	agInit(params: IHeaderParams): void {
		this.params = params;
	}

	sort() {
		let sortModel = this.params.api.getSortModel();

		this.addOrUpdateSortColumn(sortModel);

		this.params.api.setSortModel(sortModel);
	}

	private addOrUpdateSortColumn(sortModel: { colId: string; sort: string }[]) {
		let index = this.getSortIndex();
		if (index < 0) {
			let sort = {
				colId: this.params.column.getColId(),
				sort: 'asc'
			};
			sortModel.push(sort);
		} else {
			let sort = sortModel[index];
			if (sort.sort === 'asc') {
				sort.sort = 'desc';
			} else {
				sortModel.splice(index, 1);
			}
		}
	}

	getSortPriority() {
		return this.getSortIndex() + 1;
	}

	private getSortIndex() {
		return this.params.api.getSortModel().findIndex(
			sort => sort.colId === this.params.column.getColId()
		);
	}

	isMultiSort() {
		return this.params.api.getSortModel().length > 1;
	}
}
