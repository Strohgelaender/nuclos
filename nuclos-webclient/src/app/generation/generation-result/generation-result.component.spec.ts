/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GenerationResultComponent } from './generation-result.component';

xdescribe('GenerationResultComponent', () => {
	let component: GenerationResultComponent;
	let fixture: ComponentFixture<GenerationResultComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [GenerationResultComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GenerationResultComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
