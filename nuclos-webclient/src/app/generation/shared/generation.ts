import { EntityObjectData } from '../../entity-object-data/shared/bo-view.model';
import { Link } from '../../shared/link.model';

/**
 * Represents an object generator.
 */
export class Generation {
	generationId: string;
	name: string;
	target: string;
	nonstop: boolean;
	internal: boolean;

	links: {
		generate: Link;
	};
}

/**
 * Represents the result of a generation request to the REST service.
 */
export class GenerationResult {
	bo: EntityObjectData;
	complete: boolean;
	refreshSource: boolean;
	showGenerated: boolean;

	businessError?: string;
}
