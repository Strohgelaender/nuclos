import {
	Component,
	ComponentFactoryResolver,
	Input,
	OnDestroy,
	OnInit,
	ReflectiveInjector,
	Type,
	ViewChild,
	ViewContainerRef,
} from '@angular/core';
import { RESULTLIST_CONTEXT } from '@nuclos/nuclos-addon-api';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../authentication';
import { EntityObjectResultService } from '../../entity-object-data/shared/entity-object-result.service';
import { Logger } from '../../log/shared/logger';
import { AddonPosition } from '../addon';
import { ResultlistContextImplementation } from '../addon-api-implementation';
import { AddonRegistration, AddonService } from '../addon.service';

/**
 * instantiates addon components in the webclient
 */
@Component({
	selector: 'nuc-addon-executor',
	templateUrl: './addon-executor.component.html',
	styleUrls: ['./addon-executor.component.css']
})
export class AddonExecutorComponent implements OnInit, OnDestroy {

	@ViewChild('viewContainer', {read: ViewContainerRef}) viewContainerRef: ViewContainerRef;


	/**
	 * the place where the add-on is placed in the webclient
	 */
	@Input() addonPosition: AddonPosition;
	@Input() entityClassId: string | undefined;

	private subscriptions: Subscription[] = [];

	constructor(private resultlistContextImplementation: ResultlistContextImplementation,
				private componentFactoryResolver: ComponentFactoryResolver,
				private addonService: AddonService,
				private authenticationService: AuthenticationService,
				private entityObjectResultService: EntityObjectResultService,
				private $log: Logger) {
	}

	ngOnInit() {

		// adddon usage REST service can be consumed after authentication
		this.subscriptions.push(
			this.authenticationService.observeLoginStatus().subscribe(loggedin => {
				if (loggedin) {
					this.subscriptions.push(
						this.entityObjectResultService.observeSelectedEntityClassId().subscribe(
							entityClassId => {
								this.executeAddon(entityClassId);
							}
						)
					);
				}
			})
		);
	}

	private executeAddon(entityClassId) {
		this.viewContainerRef.clear();
		this.addonService.getAddonComponentRegistrationsForCurrentEntityClass(this.addonPosition).subscribe(addonComponentRegistrations => {
			if (addonComponentRegistrations) {
				for (let addonComponentRegistration of addonComponentRegistrations) {
					if (addonComponentRegistration
						&& addonComponentRegistration.entityClassId
						&& entityClassId === addonComponentRegistration.entityClassId) {
						this.instantiateAddonComponent(addonComponentRegistration);
					}
				}
			}
		});
	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

	private instantiateAddonComponent(addonRegistration: AddonRegistration) {

		// let factoryClasses: Type<Component>[] = Array.from(this.resolver['_factories'].keys());
		let factoryClasses = Array.from(this.componentFactoryResolver['_factories'].keys());
		let addOnComponentFactoryClass = factoryClasses.find((fa: any) => fa.name === addonRegistration.addonComponentName);
		if (addOnComponentFactoryClass) {
			const addonComponentFactory = this.componentFactoryResolver.resolveComponentFactory(addOnComponentFactoryClass as Type<Component>);

			this.resultlistContextImplementation.setAddonproperties(addonRegistration.properties);

			let inputProviders = [{ provide: RESULTLIST_CONTEXT, useValue: this.resultlistContextImplementation }];

			let resolvedInputs = ReflectiveInjector.resolve(inputProviders);
			let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.viewContainerRef.parentInjector);
			const cmpRef = addonComponentFactory.create(injector);

			// inject AddonContext
			this.viewContainerRef.insert(cmpRef.hostView);
		} else {
			this.$log.error('Unable to find addon \'%s\'.', addonRegistration.addonComponentName);
		}
	}

}
