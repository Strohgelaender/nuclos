import { Component, Input, NgZone, OnInit } from '@angular/core';
import { NuclosHttpService } from '../../shared/nuclos-http.service';

@Component({
	selector: 'nuc-loading-indicator',
	templateUrl: './loading-indicator.component.html',
	styleUrls: ['./loading-indicator.component.css']
})
export class LoadingIndicatorComponent implements OnInit {

	@Input()
	private thresholdInMilliseconds = 1000;

	imageSrc: string;

	private idleImageSrc = 'assets/nuclos-signet.png';
	private loadingImageSrc = 'assets/nuclos-rotating-48.gif';

	constructor(
		private http: NuclosHttpService,
		private ngZone: NgZone
	) {
		this.imageSrc = this.idleImageSrc;
	}

	ngOnInit() {
		// Updating the image every 100ms, because Angular change detection is not called often enough for this.
		this.ngZone.runOutsideAngular(() => {
			setInterval(() => {
					this.ngZone.run(() => this.updateImageSrc());
				},
				100
			);
		});
	}

	updateImageSrc() {
		if (this.showLoading()) {
			this.setImageSrc(this.loadingImageSrc);
		} else {
			this.setImageSrc(this.idleImageSrc);
		}
	}

	private showLoading() {
		let pendingSince = this.http.getPendingRequestTime();
		return pendingSince >= this.thresholdInMilliseconds;
	}

	setImageSrc(newSrc: string) {
		this.imageSrc = newSrc;
	}
}
