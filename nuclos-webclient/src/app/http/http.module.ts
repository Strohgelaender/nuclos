import { CommonModule } from '@angular/common';
import { Injector, NgModule } from '@angular/core';
import { HttpModule as AngularHttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { CacheModule } from '../cache/cache.module';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { LogModule } from '../log/log.module';
import { Logger } from '../log/shared/logger';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component';

export function httpFactory(
	backend: XHRBackend,
	options: RequestOptions,
	$log: Logger,
	cacheService: NuclosCacheService,
	injector: Injector
) {
	return new NuclosHttpService(backend, options, $log, cacheService, injector);
}

@NgModule({
	imports: [
		CommonModule,
		AngularHttpModule,

		LogModule,
		CacheModule,
	],
	declarations: [
		LoadingIndicatorComponent
	],
	exports: [
		LoadingIndicatorComponent
	],
	providers: [
		{
			provide: NuclosHttpService,
			useFactory: httpFactory,
			deps: [XHRBackend, RequestOptions, Logger, NuclosCacheService, Injector]
		},
	]
})
export class HttpModule {
}
