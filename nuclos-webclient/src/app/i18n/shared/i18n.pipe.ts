import { Pipe, PipeTransform } from '@angular/core';
import { NuclosI18nService } from './nuclos-i18n.service';

@Pipe({
	name: 'i18n'
})
export class I18nPipe implements PipeTransform {

	constructor(private nuclosI18N: NuclosI18nService) {
	}

	transform(value: any, ...params: any[]): any {
		if (value == null) {
			return null;
		}
		return this.nuclosI18N.getI18n(value, ...params);
	}
}
