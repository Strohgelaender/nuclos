//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_nucletImportFile
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_NUCLETIMPORT_FILE
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class NucletImportFile extends AbstractBusinessObject<java.lang.Long> implements Modifiable<java.lang.Long> {


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<java.lang.Long> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "KUOj", "KUOj0", java.lang.Long.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "KUOj", "KUOj1", java.util.Date.class);


/**
 * Attribute: filename
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: STRFILENAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Filename = new StringAttribute<>("Filename", "org.nuclos.businessentity", "KUOj", "KUOja", java.lang.String.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "KUOj", "KUOj2", java.lang.String.class);


/**
 * Attribute: content
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<byte[]> Content = 
	new Attribute<>("Content", "org.nuclos.businessentity", "KUOj", "KUOjb", byte[].class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "KUOj", "KUOj3", java.util.Date.class);


/**
 * Attribute: importDate
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: DATIMPORT
 *<br>Data type: org.nuclos.common2.DateTime
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<org.nuclos.common2.DateTime> ImportDate = 
	new Attribute<>("ImportDate", "org.nuclos.businessentity", "KUOj", "KUOjc", org.nuclos.common2.DateTime.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "KUOj", "KUOj4", java.lang.String.class);

public static final Dependent<org.nuclos.businessentity.NucletImport> _NucletImport = 
	new Dependent<>("_NucletImport", "null", "NucletImport", "mVjz", "nucletImportFile", "mVjzb", org.nuclos.businessentity.NucletImport.class);


public NucletImportFile() {
		super("KUOj");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("KUOj");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("KUOj1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: filename
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: STRFILENAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getFilename() {
		return getField("KUOja", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: filename
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: STRFILENAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setFilename(java.lang.String pFilename) {
		setField("KUOja", pFilename); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("KUOj2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public byte[] getContent() {
		return getField("KUOjb", byte[].class); 
}


/**
 * Setter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setContent(byte[] pContent) {
		setField("KUOjb", pContent); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("KUOj3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: importDate
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: DATIMPORT
 *<br>Data type: org.nuclos.common2.DateTime
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common2.DateTime getImportDate() {
		return getField("KUOjc", org.nuclos.common2.DateTime.class); 
}


/**
 * Setter-Method for attribute: importDate
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: DATIMPORT
 *<br>Data type: org.nuclos.common2.DateTime
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setImportDate(org.nuclos.common2.DateTime pImportDate) {
		setField("KUOjc", pImportDate); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletImportFile
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("KUOj4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nucletImportFile
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID_T_MD_NUCLETIMPORTFILE
 *<br>Data type: java.lang.Long
 *<br>Reference entity: nuclos_nucletImportFile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletImport> getNucletImport(Flag... flags) {
		return getDependents(_NucletImport, flags); 
}


/**
 * Insert-Method for attribute: nucletImportFile
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID_T_MD_NUCLETIMPORTFILE
 *<br>Data type: java.lang.Long
 *<br>Reference entity: nuclos_nucletImportFile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletImport(org.nuclos.businessentity.NucletImport pNucletImport) {
		insertDependent(_NucletImport, pNucletImport);
}


/**
 * Delete-Method for attribute: nucletImportFile
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID_T_MD_NUCLETIMPORTFILE
 *<br>Data type: java.lang.Long
 *<br>Reference entity: nuclos_nucletImportFile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletImport(org.nuclos.businessentity.NucletImport pNucletImport) {
		deleteDependent(_NucletImport, pNucletImport);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(NucletImportFile boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public NucletImportFile copy() {
		return super.copy(NucletImportFile.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("KUOj"), id);
}
/**
* Static Get by Id
*/
public static NucletImportFile get(java.lang.Long id) {
		return get(NucletImportFile.class, id);
}
 }
