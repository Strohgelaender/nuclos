//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_nucletIntegrationField
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_NUCLET_INTEGRFIELD
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class NucletIntegrationField extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "ECUw", "ECUw0", org.nuclos.common.UID.class);


/**
 * Attribute: dataprecision
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: INTDATAPRECISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Dataprecision = 
	new NumericAttribute<>("Dataprecision", "org.nuclos.businessentity", "ECUw", "ECUwh", java.lang.Integer.class);


/**
 * Attribute: optional
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: BLNOPTIONAL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Optional = 
	new Attribute<>("Optional", "org.nuclos.businessentity", "ECUw", "ECUwi", java.lang.Boolean.class);


/**
 * Attribute: readonly
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Readonly = 
	new Attribute<>("Readonly", "org.nuclos.businessentity", "ECUw", "ECUwj", java.lang.Boolean.class);


/**
 * Attribute: integrationPointReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> IntegrationPointReferenceFieldId = 
	new ForeignKeyAttribute<>("IntegrationPointReferenceFieldId", "org.nuclos.businessentity", "ECUw", "ECUwd", org.nuclos.common.UID.class);


/**
 * Attribute: entityReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_ENTITY_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> EntityReferenceFieldId = 
	new ForeignKeyAttribute<>("EntityReferenceFieldId", "org.nuclos.businessentity", "ECUw", "ECUwe", org.nuclos.common.UID.class);


/**
 * Attribute: targetField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_TARGET_FIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> TargetFieldId = 
	new ForeignKeyAttribute<>("TargetFieldId", "org.nuclos.businessentity", "ECUw", "ECUwf", org.nuclos.common.UID.class);


/**
 * Attribute: datascale
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: INTDATASCALE
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Datascale = 
	new NumericAttribute<>("Datascale", "org.nuclos.businessentity", "ECUw", "ECUwg", java.lang.Integer.class);


/**
 * Attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> IntegrationPointId = 
	new ForeignKeyAttribute<>("IntegrationPointId", "org.nuclos.businessentity", "ECUw", "ECUwa", org.nuclos.common.UID.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "ECUw", "ECUwb", java.lang.String.class);


/**
 * Attribute: datatype
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRDATATYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Datatype = new StringAttribute<>("Datatype", "org.nuclos.businessentity", "ECUw", "ECUwc", java.lang.String.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "ECUw", "ECUw4", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "ECUw", "ECUw1", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "ECUw", "ECUw2", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "ECUw", "ECUw3", java.util.Date.class);


public NucletIntegrationField() {
		super("ECUw");
		setOptional(java.lang.Boolean.FALSE);
		setReadonly(java.lang.Boolean.FALSE);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("ECUw");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: dataprecision
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: INTDATAPRECISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public java.lang.Integer getDataprecision() {
		return getField("ECUwh", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: dataprecision
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: INTDATAPRECISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setDataprecision(java.lang.Integer pDataprecision) {
		setField("ECUwh", pDataprecision); 
}


/**
 * Getter-Method for attribute: optional
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: BLNOPTIONAL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getOptional() {
		return getField("ECUwi", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: optional
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: BLNOPTIONAL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setOptional(java.lang.Boolean pOptional) {
		setField("ECUwi", pOptional); 
}


/**
 * Getter-Method for attribute: readonly
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getReadonly() {
		return getField("ECUwj", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: readonly
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setReadonly(java.lang.Boolean pReadonly) {
		setField("ECUwj", pReadonly); 
}


/**
 * Getter-Method for attribute: integrationPointReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getIntegrationPointReferenceFieldId() {
		return getFieldUid("ECUwd");
}


/**
 * Setter-Method for attribute: integrationPointReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setIntegrationPointReferenceFieldId(org.nuclos.common.UID pIntegrationPointReferenceFieldId) {
		setFieldId("ECUwd", pIntegrationPointReferenceFieldId); 
}


/**
 * Getter-Method for attribute: integrationPointReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.NucletIntegrationPoint getIntegrationPointReferenceFieldBO() {
		return getReferencedBO(org.nuclos.businessentity.NucletIntegrationPoint.class, getFieldUid("ECUwd"), "ECUwd", "kIL5");
}


/**
 * Getter-Method for attribute: entityReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_ENTITY_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityReferenceFieldId() {
		return getFieldUid("ECUwe");
}


/**
 * Setter-Method for attribute: entityReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_ENTITY_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setEntityReferenceFieldId(org.nuclos.common.UID pEntityReferenceFieldId) {
		setFieldId("ECUwe", pEntityReferenceFieldId); 
}


/**
 * Getter-Method for attribute: entityReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_ENTITY_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Entity getEntityReferenceFieldBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("ECUwe"), "ECUwe", "5E8q");
}


/**
 * Getter-Method for attribute: targetField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_TARGET_FIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entityfield
 *<br>Reference field: field
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getTargetFieldId() {
		return getFieldUid("ECUwf");
}


/**
 * Setter-Method for attribute: targetField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_TARGET_FIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entityfield
 *<br>Reference field: field
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setTargetFieldId(org.nuclos.common.UID pTargetFieldId) {
		setFieldId("ECUwf", pTargetFieldId); 
}


/**
 * Getter-Method for attribute: targetField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_TARGET_FIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entityfield
 *<br>Reference field: field
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.EntityField getTargetFieldBO() {
		return getReferencedBO(org.nuclos.businessentity.EntityField.class, getFieldUid("ECUwf"), "ECUwf", "Khi5");
}


/**
 * Getter-Method for attribute: datascale
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: INTDATASCALE
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public java.lang.Integer getDatascale() {
		return getField("ECUwg", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: datascale
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: INTDATASCALE
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setDatascale(java.lang.Integer pDatascale) {
		setField("ECUwg", pDatascale); 
}


/**
 * Getter-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getIntegrationPointId() {
		return getFieldUid("ECUwa");
}


/**
 * Setter-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setIntegrationPointId(org.nuclos.common.UID pIntegrationPointId) {
		setFieldId("ECUwa", pIntegrationPointId); 
}


/**
 * Getter-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.NucletIntegrationPoint getIntegrationPointBO() {
		return getReferencedBO(org.nuclos.businessentity.NucletIntegrationPoint.class, getFieldUid("ECUwa"), "ECUwa", "kIL5");
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getName() {
		return getField("ECUwb", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("ECUwb", pName); 
}


/**
 * Getter-Method for attribute: datatype
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRDATATYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getDatatype() {
		return getField("ECUwc", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: datatype
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRDATATYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setDatatype(java.lang.String pDatatype) {
		setField("ECUwc", pDatatype); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("ECUw4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("ECUw1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("ECUw2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("ECUw3", java.util.Date.class); 
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(NucletIntegrationField boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public NucletIntegrationField copy() {
		return super.copy(NucletIntegrationField.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("ECUw"), id);
}
/**
* Static Get by Id
*/
public static NucletIntegrationField get(org.nuclos.common.UID id) {
		return get(NucletIntegrationField.class, id);
}
 }
