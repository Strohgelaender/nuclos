//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_webAddonProperty
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_WEBADDON_PROPERTY
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class WebAddonProperty extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "9aeC", "9aeC0", org.nuclos.common.UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "9aeC", "9aeC1", java.util.Date.class);


/**
 * Attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> WebAddonId = 
	new ForeignKeyAttribute<>("WebAddonId", "org.nuclos.businessentity", "9aeC", "9aeCa", org.nuclos.common.UID.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "9aeC", "9aeC2", java.lang.String.class);


/**
 * Attribute: property
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRPROPERTY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Property = new StringAttribute<>("Property", "org.nuclos.businessentity", "9aeC", "9aeCb", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "9aeC", "9aeC3", java.util.Date.class);


/**
 * Attribute: type
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Type = new StringAttribute<>("Type", "org.nuclos.businessentity", "9aeC", "9aeCc", java.lang.String.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "9aeC", "9aeC4", java.lang.String.class);

public static final Dependent<org.nuclos.businessentity.WebAddonResultList> _WebAddonResultList = 
	new Dependent<>("_WebAddonResultList", "null", "WebAddonResultList", "3hPM", "webAddonProperty", "3hPMc", org.nuclos.businessentity.WebAddonResultList.class);


public WebAddonProperty() {
		super("9aeC");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("9aeC");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("9aeC1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getWebAddonId() {
		return getFieldUid("9aeCa");
}


/**
 * Setter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setWebAddonId(org.nuclos.common.UID pWebAddonId) {
		setFieldId("9aeCa", pWebAddonId); 
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.WebAddon getWebAddonBO() {
		return getReferencedBO(org.nuclos.businessentity.WebAddon.class, getFieldUid("9aeCa"), "9aeCa", "hyVG");
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("9aeC2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: property
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRPROPERTY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getProperty() {
		return getField("9aeCb", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: property
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRPROPERTY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setProperty(java.lang.String pProperty) {
		setField("9aeCb", pProperty); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("9aeC3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getType() {
		return getField("9aeCc", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: type
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setType(java.lang.String pType) {
		setField("9aeCc", pType); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("9aeC4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: webAddonProperty
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON_PROPERTY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddonProperty
 *<br>Reference field: property
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.WebAddonResultList> getWebAddonResultList(Flag... flags) {
		return getDependents(_WebAddonResultList, flags); 
}


/**
 * Insert-Method for attribute: webAddonProperty
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON_PROPERTY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddonProperty
 *<br>Reference field: property
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertWebAddonResultList(org.nuclos.businessentity.WebAddonResultList pWebAddonResultList) {
		insertDependent(_WebAddonResultList, pWebAddonResultList);
}


/**
 * Delete-Method for attribute: webAddonProperty
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON_PROPERTY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddonProperty
 *<br>Reference field: property
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteWebAddonResultList(org.nuclos.businessentity.WebAddonResultList pWebAddonResultList) {
		deleteDependent(_WebAddonResultList, pWebAddonResultList);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(WebAddonProperty boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public WebAddonProperty copy() {
		return super.copy(WebAddonProperty.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("9aeC"), id);
}
/**
* Static Get by Id
*/
public static WebAddonProperty get(org.nuclos.common.UID id) {
		return get(WebAddonProperty.class, id);
}
 }
