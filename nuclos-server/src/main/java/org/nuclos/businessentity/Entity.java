//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_entity
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_ENTITY
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class Entity extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "5E8q", "5E8q0", org.nuclos.common.UID.class);


/**
 * Attribute: comment
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRCOMMENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 350
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Comment = new StringAttribute<>("Comment", "org.nuclos.businessentity", "5E8q", "5E8qW", java.lang.String.class);


/**
 * Attribute: systemidprefix
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRSYSTEMIDPREFIX
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 2
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Systemidprefix = new StringAttribute<>("Systemidprefix", "org.nuclos.businessentity", "5E8q", "5E8qd", java.lang.String.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "5E8q", "5E8qc", org.nuclos.common.UID.class);


/**
 * Attribute: lockMode
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRLOCKMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> LockMode = new StringAttribute<>("LockMode", "org.nuclos.businessentity", "5E8q", "5E8qaa", java.lang.String.class);


/**
 * Attribute: unlockMode
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUNLOCKMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> UnlockMode = new StringAttribute<>("UnlockMode", "org.nuclos.businessentity", "5E8q", "5E8qab", java.lang.String.class);


/**
 * Attribute: dbtable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDBENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Dbtable = new StringAttribute<>("Dbtable", "org.nuclos.businessentity", "5E8q", "5E8qb", java.lang.String.class);


/**
 * Attribute: ownerForeignEntityField
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STROWNERFOREIGNENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> OwnerForeignEntityField = new StringAttribute<>("OwnerForeignEntityField", "org.nuclos.businessentity", "5E8q", "5E8qac", java.lang.String.class);


/**
 * Attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Entity = new StringAttribute<>("Entity", "org.nuclos.businessentity", "5E8q", "5E8qa", java.lang.String.class);


/**
 * Attribute: logbooktracking
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNLOGBOOKTRACKING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Logbooktracking = 
	new Attribute<>("Logbooktracking", "org.nuclos.businessentity", "5E8q", "5E8qh", java.lang.Boolean.class);


/**
 * Attribute: thin
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTHIN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Thin = 
	new Attribute<>("Thin", "org.nuclos.businessentity", "5E8q", "5E8qad", java.lang.Boolean.class);


/**
 * Attribute: usessatemodel
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNUSESSTATEMODEL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Usessatemodel = 
	new Attribute<>("Usessatemodel", "org.nuclos.businessentity", "5E8q", "5E8qg", java.lang.Boolean.class);


/**
 * Attribute: generic
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNGENERIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Generic = 
	new Attribute<>("Generic", "org.nuclos.businessentity", "5E8q", "5E8qae", java.lang.Boolean.class);


/**
 * Attribute: editable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNEDITABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Editable = 
	new Attribute<>("Editable", "org.nuclos.businessentity", "5E8q", "5E8qf", java.lang.Boolean.class);


/**
 * Attribute: writeProxy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNWRITEPROXY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> WriteProxy = 
	new Attribute<>("WriteProxy", "org.nuclos.businessentity", "5E8q", "5E8qaf", java.lang.Boolean.class);


/**
 * Attribute: menushortcut
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRMENUSHORTCUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Menushortcut = new StringAttribute<>("Menushortcut", "org.nuclos.businessentity", "5E8q", "5E8qe", java.lang.String.class);


/**
 * Attribute: mandatorUnique
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNMANDATORUNIQUE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> MandatorUnique = 
	new Attribute<>("MandatorUnique", "org.nuclos.businessentity", "5E8q", "5E8qag", java.lang.Boolean.class);


/**
 * Attribute: dataLangRefPath
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDATALANGREFPATH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> DataLangRefPath = new StringAttribute<>("DataLangRefPath", "org.nuclos.businessentity", "5E8q", "5E8qZ", java.lang.String.class);


/**
 * Attribute: dataLanguageDBTable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDATALANGDBTABLE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 50
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> DataLanguageDBTable = new StringAttribute<>("DataLanguageDBTable", "org.nuclos.businessentity", "5E8q", "5E8qY", java.lang.String.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "5E8q", "5E8q4", java.lang.String.class);


/**
 * Attribute: localeresourcel
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localeresourcel = new StringAttribute<>("Localeresourcel", "org.nuclos.businessentity", "5E8q", "5E8qt", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "5E8q", "5E8q3", java.util.Date.class);


/**
 * Attribute: nuclosResource
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRNUCLOSRESOURCE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> NuclosResource = new StringAttribute<>("NuclosResource", "org.nuclos.businessentity", "5E8q", "5E8qs", java.lang.String.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "5E8q", "5E8q2", java.lang.String.class);


/**
 * Attribute: resource
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_RESOURCE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> ResourceId = 
	new ForeignKeyAttribute<>("ResourceId", "org.nuclos.businessentity", "5E8q", "5E8qr", org.nuclos.common.UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "5E8q", "5E8q1", java.util.Date.class);


/**
 * Attribute: fieldsforequality
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRFIELDS_FOR_EQUALITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 512
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Fieldsforequality = new StringAttribute<>("Fieldsforequality", "org.nuclos.businessentity", "5E8q", "5E8qq", java.lang.String.class);


/**
 * Attribute: localeresourcett
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_TT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localeresourcett = new StringAttribute<>("Localeresourcett", "org.nuclos.businessentity", "5E8q", "5E8qx", java.lang.String.class);


/**
 * Attribute: localeresourcetw
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_TW
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localeresourcetw = new StringAttribute<>("Localeresourcetw", "org.nuclos.businessentity", "5E8q", "5E8qw", java.lang.String.class);


/**
 * Attribute: localeresourced
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localeresourced = new StringAttribute<>("Localeresourced", "org.nuclos.businessentity", "5E8q", "5E8qv", java.lang.String.class);


/**
 * Attribute: localeresourcem
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_M
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localeresourcem = new StringAttribute<>("Localeresourcem", "org.nuclos.businessentity", "5E8q", "5E8qu", java.lang.String.class);


/**
 * Attribute: treegroup
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTREEGROUP
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Treegroup = 
	new Attribute<>("Treegroup", "org.nuclos.businessentity", "5E8q", "5E8ql", java.lang.Boolean.class);


/**
 * Attribute: treerelation
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTREERELATION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Treerelation = 
	new Attribute<>("Treerelation", "org.nuclos.businessentity", "5E8q", "5E8qk", java.lang.Boolean.class);


/**
 * Attribute: searchable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNSEARCHABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Searchable = 
	new Attribute<>("Searchable", "org.nuclos.businessentity", "5E8q", "5E8qj", java.lang.Boolean.class);


/**
 * Attribute: cacheable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNCACHEABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Cacheable = 
	new Attribute<>("Cacheable", "org.nuclos.businessentity", "5E8q", "5E8qi", java.lang.Boolean.class);


/**
 * Attribute: acceleratormodifier
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: INTACCELERATORMODIFIER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 2
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Acceleratormodifier = 
	new NumericAttribute<>("Acceleratormodifier", "org.nuclos.businessentity", "5E8q", "5E8qp", java.lang.Integer.class);


/**
 * Attribute: accelerator
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRACCELERATOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Accelerator = new StringAttribute<>("Accelerator", "org.nuclos.businessentity", "5E8q", "5E8qo", java.lang.String.class);


/**
 * Attribute: fieldvalueentity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNFIELDVALUEENTITY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Fieldvalueentity = 
	new Attribute<>("Fieldvalueentity", "org.nuclos.businessentity", "5E8q", "5E8qn", java.lang.Boolean.class);


/**
 * Attribute: importexport
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNIMPORTEXPORT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Importexport = 
	new Attribute<>("Importexport", "org.nuclos.businessentity", "5E8q", "5E8qm", java.lang.Boolean.class);


/**
 * Attribute: rowcolorscript
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRROWCOLORSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<org.nuclos.common.NuclosScript> Rowcolorscript = 
	new Attribute<>("Rowcolorscript", "org.nuclos.businessentity", "5E8q", "5E8qD", org.nuclos.common.NuclosScript.class);


/**
 * Attribute: readDelegate
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_READDELEGATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ReadDelegate = new StringAttribute<>("ReadDelegate", "org.nuclos.businessentity", "5E8q", "5E8qC", java.lang.String.class);


/**
 * Attribute: idFactory
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_IDFACTORY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> IdFactory = new StringAttribute<>("IdFactory", "org.nuclos.businessentity", "5E8q", "5E8qB", java.lang.String.class);


/**
 * Attribute: virtualentity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRVIRTUALENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Virtualentity = new StringAttribute<>("Virtualentity", "org.nuclos.businessentity", "5E8q", "5E8qA", java.lang.String.class);


/**
 * Attribute: resultdetailssplitview
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNRESULTDETAILSSPLITVIEW
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Resultdetailssplitview = 
	new Attribute<>("Resultdetailssplitview", "org.nuclos.businessentity", "5E8q", "5E8qH", java.lang.Boolean.class);


/**
 * Attribute: mandatorLevel
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_MANDATOR_LEVEL
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> MandatorLevelId = 
	new ForeignKeyAttribute<>("MandatorLevelId", "org.nuclos.businessentity", "5E8q", "5E8qG", org.nuclos.common.UID.class);


/**
 * Attribute: proxy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNPROXY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Proxy = 
	new Attribute<>("Proxy", "org.nuclos.businessentity", "5E8q", "5E8qF", java.lang.Boolean.class);


/**
 * Attribute: showSearch
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNSHOWSEARCH
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> ShowSearch = 
	new Attribute<>("ShowSearch", "org.nuclos.businessentity", "5E8q", "5E8qE", java.lang.Boolean.class);


/**
 * Attribute: cloneGenerator
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_CLONEGENERATOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> CloneGeneratorId = 
	new ForeignKeyAttribute<>("CloneGeneratorId", "org.nuclos.businessentity", "5E8q", "5E8qz", org.nuclos.common.UID.class);

public static final Dependent<org.nuclos.businessentity.WebAddon> _WebAddon = 
	new Dependent<>("_WebAddon", "null", "WebAddon", "hyVG", "newResultList", "hyVGi", org.nuclos.businessentity.WebAddon.class);

public static final Dependent<org.nuclos.businessentity.NucletIntegrationPoint> _NucletIntegrationPoint = 
	new Dependent<>("_NucletIntegrationPoint", "null", "NucletIntegrationPoint", "kIL5", "targetEntity", "kIL5c", org.nuclos.businessentity.NucletIntegrationPoint.class);

public static final Dependent<org.nuclos.businessentity.NucletIntegrationField> _NucletIntegrationField = 
	new Dependent<>("_NucletIntegrationField", "null", "NucletIntegrationField", "ECUw", "entityReferenceField", "ECUwe", org.nuclos.businessentity.NucletIntegrationField.class);

public static final Dependent<org.nuclos.businessentity.WebAddonResultList> _WebAddonResultList = 
	new Dependent<>("_WebAddonResultList", "null", "WebAddonResultList", "3hPM", "entity", "3hPMb", org.nuclos.businessentity.WebAddonResultList.class);

public static final Dependent<org.nuclos.businessentity.EntityField> _EntityField1 = 
	new Dependent<>("_EntityField1", "null", "EntityField", "Khi5", "entity", "Khi5a", org.nuclos.businessentity.EntityField.class);

public static final Dependent<org.nuclos.businessentity.EntityField> _EntityField2 = 
	new Dependent<>("_EntityField2", "null", "EntityField", "Khi5", "autonumberentity", "Khi5L", org.nuclos.businessentity.EntityField.class);


public Entity() {
		super("5E8q");
		setLogbooktracking(java.lang.Boolean.FALSE);
		setUsessatemodel(java.lang.Boolean.FALSE);
		setEditable(java.lang.Boolean.FALSE);
		setMandatorUnique(java.lang.Boolean.FALSE);
		setTreegroup(java.lang.Boolean.FALSE);
		setTreerelation(java.lang.Boolean.FALSE);
		setSearchable(java.lang.Boolean.FALSE);
		setCacheable(java.lang.Boolean.FALSE);
		setFieldvalueentity(java.lang.Boolean.FALSE);
		setImportexport(java.lang.Boolean.FALSE);
		setShowSearch(java.lang.Boolean.FALSE);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("5E8q");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: comment
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRCOMMENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 350
 *<br>Precision: null
**/
public java.lang.String getComment() {
		return getField("5E8qW", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: comment
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRCOMMENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 350
 *<br>Precision: null
**/
public void setComment(java.lang.String pComment) {
		setField("5E8qW", pComment); 
}


/**
 * Getter-Method for attribute: systemidprefix
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRSYSTEMIDPREFIX
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 2
 *<br>Precision: null
**/
public java.lang.String getSystemidprefix() {
		return getField("5E8qd", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: systemidprefix
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRSYSTEMIDPREFIX
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 2
 *<br>Precision: null
**/
public void setSystemidprefix(java.lang.String pSystemidprefix) {
		setField("5E8qd", pSystemidprefix); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("5E8qc");
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("5E8qc", pNucletId); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("5E8qc"), "5E8qc", "xojr");
}


/**
 * Getter-Method for attribute: lockMode
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRLOCKMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLockMode() {
		return getField("5E8qaa", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: lockMode
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRLOCKMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLockMode(java.lang.String pLockMode) {
		setField("5E8qaa", pLockMode); 
}


/**
 * Getter-Method for attribute: unlockMode
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUNLOCKMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getUnlockMode() {
		return getField("5E8qab", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: unlockMode
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUNLOCKMODE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setUnlockMode(java.lang.String pUnlockMode) {
		setField("5E8qab", pUnlockMode); 
}


/**
 * Getter-Method for attribute: dbtable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDBENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public java.lang.String getDbtable() {
		return getField("5E8qb", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: dbtable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDBENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setDbtable(java.lang.String pDbtable) {
		setField("5E8qb", pDbtable); 
}


/**
 * Getter-Method for attribute: ownerForeignEntityField
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STROWNERFOREIGNENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getOwnerForeignEntityField() {
		return getField("5E8qac", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: ownerForeignEntityField
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STROWNERFOREIGNENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setOwnerForeignEntityField(java.lang.String pOwnerForeignEntityField) {
		setField("5E8qac", pOwnerForeignEntityField); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getEntity() {
		return getField("5E8qa", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setEntity(java.lang.String pEntity) {
		setField("5E8qa", pEntity); 
}


/**
 * Getter-Method for attribute: logbooktracking
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNLOGBOOKTRACKING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getLogbooktracking() {
		return getField("5E8qh", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: logbooktracking
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNLOGBOOKTRACKING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLogbooktracking(java.lang.Boolean pLogbooktracking) {
		setField("5E8qh", pLogbooktracking); 
}


/**
 * Getter-Method for attribute: thin
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTHIN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getThin() {
		return getField("5E8qad", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: thin
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTHIN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setThin(java.lang.Boolean pThin) {
		setField("5E8qad", pThin); 
}


/**
 * Getter-Method for attribute: usessatemodel
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNUSESSTATEMODEL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getUsessatemodel() {
		return getField("5E8qg", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: usessatemodel
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNUSESSTATEMODEL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setUsessatemodel(java.lang.Boolean pUsessatemodel) {
		setField("5E8qg", pUsessatemodel); 
}


/**
 * Getter-Method for attribute: generic
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNGENERIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getGeneric() {
		return getField("5E8qae", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: generic
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNGENERIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setGeneric(java.lang.Boolean pGeneric) {
		setField("5E8qae", pGeneric); 
}


/**
 * Getter-Method for attribute: editable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNEDITABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getEditable() {
		return getField("5E8qf", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: editable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNEDITABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setEditable(java.lang.Boolean pEditable) {
		setField("5E8qf", pEditable); 
}


/**
 * Getter-Method for attribute: writeProxy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNWRITEPROXY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getWriteProxy() {
		return getField("5E8qaf", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: writeProxy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNWRITEPROXY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setWriteProxy(java.lang.Boolean pWriteProxy) {
		setField("5E8qaf", pWriteProxy); 
}


/**
 * Getter-Method for attribute: menushortcut
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRMENUSHORTCUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public java.lang.String getMenushortcut() {
		return getField("5E8qe", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: menushortcut
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRMENUSHORTCUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public void setMenushortcut(java.lang.String pMenushortcut) {
		setField("5E8qe", pMenushortcut); 
}


/**
 * Getter-Method for attribute: mandatorUnique
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNMANDATORUNIQUE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getMandatorUnique() {
		return getField("5E8qag", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: mandatorUnique
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNMANDATORUNIQUE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setMandatorUnique(java.lang.Boolean pMandatorUnique) {
		setField("5E8qag", pMandatorUnique); 
}


/**
 * Getter-Method for attribute: dataLangRefPath
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDATALANGREFPATH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1000
 *<br>Precision: null
**/
public java.lang.String getDataLangRefPath() {
		return getField("5E8qZ", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: dataLangRefPath
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDATALANGREFPATH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1000
 *<br>Precision: null
**/
public void setDataLangRefPath(java.lang.String pDataLangRefPath) {
		setField("5E8qZ", pDataLangRefPath); 
}


/**
 * Getter-Method for attribute: dataLanguageDBTable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDATALANGDBTABLE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 50
 *<br>Precision: null
**/
public java.lang.String getDataLanguageDBTable() {
		return getField("5E8qY", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: dataLanguageDBTable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRDATALANGDBTABLE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 50
 *<br>Precision: null
**/
public void setDataLanguageDBTable(java.lang.String pDataLanguageDBTable) {
		setField("5E8qY", pDataLanguageDBTable); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("5E8q4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: localeresourcel
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLocaleresourcel() {
		return getField("5E8qt", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: localeresourcel
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLocaleresourcel(java.lang.String pLocaleresourcel) {
		setField("5E8qt", pLocaleresourcel); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("5E8q3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: nuclosResource
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRNUCLOSRESOURCE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1000
 *<br>Precision: null
**/
public java.lang.String getNuclosResource() {
		return getField("5E8qs", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: nuclosResource
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRNUCLOSRESOURCE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1000
 *<br>Precision: null
**/
public void setNuclosResource(java.lang.String pNuclosResource) {
		setField("5E8qs", pNuclosResource); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("5E8q2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: resource
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_RESOURCE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_resource
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.UID getResourceId() {
		return getFieldUid("5E8qr");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("5E8q1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: fieldsforequality
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRFIELDS_FOR_EQUALITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 512
 *<br>Precision: null
**/
public java.lang.String getFieldsforequality() {
		return getField("5E8qq", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: fieldsforequality
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRFIELDS_FOR_EQUALITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 512
 *<br>Precision: null
**/
public void setFieldsforequality(java.lang.String pFieldsforequality) {
		setField("5E8qq", pFieldsforequality); 
}


/**
 * Getter-Method for attribute: localeresourcett
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_TT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLocaleresourcett() {
		return getField("5E8qx", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: localeresourcett
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_TT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLocaleresourcett(java.lang.String pLocaleresourcett) {
		setField("5E8qx", pLocaleresourcett); 
}


/**
 * Getter-Method for attribute: localeresourcetw
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_TW
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLocaleresourcetw() {
		return getField("5E8qw", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: localeresourcetw
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_TW
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLocaleresourcetw(java.lang.String pLocaleresourcetw) {
		setField("5E8qw", pLocaleresourcetw); 
}


/**
 * Getter-Method for attribute: localeresourced
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLocaleresourced() {
		return getField("5E8qv", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: localeresourced
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLocaleresourced(java.lang.String pLocaleresourced) {
		setField("5E8qv", pLocaleresourced); 
}


/**
 * Getter-Method for attribute: localeresourcem
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_M
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLocaleresourcem() {
		return getField("5E8qu", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: localeresourcem
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_LOCALERESOURCE_M
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLocaleresourcem(java.lang.String pLocaleresourcem) {
		setField("5E8qu", pLocaleresourcem); 
}


/**
 * Getter-Method for attribute: treegroup
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTREEGROUP
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getTreegroup() {
		return getField("5E8ql", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: treegroup
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTREEGROUP
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setTreegroup(java.lang.Boolean pTreegroup) {
		setField("5E8ql", pTreegroup); 
}


/**
 * Getter-Method for attribute: treerelation
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTREERELATION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getTreerelation() {
		return getField("5E8qk", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: treerelation
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNTREERELATION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setTreerelation(java.lang.Boolean pTreerelation) {
		setField("5E8qk", pTreerelation); 
}


/**
 * Getter-Method for attribute: searchable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNSEARCHABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getSearchable() {
		return getField("5E8qj", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: searchable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNSEARCHABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setSearchable(java.lang.Boolean pSearchable) {
		setField("5E8qj", pSearchable); 
}


/**
 * Getter-Method for attribute: cacheable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNCACHEABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getCacheable() {
		return getField("5E8qi", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: cacheable
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNCACHEABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setCacheable(java.lang.Boolean pCacheable) {
		setField("5E8qi", pCacheable); 
}


/**
 * Getter-Method for attribute: acceleratormodifier
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: INTACCELERATORMODIFIER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 2
 *<br>Precision: null
**/
public java.lang.Integer getAcceleratormodifier() {
		return getField("5E8qp", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: acceleratormodifier
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: INTACCELERATORMODIFIER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 2
 *<br>Precision: null
**/
public void setAcceleratormodifier(java.lang.Integer pAcceleratormodifier) {
		setField("5E8qp", pAcceleratormodifier); 
}


/**
 * Getter-Method for attribute: accelerator
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRACCELERATOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public java.lang.String getAccelerator() {
		return getField("5E8qo", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: accelerator
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRACCELERATOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1
 *<br>Precision: null
**/
public void setAccelerator(java.lang.String pAccelerator) {
		setField("5E8qo", pAccelerator); 
}


/**
 * Getter-Method for attribute: fieldvalueentity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNFIELDVALUEENTITY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getFieldvalueentity() {
		return getField("5E8qn", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: fieldvalueentity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNFIELDVALUEENTITY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setFieldvalueentity(java.lang.Boolean pFieldvalueentity) {
		setField("5E8qn", pFieldvalueentity); 
}


/**
 * Getter-Method for attribute: importexport
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNIMPORTEXPORT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getImportexport() {
		return getField("5E8qm", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: importexport
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNIMPORTEXPORT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setImportexport(java.lang.Boolean pImportexport) {
		setField("5E8qm", pImportexport); 
}


/**
 * Getter-Method for attribute: rowcolorscript
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRROWCOLORSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.NuclosScript getRowcolorscript() {
		return getField("5E8qD", org.nuclos.common.NuclosScript.class); 
}


/**
 * Setter-Method for attribute: rowcolorscript
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRROWCOLORSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setRowcolorscript(org.nuclos.common.NuclosScript pRowcolorscript) {
		setField("5E8qD", pRowcolorscript); 
}


/**
 * Getter-Method for attribute: readDelegate
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_READDELEGATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getReadDelegate() {
		return getField("5E8qC", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: readDelegate
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_READDELEGATE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setReadDelegate(java.lang.String pReadDelegate) {
		setField("5E8qC", pReadDelegate); 
}


/**
 * Getter-Method for attribute: idFactory
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_IDFACTORY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getIdFactory() {
		return getField("5E8qB", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: idFactory
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STR_IDFACTORY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setIdFactory(java.lang.String pIdFactory) {
		setField("5E8qB", pIdFactory); 
}


/**
 * Getter-Method for attribute: virtualentity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRVIRTUALENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getVirtualentity() {
		return getField("5E8qA", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: virtualentity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRVIRTUALENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setVirtualentity(java.lang.String pVirtualentity) {
		setField("5E8qA", pVirtualentity); 
}


/**
 * Getter-Method for attribute: resultdetailssplitview
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNRESULTDETAILSSPLITVIEW
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getResultdetailssplitview() {
		return getField("5E8qH", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: resultdetailssplitview
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNRESULTDETAILSSPLITVIEW
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setResultdetailssplitview(java.lang.Boolean pResultdetailssplitview) {
		setField("5E8qH", pResultdetailssplitview); 
}


/**
 * Getter-Method for attribute: mandatorLevel
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_MANDATOR_LEVEL
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_mandatorLevel
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.UID getMandatorLevelId() {
		return getFieldUid("5E8qG");
}


/**
 * Getter-Method for attribute: proxy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNPROXY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getProxy() {
		return getField("5E8qF", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: proxy
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNPROXY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setProxy(java.lang.Boolean pProxy) {
		setField("5E8qF", pProxy); 
}


/**
 * Getter-Method for attribute: showSearch
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNSHOWSEARCH
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getShowSearch() {
		return getField("5E8qE", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: showSearch
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: BLNSHOWSEARCH
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setShowSearch(java.lang.Boolean pShowSearch) {
		setField("5E8qE", pShowSearch); 
}


/**
 * Getter-Method for attribute: cloneGenerator
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_CLONEGENERATOR
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_generation
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.UID getCloneGeneratorId() {
		return getFieldUid("5E8qz");
}


/**
 * Getter-Method for attribute: newResultList
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_NEWRESULTLIST
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.WebAddon> getWebAddon(Flag... flags) {
		return getDependents(_WebAddon, flags); 
}


/**
 * Insert-Method for attribute: newResultList
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_NEWRESULTLIST
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertWebAddon(org.nuclos.businessentity.WebAddon pWebAddon) {
		insertDependent(_WebAddon, pWebAddon);
}


/**
 * Delete-Method for attribute: newResultList
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_NEWRESULTLIST
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteWebAddon(org.nuclos.businessentity.WebAddon pWebAddon) {
		deleteDependent(_WebAddon, pWebAddon);
}


/**
 * Getter-Method for attribute: targetEntity
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationPoint> getNucletIntegrationPoint(Flag... flags) {
		return getDependents(_NucletIntegrationPoint, flags); 
}


/**
 * Insert-Method for attribute: targetEntity
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationPoint(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		insertDependent(_NucletIntegrationPoint, pNucletIntegrationPoint);
}


/**
 * Delete-Method for attribute: targetEntity
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationPoint(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		deleteDependent(_NucletIntegrationPoint, pNucletIntegrationPoint);
}


/**
 * Getter-Method for attribute: entityReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_ENTITY_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationField> getNucletIntegrationField(Flag... flags) {
		return getDependents(_NucletIntegrationField, flags); 
}


/**
 * Insert-Method for attribute: entityReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_ENTITY_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationField(org.nuclos.businessentity.NucletIntegrationField pNucletIntegrationField) {
		insertDependent(_NucletIntegrationField, pNucletIntegrationField);
}


/**
 * Delete-Method for attribute: entityReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_ENTITY_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationField(org.nuclos.businessentity.NucletIntegrationField pNucletIntegrationField) {
		deleteDependent(_NucletIntegrationField, pNucletIntegrationField);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.WebAddonResultList> getWebAddonResultList(Flag... flags) {
		return getDependents(_WebAddonResultList, flags); 
}


/**
 * Insert-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertWebAddonResultList(org.nuclos.businessentity.WebAddonResultList pWebAddonResultList) {
		insertDependent(_WebAddonResultList, pWebAddonResultList);
}


/**
 * Delete-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteWebAddonResultList(org.nuclos.businessentity.WebAddonResultList pWebAddonResultList) {
		deleteDependent(_WebAddonResultList, pWebAddonResultList);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.EntityField> getEntityField1(Flag... flags) {
		return getDependents(_EntityField1, flags); 
}


/**
 * Insert-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void insertEntityField1(org.nuclos.businessentity.EntityField pEntityField) {
		insertDependent(_EntityField1, pEntityField);
}


/**
 * Delete-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void deleteEntityField1(org.nuclos.businessentity.EntityField pEntityField) {
		deleteDependent(_EntityField1, pEntityField);
}


/**
 * Getter-Method for attribute: autonumberentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_AUTONUMBERENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.EntityField> getEntityField2(Flag... flags) {
		return getDependents(_EntityField2, flags); 
}


/**
 * Insert-Method for attribute: autonumberentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_AUTONUMBERENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertEntityField2(org.nuclos.businessentity.EntityField pEntityField) {
		insertDependent(_EntityField2, pEntityField);
}


/**
 * Delete-Method for attribute: autonumberentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_AUTONUMBERENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteEntityField2(org.nuclos.businessentity.EntityField pEntityField) {
		deleteDependent(_EntityField2, pEntityField);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(Entity boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public Entity copy() {
		return super.copy(Entity.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("5E8q"), id);
}
/**
* Static Get by Id
*/
public static Entity get(org.nuclos.common.UID id) {
		return get(Entity.class, id);
}
 }
