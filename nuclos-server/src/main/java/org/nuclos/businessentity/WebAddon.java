//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_webAddon
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_WEBADDON
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class WebAddon extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "hyVG", "hyVG0", org.nuclos.common.UID.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "hyVG", "hyVG3", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "hyVG", "hyVG2", java.lang.String.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "hyVG", "hyVG4", java.lang.String.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "hyVG", "hyVGa", org.nuclos.common.UID.class);


/**
 * Attribute: active
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Active = 
	new Attribute<>("Active", "org.nuclos.businessentity", "hyVG", "hyVGc", java.lang.Boolean.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "hyVG", "hyVGb", java.lang.String.class);


/**
 * Attribute: readme
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRREADME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Readme = new StringAttribute<>("Readme", "org.nuclos.businessentity", "hyVG", "hyVGe", java.lang.String.class);


/**
 * Attribute: note
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRNOTE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Note = new StringAttribute<>("Note", "org.nuclos.businessentity", "hyVG", "hyVGd", java.lang.String.class);


/**
 * Attribute: layoutcomponent
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNLAYOUTCOMPONENT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Layoutcomponent = 
	new Attribute<>("Layoutcomponent", "org.nuclos.businessentity", "hyVG", "hyVGg", java.lang.Boolean.class);


/**
 * Attribute: resultlist
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNSEARCHRESULT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Resultlist = 
	new Attribute<>("Resultlist", "org.nuclos.businessentity", "hyVG", "hyVGf", java.lang.Boolean.class);


/**
 * Attribute: newResultList
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_NEWRESULTLIST
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NewResultListId = 
	new ForeignKeyAttribute<>("NewResultListId", "org.nuclos.businessentity", "hyVG", "hyVGi", org.nuclos.common.UID.class);


/**
 * Attribute: dashboard
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNDASHBOARD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Dashboard = 
	new Attribute<>("Dashboard", "org.nuclos.businessentity", "hyVG", "hyVGh", java.lang.Boolean.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "hyVG", "hyVG1", java.util.Date.class);

public static final Dependent<org.nuclos.businessentity.WebAddonProperty> _WebAddonProperty = 
	new Dependent<>("_WebAddonProperty", "null", "WebAddonProperty", "9aeC", "webAddon", "9aeCa", org.nuclos.businessentity.WebAddonProperty.class);

public static final Dependent<org.nuclos.businessentity.WebAddonLog> _WebAddonLog = 
	new Dependent<>("_WebAddonLog", "null", "WebAddonLog", "MJHT", "webAddon", "MJHTa", org.nuclos.businessentity.WebAddonLog.class);

public static final Dependent<org.nuclos.businessentity.WebAddonFile> _WebAddonFile = 
	new Dependent<>("_WebAddonFile", "null", "WebAddonFile", "W7ZO", "webAddon", "W7ZOa", org.nuclos.businessentity.WebAddonFile.class);

public static final Dependent<org.nuclos.businessentity.WebAddonResultList> _WebAddonResultList = 
	new Dependent<>("_WebAddonResultList", "null", "WebAddonResultList", "3hPM", "webAddon", "3hPMa", org.nuclos.businessentity.WebAddonResultList.class);


public WebAddon() {
		super("hyVG");
		setActive(java.lang.Boolean.FALSE);
		setLayoutcomponent(java.lang.Boolean.FALSE);
		setResultlist(java.lang.Boolean.FALSE);
		setDashboard(java.lang.Boolean.FALSE);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("hyVG");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("hyVG3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("hyVG2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("hyVG4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("hyVGa");
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("hyVGa", pNucletId); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("hyVGa"), "hyVGa", "xojr");
}


/**
 * Getter-Method for attribute: active
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getActive() {
		return getField("hyVGc", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: active
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNACTIVE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setActive(java.lang.Boolean pActive) {
		setField("hyVGc", pActive); 
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getName() {
		return getField("hyVGb", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("hyVGb", pName); 
}


/**
 * Getter-Method for attribute: readme
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRREADME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getReadme() {
		return getField("hyVGe", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: readme
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRREADME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setReadme(java.lang.String pReadme) {
		setField("hyVGe", pReadme); 
}


/**
 * Getter-Method for attribute: note
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRNOTE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public java.lang.String getNote() {
		return getField("hyVGd", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: note
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRNOTE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setNote(java.lang.String pNote) {
		setField("hyVGd", pNote); 
}


/**
 * Getter-Method for attribute: layoutcomponent
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNLAYOUTCOMPONENT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getLayoutcomponent() {
		return getField("hyVGg", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: layoutcomponent
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNLAYOUTCOMPONENT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLayoutcomponent(java.lang.Boolean pLayoutcomponent) {
		setField("hyVGg", pLayoutcomponent); 
}


/**
 * Getter-Method for attribute: resultlist
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNSEARCHRESULT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getResultlist() {
		return getField("hyVGf", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: resultlist
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNSEARCHRESULT
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setResultlist(java.lang.Boolean pResultlist) {
		setField("hyVGf", pResultlist); 
}


/**
 * Getter-Method for attribute: newResultList
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_NEWRESULTLIST
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNewResultListId() {
		return getFieldUid("hyVGi");
}


/**
 * Setter-Method for attribute: newResultList
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_NEWRESULTLIST
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNewResultListId(org.nuclos.common.UID pNewResultListId) {
		setFieldId("hyVGi", pNewResultListId); 
}


/**
 * Getter-Method for attribute: newResultList
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: STRUID_NEWRESULTLIST
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Entity getNewResultListBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("hyVGi"), "hyVGi", "5E8q");
}


/**
 * Getter-Method for attribute: dashboard
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNDASHBOARD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getDashboard() {
		return getField("hyVGh", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: dashboard
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: BLNDASHBOARD
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setDashboard(java.lang.Boolean pDashboard) {
		setField("hyVGh", pDashboard); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_webAddon
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("hyVG1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.WebAddonProperty> getWebAddonProperty(Flag... flags) {
		return getDependents(_WebAddonProperty, flags); 
}


/**
 * Insert-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertWebAddonProperty(org.nuclos.businessentity.WebAddonProperty pWebAddonProperty) {
		insertDependent(_WebAddonProperty, pWebAddonProperty);
}


/**
 * Delete-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonProperty
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteWebAddonProperty(org.nuclos.businessentity.WebAddonProperty pWebAddonProperty) {
		deleteDependent(_WebAddonProperty, pWebAddonProperty);
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonLog
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.WebAddonLog> getWebAddonLog(Flag... flags) {
		return getDependents(_WebAddonLog, flags); 
}


/**
 * Insert-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonLog
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertWebAddonLog(org.nuclos.businessentity.WebAddonLog pWebAddonLog) {
		insertDependent(_WebAddonLog, pWebAddonLog);
}


/**
 * Delete-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonLog
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteWebAddonLog(org.nuclos.businessentity.WebAddonLog pWebAddonLog) {
		deleteDependent(_WebAddonLog, pWebAddonLog);
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.WebAddonFile> getWebAddonFile(Flag... flags) {
		return getDependents(_WebAddonFile, flags); 
}


/**
 * Insert-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertWebAddonFile(org.nuclos.businessentity.WebAddonFile pWebAddonFile) {
		insertDependent(_WebAddonFile, pWebAddonFile);
}


/**
 * Delete-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteWebAddonFile(org.nuclos.businessentity.WebAddonFile pWebAddonFile) {
		deleteDependent(_WebAddonFile, pWebAddonFile);
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.WebAddonResultList> getWebAddonResultList(Flag... flags) {
		return getDependents(_WebAddonResultList, flags); 
}


/**
 * Insert-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertWebAddonResultList(org.nuclos.businessentity.WebAddonResultList pWebAddonResultList) {
		insertDependent(_WebAddonResultList, pWebAddonResultList);
}


/**
 * Delete-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteWebAddonResultList(org.nuclos.businessentity.WebAddonResultList pWebAddonResultList) {
		deleteDependent(_WebAddonResultList, pWebAddonResultList);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(WebAddon boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public WebAddon copy() {
		return super.copy(WebAddon.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("hyVG"), id);
}
/**
* Static Get by Id
*/
public static WebAddon get(org.nuclos.common.UID id) {
		return get(WebAddon.class, id);
}
 }
