//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_webAddonFile
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_WEBADDON_FILE
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class WebAddonFile extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "W7ZO", "W7ZO0", org.nuclos.common.UID.class);


/**
 * Attribute: content
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<byte[]> Content = 
	new Attribute<>("Content", "org.nuclos.businessentity", "W7ZO", "W7ZOe", byte[].class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "W7ZO", "W7ZO1", java.util.Date.class);


/**
 * Attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> WebAddonId = 
	new ForeignKeyAttribute<>("WebAddonId", "org.nuclos.businessentity", "W7ZO", "W7ZOa", org.nuclos.common.UID.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "W7ZO", "W7ZO2", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "W7ZO", "W7ZO3", java.util.Date.class);


/**
 * Attribute: path
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRPATH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Path = new StringAttribute<>("Path", "org.nuclos.businessentity", "W7ZO", "W7ZOc", java.lang.String.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "W7ZO", "W7ZO4", java.lang.String.class);


/**
 * Attribute: hash
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Hash = new StringAttribute<>("Hash", "org.nuclos.businessentity", "W7ZO", "W7ZOd", java.lang.String.class);


public WebAddonFile() {
		super("W7ZO");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("W7ZO");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public byte[] getContent() {
		return getField("W7ZOe", byte[].class); 
}


/**
 * Setter-Method for attribute: content
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: BLBCONTENT
 *<br>Data type: [B
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setContent(byte[] pContent) {
		setField("W7ZOe", pContent); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("W7ZO1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getWebAddonId() {
		return getFieldUid("W7ZOa");
}


/**
 * Setter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setWebAddonId(org.nuclos.common.UID pWebAddonId) {
		setFieldId("W7ZOa", pWebAddonId); 
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.WebAddon getWebAddonBO() {
		return getReferencedBO(org.nuclos.businessentity.WebAddon.class, getFieldUid("W7ZOa"), "W7ZOa", "hyVG");
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("W7ZO2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: file
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_FILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.common.NuclosFile getFile() {
		return getNuclosFile("W7ZOb");
}


/**
 * Getter-Method for attribute: file
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_FILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> T getFile(Class<T> pFile) {
		return super.getNuclosFile(pFile,"W7ZOb");
}


/**
 * Setter-Method for attribute: file
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRUID_FILE
 *<br>Data type: org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile
 *<br>Reference entity: nuclos_documentfile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public <T extends org.nuclos.api.common.NuclosFileBase> void setFile(T pFile) {
		super.setNuclosFile(pFile,"W7ZOb");
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("W7ZO3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: path
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRPATH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getPath() {
		return getField("W7ZOc", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: path
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRPATH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setPath(java.lang.String pPath) {
		setField("W7ZOc", pPath); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("W7ZO4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: hash
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getHash() {
		return getField("W7ZOd", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: hash
 *<br>
 *<br>Entity: nuclos_webAddonFile
 *<br>DB-Name: STRHASH
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setHash(java.lang.String pHash) {
		setField("W7ZOd", pHash); 
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(WebAddonFile boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public WebAddonFile copy() {
		return super.copy(WebAddonFile.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("W7ZO"), id);
}
/**
* Static Get by Id
*/
public static WebAddonFile get(org.nuclos.common.UID id) {
		return get(WebAddonFile.class, id);
}
 }
