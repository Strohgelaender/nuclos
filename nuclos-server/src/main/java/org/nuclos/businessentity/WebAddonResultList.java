//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_webAddonResultList
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_WEBADDON_RESULTLIST
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class WebAddonResultList extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "3hPM", "3hPM0", org.nuclos.common.UID.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "3hPM", "3hPM1", java.util.Date.class);


/**
 * Attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> WebAddonId = 
	new ForeignKeyAttribute<>("WebAddonId", "org.nuclos.businessentity", "3hPM", "3hPMa", org.nuclos.common.UID.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "3hPM", "3hPM4", java.lang.String.class);


/**
 * Attribute: propertyValue
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRPROPERTYVALUE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> PropertyValue = new StringAttribute<>("PropertyValue", "org.nuclos.businessentity", "3hPM", "3hPMd", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "3hPM", "3hPM3", java.util.Date.class);


/**
 * Attribute: webAddonProperty
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON_PROPERTY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> WebAddonPropertyId = 
	new ForeignKeyAttribute<>("WebAddonPropertyId", "org.nuclos.businessentity", "3hPM", "3hPMc", org.nuclos.common.UID.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "3hPM", "3hPM2", java.lang.String.class);


/**
 * Attribute: entity
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> EntityId = 
	new ForeignKeyAttribute<>("EntityId", "org.nuclos.businessentity", "3hPM", "3hPMb", org.nuclos.common.UID.class);


public WebAddonResultList() {
		super("3hPM");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("3hPM");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("3hPM1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getWebAddonId() {
		return getFieldUid("3hPMa");
}


/**
 * Setter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setWebAddonId(org.nuclos.common.UID pWebAddonId) {
		setFieldId("3hPMa", pWebAddonId); 
}


/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.WebAddon getWebAddonBO() {
		return getReferencedBO(org.nuclos.businessentity.WebAddon.class, getFieldUid("3hPMa"), "3hPMa", "hyVG");
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("3hPM4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: propertyValue
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRPROPERTYVALUE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getPropertyValue() {
		return getField("3hPMd", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: propertyValue
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRPROPERTYVALUE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setPropertyValue(java.lang.String pPropertyValue) {
		setField("3hPMd", pPropertyValue); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("3hPM3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: webAddonProperty
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON_PROPERTY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddonProperty
 *<br>Reference field: property
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getWebAddonPropertyId() {
		return getFieldUid("3hPMc");
}


/**
 * Setter-Method for attribute: webAddonProperty
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON_PROPERTY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddonProperty
 *<br>Reference field: property
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setWebAddonPropertyId(org.nuclos.common.UID pWebAddonPropertyId) {
		setFieldId("3hPMc", pWebAddonPropertyId); 
}


/**
 * Getter-Method for attribute: webAddonProperty
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_WEBADDON_PROPERTY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddonProperty
 *<br>Reference field: property
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.WebAddonProperty getWebAddonPropertyBO() {
		return getReferencedBO(org.nuclos.businessentity.WebAddonProperty.class, getFieldUid("3hPMc"), "3hPMc", "9aeC");
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("3hPM2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityId() {
		return getFieldUid("3hPMb");
}


/**
 * Setter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setEntityId(org.nuclos.common.UID pEntityId) {
		setFieldId("3hPMb", pEntityId); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_webAddonResultList
 *<br>DB-Name: STRUID_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Entity getEntityBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("3hPMb"), "3hPMb", "5E8q");
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(WebAddonResultList boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public WebAddonResultList copy() {
		return super.copy(WebAddonResultList.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("3hPM"), id);
}
/**
* Static Get by Id
*/
public static WebAddonResultList get(org.nuclos.common.UID id) {
		return get(WebAddonResultList.class, id);
}
 }
