package org.nuclos.businessentity.facade;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.nuclos.businessentity.WebAddon;
import org.slf4j.LoggerFactory;

public class WebAddonTemplate {

	public static final String ADDON_FILE_NAME_PLACEHOLDER = "${ADDON_FILE_NAME}";
	public static final String ADDON_NAME_CAMEL_CASE_PLACEHOLDER = "${ADDON_NAME_CAMEL_CASE}";

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WebAddonFacade.class);


	/**
	 * copy addon templates and replace placeholders with addon name
	 *
	 * @param webAddon
	 * @param webclientSrc
	 * @param addonOutputPath
	 */
	public static void initializeWebAddonFiles(final WebAddon webAddon, final Path webclientSrc, final Path addonOutputPath) {

		LOG.info("Writing addon template to: " + addonOutputPath);

		try {

			if (!Files.exists(addonOutputPath)) {
				Files.createDirectory(addonOutputPath);
			}



			Files.copy(
					webclientSrc.resolve("addon-template/.npmrc"),
					addonOutputPath.resolve(".npmrc"),
					StandardCopyOption.REPLACE_EXISTING
			);

			/* TODO
			Files.copy(
					webclientSrc.resolve("addon-template/README.md"),
					addonOutputPath.resolve("README.md"),
					StandardCopyOption.REPLACE_EXISTING
			);
			*/

			Files.copy(
					webclientSrc.resolve("addon-template/tsconfig.json"),
					addonOutputPath.resolve("tsconfig.json"),
					StandardCopyOption.REPLACE_EXISTING
			);

			Files.copy(
					webclientSrc.resolve("addon-template/rollup.config.js"),
					addonOutputPath.resolve("rollup.config.js"),
					StandardCopyOption.REPLACE_EXISTING
			);


			Files.createDirectories(
					addonOutputPath.resolve("src")
			);

			writeTemplate(webAddon, webclientSrc, addonOutputPath, "src/index.ts");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "src/" + ADDON_FILE_NAME_PLACEHOLDER + ".component.ts");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "src/" + ADDON_FILE_NAME_PLACEHOLDER + ".module.ts");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "src/" + ADDON_FILE_NAME_PLACEHOLDER + ".service.ts");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "package.json");
			writeTemplate(webAddon, webclientSrc, addonOutputPath, "package-dist.json");
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, addonOutputPath, ex);
		}
	}

	private static void writeTemplate(final WebAddon webAddon, final Path webclientSrc, final Path addonOutputPath, final String path) throws IOException {
		String content = new String(Files.readAllBytes(
				webclientSrc.resolve("addon-template/" + path)
		));
		content = content.replace(ADDON_NAME_CAMEL_CASE_PLACEHOLDER, webAddon.getName());
		final String addonFileName = camelCaseToFileName(webAddon.getName());
		content = content.replace(ADDON_FILE_NAME_PLACEHOLDER, addonFileName);
		String outputPath = path.replace(ADDON_FILE_NAME_PLACEHOLDER, addonFileName);
		LOG.info("Write file from template to: " + outputPath);
		Files.write(addonOutputPath.resolve(outputPath), content.toString().getBytes("UTF-8"));
	}


	/**
	 * convert camel case for file name (e.g. "TestAddon" -> "test-addon")
	 *
	 * @param s
	 * @return formatted String
	 */
	public static String camelCaseToFileName(final String s) {
		return s.replaceAll(
				String.format("%s|%s|%s",
						"(?<=[A-Z])(?=[A-Z][a-z])",
						"(?<=[^A-Z])(?=[A-Z])",
						"(?<=[A-Za-z])(?=[^A-Za-z])"
				),
				"-"
		).toLowerCase();
	}


	/**
	 * generate addon.modules.ts which contains the registered addon modules
	 */
	public static boolean generateWebAddonModulesRegistry(List<WebAddon> webAddons, boolean addonDevMode, Path path) {

		final StringBuilder addonModulesRegistry = new StringBuilder();
		addonModulesRegistry.append("/* generated file */\n\n");
		addonModulesRegistry.append("import { NgModule } from '@angular/core';\n\n");
		for (WebAddon webAddon : webAddons) {
			String fileName = camelCaseToFileName(webAddon.getName());
			if (addonDevMode) {
				fileName = "../addons/" + fileName + "/src";
			}
			addonModulesRegistry.append("import { " + webAddon.getName() + "Module } from '" + fileName + "';\n");
		}

		addonModulesRegistry.append("\n@NgModule({\n");
		addonModulesRegistry.append("\timports: [\n");
		for (WebAddon webAddon : webAddons) {
			addonModulesRegistry.append("\t\t" + webAddon.getName() + "Module,\n");
		}
		addonModulesRegistry.append("\t]\n");
		addonModulesRegistry.append("})\n");
		addonModulesRegistry.append("export class AddonModules {\n");
		addonModulesRegistry.append("}\n");

		LOG.info(addonModulesRegistry.toString());

		try {
			String oldFileContent = Files.exists(path) ? new String(Files.readAllBytes(path)) : null;
			final String addonModulesString = addonModulesRegistry.toString();

			// write only if file differs
			if (!addonModulesString.equals(oldFileContent)) {
				Files.write(path, addonModulesString.getBytes("UTF-8"));
			}
		} catch (IOException ex) {
			LOG.error("Unexpected IO ERROR: {} on {}", ex, path, ex);
			return false;
		}

		return true;
	}

}
