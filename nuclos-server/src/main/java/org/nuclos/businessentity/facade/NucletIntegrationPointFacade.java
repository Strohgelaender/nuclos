//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.facade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.Entity;
import org.nuclos.businessentity.EntityField;
import org.nuclos.businessentity.Nuclet;
import org.nuclos.businessentity.NucletIntegrationField;
import org.nuclos.businessentity.NucletIntegrationPoint;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.MetaDbEntityWrapper;
import org.nuclos.server.dblayer.MetaDbFieldWrapper;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.impl.SchemaUtils;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.structure.DbSimpleView;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.report.SchemaCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NucletIntegrationPointFacade {

	@Autowired
	MetaProvider metaProvider;

	@Autowired
	SpringDataBaseHelper dataBaseHelper;

	@Autowired
	SchemaCache schemaCache;

	public void deleteIpView(final NucletIntegrationPoint point) {
		updateOrDeleteIpView(point, true);
	}

	public void createIpView(final NucletIntegrationPoint point) {
		updateOrDeleteIpView(point, false);
	}

	private void updateOrDeleteIpView(final NucletIntegrationPoint point, boolean bDrop) {
		final Collection<MetaDbFieldWrapper> colFields = new ArrayList<>();
		final MetaDbEntityWrapper eWrapper = getEntityWrapperForView(point, colFields);
		if (eWrapper == null) {
			return;
		}

		final MetaDbHelper helper = new MetaDbHelper(E.getSchemaHelperVersion(), metaProvider, null, null);
		final DbTable dbTable = helper.getDbTable(eWrapper, colFields);
		final List<DbSimpleView> viewArtifacts = dbTable.getTableArtifacts(DbSimpleView.class);
		final Object falseRepresentationInSQL = dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(false);
		if (Boolean.TRUE.equals(point.getStateful())) {
			for (DbSimpleView view : viewArtifacts) {
				for (DbSimpleView.DbSimpleViewColumn column : view.getViewColumns()) {
					if (SF.LOGICALDELETED.getDbColumn().equals(column.getColumnName())) {
						column.setSimpleCondition(" = " + falseRepresentationInSQL);
						column.setHidden(true);
					}
				}
			}
		}

		final List<DbStructureChange> dbStructureChanges = new ArrayList<>();
		if (bDrop) {
			dbStructureChanges.addAll(SchemaUtils.drop(viewArtifacts));
		} else {
			dbStructureChanges.addAll(SchemaUtils.create(viewArtifacts));
		}
		for (DbStructureChange change : dbStructureChanges) {
			dataBaseHelper.getDbAccess().execute(change);
		}
		schemaCache.invalidate();
	}

	public void getAllEntityWrapperForViews(final Collection<MetaDbEntityWrapper> colEntities, final Collection<MetaDbFieldWrapper> colFields) {
		for (NucletIntegrationPoint point : QueryProvider.execute(QueryProvider.create(NucletIntegrationPoint.class))) {
			final MetaDbEntityWrapper eWrapper = getEntityWrapperForView(point, colFields);
			if (eWrapper == null) {
				continue;
			}
			colEntities.add(eWrapper);
		}
	}

	private MetaDbEntityWrapper getEntityWrapperForView(final NucletIntegrationPoint point, final Collection<MetaDbFieldWrapper> fieldResult) {
		final Nuclet nucletBO = point.getNucletBO();
		Entity targetEntityBO = point.getTargetEntityBO();
		if (point.getView() == null || nucletBO == null) {
			return null;
		}

		final boolean bDummy = targetEntityBO == null;
		if (bDummy) {
			// not integrated yet
			targetEntityBO = new Entity();
			targetEntityBO.setId(E.DUMMY.getUID());
			targetEntityBO.setDbtable(E.DUMMY.getDbTable());
		}

		final String sView = nucletBO.getLocalidentifier() + "_IP_" + point.getView();
		final UID viewUID = point.getId()==null? new UID() : point.getId();

		EntityMetaVO eMeta = new EntityMetaVO(UID.class);
		eMeta.setUID(viewUID);
		eMeta.setDbTable(targetEntityBO.getDbtable());
		MetaDbEntityWrapper eWrapper = new MetaDbEntityWrapper(eMeta);
		eWrapper.setIntegrationPoint(true);
		eWrapper.setAlias(sView.toUpperCase());

		Collection<FieldMeta> colFieldMetas = new ArrayList<>();
		List<NucletIntegrationField> lstFields = new ArrayList<>(point.getNucletIntegrationField1());
		Collections.sort(lstFields, new Comparator<NucletIntegrationField>() {
			@Override
			public int compare(final NucletIntegrationField o1, final NucletIntegrationField o2) {
				return StringUtils.compareIgnoreCase(o1.getName(), o2.getName());
			}
		});
		Set<String> allViewColumns = new HashSet<>();
		createSystemField(SF.PK_ID.getMetaData(viewUID), colFieldMetas, fieldResult, allViewColumns, bDummy);
		createSystemField(SF.CREATEDAT.getMetaData(viewUID), colFieldMetas, fieldResult, allViewColumns, bDummy);
		createSystemField(SF.CREATEDBY.getMetaData(viewUID), colFieldMetas, fieldResult, allViewColumns, bDummy);
		createSystemField(SF.CHANGEDAT.getMetaData(viewUID), colFieldMetas, fieldResult, allViewColumns, bDummy);
		createSystemField(SF.CHANGEDBY.getMetaData(viewUID), colFieldMetas, fieldResult, allViewColumns, bDummy);
		if (Boolean.TRUE.equals(point.getStateful())) {
			createSystemField(SF.STATE_UID.getMetaData(viewUID), colFieldMetas, fieldResult, allViewColumns, bDummy);
			createSystemField(SF.PROCESS_UID.getMetaData(viewUID), colFieldMetas, fieldResult, allViewColumns, bDummy);
			createSystemField(SF.LOGICALDELETED.getMetaData(viewUID), colFieldMetas, fieldResult, allViewColumns, bDummy);
		}
		for (NucletIntegrationField field : lstFields) {
			final EntityField targetFieldBO = field.getTargetFieldBO();
			if (targetFieldBO != null && targetFieldBO.getCalcAttributeDSId() != null) {
				// Not supported yet
				continue;
			}

			Mutable<String> viewColumn = new Mutable<>(NuclosEntityValidator.escapeMutatedVowel(field.getName()).toUpperCase());
			viewColumn.setValue(viewColumn.getValue().replaceAll("[^A-Z0-9_]", "_"));

			boolean bValid = false;
			while (!bValid) {
				boolean bNameAdjusted = false;
				for (String other : allViewColumns) {
					if (adjustNameIfNecessary(viewColumn, other, 30)) {
						bNameAdjusted = true;
					}
				}
				if (!bNameAdjusted) {
					bValid = true;
				}
			}
			allViewColumns.add(viewColumn.getValue());

			FieldMetaVO fMeta = new FieldMetaVO();
			fMeta.setUID(field.getId());
			final MetaDbFieldWrapper fieldWrapper = new MetaDbFieldWrapper(fMeta);
			if (targetFieldBO != null) {
				boolean refsToUidEntity = false;
				if (E.isNuclosEntity(targetFieldBO.getForeignentityId())) {
					refsToUidEntity = E.getByUID(targetFieldBO.getForeignentityId()).isUidEntity();
				}
				if (targetFieldBO.getDbfield().startsWith("STRVALUE_") || targetFieldBO.getDbfield().startsWith("INTVALUE_") || targetFieldBO.getDbfield().startsWith("OBJVALUE_")) {
					fMeta.setDbColumn((refsToUidEntity ? "STRUID_" : "INTID_") + targetFieldBO.getDbfield().substring(9));
				} else {
					fMeta.setDbColumn(targetFieldBO.getDbfield());
				}
				if (targetFieldBO.getForeignentityId() != null || targetFieldBO.getForeignIntegrationPointId() != null) {
					fMeta.setDataType(refsToUidEntity ? UID.class.getCanonicalName() : Long.class.getCanonicalName());
					fMeta.setScale(refsToUidEntity ? SF.UID_SCALE : SF.INTID_SCALE);
				} else {
					fMeta.setDataType(field.getDatatype());
					fMeta.setPrecision(field.getDataprecision());
					fMeta.setDataType(field.getDatatype());
				}
				fMeta.setCalcFunction(targetFieldBO.getCalcfunction());
			} else {
				// not integrated yet or optional -> null column
				fMeta.setDbColumn("null");
				if (field.getDatatype() == null) {
					if (field.getIntegrationPointReferenceFieldId() != null) {
						fMeta.setDataType(Long.class.getCanonicalName());
						fMeta.setScale(SF.INTID_SCALE);
					} else {
						if (field.getEntityReferenceFieldId() != null) {
							final EntityMeta<Object> internalEntityMeta = E.getByUID(field.getEntityReferenceFieldId());
							boolean refsToUidEntity = internalEntityMeta != null && internalEntityMeta.isUidEntity();
							fMeta.setDataType(refsToUidEntity ? UID.class.getCanonicalName() : Long.class.getCanonicalName());
							fMeta.setScale(refsToUidEntity ? SF.UID_SCALE : SF.INTID_SCALE);
						} else {
							// field without datatype is not supported
							continue;
						}
					}
				} else {
					fMeta.setDataType(field.getDatatype());
					fMeta.setPrecision(field.getDataprecision());
					fMeta.setScale(field.getDatascale());
				}
				fieldWrapper.setIsNull(true);
			}
			fMeta.setForeignEntity(field.getEntityReferenceFieldId());

			fieldWrapper.setAlias(viewColumn.getValue());

			fieldResult.add(fieldWrapper);
			colFieldMetas.add(fMeta);
		}

		eMeta.setFields(colFieldMetas);
		return eWrapper;
	}

	private static void createSystemField(final FieldMeta<?> systemField,
										  final Collection<FieldMeta> colFieldMetas,
										  final Collection<MetaDbFieldWrapper> colFieldWrappers,
										  final Set<String> allViewColumns,
										  final boolean bDummy) {
		FieldMetaVO fMeta = new FieldMetaVO();
		fMeta.setUID(systemField.getUID());
		if (systemField.getForeignEntity() != null && systemField.getDbColumn().toUpperCase().startsWith("STRVALUE_")) {
			fMeta.setDbColumn("STRUID_" + systemField.getDbColumn().substring(9));
		} else {
			fMeta.setDbColumn(systemField.getDbColumn());
		}
		if (systemField.getForeignEntity() != null ) {
			fMeta.setDataType(UID.class.getCanonicalName());
		} else {
			fMeta.setDataType(systemField.getDataType());
		}
		fMeta.setPrecision(systemField.getPrecision());
		fMeta.setScale(systemField.getScale());
		fMeta.setForeignEntity(systemField.getForeignEntity());
		colFieldMetas.add(fMeta);
		final MetaDbFieldWrapper fieldWrapper = new MetaDbFieldWrapper(fMeta);
		fieldWrapper.setAlias(fMeta.getDbColumn().toUpperCase());
		fieldWrapper.setIsNull(bDummy);
		allViewColumns.add(fieldWrapper.getAlias());
		colFieldWrappers.add(fieldWrapper);
	}

	public static boolean adjustNameIfNecessary(Mutable<String> name, String sValidationName, int maxLength) {
		String sName = name.getValue();
		if (maxLength > 0 && sName.length() > maxLength) {
			name.setValue(sName.substring(0, maxLength));
			return true;
		}
		if (sName.equalsIgnoreCase(sValidationName)) {
			String numbers[] = sName.split("[^0-9]+");
			String sNewName;
			if (numbers.length > 0) {
				String sLastDigits = numbers[numbers.length-1];
				int newCount = Integer.valueOf(sLastDigits) + 1;
				String sNameWithoutDigits = sName.substring(0, sName.length() - sLastDigits.length());
				sNewName = sNameWithoutDigits + newCount;
				if (maxLength > 0 && sNewName.length() > maxLength) {
					sNewName = sNameWithoutDigits.substring(0, sNameWithoutDigits.length() - 1);
				}
			} else {
				sNewName = sName + 1;
				if (maxLength > 0 && sNewName.length() > maxLength) {
					sNewName = sNewName.substring(0, maxLength - 2);
				}
			}
			name.setValue(sNewName);
			return true;
		}
		return false;
	}


}
