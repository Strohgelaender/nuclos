//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_nucletIntegrationPoint
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_NUCLET_INTEGRPOINT
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class NucletIntegrationPoint extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "kIL5", "kIL50", org.nuclos.common.UID.class);


/**
 * Attribute: view
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRVIEW
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 22
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> View = new StringAttribute<>("View", "org.nuclos.businessentity", "kIL5", "kIL5k", java.lang.String.class);


/**
 * Attribute: readonly
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Readonly = 
	new Attribute<>("Readonly", "org.nuclos.businessentity", "kIL5", "kIL5i", java.lang.Boolean.class);


/**
 * Attribute: stateful
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNSTATEFUL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Stateful = 
	new Attribute<>("Stateful", "org.nuclos.businessentity", "kIL5", "kIL5j", java.lang.Boolean.class);


/**
 * Attribute: readme
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRREADME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Readme = new StringAttribute<>("Readme", "org.nuclos.businessentity", "kIL5", "kIL5g", java.lang.String.class);


/**
 * Attribute: optional
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNOPTIONAL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Optional = 
	new Attribute<>("Optional", "org.nuclos.businessentity", "kIL5", "kIL5h", java.lang.Boolean.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "kIL5", "kIL53", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "kIL5", "kIL54", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "kIL5", "kIL51", java.util.Date.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "kIL5", "kIL52", java.lang.String.class);


/**
 * Attribute: problem
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNPROBLEM
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Problem = 
	new Attribute<>("Problem", "org.nuclos.businessentity", "kIL5", "kIL5e", java.lang.Boolean.class);


/**
 * Attribute: note
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRNOTE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Note = new StringAttribute<>("Note", "org.nuclos.businessentity", "kIL5", "kIL5f", java.lang.String.class);


/**
 * Attribute: targetEntity
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> TargetEntityId = 
	new ForeignKeyAttribute<>("TargetEntityId", "org.nuclos.businessentity", "kIL5", "kIL5c", org.nuclos.common.UID.class);


/**
 * Attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> TargetNucletId = 
	new ForeignKeyAttribute<>("TargetNucletId", "org.nuclos.businessentity", "kIL5", "kIL5d", org.nuclos.common.UID.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "kIL5", "kIL5a", org.nuclos.common.UID.class);


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<>("Name", "org.nuclos.businessentity", "kIL5", "kIL5b", java.lang.String.class);

public static final Dependent<org.nuclos.businessentity.NucletIntegrationProblem> _NucletIntegrationProblem = 
	new Dependent<>("_NucletIntegrationProblem", "null", "NucletIntegrationProblem", "0xHU", "integrationPoint", "0xHUa", org.nuclos.businessentity.NucletIntegrationProblem.class);

public static final Dependent<org.nuclos.businessentity.NucletIntegrationField> _NucletIntegrationField1 = 
	new Dependent<>("_NucletIntegrationField1", "null", "NucletIntegrationField", "ECUw", "integrationPoint", "ECUwa", org.nuclos.businessentity.NucletIntegrationField.class);

public static final Dependent<org.nuclos.businessentity.NucletIntegrationField> _NucletIntegrationField2 = 
	new Dependent<>("_NucletIntegrationField2", "null", "NucletIntegrationField", "ECUw", "integrationPointReferenceField", "ECUwd", org.nuclos.businessentity.NucletIntegrationField.class);

public static final Dependent<org.nuclos.businessentity.EntityField> _EntityField = 
	new Dependent<>("_EntityField", "null", "EntityField", "Khi5", "foreignIntegrationPoint", "Khi5V", org.nuclos.businessentity.EntityField.class);


public NucletIntegrationPoint() {
		super("kIL5");
		setReadonly(java.lang.Boolean.FALSE);
		setStateful(java.lang.Boolean.FALSE);
		setOptional(java.lang.Boolean.FALSE);
		setProblem(java.lang.Boolean.FALSE);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("kIL5");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: view
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRVIEW
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 22
 *<br>Precision: null
**/
public java.lang.String getView() {
		return getField("kIL5k", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: view
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRVIEW
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 22
 *<br>Precision: null
**/
public void setView(java.lang.String pView) {
		setField("kIL5k", pView); 
}


/**
 * Getter-Method for attribute: readonly
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getReadonly() {
		return getField("kIL5i", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: readonly
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setReadonly(java.lang.Boolean pReadonly) {
		setField("kIL5i", pReadonly); 
}


/**
 * Getter-Method for attribute: stateful
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNSTATEFUL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getStateful() {
		return getField("kIL5j", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: stateful
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNSTATEFUL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setStateful(java.lang.Boolean pStateful) {
		setField("kIL5j", pStateful); 
}


/**
 * Getter-Method for attribute: readme
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRREADME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getReadme() {
		return getField("kIL5g", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: readme
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRREADME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setReadme(java.lang.String pReadme) {
		setField("kIL5g", pReadme); 
}


/**
 * Getter-Method for attribute: optional
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNOPTIONAL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getOptional() {
		return getField("kIL5h", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: optional
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNOPTIONAL
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setOptional(java.lang.Boolean pOptional) {
		setField("kIL5h", pOptional); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("kIL53", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("kIL54", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("kIL51", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("kIL52", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: problem
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNPROBLEM
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getProblem() {
		return getField("kIL5e", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: problem
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: BLNPROBLEM
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setProblem(java.lang.Boolean pProblem) {
		setField("kIL5e", pProblem); 
}


/**
 * Getter-Method for attribute: note
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRNOTE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public java.lang.String getNote() {
		return getField("kIL5f", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: note
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRNOTE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setNote(java.lang.String pNote) {
		setField("kIL5f", pNote); 
}


/**
 * Getter-Method for attribute: targetEntity
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getTargetEntityId() {
		return getFieldUid("kIL5c");
}


/**
 * Setter-Method for attribute: targetEntity
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setTargetEntityId(org.nuclos.common.UID pTargetEntityId) {
		setFieldId("kIL5c", pTargetEntityId); 
}


/**
 * Getter-Method for attribute: targetEntity
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Entity getTargetEntityBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("kIL5c"), "kIL5c", "5E8q");
}


/**
 * Getter-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getTargetNucletId() {
		return getFieldUid("kIL5d");
}


/**
 * Setter-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setTargetNucletId(org.nuclos.common.UID pTargetNucletId) {
		setFieldId("kIL5d", pTargetNucletId); 
}


/**
 * Getter-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Nuclet getTargetNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("kIL5d"), "kIL5d", "xojr");
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("kIL5a");
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("kIL5a", pNucletId); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("kIL5a"), "kIL5a", "xojr");
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getName() {
		return getField("kIL5b", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRNAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("kIL5b", pName); 
}


/**
 * Getter-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationProblem
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationProblem> getNucletIntegrationProblem(Flag... flags) {
		return getDependents(_NucletIntegrationProblem, flags); 
}


/**
 * Insert-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationProblem
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationProblem(org.nuclos.businessentity.NucletIntegrationProblem pNucletIntegrationProblem) {
		insertDependent(_NucletIntegrationProblem, pNucletIntegrationProblem);
}


/**
 * Delete-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationProblem
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationProblem(org.nuclos.businessentity.NucletIntegrationProblem pNucletIntegrationProblem) {
		deleteDependent(_NucletIntegrationProblem, pNucletIntegrationProblem);
}


/**
 * Getter-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationField> getNucletIntegrationField1(Flag... flags) {
		return getDependents(_NucletIntegrationField1, flags); 
}


/**
 * Insert-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationField1(org.nuclos.businessentity.NucletIntegrationField pNucletIntegrationField) {
		insertDependent(_NucletIntegrationField1, pNucletIntegrationField);
}


/**
 * Delete-Method for attribute: integrationPoint
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationField1(org.nuclos.businessentity.NucletIntegrationField pNucletIntegrationField) {
		deleteDependent(_NucletIntegrationField1, pNucletIntegrationField);
}


/**
 * Getter-Method for attribute: integrationPointReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationField> getNucletIntegrationField2(Flag... flags) {
		return getDependents(_NucletIntegrationField2, flags); 
}


/**
 * Insert-Method for attribute: integrationPointReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationField2(org.nuclos.businessentity.NucletIntegrationField pNucletIntegrationField) {
		insertDependent(_NucletIntegrationField2, pNucletIntegrationField);
}


/**
 * Delete-Method for attribute: integrationPointReferenceField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_INTEGRPOINT_REFFIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationField2(org.nuclos.businessentity.NucletIntegrationField pNucletIntegrationField) {
		deleteDependent(_NucletIntegrationField2, pNucletIntegrationField);
}


/**
 * Getter-Method for attribute: foreignIntegrationPoint
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGN_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.EntityField> getEntityField(Flag... flags) {
		return getDependents(_EntityField, flags); 
}


/**
 * Insert-Method for attribute: foreignIntegrationPoint
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGN_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertEntityField(org.nuclos.businessentity.EntityField pEntityField) {
		insertDependent(_EntityField, pEntityField);
}


/**
 * Delete-Method for attribute: foreignIntegrationPoint
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGN_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteEntityField(org.nuclos.businessentity.EntityField pEntityField) {
		deleteDependent(_EntityField, pEntityField);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(NucletIntegrationPoint boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public NucletIntegrationPoint copy() {
		return super.copy(NucletIntegrationPoint.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("kIL5"), id);
}
/**
* Static Get by Id
*/
public static NucletIntegrationPoint get(org.nuclos.common.UID id) {
		return get(NucletIntegrationPoint.class, id);
}
 }
