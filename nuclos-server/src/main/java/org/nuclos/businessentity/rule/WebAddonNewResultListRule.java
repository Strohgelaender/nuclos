//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.CustomContext;
import org.nuclos.api.context.MessageContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.businessentity.WebAddon;
import org.nuclos.businessentity.WebAddonProperty;
import org.nuclos.businessentity.WebAddonResultList;
import org.nuclos.businessentity.facade.WebAddonFacade;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.UID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "WebAddonNewResultListRule",
		description = "")
@SystemRuleUsage(
		boClass = WebAddon.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class WebAddonNewResultListRule implements CustomRule {

	@Autowired
	private WebAddonFacade webAddonFacade;

	@Override
	public void custom(final CustomContext context) throws BusinessException {
		final WebAddon webAddon = context.getBusinessObject(WebAddon.class);
		if (!Boolean.TRUE.equals(webAddon.getResultlist())) {
			throw new BusinessException("This addon does not support result lists.");
		}
		final UID entityId = webAddon.getNewResultListId();
		if (entityId != null) {
			for (WebAddonProperty prop : webAddon.getWebAddonProperty()) {
				WebAddonResultList reslist = new WebAddonResultList();
				reslist.setEntityId(entityId);
				reslist.setWebAddonPropertyId(prop.getId());
				webAddon.insertWebAddonResultList(reslist);
			}
		}
	}
}
