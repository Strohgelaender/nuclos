//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.LogContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.businessentity.Nuclet;
import org.nuclos.businessentity.facade.NucletFacade;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "NucletSaveFinalRule",
		description = "")
@SystemRuleUsage(
		boClass = Nuclet.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class NucletSaveFinalRule implements InsertFinalRule, UpdateFinalRule {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NucletSaveFinalRule.class);

	@Autowired
	private NucletFacade nucletFacade;

	@Override
	public void insertFinal(final InsertContext insertContext) throws BusinessException {
		this.saveFinal(insertContext.getBusinessObject(Nuclet.class), insertContext);
	}

	@Override
	public void updateFinal(final UpdateContext updateContext) throws BusinessException {
		this.saveFinal(updateContext.getBusinessObject(Nuclet.class), updateContext);
	}

	private void saveFinal(final Nuclet nuclet, LogContext log) throws BusinessException {
		try {
			nucletFacade.updateExtensionsIfNecessary();
		} finally {
			NucletSaveRule.UPDATE_EXTENSIONS.set(false);
		}
	}

}
