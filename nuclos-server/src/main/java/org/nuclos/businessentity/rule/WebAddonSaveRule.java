//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.annotation.Rule;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.LogContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.WebAddon;
import org.nuclos.businessentity.WebAddonFile;
import org.nuclos.businessentity.WebAddonProperty;
import org.nuclos.businessentity.WebAddonResultList;
import org.nuclos.businessentity.facade.WebAddonFacade;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import sun.misc.BASE64Encoder;

@Rule(
		name = "WebAddonSaveRule",
		description = "")
@SystemRuleUsage(
		boClass = WebAddon.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class WebAddonSaveRule implements InsertRule, UpdateRule {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(WebAddonSaveRule.class);

	public static final ThreadLocal<Boolean> DISABLE_OUTPUT = new ThreadLocal<>();
	static {
		DISABLE_OUTPUT.set(false);
	}

	@Autowired
	private WebAddonFacade webAddonFacade;

	@Override
	public void insert(final InsertContext insertContext) throws BusinessException {
		WebAddon webAddon = insertContext.getBusinessObject(WebAddon.class);
		if (webAddon.getActive()) {
			webAddonFacade.initializeWebAddonFiles(webAddon);
		}
		this.save(webAddon, insertContext);
	}

	@Override
	public void update(final UpdateContext updateContext) throws BusinessException {
		WebAddon webAddon = updateContext.getBusinessObject(WebAddon.class);

		// initialize only if webaddon wasn't already initialized (e.g. created with active=false)
		final boolean initialize = webAddon.getActive() && webAddon.getWebAddonFile().isEmpty() && !webAddonFacade.isWebAddonInitialized(webAddon);
		LOG.info("Initialize addon {} : {}", webAddon.getName(), initialize);
		if (initialize) {
			webAddonFacade.initializeWebAddonFiles(webAddon);
		}
		this.save(webAddon, updateContext);
	}

	private void save(final WebAddon webAddon, LogContext log) throws BusinessException {

		// adjust name
		String name = webAddon.getName();
		if (name == null) {
			return; // no exception. nuclos checks for non null...
		}
		name = name.replaceAll("[^A-Za-z0-9]", "");
		webAddon.setName(name);

		// check result list properties
		if (Boolean.TRUE.equals(webAddon.getResultlist())) {
			Map<UID, String> propertyTypes = new HashMap<>();
			for (WebAddonProperty prop : webAddon.getWebAddonProperty()) {
				if (!String.class.getCanonicalName().equals(prop.getType()) &&
					!Integer.class.getCanonicalName().equals(prop.getType()) &&
					!Boolean.class.getCanonicalName().equals(prop.getType())) {
					throw new BusinessException("Property type \"" + prop.getType() + "\" is not supported. Select between "
						+ String.class.getCanonicalName() + ", "
						+ Integer.class.getCanonicalName() + " or "
						+ Boolean.class.getCanonicalName());
				}
				propertyTypes.put(prop.getId(), prop.getType());
			}
			// check result list
			for (WebAddonResultList reslist : webAddon.getWebAddonResultList()) {
				String value = reslist.getPropertyValue();
				if (value == null) {
					continue;
				}
				final String type = propertyTypes.get(reslist.getWebAddonPropertyId());
				if (type != null) {
					if (type.equals(Integer.class.getCanonicalName())) {
						try {
							Integer.parseInt(value);
						} catch (Exception ex) {
							throw new BusinessException("Result list property value \"" + value + "\" is not an Integer.");
						}
					}
					if (type.equals(Boolean.class.getCanonicalName())) {
						// allow only 'yes' or 'no' (like the layout)
						if ("yes".equalsIgnoreCase(value) || "no".equalsIgnoreCase(value)) {
							if (!"yes".equals(value) || !"no".equals(value)) {
								reslist.setPropertyValue(value.toLowerCase());
							}
						} else {
							try {
								boolean bValue = Boolean.parseBoolean(value);
								reslist.setPropertyValue(bValue ? "yes" : "no");
							} catch (Exception ex) {
								throw new BusinessException("Result list property value \"" + value + "\" is not a Boolean: " + ex.getMessage());
							}
						}
					}
				}
			}
		} else {
			for (WebAddonResultList reslist : webAddon.getWebAddonResultList()) {
				webAddon.deleteWebAddonResultList(reslist);
			}
		}

		WebAddon webAddonDb = null;
		if (!webAddon.isInsert()) {
			webAddonDb = QueryProvider.getById(WebAddon.class, webAddon.getId());
		}
		boolean bNewOutput = false;
		if (webAddon.isInsert()) {
			// new addon
			bNewOutput = true;
		}
		if (!bNewOutput && !webAddon.getWebAddonFile(Flag.DELETE, Flag.UPDATE, Flag.INSERT).isEmpty()) {
			// addon files have changed
			bNewOutput = true;
		}
		if (!bNewOutput && webAddonDb != null && Boolean.TRUE.equals(webAddon.getActive()) != Boolean.TRUE.equals(webAddonDb.getActive())) {
			// addon has been activated or deactivated
			bNewOutput = true;
		}
		if (!bNewOutput && webAddonDb != null && !name.equals(webAddonDb.getName())) {
			// addon name has changed
			bNewOutput = true;
		}

		boolean bDisabled = !Boolean.TRUE.equals(webAddon.getActive()) || (webAddonDb != null && !Boolean.TRUE.equals(webAddonDb.getActive()));

		if (bNewOutput && !Boolean.TRUE.equals(DISABLE_OUTPUT.get())) {

			List<WebAddon> activeWebAddons = webAddonFacade.getActiveWebAddons();
			// consider changes in unsaved "webAddon"
			if (webAddon.getActive()) {
				if (!activeWebAddons.contains(webAddon)) {
					activeWebAddons.add(webAddon);
				}
			} else {
				activeWebAddons = activeWebAddons.stream().filter(a -> !a.getId().equals(webAddon.getId())).collect(Collectors.toList());
			}

			webAddonFacade.generateWebAddonModulesRegistry(activeWebAddons);
			if (webAddonFacade.outputWebAddon(webAddon)) {
				if (!webAddonFacade.isAddonDevMode()) {
					if (!webAddonFacade.buildWebAddon(webAddon)) {
						LOG.error("Error compiling webAddon " + webAddon.getName());
					}
					webAddonFacade.buildWebclient(activeWebAddons);
				}
			} else {
				if (bDisabled) {
					webAddonFacade.buildWebclient(activeWebAddons);
				}
			}
		}

		if (webAddon.getNewResultListId() != null) {
			webAddon.setNewResultListId(null);
		}

		// update file hashes
		boolean bHashUpdated = false;
		for (WebAddonFile webAddonFile : webAddon.getWebAddonFile(Flag.UPDATE, Flag.INSERT)) {
			updateHash(webAddonFile);
			bHashUpdated = true;
		}
		for (WebAddonFile webAddonFile : webAddon.getWebAddonFile(Flag.NONE)) {
			if (StringUtils.isEmpty(webAddonFile.getHash())) {
				updateHash(webAddonFile);
				bHashUpdated = true;
			}
		}
		if (bHashUpdated && Boolean.TRUE.equals(DISABLE_OUTPUT.get())) {
			WebAddonSaveFinalRule.WRITE_HASH_VALUES.set(true);
		}
	}

	private void updateHash(WebAddonFile webAddonFile) {
		try {
			final byte[] content = webAddonFile.getFile().getContent();
			final MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(content);
			final byte[] hash = digest.digest();
			webAddonFile.setHash(new BASE64Encoder().encode(hash));
		} catch (NoSuchAlgorithmException e) {
			throw new NuclosFatalException(e);
		}
	}
}
