//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.rule;

import org.nuclos.api.annotation.Rule;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.context.InsertContext;
import org.nuclos.api.context.LogContext;
import org.nuclos.api.context.UpdateContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.Nuclet;
import org.nuclos.businessentity.NucletExtension;
import org.nuclos.businessentity.facade.NucletFacade;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Rule(
		name = "NucletSaveRule",
		description = "")
@SystemRuleUsage(
		boClass = Nuclet.class,
		order = 1
)
@Configurable
@SuppressWarnings("unused") // system internal usage
public class NucletSaveRule implements InsertRule, UpdateRule {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NucletSaveRule.class);

	public static final ThreadLocal<Boolean> UPDATE_EXTENSIONS = new ThreadLocal<>();
	static {
		UPDATE_EXTENSIONS.set(false);
	}

	@Autowired
	private NucletFacade nucletFacade;

	@Override
	public void insert(final InsertContext insertContext) throws BusinessException {
		this.save(insertContext.getBusinessObject(Nuclet.class), insertContext);
	}

	@Override
	public void update(final UpdateContext updateContext) throws BusinessException {
		this.save(updateContext.getBusinessObject(Nuclet.class), updateContext);
	}

	private void save(final Nuclet nuclet, LogContext log) throws BusinessException {
		UPDATE_EXTENSIONS.set(false);
		boolean bUpdateExtensions = false;
		for (NucletExtension nExt : nuclet.getNucletExtension(Flag.INSERT, Flag.UPDATE)) {
			if (nExt.getFile() == null) {
				// ignore everything. Db NOT NULL triggers rollback...
				return;
			}
			nExt.setHash(nucletFacade.getHash(nExt));
			bUpdateExtensions = true;
		}
		if (bUpdateExtensions) {
			UPDATE_EXTENSIONS.set(true);
		}
	}

}
