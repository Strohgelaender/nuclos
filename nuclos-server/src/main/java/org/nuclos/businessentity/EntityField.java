//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_entityfield
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_ENTITY_FIELD
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class EntityField extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "Khi5", "Khi50", org.nuclos.common.UID.class);


/**
 * Attribute: entityfieldgroup
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY_FIELD_GROUP
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> EntityfieldgroupId = 
	new ForeignKeyAttribute<>("EntityfieldgroupId", "org.nuclos.businessentity", "Khi5", "Khi5b", org.nuclos.common.UID.class);


/**
 * Attribute: entity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> EntityId = 
	new ForeignKeyAttribute<>("EntityId", "org.nuclos.businessentity", "Khi5", "Khi5a", org.nuclos.common.UID.class);


/**
 * Attribute: dbfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDBFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Dbfield = new StringAttribute<>("Dbfield", "org.nuclos.businessentity", "Khi5", "Khi5d", java.lang.String.class);


/**
 * Attribute: field
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Field = new StringAttribute<>("Field", "org.nuclos.businessentity", "Khi5", "Khi5c", java.lang.String.class);


/**
 * Attribute: defaultcomponenttype
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDEFAULTCOMPONENTTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Defaultcomponenttype = new StringAttribute<>("Defaultcomponenttype", "org.nuclos.businessentity", "Khi5", "Khi5f", java.lang.String.class);


/**
 * Attribute: datatype
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDATATYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Datatype = new StringAttribute<>("Datatype", "org.nuclos.businessentity", "Khi5", "Khi5e", java.lang.String.class);


/**
 * Attribute: foreignentityfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFOREIGNENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Foreignentityfield = new StringAttribute<>("Foreignentityfield", "org.nuclos.businessentity", "Khi5", "Khi5h", java.lang.String.class);


/**
 * Attribute: foreignentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGNENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> ForeignentityId = 
	new ForeignKeyAttribute<>("ForeignentityId", "org.nuclos.businessentity", "Khi5", "Khi5g", org.nuclos.common.UID.class);


/**
 * Attribute: searchfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSEARCHFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Searchfield = new StringAttribute<>("Searchfield", "org.nuclos.businessentity", "Khi5", "Khi5J", java.lang.String.class);


/**
 * Attribute: calculationscript
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCULATIONSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<org.nuclos.common.NuclosScript> Calculationscript = 
	new Attribute<>("Calculationscript", "org.nuclos.businessentity", "Khi5", "Khi5I", org.nuclos.common.NuclosScript.class);


/**
 * Attribute: autonumberentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_AUTONUMBERENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> AutonumberentityId = 
	new ForeignKeyAttribute<>("AutonumberentityId", "org.nuclos.businessentity", "Khi5", "Khi5L", org.nuclos.common.UID.class);


/**
 * Attribute: backgroundcolorscript
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRBGCOLORSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<org.nuclos.common.NuclosScript> Backgroundcolorscript = 
	new Attribute<>("Backgroundcolorscript", "org.nuclos.businessentity", "Khi5", "Khi5K", org.nuclos.common.NuclosScript.class);


/**
 * Attribute: ruleBackgroundColor
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRRULEBGCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> RuleBackgroundColor = new StringAttribute<>("RuleBackgroundColor", "org.nuclos.businessentity", "Khi5", "Khi5N", java.lang.String.class);


/**
 * Attribute: ruleCalculation
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRRULECALCULATION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> RuleCalculation = new StringAttribute<>("RuleCalculation", "org.nuclos.businessentity", "Khi5", "Khi5M", java.lang.String.class);


/**
 * Attribute: calcAttributeDS
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_CALCATTRIBUTE
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> CalcAttributeDSId = 
	new ForeignKeyAttribute<>("CalcAttributeDSId", "org.nuclos.businessentity", "Khi5", "Khi5P", org.nuclos.common.UID.class);


/**
 * Attribute: hidden
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNHIDDEN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Hidden = 
	new Attribute<>("Hidden", "org.nuclos.businessentity", "Khi5", "Khi5O", java.lang.Boolean.class);


/**
 * Attribute: calcAttributeParamValues
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCATTRIBUTEPARAMVALUES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CalcAttributeParamValues = new StringAttribute<>("CalcAttributeParamValues", "org.nuclos.businessentity", "Khi5", "Khi5R", java.lang.String.class);


/**
 * Attribute: calcOndemand
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNCALCONDEMAND
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> CalcOndemand = 
	new Attribute<>("CalcOndemand", "org.nuclos.businessentity", "Khi5", "Khi5Q", java.lang.Boolean.class);


/**
 * Attribute: isLocalized
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNISLOCALIZED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> IsLocalized = 
	new Attribute<>("IsLocalized", "org.nuclos.businessentity", "Khi5", "Khi5T", java.lang.Boolean.class);


/**
 * Attribute: calcAttributeAllowCustomization
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNCALCATTRALLOWCUSTOMIZATION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> CalcAttributeAllowCustomization = 
	new Attribute<>("CalcAttributeAllowCustomization", "org.nuclos.businessentity", "Khi5", "Khi5S", java.lang.Boolean.class);


/**
 * Attribute: foreignIntegrationPoint
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGN_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> ForeignIntegrationPointId = 
	new ForeignKeyAttribute<>("ForeignIntegrationPointId", "org.nuclos.businessentity", "Khi5", "Khi5V", org.nuclos.common.UID.class);


/**
 * Attribute: comment
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCOMMENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 350
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Comment = new StringAttribute<>("Comment", "org.nuclos.businessentity", "Khi5", "Khi5U", java.lang.String.class);


/**
 * Attribute: sortationasc
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSORTATIONASC
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Sortationasc = new StringAttribute<>("Sortationasc", "org.nuclos.businessentity", "Khi5", "Khi5z", java.lang.String.class);


/**
 * Attribute: calcfunction
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCFUNCTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Calcfunction = new StringAttribute<>("Calcfunction", "org.nuclos.businessentity", "Khi5", "Khi5y", java.lang.String.class);


/**
 * Attribute: lookupentityfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRLOOKUPENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Lookupentityfield = new StringAttribute<>("Lookupentityfield", "org.nuclos.businessentity", "Khi5", "Khi5j", java.lang.String.class);


/**
 * Attribute: lookupentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_LOOKUPENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> LookupentityId = 
	new ForeignKeyAttribute<>("LookupentityId", "org.nuclos.businessentity", "Khi5", "Khi5i", org.nuclos.common.UID.class);


/**
 * Attribute: dataprecision
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTDATAPRECISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Dataprecision = 
	new NumericAttribute<>("Dataprecision", "org.nuclos.businessentity", "Khi5", "Khi5l", java.lang.Integer.class);


/**
 * Attribute: datascale
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTDATASCALE
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Datascale = 
	new NumericAttribute<>("Datascale", "org.nuclos.businessentity", "Khi5", "Khi5k", java.lang.Integer.class);


/**
 * Attribute: valuedefault
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRVALUE_DEFAULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Valuedefault = new StringAttribute<>("Valuedefault", "org.nuclos.businessentity", "Khi5", "Khi5n", java.lang.String.class);


/**
 * Attribute: foreigndefault
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTID_FOREIGN_DEFAULT
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Long> Foreigndefault = 
	new NumericAttribute<>("Foreigndefault", "org.nuclos.businessentity", "Khi5", "Khi5m", java.lang.Long.class);


/**
 * Attribute: formatoutput
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFORMATOUTPUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Formatoutput = new StringAttribute<>("Formatoutput", "org.nuclos.businessentity", "Khi5", "Khi5p", java.lang.String.class);


/**
 * Attribute: formatinput
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFORMATINPUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Formatinput = new StringAttribute<>("Formatinput", "org.nuclos.businessentity", "Khi5", "Khi5o", java.lang.String.class);


/**
 * Attribute: nullable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNNULLABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Nullable = 
	new Attribute<>("Nullable", "org.nuclos.businessentity", "Khi5", "Khi5r", java.lang.Boolean.class);


/**
 * Attribute: unique
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNUNIQUE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Unique = 
	new Attribute<>("Unique", "org.nuclos.businessentity", "Khi5", "Khi5q", java.lang.Boolean.class);


/**
 * Attribute: modifiable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNMODIFIABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Modifiable = 
	new Attribute<>("Modifiable", "org.nuclos.businessentity", "Khi5", "Khi5t", java.lang.Boolean.class);


/**
 * Attribute: searchable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNSEARCHABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Searchable = 
	new Attribute<>("Searchable", "org.nuclos.businessentity", "Khi5", "Khi5s", java.lang.Boolean.class);


/**
 * Attribute: logbooktracking
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNLOGBOOKTRACKING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Logbooktracking = 
	new Attribute<>("Logbooktracking", "org.nuclos.businessentity", "Khi5", "Khi5v", java.lang.Boolean.class);


/**
 * Attribute: insertable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNINSERTABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Insertable = 
	new Attribute<>("Insertable", "org.nuclos.businessentity", "Khi5", "Khi5u", java.lang.Boolean.class);


/**
 * Attribute: readonly
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Readonly = 
	new Attribute<>("Readonly", "org.nuclos.businessentity", "Khi5", "Khi5x", java.lang.Boolean.class);


/**
 * Attribute: showmnemonic
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNSHOWMNEMONIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Showmnemonic = 
	new Attribute<>("Showmnemonic", "org.nuclos.businessentity", "Khi5", "Khi5w", java.lang.Boolean.class);


/**
 * Attribute: localeresourcel
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localeresourcel = new StringAttribute<>("Localeresourcel", "org.nuclos.businessentity", "Khi5", "Khi5B", java.lang.String.class);


/**
 * Attribute: sortationdesc
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSORTATIONDESC
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Sortationdesc = new StringAttribute<>("Sortationdesc", "org.nuclos.businessentity", "Khi5", "Khi5A", java.lang.String.class);


/**
 * Attribute: permissiontransfer
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNPERMISSIONTRANSFER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Permissiontransfer = 
	new Attribute<>("Permissiontransfer", "org.nuclos.businessentity", "Khi5", "Khi5D", java.lang.Boolean.class);


/**
 * Attribute: localeresourced
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localeresourced = new StringAttribute<>("Localeresourced", "org.nuclos.businessentity", "Khi5", "Khi5C", java.lang.String.class);


/**
 * Attribute: defaultmandatory
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_DEFAULT_MANDATORY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Defaultmandatory = new StringAttribute<>("Defaultmandatory", "org.nuclos.businessentity", "Khi5", "Khi5F", java.lang.String.class);


/**
 * Attribute: indexed
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNINDEXED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Indexed = 
	new Attribute<>("Indexed", "org.nuclos.businessentity", "Khi5", "Khi5E", java.lang.Boolean.class);


/**
 * Attribute: order
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> Order = 
	new NumericAttribute<>("Order", "org.nuclos.businessentity", "Khi5", "Khi5H", java.lang.Integer.class);


/**
 * Attribute: ondeletecascade
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNONDELETECASCADE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Ondeletecascade = 
	new Attribute<>("Ondeletecascade", "org.nuclos.businessentity", "Khi5", "Khi5G", java.lang.Boolean.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "Khi5", "Khi52", java.lang.String.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "Khi5", "Khi51", java.util.Date.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "Khi5", "Khi54", java.lang.String.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "Khi5", "Khi53", java.util.Date.class);

public static final Dependent<org.nuclos.businessentity.NucletIntegrationField> _NucletIntegrationField = 
	new Dependent<>("_NucletIntegrationField", "null", "NucletIntegrationField", "ECUw", "targetField", "ECUwf", org.nuclos.businessentity.NucletIntegrationField.class);


public EntityField() {
		super("Khi5");
		setHidden(java.lang.Boolean.FALSE);
		setCalcOndemand(java.lang.Boolean.FALSE);
		setIsLocalized(java.lang.Boolean.FALSE);
		setCalcAttributeAllowCustomization(java.lang.Boolean.FALSE);
		setNullable(java.lang.Boolean.FALSE);
		setUnique(java.lang.Boolean.FALSE);
		setModifiable(java.lang.Boolean.FALSE);
		setSearchable(java.lang.Boolean.FALSE);
		setLogbooktracking(java.lang.Boolean.FALSE);
		setInsertable(java.lang.Boolean.FALSE);
		setReadonly(java.lang.Boolean.FALSE);
		setShowmnemonic(java.lang.Boolean.FALSE);
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("Khi5");
}


/**
 * Getter-Method for attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
@java.lang.Override
public void setId(org.nuclos.common.UID id) {
		super.setId(id);
}


/**
 * Getter-Method for attribute: entityfieldgroup
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY_FIELD_GROUP
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entityfieldgroup
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.api.UID getEntityfieldgroupId() {
		return getFieldUid("Khi5b");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityId() {
		return getFieldUid("Khi5a");
}


/**
 * Setter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setEntityId(org.nuclos.common.UID pEntityId) {
		setFieldId("Khi5a", pEntityId); 
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_ENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.businessentity.Entity getEntityBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("Khi5a"), "Khi5a", "5E8q");
}


/**
 * Getter-Method for attribute: dbfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDBFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public java.lang.String getDbfield() {
		return getField("Khi5d", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: dbfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDBFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 30
 *<br>Precision: null
**/
public void setDbfield(java.lang.String pDbfield) {
		setField("Khi5d", pDbfield); 
}


/**
 * Getter-Method for attribute: field
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getField() {
		return getField("Khi5c", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: field
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setField(java.lang.String pField) {
		setField("Khi5c", pField); 
}


/**
 * Getter-Method for attribute: defaultcomponenttype
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDEFAULTCOMPONENTTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getDefaultcomponenttype() {
		return getField("Khi5f", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: defaultcomponenttype
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDEFAULTCOMPONENTTYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setDefaultcomponenttype(java.lang.String pDefaultcomponenttype) {
		setField("Khi5f", pDefaultcomponenttype); 
}


/**
 * Getter-Method for attribute: datatype
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDATATYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getDatatype() {
		return getField("Khi5e", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: datatype
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRDATATYPE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setDatatype(java.lang.String pDatatype) {
		setField("Khi5e", pDatatype); 
}


/**
 * Getter-Method for attribute: foreignentityfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFOREIGNENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public java.lang.String getForeignentityfield() {
		return getField("Khi5h", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: foreignentityfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFOREIGNENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public void setForeignentityfield(java.lang.String pForeignentityfield) {
		setField("Khi5h", pForeignentityfield); 
}


/**
 * Getter-Method for attribute: foreignentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGNENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getForeignentityId() {
		return getFieldUid("Khi5g");
}


/**
 * Setter-Method for attribute: foreignentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGNENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setForeignentityId(org.nuclos.common.UID pForeignentityId) {
		setFieldId("Khi5g", pForeignentityId); 
}


/**
 * Getter-Method for attribute: foreignentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGNENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Entity getForeignentityBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("Khi5g"), "Khi5g", "5E8q");
}


/**
 * Getter-Method for attribute: searchfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSEARCHFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public java.lang.String getSearchfield() {
		return getField("Khi5J", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: searchfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSEARCHFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public void setSearchfield(java.lang.String pSearchfield) {
		setField("Khi5J", pSearchfield); 
}


/**
 * Getter-Method for attribute: calculationscript
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCULATIONSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.NuclosScript getCalculationscript() {
		return getField("Khi5I", org.nuclos.common.NuclosScript.class); 
}


/**
 * Setter-Method for attribute: calculationscript
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCULATIONSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setCalculationscript(org.nuclos.common.NuclosScript pCalculationscript) {
		setField("Khi5I", pCalculationscript); 
}


/**
 * Getter-Method for attribute: autonumberentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_AUTONUMBERENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getAutonumberentityId() {
		return getFieldUid("Khi5L");
}


/**
 * Setter-Method for attribute: autonumberentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_AUTONUMBERENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setAutonumberentityId(org.nuclos.common.UID pAutonumberentityId) {
		setFieldId("Khi5L", pAutonumberentityId); 
}


/**
 * Getter-Method for attribute: autonumberentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_AUTONUMBERENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entity
 *<br>Reference field: entity
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Entity getAutonumberentityBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("Khi5L"), "Khi5L", "5E8q");
}


/**
 * Getter-Method for attribute: backgroundcolorscript
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRBGCOLORSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.NuclosScript getBackgroundcolorscript() {
		return getField("Khi5K", org.nuclos.common.NuclosScript.class); 
}


/**
 * Setter-Method for attribute: backgroundcolorscript
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRBGCOLORSCRIPT
 *<br>Data type: org.nuclos.common.NuclosScript
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setBackgroundcolorscript(org.nuclos.common.NuclosScript pBackgroundcolorscript) {
		setField("Khi5K", pBackgroundcolorscript); 
}


/**
 * Getter-Method for attribute: ruleBackgroundColor
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRRULEBGCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.String getRuleBackgroundColor() {
		return getField("Khi5N", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: ruleBackgroundColor
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRRULEBGCOLOR
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setRuleBackgroundColor(java.lang.String pRuleBackgroundColor) {
		setField("Khi5N", pRuleBackgroundColor); 
}


/**
 * Getter-Method for attribute: ruleCalculation
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRRULECALCULATION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.String getRuleCalculation() {
		return getField("Khi5M", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: ruleCalculation
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRRULECALCULATION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setRuleCalculation(java.lang.String pRuleCalculation) {
		setField("Khi5M", pRuleCalculation); 
}


/**
 * Getter-Method for attribute: calcAttributeDS
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_T_MD_CALCATTRIBUTE
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_calcAttribute
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.api.UID getCalcAttributeDSId() {
		return getFieldUid("Khi5P");
}


/**
 * Getter-Method for attribute: hidden
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNHIDDEN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getHidden() {
		return getField("Khi5O", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: hidden
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNHIDDEN
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setHidden(java.lang.Boolean pHidden) {
		setField("Khi5O", pHidden); 
}


/**
 * Getter-Method for attribute: calcAttributeParamValues
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCATTRIBUTEPARAMVALUES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public java.lang.String getCalcAttributeParamValues() {
		return getField("Khi5R", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: calcAttributeParamValues
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCATTRIBUTEPARAMVALUES
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setCalcAttributeParamValues(java.lang.String pCalcAttributeParamValues) {
		setField("Khi5R", pCalcAttributeParamValues); 
}


/**
 * Getter-Method for attribute: calcOndemand
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNCALCONDEMAND
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getCalcOndemand() {
		return getField("Khi5Q", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: calcOndemand
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNCALCONDEMAND
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setCalcOndemand(java.lang.Boolean pCalcOndemand) {
		setField("Khi5Q", pCalcOndemand); 
}


/**
 * Getter-Method for attribute: isLocalized
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNISLOCALIZED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getIsLocalized() {
		return getField("Khi5T", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: isLocalized
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNISLOCALIZED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setIsLocalized(java.lang.Boolean pIsLocalized) {
		setField("Khi5T", pIsLocalized); 
}


/**
 * Getter-Method for attribute: calcAttributeAllowCustomization
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNCALCATTRALLOWCUSTOMIZATION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getCalcAttributeAllowCustomization() {
		return getField("Khi5S", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: calcAttributeAllowCustomization
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNCALCATTRALLOWCUSTOMIZATION
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setCalcAttributeAllowCustomization(java.lang.Boolean pCalcAttributeAllowCustomization) {
		setField("Khi5S", pCalcAttributeAllowCustomization); 
}


/**
 * Getter-Method for attribute: foreignIntegrationPoint
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGN_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getForeignIntegrationPointId() {
		return getFieldUid("Khi5V");
}


/**
 * Setter-Method for attribute: foreignIntegrationPoint
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGN_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setForeignIntegrationPointId(org.nuclos.common.UID pForeignIntegrationPointId) {
		setFieldId("Khi5V", pForeignIntegrationPointId); 
}


/**
 * Getter-Method for attribute: foreignIntegrationPoint
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_FOREIGN_INTEGRPOINT
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nucletIntegrationPoint
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.NucletIntegrationPoint getForeignIntegrationPointBO() {
		return getReferencedBO(org.nuclos.businessentity.NucletIntegrationPoint.class, getFieldUid("Khi5V"), "Khi5V", "kIL5");
}


/**
 * Getter-Method for attribute: comment
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCOMMENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 350
 *<br>Precision: null
**/
public java.lang.String getComment() {
		return getField("Khi5U", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: comment
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCOMMENT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 350
 *<br>Precision: null
**/
public void setComment(java.lang.String pComment) {
		setField("Khi5U", pComment); 
}


/**
 * Getter-Method for attribute: sortationasc
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSORTATIONASC
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public java.lang.String getSortationasc() {
		return getField("Khi5z", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: sortationasc
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSORTATIONASC
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setSortationasc(java.lang.String pSortationasc) {
		setField("Khi5z", pSortationasc); 
}


/**
 * Getter-Method for attribute: calcfunction
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCFUNCTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCalcfunction() {
		return getField("Khi5y", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: calcfunction
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCALCFUNCTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setCalcfunction(java.lang.String pCalcfunction) {
		setField("Khi5y", pCalcfunction); 
}


/**
 * Getter-Method for attribute: lookupentityfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRLOOKUPENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public java.lang.String getLookupentityfield() {
		return getField("Khi5j", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: lookupentityfield
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRLOOKUPENTITYFIELD
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 1028
 *<br>Precision: null
**/
public void setLookupentityfield(java.lang.String pLookupentityfield) {
		setField("Khi5j", pLookupentityfield); 
}


/**
 * Getter-Method for attribute: lookupentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_LOOKUPENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getLookupentityId() {
		return getFieldUid("Khi5i");
}


/**
 * Setter-Method for attribute: lookupentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_LOOKUPENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLookupentityId(org.nuclos.common.UID pLookupentityId) {
		setFieldId("Khi5i", pLookupentityId); 
}


/**
 * Getter-Method for attribute: lookupentity
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRUID_LOOKUPENTITY
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Entity getLookupentityBO() {
		return getReferencedBO(org.nuclos.businessentity.Entity.class, getFieldUid("Khi5i"), "Khi5i", "5E8q");
}


/**
 * Getter-Method for attribute: dataprecision
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTDATAPRECISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public java.lang.Integer getDataprecision() {
		return getField("Khi5l", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: dataprecision
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTDATAPRECISION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setDataprecision(java.lang.Integer pDataprecision) {
		setField("Khi5l", pDataprecision); 
}


/**
 * Getter-Method for attribute: datascale
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTDATASCALE
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public java.lang.Integer getDatascale() {
		return getField("Khi5k", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: datascale
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTDATASCALE
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setDatascale(java.lang.Integer pDatascale) {
		setField("Khi5k", pDatascale); 
}


/**
 * Getter-Method for attribute: valuedefault
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRVALUE_DEFAULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getValuedefault() {
		return getField("Khi5n", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: valuedefault
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRVALUE_DEFAULT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setValuedefault(java.lang.String pValuedefault) {
		setField("Khi5n", pValuedefault); 
}


/**
 * Getter-Method for attribute: foreigndefault
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTID_FOREIGN_DEFAULT
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public java.lang.Long getForeigndefault() {
		return getField("Khi5m", java.lang.Long.class); 
}


/**
 * Setter-Method for attribute: foreigndefault
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTID_FOREIGN_DEFAULT
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public void setForeigndefault(java.lang.Long pForeigndefault) {
		setField("Khi5m", pForeigndefault); 
}


/**
 * Getter-Method for attribute: formatoutput
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFORMATOUTPUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getFormatoutput() {
		return getField("Khi5p", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: formatoutput
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFORMATOUTPUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setFormatoutput(java.lang.String pFormatoutput) {
		setField("Khi5p", pFormatoutput); 
}


/**
 * Getter-Method for attribute: formatinput
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFORMATINPUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getFormatinput() {
		return getField("Khi5o", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: formatinput
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRFORMATINPUT
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setFormatinput(java.lang.String pFormatinput) {
		setField("Khi5o", pFormatinput); 
}


/**
 * Getter-Method for attribute: nullable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNNULLABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getNullable() {
		return getField("Khi5r", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: nullable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNNULLABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNullable(java.lang.Boolean pNullable) {
		setField("Khi5r", pNullable); 
}


/**
 * Getter-Method for attribute: unique
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNUNIQUE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getUnique() {
		return getField("Khi5q", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: unique
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNUNIQUE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setUnique(java.lang.Boolean pUnique) {
		setField("Khi5q", pUnique); 
}


/**
 * Getter-Method for attribute: modifiable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNMODIFIABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getModifiable() {
		return getField("Khi5t", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: modifiable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNMODIFIABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setModifiable(java.lang.Boolean pModifiable) {
		setField("Khi5t", pModifiable); 
}


/**
 * Getter-Method for attribute: searchable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNSEARCHABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getSearchable() {
		return getField("Khi5s", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: searchable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNSEARCHABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setSearchable(java.lang.Boolean pSearchable) {
		setField("Khi5s", pSearchable); 
}


/**
 * Getter-Method for attribute: logbooktracking
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNLOGBOOKTRACKING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getLogbooktracking() {
		return getField("Khi5v", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: logbooktracking
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNLOGBOOKTRACKING
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setLogbooktracking(java.lang.Boolean pLogbooktracking) {
		setField("Khi5v", pLogbooktracking); 
}


/**
 * Getter-Method for attribute: insertable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNINSERTABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getInsertable() {
		return getField("Khi5u", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: insertable
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNINSERTABLE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setInsertable(java.lang.Boolean pInsertable) {
		setField("Khi5u", pInsertable); 
}


/**
 * Getter-Method for attribute: readonly
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getReadonly() {
		return getField("Khi5x", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: readonly
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNREADONLY
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setReadonly(java.lang.Boolean pReadonly) {
		setField("Khi5x", pReadonly); 
}


/**
 * Getter-Method for attribute: showmnemonic
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNSHOWMNEMONIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getShowmnemonic() {
		return getField("Khi5w", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: showmnemonic
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNSHOWMNEMONIC
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setShowmnemonic(java.lang.Boolean pShowmnemonic) {
		setField("Khi5w", pShowmnemonic); 
}


/**
 * Getter-Method for attribute: localeresourcel
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLocaleresourcel() {
		return getField("Khi5B", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: localeresourcel
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_LOCALERESOURCE_L
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLocaleresourcel(java.lang.String pLocaleresourcel) {
		setField("Khi5B", pLocaleresourcel); 
}


/**
 * Getter-Method for attribute: sortationdesc
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSORTATIONDESC
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public java.lang.String getSortationdesc() {
		return getField("Khi5A", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: sortationdesc
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRSORTATIONDESC
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setSortationdesc(java.lang.String pSortationdesc) {
		setField("Khi5A", pSortationdesc); 
}


/**
 * Getter-Method for attribute: permissiontransfer
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNPERMISSIONTRANSFER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getPermissiontransfer() {
		return getField("Khi5D", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: permissiontransfer
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNPERMISSIONTRANSFER
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setPermissiontransfer(java.lang.Boolean pPermissiontransfer) {
		setField("Khi5D", pPermissiontransfer); 
}


/**
 * Getter-Method for attribute: localeresourced
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getLocaleresourced() {
		return getField("Khi5C", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: localeresourced
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_LOCALERESOURCE_D
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setLocaleresourced(java.lang.String pLocaleresourced) {
		setField("Khi5C", pLocaleresourced); 
}


/**
 * Getter-Method for attribute: defaultmandatory
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_DEFAULT_MANDATORY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public java.lang.String getDefaultmandatory() {
		return getField("Khi5F", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: defaultmandatory
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STR_DEFAULT_MANDATORY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setDefaultmandatory(java.lang.String pDefaultmandatory) {
		setField("Khi5F", pDefaultmandatory); 
}


/**
 * Getter-Method for attribute: indexed
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNINDEXED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getIndexed() {
		return getField("Khi5E", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: indexed
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNINDEXED
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setIndexed(java.lang.Boolean pIndexed) {
		setField("Khi5E", pIndexed); 
}


/**
 * Getter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public java.lang.Integer getOrder() {
		return getField("Khi5H", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: order
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: INTORDER
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setOrder(java.lang.Integer pOrder) {
		setField("Khi5H", pOrder); 
}


/**
 * Getter-Method for attribute: ondeletecascade
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNONDELETECASCADE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getOndeletecascade() {
		return getField("Khi5G", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: ondeletecascade
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: BLNONDELETECASCADE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setOndeletecascade(java.lang.Boolean pOndeletecascade) {
		setField("Khi5G", pOndeletecascade); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("Khi52", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("Khi51", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("Khi54", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_entityfield
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("Khi53", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: targetField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_TARGET_FIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entityfield
 *<br>Reference field: field
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationField> getNucletIntegrationField(Flag... flags) {
		return getDependents(_NucletIntegrationField, flags); 
}


/**
 * Insert-Method for attribute: targetField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_TARGET_FIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entityfield
 *<br>Reference field: field
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationField(org.nuclos.businessentity.NucletIntegrationField pNucletIntegrationField) {
		insertDependent(_NucletIntegrationField, pNucletIntegrationField);
}


/**
 * Delete-Method for attribute: targetField
 *<br>
 *<br>Entity: nuclos_nucletIntegrationField
 *<br>DB-Name: STRUID_TARGET_FIELD
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_entityfield
 *<br>Reference field: field
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationField(org.nuclos.businessentity.NucletIntegrationField pNucletIntegrationField) {
		deleteDependent(_NucletIntegrationField, pNucletIntegrationField);
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(EntityField boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public EntityField copy() {
		return super.copy(EntityField.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(org.nuclos.common.UID id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("Khi5"), id);
}
/**
* Static Get by Id
*/
public static EntityField get(org.nuclos.common.UID id) {
		return get(EntityField.class, id);
}
 }
