//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 

/**
 * BusinessObject: nuclos_nucletImport
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_MD_NUCLETIMPORT
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class NucletImport extends AbstractBusinessObject<java.lang.Long> implements Modifiable<java.lang.Long> {


/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 20
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<java.lang.Long> Id = 
	new PrimaryKeyAttribute<>("Id", "org.nuclos.businessentity", "mVjz", "mVjz0", java.lang.Long.class);


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<>("CreatedAt", "org.nuclos.businessentity", "mVjz", "mVjz1", java.util.Date.class);


/**
 * Attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<org.nuclos.common.UID> NucletId = 
	new ForeignKeyAttribute<>("NucletId", "org.nuclos.businessentity", "mVjz", "mVjza", org.nuclos.common.UID.class);


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<>("CreatedBy", "org.nuclos.businessentity", "mVjz", "mVjz2", java.lang.String.class);


/**
 * Attribute: nucletImportFile
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID_T_MD_NUCLETIMPORTFILE
 *<br>Data type: java.lang.Long
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final ForeignKeyAttribute<java.lang.Long> NucletImportFileId = 
	new ForeignKeyAttribute<>("NucletImportFileId", "org.nuclos.businessentity", "mVjz", "mVjzb", java.lang.Long.class);


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<>("ChangedAt", "org.nuclos.businessentity", "mVjz", "mVjz3", java.util.Date.class);


/**
 * Attribute: importDate
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: DATIMPORT
 *<br>Data type: org.nuclos.common2.DateTime
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<org.nuclos.common2.DateTime> ImportDate = 
	new Attribute<>("ImportDate", "org.nuclos.businessentity", "mVjz", "mVjzc", org.nuclos.common2.DateTime.class);


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<>("ChangedBy", "org.nuclos.businessentity", "mVjz", "mVjz4", java.lang.String.class);


/**
 * Attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> NucletVersion = 
	new NumericAttribute<>("NucletVersion", "org.nuclos.businessentity", "mVjz", "mVjzd", java.lang.Integer.class);


public NucletImport() {
		super("mVjz");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("mVjz");
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("mVjz1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNuclet() {
		return getField("mVjza", org.nuclos.common.UID.class); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common.UID getNucletId() {
		return getFieldUid("mVjza");
}


/**
 * Setter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletId(org.nuclos.common.UID pNucletId) {
		setFieldId("mVjza", pNucletId); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.Nuclet getNucletBO() {
		return getReferencedBO(org.nuclos.businessentity.Nuclet.class, getFieldUid("mVjza"), "mVjza", "xojr");
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("mVjz2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nucletImportFile
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID_T_MD_NUCLETIMPORTFILE
 *<br>Data type: java.lang.Long
 *<br>Reference entity: nuclos_nucletImportFile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Long getNucletImportFile() {
		return getField("mVjzb", java.lang.Long.class); 
}


/**
 * Getter-Method for attribute: nucletImportFile
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID_T_MD_NUCLETIMPORTFILE
 *<br>Data type: java.lang.Long
 *<br>Reference entity: nuclos_nucletImportFile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Long getNucletImportFileId() {
		return getFieldId("mVjzb");
}


/**
 * Setter-Method for attribute: nucletImportFile
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID_T_MD_NUCLETIMPORTFILE
 *<br>Data type: java.lang.Long
 *<br>Reference entity: nuclos_nucletImportFile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNucletImportFileId(java.lang.Long pNucletImportFileId) {
		setFieldId("mVjzb", pNucletImportFileId); 
}


/**
 * Getter-Method for attribute: nucletImportFile
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTID_T_MD_NUCLETIMPORTFILE
 *<br>Data type: java.lang.Long
 *<br>Reference entity: nuclos_nucletImportFile
 *<br>Reference field: filename
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.businessentity.NucletImportFile getNucletImportFileBO() {
		return getReferencedBO(org.nuclos.businessentity.NucletImportFile.class, getFieldId("mVjzb"), "mVjzb", "KUOj");
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("mVjz3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: importDate
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: DATIMPORT
 *<br>Data type: org.nuclos.common2.DateTime
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public org.nuclos.common2.DateTime getImportDate() {
		return getField("mVjzc", org.nuclos.common2.DateTime.class); 
}


/**
 * Setter-Method for attribute: importDate
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: DATIMPORT
 *<br>Data type: org.nuclos.common2.DateTime
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setImportDate(org.nuclos.common2.DateTime pImportDate) {
		setField("mVjzc", pImportDate); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("mVjz4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public java.lang.Integer getNucletVersion() {
		return getField("mVjzd", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nucletImport
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setNucletVersion(java.lang.Integer pNucletVersion) {
		setField("mVjzd", pNucletVersion); 
}
/**
 * This method compares the current BusinessObject with an other BusinessObject.
 * If all fields, all references (excluding system fields) and all dependents are equal, the method returns true
**/
public boolean compare(NucletImport boToCompareWith) {
		return super.compare(boToCompareWith);
}
/**
 * This method creates a copy of the current BusinessObject
 * and resets it by removing the primary key and setting the state flag to 'new'
**/
public NucletImport copy() {
		return super.copy(NucletImport.class);
}
/**
* Save this BO. Use this instead of BusinessObjectProvider
*/
public void save() throws org.nuclos.api.exception.BusinessException {
		super.save();
}
/**
* Delete this BO. Use this instead of BusinessObjectProvider
*/
public void delete() throws org.nuclos.api.exception.BusinessException {
		super.delete();
}
/**
* Static Delete for an Id
*/
public static void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException {
		delete(new org.nuclos.common.UID("mVjz"), id);
}
/**
* Static Get by Id
*/
public static NucletImport get(java.lang.Long id) {
		return get(NucletImport.class, id);
}
 }
