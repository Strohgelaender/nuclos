//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;

import org.nuclos.businessentity.*; 
import org.nuclos.api.UID; 
import java.util.List; 

/**
 * BusinessObject: nuclos_webAddonLog
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: [proxy]
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public interface WebAddonLogProxy {

public void setUser(org.nuclos.api.User user);
public List<WebAddonLog> getAll();
public List<java.lang.Long> getAllIds();
public WebAddonLog getById(java.lang.Long id);

/**
 * Getter-Method for attribute: webAddon
 *<br>
 *<br>Entity: nuclos_webAddonLog
 *<br>DB-Name: STRUID_WEBADDON
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_webAddon
 *<br>Reference field: name
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<WebAddonLog> getByWebAddon(org.nuclos.common.UID pNucloswebAddonId);
public void insert(WebAddonLog pWebAddonLog) throws org.nuclos.api.exception.BusinessException;
public void update(WebAddonLog pWebAddonLog) throws org.nuclos.api.exception.BusinessException;
public void delete(java.lang.Long id) throws org.nuclos.api.exception.BusinessException;
public void commit();
public void rollback(); }
