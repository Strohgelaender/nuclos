//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectRelationVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.RelationDirection;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

// @Local
public interface GenericObjectFacadeLocal {

	/**
	 * §postcondition result != null
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param iGenericObjectId
	 * @return the generic object with the given id
	 * @throws CommonFinderException if there is no object with the given id.
	 * @throws CommonPermissionException if the user doesn't have the permission to view the generic object with the given id.
	 */
	@RolesAllowed("Login")
	GenericObjectVO get(UID entity, Long iGenericObjectId)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * @param govo
	 * @param mpDependants
	 * @param bAll
	 * @return reload the dependant data of the genericobject, if bAll is false, only the direct
	 *         dependants (highest hierarchie of subforms) will be reloaded
	 * @throws CommonFinderException if no such object was found.
	 */
	@RolesAllowed("Login")
	IDependentDataMap reloadDependants(
		GenericObjectVO govo, IDependentDataMap mpDependants, boolean bAll, String customUsage)
		throws CommonFinderException;


	/**
	 * gets all generic objects that match a given search condition
	 * 
	 * §postcondition result != null
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr
	 * @param stRequiredSubEntityNames
	 * @return list of generic object value objects with specified dependants and without parent objects!
	 * 
	 * @deprecated use with customUsage
	 */
	@Deprecated
	List<GenericObjectWithDependantsVO> getGenericObjects(
		UID iModuleId, CollectableSearchExpression clctexpr,
		Set<UID> stRequiredSubEntityNames);
	

	/**
	 * gets all generic objects along with its dependants, that match a given search condition, but
	 * clears the values of all attributes on which the current user has no read permission
	 * NOTE: use only within report mechanism
	 * 
	 * §precondition stRequiredSubEntityNames != null
	 * §todo rename to getGenericObjectProxyList?
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr value object containing search expression
	 * @param stRequiredAttributeIds may be <code>null</code>, which means all attributes are required
	 * @param stRequiredSubEntityNames
	 * @return list of generic object value objects
	 * 
	 * @deprecated use with customUsage
	 */

	@RolesAllowed("Login")
	Collection<GenericObjectWithDependantsVO> getPrintableGenericObjectsWithDependantsNoCheck(
		UID iModuleId, CollectableSearchExpression clctexpr,
		Set<UID> stRequiredAttributeIds,
		Set<UID> stRequiredSubEntityNames, String customUsage);

	/**
	 * gets all generic objects that match a given search condition
	 * 
	 * §precondition stRequiredSubEntityNames != null
	 * §precondition iMaxRowCount &gt; 0
	 * §postcondition result.size() &lt;= iMaxRowCount
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr
	 * @param stRequiredSubEntityNames
	 * @param iMaxRowCount the maximum number of rows
	 * @return list of generic object value objects
	 * 
	 */

	@RolesAllowed("Login")
	TruncatableCollection<GenericObjectWithDependantsVO> getRestrictedNumberOfGenericObjectsNoCheck(
		UID iModuleId, CollectableSearchExpression clctexpr,
		Set<UID> stRequiredSubEntityNames, String customUsage,
		Long iMaxRowCount);


	/**
	 * gets the ids of all generic objects that match a given search condition
	 * @param iModuleId id of module to search for generic objects in
	 * @param cond condition that the generic objects to be found must satisfy
	 * @return List&lt;Integer&gt; list of generic object ids
	 */
	List<Long> getGenericObjectIdsNoCheck(UID iModuleId, CollectableSearchExpression cse);

	/**
	 * creates a new generic object, along with its dependants.
	 * 
	 * §precondition gowdvo.getId() == null
	 * §precondition Modules.getInstance().isSubModule(gowdvo.getModuleId()) --&gt; gowdvo.getParentId() != null
	 * §precondition (gowdvo.getDependants() != null) --&gt; gowdvo.getDependants().areAllDependantsNew()
	 *
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param gowdvo the generic object, along with its dependants.
	 * @return the created object witout dependants.
	 * @throws CommonPermissionException if the current user doesn't have the right to create such an object.
	 * @throws NuclosBusinessRuleException if the object could not be created because a business rule failed.
	 * @throws CommonCreateException if the object could not be created for other reasons, eg. because of a database constraint violation.
	 *
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	GenericObjectVO create(GenericObjectWithDependantsVO gowdvo)
		throws CommonPermissionException, NuclosBusinessRuleException,
		CommonCreateException;
	
	@RolesAllowed("Login")
	GenericObjectVO create(GenericObjectWithDependantsVO gowdvo, String customUsage)
			throws CommonPermissionException, NuclosBusinessRuleException,
			CommonCreateException;

	/**
	 * updates an existing generic object in the database and returns the written object along with its dependants.
	 * 
	 * §precondition iModuleId != null
	 * §precondition lowdcvo.getModuleId() == iModuleId
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param lowdcvo containing the generic object data
	 * @return same generic object as value object
	 * 
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO lowdcvo) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException;
	
	/**
	 * updates an existing generic object in the database and returns the written object along with its dependants.
	 * 
	 * §precondition iModuleId != null
	 * §precondition lowdcvo.getModuleId() == iModuleId
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param lowdcvo containing the generic object data
	 * @return same generic object as value object
	 * 
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO lowdcvo, boolean isCollectiveProcessing) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException;
	
	@RolesAllowed("Login")
	GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO lowdcvo, String customUsage) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException;
	
	@RolesAllowed("Login")
	GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO lowdcvo, String customUsage, boolean isCollectiveProcessing) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException;

	/*/**
	 * modifies an existing generic object.
	 * 
	 * §nucleus.permission mayWrite(module)
	 * §todo change signature into GenericObjectVO modify(GenericObjectWithDependantsVO lowdcvo, boolean bFireSaveEvent)
	 * 
	 * @param govo containing the generic object data
	 * @param bFireSaveEvent
	 * @return same generic object as value object
	 * @deprecated use with customUsage
	 *
	@RolesAllowed("Login")
	@Deprecated 
	GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent)
		throws CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException,
		CommonCreateException, CommonFinderException, CommonRemoveException;*/
	
	/**
	 * modifies an existing generic object.
	 * 
	 * §nucleus.permission mayWrite(module)
	 * §todo change signature into GenericObjectVO modify(GenericObjectWithDependantsVO lowdcvo, boolean bFireSaveEvent)
	 * 
	 * @param govo containing the generic object data
	 * @param bFireSaveEvent
	 * @return same generic object as value object
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated 
	GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent, boolean isCollectiveProcessing)
		throws CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException,
		CommonCreateException, CommonFinderException, CommonRemoveException;
	
	@RolesAllowed("Login")
	GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent, String customUsage)
		throws CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException,
		CommonCreateException, CommonFinderException, CommonRemoveException;
	
	@RolesAllowed("Login")
	GenericObjectWithDependantsVO modify(GenericObjectWithDependantsVO govo, boolean bFireSaveEvent, String customUsage, boolean isCollectiveProcessing)
		throws CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException,
		CommonCreateException, CommonFinderException, CommonRemoveException;

	/**
	 * delete generic object from database
	 * 
	 * §nucleus.permission mayDelete(module, bDeletePhysically)
	 * 
	 * @param gowdvo containing the generic object data
	 * @param bDeletePhysically remove from db?
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	void remove(UID entity, Long id,
		boolean bDeletePhysically) throws NuclosBusinessException,
		CommonFinderException,
		CommonRemoveException, CommonPermissionException,
		CommonStaleVersionException,
		CommonCreateException;
	
	@RolesAllowed("Login")
	void remove(UID entity, Long id,
		boolean bDeletePhysically, String customUsage) throws NuclosBusinessException,
		CommonFinderException,
		CommonRemoveException, CommonPermissionException,
		CommonStaleVersionException,
		CommonCreateException;

	/**
	 * relates a generic object to another generic object.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget.intValue())
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iModuleIdTarget module id of target generic object to be related
	 * @param iGenericObjectIdTarget id of target generic object to be related
	 * @param iGenericObjectIdSource id of source generic object to be related to
	 * @param relationType relation type
	 */
	@RolesAllowed("Login")
	void relate(UID iModuleIdTarget,
		Long iGenericObjectIdTarget, Long iGenericObjectIdSource,
		String relationType) throws CommonFinderException, CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * relates a generic object to another generic object.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget.intValue())
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iModuleIdTarget module id of target generic object to be related (i.e. of request)
	 * @param iGenericObjectIdTarget id of target generic object to be related (i.e. request)
	 * @param iGenericObjectIdSource id of source generic object to be related to (i.e. demand)
	 * @param relationType relation type
	 * @param dateValidFrom Must be <code>null</code> for system defined relation types.
	 * @param dateValidUntil Must be <code>null</code> for system defined relation types.
	 * @param sDescription Must be <code>null</code> for system defined relation types.
	 */
	@RolesAllowed("Login")
	void relate(UID iModuleIdTarget,
		Long iGenericObjectIdTarget, Long iGenericObjectIdSource,
		String relationType, Date dateValidFrom, Date dateValidUntil,
		String sDescription) throws CommonFinderException,
		CommonCreateException, CommonPermissionException,
		NuclosBusinessRuleException;

	/**
	 * unrelates a generic object from another generic object. All relations between these objects with the given
	 * relation type are removed.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget.intValue())
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iModuleIdTarget module id of target generic object to be unrelated (i.e. request)
	 * @param iGenericObjectIdTarget id of target generic object to be unrelated (i.e. request)
	 * @param iGenericObjectIdSource id of source generic object to be unrelated (i.e. demand)
	 * @param relationType
	 */
	void unrelate(UID iModuleIdTarget,
		Long iGenericObjectIdTarget, Long iGenericObjectIdSource,
		String relationType) throws CommonFinderException,
		NuclosBusinessRuleException, CommonPermissionException,
		CommonRemoveException;

	/**
	 * finds relations between two given generic objects.
	 * 
	 * §postcondition result != null
	 * 
	 * @param iGenericObjectIdSource
	 * @param relationType
	 * @param iGenericObjectIdTarget
	 * @return the relation.
	 */
	Collection<GenericObjectRelationVO> findRelations(
		Long iGenericObjectIdSource, String relationType,
		Long iGenericObjectIdTarget) throws CommonFinderException,
		CommonPermissionException;

	Collection<GenericObjectRelationVO> findRelationsByGenericObjectId(
		Long iGenericObjectId) throws CommonFinderException,
		CommonPermissionException;

	/**
	 * checks if a given generic object belongs to a given module.
	 * @param iModuleId			 id of module to validate generic object for
	 * @param iGenericObjectId id of generic object to check
	 * @return true if in module or false if not
	 */
	boolean isGenericObjectInModule(UID iModuleId,
		Long iGenericObjectId) throws CommonFinderException;

	/**
	 * @param genericObjectId
	 * @return the id of the module containing the generic object with the given id.
	 * @throws CommonFinderException if there is no generic object with the given id.
	 */
	@RolesAllowed("Login")
	UID getModuleContainingGenericObject(Long genericObjectId)
		throws CommonFinderException;


	Collection<GenericObjectWithDependantsVO> getGenericObjectsMoreNoCheck(UID iModuleId, List<Long> lstIds,
			Set<UID> stRequiredSubEntityNames, String customUsage);


	/**
	 * @deprecated Does not belong into this facade.
	 */
	String getCurrentUserName();
	
	/**
	 * Inserts or remove generic object data based on wether the entity is currently marked as a module.
	 *
	 * @param entityUID
	 * @throws NuclosBusinessRuleException
	 */
	@RolesAllowed("Login")
	void updateGenericObjectEntries(UID entityUID) throws NuclosBusinessException;

	/**
	 * Adds generic object data for the given entity.
	 *
	 * @param entityUID
	 * @throws NuclosBusinessRuleException
	 */
	@RolesAllowed("Login")
	void addGenericObjectEntries(UID entityUID) throws NuclosBusinessException;

	/**
	 * Deletes generic object data for the given entity.
	 *
	 * @param entityUID uid of the entity
	 * @throws NuclosBusinessRuleException
	 */
	@RolesAllowed("Login")
	void removeGenericObjectEntries(UID entityUID) throws NuclosBusinessException;

	@RolesAllowed("Login")
	public UsageCriteria getGOMeta(Long id, UID entity, String customUsage);
	
	@RolesAllowed("Login")
	void attachDocumentToObject(MasterDataVO<Long> mdvoDocument);
	
	@RolesAllowed("Login")
	void attachDocumentToObject(MasterDataVO<Long> mdvoDocument, UID parentAttribute);
	
	public GenericObjectVO create(GenericObjectWithDependantsVO gowdvo, String customUsage, boolean checkUser) 
			throws CommonPermissionException, NuclosBusinessRuleException, CommonCreateException;
}
