//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.AttributeProvider;
import org.nuclos.common.E;
import org.nuclos.common.NuclosAttributeNotFoundException;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.RecordGrantUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Helper class for the GenericObjectFacade
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
@Component
public class GenericObjectFacadeHelper {

	private static final Logger LOG = LoggerFactory.getLogger(GenericObjectFacadeHelper.class);

	/** 
	 * §todo tune the DEFAULT_PAGESIZE (300 seems to be much better than 1000) 
	 */
	public static final int DEFAULT_PAGESIZE = 300;
	
	//

	private MasterDataFacadeLocal mdFacade;

	private GenericObjectFacadeLocal goFacade;

	private RecordGrantUtils grantUtils;
	
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private MandatorUtils mandatorUtils;
	
	/**
	 * @deprecated
	 */
	private MasterDataFacadeHelper masterDataFacadeHelper;

	public GenericObjectFacadeHelper() {
	}
	
	@Autowired
	void setRecordGrantUtils(RecordGrantUtils grantUtils) {
		this.grantUtils = grantUtils;
	}
	
	@Autowired
	void setMasterDataFacadeHelper(MasterDataFacadeHelper masterDataFacadeHelper) {
		this.masterDataFacadeHelper = masterDataFacadeHelper;
	}
	
	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	private GenericObjectFacadeLocal getGenericObjectFacade() {
		if (goFacade == null)
			goFacade = ServerServiceLocator.getInstance().getFacade(GenericObjectFacadeLocal.class);

		return goFacade;
	}

	private MasterDataFacadeLocal getMasterDataFacade() {
		if (mdFacade == null)
			mdFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
		return mdFacade;
	}

	/**
	 * efficiently creates an ArrayList by adding each element of the given Iterator to it.
	 * 
	 * §postcondition result != null
	 * 
	 * @param ksiter
	 * @return ArrayList&lt;E&gt;
	 */
	public static <E> ArrayList<E> newArrayList(KnownSizeIterator<? extends E> ksiter) {
		final ArrayList<E> result = new ArrayList<E>(ksiter.size());
		CollectionUtils.addAll(result, ksiter);
		return result;
	}

	public static <PK> Map<UID, DynamicAttributeVO> getHistoricalAttributes(GenericObjectVO govo,
			Date dateHistorical, final IDependentDataMap mpDependantsResult) {

		final Map<UID, DynamicAttributeVO> result = CollectionUtils.newHashMap();
		for (DynamicAttributeVO attrvo : govo.getAttributes()) {
			result.put(attrvo.getAttributeUID(), attrvo);
		}

		DbQueryBuilder builder = SpringDataBaseHelper.getInstance().getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<Long> t = query.from(E.GENERICOBJECTLOGBOOK);
		query.multiselect(
			t.baseColumn(E.GENERICOBJECTLOGBOOK.attributeId), // 0
			t.baseColumn(E.GENERICOBJECTLOGBOOK.oldattributevalue), // 1
			t.baseColumn(E.GENERICOBJECTLOGBOOK.oldexternalvalue),   // 2
			t.baseColumn(E.GENERICOBJECTLOGBOOK.oldvalue),          // 3
			t.baseColumn(E.GENERICOBJECTLOGBOOK.externalid),     // 4
			t.baseColumn(E.GENERICOBJECTLOGBOOK.action),          // 5
			t.baseColumn(E.GENERICOBJECTLOGBOOK.masterdataId), // 6
			t.baseColumn(E.GENERICOBJECTLOGBOOK.entityfieldId));  // 7
		query.where(builder.and(
			builder.equalValue(t.baseColumn(E.GENERICOBJECTLOGBOOK.genericObject), govo.getId()),
			builder.greaterThanOrEqualTo(t.baseColumn(SF.CREATEDAT), builder.literal(InternalTimestamp.toInternalTimestamp(dateHistorical)))));
		query.orderBy(builder.desc(t.baseColumn(SF.CREATEDAT)));
		
		for (DbTuple tuple : SpringDataBaseHelper.getInstance().getDbAccess().executeQuery(query)) {
			try {
				final UID iAttributeId = tuple.get(0, UID.class);
				// If the value of an existing attribute (not generated by a migration) is given...				
				if (iAttributeId != null) {
					try {
						AttributeCache.getInstance().getAttribute(iAttributeId);
					}
					catch(NuclosAttributeNotFoundException e) {
						// Attribute was deleted
						LOG.debug("getHistoricalAttributes: {}", e);
						continue;
					}
					final Long iValueId = (tuple.get(1) != null) ? tuple.get(1, Long.class) : tuple.get(2, Long.class);
					final String sValue = tuple.get(3, String.class);
					DynamicAttributeVO loavo = result.get(iAttributeId);
					if (loavo != null) {
						loavo.setValueId(iValueId);
						loavo.setCanonicalValue(sValue, AttributeCache.getInstance());
					}
					else {
						loavo = DynamicAttributeVO.createGenericObjectAttributeVOCanonical(iAttributeId, iValueId, null, sValue, AttributeCache.getInstance());
					}
					result.put(iAttributeId, loavo);
				}
				else {
					// If a change in a subform entry is given...
					final Long iMasterDataRecordId = tuple.get(4, Long.class);
					if (iMasterDataRecordId != null) {
						final String sAction = tuple.get(5, String.class);
						final UID iMasterDataMetaId = tuple.get(6, UID.class);
						final UID iMasterDataMetaFieldId = tuple.get(7, UID.class);
						//final Integer iRecordId = Helper.getInteger(rs, "intid_md_external");
						
						// get referencing field from layout...
						UsageCriteria usage = govo.getUsageCriteria(null);
						IDependentKey dependentKey = SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class).getDependentKeyBetween(usage, iMasterDataMetaId);
						if (dependentKey == null) {
							// may be subform not in layout any more?
							// or is sub-subform (no support in getDependentKeyBetween)?
							continue;
						}

						//@see RSWORGA-59
						if (sAction == null || iMasterDataMetaId == null || iMasterDataMetaFieldId == null)
							continue;
						
//						final EntityMeta mdmetavo = MetaProvider.getInstance().getEntity(iMasterDataMetaId);
//						final FieldMeta mdmetafieldvo = MetaProvider.getInstance().getEntityField(iMasterDataMetaFieldId);
						final String sValue = tuple.get(3, String.class);

						final Collection<EntityObjectVO<PK>> collmdvo = mpDependantsResult.<PK>getDataPk(dependentKey);

						// Find the masterdata cvo in the dependencies if possible
						EntityObjectVO mdvo = null;
						for (EntityObjectVO mdvo1 : collmdvo) {
							if (iMasterDataRecordId.equals(mdvo1.getId())) {
								mdvo = mdvo1;
								break;
							}
						}

						if ("D".equals(sAction)) {
							// Entry has been deleted and must be recreated before value can be set
							if (mdvo == null) {
								mdvo = new EntityObjectVO<Long>(iMasterDataMetaId);
								mdvo.setPrimaryKey(iMasterDataRecordId);
								mpDependantsResult.addData(dependentKey, mdvo);
							}
							mdvo.setFieldValue(iMasterDataMetaFieldId, sValue);
						}
						else if ("C".equals(sAction)) {
							// Entry has been newly created and has simply to be deleted
							if (mdvo != null) {
								mpDependantsResult.removeKey(dependentKey);
								collmdvo.remove(mdvo);
								mpDependantsResult.addAllData(dependentKey, collmdvo);
							}
						}
						else if ("M".equals(sAction)) {
							// Existing entry has just been modified
							if (mdvo != null) {	//ought to be always so with this action
								mdvo.setFieldValue(iMasterDataMetaFieldId, sValue);
							}
						}
					}
					// else it must be a migrated value and can be ignored here
				}
			} catch (CommonValidationException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		
		return result;
	}

	public void createDependants(UID entityname, Long iGenericObjectId, IDependentDataMap mpDependants, Map<EntityAndField, UID> collSubEntities) throws CommonCreateException {
		this.createDependants(entityname, iGenericObjectId, mpDependants, collSubEntities, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
	}
	
	public void createDependants(UID entityname, Long iGenericObjectId, IDependentDataMap mpDependants, 
			Map<EntityAndField, UID> collSubEntities, String customUsage) throws CommonCreateException {
		
		if (!mpDependants.areAllDependentsNew()) {
			throw new IllegalArgumentException("Dependants must be new (must have empty ids).");
		}
//		mpDependants.setParent(entityname, iGenericObjectId, collSubEntities);
		for (EntityAndField subEntity : collSubEntities.keySet()) {
			for (EntityObjectVO<?> depVO : mpDependants.getDataPk(subEntity.getDependentKey())) {
				depVO.setFieldId(subEntity.getField(), iGenericObjectId);
			}
		}

		try {
			// @todo: genericObjectId makes no sense as an entityName
			this.getMasterDataFacade().createDependants(iGenericObjectId, mpDependants, customUsage);
		}
		catch (CommonPermissionException ex) {
			// This must never happen when inserting a new object:
			throw new CommonFatalException(ex);
		}
	}

	private void storeSystemAttributes(GenericObjectVO govo, AttributeProvider attrprovider, boolean bLogbookTracking) {
		try {
			storeAttributeValue(govo, attrprovider.getAttribute(SF.CREATEDAT.getUID(govo.getModule())), govo.getCreatedAt(), bLogbookTracking);
			storeAttributeValue(govo, attrprovider.getAttribute(SF.CREATEDBY.getUID(govo.getModule())), govo.getCreatedBy(), bLogbookTracking);
			storeAttributeValue(govo, attrprovider.getAttribute(SF.CHANGEDAT.getUID(govo.getModule())), govo.getChangedAt(), bLogbookTracking);
			storeAttributeValue(govo, attrprovider.getAttribute(SF.CHANGEDBY.getUID(govo.getModule())), govo.getChangedBy(), bLogbookTracking);
		}
		catch (CommonValidationException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (NuclosBusinessException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/**
	 * §precondition attrcvo != null
	 * §precondition oValue != null
	 */
	private void storeAttributeValue(GenericObjectVO govo, AttributeCVO attrcvo, Object oValue, boolean bLogbookTracking) throws CommonValidationException, NuclosBusinessException {
		if (oValue == null) {
			throw new NullArgumentException("oValue");
		}
		final DynamicAttributeVO attrvo = new DynamicAttributeVO(attrcvo.getId(), null, null, oValue);
	}
	
	public void removeDependantComments(Long govoId) {
		DbStatement stmt = DbStatementUtils.deleteFrom(E.GENERALSEARCHCOMMENT,
				E.GENERALSEARCHCOMMENT.getPk(), govoId);
		dataBaseHelper.getDbAccess().execute(stmt);
	}
	public void removeDependantComments(UID entityUID) {
		final String sDBEntity = MetaProvider.getInstance().getEntity(entityUID).getDbTable();
		final String sql = "DELETE FROM T_UD_GO_COMMENT WHERE INTID_T_UD_GENERICOBJECT IN (SELECT INTID FROM " + sDBEntity + ")"; 
		dataBaseHelper.getDbAccess().executePlainUpdate(sql);
	}
	
	public void removeDependantDocuments(Long govoId) {
		DbStatement stmt = DbStatementUtils.deleteFrom(E.GENERALSEARCHDOCUMENT,
				E.GENERALSEARCHDOCUMENT.getPk(), govoId);
		dataBaseHelper.getDbAccess().execute(stmt);
	}
	public void removeDependantDocuments(UID entityUID) {
		final String sDBEntity = MetaProvider.getInstance().getEntity(entityUID).getDbTable();
		final String sql = "DELETE FROM T_UD_GO_DOCUMENT WHERE INTID_T_UD_GENERICOBJECT IN (SELECT INTID FROM " + sDBEntity + ")"; 
		dataBaseHelper.getDbAccess().executePlainUpdate(sql);
	}

	public void removeStateHistoryBelonging(Long govoId) {
		DbStatement stmt = DbStatementUtils.deleteFrom(E.STATEHISTORY, E.STATEHISTORY.genericObject, govoId);
		dataBaseHelper.getDbAccess().execute(stmt);
	}
	
	public void removeStateHistoryBelonging(UID sEntity) {
		final String sDBEntity = MetaProvider.getInstance().getEntity(sEntity).getDbTable();
		final String sql = String.format("DELETE FROM %s WHERE %s IN (SELECT INTID FROM %s)",
				E.STATEHISTORY.getDbTable(), E.STATEHISTORY.genericObject.getDbColumn(), sDBEntity); 
		dataBaseHelper.getDbAccess().executePlainUpdate(sql);
	}

	/**
	 * Adapter for KnownSizeIterator.
	 */
	static class KnownSizeIteratorAdapter<E> implements KnownSizeIterator<E> {
		private final Iterator<E> iter;
		private final int iSize;

		/**
		 * creates an Iterator over the given Collection that knows its size in advance.
		 * 
		 * §precondition coll != null
		 * §postcondition this.size() == coll.size();
		 */
		KnownSizeIteratorAdapter(Collection<E> coll) {
			this.iter = coll.iterator();
			this.iSize = coll.size();

			assert this.size() == coll.size();
		}

		@Override
		public boolean hasNext() {
			return this.iter.hasNext();
		}

		@Override
		public E next() {
			return this.iter.next();
		}

	    // default implementation in java8 @Override 
		public void remove() {
			this.iter.remove();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int size() {
			return this.iSize;
		}

	}	// inner class KnownSizeIteratorAdapter
	
}	// class GenericObjectFacadeHelper
