//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.collections15.map.HashedMap;
import org.apache.commons.lang.ObjectUtils;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.AbstractDalVOBasic;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.context.GenerationContextBuilder;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorUsageVO;
import org.nuclos.server.genericobject.valueobject.GeneratorVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for all generic object generator functions. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = {Exception.class})
public class GeneratorFacadeBean extends NuclosFacadeBean implements GeneratorFacadeLocal, GeneratorFacadeRemote {

	@Autowired
	private GenericObjectFacadeLocal genericObjectFacade;

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private EntityObjectFacadeLocal eoFacade;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	public GeneratorFacadeBean() {
	}

	/**
	 * @return all generator actions allowed for the current user.
	 */
	@RolesAllowed("Login")
	public GeneratorVO getGeneratorActions() {
		List<GeneratorActionVO> actions = new LinkedList<>();
		boolean bIsSuperUser = securityCache.isSuperUser(getCurrentUserName());
		final UID currentMandatorUID = getCurrentMandatorUID();

		Collection<UID> assignedGenerations = securityCache.getAssignedGenerations(getCurrentUserName(), currentMandatorUID);
		Collection<MasterDataVO<UID>> mdGenerationsVO = masterDataFacade.getMasterData(E.GENERATION, null);

		Map<UID, MasterDataVO<UID>> actionMdv0 = new HashMap<>();
		for (MasterDataVO<UID> mdVO : mdGenerationsVO) {
			if (bIsSuperUser || assignedGenerations.contains(mdVO.getPrimaryKey())) {
				boolean add = true;
				if (securityCache.isMandatorPresent() && currentMandatorUID != null) {
					final UID sourceMandatorUID = mdVO.getFieldUid(E.GENERATION.sourceMandator);
					final UID targetMandatorUID = mdVO.getFieldUid(E.GENERATION.targetMandator);
					final Set<UID> accessibleMandators = securityCache.getAccessibleMandators(currentMandatorUID);
					if (sourceMandatorUID != null && !accessibleMandators.contains(sourceMandatorUID)) {
						add = false;
					}
					if (targetMandatorUID != null && !accessibleMandators.contains(targetMandatorUID)) {
						add = false;
					}
				}
				if (add) {
					actionMdv0.put(mdVO.getPrimaryKey(), mdVO);
				}
			}
		}

		UID[] uids = actionMdv0.keySet().toArray(new UID[]{});
		Map<UID, Collection<GeneratorUsageVO>> mpUsages = getGeneratorUsages(uids);
		for (UID uid  : mpUsages.keySet()) {
			Collection<GeneratorUsageVO> usages = mpUsages.get(uid);
			actions.add(MasterDataWrapper.getGeneratorActionVO(actionMdv0.get(uid), usages));
		}

		return new GeneratorVO(actions);
	}

	/**
	 * @return object grouped by grouping function within generation
	 */
	@RolesAllowed("Login")
	public <PK> Map<String, Collection<EntityObjectVO<PK>>> groupObjects(Collection<PK> sourceIds, GeneratorActionVO generatoractionvo) throws CommonPermissionException {
		final EntityMeta<?> meta = metaProvider.getEntity(generatoractionvo.getSourceModule());

		checkReadAllowed(meta);

		if (isCalledRemotely() && meta.isStateModel()) {
			checkReadAllowedForModule(meta.getUID());
		}

		Collection<EntityObjectVO<PK>> objects = CollectionUtils.transform(
				sourceIds,
				pk -> nucletDalProvider.<PK>getEntityObjectProcessor(meta.getUID()).getByPrimaryKey(pk)
		);

		return groupGenerationSources(objects, generatoractionvo);
	}

	@RolesAllowed("Login")
	public <PK> GenerationResult<PK> generateGenericObjects(GenerationContext<PK> context) throws CommonBusinessException {

		if (isCalledRemotely()) {
			//1. Check if user can write into the target entity.
			final EntityMeta<?> targetMeta = metaProvider.getEntity(context.getGenerator().getTargetModule());

			checkWriteAllowed(targetMeta);

			//2. Check if user is allowed to use this generation at all.
			Collection<UID> assignedGenerations = securityCache.getAssignedGenerations(getCurrentUserName(), getCurrentMandatorUID());
			if (!assignedGenerations.contains(context.getGenerator().getId())) {
				throw new CommonPermissionException("Generation '" + context.getGenerator() + "' not allowed for User " + getCurrentUserName());
			}

			final EntityMeta<?> sourceMeta = metaProvider.getEntity(context.getGenerator().getSourceModule());

			if (sourceMeta.isStateModel() && !context.isCloningAction()) {
				//3. Check if state and process fits the assigned usage of this generation
				UID uid = context.getGenerator().getId();
				Collection<GeneratorUsageVO> generatorUsages = getGeneratorUsages(uid).get(uid);
				if (!generatorUsages.isEmpty()) {

					for (EntityObjectVO<PK> eo : context.getSourceEos()) {
						GeneratorUsageVO usage2Test = new GeneratorUsageVO(eo.getFieldUid(SF.STATE_UID), eo.getFieldUid(SF.PROCESS_UID));
						boolean usageOkay = false;
						for (GeneratorUsageVO usageVO : generatorUsages) {
							if (usageVO.test(usage2Test)) {
								usageOkay = true;
								break;
							}
						}
						if (!usageOkay) {
							throw new CommonPermissionException("Generation '" + context.getGenerator() + "' not allowed for this usage: " + usage2Test);
						}
					}

				}
			}
		}

		return generateGenericObjectsImpl(context);
	}

	/**
	 * generate one or more generic objects from an existing generic object
	 * (copying selected attributes and subforms)
	 * <p>
	 * §nucleus.permission mayWrite(generatoractionvo.getTargetModuleId())
	 *
	 * @param iSourceObjectId   source generic object id to generate from
	 * @param generatoractionvo generator action value object to determine what to do
	 * @return id of generated generic object (if exactly one object was
	 * generated)
	 */
	@RolesAllowed("Login")
	public GenerationResult generateGenericObject(
			Long iSourceObjectId,
			Long parameterObjectId,
			GeneratorActionVO generatoractionvo,
			String customUsage
	) throws CommonBusinessException {

		EntityMeta<?> sourceMeta = metaProvider.getEntity(generatoractionvo.getSourceModule());
		EntityMeta<?> targetMeta = metaProvider.getEntity(generatoractionvo.getTargetModule());

		this.checkWriteAllowed(targetMeta);

		// We need generic objects, so the entity must be of type PK == Long
		if (sourceMeta.isUidEntity())
			throw new IllegalArgumentException("SourceModule is an UID entity. Only entities of type Long are allowed.");

		IEntityObjectProcessor<Long> proc = (IEntityObjectProcessor<Long>) nucletDalProvider.getEntityObjectProcessor(sourceMeta);
		EntityObjectVO<Long> eo = proc.getByPrimaryKey(iSourceObjectId);

		GenerationContext<Long> context = new GenerationContextBuilder<>(Collections.singletonList(eo), generatoractionvo)
				.parameterObjectId(parameterObjectId)
				.customUsage(customUsage)
				.cloning(false)
				.build();

		return generateGenericObjectsImpl(context);
	}

	/**
	 * Generate a single new object w.r.t. the object generation definition.
	 * <p>
	 * If the generator action is configured with attribute grouping, the
	 * grouping has to be done in advance.
	 * <p>
	 * The general problem with this method is that we need the generated object even if
	 * the generations fails! This is because the client GUI needs to display the
	 * (unfinished) object for the user the find out what has failed.
	 * <p>
	 * We have a long history of problems reports (e.g. http://support.nuclos.de/browse/NUCLOS-3311)
	 * due to the fact that is PLAIN WRONG to throw an exception and bail out here.
	 *
	 * @author Thomas Pasch (javadoc)
	 */
	private <PK> GenerationResult<PK> generateGenericObjectsImpl(
			GenerationContext<PK> context
	) throws CommonBusinessException {

		final EntityMeta<?> sourceMeta = metaProvider.getEntity(context.getGenerator().getSourceModule());
		final EntityMeta<?> targetMeta = metaProvider.getEntity(context.getGenerator().getTargetModule());

		// Create a template for the target object (from single or multiple objects)
		final int sourceSize = context.getSourceEos().size();
		EntityObjectVO<PK> target;
		if (sourceSize > 1) {
			target = getNewObject(context.getSourceEos(), context.getGenerator());
		} else if (sourceSize == 1) {
			final EntityObjectVO<PK> source = context.getSourceEos().iterator().next();
			target = getNewObject(source, context.getGenerator());

			// Create target origin
			if (targetMeta.isStateModel() && sourceMeta.isStateModel()) {
				target.setFieldValue(SF.ORIGIN, source.getFieldValue(SF.SYSTEMIDENTIFIER));
			}

			// link target with source object if possible
			if (context.getGenerator().isCreateRelationBetweenObjects()) {
				UID targetAttribute = getTargetFieldIdIfAny(context.getGenerator(), context.getGenerator().getSourceModule());
				if (targetAttribute != null) {
					if (source.getPrimaryKey() instanceof UID) {
						target.setFieldUid(targetAttribute, (UID) source.getPrimaryKey());
					} else {
						target.setFieldId(targetAttribute, (Long) source.getPrimaryKey());
					}

					// NUCLOS-3311
					// find ref field
					FieldMeta<?> relationFieldMeta = null;
					for (FieldMeta<?> mf : targetMeta.getFields()) {
						if (sourceMeta.getUID().equals(mf.getForeignEntity())) {
							relationFieldMeta = mf;
							break;
						}
					}
					if (relationFieldMeta != null) {
						final String value = RefValueExtractor.get(source, relationFieldMeta.getUID(), null);
						target.setFieldValue(targetAttribute, value);
					}
				}
			}
		} else {
			throw new NuclosFatalException("No source objects for generation");
		}

		// Create target process
		if (targetMeta.isStateModel() && context.getGenerator().getTargetProcess() != null) {
			final String sTargetProcess = masterDataFacade.get(E.PROCESS, context.getGenerator().getTargetProcess()).getFieldValue(E.PROCESS.name);
			target.setFieldValue(SF.PROCESS, sTargetProcess);
			target.setFieldUid(SF.PROCESS_UID, context.getGenerator().getTargetProcess());
		}

		Collection<UsageCriteria> sourceUsages = new ArrayList<>();
		for (EntityObjectVO<PK> sourceObject : context.getSourceEos()) {
			sourceUsages.add(UsageCriteria.createUsageCriteriaFromEO(sourceObject, context.getCustomUsage()));
		}
		final UsageCriteria sourceUsage = UsageCriteria.getGreatestCommonUsageCriteria(sourceUsages);
		final UsageCriteria targetUsage = UsageCriteria.createUsageCriteriaFromEO(target, context.getCustomUsage());

		// Load parameter object (if available)
		final UID parameterEntityUID;
		final EntityObjectVO<PK> parameterObject;
		if (context.getGenerator().getParameterEntity() != null) {
			if (context.getParameterObjectId() == null) {
				// TODO: exception that parameter object is missing
				throw new NuclosFatalException("Missing parameter object");
			}

			parameterEntityUID = context.getGenerator().getParameterEntity();
			parameterObject = (EntityObjectVO<PK>) nucletDalProvider.getEntityObjectProcessor(parameterEntityUID)
					.getByPrimaryKey(context.getParameterObjectId());

			if (context.getGenerator().isCreateRelationToParameterObject()) {
				UID targetAttribute = getTargetFieldIdIfAny(context.getGenerator(), context.getGenerator().getParameterEntity());
				if (targetAttribute != null) {
					target.setFieldId(targetAttribute, context.getParameterObjectId());
					// NUCLOS-2559: create stringified value
					target.setFieldValue(targetAttribute,
							RefValueExtractor.get(parameterObject, targetAttribute, null, metaProvider));
				}
			}
		} else {
			parameterEntityUID = null;
			parameterObject = null;
		}

		// Copy parameter attributes
		if (parameterObject != null) {
			copyParameterAttributes(parameterObject, parameterEntityUID, target, context.getGenerator());
		}

		// add dependants
		final IDependentDataMap dependants = new DependentDataMap();
		final Collection<EntityObjectVO<UID>> subentities =
				masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONSUBENTITY.generation,
						context.getGenerator().getId());

		// copy dependants (grouping == false)
		for (EntityObjectVO<UID> subentity : subentities) {
			if (LangUtils.defaultIfNull(subentity.getFieldValue(E.GENERATIONSUBENTITY.groupAttributes),
					Boolean.FALSE)) {
				continue;
			}
			copyDependants(
					context.getSourceEos(),
					parameterObject,
					context.getGenerator(),
					subentity,
					target,
					dependants,
					context.getCustomUsage()
			);
		}

		// aggregate dependants
		for (EntityObjectVO<UID> subentity : subentities) {
			if (!LangUtils.defaultIfNull(subentity.getFieldValue(E.GENERATIONSUBENTITY.groupAttributes),
					Boolean.FALSE)) {
				continue;
			}
			Collection<PK> sourceObjectIds = CollectionUtils.transform(
					context.getSourceEos(),
					EntityObjectVO::getPrimaryKey
			);
			aggregateDependants(
					sourceObjectIds,
					sourceUsage,
					subentity,
					targetUsage,
					dependants
			);
		}

		// restore current mandator later
		final UID mandatorInContext = getCurrentMandatorUID();

		if (targetMeta.isMandator()) {
			UID targetMandator = context.getGenerator().getTargetMandator();
			if (targetMandator != null) {
				// change current mandator in context if generator is mandator specific
				setCurrentMandatorUID(targetMandator);
			}
			if (targetMandator == null) {
				if (sourceMeta.isMandator()) {
					final Set<UID> sourceMandators = new HashSet<>();
					for (EntityObjectVO<?> source : context.getSourceEos()) {
						sourceMandators.add(source.getFieldUid(SF.MANDATOR_UID));
					}
					if (targetMeta.getMandatorLevel().equals(sourceMeta.getMandatorLevel())) {
						if (sourceMandators.size() == 1) {
							targetMandator = sourceMandators.iterator().next();
						}
					} else if (!sourceMandators.isEmpty()) {
						// Gemeinsamen Nenner finden (vielleicht sind die sourceMandators alle in einer niedrigeren Ebene, und es gibt einen gemeinsamen Mandanten oberhalb)
						Set<UID> accessibleMandators = new HashSet<>();
						for (UID sourceMandator : sourceMandators) {
							accessibleMandators.addAll(SecurityCache.getInstance().getAccessibleMandators(sourceMandator));
						}
						// Nach Target Ebene filtern
						Iterator<UID> itAccMan = accessibleMandators.iterator();
						while (itAccMan.hasNext()) {
							MandatorVO mandatorVO = SecurityCache.getInstance().getMandator(itAccMan.next());
							if (!LangUtils.equal(mandatorVO.getLevelUID(), targetMeta.getMandatorLevel())) {
								itAccMan.remove();
							}
						}
						if (accessibleMandators.size() == 1) {
							targetMandator = accessibleMandators.iterator().next();
						}
					}
				}
			}
			if (targetMandator != null) {
				target.setFieldUid(SF.MANDATOR_UID, targetMandator);
				target.setFieldValue(SF.MANDATOR, SecurityCache.getInstance().getMandator(targetMandator).getName());
			}
		}

		try {
			if (targetMeta.isStateModel()) {
				//TODO: Generics, assuming that Generic Objects will always have Long ids.
				GenerationResult<PK> result = createGeneratedGO(
						targetMeta,
						(EntityObjectVO<Long>) target,
						sourceMeta,
						(Collection<EntityObjectVO<Long>>) (Object) context.getSourceEos(),
						dependants,
						parameterObject,
						context.getGenerator(),
						context.getCustomUsage(),
						context.isCloningAction()
				);
				if (context.isCloningAction()) {
					fillInMissingRefValues(result.getGeneratedObject());
				}
				return result;
			} else {
				return createGeneratedMD(
						targetMeta,
						target,
						context.getSourceEos(),
						parameterObject,
						context.getGenerator(),
						context.getCustomUsage(),
						context.isCloningAction()
				);
			}
		} catch (GeneratorFailedException gfex) {
			EntityObjectVO<?> eo = gfex.getGenerationResult().getGeneratedObject();
			fillInMissingRefValues(eo);
			throw gfex;
		} finally {
			setCurrentMandatorUID(mandatorInContext);
		}
	}

	/**
	 * TODO: TOO MANY PARAMETERS!
	 */
	private GenerationResult createGeneratedGO(
			EntityMeta<?> targetMeta,
			EntityObjectVO<Long> target,
			EntityMeta<?> sourceMeta,
			Collection<EntityObjectVO<Long>> sourceObjects,
			IDependentDataMap dependants,
			EntityObjectVO<?> parameterObject,
			GeneratorActionVO generatoractionvo,
			String customUsage,
			boolean bIsCloningAction
	) throws GeneratorFailedException {

		target.setFieldValue(SF.LOGICALDELETED, Boolean.FALSE);
		final GenericObjectVO go = DalSupportForGO.getGenericObjectVO(target);

		final GenericObjectVO result =
				new GenericObjectVO(generatoractionvo.getTargetModule(), GenericObjectMetaDataCache.getInstance());

		final Set<UID> targetAttributes =
				metaProvider.getAllEntityFieldsByEntity(targetMeta.getUID()).keySet();

		for (UID targetAttribute : targetAttributes) {
			DynamicAttributeVO davo = go.getAttribute(targetAttribute);
			if (davo != null) {
				result.setAttribute(new DynamicAttributeVO(targetAttribute, davo.getValueId(), davo.getValueUid(),
						davo.getValue()));
			}
		}

		GenericObjectWithDependantsVO targetGODep = new GenericObjectWithDependantsVO(result, dependants, result.getDataLanguageMap());
		// Create the new object
		EntityObjectVO<Long> executeGenerationEventSupports = DalSupportForGO.wrapGenericObjectVO(targetGODep);
		try {
			// execute rules (before)
			final List<String> lstActions = new ArrayList<>();
			executeGenerationEventSupports = executeGenerationEventSupports(generatoractionvo,
					executeGenerationEventSupports, sourceObjects, parameterObject,
					lstActions, false);
			targetGODep = DalSupportForGO.getGenericObjectWithDependants(executeGenerationEventSupports);

			if (!bIsCloningAction) {
				GenericObjectVO created = genericObjectFacade.create(targetGODep, customUsage, false);
				performDeferredActionsFromRules(lstActions, created.getId(), genericObjectFacade);
				if (sourceMeta.isStateModel() && generatoractionvo.isCreateRelationBetweenObjects()) {
					for (EntityObjectVO<Long> source : sourceObjects) {
						relateCreatedGenericObjectToParent(source.getPrimaryKey(), genericObjectFacade,
								generatoractionvo, created.getId());
					}
				}
				// execute rules (after)
				executeGenerationEventSupports = DalSupportForGO.wrapGenericObjectVO(created);
				executeGenerationEventSupports = executeGenerationEventSupports(
						generatoractionvo, executeGenerationEventSupports, sourceObjects,
						parameterObject, lstActions, true);

				return new GenerationResult(CollectionUtils.transform(sourceObjects, new ExtractIdTransformer()),
						DalSupportForGO.wrapGenericObjectVO(genericObjectFacade.get(created.getModule(), created.getId())), null);
			} else {
				return new GenerationResult(CollectionUtils.transform(sourceObjects, new ExtractIdTransformer()),
						DalSupportForGO.wrapGenericObjectVO(targetGODep), null);
			}
		} catch (NuclosBusinessRuleException e) {
			// needed for http://project.nuclos.de/browse/LIN-116
			final BusinessException cause = e.getCausingBusinessException();
			final EntityObjectVO<Long> temp = executeGenerationEventSupports;
			temp.setPrimaryKey(null);
			if (cause != null) {
				final GenerationResult gr = new GenerationResult(
						CollectionUtils.transform(sourceObjects, new ExtractIdTransformer()),
						temp,
						cause.getMessage()
				);
				throw new GeneratorFailedException("Generation of generic object failed: " + temp
						+ " action: " + generatoractionvo, gr, cause);
			} else {
				final GenerationResult gr = new GenerationResult(
						CollectionUtils.transform(sourceObjects, new ExtractIdTransformer()),
						temp,
						e.getMessage()
				);
				throw new GeneratorFailedException("Generation of generic object failed: " + temp
						+ " action: " + generatoractionvo, gr, e);
			}
		} catch (CommonBusinessException ex) {
			final EntityObjectVO<Long> temp = executeGenerationEventSupports;
			temp.setPrimaryKey(null);
			final GenerationResult gr = new GenerationResult(
					CollectionUtils.transform(sourceObjects, new ExtractIdTransformer()),
					temp,
					ex.getMessage()
			);
			throw new GeneratorFailedException("Generation of generic object failed: " + temp
					+ " action: " + generatoractionvo, gr, ex);
		}
	}

	/**
	 * TODO: TOO MANY PARAMETERS!
	 */
	private <PK> GenerationResult<PK> createGeneratedMD(
			EntityMeta<?> targetMeta,
			EntityObjectVO<PK> target,
			Collection<EntityObjectVO<PK>> sourceObjects,
			EntityObjectVO<?> parameterObject,
			GeneratorActionVO generatoractionvo,
			String customUsage,
			boolean bIsCloningAction
	) throws GeneratorFailedException {

		MasterDataVO<PK> md = new MasterDataVO<>(target);

		// Create the new object
		final UID uid = targetMeta.getUID();
		try {
			// execute rules (before)
			final List<String> lstActions = new ArrayList<>();
			EntityObjectVO<PK> executeGenerationEventSupports = executeGenerationEventSupports(
					generatoractionvo, target, sourceObjects, parameterObject, lstActions, false);
			md = new MasterDataVO<>(executeGenerationEventSupports);

			if (!bIsCloningAction) {
				target = masterDataFacade.create(md, customUsage).getEntityObject();

				// execute rules (after)
				executeGenerationEventSupports(generatoractionvo, target, sourceObjects, parameterObject, new ArrayList<>(), true);

				return new GenerationResult<>(CollectionUtils.transform(sourceObjects, new ExtractIdTransformer<>()),
						masterDataFacade.get(uid, target.getPrimaryKey()).getEntityObject(), null);
			} else {
				fillInMissingRefValues(md.getEntityObject());
				return new GenerationResult<>(CollectionUtils.transform(sourceObjects, new ExtractIdTransformer<>()),
						md.getEntityObject(), null);
			}
		} catch (NuclosBusinessRuleException e) {
			// needed for http://project.nuclos.de/browse/LIN-116
			final BusinessException cause = e.getCausingBusinessException() != null ? e.getCausingBusinessException() : new BusinessException(e);
			md.setPrimaryKey(null);
			EntityObjectVO<PK> temp = md.getEntityObject();
			temp.setDependents(md.getDependents());
			final GenerationResult<PK> gr = new GenerationResult<>(CollectionUtils.transform(
					sourceObjects, new ExtractIdTransformer<>()), temp, cause.getMessage());
			throw new GeneratorFailedException("Generation of MD object failed: " + temp
					+ " action: " + generatoractionvo, gr, cause);
		} catch (CommonBusinessException ex) {
			// this is required, because the id is already generated
			md.setPrimaryKey(null);
			EntityObjectVO<PK> temp = md.getEntityObject();
			temp.setDependents(md.getDependents());
			final GenerationResult<PK> gr = new GenerationResult<>(CollectionUtils.transform(
					sourceObjects, new ExtractIdTransformer<>()), temp, ex.getMessage());
			throw new GeneratorFailedException("Generation of MD object failed: " + temp
					+ " action: " + generatoractionvo, gr, ex);
		}

	}

	private <PK> EntityObjectVO<PK> executeGenerationEventSupports(
			GeneratorActionVO generatoractionvo,
			EntityObjectVO<PK> loccvoTargetBeforeRules,
			Collection<EntityObjectVO<PK>> loccvoSourceObjects,
			EntityObjectVO<?> loccvoParameter,
			List<String> lstActions,
			boolean after
	)
			throws NuclosBusinessRuleException, NuclosCompileException, CommonPermissionException {

		final EventSupportFacadeLocal facade = SpringApplicationContextHolder.getBean(EventSupportFacadeLocal.class);
		return facade.fireGenerationEventSupport(generatoractionvo.getId(), loccvoTargetBeforeRules,
				loccvoSourceObjects, loccvoParameter, lstActions, generatoractionvo.getProperties(), after);
	}

	private UID getTargetFieldIdIfAny(GeneratorActionVO generatoractionvo, UID sourceEntityUid) {
		final UID iTargetModuleId = generatoractionvo.getTargetModule();
		final Map<UID, FieldMeta<?>> mp = metaProvider.getAllEntityFieldsByEntity(iTargetModuleId);
		for (UID sFieldName : mp.keySet()) {
			FieldMeta<?> voField = mp.get(sFieldName);
			if (sourceEntityUid.equals(voField.getForeignEntity())) {
				return voField.getUID();
			}
		}
		return null;
	}

	/**
	 * Relate the new target object to the source object. If source object is an
	 * invoice section relate the new object to the invoice:
	 */
	private void relateCreatedGenericObjectToParent(
			Long sourceId,
			GenericObjectFacadeLocal lofacade,
			GeneratorActionVO generatoractionvo,
			Long targetGenericObjectId
	) throws CommonFinderException, CommonPermissionException, NuclosBusinessRuleException {
		if (sourceId != null) {
			try {
				lofacade.relate(generatoractionvo.getTargetModule(), targetGenericObjectId, sourceId, GenericObjectTreeNode.SystemRelationType.PREDECESSOR_OF.getValue());
			} catch (CommonCreateException ex) {
				throw new NuclosFatalException(ex);
			}
		}
	}

	/**
	 * Executes a list of commands prepared by the rule interface for actions,
	 * which cannot be performed before object creation, e.g. object relation
	 * with the freshly created object.
	 *
	 * @param lstActionsFromRules    List<String> of commands from deferred methods
	 * @param iTargetGenericObjectId id of the freshly created object (if any)
	 * @param lofacade               (for performance)
	 */
	private void performDeferredActionsFromRules(
			List<String> lstActionsFromRules,
			Long iTargetGenericObjectId,
			GenericObjectFacadeLocal lofacade
	) throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, CommonPermissionException {

		for (String sAction : lstActionsFromRules) {
			final String[] asAction = sAction.split(":", 4);
			if ("relate".equals(asAction[0])) {
				if (asAction[1].equals(asAction[2])) {
					throw new NuclosBusinessRuleException("generator.facade.exception.1");// "Ein Objekt darf nicht mit sich selbst verkn\u00fcpft werden.");
				}
				final Long iTargetId = "this".equals(asAction[1]) ? iTargetGenericObjectId : Long.parseLong(asAction[1]);
				final UID iTargetModuleId = lofacade.getModuleContainingGenericObject(iTargetId);
				String relationType = asAction[3];
				lofacade.relate(iTargetModuleId, iTargetId, "this".equals(asAction[2]) ? iTargetGenericObjectId : Integer.parseInt(asAction[2]), relationType);
			}
		}
	}

	private <PK> Map<String, Collection<EntityObjectVO<PK>>> groupGenerationSources(Collection<EntityObjectVO<PK>> col, GeneratorActionVO generatoractionvo) {
		Set<UID> groupingAttributes = new HashSet<>();

		Collection<EntityObjectVO<UID>> attributes =
				masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONATTRIBUTE.generation, generatoractionvo.getId());

		for (EntityObjectVO<UID> attribute : attributes) {
			if (!StringUtils.isNullOrEmpty(attribute.getFieldValue(E.GENERATIONATTRIBUTE.sourceType))) {
				continue;
			}
			String groupFunction = attribute.getFieldValue(E.GENERATIONATTRIBUTE.groupfunction);
			if (!StringUtils.isNullOrEmpty(groupFunction) && !"group by".equals(groupFunction)) {
				continue;
			}
			UID source = attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource);
			if (E.isNuclosEntity(source)) {
				continue;
			}
			groupingAttributes.add(source);
		}

		Map<String, Collection<EntityObjectVO<PK>>> mp = new Hashtable<>();

		for (EntityObjectVO<PK> eo : col) {
			StringBuilder sb = new StringBuilder();
			for (UID attribute : groupingAttributes) {
				Object valueToUse = null;
				// This UID can identify an UID, a Long or a simple value object
				if (eo.getFieldUid(attribute) != null) {
					valueToUse = eo.getFieldUid(attribute);
				} else if (eo.getFieldId(attribute) != null) {
					valueToUse = eo.getFieldId(attribute);
				} else if (eo.getFieldValue(attribute) != null) {
					valueToUse = eo.getFieldValue(attribute);
				}

				sb.append(valueToUse);
				sb.append(".");
			}
			final String key = sb.toString();
			if (!mp.containsKey(key)) {
				mp.put(key, new ArrayList<>());
				mp.get(key).add(eo);
			} else {
				mp.get(key).add(eo);
			}
		}

		return mp;
	}

	private <PK> EntityObjectVO<PK> getGroupedGeneratedObject(GeneratorActionVO generator, Collection<PK> sourceIds) {

		final UID sourceEntity =
				metaProvider.getEntity(generator.getSourceModule()).getUID();

		final UID targetEntity =
				metaProvider.getEntity(generator.getTargetModule()).getUID();

		final Collection<EntityObjectVO<UID>> attributes =
				masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONATTRIBUTE.generation, generator.getId());

		EntityMeta<?> entity = metaProvider.getEntity(sourceEntity);
		// NUCLOSINT-1358

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<?> t = query.from(entity);

		List<DbSelection<?>> selection = new ArrayList<>();
		List<DbExpression<?>> groupby = new ArrayList<>();

		for (EntityObjectVO<UID> attribute : attributes) {
			String fieldSourceType = attribute.getFieldValue(E.GENERATIONATTRIBUTE.sourceType);

			//	final String type = (String) attribute.getFieldValue("sourceType");
			// only process source entity attributes (skip parameter entity attributes)
			if (StringUtils.isNullOrEmpty(fieldSourceType)) {

				final String function = attribute.getFieldValue(E.GENERATIONATTRIBUTE.groupfunction);
				final UID source = attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource);

				final FieldMeta<?> meta = metaProvider.getEntityField(source);
				final String column = meta.getDbColumn();
				DbColumnExpression<?> c;
				c = t.baseColumn(meta);

				//@see NUCLOS-1436
				final String sField = meta.getFieldName().equals("name") ? "\"" + meta.getFieldName() + "\"" : meta.getFieldName();
				if (function == null || "group by".equals(function)) {
					if (meta.getForeignEntity() != null && meta.getForeignEntityField() != null) {
						c = t.baseColumn(SimpleDbField.createRef(column, false));
					}
					c.alias(sField);
					selection.add(c);
					groupby.add(c);
				} else if ("summate".equals(function)) {
					selection.add(builder.sum(c).alias(sField));
				} else if ("minimum value".equals(function)) {
					selection.add(builder.min(c).alias(sField));
				} else if ("maximum value".equals(function)) {
					selection.add(builder.max(c).alias(sField));
				}
			}
		}

		query.multiselect(selection);
		if (groupby.size() > 0) {
			query.groupBy(groupby);
		}

		ArrayList<DbCondition> conditions = new ArrayList<>();
		for (PK sourceId : sourceIds) {
			if (sourceId instanceof UID) {
				conditions.add(builder.equalValue(t.baseColumn(SF.PK_UID), (UID) sourceId));
			} else {
				conditions.add(builder.equalValue(t.baseColumn(SF.PK_ID), (Long) sourceId));
			}
		}
		query.where(builder.or(conditions.toArray(new DbCondition[0])));

		DbTuple tuple = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);

		final EntityObjectVO<PK> result = new EntityObjectVO<>(targetEntity);

		if (metaProvider.getEntity(targetEntity).isStateModel()) {
			result.setFieldValue(SF.LOGICALDELETED, Boolean.FALSE);
		}

		for (EntityObjectVO<UID> attribute : attributes) {
			final String type = attribute.getFieldValue(E.GENERATIONATTRIBUTE.sourceType);
			if (!StringUtils.isNullOrEmpty(type)) {
				continue;
			}
			final UID target = attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeTarget);

			//@see NUCLOS-1436
			FieldMeta<?> meta = metaProvider.getEntityField(attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource));
			final String sField = meta.getFieldName().equals("name") ? "\"" + meta.getFieldName() + "\"" : meta.getFieldName();
			if (meta.getForeignEntity() != null && meta.getForeignEntityField() != null) {
				Long refId = tuple.get(sField, Long.class);
				final IEntityObjectProcessor<Object> eop = nucletDalProvider.getEntityObjectProcessor(meta.getForeignEntity());
				final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
				try {
					eop.setIgnoreRecordGrantsAndOthers(true);
					EntityObjectVO<?> referencedObject = eop.getByPrimaryKey(refId);
					String refValue = RefValueExtractor.get(referencedObject, target, null, metaProvider);
					result.setFieldValue(target, refValue);
					result.setFieldId(target, refId);
				} finally {
					eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
				}
			} else {
				result.setFieldValue(target, tuple.get(sField));
			}
		}

		return result;
	}

	private <PK> void aggregateDependants(
			Collection<PK> sourceIds,
			UsageCriteria sourceUsage,
			EntityObjectVO<UID> subentity,
			UsageCriteria targetUsage,
			IDependentDataMap dependants
	) {

		final Collection<EntityObjectVO<UID>> attributes =
				masterDataFacade.getDependantMd4FieldMeta(
						E.GENERATIONSUBENTITYATTRIBUTE.entity, subentity.getPrimaryKey());
		if (attributes.size() == 0) {
			return;
		}

		final EntityMeta<?> sourceEntity =
				metaProvider.getEntity(subentity.getFieldUid(E.GENERATIONSUBENTITY.entitySource));
		final EntityMeta<?> targetEntity =
				metaProvider.getEntity(subentity.getFieldUid(E.GENERATIONSUBENTITY.entityTarget));

		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		final DbFrom<?> t = query.from(sourceEntity);

		final List<DbSelection<?>> selection = new ArrayList<>();
		final List<DbExpression<?>> groupby = new ArrayList<>();
		final Map<EntityObjectVO<?>, String> aliases = new HashedMap<>();
		for (EntityObjectVO<?> attribute : attributes) {
			final String function = attribute.getFieldValue(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeGrouping);
			final UID source = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeSource);

			final FieldMeta<?> meta = metaProvider.getEntityField(source);
			// final String column = meta.getDbColumn();
			DbColumnExpression<?> c = t.baseColumn(meta);

			if ("group by".equals(function)) {
				if (meta.getForeignEntity() != null && meta.getForeignEntityField() != null) {
					// c = t.baseColumn(SimpleDbField.createRef(column, meta.getJavaClass()));
					// c.alias(meta.getFieldName() + "Id");
					// selection.add(c);
					// groupby.add(c);
					c = t.baseColumnRef(meta, sourceEntity.isUidEntity());
					c.alias(meta.getFieldName());
					selection.add(c);
					groupby.add(c);
				} else {
					selection.add(c);
					groupby.add(c);
				}
			} else if ("summate".equals(function)) {
				selection.add(builder.sum(c).alias(meta.getDbColumn()));
			} else if ("minimum value".equals(function)) {
				selection.add(builder.min(c).alias(meta.getDbColumn()));
			} else if ("maximum value".equals(function)) {
				selection.add(builder.max(c).alias(meta.getDbColumn()));
			}
			aliases.put(attribute, c.getAlias());
		}

		query.multiselect(selection);
		if (groupby.size() > 0) {
			query.groupBy(groupby);
		}

		final IDependentKey foreignFieldSource = getForeignKeyField(sourceEntity, sourceUsage);
		final IDependentKey foreignFieldTarget = getForeignKeyField(targetEntity, targetUsage);

		final ArrayList<DbCondition> conditions = new ArrayList<>();
		for (PK sourceId : sourceIds) {
			conditions.add(builder.equalValue((DbColumnExpression<PK>) t.baseColumnRef(metaProvider.getEntityField(foreignFieldSource.getDependentRefFieldUID()), false), sourceId));
		}
		query.where(builder.or(conditions.toArray(new DbCondition[0])));

		final List<EntityObjectVO<Long>> aggregated = dataBaseHelper.getDbAccess().executeQuery(query, tuple -> {
			final EntityObjectVO<Long> result = new EntityObjectVO<>(targetEntity.getUID());

			for (EntityObjectVO<?> attribute : attributes) {
				final UID source = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeSource);
				final UID target = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeTarget);
				final FieldMeta<?> meta = metaProvider.getEntityField(source);
				// result.setFieldValue(target, tuple.get(meta.getDbColumn()));
				result.setFieldValue(target, tuple.get(aliases.get(attribute)));
				if (meta.getForeignEntity() != null && meta.getForeignEntityField() != null) {
					result.setFieldId(target, tuple.get(meta.getFieldName(), Long.class));
				}
			}
			return result;
		});

		dependants.addAllData(foreignFieldTarget, aggregated);
	}

	private IDependentKey getForeignKeyField(EntityMeta<?> mdmetavo, UsageCriteria foreignEntityUsage) {
		// try to get from layout first...
		IDependentKey dependentKey = SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class).getDependentKeyBetween(foreignEntityUsage, mdmetavo.getUID());
		if (dependentKey != null) {
			return dependentKey;
		}

		final UID foreignKeyField;
		final Set<UID> foreignKeyFields = new TreeSet<>();
		for (FieldMeta fieldMeta : mdmetavo.getFields()) {
			if (foreignEntityUsage.getEntityUID().equals(fieldMeta.getForeignEntity())) {
				foreignKeyFields.add(fieldMeta.getUID());
			}
		}
		switch (foreignKeyFields.size()) {
			case 0:
				throw new NuclosFatalException("There is no foreign key in entity=" + mdmetavo);
			default:
				foreignKeyField = foreignKeyFields.iterator().next();
		}

		return DependentDataMap.createDependentKey(foreignKeyField);
	}

	/**
	 * copies all dependant records from source object to target object. Copies
	 * also a basekey and ordernumber attribute value as dependant into target,
	 * if appropriate and desired
	 *
	 * @param sources    source generic object with dependant masterdata
	 * @param gavo       details about generation action
	 * @param dependants DependantMasterDataMap for the the result
	 */
	private <PK> void copyDependants(
			Collection<EntityObjectVO<PK>> sources,
			EntityObjectVO parameterObject,
			GeneratorActionVO gavo,
			EntityObjectVO<UID> subentity,
			EntityObjectVO<PK> target,
			IDependentDataMap dependants,
			String customUsage
	) throws CommonPermissionException, CommonFinderException {
		final GenericObjectMetaDataCache lometacache = GenericObjectMetaDataCache.getInstance();

		final UsageCriteria criteria = new UsageCriteria(gavo.getTargetModule(),
				target.getFieldUid(SF.PROCESS_UID),
				target.getFieldUid(SF.STATE_UID), customUsage);

		final UID layoutUidTarget = lometacache.getBestMatchingLayout(criteria, false);
		final Set<UID> setEntityNamesTarget = lometacache.getSubFormEntityByLayout(layoutUidTarget);

		final EntityMeta<?> sourceMeta =
				metaProvider.getEntity(subentity.getFieldUid(E.GENERATIONSUBENTITY.entitySource));
		final EntityMeta<?> targetMeta =
				metaProvider.getEntity(subentity.getFieldUid(E.GENERATIONSUBENTITY.entityTarget));

		UID sSource = sourceMeta.getUID();
		UID sTarget = targetMeta.getUID();

		if (!setEntityNamesTarget.contains(sTarget)) {
			return;
		}
		if (!targetMeta.isEditable()) {
			throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("generator.dependant.noneditable", sTarget));
		}

		final Collection<EntityObjectVO<UID>> attributes = masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONSUBENTITYATTRIBUTE.entity, subentity.getPrimaryKey());
		String sourceType = subentity.getFieldValue(E.GENERATIONSUBENTITY.sourceType);

		UsageCriteria targetUsage = UsageCriteria.createUsageCriteriaFromEO(target, customUsage);

		if (StringUtils.isNullOrEmpty(sourceType)) {
			for (EntityObjectVO<?> source : sources) {
				final UsageCriteria sourcecriteria = new UsageCriteria(gavo.getSourceModule(),
						source.getFieldUid(SF.PROCESS_UID), source.getFieldUid(SF.STATE_UID), customUsage);

				final UID iLayoutIdSource = lometacache.getBestMatchingLayout(sourcecriteria, false);
				final Set<UID> setEntityNamesSource = lometacache.getSubFormEntityByLayout(iLayoutIdSource);

				if (!setEntityNamesSource.contains(sSource)) {
					continue;
				}

				final Collection<EntityAndField> eafns = lometacache.getSubFormEntityAndForeignKeyFieldsByLayout(iLayoutIdSource);

				UID foreignfield = null;
				for (EntityAndField eafn : eafns) {
					if (eafn.getEntity().equals(sSource)) {
						foreignfield = eafn.getField();
					}
				}
				if (foreignfield == null) {
					throw new NuclosFatalException();
				}

				copyAttributes(gavo, attributes, sSource, foreignfield, source.getPrimaryKey(), targetUsage, sTarget, dependants);
			}
		} else {
			EntityMeta<?> parameterEntity = metaProvider.getEntity(gavo.getParameterEntity());
			LayoutFacadeLocal layoutLocal = SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class);

			Map<EntityAndField, UID> eafns =
					layoutLocal.getSubFormEntityAndParentSubFormEntities(parameterEntity.getUID(), parameterObject.getPrimaryKey(), false, customUsage);

			UID foreignfield = null;
			for (Map.Entry<EntityAndField, UID> e : eafns.entrySet()) {
				if (e.getValue() == null) {
					if (e.getKey().getEntity().equals(sSource)) {
						foreignfield = e.getKey().getField();
					}
				}
			}

			if (foreignfield == null) {
				throw new NuclosFatalException();
			}

			copyAttributes(gavo, attributes, sSource, foreignfield, parameterObject.getPrimaryKey(), targetUsage, sTarget, dependants);
		}
	}

	private void copyAttributes(GeneratorActionVO gavo, Collection<EntityObjectVO<UID>> attributes, UID source, UID fef,
								Object sourceId, UsageCriteria targetUsage, UID target, IDependentDataMap dependants) {

		EntityMeta<Object> subformMeta = metaProvider.getEntity(target);
		final IDependentKey foreignFieldTarget = getForeignKeyField(subformMeta, targetUsage);

		Collection<EntityObjectVO<Object>> data = masterDataFacade.getDependantMasterData(fef, sourceId);
		if (attributes.size() == 0) {
			for (EntityObjectVO<?> mdvoOriginal : data) {
				final EntityObjectVO<?> mdvo = mdvoOriginal.copyFlat();
				mdvo.setDependents(new DependentDataMap());

				final EntityMeta<?> eMetaSource = metaProvider.getEntity(gavo.getSourceModule());
				for (FieldMeta<?> fieldmeta : metaProvider.getAllEntityFieldsByEntity(source).values()) {
					if (eMetaSource.getUID().equals(fieldmeta.getForeignEntity())) {
						if (!eMetaSource.isUidEntity())
							mdvo.setFieldId(fieldmeta.getUID(), null);
						else
							mdvo.setFieldUid(fieldmeta.getUID(), null);
					}
				}
				dependants.addData(foreignFieldTarget, mdvo);
			}
		} else {
			for (EntityObjectVO<?> mdvoOriginal : data) {
				final MasterDataVO<?> mdvo = new MasterDataVO<>(metaProvider.getEntity(target), true);
				for (EntityObjectVO<?> attribute : attributes) {
					final UID fieldAttSource = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeSource);
					final UID fieldAttTarget = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeTarget);

					final Object value = mdvoOriginal.getFieldValue(fieldAttSource);
					mdvo.setFieldValue(fieldAttTarget, value);

					final FieldMeta<?> metaField = metaProvider.getEntityField(fieldAttSource);
					if (metaField.getForeignEntity() != null) {
						if (!metaProvider.getEntity(metaField.getForeignEntity()).isUidEntity()) {
							mdvo.setFieldId(fieldAttTarget, mdvoOriginal.getFieldId(fieldAttSource));
						} else {
							mdvo.setFieldUid(fieldAttTarget, mdvoOriginal.getFieldUid(fieldAttSource));
						}
					}
				}
				dependants.addData(foreignFieldTarget, mdvo.getEntityObject());
			}
		}
	}

	/**
	 * copies all attributes from source object to target template
	 */
	private <PK> EntityObjectVO<PK> getNewObject(
			EntityObjectVO<PK> eoSource,
			GeneratorActionVO generatoractionvo
	) {
		final EntityMeta<?> sourceMeta = metaProvider.getEntity(generatoractionvo.getSourceModule());
		final EntityMeta<?> targetMeta = metaProvider.getEntity(generatoractionvo.getTargetModule());

		final EntityObjectVO<PK> result = new EntityObjectVO<>(targetMeta);
		eoFacade.setDefaultValues(result);

		final List<Pair<UID, UID>> includedAttributes = getIncludedAttributes(generatoractionvo, null);

		final Set<UID> stSourceAttributes = metaProvider.getAllEntityFieldsByEntity(sourceMeta.getUID()).keySet();
		final Set<UID> stTargetAttribute = metaProvider.getAllEntityFieldsByEntity(targetMeta.getUID()).keySet();

		setAttValues(includedAttributes, stSourceAttributes, stTargetAttribute, eoSource, result);
		return result;
	}

	private <PK> EntityObjectVO<PK> getNewObject(Collection<EntityObjectVO<PK>> sourceObjects, GeneratorActionVO generatoractionvo) {
		return getGroupedGeneratedObject(
				generatoractionvo,
				CollectionUtils.transform(sourceObjects, AbstractDalVOBasic::getPrimaryKey)
		);
	}

	private static <PK> void setAttValues(List<Pair<UID, UID>> includedAttributes, Set<UID> stSourceAttributes,
								  Set<UID> stTargetAttribute, EntityObjectVO<PK> eoSource, EntityObjectVO<PK> result) {
		for (Pair<UID, UID> p : includedAttributes) {
			if (stSourceAttributes.contains(p.x)) {
				if (stTargetAttribute.contains(p.y)) {
					if (eoSource.getFieldValue(p.x) != null) {
						result.setFieldValue(p.y, eoSource.getFieldValue(p.x));
					}
					if (eoSource.getFieldId(p.x) != null) {
						result.setFieldId(p.y, eoSource.getFieldId(p.x));
					}
					if (eoSource.getFieldUid(p.x) != null) {
						result.setFieldUid(p.y, eoSource.getFieldUid(p.x));
					}
				}
			}
		}
	}

	private <PK> void copyParameterAttributes(EntityObjectVO<PK> sourceVO, UID sourceEntityUID, EntityObjectVO<PK> target, GeneratorActionVO generatoractionvo) {
		final List<Pair<UID, UID>> includedAttributes = getIncludedAttributes(generatoractionvo, "parameter");

		final Set<UID> stSourceAttributes = metaProvider.getAllEntityFieldsByEntity(sourceEntityUID).keySet();
		final Set<UID> stTargetAttribute = metaProvider.getAllEntityFieldsByEntity(target.getDalEntity()).keySet();

		setAttValues(includedAttributes, stSourceAttributes, stTargetAttribute, sourceVO, target);

	}

	/**
	 * Extract the IDs of the attributes to copy from the given
	 * GeneratorActionVO as a collection.
	 *
	 * @return list of pairs (source id, target id)
	 */
	private List<Pair<UID, UID>> getIncludedAttributes(GeneratorActionVO generatoractionvo, String sourceType) {
		Collection<EntityObjectVO<UID>> colmdvo =
				masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONATTRIBUTE.generation, generatoractionvo.getId());
		List<Pair<UID, UID>> result = new ArrayList<>(colmdvo.size());
		for (EntityObjectVO<UID> mdvo : colmdvo) {
			if (!ObjectUtils.equals(sourceType, mdvo.getFieldValue(E.GENERATIONATTRIBUTE.sourceType)))
				continue;
			UID source = mdvo.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource);
			UID target = mdvo.getFieldUid(E.GENERATIONATTRIBUTE.attributeTarget);

			if (E.isNuclosEntity(source) || E.isNuclosEntity(target))
				continue;
			result.add(Pair.makePair(source, target));
		}
		return result;
	}

	/**
	 * get generator usages for specified GeneratorId
	 */
	public Map<UID, Collection<GeneratorUsageVO>> getGeneratorUsages(UID...uids) throws CommonFatalException {
		Map<UID, Collection<GeneratorUsageVO>> mpUsages = new HashMap<>();
		for (UID uid : uids) {
			mpUsages.put(uid, new ArrayList<>());
		}

		CollectableEntityField cef = SearchConditionUtils.newEntityField(E.GENERATIONUSAGE.generation);
		CollectableInCondition<UID> cond2 = new CollectableInCondition<>(cef, Arrays.asList(uids));
		Collection<MasterDataVO<UID>> mdUsagesVO =
				masterDataFacade.getMasterData(E.GENERATIONUSAGE, cond2);

		for (MasterDataVO<UID> md : mdUsagesVO) {
			UID genUid = md.getFieldUid(E.GENERATIONUSAGE.generation);
			mpUsages.get(genUid).add(MasterDataWrapper.getGeneratorUsageVO(md));
		}

		return mpUsages;

	}

	private class ExtractIdTransformer<PK> implements Transformer<EntityObjectVO<PK>, PK> {
		@Override

		public PK transform(EntityObjectVO<PK> i) {
			return i.getPrimaryKey();
		}

	}

	private void fillInMissingRefValues(EntityObjectVO<?> eo) {
		EntityMeta<?> eMeta = metaProvider.getEntity(eo.getDalEntity());
		for (FieldMeta<?> fMeta : eMeta.getFields()) {
			if (fMeta.getForeignEntity() != null) {
				Long refId = eo.getFieldId(fMeta.getUID());
				Object refValue = eo.getFieldValue(fMeta.getUID());
				if (refId != null && refValue == null) {
					final IEntityObjectProcessor<Object> eop = nucletDalProvider.getEntityObjectProcessor(fMeta.getForeignEntity());
					final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
					try {
						eop.setIgnoreRecordGrantsAndOthers(true);
						EntityObjectVO<?> refEo = eop.getByPrimaryKey(refId);
						if (refEo != null) {
							try {
								String missingStringifiedValue = RefValueExtractor.get(refEo, fMeta.getForeignEntityField(), null, metaProvider);
								eo.setFieldValue(fMeta.getUID(), missingStringifiedValue);
							} catch (Exception ex) {
								LOG.warn(String.format("fill in missing stringified value failed for entity:%s pk:%s and foreign entity field '%s': %s",
										fMeta.getForeignEntity(), refId, fMeta.getForeignEntityField(), ex.getMessage()), ex);
							}
						} else {
							LOG.warn(String.format("reference %s.%s not found for entity:%s pk:%s",
									eMeta.getEntityName(), fMeta.getFieldName(), fMeta.getForeignEntity(), refId));
						}
					} finally {
						eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
					}
				}
			}
		}

		for (IDependentKey dependentKey : eo.getDependents().getKeySet()) {
			for (EntityObjectVO<?> eoDep : eo.getDependents().getData(dependentKey)) {
				fillInMissingRefValues(eoDep);
			}
		}
	}

}
