//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.Collection;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorUsageVO;

// @Local
public interface GeneratorFacadeLocal {

	/**
	 * get generator usages for specified GeneratorId
	 *
	 * @return Collection&lt;GeneratorUsageVO&gt;
	 * @throws CommonFatalException
	 */
	Map<UID, Collection<GeneratorUsageVO>> getGeneratorUsages(UID...uids)
			throws CommonFatalException;

	GenerationResult generateGenericObject(
			Long iSourceObjectId,
			Long parameterObjectId,
			GeneratorActionVO generatoractionvo,
			String customUsage
	) throws CommonBusinessException;

	<PK> Map<String, Collection<EntityObjectVO<PK>>> groupObjects(
			Collection<PK> sourceIds,
			GeneratorActionVO generatoractionvo
	) throws CommonPermissionException;

	@RolesAllowed("Login")
	<PK> GenerationResult generateGenericObjects(GenerationContext<PK> context) throws CommonBusinessException;

}

