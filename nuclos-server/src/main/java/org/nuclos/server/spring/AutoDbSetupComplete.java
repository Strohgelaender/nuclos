//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.spring;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicReference;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.cluster.ClusterTimerTask;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.job.NuclosSchedulerFactoryBean;
import org.nuclos.server.security.FailDuringStartupAuthenticationProvider;
import org.nuclos.server.security.NuclosAuthenticationProvider;
import org.nuclos.server.validation.DefaultSchemaValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.web.context.WebApplicationContext;

/**
 * In the server, the Spring startup context is minimized to (only) allow AutoDbSetup (and migration).
 * This Spring Bean will be called to trigger the initialization of the 'real' big spring server context.
 * 
 * @see DispatcherApplicationContextInitializer
 * @author Thomas Pasch
 * @since Nuclos 4.0
 */
public class AutoDbSetupComplete implements InitializingBean, ApplicationContextAware {

	private static final Logger LOG = LoggerFactory.getLogger(AutoDbSetupComplete.class);
	
	private static final String[] SERVER_SPRING_BEANS = new String[] {
		"classpath*:META-INF/nuclos/*-beans.xml"
	};
	
	private static class MyWebApplicationContext extends ClassPathXmlApplicationContext implements WebApplicationContext {
		
		private WebApplicationContext parentContext;
		
		private MyWebApplicationContext(String[] configLocations, boolean refresh, ApplicationContext parent)
				throws BeansException {
			super(configLocations, refresh, parent);
			this.parentContext = (WebApplicationContext) parent;
		}

		@Override
		public ServletContext getServletContext() {
			return parentContext.getServletContext();
		}
		
		@Override
		public Resource getResource(String location) {
			return parentContext.getResource(location);
		}
		
		/*
		@Override
		public ClassLoader getClassLoader() {
			return parentContext.getClassLoader();
		}
		*/

	}
	
	private static final AtomicReference<WebApplicationContext> MAIN_CONTEXT = new AtomicReference<WebApplicationContext>();
	
	//

	private long testDelay;
	
	private WebApplicationContext startupContext;
	
	// Spring injection
	
	@Autowired
	Timer timer;
	
	@Autowired
	FailDuringStartupAuthenticationProvider authenticationProvider;
	
	// end of Spring injection

	AutoDbSetupComplete() {
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (testDelay > 0) {
			// test if the 'real' spring context will start
			final TimerTask task = new TimerTask() {
				@Override
				public void run() {
					try {
						afterAutoDbSetup();
					} catch (Exception e) {
						LOG.error("afterAutoDbSetup failed: {}", e, e);
					}
				}
			};
			timer.schedule(task, testDelay);
		}
	}

	public void afterAutoDbSetup() {
		final WebApplicationContext mc = createMainContext();
		
		setupCluster();
		
		final EventSupportFacadeLocal eventSupportFacadeLocal = mc.getBean(EventSupportFacadeLocal.class);
		// create and load BusinessObjects on startup
		try {
			eventSupportFacadeLocal.createBusinessObjects(true);
		} catch(NuclosCompileException e) {
			LOG.warn("Compile errors while creating business objects: {}", e, e);
		} catch (Exception e) {
			Marker fatal = MarkerFactory.getMarker("FATAL");
			LOG.error(fatal, "Failed to create business objects: {}", e, e);
		}
		
		NuclosSchedulerFactoryBean beanScheduler = (NuclosSchedulerFactoryBean) SpringApplicationContextHolder.getBean(NuclosSchedulerFactoryBean.class);
		beanScheduler.startNuclosScheduler();
		
		// set the 'real' authentication provider (tp)
		final NuclosAuthenticationProvider nuclosAuthenticationProvider = mc.getBean(NuclosAuthenticationProvider.class);
		authenticationProvider.setNuclosAuthenticationProvider(nuclosAuthenticationProvider);
		
		// set the 'real' jaxb2Marshaller in SchemaHelper (tp)
		final AnnotationJaxb2Marshaller jaxb2Marshaller = mc.getBean(AnnotationJaxb2Marshaller.class);
		
		final boolean setContext = MAIN_CONTEXT.compareAndSet(null, mc);
		assert setContext;
		synchronized (MAIN_CONTEXT) {
			MAIN_CONTEXT.notify();
		}
		// HACK: get inc count to 2, even on server (tp)
		SpringApplicationContextHolder.setSpringReady();
		
		if (NuclosSystemParameters.is(NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED, false)) {
			try {
				DefaultSchemaValidation.removeSysContraints(NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED);
				DefaultSchemaValidation.validate(true, jaxb2Marshaller, true);
			} catch (Exception e) {
				LOG.error("remove sys constraints before {} failed: {}",
				          NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED, e.getMessage(),
				          e);
			} finally {
				try {
					DefaultSchemaValidation.createSysConstraints(NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED);
				} catch (Exception e) {
					LOG.error("recreate sys constraints after {} failed: {}",
					          NuclosSystemParameters.AUTOSETUP_VALIDATE_SCHEMA_ENABLED, e.getMessage(), e);
					throw new NuclosFatalException("recreate sys constraints after entity remove failed: " + e.getMessage(), e);
				} 
			}
		}
		
		File docUploadDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_UPLOAD_PATH);
		if (docUploadDir != null && docUploadDir.exists() && docUploadDir.isDirectory()) {
			try {
				FileUtils.cleanDirectory(docUploadDir);
			} catch (Exception ex) {
				LOG.error("Error during cleanup of document upload directory: {}", ex.getMessage(), ex);
			}
		}
	}
	
	/*
	 * CLUSTERING
	 * check if Nuclos run in Cluster Mode
	 * if true ? register && ping server && check all other registered Servers : do nothing
	 */
	private void setupCluster() {
		if (NuclosClusterRegisterHelper.runInClusterMode()) {
			NuclosClusterRegisterHelper.registerClusterServer(false);			

			final TimerTask serverPing = new TimerTask() {
				
				@Override
				public void run() {
					NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.SERVERKEEPALIVE_ACTION);
					NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);	
				}
			};
			
			timer.schedule(serverPing, 0, 10 * 1000);
			
			final TimerTask clusterNodeCache = new ClusterTimerTask();
			
			timer.schedule(clusterNodeCache, 1000 * 10, 1000 * 10);
		}
	}
	
	private WebApplicationContext createMainContext() {
		final MyWebApplicationContext result = new MyWebApplicationContext(SERVER_SPRING_BEANS, false, startupContext);
		result.setClassLoader(startupContext.getClassLoader());
		result.refresh();
		return result;
	}

	public long getTestDelay() {
		return testDelay;
	}

	public void setTestDelay(long testDelay) {
		this.testDelay = testDelay;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		startupContext = (WebApplicationContext) applicationContext;
	}
	
	public static WebApplicationContext getMainContext() throws InterruptedException {
		WebApplicationContext result = null;
		do {
			result = MAIN_CONTEXT.get();
			if (result != null) break;
			synchronized (MAIN_CONTEXT) {
				LOG.warn("Spring main context not initialized, waiting ...");
				MAIN_CONTEXT.wait(500);				
			}
		} while (result == null);
		return result;
	}

}
