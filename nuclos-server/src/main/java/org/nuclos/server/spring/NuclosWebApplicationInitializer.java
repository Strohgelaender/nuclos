package org.nuclos.server.spring;

import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.Registration;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.RequestContextFilter;

/**
 * This file replaces the old web.xml.
 * As of the Servlet 3.0 specification it is now possible to initialize WebApps programmatically.
 * <p>
 * See http://support.nuclos.de/browse/NUCLOS-5322
 * <p>
 * Extension developers may extend this class and add mappings to Nuclos Filters and Servlets,
 * or use a completely independent WebApplicationInitializer.
 * A web.xml can also still be used.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NuclosWebApplicationInitializer implements WebApplicationInitializer {

	public final static int DEFAULT_SESSION_TIMEOUT = 60*60*24*7;	// 7 days

	@Override
	public void onStartup(ServletContext container) {
		container.getSessionCookieConfig().setMaxAge(DEFAULT_SESSION_TIMEOUT);
		container.setInitParameter("contextConfigLocation", "classpath*:META-INF/nuclos/nuclos-beans-startup.xml");

		addFilterClientLifecycle(container);
		addFilterSpringSecurity(container);
		addFilterSpringRequestContext(container);

		addServletJnlp(container);
		addServletJarDownload(container);
		addServletRemoting(container);
		addServletJmsBroker(container);
		addServletCxf(container);
		addServletSpringWS(container);
		addServletReports(container);
		addServletRest(container);
		addServletSiDownload(container);
		addServletApiDownload(container);
		addServletAtmosphere(container);
		addServletLauncherDownload(container);
	}

	/**
	 * Returns the filter that matches the given filterName.
	 * If no such filter exists, it is registered first.
	 *
	 * @param container
	 * @param filterName
	 * @param filterClass
	 * @return
	 */
	protected FilterRegistration getFilter(
			final ServletContext container,
			final String filterName,
			final Class<? extends Filter> filterClass
	) {
		FilterRegistration filter = container.getFilterRegistration(filterName);

		if (filter == null) {
			FilterRegistration.Dynamic dynamicFilter = container.addFilter(
					filterName,
					filterClass
			);
			filter = dynamicFilter;
		}

		return filter;
	}

	/**
	 * See {@link #getFilter(ServletContext, String, Class)}
	 * This method additionally sets the "asyncSupported" parameter.
	 *
	 * @param container
	 * @param filterName
	 * @param filterClass
	 * @param asyncSupported
	 * @return
	 */
	protected FilterRegistration getFilter(
			final ServletContext container,
			final String filterName,
			final Class<? extends Filter> filterClass,
			final boolean asyncSupported
	) {
		FilterRegistration filter = getFilter(container, filterName, filterClass);

		if (filter instanceof FilterRegistration.Dynamic) {
			((FilterRegistration.Dynamic) filter).setAsyncSupported(asyncSupported);
		}

		return filter;
	}

	/**
	 * @param container
	 * @return
	 */
	protected FilterRegistration addFilterSpringRequestContext(final ServletContext container) {
		FilterRegistration filter = getFilter(
				container,
				"springRequestContextFilter",
				RequestContextFilter.class,
				true
		);

		filter.addMappingForUrlPatterns(null, true, "/rest/*");

		return filter;
	}

	/**
	 * @param container
	 * @return
	 */
	protected FilterRegistration addFilterSpringSecurity(final ServletContext container) {
		FilterRegistration filter = getFilter(
				container,
				"springSecurityFilterChain",
				org.springframework.web.filter.DelegatingFilterProxy.class,
				true
		);

		filter.addMappingForUrlPatterns(null, true, "/*");

		return filter;
	}

	/**
	 * @param container
	 * @return
	 */
	protected FilterRegistration addFilterClientLifecycle(final ServletContext container) {
		FilterRegistration filter = getFilter(
				container,
				"clientLifecycleFilter",
				org.springframework.web.filter.DelegatingFilterProxy.class
		);

		filter.addMappingForUrlPatterns(null, true, "/app/*");

		return filter;
	}

	/**
	 * Returns the servlet that matches the simple name of the given servlet class.
	 * If no such servlet exists, it is registered first.
	 *
	 * @param container
	 * @param servletName
	 * @param servletClass
	 * @return
	 */
	protected ServletRegistration getServlet(
			final ServletContext container,
			final String servletName,
			final Class<? extends Servlet> servletClass
	) {
		javax.servlet.ServletRegistration servlet = container.getServletRegistration(servletName);

		if (servlet == null) {
			ServletRegistration.Dynamic dynamicServlet = container.addServlet(
					servletName,
					servletClass
			);
			servlet = dynamicServlet;
		}

		return servlet;
	}

	/**
	 * See {@link #getServlet(ServletContext, String, Class)}
	 * This method additionally sets the "loadOnStartup" parameter.
	 *
	 * @param container
	 * @param servletName
	 * @param servletClass
	 * @param loadOnStartup
	 * @return
	 */
	protected ServletRegistration getServlet(
			final ServletContext container,
			final String servletName,
			final Class<? extends Servlet> servletClass,
			final int loadOnStartup
	) {
		javax.servlet.ServletRegistration servlet = getServlet(container, servletName, servletClass);

		if (servlet instanceof ServletRegistration.Dynamic) {
			((ServletRegistration.Dynamic) servlet).setLoadOnStartup(loadOnStartup);
		}

		return servlet;
	}

	/**
	 * See {@link #getServlet(ServletContext, String, Class)}
	 * This method additionally sets the "loadOnStartup" and "asyncSupported" parameters.
	 *
	 * @param container
	 * @param servletName
	 * @param servletClass
	 * @param loadOnStartup
	 * @return
	 */
	protected ServletRegistration getServlet(
			final ServletContext container,
			final String servletName,
			final Class<? extends Servlet> servletClass,
			final int loadOnStartup,
			final boolean asyncSupported
	) {
		javax.servlet.ServletRegistration servlet = getServlet(container, servletName, servletClass, loadOnStartup);

		if (servlet instanceof Registration.Dynamic) {
			((Registration.Dynamic) servlet).setAsyncSupported(asyncSupported);
		}

		return servlet;
	}

	/**
	 * Adds the Atmosphere servlet which handles websocket connections and maps it to "/websocket/*".
	 *
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletAtmosphere(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				org.atmosphere.cpr.AtmosphereServlet.class.getSimpleName(),
				org.atmosphere.cpr.AtmosphereServlet.class,
				10
		);

		servlet.addMapping("/websocket/*");

		return servlet;
	}

	/**
	 * Adds the JarDownloadServlet which handles .jar downloads (Web Start).
	 *
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletJarDownload(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				org.nuclos.server.jnlp.JarDownloadServlet.class.getSimpleName(),
				org.nuclos.server.jnlp.JarDownloadServlet.class
		);

		servlet.addMapping("*.jar");

		return servlet;
	}

	/**
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletApiDownload(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"apidownload",
				org.nuclos.server.web.ApiDownloadServlet.class,
				-8
		);

		servlet.addMapping("/apidownload/*");

		return servlet;
	}
	protected ServletRegistration addServletLauncherDownload(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"launcherdownload",
				org.nuclos.server.web.LauncherDownloadServlet.class,
				10
		);

		servlet.addMapping("/launcherdownload/*");

		return servlet;
	}
	
	

	/**
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletSiDownload(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"sidownload",
				org.nuclos.server.web.SourceItemDownloadServlet.class,
				7
		);

		servlet.addMapping("/sidownload/*");

		return servlet;
	}

	/**
	 * Adds a jersey servlet for the REST service (/rest/*).
	 *
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletRest(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"JAX-RS REST Servlet",
				org.glassfish.jersey.servlet.ServletContainer.class,
				6,
				true
		);

		servlet.setInitParameter("javax.ws.rs.Application", "org.nuclos.server.rest.NuclosRestApplication");
		servlet.addMapping("/rest/*");

		return servlet;
	}

	/**
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletReports(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"reports",
				org.springframework.web.servlet.DispatcherServlet.class,
				-5
		);

		servlet.setInitParameter("contextConfigLocation", "classpath*:META-INF/nuclos/*-reports.xml");
		servlet.setInitParameter(
				"contextInitializerClasses",
				org.nuclos.server.spring.DispatcherApplicationContextInitializer.class.getName()
		);
		servlet.addMapping("/reports/*");

		return servlet;
	}

	/**
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletSpringWS(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"spring-ws",
				org.springframework.ws.transport.http.MessageDispatcherServlet.class,
				-4
		);

		servlet.setInitParameter("contextConfigLocation", "classpath*:META-INF/nuclos/*-ws.xml");
		servlet.setInitParameter("transformWsdlLocations", "true");
		servlet.setInitParameter(
				"contextInitializerClasses",
				org.nuclos.server.spring.DispatcherApplicationContextInitializer.class.getName()
		);
		servlet.addMapping("/springws/*");

		return servlet;
	}

	/**
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletCxf(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"CXFServlet",
				org.apache.cxf.transport.servlet.CXFServlet.class,
				3
		);

		servlet.addMapping("/ws/*");

		return servlet;
	}

	/**
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletJmsBroker(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"jmsbroker",
				org.nuclos.server.web.activemq.NuclosJMSBrokerTunnelServlet.class,
				-2
		);

		servlet.addMapping("/jmsbroker");

		return servlet;
	}

	/**
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletRemoting(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"remoting",
				org.springframework.web.servlet.DispatcherServlet.class,
				-1
		);

		servlet.setInitParameter("contextConfigLocation", "classpath*:META-INF/nuclos/*-remoting.xml");
		servlet.setInitParameter(
				"contextInitializerClasses",
				"org.nuclos.server.spring.DispatcherApplicationContextInitializer"
		);
		servlet.addMapping("/remoting/*");

		return servlet;
	}

	/**
	 * Adds the JnlpServlet which handles .jnlp downloads (Web Start).
	 *
	 * @param container
	 * @return
	 */
	protected ServletRegistration addServletJnlp(final ServletContext container) {
		ServletRegistration servlet = getServlet(
				container,
				"JnlpServlet",
				org.nuclos.server.jnlp.JnlpServlet.class
		);

		servlet.addMapping("*.jnlp");

		return servlet;
	}
}
