//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import static org.nuclos.server.dal.processor.AbstractDalProcessor.DT_BOOLEAN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.nuclos.businessentity.utils.BusinessObjectBuilderForInternalUse;
import org.nuclos.common.E;
import org.nuclos.common.EntityLafParameterVO;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.EOGenericObjectVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.PreferenceVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dal.processor.jdbc.impl.ChartEntityObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DataLanguageMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicEntityObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicTasklistEntityObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EOGenericObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EntityLafParameterProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EntityMetaProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.EntityObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.FieldMetaProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.ImportObjectProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.PreferenceProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.WorkspaceProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityObjectProcessor;
import org.nuclos.server.dal.processor.proxy.impl.ProxyEntityObjectProcessor;
import org.nuclos.server.dal.processor.proxy.impl.WriteProxyEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.query.DbReferencedCompoundColumnExpression;
import org.nuclos.server.fileimport.ImportStructure;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.springframework.beans.factory.annotation.Autowired;

public class ProcessorFactorySingleton {

	private static ProcessorFactorySingleton INSTANCE;
	
	// Spring injection
	
	private TableAliasSingleton tableAliasSingleton;
	
	private SpringDataBaseHelper dataBaseHelper;
	
	private SessionUtils utils;

	private NucletDalProvider nucletDalProvider;
	
	private DataLanguageCache dataLangCache;
	
	private NuclosUserDetailsContextHolder userContext;
	
	
	// end of Spring injection
	
	private ProcessorFactorySingleton() {
		INSTANCE = this;
	}

	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static ProcessorFactorySingleton getInstance() {
		return INSTANCE;
	}
	
	@Autowired
	void setTableAliasSingleton(TableAliasSingleton tableAliasSingleton) {
		this.tableAliasSingleton = tableAliasSingleton;
	}
	
	@Autowired
	void setDataLanguageCache(DataLanguageCache dataLangCache) {
		this.dataLangCache = dataLangCache;
	}
	
	@Autowired
	void setSpringDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}
	
	@Autowired
	void setNuclosUserDetailsContextHolder(NuclosUserDetailsContextHolder userContext) {
		this.userContext = userContext;
	}
	
	@Autowired
	void setSessionUtils(SessionUtils utils) {
		this.utils = utils;
	}
	
	@Autowired
	void setNucletDalProvider(NucletDalProvider nucletDalProvider) {
		this.nucletDalProvider = nucletDalProvider;
	}

	public static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, FieldMeta<S> ef, String methodRadical) {
		try {
			return createBeanMapping(alias, type, ef.getDbColumn(), methodRadical, ef.getUID(), (Class<S>)Class.forName(ef.getDataType()), false, false);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, SF<S> sf, UID entityUID) {
		Class<S> javaClass = sf.getJavaClass();
		return createBeanMapping(alias, type, sf.getDbColumn(), sf.getFieldName(), entityUID == null ? null : sf.getUID(entityUID), javaClass, false, false);
	}

	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, SF<S> sf, UID entityUID, boolean bCaseSensitive) {
		Class<S> javaClass = sf.getJavaClass();
		return createBeanMapping(alias, type, sf.getDbColumn(), sf.getFieldName(), entityUID == null ? null : sf.getUID(entityUID), javaClass, false, bCaseSensitive);
	}

	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, FieldMeta<S> fieldMeta) {
		try {
			return createBeanMapping(alias, type, fieldMeta.getDbColumn(), fieldMeta.getFieldName(), fieldMeta.getUID(), (Class<S>)Class.forName(fieldMeta.getDataType()), false, false);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, String column, String methodRadical, 
			UID fieldUID, Class<S> dataType, boolean isReadonly, boolean bCaseSensitive) {
		final String xetterSuffix = methodRadical.substring(0, 1).toUpperCase() + methodRadical.substring(1);
		
		Class<?> methodParameterType = dataType;
		if ("primaryKey".equals(methodRadical)) {
			methodParameterType = Object.class;
		}
		try {
			return new ColumnToBeanVOMapping<S, PK>(alias, column, fieldUID, type.getMethod("set" + xetterSuffix, methodParameterType),
					type.getMethod((DT_BOOLEAN.equals(dataType) ? "is" : "get") + xetterSuffix), dataType, isReadonly, bCaseSensitive);
		} catch (Exception e) {
			throw new CommonFatalException("On " + type + ": " + e);
		}
	}

	private static <PK> boolean isColumnInList(List<IColumnToVOMapping<?, PK>> list, String columnName) {
		return getColumnFromList(list, columnName) != null;
	}

	private static <PK> IColumnToVOMapping<?, ?> getColumnFromList(List<IColumnToVOMapping<?, PK>> list, String columnName) {
		for (IColumnToVOMapping<?, ?> column : list) {
			if (column.getColumn().equals(columnName)) {
				return column;
			}
		}
		return null;
	}
	
	protected static <S extends Object, PK> IColumnToVOMapping<S, PK> createFieldMapping(String alias, FieldMeta<?> field, boolean bCaseSensitive) {
		return createFieldMapping(alias, field, field.isReadonly(), bCaseSensitive);
	}

	protected static <S extends Object, PK> IColumnToVOMapping<S, PK> createFieldMapping(String alias, FieldMeta<?> field, boolean readonly, boolean bCaseSensitive) {
		try {
			return new ColumnToFieldVOMapping<S, PK>(alias, field, readonly, bCaseSensitive);
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	public static <S extends Object, PK> IColumnToVOMapping<S, PK> createLanguageFieldMapping(String alias, UID langUID, FieldMeta<?> field, boolean readonly, boolean forDataMap) {
		try {
			return new ColumnToLanguageFieldVOMapping<S, PK>(alias, field, langUID, readonly, forDataMap);
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}
	
	protected <S extends Object, PK> IColumnToVOMapping<S, PK> createRefFieldMapping(FieldMeta<S> field) {
		try {
			final String alias = tableAliasSingleton.getAlias(field);
			return new ColumnToRefFieldVOMapping<S, PK>(alias, field);
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	protected static <S extends Object, PK> IColumnToVOMapping<S, PK> createFieldIdMapping(String alias, FieldMeta<?> field, boolean bUidEntity, boolean bCaseSensitive) {
		try {
			return new ColumnToFieldIdVOMapping<S, PK>(alias, field, bUidEntity, bCaseSensitive);
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	public <PK> IEntityObjectProcessor<PK> newEntityObjectProcessor(EntityMeta<?> eMeta, Collection<FieldMeta<?>> colEfMeta, boolean addSystemColumns) {
		final Class<EntityObjectVO<PK>> eov = LangUtils.getGenericClass(EntityObjectVO.class);
		
		if (eMeta.isProxy()) {
			final boolean bForInternalUseOnly = E.isNuclosEntity(eMeta.getUID()) && BusinessObjectBuilderForInternalUse.getEntityMetas().contains(eMeta);
			final IEntityObjectProcessor<PK> result = new ProxyEntityObjectProcessor<PK>(
					eMeta, eov, (Class<PK>) (eMeta.isUidEntity() ?
					(bForInternalUseOnly ? org.nuclos.common.UID.class : org.nuclos.api.UID.class) :
					Long.class));
			return result;
		} else {
			final Class<? extends IDalVO<PK>> type = (Class<? extends IDalVO<PK>>) eov;

			final ProcessorConfiguration<PK> config = newProcessorConfiguration(type, eMeta, colEfMeta, addSystemColumns);

			EntityObjectProcessor<PK> result=null;
			
			if(eMeta.isWriteProxy())
				result= new WriteProxyEntityObjectProcessor<PK>(config);
			else
				result= new EntityObjectProcessor<PK>(config);
			
			// HACK: force spring, as @Autowired on EntityObjectProcessor does not work (tp)
			result.setDataBaseHelper(dataBaseHelper);
			result.setTableAliasSingleton(tableAliasSingleton);
			result.setDataLanguageCache(dataLangCache);
			result.setNuclosUserDetailsContextHolder(userContext);
			result.setSessionUtils(utils);
			
			return result;
		}		
	}

	private <PK> ProcessorConfiguration<PK> newProcessorConfiguration(Class<? extends IDalVO<PK>> type, EntityMeta<?> eMeta, 
			Collection<FieldMeta<?>> colEfMeta, boolean addSystemColumns) {
		final List<IColumnToVOMapping<? extends Object, PK>> allColumns = new ArrayList<IColumnToVOMapping<? extends Object, PK>>();
		final DataSourceCaseSensivity dataSourceCaseSensivity = new DataSourceCaseSensivity(eMeta);

		Set<UID> staticSystemFields = new HashSet<UID>();
		
		IColumnToVOMapping<?, PK> pkColumn = null;
		
		if (!eMeta.isDynamic()) {
			
			if (eMeta.isUidEntity()) {
				pkColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, eMeta.getUID());
			} else {
				pkColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_ID, eMeta.getUID());
			}
			
			allColumns.add(pkColumn);
			
		} else {
			
			if (eMeta.getPkClass().equals(SF.PK_UID.getJavaClass())) {
				boolean bCaseSensitive = dataSourceCaseSensivity.isCaseSensitive(SF.PK_UID.getMetaData(eMeta));
				pkColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, eMeta.getUID(), bCaseSensitive);
				
			} else if (eMeta.getPkClass().equals(SF.PK_ID.getJavaClass())) {
				boolean bCaseSensitive = dataSourceCaseSensivity.isCaseSensitive(SF.PK_ID.getMetaData(eMeta));				
				pkColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_ID, eMeta.getUID(), bCaseSensitive);
				
			} else {
				// FIXME handle this case
			}
			
			if (null != pkColumn) {
				allColumns.add(pkColumn);
			}
			
		}
		final IColumnToVOMapping<Integer, PK> versionColumn;

		if (addSystemColumns) {
			allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, eMeta.getUID()));
			allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, eMeta.getUID()));
			allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, eMeta.getUID()));
			allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, eMeta.getUID()));
			versionColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, eMeta.getUID());
			allColumns.add(versionColumn);
			if (eMeta.isUidEntity()) {
				colEfMeta = new ArrayList<FieldMeta<?>>(colEfMeta);
				colEfMeta.add(SF.IMPORTVERSION.getMetaData(eMeta));
				colEfMeta.add(SF.ORIGINUID.getMetaData(eMeta));
			}
		} else {
			versionColumn = null;
		}
		
		for (IColumnToVOMapping<?, ?> col : allColumns) {
			staticSystemFields.add(col.getUID());
		}

		for (FieldMeta<?> efMeta : colEfMeta) {
			if (efMeta.getDbColumn() == null) {
				continue;
			}
			
			if (staticSystemFields.contains(efMeta.getUID())
					|| efMeta.getDbColumn().equalsIgnoreCase(SF.PK_ID.getDbColumn())) {
				// hier nur dynamische Zuweisungen
				continue;
			}
			
			if (efMeta.isCalcAttributeAllowCustomization()) {
				continue;
			}

			boolean bCaseSensitive = dataSourceCaseSensivity.isCaseSensitive(efMeta);
			
			// normal (non-reference) field
			if (efMeta.getForeignEntity() == null && efMeta.getUnreferencedForeignEntity() == null) {				
				allColumns.add(this.<Object, PK>createFieldMapping(SystemFields.BASE_ALIAS, efMeta, bCaseSensitive));		

			}
			// column is ref to foreign table
			else {
				boolean bUidEntity = false;
				EntityMeta<?> sysMeta = E.getByUID(LangUtils.defaultIfNull(efMeta.getForeignEntity(), efMeta.getUnreferencedForeignEntity()));
				if (sysMeta != null) {
					bUidEntity = sysMeta.isUidEntity();					
				}
				
				// only an primary key ref to foreign table
				if (efMeta.getJavaClass() == Long.class && efMeta.getDbColumn().toUpperCase().startsWith("INTID_")) {
					// kein join nötig!
					if (!isColumnInList(allColumns, efMeta.getDbColumn())) {
						allColumns.add(this.<Object, PK>createFieldIdMapping(SystemFields.BASE_ALIAS, efMeta, bUidEntity, bCaseSensitive));						
					}
				} else if (efMeta.getJavaClass() == UID.class && efMeta.getForeignEntityField() == null && efMeta.getUnreferencedForeignEntityField() == null) { 
					// kein join nötig!
					if (!isColumnInList(allColumns, efMeta.getDbColumn())) {
						allColumns.add(this.<Object, PK>createFieldIdMapping(SystemFields.BASE_ALIAS, efMeta, bUidEntity, bCaseSensitive));						
					}
				}
				// normal case: key ref and 'stringified' ref to foreign table
				else {
					
					// Add foreign key first
					final String dbIdFieldName = DbUtils.getDbIdFieldName(efMeta, bUidEntity);
					if (!isColumnInList(allColumns, dbIdFieldName)) {
						allColumns.add(this.<Object, PK>createFieldIdMapping(SystemFields.BASE_ALIAS, efMeta, bUidEntity, bCaseSensitive));
					}
					// id column is already in allColumns:
					// Replace the id column if the one present is read-only and the current is not read-only
					// This in effect only switched the read-only flag to false.
					else {
						final IColumnToVOMapping<?, ?> col = getColumnFromList(allColumns, dbIdFieldName);
						if (col.isReadonly() && !efMeta.isReadonly()) {
							allColumns.remove(col);
							allColumns.add(this.<Object, PK>createFieldIdMapping(SystemFields.BASE_ALIAS, efMeta, bUidEntity, bCaseSensitive));
						}
					}
					
					// Also add 'stringified' ref to column mapping
					String stringifiedColumn = efMeta.getDbColumn();
					// Adjust column name if stringified value is given
					if (LangUtils.equal(dbIdFieldName, stringifiedColumn) && efMeta.getForeignEntityField() != null) {
						stringifiedColumn = DbReferencedCompoundColumnExpression.getRefAlias(efMeta);
					}
					// In case the ref column does not want an 'stringified' column (= INTID_...)
					// check if column is already in list
					if (!LangUtils.equal(dbIdFieldName, stringifiedColumn) && !isColumnInList(allColumns, stringifiedColumn)) {
						if (eMeta.getReadDelegate() != null) {
							allColumns.add(this.<Object, PK>createFieldMapping(SystemFields.BASE_ALIAS, efMeta, /*readonly=*/true));
						} else {
							allColumns.add(this.<Object, PK>createRefFieldMapping((FieldMeta<Object>)efMeta));
						}
					}
				}
			}
		}

		return new ProcessorConfiguration(type, eMeta, allColumns, pkColumn, versionColumn, addSystemColumns);
	}

	public FieldMetaProcessor newFieldMetaProcessor() {
		final Class<IDalVO<UID>> nfm = LangUtils.getGenericClass(NucletFieldMeta.class);
		final Class<? extends IDalVO<UID>> type = (Class<? extends IDalVO<UID>>) nfm;
		final List<IColumnToVOMapping<? extends Object, UID>> allColumns = new ArrayList<IColumnToVOMapping<? extends Object, UID>>();

		final IColumnToVOMapping<UID, UID> idColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.ENTITYFIELD.getUID());
		allColumns.add(idColumn);
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.ENTITYFIELD.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.ENTITYFIELD.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.ENTITYFIELD.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.ENTITYFIELD.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.ENTITYFIELD.getUID()));
		
		final IColumnToVOMapping<UID, UID> entityIdColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.entity);
		allColumns.add(entityIdColumn);
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.entityfieldgroup, "fieldGroup"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.field, "fieldName"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.dbfield, "dbColumn"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.foreignIntegrationPoint));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.foreignentity, "foreignEntity"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.foreignentityfield, "foreignEntityField"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.lookupentity, "lookupEntity"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.lookupentityfield, "lookupEntityField"));
		
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.searchfield, "searchField"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.datatype, "dataType"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.defaultcomponenttype, "defaultComponentType"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.datascale, "scale"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.dataprecision, "precision"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.formatinput, "formatInput"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.formatoutput, "formatOutput"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.foreigndefault, "defaultForeignId"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.valuedefault, "defaultValue"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.readonly));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.unique));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.nullable));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.indexed));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.searchable));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.modifiable));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.insertable));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.hidden));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.logbooktracking, "logBookTracking"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.showmnemonic, "showMnemonic"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcfunction, "calcFunction"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcAttributeDS));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcAttributeParamValues));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcAttributeAllowCustomization));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calcOndemand));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.sortationasc, "sortorderASC"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.sortationdesc, "sortorderDESC"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.localeresourcel, "localeResourceIdForLabel"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.localeresourced, "localeResourceIdForDescription"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.defaultmandatory, "defaultMandatory"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.ondeletecascade, "onDeleteCascade"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.order));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.calculationscript, "calculationScript"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.backgroundcolorscript, "backgroundColorScript"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.autonumberentity, "autonumberEntity"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.isLocalized, "localized"));
		
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYFIELD.comment, "comment"));
		
		FieldMetaProcessor result = new FieldMetaProcessor(allColumns, entityIdColumn, idColumn);
		result.setSessionUtils(utils);
		return result;
	}

	public DataLanguageMetaDataProcessor newDataLanguageMetaDataProcessor() {
		DataLanguageMetaDataProcessor processor = new DataLanguageMetaDataProcessor();
		processor.setNucletDalProvider(nucletDalProvider);
		return processor;
	}
	
	public EntityMetaProcessor newEntityMetaProcessor() {
		final Class<? extends IDalVO<UID>> type = NucletEntityMeta.class;
		final List<IColumnToVOMapping<? extends Object, UID>> allColumns = new ArrayList<IColumnToVOMapping<? extends Object, UID>>();
		final IColumnToVOMapping<UID, UID> idColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.ENTITY.getUID());

		/*
		 * Attention: The sequence of initialization is important. 
		 * <p>
		 * As a general rule the last 3 items to set are:
		 * <ol>
		 *   <li>virtualEntity</li>
		 *   <li>entity</li>
		 *   <li>dbEntity</li>
		 * </ol>
		 * </p>
		 */
		allColumns.add(idColumn);
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.ENTITY.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.ENTITY.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.ENTITY.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.ENTITY.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.ENTITY.getUID()));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.nuclet));
		
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.systemidprefix, "systemIdPrefix"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.menushortcut, "menuShortcut"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.editable));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.resultdetailssplitview));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.usesstatemodel, "stateModel"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.logbooktracking, "logBookTracking"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.cacheable));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.searchable));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.showSearch));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.thin));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.treerelation, "treeRelation"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.treegroup, "treeGroup"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.importexport, "importExport"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.fieldvalueentity, "fieldValueEntity"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.accelerator));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.acceleratormodifier, "acceleratorModifier"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.fieldsforequality, "fieldsForEquality"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.resource));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.nuclosResource));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcel, "localeResourceIdForLabel"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcem, "localeResourceIdForMenuPath"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourced, "localeResourceIdForDescription"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcetw, "localeResourceIdForTreeView"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.localeresourcett, "localeResourceIdForTreeViewDescription"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.idFactory));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.readDelegate));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.rowcolorscript, "rowColorScript"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.clonegenerator, "cloneGenerator"));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.virtualentity, "virtualEntity"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.entity, "entityName"));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.dbtable, "dbTable"));
		
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.proxy));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.writeproxy));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.generic));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.mandatorLevel));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.mandatorUnique));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.dataLangRefPath));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.dataLanguageDBTable));
		
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.comment));
		
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.lockMode));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.ownerForeignEntityField));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITY.unlockMode));
		
		EntityMetaProcessor result = new EntityMetaProcessor(allColumns, idColumn);
		result.setSessionUtils(utils);
		return result;
	}
	
	public EntityLafParameterProcessor newEntityLafParameterProcessor() {
		final Class<? extends IDalVO<UID>> type = EntityLafParameterVO.class;
		final List<IColumnToVOMapping<? extends Object, UID>> allColumns = new ArrayList<IColumnToVOMapping<? extends Object, UID>>();
		final IColumnToVOMapping<UID, UID> idColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.ENTITYLAFPARAMETER.getUID());

		allColumns.add(idColumn);
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.ENTITYLAFPARAMETER.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.ENTITYLAFPARAMETER.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.ENTITYLAFPARAMETER.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.ENTITYLAFPARAMETER.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.ENTITYLAFPARAMETER.getUID()));

//		allColumns.add(createBeanRefMapping("INTID_T_MD_ENTITY", "T_MD_ENTITY", "ENTITY", JoinType.LEFT, type, "STRENTITY", "STRVALUE_T_MD_ENTITY", "entity", DT_STRING));

		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYLAFPARAMETER.entity));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYLAFPARAMETER.parameter));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.ENTITYLAFPARAMETER.value));
		
		EntityLafParameterProcessor result = new EntityLafParameterProcessor(allColumns, idColumn);
		result.setSessionUtils(utils);
		return result;
	}

	public EOGenericObjectProcessor newEOGenericObjectProcessor() {
		final Class<? extends IDalVO<Long>> type = EOGenericObjectVO.class;
		final List<IColumnToVOMapping<? extends Object, Long>> allColumns = new ArrayList<IColumnToVOMapping<? extends Object, Long>>();
		final IColumnToVOMapping<Long, Long> idColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_ID, E.GENERICOBJECT.getUID());

		allColumns.add(idColumn);
		final IColumnToVOMapping<UID, Long> moduleColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.GENERICOBJECT.module, "entityUID");
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.GENERICOBJECT.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.GENERICOBJECT.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.GENERICOBJECT.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.GENERICOBJECT.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.GENERICOBJECT.getUID()));
		allColumns.add(moduleColumn);

		EOGenericObjectProcessor result = new EOGenericObjectProcessor(allColumns, moduleColumn, idColumn);
		result.setSessionUtils(utils);
		return result;
	}
	
	public PreferenceProcessor newPreferenceProcessor() {
		final Class<? extends IDalVO<UID>> type = PreferenceVO.class;
		final List<IColumnToVOMapping<? extends Object, UID>> allColumns = new ArrayList<IColumnToVOMapping<? extends Object, UID>>();
		final IColumnToVOMapping<UID, UID> idColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.PREFERENCE.getUID());
		allColumns.add(idColumn);
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.PREFERENCE.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.PREFERENCE.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.PREFERENCE.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.PREFERENCE.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.PREFERENCE.getUID()));
		
		final IColumnToVOMapping<String, UID> appColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.app);
		final IColumnToVOMapping<String, UID> typeColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.type);
		final IColumnToVOMapping<UID, UID> userColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.user);
		final IColumnToVOMapping<UID, UID> entityColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.entity);
		final IColumnToVOMapping<UID, UID> layoutColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.layout);
		final IColumnToVOMapping<UID, UID> nucletColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.nuclet);
		final IColumnToVOMapping<UID, UID> sharedColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.sharedPreference);
		final IColumnToVOMapping<Boolean, UID> menuRelevantColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.menuRelevant);
		allColumns.add(appColumn);
		allColumns.add(typeColumn);
		allColumns.add(userColumn);
		allColumns.add(entityColumn);
		allColumns.add(layoutColumn);
		allColumns.add(nucletColumn);
		allColumns.add(sharedColumn);
		allColumns.add(menuRelevantColumn);
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.name));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.PREFERENCE.json));
		PreferenceProcessor result 
			= new PreferenceProcessor(allColumns, idColumn, appColumn, typeColumn, userColumn, entityColumn, layoutColumn, nucletColumn, sharedColumn, menuRelevantColumn);
		result.setSessionUtils(utils);
		return result;
	}

	public WorkspaceProcessor newWorkspaceProcessor() {
		final Class<? extends IDalVO<UID>> type = WorkspaceVO.class;
		final List<IColumnToVOMapping<? extends Object, UID>> allColumns = new ArrayList<IColumnToVOMapping<? extends Object, UID>>();
		final IColumnToVOMapping<UID, UID> idColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, SF.PK_UID, E.WORKSPACE.getUID());
		allColumns.add(idColumn);
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDAT, E.WORKSPACE.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CREATEDBY, E.WORKSPACE.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDAT, E.WORKSPACE.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.CHANGEDBY, E.WORKSPACE.getUID()));
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, SF.VERSION, E.WORKSPACE.getUID()));

		final IColumnToVOMapping<String, UID> nameColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.WORKSPACE.name);
		allColumns.add(nameColumn);
		allColumns.add(createBeanMapping(SystemFields.BASE_ALIAS, type, E.WORKSPACE.clbworkspace));
		final IColumnToVOMapping<UID, UID> userColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.WORKSPACE.user);
		allColumns.add(userColumn);
		final IColumnToVOMapping<UID, UID> assignedColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.WORKSPACE.assignedWorkspace);
		allColumns.add(assignedColumn);
		final IColumnToVOMapping<UID, UID> nucletColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.WORKSPACE.nuclet);
		allColumns.add(nucletColumn);
		final IColumnToVOMapping<Boolean, UID> maintenanceColumn = createBeanMapping(SystemFields.BASE_ALIAS, type, E.WORKSPACE.maintenance);
		allColumns.add(maintenanceColumn);

		final WorkspaceProcessor result = new WorkspaceProcessor(
				allColumns, idColumn, userColumn, nameColumn, nucletColumn, assignedColumn);
		
		// HACK: force spring, as @Autowired on WorkspaceProcessor does not work (tp)
		result.setDataBaseHelper(dataBaseHelper);
		result.setNucletDalProvider(nucletDalProvider);
		result.setSessionUtils(utils);
		
		return result;
	}

	public <PK> DynamicEntityObjectProcessor<PK> newDynamicEntityObjectProcessor(EntityMeta<?> eMeta, Collection<FieldMeta<?>> colEfMeta) {
		final Class<IDalVO<PK>> eov = LangUtils.getGenericClass(EntityObjectVO.class);
		final Class<? extends IDalVO<PK>> type = (Class<? extends IDalVO<PK>>) eov;
		final ProcessorConfiguration<PK> config = newProcessorConfiguration(type, eMeta, colEfMeta, false);
		
		final DynamicEntityObjectProcessor<PK> result = new DynamicEntityObjectProcessor<PK>(config);
		
		// HACK: force spring, as @Autowired on EntityObjectProcessor does not work (tp)
		result.setDataBaseHelper(dataBaseHelper);
		result.setTableAliasSingleton(tableAliasSingleton);
		result.setDataLanguageCache(dataLangCache);
		result.setNuclosUserDetailsContextHolder(userContext);
		result.setSessionUtils(utils);
		
		return result;
	}

	public <PK> ChartEntityObjectProcessor<PK> newChartEntityObjectProcessor(EntityMeta<?> eMeta, Collection<FieldMeta<?>> colEfMeta) {
		final Class<IDalVO<PK>> eov = LangUtils.getGenericClass(EntityObjectVO.class);
		final Class<? extends IDalVO<PK>> type = (Class<? extends IDalVO<PK>>) eov;
		final ProcessorConfiguration<PK> config = newProcessorConfiguration(type, eMeta, colEfMeta, false);
		
		final ChartEntityObjectProcessor<PK> result = new ChartEntityObjectProcessor<PK>(config);
		
		// HACK: force spring, as @Autowired on EntityObjectProcessor does not work (tp)
		result.setDataBaseHelper(dataBaseHelper);
		result.setTableAliasSingleton(tableAliasSingleton);
		result.setDataLanguageCache(dataLangCache);
		result.setNuclosUserDetailsContextHolder(userContext);
		result.setSessionUtils(utils);
		
		return result;
	}

	public <PK> DynamicTasklistEntityObjectProcessor<PK> newDynamicTasklistEntityObjectProcessor(EntityMeta<?> eMeta, Collection<FieldMeta<?>> colEfMeta) {
		final Class<IDalVO<PK>> eov = LangUtils.getGenericClass(EntityObjectVO.class);
		final Class<? extends IDalVO<PK>> type = (Class<? extends IDalVO<PK>>) eov;
		final ProcessorConfiguration<PK> config = newProcessorConfiguration(type, eMeta, colEfMeta, false);

		final DynamicTasklistEntityObjectProcessor<PK> result = new DynamicTasklistEntityObjectProcessor<PK>(config);

		// HACK: force spring, as @Autowired on EntityObjectProcessor does not work (tp)
		result.setDataBaseHelper(dataBaseHelper);
		result.setTableAliasSingleton(tableAliasSingleton);
		result.setDataLanguageCache(dataLangCache);
		result.setNuclosUserDetailsContextHolder(userContext);
		result.setSessionUtils(utils);

		return result;
	}

	public <PK> ImportObjectProcessor<PK> newImportObjectProcessor(
			EntityMeta<? extends Object> eMeta, Collection<FieldMeta<?>> colEfMeta, ImportStructure structure) {
		
		final Class<IDalVO<PK>> eov = LangUtils.getGenericClass(EntityObjectVO.class);
		final Class<? extends IDalVO<PK>> type = (Class<? extends IDalVO<PK>>) eov;
		final ProcessorConfiguration<PK> config = newProcessorConfiguration(type, eMeta, colEfMeta, true);
		final ImportObjectProcessor<PK> result = new ImportObjectProcessor<PK>(config, structure);
		
		// HACK: force spring, as @Autowired on EntityObjectProcessor does not work (tp)
		result.setDataBaseHelper(dataBaseHelper);
		result.setTableAliasSingleton(tableAliasSingleton);
		result.setDataLanguageCache(dataLangCache);
		result.setNuclosUserDetailsContextHolder(userContext);
		result.setSessionUtils(utils);
		
		return result;
	}

	public <PK> void addToColumns(JdbcEntityObjectProcessor<PK> processor, FieldMeta<?> field) {
		final IColumnToVOMapping<?, PK> mapping;
		final IMetaProvider mdProv = MetaProvider.getInstance();
		final EntityMeta<?> mdEnitiy = mdProv.getEntity(field.getEntity());

		final String alias;
		if (mdEnitiy.equals(processor.getMetaData())) {
			alias = SystemFields.BASE_ALIAS;
		}
		else {
			alias = mdEnitiy.getEntityName();
		}

		mapping = createFieldMapping(alias, field, false);
		processor.addToColumns(mapping);
	}

}
