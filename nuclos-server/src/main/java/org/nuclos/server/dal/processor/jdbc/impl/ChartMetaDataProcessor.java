//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.transform.Source;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.util.DalTransformations;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.specification.IDalReadSpecification;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.datasource.DatasourceMetaVO.ColumnMeta;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class ChartMetaDataProcessor implements IDalReadSpecification<NucletEntityMeta, UID> {

	private static final Logger LOG = LoggerFactory.getLogger(ChartMetaDataProcessor.class);
	
	private static ChartMetaDataProcessor INSTANCE;
	
	// Spring injection
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private AnnotationJaxb2Marshaller jaxb2Marshaller;
	
	// end of Spring injection

	ChartMetaDataProcessor() {
		INSTANCE = this;
	}
	
	public static ChartMetaDataProcessor getInstance() {
		if (INSTANCE.dataBaseHelper == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	@Override
    public List<NucletEntityMeta> getAll() {
	    return getChartEntities();
    }
	
	@Override
	public NucletEntityMeta getByPrimaryKey(final UID uid, Collection<FieldMeta<?>> fields) {
			return getByPrimaryKey(uid);
    }

	@Override
	public NucletEntityMeta getByPrimaryKey(final UID uid) {
		return CollectionUtils.findFirst(getAll(), (EntityMeta<?> t) -> uid.equals(t.getUID()));
	}

	@Override
	public List<NucletEntityMeta> getByPrimaryKeys(final List<UID> uids) {
		return CollectionUtils.applyFilter(getAll(), (NucletEntityMeta t) -> uids.contains(t.getUID()));
	}

	@Override
	public List<UID> getAllIds() {
		return CollectionUtils.transform(getAll(), DalTransformations.getId(UID.class));
	}

	public List<NucletEntityMeta> getChartEntities() {
		ArrayList<NucletEntityMeta> res = new ArrayList<>();
		
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.CHART);
		query.multiselect(
				t.baseColumn(E.CHART.getPk()), 
				t.baseColumn(E.CHART.name),
				t.baseColumn(E.CHART.meta),
				t.baseColumn(E.CHART.query),
				t.baseColumn(E.CHART.nuclet),
				t.baseColumn(E.CHART.detailsEntity),
				t.baseColumn(E.CHART.parentEntity)
				);

		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
			final UID uid = tuple.get(E.CHART.getPk());
			final String name = tuple.get(E.CHART.name);
			final String sMetaXML = tuple.get(E.CHART.meta);
			final Source xmlSource = SourceResultHelper.newSource(sMetaXML);
			if (xmlSource != null) {
				xmlSource.setSystemId("E.CHART.meta");
			}
			final String sQuery = tuple.get(E.CHART.query);
			final UID nucletUID = tuple.get(E.CHART.nuclet);
			final UID detailsEntityUID = tuple.get(E.CHART.detailsEntity);
			final UID parentEntityUID = tuple.get(E.CHART.parentEntity);
			final DatasourceMetaVO dsmeta;
			try {
				dsmeta = (DatasourceMetaVO) jaxb2Marshaller.unmarshal(xmlSource);
			} catch (OutOfMemoryError e) {
				LOG.error("JAXB unmarshal failed: meta= {} xml is:\n{}", sMetaXML, xmlSource, e);
				throw e;
			}
			if (dsmeta != null) {
				try {
					NucletEntityMeta emeta = new NucletEntityMeta();
					emeta.setNuclet(nucletUID);
					emeta.setPrimaryKey(dsmeta.getEntityUID());
					emeta.setUidEntity(false);
					final String entityName = name + "CRT";
					emeta.setSearchable(true);
					emeta.setEditable(false);
					emeta.setCacheable(false);
					emeta.setImportExport(false);
					emeta.setStateModel(false);
					emeta.setLogBookTracking(false);
					emeta.setTreeGroup(false);
					emeta.setTreeRelation(false);
					emeta.setFieldValueEntity(false);
					emeta.setDynamic(false);
					emeta.setChart(true);
					emeta.setEntityName(entityName);
					emeta.setDbTable(sQuery); //emeta.setDbTable("("+ sQuery +") as crtds"); // AS will be added by EntityMeta.getDbSelect(). -- /* Oracle don't like it " AS " + */
					emeta.setDetailEntity(detailsEntityUID);
					emeta.setParentEntity(parentEntityUID);
					
					Collection<FieldMeta<?>> fields = new ArrayList<>();
					if (dsmeta.getColumns() != null) {
						for (ColumnMeta colmeta : dsmeta.getColumns()) {
							NucletFieldMeta<?> fmeta = DalUtils.getFieldMeta(colmeta);
							fmeta.setEntity(dsmeta.getEntityUID());
							if(DataSourceCaseSensivity.PRIMARY_KEY.equals(colmeta.getColumnName().toUpperCase())) {
								fmeta.setDataType("java.lang.Long");
							}
							if(DataSourceCaseSensivity.REF_ENTITY.equals(colmeta.getColumnName().toUpperCase())) {
								fmeta.setDbColumn(DataSourceCaseSensivity.REF_ENTITY);
								fmeta.setDataType("java.lang.Long");
								// NUCLOS-9: field must be reference field (i.e. foreign entity must be set!)
								if (parentEntityUID != null) {
									fmeta.setForeignEntity(parentEntityUID);
								} else {
									fmeta.setForeignEntity(E.GENERICOBJECT.getUID());
								}
								fmeta.setScale(255);
								fmeta.setPrecision(null);
								if (new DataSourceCaseSensivity(sQuery).isIntidGenericObjectCaseInsensitive()) {
									fmeta.setDynamic(false);
								} else {
									// this is a special column so we do not mark it as dynamic (esp. it's not case-sensitive)
									// (Note: if you need to distinguish it, use the entity's dynamic flag)
									fmeta.setDynamic(true);
								}
							}
							else {
								fmeta.setDynamic(true);
							}
							fmeta.setFallbackLabel(fmeta.getFieldName());
							fmeta.setNullable(true);
							fmeta.setSearchable(true);
							fmeta.setUnique(false);
							fmeta.setIndexed(false);
							fmeta.setLogBookTracking(false);
							fmeta.setInsertable(false);
							fmeta.setReadonly(true);
							
							fields.add(fmeta);
						}
					}
					
					emeta.setFields(fields);
		
					res.add(emeta);
				} catch (Exception ex) {
					LOG.error("Unable to init dynamic entity \"{}\" uid={}: {}",
					          name, uid, ex.getMessage(), ex);
				}
			}
		}
	
		return res;
	}

}
