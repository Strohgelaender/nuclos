package org.nuclos.server.dal.processor.jdbc;

import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalWithFieldsVO;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.springframework.beans.factory.annotation.Configurable;

public abstract class AbstractJdbcWithFieldsDalProcessor<DalVO extends IDalWithFieldsVO<?, PK>, PK> extends AbstractJdbcDalProcessor<DalVO, PK> {

	protected AbstractJdbcWithFieldsDalProcessor(UID entity, Class<DalVO> type, Class<PK> pkType, List<IColumnToVOMapping<?, PK>> allColumns) {
		super(entity, type, pkType, allColumns);
	}

}
