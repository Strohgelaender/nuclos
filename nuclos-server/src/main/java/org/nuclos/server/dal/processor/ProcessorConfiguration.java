//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.util.List;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.dal.vo.IDalVO;

public class ProcessorConfiguration<PK> {
	
	private final Class<? extends IDalVO<PK>> type;

	private final EntityMeta<PK> eMeta;
	
	private final List<IColumnToVOMapping<? extends Object, PK>> allColumns;
	
	private final IColumnToVOMapping<PK, PK> pkColumn;
	
	private final IColumnToVOMapping<Integer, PK> versionColumn;
	
	private final boolean addSystemColumns;
	
	public ProcessorConfiguration(Class<? extends IDalVO<PK>> type, EntityMeta<PK> eMeta, List<IColumnToVOMapping<? extends Object, PK>> allColumns, 
			IColumnToVOMapping<PK, PK> pkColumn, IColumnToVOMapping<Integer, PK> versionColumn, boolean addSystemColumns) {
		
		this.type = type;
		this.eMeta = eMeta;
		this.allColumns = allColumns;
		this.pkColumn = pkColumn;
		this.versionColumn = versionColumn;
		this.addSystemColumns = addSystemColumns;
	}
	
	public Class<? extends IDalVO<PK>> getDalType() {
		return type;
	}

	public EntityMeta<PK> getMetaData() {
		return eMeta;
	}

	public List<IColumnToVOMapping<? extends Object, PK>> getAllColumns() {
		return allColumns;
	}

	public IColumnToVOMapping<PK, PK> getPkColumn() {
		return pkColumn;
	}

	public IColumnToVOMapping<Integer, PK> getVersionColumn() {
		return versionColumn;
	}

	public boolean isAddSystemColumns() {
		return addSystemColumns;
	}
	
}
