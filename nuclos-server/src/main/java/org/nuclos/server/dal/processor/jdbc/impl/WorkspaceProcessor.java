//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosLogicalUniqueViolationException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.preferences.WorkspaceManager;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.CommonPreferencesFacade;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.IWorkspaceProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

@Configurable
public class WorkspaceProcessor extends AbstractJdbcDalProcessor<WorkspaceVO, UID> implements IWorkspaceProcessor {
	
	// Spring injection
	
	private NucletDalProvider nucletDalProvider;
	
	private SpringLocaleDelegate localeDelegate;
	
	// end of Spring injection
	
	private final IColumnToVOMapping<UID, UID> idColumn; 
	private final IColumnToVOMapping<UID, UID> userColumn;
	private final IColumnToVOMapping<String, UID> nameColumn;
	private final IColumnToVOMapping<UID, UID> nucletColumn;
	private final IColumnToVOMapping<UID, UID> assignedColumn;
	
	private final List<IColumnToVOMapping<?, UID>> logicalUniqueConstraintCombinations;

	private final List<IColumnToVOMapping<?, UID>> updateCheckColumnList;
	
	public WorkspaceProcessor(List<IColumnToVOMapping<?, UID>> allColumns, IColumnToVOMapping<UID, UID> idColumn,
				IColumnToVOMapping<UID, UID> userColumn, IColumnToVOMapping<String, UID> nameColumn, IColumnToVOMapping<UID, UID> nucletColumn, IColumnToVOMapping<UID, UID> assignedColumn) {
		super(E.WORKSPACE.getUID(), WorkspaceVO.class, UID.class, allColumns);
		
		this.idColumn = idColumn;
		this.userColumn = userColumn;
		this.nameColumn = nameColumn;
		this.assignedColumn = assignedColumn;
		this.nucletColumn = nucletColumn;
		
		updateCheckColumnList = new ArrayList<>();
		updateCheckColumnList.add(idColumn);
		
		logicalUniqueConstraintCombinations = new ArrayList<>();
		logicalUniqueConstraintCombinations.add(this.userColumn);
		logicalUniqueConstraintCombinations.add(this.nameColumn);
		logicalUniqueConstraintCombinations.add(this.assignedColumn);
		logicalUniqueConstraintCombinations.add(this.nucletColumn);
	}
	
	// @Autowired
	public void setNucletDalProvider(NucletDalProvider nucletDalProvider) {
		this.nucletDalProvider = nucletDalProvider;
	}

	@Override
	public List<UID> getAllIds() {
		return cloneUIDs(_getAllIds());
	}

	@Cacheable(value="workspacePrimaryKeys")
	private List<UID> _getAllIds() {
		return super.getAllIds();
	}
	
	@Override
	public WorkspaceVO getByPrimaryKey(UID id) {
		return clone(_getByPrimaryKey(id));
	}
	
	@Cacheable(value="workspaceByPrimaryKey", key="#p0")
	private WorkspaceVO _getByPrimaryKey(UID id) {
		return super.getByPrimaryKey(id);
	}
	
	private List<WorkspaceVO> getByUserId(UID userUID) {
		return clone(_getByUserId(userUID));
	}

	//TODO: There is the educated guess that it is faster to iterate through allIDs() and getByPrimaryKey. To be checked.
	@Cacheable(value="workspaceByUserId", key="#p0")
	private List<WorkspaceVO> _getByUserId(UID userUID) {
		final DbQuery<Object[]> query = createQuery(allColumns, true, null);
		final DbFrom<UID> from = (DbFrom<UID>) CollectionUtils.getSingleIfExist(query.getRoots());
		final DbQueryBuilder builder = query.getBuilder();
		if (UID.UID_NULL.equals(userUID)) {
			query.where(builder.isNull(from.baseColumn(userColumn)));
		} else {
			query.where(builder.equalValue(from.baseColumn(userColumn), userUID));
		}
		
		return dataBaseHelper.getDbAccess().executeQuery(query, createResultTransformer(allColumns));
	}
	
	private List<EntityObjectVO<UID>> getAssignedRolesForRole(UID roleUid) {
		return new ArrayList<EntityObjectVO<UID>>(_getAssignedRolesForRole(roleUid));
	}
	
	@Cacheable(value="roleWorkspaceByRoleUid", key="#p0")
	private List<EntityObjectVO<UID>> _getAssignedRolesForRole(UID roleUid) {
		return getAssignedRoles(E.ROLEWORKSPACE.role, roleUid);
	}
	
	private List<EntityObjectVO<UID>> getAssignedRolesForWorkspace(UID workspaceUid) {
		return new ArrayList<EntityObjectVO<UID>>(_getAssignedRolesForWorkspace(workspaceUid));
	}
	
	@Cacheable(value="roleWorkspaceByWorkspaceUid", key="#p0")
	private List<EntityObjectVO<UID>> _getAssignedRolesForWorkspace(UID workspaceUid) {
		return getAssignedRoles(E.ROLEWORKSPACE.workspace, workspaceUid);
	}
	
	private List<EntityObjectVO<UID>> getAssignedRoles(FieldMeta<?> field, UID foreignkey) {
		IEntityObjectProcessor<UID> rowoProc = nucletDalProvider.getEntityObjectProcessor(E.ROLEWORKSPACE);

		final List<EntityObjectVO<UID>> assignedRoles = rowoProc.getBySearchExpression(new CollectableSearchExpression(
				SearchConditionUtils.newUidComparison(
						field,
						ComparisonOperator.EQUAL,
						foreignkey)));
		
		return assignedRoles;		
	}
	
	@CacheEvict(value="workspacePrimaryKeys", allEntries=true)
	private void insert(WorkspaceVO dalVO) {
	}

	private void update(WorkspaceVO dalVO) {
		if (getByPrimaryKey(updateCheckColumnList, dalVO.getPrimaryKey()) == null) {
			throw new DbException("Workspace.not.found");
		}
	}

	@Override
	@Caching(evict= {
			@CacheEvict(value="workspaceByPrimaryKey", key="#p0.getPrimaryKey()"),
			@CacheEvict(value="workspaceByUserId", key="#p0.getUser()", condition="#p0.getUser() != null"),
			@CacheEvict(value="defaultWorkspaceByUser", allEntries=true)
	})
	public Object insertOrUpdate(WorkspaceVO dalVO) {
		if (dalVO.isFlagUpdated()) {
			update(dalVO);
		} else {
			insert(dalVO);
		}
		Object res=super.insertOrUpdate(dalVO);
		checkLogicalUniqueConstraint(dalVO);
		return res;
	}

	@Override
	@Caching(evict= {
			@CacheEvict(value="workspacePrimaryKeys", allEntries=true),
			@CacheEvict(value="workspaceByPrimaryKey", key="#p0.getPrimaryKey()"),
			@CacheEvict(value="workspaceByUserId", allEntries=true),
			@CacheEvict(value="defaultWorkspaceByUser", allEntries=true)			
	})
	public void delete(Delete<UID> id) throws DbException {
		super.delete(id);
	}
	
	@Override
	public List<WorkspaceVO> getAll() {
		return super.getAll();
	}
	
	@Override
	public List<WorkspaceVO> getByPrimaryKeys(List<UID> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

	@Override
	public EntityMeta<UID> getMetaData() {
		return E.WORKSPACE;
	}

	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return idColumn;
	}
	
	private void deleteByAssigned(UID assignedId) throws DbException {
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		final DbFrom<UID> from = query.from(getMetaData());
		final DbJoin<UID> join = from.innerJoin(E.USER, "t2").on(E.WORKSPACE.user, E.USER.getPk());
		
		query.multiselect(
				from.baseColumn(idColumn),
				join.baseColumn(E.USER.name));
		
		final DbQuery<UID> rousQuery = builder.createQuery(UID.class);
		final DbFrom<UID> rousFrom = rousQuery.from(E.ROLEUSER, "ROUS");
		rousQuery.select(rousFrom.baseColumn(E.ROLEUSER.role));
		rousQuery.where(builder.equal(rousFrom.baseColumn(E.ROLEUSER.user), from.baseColumn(userColumn)));
		
		final DbQuery<UID> rowoQuery = builder.createQuery(UID.class);
		final DbFrom<UID> rowoFrom = rowoQuery.from(E.ROLEWORKSPACE, "ROWO");
		rowoQuery.select(rowoFrom.baseColumn(E.ROLEWORKSPACE.workspace));
		rowoQuery.where(rowoFrom.baseColumn(E.ROLEWORKSPACE.role).in(rousQuery));
		
		query.where(builder.equalValue(from.baseColumn(assignedColumn), assignedId));
		query.addToWhereAsAnd(builder.not(from.baseColumn(assignedColumn).in(rowoQuery)));
		
		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
			UID id = (UID) tuple.get(0);
			String user = (String) tuple.get(1);
			if (!SecurityCache.getInstance().getAllowedActions(user, null).contains(Actions.ACTION_WORKSPACE_ASSIGN)) {
				delete(new Delete<UID>(id));
			}
		}
	}
	
	@Override
	public void deleteAllByAssigned(UID assignedId) throws DbException {
		dataBaseHelper.getDbAccess().execute(DbStatementUtils.deleteFrom(E.WORKSPACE, assignedColumn, assignedId));
	}

	@Override
	public List<WorkspaceVO> getByUser(String user) {
		final UID userUID = SecurityCache.getInstance().getUserUid(user);
		return getByUserId(userUID);
	}
	
	@Override
	public List<WorkspaceVO> getByAssignedByRole(String user) {
		List<WorkspaceVO> lstRet = new ArrayList<>();
		
		Set<UID> roles = SecurityCache.getInstance().getUserRoles(user, null);
		for (UID role : roles) {
			List<EntityObjectVO<UID>> lstGranted = getAssignedRolesForRole(role);
			for (EntityObjectVO<UID> granted : lstGranted) {
				WorkspaceVO wvo = getByPrimaryKey(granted.getFieldUid(E.ROLEWORKSPACE.workspace));
				if (wvo != null && !lstRet.contains(wvo)) {
					lstRet.add(wvo);					
				}
			}
		}
		
		return lstRet;
	}
	
	@Override
	public List<WorkspaceVO> getNewAssigned(String user) {
		List<WorkspaceVO> lstRet = new ArrayList<>();
		List<WorkspaceVO> lstWorkspaceUsers = getByUser(user);

		boolean assigner = SecurityCache.getInstance().getAllowedActions(user, null).contains(Actions.ACTION_WORKSPACE_ASSIGN);
		if (assigner) {
			// all assignable workspaces:
			List<WorkspaceVO> lst = getByUserId(UID.UID_NULL);
			for (WorkspaceVO wvo : lst) {
				boolean alreadyAssigned = false;
				for (WorkspaceVO wvoUsers : lstWorkspaceUsers) {
					if (wvo.getPrimaryKey().equals(wvoUsers.getAssignedWorkspace())) {
						alreadyAssigned = true;
						break;
					}
				}
				if (!alreadyAssigned && !lstRet.contains(wvo)) {
					lstRet.add(wvo);					
				}				
			}
		} else {
			Set<UID> roles = SecurityCache.getInstance().getUserRoles(user, null);
			for (UID role : roles) {
				List<EntityObjectVO<UID>> lstGranted = getAssignedRolesForRole(role);
				for (EntityObjectVO<UID> granted : lstGranted) {
					WorkspaceVO wvo = getRelevantWorkspaceForUid(granted.getFieldUid(E.ROLEWORKSPACE.workspace), lstWorkspaceUsers);
					if (wvo != null && !lstRet.contains(wvo)) {
						lstRet.add(wvo);					
					}
				}
			}
		}
		
		return lstRet;	
	}

	private WorkspaceVO getRelevantWorkspaceForUid(UID workspaceUID, List<WorkspaceVO> lstWorkspaceUsers) {
		for (WorkspaceVO wvoUsers : lstWorkspaceUsers) {
			if (workspaceUID.equals(wvoUsers.getAssignedWorkspace())) {
				return null;
			}
		}
		WorkspaceVO wvo = getByPrimaryKey(workspaceUID);
		if (wvo != null && wvo.getUser() == null) {
			return wvo;
		}
		return null;
	}
	
	private void checkLogicalUniqueConstraint(WorkspaceVO wovo) throws DbException {
		final Map<IColumnToVOMapping<?, UID>, Object> checkValues = super.getColumnValuesMapWithMapping(logicalUniqueConstraintCombinations, wovo);
		final NuclosLogicalUniqueViolationException checkResult = super.checkLogicalUniqueConstraint(checkValues, wovo.getPrimaryKey());
		if (checkResult != null)
			throw new DbException("Workspace.name.in.use");
	}
	
	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<UID> dalVO) throws DbException {
		throw new NotImplementedException();
	}
	
	@Override
	public void modifyWorkspaceAssignments(Collection<UID> roleUids, final UID assignedWorkspaceUid, String user) {
		IEntityObjectProcessor<UID> rowoProc = nucletDalProvider.getEntityObjectProcessor(E.ROLEWORKSPACE);
		
		final List<EntityObjectVO<UID>> grantedRoles = getAssignedRolesForWorkspace(assignedWorkspaceUid);

		// remove assignments
		for (final EntityObjectVO<UID> grantedRole : grantedRoles) {
			UID grantedRoleUID = grantedRole.getFieldUid(E.ROLEWORKSPACE.role);
			if (!roleUids.contains(grantedRoleUID)) {
				rowoProc.delete(new Delete<>(grantedRole.getPrimaryKey()));
				invalideRoleWorkspaceCaches(grantedRoleUID, grantedRole.getFieldUid(E.ROLEWORKSPACE.workspace));

				// remove customized workspaces
				deleteByAssigned(assignedWorkspaceUid);
			}
		}


		// add new assignments
		for (final UID roleUid : roleUids) {
			boolean alreadyGranted = false;
			if (grantedRoles != null) {
				for (EntityObjectVO<UID> grantedRole : grantedRoles) {
					if (roleUid.equals(grantedRole.getFieldUid(E.ROLEWORKSPACE.role))) {
						alreadyGranted = true;
					}
				}
			}

			if (!alreadyGranted) {
				// assign role
				
				EntityObjectVO<UID> newGrant = new EntityObjectVO<>(E.ROLEWORKSPACE);
				newGrant.flagNew();
				newGrant.setPrimaryKey(new UID());
				DalUtils.updateVersionInformation(newGrant, user);
				newGrant.setFieldUid(E.ROLEWORKSPACE.role, roleUid);
				newGrant.setFieldUid(E.ROLEWORKSPACE.workspace, assignedWorkspaceUid);
				rowoProc.insertOrUpdate(newGrant);
				invalideRoleWorkspaceCaches(roleUid, assignedWorkspaceUid);
			}
		}
	}
	
	@Cacheable(value="defaultWorkspaceByUser", key="#p0")
	@Override
	public WorkspaceVO getDefaultWorkspace(String user, CommonPreferencesFacade preferencesFacade) throws CommonBusinessException {
		WorkspaceManager wm = new WorkspaceManager(localeDelegate);
		return wm.initAndGetWorkspace(preferencesFacade);
	}

	@Caching(evict= {
			@CacheEvict(value="roleWorkspaceByRoleUid", key="#p0"),
			@CacheEvict(value="roleWorkspaceByWorkspaceUid", key="#p1")
	})
	private void invalideRoleWorkspaceCaches(UID role, UID workspace) {
	}

	@Override
	public Collection<UID> getAssignedRoleIds(UID assignedWorkspaceUid) {
		if (assignedWorkspaceUid == null) {
			return new ArrayList<UID>();
		}

		List<EntityObjectVO<UID>> assignedRoles = getAssignedRolesForWorkspace(assignedWorkspaceUid);
		
		return CollectionUtils.transform(assignedRoles, (EntityObjectVO<UID> eovo) -> eovo.getFieldUid(E.ROLEWORKSPACE.role));
	}
	
	@Override
	@Caching(evict= {
			@CacheEvict(value="workspacePrimaryKeys", allEntries=true),
			@CacheEvict(value="workspaceByPrimaryKey", allEntries=true),
			@CacheEvict(value="workspaceByUserId", allEntries=true),
			@CacheEvict(value="defaultWorkspaceByUser", allEntries=true),
			@CacheEvict(value="roleWorkspaceByRoleUid", allEntries=true),
			@CacheEvict(value="roleWorkspaceByWorkspaceUid", allEntries=true)
	})
	public void invalidateWorkspaceCaches() {
	}
	
	private static WorkspaceVO clone(WorkspaceVO woToClone) {
		if (woToClone == null) {
			return null;
		}
		return woToClone.clone();
	}
	
	private static List<WorkspaceVO> clone(List<WorkspaceVO> lstToClone) {
		if (lstToClone == null) {
			return null;
		}
		List<WorkspaceVO> result = new ArrayList<>(lstToClone.size());
		for (WorkspaceVO vo : lstToClone) {
			result.add(vo.clone());
		}
		return result;
	}
	
	private static List<UID> cloneUIDs(List<UID> lstToClone) {
		if (lstToClone == null) {
			return null;
		}
		return new ArrayList<>(lstToClone);
	}
}
