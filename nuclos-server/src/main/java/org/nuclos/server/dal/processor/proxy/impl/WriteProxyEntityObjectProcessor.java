package org.nuclos.server.dal.processor.proxy.impl;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.dal.processor.ProcessorConfiguration;
import org.nuclos.server.dal.processor.jdbc.impl.EntityObjectProcessor;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.eventsupport.ejb3.ProxyContext;
import org.nuclos.server.eventsupport.ejb3.ProxyContext.Type;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 *
 */
public class WriteProxyEntityObjectProcessor<PK> extends EntityObjectProcessor<PK> {
	private static final Logger LOG = LoggerFactory.getLogger(ProxyEntityObjectProcessor.class);

	private boolean ignoreRecordGrantsAndOthers = false;

	private boolean thinReadEnabled = false;

	private EventSupportFacadeLocal _evsuFacade;

	public WriteProxyEntityObjectProcessor(ProcessorConfiguration<PK> config) {
		super(config);
	}

	public EventSupportFacadeLocal getEventSupportFacade() {
		if (_evsuFacade == null) {
			_evsuFacade = SpringApplicationContextHolder.getBean(EventSupportFacadeLocal.class);
		}
		return _evsuFacade;
	}

	/**
	 * 
	 * @param pc
	 * @return proxy object (for rollback)
	 */
	private Object executeCall(final ProxyContext pc) {
		try {
			return getEventSupportFacade().executeProxyCall(pc);
		} catch (NuclosBusinessRuleException e) {
			String error = String.format("Proxy getter for entity %s not accessible: %s", getEntityUID(), e.getCause() != null ? e.getCause().toString() : e.getMessage());
			LOG.error(error, e);
			throw new NuclosFatalException(error, e);
		} catch (NuclosCompileException e) {
			String error = String.format("Proxy getter for entity %s not accessible: %s", getEntityUID(), e.getCause() != null ? e.getCause().toString() : e.getMessage());
			LOG.error(error, e);
			throw new NuclosFatalException(error, e);
		}
	}

	@Override
	public Object insertOrUpdate(EntityObjectVO<PK> dalVO)  throws DbException{
		final ProxyContext pc = new ProxyContext(getEntityUID(), getPkType(), Type.INSERT_OR_UPDATE);
		pc.setParamObject(dalVO);
		Object proxy = executeCall(pc);
		registerTransactionSynchronization(getEntityUID(), proxy);
		return pc.getPKResult();
	}

	@Override
	public void delete(Delete del) throws DbException {
		final ProxyContext pc = new ProxyContext(getEntityUID(), getPkType(), Type.DELETE);
		pc.setParamId(del.getPrimaryKey());
		Object proxy = executeCall(pc);
		registerTransactionSynchronization(getEntityUID(), proxy);
	}

	private void registerTransactionSynchronization(final UID entity, final Object proxy) {
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCompletion(int status) {
				final String sStatus = (status == TransactionSynchronization.STATUS_COMMITTED) ? "Commit" : "Rollback";
				try {
					if (TransactionSynchronization.STATUS_COMMITTED == status) {
						getEventSupportFacade().finishProxyCall(entity, proxy, true);
					} else {
						getEventSupportFacade().finishProxyCall(entity, proxy, false);
					}
				} catch (NuclosBusinessRuleException e) {
					LOG.error("{} of proxy {} failed [entity={}]", sStatus, proxy, entity, e);
				} catch (NuclosCompileException e) {
					LOG.error("{} of proxy {} failed [entity={}]", sStatus, proxy, entity, e);
				}
			}
		});
	}

	@Override
	public void setIgnoreRecordGrantsAndOthers(boolean ignore) {
		this.ignoreRecordGrantsAndOthers = ignore;
	}

	@Override
	public boolean getIgnoreRecordGrantsAndOthers() {
		return this.ignoreRecordGrantsAndOthers;
	}

	@Override
	public void setThinReadEnabled(boolean enabled) {
		this.thinReadEnabled = enabled;
	}

	@Override
	public boolean isThinReadEnabled() {
		return this.thinReadEnabled;
	}
}
