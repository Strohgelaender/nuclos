//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EOGenericObjectVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclet.IEOGenericObjectProcessor;
import org.nuclos.server.dblayer.DbException;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class EOGenericObjectProcessor extends AbstractJdbcDalProcessor<EOGenericObjectVO, Long>
	implements IEOGenericObjectProcessor {
	
	private IColumnToVOMapping<Long, Long> idColumn; 
	private IColumnToVOMapping<UID, Long> moduleColumn;
	
	public EOGenericObjectProcessor(List<IColumnToVOMapping<? extends Object, Long>> allColumns, 
			IColumnToVOMapping<UID, Long> moduleColumn, IColumnToVOMapping<Long, Long> idColumn) {
		super(E.GENERICOBJECT.getUID(), EOGenericObjectVO.class, Long.class, allColumns);
		this.moduleColumn = moduleColumn;
		this.idColumn = idColumn;
	}

	@Override
	public EntityMeta<Long> getMetaData() {
		return E.GENERICOBJECT;
	}

	@Override
	protected IColumnToVOMapping<Long, Long> getPrimaryKeyColumn() {
		return idColumn;
	}

	@Override
	public void delete(Delete<Long> id) throws DbException {
		super.delete(id);
	}

	@Override
	public Object insertOrUpdate(EOGenericObjectVO dalVO) {
		return super.insertOrUpdate(dalVO);
	}

	@Override
	public List<EOGenericObjectVO> getAll() {
		return super.getAll();
	}
	
	@Override
	public EOGenericObjectVO getByPrimaryKey(Long id) {
		return super.getByPrimaryKey(id);
	}

	@Override
	public List<EOGenericObjectVO> getByPrimaryKeys(List<Long> ids) {
		return super.getByPrimaryKeys(allColumns, ids);
	}

	@Override
	public List<EOGenericObjectVO> getByParent(UID parentId) {
		return super.getByColumn(allColumns, moduleColumn, parentId);
	}
	
	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<Long> dalVO) throws DbException {
		throw new NotImplementedException();
	}
	
}
