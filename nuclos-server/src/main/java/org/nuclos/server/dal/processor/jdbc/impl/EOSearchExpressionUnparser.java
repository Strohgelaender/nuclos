//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonDateValues;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithOtherField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithParameter;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIsNullCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSelfSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.GeneralJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.PlainSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.ReferencingCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.collect.collectable.searchcondition.visit.AtomicVisitor;
import org.nuclos.common.collect.collectable.searchcondition.visit.CompositeVisitor;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dblayer.FieldUIDRefIterator;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.processor.ColumnToFieldIdVOMapping;
import org.nuclos.server.dal.processor.ColumnToFieldVOMapping;
import org.nuclos.server.dal.processor.ColumnToRefFieldVOMapping;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCompoundColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbOrder;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbReferencedCompoundColumnExpression;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbNamedObject;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// @Configurable
public class EOSearchExpressionUnparser {

	private static final Logger LOG = LoggerFactory.getLogger(EOSearchExpressionUnparser.class);
	
	private final DbQueryBuilder queryBuilder;
	private final DbQuery<?> query;
	private DbFrom table;
	private final EntityMeta<?> entity;
	
	private final DataLanguageCache dlCache;
	private final SessionUtils utils;

	private final Map<UID, String> aliasMap;
	private final Map<UID, FieldMeta> langFieldsMap;
	private final List<Pair<UID, FieldMeta>> additionalColumns;
	private final Map<UID, DbExpression> additionalColumnExpressionsMap;
	
	public EOSearchExpressionUnparser(DbQuery<?> query, EntityMeta<?> entityMeta, SessionUtils utils, DataLanguageCache dlCache) {
		if (query == null || entityMeta == null || utils == null) {
			throw new NullPointerException();
		}
		this.entity = entityMeta;
		this.query = query;
		this.queryBuilder = query.getBuilder();
		this.table = CollectionUtils.getFirst(query.getRoots());
		this.utils = utils;
		this.dlCache = dlCache;
		this.aliasMap = new HashMap<>();
		this.langFieldsMap = new HashMap<>();
		this.additionalColumns = new ArrayList<>();
		this.additionalColumnExpressionsMap = new HashMap<>();
	}
	
	public void unparseSearchCondition(CollectableSearchCondition clctcond) {
		if (clctcond != null) {
			if (!clctcond.isSyntacticallyCorrect()) {
				throw new IllegalArgumentException("mdsearch.unparser.error.invalid.expression");
			}

			DbCondition condition = clctcond.accept(new UnparseVisitor());
			query.where(condition);
		}
	}
	
	public void unparseSortingOrder(final List<CollectableSorting> sorting, UID language, DbExpression<?> pkColumn) {
		final List<DbOrder> orderList = new ArrayList<>();
		final MetaProvider metaProvider = MetaProvider.getInstance();
		boolean bPrimaryKeyInSorting = false;
		
		if (sorting != null) {
			for (CollectableSorting cs : sorting) {
				DbExpression<?> column = getDbColumn(cs);
				
				FieldMeta<?> fm = metaProvider.getEntityField(cs.getField());
				EntityMeta<?> em = metaProvider.getEntity(fm.getEntity());
				
				if (entity.getUID().equals(em.getUID()) && fm.isLocalized() && language != null) {
				    EntityMeta<?> langEntityMeta = metaProvider.getEntity(NucletEntityMeta.getEntityLanguageUID(em));
				    FieldMeta<?> field = langEntityMeta.getField(DataLanguageServerUtils.extractFieldUID(fm.getUID()));
				    
				    DbNamedObject nmdObj = new DbNamedObject(langEntityMeta.getUID(), langEntityMeta.getDbTable());
					DbColumnType columnType = MetaDbHelper.createDbColumnType(field);
				    DbColumn col = new DbColumn(field.getUID(), nmdObj, field.getDbColumn(), columnType, DbNullable.NULL, field.getDefaultValue(), field.getOrder());
				    
				    Iterator<DbFrom<?>> iterator = query.getRoots().iterator();
				    
				    boolean found = false;
				    
				    while(iterator.hasNext()) {
				    	DbFrom<?> next = iterator.next();				    	
				    	Iterator<DbJoin<?>> iterator2 = next.getJoins().iterator();
				    	
				    	while(iterator2.hasNext()) {
				    		DbJoin<?> next2 = iterator2.next();
				    		
				    		if (next2.getAlias().equals("LANG_" + language.toString())) {
				    			column = next2.baseColumn(col);
				    			found = true;
				    			break;
				    		}
				    	}
				    	
				    	if (found) {
				    		break;					    		
				    	}
				    }
				}
				
				if (pkColumn != null && !bPrimaryKeyInSorting && pkColumn.getSqlColumnExpr().equals(column.getSqlColumnExpr())) {
					bPrimaryKeyInSorting = true;
				}
				
				orderList.add(cs.isAscending() ? queryBuilder.asc(column) : queryBuilder.desc(column));
			}
		}
		
		if (pkColumn != null && !bPrimaryKeyInSorting) {
			orderList.add(queryBuilder.asc(pkColumn));			
		}
		
		query.orderBy(orderList);
	}
	
	public void addRefJoinCondition(RefJoinCondition rjc) {
		rjc.accept(new UnparseVisitor());
	}

	private <T> DbExpression<T> getDbColumn(CollectableSorting sort) {
		final FieldMeta<?> entityField = MetaProvider.getInstance().getEntityField(sort.getField());
		final String calcFunction = entityField.getCalcFunction();
		final Class<T> type = (Class<T>) normalizeJavaType(entityField.getDataType());
		DbField<T> dbf = SimpleDbField.create(entityField.getDbColumn(), type);
		final DbExpression<T> result;
		if (sort.isBaseEntity()) {
			if (!RigidUtils.isNullOrEmpty(calcFunction)) {
				if (SF.OWNER.checkField(entityField.getEntity(), entityField.getUID())) {
					DbExpression<T> ownerExpr = null;
					for (DbSelection<?> selection : query.getSelections()) {
						if (selection instanceof DbExpression && StringUtils.equals(selection.getAlias(), SF.OWNER.getDbColumn())) {
							ownerExpr = (DbExpression<T>) selection;
							break;
						}
					}
					if (ownerExpr == null) {
						// fallback sorting by uid / should not happen, because owner is always part of selection
						result = ColumnToRefFieldVOMapping.getOwnerRefColumn(entityField, table);
					} else {
						result = ownerExpr;
					}
				} else {
					final List<DbSelection<Object>> selects = table.getQuery().getSelections();
					// only INTID in SELECT part of SQL query
					if (selects.size() == 1) {
						// we must add the sorting function to the SELECT part in order to get ORDER BY to work
						// NUCLOS-311: MS SQL needs a schema name here.
						final DbSelection<Object> select = (DbSelection<Object>) table.getQuery().getBuilder().plainExpression(type, 
								queryBuilder.getDBAccess().getSchemaName() + "." 
								+ calcFunction + "(" + table.getAlias() + ".INTID) " 
								+ entityField.getDbColumn());
						selects.add(select);
					}
					// no tableAlias, only select alias (i.e. unqualified)...
					result = table.column(null, dbf);
				}
			}
			else if (entity.isDynamic() || entity.isChart()) {
				boolean bCaseSensitive = DbFrom.useCaseSensitiveConcerningUdGenericObject(entity.getDbTable(), dbf.getDbColumn());
				result = table.baseColumnSensitiveOrInsensitive(dbf, bCaseSensitive, false);
			}
			else {
				result = table.baseColumn(dbf);
			}
		}
		else {
			if (calcFunction != null) {
				throw new IllegalStateException();
			}
			else {
				result = table.column(sort.getTableAlias(), dbf);
			}
		}
		return result;
	}

	private class UnparseVisitor implements Visitor<DbCondition, RuntimeException>, CompositeVisitor<DbCondition, RuntimeException>, AtomicVisitor<DbCondition, RuntimeException> {

		UnparseVisitor() {
		}

		@Override
		public DbCondition visitTrueCondition(TrueCondition truecond) {
			return queryBuilder.alwaysTrue();
		}

		@Override
		public DbCondition visitAtomicCondition(AtomicCollectableSearchCondition atomiccond) {
			// the cast is needed for the correct method dispatch
			return atomiccond.accept((AtomicVisitor<DbCondition, RuntimeException>) this);
		}

		@Override
		public DbCondition visitCompositeCondition(CompositeCollectableSearchCondition compositecond) {
			List<CollectableSearchCondition> operands = compositecond.getOperands();
			DbCondition[] conditions = new DbCondition[operands.size()];
			for (int i = 0; i < operands.size(); i++)
				conditions[i] = operands.get(i).accept(this);
			
			switch (compositecond.getLogicalOperator()) {
			case NOT:
				if (conditions.length != 1)
					throw new IllegalArgumentException("mdsearch.unparser.error.invalid.condition");//"Eine Suchbedingung mit NICHT darf nicht mehr als einen Operand haben.");
				return queryBuilder.not(conditions[0]);
			case AND:
				return queryBuilder.and(conditions);
			case OR:
				if (conditions.length == 0)
					return queryBuilder.alwaysTrue();
				return queryBuilder.or(conditions);
			default:
				throw new IllegalArgumentException("Invalid logical operator " + compositecond.getLogicalOperator());
			}
		}

		@Override
		public DbCondition visitIdCondition(CollectableIdCondition idcond) {
			/** @todo This only works for entities that have an INTID field. */

			if (idcond.getId() instanceof UID) {
				return queryBuilder.equal(table.baseColumn(SF.PK_UID), queryBuilder.literal(idcond.getId()));
			} else {
				return queryBuilder.equal(table.baseColumn(SF.PK_ID), queryBuilder.literal(idcond.getId()));
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		public DbCondition visitSubCondition(CollectableSubCondition subcond) {
			EntityMeta<?> subEntityMeta =  MetaProvider.getInstance().getEntity(subcond.getSubEntityUID());

			if (subEntityMeta.isUidEntity()
					&& !subEntityMeta.isChart() && !subEntityMeta.isDynamic()) {
				DbQuery<UID> subQuery = query.subquery(UID.class);
				DbFrom<?> subTable = subQuery.from(subEntityMeta, "sub");
				subQuery.distinct(true);
				subQuery.select(subcond.getForeignKeyFieldUID() == null ?
						subTable.baseColumn(SF.PK_UID) :
						subTable.baseColumn(SimpleDbField.create(DbUtils.getDbIdFieldName(MetaProvider.getInstance().getEntityField(subcond.getForeignKeyFieldUID()),
								true), UID.class)));
				
				EOSearchExpressionUnparser subUnparser = new EOSearchExpressionUnparser(subQuery, subEntityMeta, utils, getDlCache());
				subUnparser.unparseSearchCondition(subcond.getSubCondition());
				
				if (subcond.getFieldUID() == null) {
					return table.baseColumn(SF.PK_UID).in(subQuery);
				} else {
					return table.baseColumn(SimpleDbField.create(DbUtils.getDbIdFieldName(MetaProvider.getInstance().getEntityField(subcond.getFieldUID()),
							true), String.class)).in(subQuery);
				}
			} 
			else {
				DbQuery<Long> subQuery = query.subquery(Long.class);
				DbFrom<?> subTable = subQuery.from(subEntityMeta, "sub");
				subQuery.distinct(true);
				if (subEntityMeta.isChart() || subEntityMeta.isDynamic()) {
					if (subcond.getForeignKeyFieldUID() == null) {
						subQuery.select(subTable.baseColumn(SF.PK_ID));
					} else {
						FieldMeta<?> fm = MetaProvider.getInstance().getEntityField(subcond.getForeignKeyFieldUID());
						DbField<Long> dbf = SimpleDbField.create(DbUtils.getDbIdFieldName(fm, false), Long.class);
						boolean bCaseSensitive = DbFrom.useCaseSensitiveConcerningUdGenericObject(subEntityMeta.getDbTable(), dbf.getDbColumn());
						subQuery.select(subTable.baseColumnSensitiveOrInsensitive(dbf, bCaseSensitive, false));
					}
				} else {
					subQuery.select(subcond.getForeignKeyFieldUID() == null ?
							subTable.baseColumn(SF.PK_ID) :
							subTable.baseColumn(SimpleDbField.create(DbUtils.getDbIdFieldName(
									MetaProvider.getInstance().getEntityField(subcond.getForeignKeyFieldUID()),
									false), Long.class)));
				}
				EOSearchExpressionUnparser subUnparser = new EOSearchExpressionUnparser(subQuery, subEntityMeta, utils, getDlCache());
				subUnparser.unparseSearchCondition(subcond.getSubCondition());
	
				if (subcond.getFieldUID() == null) {
					return table.baseColumn(SF.PK_ID).in(subQuery);
				} else {
					return table.baseColumn(SimpleDbField.create(DbUtils.getDbIdFieldName(
							MetaProvider.getInstance().getEntityField(subcond.getFieldUID()),
							false), Long.class)).in(subQuery);
				}
			}
		}

		@Override
		public DbCondition visitRefJoinCondition(RefJoinCondition joincond) {
			final IMetaProvider mdProv = MetaProvider.getInstance();
			
			final FieldMeta<?> field = joincond.getField();
			final boolean isLookupField = field.getLookupEntity() != null;
			final boolean isRefField = field.getForeignEntity() != null;
			final EntityMeta<?> refEntity;
			try{
				refEntity = mdProv.getEntity(RigidUtils.firstNonNull(field.getForeignEntity(), field.getUnreferencedForeignEntity()));
			}catch(org.nuclos.common2.exception.CommonFatalException e){
				if(!isLookupField && isRefField) throw e;
				else return null;
			}
			LOG.debug("visitRefJoinCondition: apply {} on {}", joincond, table);

			boolean isLOVSearch = table.getAlias().equals(joincond.getTableAliasRight());

			if (!table.containsAlias(joincond.getTableAliasRight()) || isLOVSearch) {
				if (table.getAlias().equals(joincond.getTableAliasLeft())) {
					if (refEntity.isUidEntity()) {
						final DbJoin<UID> join = table.join(refEntity, JoinType.LEFT, joincond.getTableAliasRight()).on(
							SimpleDbField.create(DbUtils.getDbIdFieldName(field, true), UID.class), SF.PK_UID);
					} else {
						final DbJoin<Long> join = table.join(refEntity, JoinType.LEFT, joincond.getTableAliasRight()).on(
							SimpleDbField.create(DbUtils.getDbIdFieldName(field, false), Long.class), SF.PK_ID);
					}
				} else if (!table.getAlias().equals(joincond.getTableAliasRight())) {
					Set<DbJoin<?>> joins = table.getJoins();
					DbJoin<?> join = null;
					for (DbJoin<?> j : joins) {
						if (j.getAlias().equals(joincond.getTableAliasLeft())) {
							join = j;
							break;
						}
					}
					if (join == null) {
						throw new NuclosFatalException(String.format("Join for alias \"%s\" does not exist", joincond.getTableAliasLeft()));
					}
					if (refEntity.isUidEntity()) {
						join.join(refEntity, JoinType.LEFT, joincond.getTableAliasRight()).on(
								SimpleDbField.create(DbUtils.getDbIdFieldName(field, true), UID.class), SF.PK_UID);
					} else {
						join.join(refEntity, JoinType.LEFT, joincond.getTableAliasRight()).on(
								SimpleDbField.create(DbUtils.getDbIdFieldName(field, false), Long.class), SF.PK_ID);
					}
				}

				if (field.getForeignEntity() != null) {
					EntityMeta<?> emRefEntity = mdProv.getEntity(field.getForeignEntity());

					if (emRefEntity.IsLocalized()) {
						// If this reference uses localized fields as stringifiers we have to extend the join to enable sorting and searching
						if (field.getForeignEntityField() != null) {
							final UID dlFieldUID = DataLanguageServerUtils.extractFieldUID(field.getUID());
							boolean localizedFieldsAsRefStringifier = usingJoinConditionWithLocalizedReferenceFields(joincond);

							if (localizedFieldsAsRefStringifier) {
								FieldMeta<Long> refPKMeta = SF.PK_ID.getMetaData(emRefEntity);

								EntityMeta<?> emRefEntityLang = mdProv.getEntity(NucletEntityMeta.getEntityLanguageUID(emRefEntity));
								UID refForeignRefLangUID = DataLanguageServerUtils.extractForeignEntityReference(emRefEntity.getUID());
								UID refDataLangUID = DataLanguageServerUtils.extractDataLanguageReference(emRefEntity.getUID());

								FieldMeta<?> refForeignRefLangMeta = emRefEntityLang.getField(refForeignRefLangUID);
								FieldMeta<?> refDataLangMeta = emRefEntityLang.getField(refDataLangUID);

								// this part joins the data lang tables to the main query
								if (query != null) {
									DbFrom<?> from = query.getDbFrom();

									if (from != null) {
										List<DbFrom> allJoinsInclNested = new ArrayList<>();
										if (isLOVSearch) {
											allJoinsInclNested.add(from);
										}
										allJoinsInclNested.addAll(from.getJoins());
										for (DbJoin j : from.getJoins()) {
											allJoinsInclNested.addAll(j.getJoins());
										}

										for (DbFrom curJoin : allJoinsInclNested) {

											if (curJoin.getEntity().getUID().equals(emRefEntity.getUID()) &&
													curJoin.getAlias().equals(joincond.getTableAliasRight())) {
												DbNamedObject nmdObjMainEntity = new DbNamedObject(emRefEntity.getUID(), emRefEntity.getDbTable());
												DbColumnType columnTypeMainEntity = MetaDbHelper.createDbColumnType(refPKMeta);
												DbColumn colMainEntity = new DbColumn(refPKMeta.getUID(), nmdObjMainEntity,
														refPKMeta.getDbColumn(), columnTypeMainEntity, DbNullable.NULL, refPKMeta.getDefaultValue(), refPKMeta.getOrder());

												DbNamedObject nmdObjLangEntity = new DbNamedObject(emRefEntityLang.getUID(), emRefEntityLang.getDbTable());
												DbColumnType columnTypeLangEntity = MetaDbHelper.createDbColumnType(refForeignRefLangMeta);
												DbColumn colLangEntity = new DbColumn(refForeignRefLangMeta.getUID(), nmdObjLangEntity,
														refForeignRefLangMeta.getDbColumn(), columnTypeLangEntity, DbNullable.NULL, refForeignRefLangMeta.getDefaultValue(), refForeignRefLangMeta.getOrder());

												DbColumnType columnTypeRefLangEntity = MetaDbHelper.createDbColumnType(refDataLangMeta);
												DbColumn colRefLangEntity = new DbColumn(refDataLangMeta.getUID(), nmdObjLangEntity,
														refDataLangMeta.getDbColumn(), columnTypeRefLangEntity, DbNullable.NULL, refDataLangMeta.getDefaultValue(), refDataLangMeta.getOrder());

												String aliasForlocalizedForeignFieldUsedAsStringifiers =
														TableAliasSingleton.getInstance().getAlias(refForeignRefLangMeta, curJoin.getAlias());

												DbJoin<?> joinToLangTable = curJoin.join(emRefEntityLang, JoinType.LEFT, aliasForlocalizedForeignFieldUsedAsStringifiers);

												joinToLangTable.onAnd(
														queryBuilder.equal(curJoin.baseColumn(colMainEntity), joinToLangTable.baseColumn(colLangEntity)),
														queryBuilder.equalValue(joinToLangTable.baseColumn(colRefLangEntity), getDlCache().getLanguageToUse().toString()));

												NucletFieldMeta<?> newStringifiedField = new NucletFieldMeta(field);
												newStringifiedField.setForeignEntity(refForeignRefLangMeta.getForeignEntity());
												newStringifiedField.setPrimaryKey(refForeignRefLangUID);

												// now add language fields
												Map<UID, String> aliasMap = getAliasFieldMap(newStringifiedField, aliasForlocalizedForeignFieldUsedAsStringifiers, curJoin.getAlias());

												// update additionalColumn in case of a inherit reference
												for (Pair<UID, FieldMeta> pair : additionalColumns) {
													final FieldMeta otherStringifiedField = pair.y;
													boolean bUpdateInherit = false;

													FieldUIDRefIterator it = new FieldUIDRefIterator(ForeignEntityFieldUIDParser.REF_PATTERN, otherStringifiedField.getForeignEntityField());
													while (it.hasNext()) {
														IFieldUIDRef next = it.next();
														if (next.isUID()) {
															if (next.getUID().equals(field.getUID())) {
																bUpdateInherit = true;
																break;
															}
														}
													}

													if (bUpdateInherit) {
														DbExpression dbExpression = additionalColumnExpressionsMap.get(otherStringifiedField.getUID());
														query.getSelections().remove(dbExpression);

														it = new FieldUIDRefIterator(ForeignEntityFieldUIDParser.REF_PATTERN, field.getForeignEntityField());
														while (it.hasNext()) {
															IFieldUIDRef next = it.next();
															if (next.isUID()) {
																final FieldMeta<?> inheritField = mdProv.getEntityField(next.getUID());
																if (inheritField.isLocalized()) {
																	final UID inheritDlFieldUID = DataLanguageServerUtils.extractFieldUID(inheritField.getUID());
																	final FieldMeta<?> inheritDlField = mdProv.getEntityField(inheritDlFieldUID);
																	langFieldsMap.put(inheritField.getUID(), inheritDlField);
																	aliasMap.put(inheritDlField.getUID(), aliasForlocalizedForeignFieldUsedAsStringifiers);
																}
															}
														}

														if (dbExpression instanceof DbCompoundColumnExpression) {
															final DbCompoundColumnExpression dbCompoundColumnExpression = new DbCompoundColumnExpression(
																	query.getDbFrom(),
																	otherStringifiedField, otherStringifiedField.getDbColumn() + "_lang", aliasMap, langFieldsMap, isLOVSearch);
															query.addSelection(dbCompoundColumnExpression);
															additionalColumnExpressionsMap.put(newStringifiedField.getUID(), dbCompoundColumnExpression);
														} else {
															final DbReferencedCompoundColumnExpression dbReferencedCompoundColumnExpression = new DbReferencedCompoundColumnExpression(
																	query.getDbFrom(),
																	otherStringifiedField, String.class, otherStringifiedField.getDbColumn() + "_lang", aliasMap, langFieldsMap);
															query.addSelection(dbReferencedCompoundColumnExpression);
															additionalColumnExpressionsMap.put(newStringifiedField.getUID(), dbReferencedCompoundColumnExpression);
														}
													}
												}

												if (isLOVSearch || query.getSelections().size() > 1) {
													additionalColumns.add(new Pair<UID, FieldMeta>(dlCache.getLanguageToUse(), newStringifiedField));
													newStringifiedField.setDataType(String.class.getCanonicalName());
													if (isLOVSearch) {
														final DbCompoundColumnExpression dbCompoundColumnExpression = new DbCompoundColumnExpression(
																curJoin,
																newStringifiedField, newStringifiedField.getDbColumn() + "_lang", aliasMap, langFieldsMap, isLOVSearch);
														query.addSelection(dbCompoundColumnExpression);
														additionalColumnExpressionsMap.put(newStringifiedField.getUID(), dbCompoundColumnExpression);
													} else {
														final DbReferencedCompoundColumnExpression dbReferencedCompoundColumnExpression = new DbReferencedCompoundColumnExpression(
																((DbJoin) curJoin).getParent(),
																newStringifiedField, String.class, newStringifiedField.getDbColumn() + "_lang", aliasMap, langFieldsMap);
														query.addSelection(dbReferencedCompoundColumnExpression);
														additionalColumnExpressionsMap.put(newStringifiedField.getUID(), dbReferencedCompoundColumnExpression);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

			// ??? We have no real condition, all is said within the join...
			return queryBuilder.alwaysTrue();
		}
		
		private Map<UID, String> getAliasFieldMap(FieldMeta<?> field,
				String aliasForlocalizedForeignFieldUsedAsStringifiers,
				String alias) {

			FieldUIDRefIterator it = new FieldUIDRefIterator(ForeignEntityFieldUIDParser.REF_PATTERN, field.getForeignEntityField());
			
			while(it.hasNext()) {
				IFieldUIDRef next = it.next();
				if (next.isUID()) {
					UID fieldRefStringifierUID = next.getUID();
					FieldMeta fmRef = MetaProvider.getInstance().getEntityField(fieldRefStringifierUID);
					if (fmRef.isLocalized()) {
						final UID extractFieldUID = DataLanguageServerUtils.extractFieldUID(fieldRefStringifierUID);
						langFieldsMap.put(fieldRefStringifierUID, MetaProvider.getInstance().getEntityField(extractFieldUID));
						aliasMap.put(extractFieldUID, aliasForlocalizedForeignFieldUsedAsStringifiers);
					} else {
						aliasMap.put(fieldRefStringifierUID, alias);
					}
				}
			}
			
			return aliasMap;
		}

		@Override
		public DbCondition visitGeneralJoinCondition(GeneralJoinCondition joincond) {
			final IMetaProvider mdProv = MetaProvider.getInstance();
			
			final DbField<?> field1 = joincond.getField1();
			final EntityMeta<?> base1;
			final EntityMeta<?> refEntity1;
			if (field1 instanceof FieldMeta) {
				final FieldMeta<?> f1 = (FieldMeta<?>) field1;
				base1 = mdProv.getEntity(f1.getEntity());
				UID firstNonNull = RigidUtils.firstNonNull(f1.getForeignEntity(), f1.getUnreferencedForeignEntity());
				refEntity1 = firstNonNull != null ? mdProv.getEntity(firstNonNull) : null;
			} else {
				base1 = null;
				refEntity1 = null;
			}
			final DbField<?> field2 = joincond.getField2();
			final EntityMeta<?> base2;
			final EntityMeta<?> refEntity2;
			if (field2 instanceof FieldMeta) {
				final FieldMeta<?> f2 = (FieldMeta<?>) field2;
				base2 = mdProv.getEntity(f2.getEntity());
				UID firstNonNull = RigidUtils.firstNonNull(f2.getForeignEntity(), f2.getUnreferencedForeignEntity());
				refEntity2 = firstNonNull != null ? mdProv.getEntity(firstNonNull) : null;
			} else {
				base2 = null;
				refEntity2 = null;
			}
			
			if (base1 != null && base2 != null && base1.isUidEntity() != base2.isUidEntity()) {
				throw new IllegalStateException("We can't join a UID and a non-UID entity: " + base1 + " and " + base2);
			}
			if (refEntity1 != null) {
				if (refEntity2 != null) {
					throw new IllegalStateException("We can't join on two reference fields: " + field1 + " and " + field2);
				}
				if (base2 != null && !base2.checkEntityUID(refEntity1.getUID())) {
					throw new IllegalStateException("We can't join on reference to entity " + refEntity1 + " to base " + base2);
				}
			} else if (refEntity2 != null) {
				if (base1 != null && !base1.checkEntityUID(refEntity2.getUID())) {
					throw new IllegalStateException("We can't join on reference to entity " + refEntity2 + " to base " + base1);
				}
			}
			LOG.debug("visitGeneralJoinCondition: apply {} on {}", joincond, table);
			DbFrom<?> from;
			EntityMeta<?> join;
			String joinAlias;
			// base1 should be the already joined table
			// BUT if we get a PK, base1 == null
			if (base1 != null) {
				// this MUST lead to an already joined table
				from = table.findFrom(base1.getUID());
				if (from == null) {
					throw new IllegalStateException("Based on (already joined) field " + field1 + " we found entity " + 
							base1 + " that is not used in SQL from/join clauses yet");
				}
				if (base2 != null) {
					// field2 is not a PK and hence base2 is non-null and the entity to join.
					join = base2;
				} else {
					// field2 is a PK and hence refEntity2 is non-null and the entity to join.
					join = refEntity1;
					if (join == null) {
						throw new IllegalStateException("We want to join on field " + field2 +
								" but field " + field1 + " is not a reference field");
					}
				}
				joinAlias = joincond.getTableAlias2();
			} else if (base2 != null) {
				// base2 should be the table to join
				// we only get here if base1 == null 
				// we know that field1 is a PK
				from = table.findFrom(refEntity2.getUID());
				join = base2;
				if (from == null) {
					throw new IllegalStateException("Based on field (to join) " + field2 + " we found entity " + 
							base2 + " and reference entity " + refEntity2 + 
							" but this reference is not used in SQL from/join clauses yet");
				}
				/*
				if (join == null) {
					throw new IllegalStateException("We want to join on field " + field2 +
							" but the could find out the entity this field belongs to");
				}
				 */
			} else {
				throw new IllegalStateException("We can't figure out the tables of the join: " + field1 + " and " + field2);
			}
			if (table.findFrom(join.getUID()) != null) {
				throw new IllegalStateException("We want to join with entity " + join +
						" but this entity is already used in SQL from/join clauses");
			}
			if (join.isUidEntity()) {
				final DbJoin<UID> joined = ((DbFrom<UID>) from).join((EntityMeta<UID>) join, JoinType.LEFT, joincond.getTableAlias2()).on(
					DbUtils.newSimpleUidDbField(field1), DbUtils.newSimpleUidDbField(field2));
			} else {
				final DbJoin<Long> joined = ((DbFrom<Long>) from).join((EntityMeta<Long>) join, JoinType.LEFT, joincond.getTableAlias2()).on(
						DbUtils.newSimpleLongDbField(field1), DbUtils.newSimpleLongDbField(field2));
			}
			// ??? We have no real condition, all is said within the join...
			return queryBuilder.alwaysTrue();
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public DbCondition visitReferencingCondition(ReferencingCollectableSearchCondition refcond) {
			DbExpression<?> refColumn = getDbIdColumn(refcond.getReferencingField());
			EntityMeta refEntityMeta = MetaProvider.getInstance().getEntity(refcond.getReferencedEntityUID());
			if (refColumn.getJavaType().equals(UID.class)) {
				DbQuery<UID> subQuery = query.subquery(UID.class);
				DbFrom<?> refTable = subQuery.from(refEntityMeta, "ref");
				subQuery.distinct(true);
				subQuery.select(refTable.baseColumn(SF.PK_UID));
				EOSearchExpressionUnparser subUnparser = new EOSearchExpressionUnparser(subQuery, refEntityMeta, utils, getDlCache());
				subUnparser.unparseSearchCondition(refcond.getSubCondition());

				return ((DbColumnExpression<UID>) refColumn).in(subQuery);
			} else {
				DbQuery<Long> subQuery = query.subquery(Long.class);
				DbFrom<?> refTable = subQuery.from(refEntityMeta, "ref");
				subQuery.distinct(true);
				subQuery.select(refTable.baseColumn(SF.PK_ID));
				EOSearchExpressionUnparser subUnparser = new EOSearchExpressionUnparser(subQuery, refEntityMeta, utils, getDlCache());
				subUnparser.unparseSearchCondition(refcond.getSubCondition());

				return ((DbColumnExpression<Long>) refColumn).in(subQuery);
			}
		}

		@Override
		public DbCondition visitPlainSubCondition(PlainSubCondition subcond) {
			// TODO: get rid of plain sql conditions
			DbField field = table.getEntity().isUidEntity() ? SF.PK_UID : SF.PK_ID;
			Class clas = table.getEntity().isUidEntity() ? UID.class : Integer.class;
			return queryBuilder.isMember(table.baseColumn(field), queryBuilder.plainExpression(clas, subcond.getPlainSQL()));
		}

		@Override
		public DbCondition visitSelfSubCondition(CollectableSelfSubCondition subcond) {
			// TODO: what does it mean that this returns null
			return null;
		}

		@Override
		public DbCondition visitComparison(CollectableComparison comparison) {
			final CollectableField clctfComparand = comparison.getComparand();
			final CollectableEntityField clctEntityField = comparison.getEntityField();
			final FieldMeta<?> efMeta;
			if (clctEntityField instanceof CollectableEOEntityField) {
				efMeta = ((CollectableEOEntityField) clctEntityField).getMeta();
			} else {
				efMeta = MetaProvider.getInstance().getEntityField(clctEntityField.getUID());
			}
			Object valueId = clctfComparand.isIdField() ? clctfComparand.getValueId() : null;
			final Object value = clctfComparand.getValue();
			if (valueId == null && value instanceof UID && (efMeta.getForeignEntity() != null || efMeta.getUnreferencedForeignEntity() != null)) {
				// Auto fix condition
				LOG.warn("Fixing UID condition: op=" + comparison.getComparisonOperator() + ", value=" + value + ", valueId=" + valueId);
				valueId = value;
			}
			switch (comparison.getComparisonOperator()) {
			case EQUAL:
				if (valueId != null) {
					DbExpression<?> x = getDbIdColumn(clctEntityField);
					return queryBuilder.equal(x, queryBuilder.literal(valueId));
				}
			case NOT_EQUAL:
				if (valueId != null) {
					DbExpression<?> x = getDbIdColumn(clctEntityField);
					return queryBuilder.notEqual(x, queryBuilder.literal(valueId));
				}
			default:
				DbExpression<? extends Comparable> x = (DbExpression<Comparable>) getDbColumn(clctEntityField);
				DbExpression<? extends Comparable> y = (DbExpression<Comparable>) normalizedLiteral(value);

				if (x.getJavaType() == String.class) {
					x = queryBuilder.upper(x.as(String.class));
					y = queryBuilder.upper(y.as(String.class));
				}
				else if (x.getJavaType() == java.util.Date.class) {
					x = queryBuilder.truncDate(x.as(java.util.Date.class));
				}

				// @see NUCLOS-630 datcreated and datchange should not be interpreted with time, so do not truncate time!
				if (x instanceof DbColumnExpression) {
					// Convert Timestamp to Date only if the given value for comparison is not a Timestamp
					if (x.getJavaType() == InternalTimestamp.class && !(value instanceof Timestamp)) {
						x = queryBuilder.convertInternalTimestampToDate(x.as(InternalTimestamp.class));
					}
				}

				// InternalTimestamp should be interpreted with time, so do not truncate time!
				/*if (x.getJavaType() == InternalTimestamp.class) {
					x = queryBuilder.convertInternalTimestampToDate(x.as(InternalTimestamp.class));
				}*/
				return compare(comparison.getComparisonOperator(), x, y);
			}
		}

		@Override
		public DbCondition visitComparisonWithParameter(CollectableComparisonWithParameter comparisonwp) {
			final ComparisonOperator compop = comparisonwp.getComparisonOperator();
			final CollectableEntityField field = comparisonwp.getEntityField();
			final CollectableField comparand;
			// Reduce parameter to a regular comparand value...
			switch (comparisonwp.getParameter()) {
			case TODAY:
				comparand = new CollectableValueField(RelativeDate.today());
				break;
			case USER:
				String userName = utils.getCurrentUserName();
				if (field.isIdField()) {
					UID userUID = SecurityCache.getInstance().getUserUid(userName);
					comparand = new CollectableValueIdField(userUID, userName);
				} else {
					comparand = new CollectableValueField(userName);
				}
				break;
			default:
				throw new UnsupportedOperationException("Unsupported parameter " + comparisonwp.getParameter());
			}
			// ...and delegate to that handler method
			return visitComparison(new CollectableComparison(field, compop, comparand));
		}

		@Override
		public DbCondition visitComparisonDateValues(CollectableComparisonDateValues comparisondv) {
			final CollectableEntityField field = comparisondv.getEntityField();
			if (comparisondv.getDateValues().getToDate() != null && comparisondv.getDateValues().getFromDate() != null) {
				final CompositeCollectableSearchCondition collectableComparison =  new CompositeCollectableSearchCondition(LogicalOperator.AND, Arrays.asList(
						new CollectableComparison(field, ComparisonOperator.LESS_OR_EQUAL, new CollectableValueField(comparisondv.getDateValues().getToDate())),
						new CollectableComparison(field, ComparisonOperator.GREATER_OR_EQUAL, new CollectableValueField(comparisondv.getDateValues().getFromDate()))));
				// ...and delegate to that handler method
				return visitCompositeCondition(collectableComparison);
			}
			if (comparisondv.getDateValues().getToDate() != null) {
				return visitComparison(new CollectableComparison(field, ComparisonOperator.LESS_OR_EQUAL, new CollectableValueField(comparisondv.getDateValues().getToDate())));
			}
			if (comparisondv.getDateValues().getFromDate() != null) {
				return visitComparison(new CollectableComparison(field, ComparisonOperator.GREATER_OR_EQUAL, new CollectableValueField(comparisondv.getDateValues().getFromDate())));
			}
			throw new IllegalArgumentException();
		}

		@Override
		public DbCondition visitComparisonWithOtherField(CollectableComparisonWithOtherField comparisonwf) {
			final ComparisonOperator compop = comparisonwf.getComparisonOperator();
			final CollectableEntityField left = comparisonwf.getEntityField();
			final CollectableEntityField right = comparisonwf.getOtherField();

			DbExpression<? extends Comparable> x, y;
			if (left.isIdField() && right.isIdField() && (compop == ComparisonOperator.EQUAL || compop == ComparisonOperator.NOT_EQUAL))
			{
				x = (DbExpression<? extends Comparable>) getDbIdColumn(left);
				y = (DbExpression<? extends Comparable>) getDbIdColumn(right);
			} else {
				x = (DbExpression<? extends Comparable>) getDbColumn(left);
				y = (DbExpression<? extends Comparable>) getDbColumn(right);
				if (x.getJavaType() == String.class) {
					x = queryBuilder.upper(x.as(String.class));
					y = queryBuilder.upper(y.as(String.class));
				}
			}
			return compare(compop, x, y);
		}

		@Override
		public DbCondition visitLikeCondition(CollectableLikeCondition cond) {
			DbExpression<String> x = getDbColumn(cond.getEntityField()).as(String.class);
			Class<?> _class = cond.getEntityField().getJavaClass();
			boolean useILike = queryBuilder.supportsILike() && _class == String.class;
			if (_class == java.util.Date.class) {
				// TODO: ...
				String pattern = DbQueryBuilder.DATE_PATTERN_GERMAN;
				DateFormat df = SpringLocaleDelegate.getInstance().getDateFormat();
				if (df instanceof SimpleDateFormat) {
					pattern = ((SimpleDateFormat)df).toPattern();
				}
				x = queryBuilder.convertDateToString(x.as(java.util.Date.class), pattern);
			} else if (!useILike) {
				x = queryBuilder.upper(x.as(String.class));
			}
			String pattern = cond.getSqlCompatibleLikeComparand();
			switch (cond.getComparisonOperator()) {
			case LIKE:
				return useILike ? queryBuilder.iLike(x, pattern) :
						queryBuilder.like(x, StringUtils.toUpperCase(pattern));
			case NOT_LIKE:
				return useILike ? queryBuilder.notILike(x, pattern) :
						queryBuilder.notLike(x, StringUtils.toUpperCase(pattern));
			default:
				throw new IllegalArgumentException("Invalid like operator " + cond.getComparisonOperator());
			}
		}

		@Override
		public <T> DbCondition visitInCondition(CollectableInCondition<T> cond) {
			DbExpression<T> x;
			if (cond instanceof CollectableInIdCondition) {
				x = (DbExpression<T>)getDbIdColumn(cond.getEntityField());
			} else {
				x = (DbExpression<T>)getDbColumn(cond.getEntityField());
			}
			switch (cond.getComparisonOperator()) {
				case IN:
					return queryBuilder.in(x, cond.getInComparands());
				case NOT_IN:
					return queryBuilder.notin(x, cond.getInComparands());
				default:
					throw new IllegalArgumentException("Invalid in operator " + cond.getComparisonOperator());
			}			
		}
		
		@Override
		public DbCondition visitIsNullCondition(CollectableIsNullCondition cond) {
			final CollectableEntityField field = cond.getEntityField();
			final DbCondition result;
			final FieldMeta<?> efMeta = MetaProvider.getInstance().getEntityField(field.getUID());
			if (cond.isPositive()) {
				if (field.isReferencing() && efMeta.getLookupEntity() == null) { // lookup entities are not real referencing via db column intid_...
					result = queryBuilder.isNull(getRefIdDbColumn(field));
				}
				else {
					result = queryBuilder.isNull(getDbColumn(field));
				}
			} else {
				if (field.isReferencing() && efMeta.getLookupEntity() == null) { // lookup entities are not real referencing via db column intid_...
					result = queryBuilder.isNotNull(getRefIdDbColumn(field));
				}
				else {
					result = queryBuilder.isNotNull(getDbColumn(field));
				}
			}
			return result;
		}

		private <C, T extends Comparable<? extends C>, O extends Comparable<? extends C>> 
				DbCondition compare(ComparisonOperator op, DbExpression<T> x, DbExpression<O> y) {
			
			switch (op) {
			case LIKE:
				return queryBuilder.like(x.as(String.class), y.as(String.class));
			case NOT_LIKE:
				return queryBuilder.notLike(x.as(String.class), y.as(String.class));
			case EQUAL:
				return queryBuilder.equal(x, y);
			case NOT_EQUAL:
				return queryBuilder.notEqual(x, y);
			case LESS:
				return queryBuilder.lessThan(x, y);
			case LESS_OR_EQUAL:
				return queryBuilder.lessThanOrEqualTo(x, y);
			case GREATER:
				return queryBuilder.greaterThan(x, y);
			case GREATER_OR_EQUAL:
				return queryBuilder.greaterThanOrEqualTo(x, y);
				// This should not happen because op's arity should be 2, ...
			case IS_NULL:
				return queryBuilder.isNull(x);
			case IS_NOT_NULL:
				return queryBuilder.isNotNull(x);
			default:
				throw new IllegalArgumentException("Invalid comparison operator " + op);
			}
		}

		@Override
        public DbCondition visitIdListCondition(CollectableIdListCondition collectableIdListCondition) throws RuntimeException {
			final Collection<?> ids = collectableIdListCondition.getIds();
			boolean bUseUID = false;
			for (Object id : ids) {
				if (id instanceof UID || (id instanceof String && UID.isStringifiedUID((String)id))) {
					bUseUID = true;
				}
			}
			if (bUseUID) {
				List<List<UID>> split = CollectionUtils.splitEvery(collectableIdListCondition.getUIDIds(), query.getBuilder().getInLimit());
				DbCondition[] inConditions = new DbCondition[split.size()];
				for (int i = 0; i < split.size(); i++) {
					inConditions[i] = table.baseColumn(SF.PK_UID).as(UID.class).in(split.get(i));
				}

				return queryBuilder.or(inConditions);
			} else {
				List<List<Long>> split = CollectionUtils.splitEvery(collectableIdListCondition.getLongIds(), query.getBuilder().getInLimit());
				DbCondition[] inConditions = new DbCondition[split.size()];
				for (int i = 0; i < split.size(); i++) {
					inConditions[i] = table.baseColumn(SF.PK_ID).as(Long.class).in(split.get(i));
				}

				return queryBuilder.or(inConditions);
			}
        }
		
		/**
		 * Get the DBExpression for use in the condition part of the SQL.
		 * <p>
		 * TODO: {@link org.nuclos.server.dal.processor.IColumnToVOMapping#getDbColumn(DbFrom)}
		 * does the same for the SELECT part of the SQL.
		 * </p>
		 */
		private DbExpression<?> getDbColumn(CollectableEntityField field) {
			final IMetaProvider mdProv = MetaProvider.getInstance();
			final FieldMeta<?> entityField;
			if (field instanceof CollectableEOEntityField) {
				entityField = ((CollectableEOEntityField)field).getMeta();
			} else {
				entityField = mdProv.getEntityField(field.getUID());
			}
			
			String dbColumn = entityField.getDbColumn();
			final String tableAlias;
			if (!entityField.isCalculated()) {
				// 'stringified' nuclos reference field case
				final String dbRefColumn = (entityField.getForeignEntity() != null || entityField.getUnreferencedForeignEntity() != null) ?
						DbReferencedCompoundColumnExpression.getRefAlias(entityField) : null;
				if (dbRefColumn != null &&
						(dbRefColumn.startsWith("STRVALUE_") || dbRefColumn.startsWith("INTVALUE_") || dbRefColumn.startsWith("OBJVALUE_"))) {
					tableAlias = TableAliasSingleton.getInstance().getAlias(entityField);
					
					// It might be that the stringified reference used in the condition is not used for a join so far... (tp)
					if (!table.containsAlias(tableAlias)) {
						for (RefJoinCondition rjc : TableAliasSingleton.getInstance().getRefJoinCondition(entityField, table.getAlias())) {
							visitRefJoinCondition(rjc);
						}
					}
					// We must repeat the SQL 'case when ...' expression here
					return new DbReferencedCompoundColumnExpression<Object>(table, (FieldMeta<Object>)entityField, 
							(Class<Object>) (entityField.getJavaClass()==UID.class?String.class:entityField.getJavaClass()), false);
				}
				// normal field case
				else {
					// tableAlias = table.getAlias();
					// Since GeneralJoinCondition, we have to find out which alias to use. (tp)
					final DbFrom<?> t = table.findFrom(entityField.getEntity());
					tableAlias = t.getAlias();
					if (tableAlias == null) {
						
						throw new IllegalStateException("Unable to find alias for field " + entityField + " and DbFrom " + t);
					}
				}
			}
			// db calc function case
			else {
				if (entityField.getCalcAttributeDS() != null) {
					tableAlias = table.getAlias();
					final String dsSQL = ColumnToFieldVOMapping.createCalcAttributeSQL(entityField, table);
					return queryBuilder.plainExpression(field.getJavaClass(), 
								" (" + dsSQL + ")");
				} else {
					if (SF.OWNER.checkField(entityField.getEntity(), entityField.getUID())) {
						final DbFrom<?> t = table.findFrom(entityField.getEntity());
						return ColumnToRefFieldVOMapping.getOwnerSubselectColumn(entityField, t, queryBuilder);
					} else {
						String localFunctionCall = entityField.isLocalized() ? " , '" + dlCache.getLanguageToUse() + "'"  : "";
						tableAlias = table.getAlias();
						return queryBuilder.plainExpression(field.getJavaClass(), 
							queryBuilder.getDBAccess().getSchemaName() + "."
							+ entityField.getCalcFunction() + "(" + tableAlias + ".INTID" + localFunctionCall + ")");
					}
				}
			}
			if((entity.isDynamic() || entity.isChart()) && entityField.isDynamic()) {
				return table.columnCaseSensitive(tableAlias, entityField);
			}
			else {
				return table.column(tableAlias, entityField);
			}
		}
	}

	private DbExpression<?> getRefIdDbColumn(CollectableEntityField field) {
		if (!field.isReferencing()) {
			throw new IllegalArgumentException("Wrong field:" + field);
		}
		final IMetaProvider mdProv = MetaProvider.getInstance();
		final FieldMeta mdField = mdProv.getEntityField(field.getUID());
		final EntityMeta<?> entityOfField = mdProv.getEntity(mdField.getEntity());
		final EntityMeta<?> foreignEntity = mdProv.getEntity(mdField.getForeignEntity());
		final boolean uidEntity = foreignEntity.isUidEntity();
		final DbExpression<?> result;
		if (SF.OWNER.checkField(mdField.getEntity(), mdField.getUID())) {
			result = ColumnToFieldIdVOMapping.getOwnerIdColumn(mdField, table, false);
		} else {
			Class<?> _clazz = RigidUtils.uncheckedCast(uidEntity ? UID.class : Long.class);
			if (table.getEntity().equals(entityOfField)) {
				result = table.column(table.getAlias(), SimpleDbField.create(DbUtils.getDbIdFieldName(mdField, uidEntity), 
					_clazz));
			} else {
				// Look for the join the field refers to
				DbJoin<?> j = null;
				for (final Object o: table.getJoins()) {
					j = (DbJoin<?>) o;
					if (j.getEntity().equals(foreignEntity)) {
						break;
					}
				}
				if (j == null) {
					throw new IllegalArgumentException("Field " + field + " of entity " + foreignEntity + " is not joined.");
				}
				result = j.column(j.getAlias(), SimpleDbField.create(DbUtils.getDbIdFieldName(mdField, uidEntity), 
						_clazz));
			}
		}
		return result;
	}
	
	/**
	 * Some high-level data types are represented by more primitive values in the
	 * database.  For example, on the database layer {@link org.nuclos.common2.File}
	 * objects are treated as strings (paths).
	 */
	private Class<?> normalizeJavaType(Class<?> clazz) {
		if (clazz == org.nuclos.common2.File.class) {
			return String.class;
		}
		return clazz;
	}

	private Class<?> normalizeJavaType(String className) {
		try {
			return normalizeJavaType(RigidUtils.getClassLoaderThatWorksForWebStart().loadClass(className));
		}
		catch(ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}

	private DbExpression<?> normalizedLiteral(Object value) {
		if (value == RelativeDate.today()) {
			return queryBuilder.currentDate();
		} else if (value instanceof org.nuclos.common2.File) {
			value = ((org.nuclos.common2.File) value).getFilename();
		}
		return queryBuilder.literal(value);

	}

	private DbExpression<?> getDbIdColumn(CollectableEntityField field) {
		final FieldMeta<?> entityField;
		if (field instanceof CollectableEOEntityField) {
			entityField = ((CollectableEOEntityField) field).getMeta();
		} else {
			entityField = MetaProvider.getInstance().getEntityField(field.getUID());
		}
		final DbExpression<?> result;
		
		//TODO: NOAINT-592 entityField.isDynamic() is always false?!
		if (entity.isDynamic() && (entityField.isDynamic() || new DataSourceCaseSensivity(entity).isCaseSensitive(entityField))) {
			// dynamic entity case
			DbField<Long> dbf = SimpleDbField.create(DbUtils.getDbIdFieldName(entityField, false), Long.class);
			boolean bCaseSensitive = DbFrom.useCaseSensitiveConcerningUdGenericObject(entity.getDbTable(), dbf.getDbColumn());
			result = table.baseColumnSensitiveOrInsensitive(dbf, bCaseSensitive, false);
			
		} else if (entityField.getForeignEntity() != null || entityField.getUnreferencedForeignEntity() != null) {
			// foreign id field case
			if (SF.OWNER.checkField(entityField.getEntity(), entityField.getUID())) {
				result = ColumnToFieldIdVOMapping.getOwnerIdColumn(entityField, table, false);
			} else {
				final boolean uidEntity = MetaProvider.getInstance().getEntity(RigidUtils.defaultIfNull(
					entityField.getForeignEntity(), entityField.getUnreferencedForeignEntity())).isUidEntity();
				Class<?> _clazz = RigidUtils.uncheckedCast(uidEntity ? UID.class : Long.class);
				result = table.baseColumn(SimpleDbField.create(DbUtils.getDbIdFieldName(entityField, uidEntity),
						_clazz));
			}
		} else {
			// entityField is pk of entity
			final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(entityField.getEntity());
			result = table.baseColumn(eMeta);
			
		}
		return result;
	}
	
	private boolean usingJoinConditionWithLocalizedReferenceFields(RefJoinCondition refJoin) {
		boolean localizedFieldsAsRefStringifier = false;
		
		FieldUIDRefIterator it = new FieldUIDRefIterator(ForeignEntityFieldUIDParser.REF_PATTERN, refJoin.getField().getForeignEntityField());
		
		while(it.hasNext()) {
			IFieldUIDRef next = it.next();
			if (next.isUID()) {
				UID fieldRefStringifierUID = next.getUID();
				FieldMeta fmRef = MetaProvider.getInstance().getEntityField(fieldRefStringifierUID);
				if (fmRef.isLocalized()) {
					localizedFieldsAsRefStringifier = true;
					break;
				}
			}
		}
		return localizedFieldsAsRefStringifier;
	}

	public DataLanguageCache getDlCache() {
		return dlCache;
	}

	public List<Pair<UID, FieldMeta>> getAdditionalColumns() {
		return additionalColumns;
	}
	
}
