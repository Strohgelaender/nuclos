//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common2.exception.CommonFatalException;

public abstract class AbstractDalProcessor<DalVO extends IDalVO<PK>, PK> {

	private final UID entity;
	
	private final Class<DalVO> dalVOClzz;
	
	private final Class<PK> pkClzz;

	/**
	 * Data Types
	 */
	public static final Class<Boolean>	        DT_BOOLEAN	         = java.lang.Boolean.class;

	protected AbstractDalProcessor(UID entity, Class<DalVO> type, Class<PK> pkType) {
		this.entity = entity;
		this.dalVOClzz = type;
		pkClzz = pkType;
	}
	
	public UID getEntityUID() {
		return entity;
	}

	public final String getProcessor() {
		Class<?>[] interfaces = getClass().getInterfaces();
		if (interfaces.length == 0)
			return "<[none]>";
		return interfaces[0].getName();
	}

	public final Class<DalVO> getDalType() {
		return dalVOClzz;
	}
	
	public final Class<PK> getPkType() {
		return pkClzz;
	}

	protected DalVO newDalVOInstance() {
		try {
			DalVO newInstance = getDalType().getConstructor(UID.class).newInstance(getEntityUID());
			return newInstance;
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

}
