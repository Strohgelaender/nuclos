//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosLogicalUniqueViolationException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RecordGrantMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalReadVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.format.FormattingUIDTransformer;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.RecordGrantUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.dal.processor.ColumnToBeanVOMapping;
import org.nuclos.server.dal.processor.ColumnToFieldIdVOMapping;
import org.nuclos.server.dal.processor.ColumnToFieldVOMapping;
import org.nuclos.server.dal.processor.ColumnToRefFieldVOMapping;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.IColumnWithMdToVOMapping;
import org.nuclos.server.dal.processor.ProcessorConfiguration;
import org.nuclos.server.dal.processor.ProcessorFactorySingleton;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcWithFieldsDalProcessor;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbBusinessException;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.incubator.DbExecutor.LimitedResultSetRunner;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;
import org.nuclos.server.livesearch.ejb3.LiveSearchFacadeLocal;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.report.SchemaCache.NamedTableConstraintCacheEntry;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * TODO: @Autowired (and @Configurable ?) do not work here - but why?
 * Instead we hard-wire in ProcessorFactorySingleton at present. (tp)
 */
public class EntityObjectProcessor<PK>
		extends AbstractJdbcWithFieldsDalProcessor<EntityObjectVO<PK>, PK>
		implements JdbcEntityObjectProcessor<PK> {

	// static variables

	private static final Logger LOG = LoggerFactory.getLogger(EntityObjectProcessor.class);

	private final Collection<List<IColumnToVOMapping<?, PK>>> logicalUniqueConstraintCombinations = new ArrayList<>();

	// instance variables

	protected final EntityMeta<PK> eMeta;

	private final IColumnToVOMapping<PK, PK> pkColumn;
	private final IColumnToVOMapping<Integer, PK> versionColumn;

	private RecordGrantUtils grantUtils;
	private DatasourceServerUtils datasourceServerUtils;
	private MetaProvider metaProvider;
	private LiveSearchFacadeLocal searchFacade;

	private TableAliasSingleton tableAliasSingleton;

	private final ThreadLocal<Boolean> ignoreRecordGrantsAndOthers = new ThreadLocal<>();

	private final ThreadLocal<String> transactionId = new ThreadLocal<>();

	private final ThreadLocal<Boolean> thinReadEnabled = new ThreadLocal<>();

	public EntityObjectProcessor(ProcessorConfiguration<PK> config) {
		super(config.getMetaData().getUID(), (Class<EntityObjectVO<PK>>) config.getDalType(), config.getPkColumn() == null ? null : config.getPkColumn().getJavaClass(), config.getAllColumns());

		this.eMeta = config.getMetaData();
		this.pkColumn = config.getPkColumn();
		this.versionColumn = config.getVersionColumn();

		/**
		 * Logical Unique Constraints
		 */
		if (eMeta.getLogicalUniqueFieldCombinations() != null) {
			for (UID[] lufCombination : eMeta.getLogicalUniqueFieldCombinations()) {
				List<IColumnToVOMapping<?, PK>> ctovMappings =
						new ArrayList<>(lufCombination.length);

				for (UID luField : lufCombination) {
					for (IColumnToVOMapping<?, PK> ctovMap : allColumns) {
						if (!ctovMap.isReadonly() && luField.equals(ctovMap.getUID())) {
							ctovMappings.add(ctovMap);
						}
					}
				}
				logicalUniqueConstraintCombinations.add(ctovMappings);
			}
		}
	}

	private RecordGrantUtils getRecordGrantUtils() {
		if (grantUtils == null) {
			grantUtils = SpringApplicationContextHolder.getBean(RecordGrantUtils.class);
		}
		return grantUtils;
	}

	private DatasourceServerUtils getDatasourceServerUtils() {
		if (datasourceServerUtils == null) {
			datasourceServerUtils = SpringApplicationContextHolder.getBean(DatasourceServerUtils.class);
		}
		return datasourceServerUtils;
	}

	private MetaProvider getMetaProvider() {
		if (metaProvider == null) {
			metaProvider = MetaProvider.getInstance();
		}
		return metaProvider;
	}

	private LiveSearchFacadeLocal getSearchFacade() {
		if (searchFacade == null) {
			searchFacade = SpringApplicationContextHolder.getBean(LiveSearchFacadeLocal.class);
		}
		return searchFacade;
	}

	// @Autowired
	public void setTableAliasSingleton(TableAliasSingleton tableAliasSingleton) {
		this.tableAliasSingleton = tableAliasSingleton;
	}

	@Override
	public EntityMeta<PK> getMetaData() {
		return eMeta;
	}

	@Override
	protected IColumnToVOMapping<PK, PK> getPrimaryKeyColumn() {
		return pkColumn;
	}

	/* BLOCK A: Insert/Update/Delete - Block */

	private void addToIndexQueue(final IDalReadVO<PK> idrVO) {
		if (!MetaProvider.isDataEntity(getMetaData())) {
			return;
		}

		synchronized (transactionId) {
			if (transactionId.get() == null) {
				String transActionNameShort = TransactionSynchronizationManager.getCurrentTransactionName().replaceFirst("org\\.nuclos\\.server\\.", "");
				transactionId.set(transActionNameShort + Thread.currentThread().hashCode());
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public void afterCompletion(int status) {
						synchronized (transactionId) {
							getSearchFacade().transactionComplete(transactionId.get(), status == TransactionSynchronization.STATUS_COMMITTED, true);

							//In any case, remove this Transaction from ThreadLocal
							transactionId.set(null);
						}
					}
				});
			}

			getSearchFacade().sendVOintoQueue(idrVO, transactionId.get());
		}
	}

	@Override
	public Object insertOrUpdate(EntityObjectVO<PK> dalVO) throws DbException {
		return this.insertOrUpdateWithOrWithoutForce(dalVO, false);
	}

	/**
	 * @param dalVO
	 * @param force true: do not check logical unique constraints
	 *              false: check!
	 */
	public Object insertOrUpdateWithOrWithoutForce(EntityObjectVO<PK> dalVO, boolean force) {
		Object pk = super.insertOrUpdate(dalVO);

		if (!force) {
			final DalCallResult result = new DalCallResult();
			checkLogicalUniqueConstraint(result, dalVO);
			result.throwFirstException();
		}

		insertOrUpdateLanguageData(dalVO);
		addToIndexQueue(dalVO);
		return pk;
	}

	@Override
	public void delete(Delete<PK> id) throws DbException {
		deleteLanguageData(id);
		super.delete(id);
		addToIndexQueue(id);
		if (eMeta.isOwner()) {
			this.unlock(id.getPrimaryKey());
		}
	}

	private void insertOrUpdateLanguageData(EntityObjectVO<PK> dalVO) {
		if (dalVO.getDataLanguageMap() == null) {
			return;
		}

		Map<UID, DataLanguageLocalizedEntityEntry> dlMapField = dalVO.getDataLanguageMap().getLanguageMap();
		for (UID language : dlMapField.keySet()) {
			DataLanguageLocalizedEntityEntry value = dlMapField.get(language);
			if (value != null) {
				if (value.getEntityEntryPrimaryKey() == null) {
					value.setEntityEntryPrimaryKey((Long) dalVO.getPrimaryKey());
				}

				if (value.getPrimaryKey() != null) {
					value.flagUpdate();
				}

				if (value.isFlagNew()) {
					value.setCreatedAt(InternalTimestamp.toInternalTimestamp(new Date()));
					value.setChangedAt(InternalTimestamp.toInternalTimestamp(new Date()));
					value.setCreatedBy("nuclos");
					value.setChangedBy("nuclos");
					value.setVersion(1);
					value.setPrimaryKey(dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE));
				}

				NucletDalProvider.getInstance().getEntityObjectProcessor(value.getDalEntity()).insertOrUpdate(value);
			}
		}
	}

	private void deleteLanguageData(Delete<PK> d) {
		if (d.getDalEntity() != null) {
			UID langDataUID = NucletEntityMeta.getEntityLanguageUID(d.getDalEntity());
			UID langDataRefFieldUID = DataLanguageServerUtils.extractForeignEntityReference(d.getDalEntity());

			if (getMetaProvider().hasEntity(langDataUID)) {
				IEntityObjectProcessor<PK> dlprocessor = NucletDalProvider.getInstance().getEntityObjectProcessor(langDataUID);
				CollectableSearchExpression cse = new CollectableSearchExpression(SearchConditionUtils.newComparison(langDataRefFieldUID, ComparisonOperator.EQUAL, d.getPrimaryKey()));

				for (PK foundDataLangEntry : dlprocessor.getIdsBySearchExpression(cse)) {
					dlprocessor.delete(new Delete<PK>(foundDataLangEntry, langDataUID));
				}
			}
		}
	}

	private void checkLogicalUniqueConstraint(DalCallResult result, EntityObjectVO<PK> dalVO) {
		if (!logicalUniqueConstraintCombinations.isEmpty()) {
			for (List<IColumnToVOMapping<?, PK>> columns : logicalUniqueConstraintCombinations) {
				final Map<IColumnToVOMapping<?, PK>, Object> checkValues = super.getColumnValuesMapWithMapping(columns, dalVO);
				final NuclosLogicalUniqueViolationException checkResult = super.checkLogicalUniqueConstraint(checkValues, dalVO.getPrimaryKey());
				if (checkResult != null)
					result.addDbException(dalVO.getPrimaryKey(), null, checkResult);
			}
		}
	}

	/* End BLOCK A: Insert/Update/Delete - Block */

	/* BLOCK B: Select - Block */

	/**
	 * Includes joins for (former) views.
	 */
	@Override
	public List<EntityObjectVO<PK>> getAll() {
		return getBySearchExpression(CollectableSearchExpression.TRUE_SEARCH_EXPR);
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(PK id) {
		return getByPrimaryKey(id, null);
	}

	/**
	 * Includes joins for (former) views.
	 */
	@Override
	public EntityObjectVO<PK> getByPrimaryKey(PK id, Collection<FieldMeta<?>> fields) {
		return CollectionUtils.getSingleIfExist(getBySearchExpressionAndPrimaryKeysImpl(CollectableSearchExpression.TRUE_SEARCH_EXPR, Collections.singletonList(id), fields));
	}

	/**
	 * Includes joins for (former) views.
	 */
	@Override
	public List<EntityObjectVO<PK>> getByPrimaryKeys(List<PK> pks) {
		return getBySearchExpressionAndPrimaryKeysImpl(CollectableSearchExpression.TRUE_SEARCH_EXPR, pks, null);
	}

	/**
	 * Includes joins for (former) views.
	 * <p>
	 * Attention: In contrast to getChunkBySearchExcpression here is no automatic addition for systemfields except "id"
	 * When using "fields" the caller is responsible for the any field
	 * //TODO: Find a consistent way for both methods and implement
	 */
	public List<EntityObjectVO<PK>> getBySearchExpressionAndPrimaryKeysImpl(CollectableSearchExpression clctexpr, List<PK> pks, Collection<FieldMeta<?>> fields) {
		List<IColumnToVOMapping<?, PK>> columns = new ArrayList<>(fields == null ? getDefaultColumns() : getColumnsByMeta(fields));

		if (fields != null) {
			Set<UID> sfields = new HashSet<>();
			for (FieldMeta<?> efmdv : fields) sfields.add(efmdv.getUID());
			columns = new ArrayList<>();
			IColumnToVOMapping<?, PK> primaryCol = getPrimaryKeyColumn();

			for (IColumnToVOMapping<?, PK> col : allColumns) {
				if (col == primaryCol || sfields.contains(col.getUID())) {
					columns.add(col);
				}
			}
		}

		boolean bHasAllColums = hasAllColumns(columns);

		DbQuery<Object[]> query = createOptimalQueryWithJoinsOrSubselects(clctexpr, columns, true);

		DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
		DbExpression<?> pkExpr = getPrimaryKeyColumn().getDbColumn(from);
		Transformer<Object[], EntityObjectVO<PK>> transformer = createResultTransformer(columns);

		List<EntityObjectVO<PK>> result = new ArrayList<>(pks.size());

		for (List<PK> pkSubList : CollectionUtils.splitEvery(pks,
				query.getBuilder().getInLimit())) {
			query.addToWhereAsAnd(pkExpr.as(getPkType()).in(pkSubList));
			result.addAll(dataBaseHelper.getDbAccess().executeQuery(query, transformer));
		}

		for (EntityObjectVO<PK> eo : result) {
			eo.setComplete(bHasAllColums);
		}

		return result;
	}

	@Override
	public List<EntityObjectVO<PK>> getChunkBySearchExpressionImpl(CollectableSearchExpression clctexpr, ResultParams resultParams) {

		Collection<UID> fields = resultParams.getFields();
		List<IColumnToVOMapping<?, PK>> columns;

		if (fields != null) {
			Collection<UID> colFields = new HashSet<>(fields);

			for (IColumnToVOMapping<?, PK> col : allColumns) {

				if (colFields.contains(col.getUID())) {
					continue;
				}

				// This adds the most basic system fields like PK, DATCREATED and INTVERSION, if there are not included yet
				if (col.getClass() == ColumnToBeanVOMapping.class) {
					colFields.add(col.getUID());
					continue;
				}

				FieldMeta<?> fMeta;
				if (col instanceof IColumnWithMdToVOMapping) {
					fMeta = ((IColumnWithMdToVOMapping<?, PK>) col).getMeta();
				} else if (col instanceof ColumnToFieldIdVOMapping) {
					fMeta = ((ColumnToFieldIdVOMapping<?, PK>) col).getMeta();
				} else {
					continue;
				}

				// This add any other system field and nuclosRowColor, if available (NUCLOS-6068 Part 1)
				if (fMeta.isSystemField() || fMeta.isNuclosRowColor()) {
					colFields.add(col.getUID());
				}
			}

			columns = getColumns(colFields);

		} else {
			columns = new ArrayList<>(getDefaultColumns());
		}

		return getChunk(columns, clctexpr, resultParams);
	}

	private List<EntityObjectVO<PK>> getChunk(List<IColumnToVOMapping<?, PK>> columns, CollectableSearchExpression clctexpr, ResultParams resultParams) {
		boolean bHasAllColumns = hasAllColumns(columns);

		DbQuery<Object[]> query = createOptimalQueryWithJoinsOrSubselects(clctexpr, columns, false);

		if (!E.isNuclosEntity(getEntityUID()) && usePrimaryKeySubSelect()) {
			// NUCLOS-5779 Gain performance by using a sub-query for fetching the primary keys
			DbQuery<PK> subquery = getIdQuery(clctexpr, ignoreRecordGrantsAndOthers.get());
			setLimitAndOffset(subquery, resultParams);

			DbFrom<?> from = query.getRoots().iterator().next();
			DbCondition subcondition = query.getBuilder().in(from.baseColumn(getPrimaryKeyColumn()), subquery);
			query.replaceWhere(subcondition);

		} else {
			setLimitAndOffset(query, resultParams);
		}

		Transformer<Object[], EntityObjectVO<PK>> transformer = createResultTransformer(columns);
		List<EntityObjectVO<PK>> result = new ArrayList<>();

		result.addAll(dataBaseHelper.getDbAccess().executeQuery(query, transformer));

		boolean resetNextFireTime = E.JOBCONTROLLER.getUID().equals(getEntityUID()) &&
				NuclosLocalServerSession.getInstance().isAutomaticJobStartOff();

		for (EntityObjectVO<PK> eo : result) {
			eo.setComplete(bHasAllColumns);
			// NUCLOS-6708
			if (resetNextFireTime && "Aktiviert".equals(eo.getFieldValue(E.JOBCONTROLLER.laststate))) {
				eo.setFieldValue(E.JOBCONTROLLER.nextfiretime, "AutoStart=OFF");
			}
		}

		return result;
	}

	private <T> void setLimitAndOffset(DbQuery<T> query, ResultParams resultParams) {
		Long lLimit = resultParams.getLimit();
		if (lLimit == null && utils.isCalledRemotely()) {
			lLimit = LimitedResultSetRunner.MAXFETCHSIZE.longValue();
		}

		if (lLimit != null) {
			query.limit(lLimit);
		}

		Long lOffset = resultParams.getOffset();
		if (lOffset != null && lOffset >= 0) {

			//NUCLOS-5302 for now only if there is no sorting.
			if (lOffset > 0 && resultParams.getAnchor().size() == 1) {
				CollectableIdCondition cic = (CollectableIdCondition) resultParams.getAnchor().get(0);
				String sPkCompare = getMetaData().isUidEntity() ? "t.STRUID > '$1'" : "t.INTID > $1";
				DbCondition cond = dataBaseHelper.getDbAccess().getQueryBuilder().plainCondition(sPkCompare.replace("$1", cic.getId().toString()));
				query.addToWhereAsAnd(cond);
			} else if (lOffset > 0 || dataBaseHelper.getDbAccess().withOffsetZero()) {
				query.offset(lOffset);
			}
		}
	}

	/**
	 * getSlimChunk, a method to get only the requested columns of a DB-Table
	 *
	 * @param clctexpr SearchExpression, same as else
	 * @param resultParams   ResultParams
	 * @return List of EntityObjectVO
	 */
	public List<EntityObjectVO<PK>> getSlimChunk(CollectableSearchExpression clctexpr, ResultParams resultParams) {
		return getChunk(getColumns(resultParams.getFields()), clctexpr, resultParams);
	}

	/**
	 * Includes joins for (former) views.
	 */
	@Override
	public List<EntityObjectVO<PK>> getBySearchExpression(CollectableSearchExpression clctexpr) {
		return getBySearchExprResultParams(clctexpr, ResultParams.DEFAULT_RESULT_PARAMS);
	}

	/**
	 * Includes joins for (former) views.
	 */

	@Override
	public List<EntityObjectVO<PK>> getBySearchExprResultParams(CollectableSearchExpression clctexpr, ResultParams resultParams) {
		Collection<UID> fields = resultParams.getFields();
		List<IColumnToVOMapping<?, PK>> columns = fields == null ? getDefaultColumns() : getColumns(fields);

		if (fields != null && !columns.contains(pkColumn)) {
			columns.add(pkColumn);
		}

		boolean bHasAllColumns = hasAllColumns(columns);

		DbQuery<Object[]> query = createOptimalQueryWithJoinsOrSubselects(clctexpr, columns, true);

		setLimitAndOffset(query, resultParams);

		List<EntityObjectVO<PK>> result = dataBaseHelper.getDbAccess().executeQuery(query, createResultTransformer(columns));

		for (EntityObjectVO<PK> eo : result) {
			eo.setComplete(bHasAllColumns);
		}
		return result;
	}

	/**
	 * Includes joins for (former) views (needed for sorting order).
	 */
	@Override
	public List<PK> getIdsBySearchExpression(CollectableSearchExpression clctexpr) {
		return dataBaseHelper.getDbAccess().executeQuery(getIdQuery(clctexpr, ignoreRecordGrantsAndOthers.get()));
	}

	@Override
	public List<PK> getAllIds() {
		DbQuery<PK> query = createSingleColumnQuery(getPrimaryKeyColumn(), true);
		return dataBaseHelper.getDbAccess().executeQuery(query);
	}

	@Override
	public Integer getVersion(PK pk) {
		if (pk == null || versionColumn == null)
			return null;

		DbQuery<Integer> query = createSingleColumnQuery(versionColumn, true);
		DbFrom<PK> from = (DbFrom<PK>) CollectionUtils.getFirst(query.getRoots());
		query.where(query.getBuilder().equalValue(getPrimaryKeyColumn().getDbColumn(from), pk));
		try {
			return dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		} catch (DbInvalidResultSizeException e) {
			if (e.wasEmpty()) {
				throw new CommonFatalException("No record with pk " + pk + " in table " + eMeta.getDbTable());
			} else {
				throw new CommonFatalException("Primary key is not unique!");
			}
		}
	}

	@Override
	public Long count(CollectableSearchExpression clctexpr) {
		CollectableSearchCondition condition = getSearchConditionWithDeletedAndVLP(clctexpr);
		final RecordGrantMeta rgMeta = getAssignedRecordgrantMeta(null);

		//NUCLOS-5241 Estimated count doesn't work for dynamic entities and charts and are not necessary either
		//NUCLOS-5947 Estimated count is now optional and disabled by default
		if (getMetaData().isDynamic() || getMetaData().isChart() ||
				!ServerParameterProvider.getInstance().isEnabled(ParameterProvider.KEY_ESTIMATE_COUNT)) {
			return countExact(rgMeta, condition);
		}

		boolean bNoCondition = condition == null && rgMeta == null;
		DbQuery<PK> query2Estimate = bNoCondition ? null : getSimpleIdQuery(rgMeta, condition);

		Long estimatedCount = dataBaseHelper.getDbAccess().estimateCount(eMeta, query2Estimate);

		if (estimatedCount != null) {
			return estimatedCount;
		}

		return countExact(rgMeta, condition);
	}

	private Long countExact(final RecordGrantMeta rgMeta, CollectableSearchCondition condition) {
		DbQuery<Long> query = createCountQuery(getPrimaryKeyColumn());
		addJoinsForRecordgrant(query, rgMeta);

		EOSearchExpressionUnparser unparser = new EOSearchExpressionUnparser(query, eMeta, utils, getDataLangCache());
		unparser.unparseSearchCondition(condition);

		return dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
	}

	private DbQuery<PK> getSimpleIdQuery(final RecordGrantMeta rgMeta, CollectableSearchCondition condition) {
		DbQuery<PK> query = createSingleColumnQuery(getPrimaryKeyColumn(), true);
		addJoinsForRecordgrant(query, rgMeta);

		EOSearchExpressionUnparser unparser = new EOSearchExpressionUnparser(query, eMeta, utils, getDataLangCache());
		unparser.unparseSearchCondition(condition);

		return query;
	}

	/* End BLOCK B: Select - Block */

	private DbQuery<PK> getIdQuery(CollectableSearchExpression clctexpr, Boolean bIgnoreRecordGrants) {
		DbQuery<PK> query = createSingleColumnQuery(getPrimaryKeyColumn(), false);
		if (clctexpr.getLimit() != null) {
			query.limit(clctexpr.getLimit());
		}

		RecordGrantMeta rgMeta = null;

		if (!Boolean.TRUE.equals(bIgnoreRecordGrants)) {

			List<IColumnToVOMapping<?, PK>> pkColumnsList = new ArrayList<>();
			pkColumnsList.add(getPrimaryKeyColumn());

			rgMeta = getAssignedRecordgrantMeta(pkColumnsList);

		}

		// we modify the search expr here, too.
		clctexpr = composeSearchCondition(clctexpr, new ArrayList<>());

		addSearchExpressionAndGrantsToQuery(clctexpr, query, rgMeta, allColumns);

		return query;
	}

	private boolean addSearchExpressionAndGrantsToQuery(CollectableSearchExpression clctexpr, DbQuery<?> query,
														RecordGrantMeta rgMeta, List<IColumnToVOMapping<?, PK>> columns) {

		final boolean bRecordGrantsJoined = addJoinsForRecordgrant(query, rgMeta);

		EOSearchExpressionUnparser unparser = new EOSearchExpressionUnparser(query, eMeta, utils, getDataLangCache());
		unparser.unparseSearchCondition(getSearchConditionWithDeletedAndVLP(clctexpr));

		for (Pair<UID, FieldMeta> column : unparser.getAdditionalColumns()) {
			columns.add(ProcessorFactorySingleton.createLanguageFieldMapping(
					SystemFields.BASE_ALIAS, column.getX(), column.getY(), false, false));
		}

		final DbFrom<PK> from = query.getDbFrom();
		DbExpression<PK> primaryCol = pkColumn.getDbColumn(from);
		unparser.unparseSortingOrder(clctexpr.getSortingOrder(), dataLangCache.getLanguageToUse(), primaryCol);
		return bRecordGrantsJoined;
	}

	private DbQuery<Object[]> createOptimalQueryWithJoinsOrSubselects(CollectableSearchExpression clctexpr,
																	  List<IColumnToVOMapping<?, PK>> columns,
																	  boolean bAllLanguages) {
		final RecordGrantMeta rgMeta = getAssignedRecordgrantMeta(columns);

		List<IColumnToVOMapping<?, PK>> referenceColumns = getReferenceColumns(columns);
		Collection<IColumnWithMdToVOMapping<?, PK>> subSelectableReferenceColumns = getSubselectableReferenceColumns(referenceColumns, clctexpr);

		DbQuery<Object[]> query = createQuery(columns, bAllLanguages, subSelectableReferenceColumns);

		referenceColumns.removeAll(subSelectableReferenceColumns);

		List<CollectableSearchCondition> joins = getJoinsForReferenceColumns(referenceColumns);
		// search expression will be modified
		clctexpr = composeSearchCondition(clctexpr, joins);

		addSearchExpressionAndGrantsToQuery(clctexpr, query, rgMeta, columns);

		return query;
	}

	private List<IColumnToVOMapping<?, PK>> getDefaultColumns() {
		List<IColumnToVOMapping<?, PK>> result = new ArrayList<>(allColumns.size());

		final boolean skipCalculated = isThinReadEnabled();
		final boolean skipStringifiedReferences = isThinReadEnabled();

		for (IColumnToVOMapping<?, PK> col : allColumns) {
			boolean add = true;

			if (col instanceof ColumnToFieldVOMapping) {
				FieldMeta<?> fieldMeta = ((ColumnToFieldVOMapping<?, PK>) col).getMeta();

				if (fieldMeta.isCalcOndemand()) {
					add = false;
				} else if (skipCalculated && fieldMeta.isCalculated()) {
					add = false;
				}
			} else if (col instanceof ColumnToRefFieldVOMapping) {
				if (skipStringifiedReferences) {
					add = false;
				}
			}

			if (add) {
				result.add(col);
			}
		}
		return result;
	}

	private Collection<IColumnWithMdToVOMapping<?, PK>> getSubselectableReferenceColumns(
			List<IColumnToVOMapping<?, PK>> referenceColumns, CollectableSearchExpression clctexpr) {
		Collection<IColumnWithMdToVOMapping<?, PK>> ret = new HashSet<>();

		if (referenceColumns.isEmpty() || !useReferenceSubSelect()) {
			return ret;
		}

		Collection<UID> fieldsNotToBeSubselected = new HashSet<>();

		if (clctexpr != null && !CollectableSearchExpression.TRUE_SEARCH_EXPR.equals(clctexpr)) {
			//if (clctexpr.getValueListProviderDatasource() != null) {
			//	return ret; //TODO: Check whether to accept or not
			//}

			for (AtomicCollectableSearchCondition atomic : clctexpr.getAllAtomicConditions()) {
				boolean bSameEntity = atomic.compareEntity(getMetaData().getUID());

				if (bSameEntity && atomic.isComparandAnId()) {
					//An Id comparison within the same entity will not interfere with the stringified reference
					continue;
				}
				final FieldMeta<?> atomicFieldMeta = getMetaProvider().getEntityField(atomic.getFieldUID());
				if (SF.OWNER.checkField(atomicFieldMeta.getEntity(), atomicFieldMeta.getUID())) {
					// No join for owner (first calculated ref field!)... use subselect...
					continue;
				}
				fieldsNotToBeSubselected.add(atomic.getFieldUID());
				//TODO: There are quite some kinds of comparisons where the field still can be subselected
			}

			for (CollectableSorting sorting : clctexpr.getSortingOrder()) {
				final FieldMeta<?> sortingFieldMeta = getMetaProvider().getEntityField(sorting.getField());
				if (SF.OWNER.checkField(sortingFieldMeta.getEntity(), sortingFieldMeta.getUID())) {
					// No join for owner (first calculated ref field!)... use subselect...
					continue;
				}
				fieldsNotToBeSubselected.add(sorting.getField());
			}

		}

		for (IColumnToVOMapping<?, PK> column : referenceColumns) {

			if (column.getUID() != null && getMetaProvider().checkEntityField(column.getUID())) {
				final FieldMeta<?> fieldMeta = getMetaProvider().getEntityField(column.getUID());
				// Do not subselect localized columns!
				if (fieldMeta.getForeignEntity() != null && fieldMeta.getForeignEntityField() != null) {
					boolean bContinue = false;
					for (UID stringifiedFieldUID : RefValueExtractor.getAttributes(fieldMeta.getForeignEntityField())) {
						final FieldMeta<?> stringifiedFieldMeta = getMetaProvider().getEntityField(stringifiedFieldUID);
						if (stringifiedFieldMeta.isLocalized()) {
							bContinue = true;
							break;
						}
					}
					if (bContinue) {
						continue;
					}
				}
			}

			if (column instanceof IColumnWithMdToVOMapping) {
				IColumnWithMdToVOMapping<?, PK> col = (IColumnWithMdToVOMapping<?, PK>) column;

				if (fieldsNotToBeSubselected.contains(col.getUID())) {
					continue;
				}

				if (tableAliasSingleton.getRefJoinCondition(col.getMeta()).size() == 1) {
					ret.add(col);
				}
			}
		}

		return ret;
	}

	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<PK> dalVO) throws DbException {
		final DalCallResult result = new DalCallResult();
		checkLogicalUniqueConstraint(result, dalVO);
		result.throwFirstException();
	}

	private CollectableSearchCondition getSearchConditionWithDeletedAndVLP(CollectableSearchExpression clctexpr) {
		CollectableSearchCondition result = null;
		if (clctexpr.getValueListProviderDatasource() != null) {
			try {
				if (clctexpr.getValueListProviderDatasource().getValid())
					result = getDatasourceServerUtils().getConditionWithIdForInClause(
							clctexpr.getValueListProviderDatasource(),
							clctexpr.getValueListProviderDatasourceParameter(),
							clctexpr.getMandator(),
							getDatasourceServerUtils().getDataLanguage());
			} catch (NuclosDatasourceException e) {
				throw new NuclosFatalException("datasource.error.valuelistprovider.invalid", e);
			}
		}

		UID entityUID = getEntityUID();

		if (clctexpr instanceof CollectableGenericObjectSearchExpression) {
			final CollectableGenericObjectSearchExpression clctGOexpr = (CollectableGenericObjectSearchExpression) clctexpr;
			if (!clctGOexpr.getSearchDeleted().equals(CollectableGenericObjectSearchExpression.SEARCH_BOTH)) {
				final Boolean bSearchDeleted = CollectableGenericObjectSearchExpression.SEARCH_DELETED == clctGOexpr.getSearchDeleted();
				final CollectableSearchCondition condSearchDeleted = org.nuclos.common.SearchConditionUtils.newComparison(SF.LOGICALDELETED.getUID(entityUID), ComparisonOperator.EQUAL, bSearchDeleted);
				result = result != null ? SearchConditionUtils.and(condSearchDeleted, result) : condSearchDeleted;
			}
		}

		NucletDalProvider dalProvider = null;
		try {
			dalProvider = NucletDalProvider.getInstance();
		} catch (Exception ex) {
			// too early .. ignore here
			// internal use of processor (e.g. building class sources)
		}
		if (dalProvider != null) {
			if (dalProvider.getAccessibleMandators() != null
					&& getMetaProvider().getEntity(entityUID).isMandator()
					&& SecurityCache.getInstance().isMandatorPresent()) {

				Set<UID> accessibleMandators = dalProvider.getAccessibleMandators();
				final CollectableSearchCondition condMandators;
				if (accessibleMandators.isEmpty()) {
					// could it be?
					condMandators = SearchConditionUtils.newIsNullCondition(SF.MANDATOR_UID.getMetaData(entityUID));
				} else {
					condMandators = new CollectableInIdCondition<>(
							SearchConditionUtils.newEntityField(SF.MANDATOR_UID.getMetaData(entityUID)),
							new ArrayList<>(accessibleMandators));
				}
				result = result != null ? SearchConditionUtils.and(condMandators, result) : condMandators;
			}
		}

		if (result == null) {
			result = clctexpr.getSearchCondition() == null ? null : setLocalizationSearchConditions(clctexpr.getSearchCondition());
		} else {
			result = clctexpr.getSearchCondition() == null ? result : SearchConditionUtils.and(setLocalizationSearchConditions(clctexpr.getSearchCondition()), result);
		}

		if (!E.isNuclosEntity(entityUID)) {
			if (!Boolean.TRUE.equals(ignoreRecordGrantsAndOthers.get())) {
				result = getRecordGrantUtils().appendCompulsorySearchFilters(result, entityUID);
			}
		}

		return result;
	}

	private CollectableSearchCondition setLocalizationSearchConditions(CollectableSearchCondition sc) {

		List<CollectableSearchCondition> toAdd = new ArrayList<>();
		List<CollectableSearchCondition> toRemove = new ArrayList<>();

		if (sc instanceof CompositeCollectableSearchCondition) {
			CompositeCollectableSearchCondition csc = (CompositeCollectableSearchCondition) sc;
			for (CollectableSearchCondition curCS : csc.getOperands()) {
				if (curCS instanceof CollectableLikeCondition) {
					CollectableLikeCondition acsc = (CollectableLikeCondition) curCS;
					if (acsc.getEntityField().isLocalized() && !acsc.getEntityField().isCalculated()) {
						toRemove.add(acsc);
						toAdd.add(org.nuclos.common.SearchConditionUtils.newLikeCondition(
								DataLanguageServerUtils.extractFieldUID(acsc.getFieldUID()), acsc.getLikeComparand()));
					}
				}
			}
			for (CollectableSearchCondition c : toRemove) {
				csc.removeOperand(c);
			}
			csc.addAllOperands(toAdd);
		} else if (sc instanceof CollectableLikeCondition) {
			CollectableLikeCondition acsc = (CollectableLikeCondition) sc;
			if (acsc.getEntityField().isLocalized() && !acsc.getEntityField().isCalculated()) {
				sc = org.nuclos.common.SearchConditionUtils.newLikeCondition(
						DataLanguageServerUtils.extractFieldUID(acsc.getFieldUID()), acsc.getLikeComparand());
			}

		}
		return sc;
	}

	private List<IColumnToVOMapping<?, PK>> getColumnsByMeta(final Collection<FieldMeta<?>> fields) {
		Collection<UID> fieldUIDs = new ArrayList<>();
		for (FieldMeta<?> fMeta : fields) {
			fieldUIDs.add(fMeta.getUID());
		}
		return getColumns(fieldUIDs);
	}

	private List<IColumnToVOMapping<?, PK>> getColumns(final Collection<UID> fields) {
		final Set<UID> notFound = new HashSet<>(fields);
		List<IColumnToVOMapping<?, PK>> result =
				CollectionUtils.select(new ArrayList<>(allColumns), (IColumnToVOMapping<?, PK> column) -> {
					notFound.remove(column.getUID());
					return fields.contains(column.getUID());
				});
		for (UID nfUID : notFound) {
			if (CalcAttributeUtils.isCalcAttributeCustomization(nfUID)) {
				try {
					result.add(new ColumnToFieldVOMapping<>(SystemFields.BASE_ALIAS, getMetaProvider().getEntityField(nfUID), true, false));
				} catch (ClassNotFoundException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}

	private CollectableSearchExpression composeSearchCondition(CollectableSearchExpression clctexpr, List<CollectableSearchCondition> joins) {
		List<CollectableSorting> sortingOrder = addJoinsForSortingOrder(clctexpr, joins);
		CollectableSearchCondition searchCondition = clctexpr.getSearchCondition();

		if (joins != null && !joins.isEmpty()) {
			if (searchCondition != null) {
				joins.add(searchCondition);
			}

			searchCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND, joins);
		}

		//NUCLOS-4716: Make a copy, because a new sorting would alter the original SearchExpression		
		CollectableSearchExpression result;

		if (clctexpr instanceof CollectableGenericObjectSearchExpression) {
			// we need this. because others will check instance and add SEARCH_DELETED vlp or something like that.
			result = new CollectableGenericObjectSearchExpression(searchCondition, sortingOrder, ((CollectableGenericObjectSearchExpression) clctexpr).getSearchDeleted());

		} else {
			result = new CollectableSearchExpression(searchCondition, sortingOrder);

		}

		// reset vlps, mandator, etc.
		result.setIncludingSystemData(clctexpr.isIncludingSystemData());
		result.setValueListProviderDatasource(clctexpr.getValueListProviderDatasource());
		result.setValueListProviderDatasourceParameter(clctexpr.getValueListProviderDatasourceParameter());
		result.setMandator(clctexpr.getMandator());
		return result;
	}

	private List<IColumnToVOMapping<?, PK>> getReferenceColumns(final List<IColumnToVOMapping<?, PK>> columns) {
		final List<IColumnToVOMapping<?, PK>> result = new ArrayList<>();

		for (IColumnToVOMapping<?, PK> m : columns) {
			boolean add = false;
			final String f = m.getColumn();
			if (m instanceof IColumnWithMdToVOMapping) {
				final IColumnWithMdToVOMapping<?, PK> mapping = (IColumnWithMdToVOMapping<?, PK>) m;
				if (!mapping.constructJoinForStringifiedRefs()) {
					continue;
				}

				final FieldMeta<?> meta = mapping.getMeta();
				final UID fentity = LangUtils.firstNonNull(meta.getForeignEntity(), meta.getUnreferencedForeignEntity());
				if (fentity != null) {
					add = true;
				}
			}
			// ???
			else if (f.startsWith("STRVALUE_") || f.startsWith("INTVALUE_") || f.startsWith("OBJVALUE_")) {
				add = true;
			}

			if (add) {
				result.add(m);
			}
		}
		return result;
	}

	private List<CollectableSearchCondition> getJoinsForReferenceColumns(final List<IColumnToVOMapping<?, PK>> columns) {
		final List<CollectableSearchCondition> result = new ArrayList<>();
		if (eMeta.getReadDelegate() != null) {
			return result;
		}

		for (IColumnToVOMapping<?, PK> m : columns) {
			String alias;
			if (m instanceof IColumnWithMdToVOMapping) {
				final IColumnWithMdToVOMapping<?, PK> mapping = (IColumnWithMdToVOMapping<?, PK>) m;
				final FieldMeta<?> meta = mapping.getMeta();
				if (meta.isCalculated()) {
					continue;
				}
				alias = tableAliasSingleton.getAlias(meta);
			}
			// ???
			else {
				alias = tableAliasSingleton.getAlias(m);
			}

			LOG.debug("getJoinsForColumns: apply RefJoinCondition for {} alias {}", m, alias);
			result.addAll(tableAliasSingleton.getRefJoinCondition(m));
		}
		return result;
	}

	private List<CollectableSorting> addJoinsForSortingOrder(CollectableSearchExpression searchExpression, List<CollectableSearchCondition> result) {
		final List<CollectableSorting> sortingOrder = searchExpression.getSortingOrder();

		if (sortingOrder == null) {
			return null;
		}

		// 
		// Also add join conditions needed by sorting order. (tp)
		final List<CollectableSorting> newSortingOrder = new ArrayList<>(sortingOrder.size());
		final UID baseEntity = eMeta.getUID();
		for (CollectableSorting cs : sortingOrder) {
			UID sortingEntity = cs.getEntity();

			// This could happen only for the base entity, for foreign entity fields ('STRVALUE_...'),
			// because sorting is only available for the base entity.

			// In case there is no entity given we can safely assume the it's the baseEntity
			if (sortingEntity == null || sortingEntity.equals(baseEntity)) {

				// not needed. otherwise we can not sort fields referencing baseEntity. references to entity it self... @see NUCLOS-1472
				//assert cs.getTableAlias().equals(SystemFields.BASE_ALIAS); 
				final FieldMeta<?> mdField = getMetaProvider().getEntityField(cs.getField());
				final UID fentity = mdField.getForeignEntity();
				if (fentity == null || mdField.isCalculated()) {
					// copy sorting order
					newSortingOrder.add(cs);
					continue;
				}

				final List<RefJoinCondition> joins = tableAliasSingleton.getRefJoinCondition(mdField);
				final RefJoinCondition join = joins.get(0);

				if (!result.contains(join)) {
					result.add(join);
				}

				// retrieve sorting order
				for (IFieldUIDRef ref : new ForeignEntityFieldUIDParser(mdField, getMetaProvider())) {
					if (ref.isUID()) {
						FieldMeta<?> refFieldPart = getMetaProvider().getEntityField(ref.getUID());
						if (refFieldPart.getForeignEntity() != null) {
							for (IFieldUIDRef ref2 : new ForeignEntityFieldUIDParser(refFieldPart, getMetaProvider())) {
								if (ref2.isUID()) {
									FieldMeta<?> refFieldPart2 = getMetaProvider().getEntityField(ref2.getUID());
									if (refFieldPart2.getForeignEntity() == null) {
										RefJoinCondition join2 = null;
										for (int i = 1; i < joins.size(); i++) {
											RefJoinCondition iJoin = joins.get(i);
											if (ref.getUID().equals(iJoin.getField().getUID())) {
												join2 = iJoin;
											}
										}
										if (join2 != null) {
											newSortingOrder.add(new CollectableSorting(
													join2.getTableAliasRight(), refFieldPart.getForeignEntity(), false, ref2.getUID(), cs.isAscending()));
										}
									}
								}
							}
						} else {
							if (refFieldPart.isLocalized() && refFieldPart.getCalcFunction() == null) {
								EntityMeta<?> emRefFieldEntity = getMetaProvider().getEntity(refFieldPart.getEntity());
								EntityMeta<?> emRefEntityLang = getMetaProvider().getEntity(NucletEntityMeta.getEntityLanguageUID(refFieldPart.getEntity()));

								FieldMeta<?> refForeignRefLangMeta = emRefEntityLang.getField(
										DataLanguageServerUtils.extractForeignEntityReference(emRefFieldEntity.getUID()));
								FieldMeta<?> refForeignRefFieldLangMeta =
										emRefEntityLang.getField(DataLanguageServerUtils.extractFieldUID(ref.getUID()));

								newSortingOrder.add(new CollectableSorting(
										TableAliasSingleton.getInstance().getAlias(refForeignRefLangMeta, join.getTableAliasRight()), fentity, false, refForeignRefFieldLangMeta.getUID(), cs.isAscending()));

							} else {
								newSortingOrder.add(new CollectableSorting(
										join.getTableAliasRight(), fentity, false, ref.getUID(), cs.isAscending()));
							}
						}
					}
				}
			} else {
				// copy sorting order
				newSortingOrder.add(cs);
			}
		}

		return newSortingOrder;

	}

	private boolean addJoinsForRecordgrant(DbQuery<?> query, final RecordGrantMeta rgMeta) {
		if (rgMeta != null) {
			final DbFrom<?> from = query.getDbFrom();
			from.joinOnJoinedPk(rgMeta, JoinType.RIGHT, new DbField<Long>() {
				@Override
				public String getDbColumn() {
					return "INTID";
				}

				@Override
				public Class<Long> getJavaClass() {
					return Long.class;
				}
			}, rgMeta.getEntityName());

			return true;
		}
		return false;
	}

	private RecordGrantMeta getAssignedRecordgrantMeta(List<IColumnToVOMapping<?, PK>> columns) {
		//BMWFDM-696: Only Remote Calls should use the Record-Grants. Else ignore them.
		if (!utils.isCalledRemotely()) {
			return null;
		}
		if (Boolean.TRUE.equals(ignoreRecordGrantsAndOthers.get())) {
			return null;
		}

		RecordGrantMeta rgMeta = null;
		try {
			final Set<RecordGrantVO> recordGrant = getRecordGrantUtils().getByEntity(getEntityUID());
			if (utils.getCurrentUserName() != null && !SecurityCache.getInstance().isSuperUser(utils.getCurrentUserName()) && !recordGrant.isEmpty()) {
				final RecordGrantVO rgVO = recordGrant.iterator().next();
				if (rgVO.getValid() && SecurityCache.getInstance().getAssignedRecordgrants(utils.getCurrentUserName(), userCtx.getMandatorUID()).contains(rgVO.getId())) {
					final String sql;
					List<String> rgColumns = null;
					try {
						sql = getDatasourceServerUtils().createSQL(rgVO, new HashMap<>(), null);
						rgColumns = DatasourceUtils.getColumnsWithoutQuotes(DatasourceUtils.getColumns(sql));
						rgColumns = CollectionUtils.transform(rgColumns, (String i) -> i.toUpperCase());
					} catch (NuclosDatasourceException e) {
						throw new NuclosFatalException("unable to create sql from record grant datasource.", e);
					}
					// add fields for record grants here.
					boolean bHasCanSomething = false;

					if (columns != null) {
						final Class<IDalVO<PK>> eov = LangUtils.getGenericClass(EntityObjectVO.class);

						if (rgColumns.contains(SF.CANWRITE.getDbColumn())) {
							bHasCanSomething = true;

							final FieldMeta<?> efCanwrite = SF.CANWRITE.getMetaData(getEntityUID());
							columns.add(createBeanMapping(tableAliasSingleton.getRecordGrantAlias(), eov, efCanwrite, efCanwrite.getFieldName()));

						}
						if (rgColumns.contains(SF.CANSTATECHANGE.getDbColumn())) {
							bHasCanSomething = true;

							final FieldMeta<?> efCanstatechange = SF.CANSTATECHANGE.getMetaData(getEntityUID());
							columns.add(createBeanMapping(tableAliasSingleton.getRecordGrantAlias(), eov, efCanstatechange, efCanstatechange.getFieldName()));

						}
						if (rgColumns.contains(SF.CANDELETE.getDbColumn())) {
							bHasCanSomething = true;

							final FieldMeta<?> efCandelete = SF.CANDELETE.getMetaData(getEntityUID());
							columns.add(createBeanMapping(tableAliasSingleton.getRecordGrantAlias(), eov, efCandelete, efCandelete.getFieldName()));
						}
					}

					rgMeta = new RecordGrantMeta(sql, tableAliasSingleton.getRecordGrantAlias(), bHasCanSomething);

				}
			}
		} catch (IllegalStateException e) {
			// we are to early. called from datasource cache. so retun null here.
			LOG.info("catching IllegalStateException getting recordgrants.", e);
		}
		return rgMeta;
	}

	/*
	 * we have to return our own BeanMapping here. cause "Object.class" will be mapped as "byte[].class".
	 * But we want to access resultset with rs.getObject() to get valid entries for canwrite, canstatechange or candelete.
	 */
	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, FieldMeta<S> ef, String methodRadical) {
		try {
			return (IColumnToVOMapping<S, PK>) createBeanMapping(alias, type, ef.getDbColumn(), methodRadical, ef.getUID(), Class.forName(ef.getDataType()), false);
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}

	private static <S, PK> IColumnToVOMapping<S, PK> createBeanMapping(String alias, Class<? extends IDalVO<PK>> type, String column, String methodRadical, UID fieldUID, Class<S> dataType, boolean isReadonly) {
		final String xetterSuffix = methodRadical.substring(0, 1).toUpperCase() + methodRadical.substring(1);
		// final Class<?> clazz = getDalVOClass();
		Class<?> methodParameterType = dataType;
		if ("primaryKey".equals(methodRadical)) {
			methodParameterType = Object.class;
		}
		try {
			return new ColumnToBeanVOMapping<S, PK>(alias, column, fieldUID, type.getMethod("set" + xetterSuffix, methodParameterType),
					type.getMethod((DT_BOOLEAN.equals(dataType) ? "is" : "get") + xetterSuffix), dataType, isReadonly, false) {
				@Override
				public Class getJavaClass() {
					return Object.class;
				}
			};
		} catch (Exception e) {
			throw new CommonFatalException("On " + type + ": " + e);
		}
	}

	private boolean hasAllColumns(List<IColumnToVOMapping<?, PK>> selectedColumns) {
		Set<UID> selUIDs = new HashSet<UID>();
		for (IColumnToVOMapping<?, PK> selCol : selectedColumns) {
			selUIDs.add(selCol.getUID());
		}
		for (IColumnToVOMapping<?, PK> defCol : getDefaultColumns()) {
			if (!selUIDs.contains(defCol.getUID())) {
				return false;
			}
		}
		return true;
	}

	@Override
	protected DbException transformInsertOrUpdateException(DbException ex, final EntityObjectVO<PK> eo) {
		try {
			NamedTableConstraintCacheEntry constraint = SchemaCache.getInstance().getTableConstraintFromDbExceptionIfAny(ex);
			if (constraint != null) {
				SpringLocaleDelegate localDelegate = SpringLocaleDelegate.getInstance();
				String message;
				if (constraint.getType() == NamedTableConstraintCacheEntry.TYPE_UNIQUE_KEY) {
					String bo = localDelegate.getText(constraint.getEntityMeta().getLocaleResourceIdForLabel());
					StringBuffer fields = new StringBuffer();
					final FormattingUIDTransformer formatter = new FormattingUIDTransformer(true, getMetaProvider()) {
						@Override
						protected Object getValue(UID field) {
							return eo.getFieldValue(field);
						}
					};
					for (FieldMeta<?> fMeta : constraint.getFieldMetas()) {
						if (fields.length() > 0) {
							fields.append(", ");
						}
						fields.append(localDelegate.getText(fMeta.getLocaleResourceIdForLabel()));
						fields.append("=");
						fields.append(formatter.transform(fMeta.getUID()));
					}

					//Das Businessobjekt {0} enthält bereits einen Datensatz mit {1}
					message = localDelegate.getMsg("db.unique.constraint.violation", bo, fields.toString());

				} else if (constraint.getType() == NamedTableConstraintCacheEntry.TYPE_FOREIGN_KEY) {
					String bo = localDelegate.getText(constraint.getEntityMeta().getLocaleResourceIdForLabel());
					FieldMeta<?> fMeta = constraint.getFieldMetas().get(0);
					String reffield = localDelegate.getText(fMeta.getLocaleResourceIdForLabel());
					String refbo = localDelegate.getText(getMetaProvider().getEntity(fMeta.getForeignEntity()).getLocaleResourceIdForLabel());
					String refValue = eo.getFieldValue(fMeta.getUID(), String.class);
					Object refId = eo.getFieldId(fMeta.getUID());
					if (refId == null) {
						if (eo.getFieldUid(fMeta.getUID()) != null) {
							refId = eo.getFieldUid(fMeta.getUID()).getString();
						}
					} else {
						refId = refId.toString();
					}

					//Der Datensatz \"{0}\" (ID={1}) im Businessobjekt {2} existiert nicht, wird aber verwendet: {3}.{4}
					message = localDelegate.getMsg("db.foreign.constraint.violation.during.insertupdate", refValue, refId, refbo, bo, reffield);

				} else {
					throw new IllegalArgumentException("Constraint type " + constraint.getType());
				}

				return new DbBusinessException(ex.getPk(), message, ex.getSqlCause());
			}
		} catch (Exception e1) {
			String message = "Exception during transformation of insert or update exception: " + e1.getMessage();
			LOG.warn(message);
			LOG.debug(message, e1);
		}
		return super.transformInsertOrUpdateException(ex, eo);
	}

	@Override
	protected DbException transformDeleteException(DbException ex, PK pk) {
		try {
			NamedTableConstraintCacheEntry constraint = SchemaCache.getInstance().getTableConstraintFromDbExceptionIfAny(ex);
			if (constraint != null) {
				String message;
				if (constraint.getType() == NamedTableConstraintCacheEntry.TYPE_FOREIGN_KEY) {
					SpringLocaleDelegate localDelegate = SpringLocaleDelegate.getInstance();
					String bo = localDelegate.getText(getMetaData().getLocaleResourceIdForLabel());
					String refbo = localDelegate.getText(constraint.getEntityMeta().getLocaleResourceIdForLabel());
					FieldMeta<?> fMeta = constraint.getFieldMetas().get(0);
					String reffield = localDelegate.getText(fMeta.getLocaleResourceIdForLabel());

					EntityObjectVO<PK> eo = getByPrimaryKey(pk);
					String refValue = RefValueExtractor.get(eo, fMeta.getUID(), null, true, getMetaProvider());

					//Der Datensatz \"{0}\" ({1}) kann nicht gelöscht werden, da noch Datensätze aus dem Businessobjekt {2} im Attribut {3} auf diesen verweisen.
					message = localDelegate.getMsg("db.foreign.constraint.violation.during.delete", refValue, bo, refbo, reffield);

				} else {
					throw new IllegalArgumentException("Constraint type " + constraint.getType());
				}

				return new DbBusinessException(ex.getPk(), message, ex.getSqlCause());
			}
		} catch (Exception e1) {
			String message = "Exception during transformation of delete exception: " + e1.getMessage();
			LOG.warn(message);
			LOG.debug(message, e1);
		}
		return super.transformDeleteException(ex, pk);
	}

	@Override
	public void setIgnoreRecordGrantsAndOthers(boolean ignore) {
		synchronized (ignoreRecordGrantsAndOthers) {
			if (ignore && !Boolean.TRUE.equals(ignoreRecordGrantsAndOthers.get())
					&& TransactionSynchronizationManager.isSynchronizationActive()) {
				// Security fallback only...
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public void afterCompletion(int status) {
						synchronized (ignoreRecordGrantsAndOthers) {
							ignoreRecordGrantsAndOthers.set(false);
						}
					}
				});
			}
			ignoreRecordGrantsAndOthers.set(ignore);
		}
	}

	@Override
	public boolean getIgnoreRecordGrantsAndOthers() {
		return Boolean.TRUE.equals(ignoreRecordGrantsAndOthers.get());
	}

	@Override
	public void setThinReadEnabled(boolean enabled) {
		synchronized (thinReadEnabled) {
			if (eMeta.isThin() && enabled && !Boolean.TRUE.equals(thinReadEnabled.get())
					&& TransactionSynchronizationManager.isSynchronizationActive()) {
				// Security fallback only...
				TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
					@Override
					public void afterCompletion(int status) {
						synchronized (thinReadEnabled) {
							thinReadEnabled.set(false);
						}
					}
				});
			}
			thinReadEnabled.set(eMeta.isThin() && enabled);
		}
	}

	@Override
	public boolean isThinReadEnabled() {
		return Boolean.TRUE.equals(thinReadEnabled.get());
	}

	@Override
	protected Transformer<Object[], EntityObjectVO<PK>> createResultTransformer(List<IColumnToVOMapping<?, PK>> columns) {
		final Transformer<Object[], EntityObjectVO<PK>> jdbcTransformer = super.createResultTransformer(columns);
		if (eMeta.isOwner()) {
			return (Object[] o) -> {
				EntityObjectVO<PK> result = jdbcTransformer.transform(o);
				UID ownerUID = result.getFieldUid(SF.OWNER_UID);
				if (ownerUID != null) {
					String userName = getCurrentUserName();
					final UID currentUserUID = userName != null ? SecurityCache.getInstance().getUserUid(userName) : null;
					if (!LangUtils.equal(ownerUID, currentUserUID)) {
						result.setCanWrite(false);
						result.setCanDelete(false);
						result.setCanStateChange(false);
					}
				}
				return result;
			};
		} else {
			return jdbcTransformer;
		}
	}

	public final String getCurrentUserName() {
		return utils.getCurrentUserName();
	}

	@Override
	public UID getOwner(PK id) {
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not an owner enabled entity!");
		}
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<UID> q = builder.createQuery(UID.class);
		DbFrom<Long> from = q.from(E.ENTITYOBJECTLOCK);
		q.select(from.baseColumn(E.ENTITYOBJECTLOCK.owner));
		q.where(builder.equalValue(from.baseColumn(E.ENTITYOBJECTLOCK.objectId), (Long) id));
		q.addToWhereAsAnd(builder.equalValue(from.baseColumn(E.ENTITYOBJECTLOCK.entity), eMeta.getUID()));
		try {
			return dbAccess.executeQuerySingleResult(q);
		} catch (DbInvalidResultSizeException ex) {
			// no lock
			return null;
		}
	}

	@Override
	public void lock(PK id, UID userUID) {
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not an owner enabled entity!");
		}
		this.unlock(id);
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbMap values = new DbMap();
		values.put(SF.PK_ID, new DbId()); //DalUtils.getNextId());
		values.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		values.put(SF.CREATEDBY, getCurrentUserName());
		values.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		values.put(SF.CHANGEDBY, getCurrentUserName());
		values.put(SF.VERSION, 1);
		values.put(E.ENTITYOBJECTLOCK.owner, userUID);
		values.putUnsafe(E.ENTITYOBJECTLOCK.objectId, id);
		values.put(E.ENTITYOBJECTLOCK.entity, eMeta.getUID());
		DbInsertStatement<?> insert = new DbInsertStatement<>(E.ENTITYOBJECTLOCK, values);
		dbAccess.execute(insert);
	}

	@Override
	public void unlock(PK id) {
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not an owner enabled entity!");
		}
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbMap conditions = new DbMap();
		conditions.putUnsafe(E.ENTITYOBJECTLOCK.objectId, id);
		conditions.put(E.ENTITYOBJECTLOCK.entity, eMeta.getUID());
		DbDeleteStatement<?> delete = new DbDeleteStatement<>(E.ENTITYOBJECTLOCK, conditions);
		dbAccess.execute(delete);
	}

	@Override
	protected void adjustFrom(final DbFrom<PK> from) {
		from.setUsername(utils.getCurrentUserName());
	}
}
