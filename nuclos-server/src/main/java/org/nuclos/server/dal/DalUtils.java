//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.AbstractDalVOWithVersion;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IVersionVO;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.server.dal.specification.IDalVersionSpecification;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.datasource.DatasourceMetaVO;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.resource.valueobject.ResourceFile;

public class DalUtils {
	
	public static void addStaticFields(Collection<FieldMeta<?>> entityFields, EntityMeta<?> eMeta) {
		addStaticFields(entityFields, eMeta.getUID(), eMeta.isStateModel(), eMeta.isMandator(), eMeta.isOwner(), eMeta.getOwnerForeignEntityField());
	}
	
	private static void addStaticFields(Collection<FieldMeta<?>> entityFields, UID entityUID, boolean isStateModel, boolean isMandator, boolean isOwner, String sOwnerForeignEntityField) {
		Map<UID, FieldMeta<?>> mpFields = new HashMap<UID, FieldMeta<?>>();
		
		for (FieldMeta<?> fm : entityFields) {
			mpFields.put(fm.getUID(), fm);
		}
		
		List<FieldMeta<?>> toRemove = new ArrayList<FieldMeta<?>>();
		List<FieldMeta<?>> lstStaticFields = new ArrayList<FieldMeta<?>>();
		
		lstStaticFields.add(getFieldMeta(SF.CHANGEDAT.getMetaData(entityUID), mpFields, toRemove));
		lstStaticFields.add(getFieldMeta(SF.CHANGEDBY.getMetaData(entityUID), mpFields, toRemove));
		lstStaticFields.add(getFieldMeta(SF.CREATEDAT.getMetaData(entityUID), mpFields, toRemove));
		lstStaticFields.add(getFieldMeta(SF.CREATEDBY.getMetaData(entityUID), mpFields, toRemove));
//		lstStaticFields.add(NuclosEOField.VERSION.getMetaData()); // why? Good Question! (OB)
		if (isMandator) {
			lstStaticFields.add(getFieldMeta(SF.MANDATOR.getMetaData(entityUID), mpFields, toRemove));
		}
		if (isOwner) {
			FieldMeta<?> ownerMetaData = SF.OWNER.getMetaData(entityUID);
			if (sOwnerForeignEntityField != null) {
				ownerMetaData = new FieldMetaVO(ownerMetaData);
				((FieldMetaVO)ownerMetaData).setForeignEntityField(sOwnerForeignEntityField);
			}
			lstStaticFields.add(getFieldMeta(ownerMetaData, mpFields, toRemove));
		}
		if (isStateModel) {
			lstStaticFields.add(getFieldMeta(SF.SYSTEMIDENTIFIER.getMetaData(entityUID), mpFields, toRemove));
			lstStaticFields.add(getFieldMeta(SF.PROCESS.getMetaData(entityUID), mpFields, toRemove));
			lstStaticFields.add(getFieldMeta(SF.ORIGIN.getMetaData(entityUID), mpFields, toRemove));
			lstStaticFields.add(getFieldMeta(SF.LOGICALDELETED.getMetaData(entityUID), mpFields, toRemove));
			lstStaticFields.add(getFieldMeta(SF.STATE.getMetaData(entityUID), mpFields, toRemove));
			lstStaticFields.add(getFieldMeta(SF.STATENUMBER.getMetaData(entityUID), mpFields, toRemove));
			lstStaticFields.add(getFieldMeta(SF.STATEICON.getMetaData(entityUID), mpFields, toRemove));
		}
		
		entityFields.removeAll(toRemove);
		entityFields.addAll(lstStaticFields);
	}
	
	private static FieldMeta<?> getFieldMeta(FieldMeta<?> fm, Map<UID, FieldMeta<?>> mpFieldsSource, List<FieldMeta<?>> toRemove) {
		FieldMeta<?> fmSource = mpFieldsSource.get(fm.getUID());
		
		if (fmSource instanceof NucletFieldMeta) {
			
			if (fm instanceof FieldMetaVO) {
				FieldMetaVO<?> fmvo = (FieldMetaVO<?>) fm;
				
				fmvo.setHidden(fmSource.isHidden());
				if (fmSource.getFieldGroup() != null) {
					fmvo.setFieldGroup(fmSource.getFieldGroup());				
				}
				
				fmvo.setLocaleResourceIdForLabel(fmSource.getLocaleResourceIdForLabel());
				fmvo.setLocaleResourceIdForDescription(fmSource.getLocaleResourceIdForDescription());
			}
			
			toRemove.add(fmSource);
		}
		return fm;
	}
	
	public static Collection<FieldMeta<?>> getStaticFields(UID entityUID, boolean isStateModel, boolean isMandator, boolean isOwner, String sOwnerForeignEntityField) {
		Collection<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		addStaticFields(result, entityUID, isStateModel, isMandator, isOwner, sOwnerForeignEntityField);
		return result;
	}

	public static boolean isNucletEOSystemField(FieldMeta<?> voField) {
		return SF.isEOField(voField.getFieldName());
	}

	public static Long getNextId() {
		return SpringDataBaseHelper.getInstance().getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE);
	}

	public static <PK> void handleVersionUpdate(IDalVersionSpecification<PK> processor,
		EntityObjectVO<PK> vo, String user) throws CommonStaleVersionException {

		if (vo.getPrimaryKey() != null) {
			final Integer oldVersion = processor.getVersion(vo.getPrimaryKey());
			if (!oldVersion.equals(Integer.valueOf(vo.getVersion()))) {
				throw new CommonStaleVersionException(vo, oldVersion);
			}
		}

		updateVersionInformation(vo, user);
		vo.flagUpdate();
	}

	public static void updateVersionInformation(IVersionVO<?> vo, String user) {
		Date sysdate = new Date();
		if (vo.getCreatedBy() == null) {
			vo.setCreatedBy(user);
		}
		if (vo.getCreatedAt() == null) {
			vo.setCreatedAt(InternalTimestamp.toInternalTimestamp(sysdate));
		}
		vo.setVersion(vo.getVersion()+1);

		vo.setChangedBy(user);
		vo.setChangedAt(InternalTimestamp.toInternalTimestamp(sysdate));
	}

	public static <T> boolean isNuclosProcessor(AbstractDalVOWithVersion dalVO) {
		return dalVO.processor() != null && isNuclosProcessor(dalVO.processor());
	}

	public static boolean isNuclosProcessor(String processor) {
		try {
			Class<?> processorClzz = LangUtils.getClassLoaderThatWorksForWebStart().loadClass(processor);
			for (Class<?> processorInterface : processorClzz.getInterfaces()){
				if ("org.nuclos.server.dal.processor.nuclos".equals(processorInterface.getPackage().getName())){
					return true;
				}
			}
			return false;
		}
		catch(ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}
	
	public static NucletFieldMeta<?> getFieldMeta(DatasourceMetaVO.ColumnMeta column) {
		String outputformat = null;
		
		try {
			if (Number.class.isAssignableFrom(Class.forName(column.getJavaType()))) {
				if (Double.class.getName().equals(column.getJavaType())) {
					outputformat = "#,##0.";
					for (int i = 0; i < column.getPrecision(); i++) {
						outputformat += "0";
					}
				}
			}
		} catch (ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
		
		NucletFieldMeta<?> field = new NucletFieldMeta();
		field.setUID(column.getFieldUID());
		field.setFieldName(column.getColumnName());
		field.setDbColumn(column.getColumnName());
		field.setDataType(column.getJavaType());
		field.setScale(column.getScale());
		field.setPrecision(column.getPrecision());
		field.setFormatOutput(outputformat);
		field.setReadonly(false);
		field.setUnique(false);
		field.setNullable(true);
		field.setIndexed(false);
		field.setSearchable(false);
		field.setModifiable(false);
		field.setInsertable(false);
		field.setLogBookTracking(false);
		field.setShowMnemonic(false);
		field.setPermissionTransfer(false);
		return field;
	}

	public static NucletFieldMeta<?> getFieldMeta(DbColumn column) {
		Class<?> cls = String.class;
		Integer scale = null;
		Integer precision = null;
		String outputformat = null;
		DbColumnType columnType = column.getColumnType();
		if(columnType.getGenericType() != null) {
			switch (columnType.getGenericType()) {
			case DATE:
			case DATETIME:
				cls = Date.class;
				break;
			case NUMERIC:
				scale = columnType.getPrecision();
				precision = columnType.getScale();
				// this seems wrong because it can be floating point number, too...
				if(columnType.getPrecision() != null && columnType.getPrecision() == 1
					&& columnType.getScale() != null && columnType.getScale() == 0) {
					// TODO
					// booleans are mapped as NUMBER(1), but it's possible that there are
					// other columns which are not meant as a boolean.
					cls = Boolean.class;
					precision = null;
				}
				else if (columnType.getScale() != null && columnType.getScale() > 0) {
					cls = Double.class;
					outputformat = "#,##0.";
					for (int i = 0; i < columnType.getScale(); i++) {
						outputformat += "0";
					}
				}
				else {
					if (scale == null) {
						// default
						scale = 9;
					}
					cls = scale > 10 && columnType.getTypeName().equals("int8") ? Long.class : Integer.class;
				}
				break;
			case CLOB:
				cls = String.class;
				scale = null;
				break;
			case VARCHAR:
				cls = String.class;
				scale = columnType.getLength();
				break;
			case NVARCHAR:
				cls = String.class;
				scale = columnType.getLength();
				break;
			case BOOLEAN:
				cls = Boolean.class;
				scale = 1;
				precision = null;
				break;
			case BLOB:
				cls = byte[].class;
				scale = columnType.getLength();
				precision = null;
				break;
			}	
		}

		NucletFieldMeta<?> field = new NucletFieldMeta();
		field.setFieldName(column.getColumnName());
		field.setDbColumn(column.getColumnName());
		field.setDataType(cls.getName());
		field.setScale(scale);
		field.setPrecision(precision);
		field.setFormatOutput(outputformat);
		field.setReadonly(false);
		field.setUnique(false);
		field.setNullable(true);
		field.setIndexed(false);
		field.setSearchable(false);
		field.setModifiable(false);
		field.setInsertable(false);
		field.setLogBookTracking(false);
		field.setShowMnemonic(false);
		field.setPermissionTransfer(false);
		return field;
	}

	public static Class<?> getDbType(Class<?> javaType) {
		if (javaType == ByteArrayCarrier.class || javaType == Object.class || javaType == NuclosImage.class) {
			javaType = byte[].class; // Column stores data (blob)
		} else if (javaType == ResourceFile.class || javaType == GenericObjectDocumentFile.class) {
			return String.class; // Column stores filename
		} else if (javaType == DateTime.class) {
			return InternalTimestamp.class;
		} else if (javaType == NuclosPassword.class) {
			return NuclosPassword.class;
		}
		return javaType;
	}

	public static Class<?> getDbType(String javaType) {
		try {
			return getDbType(Class.forName(javaType));
		}
		catch (ClassNotFoundException e) {
			throw new IllegalArgumentException(javaType);
		}
	}

}
