//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.util.Map;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.dal.vo.IDalWithFieldsVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.i18n.language.data.DataLanguageFacadeRemote;

/**
 * Map a database column to a simple value reference entity field representation.
 * 
 * @see org.nuclos.common.dal.vo.IDalWithFieldsVO#getFieldValues()
 *
 * @param <T> Java type for the data in this column of the database.
 */
public class ColumnToFieldVOMapping<T extends Object, PK> extends AbstractColumnToVOMapping<T, PK>
	implements IColumnWithMdToVOMapping<T, PK>
{

	private final FieldMeta<?> field;
	
	/**
	 * Konstruktor für dynamische VO Werte (Die Werte werden in einer "Fields"-Liste gespeichert).
	 * @throws ClassNotFoundException 
	 */
	public ColumnToFieldVOMapping(String tableAlias, FieldMeta<?> field) throws ClassNotFoundException {
		this(tableAlias, field, field.isReadonly(), false);
	}
	
	public ColumnToFieldVOMapping(String tableAlias, FieldMeta<?> field, boolean readonly, boolean bCaseSensitive) throws ClassNotFoundException {
		super(tableAlias, field.getDbColumn(), field.getUID(), field.getDataType(), readonly, bCaseSensitive || field.isDynamic());
		this.field = field;
	}

	@Override
	public DbExpression<T> getDbColumn(DbFrom<PK> from) {
		final DbExpression<T> result;
		if (!field.isCalculated()) {
			result = super.getDbColumn(from);
		}
		else {
			String localeAdd = field.isLocalized() ? " , '" + getDataLanguage().getLanguageToUse() + "'"  : "";
			
			if (field.getCalcAttributeDS() != null) {
				final DbQueryBuilder builder = from.getQuery().getBuilder();
				final String dsSQL = createCalcAttributeSQL(field, from);
				result = (DbExpression<T>) builder.plainExpression(
						DbUtils.getDbType(field.getDataType()), 
						" (" + dsSQL + ") " + field.getDbColumn());
			} else {
				final String tableAlias = from.getAlias();
				final DbQueryBuilder builder = from.getQuery().getBuilder();
				result = (DbExpression<T>) builder.plainExpression(
						DbUtils.getDbType(field.getDataType()), 
						builder.getDBAccess().getSchemaName() + "." 
						+ field.getCalcFunction() + "(" + tableAlias + ".INTID" + localeAdd + ") " + field.getDbColumn());
			}
		}
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("col=").append(getColumn());
		result.append(", tableAlias=").append(getTableAlias());
		result.append(", field=").append(field);
		if (getDataType() != null)
			result.append(", type=").append(getDataType().getName());
		result.append("]");
		return result.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ColumnToFieldVOMapping)) return false;
		final ColumnToFieldVOMapping<T, PK> other = (ColumnToFieldVOMapping<T, PK>) o;
		return field.equals(other.field);
	}
	
	@Override
	public int hashCode() {
		int result = getColumn().hashCode();
		result += 3 * field.hashCode();
		return result;
	}
	
	public UID getUID() {
		return field.getUID();
	}

	@Override
	public FieldMeta<?> getMeta() {
		return field;
	}

	@Override
	public Object convertFromDalFieldToDbValue(IDalVO<PK> dal) {
		final IDalWithFieldsVO<?, PK> realDal = (IDalWithFieldsVO<?, PK>) dal;
		try {
			return convertToDbValue(getDataType(), realDal.getFieldValue(field.getUID()));
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

	@Override
	public void convertFromDbValueToDalField(IDalVO<PK> result, T o) {
		final IDalWithFieldsVO<Object, PK> realDal = (IDalWithFieldsVO<Object, PK>) result;
		try {
			realDal.setFieldValue(field.getUID(),
					convertFromDbValue(o, getColumn(), getDataType(), result.getPrimaryKey()));
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

	@Override
	public boolean constructJoinForStringifiedRefs() {
		return true;
	}
	
	private static DatasourceServerUtils getDatasourceServerUtils() {
		return SpringApplicationContextHolder.getBean(DatasourceServerUtils.class);
	}
	
	private static IMetaProvider getMetaProvider() {
		return SpringApplicationContextHolder.getBean(MetaProvider.class);
	}
	
	private static DataLanguageFacadeRemote getDataLanguage() {
		return SpringApplicationContextHolder.getBean(DataLanguageFacadeRemote.class);
	}
	
	public static String createCalcAttributeSQL(FieldMeta<?> field, DbFrom<?> from) {
		Map<String, Object> mpParams = CalcAttributeUtils.stringToMap(field.getCalcAttributeParamValues());
		mpParams.put("intid", from.getAlias() + "." + from.basePk().getAlias());
		UID dataMandator = null;
		if (getMetaProvider().getEntity(field.getEntity()).isMandator()) {
			dataMandator = SystemFields.getMandatorColumnUID(from.getAlias());
		}
		try {
			CalcAttributeVO calcAttributeDS = DatasourceCache.getInstance().getCalcAttribute(field.getCalcAttributeDS());
			String dsSQL = getDatasourceServerUtils().createSQL(calcAttributeDS, mpParams, dataMandator);
			return dsSQL;
		} catch (NuclosDatasourceException e) {
			throw new NuclosFatalException(e);
		}
	}

}
