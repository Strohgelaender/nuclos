package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dal.specification.IDalReadSpecification;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.MetaDbProvider;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.impl.SchemaUtils;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.statements.DbStructureChange.Type;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbNamedObject;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.structure.DbTableArtifact;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable()
public class DataLanguageMetaDataProcessor implements
		IDalReadSpecification<NucletEntityMeta, UID> {
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	private NucletDalProvider nucletDalProvider;

	public List<NucletEntityMeta> getAll(Map<UID, EntityMeta<?>> mapEntityMetaData) {
		
		List<NucletEntityMeta> retVal = new ArrayList<>();
		
		for (EntityMeta entity : mapEntityMetaData.values()) {
			
			if (entity instanceof NucletEntityMeta) {
				
				if (entity.IsLocalized()) {
					NucletEntityMeta entityLang = 
							DataLanguageServerUtils
								.createEntityLanguageMeta((NucletEntityMeta) entity,
								                          SF.PK_ID.getMetaData(entity.getUID()));
					
					// Why?
					//if (dataBaseHelper.isTableAvailable(entityLang)) {
						retVal.add(entityLang);
					//}				
					
				}
			}
		}
		
		return retVal;
	}

	@Override
	public List<NucletEntityMeta> getAll() {
		return getAll(MetaProvider.getInstance().getMapEntityMetaData());
	}
	
	@Override
	public List<UID> getAllIds() {
		
		List<UID> retVal = new ArrayList<UID>();
		
		for(NucletEntityMeta meta : getAll()) {
			retVal.add(meta.getPrimaryKey());
		}
		
		return retVal;
	}

	@Override
	public NucletEntityMeta getByPrimaryKey(UID id) {
		for(NucletEntityMeta meta : getAll()) {			
			if (meta.getPrimaryKey().equals(id)) {
				return meta;
			}
			
		}		
		return null;
	}

	@Override
	public NucletEntityMeta getByPrimaryKey(UID id, Collection<FieldMeta<?>> fields) {
		return getByPrimaryKey(id);
	}

	@Override
	public List<NucletEntityMeta> getByPrimaryKeys(List<UID> ids) {
		List<NucletEntityMeta> retVal = new ArrayList<>();
		
		for(NucletEntityMeta meta : getAll()) {			
			if (ids.contains(meta.getPrimaryKey())) {
				retVal.add(meta);
			}
		}	
		return retVal;
	}

	public NucletDalProvider getNucletDalProvider() {
		return nucletDalProvider;
	}

	public void setNucletDalProvider(NucletDalProvider nucletDalProvider) {
		this.nucletDalProvider = nucletDalProvider;
	}
	
	public void remove(EntityMeta<?> nem) throws DbException{
		final MetaDbHelper dbHelperSoll = 
				new MetaDbHelper(E.getSchemaHelperVersion(), dataBaseHelper.getDbAccess(), MetaProvider.getInstance(),
						null, null);
		final DbTable tableSoll = dbHelperSoll.getDbTable(nem, nem.getFields());
		final DbTable tableIst = dataBaseHelper.getDbAccess().getTableMetaData(nem.getDbTable());
		if (tableIst != null) {
			final List<DbTableArtifact> artifactsIst = tableIst.getTableArtifacts();
			final List<DbStructureChange> lstStructureChanges = SchemaUtils.drop(tableSoll);
			
			for(DbStructureChange ds : lstStructureChanges) {
				// only drop artifacts, that actual exist in real table
				if (ds.getType() == Type.DROP && !(ds.getArtifact1() instanceof DbTable) &&
						!artifactsIst.contains(ds.getArtifact1())) {
					continue;
				}
				dataBaseHelper.getDbAccess().execute(ds);
			}
		}
	}
	
	public void create(EntityMeta<?> entityLanguageMeta, MetaDbProvider updatedProvider) throws DbException {
		final MetaDbHelper dbHelperSoll = 
				new MetaDbHelper(E.getSchemaHelperVersion(), dataBaseHelper.getDbAccess(),
						updatedProvider);
		for (FieldMeta meta : entityLanguageMeta.getFields()) {
			((NucletFieldMeta)meta).setLocalized(true);
		}
		final DbTable tableSoll = dbHelperSoll.getDbTable(entityLanguageMeta, entityLanguageMeta.getFields());
		List<DbStructureChange> lstStructureChanges = SchemaUtils.create(tableSoll);
	
		for(DbStructureChange ds : lstStructureChanges) {
			dataBaseHelper.getDbAccess().execute(ds);
		}
	}
	
	public void modifyReferenceColumn(NucletEntityMeta updatedLangTable, FieldMeta changedRefField, String oldDLTableName, PersistentDbAccess dbAccess) {
		if (dataBaseHelper.isTableAvailable(updatedLangTable)) {
			DbTable tableMetaData = 
					dataBaseHelper.getDbAccess().getTableMetaData(updatedLangTable.getDbTable());
			DbColumn existColumn = null;
			for (DbColumn foundcol : tableMetaData.getTableColumns()) {
				if (foundcol.getColumnName().toLowerCase().equals(oldDLTableName.toLowerCase())) {
					existColumn = foundcol;
					break;
				}
			}
			
			if (existColumn != null) {
				DbColumn tmpColumn = new DbColumn(changedRefField.getUID(), existColumn.getNamedObject(), existColumn.getDbColumn(), existColumn.getColumnType(), existColumn.getNullable(), changedRefField.getDefaultValue(), changedRefField.getOrder());
				DbNamedObject nmdObj = new DbNamedObject(updatedLangTable.getUID(), updatedLangTable.getDbTable());
				DbColumnType columnType = MetaDbHelper.createDbLangColumnType(changedRefField);
				DbColumn col = new DbColumn(changedRefField.getUID(), nmdObj, 
						changedRefField.getDbColumn(), columnType, DbNullable.NULL, changedRefField.getDefaultValue(), changedRefField.getOrder());

				List<DbStructureChange> modify = SchemaUtils.modify(tmpColumn, col, false);
				for(DbStructureChange ds : modify) {
					dbAccess.execute(ds);
				}
			}
		}
			
	}
	public void modify(EntityMeta<?> entityLanguageMeta, Map<UID, FieldMeta<?>> lstFields) throws DbException {		
		
		// modify fields first
		for (FieldMeta fm : entityLanguageMeta.getFields()) {			
			if (fm instanceof NucletFieldMeta) {
				NucletFieldMeta nfm = (NucletFieldMeta) fm;
				
				DbNamedObject nmdObj = new DbNamedObject(entityLanguageMeta.getUID(), entityLanguageMeta.getDbTable());
				
				DbColumnType columnType = null;
				
				UID originUID = DataLanguageServerUtils.extractOriginalFieldUID(nfm.getUID());
				
				boolean originFieldIsLocalized = lstFields.containsKey(originUID) ? lstFields.get(originUID).isLocalized() : false;
				
				if (!originFieldIsLocalized) {
					columnType = MetaDbHelper.createDbColumnType(nfm);	
				} else {
					columnType = MetaDbHelper.createDbLangColumnType(nfm);
				}
				
				DbColumn col = new DbColumn(nfm.getUID(), nmdObj, 
						nfm.getDbColumn(), columnType, DbNullable.NULL, nfm.getDefaultValue(), nfm.getOrder());
				
				if (nfm.isFlagRemoved()) {
					List<DbStructureChange> drop = SchemaUtils.drop(col);
					for(DbStructureChange ds : drop) {
						dataBaseHelper.getDbAccess().execute(ds);
					}					
				} else if (nfm.isFlagNew()) {
					List<DbStructureChange> create = SchemaUtils.create(col);
					for(DbStructureChange ds : create) {
						dataBaseHelper.getDbAccess().execute(ds);
					}
				} else if (nfm.isFlagUpdated()) {
					// we have an existing data lang field that has been is marked updated
					// it means that the db column name has been modified
					if (dataBaseHelper.isTableAvailable(entityLanguageMeta)) {
						DbTable tableMetaData = 
								dataBaseHelper.getDbAccess().getTableMetaData(entityLanguageMeta.getDbTable());
						DbColumn existColumn = null;
						for (DbColumn foundcol : tableMetaData.getTableColumns()) {
							if (foundcol.getColumnName().toLowerCase().equals(nfm.getDbColumn().toLowerCase())) {
								existColumn = foundcol;
								break;
							}
						}
						
						if (existColumn != null) {
							List<DbStructureChange> modify = SchemaUtils.modify(existColumn, col, true);
							for(DbStructureChange ds : modify) {
								dataBaseHelper.getDbAccess().execute(ds);
							}
						} else {
							List<DbStructureChange> modify = SchemaUtils.create(col);
							for(DbStructureChange ds : modify) {
								dataBaseHelper.getDbAccess().execute(ds);
							}
						}						
					} else {
						throw new DbException("Data Language table not found.");
					}
				}
			}
		}
	}

	public void validateDBTableName(NucletEntityMeta langEntityMeta) {
		while (true) {
			int idx = 0;
			if (dataBaseHelper.isTableAvailable(langEntityMeta)) {
				if (langEntityMeta.getDbTable().length() >= NucletEntityMeta.DATA_LANG_DB_TABLE_NAME_MAX_LENGTH) {
						langEntityMeta.setDbTable(
							langEntityMeta.getDbTable().substring(0, NucletEntityMeta.DATA_LANG_DB_TABLE_NAME_MAX_LENGTH - 1) + ++idx);							
				} else {
					langEntityMeta.setDbTable(
							langEntityMeta.getDbTable() + ++idx);
				}
			} else {
				break;
			}
		}
	}

	public void validateDBColumnName(String dbTableName, NucletFieldMeta existDatLangField) {
		
		DbTable tableMetaData = 
				dataBaseHelper.getDbAccess().getTableMetaData(dbTableName);
		
		String columnNameToUse = existDatLangField.getDbColumn();
		
		int idx = 0;
		boolean validFound = false;
		
		while(!validFound) {
			for (DbColumn column : tableMetaData.getTableColumns()) {
				if (column.getColumnName().equals(columnNameToUse)) {
					if (columnNameToUse.length() >= NucletFieldMeta.DATA_LANG_DB_COLUMN_NAME_MAX_LENGTH ) {
						columnNameToUse = columnNameToUse.substring(0, NucletFieldMeta.DATA_LANG_DB_COLUMN_NAME_MAX_LENGTH - 1) +  ++idx;						
					} else {
						columnNameToUse += ++idx;
					}
					break;
				}
			}
			validFound = true;
		}
		
		existDatLangField.setDbColumn(columnNameToUse);
	}
	
}
