//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.PreferenceVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclet.IPreferenceProcessor;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

@Configurable
public class PreferenceProcessor extends AbstractJdbcDalProcessor<PreferenceVO, UID> implements IPreferenceProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(PreferenceProcessor.class);

	private final IColumnToVOMapping<UID, UID> idColumn;
	private final IColumnToVOMapping<String, UID> appColumn;
	private final IColumnToVOMapping<String, UID> typeColumn;
	private final IColumnToVOMapping<UID, UID> userColumn;
	private final IColumnToVOMapping<UID, UID> entityColumn;
	private final IColumnToVOMapping<UID, UID> layoutColumn;
	private final IColumnToVOMapping<UID, UID> nucletColumn;
	private final IColumnToVOMapping<UID, UID> sharedColumn;
	private final IColumnToVOMapping<Boolean, UID> menuRelevantColumn;

	public PreferenceProcessor(
			List<IColumnToVOMapping<? extends Object, UID>> allColumns, IColumnToVOMapping<UID, UID> idColumn,
			IColumnToVOMapping<String, UID> appColumn, IColumnToVOMapping<String, UID> typeColumn,
			IColumnToVOMapping<UID, UID> userColumn, IColumnToVOMapping<UID, UID> entityColumn, IColumnToVOMapping<UID, UID> layoutColumn,
			IColumnToVOMapping<UID, UID> nucletColumn, IColumnToVOMapping<UID, UID> sharedColumn, IColumnToVOMapping<Boolean, UID> menuRelevantColumn) {
		super(E.PREFERENCE.getUID(), PreferenceVO.class, UID.class, allColumns);
		this.idColumn = idColumn;
		this.appColumn = appColumn;
		this.typeColumn = typeColumn;
		this.userColumn = userColumn;
		this.entityColumn = entityColumn;
		this.layoutColumn = layoutColumn;
		this.nucletColumn = nucletColumn;
		this.sharedColumn = sharedColumn;
		this.menuRelevantColumn = menuRelevantColumn;
	}

	@Override
	public EntityMeta<UID> getMetaData() {
		return E.PREFERENCE;
	}

	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return idColumn;
	}

	@Override
	public void delete(Delete<UID> id) throws DbException {
		super.delete(id);
		evictPreferences();
	}

	@Override
	public void deleteUserPreferences(UID user) throws DbException {
		DbQueryBuilder queryBuilder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbDelete dbDelete = queryBuilder.createDelete(E.PREFERENCE);
		dbDelete.addToWhereAsAnd(queryBuilder.equalValue(dbDelete.baseColumn(E.PREFERENCE.user), user));
		dataBaseHelper.getDbAccess().executeDelete(dbDelete);
		evictPreferences();
	}

	@Override
	public Object insertOrUpdate(PreferenceVO dalVO) {
		if (PreferenceVO.APP_NUCLOS.equals(dalVO.getApp())) {
			// validate type
			NuclosPreferenceType.get(dalVO.getType());
		}
		Object res=super.insertOrUpdate(dalVO);
		evictPreferences();
		return res;
	}

	/**
	 * reset user customizations, if any
	 * 
	 * @param sharedPreference
	 */
	@Override
	public void batchResetUserCustomizations(PreferenceVO sharedPreference) {
		if (sharedPreference.getSharedPreference() != null) {
			// shared-shared???
			throw new IllegalArgumentException("a shared - shared preference ??? " + sharedPreference);
		}
		if (sharedPreference.getUser() != null) {
			throw new IllegalArgumentException("reset with a customized preference ??? "+ sharedPreference);
		}
		DbMap conditions = new DbMap();
		conditions.put(E.PREFERENCE.sharedPreference, sharedPreference.getPrimaryKey());
		DbDeleteStatement<UID> delete = new DbDeleteStatement<>(E.PREFERENCE, conditions);
		dataBaseHelper.execute(delete);
		evictPreferences();
	}

	/**
	 * delete customizations for users who lost their grant completely
	 * 
	 * @param prefUID
	 * @param superUsersToo usually, super users customizations are not deleted.
	 *        Set it true handles a super user like a default one.
	 */
	@Override
	public void batchDeleteCustomizations(UID prefUID, boolean superUsersToo) {
		this.batchDeleteCustomizations(prefUID, null, superUsersToo);
	}

	/**
	 * delete customizations for the given user, but only if he or she lost their grant completely
	 * 
	 * @param userUID
	 */
	@Override
	public void batchDeleteCustomizationsForUser(UID userUID) {
		this.batchDeleteCustomizations(null, userUID, false);
	}

	static final Collection<FieldMeta<?>> PREF_FIELDS_FOR_SELECTION = new ArrayList<>();
	static {
		PREF_FIELDS_FOR_SELECTION.add(E.PREFERENCE.app);
		PREF_FIELDS_FOR_SELECTION.add(E.PREFERENCE.entity);
		PREF_FIELDS_FOR_SELECTION.add(E.PREFERENCE.type);
	}

	/**
	 * select the given preference for the user and layout. Overwrites a selection of the same type, entity and app (user dependent)
	 * @param prefUID
	 * @param userUID
	 * @param layoutUID (optional)
	 *                  layoutUID checked against a possible layoutUID in the pref. But only the the given layoutUID is used for selection!
	 */
	@Override
	public void selectPreferenceForUser(final UID prefUID, final UID userUID, final UID layoutUID) {
		if (prefUID == null) {
			throw new IllegalArgumentException();
		}
		final PreferenceVO pref = getByPrimaryKey(prefUID, PREF_FIELDS_FOR_SELECTION);
		this.selectPreferenceForUser(pref, userUID, layoutUID);
	}

	/**
	 * select the given preference for the user and layout. Overwrites a selection of the same type, entity and app (user dependent)
	 * @param pref
	 * @param userUID
	 * @param layoutUID (optional)
	 * 					layoutUID checked against a possible layoutUID in the pref. But only the the given layoutUID is used for selection!
	 * @return false: if selection is not possible, e.g. subform-table without layout declaration
	 */
	@Override
	public boolean selectPreferenceForUser(final PreferenceVO pref, final UID userUID, final UID layoutUID) {
		if (pref == null || userUID == null) {
			throw new IllegalArgumentException();
		}
		if (pref.getLayout() != null && layoutUID != null && !pref.getLayout().equals(layoutUID)) {
			throw new IllegalArgumentException("Layout id does not match, pref=" + pref.getLayout() + ", param=" + layoutUID);
		}
		if (NuclosPreferenceType.SUBFORMTABLE.getType().equals(pref.getType()) && layoutUID == null) {
			LOG.warn("Selection without layout declaration is not allowed for type " + pref.getType() + ". prefId=" + pref.getPrimaryKey().getString());
			return false;
		}
		final UID prefUID = RigidUtils.defaultIfNull(pref.getSharedPreference(), pref.getPrimaryKey());
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbMap mapValues = new DbMap();
		mapValues.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		mapValues.put(SF.VERSION, DbIncrement.INCREMENT);
		mapValues.put(E.PREFERENCE_SELECTED.selectedPreference, prefUID);

		// try to update a selection for user, type, entity and app, first. if nothing updated insert a selection.
		final DbUpdate update = builder.createUpdate(E.PREFERENCE_SELECTED, mapValues);
		final List<DbCondition> ands = new ArrayList<>();
		ands.add(builder.equalValue(update.baseColumn(E.PREFERENCE_SELECTED.user), userUID));
		if (pref.getType() == null) {
			ands.add(builder.isNull(update.baseColumn(E.PREFERENCE_SELECTED.type)));
		} else {
			ands.add(builder.equalValue(update.baseColumn(E.PREFERENCE_SELECTED.type), pref.getType()));
		}
		if (pref.getEntity() == null) {
			ands.add(builder.isNull(update.baseColumn(E.PREFERENCE_SELECTED.entity)));
		} else {
			ands.add(builder.equalValue(update.baseColumn(E.PREFERENCE_SELECTED.entity), pref.getEntity()));
		}
		if (layoutUID != null) {
			ands.add(builder.equalValue(update.baseColumn(E.PREFERENCE_SELECTED.layout), layoutUID));
		}
		if (pref.getApp() == null) {
			ands.add(builder.isNull(update.baseColumn(E.PREFERENCE_SELECTED.app)));
		} else {
			ands.add(builder.equalValue(update.baseColumn(E.PREFERENCE_SELECTED.app), pref.getApp()));
		}
		update.where(builder.and(ands.toArray(new DbCondition[] {})));
		int updated = dataBaseHelper.getDbAccess().executeUpdate(update);
		if (updated > 1) {
			final DbDelete delete = builder.createDelete(E.PREFERENCE_SELECTED);
			delete.where(builder.and(ands.toArray(new DbCondition[] {})));
			dataBaseHelper.getDbAccess().executeDelete(delete);
			LOG.warn("Selection for preference " + pref.getName() + " (UID=" + prefUID.getString() + " not unique! User=" + userUID.getString() + ", Layout=" + (layoutUID==null?"null":layoutUID.getString()));
			updated = 0;
		}
		if (updated == 0) {
			// insert new selection.
			mapValues.put(E.PREFERENCE_SELECTED.getPk(), new UID());
			mapValues.put(E.PREFERENCE_SELECTED.user, userUID);
			mapValues.put(E.PREFERENCE_SELECTED.type, pref.getType());
			if (pref.getEntity() != null) {
				mapValues.put(E.PREFERENCE_SELECTED.entity, pref.getEntity());
			}
			if (layoutUID != null) {
				mapValues.put(E.PREFERENCE_SELECTED.layout, layoutUID);
			}
			mapValues.put(E.PREFERENCE_SELECTED.app, pref.getApp());
			final String userName = SecurityCache.getInstance().getUserName(userUID);
			mapValues.put(SF.CREATEDBY, userName);
			mapValues.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
			mapValues.put(SF.CHANGEDBY, userName);
			mapValues.put(SF.VERSION, 1);
			DbInsertStatement insert = new DbInsertStatement(E.PREFERENCE_SELECTED, mapValues);
			dataBaseHelper.getDbAccess().execute(insert);
		}
		return true;
	}

	/**
	 * de-select the given preference for the user.
	 * @param prefUID
	 * @param userUID
	 * @param layoutUID (optional)
	 */
	@Override
	public void deselectPreferenceForUser(UID prefUID, UID userUID, final UID layoutUID) {
		if (prefUID == null || userUID == null) {
			throw new IllegalArgumentException();
		}
		final DbMap mapCondition = new DbMap();
		mapCondition.put(E.PREFERENCE_SELECTED.selectedPreference, prefUID);
		mapCondition.put(E.PREFERENCE_SELECTED.user, userUID);
		if (layoutUID != null) {
			mapCondition.put(E.PREFERENCE_SELECTED.layout, layoutUID);
		}
		dataBaseHelper.getDbAccess().execute(new DbDeleteStatement(E.PREFERENCE_SELECTED, mapCondition));
	}

	/**
	 * delete all customized preferences of these types for the given user and entity.
	 * @param userUID
	 * @param entity
	 * @param type
	 */
	@Override
	public void batchDeleteAllCustomizationsForUser(UID userUID, UID entity, String...type) {
		if (userUID == null) {
			throw new IllegalArgumentException();
		}
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		final DbDelete delete = builder.createDelete(E.PREFERENCE);
		delete.where(builder.equalValue(delete.baseColumn(E.PREFERENCE.user), userUID));
		delete.addToWhereAsAnd(builder.isNotNull(E.PREFERENCE.sharedPreference));

		if (entity != null) {
			delete.addToWhereAsAnd(builder.equalValue(delete.baseColumn(E.PREFERENCE.entity), entity));
		}

		if (type != null) {
			Collection<DbCondition> conditions = new ArrayList<>();
			for (String t : type) {
				final DbCondition cond = builder.equalValue(delete.baseColumn(E.PREFERENCE.type), t);
				conditions.add(cond);
			}
			delete.addToWhereAsAnd(builder.or(conditions));
		}

		dataBaseHelper.getDbAccess().executeDelete(delete);
		evictPreferences();
	}

	private void batchDeleteCustomizations(UID prefUID, UID userUID, boolean superUsersToo) {
		if (prefUID != null && userUID != null) {
			throw new IllegalArgumentException();
		}
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

		final DbQuery<UID> subSuperuser = builder.createQuery(UID.class);
		final DbFrom<UID> subSUPERU = subSuperuser.from(E.USER, "SUPERU");
		subSuperuser.select(subSUPERU.basePk());
		subSuperuser.where(builder.equalValue(subSUPERU.baseColumn(E.USER.superuser), true));

		final DbQuery<UID> subSharing = builder.createQuery(UID.class);
		final DbFrom<UID> subROLEP = subSharing.from(E.ROLEPREFERENCE, "ROLEP");
		final DbFrom<UID> subROLEU = subSharing.from(E.ROLEUSER, "ROLEU");
		subSharing.select(subROLEP.baseColumn(E.ROLEPREFERENCE.preference));
		subSharing.where(builder.equal(subROLEP.baseColumn(E.ROLEPREFERENCE.role), subROLEU.baseColumn(E.ROLEUSER.role)));

		final DbDelete delete = builder.createDelete(E.PREFERENCE);
		if (prefUID != null) {
			delete.where(builder.equalValue(delete.baseColumn(E.PREFERENCE.sharedPreference), prefUID));
		} else {
			delete.where(builder.equalValue(delete.baseColumn(E.PREFERENCE.user), userUID));
			delete.addToWhereAsAnd(builder.isNotNull(E.PREFERENCE.sharedPreference));
		}

		subSharing.addToWhereAsAnd(builder.equal(subROLEU.baseColumn(E.ROLEUSER.user), delete.baseColumn(E.PREFERENCE.user)));
		delete.addToWhereAsAnd(builder.not(builder.in(delete.baseColumn(E.PREFERENCE.sharedPreference), subSharing)));
		if (!superUsersToo) {
			delete.addToWhereAsAnd(builder.not(builder.in(delete.baseColumn(E.PREFERENCE.user), subSuperuser)));
		}

		dataBaseHelper.getDbAccess().executeDelete(delete);
		evictPreferences();
	}

	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<UID> dalVO) throws DbException {
		throw new NotImplementedException();
	}

	@Override
	public List<PreferenceVO> getAll() {
		throw new NuclosFatalException("not supported");
	}

	@Override
	public PreferenceVO getByPrimaryKey(UID id) {
		return super.getByPrimaryKey(id);
	}

	@Override
	public PreferenceVO getByPrimaryKeyWithSelected(UID id, UID userUID) {
		PreferenceVO result = super.getByPrimaryKey(id);
		if (result != null) {
			final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			final DbQuery<Long> query = builder.createQuery(Long.class);
			final DbFrom<UID> from = query.from(E.PREFERENCE_SELECTED);
			query.select(builder.countRows());
			query.where(builder.equalValue(from.baseColumn(E.PREFERENCE_SELECTED.user), userUID));
			query.addToWhereAsAnd(builder.equalValue(from.baseColumn(E.PREFERENCE_SELECTED.selectedPreference), id));
			final Long countSelected = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			result.setSelected(countSelected > 0L);
			if (NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT, false)) {
				result.getPrimaryKey().setDebugInfo(result.getType() + ": " + result.getName());
			}
		}
		return result;
	}

	@Override
	public List<PreferenceVO> getByPrimaryKeys(List<UID> ids) {
		throw new NuclosFatalException("not supported");
	}

	//TODO: Implement more particular CacheEvict, e.g. by user
	@CacheEvict(value = "prefsByPrefParameter", allEntries = true)
	public void evictPreferences() {
	}

	private List<PreferenceVO> getPrefsByPrefParameter(PrefParameter p) {
		final List<PreferenceVO> result = getPrefsByPrefParameterCached(p);

		// add the selection in every cached result for a user dependent query only
		final Set<UID> selectedPrefs;
		if (p.user != null) {
			final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			final DbQuery<UID> query = builder.createQuery(UID.class);
			final DbFrom<UID> from = query.from(E.PREFERENCE_SELECTED);
			query.select(from.baseColumn(E.PREFERENCE_SELECTED.selectedPreference));
			addBaseCondition(p, query, from.baseColumn(E.PREFERENCE_SELECTED.app),
					from.baseColumn(E.PREFERENCE_SELECTED.type),
					from.baseColumn(E.PREFERENCE_SELECTED.entity),
					from.baseColumn(E.PREFERENCE_SELECTED.layout));
			query.addToWhereAsAnd(builder.equalValue(from.baseColumn(E.PREFERENCE_SELECTED.user), p.user));
			selectedPrefs = new HashSet<>(dataBaseHelper.getDbAccess().executeQuery(query));
		} else {
			selectedPrefs = null;
		}

		for (PreferenceVO pref : result) {
			pref.setSelected(selectedPrefs != null &&
					(selectedPrefs.contains(RigidUtils.defaultIfNull(pref.getSharedPreference(), pref.getPrimaryKey()))));
		}
		return result;
	}

	@Cacheable(value = "prefsByPrefParameter", key = "#p0.hashCode()")
	private List<PreferenceVO> getPrefsByPrefParameterCached(PrefParameter p) {
		String app = p.app == null ? PreferenceVO.APP_NUCLOS : p.app;

		final DbQuery<Object[]> query = createQuery(allColumns, true, null);
		final DbFrom<UID> from = (DbFrom<UID>) CollectionUtils.getSingleIfExist(query.getRoots());
		final DbQueryBuilder builder = query.getBuilder();
		addBaseCondition(p, query, from.baseColumn(appColumn),
				from.baseColumn(typeColumn),
				from.baseColumn(entityColumn),
				from.baseColumn(layoutColumn));

		if (p.userIsNull) {
			query.addToWhereAsAnd(builder.isNull(from.baseColumn(userColumn)));
		} else if (p.user != null) {
			if (isSuperuser(p.user)) {
				// all shared preferences
				query.addToWhereAsAnd(builder.or(
						builder.equalValue(from.baseColumn(userColumn), p.user),
						builder.isNull(from.baseColumn(userColumn))));
			} else {
				// shared preferences
				final DbQuery<UID> roleQuery = builder.createQuery(UID.class);
				final DbFrom<UID> fromROLEP = roleQuery.from(E.ROLEPREFERENCE, "ROLEP");
				final DbFrom<UID> fromROLEU = roleQuery.from(E.ROLEUSER, "ROLEU");
				roleQuery.select(fromROLEP.baseColumn(E.ROLEPREFERENCE.preference));
				roleQuery.where(builder.equal(fromROLEP.baseColumn(E.ROLEPREFERENCE.role), fromROLEU.baseColumn(E.ROLEUSER.role)));
				roleQuery.addToWhereAsAnd(builder.equalValue(fromROLEU.baseColumn(E.ROLEUSER.user), p.user));

				query.addToWhereAsAnd(builder.or(
						builder.equalValue(from.baseColumn(userColumn), p.user),
						builder.in(from.basePk(), roleQuery)));
			}
		}
		if (p.nucletIsNull) {
			query.addToWhereAsAnd(builder.isNull(from.baseColumn(nucletColumn)));
		} else if (p.nuclet != null) {
			query.addToWhereAsAnd(builder.equalValue(from.baseColumn(nucletColumn), p.nuclet));
		}
		if (p.menuRelevant != null) {
			if (Boolean.TRUE.equals(p.menuRelevant)) {
				query.addToWhereAsAnd(builder.equalValue(from.baseColumn(menuRelevantColumn), true));
			} else {
				query.addToWhereAsAnd(builder.or(builder.equalValue(from.baseColumn(menuRelevantColumn), false),
												 builder.isNull(from.baseColumn(menuRelevantColumn))));
			}
		}
		if (p.sharedPreferenceKey != null) {
			query.addToWhereAsAnd(builder.equalValue(from.baseColumn(sharedColumn), p.sharedPreferenceKey));
		}

		final Map<UID, PreferenceVO> mapSharedPreferences = new HashMap<>();
		final List<PreferenceVO> dbResult = dataBaseHelper.getDbAccess().executeQuery(query, createResultTransformer(allColumns));
		final Set<UID> sharedCustomizationsPrefs = new HashSet<UID>();
		for (PreferenceVO pref : dbResult) {
			if (pref.getSharedPreference() != null) {
				// is customized
				sharedCustomizationsPrefs.add(pref.getSharedPreference());
			}
			if (pref.getUser() == null) {
				// is shared
				mapSharedPreferences.put(pref.getPrimaryKey(), pref);
			}
		}
		final List<PreferenceVO> result = new ArrayList<>();
		for (PreferenceVO pref : dbResult) {
			if (pref.getSharedPreference() != null) {
				// is customized, copy Nuclet id from shared
				final PreferenceVO sharedPref = mapSharedPreferences.get(pref.getSharedPreference());
				if (sharedPref != null) { // could not be null, but for safety
					pref.setNuclet(sharedPref.getNuclet());
				}
			}
			if (sharedCustomizationsPrefs.contains(pref.getPrimaryKey())) {
				// ignore origin shared pref if customization exist
				continue;
			}
			if (NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT, false)) {
				pref.getPrimaryKey().setDebugInfo(pref.getType() + ": " + pref.getName());
			}
			result.add(pref);
		}

		return result;
	}

	/**
	 * base condition for preferences and selected
	 */
	private static void addBaseCondition(final PrefParameter p, final DbQuery<?> query,
								  final DbColumnExpression<String> appColumn,
								  final DbColumnExpression<String> typeColumn,
								  final DbColumnExpression<UID> entityColumn,
								  final DbColumnExpression<UID> layoutColumn) {
		final DbQueryBuilder builder = query.getBuilder();
		if (p.app != null) {
			query.addToWhereAsAnd(builder.equalValue(appColumn, p.app));
		}
		if (!p.types.isEmpty()) {
			List<DbCondition> conditions = new ArrayList<>();
			for (String type: p.types) {
				DbCondition dbCondition = builder.equalValue(typeColumn, type);
				conditions.add(dbCondition);
			}
			query.addToWhereAsAnd(builder.or(conditions));
		}
		if (p.entityIsNull) {
			query.addToWhereAsAnd(builder.isNull(entityColumn));
		} else if (p.entity != null) {
			if (p.entity.size() == 1) {
				query.addToWhereAsAnd(builder.equalValue(entityColumn, p.entity.iterator().next()));
			} else {
				final Collection<DbCondition> entityOrs = new ArrayList<>();
				final Iterator<UID> it = p.entity.iterator();
				while (it.hasNext()) {
					entityOrs.add(builder.equalValue(entityColumn, it.next()));
				}
				query.addToWhereAsAnd(builder.or(entityOrs));
			}
			final DbCondition condLayoutEqual = p.layout != null ? builder.equalValue(layoutColumn, p.layout) : null;
			final DbCondition condLayoutIsNull = p.orLayoutIsNull ? builder.isNull(layoutColumn) : null;

			if (condLayoutEqual != null && condLayoutIsNull != null) {
				query.addToWhereAsAnd(builder.or(condLayoutEqual, condLayoutIsNull));
			} else {
				if (condLayoutEqual != null) {
					query.addToWhereAsAnd(condLayoutEqual);
				} else if (condLayoutIsNull != null) {
					query.addToWhereAsAnd(condLayoutIsNull);
				}
			}
		}
	}

	@Override
	public List<PreferenceVO> getByAppAndType(String app, String type) {
		return getPrefsByPrefParameter(new PrefParameter(app, type, null, false, null, false, null, false, null, false, null, null));
	}

	@Override
	public List<PreferenceVO> getByAppTypeAndUser(String app, String type, UID user) {
		return getPrefsByPrefParameter(new PrefParameter(app, type, user, user == null, null, false, null, false, null, false, null, null));
	}
	
	@Override
	public List<PreferenceVO> getByAppTypeUserAndMenuRelevance(String app, String type, UID user, boolean menuRelevant) {
		return getPrefsByPrefParameter(new PrefParameter(app, type, user, user == null, null, false, null, false, null, false, menuRelevant, null));
	}

	@Override
	public List<PreferenceVO> getByAppTypeUserAndEntity(String app, String type, UID user, Set<UID> entity, UID layout, boolean orLayoutIsNull) {
		return getPrefsByPrefParameter(new PrefParameter(app, type, user, user == null, entity, entity == null, layout, orLayoutIsNull, null, false, null, null));
	}

	@Override
	public List<PreferenceVO> getByAppAndNuclet(String app, UID nuclet) {
		return getPrefsByPrefParameter(new PrefParameter(app, null, null, false, null, false, null, false, nuclet, nuclet == null, null, null));
	}

	@Override
	public List<PreferenceVO> getByAppAndUser(String app, UID user) {
		return getPrefsByPrefParameter(new PrefParameter(app, null, user, user == null, null, false, null, false, null, false, null, null));
	}
	
	@Override
	public List<PreferenceVO> getByAppUserAndMenuRelevance(String app, UID user, boolean menuRelevant) {
		return getPrefsByPrefParameter(new PrefParameter(app, null, user, user == null, null, false, null, false, null, false, menuRelevant, null));
	}

	@Override
	public List<PreferenceVO> getByAppUserAndEntity(String app, UID user, Set<UID> entity, UID layout, boolean orLayoutIsNull) {
		return getPrefsByPrefParameter(new PrefParameter(app, null, user, user == null, entity, entity == null, layout, orLayoutIsNull, null, false, null, null));
	}

	@Override
	public PreferenceVO getBySharedKeyAndUser(UID sharedPreferenceKey, UID user) {
		List<PreferenceVO> list =  getPrefsByPrefParameter(new PrefParameter(null, null, user, user == null, null, false, null, false, null, false, null, sharedPreferenceKey));
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	private boolean isSuperuser(UID user) {
		return SecurityCache.getInstance().isSuperUser(SecurityCache.getInstance().getUserName(user));
	}

	private static class PrefParameter {
		private final String app;
		private final Set<String> types;
		private final UID user;
		private final boolean userIsNull;
		private final Set<UID> entity;
		private final boolean entityIsNull;
		private final UID layout;
		private final boolean orLayoutIsNull;
		private final UID nuclet;
		private final boolean nucletIsNull;
		private final Boolean menuRelevant;
		private final UID sharedPreferenceKey;

		private PrefParameter(
				String app,
				String types,
				UID user,
				boolean userIsNull,
				Set<UID> entity,
				boolean entityIsNull,
				UID layout,
				boolean orLayoutIsNull,
				UID nuclet,
				boolean nucletIsNull,
				Boolean menuRelevant,
				UID sharedPreferenceKey
		) {
			this.app = app;

			this.types = new HashSet<>();
			addTypes(types);

			this.user = user;
			this.userIsNull = userIsNull;
			this.entity = entity;
			this.entityIsNull = entityIsNull;
			this.layout = layout;
			this.orLayoutIsNull = orLayoutIsNull;
			this.nuclet = nuclet;
			this.nucletIsNull = nucletIsNull;
			this.menuRelevant = menuRelevant;
			this.sharedPreferenceKey = sharedPreferenceKey;
		}

		private void addTypes(final String types) {
			if (types != null) {
				String[] typeArray = types.split(",");
				for (String type: typeArray) {
					this.types.add(type);
				}
			}
		}

		@Override
		public int hashCode() {
			return LangUtils.hash(
					app,
					types,
					user,
					userIsNull,
					entity,
					entityIsNull,
					layout,
					orLayoutIsNull,
					nuclet,
					nucletIsNull,
					menuRelevant,
					sharedPreferenceKey
			);
		}
	}

}
