//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor;

import java.sql.SQLException;

import javax.json.JsonObject;

import org.nuclos.common.JsonUtils;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.RigidFile;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.util.ServerCryptUtil;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.resource.valueobject.ResourceFile;

/**
 * Type parameter T is the java type
 */
abstract class AbstractColumnToVOMapping<T, PK> implements IColumnToVOMapping<T, PK> {

	private final String tableAlias;
	private final String column;
	private final UID field;
	private final Class<T> dataType;
	private final boolean isReadonly;
	private final boolean caseSensitive;

	AbstractColumnToVOMapping(String tableAlias, String column, UID field, String dataType, boolean isReadonly, boolean caseSensitive) throws ClassNotFoundException {
		this(tableAlias, column, field, (Class<T>) Class.forName(dataType), isReadonly, caseSensitive);
	}

	AbstractColumnToVOMapping(String tableAlias, String column, UID field, Class<T> dataType, boolean isReadonly, boolean caseSensitive) {
		if (tableAlias == null) throw new NullPointerException();
		if (column == null) throw new NullPointerException();
		if (field == null) throw new NullPointerException();
		this.tableAlias = tableAlias;
		this.column = column;
		this.field = field;
		this.dataType = dataType;
		this.isReadonly = isReadonly;
		this.caseSensitive = caseSensitive;
	}

	@Override
	public String getTableAlias() {
		return tableAlias;
	}

	@Override
	public final String getColumn() {
		return column;
	}
	
	@Override
	public UID getUID() {
		return field;
	}

	@Override
	public final boolean isCaseSensitive() {
		return caseSensitive;
	}

	@Override
	public final boolean isReadonly() {
		return isReadonly;
	}

	@Override
	public final Class<T> getDataType() {
		return dataType;
	}
	
	@Override
	public String getDbColumn() {
		return getColumn();
	}

	@Override
	public Class<T> getJavaClass() {
		return (Class<T>) DbUtils.getDbType(getDataType());
	}

	@Override
	public DbExpression<T> getDbColumn(DbFrom<PK> table) {
		if (isCaseSensitive()) {
			return table.columnCaseSensitive(getTableAlias(), this);
		}
		else {
			return table.column(getTableAlias(), this);
		}
	}

	<S> S convertFromDbValue(Object value, String column, final Class<S> dataType, final PK recordPk) {
		if (dataType == ResourceFile.class) {
			if (value == null) {
				return null;
			}
			RigidFile rf = (RigidFile) value;
			return (S) new ResourceFile(rf.getFilename(), recordPk);
		} else if (dataType == GenericObjectDocumentFile.class) {
			if (value == null) {
				return null;
			}
			RigidFile rf = (RigidFile) value;
			return (S) new GenericObjectDocumentFile(rf.getFilename(), recordPk);
		} else if (dataType == NuclosPassword.class) {
			try {
				return (S) new NuclosPassword(ServerCryptUtil.decrypt((String) value));
			} catch (SQLException e) {
				throw new CommonFatalException(e);
			}
		} else if (dataType == JsonObject.class) {
			if (value instanceof JsonObject) {
				return (S) value;
			} else if (value instanceof String) {
				return (S) JsonUtils.stringToObject((String) value);
			} else {
				throw new IllegalArgumentException("Value " + value + " could not be converted to " + JsonObject.class.getCanonicalName());
			}
		}
		else {
			return dataType.cast(value);
		}
	}

	static Object convertToDbValue(Class<?> javaType, Object value) {
		if (value == null) {
			return DbNull.forType(DalUtils.getDbType(javaType));
		} else if (value instanceof ByteArrayCarrier) {
			return ((ByteArrayCarrier) value).getData();
		} else if (value instanceof NuclosImage) {
			NuclosImage ni = (NuclosImage) value;
			if (ni.getContent() != null) {
				ByteArrayCarrier bac = new ByteArrayCarrier(ni.getContent());
				return bac.getData();
			} else {
				return DbNull.forType(DalUtils.getDbType(javaType));
			}
		} else if (value instanceof ResourceFile) {
			return ((ResourceFile) value).getFilename();
		} else if (value instanceof GenericObjectDocumentFile) {
			return ((GenericObjectDocumentFile) value).getFilename();
		} else if (value instanceof DateTime) {
			return new InternalTimestamp(((DateTime) value).getTime());
		} else if (value instanceof NuclosPassword) {
			try {
				String encrypted = ServerCryptUtil.encrypt(((NuclosPassword) value).getValue());
				if (encrypted == null) {
					return DbNull.forType(java.lang.String.class);
				} else {
					return encrypted;
				}
			} catch (SQLException e) {
				throw new CommonFatalException(e);
			}
		} else if (value instanceof JsonObject) {
			return JsonUtils.objectToString((JsonObject) value);
		} else {
			return value;
		}
	}


}
