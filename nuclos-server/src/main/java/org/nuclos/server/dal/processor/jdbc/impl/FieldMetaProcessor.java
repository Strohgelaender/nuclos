//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.jdbc.impl;

import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.dal.processor.IColumnToVOMapping;
import org.nuclos.server.dal.processor.jdbc.AbstractJdbcDalProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityFieldMetaDataProcessor;
import org.nuclos.server.dblayer.DbException;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class FieldMetaProcessor extends AbstractJdbcDalProcessor<NucletFieldMeta<?>, UID> 
	implements JdbcEntityFieldMetaDataProcessor {
	
	private final IColumnToVOMapping<UID, UID> uidColumn;
	private final IColumnToVOMapping<UID, UID> entityUidColumn;
	
	@SuppressWarnings("unchecked")
	public FieldMetaProcessor(List<IColumnToVOMapping<? extends Object, UID>> allColumns, 
			IColumnToVOMapping<UID, UID> entityUidColumn, IColumnToVOMapping<UID, UID> uidColumn) {
		super(E.ENTITYFIELD.getUID(), 
				LangUtils.getGenericClass(NucletFieldMeta.class),
				UID.class, allColumns);
		this.entityUidColumn = entityUidColumn;
		this.uidColumn = uidColumn;
	}
	
	@Override
	public EntityMeta<UID> getMetaData() {
		return E.ENTITYFIELD;
	}
	
	@Override
	protected IColumnToVOMapping<UID, UID> getPrimaryKeyColumn() {
		return uidColumn;
	}

	@Override
	public List<NucletFieldMeta<?>> getAll() {
		return super.getAll();
	}

	@Override
	public List<NucletFieldMeta<?>> getByParent(UID entity) {
		return super.getByColumn(allColumns, entityUidColumn, entity);
	}

	@Override
	public void delete(Delete<UID> id) throws DbException {
		super.delete(id);
	}

	@Override
	public NucletFieldMeta<?> getByPrimaryKey(UID uid) {
		return super.getByPrimaryKey(uid);
	}
	
	@Override
	public List<NucletFieldMeta<?>> getByPrimaryKeys(List<UID> uids) {
		return super.getByPrimaryKeys(allColumns, uids);
	}

	@Override
	public Object insertOrUpdate(NucletFieldMeta<?> dalVO) {
		return super.insertOrUpdate(dalVO);
	}

	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<UID> dalVO) throws DbException {
		throw new NotImplementedException();
	}
	
	@Override
	protected void setDebugInfo(NucletFieldMeta<?> dalVO) {
		if (dalVO.getPrimaryKey() != null) {
			dalVO.getPrimaryKey().setDebugInfo(dalVO.getFieldName());
		}
   	}
	
}
