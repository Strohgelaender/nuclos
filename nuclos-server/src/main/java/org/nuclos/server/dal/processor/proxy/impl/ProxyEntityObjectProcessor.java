//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.processor.proxy.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.businessentity.utils.BusinessObjectBuilderForInternalUse;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.GeneralJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.ReferencingCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.AbstractDalProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.eventsupport.ejb3.ProxyContext;
import org.nuclos.server.eventsupport.ejb3.ProxyContext.Type;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class ProxyEntityObjectProcessor<PK> extends AbstractDalProcessor<EntityObjectVO<PK>, PK>
	implements IEntityObjectProcessor<PK>{
	
	private static final Logger LOG = LoggerFactory.getLogger(ProxyEntityObjectProcessor.class);
	
	private final DependentSearchConditionVisitor dependentVisitor;
	
	private boolean ignoreRecordGrantsAndOthers = false;
	
	private boolean thinReadEnabled = false;
	
	private EventSupportFacadeLocal _evsuFacade;
	
	public ProxyEntityObjectProcessor(EntityMeta<?> eMeta, Class<EntityObjectVO<PK>> type, Class<PK> pkType) {
		super(eMeta.getUID(), type, pkType);
		dependentVisitor = new DependentSearchConditionVisitor();
	}

	public EventSupportFacadeLocal getEventSupportFacade() {
		if (_evsuFacade == null) {
			_evsuFacade = SpringApplicationContextHolder.getBean(EventSupportFacadeLocal.class);
		}
		return _evsuFacade;
	}
	
	@Override
	public void cancelRunningStatements() {
		// TODO NUCLOS-5224 Implement for Proxy Entities
	}
	
	/**
	 * 
	 * @param pc
	 * @return proxy object (for rollback)
	 */
	private Object executeCall(final ProxyContext<PK> pc) {
		try {
			return getEventSupportFacade().executeProxyCall(pc);
		} catch (NuclosBusinessRuleException e) {
			String error = String.format("Proxy getter for entity %s not accessible: %s", getEntityUID(), e.getCause()!=null?e.getCause().toString():e.getMessage());
			LOG.error(error, e);
			throw new NuclosFatalException(error, e);
		} catch (NuclosCompileException e) {
			String error = String.format("Proxy getter for entity %s not accessible: %s", getEntityUID(), e.getCause()!=null?e.getCause().toString():e.getMessage());
			LOG.error(error, e);
			throw new NuclosFatalException(error, e);
		}
	}
	
	

	@Override
	public List<EntityObjectVO<PK>> getAll() {
		final ProxyContext<PK> pc = new ProxyContext<PK>(getEntityUID(), getPkType(), ProxyContext.Type.GET_ALL);
		executeCall(pc);
		return pc.getMultiResult();
	}

	@Override
	public List<PK> getAllIds() {
		final ProxyContext<PK> pc = new ProxyContext<PK>(getEntityUID(), getPkType(), ProxyContext.Type.GET_ALL_IDS);
		executeCall(pc);
		return pc.getIdsResult();
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(PK id) {
		final ProxyContext<PK> pc = new ProxyContext<PK>(getEntityUID(), getPkType(), ProxyContext.Type.GET_BY_ID);
		pc.setParamId(id);
		executeCall(pc);
		return pc.getSingleResult();
	}

	@Override
	public EntityObjectVO<PK> getByPrimaryKey(PK id, Collection<FieldMeta<?>> fields) {
		return getByPrimaryKey(id);
	}

	@Override
	public List<EntityObjectVO<PK>> getByPrimaryKeys(List<PK> ids) {
		final ProxyContext<PK> pc = new ProxyContext<PK>(getEntityUID(), getPkType(), ProxyContext.Type.GET_BY_IDS);
		pc.setParamIds(ids);
		executeCall(pc);
		return pc.getMultiResult();
	}
	
	private List<EntityObjectVO<PK>> getByForeignKeys(UID foreignFieldUid, Collection<Object> keysSearchFor) {
		MetaProvider provider = MetaProvider.getInstance();
		FieldMeta<?> foreignField = provider.getEntityField(foreignFieldUid);		
		EntityMeta<?> eRefMeta = provider.getEntity(foreignField.getForeignEntity());

		final boolean bForInternalUseOnly = E.isNuclosEntity(eRefMeta.getUID()) && BusinessObjectBuilderForInternalUse.getEntityMetas().contains(eRefMeta);
		String foreignMethodName = NuclosBusinessObjectBuilder.getProxyGetterForeignMethodName(foreignField);
		final ProxyContext<PK> pc = new ProxyContext<PK>(getEntityUID(), getPkType(), ProxyContext.Type.GET_BY_FOREIGN_KEY);
		pc.setParamForeignMethodName(foreignMethodName);
		pc.setParamForeignIdClass(eRefMeta.isUidEntity() ?
				(bForInternalUseOnly ? org.nuclos.common.UID.class : org.nuclos.api.UID.class) :
				Long.class);
		pc.setParamForeignIds(keysSearchFor);
		executeCall(pc);
		return pc.getMultiResult();
	}

	@Override
	public PK insertOrUpdate(EntityObjectVO<PK> dalVO) throws DbException {
		final ProxyContext<PK> pc = new ProxyContext<PK>(getEntityUID(), getPkType(), Type.INSERT_OR_UPDATE);
		pc.setParamObject(dalVO);
		Object proxy = executeCall(pc);
		registerTransactionSynchronization(getEntityUID(), proxy);
		return null; // TODO ggf. noch implementieren. der Return Value wird aber erstmal nur für den WriteProxy  gebraucht. Daher hier null.
	}

	@Override
	public void delete(Delete<PK> del) throws DbException {
		final ProxyContext<PK> pc = new ProxyContext<PK>(getEntityUID(), getPkType(), Type.DELETE);
		pc.setParamId(del.getPrimaryKey());
		Object proxy = executeCall(pc);
		registerTransactionSynchronization(getEntityUID(), proxy);
	}

	@Override
	public List<EntityObjectVO<PK>> getBySearchExpression(CollectableSearchExpression clctexpr) {
		return getBySearchExprResultParams(clctexpr, ResultParams.DEFAULT_RESULT_PARAMS);
	}

	@Override
	public List<EntityObjectVO<PK>> getBySearchExprResultParams(CollectableSearchExpression clctexpr, ResultParams resultParams) {
	
		final List<EntityObjectVO<PK>> result = clctexpr.getSearchCondition().accept(dependentVisitor);
		if (resultParams.isSortResult()) {
			final List<CollectableSorting> sorts = clctexpr.getSortingOrder();
			EntityObjectUtils.sort(result, sorts);
		}
		return result;
	}
	
	@Override
	public List<PK> getIdsBySearchExpression(CollectableSearchExpression clctexpr) {
		throw new NotImplementedException();
	}

	@Override
	public Integer getVersion(PK pk) {
		throw new NotImplementedException();
	}

	@Override
	public Long count(CollectableSearchExpression clctexpr) {
        //IMPL for RestClient, it is used by subform
		return null;
	}
	
	@Override
	public void checkLogicalUniqueConstraint(EntityObjectVO<PK> dalVO) throws DbException {
		// do nothing here, it is not configurable
	}
	
	private void registerTransactionSynchronization(final UID entity, final Object proxy) {
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCompletion(int status) {
				final String sStatus = (status == TransactionSynchronization.STATUS_COMMITTED) ? "Commit" : "Rollback";
				try {
					if (TransactionSynchronization.STATUS_COMMITTED == status) {
						getEventSupportFacade().finishProxyCall(entity, proxy, true);
					} else {
						getEventSupportFacade().finishProxyCall(entity, proxy, false);
					}
				} catch (NuclosBusinessRuleException e) {
					LOG.error("{} of proxy {} failed [entity={}]", sStatus, proxy, entity, e);
				} catch (NuclosCompileException e) {
					LOG.error("{} of proxy {} failed [entity={}]", sStatus, proxy, entity, e);
				}
			}
		});
	}
	
	public class DependentSearchConditionVisitor implements Visitor<List<EntityObjectVO<PK>>, NuclosFatalException> {

		@Override
		public List<EntityObjectVO<PK>> visitAtomicCondition(AtomicCollectableSearchCondition atomiccond) throws NuclosFatalException {
			if (atomiccond.isForeignKeyComparision()) {
				Object pk = ((CollectableComparison)atomiccond).getComparand().getValueId();
				return getByForeignKeys(atomiccond.getEntityField().getUID(), Collections.singleton(pk));
			}
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}
		
		@Override
		public List<EntityObjectVO<PK>> visitCompositeCondition(CompositeCollectableSearchCondition compositecond) throws NuclosFatalException {
		    List<CollectableSearchCondition> operands = compositecond.getOperands();
		    if (operands.size() == 1) {
				return operands.get(0).accept(dependentVisitor);
				
			} else if (operands.size() > 1 && compositecond.getLogicalOperator() == LogicalOperator.OR) {
				UID foreignField = null;
				Collection<Object> ids = new ArrayList<>();
				for (CollectableSearchCondition operand : operands) {
			        if (operand instanceof CollectableComparison) {
			        	CollectableComparison cc = (CollectableComparison) operand;
			        	if (cc.isForeignKeyComparision()) {
			        		UID localForeignField = cc.getEntityField().getUID();
			        		if (foreignField == null) {
			        			foreignField = localForeignField;
			        		}
			        		if (foreignField.equals(localForeignField)) {
			        			ids.add(cc.getComparand().getValueId());
			        			continue;
			        		}
			        	}
			        } 
			        
			        throw new IllegalArgumentException("SearchCondition is not a default dependent query");
                }
			    return getByForeignKeys(foreignField, ids);			    
            }
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}

		@Override
		public List<EntityObjectVO<PK>> visitTrueCondition(TrueCondition truecond) throws NuclosFatalException {
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}

		@Override
		public List<EntityObjectVO<PK>> visitIdCondition(CollectableIdCondition idcond)	throws NuclosFatalException {
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}

		@Override
		public List<EntityObjectVO<PK>> visitIdListCondition(CollectableIdListCondition collectableIdListCondition)	throws NuclosFatalException {
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}

		@Override
		public List<EntityObjectVO<PK>> visitSubCondition(CollectableSubCondition subcond) throws NuclosFatalException {
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}

		@Override
		public List<EntityObjectVO<PK>> visitRefJoinCondition(RefJoinCondition joincond) throws NuclosFatalException {
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}

		@Override
		public List<EntityObjectVO<PK>> visitGeneralJoinCondition(GeneralJoinCondition joincond) throws RuntimeException {
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}
		
		@Override
		public List<EntityObjectVO<PK>> visitReferencingCondition(ReferencingCollectableSearchCondition refcond) throws NuclosFatalException {
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}

		@Override
		public <T> List<EntityObjectVO<PK>> visitInCondition(CollectableInCondition<T> collectableInCondition) throws NuclosFatalException {
			throw new IllegalArgumentException("SearchCondition is not a default dependent query");
		}
		
	}
	
	@Override
	public void setIgnoreRecordGrantsAndOthers(boolean ignore) {
		this.ignoreRecordGrantsAndOthers = ignore;
	}

	@Override
	public boolean getIgnoreRecordGrantsAndOthers() {
		return this.ignoreRecordGrantsAndOthers;
	}

	@Override
	public void unlock(Object id) {
		throw new NotImplementedException();
	}

	@Override
	public void lock(PK id, UID userUID) {
		throw new NotImplementedException();
	}
	
	@Override
	public UID getOwner(PK id) {
		throw new NotImplementedException();
	}

	@Override
	public void setThinReadEnabled(boolean enabled) {
		this.thinReadEnabled = enabled;
	}

	@Override
	public boolean isThinReadEnabled() {
		return this.thinReadEnabled;
	}

}
