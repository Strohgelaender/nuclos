//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.metadata.TreeMetaProvider;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.i18n.language.data.DataLanguageFacadeRemote;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.RelationDirection;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.SystemRelationType;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Factory that creates <code>GenericObjectTreeNode</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
public class GenericObjectTreeNodeFactory {

	private static GenericObjectTreeNodeFactory singleton;
	
	private transient MetaProvider metaProvider;
	private transient TreeMetaProvider treeProvider;
	private transient SpringLocaleDelegate localeDelegate;
	private transient DataLanguageFacadeRemote dataLangFacade;
	
	protected GenericObjectTreeNodeFactory() {
		setTreeProvider((TreeMetaProvider)SpringApplicationContextHolder.getBean("treeService"));
		setMetaProvider(SpringApplicationContextHolder.getBean(MetaProvider.class));
		setLocaleDelegate(SpringLocaleDelegate.getInstance());
		setDataLangFacade(SpringApplicationContextHolder.getBean(DataLanguageFacadeRemote.class));
	}
	
	public static synchronized GenericObjectTreeNodeFactory getInstance() {
		if (singleton == null) {
			singleton = newFactory();
		}
		return singleton;
	}

	private static GenericObjectTreeNodeFactory newFactory() {
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getGenericObjectTreeNodeFactoryClassName(),
					GenericObjectTreeNodeFactory.class.getName());
			

			return (GenericObjectTreeNodeFactory) 
					LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName).newInstance();
		}
		catch (Exception ex) {
			throw new CommonFatalException("GenericObjectTreeNodeFactory cannot be created.", ex);
		}
	}

	public MetaProvider getMetaProvider() {
		return metaProvider;
	}
	//@Autowired
	public void setLocaleDelegate(SpringLocaleDelegate localeDelegate) {
		this.localeDelegate = localeDelegate;
	}

		
	//@Autowired
	public void setMetaProvider(MetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}
	
	//@Autowired
	public void setTreeProvider(TreeMetaProvider treeProvider) {
		this.treeProvider = treeProvider;
	}

	
	public TreeMetaProvider getTreeProvider() {
		return treeProvider;
	}
	
	/**
	 * creates a GenericObjectTreeNode.
	 * 
	 * §postcondition result != null
	 *
	 * @return a new GenericObjectTreeNode
	 */
	public GenericObjectTreeNode newTreeNode(
			GenericObjectWithDependantsVO gowdvo,
			Long iRelationId,
			SystemRelationType relationtype,
			RelationDirection direction,
			String sUserName,
			String customUsage,
			Long parentId,
			UID uidNode,
			Long idRoot,
			UID mandator
	) {
		String label = getIdentifier(gowdvo, uidNode);
		String description = getDescription(gowdvo, uidNode);
		GenericObjectTreeNodeParameters params = new GenericObjectTreeNodeParameters(
				gowdvo.getId(),
				gowdvo.getUsageCriteria(customUsage),
				gowdvo.getSystemIdentifier(),
				iRelationId,
				relationtype,
				direction,
				sUserName,
				uidNode,
				idRoot,
				label,
				gowdvo.getChangedAt(),
				description
		);
		return new GenericObjectTreeNode(params);
	}

	/**
	 * get the label representation of this node in the tree
	 * 
	 * @param gowdvo		{@link GenericObjectWithDependantsVO}
	 * @param uidNode		node id {@link EntityTreeViewVO}
	 */
	protected String getIdentifier(GenericObjectWithDependantsVO gowdvo, UID uidNode) {
		final Map<UID, Object> values = getFieldValues(gowdvo);
		StateVO state = StateCache.getInstance().getState(gowdvo.getStatus());
		return TreeNodeUtils.getIdentifier(
				uidNode,
				gowdvo.getModule(),
				values,
				treeProvider,
				localeDelegate,
				metaProvider,
				state,
				getDataLangFacade().getLanguageToUse()
		);
	}

	protected Map<UID, Object> getFieldValues(final GenericObjectWithDependantsVO gowdvo) {
		final Map<UID, Object> values = new HashMap<>();
		for (DynamicAttributeVO att : gowdvo.getAttributes()) {
			final UID attname = att.getAttributeUID();
			values.put(attname, att.getValue());
		}
		return values;
	}

	/**
	 * get the description of the representation of this node in the tree
	 * 
	 * @param gowdvo		{@link GenericObjectWithDependantsVO}
	 * @param uidNode		{@link EntityTreeViewVO} node id
	 */
	protected String getDescription(
			GenericObjectWithDependantsVO gowdvo,
			UID uidNode
	){
		final Map<UID, Object> values = getFieldValues(gowdvo);
		return TreeNodeUtils.getDescription(
				uidNode,
				gowdvo.getModule(),
				values,
				treeProvider,
				localeDelegate,
				metaProvider,
				getDataLangFacade().getLanguageToUse()
		);
	}

	private DataLanguageFacadeRemote getDataLangFacade() {
		return dataLangFacade;
	}

	private void setDataLangFacade(DataLanguageFacadeRemote dataLangFacade) {
		this.dataLangFacade = dataLangFacade;
	}
}
