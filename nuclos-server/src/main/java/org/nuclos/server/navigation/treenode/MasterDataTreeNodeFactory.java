//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.metadata.TreeMetaProvider;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Factory that creates <code>MasterDataTreeNode</code>s.
 * 
 * this factory was created to separate label and description from MasterDataTreeNode
 * (in analogy to GenericObjectTreeNodeFactory)
 * 
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 */
@RolesAllowed("Login")
public class MasterDataTreeNodeFactory {

	
	
	@Autowired
	private MetaProvider metaProvider;
	
	@Autowired
	private TreeMetaProvider treeProvider;
	
	@Autowired
	private SpringLocaleDelegate localeDelegate;
	
	@Autowired
	private DataLanguageCache dataLangCache;
	
	/**
	 * create new {@link DefaultMasterDataTreeNode}
	 * 
	 * @param mdVO			{@link MasterDataVO}
	 * @param uidNode 		node id
	 * @param idRoot		root node id
	 * 
	 * @return {@link DefaultMasterDataTreeNode}
	 */
	public DefaultMasterDataTreeNode newDefaultMasterDataTreeNode(final MasterDataVO mdVO, final UID uidNode, final Long idRoot) {
		final UID entity = mdVO.getEntityObject().getDalEntity();
		final Object id = mdVO.getPrimaryKey();
		final String label = getIdentifier(mdVO, uidNode);
		final String description = getDescription(mdVO, uidNode);
		final DefaultMasterDataTreeNodeParameters params = new DefaultMasterDataTreeNodeParameters(entity, id, uidNode, idRoot, label, description);
		return new DefaultMasterDataTreeNode(params);
	}
	
	/**
	 * Create new {@link SubFormEntryTreeNode}
	 * 
	 * @param mdVO			{@link MasterDataVO}
	 * @param uidNode 		node id
	 * @param idRoot		root node id
	 * 
	 * @return {@link SubFormEntryTreeNode}
	 */
	public SubFormEntryTreeNode newSubFormEntryTreeNode(final  MasterDataVO mdVO, final UID uidNode, final Long idRoot) {
		final String label = getIdentifier(mdVO,  uidNode);
		final String description = getDescription(mdVO, uidNode);
		return new SubFormEntryTreeNode(mdVO, uidNode, idRoot, label, description);
	}
	

	/**
	 * get the label representation of this node in the tree
	 * 
	 * @param mdVO			{@link MasterDataVO}
	 * @param uidNode		node id {@link EntityTreeViewVO}
	 * @return
	 */
	protected String getIdentifier(MasterDataVO mdVO, UID uidNode) {
		return TreeNodeUtils.getIdentifier(
				uidNode,
				mdVO.getEntityObject().getDalEntity(),
				mdVO.getFieldValues(),
				treeProvider,
				localeDelegate,
				metaProvider,
				null,
				dataLangCache.getLanguageToUse()
		);
	}

	/**
	 * get the description of the representation of this node in the tree
	 * 
	 * @param mdVO			{@link MasterDataVO}
	 * @param uidNode		{@link EntityTreeViewVO} node id
	 */
	protected String getDescription(MasterDataVO mdVO, UID uidNode){
		return TreeNodeUtils.getDescription(
				uidNode,
				mdVO.getEntityObject().getDalEntity(),
				mdVO.getFieldValues(),
				treeProvider,
				localeDelegate,
				metaProvider,
				dataLangCache.getLanguageToUse()
		);
	}

	public Collection<UID> getNeededFieldsForIdentifierAndDescription(UID uidNode, UID uidEntity) {
		EntityTreeViewVO node = null;
		if (null != uidNode) {
			try {
				// This is cached
				node = treeProvider.getNode(uidNode);
			} catch (CommonPermissionException cpe) {
				LoggerFactory.getLogger(MasterDataTreeNodeFactory.class).warn(cpe.getMessage(), cpe);
			}
		}
		return getNeededFieldsForIdentifierAndDescription(node, uidEntity);
	}

	public Collection<UID> getNeededFieldsForIdentifierAndDescription(EntityTreeViewVO node, UID uidEntity) {
		Collection<UID> fields = new HashSet<>();

		// 1. Identifier
		// read label from node configuration
		if (node != null && node.getNode() != null) {
			fields.addAll(SpringLocaleDelegate.getUsedFieldsInInputString(node.getNode()));
		} else {
			// No node configuration, take it from BO-Configuration
			String fieldConf = localeDelegate.getTreeViewFromMetaDataVO(metaProvider.getEntity(uidEntity));
			if (fieldConf != null) {
				fields.addAll(SpringLocaleDelegate.getUsedFieldsInInputString(fieldConf));
			}

		}

		// 2. Description
		// read identifier from node configuration
		if (node != null && node.getNodeTooltip() != null) {
			fields.addAll(SpringLocaleDelegate.getUsedFieldsInInputString(node.getNodeTooltip()));
		} else {
			// No node configuration, take it from BO-Configuration
			String fieldConf = localeDelegate.getTreeViewDescriptionFromMetaDataVO(metaProvider.getEntity(uidEntity));
			if (fieldConf != null) {
				fields.addAll(SpringLocaleDelegate.getUsedFieldsInInputString(fieldConf));
			}

		}

		return fields;
	}

}

