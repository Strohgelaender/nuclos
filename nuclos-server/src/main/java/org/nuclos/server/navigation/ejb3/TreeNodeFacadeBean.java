//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.ejb3;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.metadata.TreeMetaProvider;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.EntityObjectFacadeBean;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dbtransfer.TransferUtils;
import org.nuclos.server.dbtransfer.content.PreferenceNucletContent;
import org.nuclos.server.dbtransfer.content.WorkspaceNucletContent;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.ejb3.EntityFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.AbstractTreeNode;
import org.nuclos.server.navigation.treenode.AbstractTreeNodeConfigured;
import org.nuclos.server.navigation.treenode.DefaultMasterDataTreeNode;
import org.nuclos.server.navigation.treenode.DynamicTreeNode;
import org.nuclos.server.navigation.treenode.EntitySearchResultTreeNode;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.RelationDirection;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.SystemRelationType;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNodeFactory;
import org.nuclos.server.navigation.treenode.IGroupableNode;
import org.nuclos.server.navigation.treenode.LabelTreeNode;
import org.nuclos.server.navigation.treenode.MasterDataSearchResultTreeNode;
import org.nuclos.server.navigation.treenode.MasterDataTreeNode;
import org.nuclos.server.navigation.treenode.MasterDataTreeNodeFactory;
import org.nuclos.server.navigation.treenode.RelationTreeNode;
import org.nuclos.server.navigation.treenode.SimpleTreeNode;
import org.nuclos.server.navigation.treenode.SubFormEntryTreeNode;
import org.nuclos.server.navigation.treenode.SubFormTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.navigation.treenode.nuclet.NucletTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.NucletTreeNodeParameters;
import org.nuclos.server.navigation.treenode.nuclet.NuclosInstanceTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.AbstractNucletContentEntryTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.DefaultNucletContentEntryTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentCustomComponentTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentEntityTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentPreferenceTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentProcessTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentSearchfilterNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.ReportNucletContentTreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Supplier;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;


/**
 * Facade bean for managing explorer tree structures and contents.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * �todo restrict
 */
@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("Login")
public class TreeNodeFacadeBean extends NuclosFacadeBean implements TreeNodeFacadeLocal, TreeNodeFacadeRemote {

	private static final int DEFAULT_ROWCOUNT_FOR_SEARCHRESULT = 500;

	//
	private MasterDataTreeNodeFactory mdTreeNodeFactory;
	
	private MetaProvider metaProvider;

	private EntityFacadeBean entityFacade;

	private ServerParameterProvider serverParameterProvider;

	private GenericObjectFacadeLocal genericObjectFacade;

	private MasterDataFacadeLocal masterDataFacade;
	
	private EntityObjectFacadeBean entityObjectFacade;

	private LocaleFacadeLocal localeFacade;

	private TreeMetaProvider treeProvider;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private MandatorUtils mandatorUtils;
	
	@Autowired
	private GenericObjectFacadeRemote genericObjectFacadeRemote;

	public TreeNodeFacadeBean() {
	}


	public MetaProvider getMetaProvider() {
		return metaProvider;
	}

	
	@Autowired
	public void setMasterDataTreeNodeFactory(MasterDataTreeNodeFactory mdTreeNodeFactory) {
		this.mdTreeNodeFactory = mdTreeNodeFactory;
	}

	@Autowired
	public void setMetaProvider(MetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}


	public EntityFacadeBean getEntityFacade() {
		return entityFacade;
	}

	@Autowired
	public void setEntityFacade(EntityFacadeBean entityFacade) {
		this.entityFacade = entityFacade;
	}


	@Autowired
	public void setTreeProvider(TreeMetaProvider treeProvider) {
		this.treeProvider = treeProvider;
	}


	public TreeMetaProvider getTreeProvider() {
		return treeProvider;
	}

	@Autowired
	void setServerParameterProvider(ServerParameterProvider serverParameterProvider) {
		this.serverParameterProvider = serverParameterProvider;
	}

	@Autowired
	final void setGenericObjectFacade(GenericObjectFacadeLocal genericObjectFacade) {
		this.genericObjectFacade = genericObjectFacade;
	}

	private final GenericObjectFacadeLocal getGenericObjectFacade() {
		return genericObjectFacade;
	}

	@Autowired
	final void setMasterDataFacade(MasterDataFacadeLocal masterDataFacade) {
		this.masterDataFacade = masterDataFacade;
	}
	
	@Autowired
    final void setEntityObjectFacade(EntityObjectFacadeBean entityObjectFacade) {
        this.entityObjectFacade = entityObjectFacade;
    }

	@Autowired
	final void setLocaleFacade(LocaleFacadeLocal localeFacade) {
		this.localeFacade = localeFacade;
	}

	/**
	 * gets a generic object tree node for a specific generic object
	 * 
	 * �postcondition result != null
	 * 
	 * @param genericObjectId id of generic object to get tree node for
	 * @return generic object tree node for given id, if existing and allowed. null otherwise.
	 * @throws CommonFinderException if the object doesn't exist (anymore).
	 */
	@Override
	public GenericObjectTreeNode getGenericObjectTreeNode(Long genericObjectId, UID module) throws CommonFinderException, CommonPermissionException {
		checkReadAllowed(module);
		Long idParent = null; 
		UID uidNode = null;
		Long idRoot = null;
		final GenericObjectTreeNode result = newGenericObjectTreeNode(genericObjectId, module, null, null, null, null, idParent, uidNode, idRoot);
		assert result != null;
		return result;
	}

	/**
	 *
	 * �postcondition result != null
	 * 
	 * @param iGenericObjectId
	 * @param moduleId the module id
	 * @param iRelationId
	 * @param relationtype
	 * @param direction
	 * @param uidNode id for {@link EntityTreeViewVO}
	 * @return a new tree node for the generic object with the given id.
	 * @throws CommonFinderException if the object doesn't exist (anymore).
	 */
	@Override
	public GenericObjectTreeNode newGenericObjectTreeNode(Long iGenericObjectId,
			UID moduleId, Long iRelationId, SystemRelationType relationtype, RelationDirection direction,
			String customUsage, Long parentId, UID uidNode, Long idRoot) throws CommonFinderException, CommonPermissionException {
		checkReadAllowed(moduleId);
		List<AbstractTreeNodeConfigured<Long>> lstSingle =
			newGenericObjectTreeNodesImpl(Collections.singleton(iGenericObjectId), moduleId, iRelationId, relationtype, direction,
				customUsage, parentId, uidNode, idRoot, null);
		if (lstSingle.isEmpty()) {
			throw new CommonFinderException("Could not find GO for ID=" + iGenericObjectId + " Entity=" + moduleId);
		}
		return (GenericObjectTreeNode)lstSingle.get(0);
	}
	
	private List<AbstractTreeNodeConfigured<Long>> newGenericObjectTreeNodesImpl(Collection<Long> iGenericObjectIds,
																				 UID moduleId, Long iRelationId, SystemRelationType relationtype, RelationDirection direction,
																				 String customUsage, Long parentId, UID uidNode, Long idRoot, List<UID> lstGroupingFields) throws CommonFinderException {

		// �todo 1. write/use LOFB method that doesn't require the module id
		// �todo 2. Fix BUG: getWithDependants() throws a CommonPermissionException, even if bIgnoreUser == true

		final Collection<GenericObjectWithDependantsVO> gowdvos = getWithDependants(genericObjectFacade, moduleId, iGenericObjectIds);

		List<AbstractTreeNodeConfigured<Long>> result = new ArrayList<>();

		for (GenericObjectWithDependantsVO gowdvo : gowdvos) {
			// FIXME parentId is not used anymore?
			final GenericObjectTreeNode gotn = GenericObjectTreeNodeFactory.getInstance().newTreeNode(gowdvo,
					iRelationId, relationtype, direction, getCurrentUserName(), customUsage, parentId, uidNode, idRoot, getCurrentMandatorUID());

			if (lstGroupingFields != null) {
				for (UID gf : lstGroupingFields) {
					DynamicAttributeVO a = gowdvo.getAttribute(gf);
					if (a != null) {
						gotn.setGroupingValue(gf, a.getValue());
					}
				}
			}
			result.add(gotn);
		}

		return result;
	}
	
	

	/**
	 * @param lofacade
	 * @param moduleUid
	 * @param iGenericObjectIds
	 * @return the generic object with the given id, along with necessary attributes and dependants.
	 */
	private static Collection<GenericObjectWithDependantsVO> getWithDependants(
			GenericObjectFacadeLocal lofacade, UID moduleUid, Collection<Long> iGenericObjectIds)
			throws CommonFinderException {
		// WORKAROUND: Load only necessary attributes (and to avoid slow calculated attributes):
		// @todo move this workaround to GenericObjectFacadeBean

		final CollectableSearchExpression clctexpr = new CollectableSearchExpression(
				SearchConditionUtils.getCollectableSearchConditionForIds(iGenericObjectIds));

		return lofacade.getGenericObjects(moduleUid, clctexpr, Collections.emptySet());

	}

	/**
	 * gets the list of sub nodes for a specific generic object tree node.
	 * Note that there is a specific method right on this method.
	 * 
	 * �postcondition result != null
	 * 
	 * @param node tree node of type generic object tree node
	 * @return list of sub nodes for given tree node
	 */
	public List<TreeNode> getSubNodesForGenericObjectTreeNode(GenericObjectTreeNode node) throws CommonPermissionException {
		checkReadAllowed(node.getEntityUID());
		return getSubNodesIgnoreUser(node, new HashSet<>());
	}
	
	private List<TreeNode> getSubNodesIgnoreUser(GenericObjectTreeNode node, Set<Pair<UID, Object>> alreadyLoaded) {
		final List<TreeNode> result = new ArrayList<>();
		final EntityMeta<?> mdcvoModule = metaProvider.getEntity(node.getEntityUID());
		//add relations
		if(mdcvoModule.isTreeRelation()) {
			result.addAll(getRelatedSubNodes(node, RelationDirection.FORWARD));
			result.addAll(getRelatedSubNodes(node, RelationDirection.REVERSE));
			Collections.sort(result, new GenericObjectTreeNodeChildrenComparator());
		}

		final UID base = mdcvoModule.getUID();

		try {
			final List<EntityTreeViewVO> lstEtvVO = new ArrayList<>();
			if (null != node.getNodeId()) {
				if (null == node.getNodeId()) {
					throw new IllegalArgumentException("nodeId for " + node + " must not be null");
				}
				final EntityTreeViewVO etvVO = getTreeProvider().getNode(node.getNodeId());

				if (etvVO.getIsInheritNodes()) {
					// inherited nodes
					final EntityMeta metaEntity = metaProvider.getEntity(etvVO.getEntity());
					lstEtvVO.addAll(getTreeProvider().getTopLevelNodes(metaEntity.getUID()));

				} else {
					// subnodes
					lstEtvVO.addAll(getTreeProvider().getSubNodes(node.getNodeId()));
				}
			} else {
				// toplevel
				lstEtvVO.addAll(getTreeProvider().getTopLevelNodes(base));
			}
			subformSubnodes(result, node, lstEtvVO, alreadyLoaded, null);
			return result;
		} catch (CommonPermissionException e) {
			// FIXME
			LOG.warn(e.getMessage(), e);
			return Collections.emptyList();
		}
	}

	private void subformSubnodes(
			final List<TreeNode> result,
			TreeNode node,
			Collection<EntityTreeViewVO> lstEtvVO,
			Set<Pair<UID, Object>> alreadyLoaded,
			CollectableSearchCondition additionalCondition
	) {

		// If the sortOrder key is not unique, we have lost entry before changeing this to SetMultimap (tp)
		final SetMultimap<Integer,TreeNode> subforms = Multimaps.newSetMultimap(
				new TreeMap<Integer,Collection<TreeNode>>(), new SetFactory<TreeNode>());
		// add subnodes defined in the module meta data
		for (final EntityTreeViewVO etvVO : lstEtvVO) {
			// This seems to be all right - except for bmw-fdm. (tp)
			/*
			assert IdUtils.equals(etv.getOriginentityid(), mdvoSub.getId()) 
				: "org: " + etv.getOriginentityid() + " sub: " + mdvoSub.getIntId();
			 */

			final UID uidEntity = etvVO.getEntity();
			
			// support old foldername
			final boolean isShowEntityFolder= (etvVO.getIsShowEntityFolder() || !StringUtils.looksEmpty(etvVO.getFoldername()));
			final boolean active = etvVO.isActive();
			final Integer sortOrder = etvVO.getSortOrder() == null ? Integer.valueOf(0) : etvVO.getSortOrder();

			if (active) {
				List<TreeNode> lstSubNodes = getEntityObjectSubNodes(node, etvVO.getPrimaryKey(), additionalCondition);
				if (isShowEntityFolder && !lstSubNodes.isEmpty()) {
					if (node instanceof GenericObjectTreeNode 
							|| node instanceof MasterDataTreeNode) { // only allow Treenodes of those types.
						/*
						SubFormTreeNode treenode = new SubFormTreeNode<Integer>(null, node);
						treenode.getSubNodes();
						subforms.put(sortOrder, Collections.singletonList(treenode));
						 */
						// FIXME localize
						EntityMeta<Object> metaEntity = getMetaProvider().getEntity(etvVO.getEntity());
						String label = "";
						if (metaEntity.isDynamic()) {
							 label = metaProvider.getEntity(getEntityFacade().getBaseEntity(metaEntity.getDataSource())).getEntityName();
						} else {
							// localized label for (system)entities
							label = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(metaEntity);
						}
						final String description = label;
						final LabelTreeNode<Long> lblTreeNode = new LabelTreeNode<>(IdUtils.toLongId(node.getId()), label, description, lstSubNodes);
						lblTreeNode.getSubNodes();
						subforms.put(sortOrder, lblTreeNode);
					}
				} else {
					subforms.putAll(sortOrder, lstSubNodes);
				}

				if (etvVO.getIsInheritNodes() && lstSubNodes!=null && lstSubNodes.size()==0) {
					// inherited subnode configuration and loading max deep 1
					handleInheritedSubNodes(lstSubNodes, alreadyLoaded);
				}
			}
		}
		for (Integer i: subforms.keySet()) {
			result.addAll(subforms.get(i));
		}
	}
	
	private void handleInheritedSubNodes(final List<? extends TreeNode>  lstSubNodes, Set<Pair<UID, Object>> alreadyLoaded) {
		for (final TreeNode node : lstSubNodes) {
			if (alreadyLoaded.contains(new Pair<>(node.getEntityUID(), node.getId()))) {
				// Sonst Endlosschleife...
				continue;
			} else {
				alreadyLoaded.add(new Pair<>(node.getEntityUID(), node.getId()));
			}
			// inherited nodes
			if (node instanceof DefaultMasterDataTreeNode) {
				// masterdata
				getSubnodesImpl((DefaultMasterDataTreeNode)node, alreadyLoaded, null);
			} else if (node instanceof GenericObjectTreeNode) {
				// genericobject
				getSubNodesIgnoreUser((GenericObjectTreeNode)node, alreadyLoaded);
			} else {
				throw new NotImplementedException("node " + node + " does not support inherited nodes");
			}
		}
	}
	
	private void unflattenGrouping(final List<TreeNode> lstResult, final List<UID> lstGroupingFields, int index) {
		if (lstGroupingFields.size() <= index || lstResult == null || lstResult.isEmpty()) {
			return;
		}
		
		UID gf = lstGroupingFields.get(index);
		Map<String, AbstractTreeNode> mpGroups = new TreeMap<>();
		
		for (TreeNode subNode : lstResult) {
			if (subNode instanceof IGroupableNode) {
				IGroupableNode gSubNode = (IGroupableNode) subNode;
				
				String s = gSubNode.getGroupingValue(gf) != null ? gSubNode.getGroupingValue(gf).toString() : "...";
				if (s.isEmpty()) {
					s = "...";
				}
				
				if (!mpGroups.containsKey(s)) {
					mpGroups.put(s, new LabelTreeNode<>(s, s, s, new ArrayList<>()));
				}
				
				mpGroups.get(s).getSubNodes().add(subNode);
			}
		}
		lstResult.clear();
		for (String s : mpGroups.keySet()) {
			AbstractTreeNode subNode = mpGroups.get(s);
			unflattenGrouping(subNode.getSubNodes(), lstGroupingFields, index + 1);
			lstResult.add(subNode);
		}
	}
	
	/**

	 * @param node
	 * @param uidNode
	 * @return
	 */
	private List<TreeNode> getEntityObjectSubNodes(final TreeNode node, final UID uidNode, CollectableSearchCondition additionalCondition) {
		final List<TreeNode> lstResult;
		try {
			final EntityTreeViewVO etvVO = getTreeProvider().getNode(uidNode);
			if (etvVO == null) {
				return Collections.emptyList();
			}
			
			final List<UID> lstGroupingFields = getTreeProvider().getFieldsFromStringifiedConfiguration(etvVO.getGroupBy(), etvVO.getEntity());

			final CollectableSearchExpression exp = createSubNodeExpression(node, etvVO, additionalCondition);
			lstResult = getEOSubnodesWithSearchExpression(node, etvVO, exp, lstGroupingFields);
			
			if (!lstGroupingFields.isEmpty() && lstResult != null && !lstResult.isEmpty() && lstResult.get(0) instanceof IGroupableNode) {
				unflattenGrouping(lstResult, lstGroupingFields, 0);
			} 
			
			//BMWFDM-717 21) concerning nuclos core, too: No children allowed for sure leaf nodes -> No plus sign
			final boolean forceLeaf = (etvVO.getChildNodes() == null || etvVO.getChildNodes().isEmpty()) && !etvVO.getIsInheritNodes();
			if (forceLeaf) {
				for (TreeNode tn : lstResult) {
					if (tn instanceof AbstractTreeNodeConfigured) {
						((AbstractTreeNodeConfigured<?>)tn).setForceLeaf(true);
					}
				}
			}

		} catch (final CommonPermissionException ex) {
			LOG.warn(ex.getMessage(), ex);
			return Collections.emptyList();
		}
		return lstResult;
	}
	
	private List<TreeNode> getEOSubnodesWithSearchExpression(final TreeNode node, final EntityTreeViewVO etvVO, 
			final CollectableSearchExpression exp, final List<UID> lstGroupingFields) throws CommonPermissionException {
		final Long idNode = IdUtils.toLongId(node.getId());
		final Long idRoot = (node.getRootId() != null) ? IdUtils.toLongId(node.getRootId()) : idNode;
		
		final List<TreeNode> lstResult = new ArrayList<>();
		if(Modules.getInstance().isModule(etvVO.getEntity())) {
			if(idNode == null) {
				return Collections.emptyList();
			}
			// TODO NUCLOS-6891 b) It's possible to use getMasterDataSubNodes here, too but this would return another type of
			// TODO TreeNode with the same function (GenericObjectTreeNode), which could leader fail a cast.
			lstResult.addAll(getModuleSubNodes(etvVO, exp, idRoot, idNode, lstGroupingFields));
		}
		else {
			lstResult.addAll(getMasterDataSubNodes(etvVO, exp, idRoot, lstGroupingFields));
		}
		return lstResult;
	}

	/**
	 * get the masterdata subnodes for the given node
	 * @param node
	 * @param sEntity
	 * @param sField
	 * @return
	 */
	private <PK> List<AbstractTreeNodeConfigured<PK>> getMasterDataSubNodes(final EntityTreeViewVO etvVO, final CollectableSearchExpression exp, 
			final Long idRoot, final List<UID> lstGroupingFields) {
		final UID uidEntity = etvVO.getEntity();
		Collection<UID> fields = mdTreeNodeFactory.getNeededFieldsForIdentifierAndDescription(etvVO, uidEntity);
		fields.addAll(lstGroupingFields);

		IEntityObjectProcessor<PK> proc = NucletDalProvider.getInstance().getEntityObjectProcessor(uidEntity);
		ResultParams resultParams = new ResultParams(fields);
		List<EntityObjectVO<PK>> lstEO = proc.getBySearchExprResultParams(exp, resultParams);
		final Collection<MasterDataVO<PK>> collMdVO = CollectionUtils.transform(lstEO, (EntityObjectVO<PK> eo) -> new MasterDataVO<>(eo));

		//final Collection<MasterDataVO<PK>> collMdVO = masterDataFacade.getMasterData(metaProvider.getEntity(uidEntity), exp.getSearchCondition());
		final Collection<AbstractTreeNodeConfigured<PK>> colResult = CollectionUtils.transform(collMdVO, (MasterDataVO<PK> mdvo) -> {
			DefaultMasterDataTreeNode<PK> dmdrn = mdTreeNodeFactory.newDefaultMasterDataTreeNode(mdvo, etvVO.getPrimaryKey(), idRoot);
			if (lstGroupingFields != null) {
				for (UID gf : lstGroupingFields) {
					dmdrn.setGroupingValue(gf, mdvo.getFieldValue(gf));
				}
			}
			return dmdrn;
		});

		final List<AbstractTreeNodeConfigured<PK>> result = new ArrayList<>(colResult);
		Collections.sort(result);
		return result;

	}

	/**
	 * get subnodes of type sEntity
	 * 
	 * @param etvVO	{@link EntityTreeViewVO}
	 * @return
	 * @throws NoSuchElementException
	 */
	private List<AbstractTreeNodeConfigured<Long>> getModuleSubNodes(final EntityTreeViewVO etvVO, final CollectableSearchExpression exp, 
			final Long idRoot, final Long idNode, final List<UID> lstGroupingFields) throws NoSuchElementException, CommonPermissionException {
		final UID uidEntity = etvVO.getEntity();

		final List<Long> lstIds = new ArrayList<>();

		// use remote interface to apply recordgrant (Datensatzfreigabe) 
		lstIds.addAll(genericObjectFacadeRemote.getGenericObjectIds(uidEntity, exp));

		// NUCLOS-6891 c) Load all nodes together and not each one, as it was before.
		try {
			return newGenericObjectTreeNodesImpl(lstIds, uidEntity, null, null, null,
					null, idNode, etvVO.getPrimaryKey(), idRoot, lstGroupingFields);
		}
		catch(CommonFinderException e) {
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * create {@link CollectableSearchExpression} for node meta data {@link EntityTreeViewVO}
	 * 
	 * @param node	node {@link TreeNode}
	 * @param etvVO	node meta data {@link EntityTreeViewVO}
	 * @return {@link CollectableSearchExpression}
	 */
	private final CollectableSearchExpression createSubNodeExpression(
			final TreeNode node, final EntityTreeViewVO etvVO, CollectableSearchCondition additionalCondition) {
		final UID uidField = etvVO.getField();
		final UID uidFieldToRoot = etvVO.getFieldRoot();
		final Long nodeId = IdUtils.toLongId(node.getId());

		final CompositeCollectableSearchCondition cond = SearchConditionUtils.and();
		if (null != uidField) {
			FieldMeta<?> metaField = getMetaProvider().getEntityField(uidField);
			/*
			 * FIXME doesn't work for dynamic entity references (no meta information for id fields available)
			 */
			if (null != metaField.getForeignEntity()) {
				// reference to parent
				cond.addOperand(SearchConditionUtils.newIdComparison(uidField, ComparisonOperator.EQUAL, 
						IdUtils.toLongId(nodeId)));
			} else {
				cond.addOperand(SearchConditionUtils.newComparison(uidField, ComparisonOperator.EQUAL, 
						IdUtils.toLongId(nodeId)));
			}

		} else {
			// redundant for understanding
			// show all, leave conditions empty
		}
		if (null != uidFieldToRoot) {
			final Long idRoot = (node.getRootId() != null) ? IdUtils.toLongId(node.getRootId()) : IdUtils.toLongId(node.getId());
			// reference to top level tree node
			// assert node.getRootId() != null;
			final FieldMeta<?> metaFieldToRoot = getMetaProvider().getEntityField(uidFieldToRoot);
			if (null != metaFieldToRoot.getForeignEntity()) {
				/*
				 * FIXME doesn't work for dynamic entity references (no meta information for id fields available)
				 */	
				cond.addOperand(SearchConditionUtils.newComparison(uidFieldToRoot, ComparisonOperator.EQUAL, idRoot));
			} else {
				cond.addOperand(SearchConditionUtils.newComparison(uidFieldToRoot, ComparisonOperator.EQUAL, idRoot));
			}

		}

		if (additionalCondition != null) {
			cond.addOperand(additionalCondition);
		}
		final CollectableSearchExpression exp = (cond.getOperandCount() == 0) ? new CollectableSearchExpression() : new CollectableSearchExpression(cond);
		return exp;
	}

	/**
	 * Note that user rights are always ignored here.
	 * @param node
	 * @param direction
	 * @return the given node's subnodes related in the given direction (excluding GenericObjectTreeNode.RELATIONTYPE_INVOICE_OF).
	 * @throws CommonPermissionException
	 */
	private List<TreeNode> getRelatedSubNodes(final GenericObjectTreeNode node, final RelationDirection direction) {
		final List<TreeNode> result = new ArrayList<>();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		// DbFrom r = query.from("T_UD_GO_RELATION").alias("r");
		DbFrom<Long> r = query.from(E.GENERICOBJECTRELATION, "r");
		// DbFrom l = query.from("T_UD_GENERICOBJECT").alias("l");
		DbFrom<Long> l = query.from(E.GENERICOBJECT, "l");
		// DbColumnExpression<Integer> genericObject1 = r.baseColumn(direction.isForward() ? "INTID_T_UD_GO_1" : "INTID_T_UD_GO_2", Integer.class);
		DbColumnExpression<Long> genericObject1 = r.baseColumn(direction.isForward() ? 
				E.GENERICOBJECTRELATION.source : E.GENERICOBJECTRELATION.destination);
		// DbColumnExpression<Integer> genericObject2 = r.baseColumn(direction.isForward() ? "INTID_T_UD_GO_2" : "INTID_T_UD_GO_1", Integer.class);
		DbColumnExpression<Long> genericObject2 = r.baseColumn(direction.isForward() ? 
				E.GENERICOBJECTRELATION.destination : E.GENERICOBJECTRELATION.source);
		query.multiselect(
			r.baseColumn(E.GENERICOBJECTRELATION),
			l.baseColumn(E.GENERICOBJECT),
			// r.baseColumn("STRRELATIONTYPE", String.class),
			r.baseColumn(E.GENERICOBJECTRELATION.relationType),
			// l.baseColumn("INTID_T_MD_MODULE", Integer.class)
			l.baseColumn(E.GENERICOBJECT.module)
			);
		query.where(builder.and(
			builder.equal(genericObject2, l.baseColumn(E.GENERICOBJECT)),
			// node.getParentId() != null ? builder.equal(genericObject2, node.getParentId()).not() : builder.alwaysTrue(),
			builder.alwaysTrue(),
			builder.equalValue(genericObject1, node.getId())//,
			));
		// order by type ???

		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
			final Long iRelationId = tuple.get(0, Long.class);
			final Long iGenericObjectId = tuple.get(1, Long.class);

			final String relationType = tuple.get(2, String.class);
			final UID moduleId = tuple.get(3, UID.class);

			SystemRelationType systemRelationType = SystemRelationType.findSystemRelationType(relationType);

			// root nodes not supported for related sub nodes
			final Long idRoot = null;

			try {
				if (systemRelationType != null) {
				// predecessor or part-of relation:
					Long idParent = node.getId();
					UID uidNode = node.getNodeId();
					result.add(newGenericObjectTreeNode(iGenericObjectId, moduleId, iRelationId, systemRelationType, direction, null, idParent, uidNode, idRoot));
				} else {
					Long idParent = null;
					UID uidNode = node.getNodeId();
					
					final GenericObjectTreeNode nodeRelatedObject = newGenericObjectTreeNode(iGenericObjectId, moduleId, null, null, null, null, idParent, uidNode, idRoot);
					String label = getRelationTypeLabel(relationType);
					result.add(new RelationTreeNode(iRelationId, label, relationType, direction, nodeRelatedObject));
				}
			} catch (CommonFinderException | CommonPermissionException ex) {
				// the object doesn't exist anymore or permissions have changed - warn.
				LOG.warn(ex.getMessage());
			}
		}

		return result;
	}

	private String getRelationTypeLabel(String relationType) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<String> query = builder.createQuery(String.class);
		DbFrom<UID> t = query.from(E.RELATIONTYPE);
		query.select(t.baseColumn(E.RELATIONTYPE.labelres));
		query.where(builder.equalValue(t.baseColumn(E.RELATIONTYPE.name), relationType));
		String resourceId = CollectionUtils.getFirst(dataBaseHelper.getDbAccess().executeQuery(query));
		if (resourceId != null) {
			return SpringLocaleDelegate.getInstance().getMessage(resourceId, relationType);
		}
		return null;
	}

	public NucletTreeNode getNucletTreeNode(UID iId) throws CommonFinderException {
		try {
			EntityObjectVO<UID> eovo = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLET).getByPrimaryKey(iId);
			return new NucletTreeNode(new NucletTreeNodeParameters(eovo, false));
		} catch (Exception ex) {
			throw new CommonFinderException();
		}
	}

	/**
	 * method to get a masterdata tree node for a specific masterdata record
	 * 
	 * �postcondition result != null
	 * 
	 * @param iId id of masterdata record to get tree node for
	 * @param uidNode id of {@link EntityTreeViewVO}
	 * @return masterdata tree node for given id
	 * @throws CommonPermissionException
	 */
	@Override
	public <PK> MasterDataTreeNode<PK> getMasterDataTreeNode(final PK iId, final UID entityUid, UID uidNode, boolean bLoadSubNodes) throws CommonFinderException, CommonPermissionException {
		final MasterDataVO<PK> mdvo = masterDataFacade.get(entityUid, iId);
		final MasterDataTreeNode<PK> result = mdTreeNodeFactory.newDefaultMasterDataTreeNode(mdvo, uidNode, null);

		if (bLoadSubNodes) {
			result.getSubNodes();
		}
		assert result != null;
		return result;
	}

	public <PK> SubFormEntryTreeNode<PK> getSubFormEntryTreeNode(PK iId, UID entityUid, final UID uidNode, final Long idRoot, boolean bLoadSubNodes) throws CommonFinderException, CommonPermissionException {
		final MasterDataVO<PK> mdvo = masterDataFacade.get(entityUid, iId);
		final SubFormEntryTreeNode<PK> result = mdTreeNodeFactory.newSubFormEntryTreeNode(mdvo, uidNode, idRoot);

		if (bLoadSubNodes) {
			result.getSubNodes();
		}
		assert result != null;
		return result;
	}

	public List<TreeNode> getSubNodes(NucletTreeNode node) {
		List<TreeNode> result = new ArrayList<>();
		if (node.isShowDependencies()) {

			CollectableSearchCondition cond = SearchConditionUtils.newUidComparison(
				// E.NUCLETDEPENDENCE, 
				E.NUCLETDEPENDENCE.nuclet,
				ComparisonOperator.EQUAL,
				(UID) node.getId());

			List<NucletTreeNode> nucletNodes = new ArrayList<NucletTreeNode>();
			for (EntityObjectVO<UID> eoDependence : NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLETDEPENDENCE).getBySearchExpression(
					new CollectableSearchExpression(cond))) {
				EntityObjectVO<UID> eoNuclet = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLET).getByPrimaryKey(
						eoDependence.getFieldUid(E.NUCLETDEPENDENCE.nucletDependence));
				nucletNodes.add(new NucletTreeNode(new NucletTreeNodeParameters(eoNuclet, node.isShowDependencies())));
			}
			result.addAll(CollectionUtils.sorted(nucletNodes, (NucletTreeNode o1, NucletTreeNode o2) ->
					LangUtils.compare(o1.getLabel(), o2.getLabel())));

		} else {
			result.addAll(getNucletContentTypes(node.getId()));
		}
		return result;
	}

	private List<NucletContentTreeNode> getNucletContentTypes(UID nucletId) {
		List<NucletContentTreeNode> result = new ArrayList<>();
		for (EntityMeta<UID> ne : AbstractNucletContentEntryTreeNode.getNucletContentEntities()) {
			/*if (E.ENTITY.equals(ne)) {
				// NUCLOS-6296
				continue;
			}*/
			if (E.REPORT.equals(ne)) {
				result.add(new ReportNucletContentTreeNode(nucletId));
			}
			else {
				result.add(new NucletContentTreeNode(nucletId, ne));	
			}
		}
		return result;
	}

	/**
	 * �postcondition result != null
	 * 
	 * @param node
	 * @return the subnodes for the given node.
	 */
	@Override
	public List<TreeNode> getSubnodes(DefaultMasterDataTreeNode<?> node, CollectableSearchCondition additionalCondition)
			throws CommonPermissionException {
		checkReadAllowed(node.getEntityUID());
		return getSubnodesImpl(node, new HashSet<>(), additionalCondition);
	}
	
	private List<TreeNode> getSubnodesImpl(DefaultMasterDataTreeNode<?> node, Set<Pair<UID, Object>> alreadyLoaded,
			CollectableSearchCondition additionalCondition) {
		final List<TreeNode> result = new ArrayList<>();
		try {
			final List<EntityTreeViewVO> lstEtvVO = new ArrayList<>();
			if (null != node.getNodeId()) {

				EntityTreeViewVO etvVO = getTreeProvider().getNode(node.getNodeId());
				if (etvVO.getIsInheritNodes()) {
					// inherited nodes
					final EntityMeta metaEntity = getMetaProvider().getEntity(etvVO.getEntity());
					lstEtvVO.addAll(getTreeProvider().getTopLevelNodes(metaEntity.getUID()));
				} else {
					// subnodes
					lstEtvVO.addAll(getTreeProvider().getSubNodes(node.getNodeId()));
				}
			} else {
				// toplevel
				lstEtvVO.addAll(getTreeProvider().getTopLevelNodes(node.getEntityUID()));
			}
			subformSubnodes(result, node, lstEtvVO, alreadyLoaded, additionalCondition);
			return result;
		} catch (CommonPermissionException e) {
			LOG.info("No permission.", e);
			return Collections.emptyList();
		}
	}

	/**
	 * �postcondition result != null
	 * 
	 * @param node
	 * @return the subnodes for the given node.
	 */
	public List<AbstractNucletContentEntryTreeNode> getSubNodes(NucletContentTreeNode node) {
		final List<AbstractNucletContentEntryTreeNode> result = new ArrayList<>();

		final EntityMeta<?> eMeta = metaProvider.getEntity(node.getEntityUID());
		final FieldMeta<?> efMetaNuclet = TransferUtils.getRefToNuclet(eMeta);

		CollectableSearchCondition cond = SearchConditionUtils.newUidComparison(
			// eMeta.getEntity(),
			efMetaNuclet,
			ComparisonOperator.EQUAL,
			node.getNuclet());

		for (EntityObjectVO<?> eo : nucletDalProvider.getEntityObjectProcessor(eMeta).getBySearchExpression(
				new CollectableSearchExpression(cond))) {
			result.add(getNucletContentEntryNode(eo));
		}

		return sortAbstractNucletContentEntryTreeNodes(result);
	}

	public List<AbstractNucletContentEntryTreeNode> getNucletContent(NucletTreeNode node) {
		final List<AbstractNucletContentEntryTreeNode> result = new ArrayList<>();

		for (NucletContentTreeNode contentTypeNode : getNucletContentTypes(node.getId())) {
			result.addAll(getSubNodes(contentTypeNode));
		}
		return result;
	}

	public List<NucletTreeNode> getSubNodes(NuclosInstanceTreeNode node) {
		final Collection<NucletTreeNode> result = new ArrayList<>();
		FieldMeta<UID> efMetaDependence = E.NUCLETDEPENDENCE.nucletDependence;

		for (EntityObjectVO<UID> eoNuclet : nucletDalProvider.getEntityObjectProcessor(E.NUCLET).getAll()) {

			CollectableSearchCondition cond = org.nuclos.common.SearchConditionUtils.newKeyComparison(
				efMetaDependence,
				ComparisonOperator.EQUAL,
				eoNuclet.getPrimaryKey());

			if (nucletDalProvider.getEntityObjectProcessor(E.NUCLETDEPENDENCE)
				.count(new CollectableSearchExpression(cond)) == 0) {
				// is root Nuclet
				result.add(new NucletTreeNode(new NucletTreeNodeParameters(eoNuclet, true)));
			}
		}

		return CollectionUtils.sorted(result, (NucletTreeNode o1, NucletTreeNode o2) ->
					LangUtils.compare(o1.getLabel(), o2.getLabel()));
	}

	public List<AbstractNucletContentEntryTreeNode> getAvailableNucletContents() {
		final List<AbstractNucletContentEntryTreeNode> result = new ArrayList<>();
		for (EntityMeta<UID> ne : AbstractNucletContentEntryTreeNode.getNucletContentEntities()) {
			FieldMeta<?> efMetaNuclet = TransferUtils.getRefToNuclet(ne);

			CollectableSearchCondition cond = SearchConditionUtils.newIsNullCondition(efMetaNuclet);
			
			if (E.WORKSPACE.equals(ne)) {
				cond = SearchConditionUtils.and(cond, WorkspaceNucletContent.cachingConditionStatic());
			} else if (E.PREFERENCE.equals(ne)) {
				cond = SearchConditionUtils.and(cond, PreferenceNucletContent.cachingConditionStatic());
			}

			List<AbstractNucletContentEntryTreeNode> nodes = new ArrayList<>();
			for (EntityObjectVO<?> eo : nucletDalProvider.getEntityObjectProcessor(ne).getBySearchExpression(
					new CollectableSearchExpression(cond))) {
				nodes.add(getNucletContentEntryNode(eo));
			}
			result.addAll(sortAbstractNucletContentEntryTreeNodes(nodes));
		}
		return result;
	}

	private List<AbstractNucletContentEntryTreeNode> sortAbstractNucletContentEntryTreeNodes(List<AbstractNucletContentEntryTreeNode> nodes) {
		return CollectionUtils.sorted(nodes, new AbstractNucletContentEntryTreeNode.Comparator());
	}

	@Override
	public AbstractNucletContentEntryTreeNode getNucletContentEntryNode(EntityMeta<UID> entity, UID eoId) {
		return getNucletContentEntryNode(nucletDalProvider.getEntityObjectProcessor(entity).getByPrimaryKey(eoId));
	}

	private AbstractNucletContentEntryTreeNode getNucletContentEntryNode(EntityObjectVO<?> eo) {
		if (eo == null) {
			throw new IllegalArgumentException("eo must not be null");
		}
		
		UID entityUID = eo.getDalEntity();
		if (!E.isNuclosEntity(entityUID)) {
			throw new IllegalArgumentException("entity object must be nuclos entity");
		}
		if (E.ENTITY.checkEntityUID(entityUID)) {
			return new NucletContentEntityTreeNode((EntityObjectVO<UID>) eo);
		}
		else if (E.CUSTOMCOMPONENT.checkEntityUID(entityUID)) {
			return new NucletContentCustomComponentTreeNode(eo);
		}
		else if (E.PROCESS.checkEntityUID(entityUID)) {
			return new NucletContentProcessTreeNode((EntityObjectVO<UID>) eo);
		}
		else if (E.SEARCHFILTER.checkEntityUID(entityUID)) {
			return new NucletContentSearchfilterNode(eo);
		}
		else if (E.PREFERENCE.checkEntityUID(entityUID)) {
			return new NucletContentPreferenceTreeNode(eo);
		}
		else {
			return new DefaultNucletContentEntryTreeNode(eo);
		}
	}

	public <PK> DynamicTreeNode<PK> getDynamicTreeNode(TreeNode node, MasterDataVO<PK> mdVO) {
		return new DynamicTreeNode<>(null, node, mdVO);
	}

	public <PK> SubFormTreeNode<PK> getSubFormTreeNode(TreeNode node, MasterDataVO<PK> mdVO) {
		return new SubFormTreeNode<>(null, node, mdVO);
	}

	@Override
	public List<TreeNode> getSubNodesForDynamicTreeNode(TreeNode node) {
		if (null == node.getNodeId()) {
			throw new IllegalArgumentException("nodeId for " + node + " must not be null");
		}
		return getEntityObjectSubNodes(node, node.getNodeId(), null);
	}

	/**
	 * get the subnodes for a masterdata search result
	 * 
	 * �postcondition result != null
	 * 
	 * @param node
	 * @return the subnodes for the given node.
	 */
	@Override
	public <PK> List<DefaultMasterDataTreeNode<PK>> getSubNodes(MasterDataSearchResultTreeNode<PK> node) throws CommonPermissionException {
		checkReadAllowed(node.getEntityUID());
		final List<DefaultMasterDataTreeNode<PK>> result = new ArrayList<>();
		final Long idRoot = (node.getRootId() != null) ? IdUtils.toLongId(node.getRootId()) : IdUtils.toLongId(node.getId());
		final EntityMeta<PK> eMeta = metaProvider.getEntity(node.getEntityUID());
		CollectableSearchCondition cond = mandatorUtils.append(node.getSearchCondition(), eMeta);
		for (MasterDataVO mdvo : masterDataFacade.getMasterData(eMeta, node.getSearchCondition())) {
			result.add(mdTreeNodeFactory.newDefaultMasterDataTreeNode(mdvo, node.getNodeId(), idRoot));
		}

		Collections.sort(result);
		assert result != null;
		return result;
	}


	/**
	 * method to get the list of sub nodes for a specific generic object search result tree node
	 * 
	 * �postcondition result != null
	 * 
	 * @param node tree node of type search result tree node
	 * @return list of sub nodes for given tree node
	 */
	public List<TreeNode> getSubNodes(EntitySearchResultTreeNode node) throws CommonPermissionException {
		final UID eId = node.getEntityUID();
		checkReadAllowed(eId);
		final Integer iMaxRowCount = Math.min(DEFAULT_ROWCOUNT_FOR_SEARCHRESULT, serverParameterProvider.getIntValue(
				ParameterProvider.KEY_MAX_ROWCOUNT_FOR_SEARCHRESULT_IN_TREE, DEFAULT_ROWCOUNT_FOR_SEARCHRESULT));
		final List<TreeNode> result = new ArrayList<>(iMaxRowCount);

		node.getSearchExpression().setSearchCondition(mandatorUtils.append(node.getSearchExpression(), metaProvider.getEntity(node.getEntityUID())));
		Integer truncatedFrom = null;

		if (Modules.getInstance().isModule(eId)) {

			// final EntityMeta iModuleId = Modules.getInstance().getModule(node.getEntity());
			final TruncatableCollection<GenericObjectWithDependantsVO> collgowdvo =
					genericObjectFacade.getRestrictedNumberOfGenericObjectsNoCheck(
							eId, node.getSearchExpression(),
							Collections.emptySet(),
							serverParameterProvider.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), iMaxRowCount.longValue());

			final Long idRoot = (node.getRootId() != null) ? IdUtils.toLongId(node.getRootId()) : IdUtils.toLongId(node.getId());
			for (GenericObjectWithDependantsVO gowdvo : collgowdvo) {
				result.add(GenericObjectTreeNodeFactory.getInstance().newTreeNode(gowdvo, null, null, null, getCurrentUserName(), null, null, node.getNodeId(), idRoot, getCurrentMandatorUID()));
			}

			if (collgowdvo.isTruncated()) {
				truncatedFrom = collgowdvo.totalSize();
			}

		} else {

			// NUCLOS-6937 because of some strange "hack" of (de)serializer in EntitySearchResultTreeNode.java
			// node.getSearchException is of the type CollectableGenericObjectSearchExpression, which leads later to error.
			CollectableSearchExpression cse = new CollectableSearchExpression(node.getSearchExpression().getSearchCondition(),
					node.getSearchExpression().getSortingOrder());
			List<Object> ids = masterDataFacade.getMasterDataIdsNoCheck(node.getEntityUID(), cse);
			if (ids.size() > iMaxRowCount) {
				truncatedFrom = ids.size();
				ids = ids.subList(0, iMaxRowCount);
			}

			List<EntityObjectVO<Object>> eos =
				nucletDalProvider.getEntityObjectProcessor(node.getEntityUID()).getByPrimaryKeys(ids);

			for (EntityObjectVO<Object> eo : eos) {
				MasterDataVO<Object> mdvo = new MasterDataVO<>(eo);
				result.add(mdTreeNodeFactory.newDefaultMasterDataTreeNode(mdvo, node.getNodeId(), IdUtils.toLongId(node.getId())));
			}

		}

		if (truncatedFrom != null) {
			String sLabel = MessageFormat.format(localeFacade.getResourceById(localeFacade.getUserLocale(),
					"treenode.subnode.label"),
					node.getLabel(), result.size(), truncatedFrom);
			node.setLabel(sLabel);
		}

		/** @todo OPTIMIZE: sort in the database! */
		//Collections.sort(result);

		assert result != null;
		return result;

	}

	private static class GenericObjectTreeNodeChildrenComparator implements Comparator<TreeNode> {
		@Override
		public int compare(TreeNode tn1, TreeNode tn2) {
			int result = getOrder(tn1) - getOrder(tn2);
			if (result == 0) {
				if (tn1 instanceof GenericObjectTreeNode) {
					result = ((Comparable<GenericObjectTreeNode>) tn1).compareTo((GenericObjectTreeNode) tn2);
				}
				else if (tn1 instanceof RelationTreeNode) {
					result = ((Comparable<TreeNode>) tn1).compareTo(tn2);
				}
				else {
					throw new CommonFatalException("Cannot compare the given TreeNodes.");
				}
			}
			return result;
		}

		private int getOrder(TreeNode treenode) {
			return LangUtils.isInstanceOf(treenode, GenericObjectTreeNode.class) ? 1 : 2;
		}

	}	// inner class GenericObjectTreeNodeChildrenComparator

	@Override
	public <PK> List<SubFormEntryTreeNode<PK>> getSubNodesForSubFormTreeNode(TreeNode node, MasterDataVO<PK> mdVO) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void importNodes(UID targetEntityUid, Object id, Collection<Pair<UID, Object>> sourceNodeIds) throws CommonBusinessException {
		Set<UID> writeChecked = new HashSet<>();
		for (Pair<UID, Object> sourceNodeId : sourceNodeIds) {
			EntityTreeViewVO sourceNode = treeProvider.getNode(sourceNodeId.x);
			// Pr�fe Schreibberechtigung
			UID sourceEntityUid = sourceNode.getEntity();
			if (!writeChecked.contains(sourceEntityUid)) {
				checkWriteAllowed(sourceEntityUid);
				writeChecked.add(sourceEntityUid);
			}
			// Pr�fe ob die einstellte Referenz vom Quellknoten zum Parent mit dem Zielknoten Typ �bereinstimmt:
			FieldMeta<?> fm = metaProvider.getEntityField(sourceNode.getField());
			if (!targetEntityUid.equals(fm.getForeignEntity())) {
				throw new CommonValidationException("TreeNodeNotAssignable");
			}
			EntityObjectFacadeLocal eoFacade = SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class);
			EntityMeta<?> eoMeta = metaProvider.getEntity(targetEntityUid);
			EntityObjectVO<?> eo = eoFacade.get(sourceNode.getEntity(), sourceNodeId.y);
			if (eoMeta.isUidEntity()) {
				eo.setFieldUid(fm.getUID(), (UID) id);
			} else {
				eo.setFieldId(fm.getUID(), (Long) id);
			}
			eoFacade.update(eo, false);
		}
	}
	
	private static class SetFactory<T2> implements Supplier<Set<T2>> {
		
		private SetFactory() {
		}

		@Override
		public Set<T2> get() {
			return new TreeSet<>((T2 t1, T2 t2) -> {
				try {
					if (t1 instanceof Comparable && t2 instanceof Comparable) {
						return ObjectUtils.compare((Comparable)t1, (Comparable)t2);
					} else if (t1 instanceof AbstractTreeNode && t2 instanceof AbstractTreeNode) {
						return RigidUtils.compareBasedOnToString(((AbstractTreeNode)t1).getLabel(), ((AbstractTreeNode)t2).getLabel());
					}
					return RigidUtils.compareBasedOnToString(t1, t2);
				} catch (Exception ex) {
					LOG.error("Error during tree sort: {}, t1={} ,t2={}",
							  ex.getMessage(), t1, t2);
					return 0;
				}
			});
		}
		
	}
	
	
	/**
	 *
     */
    public SimpleTreeNode getExpandedTreeWithSearch(DefaultMasterDataTreeNode root, String search) throws CommonBusinessException {
    	checkReadAllowed(root.getEntityUID());

        long maxSize = 200; //TODO FLEXIBLE
        int maxDeep = 5;    //TODO FLEXIBLE
        
        // LOAD META DATA
        Map<Integer,ArrayList<EntityTreeViewVO>> levelMeta = new HashMap<>();
        Map<UID, Pair<EntityTreeViewVO, Integer>> levelEntity = new HashMap<>();
        for (int level=0; level<=maxDeep; level++) {
            levelMeta.put(level, new ArrayList<>());
        }
        // META DATA
        List<EntityTreeViewVO> treeL1 = new ArrayList<>();
        getTreeProvider().loadTreeConfiguration(root.getEntityUID(), treeL1 ); //TODO CACHEABLE
        
        //RECRUSIVE INIT UNTIL LEVEL maxDEEP
        initLevelMetaAndLevelEntity(treeL1, levelMeta, levelEntity, 1, maxDeep);
       
        // SEARCH ALL LEVEL
        HashMap<Object,Object> idsFind = new HashMap<>(); //uid / EntityUid
        
        Map<Integer,ArrayList<Pair<EntityTreeViewVO, EntityObjectVO<Object>>>> levelData = new HashMap<>();
        for (int level=0; level<maxDeep; level++) {
            levelData.put(level, new ArrayList<>());
        }
        
        for (int level=0; level<maxDeep; level++) {
            for (EntityTreeViewVO n : levelMeta.get(level)) {
                
                EntityMeta<Object> e = metaProvider.getEntity(n.getEntity());
                CompositeCollectableSearchCondition likes = new CompositeCollectableSearchCondition(LogicalOperator.OR);
                List<CollectableEntityField> fields = extractUids(new CollectableEOEntity(getMetaProvider().getEntity(n.getEntity())), n.getNode());
                for (CollectableEntityField field : fields) {
                    likes.addOperand(new CollectableLikeCondition(field, search));
                }
                List<CollectableEntityField> fieldsTooltip = extractUids(new CollectableEOEntity(getMetaProvider().getEntity(n.getEntity())), n.getNodeTooltip());
                for (CollectableEntityField field : fieldsTooltip) {
                    likes.addOperand(new CollectableLikeCondition(field, search));
                }            
                CollectableSearchExpression clctexpr = new CollectableSearchExpression(likes);
                
                // RESULT FIELDs
                Collection<UID> fieldUids = new ArrayList<>();
                for (CollectableEntityField field : fields) {
                	fieldUids.add(field.getUID());   //LABEL FIELDs
                }
                fieldUids.add(n.getField());         //PARENT NODE REFERENCE FIELD
                
                ResultParams resultParams = new ResultParams(fieldUids, 0L, maxSize, true);
                Collection<EntityObjectVO<Object>> level2result2 = entityObjectFacade.getEntityObjectsChunk(n.getEntity(), clctexpr, resultParams, null);
                for (EntityObjectVO<Object> entityObjectVO : level2result2) {
                    levelData.get(level).add(new Pair<>(n, entityObjectVO));  //ADD DATA
                    idsFind.put(entityObjectVO.getPrimaryKey(), n);        //ADD FIND ID
                }

                //APPEND required PARENTs to previous LEVEL 
                List<Object> inComparands = new ArrayList<>();
                for (EntityObjectVO<Object> entityObjectVO : level2result2) {
                    inComparands.add(entityObjectVO.getFieldId(n.getField()));
                }
               
                loadNodesByIdList(maxSize, levelEntity, idsFind, levelData, n, inComparands);
                
                
            }
        }
        
        //LOAD MISSING Parent-Ids
        boolean loadRequired = true;
        int rounds = 0;                 // LIMIT PARENT REQUEST ROUNDs
        while(loadRequired){
            loadRequired = findMissingIdsAndLoad(maxSize, maxDeep, levelEntity, idsFind, levelData);
            if(loadRequired) loadRequired=rounds<maxDeep;
            rounds++;
        }
        
        // CREATE EXTENDED TREE
        SimpleTreeNode result = new SimpleTreeNode(root.getId(), root.getLabel(), root.getDescription(), null, root.getEntityUID()) ;
        //1. TRANSFORM ALL EVO&DATA --> SimpleTreeNode
        Map<Object, SimpleTreeNode> idAndNodes = new HashMap<>();
        for (int level=0; level<maxDeep; level++) {
            for (Pair<EntityTreeViewVO, EntityObjectVO<Object>> pair : levelData.get(level)) {
                EntityTreeViewVO n = pair.getX();
                EntityObjectVO<Object> eov = pair.getY();
                Long parentId = eov.getFieldId(n.getField());
                
                List<CollectableEntityField> fields = extractUids(new CollectableEOEntity(getMetaProvider().getEntity(n.getEntity())), n.getNode());
                String label = n.getNode();
                for (CollectableEntityField collectableEntityField : fields) {
                    String place = "uid\\{"+collectableEntityField.getUID()+"\\}";
                    String value = eov.getFieldValue(collectableEntityField.getUID(), String.class);
                    label = label.replaceAll(place, value);
                }
                
                List<CollectableEntityField> descriptionFields = extractUids(new CollectableEOEntity(getMetaProvider().getEntity(n.getEntity())), n.getNodeTooltip());
                String description = n.getNode();
                for (CollectableEntityField collectableEntityField : descriptionFields) {
                    String place = "uid\\{"+collectableEntityField.getUID()+"\\}";
                    String value = eov.getFieldValue(collectableEntityField.getUID(), String.class);
                    description = description.replaceAll(place, value);
                }
                
                idAndNodes.put(eov.getPrimaryKey(),  new SimpleTreeNode(eov.getPrimaryKey(), label, description, parentId, n.getEntity()));
            }
        }
        
        //2. FIND PARENTS AND ADD IN PARENTS SUBNODEs
        for (SimpleTreeNode n : idAndNodes.values()){
           if(n.getParentId()!=null && n.getParentId().equals(root.getId())){
               result.getSubNodes().add(n);
           }else if(n.getParentId()!=null){
               idAndNodes.get(n.getParentId()).getSubNodes().add(n);
           }
        }
        
        //3. SORT SUBNODEs
        result.sortSubNodesByLabel();
        for (SimpleTreeNode n : idAndNodes.values()){
            n.sortSubNodesByLabel();
        }
        
        
        return result;
    }

    /**
     * 
     * @param tree
     * @param levelMeta
     * @param levelEntity
     * @param level
     * @param maxDeep
     */
    private void initLevelMetaAndLevelEntity(List<EntityTreeViewVO> tree, Map<Integer,ArrayList<EntityTreeViewVO>> levelMeta, Map<UID, Pair<EntityTreeViewVO, Integer>> levelEntity, int level, int maxDeep){
        for (EntityTreeViewVO node : tree) {
            levelMeta.get(level).add(node);
            levelEntity.put(node.getEntity(), new Pair<>(node, level));
            level++;
            List<EntityTreeViewVO> treeNext = node.getChildNodes();
            if(level<maxDeep){
                initLevelMetaAndLevelEntity(treeNext, levelMeta, levelEntity, level, maxDeep);
            }
        }
    }

    private boolean findMissingIdsAndLoad(long maxSize, int maxDeep, Map<UID, Pair<EntityTreeViewVO, Integer>> levelEntity, HashMap<Object, Object> idsFind,
            Map<Integer, ArrayList<Pair<EntityTreeViewVO, EntityObjectVO<Object>>>> levelData) {
        boolean loadRequired = false;
        for (int level=maxDeep-1; level>=0; level--) {
            ArrayList<Pair<EntityTreeViewVO, EntityObjectVO<Object>>> nodes = levelData.get(level);
            Map<EntityTreeViewVO, ArrayList<Object>> nodeAndIn= new HashMap<>();
            //FIND MISSING
            for (Pair<EntityTreeViewVO, EntityObjectVO<Object>> pair : nodes) {
                EntityTreeViewVO n = pair.getX();
                EntityObjectVO<Object> eov = pair.getY();
                UID parentEntityUID = new CollectableEOEntity(getMetaProvider().getEntity(n.getEntity())).getEntityField(n.getField()).getReferencedEntityUID();
                Long parenID = eov.getFieldId(n.getField());
                if(parenID!=null && idsFind.get(parenID)==null){ 
                    ArrayList<Object> ins = nodeAndIn.get(n);
                    if(ins==null){
                        ins = new ArrayList<>();
                        nodeAndIn.put(n, ins);
                        loadRequired = true;
                    }
                    ins.add(parenID);
                }
            }
            
            //LOAD MISSING
            for (Entry<EntityTreeViewVO, ArrayList<Object>> entry : nodeAndIn.entrySet()) {
                loadNodesByIdList(maxSize, levelEntity, idsFind, levelData, entry.getKey(), entry.getValue());
            }
        }
        return loadRequired;
    }

    /**
     * Load Nodes by a list from Id
     * @param maxSize
     * @param levelEntity
     * @param idsFind
     * @param levelData
     * @param n
     * @param idList
     * @return
     */
    private Collection<EntityObjectVO<Object>> loadNodesByIdList(long maxSize, Map<UID, Pair<EntityTreeViewVO, Integer>> levelEntity, HashMap<Object, Object> idsFind,
            Map<Integer, ArrayList<Pair<EntityTreeViewVO, EntityObjectVO<Object>>>> levelData, EntityTreeViewVO n, List<Object> idList) {
        UID parentEntityUID = new CollectableEOEntity(getMetaProvider().getEntity(n.getEntity())).getEntityField(n.getField()).getReferencedEntityUID();
        Integer parentLevel = levelEntity.get(parentEntityUID).getY();
        EntityTreeViewVO parentN = levelEntity.get(parentEntityUID).getX();
        CollectableEOEntity parentEntity = new CollectableEOEntity(getMetaProvider().getEntity(parentEntityUID));
        CollectableEntityField parentEntityField = parentEntity.getEntityField( new UID(parentEntity.getUID()+"0")); //TODO BETTER WAY FOR GET THE PK-FIELD
        CollectableInIdCondition<Object> ins = new CollectableInIdCondition<>(parentEntityField, idList);
        ResultParams resultParams = new ResultParams(maxSize, true);
        Collection<EntityObjectVO<Object>> result = entityObjectFacade.getEntityObjectsChunkNoCheck(parentEntityUID, new CollectableSearchExpression(ins), resultParams, null);
        for (EntityObjectVO<Object> entityObjectVO : result) {
            levelData.get(parentLevel).add(new Pair<>(parentN, entityObjectVO));  //ADD DATA
            idsFind.put(entityObjectVO.getPrimaryKey(), parentN);                                                         //ADD FIND ID
        }
        return result;
    }
	
    /**
     * Extract UID List from a Label/Tooltip String for Search Conditions
     * @param str
     * @return
     */
    private List<CollectableEntityField> extractUids(CollectableEOEntity entity, String str){
        List<CollectableEntityField> uids  = new ArrayList<>();
        boolean next = str.contains("uid{");
        while(next){
            str = str.replaceAll(" ", "");
            String result = str.substring(str.indexOf("uid{") + 4, str.indexOf("}"));
            str = str.replaceFirst("uid\\{"+result+"\\}","");
            uids.add(entity.getEntityField(new UID(result)));
            
            next = str.contains("uid{");
        }
        return uids;
    }

}
