//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.api.eventsupport;

import java.util.Map;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;
import org.nuclos.common.UID;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class DeleteEventObjectImpl extends AbstractEntityEventObject implements org.nuclos.api.context.DeleteContext {


	private final boolean logical;
	private final BusinessObject busiObject;
	
	public DeleteEventObjectImpl(Map<String, Object> eventCache, UID entity, Modifiable busiObject, boolean logical, String pRuleClassname, UID dataLanguage) {
		super(eventCache, entity, pRuleClassname, dataLanguage);
		
		this.logical = logical;
		this.busiObject = (BusinessObject) busiObject;
	}

	@Override
	public boolean isLogical() {
		return logical;
	}

	@Override
	public <T extends Modifiable> T getBusinessObject(Class<T> t) {
		return super.getBusinessObject(busiObject, t);
	}

	@Override
	public <T extends GenericBusinessObject> T getGenericBusinessObject(final Class<T> t) throws BusinessException {
		return super.wrapBusinessObject(this.busiObject, t);
	}

	public void notify(String message, Priority prio) {
		super.notify(message, prio, busiObject, null);
	}
	
	public void notify(String message) {
		notify(message, Priority.NORMAL, busiObject, null);
	}
}
