//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.api.eventsupport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;
import org.nuclos.common.UID;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class GenerateContextImpl extends AbstractEventObjectWithCache implements org.nuclos.api.context.GenerateContext {
	
	private final Collection<? extends Modifiable> sourceObjects;
	private final BusinessObject targetObject;
	private final BusinessObject parameterObject;
	
	public <T extends Modifiable> GenerateContextImpl(Map<String, Object> eventCache, Collection<T> sourceObjects, 
			BusinessObject targetObject, BusinessObject parameterObject, String pRuleClassname, UID dataLanguage) {
		super(eventCache, pRuleClassname, dataLanguage);
		
		this.sourceObjects = sourceObjects;
		this.targetObject = targetObject;
		this.parameterObject = parameterObject;
	}

	@Override
	public <T extends Modifiable> Collection<T> getSourceObjects(Class<T> t) {
		List<T> retVal = new ArrayList<T>();
			for (Modifiable mbo : this.sourceObjects) {
				retVal.add((T) mbo);
			}
		return retVal;
	}

	@Override
	public <T extends Modifiable> T getTargetObject(Class<T> t) {
		return (T) this.targetObject;
	}

	@Override
	public BusinessObject getParameterObject() {
		return parameterObject;
	}

	@Override
	public <T extends GenericBusinessObject> T getSourceGenericObject(final Class<T> t) throws BusinessException {
		if (sourceObjects.size() <= 0 || sourceObjects.size() > 1)
			throw new BusinessException("Invalid number of SourceObjects found in this context. Method getSourceObject() just allows one item. Use getSourceObjects() instead.");

		Modifiable modiObject = sourceObjects.iterator().next();
		return super.wrapModifiable(modiObject, t);
	}

	@Override
	public <T extends GenericBusinessObject> Collection<T> getSourceGenericObjects(final Class<T> t) throws BusinessException {
		Collection<T> result = new ArrayList<>();
		for (Modifiable mbo : this.sourceObjects) {
			result.add(super.wrapModifiable(mbo, t));
		}
		return result;
	}

	@Override
	public <T extends GenericBusinessObject> T getTargetGenericObject(final Class<T> t) throws BusinessException {
		return super.wrapBusinessObject(this.targetObject, t);
	}

	@Override
	public <T extends GenericBusinessObject> T getParameterGenericObject(final Class<T> t) throws BusinessException {
		return super.wrapBusinessObject(this.parameterObject, t);
	}

	@Override
	public void notify(String message, Priority prio, BusinessObject source) {
		super.notify(message, prio, source, this.targetObject);
	}

	@Override
	public void notify(String message, BusinessObject source) {
		super.notify(message, Priority.NORMAL, source, this.targetObject);
	}

	// @Override
	public <T extends Modifiable> T getSourceObject(Class<T> t)
			throws BusinessException {
		Collection<T> results = this.getSourceObjects(t);
		
		if (results.size() <= 0 || results.size() > 1)
			throw new BusinessException("Invalid number of SourceObjects found in this context. Method getSourceObject() just allows one item. Use getSourceObjects() instead.");
		
		return results.iterator().next();
	}

	
	
}
