//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.serializable;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.json.JsonObject;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMeta.Valueable;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.TransferConstants;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.nuclet.AbstractNucletContentConstants;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.dbtransfer.TransferUtils;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class DefaultContentSerializable implements TransferConstants {
	
	private static final Logger LOG = LoggerFactory.getLogger(DefaultContentSerializable.class);
	
	public static void marshal(TransferEO teo, HierarchicalStreamWriter writer,	MarshallingContext context) {
		EntityObjectVO<UID> eo = teo.eo;
		UID entityUID = eo.getDalEntity();
		EntityMeta<UID> eMeta = MetaProvider.getInstance().getEntity(entityUID);
		Map<UID, FieldMeta<?>> efMetas = MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUID);
		Set<Valueable<String>> clobFields = TransferUtils.getClobFields(eMeta);
		Set<Valueable<JsonObject>> jsonFields = TransferUtils.getJsonFields(eMeta);
		
		for (UID fieldUID : CollectionUtils.sorted(efMetas.keySet())) {
			if (SF.CREATEDBY.checkField(entityUID, fieldUID) ||
					SF.CREATEDAT.checkField(entityUID, fieldUID) ||
					SF.CHANGEDBY.checkField(entityUID, fieldUID) ||
					SF.CHANGEDAT.checkField(entityUID, fieldUID) ||
					SF.IMPORTVERSION.checkField(entityUID, fieldUID) ||
					SF.ORIGINUID.checkField(entityUID, fieldUID)) {
				continue;
			}
			FieldMeta<?> efMeta = efMetas.get(fieldUID);
			if (clobFields.contains(efMeta) || jsonFields.contains(efMeta)) {
				continue;
			}
			
			Object value = eo.getFieldValue(fieldUID);
			UID valueUid = eo.getFieldUid(fieldUID);
			if (value != null || valueUid != null) {
				writer.startNode(efMeta.getFieldName());
				writer.addAttribute("fielduid", fieldUID.getString());
				if (valueUid != null && !GenericObjectDocumentFile.class.getCanonicalName().equals(efMeta.getDataType())) {
					context.convertAnother(valueUid);
				} else if (value != null) {
					context.convertAnother(value);
				}
				writer.endNode();
			}
		}
		
		if (eo.getFieldValues().containsKey(AbstractNucletContentConstants.LOCALE_RESOURCE_MAPPING_UID)) {
			@SuppressWarnings("unchecked")
			Map<LocaleInfo, Map<UID, String>> localeResources = (Map<LocaleInfo, Map<UID, String>>) eo.getFieldValue(AbstractNucletContentConstants.LOCALE_RESOURCE_MAPPING_UID);
			for (LocaleInfo li : CollectionUtils.sorted(localeResources.keySet(), new Comparator<LocaleInfo>() {
				@Override
				public int compare(LocaleInfo o1, LocaleInfo o2) {
					int result = StringUtils.compareIgnoreCase(o1.getCountry(), o2.getCountry());
					if (result != 0) {
						return result;
					}
					result = StringUtils.compareIgnoreCase(o1.getLanguage(), o2.getLanguage());
					return result;
				}
			})) {
				if (!localeResources.get(li).isEmpty()) {
					writer.startNode(FIELD_FOR_LOCALE_RESOURCES);
					if (li.getCountry() != null) writer.addAttribute("country", li.getCountry());
					if (li.getLanguage() != null) writer.addAttribute("language", li.getLanguage());
					for (UID key : CollectionUtils.sorted(localeResources.get(li).keySet())) {
						String nodeName = MetaProvider.getInstance().getEntityField(key).getFieldName();
						if (localeResources.get(li).get(key) != null) {
							writer.startNode(nodeName);
							writer.addAttribute("fielduid", key.getString());
							context.convertAnother(localeResources.get(li).get(key));
							writer.endNode();
						}
					}
					writer.endNode();
				}
			}
		}
	}
	
	public static void unmarshal(TransferEO teo, HierarchicalStreamReader reader, UnmarshallingContext context, Map<String, Runnable> childRunners) {
		EntityObjectVO<UID> eo = teo.eo;
		UID entityUID = eo.getDalEntity();
		EntityMeta<UID> eMeta = MetaProvider.getInstance().getEntity(entityUID);
		Map<UID, FieldMeta<?>> efMetas = MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUID);
		
		Map<LocaleInfo, Map<UID, String>> localeResources = new HashMap<LocaleInfo, Map<UID, String>>();
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			try {
				String fieldName = reader.getNodeName();
				UID fieldUID = UID.parseUID(reader.getAttribute("fielduid"));
				if (childRunners.containsKey(fieldName)) {
					childRunners.get(fieldName).run();
				} else if (FIELD_FOR_LOCALE_RESOURCES.equals(fieldName)) {
					String country = reader.getAttribute("country");
					String language = reader.getAttribute("language");
					LocaleFacadeLocal localeFacade = ServerServiceLocator.getInstance().getFacade(LocaleFacadeLocal.class);
					for (LocaleInfo li : localeFacade.getAllLocales(false)) {
						if (StringUtils.equals(country, li.getCountry()) && StringUtils.equals(language, li.getLanguage())) {
							while (reader.hasMoreChildren()) {
								reader.moveDown();
								try {
									UID resourceKey = UID.parseUID(reader.getAttribute("fielduid"));
									String resourceValue = reader.getValue();
									if (!localeResources.containsKey(li)) {
										localeResources.put(li, new HashMap<UID, String>());
									}
									localeResources.get(li).put(resourceKey, resourceValue);
								} catch (Exception ex) {
									LOG.warn("Error during unmarshal eoml. entity={}, uid={}",
									         eMeta.getEntityName(), entityUID.getString(), ex);
								} finally {
									reader.moveUp();
								}
							}
						}
					}
				} else {
					if (efMetas.containsKey(fieldUID)) {
						FieldMeta<?> efMeta = efMetas.get(fieldUID);
						boolean convert = true;
						if (Long.class.getName().equals(efMeta.getDataType()) ||
								Integer.class.getName().equals(efMeta.getDataType()) || 
								Double.class.getName().equals(efMeta.getDataType())) {
							// otherwise NumberFormatException for empty Strings!
							String sValue = reader.getValue();
							if (StringUtils.looksEmpty(sValue)) {
								convert = false;
							}
						}
						if (convert) {
							Object obj = context.convertAnother(
									eo, LangUtils.getClassLoaderThatWorksForWebStart().loadClass(efMeta.getDataType()));
							if (obj != null) {
								if (efMeta.getForeignEntity() != null || efMeta.getUnreferencedForeignEntity() != null) {
									if (obj instanceof GenericObjectDocumentFile &&
											GenericObjectDocumentFile.class.getCanonicalName().equals(efMeta.getDataType())) {
										eo.setFieldUid(fieldUID, (UID) ((GenericObjectDocumentFile) obj).getDocumentFilePk());
										eo.setFieldValue(fieldUID, obj);
									} else {
										eo.setFieldUid(fieldUID, (UID) obj);
									}
								} else {
									eo.setFieldValue(fieldUID, obj);
								}
							}
						}
					}
				}
			} catch (Exception ex) {
				LOG.warn("Error during unmarshal eoml. entity={}, uid={}",
				         eMeta.getEntityName(), entityUID.getString(), ex);
			} finally {
				reader.moveUp();
			}
		}
		
		if (!localeResources.isEmpty()) {
			eo.setFieldValue(AbstractNucletContentConstants.LOCALE_RESOURCE_MAPPING_UID, localeResources);
		}
	}
	
}
