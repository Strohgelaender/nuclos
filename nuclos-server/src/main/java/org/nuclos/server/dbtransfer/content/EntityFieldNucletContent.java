//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dbtransfer.TransferUtils;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class EntityFieldNucletContent extends DefaultNucletContent {

	public EntityFieldNucletContent(List<INucletContent> contentTypes) {
		super(E.ENTITYFIELD.entity, contentTypes);
	}

	@Override
	public String getIdentifier(EntityObjectVO<UID> eo, NucletContentMap importContentMap) {
		return eo.getFieldValue(getIdentifierField(), String.class);
	}

	@Override
	public UID getIdentifierField() {
		return E.ENTITYFIELD.field.getUID();
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		storeLocaleResources(ncObject, E.ENTITYFIELD.localeresourcel, E.ENTITYFIELD.localeresourced);
		return ncObject;
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean isNuclon, boolean testOnly) {
		restoreLocaleResources(ncObject);
		
		if (!ncObject.isFlagNew()) {
			EntityObjectVO<UID> existing = getEOOriginal(E.ENTITYFIELD.getUID(), ncObject.getPrimaryKey());		
			if (ncObject.getFieldValue(E.ENTITYFIELD.isLocalized) == null && existing.getFieldValue(E.ENTITYFIELD.isLocalized) != null) {
				ncObject.setFieldValue(E.ENTITYFIELD.isLocalized, existing.getFieldValue(E.ENTITYFIELD.isLocalized));
			}
			final UID newForeignIntegrationPoint = ncObject.getFieldUid(E.ENTITYFIELD.foreignIntegrationPoint);
			final UID oldForeignIntegrationPoint = existing.getFieldUid(E.ENTITYFIELD.foreignIntegrationPoint);
			if (newForeignIntegrationPoint != null && RigidUtils.equal(newForeignIntegrationPoint, oldForeignIntegrationPoint)) {
				ncObject.setFieldUid(E.ENTITYFIELD.foreignentity, existing.getFieldUid(E.ENTITYFIELD.foreignentity));
				ncObject.setFieldValue(E.ENTITYFIELD.foreignentityfield, existing.getFieldValue(E.ENTITYFIELD.foreignentityfield));
			}
		}
		
		return super.insertOrUpdateNcObject(result, ncObject, isNuclon, testOnly);
	}
	
	
	@Override
	public boolean validate(TransferEO teo, ValidationType type, NucletContentMap importContentMap, 
			Set<UID> existingNucletUIDs, ValidityLogEntry log, Map<TransferOption, Serializable> transferOptions, boolean hasDataLanguages) {
		boolean result =  super.validate(teo, type, importContentMap, existingNucletUIDs, log, transferOptions, hasDataLanguages);
		if (result == false) {
			return result;
		}
		final EntityObjectVO<UID> ncObject = teo.eo;
		final UID foreignentity = ncObject.getFieldUid(E.ENTITYFIELD.foreignentity);
		
		// validate simple refs
		switch (type) {
		case INSERT:
		case UPDATE:
			UID fieldGroupUID = ncObject.getFieldUid(E.ENTITYFIELD.entityfieldgroup);
			
			Boolean isLocalized = ncObject.getFieldValue(E.ENTITYFIELD.isLocalized) == null ? Boolean.FALSE: 
				Boolean.valueOf(ncObject.getFieldValue(E.ENTITYFIELD.isLocalized));
			
			if (isLocalized && !hasDataLanguages) {
				log.newCriticalLine("Keine Mehrsprachigkeit im Zielsystem vorhanden");
			}
			
			if (fieldGroupUID != null && TransferUtils.getEntityObjectVO(importContentMap.getValues(E.ENTITYFIELDGROUP), fieldGroupUID) == null) {
				if (TransferUtils.getEntityObject(E.ENTITYFIELD, fieldGroupUID) == null) {
					// not adjusted...
					ncObject.removeFieldUid(E.ENTITYFIELD.entityfieldgroup.getUID());
				}
			}
			
			UID autoNumberUID = ncObject.getFieldUid(E.ENTITYFIELD.autonumberentity);
			if (autoNumberUID != null && TransferUtils.getEntityObjectVO(importContentMap.getValues(E.ENTITY), autoNumberUID) == null) {
				if (TransferUtils.getEntityObject(E.ENTITY, autoNumberUID) == null) {
					// not adjusted...
					ncObject.removeFieldUid(E.ENTITYFIELD.autonumberentity.getUID());
				}
			}
			break;
		default: break;
		}
		
		// validate foreign entity
		switch (type) {
		case INSERT:
			// check if references are possible
			if (foreignentity != null && TransferUtils.getEntityObjectVO(importContentMap.getValues(E.ENTITY), foreignentity) == null) {
				boolean dummy = true;
				if (E.isNuclosEntity(foreignentity)) {
					dummy = false;
				} else {
					EntityObjectVO<UID> foreinentityVO = TransferUtils.getEntityObject(E.ENTITY, foreignentity);
					if (foreinentityVO != null) {
						dummy = false;						
					} else {
						// no entity with uid found. seach for entity with same name and use it
						for (EntityMeta<?> eMeta : MetaProvider.getInstance().getAllEntities()) {
							if (LangUtils.equal(eMeta.getEntityName(), ncObject.getFieldValue(E.ENTITYFIELD.foreignentity.getUID()))) {
								dummy = false;
							}
						}
					}
					//  TODO check field presentation 
				}
				if (dummy) {
					EntityObjectVO<UID> entityVO = TransferUtils.getEntityObjectVO(importContentMap.getValues(E.ENTITY), ncObject.getFieldUid(E.ENTITYFIELD.entity)).eo;
					log.newWarningLine("Entity " + entityVO.getFieldValue(E.ENTITY.entity) + " references to unknown entity " + foreignentity + ". Redirect to dummy entity!");
					ncObject.setFieldUid(E.ENTITYFIELD.foreignentity, E.DUMMY.getUID());
					ncObject.setFieldValue(E.ENTITYFIELD.foreignentityfield, E.DUMMY.name.getUID().getStringifiedDefinition());
				}
			}
			break;
		case UPDATE:
			if (foreignentity != null && TransferUtils.getEntityObjectVO(importContentMap.getValues(E.ENTITY), foreignentity) == null) {
				// adjust foreignentity
				UID uid = ncObject.getPrimaryKey();
//				EntityObjectVO<UID> existingFieldVO = TransferUtils.getEntityObject(E.ENTITYFIELD, uid);
				EntityObjectVO<UID> existingFieldVO = getEOOriginal(E.ENTITYFIELD.getUID(), uid);
				UID existingForeignEntity = existingFieldVO.getFieldUid(E.ENTITYFIELD.foreignentity);
				
				ncObject.setFieldUid(E.ENTITYFIELD.foreignentity, existingForeignEntity);
				ncObject.setFieldValue(E.ENTITYFIELD.foreignentityfield, existingFieldVO.getFieldValue(E.ENTITYFIELD.foreignentityfield));
			}
			break;
		default:
			break;
		}
		
		return true;
	}
	
}
