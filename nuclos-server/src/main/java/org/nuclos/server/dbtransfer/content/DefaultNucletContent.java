//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.List;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.NucletContentMap;


public class DefaultNucletContent extends AbstractNucletContent {

	public DefaultNucletContent(EntityMeta<UID> entity,	List<INucletContent> contentTypes, boolean ignoreReferenceToNuclet) {
		super(entity, contentTypes, ignoreReferenceToNuclet);
	}

	public DefaultNucletContent(EntityMeta<UID> entity,	List<INucletContent> contentTypes) {
		super(entity, contentTypes);
	}

	public DefaultNucletContent(FieldMeta<UID> fieldToParent, List<INucletContent> contentTypes, boolean ignoreReferenceToNuclet) {
		super(fieldToParent, contentTypes, ignoreReferenceToNuclet);
	}

	public DefaultNucletContent(FieldMeta<UID> fieldToParent, List<INucletContent> contentTypes) {
		super(fieldToParent, contentTypes);
	}

	@Override
	public boolean canDelete() {
		return true;
	}

	@Override
	public boolean canUpdate() {
		return true;
	}
	
	@Override
	public String getIdentifier(EntityObjectVO<UID> eo, NucletContentMap importContentMap) {
		Object ident = eo.getFieldValue(getIdentifierField());
		if (ident == null) {
			return "ID("+(eo.getPrimaryKey())+")";
		} else {
			return ident.toString();
		}
	}
	
	@Override
	public boolean hasNameIdentifier(EntityObjectVO<UID> eo) {
		Object ident = eo.getFieldValue(getIdentifierField());
		if (ident == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public UID getIdentifierField() {
		for (FieldMeta<?> fMeta : getEntity().getFields()) {
			if ("name".equals(fMeta.getFieldName())) {
				return fMeta.getUID();
			}
		}
		return new UID("name"); // does never exist, but prevents NullPointer from maps
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		return ncObject;
	}
	
	@Override
	public CollectableSearchCondition cachingCondition() {
		return null;
	}

}
