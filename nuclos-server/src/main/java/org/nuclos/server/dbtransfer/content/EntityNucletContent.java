//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class EntityNucletContent extends DefaultNucletContent {
	
	private static final Logger LOG = LoggerFactory.getLogger(EntityNucletContent.class);

	public EntityNucletContent(List<INucletContent> contentTypes) {
		super(E.ENTITY, contentTypes);
	}
	
	@Override
	public UID getIdentifierField() {
		return E.ENTITY.entity.getUID();
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		storeLocaleResources(ncObject, 
				E.ENTITY.localeresourcel, 
				E.ENTITY.localeresourcem, 
				E.ENTITY.localeresourced, 
				E.ENTITY.localeresourcetw, 
				E.ENTITY.localeresourcett);
		ncObject.removeFieldUid(E.ENTITY.mandatorLevel.getUID());
		ncObject.removeFieldValue(E.ENTITY.mandatorLevel.getUID());
		ncObject.removeFieldValue(E.ENTITY.mandatorUnique.getUID());
		return ncObject;
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean isNuclon, boolean testOnly) {
		restoreLocaleResources(ncObject);
		if (ncObject.isFlagNew()) {
			ncObject.setFieldValue(E.ENTITY.mandatorUnique, false); // mandatorUnique is mandatory
		} else {
			//EntityObjectVO<UID> existing = NucletDalProvider.getInstance().getEntityObjectProcessor(E.ENTITY).getByPrimaryKey(ncObject.getPrimaryKey());
			EntityObjectVO<UID> existing = getEOOriginal(E.ENTITY.getUID(), ncObject.getPrimaryKey());
			if (existing == null) {
				// wegen git merge problemen vermutlich doppelt vorhanden...
				LOG.info("Flag for update, but not found. entity=" + E.ENTITY.getEntityName() + ", pk=" + ncObject.getPrimaryKey());
				return false;
			}
			if (existing.getFieldUid(E.ENTITY.mandatorLevel) != null) {
				ncObject.setFieldUid(E.ENTITY.mandatorLevel, existing.getFieldUid(E.ENTITY.mandatorLevel));
			}
			ncObject.setFieldValue(E.ENTITY.mandatorUnique, existing.getFieldValue(E.ENTITY.mandatorUnique));
			if (existing.getFieldValue(E.ENTITY.dataLangRefPath) != null) {
				ncObject.setFieldValue(E.ENTITY.dataLangRefPath, existing.getFieldValue(E.ENTITY.dataLangRefPath));
			}
			if (existing.getFieldValue(E.ENTITY.dataLanguageDBTable) != null) {
				ncObject.setFieldValue(E.ENTITY.dataLanguageDBTable, existing.getFieldValue(E.ENTITY.dataLanguageDBTable));
			}
			if (existing.getFieldValue(E.ENTITY.lockMode) != null) {
				ncObject.setFieldValue(E.ENTITY.lockMode, existing.getFieldValue(E.ENTITY.lockMode));
			}
			if (existing.getFieldValue(E.ENTITY.ownerForeignEntityField) != null) {
				ncObject.setFieldValue(E.ENTITY.ownerForeignEntityField, existing.getFieldValue(E.ENTITY.ownerForeignEntityField));
			}
			if (existing.getFieldValue(E.ENTITY.unlockMode) != null) {
				ncObject.setFieldValue(E.ENTITY.unlockMode, existing.getFieldValue(E.ENTITY.unlockMode));
			}
		}
		return super.insertOrUpdateNcObject(result, ncObject, isNuclon, testOnly);
	}

	@Override
	public void deleteNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly) {
		for (Long goId : getReferencingEOids(E.GENERICOBJECT.module, ncObject.getPrimaryKey())) {
//			result.add(removeReferencingEOs(E.STATEHISTORY.genericObject, goId));
//			result.add(removeReferencingEOs(E.GENERICOBJECTLOGBOOK.genericObject, goId));
//			result.add(removeReferencingEOs(E.GENERICOBJECTGROUP.genericObject, goId));
//			result.add(removeReferencingEOs(E.TIMELIMITTASK.genericobject, goId));
//			result.add(removeReferencingEOs(E.GENERICOBJECTRELATION.source, goId));
//			result.add(removeReferencingEOs(E.GENERICOBJECTRELATION.source, goId));
//			result.add(removeReferencingEOs(E.GENERALSEARCHCOMMENT.genericObject, goId));
//			result.add(removeReferencingEOs(E.GENERALSEARCHDOCUMENT.genericObject, goId));
			try {
				NucletDalProvider.getInstance().getEntityObjectProcessor(E.GENERICOBJECT).delete(new Delete(goId));
			}
			catch (DbException e) {
				result.addRuntimeException(e);
			}
		}
		super.deleteNcObject(result, ncObject, testOnly);
	}

	private List<Long> getReferencingEOids(FieldMeta<UID> referencingField, UID id) {
		return NucletDalProvider.getInstance().<Long>getEntityObjectProcessor(referencingField.getEntity()).getIdsBySearchExpression(
			new CollectableSearchExpression(SearchConditionUtils.newUidComparison( 
				referencingField, ComparisonOperator.EQUAL, id)));
	}
	
	private List<Long> getReferencingEOids(FieldMeta<Long> referencingField, Long id) {
		return NucletDalProvider.getInstance().<Long>getEntityObjectProcessor(referencingField.getEntity()).getIdsBySearchExpression(
			new CollectableSearchExpression(SearchConditionUtils.newIdComparison( 
				referencingField, ComparisonOperator.EQUAL, id)));
	}
	
}
