//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.Date;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.job.ejb3.JobControlFacadeLocal;
import org.springframework.security.core.context.SecurityContextHolder;


public class JobControllerNucletContent extends DefaultNucletContent {

	public JobControllerNucletContent(List<INucletContent> contentTypes) {
		super(E.JOBCONTROLLER, contentTypes);
	}

	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		removeStatusFields(ncObject);
		ncObject.setVersion(1);
		return super.readNc(ncObject);
	}

	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean isNuclon, boolean testOnly) {
		if (ncObject.isFlagUpdated()) {
			EntityObjectVO<UID> existing = NucletDalProvider.getInstance().getEntityObjectProcessor(E.JOBCONTROLLER).getByPrimaryKey(ncObject.getPrimaryKey());
			Date sysdate = new Date();
			existing.setFieldValue(E.JOBCONTROLLER.name, ncObject.getFieldValue(E.JOBCONTROLLER.name));
			existing.setFieldValue(E.JOBCONTROLLER.type, ncObject.getFieldValue(E.JOBCONTROLLER.type));
			existing.setFieldValue(E.JOBCONTROLLER.description, ncObject.getFieldValue(E.JOBCONTROLLER.description));
			existing.setFieldUid(E.JOBCONTROLLER.nuclet, ncObject.getFieldUid(E.JOBCONTROLLER.nuclet));
			existing.setVersion(ncObject.getVersion());
			existing.setChangedAt(InternalTimestamp.toInternalTimestamp(sysdate));
			existing.setChangedBy(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
			ncObject = existing;
			ncObject.flagUpdate();
		} else {
			removeStatusFields(ncObject);
		}

		return super.insertOrUpdateNcObject(result, ncObject, isNuclon, testOnly);
	}
	
	private void removeStatusFields(EntityObjectVO<UID> ncObject) {
		ncObject.setFieldValue(E.JOBCONTROLLER.laststate, JobControlFacadeLocal.NOT_ACTIVATED);
		ncObject.setFieldValue(E.JOBCONTROLLER.running, false);
		ncObject.setFieldValue(E.JOBCONTROLLER.lastfiretime, null);
		ncObject.setFieldValue(E.JOBCONTROLLER.result, null);
		ncObject.setFieldValue(E.JOBCONTROLLER.resultdetails, null);
		ncObject.setFieldValue(E.JOBCONTROLLER.nextfiretime, null);
	}

}
