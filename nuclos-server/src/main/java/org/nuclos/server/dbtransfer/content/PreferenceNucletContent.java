//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.PreferenceVO;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.nuclet.IPreferenceProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dbtransfer.TransferUtils;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentPreferenceTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentPreferenceTreeNode.PreferenceEntityNameResolver;

public class PreferenceNucletContent extends DefaultNucletContent {

	public PreferenceNucletContent(List<INucletContent> contentTypes) {
		super(E.PREFERENCE, contentTypes);
	}
	
	@Override
	public CollectableSearchCondition cachingCondition() {
		return cachingConditionStatic();
	}
	
	public static CollectableSearchCondition cachingConditionStatic() {
		return SearchConditionUtils.newIsNullCondition(E.PREFERENCE.user);
	}
	
	@Override
	public String getIdentifier(EntityObjectVO<UID> eo, final NucletContentMap importContentMap) {
		return NucletContentPreferenceTreeNode.getName(eo, new PreferenceEntityNameResolver() {
			@Override
			public String getEntityName(UID entityUID) {
				if (entityUID != null) {
					// 1. search in importContentMap
					if (importContentMap != null) {
						TransferEO entityObjectVO = TransferUtils.getEntityObjectVO(importContentMap.getValues(E.ENTITY), entityUID);
						if (entityObjectVO != null) {
							return entityObjectVO.eo.getFieldValue(E.ENTITY.entity);
						}
					}
					// 2. search in local MetaProvider
					try {
						return MetaProvider.getInstance().getEntity(entityUID).getEntityName();
					} catch (Exception ex) {
						// ignore FatalException '...does not exist'
					}
				}
				return "unknown";
			}
		});
	}
	
	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean isNuclon, boolean testOnly) {
		boolean updated = super.insertOrUpdateNcObject(result, ncObject, isNuclon, testOnly);
		if (!testOnly && updated) {
			IPreferenceProcessor prefsProc = SpringApplicationContextHolder.getBean(IPreferenceProcessor.class);
			PreferenceVO sharedPreference = prefsProc.getByPrimaryKey(ncObject.getPrimaryKey());
			if (sharedPreference != null) { // NUCLOS-6422: Renaming of preferences is not a good idea in combination with GIT
				prefsProc.batchResetUserCustomizations(sharedPreference);
			}
		}
		return updated;
	}
	
	@Override
	public void deleteNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly) {
		if (ncObject.getFieldUid(E.PREFERENCE.user) == null) { // is assignable workspace
			// Remove user customizations.
			SpringDataBaseHelper.getInstance().getDbAccess().execute(DbStatementUtils.deleteFrom(E.PREFERENCE, E.PREFERENCE.sharedPreference, ncObject.getPrimaryKey()));
		}
		super.deleteNcObject(result, ncObject, testOnly);
	}
	
}
