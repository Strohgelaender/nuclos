//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.server.report.ejb3.ReportCompiler;

public class ReportOutputNucletContent extends DefaultNucletContent implements IExternalizeBytes {
	
	public ReportOutputNucletContent(List<INucletContent> contentTypes) {
		super(E.REPORTOUTPUT.parent, contentTypes);
	}
	
	@Override
	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject) {
		ncObject.removeFieldValue(E.REPORTOUTPUT.reportCLS.getUID()); // no compiled reports any more!
		return ncObject;
	}

	@Override
	public Map<FieldMeta<?>, Pair<String, byte[]>> externalize(EntityObjectVO<UID> ncObject) {
		String filename = ncObject.getFieldValue(E.REPORTOUTPUT.sourceFile);
		if (filename != null) {
			ByteArrayCarrier bytes = ncObject.getFieldValue(E.REPORTOUTPUT.sourceFileContent);
			ncObject.removeFieldValue(E.REPORTOUTPUT.sourceFileContent.getUID());
			ncObject.removeFieldValue(E.REPORTOUTPUT.reportCLS.getUID());
			
			int dot = filename.lastIndexOf(".");
			String fileType = filename;
			if (dot > 0) {
				fileType = filename.substring(dot+1);
			}
			
			if (bytes != null) {
				Map<FieldMeta<?>, Pair<String, byte[]>> result = new HashMap<FieldMeta<?>, Pair<String, byte[]>>();
				result.put(E.REPORTOUTPUT.sourceFileContent, new Pair<String, byte[]>(fileType.toLowerCase(), bytes.getData()));
				return result;
			}
		}
		return null;
	}

	@Override
	public void importBytes(EntityObjectVO<UID> ncObject, FieldMeta<?> efMeta, byte[] bytes) {
		if (E.REPORTOUTPUT.sourceFileContent.equals(efMeta)) {
			ByteArrayCarrier bac = new ByteArrayCarrier(bytes);
			ncObject.setFieldValue(E.REPORTOUTPUT.sourceFileContent, bac);
			if ("PDF".equals(ncObject.getFieldValue(E.REPORTOUTPUT.format))) {
				ncObject.setFieldValue(E.REPORTOUTPUT.reportCLS, ReportCompiler.compileReport(bac));
			}
		}
	}
	
}
