//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.serializable;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common2.IOUtils;
import org.nuclos.server.statemodel.valueobject.NoteLayout;
import org.nuclos.server.statemodel.valueobject.StateLayout;
import org.nuclos.server.statemodel.valueobject.StateModelLayout;
import org.nuclos.server.statemodel.valueobject.TransitionLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


public class StateModelContentSerializable {

	private static final Logger LOG = LoggerFactory.getLogger(StateModelContentSerializable.class);
	
	private static final String NODE_STATE = "state";
	private static final String NODE_TRANSITION = "transition";
	private static final String NODE_NOTE = "note";
	
	private static final String ATTR_HEIGHT = "height";
	private static final String ATTR_WIDTH = "width";
	private static final String ATTR_X = "x";
	private static final String ATTR_Y = "y";
	private static final String ATTR_CONNECTION_START = "connectionStart";
	private static final String ATTR_CONNECTION_END = "connectionEnd";
	private static final String ATTR_TEXT = "text";
	
	public static void marshal(TransferEO teo, HierarchicalStreamWriter writer,	MarshallingContext context) {
		StateModelLayout layout = null;
		try {
			byte b[] = teo.eo.getFieldValue(E.STATEMODEL.layout); 
			Object o = IOUtils.fromByteArray(b);
			if (o != null && o instanceof StateModelLayout) {
				layout = (StateModelLayout) o;
				teo.eo.removeFieldValue(E.STATEMODEL.layout.getUID());
			}
		} catch (Exception ex) {
			LOG.error("Unable to marshal status model layout", ex);
		}
		
		DefaultContentSerializable.marshal(teo, writer, context);
		
		if (layout != null) {
			writer.startNode("layout");
			for (UID stateUID : RigidUtils.sorted(layout.getStates().keySet(), new UID.Comparator())) {
				StateLayout statelayout = layout.getStates().get(stateUID);
				if (statelayout != null) {
					writer.startNode(NODE_STATE);
					writer.addAttribute("uid", stateUID.getStringifiedDefinitionWithEntity(E.STATE.getUID()));
					writer.addAttribute(ATTR_HEIGHT, Double.toString(statelayout.getHeight()));
					writer.addAttribute(ATTR_WIDTH, Double.toString(statelayout.getWidth()));
					writer.addAttribute(ATTR_X, Double.toString(statelayout.getX()));
					writer.addAttribute(ATTR_Y, Double.toString(statelayout.getY()));
					writer.endNode();
				}
			}
			for (UID transitionUID : RigidUtils.sorted(layout.getTransitions().keySet(), new UID.Comparator())) {
				TransitionLayout transitionlayout = layout.getTransitions().get(transitionUID);
				if (transitionlayout != null) {
					writer.startNode(NODE_TRANSITION);
					writer.addAttribute("uid", transitionUID.getStringifiedDefinitionWithEntity(E.STATETRANSITION.getUID()));
					writer.addAttribute(ATTR_CONNECTION_START, Integer.toString(transitionlayout.getConnectionStart()));
					writer.addAttribute(ATTR_CONNECTION_END, Integer.toString(transitionlayout.getConnectionEnd()));
					writer.endNode();
				}
			}
			for (NoteLayout notelayout : RigidUtils.sorted(layout.getNotes(), new Comparator<NoteLayout>() {
				@Override
				public int compare(NoteLayout o1, NoteLayout o2) {
					if (RigidUtils.equal(o1.getText(), o2.getText())) {
						if (o1.getX() != o2.getX()) {
							return Double.compare(o1.getX(), o2.getX());
						}
						if (o1.getY() != o2.getY()) {
							return Double.compare(o1.getY(), o2.getY());
						}
						return 0;
					} else {
						if (o1.getText() != null) {
							if (o2.getText() != null) {
								return o1.getText().compareTo(o2.getText());
							} else {
								return 1;
							}
						} else {
							return -1;
						}
					}
				}
			})) {
				writer.startNode(NODE_NOTE);
				writer.addAttribute(ATTR_HEIGHT, Double.toString(notelayout.getHeight()));
				writer.addAttribute(ATTR_WIDTH, Double.toString(notelayout.getWidth()));
				writer.addAttribute(ATTR_X, Double.toString(notelayout.getX()));
				writer.addAttribute(ATTR_Y, Double.toString(notelayout.getY()));
				writer.addAttribute(ATTR_TEXT, notelayout.getText());
				writer.endNode();
			}
			writer.endNode();
		}
	}

	public static void unmarshal(
			final TransferEO teo, final HierarchicalStreamReader reader, UnmarshallingContext context, Map<String, Runnable> childRunners, 
			final NucletContentMap importContentMap, Collection<Runnable> afterDeserialize) {
		
		final StateModelLayout layout = new StateModelLayout();
		final Map<UID, StateLayout> statelayouts = new HashMap<UID, StateLayout>();
		final Map<UID, TransitionLayout> transitionlayouts = new HashMap<UID, TransitionLayout>();
		
		childRunners.put("layout", new Runnable() {
			@Override
			public void run() {
				while (reader.hasMoreChildren()) {
					reader.moveDown();
					try {
						String field = reader.getNodeName();
						if (NODE_STATE.equals(field)) {
							statelayouts.put(UID.parseUID(reader.getAttribute("uid")), new StateLayout(
									Double.parseDouble(reader.getAttribute(ATTR_X)),
									Double.parseDouble(reader.getAttribute(ATTR_Y)),
									Double.parseDouble(reader.getAttribute(ATTR_WIDTH)),
									Double.parseDouble(reader.getAttribute(ATTR_HEIGHT))));
						} else if (NODE_TRANSITION.equals(field)) {
							UID transitionUID = UID.parseUID(reader.getAttribute("uid"));
							transitionlayouts.put(transitionUID, new TransitionLayout(
									transitionUID,
									Integer.parseInt(reader.getAttribute(ATTR_CONNECTION_START)),
									Integer.parseInt(reader.getAttribute(ATTR_CONNECTION_END))));
						} else if (NODE_NOTE.equals(field)) {
							layout.getNotes().add(new NoteLayout(
									reader.getAttribute(ATTR_TEXT),
									Double.parseDouble(reader.getAttribute(ATTR_X)),
									Double.parseDouble(reader.getAttribute(ATTR_Y)),
									Double.parseDouble(reader.getAttribute(ATTR_WIDTH)),
									Double.parseDouble(reader.getAttribute(ATTR_HEIGHT))));
						}
					} catch (Exception ex) {
						LOG.error("Unable to unmarshal status model layout", ex);
					} finally {
						reader.moveUp();
					}
				}
			}
		});
		DefaultContentSerializable.unmarshal(teo, reader, context, childRunners);
		
		for (Entry<UID, StateLayout> statelayout : statelayouts.entrySet()) {
			layout.insertStateLayout(statelayout.getKey(), statelayout.getValue());
		}
		for (Entry<UID, TransitionLayout> transitionlayout : transitionlayouts.entrySet()) {
			layout.insertTransitionLayout(transitionlayout.getKey(), transitionlayout.getValue());
		}
		try {
			teo.eo.setFieldValue(E.STATEMODEL.layout, IOUtils.toByteArray(layout));
		} catch (IOException ex) {
			LOG.error("Unable to unmarshal status model layout", ex);
		}
	}

}
