//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbStatementUtils;


public class XmlImportNucletContent extends DefaultNucletContent {

	public XmlImportNucletContent(List<INucletContent> contentTypes) {
		super(E.XML_IMPORT, contentTypes);
	}

	@Override
	public void deleteNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly) {
		SpringDataBaseHelper.getInstance().getDbAccess().execute(DbStatementUtils.deleteFrom(E.IMPORTUSAGE, E.IMPORTUSAGE.xmlimportfield, ncObject.getPrimaryKey()));
		super.deleteNcObject(result, ncObject, testOnly);
	}

}
