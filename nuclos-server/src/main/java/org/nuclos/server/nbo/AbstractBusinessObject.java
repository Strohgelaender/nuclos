package org.nuclos.server.nbo;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Dependent;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.service.BusinessObjectService;
import org.nuclos.api.service.QueryService;
import org.nuclos.api.service.StatemodelService;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DataLanguageMap;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeBean;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;
import org.nuclos.server.i18n.language.data.DataLanguageVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public abstract class AbstractBusinessObject<PK> implements BusinessObject<PK> {
	
	private Object eo; 
	private Set<Dependent<?>> alreadyLoadedDependents;
	private transient Object dlCache;
	private List languages;
	private Map<UID, Object> references;
	private Object layoutCache;

	private static final Flag[] DEPENENT_FLAGS = new Flag[]{Flag.NONE, Flag.INSERT, Flag.UPDATE};
	
	private static final Dependent<NuclosFile> DEPENDENT_ATTACHEMENT = new Dependent<NuclosFile>("E.GENERALSEARCHDOCUMENT", "null", E.GENERALSEARCHDOCUMENT.getEntityName(), E.GENERALSEARCHDOCUMENT.getUID().getString(),
			E.GENERALSEARCHDOCUMENT.genericObject.getFieldName(), E.GENERALSEARCHDOCUMENT.genericObject.getUID().getString(), NuclosFile.class);
	
	public AbstractBusinessObject(String sEntityUID) {
		UID entityUID = new UID(sEntityUID);
		this.eo = new EntityObjectVO(entityUID);
		((EntityObjectVO<PK>) this.eo).setFieldValue(SF.LOGICALDELETED.getUID(entityUID), Boolean.FALSE);
		alreadyLoadedDependents = new HashSet<Dependent<?>>();
		((EntityObjectVO<PK>) this.eo).flagNew();
		references = new HashMap<UID, Object>();
	}

	void setEntityObjectVO(Object eo) {
		this.eo = eo;
	}

	Object getEntityObjectVO() {
		return eo;
	}
	
	protected void setId(PK id) {
		((EntityObjectVO<PK>) this.eo).setPrimaryKey(id);
	}
	
	@Override
	public PK getId() {
		return ((EntityObjectVO<PK>) this.eo).getPrimaryKey();
	}

	@Override
	public java.lang.Integer getVersion() {
		return ((EntityObjectVO<PK>) this.eo).getVersion();
	}
	
	@Override
	public boolean isInsert() {
		return ((EntityObjectVO<PK>) this.eo).isFlagNew();
	}
	
	@Override
	public boolean isUpdate() {
		return ((EntityObjectVO<PK>) this.eo).isFlagUpdated();
	}
	
	@Override
	public boolean isDelete() {
		return ((EntityObjectVO<PK>) this.eo).isFlagRemoved();
	}
	
	@Override
	public String getEntity() {
		return MetaProvider.getInstance().getEntity(((EntityObjectVO<PK>) this.eo).getDalEntity()).getEntityName();
	}
	
	public boolean isUnchanged() {
		return ((EntityObjectVO<PK>) this.eo).isFlagUnchanged();
	}
	
	
	@Override
	public org.nuclos.api.UID getEntityUid() {
		return ((EntityObjectVO<PK>) this.eo).getDalEntity();
	}
	
	protected <T> T getField(String field, Class<T> t) {
		final UID fieldUID = new org.nuclos.common.UID(field);
		if (!MetaProvider.getInstance().checkEntityField(fieldUID)) {
			// not integrated optional point
			return null;
		}
		FieldMeta meta=MetaProvider.getInstance().getEntityField(fieldUID);
		if(meta.getJavaClass().equals(NuclosPassword.class))
		{
			NuclosPassword pw=((NuclosPassword)((EntityObjectVO<PK>) this.eo).getFieldValue(new org.nuclos.common.UID(field), meta.getJavaClass()));
			if(pw!=null)
				return (T) pw.getValue();
			else 
				return null;					
		}
		else
		{
			return getFieldValue(field, t, meta);
		}
	}
	
	protected <T> T getField(String field, NuclosLocale locale, Class<T> t) {
		UID language = new org.nuclos.common.UID (locale.toString());
		UID fieldUID = new org.nuclos.common.UID(field);
		
		this.checkAndLoadLanguages(language, false);
		
		EntityObjectVO<PK> pEO = (EntityObjectVO<PK>) this.eo;
		
		if (pEO.getDataLanguageMap() != null && 
			pEO.getDataLanguageMap().getDataLanguage(language) != null) {
			
			Object fieldValue = pEO.getDataLanguageMap().
					getDataLanguage(language).getFieldValue(new UID("DL_" + fieldUID.toString()), t);
			return fieldValue != null ? (T) fieldValue : null;
		} else {
			return getFieldValue(field, t, null);			
		}
	}
	
	private <T> T getFieldValue(String field, Class<T> t, Object objectFieldMeta) {
		UID fieldUID = new org.nuclos.common.UID(field);
		EntityObjectVO<PK> pEO = (EntityObjectVO<PK>) this.eo;
		
		T result = pEO.getFieldValue(fieldUID, t);
		if (result == null) {
			if (objectFieldMeta == null && !MetaProvider.getInstance().checkEntityField(fieldUID)) {
				// not integrated optional point
				return result;
			}
			FieldMeta<?> fMeta = objectFieldMeta != null ? (FieldMeta<?>)objectFieldMeta : MetaProvider.getInstance().getEntityField(fieldUID);
			if (fMeta.getForeignEntity() != null) {
				Long id = pEO.getFieldId(fieldUID);
				UID uid = pEO.getFieldUid(fieldUID);
				if (id != null || uid != null) {
					// try to load stringified value...
					final IEntityObjectProcessor<Object> eop = NucletDalProvider.getInstance().getEntityObjectProcessor(fMeta.getForeignEntity());
					final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
					try {
						eop.setIgnoreRecordGrantsAndOthers(true);
						EntityObjectVO<?> refEo = eop.getByPrimaryKey(id!=null?id:uid);
						if (refEo != null) { // missing db constraint?
							result = (T) RefValueExtractor.get(refEo, fMeta.getForeignEntityField(), null, MetaProvider.getInstance());
							if (fMeta.isFileDataType()) {
								result = (T) new GenericObjectDocumentFile((String) result, uid);
							}
						}
						pEO.setFieldValue(fMeta.getUID(), result);
					} finally {
						eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
					}
				}
			}
		}
		return result;
	}
	
	protected Long getFieldId(String field) {
		return ((EntityObjectVO<PK>) this.eo).getFieldId(new org.nuclos.common.UID(field));
	} 
	
	protected <T extends org.nuclos.api.common.NuclosFileBase> T getNuclosFile(Class<T> fileClass, String fieldUID) {
		if (fileClass.isAssignableFrom(org.nuclos.api.common.NuclosFile.class)) {
			Object godoc = getField(fieldUID, Object.class);
			if (godoc == null) {
				return null;
			}
			org.nuclos.common.NuclosFile result = ((org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile) godoc).getNuclosFile();
			UID fileId = getFieldUid(fieldUID);
			if (fileId != null) {
				result.setId(fileId);
			}
			return (T) result;
		} else if (fileClass.isAssignableFrom(org.nuclos.api.common.NuclosFileLink.class)) {
			UID fileId = getFieldUid(fieldUID);
			if (fileId == null) {
				return null;
			}
			return (T) new org.nuclos.common.NuclosFileLink(fileId);
		} else {
			throw new org.apache.commons.lang.NotImplementedException();
		}
	}
	
	protected <T extends org.nuclos.api.common.NuclosFileBase> void setNuclosFile(T file, String fieldUID) {
		if (file instanceof org.nuclos.common.NuclosFile) {
			setField(fieldUID, createGenericObjectDocumentFile((org.nuclos.common.NuclosFile) file));
			// Setting of ID removes a value! "setField" with GenericObjectDocumentFile already sets the id
			// setFieldId(fieldUID, ((org.nuclos.common.NuclosFile) file).getId());
		} else if (file instanceof org.nuclos.common.NuclosFileLink) {
			org.nuclos.common.NuclosFileLink fileLink = (org.nuclos.common.NuclosFileLink) file;
			// Setting of ID removes a value! 			
			// setField(fieldUID, null);
			setFieldId(fieldUID, fileLink.getFileId());
		} else if (file == null) {
			// Setting of ID removes a value! 		
			// setField(fieldUID, null);
			setFieldId(fieldUID, (UID)null);
		} else {
			throw new org.apache.commons.lang.NotImplementedException();
		}
	}
	
	protected <T extends AbstractBusinessObject> T getReferencedBO(Class<T> type, Object pk, String referencingField, String foreignEntity) {
		
		Constructor ctor;
		T retVal = null;
		
		if (pk == null)
			return null;

		final UID entityUID = new UID(foreignEntity);
		if (!MetaProvider.getInstance().checkEntity(entityUID)) {
			// not integrated optional point
			return null;
		}

		if (!references.containsKey(new UID(referencingField))) {
			IEntityObjectProcessor<Object> processor = null;
			try {
				processor = NucletDalProvider.getInstance().getEntityObjectProcessor(entityUID);
				processor.setThinReadEnabled(true);

				EntityObjectVO<PK> eoVO = (EntityObjectVO<PK>) processor.getByPrimaryKey(pk);
				references.put(new UID(referencingField), eoVO);
			} finally {
				if (processor != null) {
					processor.setThinReadEnabled(false);
				}
			}
		}
		
		try {
			retVal = (T) type.newInstance();
			retVal.setEntityObjectVO(references.get(new UID(referencingField)));
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
		
		return retVal;
	}

	protected UID getFieldUid(String field) {
		return ((EntityObjectVO<PK>) this.eo).getFieldUid(new org.nuclos.common.UID(field));
	} 
	
	
	protected <T> void setField(String field, T value) {
		UID uid = new org.nuclos.common.UID(field);

		if (!MetaProvider.getInstance().checkEntityField(uid)) {
			// not integrated optional point
			return;
		}
		
		FieldMeta<?> entityField = MetaProvider.getInstance().getEntityField(uid);
		if(entityField.getJavaClass().equals(NuclosPassword.class))
		{
			NuclosPassword pw;
			try {
				pw = new NuclosPassword((String)value);
				((EntityObjectVO<PK>) this.eo).setFieldValue( new org.nuclos.common.UID(field), pw);
			} catch (Exception e) {
				throw new NuclosFatalException(e);
			}
		} else if ((entityField.getJavaClass() == GenericObjectDocumentFile.class || entityField.getDataType().equals(GenericObjectDocumentFile.class.getCanonicalName()))
				&& value != null) {
			GenericObjectDocumentFile godFile = (GenericObjectDocumentFile)value;
			UID existingDocumentFileUID = ((EntityObjectVO<PK>) this.eo).getFieldUid(uid);
			if (existingDocumentFileUID != null) {
				// if document is changed, documentFileUID has to stay the same
				godFile.setDocumentFilePk(existingDocumentFileUID);
			}
			((EntityObjectVO<PK>) this.eo).setFieldValue( uid, godFile);
			UID documentFileUID = (UID)godFile.getDocumentFilePk();
			((EntityObjectVO<PK>) this.eo).setFieldUid(uid, documentFileUID);
		} else {		
			((EntityObjectVO<PK>) this.eo).setFieldValue( uid, value);

		}
		
		// Set entry to modified
		if (!((EntityObjectVO<PK>) this.eo).isFlagNew() && !((EntityObjectVO<PK>) this.eo).isFlagRemoved()) {
			((EntityObjectVO<PK>) this.eo).flagUpdate();
		}
		
		
		if (entityField.isLocalized() && !entityField.isCalculated()) {
			setField(field, NuclosLocale.valueOf(this.getNuclosPrimaryDataLanguage().toString().toUpperCase()), (String) value);
		}
	}
	
	protected void setField(String field, NuclosLocale locale, String value) {
		UID selectedLanguage = new org.nuclos.common.UID (locale.toString());
		UID fieldUID = new org.nuclos.common.UID(field);

		if (!MetaProvider.getInstance().checkEntityField(fieldUID)) {
			// not integrated optional point
			return;
		}
		
		this.checkAndLoadLanguages(selectedLanguage, true);
		
		EntityObjectVO<PK> pEO = (EntityObjectVO<PK>) this.eo;
		
		if (this.getDataLanguageUIDs().contains(selectedLanguage)) {
			if (pEO.getDataLanguageMap() != null) {
				
				if (pEO.getDataLanguageMap().getDataLanguage(selectedLanguage) == null) {				
					EntityMeta<PK> metaData = (EntityMeta)getMetaData();				
					DataLanguageLocalizedEntityEntry newDLEntry = new DataLanguageLocalizedEntityEntry(
							new EntityMetaVO<PK>(metaData, true), (Long)pEO.getPrimaryKey(), selectedLanguage, new HashMap<UID, Object>());
					pEO.getDataLanguageMap().setDataLanguage(selectedLanguage, newDLEntry);
				} 
				
				List<UID> lstAssignedLangs = CollectionUtils.transform(
						this.getDataLanguageUIDs(), new Transformer<org.nuclos.api.UID, UID>() {
							@Override
							public UID transform(org.nuclos.api.UID i) {
								return (UID) i;
							}
						});
				
				DataLanguageServerUtils.setLocalizedValueAndValidateMap(
						pEO.getDataLanguageMap(), (Long) pEO.getPrimaryKey(), (EntityMeta)getMetaData(), selectedLanguage, 
						(UID) this.getNuclosPrimaryDataLanguage(), 						
						(UID)this.getCurrentUserDataLanguage(), fieldUID, lstAssignedLangs, value);
				
				if (selectedLanguage.equals(this.getNuclosPrimaryDataLanguage())) {
					((EntityObjectVO<PK>) this.eo).setFieldValue(fieldUID, value);
				}
				
				// Set entry to modified
				if (!((EntityObjectVO<PK>) this.eo).isFlagNew() && !((EntityObjectVO<PK>) this.eo).isFlagRemoved()) {
					((EntityObjectVO<PK>) this.eo).flagUpdate();
				}
			}			
		}
		
	}
	
	protected void setFieldId(String field, Long refId) {
		UID fieldUID = new org.nuclos.common.UID(field);
		((EntityObjectVO<PK>) this.eo).setFieldId(fieldUID, refId);
		((EntityObjectVO<PK>) this.eo).removeFieldValue(fieldUID);
		
		if (references.containsKey(fieldUID)) {
			references.remove(fieldUID);
		}
		
		if (!((EntityObjectVO<PK>) this.eo).isFlagNew() && !((EntityObjectVO<PK>) this.eo).isFlagRemoved()) {
			((EntityObjectVO<PK>) this.eo).flagUpdate();
		}
	}

	protected void setFieldId(String field, String refId) {
		this.setFieldId(field, new org.nuclos.common.UID(refId));
	}
	
	protected void setFieldId(String field, org.nuclos.api.UID refId) {
		UID fieldUID = new org.nuclos.common.UID(field);
		((EntityObjectVO<PK>) this.eo).setFieldUid(fieldUID, (org.nuclos.common.UID) refId);
		((EntityObjectVO<PK>) this.eo).removeFieldValue(fieldUID);
		
		if (references.containsKey(fieldUID)) {
			references.remove(fieldUID);
		}
		
		if (!((EntityObjectVO<PK>) this.eo).isFlagNew() && !((EntityObjectVO<PK>) this.eo).isFlagRemoved()) {
			((EntityObjectVO<PK>) this.eo).flagUpdate();
		}
	}
	
	protected <T> List<T> getDependents(Dependent<T> depAttr, Flag... flags)
	{
		List<T> retVal = new ArrayList<T>();
		
		// If there are no states mentioned return all except removed entries 
		if (flags == null || flags.length == 0) {
			flags = DEPENENT_FLAGS;
		}
		List<Flag> lstFlags = Arrays.asList(flags);

		final UID fieldUID = new UID(depAttr.getForeignAttributeUid());
		if (!MetaProvider.getInstance().checkEntityField(fieldUID)) {
			// not integrated optional point
			return retVal;
		}

		IDependentKey dependentKey = DependentDataMap.createDependentKey(fieldUID);
		
		// To fill up the EntityObjectVO with all relevant dependents we must check which
		// dependents the client already has... irrespective of the given flag(s)
		if (((EntityObjectVO<PK>) this.eo).getPrimaryKey() != null && lstFlags.contains(Flag.NONE) && !alreadyLoadedDependents.contains(depAttr)) {
			List<Object> lstEosByClientUnsorted = new ArrayList<Object>();
			if(((EntityObjectVO<PK>) this.eo).getDependents().getData(dependentKey) != null) {
				for (EntityObjectVO<?> curEO : ((EntityObjectVO<PK>) this.eo).getDependents().getData(dependentKey)) {
					lstEosByClientUnsorted.add(curEO.getPrimaryKey());
				}
			}
			List<EntityObjectVO<?>> depsFromDb = (List<EntityObjectVO<?>>) loadDependantsFromDB(new UID(depAttr.getEntityUid()), new UID(depAttr.getForeignAttributeUid()), ((EntityObjectVO<PK>) this.eo).getPrimaryKey());
			for (EntityObjectVO<?> cureo : depsFromDb) {
				if (!lstEosByClientUnsorted.contains(cureo.getPrimaryKey()))
					((EntityObjectVO<PK>) this.eo).getDependents().addData(dependentKey, cureo);
					
			}			
			alreadyLoadedDependents.add(depAttr);
		}

		// After ensuring that all(!) dependents including DB-entries are stored in the EntityObjectVO
		// we filter afterwards basing on the given flags
		for (EntityObjectVO<?> curEO : ((EntityObjectVO<PK>) this.eo).getDependents().getData(dependentKey, flags)) {
			try {
				if (depAttr.equals(DEPENDENT_ATTACHEMENT)) {
					org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile godf = 
							(org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile) curEO.getFieldValue(E.GENERALSEARCHDOCUMENT.documentfile.getUID());
					retVal.add((T) new org.nuclos.common.NuclosFile((Long)curEO.getPrimaryKey(), godf.getFilename(), godf.getContents()));
				} else {
					final T depBO = depAttr.getType().getDeclaredConstructor().newInstance();
					((AbstractBusinessObject) depBO).setEntityObjectVO(curEO);
					retVal.add(depBO);
				}
			} catch (Exception e) {
				throw new NuclosFatalException(e);
			}
		}

		return retVal;
	}
	
	protected <DPK, T extends BusinessObject<DPK>> void insertDependent(Dependent<T> depAttr, T dep) {

		if (dep == null || !(dep instanceof AbstractBusinessObject)) {
			throw new IllegalArgumentException(dep.getClass().getName());
		}
		AbstractBusinessObject newDep = (AbstractBusinessObject)dep;
		
		// Dependent must be new or insert oeprations
		if (!((EntityObjectVO<?>)newDep.eo).isFlagNew()) {
			throw new IllegalArgumentException("Only new dependents are insertable");
		}

		final UID fieldUID = new UID(depAttr.getForeignAttributeUid());
		if (!MetaProvider.getInstance().checkEntityField(fieldUID)) {
			// not integrated optional point
			return;
		}

		// Dependent-BO must have a reference to the current parent BO
		if (!((EntityObjectVO<?>)newDep.eo).getFieldIds().containsKey(fieldUID) && ((EntityObjectVO<PK>) this.eo).getPrimaryKey() != null) {
			final EntityMeta<Object> entityMeta = MetaProvider.getInstance().getEntity((UID) getEntityUid());
			if (entityMeta.isUidEntity()) {
				((EntityObjectVO<?>) newDep.eo).setFieldUid(fieldUID, (UID) ((EntityObjectVO<PK>) this.eo).getPrimaryKey());
			} else {
				((EntityObjectVO<?>) newDep.eo).setFieldId(fieldUID, (Long) ((EntityObjectVO<PK>) this.eo).getPrimaryKey());
			}
		}
		
		// If there are autonumber fields, must fill them manually
		fillAutonumberIfExisting(depAttr, newDep.eo);
		
		IDependentKey dependentKey = DependentDataMap.createDependentKey( new org.nuclos.common.UID(depAttr.getForeignAttributeUid()));
		((EntityObjectVO<PK>) this.eo).getDependents().addData(dependentKey, ((EntityObjectVO<?>)newDep.eo));
		
		
		if (!((EntityObjectVO<PK>) this.eo).isFlagNew() && !((EntityObjectVO<PK>) this.eo).isFlagUpdated() && !((EntityObjectVO<PK>) this.eo).isFlagRemoved()) {
			((EntityObjectVO<PK>) this.eo).flagUpdate();
		}
	}
	
	private void fillAutonumberIfExisting(Dependent depAttr, Object obj) {
		IDependentKey dependentKey = DependentDataMap.createDependentKey( new org.nuclos.common.UID(depAttr.getForeignAttributeUid()));
		EntityObjectVO<?> eoDep = (EntityObjectVO<?>) obj;
		Map<UID, FieldMeta<?>> allFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(eoDep.getDalEntity());
		
		for (UID field : allFields.keySet()) {
			FieldMeta<?> fieldMeta = allFields.get(field);
			if ("Autonummer".equals(fieldMeta.getDefaultComponentType()) && eoDep.getFieldValue(fieldMeta.getUID()) == null) {
				int numb = 0;
				for (EntityObjectVO eo : ((EntityObjectVO<PK>) this.eo).getDependents().getData(dependentKey)) {
					if (eo.getFieldValue(fieldMeta.getUID()) != null && !eo.isFlagRemoved())
						numb++;
				}
				eoDep.setFieldValue(fieldMeta.getUID(), Integer.valueOf(numb + 1));
			}
		}
		
	}

	protected <DPK, T extends BusinessObject<DPK>> void deleteDependent(Dependent<T> depAttr, T dep) {
		if (!(dep instanceof AbstractBusinessObject)) {
			throw new IllegalArgumentException(dep.getClass().getName());
		}

		final UID fieldUID = new UID(depAttr.getForeignAttributeUid());
		if (!MetaProvider.getInstance().checkEntityField(fieldUID)) {
			// not integrated optional point
			return;
		}

		IDependentKey dependentKey = DependentDataMap.createDependentKey(fieldUID);
		if (((EntityObjectVO<PK>) this.eo).getDependents().getData(dependentKey) == null) { 
			((EntityObjectVO<PK>) this.eo).getDependents().setData(dependentKey, new ArrayList<EntityObjectVO<T>>()); 
		}
		((EntityObjectVO<?>)((AbstractBusinessObject)dep).eo).flagRemove();
		boolean found = false;
		Iterator<EntityObjectVO<?>> iterator = ((EntityObjectVO<PK>) this.eo).getDependents().getData(dependentKey).iterator();
		while (iterator.hasNext()) {
			EntityObjectVO<?> pos = iterator.next();
			if (pos == ((AbstractBusinessObject)dep).eo ||
					(pos.getId() != null && pos.getId().equals(((EntityObjectVO<?>)((AbstractBusinessObject)dep).eo).getId()))) {
				pos.flagRemove();
				found = true;
				break;
			}
		}
		if (!found && ((EntityObjectVO<?>)((AbstractBusinessObject)dep).eo).getId() != null) {
			((EntityObjectVO<PK>) this.eo).getDependents().addData(dependentKey, ((EntityObjectVO<?>)((AbstractBusinessObject)dep).eo));
		}
		if (!((EntityObjectVO<PK>) this.eo).isFlagNew() && !((EntityObjectVO<PK>) this.eo).isFlagUpdated() && !((EntityObjectVO<PK>) this.eo).isFlagRemoved()) {
			((EntityObjectVO<PK>) this.eo).flagUpdate();
		}
	}

	private Object loadDependantsFromDB(UID foreignEntity, 
			UID foreignAttribute, Object id) {
		
		// Get all elements from the dependent entity using foreignKey and id of main entity as reference values
		CollectableSearchCondition cond = null;
		if (!MetaProvider.getInstance().getEntity(foreignEntity).getFields().contains(SF.LOGICALDELETED.getMetaData(foreignEntity)))
			cond = SearchConditionUtils.newKeyComparison(foreignAttribute, ComparisonOperator.EQUAL, id);
		else
			cond = SearchConditionUtils.and(
						SearchConditionUtils.newComparison(SF.LOGICALDELETED.getUID(foreignEntity), ComparisonOperator.EQUAL, false),
							SearchConditionUtils.newKeyComparison(foreignAttribute, ComparisonOperator.EQUAL, id));
		return NucletDalProvider.getInstance().getEntityObjectProcessor(foreignEntity).getBySearchExpression(new CollectableSearchExpression(cond));
	}
	
	private void checkAndLoadLanguages(UID language, boolean forUpdate) {
		EntityObjectVO<PK> pEO = (EntityObjectVO<PK>) this.eo;
		
		boolean doLoadLanguages = false;
		if (forUpdate) {
			// check, if all languages loaded
			doLoadLanguages = this.getDataLanguageUIDs().size() > pEO.getDataLanguageMap().getLanguageMap().size() && pEO.getPrimaryKey() != null;
		}
		else {
			// if current language is valid but not in LanguageMap
			doLoadLanguages = this.getDataLanguageUIDs().contains(language) && 
					pEO.getDataLanguageMap().getDataLanguage(language) == null &&
					pEO.getPrimaryKey() != null;
		}
		
		if (doLoadLanguages) {
			// reload entity with all languages
			EntityObjectVO<PK> pEOAllLanguages = (EntityObjectVO<PK>) NucletDalProvider.getInstance().getEntityObjectProcessor(pEO.getDalEntity()).getByPrimaryKey(pEO.getPrimaryKey());
			// set all languages that are missing
			for (org.nuclos.api.UID uidLang : this.getDataLanguageUIDs()) {
				UID uid = (UID)uidLang;
				if (pEO.getDataLanguageMap().getDataLanguage(uid) == null) {
					pEO.getDataLanguageMap().setDataLanguage(uid, pEOAllLanguages.getDataLanguageMap().getDataLanguage(uid));
				}
			}
		}
	}
	
	protected NuclosFile getNuclosFile(String fieldUID) {
		return getNuclosFile(NuclosFile.class, fieldUID);
	}
	
	public static Object createGenericObjectDocumentFile(NuclosFile file) {
		// https://support.novabit.de/browse/BMWFDM-801
		if (file == null) {
			return null;
		}
		org.nuclos.common.NuclosFile nuclosFile = (org.nuclos.common.NuclosFile) file;
		nuclosFile.setId(DocumentFileBase.newFileUID());
		return new org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile(nuclosFile);
	}
	
	protected void deleteGenericObjectDocumentAttachment(NuclosFile file) {
		Long id = ((org.nuclos.common.NuclosFile) file).getAttachmentId();
		
		if (id != null) {
			for (EntityObjectVO<Long> doc : ((EntityObjectVO<PK>) this.eo).getDependents().<Long>getDataPk(E.GENERALSEARCHDOCUMENT.genericObject)) {
				if (doc.getPrimaryKey().equals(id)) {
					doc.flagRemove();
				}
			}
		}
	}
	
	protected void insertGenericObjectDocumentAttachment(NuclosFile file, String comment) {
		MasterDataVO<Long> newAttachment = 
				new MasterDataVO<Long>(E.GENERALSEARCHDOCUMENT, false);
		
		GenericObjectDocumentFile docFile = new org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile((org.nuclos.common.NuclosFile) file);
		docFile.setDocumentFilePk(DocumentFileBase.newFileUID());
		newAttachment.setFieldUid(E.GENERALSEARCHDOCUMENT.documentfile.getUID(), (UID) docFile.getDocumentFilePk());
		newAttachment.setFieldValue(E.GENERALSEARCHDOCUMENT.documentfile.getUID(), docFile);
		newAttachment.setFieldValue(E.GENERALSEARCHDOCUMENT.comment, comment);
		newAttachment.setFieldId(E.GENERALSEARCHDOCUMENT.genericObject, (Long) ((EntityObjectVO<PK>) this.eo).getPrimaryKey());
		
		((EntityObjectVO<PK>) this.eo).getDependents().addData(E.GENERALSEARCHDOCUMENT.genericObject, newAttachment.getEntityObject());
	}

	public List<NuclosFile> getAttachments() {
		return getDependents(DEPENDENT_ATTACHEMENT);
	}
	
	private Object getMetaData() {
		return MetaProvider.getInstance().getEntity(
				((EntityObjectVO<PK>)this.eo).getDalEntity());
	}
	
	public interface NuclosMandatorProvider<T extends Modifiable<Long>> {
		
		// called via reflection from EventSupportFacadeBean...
		
		public org.nuclos.api.UID getNuclosMandatorId(T t);
		
	}

	protected void setFieldLocalizedValues(String field, List<NuclosLocale> locales, List<String> localizedValues) {
		UID fieldUID = new org.nuclos.common.UID(field);

		if (!MetaProvider.getInstance().checkEntityField(fieldUID)) {
			// not integrated optional point
			return;
		}
		
		for (NuclosLocale locale : locales) {
			this.checkAndLoadLanguages(new org.nuclos.common.UID(locale.toString()), true);
		}
		
		IDataLanguageMap dataLanguageMap = ((EntityObjectVO<PK>) this.eo).getDataLanguageMap();
		
		if (dataLanguageMap != null) {
			for (int idx=0; idx < locales.size(); idx++) {
				String value = localizedValues.get(idx);
				org.nuclos.common.UID langUID = new org.nuclos.common.UID(locales.get(idx).toString());
				if (!dataLanguageMap.getLanguageMap().containsKey(langUID)) {
					dataLanguageMap.getLanguageMap().put(langUID, 
							new DataLanguageLocalizedEntityEntry(new EntityMetaVO((EntityMeta)getMetaData(), true), getId() != null ? (Long) getId() : null, langUID,
									new HashMap<UID, Object>()));
				}
				dataLanguageMap.getDataLanguage(langUID).setFieldValue(DataLanguageServerUtils.extractFieldUID(fieldUID), value);
				dataLanguageMap.getDataLanguage(langUID).setFieldValue(DataLanguageServerUtils.extractFieldUID(fieldUID, true), Boolean.FALSE);
			}
		}
		
	}
	
	protected List<String> getFieldLocalizedValues(String field, List<NuclosLocale> locales) {
		List<String> retVal = new ArrayList<String>();
		UID fieldUID = new org.nuclos.common.UID(field);
		
		for (NuclosLocale locale : locales) {
			this.checkAndLoadLanguages(new org.nuclos.common.UID(locale.toString()), false);
		}

		if (!MetaProvider.getInstance().checkEntityField(fieldUID)) {
			// not integrated optional point
			return retVal;
		}
		
		IDataLanguageMap dataLanguageMap = ((EntityObjectVO<PK>) this.eo).getDataLanguageMap();
		
		if (dataLanguageMap != null) {
			for (NuclosLocale locale : locales) {
				Object fieldValue = dataLanguageMap.getDataLanguage(new org.nuclos.common.UID(locale.toString())).getFieldValue(DataLanguageServerUtils.extractFieldUID(fieldUID));
				retVal.add(fieldValue != null ? fieldValue.toString(): null);
			}
		}
		
		return retVal;
	}
	
	
	private List getAllLanguages() {
		if (this.languages == null) {
			languages = new ArrayList();
			for (EntityObjectVO eo : NucletDalProvider.getInstance().getEntityObjectProcessor(E.DATA_LANGUAGE).getAll()) {
				languages.add(new DataLanguageVO(eo));
			}
		}
		return this.languages;
		
	}
	
	private Object getDataLanguageCache() {
		if (dlCache == null) {
			dlCache = SpringApplicationContextHolder.getBean(DataLanguageCache.class);
		}
		return dlCache;
	}
	
	private Object getLayoutCache() {
		if (layoutCache == null) {
			layoutCache = SpringApplicationContextHolder.getBean(LayoutFacadeBean.class);			
		}
		return layoutCache;
	}
	
	private List<org.nuclos.api.UID> getDataLanguageUIDs() {
		if (getDataLanguageCache() != null) {
			return ((DataLanguageCache)getDataLanguageCache()).getDataLanguageUIDs();
		} else {
			return CollectionUtils.transform(getAllLanguages(), new Transformer<DataLanguageVO, org.nuclos.api.UID>() {
				@Override
				public org.nuclos.api.UID transform(DataLanguageVO i) {
					return i.getPrimaryKey();
				}
			});			
		}
	}
	
	public org.nuclos.api.UID getNuclosPrimaryDataLanguage() {
		if (getDataLanguageCache() != null) {
			return ((DataLanguageCache)getDataLanguageCache()).getNuclosPrimaryDataLanguage();
		} else {
			for (Object obj : getAllLanguages()) {
				DataLanguageVO dlvo=(DataLanguageVO)obj;
				if (dlvo.isPrimary())
					return dlvo.getPrimaryKey();
			}
			return null;
		}
	}
	
	public org.nuclos.api.UID getCurrentUserDataLanguage() {
		if (getDataLanguageCache() != null) {
			return ((DataLanguageCache)getDataLanguageCache()).getNuclosPrimaryDataLanguage();
		} else {
			return this.getNuclosPrimaryDataLanguage();
		}
	}
	
	@Override
	public String toString() {
		return getEntity() + " " + getId();
	}

	private Object copy(Object origin) {
		
		EntityObjectVO<PK> copy = ((EntityObjectVO)origin).copyFlat();
		
		// Copy dependents without (NUCLOS-6448) using layout-information
		copy.setDependents(null);		
		if (((EntityObjectVO)origin).getDependents() != null) {
			copy.setDependents(new DependentDataMap());
			for (IDependentKey depkey : ((EntityObjectVO)origin).getDependents().getKeySet()) {
				for (EntityObjectVO eo : ((EntityObjectVO)origin).getDependents().getData(depkey)) {
					EntityObjectVO copiedDep = (EntityObjectVO)this.copy(eo);
					// remove reference to upper bo
					copiedDep.setFieldId(depkey.getDependentRefFieldUID(), null);
					copy.getDependents().addData(depkey, copiedDep);
				}				
			}
		}
		
		// Copy languageData
		copy.setDataLanguageMap(null);		
		if (((EntityObjectVO)origin).getDataLanguageMap().getLanguageMap() != null) {
			
			copy.setDataLanguageMap(new DataLanguageMap());
			
			Map<UID, DataLanguageLocalizedEntityEntry> languageMap = ((EntityObjectVO)origin).getDataLanguageMap().getLanguageMap();
			for(UID language : languageMap.keySet()) {
				DataLanguageLocalizedEntityEntry copiedLangData = languageMap.get(language).copy();
				copy.getDataLanguageMap().setDataLanguage(language, copiedLangData);
			}			
		}
		
		copy.flagNew();
		copy.setPrimaryKey(null);
		
		return copy;
	}
	
	protected <T extends AbstractBusinessObject> T copy(Class<T> clzz) {
		
		EntityObjectVO<PK> eoThis = (EntityObjectVO<PK>) this.eo;
		EntityObjectVO<PK> copiedThis = (EntityObjectVO)this.copy(eoThis);
		
		T retVal;
		try {
			retVal = clzz.newInstance();
			EOBOBridge.setEO(retVal, copiedThis);
			
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		} 
		
		return retVal;
	}
	
	protected boolean compare(AbstractBusinessObject bo) {
		
		EntityObjectVO<PK> eoThis = (EntityObjectVO<PK>) this.eo;
		EntityObjectVO<PK> eoThat = (EntityObjectVO<PK>) bo.getEntityObjectVO();
		
		return compare(eoThis, eoThat);
	}
	
	private static <PK> boolean compare(Object objEoThis, Object objEoThat) {
		
		EntityObjectVO eoThis=(EntityObjectVO) objEoThis;
		EntityObjectVO eoThat=(EntityObjectVO) objEoThat;
		if (eoThis == null && eoThat != null) {
			return false;
		}
		if (eoThis != null && eoThat == null) {
			return false;
		}		
		if (!eoThis.getDalEntity().equals(eoThat.getDalEntity())) {
			return false;
		}

		//fields with null-value might not appear in mapField, therefore all fields need to be read from metadata

		MetaProvider metaProvider = SpringApplicationContextHolder.getBean(MetaProvider.class);

		for (Map.Entry<UID, FieldMeta<?>> entry : metaProvider.getAllEntityFieldsByEntity(eoThis.getDalEntity()).entrySet()) {

			UID fieldValue = entry.getKey();
			if (entry.getValue().getForeignEntity() == null) {

				if (SF.VERSION.getUID(eoThis.getDalEntity()).equals(fieldValue) ||
						SF.CHANGEDAT.getUID(eoThis.getDalEntity()).equals(fieldValue) ||
						SF.CHANGEDBY.getUID(eoThis.getDalEntity()).equals(fieldValue) ||
						SF.CREATEDBY.getUID(eoThis.getDalEntity()).equals(fieldValue) ||
						SF.CREATEDAT.getUID(eoThis.getDalEntity()).equals(fieldValue)) {
					continue;
				}

				if (eoThis.getFieldValue(fieldValue) == null &&
						eoThat.getFieldValue(fieldValue) == null) {
					continue;
				}

				if ((eoThis.getFieldValue(fieldValue) == null && eoThat.getFieldValue(fieldValue) != null) || (eoThis.getFieldValue(fieldValue) != null && eoThat.getFieldValue(fieldValue) == null) || (!eoThis.getFieldValue(fieldValue).equals(
						eoThat.getFieldValue(fieldValue)))) {
					return false;
				}
			} else {
				if (eoThis.getFieldId(fieldValue) == null &&
						eoThat.getFieldId(fieldValue) == null) {
					continue;
				}
				if ((eoThis.getFieldId(fieldValue) == null && eoThat.getFieldId(fieldValue) != null) || (eoThis.getFieldId(fieldValue) != null && eoThat.getFieldId(fieldValue) == null) || (!eoThis.getFieldId(fieldValue).equals(
						eoThat.getFieldId(fieldValue)))) {
					return false;
				}
			}
		}
		
		// Referencing system entities are not included
		
		// Dependents
		if (eoThis.getDependents() != null && eoThat.getDependents() != null) {
			
			if (!eoThis.getDependents().getReferencingFieldUids().containsAll(eoThat.getDependents().getReferencingFieldUids()) || 
				!eoThat.getDependents().getReferencingFieldUids().containsAll(eoThis.getDependents().getReferencingFieldUids())) {
					return false;
			}

			for (IDependentKey depEntity : eoThis.getDependents().getKeySet()) {
				
				if (eoThis.getDependents().getData(depEntity, DEPENENT_FLAGS).size() !=
					eoThat.getDependents().getData(depEntity, DEPENENT_FLAGS).size()) {
						return false;
				}
				
				for (EntityObjectVO depEO : eoThis.getDependents().getData(depEntity, DEPENENT_FLAGS)) {
					boolean foundCompare = false;
					for (EntityObjectVO depEOThat : eoThat.getDependents().getData(depEntity, DEPENENT_FLAGS)) {
						if (compare(depEO, depEOThat))
							foundCompare = true;
					}					
					if (!foundCompare)
						return false;
				}
			}
			
		} else if (eoThis.getDependents() == null && eoThat.getDependents() == null) {} 
		else {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Lock this business object by setting the current user as owner.
	 * Uses a new transaction but will not increment the version!
	 * 
	 * @throws BusinessException
	 */
	public void lock() throws BusinessException {
		try {
			UID ownerUID = SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class).lockInNewTransaction((UID)getEntityUid(), ((EntityObjectVO<PK>) this.eo).getPrimaryKey());
			this.lock(ownerUID);
		} catch (CommonPermissionException e) {
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	/**
	 * Lock this business object by setting the given owner.
	 * Uses a new transaction but will not increment the version!
	 * 
	 * @param ownerId
	 * @throws BusinessException
	 */
	public void lock(org.nuclos.api.UID ownerId) throws BusinessException {
		if (!MetaProvider.getInstance().checkEntity((UID)getEntityUid())) {
			// not integrated optional point
			return;
		}
		try {
			SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class).lockInNewTransaction((UID)getEntityUid(), ((EntityObjectVO<PK>) this.eo).getPrimaryKey(), (UID)ownerId);
			((EntityObjectVO<PK>) this.eo).setFieldUid(SF.OWNER.getUID((UID)getEntityUid()), (UID)ownerId);
		} catch (CommonPermissionException e) {
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	/**
	 * Unlock this business object by removing the owner. 
	 * The version is not incremented.
	 * 
	 * @throws BusinessException
	 */
	public void unlock() throws BusinessException {
		if (!MetaProvider.getInstance().checkEntity((UID)getEntityUid())) {
			// not integrated optional point
			return;
		}
		try {
			SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class).unlockInNewTransaction((UID)getEntityUid(), ((EntityObjectVO<PK>) this.eo).getPrimaryKey());
			((EntityObjectVO<PK>) this.eo).removeFieldUid(SF.OWNER.getUID((UID)getEntityUid()));
			((EntityObjectVO<PK>) this.eo).removeFieldValue(SF.OWNER.getUID((UID)getEntityUid()));
		} catch (CommonPermissionException e) {
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	/**
	 * Unlock this business object after commit by removing the owner. 
	 * Uses a new transaction but will not increment the version!
	 * 
	 * @throws BusinessException
	 */
	public void unlockAfterCommit() throws BusinessException {
		if (!MetaProvider.getInstance().checkEntity((UID)getEntityUid())) {
			// not integrated optional point
			return;
		}
		try {
			SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class).unlockInNewTransactionAfterCommit((UID)getEntityUid(), ((EntityObjectVO<PK>) this.eo).getPrimaryKey());
			((EntityObjectVO<PK>) this.eo).removeFieldUid(SF.OWNER.getUID((UID)getEntityUid()));
			((EntityObjectVO<PK>) this.eo).removeFieldValue(SF.OWNER.getUID((UID)getEntityUid()));
		} catch (CommonPermissionException e) {
			throw new BusinessException(e.getMessage(), e);
		}
	}

	public String getNuclosTitle() {
		final MetaProvider metaProv = MetaProvider.getInstance();
		final EntityMeta<Object> eMeta = metaProv.getEntity((UID) getEntityUid());
		final UID datalanguage = SpringApplicationContextHolder.getBean(NuclosUserDetailsContextHolder.class).getDataLocal();

		EntityObjectVO<PK> eo = (EntityObjectVO<PK>) this.eo;
		if (eMeta.isThin() && eo.getPrimaryKey() != null) {
			boolean bWithEmptyReferencesInTitle = false;
			final String localeResourceIdForTreeView = eMeta.getLocaleResourceIdForTreeView();
			if (localeResourceIdForTreeView != null) {
				final String stringifiedRepresentation = SpringLocaleDelegate.getInstance().getText(localeResourceIdForTreeView);
				if (stringifiedRepresentation != null) {
					for (UID fieldUID : RefValueExtractor.getAttributes(stringifiedRepresentation)) {
						if ((eo.getFieldId(fieldUID) != null || eo.getFieldUid(fieldUID) != null) && eo.getFieldValue(fieldUID) == null) {
							bWithEmptyReferencesInTitle = true;
							break;
						}
					}
				}
				if (bWithEmptyReferencesInTitle) {
					// Thin object without stringified values. read it from db again
					eo = (EntityObjectVO<PK>) NucletDalProvider.getInstance().getEntityObjectProcessor(eMeta).getByPrimaryKey(eo.getPrimaryKey());
				}
			}
		}
		return SpringLocaleDelegate.getInstance().getTreeViewLabel(eo, metaProv, datalanguage);
	}

	private static BusinessObjectService getBussinessObjectService() {
		return SpringApplicationContextHolder.getBean(BusinessObjectService.class);
	}

	protected void save() throws BusinessException {
		if (isInsert()) {
			getBussinessObjectService().insert(this);
		} else {
			getBussinessObjectService().update(this);
		}
	}

	protected void delete() throws BusinessException {
		getBussinessObjectService().delete(this);
	}

	protected static <PK> void delete(UID entity, PK pk) throws BusinessException {
		((BusinessObjectServiceProvider)getBussinessObjectService()).delete(entity, pk, false);
	}

	protected void changeStatus(org.nuclos.api.statemodel.State state) throws BusinessException {
		StatemodelService service = SpringApplicationContextHolder.getBean(StatemodelService.class);
		service.changeState((org.nuclos.api.businessobject.facade.thin.Stateful)this, state);
	}

	protected static <PK, T extends BusinessObject<PK>> T get(Class<T> _clazz, PK pk) {
		QueryService service = SpringApplicationContextHolder.getBean(QueryService.class);
		return service.getById(_clazz, pk);
	}

}
