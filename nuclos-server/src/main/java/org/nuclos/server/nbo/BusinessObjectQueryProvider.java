//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.nbo;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Process;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.QueryOperation;
import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.businessobject.SearchExpressionPair;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute;
import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.common.NuclosUserCommunicationAccount;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.QueryService;
import org.nuclos.api.statemodel.State;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.bo.NuclosUserCommunicationAccountBO;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("businessObjectQueryProvider") 
public class BusinessObjectQueryProvider implements QueryService {

	private static final Logger LOG = LoggerFactory.getLogger(BusinessObjectQueryProvider.class);

	@Autowired
	private NucletDalProvider nucletDalProvider;

	@Autowired
	private MetaProvider metaProvider;

	@Override
	public <PK, T extends BusinessObject<PK>> T executeQuerySingleResult(
			Query<T> query) throws BusinessException {
		List<T> retVals = executeQuery(query);
		if (retVals.isEmpty()) {
			return null;
		} else if (retVals.size() == 1) {
			return retVals.get(0);
		} else {
			throw new BusinessException("Query returns more than one row");
		}
	}
	
	private <T extends BusinessObject<?>> AbstractBusinessObject<?> newInstance(Class<T> cls) throws InstantiationException, IllegalAccessException {
		if (cls.equals(NuclosUserCommunicationAccount.class)) {
			return new NuclosUserCommunicationAccountBO();
		}
		return (AbstractBusinessObject<?>) cls.newInstance();
	}
	
	@Override
	public <PK, T extends BusinessObject<PK>> List<T> executeQuery(Query<T> arg) {
		
		List<T> retVal = new ArrayList<T>();
		
		BusinessObjectQueryImpl query= (BusinessObjectQueryImpl) arg;
		boolean doSorting = query.getOrder().size() > 0;
		
		IEntityObjectProcessor<UID> processor = null;
		try {
			// Get Processor for given entity
			EntityObjectVO<?> eo = EOBOBridge.getEO(newInstance(query.getType()));

			if (!metaProvider.checkEntity(eo.getDalEntity())) {
				// not integrated optional point
				return retVal;
			}
			
			processor = nucletDalProvider.getEntityObjectProcessor(eo.getDalEntity());
			processor.setThinReadEnabled(true);
			
			// Where, And, Exist are an AND-Condition for SearchExpression
			CompositeCollectableSearchCondition completeSearchCondition = 
					new CompositeCollectableSearchCondition(LogicalOperator.AND);	
			
			// load query elements
			createMainQueryElements(completeSearchCondition, query, eo.getDalEntity());
			
			CollectableSearchExpression searchExpr = null;
			
			// If sorting create SortingList for SearchExpression
			if(doSorting) {
				searchExpr = new CollectableSearchExpression(completeSearchCondition, createOrderByClause(query, eo));
			}
			else {
				searchExpr = new CollectableSearchExpression(completeSearchCondition);
			}
			
			// Execute search
			List<EntityObjectVO<UID>> bySearchExpression = null;
			
			
			if (completeSearchCondition.getOperandCount() == 0) {
				if(doSorting) {
					searchExpr = new CollectableSearchExpression(TrueCondition.TRUE, createOrderByClause(query, eo));
					bySearchExpression = processor.getBySearchExprResultParams(searchExpr, new ResultParams(null, doSorting));
				} else {
					bySearchExpression = processor.getAll();
				}
			}
			else {
				bySearchExpression = processor.getBySearchExprResultParams(searchExpr, new ResultParams(null, doSorting));
			}
			
			if (bySearchExpression != null && bySearchExpression.size() > 0) {
				retVal = new ArrayList<T>();
				for (EntityObjectVO<UID> eoVo : bySearchExpression) {
					// new BusinessObject
					T t = (T) newInstance(query.getType());
					// cast data to BusinessObject...
					EOBOBridge.setEO((AbstractBusinessObject<UID>) t, eoVo);
					// ... and add it to the result list to return
					retVal.add(t);
				}
			}
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			if (processor != null) {
				processor.setThinReadEnabled(false);
			}
		}
		
		return retVal;
	}	

	private void createMainQueryElements(
			CompositeCollectableSearchCondition completeSearchCondition,
			BusinessObjectQueryImpl query, UID entity) {
		
		LogicalDeletedFlag logDelFlag = new LogicalDeletedFlag();
		
		// Where-Clause, including AND
		if (query.getWhere() != null && query.getWhere().size() > 0) {
			completeSearchCondition.addOperand(createWhereElement(query, logDelFlag));
		}
		
		// If the query does not contain any specific setting for showing logical deleted elements 
		// we exclude them from the result
		if (!logDelFlag.isContainsLogicalDeletedFlag() && metaProvider.getEntity(entity).isStateModel()) {
			completeSearchCondition.addOperand(
					SearchConditionUtils.newComparison(SF.LOGICALDELETED.getUID(entity), ComparisonOperator.EQUAL, Boolean.FALSE));
		}

		// Exist-Clauses
		if (query.getSimpleExist() != null && query.getSimpleExist().size() > 0) {
			CollectableSearchCondition simpleSub = unsafeCreateSimpleExistElement(query);
			if (simpleSub != null)
				completeSearchCondition.addOperand(simpleSub);
		}
		
		if (query.getCombinedExist() != null && query.getCombinedExist().size() > 0) {
			CollectableSearchCondition combinedQuery = unsafeCreateCombinedExistElement(query);
			if (combinedQuery != null)
				completeSearchCondition.addOperand(combinedQuery);
		}
	}

	private CollectableSearchCondition unsafeCreateCombinedExistElement(BusinessObjectQueryImpl query) {
		return this.<Object,BusinessObject<Object>>createCombinedExistElement(query);
	}
	
	private <PK, T extends BusinessObject<PK>> CollectableSearchCondition createCombinedExistElement(
			BusinessObjectQueryImpl query) {
		
		CompositeCollectableSearchCondition retVal = new CompositeCollectableSearchCondition(LogicalOperator.AND);

		for (Query<T> subQuery : query.getCombinedExist().keySet()) {
			
			Pair<ForeignKeyAttribute, ForeignKeyAttribute> pair = query.getCombinedExist().get(subQuery);

			UID fieldMainQuery = new UID(pair.x.getAttributeUid());
			UID fieldSubQuery = new UID(pair.y.getAttributeUid());
			
			List<CollectableSearchCondition> listOfExplr = new ArrayList<CollectableSearchCondition>();
			
			for (T t : executeQuery(subQuery)) {
				AbstractBusinessObject obj = (AbstractBusinessObject) t;
				Object fieldValue = obj.getFieldId(fieldSubQuery.toString());
				listOfExplr.add(
						SearchConditionUtils.newKeyComparison(
								fieldMainQuery, ComparisonOperator.EQUAL, fieldValue));
			}
			
			retVal.addOperand(SearchConditionUtils.or(listOfExplr.toArray(new CollectableSearchCondition[listOfExplr.size()])));
		}
	
		return retVal;
	}
	
	private CollectableSearchCondition unsafeCreateSimpleExistElement(
			BusinessObjectQueryImpl query) {
		
		return this.<Object,BusinessObject<Object>>createSimpleExistElement(query);
	}
		
	private <PK, T extends BusinessObject<PK>> CollectableSearchCondition createSimpleExistElement(
			BusinessObjectQueryImpl query) {
		
		CompositeCollectableSearchCondition retVal = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		
		for (Query<T> subQuery : query.getSimpleExist().keySet()) {
			Attribute attr = query.getSimpleExist().get(subQuery);		
			UID fieldUid = new UID(attr.getAttributeUid());
			FieldMeta<?> fieldMeta = metaProvider.getEntityField(fieldUid);
			
			List lstPKs = new ArrayList();			
			for (T t : executeQuery(subQuery)) {
				AbstractBusinessObject obj = (AbstractBusinessObject) t;
				lstPKs.add(obj.getId());
			}
	
			if (fieldMeta.getForeignEntity() != null) {
				retVal.addOperand(new CollectableInIdCondition(new CollectableEOEntityField(fieldMeta), lstPKs));				
			} else {
				retVal.addOperand(new CollectableInCondition(new CollectableEOEntityField(fieldMeta), lstPKs));
			}
		}
	
		if (retVal.getOperandCount() == 0)
			return null;
		
		return retVal;
	}
	
	private <PK> CollectableSearchCondition createSearchAttributeElement(
			SearchExpression<PK> exp, LogicalDeletedFlag logDelFlag) {
		
		CollectableSearchCondition retVal = null;

		Attribute<PK> attr = exp.getSource();
		UID fieldUid = new UID(attr.getAttributeUid());
		UID entityUid = new UID(attr.getEntityUid());
		
		if (SF.LOGICALDELETED.getUID(entityUid).equals(fieldUid)) {
			logDelFlag.setContainsLogicalDeletedFlag(true);
		}
		
		boolean isKey = exp.getSource() instanceof PrimaryKeyAttribute ? true: false;
		boolean isForeign = exp.getSource() instanceof ForeignKeyAttribute ? true: false;
		
		switch (exp.getExpressionOperator()) {
			case EQUALS:
				if (exp.getValue() != null) {
					retVal = mapField(isKey, isForeign, exp, ComparisonOperator.EQUAL,fieldUid, entityUid);
				}
				break;
			case UNEQUALS:
				if (exp.getValue() != null) {
					if (!isKey) {
						retVal = SearchConditionUtils.or(mapField(isKey, isForeign, exp, ComparisonOperator.NOT_EQUAL,fieldUid, entityUid),
								SearchConditionUtils.newIsNullCondition(fieldUid));						
					}
					else {
						retVal = mapField(isKey, isForeign, exp, ComparisonOperator.NOT_EQUAL, fieldUid, entityUid);
					}
				}		
				break;
			case LIKE:
				if (exp.getValue() != null) {
					retVal = mapField(isKey, isForeign, exp, ComparisonOperator.LIKE, fieldUid, entityUid);
				}	
				break;
			case LT:
				if (exp.getValue() != null) {
					retVal = mapField(isKey, isForeign, exp, ComparisonOperator.LESS, fieldUid, entityUid);
				}	
				break;
			case LTE:
				if (exp.getValue() != null) {
					retVal = mapField(isKey, isForeign, exp, ComparisonOperator.LESS_OR_EQUAL, fieldUid, entityUid);
					}
				break;
			case GT:
				if (exp.getValue() != null) {
					retVal = mapField(isKey, isForeign, exp, ComparisonOperator.GREATER, fieldUid, entityUid);
				}
				break;
			case GTE:
				if (exp.getValue() != null) {
					retVal = mapField(isKey, isForeign, exp, ComparisonOperator.GREATER_OR_EQUAL, fieldUid, entityUid);
				}
				break;
			case NOTNULL:
				retVal = SearchConditionUtils.not(SearchConditionUtils.newIsNullCondition(fieldUid));
				break;
			case NULL:
				retVal = SearchConditionUtils.newIsNullCondition(fieldUid);
				break;
			default:
				break;
		}
	
		return retVal;
	}

	private <PK> CollectableSearchCondition mapField(boolean isKey, boolean isForeign,
			SearchExpression<PK> exp, ComparisonOperator operator, UID fieldUid, UID entityUid) {
		
		CollectableSearchCondition retVal = null;
	
		if (isKey || isForeign) {
			
			if (!isForeign && isKey && SF.isEOField(entityUid, fieldUid)) {
				if (E.isNuclosEntity(entityUid)) {
					retVal = SearchConditionUtils.newPkComparison(SF.PK_UID.getMetaData(entityUid), operator, (UID) exp.getValue());
				} else {
					retVal = SearchConditionUtils.newPkComparison(SF.PK_ID.getMetaData(entityUid), operator, (Long) exp.getValue());					
				}
			} else {
				retVal = SearchConditionUtils.newPkComparison(fieldUid, operator, exp.getValue());												
			}
		}
		else if (ComparisonOperator.LIKE.equals(operator)) {
			retVal = SearchConditionUtils.newLikeCondition(fieldUid, "%" + exp.getValue().toString() + "%");
		}
		else {
			retVal = SearchConditionUtils.newComparison(fieldUid, operator, exp.getValue());
		}
		
		return retVal;
	}

	private <PK> CollectableSearchCondition createWhereElement(
			BusinessObjectQueryImpl query, LogicalDeletedFlag logDelFlag) {
		
		CompositeCollectableSearchCondition retVal = new CompositeCollectableSearchCondition(LogicalOperator.AND);
			
		for (SearchExpression<PK> exp : query.getWhere()) {
			retVal.addOperand(createExpression(exp, logDelFlag));
		}
		
		return retVal;
	}

	private <PK> CollectableSearchCondition createExpression(SearchExpression<PK> se, LogicalDeletedFlag logDelFlag) {
		
		// first searchexpression
		CollectableSearchCondition retVal = createSearchAttributeElement(se, logDelFlag);
		
		if (se.hasChildSearchExpression()) {
			List<SearchExpressionPair<PK>> lstCse = se.getChildSearchExpression();
			
			for (int idx = 0; idx < lstCse.size(); idx++) {
				
				SearchExpressionPair<PK> currentItem = lstCse.get(idx);
				CompositeCollectableSearchCondition condition = null;
				CollectableSearchCondition newChild = null;
				
				newChild = createExpression(currentItem.getSearchExpression(), logDelFlag);
				
				if (QueryOperation.OR.equals(currentItem.getOperator())) {
					condition = new CompositeCollectableSearchCondition(
							LogicalOperator.OR);
					condition.addOperand(retVal);
					condition.addOperand(newChild);
				}
				else {
					condition = new CompositeCollectableSearchCondition(
							LogicalOperator.AND);
					condition.addOperand(retVal);
					condition.addOperand(newChild);
				}
				
				retVal = condition;
			}
		} 
		
		return retVal;
	}
	
	private <PK> List<CollectableSorting> createOrderByClause(BusinessObjectQueryImpl query, EntityObjectVO<PK> eo) {
		List<CollectableSorting> retVal = new ArrayList<CollectableSorting>();
		
		UID entityUid = (UID) eo.getDalEntity();
			
		for (Attribute<PK> keySet : query.getOrder().keySet()) {
				FieldMeta<?> entityField = metaProvider.getEntityField(new UID(keySet.getAttributeUid()));
				CollectableSorting sortBy = 
						new CollectableSorting(SystemFields.BASE_ALIAS, entityUid, true, entityField.getUID(), query.getOrder().get(keySet).booleanValue());
				retVal.add(sortBy);
		}
		
		return retVal;
	}

	@Override
	public <PK, T extends BusinessObject<PK>> T getById(Class<T> classtype, PK Id) {
		T t = null;
		
		IEntityObjectProcessor<PK> processor = null;
		try {
			t = classtype.getDeclaredConstructor().newInstance();
			UID entityUid = (UID) t.getEntityUid();
			
			EntityMeta<PK> entityMeta = metaProvider.getEntity(entityUid);
					
			processor = nucletDalProvider.getEntityObjectProcessor(entityMeta);
			processor.setThinReadEnabled(true);
			
			EntityObjectVO<PK> eoVO = processor.getByPrimaryKey(Id);
			if (eoVO == null) {
				return null;
			}
			
			EOBOBridge.setEO((AbstractBusinessObject) t, eoVO);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			if (processor != null) {
				processor.setThinReadEnabled(false);
			}
		}
		
		return t;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <PK, T extends BusinessObject<PK>> Query<T> createQuery(
			Class<T> type) {
		return new BusinessObjectQueryImpl(type);
	}
	
	@Override	
	public <PK, T extends Stateful & BusinessObject<PK>> List<T> getByState(Class<T> type, State state, State... additionalStates) 
			throws BusinessException {
		
		List<T> retVal = new ArrayList<T>();
		
		if (type == null)
			throw new BusinessException("Type of BusinessObject must not be null");
		
		IEntityObjectProcessor processor = null;
		try {
			UID entity = (UID) type.newInstance().getEntityUid();
			
			List<EntityObjectVO<Object>> results = null;

			processor = nucletDalProvider.getEntityObjectProcessor(metaProvider.getEntity(entity));
			processor.setThinReadEnabled(true);
			
			List<CollectableComparison> lstStates = new ArrayList<>();
			
			lstStates.add(
					SearchConditionUtils.newKeyComparison(SF.STATE.getMetaData(entity).getUID(), ComparisonOperator.EQUAL, (UID) state.getId()));
			
			for (State s : additionalStates) {
				lstStates.add(
						SearchConditionUtils.newKeyComparison(SF.STATE.getMetaData(entity).getUID(), ComparisonOperator.EQUAL, (UID) s.getId()));
			}

			CompositeCollectableSearchCondition searchCondition = SearchConditionUtils.and(SearchConditionUtils.or(lstStates.toArray(new CollectableComparison[lstStates.size()])));
			if (metaProvider.getEntity(entity).isStateModel()) {
				searchCondition.addOperand(
						SearchConditionUtils.newComparison(SF.LOGICALDELETED.getUID(entity), ComparisonOperator.EQUAL, Boolean.FALSE));
			}

			results = processor.getBySearchExpression(new CollectableSearchExpression(searchCondition));
			
			
			for (EntityObjectVO eoVO : results) {
				if (eoVO != null) {
					T resultsObject = type.newInstance();
					EOBOBridge.setEO((AbstractBusinessObject) resultsObject, eoVO);
					retVal.add(resultsObject);
				}
			}
			
		} catch (Exception e) {
			throw new BusinessException(e);
		} finally {
			if (processor != null) {
				processor.setThinReadEnabled(false);
			}
		}
		
		return retVal;
	}
	
	@Override
	public <PK, T extends Stateful & BusinessObject<PK>> List<T> getByProcess(Process<T> process, Process<T>... additionalProcesses)
			throws BusinessException {

		if (process == null)
			throw new BusinessException("There must be at least one process");
		
		UID entityUID = (UID) process.getEntityId();
		Class<T> ClzzT = process.getType();
		List<T> retVal = new ArrayList<T>();
		List<CollectableComparison> lstProcesses = new ArrayList<CollectableComparison>();
		
		IEntityObjectProcessor<PK> provider = null;
		try {
			String prosUID = ((UID) process.getId()).toString();
			lstProcesses.add(
					SearchConditionUtils.newKeyComparison(SF.PROCESS.getMetaData(entityUID).getUID(), ComparisonOperator.EQUAL, prosUID));
			
			for (Process<T> p : additionalProcesses) {
				prosUID = ((UID) p.getId()).toString();
				lstProcesses.add(
						SearchConditionUtils.newKeyComparison(SF.PROCESS.getMetaData(entityUID).getUID(), ComparisonOperator.EQUAL, prosUID));
			}
			
			provider = nucletDalProvider.getEntityObjectProcessor(entityUID);
			provider.setThinReadEnabled(true);

			CompositeCollectableSearchCondition searchCondition = SearchConditionUtils.and(SearchConditionUtils.or(lstProcesses.toArray(new CollectableComparison[lstProcesses.size()])));
			if (metaProvider.getEntity(entityUID).isStateModel()) {
				searchCondition.addOperand(
						SearchConditionUtils.newComparison(SF.LOGICALDELETED.getUID(entityUID), ComparisonOperator.EQUAL, Boolean.FALSE));
			}
			
			List<EntityObjectVO<PK>> bySearchExpression = provider.getBySearchExpression(new CollectableSearchExpression(searchCondition));
			
			for (EntityObjectVO<PK> eoVO : bySearchExpression) {
				if (eoVO != null) {
					T t = ClzzT.newInstance();					
					EOBOBridge.setEO((AbstractBusinessObject) t, eoVO);
					retVal.add(t);
				}
				
			}			
		} catch (Exception e) {
			throw new BusinessException(e);
		} finally {
			if (provider != null) {
				provider.setThinReadEnabled(false);
			}
		}
		
		return retVal;
	}

	class LogicalDeletedFlag {
		private boolean containsLogicalDeletedFlag;
		
		public LogicalDeletedFlag() {
			containsLogicalDeletedFlag = false;
		}
		
		public boolean isContainsLogicalDeletedFlag() {
			return containsLogicalDeletedFlag;
		}
		public void setContainsLogicalDeletedFlag(boolean containsLogicalDeletedFlag) {
			this.containsLogicalDeletedFlag = containsLogicalDeletedFlag;
		}
		
		
	}
}
