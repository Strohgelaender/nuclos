package org.nuclos.server.nbo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Query;
import org.nuclos.api.businessobject.SearchExpression;
import org.nuclos.api.businessobject.attribute.Attribute;
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute;
import org.nuclos.common.collection.Pair;

@SuppressWarnings("rawtypes")
public class BusinessObjectQueryImpl implements Query {

	private Class<? extends BusinessObject> type;
	private Map<Attribute, Boolean> order = new LinkedHashMap<Attribute, Boolean>();
	private List<SearchExpression> where = new ArrayList<SearchExpression>();
	private Map<Query, ForeignKeyAttribute> simpleExist = new HashMap<Query, ForeignKeyAttribute>();
	private Map<Query, Pair<ForeignKeyAttribute, ForeignKeyAttribute>> combinedExist = new HashMap<Query, Pair<ForeignKeyAttribute, ForeignKeyAttribute>>();
	
	protected BusinessObjectQueryImpl(Class<? extends BusinessObject> pType) {
		this.type = pType;
	}
	
	@Override
	public Query orderBy(Attribute pElement, Boolean bAscending) {
		order.put(pElement, bAscending);
		return this;
	}

	@Override
	public Query and(Attribute pElement,Boolean bAscending) {
		order.put(pElement, bAscending);
		return this;
	}

	@Override
	public Query where(SearchExpression elm) {
		where.add(elm);
		return this;
	}

	@Override
	public Query and(SearchExpression elm) {
		where.add(elm);
		return this;
	}
	
	@Override
	public Query exist(Query subQuery, ForeignKeyAttribute elementMainQuery,
			ForeignKeyAttribute elementSubQuery) {
		combinedExist.put(subQuery, new Pair<ForeignKeyAttribute, ForeignKeyAttribute>(elementMainQuery, elementSubQuery));
		return this;
	}

	@Override
	public Query exist(Query subQuery,
			ForeignKeyAttribute element) {
		simpleExist.put(subQuery, element);
		return this;
	}

	Class<? extends BusinessObject> getType() {
		return type;
	}

	Map<Attribute, Boolean> getOrder() {
		return order;
	}

	List<SearchExpression> getWhere() {
		return where;
	}

	Map<Query, ForeignKeyAttribute> getSimpleExist() {
		return simpleExist;
	}

	Map<Query, Pair<ForeignKeyAttribute, ForeignKeyAttribute>> getCombinedExist() {
		return combinedExist;
	}

	
}
