//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.nbo;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.nuclos.businessentity.NucletIntegrationField;
import org.nuclos.businessentity.NucletIntegrationPoint;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;

class IntegrationPointEntityMeta extends EntityMetaVO<Long> {

	static class IntegrationFieldMeta<T> extends FieldMetaVO<T> {

		private final NucletIntegrationPoint point;
		private final NucletIntegrationField field;

		public IntegrationFieldMeta(final NucletIntegrationPoint point, final FieldMeta<?> fieldMeta) {
			this(point, transform(fieldMeta));
		}

		public IntegrationFieldMeta(final NucletIntegrationPoint point, final NucletIntegrationField field) {
			this.point = point;
			this.field = field;
			setUID(field.getId());
			String sDataType = field.getDatatype();
			if (sDataType == null) {
				// Reference...
				sDataType = String.class.getCanonicalName();
			}
			setDataType(sDataType);
			setScale(field.getDatascale());
			setPrecision(field.getDataprecision());
			setReadonly(field.getReadonly());
			setFieldName(field.getName());
			setForeignEntity(field.getEntityReferenceFieldId());
			setForeignIntegrationPoint(field.getIntegrationPointReferenceFieldId());
		}

		@Override
		public final boolean isIntegrationField() {
			return true;
		}

		@Override
		public boolean isReadonly() {
			return Boolean.TRUE.equals(getIntegrationField().getReadonly());
		}

		public NucletIntegrationField getIntegrationField() {
			return field;
		}

		public NucletIntegrationPoint getIntegrationPoint() {
			return point;
		}

		private static NucletIntegrationField transform(final FieldMeta<?> fieldMeta) {
			NucletIntegrationField field = new NucletIntegrationField();
			field.setId(fieldMeta.getUID());
			String sDataType = fieldMeta.getDataType();
			field.setDatatype(fieldMeta.getDataType());
			field.setDatascale(fieldMeta.getScale());
			field.setDataprecision(fieldMeta.getPrecision());
			field.setReadonly(fieldMeta.isReadonly());
			field.setName(fieldMeta.getFieldName());
			field.setEntityReferenceFieldId(fieldMeta.getForeignEntity());
			return field;
		}
	}

	private final NucletIntegrationPoint point;

	private final Collection<FieldMeta<?>> fields = new ArrayList<>();

	public IntegrationPointEntityMeta(NucletIntegrationPoint point) {
		super(Long.class);
		this.point = point;
		final UID entityUID = point.getId();
		final UID targetEntityUID = point.getTargetEntityId();
		setUID(entityUID);
		setEntityName(point.getName());
		setNuclet(point.getNucletId());
		setThin(true);

		if (Boolean.TRUE.equals(point.getStateful())) {
			setStateModel(true);
			IntegrationFieldMeta fStateMeta = new IntegrationFieldMeta(point, SF.STATE.getMetaData(RigidUtils.defaultIfNull(targetEntityUID, entityUID)));
			fStateMeta.setEntity(entityUID);
			fields.add(fStateMeta);
		}

		for (NucletIntegrationField field : point.getNucletIntegrationField1()) {
			IntegrationFieldMeta fMeta = new IntegrationFieldMeta(point, field);
			fMeta.setEntity(entityUID);
			fields.add(fMeta);
		}
	}

	@Override
	public final boolean isIntegrationPoint() {
		return true;
	}

	@Override
	public boolean isReadonly() {
		return Boolean.TRUE.equals(getIntegrationPoint().getReadonly());
	}

	public NucletIntegrationPoint getIntegrationPoint() {
		return this.point;
	}

	@Override
	public Collection<FieldMeta<?>> getFields() {
		return Collections.unmodifiableCollection(fields);
	}

}
