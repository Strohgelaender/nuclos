//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.nbo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.nuclos.api.NuclosImage;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.facade.LogicalDeletable;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.BusinessObjectService;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class provides several services for general BO handling like
 * inserting, modifying or deleting existing or new BusinessObjects
 * via Nuclos DAL-Provider
 * 
 * This class is usually called by API
 * 
 * @author reichama
 *
 */
@Component("businessObjectServiceProvider")
public class BusinessObjectServiceProvider implements BusinessObjectService {

	@Autowired
	private EntityObjectFacadeLocal eoFacade;

	@Autowired
	private MetaProvider metaProvider;
	
	@Override
	public <PK, T extends BusinessObject<PK>> PK insert(T type) throws BusinessException {

		AbstractBusinessObject<PK> abo = RigidUtils.uncheckedCast(type);

		// EO must be new
		if (!abo.isInsert()) {
			throw new BusinessException("BusinessObject is not new and cannot be inserted.");
		}

		// Insert
		try {
			EntityObjectVO<PK> eo = EOBOBridge.getEO(abo);

			if (!metaProvider.checkEntity(eo.getDalEntity())) {
				// not integrated optional point
				return null;
			}

			eo = eoFacade.insert(eo);

			EOBOBridge.setEO(abo, eo);
			return abo.getId();
		} catch (Exception e) {
			if (e.getCause() instanceof CommonValidationException) {
				throw new BusinessException (e.getCause().toString(), e.getCause());
			}
			throw new BusinessException(e.getMessage(), e);
		}
	}

	@Override
	public <T extends BusinessObject> void update(T type) throws BusinessException {

		AbstractBusinessObject<?> abo = RigidUtils.uncheckedCast(type);

		boolean allowUpdate = abo.isUpdate() || abo.isUnchanged() || (abo.isInsert() && abo.getId() != null);

		// EO must be in a modified state
		if (!allowUpdate) {
			throw new BusinessException("BusinessObject is not in a modified state and cannot be updated.");
		}

		// Update EO
		try {
			EntityObjectVO eo = EOBOBridge.getEO(abo);

			if (!metaProvider.checkEntity(eo.getDalEntity())) {
				// not integrated optional point
				return;
			}

			eo = eoFacade.update(eo, false);
			EOBOBridge.setEO(abo, eo);
		 
		} catch (Exception e) {
			if (e instanceof CommonValidationException) {
				throw new BusinessException (e.toString(), e);
			}
			if (e.getCause() instanceof CommonValidationException) {
				throw new BusinessException (e.getCause().toString(), e.getCause());
			}
			throw new BusinessException (e.getMessage(), e);
		}	
	
	}

	private <PK, T extends BusinessObject> void delete(T type, boolean deleteLogical) throws BusinessException {
		// Remove EO
		AbstractBusinessObject<PK> abo = RigidUtils.uncheckedCast(type);
		EntityObjectVO<PK> eo = EOBOBridge.getEO(abo);
		eo.flagRemove();

		delete(eo.getDalEntity(), eo.getPrimaryKey(), deleteLogical);
	}

	public <PK> void delete(UID entity, PK pk, boolean deleteLogical) throws BusinessException {
		// Remove EO
		try {
			if (!metaProvider.checkEntity(entity)) {
				// not integrated optional point
				return;
			}

			eoFacade.delete(entity, pk, deleteLogical, true);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage(), e);
		}
	}

	@Override
	public <PK, T extends BusinessObject<PK>> Collection<PK> insertAll(Collection<T> types) throws BusinessException {
		if (types == null) {
			return Collections.emptyList();
		}

		Collection<PK> retVal = new ArrayList<>();

		for (T t : types) {
			retVal.add(insert(t));
		}

		return retVal;
	}

	@Override
	public <T extends BusinessObject> void updateAll(Collection<T> types) throws BusinessException {
		if (types == null) {
			return;
		}
		
		for (T t : types) {
			update(t);
		}
	}

	@Override
	public <T extends BusinessObject> void deleteAll(Collection<T> types) throws BusinessException {
		if (types == null) {
			return;
		}

		for (T t : types) {
			delete(t, false);
		}
	}

	@Override
	public <PK, T extends LogicalDeletable> void deleteLogical(T type) throws BusinessException {
		delete(type, true);
	}

	@Override
	public <PK, T extends LogicalDeletable> void deleteLogicalAll(Collection<T> types) throws BusinessException {
		if (types == null) {
			return;
		}

		for (T t : types) {
			delete(t, true);
		}
	}

	@Override
	public <T extends BusinessObject> void delete(T type) throws BusinessException {
		delete(type, false);
	}

	private static void checkDocuParams(String name, byte[] content) throws BusinessException {
		if (name == null) {
			throw new BusinessException("filename must not be null");
		}

		if (content == null) {
			throw new BusinessException("content must not be null");
		}
	}

	@Override
	public NuclosFile newFile(String name, byte[] content) throws BusinessException {
		checkDocuParams(name, content);
		return new org.nuclos.common.NuclosFile(name, content);
	}

	@Override
	public NuclosImage newImage(String name, byte[] content) throws BusinessException {
		checkDocuParams(name, content);
		return new org.nuclos.common.NuclosImage(name, content);
	}
	
}
