package org.nuclos.server.nbo;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Lockable;
import org.nuclos.api.businessobject.facade.LogicalDeletable;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.businessentity.utils.BusinessObjectBuilderForInternalUse;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.StaticMetaDataProvider;
import org.nuclos.common.UID;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;

public class NuclosBusinessObjectSourceBuilder {

	private final StaticMetaDataProvider metaProvider;

	public NuclosBusinessObjectSourceBuilder(final StaticMetaDataProvider metaProvider) {
		this.metaProvider = metaProvider;
	}

	/**
	 * This Method extracts and validate the meta information for all NBO-Classes to build and creates
	 * JavaSources in the OutputFolder that has been passed as parameter
	 * 
	 * @param nBOMetaDatas
	 * @return
	 */
	public List<NuclosBusinessJavaSource> createJavaSources(
		List<NuclosBusinessObjectMetaData> nBOMetaDatas,
		final String outputPathFromProperties) throws InterruptedException {

		final List<NuclosBusinessJavaSource> retVal = new ArrayList<>();
		
		for (NuclosBusinessObjectMetaData nBOMetaDataObj : nBOMetaDatas) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			String outputPath = outputPathFromProperties;
			if (BusinessObjectBuilderForInternalUse.isEnabled()) {
				for (EntityMeta<?> devSystemBo : BusinessObjectBuilderForInternalUse.getEntityMetas()) {
					if (devSystemBo.getUID().equals(nBOMetaDataObj.getEntityUid())) { // ...is dev system bo
						// Write src output to Nuclos sources...
						outputPath = BusinessObjectBuilderForInternalUse.getOutputDir().getPath().toString();
						break;
					}
				}
			}
			final String sPackage = nBOMetaDataObj.getPackage();
			final String entity = nBOMetaDataObj.getFormattedEntity();
			final String qname = sPackage + "." + entity;
			final String sFileName = outputPath + File.separatorChar 
					+ sPackage.replace(".", File.separator) + File.separatorChar 
					+ entity + ".java";
			
			final NuclosBusinessJavaSource nboSource = new NuclosBusinessJavaSource(
					qname, sFileName, createJavaSource(nBOMetaDataObj), true);
			retVal.add(nboSource);
		}
		
		return retVal;
	}

	/**
	 * Evaluates the given MetaDataObject and creates a String that represents a valid
	 * JavaSource-File
	 * @param nBOMetaDataObj
	 * @return
	 */
	private String createJavaSource(NuclosBusinessObjectMetaData nBOMetaDataObj)
		throws InterruptedException {
	
		StringBuilder sBuilder = new StringBuilder();
		
		// Header
		sBuilder.append(nBOMetaDataObj.getHeader());
		
		// Package
		sBuilder.append("package ").append(nBOMetaDataObj.getPackage()).append(";\n");
		// Imports
		for (String curImport : nBOMetaDataObj.getImports()) {
			sBuilder.append("\nimport ").append(curImport).append("; ");
		}

		final EntityMeta<?> eMeta = metaProvider.getEntity(nBOMetaDataObj.getEntityUid());
		final boolean bIsForTemporarySystemBO = BusinessObjectBuilderForInternalUse.getEntityMetas().contains(eMeta);

		// Classname with extend...
		String pkTypeOfEntity = NuclosBusinessObjectMetaData.getPkClassName(eMeta);
		
		// create javadoc
		if(nBOMetaDataObj.getComment() != null) {
			sBuilder.append(nBOMetaDataObj.getComment()).append("\n");
		} 
		
		sBuilder.append("public ");
		sBuilder.append(nBOMetaDataObj.isProxy() ? "interface " : "class ");
		sBuilder.append(nBOMetaDataObj.getFormattedEntity());
		if (nBOMetaDataObj.getExtend() != null) {
			sBuilder.append(" extends ").append(nBOMetaDataObj.getExtend())
				.append("<").append(pkTypeOfEntity).append(">");
		}
		// ... and implements
		if (nBOMetaDataObj.getImplements().size() > 0) {
			sBuilder.append(" implements ");
			Iterator<String> iterInterfaces = nBOMetaDataObj.getImplements().iterator();
			while (iterInterfaces.hasNext()) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				String next = iterInterfaces.next();
				if (LogicalDeletable.class.getSimpleName().equals(next)) {
					sBuilder.append(next).append("<").append(pkTypeOfEntity).append(">");
				} else if (Modifiable.class.getSimpleName().equals(next)) {
					sBuilder.append(next).append("<").append(pkTypeOfEntity).append(">");
				} else if (GenericBusinessObject.class.getSimpleName().equals(next)) {
					sBuilder.append(next).append("<").append(pkTypeOfEntity).append(">");
				} else if (Lockable.class.getSimpleName().equals(next)) {
					sBuilder.append(next).append("<").append(pkTypeOfEntity).append(">");
				} else {
					sBuilder.append(next);					
				}	
				if (iterInterfaces.hasNext())
					sBuilder.append(", ");
			}			
		}
		// Start Class-body
		sBuilder.append(" {\n");

		if (nBOMetaDataObj.getSerialVersionUID() != 0L) {
			sBuilder.append("private static final long serialVersionUID = ");
			sBuilder.append(Long.toString(nBOMetaDataObj.getSerialVersionUID()));
			sBuilder.append("L;\n\n");
		}
		
		if (nBOMetaDataObj.isMandator()) {
			sBuilder.append("\npublic interface NuclosMandatorProvider extends "
			                + "AbstractBusinessObject.NuclosMandatorProvider<")
				.append(nBOMetaDataObj.getFormattedEntity()).append(">{}\n\n");
		}
		
		// Constants 
		if (nBOMetaDataObj.getConstants().size() > 0)
			sBuilder.append(createConstants(nBOMetaDataObj.getConstants(), bIsForTemporarySystemBO));
		
		// Processes
		if (nBOMetaDataObj.getProcesses().size() > 0)
			sBuilder.append(createProcesses(nBOMetaDataObj.getProcesses()));
		
		// Attributes
		if (nBOMetaDataObj.getAttributes().size() > 0) {
			sBuilder.append(createAttributes(nBOMetaDataObj.getAttributes()));
		}
		
		if (LangUtils.equal(nBOMetaDataObj.getExtend(), AbstractBusinessObject.class.getSimpleName())) {
			sBuilder.append("\n\npublic ").append(nBOMetaDataObj.getFormattedEntity()).append("() {\n");
			sBuilder.append("\t\tsuper(\"").append(nBOMetaDataObj.getEntityUid().getString())
				.append("\");\n");
			// NUCLOS-6336
			for (NuclosBusinessObjectMethodMetaData md : nBOMetaDataObj.getMethods()) {
				if (md.isNotNullableBooleanSetter()) {
					sBuilder.append("\t\t" + md.getMethodName() + "(java.lang.Boolean.FALSE);\n");
				}
			}
			sBuilder.append("}\n");
		}
		
		// Methods
		if (nBOMetaDataObj.getMethods().size() > 0) 
			sBuilder.append(createMethods(nBOMetaDataObj.getMethods()));
			
		// End Class-body
		sBuilder.append(" }\n");
		return sBuilder.toString();
	}

	private static String createProcesses(List<NuclosBusinessObjectProcess> processes) {
		StringBuilder sBuilder = new StringBuilder();
		
		for (NuclosBusinessObjectProcess nboProcess : processes) {
			sBuilder
				.append("\npublic static final Process<")
				.append(nboProcess.getModuleName())
				.append("> ")
				.append(nboProcess.getProcessName()).append(" =")
				.append("\n\tnew Process(new ").append(UID.class.getCanonicalName())
				.append("(\"").append(nboProcess.getProcessId().toString()).append("\"),")
				.append("\n\t\tnew ").append(UID.class.getCanonicalName())
				.append("(\"").append(nboProcess.getModuleId().toString()).append("\"), ")
				.append(nboProcess.getModuleName()).append(".class);");
		}
		
		return sBuilder.toString();
	}
	
	private String createConstants(List<NuclosBusinessObjectConstantMetaData> constants, final boolean bIsForTemporarySystemBO) {
		StringBuilder sBuilder = new StringBuilder();
		
		for (NuclosBusinessObjectConstantMetaData constant : constants) {

			// Javadoc
			if (constant.getComment() != null)
				sBuilder.append(constant.getComment());
			
			String sAnnotations = "";
			for(NuclosBusinessObjectAttributeMetaData anno : constant.getAnnotations()) {
				sAnnotations += "\n@" + anno.getDataType();
			}
			
			String sAttName =  Character.isDigit(constant.getAttributeName().charAt(0)) ? "_" + constant.getAttributeName() : constant.getAttributeName();
			String type = bIsForTemporarySystemBO ? constant.getType() : UID.class.getCanonicalName().equals(constant.getType()) ? org.nuclos.api.UID.class.getCanonicalName() : constant.getType();
			String sParameter = "(\"" + constant.getAttributeName() + "\", \"" + constant.getPackageName() +
					"\", \"" + (constant.getEntityUid() == null ? null : constant.getEntityUid().getString()) + "\", \"" + (constant.getFieldUid() == null ? null : constant.getFieldUid().getString()) + "\", " + type + ".class)";
			String sParameterWithEntityAndFieldNames = "(\"" + constant.getAttributeName() + "\", \"" + constant.getPackageName()
					+ "\", \"" + constant.getEntityName() + "\", \"" + (constant.getEntityUid() == null ? null : constant.getEntityUid().getString())
						+ "\", \"" + constant.getFieldName() + "\", \"" + (constant.getFieldUid() == null ? null : constant.getFieldUid().getString()) + "\", " + type + ".class)";
			
			if (constant instanceof NuclosBusinessObjectDepConstantMetaData) {
				sBuilder.append("\npublic static final Dependent<" + type + "> " + sAttName + " = \n\tnew Dependent<>" + sParameterWithEntityAndFieldNames + ";\n");
			}
			else {
				if (constant.isPrimKey()) {
					sBuilder.append(sAnnotations + "\npublic static final PrimaryKeyAttribute<" + type + "> " + sAttName + " = \n\tnew PrimaryKeyAttribute<>" + sParameter + ";\n");
				}
				else if (constant.isForeignKey()) {
					sBuilder.append(sAnnotations + "\npublic static final ForeignKeyAttribute<" + type + "> " + sAttName + " = \n\tnew ForeignKeyAttribute<>" + sParameter + ";\n");
				}
				else {
					if ( Date.class.getCanonicalName().equals(constant.getType()) || 
							Long.class.getCanonicalName().equals(constant.getType()) || 
							Double.class.getCanonicalName().equals(constant.getType()) ||
							BigDecimal.class.getCanonicalName().equals(constant.getType()) ||
							Integer.class.getCanonicalName().equals(constant.getType())) {
						sBuilder.append(sAnnotations + "\npublic static final NumericAttribute<" + type + "> " + sAttName + " = \n\tnew NumericAttribute<>" + sParameter + ";\n");
					}
					else if(InternalTimestamp.class.getCanonicalName().equals(constant.getType())) {
						 sParameter = "(\"" + constant.getAttributeName() + "\", \"" + constant.getPackageName() + "\", \"" + constant.getEntityUid().getString() + "\", \"" + constant.getFieldUid().getString() + "\", " + Date.class.getCanonicalName() + ".class)";
						sBuilder.append(sAnnotations + "\npublic static final NumericAttribute<" + Date.class.getCanonicalName() + "> " + sAttName + " = new NumericAttribute<>" + sParameter + ";\n");
					}
					else if(String.class.getCanonicalName().equals(constant.getType())) {
						sBuilder.append(sAnnotations + "\npublic static final StringAttribute<" + constant.getType() + "> " + sAttName + " = new StringAttribute<>" + sParameter + ";\n");
					}
					else {
						sBuilder.append(sAnnotations + "\npublic static final Attribute<" + type + "> " + sAttName + " = \n\tnew Attribute<>" + sParameter + ";\n");
					}
				}
			}
		}
		
		return sBuilder.toString();
	}

	/**
	 * This method evaluates all methods and creates a String that represents the JavaCode of the given methods
	 * @param methods
	 * @return
	 */
	private static String createMethods(List<NuclosBusinessObjectMethodMetaData> methods) {
		StringBuilder sBuilder = new StringBuilder();
		
		for (NuclosBusinessObjectMethodMetaData method : methods) {
			
			// Javadoc
			if (method.getComment() != null)
				sBuilder.append(method.getComment());
			
			// Check Annotation
			for(NuclosBusinessObjectAttributeMetaData anno : method.getAnnotations()) {
				sBuilder.append("\n@" + anno.getDataType());
			}
			
			if (method.getReturnType() == null) {
				sBuilder.append("\n" + method.getAccessType() + " " + method.getMethodName());				
			}
			else {
				if (InternalTimestamp.class.getCanonicalName().equals(method.getReturnType())) {
					sBuilder.append("\n" + method.getAccessType() + " java.util.Date " + method.getMethodName());
				}
				else {
					sBuilder.append("\n" + method.getAccessType() + " " + method.getReturnType() + " " + method.getMethodName());					
				}
			}
			
			if (method.getParameter().size() > 0) {
				sBuilder.append("(");
				Iterator<NuclosBusinessObjectAttributeMetaData> iterParams = method.getParameter().iterator();
				while (iterParams.hasNext()) {
					NuclosBusinessObjectAttributeMetaData next = iterParams.next();
					if (InternalTimestamp.class.getCanonicalName().equals(next.getDataType())) {
						sBuilder.append(" java.util.Date " + next.getAttributeName());
					}
					else {
						sBuilder.append(extractDataType(next.getDataType()) + " " + next.getAttributeName());						
					}
					if (iterParams.hasNext())
						sBuilder.append(", ");
				}

				sBuilder.append(")");
			}
			else {
				sBuilder.append("()");
			}
			
			if (method.getThrows().size() > 0) {
				sBuilder.append(" throws ");
				Iterator<String> iterThrows = method.getThrows().iterator();
				while (iterThrows.hasNext()) {
					sBuilder.append(iterThrows.next());						
					if (iterThrows.hasNext())
						sBuilder.append(", ");
				}
			}
			
			if (method.isInterface()) {
				sBuilder.append(";");
			} else {
				sBuilder.append(" {\n\t").append(method.getMethodBody()).append("\n}\n");
			}
		}
		
		return sBuilder.toString();
	}

	public static String extractDataType(String dataType) {
		String retVal = dataType;
		try {
			Class<?> forName = LangUtils.getClassLoaderThatWorksForWebStart().loadClass(dataType);
			if (forName.isArray() && forName.getComponentType().isPrimitive()) {
				retVal = forName.getComponentType().getCanonicalName() + "[]";
			}
			else if (forName.getCanonicalName().equals(Double.class.getCanonicalName())) {
				retVal = BigDecimal.class.getCanonicalName();
			}
			else if (forName.getCanonicalName().equals(NuclosImage.class.getCanonicalName())) {
				retVal = org.nuclos.api.NuclosImage.class.getCanonicalName();
			}
			else if (forName.getCanonicalName().equals(NuclosPassword.class.getCanonicalName())) {
				retVal = String.class.getCanonicalName();
			}
		} catch (Exception e) {}
		
		return retVal;
	}

	/**
	 * This method evaluates all attributes and creates a String that represents the JavaCode of the given attributes
	 * 
	 * @param attributes
	 * @return
	 */
	private static String createAttributes(List<NuclosBusinessObjectAttributeMetaData> attributes) {
		StringBuilder sBuilder = new StringBuilder();
		
		for (NuclosBusinessObjectAttributeMetaData attribute : attributes) {

			if (attribute.getComment() != null) {
				sBuilder.append("\n" + attribute.getComment());
			}
			sBuilder.append("\nprivate " + extractDataType(attribute.getDataType()) + " " + attribute.getAttributeName() + ";\n");
		}
		return sBuilder.toString();
	}
}
