//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.console.ejb3;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.nuclos.businessentity.facade.WebAddonFacade;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.CommandMessage;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.MandatorLevelVO;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.Priority;
import org.nuclos.common.RuleNotification;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbIndex;
import org.nuclos.server.dbtransfer.TransferFacadeBean;
import org.nuclos.server.documentfile.DocumentFileFacadeLocal;
import org.nuclos.server.documentfile.DocumentFileUtils;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeRemote;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.livesearch.ejb3.LiveSearchFacadeLocal;
import org.nuclos.server.maintenance.MaintenanceFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.masterdata.ejb3.MetaDataFacadeLocal;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for all NuclosConsole functions.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = {Exception.class})
@RolesAllowed("UseManagementConsole")
public class ConsoleFacadeBean extends NuclosFacadeBean implements ConsoleFacadeLocal, ConsoleFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(ConsoleFacadeBean.class);

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	@Autowired
	private DocumentFileFacadeLocal documentFileFacade;

	@Autowired
	private TransferFacadeBean nucletTransferFacade;

	@Autowired
	private EventSupportFacadeRemote eventSupportFacade;

	@Autowired
	private SecurityCache securityCache;

	@Autowired
	private MetaProvider meta;

	@Autowired
	private NucletDalProvider nucletDal;

	@Autowired
	private LiveSearchFacadeLocal liveSearchFacade;

	@Autowired
	private WebAddonFacade webAddonFacade;

	@Autowired
	private MaintenanceFacadeBean maintenanceFacade;

	public ConsoleFacadeBean() {
	}

	/**
	 * @param sMessage the message to send
	 * @param sUser    the receiver of this message (all users if null)
	 * @param priority
	 * @param sAuthor  the author of the message
	 */
	public void sendClientNotification(String sMessage, String sUser, Priority priority, String sAuthor) {
		LOG.info("JMS send client notification to user {}: {}: {}", sUser, sMessage, this);
		final RuleNotification rn = new RuleNotification(priority, sMessage, sAuthor);
		if (sUser != null) {
			NuclosJMSUtils.sendObjectMessageAfterCommit(
					rn, JMSConstants.TOPICNAME_RULENOTIFICATION, sUser);
		} else {
			NuclosJMSUtils.sendOnceAfterCommitDelayed(rn, JMSConstants.TOPICNAME_RULENOTIFICATION);
		}
	}

	/**
	 * end all the clients of sUser
	 *
	 * @param sUser if null for all users
	 */
	public void killSession(String sUser) {
		LOG.info("JMS send killSession {}: {}", sUser, this);
		final CommandMessage cm = new CommandMessage(CommandMessage.CMD_SHUTDOWN_NOW);
		if (sUser != null) {
			NuclosJMSUtils.sendObjectMessageAfterCommit(
					cm, JMSConstants.TOPICNAME_RULENOTIFICATION, sUser);
		} else {
			NuclosJMSUtils.sendOnceAfterCommitDelayed(
					cm, JMSConstants.TOPICNAME_RULENOTIFICATION);
		}
	}

	/**
	 * check for VIEWS and FUNCTIONS which are invalid and compile them
	 *
	 * @throws SQLException
	 */
	public void compileInvalidDbObjects() throws SQLException {
		LOG.info("compiling invalid db objects (views and functions)");
		dataBaseHelper.getDbAccess().validateObjects();
	}

	/**
	 * invalidateAllServerSide Caches
	 */
	public void invalidateAllCaches() {
		nucletTransferFacade.revalidateCaches();
	}

	@Override
	@RolesAllowed("Super-User")
	public void setDevelopmentEnvironment(boolean bDevelopment) {
		if (ApplicationProperties.getInstance().isFunctionBlockDev()) {
			NuclosSystemParameters.setDevelopmentEnvironment(bDevelopment);
		} else {
			throw new IllegalStateException("no Functionblock.Dev");
		}
	}

	@Override
	@RolesAllowed("Super-User")
	public void setProductionEnvironment(boolean bProduction) {
		if (ApplicationProperties.getInstance().isFunctionBlockDev()) {
			NuclosSystemParameters.setProductionEnvironment(bProduction);
		} else {
			throw new IllegalStateException("no Functionblock.Dev");
		}
	}

	/**
	 * get Infomation about the database in use
	 */
	public String getDatabaseInformationAsHtml() {
		final StringBuilder sb = new StringBuilder("<b>Database Meta Information</b><br>");
		sb.append("<HTML><table border=\"1\">");

		for (Map.Entry<String, Object> e : dataBaseHelper.getDbAccess().getMetaDataInfo().entrySet()) {
			sb.append(String.format("<tr><td><b>%s</b></td><td>%s</td></tr>", e.getKey(), e.getValue()));
		}

		sb.append("</table><br><b>Vendor specific parameters:<br><table border=\"1\">");
		final Map<String, String> mpDbParameters = dataBaseHelper.getDbAccess().getDatabaseParameters();
		for (String sParameter : mpDbParameters.keySet()) {
			sb.append("<tr><td><b>").append(sParameter).append("</b></td><td>")
					.append(mpDbParameters.get(sParameter)).append("</td></tr>");
		}
		sb.append("</table></html>");
		return sb.toString();
	}

	/**
	 * get the system properties of the server
	 */
	public String getSystemPropertiesAsHtml() {
		final StringBuilder sbClient = new StringBuilder();
		sbClient.append("<html><b>Java System Properties (Server):</b>");
		sbClient.append("<table border=\"1\">");
		for (final Object key : System.getProperties().keySet()) {
			sbClient.append("<tr><td><b>").append((String) key).append("</b></td><td>")
					.append(System.getProperty((String) key)).append("</td></tr>");
		}
		sbClient.append("</table></html>\n");
		return sbClient.toString();
	}

	@Override
	public String[] rebuildConstraints() {
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		List<String> result = new ArrayList<String>();
		for (DbConstraint c : getConstraints(dbAccess)) {
			createOrDropConstraints(Collections.singletonList(c), false, dbAccess, result);
			createOrDropConstraints(Collections.singletonList(c), true, dbAccess, result);

		}
		return result.toArray(new String[]{});
	}

	@Override
	public String[] rebuildIndexes() {
		PersistentDbAccess dbAccess = new PersistentDbAccess(dataBaseHelper.getDbAccess());
		List<String> result = new ArrayList<String>();
		for (DbIndex c : getIndexes(dataBaseHelper.getDbAccess())) {
			createOrDropIndexes(Collections.singletonList(c), false, dbAccess, result);
			createOrDropIndexes(Collections.singletonList(c), true, dbAccess, result);

		}
		return result.toArray(new String[]{});
	}

	public String[] setMandatorLevel(String[] args) {
		Map<String, UID> mapNucletByPackage = getMapNucletByPackage();
		List<String> result = new ArrayList<String>();

		List<String> lstArgs = new ArrayList<String>();
		for (String arg : args) {
			arg = arg.trim();
			if (arg.contains("\n")) {
				for (String cut : arg.split("\n")) {
					lstArgs.add(cut.trim());
				}
			} else {
				lstArgs.add(arg);
			}
		}

		Iterator<String> itArgs = lstArgs.iterator();
		boolean nextPackage = false;

		try {
			while (itArgs.hasNext() || nextPackage) {

				String arg = itArgs.next();
				if ("-package".equals(arg) || nextPackage) {
					Map<String, String> mapArgValues = new HashMap<String, String>();
					if (nextPackage) {
						nextPackage = false;
						mapArgValues.put("-package", arg);
					} else {
						mapArgValues.put("-package", itArgs.next());
					}
					mapArgValues.put(itArgs.next(), itArgs.next());
					mapArgValues.put(itArgs.next(), itArgs.next());
					if (itArgs.hasNext()) {
						String sNext = itArgs.next();
						if ("-package".equals(sNext)) {
							nextPackage = true;
						} else {
							if ("-bo".equals(sNext) ||
									"-level".equals(sNext) ||
									"-initial".equals(sNext)) {
								mapArgValues.put(sNext, itArgs.next());
							}
						}
					}

					String sPackage = mapArgValues.get("-package");
					UID nucletUID = mapNucletByPackage.get(sPackage);
					if (nucletUID == null) {
						result.add("Nuclet with package \"" + sPackage + "\" does not exist!");
						continue;
					}

					String sBusinessObjectName = mapArgValues.get("-bo");
					EntityMeta<?> eMeta = null;
					for (UID eUID : meta.getEntities(nucletUID)) {
						EntityMeta<?> em = meta.getEntity(eUID);
						if (em.getEntityName().equals(sBusinessObjectName)) {
							eMeta = em;
						}
					}
					if (eMeta == null) {
						result.add("Business object \"" + sBusinessObjectName + "\" in nuclet \"" + sPackage + "\" does not exist!");
						continue;
					}

					String sLevel = mapArgValues.get("-level");
					int level = 0;
					try {
						level = Integer.parseInt(sLevel);
					} catch (Exception ex) {
						result.add("Level \"" + sLevel + "\" is not a number. Error: " + ex.getMessage());
						continue;
					}
					List<MandatorLevelVO> allMandatorLevelsSorted = securityCache.getAllMandatorLevels();
					if (level > allMandatorLevelsSorted.size()) {
						result.add("Level \"" + sLevel + "\" does not exist!");
						continue;
					}
					MandatorLevelVO mandatorLevelVO = null;
					if (level > 0) {
						mandatorLevelVO = allMandatorLevelsSorted.get(level - 1);
					}

					String sInitialMandatorPath = mapArgValues.get("-initial");
					MandatorVO initialMandatorVO = null;
					if (sInitialMandatorPath != null) {
						for (MandatorVO mandatorVO : securityCache.getAllMandators()) {
							if (sInitialMandatorPath.equals(mandatorVO.getPath())) {
								if (initialMandatorVO != null) {
									result.add("Mandator with path \"" + sInitialMandatorPath + "\" is not unique!");
									continue;
								}
								initialMandatorVO = mandatorVO;
							}
						}
						if (initialMandatorVO == null) {
							result.add("Initial mandator with path \"" + sInitialMandatorPath + "\" does not exist!");
							continue;
						}
					}

					result.add(setMandatorLevel(sPackage, eMeta, sLevel, mandatorLevelVO, initialMandatorVO));
				}
			}
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			result.add("ERROR: " + ex.getMessage());
		}

		return result.toArray(new String[]{});
	}

	private BidiMap<String, UID> getMapNucletByPackage() {
		BidiMap<String, UID> result = new DualHashBidiMap<String, UID>();

		for (EntityObjectVO<UID> nucletEO : nucletDal.getEntityObjectProcessor(E.NUCLET).getAll()) {
			result.put(nucletEO.getFieldValue(E.NUCLET.packagefield), nucletEO.getPrimaryKey());
		}

		return result;
	}

	/**
	 * for testing only! (only Superusers)
	 */
	@Override
	public String[] resetCompleteMandatorSetup() {
		if (!securityCache.isSuperUser(getCurrentUserName())) {
			throw new NuclosFatalException("Not allowed (only Superusers)");
		}
		List<String> result = new ArrayList<String>();
		try {
			BidiMap<String, UID> mapNucletByPackage = getMapNucletByPackage();

			Collection<EntityMeta<?>> allEntities = meta.getAllEntities();
			for (EntityMeta<?> eMeta : allEntities) {
				if (!eMeta.isMandator()) {
					continue;
				}
				String sPackage = NuclosBusinessObjectBuilder.DEFFAULT_PACKAGE_NUCLET;
				if (eMeta.getNuclet() != null) {
					sPackage = mapNucletByPackage.getKey(eMeta.getNuclet());
				}
				result.add(setMandatorLevel(sPackage, eMeta, "-1", null, null));
			}

			result.add(truncateTable(E.MANDATOR_ROLE_USER));
			result.add(truncateTable(E.MANDATOR_USER));
			result.add(truncateTable(E.MANDATOR_PARAMETERVALUE));
			result.add(truncateTable(E.MANDATOR_RESOURCE));
			result.add(truncateTable(E.MANDATOR_ACCESSIBLE));
			result.add(truncateTable(E.MANDATOR, E.MANDATOR.parentMandator, UID.class));
			result.add(truncateTable(E.MANDATOR_LEVEL, E.MANDATOR_LEVEL.parentLevel, UID.class));

		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			result.add("ERROR: " + ex.getMessage());
		}
		return result.toArray(new String[]{});
	}

	/**
	 * for testing only! (only Superusers)
	 * switch lucene indexing on or off
	 *
	 * @param enabled
	 */
	public void setIndexerEnabled(boolean enabled, boolean synchronous) {
		if (!securityCache.isSuperUser(getCurrentUserName())) {
			throw new NuclosFatalException("Not allowed (only Superusers)");
		}
		liveSearchFacade.setIndexerEnabled(enabled, synchronous);
	}

	/**
	 * for testing only! (only Superusers)
	 * rebuild Lucene Index
	 */
	public void rebuildLuceneIndex() {
		if (!securityCache.isSuperUser(getCurrentUserName())) {
			throw new NuclosFatalException("Not allowed (only Superusers)");
		}
		liveSearchFacade.rebuildLuceneIndex();
	}

	private <PK> String truncateTable(EntityMeta<PK> systemEntity) {
		return truncateTable(systemEntity, null, null);
	}

	@SuppressWarnings("unchecked")
	private <PK> String truncateTable(EntityMeta<PK> systemEntity, FieldMeta<PK> recursivParentForeignKeyField, Class<PK> pkClass) {
		IEntityObjectProcessor<PK> proc = nucletDal.getEntityObjectProcessor(systemEntity);
		List<EntityObjectVO<PK>> all = new ArrayList<>(proc.getAll());
		while (!all.isEmpty()) {
			Set<PK> parentPKs = new HashSet<>();
			if (recursivParentForeignKeyField != null) {
				for (EntityObjectVO<PK> eo : all) {
					PK parent = null;
					if (pkClass.equals(UID.class)) {
						parent = (PK) eo.getFieldUid((FieldMeta<UID>) recursivParentForeignKeyField);
					} else {
						parent = (PK) eo.getFieldId((FieldMeta<Long>) recursivParentForeignKeyField);
					}
					if (parent != null) {
						parentPKs.add(parent);
					}
				}
			}
			Iterator<EntityObjectVO<PK>> it = all.iterator();
			while (it.hasNext()) {
				EntityObjectVO<PK> eo = it.next();
				PK pk = eo.getPrimaryKey();
				if (parentPKs.isEmpty() || !parentPKs.contains(pk)) {
					it.remove();
					Delete<PK> del = new Delete<>(pk);
					proc.delete(del);
				}
			}
		}
		return "truncate table " + systemEntity.getDbTable() + ": " + all.size() + " rows";
	}

	private String setMandatorLevel(String sPackage, EntityMeta<?> eMeta, String sLevel, MandatorLevelVO mandatorLevelVO, MandatorVO initialMandatorVO) throws NuclosCompileException, InterruptedException {
		String result = sPackage + "." + eMeta.getEntityName() + " -> ";
		if (mandatorLevelVO != null) {
			if (mandatorLevelVO.getName() != null) {
				sLevel = sLevel + ":" + mandatorLevelVO.getName();
			}
			result = result + sLevel;
			if (initialMandatorVO != null) {
				result = result + " with initial mandator " + initialMandatorVO.getPath();
			}
		} else {
			result = result + "NO mandator (level)";
		}

		MetaDataFacadeLocal metaFacade = SpringApplicationContextHolder.getBean(MetaDataFacadeLocal.class);
		try {
			metaFacade.setMandatorLevel(eMeta.getUID(),
					mandatorLevelVO == null ? null : mandatorLevelVO.getUID(),
					initialMandatorVO == null ? null : initialMandatorVO.getUID());
			result = result + " DONE";
		} catch (NuclosBusinessException e) {
			result = result + " ERROR: " + e.getMessage();
		}

		invalidateAllCaches();
		rebuildAllClasses();

		return result;
	}

	public String[] checkDocumentAttachments(boolean deleteUnusedFiles, Set<String> deleteEntityObjects) {
		final List<String> result = new ArrayList<String>();
		final Set<File> foundFiles = new HashSet<File>();
		MetaProvider mdprov = SpringApplicationContextHolder.getBean(MetaProvider.class);
		MasterDataRestFqnCache fqnCache = SpringApplicationContextHolder.getBean(MasterDataRestFqnCache.class);
		for (EntityMeta<?> entityMeta : mdprov.getAllEntities()) {
			if (E.isNuclosEntity(entityMeta.getUID()) && !E.GENERALSEARCHDOCUMENT.checkEntityUID(entityMeta.getUID())) {
				continue;
			}
			String fqn = fqnCache.translateUid(E.ENTITY, entityMeta.getUID());
			Set<FieldMeta<?>> docFields = new HashSet<FieldMeta<?>>();
			for (FieldMeta<?> fieldMeta : mdprov.getAllEntityFieldsByEntity(entityMeta.getUID()).values()) {
				if (fieldMeta.getDataType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile")) {
					docFields.add(fieldMeta);
				}
			}
			if (!docFields.isEmpty()) {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<DbTuple> query = builder.createTupleQuery();
				DbFrom<?> from = query.from(entityMeta);
				List<DbSelection<?>> select = new ArrayList<DbSelection<?>>();
				select.add(from.basePk());
				boolean where = false;
				for (FieldMeta<?> fieldMeta : docFields) {
					DbField<?> dbRefColumn = SimpleDbField.createRef(fieldMeta.getDbColumn(), true);
					select.add(from.baseColumn(dbRefColumn));
					if (where) {
						query.addToWhereAsOr(builder.isNotNull(from.baseColumn(dbRefColumn)));
					} else {
						query.where(builder.isNotNull(from.baseColumn(dbRefColumn)));
						where = true;
					}
				}
				query.multiselect(select);

				dataBaseHelper.getDbAccess().executeQuery(query, new CheckDocumentAttachmentTransformer(entityMeta, fqn, docFields, deleteEntityObjects, foundFiles, result, documentFileFacade));
			}
		}

		File baseDir = new File(NuclosSystemParameters.getString(NuclosSystemParameters.DOCUMENT_PATH));
		checkDocumentAttachmentDir(baseDir, foundFiles, deleteUnusedFiles, result);

		return result.toArray(new String[]{});
	}

	private void checkDocumentAttachmentDir(final File dir, final Set<File> foundFiles, boolean deleteUnusedFiles, final List<String> result) {
		for (File file : dir.listFiles()) {
			if (file.isHidden()) {
				// ignore hidden "system" files
				continue;
			}
			if (file.isFile()) {
				if (!foundFiles.contains(file)) {
					String r = "NOT USED: " + file.getAbsolutePath();
					if (deleteUnusedFiles) {
						try {
							if (file.delete()) {
								r = r + " FILE DELETED!";
							} else {
								r = r + " FILE NOT DELETED!";
							}
						} catch (Exception ex) {
							r = r + " FILE NOT DELETED: " + ex.getMessage();
							LOG.error(r, ex);
						}
					}
					result.add(r);
					LOG.info(r);

				}
			} else {
				checkDocumentAttachmentDir(file, foundFiles, deleteUnusedFiles, result);
			}
		}
	}


	/**
	 * Find and remove duplicate document files
	 */
	public String cleanupDuplicateDocuments() {
		return DocumentFileUtils.cleanupDuplicateDocuments();
	}

	private static class CheckDocumentAttachmentTransformer implements Transformer<DbTuple, Void> {

		private final EntityMeta<?> entityMeta;
		private final String fqn;
		private final Set<FieldMeta<?>> docFields;
		private final Set<String> deleteEntityObjects;
		private final Set<File> foundFiles;
		private final List<String> result;
		private DocumentFileFacadeLocal documentFileFacade;

		public CheckDocumentAttachmentTransformer(EntityMeta<?> entityMeta, String fqn, Set<FieldMeta<?>> docFields, Set<String> deleteEntityObjects, Set<File> foundFiles,
												  List<String> result, DocumentFileFacadeLocal documentFileFacade) {
			super();
			this.entityMeta = entityMeta;
			this.fqn = fqn;
			this.docFields = docFields;
			this.deleteEntityObjects = deleteEntityObjects;
			this.foundFiles = foundFiles;
			this.result = result;
			this.documentFileFacade = documentFileFacade;
		}

		@Override
		public Void transform(DbTuple tuple) {
			Object pk = tuple.get(entityMeta.getPk());

			for (FieldMeta<?> fieldMeta : docFields) {
				DbField<?> dbRefColumn = SimpleDbField.createRef(fieldMeta.getDbColumn(), true);
				UID documentFileUID = (UID) tuple.get(dbRefColumn);
				if (documentFileUID != null) {
					String fileName = documentFileFacade.getFileName(documentFileUID);
					File documentFile = DocumentFileUtils.getDocumentFile(documentFileUID, fileName);
					if (documentFile.exists()) {
						foundFiles.add(documentFile);

						// test for a valid link
						final UID docFileUIDfromLink = documentFileFacade.findDocumentFileUID(fieldMeta.getUID(), pk);
						if (docFileUIDfromLink == null) {
							documentFileFacade.createDocumentFileLink(documentFileUID, fieldMeta.getUID(), pk);
							String r = fqn + "(" + pk + ") - LINK DOES NOT EXIST, CREATED A NEW ONE!";
							result.add(r);
							LOG.info(r);
						}

					} else {
						String r = fqn + "(" + pk + ") - NOT FOUND: " + fileName;
						if (deleteEntityObjects.contains(fqn)) {
							try {
								EntityObjectFacadeLocal eoFacade = SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class);
								eoFacade.delete(entityMeta.getUID(), pk, false, false);
								r = r + " RECORD " + pk + " DELETED!";
							} catch (Exception ex) {
								r = r + " RECORD " + pk + " NOT DELETED: " + ex.getMessage();
								LOG.error(r, ex);
							}
						}
						result.add(r);
						LOG.info(r);
					}
				}
			}

			return null;
		}
	}

	@Override
	public void rebuildAllClasses() throws NuclosCompileException, InterruptedException {
		eventSupportFacade.createBusinessObjects(true);
	}

	@Override
	public void syncActiveWebAddons() {
		webAddonFacade.syncActiveWebAddons();
	}
	
	public String getMaintenanceMode() {
		return maintenanceFacade.getMaintenanceMode();
	}

	public String startMaintenance() {
		return maintenanceFacade.enterMaintenanceMode(getCurrentUserName());
	}

	public void stopMaintenance() throws CommonValidationException {
		maintenanceFacade.exitMaintenanceMode();
	}

	public void clearUserPreferences(String userName) throws CommonFinderException {
		EntityObjectFacadeLocal eoFacade = SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class);
		eoFacade.clearUserPreferences(userName);
	}
}
