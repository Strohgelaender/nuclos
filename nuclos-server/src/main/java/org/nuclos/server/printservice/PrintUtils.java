package org.nuclos.server.printservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.fusesource.hawtbuf.ByteArrayInputStream;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.exception.CommonPrintException;
import org.odftoolkit.odfdom.doc.OdfTextDocument;

public class PrintUtils {

	/**
	 * Transforms the given file to PDF.
	 * Supports DOCX and ODT.
	 * PDFs are returned without manipulation.
	 * 
	 * Only checks the file name to find out what to do.
	 * 
	 * @param file
	 * @return
	 * @throws CommonPrintException
	 */
	public static NuclosFile toPdf(NuclosFile file) throws CommonPrintException {
		final String filenameLC = file.getName().toLowerCase();
		if (filenameLC.endsWith(".pdf")) {
			return file;
		}
		final String pdfFilename;
		byte[] bytes = file.getContent();
		try {
			if (filenameLC.endsWith(".docx")) {
				XWPFDocument document = new XWPFDocument(new ByteArrayInputStream(bytes));
				org.apache.poi.xwpf.converter.pdf.PdfOptions options = org.apache.poi.xwpf.converter.pdf.PdfOptions.create();
				File tempFile = File.createTempFile(filenameLC + ".pdf", "");
				tempFile.deleteOnExit();
				OutputStream out = new FileOutputStream(tempFile);
				org.apache.poi.xwpf.converter.pdf.PdfConverter.getInstance().convert(document, out, options);
				bytes = IOUtils.readFromBinaryFile(tempFile);
				tempFile.delete();
				pdfFilename = file.getName().substring(0, file.getName().length() - 4) + "pdf";
			} else if (filenameLC.endsWith(".odt")) {
				OdfTextDocument document = OdfTextDocument.loadDocument(new ByteArrayInputStream(bytes));
				org.odftoolkit.odfdom.converter.pdf.PdfOptions options = org.odftoolkit.odfdom.converter.pdf.PdfOptions.create();
				File tempFile = File.createTempFile(filenameLC + ".pdf", "");
				tempFile.deleteOnExit();
				OutputStream out = new FileOutputStream(tempFile);
				org.odftoolkit.odfdom.converter.pdf.PdfConverter.getInstance().convert(document, out, options);
				bytes = IOUtils.readFromBinaryFile(tempFile);
				tempFile.delete();
				pdfFilename = file.getName().substring(0, file.getName().length() - 3) + "pdf";
			} else {
				throw new CommonPrintException("Print of " + file.getName() + " is not supported (only .pdf, .docx and .odt)!");
			}
			return new org.nuclos.common.NuclosFile(pdfFilename, bytes);
		} catch (FileNotFoundException e) {
			throw new CommonPrintException("Print of " + file.getName() + " to pdf failed: " + e.getMessage(), e);
		} catch (IOException e) {
			throw new CommonPrintException("Print of " + file.getName() + " to pdf failed: " + e.getMessage(), e);
		} catch (Exception e) {
			throw new CommonPrintException("Print of " + file.getName() + " to pdf failed: " + e.getMessage(), e);
		}
	}
	
}
