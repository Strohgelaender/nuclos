//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import java.util.List;

import org.nuclos.api.context.PrintResult;
import org.nuclos.server.printservice.exception.PrintSpoolerJobException;

/**
 * {@link PrintJob} for {@link PrintSpooler}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface PrintJob {
	
	/**
	 * describes the current state of the {@link PrintJob}
	 *
	 */
	public static enum Mode {
		INITIALIZED, // PrintJob is initialized
		SCHEDULED,   // PrintJob is scheduled by the PrintSpooler
		RUNNING,	 // PrintJob is executed by the PrintSpooler
		DONE,		 // PrintJob has finished execution without any error
		INTERRUPTED	 // PrintJob has was interrupted
	}
	
	/**
	 * get assigned {@link Printout}
	 * 
	 * @return list of {@link Printout}
	public <T extends Printout> List<T> getPrintouts();
	 */
	
	/**
	 * get assigned {@link PrintResult}
	 * 
	 * @return list of {@link PrintResult}
	 */
	public <T extends PrintResult> List<T> getPrintresults();
	
	/**
	 * get {@link Mode}
	 * 
	 * @return {@link Mode}
	 */
	public Mode getMode();

	/**
	 * set {@link Mode}
	 * 
	 * @param mode {@link Mode}
	 */
	void setMode(Mode mode);

	/**
	 * execute job (establish the actual printing process)
	 * 
	 * @throws PrintSpoolerJobException
	 */
	void execute() throws PrintSpoolerJobException;
}
