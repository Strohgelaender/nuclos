//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import java.util.List;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.api.printout.Printout;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.api.rule.PrintRule;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.printservice.PrintExecutionContext;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintResultTO;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common2.exception.CommonPrintException;

public interface PrintServiceFacadeLocal {

	/**
	 * provides printout by executing user defined {@link PrintRule}
	 * 
	 * @param usagecriteria	{@link UsageCriteria}
	 * @param pk primary key for {@link BusinessObject}	
	 * @return list of {@link PrintoutTO}
	 */
	public List<PrintoutTO> printoutsForBO(UsageCriteria usagecriteria, Long pk);

	/**
	 * provides available {@link PrintService}
	 * 
	 * @return list of {@link PrintServiceTO}
	 */
	public List<PrintServiceTO> printServices();

	/**
	 * provide {@link PrintService} by id
	 * 
	 * @param idPrintservice {@link PrintService} id
	 * 
	 * @return {@link PrintServiceTO}
	 */
	public PrintServiceTO printService(final UID idPrintservice);
	
	/**
	 * execute {@link OutputFormat}
	 * 
	 * @param outputFormat	{@link OutputFormatTO}
	 * @param idBo {@link BusinessObject} id
	 * @return	{@link PrintResultTO}
	 * @throws CommonPrintException 
	 */
	public PrintResultTO executeOutputFormat(final OutputFormatTO outputFormat, final Long idBo) throws CommonPrintException;
	
	/**
	 * execute preview for {@link Printout}
	 * 
	 * @param printouts list of {@link PrintoutTO}
	 * @param context {@link PrintExecutionContext}
	 * 
	 * @return list of {@link PrintResultTO} (printed files)
	 * @throws CommonPrintException 
	 */
	public List<PrintResultTO> executePreview(final List<PrintoutTO> printouts) throws CommonPrintException;
	
	/**
	 * execute {@link Printout}
	 * 
	 * @param printouts {@link PrintoutTO}
	 * @param context {@link PrintExecutionContext}
	 * 
	 * @throws CommonPrintException 
	 */
	public List<PrintResultTO> executePrintout(final List<PrintoutTO> printouts, final PrintExecutionContext context) throws CommonPrintException;
	
	/**
	 * print via print spooler.
	 * supports pdf, docx and odt.
	 * 
	 * @param file
	 * @param printService
	 * @throws CommonPrintException
	 */
	public void printFile(final NuclosFile file, javax.print.PrintService printService) throws CommonPrintException;
	
	/**
	 * print via print spooler.
	 * supports pdf, docx and odt.
	 * 
	 * @param file
	 * @param printServiceId
	 * @throws CommonPrintException
	 */
	public void printFile(final NuclosFile file, UID printServiceId) throws CommonPrintException;
	
	/**
	 * print via print spooler.
	 * supports pdf, docx and odt.
	 * 
	 * @param file
	 * @param properties
	 * @throws CommonPrintException
	 */
	public void printFile(final NuclosFile file, PrintProperties properties) throws CommonPrintException;
	
}
