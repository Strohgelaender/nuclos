//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.api.context.PrintResult;
import org.nuclos.common2.exception.PrintoutException;
import org.nuclos.server.printservice.exception.PrintSpoolerException;
import org.nuclos.server.printservice.printout.PrintoutServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * {@link DefaultPrintSpooler} for {@link PrintJob} management
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
@Component
@Scope("singleton")
public class DefaultPrintSpooler implements PrintSpooler {
	private final static Logger LOG = LoggerFactory.getLogger(DefaultPrintSpooler.class);
	private Mode mode;
	int maxThreads;
	
	
	@Autowired
	private PrintoutServiceProvider psFacade;
	
	@Autowired
	private PrintJobExecutionStrategy executionStrategy;
	
	/**
	 * create new {@link DefaultPrintSpooler}
	 */
	@Autowired
	public DefaultPrintSpooler(PrintJobExecutionStrategy executionStrategy) {
		this.executionStrategy = executionStrategy;
		this.mode = Mode.STARTUP;
	}
	
	@PostConstruct
	public void postConstruct() {
		LOG.info("PrintSpooler initialized");
		try {
			this.switchToMode(Mode.RUNNING);
		} catch (final PrintSpoolerException ex) {
			LOG.error(ex.getMessage(), ex);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.server.printservice.PrintSpooler#schedule(List<org.nuclos.server.printservice.PrintJob>,org.nuclos.server.printservice.PrintSpoolerScheduleContext)
	 */
	
	//public void schedule(final List<PrintJob> printJobs, final PrintSpoolerScheduleContext context) throws PrintSpoolerException {
	@Override
	public <T extends PrintResult> void schedule(List<T> printResults,
			PrintSpoolerScheduleContext context) throws PrintoutException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("schedule list of {} printResults", printResults.size());
		}
		
		executionStrategy.offerJob(printResults);
	
		for (final PrintResult printResult : printResults) {
		}
	}

	/* (non-Javadoc)
	 * @see org.nuclos.server.printservice.PrintSpooler#cancel(org.nuclos.server.printservice.PrintJob)
	 */
	@Override
	public boolean cancel(final PrintJob printJob) {
		throw new NotImplementedException("cancel printJob not implemented yet");
	}

	
	/* (non-Javadoc)
	 * @see org.nuclos.server.printservice.PrintSpooler#getMode()
	 */
	@Override
	public Mode getMode() {
		return this.mode;
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.server.printservice.PrintSpooler#switchToMode(org.nuclos.server.printservice.DefaultPrintSpooler.Mode)
	 */
	@Override
	public void switchToMode(final Mode mode) throws PrintSpoolerException {
		this.mode = mode;
	}

	@Override
	public PrintJobExecutionStrategy getExecutionStrategy() {
		return this.executionStrategy;
	}

}
