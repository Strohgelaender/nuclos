//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice.printout;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Example:
 * 
* <pre>
 * {@code
 * package org.nuclet.printout;
 * 
 * import org.nuclos.api.printout.Printout;
 * import org.nuclos.common.UID;
 * import org.nuclos.api.report.OutputFormat;
 * import org.nuclos.server.nbo.AbstractPrintout;
 * import org.nuclos.server.nbo.AbstractOutputFormat;
 * import java.io.Serializable;
 * 
 * 
 * public class FormularLieferscheinPO extends AbstractPrintout implements Serializable {
 * 
 *     public static final OutputFormat CSV_Erstellung = new AbstractOutputFormat(UID.parseUID("TbbmD96JIXWMal9solWd")){};
 *     public static final OutputFormat PDF_Erstellung_LS = new AbstractOutputFormat(UID.parseUID("BJImQBDVwY4H0rumovh5")){};
 * 
 * 
 *     public org.nuclos.api.UID getId() {
 *         return UID.parseUID("xV7job3gGm0b7st6wiLO");
 *     }
 * }
 * </pre>
*/
@Component("printoutObjectBuilder")
public class PrintoutObjectBuilder extends NuclosObjectBuilder {

	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.printout";
	private static final String DEFFAULT_ENTITY_PREFIX = "PO";
	private static final String DEFFAULT_ENTITY_POSTFIX = "PO";

	@Autowired
	private PrintoutObjectCompiler compiler;
	
	@Autowired
	private MetaProvider provider;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	public void createObjects() throws NuclosCompileException, InterruptedException {
		List<NuclosBusinessJavaSource> retVal = new ArrayList<>();

		CollectableSearchExpression clctExpr = new CollectableSearchExpression(SearchConditionUtils.newComparison(E.REPORT.withRuleClass, ComparisonOperator.EQUAL, Boolean.TRUE));
		// get all reports and report outputs:
		List<EntityObjectVO<UID>> allReport = nucletDalProvider
				.getEntityObjectProcessor(E.REPORT).getBySearchExpression(clctExpr);
		List<UID> allReportIds = new ArrayList<>();
		for (EntityObjectVO<UID> report : allReport) {
			allReportIds.add(report.getPrimaryKey());
		}
		if (Thread.currentThread().isInterrupted()) {
			throw new InterruptedException();
		}
		CollectableSearchExpression clctExprOutputs = new CollectableSearchExpression(new CollectableInIdCondition<UID>(SearchConditionUtils.newEntityField(E.REPORTOUTPUT.parent), allReportIds));
		List<EntityObjectVO<UID>> allOutputs = nucletDalProvider
			.getEntityObjectProcessor(E.REPORTOUTPUT).getBySearchExpression(clctExprOutputs);
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputsMap =
			new MultiListHashMap<>(allReport.size());
		for (EntityObjectVO<UID> output : allOutputs) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			reportOutputsMap.addValue(output.getFieldUid(E.REPORTOUTPUT.parent), output);
		}
				
		if (allReport.size() > 0) {
			for (EntityObjectVO<UID> eoVO : allReport) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				DefaultReportVO sourceVO = MasterDataWrapper.getReportVO(new MasterDataVO<>(eoVO, false), "INITIAL", null);
				if (ReportType.FORM.equals(sourceVO.getType()))
						retVal.add(createJavaSourceFile(sourceVO, eoVO.getFieldUid(E.REPORT.nuclet), reportOutputsMap));
			}
		}
		
		if (retVal.size() > 0) {
			compiler.compileSourcesAndJar(retVal);
		}
	}
	
	private NuclosBusinessJavaSource createJavaSourceFile(
		DefaultReportVO model,
		UID nucletUID,
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputsMap) {
		final String sPackage = getNucletPackageStatic(nucletUID, provider);
		final String formatEntity = getNameForFqn(model.getName());
		final String qname = sPackage + "." + formatEntity;
		final String filename = NuclosCodegeneratorUtils.printoutSource(sPackage, formatEntity).toString();
		final String content = createJavaSourceContent(sPackage, formatEntity, model, reportOutputsMap);
		
		return new NuclosBusinessJavaSource(qname, filename, content, true);
	}

	private String createJavaSourceContent(
		String sPackage,
		String formatEntity,
		DefaultReportVO model,
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputsMap) {
		StringBuilder template = new StringBuilder();
		template.append("package ").append(sPackage).append(";\n\n");
		
		template.append("import org.nuclos.api.printout.Printout;\n");
		template.append("import org.nuclos.common.UID;\n");
		template.append("import org.nuclos.api.report.OutputFormat;\n");
		template.append("import org.nuclos.server.nbo.AbstractPrintout;\n");
		template.append("import org.nuclos.server.nbo.AbstractOutputFormat;\n\n");
		template.append("import java.io.Serializable;\n");
			
		template.append("public class ").append(formatEntity)
			.append(" extends AbstractPrintout implements Serializable {\n\n");
		
		template.append(createJavaFileContent(model, reportOutputsMap));
		
		template.append("\n\n}");
		return template.toString();
	}

	private String createJavaFileContent(
		DefaultReportVO model,
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputsMap) {
		StringBuilder s = new StringBuilder();
		
		visitOutputFormats(model.getId(), new OutputFormatCodeBuilderVisitor(s), reportOutputsMap);
	
		s.append("\n\npublic org.nuclos.api.UID getId() {\n\treturn UID.parseUID(\"")
			.append(model.getId().getString()).append("\"); \n}\n");
		
		return s.toString();
	}
	
	public static void visitOutputFormats(UID reportUID, OutputFormatVisitor visitor) {
		visitOutputFormats(reportUID, visitor, null);
	}
	
	public static void visitOutputFormats(
		UID reportUID,
		OutputFormatVisitor visitor,
		MultiListMap<UID, EntityObjectVO<UID>> reportOutputsMap) {
		List<EntityObjectVO<UID>> allReports;
		if (reportOutputsMap != null) {
			allReports = reportOutputsMap.getValues(reportUID);
		} else {
			allReports = NucletDalProvider.getInstance().getEntityObjectProcessor(E.REPORTOUTPUT).
					getBySearchExpression(new CollectableSearchExpression(
							SearchConditionUtils.newUidComparison(E.REPORTOUTPUT.parent, ComparisonOperator.EQUAL, reportUID)));
		}
		
		Map<String, Integer> lstUsedReportNames = new HashMap<>();
		
		allReports = CollectionUtils.sorted(allReports, new Comparator<EntityObjectVO<UID>>() {
			@Override
			public int compare(EntityObjectVO<UID> o1, EntityObjectVO<UID> o2) {
				return StringUtils.compare(o1.getPrimaryKey().getString(), o2.getPrimaryKey().getString());
			}
		});
		
		for (EntityObjectVO<UID> eoVo : allReports) {
			DefaultReportOutputVO reportOutputVO = MasterDataWrapper.getReportOutputVO(new MasterDataVO<UID>(eoVo, true));
			boolean serverSideSupport;
			switch (reportOutputVO.getFormat()) {
			case PDF:
			case CSV:
			case TSV:
			case XLS:
			case XLSX:
			case DOC:
			case DOCX:
				serverSideSupport = true;
				break;
			default:
				serverSideSupport = false;
			}
			if (serverSideSupport) {
				
				String formatName = validateOutputFormatName(reportOutputVO.getDescription());
				Integer formatNumber = null;
				
				if (StringUtils.looksEmpty(formatName)) {
					continue;
				}
				
				if (lstUsedReportNames.containsKey(formatName)) {
					formatNumber = lstUsedReportNames.get(formatName) + 1;
					lstUsedReportNames.put(formatName, formatNumber);
				}
				else {
					lstUsedReportNames.put(formatName, 1);
				}
				String formatNumberAsString = formatNumber != null ? "_" + formatNumber : "";
				visitor.visitOutputFormat(reportOutputVO.getId(), formatName + formatNumberAsString);
			}
		}
	}
	
	public static interface OutputFormatVisitor {
		void visitOutputFormat(UID outputFormatUID, String fullyQualifiedName);
	}
	
	private static class OutputFormatCodeBuilderVisitor implements OutputFormatVisitor {

		private final StringBuilder s;
		
		public OutputFormatCodeBuilderVisitor(StringBuilder s) {
			this.s = s;
		}
		
		@Override
		public void visitOutputFormat(UID outputFormatUID, String fullyQualifiedName) {
			s.append("public static final OutputFormat ").append(fullyQualifiedName)
				.append(" = new AbstractOutputFormat(UID.parseUID(\"")
				.append(outputFormatUID.getString())
				.append("\")){};\n");
		}
		
	}
	
	public static String getNameForFqn(String sName) {
		return formatMethodName(NuclosEntityValidator.escapeJavaIdentifier(sName, DEFFAULT_ENTITY_PREFIX)) + DEFFAULT_ENTITY_POSTFIX;
	}
	
	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}
	
	protected static String validateOutputFormatName(String value) {
		String retVal = value;
		
		retVal = retVal.replace("ä", "ae");
		retVal = retVal.replace("ü", "ue");
		retVal = retVal.replace("ö", "oe");
		retVal = retVal.replace("Ä", "Ae");
		retVal = retVal.replace("Ü", "Ue");
		retVal = retVal.replace("Ö", "Oe");
		retVal = retVal.replace("ß", "ss");
		
		retVal = retVal.replaceAll("[^a-zA-Z ]+","");
		retVal = retVal.replaceAll(" ","_");
			
		return retVal;
	}
	
}
