package org.nuclos.server.resource;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Oliver Brausch on 18.09.17.
 */
public class LocaleResourceCleanser {

	public static void main(String[] args) throws IOException {
		String path = args.length > 0 ? args[0] : "/tmp";

		Set<String> hash = new HashSet<>();
		FileReader fr = new FileReader(path + "/localeresource.json");
		FileWriter fw = new FileWriter(path + "/localeresource_cleansed.json");
		boolean inblock = false;
		List<String> block = new ArrayList<>();
		String blockString = "";

		BufferedReader br = new BufferedReader(fr);
		for (;;) {
			String line = br.readLine();
			if (line == null) {
				break;
			}

			String strim = line.trim();
			if (strim.equals("{")) {
				inblock = true;
				blockString = "";
				block.clear();
			}

			if (inblock) {
				block.add(line);
				blockString += strim;
			} else {
				fw.write(line + "\n");
				System.out.println(line);
				continue;
			}

			if (strim.equals("},") || strim.equals("}")) {
				inblock = false;
				String blockString2 = blockString.replaceFirst("locale\": \"en", "locale\": \"i-default");
				if (!hash.contains(blockString) && !hash.contains(blockString2)) {
					for (String b : block) {
						fw.write(b + "\n");
						System.out.println(b);
					}
					hash.add(blockString);
				}

			}
		}
		fw.close();
		br.close();
		fr.close();;
	}

}
