package org.nuclos.server.attribute.ejb3;

import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.nbo.NuclosObjectBuilder;

public class LayoutObjectBuilder extends NuclosObjectBuilder {

	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.layout";
	
	private static String DEFAULT_PREFIX = "Lo";
	private static String DEFAULT_POSTFIX = "LO";
	
	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}
	
	public static String getNameForFqn(String sName) {
		return formatMethodName(NuclosEntityValidator.escapeJavaIdentifier(sName, DEFAULT_PREFIX)) + DEFAULT_POSTFIX;
	}

	@Override
	protected void createObjects() throws CommonBusinessException {}
	
}
