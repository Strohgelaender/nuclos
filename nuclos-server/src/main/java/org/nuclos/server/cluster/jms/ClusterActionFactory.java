//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.jms;

import org.nuclos.server.cluster.ClusterNode;

/*
 * CLUSTERING
 * Factory creates one of the Action for the other cluster node's
 * action.doAction() were execute on the other cluster node's 
 */
public abstract class ClusterActionFactory {
	
	public static NuclosClusterAction createClusterAction(int action, Object... obj) {
		
		switch (action) {
		case NuclosClusterAction.PARAMTER_ACTION:
			return new ParameterClusterAction();
		case NuclosClusterAction.RESOURCE_ACTION:
			return new ResourceClusterAction();
		case NuclosClusterAction.STATE_ACTION:
			return new StateClusterAction();			
		case NuclosClusterAction.ATTRIBUTE_ACTION:
			return new AttributeClusterAction();
		case NuclosClusterAction.STATEMODELUSAGE_ACTION:
			return new StateModelUsageClusterAction();
		case NuclosClusterAction.SCHEMA_ACTION:
			return new SchemaClusterAction();
		case NuclosClusterAction.RULE_ACTION:
			return new RuleClusterAction();
		case NuclosClusterAction.MASTERDATAMETA_ACTION:
			return new MasterDataMetaClusterAction();
		case NuclosClusterAction.DATASOURCE_ACTION:
			return new DatasourceClusterAction();
		case NuclosClusterAction.SECURITY_ACTION:
			return new SecurityClusterAction();		
		case NuclosClusterAction.LOCALE_ACTION:
			return new LocaleClusterAction((Boolean)(obj[0]));
		case NuclosClusterAction.MODULES_ACTION:
			return new ModulesClusterAction();
		case NuclosClusterAction.STARTUP_ACTION:
			return new StartupClusterAction((String) obj[0], (String)obj[1]);
		case NuclosClusterAction.SERVERKEEPALIVE_ACTION:
			return new ServerKeepAliveClusterAction();
		case NuclosClusterAction.SHUTDOWN_ACTION:
			return new ShutdownClusterAction((String) obj[0]);
		case NuclosClusterAction.RAISETOMASTER_ACTION:
			return new RaiseNodeToMasterClusterAction((ClusterNode) obj[0], (Boolean)(obj[1]));
		case NuclosClusterAction.SPRINGCACHE_ACTION:
			return new SpringCacheClusterAction((String)obj[0], obj[1], (Boolean)obj[2]);
		case NuclosClusterAction.METAPROVIDER_ACTION:
			return new MetaProviderClusterAction();
			
		default:
			break;
		}
		
		// dummy action
		return new NuclosClusterAction() {
			
			@Override
			public boolean doOnMaster() {
				return false;
			}
			
			@Override
			public void doAction() {
				// do nothing dummy action				
			}
		};
		
	}

}
