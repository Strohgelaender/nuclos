package org.nuclos.server.cluster.jms;

import org.nuclos.server.common.AttributeCache;

public class AttributeClusterAction implements NuclosClusterAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6737087530293741695L;

	@Override
	public void doAction() {
		AttributeCache.getInstance().invalidateCache(true, false);
	}

	@Override
	public boolean doOnMaster() {
		return false;
	}

}
