package org.nuclos.server.cluster.jms;

import java.io.Serializable;

/*
 * all action to be executed on a cluster node
 * implement this interface
 */
public interface NuclosClusterAction extends Serializable {
	
	public static int PARAMTER_ACTION = 0;
	
	public static int STATE_ACTION = 1;
	
	public static int RESOURCE_ACTION = 2;
	
	public static int ATTRIBUTE_ACTION = 3;
	
	public static int STATEMODELUSAGE_ACTION = 4;
	
	public static int SCHEMA_ACTION = 5;
	
	public static int RULE_ACTION = 6;
	
	public static int MASTERDATAMETA_ACTION = 7;
	
	public static int DATASOURCE_ACTION = 8;
	
	public static int SECURITY_ACTION = 9;
	
	public static int LOCALE_ACTION = 10;
	
	public static int MODULES_ACTION = 11;
	
	public static int METADATA_ACTION = 12;
	
	public static int LUCENIAN_ACTION = 13;
	
	public static int STARTUP_ACTION = 14;
	
	public static int SERVERKEEPALIVE_ACTION = 15;
	
	public static int SHUTDOWN_ACTION = 16;
	
	public static int RAISETOMASTER_ACTION = 17;
	
	public static int SPRINGCACHE_ACTION = 18;
	
	public static int METAPROVIDER_ACTION = 19;
	
	public void doAction();
	
	public boolean doOnMaster();

}
