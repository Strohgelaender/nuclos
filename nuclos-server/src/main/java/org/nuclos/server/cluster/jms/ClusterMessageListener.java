//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.cluster.ActionHandler;
import org.nuclos.server.cluster.ActionHandlerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * CLUSTERING
 * Listener for all incoming cluster jms messages
 */
public class ClusterMessageListener implements MessageListener {

	private static final Logger LOG = LoggerFactory.getLogger(ClusterMessageListener.class);
	

	@Override
	public void onMessage(Message message) {
		if(message instanceof ObjectMessage) {
			ObjectMessage oMessage = (ObjectMessage)message;
			try {
				ClusterPropertiesContextHolder holder = SpringApplicationContextHolder
					.getBean(ClusterPropertiesContextHolder.class);
				holder.put("");
				Object messageObject = oMessage.getObject();				
				ActionHandler handler = ActionHandlerFactory
					.createActionHandler((NuclosClusterAction)messageObject);
				LOG.info("{}", messageObject);
				handler.handleAction();
				holder.clear();
				
			} catch (JMSException e) {
				LOG.error("Unable to process message:", e);
			}			
			
		}
	}
	
}
