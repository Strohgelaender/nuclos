//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.jnlp;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.Mutable;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.startup.NuclosEnviromentConstants;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Servlet for dynamic jnlp generation.
 * The codebase for the webstart client is /app.
 *
 * The servlet uses /WEB-INF/jnlp/jnlp.xsl as generation template and /WEB-INF/jnlp/jnlp.properties as a source for parameters.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class JnlpServlet extends AbstractWebstartServlet {

	private static final Logger LOG = LoggerFactory.getLogger(JnlpServlet.class);
	
	//private static final Pattern NUCLOS_CLIENT_PAT = Pattern.compile("^nuclos-client-[0-9\\.]+(-r[0-9\\.]+(.*)?)?\\.jar");
	
	private static final SortedSet<String> LAZY_LIBS;

	private static final String CONTENT_TYPE_JNLP = "application/x-java-jnlp-file";
	
	static {
		final SortedSet<String> l = new TreeSet<String>();
		// should be lazy.txt - but is temporary disabled (tp)
		final InputStream ins = LangUtils.getClassLoaderThatWorksForWebStart().getResourceAsStream("jnlp/lazy2.txt");
		BufferedReader in = null;
		try {
			if (ins != null) {
				in = new BufferedReader(new InputStreamReader(ins, "UTF8"));
				String line;
				do {
					line = in.readLine();
					if (line != null)  {
						line = line.trim();
						if (!"".equals(line) && !line.startsWith("#")) {
							l.add(line);
						}
					}
				} while(line != null);
			}
		}
		catch (IOException e) {
			// ignore
			LOG.warn("Loading of lazy.properties failed: {}", e.toString());
		}
		finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (IOException e) {
					// ignore
				}
			}
		}
		LAZY_LIBS = Collections.unmodifiableSortedSet(l);
	}
	
	//

	private boolean singleinstance = false;

	private Map<String, String> extensionsVersion;
	private Map<String, Long> extensionsLastModified;
	
	private boolean hasCustomerIcon = false;
	private boolean hasSplashScreen = false;

	private Map<String, String> themesVersion;
	private Map<String, Long> themesLastModified;
	
	private long nuclosJnlpLastModified;
	private String nuclosJnlpVersion;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		try {
			Properties props = ServerProperties.loadProperties(ServerProperties.JNDI_SERVER_PROPERTIES);
			singleinstance = Boolean.parseBoolean(props.getProperty("client.singleinstance"));

			final DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			nuclosJnlpLastModified = 0l;
			extensionsVersion = new HashMap<String, String>();
			extensionsLastModified = new HashMap<String, Long>();
			themesVersion = new HashMap<String, String>();
			themesLastModified = new HashMap<String, Long>();
			
			Set<Entry<String, JarDownloadResource>> jarEntries = getCodebaseUriToResource().entrySet();
			for (Entry<String, JarDownloadResource> entry : jarEntries) {
				final JarDownloadResource jarResource = entry.getValue();
				final File f = jarResource.getFile();
				final String filename = f.getName();
				final long lastModified = f.lastModified();
				if (jarResource.isExtension() || jarResource.isTheme()) {
					LOG.debug("Found client extension/theme jar: {}; LastModified: {}",
							filename, df.format(new Date(lastModified)));
					String jarName = jarResource.getJarName();
					String name = jarName.substring(0, jarName.length() - 4);
					if (jarResource.isExtension()) {
						extensionsLastModified.put(name, lastModified);
						extensionsVersion.put(name, df.format(new Date(lastModified)));
					} else if (jarResource.isTheme()) {
						themesLastModified.put(name, lastModified);
						themesVersion.put(name, df.format(new Date(lastModified)));
					}
				}
				if (nuclosJnlpLastModified < lastModified) {
					nuclosJnlpLastModified = lastModified;
				}
			}
			nuclosJnlpVersion = df.format(new Date(nuclosJnlpLastModified));
			
			if (new File(appDir, "customer-icon.gif").exists()) {
				hasCustomerIcon = true;
			}
			if (new File(appDir, "splash-screen.gif").exists()) {
				hasSplashScreen = true;
			}
		}
		catch (Exception e) {
			LOG.error("Failed to initialize JnlpServlet.", e);
		}
	}

	public static boolean isExtensionRequest(HttpServletRequest request) {
		boolean isExtensionRequest = Pattern.matches(".*extension[^/]*\\.jnlp", request.getRequestURI());
		return isExtensionRequest;
	}
	
	public static boolean isThemeRequest(HttpServletRequest request) {
		boolean isThemeRequest = Pattern.matches(".*theme[^/]*\\.jnlp", request.getRequestURI());
		return isThemeRequest;
	}
	
	@Override
	protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String requestURI = request.getRequestURI();
		LOG.debug("Handling request head: " + requestURI);
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		
		Mutable<String> jnlpName = new Mutable<String>();
		Mutable<String> jnlpVersion = new Mutable<String>();
		Mutable<Long> jnlpLastModified = new Mutable<Long>();
		generateJnlp(request, bytes, jnlpName, jnlpVersion, jnlpLastModified);
		
		response.setContentType(CONTENT_TYPE_JNLP);
        response.setContentLength(bytes.size());
        if (jnlpVersion.getValue() != null) {
            response.setHeader(JNLP_VERSION_ID, jnlpVersion.getValue());
        }
        if (jnlpLastModified.getValue() != 0) {
            response.setDateHeader(LAST_MODIFIED, jnlpLastModified.getValue());
        }
        response.sendError(HttpServletResponse.SC_OK);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Mutable<String> jnlpName = new Mutable<String>();
		Mutable<String> jnlpVersion = new Mutable<String>();
		Mutable<Long> jnlpLastModified = new Mutable<Long>();
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		generateJnlp(request, bytes, jnlpName, jnlpVersion, jnlpLastModified);
		
		long ifModifiedSince = request.getDateHeader("If-Modified-Since");
		
		if (ifModifiedSince != -1 && jnlpLastModified.getValue() != 0 && 
                (ifModifiedSince / 1000) >=
                (jnlpLastModified.getValue() / 1000)) {
            // see Sample Jnlp Download Servlet for more information (/1000)
            response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
		} else {
			
			String attachment = "inline; filename=\"" + jnlpName.getValue() + "\"";
			response.setContentType(CONTENT_TYPE_JNLP);
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Content-disposition", attachment);
	        response.setContentLength(bytes.size());
	        if (jnlpVersion.getValue() != null) {
	            response.setHeader(JNLP_VERSION_ID, jnlpVersion.getValue());
	        }
	        if (jnlpLastModified.getValue() != 0) {
	            response.setDateHeader(LAST_MODIFIED, jnlpLastModified.getValue());
	        }

	        response.getOutputStream().write(bytes.toByteArray());
	        response.getOutputStream().flush();
	        response.getOutputStream().close();
		}
	}
	
	private void generateJnlp(HttpServletRequest request, ByteArrayOutputStream writeTo, Mutable<String> jnlpName, Mutable<String> jnlpVersion, Mutable<Long> lastModified) throws ServletException {
		final boolean isExtensionRequest = isExtensionRequest(request);
		final boolean isThemeRequest = isThemeRequest(request);
		
		if (isExtensionRequest || isThemeRequest) {
			jnlpName.setValue(request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/") + 1));
		}
		else {
			jnlpName.setValue("nuclos.jnlp");
		}
		
		String serverBaseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
				+ request.getContextPath();

		String restBaseUrl = serverBaseUrl + NuclosEnviromentConstants.REST_ENDING;

		Properties props = new Properties();
		// enable pack200 - see http://docs.oracle.com/javase/6/docs/technotes/guides/jweb/tools/pack200.html
		props.put("jnlp.packEnabled", "true");
		// http://docs.oracle.com/javase/7/docs/technotes/guides/jweb/riaJnlpProps.html
		props.put("jnlp.concurrentDownloads", "2");
		
		props.put(NuclosEnviromentConstants.SERVER_VARIABLE, serverBaseUrl);
		props.put(NuclosEnviromentConstants.REST_VARIABLE, restBaseUrl);
		props.put("singleinstance", Boolean.toString(singleinstance));
		props.put("nuclos.version", ApplicationProperties.getInstance().getNuclosVersion().getVersionNumber());
		//props.put("extensions", Boolean.toString(hasExtensions));
		props.put("max-heap-size", getSystemParameter(ParameterProvider.KEY_CLIENT_MAX_HEAP_SIZE, "512m"));
		props.put("customerIcon", Boolean.toString(hasCustomerIcon));
		props.put("splashScreen", Boolean.toString(hasSplashScreen));
		//if (hasExtensions) {
		//	props.put("extension-version", extensionVersion);
		//}
		
		InputStream is;
		if (isExtensionRequest) {
			is = LangUtils.getClassLoaderThatWorksForWebStart().getResourceAsStream("jnlp/extension.jnlp.xsl");
		}
		else if (isThemeRequest) {
			is = LangUtils.getClassLoaderThatWorksForWebStart().getResourceAsStream("jnlp/theme.jnlp.xsl");
		}
		else {
			is = LangUtils.getClassLoaderThatWorksForWebStart().getResourceAsStream("jnlp/jnlp.xsl");
		}

		try {
			Document source;
			if (isExtensionRequest) {
				String extension = jnlpName.getValue().substring(10, jnlpName.getValue().length() - 5);
				props.put("codebase", serverBaseUrl + NuclosEnviromentConstants.CODEBASE_ENDING + "/extensions");
				props.put("href", serverBaseUrl + NuclosEnviromentConstants.CODEBASE_ENDING + "/extensions/" + jnlpName.getValue());
				props.put("version", extensionsVersion.get(extension));
				jnlpVersion.setValue(extensionsVersion.get(extension));
				lastModified.setValue(extensionsLastModified.get(extension));
				source = getExtTransformationSource(extension);
			}
			else if (isThemeRequest) {
				String theme = jnlpName.getValue().substring(6, jnlpName.getValue().length() - 5);
				props.put("codebase", serverBaseUrl + NuclosEnviromentConstants.CODEBASE_ENDING + "/extensions/themes");
				props.put("href", serverBaseUrl + NuclosEnviromentConstants.CODEBASE_ENDING + "/extensions/themes/" + jnlpName.getValue());
				props.put("version", themesVersion.get(theme));
				jnlpVersion.setValue(themesVersion.get(theme));
				lastModified.setValue(themesLastModified.get(theme));
				source = getThemeTransformationSource(theme);
			}
			else {
				StringBuilder sbParameters = null;
				for (Map.Entry<String, String[]> e : request.getParameterMap().entrySet()) {
					if (sbParameters == null) {
						sbParameters = new StringBuilder();
						sbParameters.append("?");
					} else {
						sbParameters.append("&");
					}
					sbParameters.append(e.getKey() + (e.getValue() != null ? "=" + (e.getValue())[0] : ""));
				}

				props.put("codebase", serverBaseUrl + NuclosEnviromentConstants.CODEBASE_ENDING);
				props.put("href", serverBaseUrl + NuclosEnviromentConstants.CODEBASE_ENDING + "/webstart.jnlp" + (sbParameters != null ? sbParameters.toString() : ""));
				props.put("version", nuclosJnlpVersion);
				jnlpVersion.setValue(nuclosJnlpVersion);
				lastModified.setValue(nuclosJnlpLastModified);
				source = getTransformationSource(request.getParameterMap());
			}

			TransformerFactory transformFactory = TransformerFactory.newInstance();
			final Transformer transformer = transformFactory.newTransformer(new StreamSource(is));
			
			for (Map.Entry<Object, Object> e : props.entrySet()) {
				transformer.setParameter((String) e.getKey(), e.getValue());
			}

			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			Result output = new StreamResult(writeTo);
			transformer.setErrorListener(new ErrorListener() {
				@Override
				public void error(TransformerException exception) throws TransformerException {
					LOG.error("error: {}", exception, exception);
				}
				
				@Override
				public void fatalError(TransformerException exception) throws TransformerException {
					if (getSocketExceptionIfAny(exception) == null) {
						Marker fatal = MarkerFactory.getMarker("FATAL");
						LOG.error(fatal, "fatalError: {}", exception, exception);
					} 
					// else ...
					// ... 'Broken pipe'
					// Webstart scheint die Verbindung abzubrechen, wenn kein Update nötig ist.	
				}
				
				@Override
				public void warning(TransformerException exception) throws TransformerException {
					LOG.warn("warning: {}", exception, exception);
				}
			});
			
			transformer.transform(new DOMSource(source), output);
			String jnlpOutput = new String(writeTo.toByteArray(), StandardCharsets.UTF_8);
			Pattern patHref = Pattern.compile(".*?<jnlp.*?href=\"(.*?)\".*?>.*", Pattern.DOTALL);
			Matcher matHref = patHref.matcher(jnlpOutput);
			if (matHref.matches()) {
				int start = matHref.start(1);
				int end = matHref.end(1);
				String href = matHref.group(1);
				href = href.replace("?", "&#063;");
				href = href.replace("&amp;", "&");
				jnlpOutput = (new StringBuilder(jnlpOutput)).replace(start,end,href).toString();
			}
			writeTo.reset();
			writeTo.write(jnlpOutput.getBytes(StandardCharsets.UTF_8));
		}
		catch (Exception ex) {
			throw new ServletException(ex);
		}
	}
	
	private SocketException getSocketExceptionIfAny(Exception ex) {
		if (ex instanceof SocketException) {
			return (SocketException) ex;
		}
		if (ex.getCause() != null && ex.getCause() instanceof Exception) {
			return getSocketExceptionIfAny((Exception) ex.getCause());
		}
		return null;
	}

	private Document getTransformationSource(Map<?, ?> parameters) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();

		Node jnlp = document.appendChild(document.createElement("jnlp"));
		Node jars = jnlp.appendChild(document.createElement("jars"));

		List<JarDownloadResource> jarResources = new ArrayList<JarDownloadResource>(getCodebaseUriToResource().values());
		Collections.sort(jarResources);
		for (JarDownloadResource jarResource : jarResources) {
			if (jarResource.getJarName().startsWith("nuclos-native-jacob-")) {
				// constant in jnlp.xsl
				continue;
			}
			if (jarResource.isExtension() || jarResource.isTheme()) {
				continue;
			}
			Element jar = (Element) jars.appendChild(document.createElement("jar"));
			jar.setTextContent(jarResource.getJarName());
			jar.setAttribute("version", jarResource.getJnlpVersionInfo());
			jar.setAttribute("download", getDownloadAttrValue(jarResource.getJarName()));
//			final Matcher m = NUCLOS_CLIENT_PAT.matcher(jarResource.getJarName());
//			jar.setAttribute("main", Boolean.toString(m.matches()));
			jar.setAttribute("main", Boolean.toString("nuclos-client.jar".equalsIgnoreCase(jarResource.getJarName())));
		}
		
		Node arguments = jnlp.appendChild(document.createElement("arguments"));

		// convert map-like request parameters to program-arguments
		for (Map.Entry<?, ?> e : parameters.entrySet()) {
			Node argument = document.createElement("argument");
			argument.setTextContent(e.getKey() + (e.getValue() != null ? "=" + ((String[]) e.getValue())[0] : ""));
			arguments.appendChild(argument);
		}

		Node extensions = jnlp.appendChild(document.createElement("extensions"));
		if (this.extensionsVersion != null) {
			for (String extension : this.extensionsVersion.keySet()) {
				Element extensionNode = document.createElement("extension");
				extensionNode.setAttribute("name", extension);
				extensionNode.setAttribute("version", this.extensionsVersion.get(extension));
				extensions.appendChild(extensionNode);
			}
		}

		Node themes = jnlp.appendChild(document.createElement("themes"));
		if (this.themesVersion != null) {
			for (String theme : this.themesVersion.keySet()) {
				Element themeNode = document.createElement("theme");
				themeNode.setAttribute("name", theme);
				themeNode.setAttribute("version", this.themesVersion.get(theme));
				themes.appendChild(themeNode);
			}
		}

		return document;
	}

	private Document getExtTransformationSource(final String extension) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();

		Node jnlp = document.appendChild(document.createElement("jnlp"));
		Node jars = jnlp.appendChild(document.createElement("jars"));
		Node natives = jnlp.appendChild(document.createElement("native"));
		
		List<JarDownloadResource> jarResources = new ArrayList<JarDownloadResource>(getCodebaseUriToResource().values());
		Collections.sort(jarResources);
		for (JarDownloadResource jarResource : jarResources) {
			if (jarResource.isExtension() && jarResource.getJarName().equals(extension + ".jar")) {
				Element jar;
				if (jarResource.isNative()) {
					jar = (Element) natives.appendChild(document.createElement("jar"));
				} else {
					jar = (Element) jars.appendChild(document.createElement("jar"));
				}
				jar.setTextContent(jarResource.getJarName());
				jar.setAttribute("version", jarResource.getJnlpVersionInfo());
				jar.setAttribute("download", getDownloadAttrValue(jarResource.getJarName()));
			}
		}

		return document;
	}

	private Document getThemeTransformationSource(String theme) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();

		List<JarDownloadResource> jarResources = new ArrayList<JarDownloadResource>(getCodebaseUriToResource().values());
		Collections.sort(jarResources);
		for (JarDownloadResource jarResource : jarResources) {
			if (jarResource.isTheme() && jarResource.getJarName().equals(theme + ".jar")) {
				Element jar = (Element) document.appendChild(document.createElement("jar"));
				jar.setTextContent(jarResource.getJarName());
				jar.setAttribute("version", jarResource.getJnlpVersionInfo());
				jar.setAttribute("download", getDownloadAttrValue(jarResource.getJarName()));
				break;
			}
		}
		
		return document;
	}
	
	private String getDownloadAttrValue(String lib) {
		String result = "eager";
		for (String s: LAZY_LIBS) {
			if (lib.startsWith(s)) {
				result = "lazy";
				break;
			}
			/*
			if (lib.compareTo(s) > 0) {
				break;
			}
			 */
		}
		return result;
	}
	
	private String getSystemParameter(String parameterName, String defaultValue) throws ServletException {
		ServerParameterProvider instance = ServerParameterProvider.getInstance();
		while (instance == null) {
			// Server start...
			try {
				Thread.sleep(1000);
				instance = ServerParameterProvider.getInstance();
			} catch (InterruptedException e) {
				LOG.error(e.getMessage(), e);
				break;
			}
		}
		if (instance == null) {
			// Server start...
			throw new UnavailableException("Server is not ready, please try again later.", 60);
		}
		String value = instance.getValue(parameterName);
		if (value == null) {
			value = defaultValue;
		}
		return value;
	}
	
}
