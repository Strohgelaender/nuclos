package org.nuclos.server.jnlp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.codec.digest.DigestUtils;
import org.nuclos.common.startup.NuclosEnviromentConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractWebstartServlet extends HttpServlet {
	
	private static final Logger LOG = LoggerFactory.getLogger(AbstractWebstartServlet.class);
	
	public static final String JNLP_VERSION_ID = "x-java-jnlp-version-id";
	
	public static final String LAST_MODIFIED = "Last-Modified";
	
	public static final String CONTENT_ENCODING = "content-encoding";
	
	public static final String CONTENT_ENCODING_PACK200_GZIP = "pack200-gzip";
	
	public static final String JAR_PACK_GZ_FILE_SUFFIX = ".jar.pack.gz";
	public static final String JAR_FILE_SUFFIX = ".jar";
	public static final String[] JAR_FILE_SUFFIXES = new String[]{JAR_PACK_GZ_FILE_SUFFIX, JAR_FILE_SUFFIX};
	
	protected File appDir = null;
	
	protected static Map<String, JarDownloadResource> codebaseUriToResource;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		appDir = new File(config.getServletContext().getRealPath(""), "app");
		// Uncomment and change the next line for developing only!
		// appDir = new File("C:\\nuclos\\workspace\\webapp\\app");

		fillCaches();
	}
	
	private synchronized void fillCaches() {
		if (codebaseUriToResource == null) {
			try {
				Map<String, JarDownloadResource> newCodebaseUriToResourceMap = new HashMap<String, JarDownloadResource>();
				File[] appJars = appDir.listFiles(new JarFileFilter());
				for (File jar : appJars) {
					if (jar.isFile()) {
						JarDownloadResource jarResource = newJarDownloadResource(jar);
						newCodebaseUriToResourceMap.put(NuclosEnviromentConstants.CODEBASE_ENDING + "/" + jarResource.getJarName(), jarResource);
					}
				}
	
				File extensionDir = new File(appDir, "extensions");
				if (extensionDir.isDirectory()) {
					String[] files = extensionDir.list(new JarFileFilter());
					if (files.length > 0) {
						for (String filename : files) {
							File f = new File(extensionDir, filename);
							if (f.isFile()) {
								JarDownloadResource jarResource = newJarDownloadResource(f);
								jarResource.setExtension(true);
								newCodebaseUriToResourceMap.put(NuclosEnviromentConstants.CODEBASE_ENDING + "/extensions/" + jarResource.getJarName(), jarResource);
							}
						}
					}
				}
				
				File nativeExtensions = new File(extensionDir, "native");
				if (nativeExtensions.isDirectory()) {
					String[] files = extensionDir.list(new JarFileFilter());
					if (files.length > 0) {
						for (String filename : files) {
							File f = new File(extensionDir, filename);
							if (f.isFile()) {
								JarDownloadResource jarResource = newJarDownloadResource(f);
								jarResource.setExtension(true);
								jarResource.setNative(true);
								newCodebaseUriToResourceMap.put(NuclosEnviromentConstants.CODEBASE_ENDING + "/extensions/native/" + jarResource.getJarName(), jarResource);
							}
						}
					}
				}
	
				File themesDir = new File(appDir, "extensions/themes");
				if (themesDir.isDirectory()) {
					String[] files = themesDir.list(new JarFileFilter());
					if (files.length > 0) {
						for (String filename : files) {
							File f = new File(themesDir, filename);
							if (f.isFile() && !filename.startsWith("jnlp-1.0.")) {
								JarDownloadResource jarResource = newJarDownloadResource(f);
								jarResource.setTheme(true);
								newCodebaseUriToResourceMap.put(NuclosEnviromentConstants.CODEBASE_ENDING + "/extensions/themes/" + jarResource.getJarName(), jarResource);
							}
						}
					}
				}
				
				codebaseUriToResource = Collections.unmodifiableMap(newCodebaseUriToResourceMap);
			}
			catch (Exception e) {
				LOG.error("Failed to initialize JarDownloadServlet.", e);
			}
		}
	}
	
	private static JarDownloadResource newJarDownloadResource(File jarFile) {
		final JarDownloadResource jarResource = new JarDownloadResource();
		final String[] jarNameAndVersion = splitJarNameVersion(getJarName(jarFile.getName()));
		jarResource.setJarName(jarNameAndVersion[0]);
		jarResource.setVersion(jarNameAndVersion[1]);
		jarResource.setFile(jarFile);
		if (jarFile.getName().toLowerCase().endsWith(JAR_PACK_GZ_FILE_SUFFIX)) {
			jarResource.setContentEncoding(CONTENT_ENCODING_PACK200_GZIP);
		}
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(jarFile);
			String hash = DigestUtils.md5Hex(new FileInputStream(jarFile));
			jarResource.setHash(hash);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
		return jarResource;
	}
	
//	private void addToCache(String uriPrefix, JarDownloadResource jarResource) {
		// Mal setzt Webstart ein Request mit Versionsnummer im Namen ab, mal ohne. 
		// Warum ist unklar. Vielleicht nur beim einmaligen Wechsel auf die neue "jnlp.versionEnabled" Version?
//		codebaseUriToResource.put(uriPrefix + getJarName(jarResource.getFile().getName()), jarResource);
//		codebaseUriToResource.put(uriPrefix + jarResource.getJarName(), jarResource);
//	}
	
	public static boolean isJarFile(final String jarName) {
		String lowerJarName = jarName.toLowerCase();
		for (String jarSuffix : JAR_FILE_SUFFIXES) {
			if (lowerJarName.endsWith(jarSuffix)) {
				return true;
			}
		}
		return false;
	}
	
	public static String[] splitJarNameVersion(final String jarNameWithVersion) {
		String jarName = jarNameWithVersion;
		String jarVersion = "unknown";
		String[] jarNameParts = jarName.split("-");
		if (jarNameParts.length > 1) {
			String lastPart = jarNameParts[jarNameParts.length-1];
			if (jarNameParts.length > 1 &&
					"nuclos".equals(jarNameParts[0])) {
				// for e.g. 'nuclos-client-4.10.3-r4.10.2-38-ga47d45f-dirty.jar'
				jarName = "";
				jarVersion = "";
				boolean versionNow = false;
				for (int i = 0; i < jarNameParts.length; i++) {
					if (!versionNow && Character.isDigit(jarNameParts[i].charAt(0))) {
						// switch to version if first 'digit' part found...
						versionNow = true;
					}
					String addTo = versionNow ? jarVersion : jarName;
					if (addTo.length() > 0) {
						addTo += "-";
					}
					addTo += jarNameParts[i];
					if (versionNow) {
						jarVersion = addTo;
					} else {
						jarName = addTo;
					}
				}
			} else if (jarNameParts.length > 3 && 
					"all".equals(jarNameParts[0]) &&
					"in".equals(jarNameParts[1]) &&
					"one".equals(jarNameParts[2])) {
				// for e.g. 'all-in-one-b6b7ef6d2cb7d6468d962d820f12646915ed4bf0.jar'
				jarVersion = lastPart;
				jarName = "";
				for (int i = 0; i < jarNameParts.length-1; i++) {
					jarName += jarNameParts[i];
					if (i < jarNameParts.length-2) {
						jarName += "-";
					}
				}
			} else if (lastPart.length() > 0 && Character.isDigit(lastPart.charAt(0))) {
				jarVersion = lastPart;
				jarName = "";
				for (int i = 0; i < jarNameParts.length-1; i++) {
					jarName += jarNameParts[i];
					if (i < jarNameParts.length-2) {
						jarName += "-";
					}
				}
			} else if (lastPart.matches("[a-zA-Z]+\\.jar") && jarNameParts.length > 1) {
				// for e.g. 'org.eclipse.jgit-3.5.0.201409260305-r.jar' 
				// or 'maik1-1.0-SNAPSHOT.jar'
				jarVersion = jarNameParts[jarNameParts.length-2] + "-" + jarNameParts[jarNameParts.length-1];
				jarName = "";
				for (int i = 0; i < jarNameParts.length-2; i++) {
					jarName += jarNameParts[i];
					if (i < jarNameParts.length-3) {
						jarName += "-";
					}
				}
			}
		}
		
		for (String jarSuffix : JAR_FILE_SUFFIXES) {
			if (jarVersion.toLowerCase().endsWith(jarSuffix) && !jarName.toLowerCase().endsWith(jarSuffix)) {
				jarName += jarVersion.substring(jarVersion.length()-jarSuffix.length());
				jarVersion = jarVersion.substring(0, jarVersion.length()-jarSuffix.length());
				break;
			}
		}
		
		return new String[]{jarName, jarVersion};
	}
	
	protected static String getJarName(String lib) {
		String result = lib;
		if (lib.toLowerCase().endsWith(".pack.gz")) {
			result = result.substring(0, result.length() - 8);
		}
		if (lib.toLowerCase().endsWith(".pack")) {
			result = result.substring(0, result.length() - 5);
		}
		return result;
	}
	
	public static class JarFileFilter implements FilenameFilter {
		@Override
		public boolean accept(File dir, String name) {
			return isJarFile(name);
		}
	}

	public static void invalidateCaches() {
		codebaseUriToResource = null;
	}

	public Map<String, JarDownloadResource> getCodebaseUriToResource() {
		if (codebaseUriToResource == null) {
			fillCaches();
		}
		return codebaseUriToResource;
	}
	
}
