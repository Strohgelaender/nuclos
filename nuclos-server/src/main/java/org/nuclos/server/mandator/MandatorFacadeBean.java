//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.mandator;

import java.util.Arrays;
import java.util.Collection;
import javax.annotation.security.RolesAllowed;
import org.nuclos.common.E;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.security.MandatorFacadeRemote;
import org.nuclos.common.security.UserVO;
import org.nuclos.common.valueobject.MandatorUserVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, noRollbackFor = {Exception.class})
@RolesAllowed("Login")
public class MandatorFacadeBean extends NuclosFacadeBean implements MandatorFacadeRemote {

    @Autowired
    private MasterDataFacadeLocal masterDataFacade;


    public MandatorFacadeBean() {
    }

    public MandatorVO getByUID(UID id) throws CommonBusinessException {
        return new MandatorVO(masterDataFacade.get(E.MANDATOR, id));
    }


    public MandatorVO create(MandatorVO vo) throws CommonBusinessException {

        MasterDataVO<UID> mdvo = vo.toMasterDataVO();
        mdvo = masterDataFacade.create(mdvo, null);

        return new MandatorVO(mdvo);
    }




	public MandatorVO modify(MandatorVO vo, IDependentDataMap mpDependants, String customUsage) throws CommonBusinessException {
        throw new NuclosBusinessRuleException("not implemented");
    }

    @Override
    public void remove(MandatorVO vo) throws CommonBusinessException {
		throw new NuclosBusinessRuleException("not implemented");
    }

}
