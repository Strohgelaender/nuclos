//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.mandator;

import org.nuclos.common.MandatorVO;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.security.UserVO;
import org.nuclos.common.valueobject.MandatorUserVO;
import org.nuclos.common2.exception.CommonBusinessException;

public interface MandatorFacadeLocal {

    MandatorVO getByMandatorName(String mandatorname) throws CommonBusinessException;

    MandatorVO create(MandatorVO vo) throws CommonBusinessException;

    MandatorVO modify(MandatorVO vo, IDependentDataMap mpDependants, String customUsage) throws CommonBusinessException;

    void remove(MandatorVO vo) throws CommonBusinessException;

    MandatorVO getByUID(UID id) throws CommonBusinessException;

}
