//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.tasklist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common.tasklist.TasklistFacadeRemote;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.transaction.annotation.Transactional;

@RolesAllowed("Login")
@Transactional
public class TasklistFacadeBean extends NuclosFacadeBean implements TasklistFacadeLocal, TasklistFacadeRemote {

	private final static Logger LOG = LoggerFactory.getLogger(TasklistFacadeBean.class);

	@Autowired
	private MasterDataFacadeLocal mdfacade;

	@Override
	public Collection<TasklistDefinition> getUsersTasklists() {
		UID mandatorUID = getCurrentMandatorUID();
		if (mandatorUID == null) {
			mandatorUID = UID.UID_NULL;
		}
		return getTasklists(getCurrentUserName(), mandatorUID);
	}

	@Cacheable(value="dynamicTasklistDefinitionsByUserAndMandator", key="#p0 + #p1.getString()")
	private Collection<TasklistDefinition> getTasklists(String user, UID mandator) {
		final List<TasklistDefinition> result = new ArrayList<>();

		final Set<UID> tasklists = new HashSet<UID>();
		if (SecurityCache.getInstance().isSuperUser(user)) {
			for (final Object tasklistObj : mdfacade.getMasterDataIds(E.TASKLIST.getUID())) {
				final UID tasklistUid = (UID) tasklistObj;
				tasklists.add(tasklistUid);
			}
		}
		else {
			for (final UID roleUid : SecurityCache.getInstance().getUserRoles(user, mandator)) {
				final CollectableSearchCondition cond = SearchConditionUtils.newUidComparison(E.TASKLISTROLE.role, ComparisonOperator.EQUAL, roleUid);
				for (final MasterDataVO<UID> tasklistrole : mdfacade.getMasterData(E.TASKLISTROLE, cond)) {
					tasklists.add(tasklistrole.getFieldUid(E.TASKLISTROLE.tasklist));
				}
			}
		}
		
		for (final UID tasklistUid : tasklists) {
			try {
				final MasterDataVO<UID> tasklist = mdfacade.get(E.TASKLIST, tasklistUid);

				final TasklistDefinition def = new TasklistDefinition(tasklist.getPrimaryKey());
				def.setName(tasklist.getFieldValue(E.TASKLIST.name));
				def.setDescription(tasklist.getFieldValue(E.TASKLIST.description));
				def.setLabelResourceId(tasklist.getFieldValue(E.TASKLIST.labelres));
				def.setDescriptionResourceId(tasklist.getFieldValue(E.TASKLIST.descriptionres));
				def.setMenupathResourceId(tasklist.getFieldValue(E.TASKLIST.menupathres));
				def.setDynamicTasklistUID(tasklist.getFieldUid(E.TASKLIST.datasource));
				def.setTaskEntityUID(tasklist.getFieldUid(E.TASKLIST.taskentity));
				def.setDynamicTasklistIdFieldUid(UID.parseUID(tasklist.getFieldValue(E.TASKLIST.datasourceIdField)));
				def.setDynamicTasklistEntityFieldUid(UID.parseUID(tasklist.getFieldValue(E.TASKLIST.datasourceEntityField)));
				def.setCustomRuleIdFieldUid(UID.parseUID(tasklist.getFieldValue(E.TASKLIST.customRuleIdField)));
				def.setCustomRuleEntityFieldUid(UID.parseUID(tasklist.getFieldValue(E.TASKLIST.customRuleEntityField)));

				result.add(def);
			}
			catch (CommonBusinessException e) {
				LOG.warn("Unable to get user tasklists {}", tasklistUid, e);
			}
		}
		return Collections.unmodifiableList(result);
	}

	@Caching(evict={
			@CacheEvict(value="dynamicTasklistDefinitionsByUserAndMandator", allEntries=true),
	})
	public void invalidateCaches() {

	}

}
