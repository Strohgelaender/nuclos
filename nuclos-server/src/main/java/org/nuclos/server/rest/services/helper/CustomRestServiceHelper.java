package org.nuclos.server.rest.services.helper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.json.JsonObject;
import javax.ws.rs.core.Response;

import org.nuclos.api.User;
import org.nuclos.api.context.CustomRestContext;
import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.common.UID;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.security.UserFacadeLocal;
import org.nuclos.server.security.UserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomRestServiceHelper extends DataServiceHelper {

	@Autowired
	UserFacadeLocal userFacade;

	@Autowired
	private DataLanguageCache dataLanguageCache;

	@Autowired
	private CustomCodeManager customCodeManager;

	@Autowired
	private SecurityCache securityCache;

	@Autowired
	private EventSupportCache esCache;

	protected String invokeRestRuleMethod(final String className, final String methodName, final Class methodType, final JsonObject data)
			throws NuclosWebException, IllegalAccessException {
		try {

			final Map<UID, String> allowedCustomRestRules = securityCache.getAllowedCustomRestRules(this.getUser(), this.getMandatorUID());
			if (!allowedCustomRestRules.containsValue(className)) {
				throw new NuclosWebException(Response.Status.UNAUTHORIZED);
			}

			EventSupportSourceVO essV0 = esCache.getEventSupportSourceByClassname(className);
			if (essV0 == null || !essV0.isActive()) {
				throw new NuclosWebException(Response.Status.NOT_IMPLEMENTED);
			}

			final ClassLoader classLoader = this.customCodeManager.getClassLoaderAndCompileIfNeeded();
			final CustomRestRule rule = (CustomRestRule)classLoader.loadClass(className).newInstance();
			final String language = dataLanguageCache.getLanguageToUse() != null ? dataLanguageCache.getLanguageToUse().toString() : null;
			final Method method = rule.getClass().getMethod(methodName, CustomRestContext.class);

			if (method.getAnnotation(methodType) == null) {
				throw new IllegalAccessException("'" + methodName + "' is not a " + methodType + " method.");
			}

			final Object methodReturnValue = method.invoke(
					rule,
					buildCustomRestContext(data, this.getUser(), rule.getClass(), language)
			);

			if (methodReturnValue instanceof String || methodReturnValue instanceof JsonObject) {
				return methodReturnValue.toString();
			} else {
				return new ObjectMapper().writeValueAsString(methodReturnValue);
			}

		} catch (NoSuchMethodException
				| InstantiationException
				| InvocationTargetException
				| JsonProcessingException
				| NuclosCompileException | ClassNotFoundException e) {
			throw new NuclosWebException(e);
		}
	}

	private CustomRestContext buildCustomRestContext(final JsonObject postData, final String userName, final Class ruleClass, final String language) {
		return new CustomRestContext() {

			private final Logger logger = LoggerFactory.getLogger(ruleClass);
			private final Map<String, Object> cache = new HashMap<String, Object>();

			@Override
			public String getRequestParameter(final String key) {
				return getFirstParameter(key, String.class);
			}

			@Override
			public JsonObject getRequestData() {
				return postData;
			}

			@Override
			public Object getContextCacheValue(String key) {
				return cache.get(key);
			}

			@Override
			public void addContextCacheValue(String key, Object value) {
				if (cache.containsKey(key)) {
					throw new IllegalArgumentException(String.format("Event cache contains key %s", key));
				}
				cache.put(key, value);
			}

			@Override
			public void removeContextCacheValue(String key) {
				cache.remove(key);
			}

			@Override
			public User getUser() {
				User user = null;
				UserVO userVo = null;
				try {
					userVo = userFacade.getByUserName(userName);
					user = new UserImpl(userVo.getPrimaryKey(),
							userVo.getName(), userVo.getLastname(), userVo.getFirstname(), userVo.getEmail(),
							userVo.getLocked(), userVo.getSuperuser(), userVo.getPasswordChanged(),
							userVo.getExpirationDate(), userVo.getRequirePasswordChange(),
							language != null ? NuclosLocale.valueOf(language.toUpperCase()) : null
					);
				} catch (CommonBusinessException e) {
					throw new RuntimeException("Unable to get user '" + userName + "'.", e);
				}
				return user;
			}

			@Override
			public String getLanguage() {
				return language;
			}

			@Override
			public void log(final String message) {
				logger.info(message);
			}

			@Override
			public void logWarn(final String message) {
				logger.warn(message);
			}

			@Override
			public void logError(final String message, final Exception ex) {
				logger.error(message, ex);
			}
		};
	}
}