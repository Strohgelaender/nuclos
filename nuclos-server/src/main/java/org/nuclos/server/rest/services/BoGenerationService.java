package org.nuclos.server.rest.services;

import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;

@Path("/boGenerations")
@Produces(MediaType.APPLICATION_JSON)
public class BoGenerationService extends DataServiceHelper {

	@GET
	@Path("/{boMetaId}")
	@RestServiceInfo(identifier = "boWithDefaultValues", isFinalized = true, description = "Get a new BO with default values for inserting")
	public JsonObject boWithDefaultValues(@PathParam("boMetaId") String boMetaId) {
		return getBoWithAllDefaultValues(boMetaId).build();
	}

	/**
	 * @return the generated BO
	 * <p>
	 * In case of an error the BO is unsaved, which means the ID is NULL.
	 * The error could be found in the BO under the key "businessError".
	 */
	@POST
	@Path("/{boMetaId}/{boId}/{boVersion}/generate/{generationId}")
	@RestServiceInfo(identifier = "boGeneration", description = "Run the generation and returns the BO (in case of an error the BO is unsaved)")
	public JsonObject boSingleGeneration(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("boVersion") int boVersion,
			@PathParam("generationId") String generationId,
			JsonObject data
	) {
		return generateBo(boMetaId, boId, boVersion, generationId, data).build();
	}

}