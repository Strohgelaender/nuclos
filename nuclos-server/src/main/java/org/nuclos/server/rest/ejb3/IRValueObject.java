package org.nuclos.server.rest.ejb3;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

public interface IRValueObject {

	class JsonBuilderConfiguration {
		public boolean withTitleAndInfo = false;
		public boolean withMetaLink = false;
		public boolean withLayoutLink = false;
		public final boolean withLock;
		public final boolean noRestrictions;
		public final boolean skipStatesAndGenerations;
		private final String queryAttributes;
		private boolean withDetailLink;

		/**
		 * TODO: Too many parameters, use a builder pattern!
		 */
		public JsonBuilderConfiguration(
				boolean withTitleAndInfo,
				boolean withMetaLink,
				boolean withLayoutLink,
				boolean withLock,
				boolean noRestrictions,
				String queryAttributes,
				boolean skipStatesAndGenerations,
				boolean withDetailLink
		) {
			this.withTitleAndInfo = withTitleAndInfo;
			this.withMetaLink = withMetaLink;
			this.withLayoutLink = withLayoutLink;
			this.withLock = withLock;
			this.noRestrictions = noRestrictions;
			this.queryAttributes = queryAttributes;
			this.skipStatesAndGenerations = skipStatesAndGenerations;
			this.withDetailLink = withDetailLink;
		}

		public static JsonBuilderConfiguration getFull() {
			return new JsonBuilderConfiguration(
					true,
					true,
					true,
					true,
					false,
					null,
					false,
					true
			);
		}

		public static JsonBuilderConfiguration getNoAdds() {
			return new JsonBuilderConfiguration(
					false,
					false,
					false,
					false,
					true,
					null,
					false,
					false
			);
		}

		public static JsonBuilderConfiguration getList(String attributes, boolean bSkipStatesAndGenerations) {
			return new JsonBuilderConfiguration(
					true,
					true,
					false,
					false,
					false,
					attributes,
					bSkipStatesAndGenerations,
					true
			);
		}

		public static JsonBuilderConfiguration getNoAddsAndSkipStates() {
			return new JsonBuilderConfiguration(
					false,
					false,
					false,
					false,
					true,
					null,
					true,
					false
			);
		}

		public String getAttributes() {
			return queryAttributes;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof JsonBuilderConfiguration) {
				JsonBuilderConfiguration that = (JsonBuilderConfiguration) obj;
				return LangUtils.equal(withTitleAndInfo, that.withTitleAndInfo)
						&& LangUtils.equal(withMetaLink, that.withMetaLink)
						&& LangUtils.equal(withLayoutLink, that.withLayoutLink)
						&& LangUtils.equal(withLock, that.withLock)
						&& LangUtils.equal(noRestrictions, that.noRestrictions)
						&& LangUtils.equal(queryAttributes, that.queryAttributes)
						&& LangUtils.equal(skipStatesAndGenerations, that.skipStatesAndGenerations);
			}
			return false;
		}

		@Override
		public int hashCode() {
			return LangUtils.hash(withTitleAndInfo, withMetaLink, withLayoutLink, withLock, noRestrictions, queryAttributes, skipStatesAndGenerations);
		}

		public boolean isWithDetailLink() {
			return withDetailLink;
		}

		public void setWithDetailLink(final boolean withDetailLink) {
			this.withDetailLink = withDetailLink;
		}
	}

	/**
	 * temporary ID of the stateful cached EntityObjectVO in RestFacadeBean
	 */
	UID TEMPORARY_ID_FIELD = new UID("nuclosTemporaryId");

	UID TEMPORARY_DEPENDENTS_ALREADY_LOADED_FIELD = new UID("nuclosTemporaryDependentsAlreadyLoaded");

	String TEMPORARY_ID_JSON_KEY = "temporaryId";
}
