package org.nuclos.server.rest.services.rvo;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.RestLinks.Verbs;

class GeneratorActionRVO {
	private final String generationId;
	private final String name;
	private final EntityMeta<?> targetEntity;
	private final String target;
	private final boolean nonstop;
	private final boolean internal;
	private final String boMetaId;

	GeneratorActionRVO(GeneratorActionVO generation, String boMetaId) {
		generationId = Rest.translateUid(E.GENERATION, generation.getId());
		name = StringUtils.defaultIfNull(generation.getLabel(), generation.getName());
		targetEntity = Rest.getEntity(generation.getTargetModule());
		target = targetEntity != null ? Rest.getLabelFromMetaDataVO(targetEntity) : "";
		nonstop = generation.isNonstop();
		internal = generation.isRuleOnly();

		this.boMetaId = boMetaId;
	}

	//Note: It's not trivial to put this into JsonFactory, as the result is dependent of boId
	JsonObjectBuilder getJsonObjectBuilder(
			final String boId,
			final int version,
			final IWebContext context
	) {
		JsonObjectBuilder json = Json.createObjectBuilder();

		json.add("generationId", generationId)
				.add("name", name)
				.add("target", target)
				.add("nonstop", nonstop)
				.add("internal", internal);

		RestLinks links = new RestLinks(json);
		links.addLinkHref(
				"generate",
				"boGeneration",
				Verbs.GET,
				boMetaId,
				boId,
				version,
				generationId
		);
		links.buildJson(context);

		return json;
	}


}
