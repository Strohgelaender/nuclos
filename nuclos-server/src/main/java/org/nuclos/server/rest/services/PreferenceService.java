//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.preferences.Preference;
import org.nuclos.common.preferences.PreferenceShareVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.PreferencesFacadeLocal;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/preferences")
@Produces(MediaType.APPLICATION_JSON)
public class PreferenceService extends WebContext {

	private static final Logger LOG = LoggerFactory.getLogger(PreferenceService.class);

	@Autowired
	private PreferencesFacadeLocal prefsFacade;

	@GET
	@RestServiceInfo(identifier = "preferences", isFinalized = true, description = "List of preferences for the current user.")
	public JsonArray preferences() {
		try {
			String app = getFirstParameter("app", String.class);
			String boMetaId = getFirstParameter("boMetaId", String.class);
			String layoutId = getFirstParameter("layoutId", String.class);
			boolean orLayoutIsNull = getQueryParameters().containsKey("orLayoutIsNull");
			String type = getFirstParameter("type", String.class);
			Boolean menuRelevant = getFirstParameter("menuRelevant", Boolean.class);
			boolean returnSubBo = getQueryParameters().containsKey("returnSubBo");
			UID user = SecurityCache.getInstance().getUserUid(getUser());
			UID entity = null;
			if (boMetaId != null) {
				entity = Rest.translateFqn(E.ENTITY, boMetaId);
			}
			UID layout = null;
			if (layoutId != null) {
				layout = Rest.translateFqn(E.LAYOUT, layoutId);
			}
			final List<Preference> prefs = prefsFacade.getPreferences(app, type, entity, layout, orLayoutIsNull, user, menuRelevant, returnSubBo);
			JsonArrayBuilder builder = Json.createArrayBuilder();
			for (Preference pref : prefs) {
				builder.add(buildOutgoing(pref));
			}
			return builder.build();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosWebException(e, E.PREFERENCE.getUID());
		}
	}

	@DELETE
	@Path("/{prefId}/share/{userRoleId}")
	@RestServiceInfo(identifier = "preferenceUnShare", isFinalized = false, description = "Un-share the preference for the given user role")
	public void preferenceUnShare(@PathParam("prefId") String prefId, @PathParam("userRoleId") String userRoleId) {
		UID prefUID = UID.parseUID(prefId);
		UID roleUID = Rest.translateFqn(E.ROLE, userRoleId);
		try {
			prefsFacade.unSharePreference(prefUID, roleUID);
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			// ignore this request if preference is not shared with this role
		}
	}

	@POST
	@Path("/{prefId}/share/{userRoleId}")
	@RestServiceInfo(identifier = "preferenceShare", isFinalized = false, description = "Share the preference with the given user role")
	public void preferenceShare(@PathParam("prefId") String prefId, @PathParam("userRoleId") String userRoleId) {
		UID prefUID = UID.parseUID(prefId);
		UID roleUID = Rest.translateFqn(E.ROLE, userRoleId);
		try {
			// ignore this request if preference is already shared with this role
			for (PreferenceShareVO share : prefsFacade.getPreferenceShares(prefUID)) {
				if (share.getRole() == roleUID) {
					return;
				}
			}
			prefsFacade.sharePreference(prefUID, roleUID);
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
	}

	@GET
	@Path("/{prefId}/share")
	@RestServiceInfo(identifier = "preferenceShareGroups", isFinalized = false, description = "Get a list of usergroups to share prefernces with")
	public JsonObject preferenceShareGroups(@PathParam("prefId") String prefId) {
		UID prefUID = UID.parseUID(prefId);
		try {
			final List<PreferenceShareVO> preferenceShares = prefsFacade.getPreferenceShares(prefUID);
			JsonObjectBuilder result = Json.createObjectBuilder();
			JsonArrayBuilder userRolesJson = Json.createArrayBuilder();
			for (PreferenceShareVO share : preferenceShares) {
				JsonObjectBuilder urJson = Json.createObjectBuilder();
				urJson.add("userRoleId", Rest.translateUid(E.ROLE, share.getRole()));
				urJson.add("name", share.getRoleName());
				if (share.isShared()) {
					urJson.add("shared", true);
				}
				userRolesJson.add(urJson);
			}
			result.add("userRoles", userRolesJson);
			return result.build();
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
	}

	@GET
	@Path("/{prefId}")
	@RestServiceInfo(identifier = "preference", isFinalized = true, description = "Get a single preference of the current user.")
	public JsonObject preference(@PathParam("prefId") String prefId) {
		UID prefUID = UID.parseUID(prefId);
		try {
			Preference pref = prefsFacade.getPreference(prefUID);
			try {
				return buildOutgoing(pref);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new NuclosWebException(e, E.PREFERENCE.getUID());
			}
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(identifier = "insertPreference", isFinalized = true, description = "Insert a single preference for the current user.", examplePostData =
			"{\n" +
					"	\"prefId\": null,\n" +
					"	\"app\": \"nuclos\",\n" +
					"	\"type\": \"chart\",\n" +
					"	\"boMetaId\": null,\n" +
					"	\"content\": {\n" +
					"	    \"name\": {\"en\": \"default name\", \"de\": \"german name\"},\n" +
					"	    \"myPrefs\": \"store what you want in json format...\",\n" +
					"	    \"hint1\": \"app is optional. Would be 'nuclos' if not set\",\n" +
					"	    \"hint2\": \"type is mandatory. In case of app-'nuclos' it must be registered in org.nuclos.common.NuclosPreferenceType\"\n" +
					"	}\n" +
					"}")
	public JsonObject insertPreference(JsonObject data) {
		Preference.WritablePreference pref = buildIncoming(data);
		try {
			Preference newPref = prefsFacade.insertPreference(pref);
			return buildOutgoing(newPref);
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}
	}

	@PUT
	@Path("/{prefId}")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(identifier = "updatePreference", isFinalized = true, description = "Update a single preference of the current user.")
	public void updatePreference(@PathParam("prefId") String prefId, JsonObject data) {
		Preference.WritablePreference pref = buildIncoming(data);
		if (pref.getUID() == null || !pref.getUID().getString().equals(prefId)) {
			throw new NuclosWebException(Status.NOT_ACCEPTABLE, "Two different prefIds or missing prefId");
		}
		try {
			prefsFacade.updatePreference(pref);
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
	}

	@PUT
	@Path("/{prefId}/share")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(identifier = "preferenceShareUpdate", isFinalized = false, description = "Update the shared preference with the given customization. (User publish changes.)")
	public void updatePreferenceShare(@PathParam("prefId") String prefId, JsonObject data) {
		Preference.WritablePreference pref = buildIncoming(data);
		if (pref.getUID() == null || !pref.getUID().getString().equals(prefId)) {
			throw new NuclosWebException(Status.NOT_ACCEPTABLE, "Two different prefIds or missing prefId");
		}
		try {
			prefsFacade.updatePreferenceShare(pref);
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
	}

	@DELETE
	@Path("/{prefId}")
	@RestServiceInfo(identifier = "deletePreference", isFinalized = true, description = "Delete a single preference of the current user.")
	public Response deletePreference(@PathParam("prefId") String prefId) {
		UID prefUID = UID.parseUID(prefId);
		try {
			prefsFacade.deletePreference(prefUID);

		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
		return Response.status(Status.OK).build();
	}

	/**
	 * TODO: Wrong method - use GET!
	 * see NUCLOS-6610
	 */
	@POST
	@Path("/findReferencingPreferences")
	@RestServiceInfo(identifier = "findReferencingPreferences", isFinalized = true, description = "Find preferences which are referencing given preference ids.")
	@Deprecated
	public Preference[] findReferencingPreferences(String prefIds[]) {


		List<UID> prefUids = new ArrayList<>();
		for (String prefId : prefIds) {
			prefUids.add(UID.parseUID(prefId));
		}

		try {
			return prefsFacade.findReferencingPreferences(prefUids.toArray(new UID[prefUids.size()]));

		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
	}

	/**
	 * TODO: Wrong method - use DELETE!
	 * see NUCLOS-6610
	 */
	@POST
	@Path("/delete")
	@RestServiceInfo(identifier = "deletePreferences", isFinalized = true, description = "Delete multiple preference items. If they are shared they will be unshared first.")
	@Deprecated()
	public Response deletePreferences(String prefIds[]) {

		List<UID> prefUids = new ArrayList<>();
		for (String prefId : prefIds) {
			prefUids.add(UID.parseUID(prefId));
		}

		try {
			prefsFacade.deletePreference(prefUids.toArray(new UID[prefUids.size()]));
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN);
		} catch (CommonFinderException e) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
		return Response.status(Status.OK).build();
	}

	@DELETE
	@RestServiceInfo(identifier = "deleteMyPreferences", isFinalized = true, description = "Delete all preferences of the current user.")
	public Response deleteMyPreferences() {
		prefsFacade.deleteMyPreferences();
		return Response.status(Status.OK).build();
	}

	@PUT
	@Path("/{prefId}/select")
	@RestServiceInfo(identifier = "preferenceSelect", isFinalized = true, description = "Selects the preference for the current user.")
	public void preferenceSelect(@PathParam("prefId") String prefId) {
		this.preferenceSelect(prefId, null);
	}

	@PUT
	@Path("/{prefId}/select/{layoutId}")
	@RestServiceInfo(identifier = "preferenceSelect", isFinalized = true, description = "Selects the preference for the current user and layout.")
	public void preferenceSelect(@PathParam("prefId") String prefId, @PathParam("layoutId") String layoutId) {
		UID prefUID = UID.parseUID(prefId);
		UID layoutUID = null;
		if (layoutId != null) {
			layoutUID = Rest.translateFqn(E.LAYOUT, layoutId);
		}
		prefsFacade.selectPreference(prefUID, layoutUID);
	}

	@DELETE
	@Path("/{prefId}/select")
	@RestServiceInfo(identifier = "preferenceSelect", isFinalized = true, description = "De-Select the preference for the current user.")
	public void preferenceDeselect(@PathParam("prefId") String prefId) {
		this.preferenceDeselect(prefId, null);
	}

	@DELETE
	@Path("/{prefId}/select/{layoutId}")
	@RestServiceInfo(identifier = "preferenceSelect", isFinalized = true, description = "De-Select the preference for the current user and layout.")
	public void preferenceDeselect(@PathParam("prefId") String prefId, @PathParam("layoutId") String layoutId) {
		UID prefUID = UID.parseUID(prefId);
		UID layoutUID = null;
		if (layoutId != null) {
			layoutUID = Rest.translateFqn(E.LAYOUT, layoutId);
		}
		prefsFacade.deselectPreference(prefUID, layoutUID);
	}

	@DELETE
	@Path("/customized")
	@RestServiceInfo(identifier = "preferenceReset", isFinalized = true, description = "Resets customized preferences.")
	@Deprecated
	public void preferenceReset() {

		String boMetaId = getFirstParameter("boMetaId", String.class);
		final String typesString = getFirstParameter("types", String.class);
		String[] types = {};
		if (typesString != null) {
			types = typesString.split(",");
		}
		UID entity = null;
		if (boMetaId != null) {
			entity = Rest.translateFqn(E.ENTITY, boMetaId);
		}

		prefsFacade.resetCustomizedPreferences(entity, types);
	}

	/**
	 * Builds a JSON object for the given Pref.
	 * <p>
	 * TODO: Use Jackson mapping instead
	 * TODO: Javadoc!
	 */
	private JsonObject buildOutgoing(Preference pref) {
		JsonObjectBuilder builder = Json.createObjectBuilder();

		builder.add("prefId", pref.getUID().getString());

		if (!Preference.APP_NUCLOS.equals(pref.getApp())) {
			builder.add("app", pref.getApp());
		}
		builder.add("type", pref.getType());
		if (pref.getEntity() != null) {
			builder.add("boMetaId", Rest.translateUid(E.ENTITY, pref.getEntity()));
		}
		if (pref.getLayout() != null) {
			builder.add("layoutId", Rest.translateUid(E.LAYOUT, pref.getLayout()));
		}
		if (pref.getName() != null) {
			builder.add("name", pref.getName());
		}

		builder.add("shared", pref.isShared());
		builder.add("customized", pref.isCustomized());

		if (Boolean.TRUE.equals(pref.isMenuRelevant())) {
			builder.add("menuRelevant", true);
		}
		if (Boolean.TRUE.equals(pref.isSelected())) {
			builder.add("selected", true);
		}

		if (pref.getNuclet() != null) {
			builder.add("nucletId", Rest.translateUid(E.NUCLET, pref.getNuclet()));
		}

		builder.add("content", translateUids(pref.getJson()));

		return builder.build();
	}

	/**
	 * Builds a {@link Preference} from the given JSON object.
	 * <p>
	 * TODO: Use Jackson mapping instead
	 */
	private Preference.WritablePreference buildIncoming(JsonObject pref) {
		Preference.WritablePreference result = new Preference.WritablePreference();
		if (pref.get("prefId") != null && !pref.isNull("prefId")) {
			result.setUID(UID.parseUID(pref.getString("prefId")));
		}
		if (pref.get("app") != null && !pref.isNull("app")) {
			result.setApp(pref.getString("app"));
		}
		result.setType(pref.getString("type"));
		if (pref.get("boMetaId") != null && !pref.isNull("boMetaId")) {
			result.setEntity(Rest.translateFqn(E.ENTITY, pref.getString("boMetaId")));
		}
		if (pref.get("layoutId") != null && !pref.isNull("layoutId")) {
			result.setLayout(Rest.translateFqn(E.LAYOUT, pref.getString("layoutId")));
		}
		if (pref.get("menuRelevant") != null && !pref.isNull("menuRelevant")) {
			Boolean menuRelevant = pref.getBoolean("menuRelevant", false);
			if (Boolean.TRUE.equals(menuRelevant)) {
				result.setMenuRelevant(true);
			}
		}
		if (pref.get("selected") != null && !pref.isNull("selected")) {
			Boolean selected = pref.getBoolean("selected", false);
			if (Boolean.TRUE.equals(selected)) {
				result.setSelected(true);
			}
		}

		if (pref.get("nucletId") != null && !pref.isNull("nucletId")) {
			result.setNuclet(Rest.translateFqn(E.NUCLET, pref.getString("nucletId")));
		}

		JsonObject jsonPrefs = pref.getJsonObject("content");

		result.setName(tryToGetNameFrom(jsonPrefs));
		if (result.getName() == null) {
			if (pref.get("name") != null && !pref.isNull("name")) {
				if (pref.get("name") instanceof JsonString) {
					result.setName(pref.getString("name"));
				}
			}
		}

		result.setJson(translateFqns(jsonPrefs));

		return result;
	}

	private String tryToGetNameFrom(JsonObject jsonPrefs) {
		if (jsonPrefs.containsKey("name")) {
			JsonValue jsonName = jsonPrefs.get("name");
			if (jsonName instanceof JsonString) {
				String name = ((JsonString) jsonName).getString();
				if (!StringUtils.looksEmpty(name)) {
					return name;
				}
			}
			if (jsonName instanceof JsonObject) {
				JsonObject jsonNameObject = (JsonObject) jsonName;
				// en is default, try to find it first
				if (jsonNameObject.containsKey("en")) {
					JsonValue jsonEnName = jsonNameObject.get("en");
					if (jsonEnName instanceof JsonString) {
						String name = ((JsonString) jsonEnName).getString();
						if (!StringUtils.looksEmpty(name)) {
							return name;
						}
					}
				}
				// try to find any other name
				for (Entry<String, JsonValue> entry : jsonNameObject.entrySet()) {
					if (entry.getValue() instanceof JsonString) {
						String name = ((JsonString) entry.getValue()).getString();
						if (!StringUtils.looksEmpty(name)) {
							return name;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * TODO: Javadoc!
	 */
	private JsonObject translateUids(JsonObject jsonPrefs) {
		final JsonObjectBuilder builder = Json.createObjectBuilder();
		for (Entry<String, JsonValue> entry : jsonPrefs.entrySet()) {
			final String key = entry.getKey();
			final JsonValue value = entry.getValue();
			if (value.getValueType() == ValueType.ARRAY) {
				final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
				ListIterator<JsonValue> arrayIterator = ((JsonArray) value).listIterator();
				while (arrayIterator.hasNext()) {
					JsonValue arrayItem = arrayIterator.next();
					if (arrayItem.getValueType() == ValueType.OBJECT) {
						arrayBuilder.add(translateUids((JsonObject) arrayItem));
					} else {
						arrayBuilder.add(arrayItem);
					}
				}
				builder.add(key, arrayBuilder);
			} else if (value.getValueType() == ValueType.OBJECT) {
				builder.add(key, translateUids((JsonObject) value));
			} else if (value.getValueType() == ValueType.STRING) {
				translateUidAndAdd(key, (JsonString) value, builder);
			} else {
				builder.add(key, value);
			}
		}
		return builder.build();
	}

	/**
	 * TODO: Javadoc!
	 */
	private void translateUidAndAdd(String key, JsonString stringValue, JsonObjectBuilder builder) {
		if (key.endsWith("boMetaId") || key.endsWith("BoMetaId")) {
			UID uid = UID.parseUID(stringValue.getString());
			if (uid != null) {
				builder.add(key, Rest.translateUid(E.ENTITY, uid));
			}
		} else if (key.endsWith("boAttrId") || key.endsWith("BoAttrId")) {
			UID uid = UID.parseUID(stringValue.getString());
			if (uid != null) {
				builder.add(key, Rest.translateUid(E.ENTITYFIELD, uid));
			}
		} else if (key.endsWith("taskListId")) {
			UID uid = UID.parseUID(stringValue.getString());
			if (uid != null) {
				builder.add(key, Rest.translateUid(E.TASKLIST, uid));
			}
		} else {
			builder.add(key, stringValue);
		}
	}

	/**
	 * TODO: Javadoc!
	 */
	private JsonObject translateFqns(JsonObject jsonPrefs) {
		final JsonObjectBuilder builder = Json.createObjectBuilder();
		for (Entry<String, JsonValue> entry : jsonPrefs.entrySet()) {
			final String key = entry.getKey();
			final JsonValue value = entry.getValue();
			if (value.getValueType() == ValueType.ARRAY) {
				final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
				ListIterator<JsonValue> arrayIterator = ((JsonArray) value).listIterator();
				while (arrayIterator.hasNext()) {
					JsonValue arrayItem = arrayIterator.next();
					if (arrayItem.getValueType() == ValueType.OBJECT) {
						arrayBuilder.add(translateFqns((JsonObject) arrayItem));
					} else {
						arrayBuilder.add(arrayItem);
					}
				}
				builder.add(key, arrayBuilder);
			} else if (value.getValueType() == ValueType.OBJECT) {
				builder.add(key, translateFqns((JsonObject) value));
			} else if (value.getValueType() == ValueType.STRING) {
				translateFqnAndAdd(key, (JsonString) value, builder);
			} else {
				builder.add(key, value);
			}
		}
		return builder.build();
	}

	/**
	 * TODO: Javadoc!
	 */
	private void translateFqnAndAdd(String key, JsonString stringValue, JsonObjectBuilder builder) {
		if (key.endsWith("boMetaId") || key.endsWith("BoMetaId")) {
			String fqn = stringValue.getString();
			if (fqn != null) {
				builder.add(key, Rest.translateFqn(E.ENTITY, fqn).getStringifiedDefinitionWithEntity(E.ENTITY));
			}
		} else if (key.endsWith("boAttrId") || key.endsWith("BoAttrId")) {
			String fqn = stringValue.getString();
			if (fqn != null) {
				builder.add(key, Rest.translateFqn(E.ENTITYFIELD, fqn).getStringifiedDefinitionWithEntity(E.ENTITYFIELD));
			}
		} else {
			builder.add(key, stringValue);
		}
	}
}
