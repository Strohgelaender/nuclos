//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MessageRVO  implements Serializable, CommandRVO {
	
	private static final long serialVersionUID = 1L;

	@JsonProperty("message")
	final private String message;
	
	@JsonProperty("title")
	final private String title;
	
	@JsonCreator
	public MessageRVO(@JsonProperty("message") String message, @JsonProperty("title") String title) {
		this.message = message;
		this.title = title;
	}

	@Override
	public CommandType getCommandType() {
		return CommandType.MESSAGEBOX;
	}

	@Override
	public JsonObjectBuilder toJsonBuilderObject() {
		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("commandType", JsonFactory.buildJsonValue(getCommandType().name()));
		json.add("message", JsonFactory.buildJsonValue(message));
		json.add("title", JsonFactory.buildJsonValue(title));
		return json;
	}

}
