//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.mail.NuclosMailServiceProvider;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.UserManagementHelper;
import org.nuclos.server.rest.services.rvo.PasswordChangeRVO;
import org.nuclos.server.rest.services.rvo.PasswordResetRVO;
import org.nuclos.server.rest.services.rvo.RoleRVO;
import org.nuclos.server.rest.services.rvo.UserRVO;
import org.nuclos.server.security.UserFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService extends UserManagementHelper {

	@Autowired
	private UserFacadeLocal userFacade;

	@Autowired
	ServerParameterProvider parameterProvider;

	@Autowired
	private NuclosMailServiceProvider mailServiceProvider;

	@POST
	@RestServiceInfo(identifier = "createUser", description = "User creation")
	public UserRVO createUser(UserRVO userRVO) throws CommonBusinessException {
		checkSuperUser();
		createUserImpl(userRVO);
		return userRVO;
	}

	//DATEN ANGEBEN
	//-->NOCH EIN LETZER SCHRITT ZUR ANMELDUNG. WIR HABEN IHNEN EINE EMAIL GESCHICKT. BITTE AKTIVIEREN SIE IHREN ACCOUNT.
	@POST
	@RestServiceInfo(identifier = "account", validateSession = false, description = "User registration")
	@Path("/register")
	public Response createAccount(UserRVO userRVO) throws CommonBusinessException {
		if (!mailServiceProvider.isConfiguredForSending()) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE, "webclient.account.email.server.not.available");
		}
		if (StringUtils.isNullOrEmpty(parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_SUBJECT))) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE, "webclient.account.email.server.not.available");
		}
		if (StringUtils.isNullOrEmpty(parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_MESSAGE))) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE, "webclient.account.email.server.not.available");
		}
		checkUserRVOIntegrety(userRVO);

		UserRVO user = new UserRVO();
		user.setEmail(userRVO.getEmail());
		user.setName(userRVO.getName());
		user.setFirstname(userRVO.getFirstname());
		user.setLastname(userRVO.getLastname());
		user.setNewPassword(userRVO.getNewPassword());

		// TODO: Activation code should expire after some time
		user.setActivationcode(generateToken());

		user.setLocked(true);
		user.setSuperuser(false);
		user.setPrivacyconsent(userRVO.isPrivacyconsent());

		UserVO userVO = user.toUserVO();
		String rolenamesParameter = parameterProvider.getValue(ParameterProvider.KEY_ROLE_FOR_SELF_REGISTERED_USERS);

		// Webclient-specific i18n codes are used here!
		// TODO: Define these centrally, via nuclos-common or maybe a new module
		if (StringUtils.isNullOrEmpty(rolenamesParameter)) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.self.registration.is.not.active");
		}
		String[] rolenames;
		if (rolenamesParameter.contains(",")) {
			rolenames = rolenamesParameter.split(",");
		} else {
			rolenames = new String[]{rolenamesParameter};
		}

		// Webclient-specific i18n codes are used here!
		// TODO: Define these centrally, via nuclos-common or maybe a new module
		if (userFacade.getByUserName(userRVO.getName()) != null) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.username.already.in.use");
		}
		if (userFacade.getByUserEmail(userRVO.getEmail()) != null) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.email.already.in.use");
		}


		// CREATE USER
		IDependentDataMap mpDependants = new DependentDataMap();
		for (String rolename : rolenames) {
			rolename = rolename.trim();
			EntityObjectVO<UID> eoRoleUser = new EntityObjectVO<>(E.ROLEUSER);
			MasterDataVO<UID> roleVO = userFacade.getRoleByName(rolename);
			if (roleVO == null) {
				throw new NuclosWebException(Response.Status.BAD_REQUEST, "Rolename '" + rolename + "' not exist.");
			}
			eoRoleUser.setFieldUid(E.ROLEUSER.role, roleVO.getPrimaryKey());
			mpDependants.addData(E.ROLEUSER.user, eoRoleUser);
		}
		userFacade.create(userVO, mpDependants);
		userFacade.setPassword(userVO.getName(), userVO.getNewPassword(), null);

		// SEND ACTIVATION LINK
		String subject = parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_SUBJECT);
		String message = parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_MESSAGE);

		NuclosMail mail = new NuclosMail();
		mail.addRecipient(user.getEmail());
		mail.setSubject(subject);
		message = MessageFormat.format(
				message,
				user.getFirstname(),
				user.getLastname(),
				user.getName(),
				user.getActivationcode()
		);
		mail.setMessage(message);

		checkContentType(mail);

		try {
			mailServiceProvider.send(mail);

			return Response.ok().build();
		} catch (BusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	@GET
	@Path("/roles")
	@Produces(MediaType.APPLICATION_JSON)
	public Set<RoleRVO> getRoles() {
		final Set<RoleRVO> result = new HashSet<>();

		final String username = getUser();
		Map<UID, String> allRolesForUser = userFacade.getAllRolesForUser(username);
		for (Map.Entry<UID, String> entry : allRolesForUser.entrySet()) {
			final RoleRVO roleRVO = new RoleRVO(
					entry.getKey(),
					entry.getValue()
			);
			result.add(roleRVO);
		}

		return result;
	}

	//FORMULAR BITTE EMAIL-ADRESSE ANGEBEN
	//--> IHNEN WURDE PER EMAIL DER USERNAME ZUGESENDET. SOLLTE SIE KEINE MAIL ERHALTEN HABEN, PRÜFEN SICH BITTE AUCH IHREN SPAM-ORDNER
	@POST
	@Path("/forgotUsername/{email}")
	@RestServiceInfo(validateSession = false)
	public Response forgotUsername(
			@PathParam("email") String email
	) {
		try {
			// SEND ACTIVATION LINK
			String subject = parameterProvider.getValue(ParameterProvider.KEY_USERNAME_EMAIL_SUBJECT);
			String message = parameterProvider.getValue(ParameterProvider.KEY_USERNAME_EMAIL_MESSAGE);

			if (subject == null || message == null || !mailServiceProvider.isConfiguredForSending()) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}

			UserVO user = userFacade.getByUserEmail(email);
			if (user == null) {
				throw new NuclosWebException(Response.Status.NOT_FOUND);
			} else if (!Rest.facade().isRestApiAllowed(user.getName())) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			} else {
				NuclosMail mail = new NuclosMail();
				mail.addRecipient(user.getEmail());
				mail.setSubject(subject);
				mail.setMessage(
						MessageFormat.format(message, user.getFirstname(), user.getLastname(), user.getName())
				);

				checkContentType(mail);
				mailServiceProvider.send(mail);
			}
			return Response.ok().build();
		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
	}

	@GET
	@RestServiceInfo(identifier = "getUser", description = "Get user")
	@Path("/{username}")
	public UserRVO getUser(@PathParam("username") String username) throws CommonBusinessException {
		checkSuperUser();
		UserVO userVO = userFacade.getByUserName(username);
		if (userVO == null) {
			throw new NuclosWebException(Response.Status.NOT_FOUND, "Username '" + username + "' not found.");
		}
		return new UserRVO(userVO);
	}

	@PUT
	@Path("/{username}")
	public UserRVO updateUser(@PathParam("username") String username, UserRVO userRVO) {
		throw new NotImplementedException("update user is not supported");
	}

	@DELETE
	@Path("/{username}")
	public UserRVO deleteUser(@PathParam("username") String username) {
		throw new NotImplementedException("delete user is not supported");
	}

	//ERFOLGREICH AKTIVERT
	@POST
	@Path("/{username}/activate/{code}")
	public Response activateAccount(
			@PathParam("username") String username,
			@PathParam("code") String code
	) throws CommonBusinessException {
		// VALIDATE
		if (StringUtils.isNullOrEmpty(code))
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.no.activationcode.found");
		if (StringUtils.isNullOrEmpty(username))
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.no.username.found");

		// CHECK EXISTING ALREADY
		UserVO user = userFacade.getByActivationCode(code);

		if (user == null || !user.getName().equals(username)) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.username.or.activationcode.not.found");
		} else {
			user.setActivationcode(null);
			user.setLocked(false);
			userFacade.modify(user, null, null);
			return Response.ok().build();
		}
	}

	@PUT
	@Path("/{username}/password")
	@RestServiceInfo(validateSession = false)
	public Response setPassword(
			@PathParam("username") String username,
			final PasswordChangeRVO passwordChange
	) {
		return super.setPassword(username, passwordChange);
	}

	//FORMULAR BITTE USERNAME UND EMAIL ANGEBEN...
	//--> IHNEN WURDE PER EMAIL EIN LINK ZUGESENDET, ÜBER DIESEN LINK KÖNNEN SIE IHR PASSWORT ZURÜCKSETZEN
	@POST
	@Path("/{username}/forgotPassword")
	@RestServiceInfo(validateSession = false)
	public Response forgotPassword(@PathParam("username") String username) {
		try {
			// SEND ACTIVATION LINK
			String subject = parameterProvider.getValue(ParameterProvider.KEY_RESET_PW_EMAIL_SUBJECT);
			String message = parameterProvider.getValue(ParameterProvider.KEY_RESET_PW_EMAIL_MESSAGE);

			if (subject == null || message == null || !mailServiceProvider.isConfiguredForSending()) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}

			UserVO user = userFacade.getByUserName(username);
			if (user == null) {
				throw new NuclosWebException(Response.Status.NOT_FOUND);
			} else if (!Rest.facade().isRestApiAllowed(user.getName())) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			} else if (user.getEmail() == null) {
				throw new NuclosWebException(Response.Status.PRECONDITION_FAILED);
			} else {
				// TODO: Activation code should expire after some time
				user.setPasswordresetcode(generateToken());
				userFacade.modify(user, null, null);

				NuclosMail mail = new NuclosMail();
				mail.addRecipient(user.getEmail());
				mail.setSubject(subject);
				mail.setMessage(
						MessageFormat.format(
								message,
								user.getFirstname(),
								user.getLastname(),
								user.getName(),
								user.getPasswordresetcode()
						)
				);

				checkContentType(mail);
				mailServiceProvider.send(mail);
			}
		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
		return Response.ok().build();
	}

	@POST
	@Path("/{username}/passwordReset")
	@RestServiceInfo(validateSession = false)
	public Response resetPassword(
			@PathParam("username") String username,
			final PasswordResetRVO passwordReset
	) {
		if (!Rest.facade().isRestApiAllowed(username)) {
			throw new NuclosWebException(Response.Status.FORBIDDEN);
		}

		return super.resetPassword(username, passwordReset);
	}

	@GET
	@Path("/{username}/privacyConsent")
	public Boolean getPrivacyConsent(
			@PathParam("username") final String username
	) {
		try {
			checkUser(username);

			UserVO user = userFacade.getByUserName(username);
			return user.isPrivacyConsent();
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	@PUT
	@Path("/{username}/privacyConsent")
	public void setPrivacyConsent(
			@PathParam("username") final String username,
			final Boolean privacyConsent
	) {
		try {
			checkUser(username);

			UserVO user = userFacade.getByUserName(username);
			user.setPrivacyConsent(privacyConsent);
			userFacade.modify(user, null, null);
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	private void createUserImpl(UserRVO userRVO) throws CommonBusinessException {
		checkUserRVOIntegrety(userRVO);

		UserVO userVO = userRVO.toUserVO();

		IDependentDataMap mpDependants = new DependentDataMap();
		List<String> rolenames = userRVO.getRoles();
		for (String rolename : rolenames) {
			MasterDataVO<UID> roleVO = userFacade.getRoleByName(rolename);
			if (roleVO == null) {
				throw new NuclosWebException(Response.Status.BAD_REQUEST, "Rolename '" + rolename + "' does not exist.");
			}
			EntityObjectVO<UID> eoRoleUser = new EntityObjectVO<>(E.ROLEUSER);
			eoRoleUser.setFieldUid(E.ROLEUSER.role, roleVO.getPrimaryKey());
			mpDependants.addData(E.ROLEUSER.user, eoRoleUser);
		}

		userFacade.create(userVO, mpDependants);
		userFacade.setPassword(userRVO.getName(), userRVO.getNewPassword(), null);
	}

	/**
	 * Determines the content type of the mail ("text/plain" or "text/html") based on the given message and signature.
	 * <p>
	 * TODO: This should be moved to the MailServiceProvider:
	 * The content type of NuclosMail should be NULL unless explicitly set.
	 * The MailServiceProvider should then determine the content type, only if it is NULL.
	 */
	private void checkContentType(final NuclosMail mail) {
		final String signature = parameterProvider.getValue(ParameterProvider.EMAIL_SIGNATURE);
		final boolean isHtml = (mail.getMessage() + signature).matches("(?s).*<[^>]+>.*");

		mail.setContentType(isHtml ? NuclosMail.HTML : NuclosMail.PLAIN);
	}

}

