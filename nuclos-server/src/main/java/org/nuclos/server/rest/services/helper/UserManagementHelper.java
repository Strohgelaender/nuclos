package org.nuclos.server.rest.services.helper;

import java.util.UUID;

import javax.ws.rs.core.Response;

import org.apache.commons.validator.routines.EmailValidator;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.rvo.PasswordChangeRVO;
import org.nuclos.server.rest.services.rvo.PasswordResetRVO;
import org.nuclos.server.rest.services.rvo.UserRVO;
import org.nuclos.server.security.UserFacadeBean;
import org.springframework.beans.factory.annotation.Autowired;


public class UserManagementHelper extends WebContext {

	@Autowired
	private UserFacadeBean userFacade;

	protected Response setPassword(String username, PasswordChangeRVO passwordChange) {

		String oldPassword = passwordChange.getOldPassword();
		String newPassword = passwordChange.getNewPassword();

		if (oldPassword == null) {
			oldPassword = "";
		}

		if (newPassword == null) {
			newPassword = "";
		}

		Response.Status respStatus = Rest.facade().changePassword(username, oldPassword, newPassword, getSessionId());

		if (respStatus == Response.Status.OK) {
			return Response.ok().build();
		}

		throw new NuclosWebException(respStatus);
	}

	/**
	 * Resets the password.
	 * Requires a valid activation code, but not the old password.
	 */
	protected Response resetPassword(final String username, final PasswordResetRVO passwordReset) {
		try {
			final String code = passwordReset.getPasswordResetCode();
			final String password = passwordReset.getNewPassword();

			if (org.apache.commons.lang.StringUtils.isBlank(code)
					|| org.apache.commons.lang.StringUtils.isBlank(password)
			) {
				throw new NuclosWebException(Response.Status.PRECONDITION_FAILED);
			}

			UserVO user = userFacade.getByUserName(username);
			if (code.equals(user.getPasswordresetcode())) {
				userFacade.setPasswordAndResetCode(username, passwordReset.getNewPassword());
				return Response.ok().build();
			} else {
				throw new NuclosWebException(Response.Status.PRECONDITION_FAILED);
			}
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	protected final void checkUserRVOIntegrety(UserRVO userRVO) {

		// Webclient-specific i18n codes are used here!
		// TODO: Define these centrally, via nuclos-common or maybe a new module
		if (StringUtils.isNullOrEmpty(userRVO.getName())) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.missing.username");
		}
		if (StringUtils.isNullOrEmpty(userRVO.getNewPassword())) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.missing.password");
		}
		if (StringUtils.isNullOrEmpty(userRVO.getFirstname())) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.missing.firstname");
		}
		if (StringUtils.isNullOrEmpty(userRVO.getLastname())) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.missing.lastname");
		}
		if (StringUtils.isNullOrEmpty(userRVO.getEmail())) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.missing.email");
		}
		if (!EmailValidator.getInstance().isValid(userRVO.getEmail())) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.email.invalid");
		}
		if (!StringUtils.equals(userRVO.getEmail(), userRVO.getEmail2())) {
			throw new NuclosWebException(Response.Status.BAD_REQUEST, "webclient.account.email.confirmation.unequal");
		}
	}

	protected String generateToken() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}
