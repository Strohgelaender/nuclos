package org.nuclos.server.rest;

import javax.servlet.http.HttpSession;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.AbstractSessionIdLocator;
import org.nuclos.server.security.SessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class SessionValidationRequestFilter extends AbstractSessionIdLocator implements ContainerRequestFilter {

	private final static Logger log = LoggerFactory.getLogger(SessionValidationRequestFilter.class);

	@Autowired(required = false)
	private MaintenanceFacadeLocal maintenanceFacade;

	@Context
	private ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext creq) {
		validateSessionIfNecessary();
	}

	private void validateSessionIfNecessary() {
		if (isSessionValidationEnabledForCurrentRequest()) {
			validateSession();
		}
	}

	private boolean isSessionValidationEnabledForCurrentRequest() {
		return NuclosRestApplication.isSessionValidationEnabled(
				httpRequest.getMethod(),
				resourceInfo.getResourceMethod()
		);
	}

	private void validateSession() {
		HttpSession httpSession = httpRequest.getSession();

		log.debug("Validating session " + httpSession.getId() + " on Thread " + currentThread());

		if (!httpSession.getId().equals(httpRequest.getRequestedSessionId())) {
			// tomcat session has been invalidated - remove session from SecurityCache
			Rest.facade().webLogout(httpRequest.getSession().getId());
			log.debug("Session was invalidated -> UNAUTHORIZED");
			throw new NuclosWebException(Response.Status.UNAUTHORIZED);
		} else {
			String sessionId = httpSession.getId();
			SessionContext session = Rest.facade().validateSessionAuthenticationAndInitUserContext(sessionId);
			if (session == null) {
				log.debug("Session validation enabled, but no authentication -> UNAUTHORIZED");
				throw new NuclosWebException(Response.Status.UNAUTHORIZED);
			} else if (maintenanceFacade != null && maintenanceFacade.blockUserLogin(session.getUsername())) {
				throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
			}
		}
	}

	private String currentThread() {
		return Thread.currentThread().getId() + " (" + Thread.currentThread().getName() + ")";
	}
}
