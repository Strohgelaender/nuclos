package org.nuclos.server.rest.services;

import java.util.List;

import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestLinks.RestLink;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RecursiveDependency;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;

@Path("/bos/{boMetaId}/{boId}/subBos")
@Produces(MediaType.APPLICATION_JSON)
public class DependenceService extends DataServiceHelper {

	@GET
	@Path("/{refAttrId}")
	@RestServiceInfo(identifier = "dependence_list", isFinalized = true, description = "List of Dependence-Data (Rows)")
	public JsonObject dependencelist(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("refAttrId") String refAttrId,
			@QueryParam("sort") String sortString
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return getDependenceList(
				boMetaId,
				RecursiveDependency.forRoot(boId).dependency(refAttrId),
				linksFactory,
				sortString,
				null
		).build();
	}

	//FIXME: HATEOAS Standard forbids @POST-Method for just fetching data. See also BoService @Path("/{boMetaId}/query")
	@POST
	@Path("/{refAttrId}/query")
	@RestServiceInfo(identifier = "dependence_listQuery", isFinalized = true, description = "List of Dependence-Data (Rows) by query object")
	@Consumes({MediaType.APPLICATION_JSON})
	public JsonObject dependencelist(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, @PathParam("refAttrId") String refAttrId, @QueryParam("sort") String sortString, JsonObject queryContext) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return getDependenceList(
				boMetaId,
				RecursiveDependency.forRoot(boId).dependency(refAttrId),
				linksFactory,
				sortString,
				queryContext
		).build();
	}

	@GET
	@Path("/{refAttrId}/{subBoId}")
	@RestServiceInfo(identifier = "dependence_self", isFinalized = true, description = "Full Data Row Details of Dependency")
	public JsonObject dependencydetails(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("refAttrId") String refAttrId,
			@PathParam("subBoId") String subBoId
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return getDependencyDetails(boMetaId, boId, refAttrId, subBoId, linksFactory).build();
	}

	@GET
	@Path("/recursive/{referenceAttributeAndRecordIds:.+}")
	@RestServiceInfo(
			identifier = "dependents_list_recursive",
			description = "Data for subforms nested in arbitrary depth"
	)
	public JsonObject dependentsList(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("referenceAttributeAndRecordIds") List<PathSegment> referenceAttributeAndRecordIds,
			@QueryParam("sort") String sortString
	) {
		RecursiveDependency dependency = RecursiveDependency.forRoot(boId);
		for (int i = 0; i < referenceAttributeAndRecordIds.size(); i += 2) {
			final String referenceAttributeId = referenceAttributeAndRecordIds.get(i).toString();

			String recordId = null;
			if (referenceAttributeAndRecordIds.size() > i + 1) {
				recordId = referenceAttributeAndRecordIds.get(i + 1).toString();
			}

			dependency = dependency.dependency(referenceAttributeId, recordId);
		}

		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(
				boMetaId,    // TODO: Should be the Meta-ID of the immediate parent of the dependents?
				boId,        // TODO: Should be the record ID of the immediate parent of the dependents?
				dependency.getReferenceAttributeFqn()
		);

		// TODO: Extend to allow passing params for arbitrary depth subform query
		return getDependenceList(
				boMetaId,
				dependency,
				linksFactory,
				sortString,
				null
		).build();
	}

	@GET
	@Path("/{refAttrId}/valuelist/{subBoAttrId}")
	@RestServiceInfo(identifier = "dependence_valuelist", description = "Value list for filtering")
	public JsonValue valuelist(@PathParam("boMetaId") String boMetaId,
							   @PathParam("boId") String boId,
							   @PathParam("refAttrId") String refAttrId,
							   @PathParam("subBoAttrId") String subBoAttrId) {
		UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, subBoAttrId);
		try {
			UID baseEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);
			FieldMeta fieldMeta = Rest.getEntityField(fieldUid);
			EntityMeta<?> baseEntityMeta = Rest.getEntity(baseEntityUid);
			UID refFieldUid = Rest.translateFqn(E.ENTITYFIELD, refAttrId);
			FieldMeta refFieldMeta = Rest.getEntityField(refFieldUid);

			checkPermissionForSubform(baseEntityMeta, fieldMeta, boId, true);

			List<String> sSearch = getQueryParameters("search");

			List<CollectableField> lstCF = Rest.facade().getValueList(fieldMeta, sSearch, refFieldMeta, new Long(boId));
			return JsonFactory.buildJsonArray(lstCF, this).build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, fieldUid);
		}
	}


	@GET
	@Deprecated // use dependenceListExportConsiderColumnConfiguration
	@Path("/{refAttrId}/export/{format}/{pageOrientationLandscape}/{isColumnScaled}")
	@RestServiceInfo(identifier = "dependence_list_export", isFinalized = true, description = "Export list of Dependence-Data (subform) as pdf/csv/xls")
	public ResultListExportRVO dependenceListExport(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("refAttrId") String refAttrId,
			@QueryParam("sort") String sortString,
			@PathParam("format") String format,
			@PathParam("pageOrientationLandscape") boolean pageOrientationLandscape,
			@PathParam("isColumnScaled") boolean isColumnScaled,
			JsonObject queryContext
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return exportDependenceList(boMetaId, refAttrId, boId, linksFactory, sortString, queryContext, format, pageOrientationLandscape, isColumnScaled);
	}

	@POST
	@Path("/{refAttrId}/export/{format}/{pageOrientationLandscape}/{isColumnScaled}")
	@RestServiceInfo(identifier = "dependence_list_export", isFinalized = false, description = "Export list of Dependence-Data (subform) as pdf/csv/xls")
	public ResultListExportRVO dependenceListExportConsiderColumnConfiguration(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("refAttrId") String refAttrId,
			@PathParam("format") String format,
			@PathParam("pageOrientationLandscape") boolean pageOrientationLandscape,
			@PathParam("isColumnScaled") boolean isColumnScaled,
			JsonObject queryContext
	) {
		DependenceBoLinksFactory linksFactory = new DependenceBoLinksFactory(boMetaId, boId, refAttrId);
		return exportDependenceList(boMetaId, refAttrId, boId, linksFactory, null, queryContext, format, pageOrientationLandscape, isColumnScaled);
	}


	public static class DependenceBoLinksFactory extends RValueObject.AbstractBoLinksFactory {

		private final String boMetaId;
		private final String boId;
		private final String refAttrId;

		public DependenceBoLinksFactory(String boMetaId, String boId, String refAttrId) {
			this.boMetaId = boMetaId;
			this.boId = boId;
			this.refAttrId = refAttrId;
		}

		@Override
		public RestLink addSelf(Object subBoId) {
			return links.addLinkHref("self", "dependence_self", Verbs.GET,
					boMetaId, boId, refAttrId, subBoId).protectVerbs();
		}

		@Override
		public RestLink addDetail(final String detailBoMetaId, final Object subBoId) {
			return links.addLinkHref(
					"detail",
					"bo",
					Verbs.GET,
					detailBoMetaId,
					subBoId
			).protectVerbs();
		}

		@Override
		public RestLink addMeta() {
			return links.addLinkHref("boMeta", "referencemeta_self", Verbs.GET,
					boMetaId, refAttrId);
		}

		@Override
		public RestLink addStateIcon(String stateId) {
			return null;
		}

		@Override
		public RestLink addLayout(String layoutId) {
			return links.addLinkHref("layout", "weblayoutCalculated", Verbs.GET, layoutId);
		}

		@Override
		public RestLink addDocument(String rel, String docAttrId, Object subBoId) {
			return docLinks.addLinkHref(rel, "dependence_DocumentValue", Verbs.GET,
					boMetaId, boId, refAttrId, subBoId, docAttrId);
		}

		@Override
		public RestLink addImage(String rel, String imgAttrId, Object subBoId, int version) {
			return imgLinks.addLinkHref(rel, "dependence_ImageValue", Verbs.GET,
					boMetaId, boId, refAttrId, subBoId, imgAttrId, version);
		}

		@Override
		public RestLink addPrintout(String sTranslatedEntity, Object pk) {
			return null;
		}

		@Override
		public RestLink addInsert() {
			return null;
		}

		@Override
		public RestLink addSubforminfo(String layoutId, Object pk) {
			return links.addLink("subforminfo", Verbs.GET, layoutId, pk.toString());
		}

		@Override
		public RestLink addUnlock(Object pk) {
			return null;
		}

	}

}