//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.rest.mapper.NuclosObjectMapperProvider;
import org.nuclos.server.rest.services.BoDocumentService;
import org.nuclos.server.rest.services.BoGenerationService;
import org.nuclos.server.rest.services.BoImageService;
import org.nuclos.server.rest.services.BoMetaService;
import org.nuclos.server.rest.services.BoPrintoutService;
import org.nuclos.server.rest.services.BoService;
import org.nuclos.server.rest.services.BoStateService;
import org.nuclos.server.rest.services.BusinessTestRestService;
import org.nuclos.server.rest.services.CustomRestService;
import org.nuclos.server.rest.services.DataService;
import org.nuclos.server.rest.services.DependenceService;
import org.nuclos.server.rest.services.LayoutService;
import org.nuclos.server.rest.services.LoginService;
import org.nuclos.server.rest.services.MaintenanceRestService;
import org.nuclos.server.rest.services.MessageService;
import org.nuclos.server.rest.services.MetaDataService;
import org.nuclos.server.rest.services.NucletService;
import org.nuclos.server.rest.services.PreferenceService;
import org.nuclos.server.rest.services.ResourceService;
import org.nuclos.server.rest.services.UserService;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationPath("/rest/")
public class NuclosRestApplication extends ResourceConfig {

	private final static Logger LOG = LoggerFactory.getLogger(NuclosRestApplication.class);

	public NuclosRestApplication() {

		// Disable WADL generation for the REST service
		property("jersey.config.server.wadl.disableWadl", "true");

		register(LoginService.class);
		register(BoMetaService.class);
		register(BoService.class);
		register(BoGenerationService.class);
		register(DependenceService.class);
		register(BoImageService.class);
		register(BoDocumentService.class);
		register(BoStateService.class);
		register(BoPrintoutService.class);
		register(ResourceService.class);
		register(MetaDataService.class);
		register(DataService.class);
		register(MaintenanceRestService.class);
		register(MultiPartFeature.class);
		register(PreferenceService.class);
		register(MessageService.class);
		register(NuclosObjectMapperProvider.class);
		register(BusinessTestRestService.class);
		register(UserService.class);
		register(LayoutService.class);
		register(NucletService.class);
		register(CustomRestService.class);

		final String sAdditionalServicesClassName = LangUtils.defaultIfNull(ApplicationProperties.getInstance().getAdditionalRestServices(), AdditionalRestServices.class.getName());

		try {
			final Class<? extends AdditionalRestServices> clsAdditionalServicesClass = (Class<? extends AdditionalRestServices>) Class.forName(sAdditionalServicesClassName);
			AdditionalRestServices additionalRestServices = clsAdditionalServicesClass.newInstance();
			Set<Class<?>> classes = additionalRestServices.getClasses();
			for (Class<?> clazz : classes) {
				register(clazz);
			}
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException cnfe) {
			LOG.warn(cnfe.getMessage(), cnfe);
		}

		// REGISTER SINGLETON
		registerInstances(
				new CacheHeaderFilter(),
				new CrossOriginResourceSharingFilter(),
				new SessionValidationRequestFilter(),
				new ProfilingFilter()
		);
	}

	static boolean isSessionValidationEnabled(
			final String httpMethod,
			final Method resourceMethod
	) {
		if ("OPTIONS".equals(httpMethod)) {
			return false;
		}

		RestServiceInfo annotation = resourceMethod.getAnnotation(RestServiceInfo.class);
		if (annotation == null) {
			return true;
		}

		return annotation.validateSession();
	}

	public static String getPathForService(String serviceid) {
		for (Class<?> restServiceClass : new NuclosRestApplication().getClasses()) {

			String pathValueFromClass = "";
			for (Annotation annotation : restServiceClass.getAnnotations()) {
				if (annotation instanceof Path) {
					Path path = (Path) annotation;
					pathValueFromClass = path.value();
				}
			}

			for (Method method : restServiceClass.getMethods()) {

				if (!method.isAnnotationPresent(RestServiceInfo.class)) {
					continue;
				}

				String identifier = method.getAnnotation(RestServiceInfo.class).identifier();
				if (!LangUtils.equal(serviceid, identifier)) {
					continue;
				}

				String spath = pathValueFromClass;

				if (method.isAnnotationPresent(Path.class)) {
					spath += method.getAnnotation(Path.class).value();
				}

				return spath;
			}
		}

		return null;
	}
}
