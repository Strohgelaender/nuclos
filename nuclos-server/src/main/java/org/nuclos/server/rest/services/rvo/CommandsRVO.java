//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CommandsRVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	final private List<CommandRVO> list;
	
	@JsonCreator
	public CommandsRVO(@JsonProperty("list") List<CommandRVO> list) {
		this.list = list;
	}

	public List<CommandRVO> getList() {
		return list;
	}
	
	@JsonIgnore
	public JsonObjectBuilder toJsonBuilderObject(){
		JsonObjectBuilder json = Json.createObjectBuilder();
		JsonArrayBuilder alist = Json.createArrayBuilder();
		for (CommandRVO c : list) {
			alist.add(c.toJsonBuilderObject());
		}
		json.add("list", alist);
		return json;
	}
}
