package org.nuclos.server.rest.services.rvo;

import static org.nuclos.server.rest.services.helper.WebContext.DEFAULT_RESULT_LIMIT;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.ws.rs.core.Response.Status;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.AbstractCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.rest.ejb3.IFilterJ;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestSQLParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;

public class FilterJ implements IFilterJ {

	private static final Logger LOG = LoggerFactory.getLogger(FilterJ.class);
	
	private final IWebContext webContext;
	private final JsonObject queryContext;
	
	private String search;
	private String searchFilterId;
	private String vlpId;
	private Map<String, String> vlpParameter;
	
	private Long offset;
	private Long chunkSize;
	private Boolean countTotal = false;
	
	@Deprecated
	private String fields;
	@Deprecated
	private String searchCondition;
	@Deprecated
	private String sortExpression;
	
	private String attributes;
	private String where;
	private String orderBy;

	private Boolean skipStatesAndGenerations = null;
	private Boolean withMetaLink = null;
	private Boolean withTitleAndInfo = null;
	private Boolean withLayoutLink = null;
	private Boolean withDetailLink = null;

	public FilterJ(IWebContext webContext, JsonObject queryContext, Long defaultChunkSize) {
		this.webContext = webContext;
		this.queryContext = queryContext;
		
		this.offset = get("offset", Long.class);
		this.chunkSize = get("chunkSize", Long.class);
		if (this.chunkSize == null) {
			// old style
			this.chunkSize = getWebContextValue("chunksize", Long.class);
		}
		if (this.chunkSize == null) {
			this.chunkSize = defaultChunkSize;
		}
		
		this.countTotal = get("countTotal", Boolean.class);
		if (countTotal == null) {
			// old style
			this.countTotal = getWebContextValue("gettotal", Boolean.class);
		}

		this.search = get("search", String.class);
		this.searchFilterId = get("searchFilterId", String.class);
		if (searchFilterId == null) {
			this.searchFilterId = getWebContextValue("searchFilter", String.class);
		}
		this.vlpId = get("vlpId", String.class);
		if (this.vlpId != null) {
			this.vlpParameter = new HashMap<>();
			for (String key : this.webContext.getParameterKeys()) {
				String value = this.webContext.getFirstParameter(key);
				if (value != null) {
					this.vlpParameter.put(key, value);
				}
			}
		}

		this.fields = getWebContextValue("fields", String.class);
		this.sortExpression = getWebContextValue("sort", String.class);
		this.searchCondition = getWebContextValue("searchCondition", String.class);
		
		this.where = getWhereFromQuery();
		if (this.where == null) {
			this.where = getWebContextValue("where", String.class);
		}
		this.attributes = getAttributesFromQuery();
		if (this.attributes == null) {
			this.attributes = getWebContextValue("attributes", String.class);
		}
		if (this.attributes != null && this.attributes.indexOf('_') == -1) { // attributes are not FQNs
			String boMetaId = Rest.translateUid(E.ENTITY, webContext.getBOMeta().getUID());
			List<String> attributes = new ArrayList<String>();
			if (this.attributes.length() > 0) {
				for(String attribute : this.attributes.split(",")) {
					attributes.add(boMetaId + "_" + attribute);
				}
				this.attributes = org.apache.commons.lang.StringUtils.join(attributes, ",");
			} else {
				this.attributes = null;
			}
		}
		this.orderBy = getOrderByFromQuery();
		if (this.orderBy == null) {
			this.orderBy = getWebContextValue("orderBy", String.class);
		}

		this.skipStatesAndGenerations = get("skipStatesAndGenerations", Boolean.class);
		this.withTitleAndInfo = get("withTitleAndInfo", Boolean.class);
		this.withMetaLink = get("withMetaLink", Boolean.class);
		this.withLayoutLink = get("withLayoutLink", Boolean.class);
		this.withDetailLink = get("withDetailLink", Boolean.class);
	}
	
	private <T> T get(String key, Class<T> clzz) {
		return get(key, key, clzz);
	}
	
	private <T> T get(String keyWeb, String keyReq, Class<T> clzz) {
		T valQuery = getQueryContextValue(keyReq, clzz);
		if (valQuery != null) {
			return valQuery;
		}
		T valWeb = getWebContextValue(keyWeb, clzz);
		return valWeb;
	}
	
	private <T> T getWebContextValue(String key, Class<T> clzz) {
		return webContext.getFirstParameter(key, clzz);
	}
	
	private <T> T getQueryContextValue(String key, Class<T> clzz) {
		if (queryContext != null) {
			if (queryContext.containsKey(key) && !queryContext.isNull(key)) {
				JsonValue jsonValue = queryContext.get(key);
				if (String.class.equals(clzz)) {
					if (jsonValue instanceof JsonString) {
						return (T)(((JsonString) jsonValue).getString());
					} else {
						throw new IllegalArgumentException("JsonValue for " + key + " is not a number!");
					}
				} else 
				if (Long.class.equals(clzz)) {
					if (jsonValue instanceof JsonNumber) {
						return (T)new Long(((JsonNumber) jsonValue).longValue());
					} else {
						throw new IllegalArgumentException("JsonValue for " + key + " is not a number!");
					}
				} else 
				if (Boolean.class.equals(clzz)) {
					if (jsonValue.equals(JsonValue.TRUE)) {
						return (T)Boolean.TRUE;
					} else if (jsonValue.equals(JsonValue.FALSE)) {
						return (T)Boolean.FALSE;
					} else {
						throw new IllegalArgumentException("JsonValue for " + key + " is not a boolean!");
					}
				} else {
					throw new IllegalArgumentException("JsonValue type " + clzz.getCanonicalName() + " is not supported!");
				}
			}
		}
		return null;
	}
	
	/** translates the JSON value for the key 'attributes'. Something like this...
	[
		{"boAttrId": "example_rest_Order_customer"},
		{"boAttrId": "example_rest_Order_orderDate"}
	]
	*/
	private String getAttributesFromQuery() {
		if (queryContext != null) {
			if (queryContext.containsKey("attributes") && !queryContext.isNull("attributes")) {
				JsonValue jsonAttributes = queryContext.get("attributes");
				if (jsonAttributes instanceof JsonArray) {
					JsonArray attributesArray = (JsonArray) jsonAttributes;
					StringBuilder result = new StringBuilder();
					for (int i = 0; i < attributesArray.size(); i++) {
						JsonValue jsonValue = attributesArray.get(i);
						if (jsonValue instanceof JsonObject) {
							String boAttrId = ((JsonObject) jsonValue).getString("boAttrId");							
							if (result.length() > 0) {
								result.append(", ");
							}
							result.append(boAttrId);
						}
					}
					return result.toString();
					
				} else if (jsonAttributes instanceof JsonString) {
					// simple String? This is only a fallback!
					return ((JsonString) jsonAttributes).getString();
				}
			}
		}
		return null;
	}
		
	/** translates the JSON value for the key 'where'. Something like this...
	{
		"clause": "customerId = 40000294 AND orderDate >= '2014-06-05' AND self.id IN (SELECT orderPositionRef FROM orderPosition WHERE price > 800)",
		"aliases": {
			"customerId": {"boAttrId": "example_rest_Order_customer"},
			"orderDate": {"boAttrId": "example_rest_Order_orderDate"},
			"orderPositionRef": {"boAttrId": "example_rest_OrderPosition_order"},
			"price": {"boAttrId": "example_rest_OrderPosition_price"},
			"self": {"boMetaId": "example_rest_Order"},
			"orderPosition": {"boMetaId": "example_rest_OrderPosition"}
		}
	}
	*/
	private String getWhereFromQuery() {
		if (queryContext != null) {
			if (queryContext.containsKey("where") && !queryContext.isNull("where")) {
				JsonValue jsonWhere = queryContext.get("where");
				
				if (jsonWhere instanceof JsonObject) {
					JsonObject whereObject = (JsonObject) jsonWhere;
					String whereString = whereObject.getString("clause");
					JsonObject aliasesObject = whereObject.getJsonObject("aliases");
					
					// longer keys first
					ArrayList<String> sortedKeys = CollectionUtils.sorted(aliasesObject.keySet(), new Comparator<String>() {
						@Override
						public int compare(String s1, String s2) {
							int l1 = s1.length();
							int l2 = s2.length();
							if (l1 > l2) {
								return -1;
							} else if (l1 < l2) {
								return 1;
							}
							return 0;
						}
					});
					
					for (String key : sortedKeys) {
						JsonValue jsonValue = aliasesObject.get(key);
						if (jsonValue instanceof JsonObject) {
							JsonObject aliasObject = (JsonObject) jsonValue;
							if (aliasObject.containsKey("boAttrId")) {
								String boAttrId = aliasObject.getString("boAttrId");
								if (boAttrId.indexOf('_') == -1) { // attribute is not a FQN
									String boMetaId = Rest.translateUid(E.ENTITY, webContext.getBOMeta().getUID());
									boAttrId = boMetaId + "_" + boAttrId;
								}
								// TODO move to parser
								whereString = whereString.replaceAll(key, boAttrId);
							} else if (aliasObject.containsKey("boMetaId")) {
								String boMetaId = aliasObject.getString("boMetaId");
								// TODO move to parser
								whereString = whereString.replaceAll(key, boMetaId);
							}
						}
					}
					
					return whereString;
					
				} else if (jsonWhere instanceof JsonString) {
					// simple String? This is only a fallback!
					return ((JsonString) jsonWhere).getString();
				}
			}
		}
		return null;
	}
	
	/** translates the JSON value for the key 'orderBy'. Something like this...
	[
		{"boAttrId": "example_rest_Order_customer", "asc": true},
		{"boAttrId": "example_rest_Order_orderDate", "asc": false}
	]
	*/
	private String getOrderByFromQuery() {
		if (queryContext != null) {
			if (queryContext.containsKey("orderBy") && !queryContext.isNull("orderBy")) {
				JsonValue jsonOrderBy = queryContext.get("orderBy");
				
				if (jsonOrderBy instanceof JsonArray) {
					JsonArray orderByArray = (JsonArray) jsonOrderBy;
					
					StringBuilder result = new StringBuilder();
					for (int i = 0; i < orderByArray.size(); i++) {
						JsonValue jsonValue = orderByArray.get(i);

						if (jsonValue instanceof JsonObject) {
							String boAttrId = ((JsonObject) jsonValue).getString("boAttrId");
							boolean asc = true;
							if (((JsonObject) jsonValue).containsKey("asc")) {
								asc = ((JsonObject) jsonValue).getBoolean("asc", true);
							}
							
							if (result.length() > 0) {
								result.append(", ");
							}
							result.append(boAttrId);
							result.append(" ");
							result.append(asc ? CollectableSorting.SORTING_ASCENDING : CollectableSorting.SORTING_DESCENDING);
						}
					}
					return result.toString();
					
				} else if (jsonOrderBy instanceof JsonString) {
					// simple String? This is only a fallback!
					return ((JsonString) jsonOrderBy).getString();
				}
			}
		}
		return null;
	}
	
	private String getSearch() {
		return search;
	}
	
	public String getAttributes() {
		return attributes;
	}
	
	public boolean showAllFields() {
		if ("all".equalsIgnoreCase(fields)) {
			return true;
		}
		if ("tableview".equalsIgnoreCase(fields)) {
			return true;
		}
		if (StringUtils.looksEmpty(attributes)) {
			return true;
		}
		return false;
	}

	@Override
	public Long getStart() {
		if (offset == null) {
			return 0L;
		}
		return offset;
	}

	@Override
	public Long getEnd() {
		return getStart() + getChunkSize() - 1;
	}

	private long getChunkSize() {
		if (chunkSize < 0) {
			return 0L;
		} else if (chunkSize > DEFAULT_RESULT_LIMIT) {
			return DEFAULT_RESULT_LIMIT;
		}
		return chunkSize;
	}
	
	public boolean countTotal() {
		if (countTotal == null) {
			return false;
		}
		return countTotal;
	}
	
	public JsonBuilderConfiguration getJsonConfig(Boolean bSkipStatesAndGenerations) {
		if (bSkipStatesAndGenerations == null) {
			bSkipStatesAndGenerations = Boolean.TRUE.equals(this.skipStatesAndGenerations);
		}
		JsonBuilderConfiguration result = JsonBuilderConfiguration.getList(getAttributes(), bSkipStatesAndGenerations);
		if (this.withMetaLink != null) {
			result.withMetaLink = this.withMetaLink;
		}
		if (this.withTitleAndInfo != null) {
			result.withTitleAndInfo = this.withTitleAndInfo;
		}
		if (this.withLayoutLink != null) {
			result.withLayoutLink = this.withLayoutLink;
		}
		if (this.withDetailLink != null) {
			result.setWithDetailLink(this.withDetailLink);
		}
		return result;
	}
	
	@Override
	public void setSortingOrder(CollectableSearchExpression cse, UID bometa) {
		
		String sorting = this.orderBy;
		if (StringUtils.looksEmpty(sorting)) {
			sorting = this.sortExpression;
		}
		
		List<CollectableSorting> sorts = new ArrayList<CollectableSorting>();
		
		if (!StringUtils.looksEmpty(sorting)) {
			for (String fieldFqn : sorting.split(",")) {
				String fieldFqnTrim = fieldFqn.trim();
				String sortString = CollectableSorting.SORTING_ASCENDING;
				
				//This happens twice, because the fields arrive here with full qualified names and have to translated first.
				//TODO: Find a more elegant way with parsing the asc/desc only once
				
				int n = fieldFqnTrim.indexOf(' ');
				if (n > -1) {
					sortString = fieldFqnTrim.substring(n + 1);
					fieldFqnTrim = fieldFqnTrim.substring(0, n);
				}
				
				UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, fieldFqnTrim);
				boolean asc = !sortString.equals(CollectableSorting.SORTING_DESCENDING);
				sorts.add(CollectableSorting.createSorting(fieldUid, asc));
			}
		}
		
		if (sorts.isEmpty()) {
			if (Rest.getEntity(bometa).isDatasourceBased()) {
				// no default sorting for datasources...
				return;
			} else {
				sorts.add(CollectableSorting.createSorting(Rest.getEntity(bometa).getPk().getMetaData(bometa).getUID(), false));
			}
		}
		cse.setSortingOrder(sorts);
	}

	private static class WhereParser {
		
		private final IWebContext webContext;
		
		private final String where;
		
		public WhereParser(IWebContext webContext, String where) {
			this.webContext = webContext;
			this.where = where;
		}
		
		public CollectableSearchCondition getCollectableSearchCondition() throws JSQLParserException {
			Statement statement = new CCJSqlParserManager().parse(new StringReader(
					"SELECT * FROM " + Rest.translateUid(E.ENTITY, webContext.getBOMeta().getUID()) + " WHERE " + where));
			RestSQLParser parser = new RestSQLParser();
			statement.accept(parser);
			return parser.getCondition();
		}
		
		
	}
	
	@Override
	public CollectableSearchExpression getSearchExpression(UID entity, Collection<FieldMeta<?>> fields) throws CommonBusinessException {
		CollectableSearchCondition cond1 = null;
		
		if (!StringUtils.isNullOrEmpty(where)) {
			try {
				cond1 = new WhereParser(webContext, where).getCollectableSearchCondition();
			} catch (JSQLParserException e) {
				LOG.error("{} is not parseable: {}", where, e.getMessage(), e);
				throw new NuclosWebException(Status.NOT_ACCEPTABLE, e.getLocalizedMessage());
			}
		}
		
		//NUCLOS-4111: Example syntax for a field-referencing search-condition:
		//searchCondition = "CompositeCondition:AND:[Comparison:EQUAL:seriennummer:x12,LikeCondition:LIKE:lagereinheitentyp:*LE*]";
		
		//TODO: Searches with ':,[]'  are not supported yet, as they are the separators of the arguments
		//Also nested CompositeConditions are not possible at this point (see AbstactCollectableSearchCondition.java Line 136)
		//For Nuclos-4111 there is no need for nested CompositeConditions, so this is postponed.
		
		else if (!StringUtils.isNullOrEmpty(searchCondition)) {
			Map<UID, FieldMeta<?>> mpFields = Rest.getAllEntityFieldsByEntityIncludingVersion(entity);
			Map<FieldMeta<?>, String> mpFieldsByFQN = new HashMap<FieldMeta<?>, String>();
			for (FieldMeta<?> fm : mpFields.values()) {
				mpFieldsByFQN.put(fm, Rest.translateUid(E.ENTITYFIELD, fm.getUID()));
			}
			
			cond1 = AbstractCollectableSearchCondition.createCondition(searchCondition, mpFieldsByFQN);
		}
		
		CollectableSearchCondition cond2 = null;
		
		EntityMeta<?> em = Rest.getEntity(entity);
		if (em.isStateModel()) {
			cond2 = SearchConditionUtils.newComparison(entity,
					SF.LOGICALDELETED, ComparisonOperator.EQUAL, Boolean.FALSE);
			
		}
		
		CollectableSearchCondition cond;
		if (cond1 == null && cond2 == null) {
			cond = null;
		} else if (cond1 == null) {
			cond = cond2;
		} else if (cond2 == null) {
			cond = cond1;
		} else {
			cond = SearchConditionUtils.and(cond1, cond2);
		}
		
		CollectableSearchExpression clctexpr = new CollectableSearchExpression(cond);
		
		String isp = getSearch();
		if (isp != null && !isp.isEmpty()) {
			List<CollectableEntityField> cefs = new ArrayList<CollectableEntityField>();
			for (FieldMeta<?> fm : fields) {
				if (SF.isEOField(fm.getEntity(), fm.getUID())) {
					continue;
				}
				cefs.add(new CollectableEOEntityField(fm));
			}
			SearchConditionUtils.addTextSearchToSearchExpression(isp, cefs, clctexpr, entity, MetaProvider.getInstance());
		}
		
		setSortingOrder(clctexpr, entity);
		clctexpr = addSearchFilterToExpression(clctexpr);

		if (this.vlpId != null) {
			try {
				final UID vlpUID = UID.parseUID(this.vlpId);
				// Backup the id field, 'getDatasourceParameters' will remove it
				final String dsIdField = this.vlpParameter.get(ValuelistProviderVO.DATASOURCE_IDFIELD);
				final ValuelistProviderVO valuelistProvider = Rest.facade().getValuelistProvider(vlpUID);
				final Map<String, Object> dsParameter = Rest.facade().getDatasourceParameters(valuelistProvider.getSource(), this.vlpParameter);
				if (dsIdField != null) {
					dsParameter.put(ValuelistProviderVO.DATASOURCE_IDFIELD, dsIdField);
				}
				clctexpr.setValueListProviderDatasource(valuelistProvider);
				clctexpr.setValueListProviderDatasourceParameter(dsParameter);
			} catch (CommonFinderException e) {
				// ignore
			} catch (CommonPermissionException e) {
				throw new NuclosFatalException(e);
			} catch (CollectableFieldFormatException e) {
				throw new NuclosFatalException(e);
			} catch (CommonBusinessException e) {
				throw new NuclosFatalException(e);
			} catch (ClassNotFoundException e) {
				throw new NuclosFatalException(e);
			}
		}

		return clctexpr;
	}
	
	private CollectableSearchExpression addSearchFilterToExpression(CollectableSearchExpression clctexpr) {
		if (searchFilterId == null || searchFilterId.trim().length() == 0) {
			return clctexpr;
		}
		try {
			EntitySearchFilter2 sf = Rest.facade().getSearchFilterByPk(Rest.translateFqn(E.SEARCHFILTER, searchFilterId));
			if (sf == null) {
				throw new CommonFinderException("SearchFilter with id " + searchFilterId + " not found!");
			}
			final CollectableSearchCondition csc;
			if (clctexpr.getSearchCondition() != null) {
				csc = SearchConditionUtils.and(clctexpr.getSearchCondition(), sf.getSearchExpression().getSearchCondition());
			} else {
				csc = sf.getSearchExpression().getSearchCondition();
			}
			
			// don't add filter if not correct
			try {
				csc.isSyntacticallyCorrect();
			} catch(Exception e) {
				throw new NuclosFatalException("SearchFilter with id " + searchFilterId + " is syntactically not correct: " + e.getMessage(), e);
			}
			
			CollectableSearchExpression clctexpr2 = new CollectableSearchExpression(csc);
			
			List<CollectableSorting> sorts = clctexpr.getSortingOrder();
			sorts.addAll(sf.getSearchExpression().getSortingOrder());
			clctexpr2.setSortingOrder(sorts);
			
			return clctexpr2;
		} catch (CommonBusinessException cbe) {
			throw new NuclosWebException(cbe, Rest.translateFqn(E.SEARCHFILTER, searchFilterId));
		}
	}
	
}
