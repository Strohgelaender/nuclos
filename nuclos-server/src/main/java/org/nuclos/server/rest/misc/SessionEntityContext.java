package org.nuclos.server.rest.misc;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;

public class SessionEntityContext {
	private final EntityMeta<?> masterMeta;
	private final UsageCriteria usage;
	private final FieldMeta<?> reffieldMeta;
	
	public SessionEntityContext(EntityMeta<?> masterMeta, UsageCriteria usage) {
		this(masterMeta, usage, null);
	}
	
	public SessionEntityContext(SessionEntityContext that, FieldMeta<?> refFieldMeta) {
		this(that.getMasterMeta(), that.getUsage(), refFieldMeta);
	}
	
	private SessionEntityContext(EntityMeta<?> masterMeta, UsageCriteria usage, FieldMeta<?> reffieldMeta) {
		this.masterMeta = masterMeta;
		this.usage = usage;
		this.reffieldMeta = reffieldMeta;
	}

	public EntityMeta<?> getMasterMeta() {
		return masterMeta;
	}

	public UsageCriteria getUsage() {
		return usage;
	}

	public boolean isUidEntity() {
		return masterMeta.isUidEntity();
	}
	
	public UID getEntity() {
		return usage.getEntityUID();
	}
	
	public FieldMeta<?> getReffieldMeta() {
		return reffieldMeta;
	}
}
