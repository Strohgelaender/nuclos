package org.nuclos.server.rest.services.rvo;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.layoutml.ISupportsValueListProvider;
import org.nuclos.common2.layoutml.IWebComponent;
import org.nuclos.common2.layoutml.IWebConstraints;
import org.nuclos.common2.layoutml.NuclosRuleEvent;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.RDataType;

public class LayoutAdditions {
	private final FieldMeta<?> fm;
	private final RDataType rdatatype;
	private final String label;
	private final String fieldname;
	private final boolean bmemo;
	private final List<NuclosRuleEvent> ruleEvents;
	private final WebValueListProvider valueListProvider;
	private final IWebConstraints constraints;
	private final ExtendedLayoutProperties extendedLayoutProperties;
	private final Integer tabIndex;
	private final boolean disabled;
	private final String background;
	private final boolean invisible;
	
	public LayoutAdditions(IWebComponent component, FieldMeta<?> fm, List<NuclosRuleEvent> ruleEvents) {
		this.fm = fm;
		this.rdatatype = new RDataType(fm.getDataType(), fm.getPrecision());
		this.label = Rest.getLabelFromMetaFieldDataVO(fm);
		this.fieldname = NuclosBusinessObjectBuilder.getFieldNameForFqn(fm);
		this.bmemo = component.isMemo();
		this.ruleEvents = ruleEvents;
		this.valueListProvider = component instanceof ISupportsValueListProvider ? ((ISupportsValueListProvider)component).getValueListProvider() : null;
		this.constraints = component.getConstraints();
		this.tabIndex = component.getTabIndex();
		this.disabled = disabled(fm, !component.isEnabled());
		this.background = component.getBackground();
		this.extendedLayoutProperties = new ExtendedLayoutProperties();
		for(String key : component.getProperties().keySet()) {
			this.extendedLayoutProperties.put(key, component.getProperties().get(key));
		}
		this.invisible = !component.isVisible();
	}

	public LayoutAdditions(FieldMeta<?> fm, boolean readonly, boolean bmemo, List<NuclosRuleEvent> ruleEvents, String label,
			WebValueListProvider vlp, IWebConstraints constraints, Integer tabIndex, Map<String, String> properties) {
		this.fm = fm;
		this.rdatatype = new RDataType(fm.getDataType(), fm.getPrecision());
		this.label = StringUtils.isNullOrEmpty(label) ? Rest.getLabelFromMetaFieldDataVO(fm) : label;
		this.fieldname = NuclosBusinessObjectBuilder.getFieldNameForFqn(fm);
		this.bmemo = bmemo;
		this.ruleEvents = ruleEvents;
		this.valueListProvider = vlp;
		this.constraints = constraints;
		this.tabIndex = tabIndex;
		this.disabled = disabled(fm, readonly);
		this.background = null;
		this.extendedLayoutProperties = new ExtendedLayoutProperties();
		if (properties != null) {
			for(String key : properties.keySet()) {
				this.extendedLayoutProperties.put(key, properties.get(key));
			}
		}
		this.invisible = false;
	}
	
	private boolean disabled(FieldMeta<?> fm, boolean disabled) {
		boolean ro = disabled || Rest.isAttributeReadOnly(fm) || this.rdatatype.isReadOnly();
		
		//TODO: Non-data-source/named VLPs and VLPs on non-reference fields are not supported yet.
		if (!ro && valueListProvider != null) {
			if (fm.getForeignEntity() == null) {
				ro = true;
				
			} else {
				String type  = valueListProvider.getType();
				
				if (!ValuelistProviderVO.Type.DS.getType().equals(type) && !ValuelistProviderVO.Type.NAMED.getType().equals(type)) {
					ro = true;
				}
			}
		}
		
		return ro;
	}
	
	public UID getUID() {
		return fm.getUID();
	}
	
	public FieldMeta<?> getFieldMeta() {
		return fm;
	}
	
	public boolean isDisabled() {
		return disabled;
	}
	
	public boolean isInvisible() {
		return invisible;
	}

	public String getLabel() {
		return label;
	}
	
	public String getFieldName() {
		return fieldname;
	}

	public boolean isMemo() {
		return bmemo;
	}
	
	public String getBackground() {
		return background;
	}
	
	public WebValueListProvider getValueListProvider() {
		return valueListProvider;
	}

	public List<NuclosRuleEvent> getRuleEvents() {
		return Collections.unmodifiableList(ruleEvents);
	}
	
	public IWebConstraints getConstraints() {
		return constraints;
	}
	
	public ExtendedLayoutProperties getExtendedLayoutProperties() {
		return extendedLayoutProperties;
	}
	
	public Integer getTabIndex() {
		return tabIndex;
	}
	
	public Set<UID> getTargetsOfValueChangingEvents() {
		Set<UID> targets = new HashSet<UID>();
		
		if (ruleEvents != null && !ruleEvents.isEmpty()) {
			for (NuclosRuleEvent event : ruleEvents) {
				targets.addAll(event.getTargetsOfValueChangingActions());
			}
		}
		
		return targets;
	}
	
	@Override
	public String toString() {
		return "Attr=" + fieldname + " Rulevents=" + ruleEvents;
	}

}
