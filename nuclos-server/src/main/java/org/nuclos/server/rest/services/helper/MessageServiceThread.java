//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.helper;

import java.io.IOException;
import java.io.Serializable;
import java.net.SocketException;
import java.util.Collection;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.pool.PooledConnectionFactory;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.nuclos.api.Direction;
import org.nuclos.api.Message;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.api.ApiDirectionImpl;
import org.nuclos.common.api.ApiMessageImpl;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.services.rvo.DirectionRVO;
import org.nuclos.server.rest.services.rvo.MessageRVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

//@Async
@Component
@Scope("prototype")
@DependsOn("jmsFactory")
@Deprecated
public class MessageServiceThread extends Thread {

	private static final Logger LOG = LoggerFactory.getLogger(MessageServiceThread.class);
	
	private final ObjectMapper m = new ObjectMapper();

	@Autowired
	private PooledConnectionFactory jmsFactory;
	
	private EventOutput eventOutput;
	
	private Integer receiverId;
	
	private volatile boolean running = true;
	
	/**
	 * receive messages from jms, transform from Object to Json, push to the eventOutputStream
	 * @param jmsFactory
	 * @param eventOutput
	 *
	 */
	public MessageServiceThread() {
		LOG.debug("MessageServiceThread {} constructed", hashCode());
	}
	
	
	@Override
	public void run() {
		LOG.debug("MessageServiceThread {} run with receiverId={}", hashCode(), receiverId);
		Connection jmsConnection = null;
		Session jmsSession = null;
		try {
			jmsConnection = getJmsFactory().createConnection();
			jmsConnection.start();
			jmsSession = jmsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageListener messageListener = new MessageListener() {
				
				@Override
				public void onMessage(javax.jms.Message jmsMessage) {
					try {
						final ObjectMessage objMsg = (ObjectMessage) jmsMessage;
						final Serializable obj;
						obj = objMsg.getObject();
						String jmsCorrelationID = objMsg.getJMSCorrelationID(); //SessionContext User
						LOG.debug("MessageServiceThread receiving Message with JMSCorrelationID={}",
						          jmsCorrelationID);
						
						if(obj instanceof ApiDirectionImpl){
							final ApiDirectionImpl apiDirection = (ApiDirectionImpl) obj;
							LOG.trace("ApiDirectionImpl receiverId=?=apiDirection.getReceiverId()"
							          + "-->{}=?={}", receiverId, apiDirection.getReceiverId());
							if (receiverId.equals(apiDirection.getReceiverId())) { // RequestContext TabId
								final Direction<Long> direction = apiDirection.getDirection();
								Collection<EntityMeta<?>> entities = Rest.getAllEntities();
								for (EntityMeta<?> entityMeta : entities) {
									if (direction.getEntityUid() != null && entityMeta.getUID() != null && direction.getEntityUid().equals(entityMeta.getUID())) {
										String boMetaId = Rest.translateUid(E.ENTITY, entityMeta.getUID());
										final DirectionRVO directionRVO = new DirectionRVO(direction.getId(), boMetaId, direction.isNewTab(), null);

										final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
										eventBuilder.name(JMSConstants.TOPICNAME_RULEDIRECTION);
										eventBuilder.data(String.class, m.writeValueAsString(directionRVO));

										if (getEventOutput().isClosed() == false) {
											LOG.trace("SEND-EVENT eventBuilder="
											          + "[name={},data={}]",
											          JMSConstants.TOPICNAME_RULEDIRECTION,
											          m.writeValueAsString(directionRVO));
											getEventOutput().write(eventBuilder.build());
										} else {
											terminate();
										}
										break;
									}
								}
							}
						}else if(obj instanceof ApiMessageImpl){
							final ApiMessageImpl apiMessage = (ApiMessageImpl) obj;
							LOG.trace("ApiMessageImpl receiverId=?=apiDirection.getReceiverId()"
							          + "-->{}=?={}",
							          receiverId, apiMessage.getReceiverId());
							if(receiverId.equals(apiMessage.getReceiverId())){
								final Message message = apiMessage.getMessage();
								final MessageRVO messageRVO = new MessageRVO(message.getMessage(), message.getTitle());
								final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
				                eventBuilder.name(JMSConstants.TOPICNAME_RULENOTIFICATION);
				                eventBuilder.data(String.class, m.writeValueAsString(messageRVO));
				                
				                if(getEventOutput().isClosed()==false){
				                	LOG.trace("SEND-EVENT eventBuilder="
					                          + "[name={},data={}]",
					                          JMSConstants.TOPICNAME_RULENOTIFICATION,
					                          m.writeValueAsString(messageRVO));
					                getEventOutput().write(eventBuilder.build());
				                } else {
				                	terminate();
				                }
							}
						}
						
					} catch (JMSException e) {
						LOG.info("Can not process JMS ObjectMessage", e);
						terminate();
					} catch (SocketException e) { 
						//Client disconnected, Connection disabled
						LOG.info("SocketException ({})", e.getMessage());
						terminate();
					} catch (IOException e) {
						LOG.info("IOException {}", e.getMessage());
						terminate();
					} 
				}
				
			};
			
			// NAVI
			Destination ruleDirection = jmsSession.createTopic(JMSConstants.TOPICNAME_RULEDIRECTION.replace('.', '/'));
			MessageConsumer ruleDirectionConsumer = jmsSession.createConsumer(ruleDirection);
			ruleDirectionConsumer.setMessageListener(messageListener);
			
			// MESSAGE NOTIFICATION
			Destination ruleMessage = jmsSession.createTopic(JMSConstants.TOPICNAME_RULENOTIFICATION.replace('.', '/'));
			MessageConsumer ruleMessageConsumer = jmsSession.createConsumer(ruleMessage);
			ruleMessageConsumer.setMessageListener(messageListener);

			// PROCESS BAR
			//TODO
			
			// CURRENT RECORD MODIFIED / RELOAD
			//TODO

			while (running) {
				Thread.sleep(10000);
//				final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
//              eventBuilder.name("ping").data(String.class, "{\"ping\":\""+new Date()+"\"}");
                if(getEventOutput().isClosed()==false){
                	//getEventOutput().write(eventBuilder.build());
                }else{
                	terminate();
                }
			}
			
			LOG.info("MessageServiceThread {} terminate", hashCode());
		} catch (JMSException e) {
			//CAN BE HAPPEN SERVERSIDE VM???
			LOG.info("JMSException ({})", e.getMessage(), e);
		} catch (InterruptedException e) {
			LOG.info("InterruptedException ({})", e.getMessage());
//		} catch (SocketException e) { 
//			//Client disconnected, Connection disabled
//			LOG.info("SocketException ("+e.getMessage()+")");
//		} catch (IOException e) {
//			LOG.info("IOException "+ e.getMessage());
		} catch (Exception e) {
			LOG.info("Exception", e);
		} finally {
			
			//ALL ERROR HAVE TO STOP THE THREAD
			
			// CLOSE eventOutput STREAM
			try {
				if(getEventOutput()!=null) getEventOutput().close();
			} catch (IOException e) { 
				//IGNORE
			}
			
			// CLOSE SESSION
			try {
				if(jmsSession!=null) jmsSession.close();
			} catch (JMSException e) {
				//IGNORE
			}
			
			// CLOSE CONNECTGION
			try {
				if(jmsConnection!=null) jmsConnection.close();
			} catch (JMSException e) {
				//IGNORE
			}
			
		}
		
	}
	
	public void terminate() {
        running = false;
    }
	
	@Override
	protected void finalize() throws Throwable {
		LOG.info("MessageServiceThread {} finalize", hashCode());
		super.finalize();

	}

	public PooledConnectionFactory getJmsFactory() {
		return jmsFactory;
	}

	public void setJmsFactory(PooledConnectionFactory jmsFactory) {
		this.jmsFactory = jmsFactory;
	}

	public EventOutput getEventOutput() {
		return eventOutput;
	}

	public void setEventOutput(EventOutput eventOutput) {
		this.eventOutput = eventOutput;
	}


	public int getReceiverId() {
		return receiverId;
	}


	public void setReceiverId(int receiver) {
		this.receiverId = receiver;
	}

}
