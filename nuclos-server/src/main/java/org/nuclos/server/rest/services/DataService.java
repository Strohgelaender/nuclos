package org.nuclos.server.rest.services;

import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.dblayer.incubator.DbExecutor;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.rest.CacheableResponseBuilder;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RDataType;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.RValueObject.UsageProperties;
import org.nuclos.server.rest.services.rvo.ReferenceListParameterJson;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.core.io.Resource;

@Path("/data")
@Produces(MediaType.APPLICATION_JSON)
public class DataService extends DataServiceHelper {

	//TODO: TO BE REFACTURED

	@GET
	@Path("/fieldget/{field}/{pk}")
	public JsonObject fieldget(@PathParam("field") String field, @PathParam("pk") String pk) {
		return fieldget(field, pk, null, null);
	}

	@GET
	@Path("/fieldget/{field}/{pk}/{requestAttrId}")
	public JsonObject fieldget(
			@PathParam("field") String field,
			@PathParam("pk") String pk,
			@PathParam("requestAttrId"
			) String requestAttrId) {
		return fieldget(field, pk, requestAttrId, null);
	}

	@GET
	@Path("/fieldget/{field}/{pk}/{requestAttrId}/{requestObjectId}")
	public JsonObject fieldget(
			@PathParam("field") String field,
			@PathParam("pk") String pk,
			@PathParam("requestAttrId") String requestAttrId,
			@PathParam("requestObjectId"
			) String requestObjectId) {
		try {
			FieldMeta<?> fm = Rest.getEntityField(Rest.translateFqn(E.ENTITYFIELD, field));
			EntityMeta<?> eMeta = Rest.getEntity(fm.getEntity());
			final UID sessionDataLanguageUID = getSessionContext().getDataLanguageUID();

			JsonBuilderConfiguration jsonConfig = JsonBuilderConfiguration.getNoAdds();

			if (eMeta.isUidEntity()) {
				UID pkUid = Rest.translateFqn(eMeta, pk);
				UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(pkUid, eMeta.getUID());
				checkPermissionForFieldGet(field, fm, usage, requestAttrId, requestObjectId);

				EntityObjectVO<UID> eo = Rest.facade().getDataForField(pkUid, fm);
				UsageProperties up = new UsageProperties(usage, this);
				return new RValueObject<UID>(eo, jsonConfig, up, null, sessionDataLanguageUID).getJSONObjectBuilder().build();

			} else {
				Long pkLong = Long.parseLong(pk);
				UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(pkLong, eMeta.getUID());
				checkPermissionForFieldGet(field, fm, usage, requestAttrId, requestObjectId);

				EntityObjectVO<Long> eo = Rest.facade().getDataForField(pkLong, fm);
				UsageProperties up = new UsageProperties(usage, this);
				return new RValueObject<Long>(eo, jsonConfig, up, null, sessionDataLanguageUID).getJSONObjectBuilder().build();

			}
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITYFIELD, field));
		}
	}

	@GET
	@Path("/referencelist/{field}")
	public JsonValue referencelist(@PathParam("field") String field) {
		return super.referencelistarray(field).build();
	}

	@GET
	@Path("/vlpdata")
	public JsonValue vlpdata() {
		try {
			// TODO: ReferenceListParameterJson umbenennen in VlpParameterJson
			ReferenceListParameterJson paramJson = new ReferenceListParameterJson(getQueryParameters(), this);

			WebValueListProvider wvlp = paramJson.getWebValueListProviderIfAny();
			String pk = paramJson.getPK();

			UID reffield = null;
			Object oReffield = wvlp.getParameters().get("reffield");
			if (oReffield instanceof String && ((String) oReffield).trim().length() > 0) {
				reffield = Rest.translateFqn(E.ENTITYFIELD, (String) oReffield);
			}

			int chunkSize = DEFAULT_RESULT_LIMIT;
			if (paramJson.getChunkSize() != null && paramJson.getChunkSize() > 0) {
				chunkSize = paramJson.getChunkSize();
			}

			List<CollectableField> vlpData = Rest.facade().getVLPData(
					reffield,
					wvlp,
					pk,
					paramJson.getQuickSearchInput(),
					chunkSize,
					false,
					UID.parseUID(paramJson.getMandatorId())
			);
			return JsonFactory.buildJsonArray(vlpData, this).build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, null);
		}
	}


	@GET
	@Path("/search/{text}")
	@RestServiceInfo(identifier = "search", description = "Lucene Search")
	public JsonArray search(@PathParam("text") String text) {

		List<LuceneSearchResult> res = Rest.facade().luceneIndexSearch(text);

		return JsonFactory.buildJsonArray(res, this).build();
	}

	@GET
	@Path("/statusinfo/{uid}")
	@RestServiceInfo(description = "Status information for a Businessobject.")
	public JsonObject statusinfo(@PathParam("uid") String uid) {
		Locale locale = getLocale();
		StateVO state = StateCache.getInstance().getState(Rest.translateFqn(E.STATE, uid));

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("statename", state.getStatename(locale));
		json.add("numeral", state.getNumeral());
		json.add("description", state.getDescription(locale) != null ? state.getDescription(locale) : "");
		if (state.getColor() != null) {
			json.add("color", state.getColor());
		}
		if (state.getButtonIcon() != null) {
			json.add("buttonIcon", Rest.translateUid(E.RESOURCE, state.getButtonIcon().getPrimaryKey()));
		}
		return json.build();
	}


	@GET
	@Path("/resource/{uid}")
	public Response resource(
			@Context final Request request,
			@PathParam("uid") String uid
	) {
		try {
			// TODO: Load the resource only if necessary
			final Resource resource = Rest.facade().getResource(Rest.translateFqn(E.RESOURCE, uid));

			return new CacheableResponseBuilder(request) {
				@Override
				protected Date getLastModified() throws IOException {
					return new Date(resource.lastModified());
				}

				@Override
				protected Response.ResponseBuilder newResponseBuilder() throws IOException {
					return Response.ok(resource.getFile())
							.type(URLConnection.guessContentTypeFromName(resource.getFile().getName()));
				}
			}.build();
		} catch (IOException e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.RESOURCE, uid));
		}
	}

	@POST
	@Path("/evaluatetitleexpression/{boMetaId}/{boId}")
	@RestServiceInfo(identifier = "evaluatetitleexpression", description = "Evaluates a title expression.")
	public JsonObject evaluatetitleexpression(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, String expression) {
		try {

			JsonObjectBuilder json = Json.createObjectBuilder();
			json.add("evaluated_expression", evaluateExpressionString(boMetaId, boId, expression));
			return json.build();

		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}

	@GET
	@Path("/imageresource/{boMetaId}/{boId}/{field_id}")
	@RestServiceInfo(identifier = "imageresource", description = "Image Resource")
	@Produces("image/png")
	public Response imageResource(
			@Context final Request request,
			@PathParam("boMetaId") final String boMetaId,
			@PathParam("boId") final String boId,
			@PathParam("field_id") final String fieldId
	) {
		try {
			SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);

			final EntityMeta entity = info.getMasterMeta();
			final Long id = Long.parseLong(boId);
			final InternalTimestamp changedAt = getChangedAt(entity, id);

			return new CacheableResponseBuilder(request) {
				@Override
				protected Date getLastModified() {
					return changedAt;
				}

				@Override
				protected Response.ResponseBuilder newResponseBuilder() {
					byte[] imageResource = getImageResource(boMetaId, boId, fieldId);
					return Response.ok(imageResource);
				}
			}.build();
		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.RESOURCE, boMetaId));
		}
	}

	@POST
	@Path("/data/matrix")
	@RestServiceInfo(identifier = "matrix", description = "Get matrix data.")
	public JsonObject matrix(JsonObject data) {
		String entity = data.getString("entity");
		checkPermission(E.ENTITY, entity);

		boolean bCamelCase = data.containsKey("entityMatrix");
		String valueTypeKey = bCamelCase ? "entityMatrixValueType" : "entity_matrix_value_type";
		String entity_matrix_value_type = data.get(valueTypeKey) != null ? data.getString(valueTypeKey) : null; // type of the matrix field (checkicon, number, combobox, ..)
		String refFieldKey = bCamelCase ? "entityMatrixReferenceField" : "entity_matrix_reference_field";
		String entity_matrix_reference_field = data.containsKey(refFieldKey) ? data.getString(refFieldKey) : null; // if matrix value is a reference to another BO
		String entity_matrix_value_field = data.getString(bCamelCase ? "entityMatrixValueField" : "entity_matrix_value_field");        // if matrix value is a field of the xref table itself

		String entity_x = data.getString(bCamelCase ? "entityX" : "entity_x");
		String entity_y = data.getString(bCamelCase ? "entityY" : "entity_y");
		String entity_field_matrix_parent = data.getString(bCamelCase ? "entityFieldMatrixParent" : "entity_field_matrix_parent");
		String entity_field_matrix_x_refField = data.getString(bCamelCase ? "entityFieldMatrixXRefField" : "entity_field_matrix_x_refField");

		String cellInputTypeKey = bCamelCase ? "cellInputType" : "cell_input_type";
		String cell_input_type = data.get(cellInputTypeKey) != null ? data.getString(cellInputTypeKey) : "checkicon";

		String editable = data.get("editable") != null ? data.getString("editable") : "true";

		JsonValue value = data.get("boId");

		String boId;
		if (value instanceof JsonString) {
			boId = ((JsonString) value).getString();
		} else if (value instanceof JsonNumber) {
			boId = (value).toString();
		} else {
			boId = null;
		}

		try {

			List<EntityObjectVO<Long>> lstEoRows = new ArrayList<>();
			JsonObjectBuilder json = Json.createObjectBuilder();

			if (!StringUtils.isNullOrEmpty(entity_matrix_value_type)) {
				json.add("entityMatrixValueType", entity_matrix_value_type);
			}

			// load dropdown values if matrix data is a reference to another BO
			if (!StringUtils.isNullOrEmpty(entity_matrix_reference_field)) {
				json.add("matrixValues", referencelistarray(entity_matrix_reference_field));
			}

			// load columns
			json.add("xAxisBoList", getMatrixColumns(entity_x, data, bCamelCase, boId));

			// load rows/subform data
			json.add("yAxisBoList", getMatrixRows(entity_y, data, bCamelCase, boId, lstEoRows));

			List<Long> lstIDs = new ArrayList<>();
			for (EntityObjectVO<Long> eo : lstEoRows) {
				lstIDs.add(eo.getPrimaryKey());
			}

			UID refX2Y = Rest.translateFqn(E.ENTITYFIELD, entity_field_matrix_parent);
			MasterDataFacadeLocal mdLocale = SpringApplicationContextHolder.getBean(MasterDataFacadeLocal.class);

			List<EntityObjectVO<Long>> xrefObjectList = mdLocale.readDependenciesForMultiRecords(lstIDs, refX2Y);

			// load sub-subform data (xref)

			UID entityMatrixValueFieldUid = Rest.translateFqn(E.ENTITYFIELD, entity_matrix_value_field);
			FieldMeta<?> matrixValueMeta = Rest.getEntityField(entityMatrixValueFieldUid);
			RDataType dataType = new RDataType(matrixValueMeta.getDataType(), matrixValueMeta.getPrecision());
			boolean entityMatrixValueFieldIsNumber = dataType.isNumber();

			UID matrix_x_refField = Rest.translateFqn(E.ENTITYFIELD, entity_field_matrix_x_refField);
			UID matrix_parent = Rest.translateFqn(E.ENTITYFIELD, entity_field_matrix_parent);

			JsonArrayBuilder xrefData = Json.createArrayBuilder();
			JsonObjectBuilder entityMatrixBoDict = Json.createObjectBuilder();

			// WebContext.LOG.info("BEFORE TIME CONSUMING STUFF:" + xrefObjectList.size());

			for (EntityObjectVO<Long> eo : xrefObjectList) {
				// FIXME: TIME CONSUMING INSTANCIATING RVALUEOBJECT
				// TODO: Sling out Build(), Avoid instanciation of RValueObject, and: it's not thin at all
				// JsonObjectBuilder boData = getThinEoJson(eo);
				// xrefData.add(boData);

				Object entityMatrixObject = eo.getFieldValue(entityMatrixValueFieldUid);
				if (entityMatrixObject != null) {
					String entityMatrixValue = entityMatrixObject.toString().replaceAll("\"", "");

					Long xAxisBoId = eo.getFieldId(matrix_x_refField);
					Long yAxisBoId = eo.getFieldId(matrix_parent);

					String key = xAxisBoId + "_" + yAxisBoId; // key for entityMatrixBoDict

					JsonObjectBuilder dummy = Json.createObjectBuilder();
					dummy.add("boId", eo.getPrimaryKey());
					if (StringUtils.isNullOrEmpty(entity_matrix_reference_field)) {
						if (entityMatrixValueFieldIsNumber) {
							dummy.add("value", Double.parseDouble(entityMatrixValue));
						} else if ("checkicon".equals(entity_matrix_value_type)) {
							dummy.add("value", Integer.parseInt(entityMatrixValue));
						} else {
							dummy.add("value", entityMatrixValue);
						}
					} else { // matrixcell has reference value

						if (entityMatrixObject instanceof Integer) {
							dummy.add("value", (Integer) entityMatrixObject);
						} else if (entityMatrixObject instanceof String) {
							dummy.add("value", (String) entityMatrixObject);
						}
					}
					//dummy.add("attributes", boData.get("attributes"));
					entityMatrixBoDict.add(key, dummy);
				}
			}

			json.add("entityMatrixBoDict", entityMatrixBoDict);
			// FIXME: TIME CONSUMING INSTANCIATING RVALUEOBJECT
			// json.add("bos", getJsonBos(lstEoRows));
			json.add("xrefData", xrefData);

			json.add("cellInputType", cell_input_type);
			json.add("editable", editable);

			// WebContext.LOG.info("AFTER TIME CONSUMING STUFF");
			return json.build();

		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.ENTITY, entity_x));
		}
	}

	@GET
	@Path("/tree/{boMetaId}/{boId}")
	@RestServiceInfo(identifier = "tree", description = "Tree")
	public JsonArray tree(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId) {
		return Rest.facade().getTree(boMetaId, boId).build();
	}

	@GET
	@Path("/tree/{boMetaId}/{boId}/{search}")
	@RestServiceInfo(identifier = "tree", description = "Tree")
	public JsonArray treeWithSearch(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, @PathParam("search") String search) throws CommonBusinessException {
		return Rest.facade().getTreeWithSearch(boMetaId, boId, search).build();
	}

	@GET
	@Path("/subtree/{node_id}")
	@RestServiceInfo(identifier = "subtree", description = "Subtree for a given node_id")
	public JsonArray subtree(@PathParam("node_id") String nodeId) {
		return Rest.facade().getSubtree(nodeId).build();
	}

	@GET
	@Path("/datasource/{datasourceId}")
	@RestServiceInfo(identifier = "datasource", description = "Executes the datasource and return the plain result as JsonArray. Returns max. 250.000 rows (DbExecutor.LimitedResultSetRunner.MAXFETCHSIZE)")
	public JsonArray datasource(@PathParam("datasourceId") String datasourceId) {
		return datasource(datasourceId, null);
	}

	@GET
	@Path("/datasource/{datasourceId}/{maxRowCount}")
	@RestServiceInfo(identifier = "datasource", description = "Executes the datasource and return the plain result as JsonArray. The default for maxRowCount is 250.000 (DbExecutor.LimitedResultSetRunner.MAXFETCHSIZE)")
	public JsonArray datasource(@PathParam("datasourceId") String datasourceId, @PathParam("maxRowCount") String maxRowCount) {
		try {
			final UID datasourceUID = Rest.facade().translateFqn(E.DATASOURCE, datasourceId);
			if (!Rest.facade().isDatasourceAllowed(datasourceUID)) {
				throw new NuclosWebException(Response.Status.FORBIDDEN);
			}

			final Map<String, String> datasourceParams = new HashMap<>();
			for (String key : getQueryParameters().keySet()) {
				datasourceParams.put(key, getFirstParameter(key, String.class));
			}
			final Integer iMaxRowCount = maxRowCount == null ?
					DbExecutor.LimitedResultSetRunner.MAXFETCHSIZE :
					Integer.parseInt(maxRowCount);
			return Rest.facade().executeDatasource(datasourceUID, datasourceParams, iMaxRowCount);
		} catch (Exception ex) {
			throw new NuclosWebException(ex, E.DATASOURCE.getUID());
		}
	}


}