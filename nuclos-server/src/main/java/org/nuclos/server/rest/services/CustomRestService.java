package org.nuclos.server.rest.services;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.nuclos.api.context.CustomRestContext;
import org.nuclos.server.rest.services.helper.CustomRestServiceHelper;
import org.nuclos.server.security.UserFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * executes CustomRestRule methods
 * @see org.nuclos.api.rule.CustomRestRule
 */
@Path("/execute")
@Produces(MediaType.APPLICATION_JSON)
public class CustomRestService extends CustomRestServiceHelper {

	@Autowired
	UserFacadeLocal userFacade;

	/**
	 * @see org.nuclos.api.rule.CustomRestRule
	 *
	 * @param className
	 * @param methodName
	 * @return
	 * @throws IllegalAccessException
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	@GET
	@Path("/{className}/{methodName}")
	@Consumes({MediaType.APPLICATION_JSON})
	public String executeMethod(
			@PathParam("className") String className,
			@PathParam("methodName") String methodName
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, GET.class, null);
	}

	@POST
	@Path("/{className}/{methodName}")
	@Consumes({MediaType.APPLICATION_JSON})
	public String executeMethodPostJson(
			final @PathParam("className") String className,
			final @PathParam("methodName") String methodName,
			final JsonObject data
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, POST.class, data);
	}

	@PUT
	@Path("/{className}/{methodName}")
	@Consumes({MediaType.APPLICATION_JSON})
	public String executeMethodPut(
			final @PathParam("className") String className,
			final @PathParam("methodName") String methodName,
			final JsonObject data
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, PUT.class, data);
	}

	@DELETE
	@Path("/{className}/{methodName}")
	@Consumes({MediaType.APPLICATION_JSON})
	public String executeMethodDelete(
			final @PathParam("className") String className,
			final @PathParam("methodName") String methodName,
			final JsonObject data
	) throws IllegalAccessException {
		return invokeRestRuleMethod(className, methodName, DELETE.class, data);
	}
}