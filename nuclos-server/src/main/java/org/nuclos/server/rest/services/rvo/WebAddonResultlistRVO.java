//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.businessentity.WebAddonProperty;
import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.rest.ejb3.Rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebAddonResultlistRVO implements Serializable {

	private static final Logger LOG = LoggerFactory.getLogger(WebAddonResultlistRVO.class);

	private static final long serialVersionUID = 1L;

	private String boMetaId;
	private List<WebAddonPropertyRVO> properties = new ArrayList<>();

	public WebAddonResultlistRVO() {
	}

	public WebAddonResultlistRVO(final UID entityUID, final Collection<EntityObjectVO<UID>> resultlists, final Map<UID, String> propertyNameCache) {
		boMetaId = Rest.facade().translateUid(E.ENTITY, entityUID);
		for (EntityObjectVO<UID> rlEo : resultlists) {
			if (RigidUtils.equal(entityUID, rlEo.getFieldUid(E.WEBADDON_RESULTLIST.entity))) {
				String value = rlEo.getFieldValue(E.WEBADDON_RESULTLIST.propertyValue);
				String name = rlEo.getFieldValue(E.WEBADDON_RESULTLIST.webAddonProperty.getUID(), String.class);
				if (name == null) {
					final UID propertyUID = rlEo.getFieldUid(E.WEBADDON_PROPERTY.getUID());
					if (propertyUID != null) {
						name = propertyNameCache.get(propertyUID);
						if (name == null) {
							try {
								final EntityObjectVO<UID> eoProperty = Rest.facade().getThin(E.WEBADDON_PROPERTY.getUID(), propertyUID, false);
								name = eoProperty.getFieldValue(E.WEBADDON_PROPERTY.property);
							} catch (CommonPermissionException e) {
								LOG.error(e.getMessage(), e);
							}
							propertyNameCache.put(propertyUID, name);
						}
					}
				}
				WebAddonPropertyRVO property = new WebAddonPropertyRVO();
				property.setName(name);
				property.setValue(value);
				properties.add(property);
			}
		}
	}

	public String getBoMetaId() {
		return boMetaId;
	}

	public void setBoMetaId(final String boMetaId) {
		this.boMetaId = boMetaId;
	}

	public List<WebAddonPropertyRVO> getProperties() {
		return properties;
	}

	public void addWebAddonProperty(WebAddonPropertyRVO webAddonProperty) {
		properties.add(webAddonProperty);
	}
}
