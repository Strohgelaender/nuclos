package org.nuclos.server.rest.services;

import java.net.URLConnection;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.nuclos.common.E;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache.CacheResult;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Path("/bos")
@Produces(MediaType.APPLICATION_JSON)
public class BoService extends DataServiceHelper {

	@GET
	@RestServiceInfo(identifier = "boMetas", isFinalized = true, description = "List of all readable Businessobject-metas")
	public JsonArray bometaList() {
		return getBOMetaList().build();
	}

	@GET
	@Path("/{boMetaId}")
	@RestServiceInfo(identifier = "bos", isFinalized = true, description = "List of Data (Rows)")
	public JsonObject bolist(@PathParam("boMetaId") String boMetaId) {
		return getBoList(boMetaId, null).build();
	}

	@GET
	@Path("/{boMetaId}/query")
	@RestServiceInfo(identifier = "bosQuery", isFinalized = true, description = "List of Data (Rows) by query object")
	public JsonObject getBolist(@PathParam("boMetaId") String boMetaId, @QueryParam("where") String where) {
		JsonObject queryContext = null;
		if (!StringUtils.isNullOrEmpty(where)) {
			queryContext = Json.createObjectBuilder().add("where", where).build();
		}
		return getBoList(boMetaId, queryContext).build();
	}

	//TODO: Both "query"-Methods have to be revalidated. First, they have the same signature with the finalized method:
	//
	//@GET
	//@Path("/{boMetaId}/{boId}")
	//@RestServiceInfo(identifier="bo", isFinalized=true, description="Full Data Row Details")
	//
	//Furthermore it is not HATEOAS Standard if a query-only service is a POST
	//
	//It's important that NOT-finalized Signatures do not have any fixed hard-coded links in the webclient as they may change in future.

	@POST
	@Path("/{boMetaId}/query")
	@RestServiceInfo(identifier = "bosQuery", isFinalized = true, description = "List of Data (Rows) by query object")
	@Consumes({MediaType.APPLICATION_JSON})
	public JsonObject bolist(@PathParam("boMetaId") String boMetaId, JsonObject queryContext) {
		return getBoList(boMetaId, queryContext).build();
	}


	@POST
	@Path("/{boMetaId}/boListExport/{format}/{pageOrientationLandscape}/{isColumnScaled}")
	@RestServiceInfo(identifier = "boListExport", isFinalized = false, description = "Export list of data as pdf/csv/xls")
	@Consumes({MediaType.APPLICATION_JSON})
	public ResultListExportRVO boListExport(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("format") String format,
			@PathParam("pageOrientationLandscape") boolean pageOrientationLandscape,
			@PathParam("isColumnScaled") boolean isColumnScaled,
			JsonObject queryContext
	) {
		return getBoListExport(boMetaId, format, pageOrientationLandscape, isColumnScaled, queryContext);
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/{boMetaId}/boListExport/{outputFormatId}/{fileId}")
	@RestServiceInfo(identifier = "boListExportFile", isFinalized = true, description = "Get the exported file")
	public Response boListExportFile(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("outputFormatId") String outputFormatId,
			@PathParam("fileId") String fileId) {

		RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);
		try {

			CacheResult cached = downloadCache.get(boMetaId, null, outputFormatId, fileId);

			if (cached != null) {
				ResponseBuilder responseBuilder = Response.ok(cached.content).type(URLConnection.guessContentTypeFromName(cached.fileName));
				responseBuilder.header("Content-Disposition", "attachment; filename=\"" + cached.fileName + "\"");
				return responseBuilder.build();
			}

			return Response.noContent().build();

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}


	// @Transactional required for org.nuclos.server.rest.services.BoDocumentService.getUploadedFile()
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
	@POST
	@Path("/{boMetaId}")
	@RestServiceInfo(identifier = "boinsert", isFinalized = true, description = "Data Row Insert")
	@Consumes({MediaType.APPLICATION_JSON})
	public JsonObject boinsert(@PathParam("boMetaId") String boMetaId, JsonObject data) {
		return insertBo(boMetaId, data).build();
	}

	@GET
	@Path("/{boMetaId}/{boId}")
	@RestServiceInfo(identifier = "bo", isFinalized = true, description = "Full Data Row Details")
	public JsonObject bodetails(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId) {
		return getBoSelf(boMetaId, boId).build();
	}

	// @Transactional required for org.nuclos.server.rest.services.BoDocumentService.getUploadedFile()
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
	@PUT
	@Path("/{boMetaId}/{boId}")
	@RestServiceInfo(identifier = "boupdate", isFinalized = true, description = "Full Data Row Update")
	@Consumes({MediaType.APPLICATION_JSON})
	public JsonObject boupdate(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, JsonObject data) {
		return updateBo(boId, data).build();
	}

	// @Transactional required for org.nuclos.server.rest.services.BoDocumentService.getUploadedFile()
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
	@DELETE
	@Path("/{boMetaId}/{boId}")
	@RestServiceInfo(identifier = "bodelete", isFinalized = true, description = "Full Data Row Delete")
	public Response bodelete(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId) {
		return deleteBo(boMetaId, boId);
	}

	@DELETE
	@Path("/{boMetaId}/{boId}/lock")
	@RestServiceInfo(identifier = "bos/{boMetaId}/{boId}/lock", isFinalized = true, description = "Unlocks the Row")
	public Response bounlock(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId) {
		return unlockBo(boMetaId, boId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
	@POST
	@Path("/{boMetaId}/{boId}/execute/{ruleId}")
	@RestServiceInfo(identifier = "bos/{boMetaId}/{boId}/execute/{ruleId}", description = "Executes a custom rule.")
	@Consumes({MediaType.APPLICATION_JSON})
	public JsonObject executeCustomRule(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("ruleId") String ruleId,
			final JsonObject data    // TODO: Only needed for input-required data, get rid of this
	) {
		return executeRule(boMetaId, boId, ruleId, data).build();
	}
}