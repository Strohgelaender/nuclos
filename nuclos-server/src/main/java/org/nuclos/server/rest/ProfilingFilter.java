package org.nuclos.server.rest;

import java.net.URI;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import org.glassfish.jersey.server.ContainerRequest;
import org.nuclos.server.dblayer.impl.DataSourceExecutor;
import org.nuclos.server.dblayer.impl.util.RequestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Oliver Brausch on 18.10.17.
 */
public class ProfilingFilter implements ContainerRequestFilter, ContainerResponseFilter {
	private static final Logger LOG = LoggerFactory.getLogger(ProfilingFilter.class);
	@Override
	public void filter(final ContainerRequestContext containerRequestContext) {
		DataSourceExecutor.initRequestInfo();
	}

	@Override
	public void filter(
			final ContainerRequestContext containerRequestContext,
			final ContainerResponseContext containerResponseContext
	) {
		RequestInfo requestInfo = DataSourceExecutor.getRequestInfo();
		if (requestInfo == null) {
			return;
		}

		URI uri = ((ContainerRequest)containerRequestContext).getRequestUri();
		if (requestInfo.getCountSQL() < 0) {
			LOG.debug("RequestInfo Time:" + requestInfo.getTime() + "(" + uri + ")");
			return;
		}
		LOG.info(requestInfo + " (" + uri + ")");

		containerResponseContext.getHeaders().add("nuclos-sql-count", requestInfo.getCountSQL());
		containerResponseContext.getHeaders().add("nuclos-sql-insert-update-delete-count", requestInfo.getCountInsertUpdateDelete());
		if (requestInfo.getTimeSQL() > 0) {
			containerResponseContext.getHeaders().add("nuclos-sql-time", requestInfo.getTimeSQL());
		}
	}
}
