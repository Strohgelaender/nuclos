package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.layout.LayoutInfo;
import org.nuclos.common2.layoutml.AbstractWebComponent;
import org.nuclos.common2.layoutml.AbstractWebDependents;
import org.nuclos.common2.layoutml.IHasTableLayout;
import org.nuclos.common2.layoutml.IWebConstraints;
import org.nuclos.common2.layoutml.IWebContainer;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.NuclosRuleEvent;
import org.nuclos.common2.layoutml.NuclosSubformColumn;
import org.nuclos.common2.layoutml.ParsedLayoutComponents;
import org.nuclos.common2.layoutml.WebChart;
import org.nuclos.common2.layoutml.WebCollectableComponent;
import org.nuclos.common2.layoutml.WebRoot;
import org.nuclos.common2.layoutml.WebStaticComponent;
import org.nuclos.common2.layoutml.WebSubform;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebLayout;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebLayout implements IWebLayout {

	private static final Logger LOG = LoggerFactory.getLogger(WebLayout.class);
	
	private final LayoutInfo layoutInfo;
	private final ParsedLayoutComponents parsedLayoutComps;
	
	public WebLayout(LayoutInfo layoutInfo) throws LayoutMLException { 
		this.layoutInfo = layoutInfo;
		
		if (layoutInfo != null && layoutInfo.getLayoutUID() != null) {
			this.parsedLayoutComps = new LayoutMLParser().getParsedLayoutComponents(layoutInfo);			
		} else {
			this.parsedLayoutComps = ParsedLayoutComponents.dummyParsedLayoutComponents;
		}
		
	}
	
	@Override
	public LayoutInfo getLayoutInfo() {
		return layoutInfo;
	}

	public JsonObjectBuilder getFlattenedAndTabIndexedLayout() {
		WebRoot root = parsedLayoutComps.getWebRoot();
		
//		LOG.info("L1:" + JsonFactory.buildJsonForWebComponents(root, this).build());
		
		flattenLayout(root);
//		LOG.info("L2:" + JsonFactory.buildJsonForWebComponents(root, this).build());
		buildTabIndex(root, root.getComponents());
		
		return JsonFactory.buildJsonForWebComponents(root, this);
		
	}
	
	private static void flattenLayout(IWebContainer webContainer) {
		List<AbstractWebComponent> lstComponents = webContainer.getComponents();
		
		for (AbstractWebComponent webComponent : lstComponents) {
			if (webComponent instanceof IWebContainer) {
				flattenLayout((IWebContainer)webComponent);
			}
		}
		
		if (webContainer instanceof IHasTableLayout && lstComponents.size() == 1 && lstComponents.get(0) instanceof IWebContainer) {
			IWebContainer subContainer = (IWebContainer) lstComponents.get(0);
			
			if (subContainer.getConstraints() == null || subContainer.getConstraints().isQuasiEmpty()) {
				IHasTableLayout ihtl = (IHasTableLayout) webContainer;
				
				if (ihtl.getWebTableLayout() == null || ihtl.getWebTableLayout().isQuasiEmpty()) {				
					webContainer.liftGrandChildren();
					
				}
			}
		}
		
	}
	
	private static void buildTabIndex(WebRoot webContainerRoot, List<AbstractWebComponent> components) {
		List<AbstractWebComponent> sortedComponents = new ArrayList<AbstractWebComponent>(components);
		Collections.sort(sortedComponents);
		
		for (AbstractWebComponent component : sortedComponents) {
			if(component instanceof WebCollectableComponent && !((WebCollectableComponent)component).isJustLabel()) {
				buildTabIndexForWebComponent(webContainerRoot, component, sortedComponents);
				break;
			}
			
		}	
		
		for (AbstractWebComponent component : sortedComponents) {
			if (component instanceof IWebContainer) {
				IWebContainer container = (IWebContainer) component;
				buildTabIndex(webContainerRoot, container.getComponents());
			}
		}

	}
	
	private static void buildTabIndexForWebComponent(WebRoot webContainerRoot, AbstractWebComponent component, List<AbstractWebComponent> components) {
		if (component.getTabIndex() == null) {
			component.setTabindex(webContainerRoot.getNextTabindex());
			AbstractWebComponent nextComponent = getNextFieldComponent(component, components);
			if (nextComponent != null) {
				buildTabIndexForWebComponent(webContainerRoot, nextComponent, components);
				
			}
		}
	}
	
	private static AbstractWebComponent getNextFieldComponent(AbstractWebComponent component, List<AbstractWebComponent> components) {
		String nextfocusfield = component.getNextfocusfield();
		String nextcomponent = component.getNextcomponent();
		
		String reference = nextfocusfield != null ? nextfocusfield : nextcomponent;
		if (reference != null) {
			for (AbstractWebComponent c : components) {
				if (c.getUID() == null) {
					continue;
				}
				String currentUid = c.getUID().toString(); // could be uid or name
				
				if (reference.indexOf(currentUid) != -1 
						&& (c instanceof WebCollectableComponent && !((WebCollectableComponent)c).isJustLabel() || c instanceof WebStaticComponent)) {
						return c;
				}
			}
		} else { // get next field after current
			boolean foundCurrentField = false;
			for (AbstractWebComponent c : components) {
				if (c.getUID() != null && c.getUID().equals(component.getUID())) {
					foundCurrentField = true; 
					continue;
				}
				if (foundCurrentField) {
					if(c instanceof WebCollectableComponent && !((WebCollectableComponent)c).isJustLabel()) {
						return c;
					}
				}
			}
		}
		return null;
	}
	
	private final Map<UID, FieldMeta<?>> fieldCache = new HashMap<UID, FieldMeta<?>>();
	
	private FieldMeta<?> getField(UID uid) {
		if (!fieldCache.containsKey(uid)) {
			fieldCache.put(uid, Rest.getEntityField(uid));
		}
		return fieldCache.get(uid);
	}
	
	@Override
	public LayoutAdditions getLayoutAdditionsForWebComponenet(AbstractWebComponent component) {
		if (component == null || component.isJustLabel()) {
			return null;
		}
		
		FieldMeta<?> fm = getField(component.getUID());
		if (fm == null) {
			return null;
		}
		
		List<NuclosRuleEvent> ruleEvents = getNuclosRuleEventsForComponent(null, fm);
		if (!component.isVisible() && ruleEvents.isEmpty()) {
			return null; //NUCLOS-5425 No need for invisible components without layoutml rules.
		}
		
		return new LayoutAdditions(component, fm, ruleEvents);
	}

	@Override
	public Map<UID, AbstractWebDependents> getVisibleSubLists() {
		Map<UID, AbstractWebDependents> mpSublists = new HashMap<UID, AbstractWebDependents>();
		
		for (List<AbstractWebComponent> lstComp : parsedLayoutComps.getMpComponents().values()) {
			for (AbstractWebComponent comp : lstComp) {
 				if (comp.isVisible() && comp instanceof AbstractWebDependents) {
 					AbstractWebDependents sub = (AbstractWebDependents) comp;
 					UID fkField = sub.getForeignkeyfield();
					mpSublists.put(fkField, sub);
				}
		 	}
		}
		
		return mpSublists;
	}

	@Override
	public WebSubform getWebSubform(UID subformUID) {
		List<AbstractWebComponent> lstComp = parsedLayoutComps.getMpComponents().get(subformUID);
		//Only the first subform for specified BO will be processed.
		if (lstComp != null) {
			for (AbstractWebComponent comp : lstComp) {
				if (comp instanceof WebSubform) {
					return (WebSubform) comp;
				}				
			}
		}
		return null;
	}
	
	private List<NuclosRuleEvent> getNuclosRuleEventsForComponent(UID subform, FieldMeta<?> fm) {
		List<NuclosRuleEvent> lstResult = new ArrayList<NuclosRuleEvent>();
		List<NuclosRuleEvent> lstEvents = parsedLayoutComps.getMpRuleEvents().get(subform);
		if (lstEvents != null) {
			for (NuclosRuleEvent nre : lstEvents) {
				if (nre.getSourceComponent().equals(fm.getUID())) {
					lstResult.add(nre);
				}
			}
		}
		
		if (subform == null && layoutInfo.doesLayoutChangesWithProcess() && SF.PROCESS.checkField(fm.getEntity(), fm.getUID())) {
			lstResult.add(NuclosRuleEvent.createCheckLayoutChangeRule(fm.getUID()));
		}
		
		return lstResult;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WebLayout) {
			WebLayout that = (WebLayout)obj;
			return LangUtils.equal(that.layoutInfo, layoutInfo);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(layoutInfo);
	}
	
	//Soon obsolete part
	
	private final Map<UID, Map<UID, LayoutAdditions>> compCache = new HashMap<UID, Map<UID, LayoutAdditions>>();
	
	@Override
	public List<LayoutAdditions> getComponents(UID entity, UID reffield, boolean onlyVisibleComponents) {
		return Collections.unmodifiableList(fetchVisibleComps(entity, reffield, onlyVisibleComponents));
	}
		
	private List<LayoutAdditions> fetchVisibleComps(UID entity, UID reffield, boolean onlyVisibleComponents) {
		
		final List<LayoutAdditions> lstComps = new ArrayList<LayoutAdditions>();
		//TODO: Slow for DB far from Server, 100ms
		final Collection<FieldMeta<?>> collFm = Rest.facade().getAllFieldsByEntity(entity, false);
		List<FieldMeta<?>> sortedFm = new ArrayList<FieldMeta<?>>(collFm);
		Collections.sort(sortedFm);
		
		if (!compCache.containsKey(entity)) {
			compCache.put(entity, new HashMap<UID, LayoutAdditions>());
		}
		Map <UID, LayoutAdditions> thisCompCache = compCache.get(entity);

		for (FieldMeta<?> fm : sortedFm) {
			if (thisCompCache.containsKey(fm.getUID())) {
				LayoutAdditions frvo = thisCompCache.get(fm.getUID());
				if (frvo != null) {
					lstComps.add(frvo);
				}
				continue;
			}
			
    		//Note: NUCLOS-4099 requests that the NuclosRowColor is not displayed in the web-client.
    		//TODO: Evaluate if this (within the REST-Service) is the right place to filter this attribute out of the layout
			if (fm.isNuclosRowColor()) {
				continue;
			}
			
			boolean bmemo = false;
			boolean readonly = false;
			WebValueListProvider vlp = null;
			Map<String, String> properties = null;
			IWebConstraints constraints = null;
			Integer tabIndex = null;
			
			LayoutAdditions layoutAdditions = null;
			
			if (reffield == null) {
				// State should be included in FieldsAndRestrictions later...
				//				if (!fm.getUID().equals(SF.STATE.getUID(fm.getEntity())) &&
				//				    !fm.getUID().equals(SF.STATENUMBER.getUID(fm.getEntity()))) {
					
				List<AbstractWebComponent> lstComp = parsedLayoutComps.getMpComponents().get(fm.getUID());
				if (lstComp == null || lstComp.isEmpty()) {
					continue;
				}				
				AbstractWebComponent ncomp = lstComp.get(0);
				if (onlyVisibleComponents && !ncomp.isVisible()) {
					continue;
				}
				
				layoutAdditions = getLayoutAdditionsForWebComponenet(ncomp);

			} else {
				List<AbstractWebComponent> lstComp = parsedLayoutComps.getMpComponents().get(entity);
				
				boolean visible = true;
				String label = null;
				for (AbstractWebComponent comp : lstComp) {
					if (comp instanceof WebSubform) {
						WebSubform subform = (WebSubform) comp;
						
						if (reffield.equals(subform.getForeignkeyfield())) {
							readonly = !subform.isEnabled();
							
							NuclosSubformColumn col = subform.getSubFormColumns().get(fm.getUID());
														
							if (col != null) {
								if (!col.isVisible()) {
									visible = false;
									continue;
								} else if (!col.isEnabled()) {
									readonly = true;						
								}
								vlp = col.getValueListProvider();
								properties = col.getProperties();
								label = col.getLabel();
								tabIndex = subform.getTabIndex();
							}
							
							continue;
						}
					}
				}

				if (visible && !fm.getUID().equals(SF.LOGICALDELETED.getUID(fm.getEntity()))) {
					List<NuclosRuleEvent> ruleEvents = getNuclosRuleEventsForComponent(entity, fm);
					layoutAdditions = new LayoutAdditions(fm, readonly, bmemo, ruleEvents, label, vlp, constraints, tabIndex, properties);
				}
			}

			if (layoutAdditions == null) {
				continue;
			}
			
			lstComps.add(layoutAdditions);
			
			thisCompCache.put(layoutAdditions.getUID(), layoutAdditions);
		}
		return lstComps;
	}
	
	@Override
	public boolean hasChart(UID uid) {
		List<AbstractWebComponent> lstComp = parsedLayoutComps.getMpComponents().get(uid);
		if (lstComp != null) {
			for (AbstractWebComponent comp : lstComp) {
				if (comp instanceof WebChart) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	public JsonObjectBuilder getSubformInfo(Long relatedPK) {
		
		MetaProvider metaProvider = MetaProvider.getInstance();
		
		JsonObjectBuilder result = Json.createObjectBuilder();
		JsonObjectBuilder subforms = Json.createObjectBuilder();
		
		for(UID key : parsedLayoutComps.getMpComponents().keySet()) {
			if (key != null && Rest.translateUid(E.ENTITY, key) != null) {
				
				List<AbstractWebComponent> webcomponentList = parsedLayoutComps.getMpComponents().get(key);

				for (AbstractWebComponent webcomponent: webcomponentList) {
					if (webcomponent instanceof WebSubform) {
						WebSubform subform = ((WebSubform)webcomponent);
						
						//NUCLOS-4736 2) und 3)
						if (!subform.isFirstSubformWithinTab()) {
							continue;
						}
						
						try {
							
							UID field = subform.getForeignkeyfield();
							
							Long count = 0L;
							if (relatedPK != null) {
								CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.AND);
								final CollectableSearchCondition cond1 = SearchConditionUtils.newPkComparison(field, ComparisonOperator.EQUAL, relatedPK);
								cond.addOperand(cond1);
								
								if (metaProvider.getEntity(subform.getEntity()) != null && metaProvider.getEntity(subform.getEntity()).isStateModel()) {
									final CollectableSearchCondition cond2 = SearchConditionUtils.newComparison(subform.getEntity(), SF.LOGICALDELETED, ComparisonOperator.EQUAL, Boolean.FALSE);
									cond.addOperand(cond2);
								}
								
								CollectableSearchExpression cse = new CollectableSearchExpression(cond);
								count = Rest.countEntityObjectRows(subform.getEntity(), cse);
							}
					
							JsonObjectBuilder subformJson = Json.createObjectBuilder();
							if(count!=null) subformJson.add("count", count);
							
							subforms.add(Rest.translateUid(E.ENTITYFIELD, subform.getForeignkeyfield()), subformJson);
							
						} catch (Exception e) {
							throw new NuclosWebException(e, layoutInfo != null ? layoutInfo.getLayoutUID() : null);
						}
					}
				}
			}
			 
		}

		result.add("subforms", subforms);
		return result;
	}
	
	private List<WebStaticComponent> lstEnabledButtons;
	@Override
	public List<WebStaticComponent> getEnabledButtons() {
		if (lstEnabledButtons == null) {
			lstEnabledButtons = new ArrayList<WebStaticComponent>();
			
			Map<UID, WebStaticComponent> mpComps = parsedLayoutComps.getMpStaticComps();
			for (UID uid : mpComps.keySet()) {
				WebStaticComponent comp = mpComps.get(uid);
				
				if (comp.isEnabled() && comp.isButton() && !lstEnabledButtons.contains(comp)) {
					lstEnabledButtons.add(comp);
				}
			}
			
		}
		
		return lstEnabledButtons;
	}
	
}