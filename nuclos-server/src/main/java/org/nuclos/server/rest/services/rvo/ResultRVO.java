package org.nuclos.server.rest.services.rvo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.services.rvo.RValueObject.BoLinksFactory;
import org.nuclos.server.rest.services.rvo.RValueObject.UsageProperties;

public class ResultRVO<PK> extends AbstractJsonRVO<PK> {
	private final Boolean all;
	private final Integer total;
	private final String boType;
	private final List<JsonRVO> bos;

	public ResultRVO(
			Collection<EntityObjectVO<PK>> collEo,
			Boolean all,
			Integer total,
			JsonBuilderConfiguration jsonConfig,
			IWebContext webContext,
			UID sessionDataLanguageUID
	) throws CommonBusinessException {
		this(
				collEo,
				all,
				total,
				jsonConfig,
				webContext,
				sessionDataLanguageUID,
				null,
				null
		);
	}

	public ResultRVO(
			Collection<EntityObjectVO<PK>> collEo,
			Boolean all,
			Integer total,
			JsonBuilderConfiguration jsonConfig,
			IWebContext webContext,
			UID sessionDataLanguageUID,
			String boType,
			BoLinksFactory boLinksFactory
	) throws CommonBusinessException {
		super(null);
		this.total = total;
		this.all = all;
		this.boType = boType;
		this.bos = new ArrayList<>();
		Map<UsageCriteria, UsageProperties> mpUsageProperties = new HashMap<UsageCriteria, RValueObject.UsageProperties>();

		for (EntityObjectVO<PK> eo : collEo) {
			UsageCriteria uc = Rest.getUsageCriteriaFromEO(eo);
			if (!mpUsageProperties.containsKey(uc)) {
				mpUsageProperties.put(uc, new UsageProperties(uc, webContext));
			}
			bos.add(new RValueObject<>(eo, jsonConfig, mpUsageProperties.get(uc), boLinksFactory, sessionDataLanguageUID));
		}
	}

	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		JsonObjectBuilder json = Json.createObjectBuilder();
		if (all != null) {
			json.add("all", all);
		}
		if (total != null) {
			json.add("total", total);
		}
		if (boType != null) {
			json.add("bo_type", boType);
		}

		addArray(json, "bos", bos);
		return json;
	}

	private static <PK> Collection<EntityObjectVO<PK>> collEo(EntityObjectVO<PK> eo) {
		Collection<EntityObjectVO<PK>> lstEO = new ArrayList<EntityObjectVO<PK>>();
		lstEO.add(eo);
		return lstEO;
	}
}
