package org.nuclos.server.rest.services;

import javax.json.JsonObject;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;

@Path("/boStateChanges")
@Produces(MediaType.APPLICATION_JSON)
public class BoStateService extends DataServiceHelper {

	@PUT
	@Path("/{boMetaId}/{boId}/{stateId}")
	@RestServiceInfo(identifier="boStateChange", description="Change status of BO")
	public Response boStateChange(
			@PathParam("boMetaId") String boMetaId,
			@PathParam("boId") String boId,
			@PathParam("stateId") String stateId,
			JsonObject data
	) {
		try {
			// Try to parse stateId as Integer, because it could be a status numeral
			Integer stateNumeral = Integer.parseInt(stateId);
			return stateChange(boMetaId, boId, stateNumeral);
		} catch (NumberFormatException ex) {
			// Ignore
		}

		return stateChange(boMetaId, boId, stateId, data);
	}

}