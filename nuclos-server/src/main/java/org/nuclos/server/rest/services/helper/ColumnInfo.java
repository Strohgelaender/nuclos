package org.nuclos.server.rest.services.helper;

import org.nuclos.common.FieldMeta;
import org.nuclos.server.rest.ejb3.Rest;

public class ColumnInfo {
	private final FieldMeta<?> fieldMeta;
	private final Integer columnWidth;
	private final String sortColumn;
	private final Integer sortColumnOrder;
	private final String label;
	private final String type;
	
	public ColumnInfo(FieldMeta<?> fieldMeta, Integer columnWidth, String sortColumn, Integer sortColumnOrder) {
		this.fieldMeta = fieldMeta;
		this.columnWidth = columnWidth;
		this.sortColumn = sortColumn;
		this.sortColumnOrder = sortColumnOrder; 
		this.label = Rest.getLabelFromMetaFieldDataVO(fieldMeta);
		String type = fieldMeta.getJavaClass().getName();
		if(type != null && type.indexOf('.') != -1) {
			type = type.substring(type.lastIndexOf('.') + 1);
		}
		
		if(fieldMeta.getJavaClass().getSuperclass() != null && fieldMeta.getJavaClass().getSuperclass() == Number.class) {
			type = "Number";
		} else if("Hyperlink".equals(fieldMeta.getDefaultComponentType())) {
			type = "Hyperlink";
		}
		this.type = type;
	}

	public FieldMeta<?> getFieldMeta() {
		return fieldMeta;
	}

	public Integer getColumnWidth() {
		return columnWidth;
	}

	public String getSortColumn() {
		return sortColumn;
	}
	
	public Integer getSortColumnOrder() {
		return sortColumnOrder;
	}

	public String getLabel() {
		return label;
	}

	public String getType() {
		return type;
	}
	
}
