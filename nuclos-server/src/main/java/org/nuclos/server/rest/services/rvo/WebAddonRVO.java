//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.security.UserVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rest.ejb3.Rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebAddonRVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String webAddonId;
	private String name;

	private List<WebAddonResultlistRVO> resultlists = new ArrayList<>();

	public WebAddonRVO() {
	}

	public WebAddonRVO(EntityObjectVO<UID> eo, final Map<UID, String> propertyNameCache) {
		webAddonId = Rest.facade().translateUid(E.WEBADDON, eo.getPrimaryKey());
		name = eo.getFieldValue(E.WEBADDON.name);
		if (!eo.getDependents().hasData(E.WEBADDON_RESULTLIST.webAddon)) {
			eo.getDependents().addAllData(
					E.WEBADDON_RESULTLIST.webAddon,
					Rest.facade().getDependentData(E.WEBADDON_RESULTLIST.getUID(), E.WEBADDON_RESULTLIST.webAddon.getUID(), eo.getPrimaryKey())
			);
		}
		Set<UID> entityUIDs = new HashSet<>();
		for (EntityObjectVO rlEo : eo.getDependents().getData(E.WEBADDON_RESULTLIST.webAddon)) {
			entityUIDs.add(rlEo.getFieldUid(E.WEBADDON_RESULTLIST.entity));
		}
		for (UID entityUID : entityUIDs) {
			WebAddonResultlistRVO rlRvo = new WebAddonResultlistRVO(entityUID, eo.getDependents().<UID>getDataPk(E.WEBADDON_RESULTLIST.webAddon), propertyNameCache);
			resultlists.add(rlRvo);
		}
	}

	public String getWebAddonId() {
		return webAddonId;
	}

	public String getName() {
		return name;
	}

	public void setWebAddonId(final String webAddonId) {
		this.webAddonId = webAddonId;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<WebAddonResultlistRVO> getResultlists() {
		return resultlists;
	}

	public void addWebAddonResultlist(WebAddonResultlistRVO webAddonResultlist) {
		resultlists.add(webAddonResultlist);
	}
}
