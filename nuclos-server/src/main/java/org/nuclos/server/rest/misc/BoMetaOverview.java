package org.nuclos.server.rest.misc;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common2.resource.ResourceResolver;

public class BoMetaOverview implements IMenuEntry {
	private final EntityMeta<?> eMeta;
	private final UID processUid;
	private final String label;
	private final EntityPermission permission;
	private final boolean bAsMenuTree;
	private final boolean bCreateNew;

	public BoMetaOverview(
			EntityMeta<?> eMeta,
			String label,
			EntityPermission permission,
			boolean bAsMenuTree,
			UID processUid,
			boolean createNew
	) {
		this.eMeta = eMeta;
		this.label = label;
		this.permission = permission;
		this.bAsMenuTree = bAsMenuTree;
		this.processUid = processUid;
		this.bCreateNew = createNew;
	}

	public BoMetaOverview(
			EntityMeta<?> eMeta,
			String label,
			EntityPermission permission,
			boolean bAsMenuTree
	) {
		this(eMeta, label, permission, bAsMenuTree, null, false);
	}

	@Override
	public UID getUID() {
		return eMeta.getUID();
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getIcon() {
		return eMeta.getNuclosResource() != null ? eMeta.getNuclosResource() : ResourceResolver.DEFAULT_ICON;
	}

	@Override
	public boolean asMenuTree() {
		return bAsMenuTree;
	}

	@Override
	public boolean canCreateBO() {
		return eMeta.isEditable() && permission.isInsertAllowed();
	}

	@Override
	public boolean canReadBO() {
		return permission.isReadAllowed();
	}

	@Override
	public String toString() {
		return "Entity=" + eMeta
				+ "\nLabel=" + label
				+ "\nPermission=" + permission
				+ "\nAsMenuTree" + bAsMenuTree;
	}

	@Override
	public UID getProcessUid() {
		return processUid;
	}

	@Override
	public boolean isCreateNew() {
		return bCreateNew;
	}
}
