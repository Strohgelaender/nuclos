//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.activemq.pool.PooledConnectionFactory;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.SseFeature;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.MessageServiceHelper;
import org.nuclos.server.rest.services.helper.MessageServiceSessionListener;
import org.nuclos.server.rest.services.helper.MessageServiceThread;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Path("/msg")
public class MessageService extends MessageServiceHelper {	
	
	private static final Logger LOG = LoggerFactory.getLogger(MessageService.class);
	
	private PooledConnectionFactory jmsFactory;
	
	private MessageServiceThread thread;
	
	@GET
	@RestServiceInfo(identifier="msg", description="Returns JsonObject with Messages from the JMS.")
	@Consumes(SseFeature.SERVER_SENT_EVENTS)
	@Produces(SseFeature.SERVER_SENT_EVENTS)
    public EventOutput getServerSentEvents(@QueryParam("receiverId") Integer receiverId) {
		if (!SpringApplicationContextHolder.isSpringReady()) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
		}
		
		jmsFactory = SpringApplicationContextHolder.getBean(PooledConnectionFactory.class);
		thread = SpringApplicationContextHolder.getBean(MessageServiceThread.class);
		
        final EventOutput eventOutput = new EventOutput();
        if(thread!=null && jmsFactory!=null){
        	LOG.debug("register new subscriber with receiverId={}", receiverId);
        	thread.setJmsFactory(jmsFactory);
        	thread.setEventOutput(eventOutput);
        	thread.setReceiverId(receiverId);
        	thread.setName("MessageServiceThread");
        	MessageServiceSessionListener.addMessageServiceThread(getSessionId(), thread);
        	thread.start();
        }else{
        	//TO EARLAY
        }
        return eventOutput;
    }
	
}