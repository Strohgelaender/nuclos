//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.servlet.http.HttpServletRequest;

import org.nuclos.api.Direction;
import org.nuclos.api.Message;
import org.nuclos.api.command.CloseCommand;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.api.ApiCommandImpl;
import org.nuclos.common.api.ApiDirectionImpl;
import org.nuclos.common.api.ApiMessageImpl;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common2.File;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.layoutml.AbstractWebDependents;
import org.nuclos.common2.layoutml.WebStaticComponent;
import org.nuclos.common2.layoutml.WebSubform;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.nuclos.server.rest.ejb3.IRValueObject;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.IWebLayout;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.RestLinks.RestLink;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class RValueObject<PK> extends AbstractJsonRVO<PK> implements IRValueObject {

	private static final Logger LOG = LoggerFactory.getLogger(RValueObject.class);

	private final EntityObjectVO<PK> eo;
	private final LinkedHashMap<String, Object> data;
	private LinkedHashMap<String, Object> values;
	private Map<String, JsonArrayBuilder> arrays = null;
	private Set<String> imageAttributes;
	private Set<String> documentAttributes;
	private final UID sessionDataLanguageUID;

	private final JsonBuilderConfiguration jsonConfig;
	private final UsageProperties usageProperties;
	private final BoLinksFactory boLinksFactory;

	public RValueObject(
			EntityObjectVO<PK> eo,
			JsonBuilderConfiguration jsonConfig,
			UsageProperties usageProperties,
			BoLinksFactory boLinksFactory,
			UID sessionDataLanguageUID
	) throws CommonBusinessException {
		super(MetaProvider.getInstance().getEntity(eo.getDalEntity()));
		this.jsonConfig = jsonConfig;
		this.eo = eo;
		this.usageProperties = usageProperties;
		this.sessionDataLanguageUID = sessionDataLanguageUID;
		if (boLinksFactory != null) {
			this.boLinksFactory = boLinksFactory;
		} else {
			EntityMeta<?> eMeta = getEntityMeta();
			if (eMeta.isDynamicTasklist()) {
				final TasklistDefinition tlDef = (TasklistDefinition) eMeta.getProperty(EntityMeta.PROPERTY.TASKLIST_DEFINITION);
				if (tlDef == null) {
					throw new NuclosFatalException("No TASKLIST_DEFINITION property set");
				}
				UID detailEntityUID = tlDef.getTaskEntityUID();
				if (detailEntityUID == null) {
					final UID refToEntityMeta = tlDef.getDynamicTasklistEntityFieldUid();
					if (refToEntityMeta == null) {
						throw new NuclosFatalException("TasklistDefinition without information to show details?");
					}
					FieldMeta<?> refToEntityMetaField = (FieldMeta<?>) eMeta.getProperty(EntityMeta.PROPERTY.TASKLIST_REF_TO_ENTITYMETA_FIELD);
					if (refToEntityMetaField == null) {
						throw new NuclosFatalException("No TASKLIST_REF_TO_ENTITYMETA_FIELD property set");
					}
					final Object refToEntityMetaValue = eo.getFieldValue(refToEntityMetaField.getUID());
					if (refToEntityMetaValue == null) {
						throw new NuclosFatalException("Tasklist eo with ID " + eo.getPrimaryKey() + " contains no reference to detail entity meta information");
					}
					detailEntityUID = UID.parseUID(refToEntityMetaValue.toString());
				}
				final String detailEntityFQN = Rest.translateUid(E.ENTITY, detailEntityUID);
				this.boLinksFactory = new DefaultBoLinksFactory(detailEntityFQN);
			} else {
				this.boLinksFactory = new DefaultBoLinksFactory(this.usageProperties.sTranslatedEntity);
			}
		}

		this.data = new LinkedHashMap<>();

		this.data.put("version", eo.getVersion());

		createObject();
		addStates();
		addGenerations();
		addCommands();

		Object tempId = eo.getFieldValue(TEMPORARY_ID_FIELD);
		if (tempId != null) {
			data.put(TEMPORARY_ID_JSON_KEY, tempId);
		}
	}

	/**
	 * Adds all commands which are to be sent to the client to this value object.
	 */
	private void addCommands() {
		if (jsonConfig.skipStatesAndGenerations) {
			// NUCLOS-6317 1)
			return;
		}
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		request.getAttribute("commands");

		CommandsRVO cmds = new CommandsRVO(new ArrayList<>());

		final ArrayList<Serializable> commands = (ArrayList<Serializable>) request.getAttribute("CMD");
		if (commands != null) {
			for (Serializable object : commands) {
				addCommand(cmds, object);
			}
		}

		this.data.put("commands", cmds.toJsonBuilderObject());
	}

	/**
	 * Creates a new RVO for the given command object and adds it to the commands RVO.
	 * <p>
	 * TODO: Get rid of ugly if-else instanceof switches!
	 */
	private void addCommand(final CommandsRVO cmds, final Serializable cmd) {
		if (cmd instanceof ApiDirectionImpl) {
			final ApiDirectionImpl apiDirection = (ApiDirectionImpl) cmd;
			final Direction<Long> direction = apiDirection.getDirection();

			if (direction.getAdditionalNavigation() != null) {
				final DirectionRVO directionRVO = new DirectionRVO(null, null, direction.isNewTab(), direction.getAdditionalNavigation());
				cmds.getList().add(directionRVO);
			} else {
				final Collection<EntityMeta<?>> entities = Rest.getAllEntities();
				for (EntityMeta<?> entityMeta : entities) {
					String boMetaId = Rest.translateUid(E.ENTITY, entityMeta.getUID());

					if (direction.getEntityUid() != null && entityMeta.getUID() != null && direction.getEntityUid().equals(entityMeta.getUID())) {
						final DirectionRVO directionRVO = new DirectionRVO(direction.getId(), boMetaId, direction.isNewTab(), null);
						cmds.getList().add(directionRVO);
					}
				}
			}
		} else if (cmd instanceof ApiMessageImpl) {
			final ApiMessageImpl apiMessage = (ApiMessageImpl) cmd;
			final Message message = apiMessage.getMessage();
			final MessageRVO messageRVO = new MessageRVO(message.getMessage(), message.getTitle());
			cmds.getList().add(messageRVO);
		} else if (cmd instanceof ApiCommandImpl) {
			Object command = ((ApiCommandImpl) cmd).getCommand();
			if (command instanceof CloseCommand) {
				cmds.getList().add(new CloseCommandRVO());
			}
		}
	}

	private void addStates() {
		if (jsonConfig.skipStatesAndGenerations) {
			//NUCLOS-5417 h)
			return;
		}
		if (usageProperties.bHasStateModel && eo.canStateChange()) {
			List<StateVO> states = usageProperties.getStatesSorted();

			if (!states.isEmpty()) {
				JsonArrayBuilder array = Json.createArrayBuilder();

				for (StateVO svo : states) {
					StateRVO srvo = new StateRVO(svo, usageProperties.sTranslatedEntity, StringUtils.defaultIfNull(eo.getPKStringRepresentation(),
							eo.getFieldValue(TEMPORARY_ID_FIELD, String.class)), usageProperties.usage.getStatusUID());
					array.add(JsonFactory.buildJsonObject(srvo, usageProperties.webContext));
				}

				if (arrays == null) {
					arrays = new HashMap<>();
				}

				arrays.put("nextStates", array);
			}
		}
	}

	private void addGenerations() {
		if (jsonConfig.skipStatesAndGenerations) {
			//NUCLOS-6317 1)
			return;
		}
		List<GeneratorActionRVO> generations = usageProperties.getGenerationsSorted();

		if (!generations.isEmpty()) {
			JsonArrayBuilder array = Json.createArrayBuilder();

			for (GeneratorActionRVO generation : generations) {
				array.add(
						generation.getJsonObjectBuilder(
								eo.getPKStringRepresentation(),
								eo.getVersion(),
								usageProperties.webContext
						)
				);
			}

			if (arrays == null) {
				arrays = new HashMap<>();
			}

			arrays.put("generations", array);
		}
	}

	private void addAttributeValue(FieldMeta<?> fm) {
		UID field = fm.getUID();
		Object obj = eo.getFieldValue(field);
		Object pk = getPKForJson(eo.getPrimaryKey());

		boolean isReference = fm.getForeignEntity() != null;
		if (isReference && SF.STATENUMBER.checkField(fm.getFieldName())) {
			isReference = false;
		}

		// i18n basic support
		if (eo.getDataLanguageMap() != null && sessionDataLanguageUID != null && eo.getDataLanguageMap().getDataLanguage(sessionDataLanguageUID) != null) {
			final Object i18nValue;
			if (isReference) {
				i18nValue = eo.getFieldValue(DataLanguageUtils.extractForeignEntityReference(fm.getForeignEntity()));
			} else {
				i18nValue = eo.getDataLanguageMap().getDataLanguage(sessionDataLanguageUID).getValue(DataLanguageUtils.extractFieldUID(field));
			}
			if (i18nValue != null) {
				obj = i18nValue;
			}
		}

		if (obj instanceof NuclosImage) {
			return;
		}

		if (obj instanceof Double) {
			String sDouble = String.valueOf(obj);
			obj = new BigDecimal(sDouble).setScale(fm.getPrecision(), RoundingMode.HALF_UP);
		}

		String jsonPropertyName = getJsonPropertyName(fm);

		if (!isReference) {
			if (obj == null) {
				if (pk == null) {
					// bo with defaults only
					if (SF.STATENUMBER.checkField(fm.getFieldName()) ||
							SF.STATE.checkField(fm.getFieldName()) ||
							SF.STATEICON.checkField(fm.getFieldName()) ||
							SF.CREATEDAT.checkField(fm.getFieldName()) ||
							SF.CREATEDBY.checkField(fm.getFieldName()) ||
							SF.CHANGEDBY.checkField(fm.getFieldName()) ||
							SF.VERSION.checkField(fm.getFieldName()) ||
							SF.CHANGEDAT.checkField(fm.getFieldName())) {
						return;
					}
				} else {
					//TODO: Evaluate whether to put the NULL-Value or skip this field completely
					return;
					//obj = JsonValue.NULL;
				}
			}

			if (obj instanceof InternalTimestamp) {
				// TODO: Use ISO-8601 format
				String stringValue = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj);
				values.put(jsonPropertyName, stringValue);
			} else if (obj instanceof Date) {
				String stringValue = new SimpleDateFormat("yyyy-MM-dd").format(obj);
				values.put(jsonPropertyName, stringValue);
			} else if (obj instanceof NuclosPassword) {
				values.put(jsonPropertyName, ((NuclosPassword) obj).getValue());
			} else {
				values.put(jsonPropertyName, obj);
			}


		} else {
			Object fieldid = eo.getFieldId(field);
			JsonObjectBuilder json = Json.createObjectBuilder();

			if (obj instanceof File) {
				final String filetype = ((File) obj).getFiletype();
				if (!filetype.equals(File.TYPE_UNKNOWN)) {
					json.add("fileType", filetype);
				}
			}

			if (fieldid instanceof Long) {
				json.add("id", (Long) fieldid);
				json.add("name", obj != null ? obj.toString() : "");
				obj = json;

			} else {
				fieldid = eo.getFieldUid(field);
				if (fieldid == null) {
					json.add("id", JsonValue.NULL);
					json.add("name", JsonValue.NULL);
					obj = json;
				}
				if (fieldid instanceof UID) {
					EntityMeta<?> translationEntity = MetaProvider.getInstance().getEntity(fm.getForeignEntity());
					json.add("id", Rest.translateUid(translationEntity, (UID) fieldid));
					json.add("name", obj != null ? obj.toString() : "");
					obj = json;

				}
			}

			values.put(jsonPropertyName, obj);
		}
	}

	private void addAttributeValues(List<FieldMeta<?>> fields) {
		this.values = new LinkedHashMap<>();

		for (FieldMeta<?> fm : fields) {
			addAttributeValue(fm);
		}
	}

	private void createObject() {

		AttributesAndRestrictions attrsAndRestrictions = usageProperties.getAttributesAndRestrictions(jsonConfig.getAttributes());
		if (jsonConfig.noRestrictions) {
			this.values = new LinkedHashMap<>();
			Map<UID, Object> mapFields = eo.getFieldValues();
			for (UID fieldUid : mapFields.keySet()) {
				try {
					FieldMeta<?> fm = Rest.getEntityField(fieldUid);
					addAttributeValue(fm);
				} catch (CommonFatalException cfe) {
					//Happens typically with system fields like "version"
					LOG.debug(cfe.getMessage(), cfe);
				}
			}
		} else {
			addAttributeValues(attrsAndRestrictions.lstAttributesReadAllowed);
		}
		addRowColorIfThere(attrsAndRestrictions);
		addImages(attrsAndRestrictions);

		if (arrays == null) {
			arrays = new HashMap<>();
		}

		if (jsonConfig.withTitleAndInfo) {
			String title = Rest.getTreeViewLabel(eo.getFieldValues(), eo.getDalEntity());
			String info = Rest.getTreeViewDescription(eo.getFieldValues(), eo.getDalEntity());

			if (title.equals(info)) {
				info = "";
			}
			data.put("title", title);
			data.put("info", info);
		}

		for (FieldMeta<?> fm : attrsAndRestrictions.lstAttributesReadAllowed) {
			Object obj = eo.getFieldValue(fm.getUID());
			if (obj == null) {
				continue;
			}

			if (obj instanceof NuclosImage) {
				if (imageAttributes == null) {
					imageAttributes = new HashSet<>(1);
				}
				imageAttributes.add(Rest.translateUid(E.ENTITYFIELD, fm.getUID()));
				continue;
			}

			if (obj instanceof GenericObjectDocumentFile) {
				if (documentAttributes == null) {
					documentAttributes = new HashSet<>(1);
				}
				documentAttributes.add(Rest.translateUid(E.ENTITYFIELD, fm.getUID()));
			}
		}
	}

	private String getJsonPropertyName(FieldMeta<?> fm) {
		return NuclosBusinessObjectBuilder.getFieldNameForFqn(fm);
	}

	private void addRowColorIfThere(AttributesAndRestrictions attrsAndRestrictions) {
		if (attrsAndRestrictions != null && attrsAndRestrictions.attributeRowColor != null) {
			Object o = eo.getFieldValue(attrsAndRestrictions.attributeRowColor.getUID());
			if (o instanceof String) {
				data.put("rowcolor", o);
			}
		}
	}

	private void addImages(AttributesAndRestrictions attrsAndRestrictions) {
		for (FieldMeta<?> fm : attrsAndRestrictions.lstAttributesReadAllowed) {
			UID field = fm.getUID();
			Object obj = eo.getFieldValue(field);
			if (obj instanceof NuclosImage) {
				if (imageAttributes == null) {
					imageAttributes = new HashSet<>(1);
				}
				imageAttributes.add(Rest.translateUid(E.ENTITYFIELD, fm.getUID()));
			}
		}
	}

	/**
	 * Some types of BO are not 'detailable' like dynamic or chart BOs.
	 * The ID is not unique and therefore a precise select is not possible.
	 */
	private boolean isDetailableBo() {
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(usageProperties.usage.getEntityUID());
		return !eMeta.isDynamic() && !eMeta.isChart();
	}

	@Override
	public JsonObjectBuilder getJSONObjectBuilder() {
		NuclosJsonObjectBuilder json = new NuclosJsonObjectBuilder();

		Object pk = getPKForJson(eo.getPrimaryKey());

		json.add("boId", pk);
		json.add("boMetaId", usageProperties.sTranslatedEntity);
		//canStateChange not needed: Links are only generated if allowed
		//json.add("canStateChange", eo.canStateChange());

		boolean canWrite = usageProperties.isUpdateAllowed && eo.canWrite();
		boolean canDelete = usageProperties.isDeleteAllowed && eo.canDelete();

		this.boLinksFactory.clear();
		if (pk == null) {
			if (usageProperties.isInsertAllowed) {
				boLinksFactory.addInsert();
			}
		} else {
			if (jsonConfig.isWithDetailLink() && isDetailableBo()) {
				RestLink link = boLinksFactory.addSelf(pk);
				if (link != null && !link.isProtectVerbs()) {
					if (canWrite && usageProperties.isWriteAllowed()) {
						link.setVerbPut();
					} else {
						canWrite = false;
					}
					if (canDelete && usageProperties.isDeleteAllowed()) {
						link.setVerbDelete();
					} else {
						canDelete = false;
					}
				}
			}
		}

		json.add("canWrite", canWrite);
		json.add("canDelete", canDelete);

		if (jsonConfig.withMetaLink || pk == null) {
			boLinksFactory.addMeta();
		}

		for (String key : data.keySet()) {
			json.add(key, data.get(key));
		}

		if (values != null) {
			NuclosJsonObjectBuilder jsonValues = new NuclosJsonObjectBuilder();
			for (String key : values.keySet()) {
				Object value = values.get(key);
				if (value instanceof Date) {
					String stringValue = Rest.getDateFormat().format(value);
					jsonValues.add(key, stringValue);

				} else if (value instanceof GenericObjectDocumentFile) {
					GenericObjectDocumentFile godf = (GenericObjectDocumentFile) value;
					JsonObjectBuilder jsonb = Json.createObjectBuilder();
					jsonb.add("name", godf.getFilename());
					jsonValues.add(key, jsonb);

				} else {
					jsonValues.add(key, value);
				}
			}
			json.add("attributes", jsonValues);

			AttributesAndRestrictions fieldsAndRestrictions = usageProperties.getAttributesAndRestrictions(jsonConfig.getAttributes());
			if (fieldsAndRestrictions.restrictions != null) {
				json.add("attrRestrictions", fieldsAndRestrictions.restrictions);
			}
		}

		if (imageAttributes != null) {
			JsonObjectBuilder images = Json.createObjectBuilder();
			for (String imageAttrId : imageAttributes) {
				FieldMeta<?> fm = MetaProvider.getInstance().getEntityField(Rest.translateFqn(E.ENTITYFIELD, imageAttrId));
				String rel = getJsonPropertyName(fm);
				boLinksFactory.addImage(rel, imageAttrId, pk, eo.getVersion());
			}

			boLinksFactory.buildImageJSON(images, usageProperties.webContext);
			json.add("attrImages", images);
		}

		if (documentAttributes != null) {
			JsonObjectBuilder documents = Json.createObjectBuilder();
			for (String docAttrId : documentAttributes) {
				FieldMeta<?> fm = MetaProvider.getInstance().getEntityField(Rest.translateFqn(E.ENTITYFIELD, docAttrId));
				String rel = getJsonPropertyName(fm);
				boLinksFactory.addDocument(rel, docAttrId, pk);
			}

			boLinksFactory.buildDocumetJSON(documents, usageProperties.webContext);
			json.add("attrDocuments", documents);
		}

		if (pk != null) {
			if (Rest.facade().hasPrintouts(usageProperties.usage, pk)) {
				boLinksFactory.addPrintout(usageProperties.sTranslatedEntity, pk);
			}
		}

		if ((jsonConfig.withLayoutLink || pk == null) && usageProperties.webLayout != null) {

			if (usageProperties.webLayout.getLayoutInfo().getLayoutUID() != null) {
				boLinksFactory.addLayout(Rest.translateUid(E.LAYOUT, usageProperties.webLayout.getLayoutInfo().getLayoutUID()));
				boLinksFactory.addSubforminfo(Rest.translateUid(E.LAYOUT, usageProperties.webLayout.getLayoutInfo().getLayoutUID()), pk);
			}

			Boolean bWriteAllowedForPk = null;
			Object pkForSubs = pk;
			if (pkForSubs == null) {
				pkForSubs = data.get(TEMPORARY_ID_JSON_KEY);
				bWriteAllowedForPk = Boolean.TRUE;
			}

			JsonObjectBuilder subBos = usageProperties.getSubBos(pkForSubs, bWriteAllowedForPk);
			if (subBos != null) {
				json.add("subBos", subBos);
			}

		}

		if (usageProperties.usage.getStatusUID() != null) {
			StateVO state = StateCache.getInstance().getState(usageProperties.usage.getStatusUID());
			NuclosImage image = state.getIcon();
			if (image != null) {
				boLinksFactory.addStateIcon(Rest.translateUid(E.STATE, usageProperties.usage.getStatusUID()));
			}

			if (state.getTabbedPaneName() != null) {
				json.add("defaultTabbedPaneName", state.getTabbedPaneName());
			}
		}

		if (arrays != null) {
			for (String key : arrays.keySet()) {
				json.add(key, arrays.get(key));
			}
		}

		if (jsonConfig.withLock && pk != null) {
			if (eo.getFieldUid(SF.OWNER_UID) != null) {
				if (Rest.facade().isUnlockAllowed(usageProperties.webContext.getBOMeta().getUID())) {
					boLinksFactory.addUnlock(pk);
				}
			}
		}

		if (jsonConfig.isWithDetailLink()) {
			String detailBoMetaId = Rest.translateUid(E.ENTITY, usageProperties.webContext.getBOMeta().getUID());
			boLinksFactory.addDetail(detailBoMetaId, pk);
		}

		boLinksFactory.buildJSON(json, usageProperties.webContext);

		return json;
	}


	//Note: UsageProperties only contains Properties that are dependent of the Usage Criteria, not of the Data-Row itself, or the primary key.
	public static class UsageProperties {

		private final UsageCriteria usage;
		private final IWebContext webContext;
		private final String sTranslatedEntity;

		private final List<StateVO> lstStates;
		private final IWebLayout webLayout;

		private final boolean bHasStateModel;
		private final boolean isInsertAllowed;
		private final boolean isUpdateAllowed;
		private final boolean isDeleteAllowed;

		public UsageProperties(UsageCriteria usage, IWebContext webContext) {
			this.usage = usage;
			this.webContext = webContext;
			this.sTranslatedEntity = Rest.translateUid(E.ENTITY, usage.getEntityUID());

			this.webLayout = webContext.getWebLayout(usage);

			this.bHasStateModel = Rest.hasStateModel(usage.getEntityUID());

			EntityPermission entityPermission = Rest.getEntityPermission(usage.getEntityUID());
			this.isInsertAllowed = entityPermission.isInsertAllowed() && webLayout != null && webLayout.getLayoutInfo().getLayoutUID() != null;
			this.isDeleteAllowed = entityPermission.isDeleteAllowed();
			this.isUpdateAllowed = entityPermission.isUpdateAllowed();

			if (bHasStateModel) {
				this.lstStates = Rest.getSubsequentStates(usage);
			} else {
				this.lstStates = new ArrayList<>();
			}
		}

		private final Map<String, AttributesAndRestrictions> mpAttributesAndRestrictions = new HashMap<>();

		AttributesAndRestrictions getAttributesAndRestrictions(String queryAttributes) {
			if (!mpAttributesAndRestrictions.containsKey(queryAttributes)) {
				Collection<FieldMeta<?>> fields = Rest.getEntity(usage.getEntityUID()).getFields();

				List<FieldMeta<?>> lstAttributesReadAllowed = new ArrayList<>();

				for (FieldMeta<?> fm : fields) {
					// NUCLOS-5242 Check all fields for permission (any but nuclosRowColor)
					if (Rest.isReadAllowedForField(fm, usage) || fm.isNuclosRowColor()) {
						lstAttributesReadAllowed.add(fm);
					}
				}

				JsonObjectBuilder restrictions = Json.createObjectBuilder();
				boolean hasRestrictions = false;

				for (FieldMeta<?> fm : fields) {
					String jsonPropertyName = NuclosBusinessObjectBuilder.getFieldNameForFqn(fm);
					String srestrict = null;

					if (!lstAttributesReadAllowed.contains(fm)) {
						srestrict = "hidden";

					} else {
						boolean bWrite = isUpdateAllowed && !fm.isReadonly() && Rest.isWriteAllowedForField(fm, usage) && fm.isModifiable();

						if (!bWrite) {
							if (SF.STATE.checkField(jsonPropertyName)) {
								if (lstStates.isEmpty()) {
									srestrict = "readonly";
								}

							} else {
								srestrict = "readonly";
							}
						}

					}

					if (srestrict != null) {
						restrictions.add(jsonPropertyName, srestrict);
						hasRestrictions = true;
					}

				}

				if (bHasStateModel) {
					List<WebStaticComponent> lstButtons = webLayout.getEnabledButtons();

					for (WebStaticComponent button : lstButtons) {

						if (!Rest.isButtonEnabled(button, usage, webContext.getUser())) {
							restrictions.add(Rest.translateUid(E.ACTION, button.getUID()), "disabled");
						}
					}
				}

				mpAttributesAndRestrictions.put(queryAttributes,
						new AttributesAndRestrictions(lstAttributesReadAllowed, hasRestrictions ? restrictions.build() : null));
			}

			return mpAttributesAndRestrictions.get(queryAttributes);
		}

		private List<StateVO> lstStatesSorted;

		private List<StateVO> getStatesSorted() {
			if (bHasStateModel) {

				if (lstStatesSorted != null) {
					return lstStatesSorted;
				}

				lstStatesSorted = lstStates;

				lstStatesSorted.sort((o1, o2) -> o1.getNumeral().compareTo(o2.getNumeral()));
			}

			return lstStatesSorted;
		}

		private List<GeneratorActionRVO> lstGenerationsSorted;

		private List<GeneratorActionRVO> getGenerationsSorted() {
			if (lstGenerationsSorted != null) {
				return lstGenerationsSorted;
			}

			lstGenerationsSorted = new ArrayList<>();

			List<GeneratorActionVO> lstGenerations = Rest.facade().getGenerations(usage);

			if (lstGenerations != null) {

				lstGenerations.sort((o1, o2) -> StringUtils.compareIgnoreCase(StringUtils.defaultIfNull(o1.getLabel(), o1.getName()),
						StringUtils.defaultIfNull(o2.getLabel(), o2.getName())));

				for (GeneratorActionVO generation : lstGenerations) {
					lstGenerationsSorted.add(new GeneratorActionRVO(generation, sTranslatedEntity));
				}

			}

			return lstGenerationsSorted;
		}

		JsonObjectBuilder getSubBos(Object pk, Boolean bWriteAllowedForPk) {
			JsonObjectBuilder subBos = Json.createObjectBuilder();
			boolean hasSubBos = false;

			Map<UID, AbstractWebDependents> visibleSubBos = webLayout.getVisibleSubLists();
			for (UID refkey : visibleSubBos.keySet()) {
				AbstractWebDependents webSub = visibleSubBos.get(refkey);

				UID subform = Rest.getEntityField(refkey).getEntity();
				String sRefKey = Rest.translateUid(E.ENTITYFIELD, refkey);

				if (Rest.isReadAllowedForSubform(subform, usage)) {
					hasSubBos = true;
					if (bWriteAllowedForPk == null) {
						bWriteAllowedForPk = isWriteAllowed();
					}
					JsonObjectBuilder subBo = getSubBo(sRefKey, sTranslatedEntity, webSub.getServiceIdentifier(), pk, webContext, subform, usage, bWriteAllowedForPk);
					subBos.add(sRefKey, subBo);
				}
			}

			MetaProvider metaProvider = MetaProvider.getInstance();
			for (FieldMeta<?> subRefMeta : metaProvider.getAllReferencingFields(usage.getEntityUID())) {
				EntityMeta<Object> subMeta = metaProvider.getEntity(subRefMeta.getEntity());
				if (subMeta.isChart()) {
					// chart datasources are not defined in layout
					EntityMeta<?> parentMeta = metaProvider.getEntity(usage.getEntityUID());
					boolean add = false;
					if (parentMeta.isStateModel()) {
						UID mandator = Rest.facade().getCurrentMandatorUID();
						Map<UID, SubformPermission> subPermissions = SecurityCache.getInstance().getSubForm(webContext.getUser(), subMeta.getUID(), mandator);
						SubformPermission subPermission = subPermissions.get(usage.getStatusUID());
						if (subPermission != null && subPermission.includesReading()) {
							add = true;
						}
					} else {
						add = true;
					}
					if (add) {
						hasSubBos = true;
						String sRefKey = Rest.translateUid(E.ENTITYFIELD, subRefMeta.getUID());
						if (bWriteAllowedForPk == null) {
							bWriteAllowedForPk = isWriteAllowed();
						}
						JsonObjectBuilder subBo = getSubBo(sRefKey, sTranslatedEntity, WebSubform.SERVICE_IDENTIFIER, pk, webContext, subMeta.getUID(), usage, bWriteAllowedForPk);
						subBos.add(sRefKey, subBo);
					}
				}
			}

			return hasSubBos ? subBos : null;
		}

		boolean isWriteAllowed() {
			if (!isUpdateAllowed) {
				return false;
			}

			if (!bHasStateModel) {
				return true;
			}

			return Rest.facade().isWriteAllowedForGO(usage.getEntityUID());
		}

		boolean isDeleteAllowed() {
			if (!isDeleteAllowed) {
				return false;
			}

			if (!bHasStateModel) {
				return true;
			}

			return Rest.facade().isDeleteAllowedForGO(usage.getEntityUID(), true);
		}

	}

	private static JsonObjectBuilder getSubBo(String sRefKey, String sTranslatedEntity, String serviceIdentifier,
											  Object pk, IWebContext webContext, UID subform, UsageCriteria usage, boolean isWriteAllowed) {
		JsonObjectBuilder subBo = Json.createObjectBuilder();

		RestLinks subBoLinks = new RestLinks(subBo);
		subBoLinks.addLinkHref("boMeta", "referencemeta_self", Verbs.GET, sTranslatedEntity, sRefKey);
		if (pk != null) {
			subBoLinks.addLinkHref("bos", serviceIdentifier, Verbs.GET, sTranslatedEntity, pk, sRefKey);
		}
		subBoLinks.buildJson(webContext);

		boolean bWrite = isWriteAllowed && Rest.isWriteAllowedForSubform(subform, usage);
		if (!bWrite) {
			subBo.add("restriction", "readonly");
		}
		if (usage.getStatusUID() != null) {

			// NUCLOS-6134
			SubformPermission subformPermission = Rest.facade().getSubformPermission(subform, usage);
			if (subformPermission != null) {

				if (bWrite) {
					boolean noCreate = !subformPermission.canCreate();
					boolean noDelete = !subformPermission.canDelete();

					if (noCreate && noDelete) {
						subBo.add("restriction", "nocreate,nodelete");
					} else if (noCreate) {
						subBo.add("restriction", "nocreate");
					} else if (noDelete) {
						subBo.add("restriction", "nodelete");
					}
				}

				Collection<UID> readWriteAllowedGroups = subformPermission.getReadWriteAllowedGroups();

				if (readWriteAllowedGroups != null) {
					Collection<UID> attrNotWrite = MetaProvider.getInstance().getAllFieldsByEntityFilteredByGroups(subform, readWriteAllowedGroups, false, false);
					Collection<UID> attrNotRO = Collections.emptySet();

					Collection<UID> readOnlyAllowedGroups = subformPermission.getReadOnlyAllowedGroups();
					if (readOnlyAllowedGroups != null) {
						attrNotRO = MetaProvider.getInstance().getAllFieldsByEntityFilteredByGroups(subform, readOnlyAllowedGroups, false, false);
					}

					if (!attrNotWrite.isEmpty()) {
						JsonObjectBuilder job = Json.createObjectBuilder();
						for (UID attr : attrNotWrite) {
							String restr = attrNotRO.contains(attr) ? "hidden" : "ro";
							job.add(Rest.translateUid(E.ENTITYFIELD, attr), restr);
						}
						subBo.add("readonlyattributes", job);
					}
				}

			}

		}

		return subBo;
	}

	static class AttributesAndRestrictions {
		private final List<FieldMeta<?>> lstAttributesReadAllowed;
		private final JsonObject restrictions;
		private final FieldMeta<?> attributeRowColor;

		AttributesAndRestrictions(List<FieldMeta<?>> lstAttributesReadAllowed, JsonObject jsonObject) {
			this.lstAttributesReadAllowed = lstAttributesReadAllowed;
			this.restrictions = jsonObject;
			FieldMeta<?> arc = null;
			for (FieldMeta<?> fm : lstAttributesReadAllowed) {
				if (fm.isNuclosRowColor()) {
					arc = fm;
					break;
				}
			}
			this.attributeRowColor = arc;

			//Note: NUCLOS-4099 requests that the NuclosRowColor is not displayed in the web-client.
			//TODO: Evaluate if this (within the REST-Service) is the right place to filter this attribute out of the data
			if (arc != null) {
				this.lstAttributesReadAllowed.remove(arc);
			}
		}
	}

	public interface BoLinksFactory {

		void buildJSON(JsonObjectBuilder boJson, IWebContext context);

		void buildImageJSON(JsonObjectBuilder imgJson, IWebContext context);

		void buildDocumetJSON(JsonObjectBuilder docJson, IWebContext context);

		RestLink addDocument(String rel, String docAttrId, Object boId);

		RestLink addImage(String rel, String imgAttrId, Object boId, int version);

		RestLink addStateIcon(String stateId);

		RestLink addLayout(String layoutId);

		RestLink addSubforminfo(String layoutId, Object pk);

		RestLink addPrintout(String sTranslatedEntity, Object pk);

		RestLink addUnlock(Object pk);

		RestLink addSelf(Object selfId);

		RestLink addDetail(String detailBoMetaId, Object selfId);

		RestLink addInsert();

		RestLink addMeta();

		void clear();
	}

	public static abstract class AbstractBoLinksFactory implements BoLinksFactory {

		protected final RestLinks links = new RestLinks();
		protected final RestLinks imgLinks = new RestLinks();
		protected final RestLinks docLinks = new RestLinks();

		@Override
		public void buildJSON(JsonObjectBuilder boJson, IWebContext context) {
			links.setParent(boJson);
			links.buildJson(context);
		}

		@Override
		public void buildImageJSON(JsonObjectBuilder imgJson, IWebContext context) {
			imgLinks.setParent(imgJson);
			imgLinks.buildJson(context);
		}

		@Override
		public void buildDocumetJSON(JsonObjectBuilder docJson, IWebContext context) {
			docLinks.setParent(docJson);
			docLinks.buildJson(context);
		}

		@Override
		public void clear() {
			links.clear();
			imgLinks.clear();
			docLinks.clear();
		}
	}

	private static class DefaultBoLinksFactory extends AbstractBoLinksFactory {

		private final String boMetaId;

		DefaultBoLinksFactory(String boMetaId) {
			this.boMetaId = boMetaId;
		}

		@Override
		public RestLink addSelf(Object selfId) {
			return links.addLinkHref("self", "bo", Verbs.GET, boMetaId, selfId);
		}

		@Override
		public RestLink addDetail(final String detailBoMetaId, final Object selfId) {
			return null;
		}

		@Override
		public RestLink addMeta() {
			return links.addLink("boMeta", Verbs.GET, boMetaId);
		}

		@Override
		public RestLink addLayout(String layoutId) {
			return links.addLinkHref("layout", "weblayoutCalculated", Verbs.GET, layoutId);
		}

		@Override
		public RestLink addSubforminfo(String layoutId, Object pk) {
			return links.addLink("subforminfo", Verbs.GET, layoutId, pk != null ? pk.toString() : null);
		}

		@Override
		public RestLink addStateIcon(String stateId) {
			return links.addLink("stateIcon", Verbs.GET, stateId);
		}

		@Override
		public RestLink addImage(String rel, String imgAttrId, Object boId, int version) {
			return imgLinks.addLinkHref(rel, "boImageValue", Verbs.GET, boMetaId, boId, imgAttrId, version);
		}

		@Override
		public RestLink addDocument(String rel, String docAttrId, Object boId) {
			return docLinks.addLinkHref(rel, "boDocumentValue", Verbs.GET, boMetaId, boId, docAttrId);
		}

		@Override
		public RestLink addPrintout(String sTranslatedEntity, Object pk) {
			return links.addLinkHref("printouts", "boPrintoutList", Verbs.GET, sTranslatedEntity, pk);
		}

		@Override
		public RestLink addInsert() {
			return links.addLinkHref("insert", "bos", Verbs.POST, boMetaId);
		}

		@Override
		public RestLink addUnlock(Object boId) {
			return links.addLinkHref("lock", "bos/{boMetaId}/{boId}/lock", Verbs.DELETE, boMetaId, boId);
		}

	}
}
