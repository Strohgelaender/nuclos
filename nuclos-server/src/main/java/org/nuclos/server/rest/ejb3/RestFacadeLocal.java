package org.nuclos.server.rest.ejb3;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.ws.rs.core.Response;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common2.BoDataSet;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.i18n.language.data.DataLanguageVO;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.rest.services.rvo.LayoutInfo;
import org.nuclos.server.rest.services.rvo.NucletInfo;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.core.io.Resource;

public interface RestFacadeLocal {

	SessionContext webLogin(String username, String password, Locale locale, final String datalanguage, String jSessionId);

	Response.Status changePassword(String username, String password, String newPassword, String jSessionId);

	SessionContext getSessionContext(String webSessionID);

	SessionContext validateSessionAuthenticationAndInitUserContext(String webSessionID);

	void webLogout(String webSessionID);

	EntitySearchFilter2 getSearchFilterByPk(UID pk) throws CommonBusinessException;

	List<SearchFilterVO> getSearchFilters() throws CommonBusinessException;

	Resource getResource(UID uid) throws IOException;

	EntityPermission getEntityPermission(UID entity);

	Collection<FieldMeta<?>> getAllFieldsByEntity(UID entity, boolean onlyReferenceFields);

	boolean isSuperUser();

	boolean isReadAllowedForField(FieldMeta<?> fm, UsageCriteria usage);

	boolean isWriteAllowedForField(FieldMeta<?> fm, UsageCriteria usage);

	boolean isReadAllowedForSubform(UID subform, UsageCriteria usage);

	boolean isWriteAllowedForSubform(UID subform, UsageCriteria usage);

	SubformPermission getSubformPermission(UID subform, UsageCriteria usage);

	boolean isWriteAllowedForGO(UID entity);

	boolean isDeleteAllowedForGO(UID entity, boolean bPhysically);

	Long countEntityObjectRows(UID entity, CollectableSearchExpression clctexpr) throws CommonPermissionException;

	<PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunk(UID entity, CollectableSearchExpression clctexpr, ResultParams resultParams)
			throws CommonPermissionException;

	StateVO getState(UID stateUid);

	UID getInitialStateId(UID entity);

	<PK> EntityObjectVO<PK> get(UID entity, PK pk, boolean remotely) throws CommonPermissionException;

	<PK> EntityObjectVO<PK> getThin(UID entity, PK pk, boolean remotely) throws CommonPermissionException;

	<PK> EntityObjectVO<PK> getDataForField(PK pk, FieldMeta<?> fm);

	List<CollectableField> getReferenceList(UID field, String search, Long lMaxRowCount, WebValueListProvider vlp, String pk, UID mandatorUID) throws CommonBusinessException;

	List<CollectableField> getVLPData(UID field, WebValueListProvider vlp, String pk, String quickSearchInput, Integer maxRowCount, boolean bDefault, UID mandatorUID) throws CommonBusinessException;

	List<DatasourceParameterVO> getParameterListForDataSource(UID entity) throws NuclosDatasourceException;

	<PK> EntityObjectVO<PK> insert(EntityObjectVO<PK> eoVO) throws CommonBusinessException;

	<PK> EntityObjectVO<PK> update(EntityObjectVO<PK> eoVO) throws CommonBusinessException;

	<PK> void delete(UID entity, PK pk, boolean logicalDeletion) throws CommonBusinessException;

	Collection<StateVO> getSubsequentStates(UsageCriteria usagecriteria) throws CommonBusinessException;

	void changeState(UID entity, Long pk, UID newState) throws CommonBusinessException;

	<PK> Collection<EntityObjectVO<PK>> getSubformData(UID subform, UID field, PK relatedPK, CollectableSearchExpression filterExpr, ResultParams resultParams);

	<PK> Collection<EntityObjectVO<PK>> getDependentData(UID subform, UID field, PK relatedPK);

	List<LuceneSearchResult> luceneIndexSearch(String searchString);

	String getFieldGroupName(UID groupID);

	String getDbVersion();

	boolean isActionAllowed(String action);

	<PK> EntityObjectVO<PK> fireCustomEventSupport(EntityObjectVO<PK> eoVO, String rule) throws CommonBusinessException;

	String getCustomUsage();

	WorkspaceVO getDefaultWorkspace() throws CommonBusinessException;

	String getLayoutML(UID pk) throws CommonBusinessException;

	LayoutInfo getLayoutInfo(UID layoutUID) throws CommonPermissionException;

	List<LayoutInfo> getLayoutInfosForEntity(UID entityUID);

	boolean isLayoutRestrictionsMustIgnoreGroovyRules(UID pk);

	Collection<UID> getEntitiesAssignedToLayoutId(UID layoutUID) throws CommonBusinessException;

	UID getDetailLayoutIDForUsage(UsageCriteria usage, boolean bSearchLayout);

	<PK> UsageCriteria getUsageCriteriaForPK(PK pk, UID entity) throws CommonBusinessException;

	BoDataSet importOrExportDatabase(InputStream is) throws CommonPermissionException;

	JsonArrayBuilder getTree(String boMetaId, String boId);

	JsonArrayBuilder getSubtree(String nodeId);

	JsonArrayBuilder getTreeWithSearch(String boMetaId, String boId, String search) throws CommonBusinessException;

	String getParamRestMenuEntities();

	String getInitialEntityEntry();

	String translateUid(EntityMeta<?> eMeta, UID uid);

	UID translateFqn(EntityMeta<?> eMeta, String fqn);

	boolean hasPrintouts(UsageCriteria usagecriteria, Object boId);

	String getNucletParameterByName(UID nucletUID, String parameter);

	<PK> EntityObjectVO<PK> getBoWithDefaultValues(UID entityUID);

	BoGenerationResult generateBo(
			UID entityUID,
			Map<Long, Integer> eoIdsAndVersions,
			UID generatorUID
	) throws CommonBusinessException;

	List<GeneratorActionVO> getGenerations(UsageCriteria usage);

	boolean doesLayoutChangeWithProcess(Collection<UID> entities);

	EntityObjectVO<Long> getTemporaryObject(String gpoId) throws CommonBusinessException;

	boolean isTemporaryId(String id);

	<T> List<CollectableField> getValueList(FieldMeta<T> field, List<String> sSearch, FieldMeta<Long> referencingField, Long boId);

	void updateInputContext(Map<String, Serializable> context);

	void clearInputContext();

	List<EventSupportSourceVO> findEventSupportsByUsageAndEntity(String sEventclass, UsageCriteria usagecriteria);

	boolean isUnlockAllowed(UID entity);

	<PK> void unlock(UID entity, PK pk) throws CommonPermissionException;

	void chooseMandator(UID mandatorUID) throws CommonPermissionException;

	UID getCurrentMandatorUID();

	Locale getCurrentLocale();

	String getCurrentUserName();

	ValuelistProviderVO getValuelistProvider(UID vlpUID) throws CommonFinderException, CommonPermissionException;

	Map<String, Object> getDatasourceParameters(String dsXML, Map<String, String> datasourceParams) throws CommonBusinessException, ClassNotFoundException;

	JsonArray executeDatasource(UID datasourceUID, Map<String, String> datasourceParams, Integer iMaxRowCount) throws CommonBusinessException, ClassNotFoundException;

	String executeManagementConsole(String sCommand, String sArguments) throws Exception;

	Collection<TasklistDefinition> getDynamicTaskLists();

	List<NucletInfo> getNucletInfos();

	Collection<DataLanguageVO> getDataLanguages();

	UID getPrimaryDataLanguage();

	UID getCurrentDataLanguage();

	boolean isDatasourceAllowed(UID datasourceUID);

	ResultListExportRVO.OutputFile exportBoList(
			String boMetaId,
			String format,
			boolean pageOrientationLandscape,
			boolean isColumnScaled, Collection<FieldMeta<?>> fields,
			CollectableSearchExpression clctexpr,
			FilterJ filter
	) throws NuclosReportException, IOException;

	boolean isRestApiAllowed(String username);
}
