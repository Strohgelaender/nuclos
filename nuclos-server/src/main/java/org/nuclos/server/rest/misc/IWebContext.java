package org.nuclos.server.rest.misc;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.server.rest.services.rvo.LayoutAdditions;

public interface IWebContext {

	enum NuclosMethod {GET, INSERT, UPDATE, DELETE}

	Set<String> getParameterKeys();

	String getFirstParameter(String key);

	<T> T getFirstParameter(String key, Class<T> _clazz);

	String getRestURI(String serviceid, Object... tags);

	EntityMeta<?> getBOMeta();

	FieldMeta<?> getRefFieldMeta();

	IWebLayout getWebLayout(UsageCriteria uc);

	IWebLayout getWebLayout(UsageCriteria uc, boolean search);

	String getUser();

	List<LayoutAdditions> getComponentsFromLayoutByContext(UsageCriteria usage, boolean onlyVisibleComponents);

	List<LayoutAdditions> getComponentsFromLayout(UsageCriteria baseUsage, UID subBoUID, boolean onlyVisibleComponents);

	Locale getLocale();

	UID getMandatorUID();

}
