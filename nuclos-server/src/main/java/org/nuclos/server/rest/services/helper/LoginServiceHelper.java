package org.nuclos.server.rest.services.helper;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.schema.rest.LegalDisclaimer;
import org.nuclos.schema.rest.ObjectFactory;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.rest.services.rvo.LoginJ;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.spring.NuclosWebApplicationInitializer;

public class LoginServiceHelper extends WebContext {
	
	protected JsonObjectBuilder getLoginInfo() {
		SessionContext context = getSessionContext();
		return JsonFactory.buildJsonObject(context, this);
	}
	
	protected LoginResult doLogin(JsonObject data, HttpServletRequest request) {
		try {
			LoginResult result = new LoginResult();

			String jSessionId = request.getSession().getId();
			LoginJ lrvo = new LoginJ(data);

			SessionContext sessionContext = login(lrvo, jSessionId);
			JsonObjectBuilder json = JsonFactory.buildJsonObject(sessionContext, this);
			result.setResponseJSON(json);

			NewCookie sessionCookie = getSessionCookie(lrvo, request, jSessionId);
			result.addCookie(sessionCookie);

			synchronizeSessionAndCookieTimeout(request, sessionCookie);

			return result;
			
		} catch (Exception e) {
			throw NuclosWebException.wrap(e);
		}
	}

	private void synchronizeSessionAndCookieTimeout(
			final HttpServletRequest request,
			final NewCookie sessionCookie
	) {
		int maxAge = sessionCookie.getMaxAge();
		if (maxAge <= 0) {
			// A max age of -1 means "ends with the browser session" for the cookie,
			// 0 means "ends immediately",
			// but for the HTTP session max age <= 0 means "no timeout at all".
			// Therefore we use the default session timeout here.
			maxAge = NuclosWebApplicationInitializer.DEFAULT_SESSION_TIMEOUT;
		}
		request.getSession().setMaxInactiveInterval(maxAge);
	}

	protected Response doChooseMandator(String mandatorId) {
		SessionContext session = getSessionContext();
		UID mandatorUID = UID.parseUID(mandatorId);
		try {
			Rest.facade().chooseMandator(mandatorUID);
			session.setMandatorUID(mandatorUID);
			return Response.status(Status.OK).build();
		} catch (CommonPermissionException e) {
			throw new NuclosWebException(Status.FORBIDDEN, e.getMessage());
		}
	}

	protected Response doLogout(HttpServletRequest request) {
		SessionContext context = getSessionContext();
		Rest.facade().webLogout(context.getJSessionId());

		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}

		return Response.ok().build();
	}

	protected String getVersion() {
		return ApplicationProperties.getInstance().getNuclosVersion().getSimpleVersionNumber();
	}

	protected String getDbVersion() {
		try {
			return Rest.facade().getDbVersion();
		} catch (Exception ex) {
			// TODO
			EntityMeta<?> TODO = null;
			throw new NuclosWebException(ex, Rest.translateFqn(TODO, "dbversion"));
		}
	}

	private NewCookie getSessionCookie(LoginJ lrvo, HttpServletRequest request, String jSessionId) {
		String path = request.getContextPath();
		if (!StringUtils.endsWith(path, "/")) {
			path = path +  "/";
		}
		NewCookie sessionCookie = new NewCookie(
				"JSESSIONID",				// name
				jSessionId,					// value
				path,						// path
				null,						// domain
				null,						// comment
				NewCookie.DEFAULT_MAX_AGE,	// max age in seconds
				request.isSecure(),			// HTTPS?
				true						// HttpOnly
		);

		// If Autologin is enabled, set an explicit expiration date, 
		// so the cookie does not expire at the end of the browser session
		if (lrvo.isAutologin()) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, 1);
			sessionCookie = new NewCookie(
					sessionCookie,
					sessionCookie.getComment(),
					60 * 60 * 24 * 365,
					cal.getTime(),
					sessionCookie.isSecure(),
					sessionCookie.isHttpOnly()
			);
		}

		return sessionCookie;
	}

	public static class LoginResult {
		private List<NewCookie> cookies;
		private JsonObjectBuilder responseJSON;

		public LoginResult() {
			cookies = new ArrayList<>();
		}

		public NewCookie[] getCookies() {
			return cookies.toArray(new NewCookie[]{});
		}
		public void addCookie(NewCookie cookie) {
			cookies.add(cookie);
		}
		public JsonObjectBuilder getResponseJSON() {
			return responseJSON;
		}
		public void setResponseJSON(JsonObjectBuilder responseJSON) {
			this.responseJSON = responseJSON;
		}
	}

	protected List<LegalDisclaimer> getLegalDisclaimers() {
		IEntityObjectProcessor<UID> proc = NucletDalProvider.getInstance().getEntityObjectProcessor(E.RESOURCE);
		CollectableSearchCondition csc = SearchConditionUtils.newLikeCondition(E.RESOURCE.name, "LEGAL_DISCLAIMER*");
		CollectableSorting sort = CollectableSorting.createSorting(E.RESOURCE.name.getUID(), true);

		List<EntityObjectVO<UID>> lstEO = proc.getBySearchExpression(new CollectableSearchExpression(csc, sort));

		List<LegalDisclaimer> ret = new ArrayList<>();

		for (EntityObjectVO<UID> eo : lstEO) {
			byte[] content = eo.getFieldValue(E.RESOURCE.content);

			if (content != null) {
				LegalDisclaimer disclaimer = new ObjectFactory().createLegalDisclaimer();
				disclaimer.setName(eo.getFieldValue(E.RESOURCE.description));
				try {
					disclaimer.setText(new String(content, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					LOG.warn("Disclaimer content is not UTF-8 encoded!", e);
					disclaimer.setText(new String(content));
				}
				ret.add(disclaimer);
			}
		}

		return ret;
	}
}
