//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.apache.logging.log4j.core.config.AbstractConfiguration;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common2.BoDataSet;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.dblayer.impl.DataSourceExecutor;
import org.nuclos.server.dbtransfer.TransferFacadeBean;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeBean;
import org.nuclos.server.job.ejb3.JobControlFacadeBean;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.WebContext;
import org.springframework.beans.factory.annotation.Autowired;


@Path("/maintenance")
public class MaintenanceRestService extends WebContext {

	private static final Logger LOG = LogManager.getLogger(MaintenanceRestService.class);

	@Autowired
	private MasterDataRestFqnCache masterDataRestFqnCache;

	@Autowired
	private JobControlFacadeBean jobControlFacadeBean;

	@Autowired
	private EventSupportFacadeBean eventSupportFacadeBean;

	@Autowired
	MasterDataFacadeLocal masterDataFacade;

	@GET
	@Path("/mode")
	public String mode() {
		MaintenanceFacadeBean maintenanceFacadeBean = SpringApplicationContextHolder.getBean(MaintenanceFacadeBean.class);
		return maintenanceFacadeBean.getMaintenanceMode();
	}

	@PUT
	@Path("/mode")
	public String setMode(final String mode) {
		checkSuperUser();

		if (MaintenanceConstants.MAINTENANCE_MODE_ON.equalsIgnoreCase(mode)) {
			return start();
		} else if (MaintenanceConstants.MAINTENANCE_MODE_OFF.equalsIgnoreCase(mode)) {
			return end();
		}

		throw new NuclosWebException(Status.NOT_ACCEPTABLE);
	}

	private String start() {
		checkSuperUser();

		MaintenanceFacadeBean maintenanceFacadeBean = SpringApplicationContextHolder.getBean(MaintenanceFacadeBean.class);
		String username = getUser();
		return maintenanceFacadeBean.enterMaintenanceMode(username);
	}

	private String end() {
		checkSuperUser();

		MaintenanceFacadeBean maintenanceFacadeBean = SpringApplicationContextHolder.getBean(MaintenanceFacadeBean.class);
		try {
			maintenanceFacadeBean.exitMaintenanceMode();
		} catch (org.nuclos.common2.exception.CommonValidationException e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Beispielaufruf:
	 * sessionid=`curl http://localhost:8080/nuclos-war/rest -X GET_POST -H "Content-Type: application/json" -d '{"username":"nuclos"}' | awk -v FS="\"" '{ print $4  }'`
	 * curl http://localhost:8080/nuclos-war/rest/maintenance/dbexport?sessionid=$sessionid > full.xml.gz
	 */
	@GET
	@Path("/dbexport")
	@Produces(MediaType.MULTIPART_FORM_DATA)
	public Response dbexport() {
		checkSuperUser();

		try {
			final BoDataSet dataSet = Rest.facade().importOrExportDatabase(null);
			StreamingOutput out = new StreamingOutput() {

				@Override
				public void write(OutputStream output) throws IOException,
						WebApplicationException {
					GZIPOutputStream gzipos = new GZIPOutputStream(output);
					dataSet.write(gzipos);
					gzipos.close();
				}
			};
			return Response.ok(out).build();

		} catch (Exception e) {
			// TODO
			EntityMeta<?> TODO = null;
			throw new NuclosWebException(e, Rest.translateFqn(TODO, "dbexport"));
		}
	}

	/**
	 * Beispielaufruf:
	 * sessionid=`curl http://localhost:8080/nuclos-war/rest -X GET_POST -H "Content-Type: application/json" -d '{"username":"nuclos"}' | awk -v FS="\"" '{ print $4  }'`
	 * curl -F "file=@full.xml.gz" http://localhost:8080/nuclos-war/rest/maintenance/dbimport?sessionid=$sessionid
	 */
	@POST
	@Path("/dbimport")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response dbimport(@FormDataParam("file") InputStream is) {
		checkSuperUser();

		try {
			GZIPInputStream gzipin = new GZIPInputStream(is);
			Rest.facade().importOrExportDatabase(gzipin);
			gzipin.close();
			return Response.status(Status.OK).build();

		} catch (Exception e) {
			// TODO
			EntityMeta<?> TODO = null;
			throw new NuclosWebException(e, Rest.translateFqn(TODO, "dbimport"));
		}
	}

	@POST
	@Path("/nucletexport/{nucletName}")
	public Response nucletexport(@PathParam("nucletName") String nucletName) {
		checkSuperUser();

		final boolean isDirectoryMode = LangUtils.defaultIfNull((Boolean) getFirstParameter("isDirectoryMode", Boolean.class), true);

		TransferFacadeBean transferFacadeBean = SpringApplicationContextHolder.getBean(TransferFacadeBean.class);
		MasterDataFacadeLocal masterDataFacade = SpringApplicationContextHolder.getBean(MasterDataFacadeLocal.class);

		Collection<MasterDataVO<UID>> nuclets = masterDataFacade.getMasterData(
				E.NUCLET,
				SearchConditionUtils.newComparison(E.NUCLET.name, ComparisonOperator.EQUAL, nucletName));
		if (nuclets.size() != 1) {
			String error = "Unable to find unique nuclet with name '" + nucletName + "'.";
			LOG.error(error);
			throw new NuclosWebException(Status.EXPECTATION_FAILED, error);
		}

		UID nucletUID = nuclets.iterator().next().getPrimaryKey();

		Map<TransferOption, Serializable> transferOptions = new HashMap<TransferOption, Serializable>();
		if (isDirectoryMode) {
			transferOptions.put(TransferOption.IS_DIRECTORY_MODE, null);
		}

		try {
			byte[] result = transferFacadeBean.createTransferFile(nucletUID, transferOptions);
			return Response.ok(result).build();
		} catch (NuclosBusinessException e) {
			LOG.error("Unable to export nuclet with id='" + nucletUID + "'.", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	/**
	 * Beispielaufruf Powershell:
	 * Invoke-RestMethod -Verbose -Method Post -Uri "http://localhost:8080/nuclos-war/rest" -Body '{"username":"nuclos", "password":""}' -ContentType 'application/json' -SessionVariable myWebSession
	 * Invoke-RestMethod -WebSession $myWebSession -Uri "http://localhost:8080/nuclos-war/rest/maintenance/nucletimport" -Method Post -InFile test.nuclet -ContentType 'application/octet-stream'
	 */
	@POST
	@Path("/nucletimport")
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public Response nucletimport(InputStream is) {
		checkSuperUser();
		return nucletimport(is, null);
	}

	/**
	 * Beispielaufruf:
	 * sessionid=`curl http://localhost:8080/nuclos-war/rest -X GET_POST -H "Content-Type: application/json" -d '{"username":"nuclos", "password":""}' | awk -v FS="\"" '{ print $4  }'`
	 * curl -F "file=@test.nuclet" "http://localhost:8080/nuclos-war/rest/maintenance/nucletimport?sessionid=$sessionid"
	 */
	@POST
	@Path("/nucletimport")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response nucletimport(@FormDataParam("file") InputStream is, @FormDataParam("file") FormDataContentDisposition formData) {
		checkSuperUser();

		try {
			byte[] bytes = IOUtils.toByteArray(is);
			TransferFacadeBean transferFacadeBean = SpringApplicationContextHolder.getBean(TransferFacadeBean.class);

			String fileName = null;
			if (formData != null) fileName = formData.getFileName();
			try {

				Transfer importTransferObject = transferFacadeBean.prepareTransfer(bytes, fileName, false);

				Collection<MasterDataVO<UID>> params = masterDataFacade.getMasterData(E.NUCLETPARAMETER, TrueCondition.TRUE);
				Collection<EntityObjectVO<UID>> oldParams = new ArrayList<EntityObjectVO<UID>>();
				for (MasterDataVO<UID> param : params) {
					oldParams.add(param.getEntityObject());
				}

				Collection<EntityObjectVO<UID>> newParams = importTransferObject.getParameter();

				Collection<EntityObjectVO<UID>> parameterSelection = transferFacadeBean.synchronizeParameters(oldParams, newParams);

				Transfer transfer = new Transfer(importTransferObject);
				transfer.setParameter(parameterSelection);

				transferFacadeBean.runTransfer(transfer);

				eventSupportFacadeBean.invalidateCaches();
				eventSupportFacadeBean.createBusinessObjects(true);
			} catch (NuclosBusinessException e) {
				LOG.error("Unable to import nuclet '" + fileName + "'.", e);
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}

			return Response.status(Status.OK).build();

		} catch (Exception e) {
			e.printStackTrace();
			LOG.warn("nucletimport failed: " + e, e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Path("/logging")
	@RestServiceInfo(description = "List of loggers.")
	public JsonObject getLoggerNames() {
		checkSuperUser();

		JsonObjectBuilder json = Json.createObjectBuilder();
		Enumeration<Logger> loggers = Logger.getRootLogger().getCurrentCategories();
		JsonArrayBuilder jsonarray = Json.createArrayBuilder();
		while (loggers.hasMoreElements()) {
			Logger logger = loggers.nextElement();
			if (logger.getLevel() != null) {
				JsonObjectBuilder jsonNode = Json.createObjectBuilder();
				jsonNode.add("name", logger.getName());
				jsonNode.add("level", "" + logger.getLevel());
				jsonNode.add("href", restURL("/maintenance/logging/" + logger.getName() + "/level"));
				jsonarray.add(jsonNode);
			}
		}
		json.add("logger", jsonarray);
		return json.build();
	}

	@GET
	@Path("/logging/server.log")
	@RestServiceInfo(description = "Gets the complete server.log")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getServerLogFile() {
		checkSuperUser();

		final String fileName;
		lookupFileName:
		{
			final LoggerContext ctx = (LoggerContext) org.apache.logging.log4j.LogManager.getContext(false);
			final AbstractConfiguration config = (AbstractConfiguration) ctx.getConfiguration();
			fileName = ((RollingFileAppender) config.getRootLogger().getAppenders().get("Logfile")).getFileName();
		}

		File file = new File(fileName);
		Response.ResponseBuilder response = Response.ok(file);
		response.header("Content-Disposition", "attachment; filename=server.log");
		response.header("Content-Length", FileUtils.sizeOf(file));

		return response.build();
	}

	@GET
	@Path("/logging/{logger}/level")
	@RestServiceInfo(description = "Get the log level of a certain logger.")
	public JsonObject getLogLevel(@PathParam("logger") String logger) {
		checkSuperUser();

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("level", "" + Logger.getLogger(logger).getLevel());
		return json.build();
	}

	@PUT
	@Path("/logging/{logger}/level")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(description = "Change log level of a certain logger.")
	public JsonObject setLogLevel(@PathParam("logger") String logger, JsonObject data) {
		checkSuperUser();

		Logger l = Logger.getLogger(logger);
		String level = "" + data.getString("level");
		if (level != null) {
			l.setLevel(Level.toLevel(level));
		}

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("level", "" + l.getLevel());
		return json.build();
	}

	private final static String debugSQLKey = "debugSQL";
	private final static String minExecTimeKey = "minExecTime";

	@GET
	@Path("/logging/debugSQL")
	@RestServiceInfo(description = "Get debugSQL parameter.")
	public JsonObject getDebugSQL() {
		checkSuperUser();

		JsonObjectBuilder json = Json.createObjectBuilder();
		if (DataSourceExecutor.getDebugSQL() == null) {
			json.add(debugSQLKey, JsonValue.NULL);
		} else {
			json.add(debugSQLKey, DataSourceExecutor.getDebugSQL());
		}

		if (DataSourceExecutor.getMinExecutionTimeMS() == null) {
			json.add(minExecTimeKey, JsonValue.NULL);
		} else {
			json.add(minExecTimeKey, DataSourceExecutor.getMinExecutionTimeMS());
		}

		return json.build();
	}

	@PUT
	@Path("/logging/debugSQL")
	@Consumes({MediaType.APPLICATION_JSON})
	@RestServiceInfo(description = "Set debugSQL parameter", examplePostData = "{\"debugSQL\":\"select * from test\"}")
	public JsonObject setDebugSQL(JsonObject data) {
		checkSuperUser();

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add(debugSQLKey, JsonValue.NULL);
		json.add(minExecTimeKey, JsonValue.NULL);

		String debugSQL = null;
		if (data.containsKey(debugSQLKey) && data.get(debugSQLKey) instanceof JsonString) {
			debugSQL = data.getString(debugSQLKey);
			if ("null".equals(debugSQL) || StringUtils.isNullOrEmpty(debugSQL)) {
				debugSQL = null;
			} else {
				json.add(debugSQLKey, debugSQL);
			}
		}

		Integer minExecTimeMS = null;
		if (data.containsKey(minExecTimeKey) && data.get(minExecTimeKey) instanceof JsonNumber) {
			minExecTimeMS = data.getInt(minExecTimeKey);
			if (minExecTimeMS == 0) {
				minExecTimeMS = null;
			} else {
				json.add(minExecTimeKey, minExecTimeMS);
			}
		}

		DataSourceExecutor.setDebugSQL(debugSQL);
		DataSourceExecutor.setMinExecutionTimeMS(minExecTimeMS);

		return json.build();
	}

	@POST
	@Path("/jobs/{jobName}/start")
	@RestServiceInfo(description = "Start a job immediately", isFinalized = false)
	public JsonObject startJob(@PathParam("jobName") String jobName) {
		checkSuperUser();

		JsonObjectBuilder json = Json.createObjectBuilder();

		if (jobName == null) {
			json.add("status", "job name missing");
		} else {
			try {
				UID jobUid = masterDataRestFqnCache.translateFqn(E.JOBCONTROLLER, jobName);
				if (jobUid == null) {
					json.add("status", "unknown job: " + jobName);
				} else {
					jobControlFacadeBean.startJobImmediately(jobUid);
					json.add("status", "ok");
				}
			} catch (Exception ex) {
				json.add("status", "Failed to start job: " + ex.getMessage());
			}
		}

		return json.build();
	}

	@GET
	@Path("/managementconsole")
	@RestServiceInfo(description = "get the management console command usages")
	public String managementConsole() throws Exception {
		checkSuperUser();
		return Rest.facade().executeManagementConsole(null, null);
	}

	@POST
	@Path("/managementconsole/{command}")
	@RestServiceInfo(description = "executes a management console command")
	public String managementConsole(@PathParam("command") String sCommand) throws Exception {
		checkSuperUser();
		return Rest.facade().executeManagementConsole(sCommand, null);
	}

	@POST
	@Path("/managementconsole/{command}/{arguments}")
	@RestServiceInfo(description = "executes a management console command")
	public String managementConsole(
			@PathParam("command") String sCommand,
			@PathParam("arguments") String sArguments
	) throws Exception {
		checkSuperUser();
		return Rest.facade().executeManagementConsole(sCommand, sArguments);
	}

}
