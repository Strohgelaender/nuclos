package org.nuclos.server.rest.ejb3;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;

import org.glassfish.jersey.internal.util.Base64;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.preferences.WorkspaceUtils;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.layoutml.ButtonConstants;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.nuclos.common2.layoutml.WebStaticComponent;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.parameter.NuclosParameterProvider;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Rest {

	private static final Logger LOG = LoggerFactory.getLogger(Rest.class);

	public static final byte[] EMPTY_IMAGE = Base64.decode("R0lGODlhAQABAPAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==".getBytes());
	
	private static RestFacadeLocal restFacade;
	public static RestFacadeLocal facade() {
		if (restFacade == null) {
			restFacade = SpringApplicationContextHolder.getBean(RestFacadeLocal.class);
		}
		return restFacade;
	}

	private static MetaProvider metaProvider;
	private static MetaProvider provider() {
		if (metaProvider == null) {
			metaProvider = MetaProvider.getInstance();
		}
		return metaProvider;
	}

	private static SpringLocaleDelegate localeDelegate;
	private static SpringLocaleDelegate locale() {
		if (localeDelegate == null) {
			localeDelegate = SpringLocaleDelegate.getInstance();
		}
		return localeDelegate;
	}

	//Meta Provider Block
	
	public static <PK> EntityMeta<PK> getEntity(UID entityUID) {
		return provider().getEntity(entityUID);
	}
	
	public static FieldMeta<?> getEntityField(UID fieldUID) {
		return provider().getEntityField(fieldUID);
	}
	
	public static Map<UID, FieldMeta<?>> getAllEntityFieldsByEntityIncludingVersion(UID entityUID) {
		final EntityMeta<Object> eMeta = provider().getEntity(entityUID);
		Map<UID, FieldMeta<?>> result = new HashMap<UID, FieldMeta<?>>();
		result.putAll(provider().getAllEntityFieldsByEntity(entityUID));
		if (!eMeta.isDatasourceBased()) {
			FieldMeta<?> versionMeta = SF.VERSION.getMetaData(entityUID); //TODO: Adding Version here? Brings trouble later
			result.put(versionMeta.getUID(), versionMeta);
		}
		return result;
	}
	
	public static Collection<EntityMeta<?>> getAllEntities() {
		return provider().getAllEntities();
	}
	
	public static List<EntityObjectVO<UID>> getAllEntityMenus() {
		return provider().getEntityMenus();
	}
	
	//End Meta Provider Block
	
	//Diverse Meta Block
	public static String getFieldGroupName(UID groupID) {
		return facade().getFieldGroupName(groupID);
	}
	
	public static boolean isReadAllowedForField(FieldMeta<?> fm, UsageCriteria usage) {
		return facade().isReadAllowedForField(fm, usage);
	}
	
	public static boolean isWriteAllowedForField(FieldMeta<?> fm, UsageCriteria usage) {
		return facade().isWriteAllowedForField(fm, usage);
	}
	
	public static boolean isReadAllowedForSubform(UID subform, UsageCriteria usage) {
		return facade().isReadAllowedForSubform(subform, usage);
	}

	public static boolean isWriteAllowedForSubform(UID subform, UsageCriteria usage) {
		return facade().isWriteAllowedForSubform(subform, usage);
	}
	
	public static EntityPermission getEntityPermission(UID entity) {
		return facade().getEntityPermission(entity);
	}

	//End diverse Meta Block
	
	public static Long countEntityObjectRows(UID entity, CollectableSearchExpression clctexpr) throws CommonPermissionException {
		return facade().countEntityObjectRows(entity, clctexpr);
	}
	
	//Local Delegate Block:

	public static String getLabelFromMetaFieldDataVO(FieldMeta<?> fieldMetaVO) {
		return locale().getLabelFromMetaFieldDataVO(fieldMetaVO);
	}
	
	public static String getLabelFromMetaDataVO(EntityMeta<?> entityMetaVO) {
		return locale().getLabelFromMetaDataVO(entityMetaVO);
	}
	
	public enum LabelPattern{TITLE, INFO};
	
	public static String getLabelPatternFromMetaDataVO(EntityMeta<?> entityMetaVO, LabelPattern lbl) {
		Map<UID, Object> values = new HashMap<UID, Object>();
		for (FieldMeta<?> fm : entityMetaVO.getFields()) {
			String presPart = "${" + translateUid(E.ENTITYFIELD, fm.getUID()) + "}";
			values.put(fm.getUID(), presPart);
		}
		switch (lbl) {
		case TITLE:
			return locale().getTreeViewLabel(values, entityMetaVO.getUID(), metaProvider, false, null);
		case INFO:
			return locale().getTreeViewDescription(values, entityMetaVO.getUID(), metaProvider, false, null);
		}
		throw new IllegalArgumentException("LabelPattern=" + lbl);
	}
		
	public static String getResource(String resId) {
		return locale().getResource(resId, null);
	}
	
	public static String getMessage(String rid, String otext, Object ... params) {
		return locale().getMessage(rid, otext, params);
	}
	
	public static DateFormat getDateFormat() {
		return locale().getDateFormat();
	}
	
	public static DateFormat getTimeFormat() {
		return locale().getTimeFormat();
	}
	
	public static NumberFormat getNumberFormat() {
		return locale().getNumberFormat();
	}
	
	//End Local Delegate Block
	
	//Mixed Block:
	
	public static String getTreeViewLabel(Map<UID, Object> values, UID entityUID) {
		// TODO DATALANGUAGE
		UID datalanguage = null;
		return locale().getTreeViewLabel(values, entityUID, provider(), datalanguage);
	}
	
	public static String getTreeViewDescription(Map<UID, Object> values, UID entityUID) {
		// TODO DATALANGUAGE
		UID datalanguage = null;
		return locale().getTreeViewDescription(values, entityUID, provider(), datalanguage);
	}
	
	public static String getFullMessage(CommonValidationException cve, UID entity) {
		return cve.getFullMessage(locale(), provider(), entity, null);
	}
	
	//End Mixed Block

	public static WorkspaceVO getDefaultWorkspace() throws CommonBusinessException {
		return facade().getDefaultWorkspace();
	}

	public static Set<UID> getTasks() throws CommonBusinessException {
		WorkspaceVO wovo = getDefaultWorkspace();
		if (wovo == null) {
			return new HashSet<>();
		}
		Set<UID> taskLists = WorkspaceUtils.getTaskList(wovo.getWoDesc(), facade().getCurrentMandatorUID());

		Set<UID> taskListWithNoPermission2ReadTheEntity = new HashSet<>();
		for (UID taskList : taskLists) {
			EntitySearchFilter2 sf = Rest.facade().getSearchFilterByPk(taskList);

			if (sf != null) {
				if (!getEntityPermission(sf.getVO().getEntity()).isReadAllowed()) {
					taskListWithNoPermission2ReadTheEntity.add(taskList);
				}
			}
		}

		taskLists.removeAll(taskListWithNoPermission2ReadTheEntity);
		return taskLists;
	}

	public static boolean testValidSortingField(UID field) {
		try {
			FieldMeta<?> fm = getEntityField(field);
			return fm != null && !fm.isCalculated() && !SF.STATEICON.getFieldName().equals(fm.getFieldName());
		} catch (Exception e) {
			LOG.warn(e.getMessage(), e);
		}
		return false;
	}
	
	public static String translateUid(EntityMeta<?> eMeta, UID uid) {
		if (uid == null) {
			return "";
		}
		
		return facade().translateUid(eMeta, uid);
	}
	
	public static UID translateFqn(EntityMeta<?> eMeta, String name) {
		return facade().translateFqn(eMeta, name);
	}
	
	public static Object translatePK(EntityMeta<?> eMeta, Object pk) {
		if (pk instanceof UID) {
			return Rest.translateUid(eMeta, (UID)pk);
		}
		return pk;
	}
	
	public static void addNuclosRowColorIfThere(List<FieldMeta<?>> listAttrs, UID bometa) {
	    Map<UID, FieldMeta<?>> mpFields = getAllEntityFieldsByEntityIncludingVersion(bometa);
    
	    for (UID f1 : mpFields.keySet()) {
	    	FieldMeta<?> fm = mpFields.get(f1);
	    	
	    	if (fm.isNuclosRowColor()) {
	    		if (!listAttrs.contains(fm)) {
	    			listAttrs.add(fm);
	    		}
	    	return;
	    	}
	    }
 	}

	private static UID getReplaceUID(JsonObject attrJson, UID uidValue, String fieldName, EntityMeta<?> entity) {
		if (attrJson == null || !attrJson.containsKey(fieldName)) {
			return uidValue;
		}
		
		JsonValue jsonValue = attrJson.get(fieldName);
		if (jsonValue instanceof JsonObject) {
			
			JsonObject jsonProcessOrState = (JsonObject) jsonValue;
			if (jsonProcessOrState.containsKey("id")) {
				
				JsonValue id = jsonProcessOrState.get("id");
				if (id instanceof JsonString) {
					String processOrStateId = ((JsonString)id).getString();
					UID processOrStateUid = translateFqn(entity, processOrStateId);
					
					if (processOrStateUid != null) {
						uidValue = processOrStateUid;
					}					
				}
				
			}
		}
		
		return uidValue;
	}
	
	public static UsageCriteria getUsageCriteriaFromJsonAttrAndEO(JsonObject attrJson, EntityObjectVO<?> eo) {
		UID entity = eo.getDalEntity();
		UID process = eo.getFieldUid(SF.PROCESS_UID);
		process = getReplaceUID(attrJson, process, SF.PROCESS_UID.getFieldName(), E.PROCESS);

		UID state = eo.getFieldUid(SF.STATE_UID);

		//If the entity has a state-model, there is no valid EO without state, the initial state must be assumed
		//see e.g. ISD-239
		if (state == null && hasStateModel(entity)) {
			state = facade().getInitialStateId(entity);
		}

		return new UsageCriteria(entity, process, state, Rest.facade().getCustomUsage());
	}

	public static UsageCriteria getUsageCriteriaFromEO(EntityObjectVO<?> eo) {
		return getUsageCriteriaFromJsonAttrAndEO(null, eo);
	}
	
	public static UsageCriteria getUsageCriteria(UID entity, Long pk) {
		try {
			return facade().getUsageCriteriaForPK(pk, entity);
		} catch (CommonBusinessException e) {
			// notice: relict, never thrown by eofacade
			throw new NuclosFatalException(e);
		}
	}
	
	public static UsageCriteria getDefaultUsageCriteria(UID entity) {
		return new UsageCriteria(entity, null, null, Rest.facade().getCustomUsage());
	}
	
	public static boolean hasStateModel(UID entity) {
		return getEntity(entity).isStateModel();
	}
	
	/**
	 * 
	 * @param uc
	 * @return
	 */
	public static List<StateVO> getSubsequentStates(UsageCriteria uc) {
		if (uc.getStatusUID() == null) {
			return new ArrayList<StateVO>();
		}
		
		List<StateVO> lstStates = new ArrayList<StateVO>();
		
		try {
			Collection<StateVO> collSubStates = Rest.facade().getSubsequentStates(uc);
			lstStates.addAll(collSubStates);
		} catch (CommonBusinessException cbe) {
			LOG.error(cbe.getMessage(), cbe);
		}			
		return lstStates;
	}

	/**
	 *
	 * @param entityUID
	 * @param pk
	 * @return
	 */
	public static List<StateVO> getSubsequentStates(UID entityUID, Long pk) {
		UsageCriteria uc = Rest.getUsageCriteria(entityUID, pk);
		return getSubsequentStates(uc);
	}

	public static String simpleDataType(String type) {
		String result = type.substring(type.lastIndexOf('.') + 1);
		if ("Boolean".equals(result)) {
			result = "Boolean";
		} else if (result.endsWith("Time") || 
			result.endsWith("Timestamp")) {
			result = "Timestamp";
		} else if ("Date".equals(result)) {
			result = "Date";
		} else if ("Integer".equals(result)) {
			result = "Integer";
		} else if ("Double".equals(result)) {
			result = "Decimal";
		} else if (result.endsWith("Image")) {
			result = "Image";
		} else if ("GenericObjectDocumentFile".equals(result)) {
			result = "Document";
		} else {
			result = "String";
		}
		return result;
	}
	
	public static boolean isAttributeReadOnly(FieldMeta<?> fieldMeta) {
		if (fieldMeta.isReadonly()) {
			return true;
		}
		
		if (fieldMeta.getUID().equals(SF.PROCESS_UID.getUID(fieldMeta.getEntity()))) {
			return false;
		}
		
		if (fieldMeta.getUnreferencedForeignEntity() != null) {
			return true;
		}
		
		if (fieldMeta.getCalculationScript() != null) {
			UID nucletUID = Rest.getEntity(fieldMeta.getEntity()).getNuclet();
			String ignoreGroovyRules = Rest.facade().getNucletParameterByName(nucletUID, NuclosParameterProvider.WEBCLIENT_RESTRICTIONS_MUST_IGNORE_GROOVY_RULES);
			if (!"true".equals(ignoreGroovyRules)) {
				return true;
			}
		}
		
		return false;
	}
	
	public static List<CollectableValueIdField> transformToValueIdField(List<CollectableField> lstCF) {
		List<CollectableValueIdField> result = new ArrayList<CollectableValueIdField>();
		for (CollectableField cf : lstCF) {
			CollectableValueIdField cvif;
			
			if (cf instanceof CollectableValueIdField) {
				cvif = (CollectableValueIdField) cf;
			} else {
				cvif = new CollectableValueIdField(null, cf.getValue());
			}
			
			result.add(cvif);
		}
		return result;
	}
	
	public static Set<UID> getRestMenuEntities() {
		String sp = facade().getParamRestMenuEntities();
		
		if (sp != null) {
			Set<UID> ret = new HashSet<UID>();
			for (String s : sp.split(",")) {
				String st = s.trim();
				
				UID uidEntity = translateFqn(E.ENTITY, st);
				if (uidEntity == null) {
					uidEntity = new UID(st);
				}
				
				ret.add(uidEntity);
			}
			
			return ret;
		}
		
		return null;
	}
	
	public static boolean isButtonEnabled(WebStaticComponent button, UsageCriteria uc, String user) {
		
		String action = button.getProperties().get(LayoutMLConstants.ATTRIBUTE_ACTIONCOMMAND);
		if (ButtonConstants.ACTION_EXECUTERULEBUTTON.equals(action)) {
			//TODO: mandator
			Set<String> actions = SecurityCache.getInstance().getAllowedActions(user, null);
			
			if (!actions.contains(Actions.ACTION_EXECUTE_RULE_BY_USER)) {
				return false;
			}
			
			List<EventSupportSourceVO> lstSevo = facade().findEventSupportsByUsageAndEntity(CustomRule.class.getCanonicalName(), uc);
			
			final String rule = button.getProperties().get("ruletoexecute");
			
			for (EventSupportSourceVO sevo : lstSevo) {
				if (sevo.isActive() && sevo.getClassname().equals(rule)) {
					return true;
				}
			}

			return false;
		}
		
		// TODO implement state dependent enabled/disabled for other buttons/action than execute rule.
		return true;
	}
	
	public static List<FieldMeta<?>> getFieldsFromQueryAttributes(UID entityUID, String queryAttributes) {
		List<FieldMeta<?>> lstFM = new ArrayList<FieldMeta<?>>();
		
		if (queryAttributes != null) {
			for (String qAttr : queryAttributes.split(",")) {
				try {
					UID fieldUID = translateFqn(E.ENTITYFIELD, qAttr.trim());
					lstFM.add(getEntityField(fieldUID));
				} catch (Exception e) {
					LOG.error("Unable to add attribute {} to field list:", qAttr, e);
				}
			}			
		}
		

		return lstFM;
	}
		
	public static List<FieldMeta<?>> getFieldsFromQueryAttributesAndSystem(UID entityUID, String queryAttributes) {
		List<FieldMeta<?>> lstFM = getFieldsFromQueryAttributes(entityUID, queryAttributes);
		final EntityMeta<Object> eMeta = provider().getEntity(entityUID);

		Set<UID> usedUids = new HashSet<UID>();
		if (eMeta.isDatasourceBased()) {
			final FieldMeta<?> refToEntityMetaField = (FieldMeta<?>) eMeta.getProperty(EntityMeta.PROPERTY.TASKLIST_REF_TO_ENTITYMETA_FIELD);
			if (refToEntityMetaField != null) {
				addIfNotContains(refToEntityMetaField, lstFM, usedUids);
			}
		} else {
			for (FieldMeta<?> fm : lstFM) {
				usedUids.add(fm.getUID());
			}
			addSystemAttributes(entityUID, lstFM, usedUids);
		}
		
		return lstFM;
	}
	
	public static boolean canAccessEntityWithLayout(UID entity) {
		
		if (!Rest.getEntityPermission(entity).isReadAllowed()) {
			return false;
			
		} else {
			UsageCriteria uc = Rest.getDefaultUsageCriteria(entity);
			if (Rest.facade().getDetailLayoutIDForUsage(uc, false) == null) {
				return false;            						
			}            					
		}
		
		return true;
	}
	
	private static void addSystemAttributes(UID entityUID, List<FieldMeta<?>> lstFM, Set<UID> usedUids) {
		EntityMeta<Object> eMeta = getEntity(entityUID);
		// add defaults
		addIfNotContains(eMeta.getPk().getMetaData(entityUID), lstFM, usedUids);
		addIfNotContains(SF.CREATEDAT.getMetaData(entityUID), lstFM, usedUids);
		addIfNotContains(SF.CREATEDBY.getMetaData(entityUID), lstFM, usedUids);
		addIfNotContains(SF.CHANGEDAT.getMetaData(entityUID), lstFM, usedUids);
		addIfNotContains(SF.CHANGEDBY.getMetaData(entityUID), lstFM, usedUids);
		addIfNotContains(SF.VERSION.getMetaData(entityUID), lstFM, usedUids);
		
		if (hasStateModel(entityUID)) {
			addIfNotContains(SF.PROCESS.getMetaData(entityUID), lstFM, usedUids);
			addIfNotContains(SF.STATE.getMetaData(entityUID), lstFM, usedUids);
			addIfNotContains(SF.STATENUMBER.getMetaData(entityUID), lstFM, usedUids);
			addIfNotContains(SF.SYSTEMIDENTIFIER.getMetaData(entityUID), lstFM, usedUids);
		}
		
		if (eMeta.isOwner()) {
			// Use field from eMeta, it contains special ForeignEntityValue from eMeta
			addIfNotContains(eMeta.getField(SF.OWNER.getUID(eMeta)), lstFM, usedUids);
		}

		if (eMeta.isMandator()) {
			addIfNotContains(eMeta.getField(SF.MANDATOR.getUID(eMeta)), lstFM, usedUids);
		}

		addNuclosRowColorIfThere(lstFM, entityUID);
		
	}
	
	private static void addIfNotContains(FieldMeta<?> add, List<FieldMeta<?>> to, Set<UID> toIds) {
		if (!toIds.contains(add.getUID())) {
			toIds.add(add.getUID());
			to.add(add);
		}
	}

	public static Collection<TasklistDefinition> getDynamicTaskLists() {
		return facade().getDynamicTaskLists();
	}

}
