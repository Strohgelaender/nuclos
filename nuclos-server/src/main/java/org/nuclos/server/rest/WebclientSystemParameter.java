package org.nuclos.server.rest;

/**
 * Represents all system parameters that should be publicly accessible for normal users.
 *
 * ATTENTION: Do not add sensitive parameters here!
 *
 * TODO: Move to nuclos-schema, generate for webclient
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public enum WebclientSystemParameter {
	ENVIRONMENT_DEVELOPMENT,

	FULLTEXT_SEARCH_ENABLED,
	ANONYMOUS_USER_ACCESS_ENABLED,
	USER_REGISTRATION_ENABLED,

	WEBCLIENT_CSS,

	FORGOT_LOGIN_DETAILS_ENABLED
}