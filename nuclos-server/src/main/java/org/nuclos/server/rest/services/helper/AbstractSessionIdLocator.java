package org.nuclos.server.rest.services.helper;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public abstract class AbstractSessionIdLocator {

	public static final String SESSION_COOKIE_NAME = "JSESSIONID";

	@Context protected HttpHeaders headers;
	@Context protected UriInfo uriInfo;
	@Context protected HttpServletRequest httpRequest;
	
	protected final String getSessionId() {
		String result = getSessionId("sessionId");
		if (result != null) {
			// not used by the webclient but by other clients
			return result;
		}
		Cookie cookie = headers.getCookies().get(SESSION_COOKIE_NAME);
		if (cookie != null) {
			return cookie.getValue();
		}
		return null;
	}

	private final String getSessionId(final String key) {
		List<String> sids;
		sids = getQueryParameters(key);
		if (sids != null && !sids.isEmpty()) {
			return sids.get(0);
		}
		sids = headers.getRequestHeader(key);
		if (sids != null && !sids.isEmpty()) {
			return sids.get(0);
		}
		return null;
	}

	public Set<String> getQueryParameterKeys() {
		return getQueryParameters().keySet();
	}

	protected MultivaluedMap<String, String> getQueryParameters() {
		return uriInfo.getQueryParameters();
	}

	protected List<String> getQueryParameters(String key) {
		return getQueryParameters().get(key);
	}

	private String restPath;
	private String buildRestPath() {
		if (restPath == null) {
			restPath = (httpRequest.isSecure() ? "https://" : "http://") + httpRequest.getServerName() + ":"
					+ httpRequest.getServerPort() + httpRequest.getContextPath() + httpRequest.getServletPath();
		}
		return restPath;
	}

	protected final String restURL(String relativeUrl) {
		return buildRestPath() + relativeUrl;
	}

}
