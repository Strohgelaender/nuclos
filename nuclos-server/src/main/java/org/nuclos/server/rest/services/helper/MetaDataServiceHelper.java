package org.nuclos.server.rest.services.helper;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.ProfileUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescription2.Tab;
import org.nuclos.common.WorkspaceDescription2.Tabbed;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.PreferenceVO;
import org.nuclos.common.preferences.ColumnPreferences;
import org.nuclos.common.preferences.ColumnSortingPreferences;
import org.nuclos.common.preferences.ExplorerRestorePreferences;
import org.nuclos.common.preferences.IPreferencesProvider;
import org.nuclos.common.preferences.RestorePreferences;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.tasklist.TasklistDefinition;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.resource.ResourceResolver;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.dal.processor.nuclet.IPreferenceProcessor;
import org.nuclos.server.navigation.treenode.MasterDataSearchResultNodeParameters;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.BoMetaOverview;
import org.nuclos.server.rest.misc.IMenuEntry;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.ReferenceFieldMeta;
import org.nuclos.server.rest.misc.RestLinks;
import org.nuclos.server.rest.misc.RestLinks.Verbs;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.misc.TaskOverview;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.rest.services.rvo.MenuPreferenceRVO;
import org.nuclos.server.rest.services.rvo.MenuPreferenceRVO.MenuItemRVO;
import org.nuclos.server.rest.services.rvo.MenuTreeNodeRVO;
import org.nuclos.server.rest.services.rvo.MenuTreeNodeRVO.TreeNodeBuilder;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;

public class MetaDataServiceHelper extends WebContext {
	
	@Autowired
	private IPreferenceProcessor prefsProc;

	@Autowired
	private IPreferencesProvider prefsProv;

	private static final Logger LOG = LoggerFactory.getLogger(MetaDataServiceHelper.class);

	protected JsonArrayBuilder getBOMetaList() {
		try {
			return getReadableBOMetas(null, null, false);
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(e, null);
		}
	}
	
	
	public void toggleMenu(String menu, boolean expand) {
		PreferenceVO pref = getMenuPreferenceItem();
		boolean createNewPreferenceEntry = false;
		if (pref == null) {
			createNewPreferenceEntry = true;
			pref = new PreferenceVO();
			pref.flagNew();
			UID user = SecurityCache.getInstance().getUserUid(getUser());
			pref.setPrimaryKey(new UID());
			pref.setUser(user);
			pref.setType(NuclosPreferenceType.MENU.getType());
			pref.setApp("nuclos");
			pref.setName("menu");
			pref.setJson(Json.createObjectBuilder().build());
			pref.setCreatedAt(new InternalTimestamp(new Date().getTime()));
			pref.setCreatedBy(getUser());
			pref.setChangedAt(new InternalTimestamp(new Date().getTime()));
			pref.setChangedBy(getUser());
		}
		try {
			MenuPreferenceRVO mrvo = MenuPreferenceRVO.parse(pref.getJson().toString());
			boolean updateExistingEntry = false;
			for(MenuItemRVO mirvo : mrvo.getMenuItems()) {
				if (mirvo.getName().equals(menu)) {
					mirvo.setIsExpanded(expand);
					updateExistingEntry = true;
				}
			}
			if (!updateExistingEntry) {
				mrvo.getMenuItems().add(new MenuItemRVO(menu, expand));
			}
			
			pref.setJson(Json.createReader(new StringReader(mrvo.toString())).readObject());
			if(!createNewPreferenceEntry) {
				pref.flagUpdate();
			}
			prefsProc.insertOrUpdate(pref);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.warn("Unable to update menu state.", e);
		}
	}
	
	
	private PreferenceVO getMenuPreferenceItem() {
		UID user = SecurityCache.getInstance().getUserUid(getUser());
		final List<PreferenceVO> prefs = prefsProc.getByAppTypeUserAndEntity("nuclos", NuclosPreferenceType.MENU.getType(), user, null, null, false);
		if (prefs.size() == 0) {
			return null;
		}
		if (prefs.size() > 1) {
			LOG.warn("Found more than 1 menu preferenes (" + prefs.size() + ") for user '" + user + "'.");
		}
		return prefs.get(0);
	}
	
	
	protected JsonArrayBuilder getMenu() {
		try {
			PreferenceVO pref = getMenuPreferenceItem();

			Set<String> reducedStartmenus = new HashSet<String>();
			if (pref != null) {
				MenuPreferenceRVO mrvo = MenuPreferenceRVO.parse(pref.getJson().toString());
				for(MenuItemRVO mirvo : mrvo.getMenuItems()) {
					if (!mirvo.getIsExpanded()) {
						reducedStartmenus.add(mirvo.getName());
					}
				}
			}
			return getReadableBOMetas(null, reducedStartmenus, true);
		} catch (Exception e) {
			throw new NuclosWebException(e, null);
		}
	}

	protected MenuTreeNodeRVO getMenuStructure() {
		try {
			PreferenceVO pref = getMenuPreferenceItem();

			Set<String> reducedStartmenus = new HashSet<>();
			if (pref != null) {
				MenuPreferenceRVO mrvo = MenuPreferenceRVO.parse(pref.getJson().toString());
				for(MenuItemRVO mirvo : mrvo.getMenuItems()) {
					if (!mirvo.getIsExpanded()) {
						reducedStartmenus.add(mirvo.getName());
					}
				}
			}
			return getReadableBOMetasMenuStructure(reducedStartmenus);
		} catch (Exception e) {
			throw new NuclosWebException(e, null);
		}
	}

	protected JsonArrayBuilder getTasks() {
		try {
			return getReadableBOMetas(Rest.getTasks(), null, true);
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(e, null);
		}
	}

	protected JsonArrayBuilder getTaskLists() {
		try {
			return getReadableBOMetas(Rest.getTasks(), null, false);
		} catch (CommonBusinessException e) {
			throw NuclosWebException.wrap(e);
		}
	}

	protected JsonObjectBuilder getBOMetaSelf(String id) {
		//NUCLOS-4221: Sub-forms grant implicitly the privileges for certain entities. Meta-Data is needed and thus the direct check is not possible.
		//validateSession(E.ENTITY, id);
		
		//TODO: Evaluate if this means a security breach. Until now it doesn't look very dangerous as any request for real data is not possible.
		checkPermission(E.ENTITY, id, null, false);
		try {
			return JsonFactory.buildJsonObject(getBOMeta(), this);
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, id));
		}
	}
	
	protected final JsonObjectBuilder getDependenceMetaSelf(String boMetaId, String reffield) {
		UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, reffield);

		try {
			FieldMeta<?> fieldMeta = Rest.getEntityField(fieldUid);
			EntityMeta<?> subformMeta = Rest.getEntity(fieldMeta.getEntity());
			UID baseEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);
			EntityMeta<?> baseEntityMeta = Rest.getEntity(baseEntityUid);

			SessionEntityContext info = checkPermissionForSubform(baseEntityMeta, fieldMeta, null, false);
			
			ReferenceFieldMeta rfMeta = new ReferenceFieldMeta(fieldMeta, subformMeta, info.getMasterMeta());
			return JsonFactory.buildJsonObject(rfMeta, this);
			
		} catch (Exception cbe) {
			throw new NuclosWebException(cbe, fieldUid);
		}
	}
	
	/**
	 * TODO consolidate with getReadableBOMetasMenu
	 * TODO: Duplicated code
	 * 
	 * at the moment there is 
	 * one REST service for the header menu (which is also responsible for tasks - but tasks are also shown in the dashboard) 
	 * one REST service for the dashboard menu
	 * - this needs to be refactored
	 */
	private MenuTreeNodeRVO getReadableBOMetasMenuStructure(
			Set<String> reducedStartmenus
	) {
		Set<UID> restMenuEntites = Rest.getRestMenuEntities();

		TreeNodeBuilder treeNodeList = new TreeNodeBuilder(this);
		Set<UID> menuEntities = new HashSet<>();

		final Collection<TasklistDefinition> dynamicTaskLists = Rest.getDynamicTaskLists();
		if (dynamicTaskLists != null && !dynamicTaskLists.isEmpty()) {
			Map<UID, EntityMeta<?>> mapDynamicTasklistEntityMeta = getDynamicTaskListEntityMetaMap();

			for (TasklistDefinition tlDef : dynamicTaskLists) {
				final EntityMeta<?> tasklistEntityMeta = mapDynamicTasklistEntityMeta.get(tlDef.getId());
				if (tasklistEntityMeta != null) {
					final String menu = Rest.getResource(tlDef.getMenupathResourceId());
					final String label = Rest.getResource(tlDef.getLabelResourceId());
					treeNodeList.add(
							Rest.translateUid(E.ENTITY, tasklistEntityMeta.getUID()),
							menu + "\\" + label,
							false,
							// TODO: org.nuclos.common.E._Searchfilter.getNuclosResource could be used here, if the icon wasn't blue.
							"org.nuclos.client.resource.icon.glyphish.06-magnify.png",
							false,
							null
					);
				}
			}
		}

		for (EntityObjectVO<?> conf : Rest.getAllEntityMenus()) {
			EntityMeta<UID> meta = MetaProvider.getInstance().getEntity(conf.getFieldUid(E.ENTITYMENU.entity.getUID()));
			UID entity = meta.getUID();

			if (shouldEntryBeSkipped(restMenuEntites, menuEntities, entity)) {
				continue;
			}

			String menuRootPath = null;
			String[] menuPath = splitMenuPath(
					SpringLocaleDelegate.getInstance().getResource(
							conf.getFieldValue(E.ENTITYMENU.menupath.getUID(), String.class),
							null
					)
			);

			if (menuPath != null && menuPath.length > 0) {
				menuRootPath = menuPath[0];
			}

			if (menuRootPath != null) {
				EntityPermission permission = Rest.getEntityPermission(meta.getUID());

				if (permission.isReadAllowed()) {
					String boMetaId = meta.getUID() != null ? Rest.translateUid(E.ENTITY, meta.getUID()) : null;
					boolean hidden = reducedStartmenus != null && reducedStartmenus.contains(menuRootPath);
					String icon = meta.getNuclosResource() != null ? meta.getNuclosResource() : ResourceResolver.DEFAULT_ICON;
					String path = "";
					for (String part : menuPath) {
						path += part + "\\";
					}
					path = path.substring(0, path.length() - 1);
					treeNodeList.add(
							boMetaId,
							path,
							hidden,
							icon,
							conf.getFieldValue(E.ENTITYMENU.newfield.getUID(), Boolean.class),
							Rest.translateUid(E.PROCESS, conf.getFieldUid(E.ENTITYMENU.process.getUID()))
					);
				}
				menuEntities.add(entity);
			}
		}


		for (final EntityMeta<?> meta : Rest.getAllEntities()) {
			if (shouldEntryBeSkipped(restMenuEntites, menuEntities, meta.getUID())) {
				continue;
			}

			if (meta.getLocaleResourceIdForMenuPath() != null) {
				EntityPermission permission = Rest.getEntityPermission(meta.getUID());
				if (permission.isReadAllowed()) {
					String boMetaId = meta.getUID() != null ? Rest.translateUid(E.ENTITY, meta.getUID()) : null;
					String icon = meta.getNuclosResource() != null ? meta.getNuclosResource() : ResourceResolver.DEFAULT_ICON;
					treeNodeList.add(
							boMetaId,
							Rest.getResource(meta.getLocaleResourceIdForMenuPath()) + "\\" + Rest.getResource(meta.getLocaleResourceIdForLabel()),
							false,
							icon,
							false,
							null
					);
				}
			}
		}

		return treeNodeList.build();
	}

	private Map<UID, EntityMeta<?>> getDynamicTaskListEntityMetaMap() {
		Map<UID, EntityMeta<?>> mapDynamicTasklistEntityMeta = new HashMap<>();

		for (EntityMeta<?> eMeta : Rest.getAllEntities()) {
			if (eMeta.isDynamicTasklist()) {
				final TasklistDefinition tlDef = (TasklistDefinition) eMeta.getProperty(EntityMeta.PROPERTY.TASKLIST_DEFINITION);
				if (tlDef != null) {
					mapDynamicTasklistEntityMeta.put(tlDef.getId(), eMeta);
				}
			}
		}
		return mapDynamicTasklistEntityMeta;
	}

	/**
	 * check if entry needs to be added to menu
	 * @return true if it should not be added to menu
	 */
	private boolean shouldEntryBeSkipped(
			final Set<UID> restMenuEntites,
			final Set<UID> menuEntities,
			final UID entity
	) {
		boolean bIsNuclosEntity = E.isNuclosEntity(entity);
		if (bIsNuclosEntity) {
			return true;
		}

		boolean bReadAllowedForEntity = isReadAllowedForEntity(entity);
		if (!bReadAllowedForEntity) {
			return true;
		}

		if (restMenuEntites != null && !restMenuEntites.contains(entity)) {
			return true;
		}

		return menuEntities.contains(entity);

	}


	/**
	 * TODO: Method is too complex and has too many responsibilities.
	 * TODO: Duplicated code.
	 * TODO: Return a proper entity, instead of only a JsonArrayBuilder.
	 *
	 * @param setTasks UIDs of searchfilter-based tasklists (extracted from Workspace)
	 */
	private JsonArrayBuilder getReadableBOMetas(
			Set<UID> setTasks,
			Set<String> reducedStartmenus,
			boolean bAsMenuTree
	) throws CommonBusinessException {
		JsonArrayBuilder jsonarray = Json.createArrayBuilder();

		Set<UID> restMenuEntites = null;
		Map<UID, List<SearchFilterVO>> mpSearchFilter = null;
		final boolean isTaskMode = setTasks != null;
		if (isTaskMode) {
			mpSearchFilter = getSearchFiltersByEntityForTaskLists(setTasks);
		} else if (bAsMenuTree) {
			restMenuEntites = Rest.getRestMenuEntities();
		}
		Map<String, List<JsonValue>> rootMenus = new HashMap<>();
		Set<UID> menuEntities = new HashSet<>();

		// TODO: Should be a separate method!
		if (isTaskMode) {
			final Collection<TasklistDefinition> dynamicTaskLists = Rest.getDynamicTaskLists();
			if (dynamicTaskLists != null && !dynamicTaskLists.isEmpty()) {
				Map<UID, EntityMeta<?>> mapDynamicTasklistEntityMeta = getDynamicTaskListEntityMetaMap();

				for (TasklistDefinition tlDef : dynamicTaskLists) {
					final EntityMeta<?> tasklistEntityMeta = mapDynamicTasklistEntityMeta.get(tlDef.getId());
					if (tasklistEntityMeta != null) {
						EntityPermission permission = new EntityPermission(true, false, false, false);
						final String sLabel = Rest.getResource(tlDef.getLabelResourceId());
						IMenuEntry entry = new TaskOverview(
								tasklistEntityMeta,
								tlDef.getId(),
								sLabel,
								permission,
								tlDef.getDynamicTasklistEntityFieldUid(),
								tlDef.getTaskEntityUID(),
								null
						);

						String menuroot = null;
						if (!bAsMenuTree) {
							menuroot = "";
						} else {
							String[] menuPath = splitMenuPath(Rest.getResource(tlDef.getMenupathResourceId()));
							if (menuPath != null && menuPath.length > 0) {
								menuroot = menuPath[0];
							}
						}
						createMenuItem(rootMenus, menuroot, entry);
					}
				}
			}
		}

		for (EntityObjectVO<?> conf : Rest.getAllEntityMenus()) {
			EntityMeta<UID> meta = MetaProvider.getInstance().getEntity(conf.getFieldUid(E.ENTITYMENU.entity.getUID()));
			UID entity = meta.getUID();
			if (isTaskMode && !mpSearchFilter.containsKey(entity)) {
				continue;
			}

			boolean bIsNuclosEntity = E.isNuclosEntity(entity);
			if (bIsNuclosEntity && (bAsMenuTree || !(E.USER.checkEntityUID(entity) || E.MANDATOR.checkEntityUID(entity)))) {
				continue;
			}

			boolean bReadAllowedForEntity = isReadAllowedForEntity(entity);
			if (bAsMenuTree && !bReadAllowedForEntity) {
				continue;
			}

			String menuroot = null;
			String menuLabel = null;

			if (!bAsMenuTree) {
				menuroot = "";

			} else {
				String[] menuPath = splitMenuPath(SpringLocaleDelegate.getInstance().getResource(
						conf.getFieldValue(E.ENTITYMENU.menupath.getUID(), String.class), null));

				if (menuPath != null && menuPath.length > 0) {
					menuroot = menuPath[0];
					menuLabel = menuPath.length > 1 ? menuPath[menuPath.length-1] : null;
				}
			}

			if (menuroot != null) {
				EntityPermission permission = Rest.getEntityPermission(meta.getUID());

				if (!isTaskMode) {
					String label;
					if (menuLabel == null) {
						label = menuroot;
						menuroot = "";
					} else {
						label = menuLabel;
					}
					IMenuEntry entry = new BoMetaOverview(
							meta,
							label,
							permission,
							bAsMenuTree,
							conf.getFieldUid(E.ENTITYMENU.process.getUID()),
							conf.getFieldValue(E.ENTITYMENU.newfield.getUID(), Boolean.class)
					);
					createMenuItem(rootMenus, menuroot, entry);

				} else {
					for (SearchFilterVO sf : mpSearchFilter.get(entity)) {
						IMenuEntry entry = new TaskOverview(
								meta,
								null,
								sf.getFilterName(),
								permission,
								null,
								entity,
								sf.getId()
						);
						createMenuItem(rootMenus, menuroot, entry);
					}
				}
				menuEntities.add(entity);
			}
		}

		for (final EntityMeta<?> meta : Rest.getAllEntities()) {
			UID entity = meta.getUID();
			if (isTaskMode && !mpSearchFilter.containsKey(entity)) {
				continue;
			}

			boolean bIsNuclosEntity = E.isNuclosEntity(entity);
			if (bIsNuclosEntity && (bAsMenuTree || !(E.USER.checkEntityUID(entity) || E.MANDATOR.checkEntityUID(entity)))) {
				continue;
			}

			boolean bReadAllowedForEntity = isReadAllowedForEntity(entity);
			if (bAsMenuTree && !bReadAllowedForEntity) {
				continue;
			}

			if (restMenuEntites != null && !restMenuEntites.contains(entity)) {
				continue;
			}

			if (menuEntities.contains(entity)) {
				continue;
			}

			String menuroot = null;

			if (!bAsMenuTree) {
				menuroot = "";

			} else {
				String[] menuPath = splitMenuPath(Rest.getResource(meta.getLocaleResourceIdForMenuPath()));

				if (menuPath != null && menuPath.length > 0) {
					menuroot = menuPath[0];
				}
			}

			if (menuroot != null) {
				EntityPermission permission = Rest.getEntityPermission(meta.getUID());

				if (!isTaskMode) {
					String label = Rest.getLabelFromMetaDataVO(meta);
					IMenuEntry entry = new BoMetaOverview(meta, label, permission, bAsMenuTree);
					createMenuItem(rootMenus, menuroot, entry);

				} else {
					for (SearchFilterVO sf : mpSearchFilter.get(entity)) {
						IMenuEntry entry = new TaskOverview(
								meta,
								null,
								sf.getFilterName(),
								permission,
								null,
								entity,
								sf.getId()
						);
						createMenuItem(rootMenus, menuroot, entry);
					}
				}
			}
		}

		for (String rootMenu: rootMenus.keySet()) {
			List<JsonValue> lstMenuItems = rootMenus.get(rootMenu);

			if (bAsMenuTree) {
				JsonArrayBuilder miArray = Json.createArrayBuilder();

				for (JsonValue menuItem : lstMenuItems) {
					miArray.add(menuItem);
				};


				JsonObjectBuilder job = Json.createObjectBuilder();
				job.add("path", rootMenu);
				job.add("entries", miArray.build());

				if (reducedStartmenus != null && reducedStartmenus.contains(rootMenu)) {
					job.add("hidden", true);
				}

				JsonObject menu = job.build();

				jsonarray.add(menu);

			} else {
				for (JsonValue menuItem : lstMenuItems) {
					jsonarray.add(menuItem);
				};
			}
		}
		return jsonarray;
 	}

	private Map<UID, List<SearchFilterVO>> getSearchFiltersByEntityForTaskLists(final Set<UID> setTasks) throws CommonBusinessException {
		final Map<UID, List<SearchFilterVO>> result = new HashMap<>();

		for (UID task : setTasks) {
			EntitySearchFilter2 searchFilter2 = Rest.facade().getSearchFilterByPk(task);
			if (searchFilter2 != null) {
				UID sfEntity = searchFilter2.getVO().getEntity();
				if (!result.containsKey(sfEntity)) {
					result.put(sfEntity, new ArrayList<>());
				}
				result.get(sfEntity).add(searchFilter2.getVO());
			}
		}

		return result;
	}

	private void createMenuItem(Map<String, List<JsonValue>> rootMenus, String menuroot, IMenuEntry menuEntry) {
		JsonValue menuItem = JsonFactory.buildJsonObject(menuEntry, this).build();
		
		if (!rootMenus.containsKey(menuroot)) {
			rootMenus.put(menuroot, new ArrayList<JsonValue>());
		}
		rootMenus.get(menuroot).add(menuItem);
 	}
 	
	private static String[] splitMenuPath(String menuPath) {
		if (menuPath == null || menuPath.isEmpty()) {
			return null;			
		}
		return menuPath.split("\\\\");
	}
	
	protected JsonObjectBuilder getProcessLayout(String processId, String boMetaId, String boId) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
		
		try {
			JsonObjectBuilder builder = Json.createObjectBuilder();
			
			UID entityUID = Rest.translateFqn(E.ENTITY, boMetaId);
			UID processUID = Rest.translateFqn(E.PROCESS, processId);
			UID statusUID;
			String customUsage;
			
			if (boId != null && !boId.isEmpty()) {
				UsageCriteria usage = Rest.facade().getUsageCriteriaForPK(Long.parseLong(boId), entityUID);
				statusUID = usage.getStatusUID();
				customUsage = usage.getCustom();
				
			} else {
				statusUID = Rest.facade().getInitialStateId(entityUID);
				customUsage = Rest.facade().getCustomUsage();
				
			}
			
			UsageCriteria newUsage = new UsageCriteria(entityUID, processUID, statusUID, customUsage);		
			
			UID newLayoutUID = Rest.facade().getDetailLayoutIDForUsage(newUsage, false);
			if (newLayoutUID != null) {
				RestLinks links = new RestLinks(builder);
				links.addLinkHref("layout", "weblayoutCalculated", Verbs.GET, Rest.translateUid(E.LAYOUT, newLayoutUID));
				links.buildJson(this);
			}
			
			return builder;			
		} catch (Exception e) {
			throw new NuclosWebException(e, info.getEntity());
		}
	}

	protected JsonArray buildSideviewMenuSelector() {

		JsonArrayBuilder tabs = Json.createArrayBuilder();

		try {
			UID mandator = Rest.facade().getCurrentMandatorUID();
			List<Tab> neverCloseTabs = getNeverCloseTabs(mandator);

			for(Tab tab : neverCloseTabs) {
				try {
					JsonObjectBuilder json = Json.createObjectBuilder();
					String type = "";
					RestLinks restLinks = new RestLinks(json);
					UID boMetaId = null;
					Object boId = "";
					Object preferences = RestorePreferences.objectFromXML(tab.getPreferencesXML());
					
					if (preferences instanceof ExplorerRestorePreferences) {
						ExplorerRestorePreferences explorerRestorePreferences = (ExplorerRestorePreferences) preferences;
						type = "explorer";

						if (explorerRestorePreferences.getDefaultMasterDataTreeNodeParameters() != null) {
							// This is the explorer tree from a single entity-object.
							restLinks.addLink("tree", Verbs.GET, explorerRestorePreferences.getDefaultMasterDataTreeNodeParameters().getsEntity().toString());
							boMetaId = explorerRestorePreferences.getDefaultMasterDataTreeNodeParameters().getsEntity();
							boId = explorerRestorePreferences.getDefaultMasterDataTreeNodeParameters().getId();

						} else if (explorerRestorePreferences.getMasterDataSearchResultNodeParameters() != null) {
							MasterDataSearchResultNodeParameters params = explorerRestorePreferences.getMasterDataSearchResultNodeParameters();
							boMetaId = params.getsEntity();
							// TODO: NUCLOS-5978 ExplorerTree From a ResultListe (Search) is not supported yet.
							LOG.warn("There is an explorer tree from a Search-Result. It is not delivered as the Webclient doesn't support it yet.");
							continue;

						} else if (explorerRestorePreferences.getGenericObjectTreeNodeParameters() != null) {
							restLinks.addLink("boMeta", Verbs.GET, explorerRestorePreferences.getGenericObjectTreeNodeParameters().getUsagecriteria().getEntityUID().toString());
							boMetaId = explorerRestorePreferences.getGenericObjectTreeNodeParameters().getUsagecriteria().getEntityUID();
							
						} else {
							LOG.warn("There is an explorer tree with a faulty configuration!");
							continue;
						}
						
					} else if (preferences instanceof RestorePreferences) {
						RestorePreferences restorePreferences = (RestorePreferences) preferences;
			
						if (restorePreferences.searchFilterId != null) {
							/*
							 don't show searchfilter tasklist tabs 

	 						type = "searchfilter";
							boMetaId = restorePreferences.searchFilterId;							
							restLinks.addLink("searchfilter", Verbs.GET, boMetaId.getString());
							*/
							continue;
							
						} else if (restorePreferences.tasklistId != null) {
							type =" takslist";
							//TODO: What is to do here? Until this matter has been dissolved, skip this tab
							continue;
							
						}
						
					}
					
					String sBoMetaId = Rest.translateUid(E.ENTITY, boMetaId);
					if (sBoMetaId == null) {
						//TODO: For reasons not yet understood, this may be null. So skip this tab.
						LOG.warn("sBoMetaID == null for boMetaID = {}. Skipping this tab", boMetaId);
						continue;						
					}
					
					//TODO: Not fully understood
					String sBoId = boId.toString();
					
					json.add("type", type)
						.add("label", tab.getLabel())
						.add("boMetaId", sBoMetaId)
						.add("boId", sBoId);
					
					restLinks.buildJson(this);
					
					tabs.add(json.build());
					
					

				} catch(CannotResolveClassException | ConversionException e) {
					LOG.warn(e.getMessage(), e);
				}
			}
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(e, null);
		}

		return tabs.build();
	}
	
	private List<Tab> getNeverCloseTabs(UID mandator) throws CommonBusinessException {
		List<Tab> result = new ArrayList<WorkspaceDescription2.Tab>();
		WorkspaceVO wvo = Rest.getDefaultWorkspace();
		if(wvo != null) {
			WorkspaceDescription2 wsd=wvo.getWoDesc();
			for(Tabbed tabbed : wsd.getTabbeds()) {
				for(Tab tab : tabbed.getTabs(mandator)) {
					if(tab.isNeverClose()) {
						result.add(tab);
					}
				}
			}
		}
 		return result;
	}
	
	public TableViewLayout getTableViewLayout(UID entityUID) {
		return getTableViewLayout(entityUID, null);
	}
		
	public TableViewLayout getTableViewLayout(UID entityUID, UID dependenceUID) {

		List<UID> sortedColumns = new ArrayList<UID>();
		List<Integer> columnWidths = new ArrayList<Integer>();
		List<ColumnSortingPreferences> columnSortings = null;

		try {
			final String userName = Rest.facade().getCurrentUserName();
			final UID mandatorUID = Rest.facade().getCurrentMandatorUID();
			final TablePreferencesManager tblprefManager;
			if (dependenceUID != null) {
				// TODO layout should be a parameter!
				UID layoutUID = null;
				// layout null? try to get default layout for main entity...
				if (layoutUID == null && entityUID != null) {
					layoutUID = Rest.facade().getDetailLayoutIDForUsage(new UsageCriteria(entityUID, null, null, Rest.facade().getCustomUsage()), false);
				}
				tblprefManager = prefsProv.getTablePreferencesManagerForSubformEntity(dependenceUID, layoutUID,
						userName, mandatorUID);
			} else {
				tblprefManager = prefsProv.getTablePreferencesManagerForEntity(entityUID,
						userName, mandatorUID);
			}

			if (tblprefManager != null) {
				
				TablePreferences tp = tblprefManager.getSelected();

				if (tp != null) {
					
					columnSortings = tp.getColumnSortings();
					
					for(ColumnPreferences cp : tp.getSelectedColumnPreferences()) {
						sortedColumns.add(cp.getColumn());
						columnWidths.add(cp.getWidth());
					}
				}
				
				// if no layout is available (e.g. layout was not saved in rich client workspace) create one
				if (sortedColumns.size() == 0) {
					
					Collection<FieldMeta<?>> entityFields = Rest.getAllEntityFieldsByEntityIncludingVersion(dependenceUID != null ? dependenceUID : entityUID).values();
					List<FieldMeta<?>> fields = new ArrayList<FieldMeta<?>>(entityFields);
					Collections.sort(fields);

					for(FieldMeta<?> field : fields) {
						// don't add system fields
						if(
							!field.getUID().equals(SF.CREATEDBY.getUID(field.getEntity())) &&
							!field.getUID().equals(SF.CREATEDAT.getUID(field.getEntity())) &&
							!field.getUID().equals(SF.CHANGEDBY.getUID(field.getEntity())) &&
							!field.getUID().equals(SF.CHANGEDAT.getUID(field.getEntity())) &&
							!field.getUID().equals(SF.VERSION.getUID(field.getEntity()))
						) {
							sortedColumns.add(field.getUID());
							int width = ProfileUtils.getMinimumColumnWidth(field.getJavaClass());
							columnWidths.add(width);
						}
					}
				}
				
			}
		} catch (Exception e) {
			LOG.error("Unable to get list layout from preferences.", e);
		}

		return new TableViewLayout(sortedColumns, columnWidths, columnSortings);
	}	
	
		
	public void resetTableViewLayout(String entityFQN,  String dependenceFQN) {
		
		UID entityUID = Rest.translateFqn(E.ENTITY, entityFQN);
		UID dependenceUID = dependenceFQN != null ? Rest.translateFqn(E.ENTITY, dependenceFQN) : null;

		try {
			final String userName = Rest.facade().getCurrentUserName();
			final UID mandatorUID = Rest.facade().getCurrentMandatorUID();
			final TablePreferencesManager tblprefManager;
			if (dependenceUID != null) {
				tblprefManager = prefsProv.getTablePreferencesManagerForSubformEntity(dependenceUID, null,
						userName, mandatorUID);
			} else {
				tblprefManager = prefsProv.getTablePreferencesManagerForEntity(dependenceUID,
						userName, mandatorUID);
			}

			if (tblprefManager != null) {
				TablePreferences tp = tblprefManager.getSelected();
				tblprefManager.reset(tp);
			}

		} catch (Exception e) {
			LOG.error("Unable to store table layout in prefs.", e);
		}
	}	
	
	public void storeTableViewLayout(String entityFQN,  String dependenceFQN, JsonObject data) {
		

		List<ColumnSortingPreferences> columnSortings = new ArrayList<ColumnSortingPreferences>();
		
		List<ColumnPreferences> columns = new ArrayList<ColumnPreferences>();
		JsonArray jsonColumns = data.getJsonArray("columns");
		for (JsonValue jsonValue : jsonColumns) {
			JsonObject jsonObject = (JsonObject)jsonValue;
			
			if (jsonObject.get("fieldName") != null) {

				ColumnPreferences columnPreferences = new ColumnPreferences();

				UID columnUid = Rest.translateFqn(E.ENTITYFIELD, (dependenceFQN != null ? dependenceFQN : entityFQN) + "_" + jsonObject.getString("fieldName"));
				columnPreferences.setColumn(columnUid);
				
				columnPreferences.setWidth(jsonObject.getInt("width"));
				
				if (jsonObject.get("sort") != null) {
					JsonObject sort = jsonObject.getJsonObject("sort");
					if (!sort.isEmpty()) {
						boolean isAscending = sort.getString("direction").equals("asc"); 
						int priority = sort.getInt("priority"); // TODO
						ColumnSortingPreferences columnSorting = new ColumnSortingPreferences();
						columnSorting.setAsc(isAscending);
						columnSorting.setColumn(columnUid);
						columnSortings.add(columnSorting);
					}
				}
				
				columns.add(columnPreferences);
			}
		}

		
		UID entityUID = Rest.translateFqn(E.ENTITY, entityFQN);
		UID dependenceUID = dependenceFQN != null ? Rest.translateFqn(E.ENTITY, dependenceFQN) : null;

		try {
			final String userName = Rest.facade().getCurrentUserName();
			final UID mandatorUID = Rest.facade().getCurrentMandatorUID();
			final TablePreferencesManager tblprefManager;
			if (dependenceUID != null) {
				tblprefManager = prefsProv.getTablePreferencesManagerForSubformEntity(dependenceUID, null,
						userName, mandatorUID);
			} else {
				tblprefManager = prefsProv.getTablePreferencesManagerForEntity(dependenceUID,
						userName, mandatorUID);
			}

			if (tblprefManager != null) {

				TablePreferences tp = tblprefManager.getSelected();

				List<ColumnPreferences> orderedColumnPreferences = new ArrayList<ColumnPreferences>(); 
				for (ColumnPreferences c : columns) {
					if (tp != null && tp.getSelectedColumnPreferences() != null) {
						for (ColumnPreferences cp : tp.getSelectedColumnPreferences()) {
							
							if (cp.getColumn().equals(c.getColumn())) {
								cp.setWidth(c.getWidth());
								orderedColumnPreferences.add(cp);
							}
						}
					}
				}

				if (orderedColumnPreferences.size() == 0) {
					// no column definition found in workspace - add them
					orderedColumnPreferences.addAll(columns);
				}
				if (tp != null) {
					tp.removeAllSelectedColumnPreferences();
					tp.addAllSelectedColumnPreferences(orderedColumnPreferences);
					
					tp.removeAllColumnSortings();
					tp.addAllColumnSortings(columnSortings);

					tblprefManager.update(tp);
				}
			}
			
		} catch (Exception e) {
			LOG.error("Unable to store table layout in prefs.", e);
		}
	}
	
}
