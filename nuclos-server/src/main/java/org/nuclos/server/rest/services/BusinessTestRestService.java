//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.nuclos.common.UID;
import org.nuclos.common.businesstest.BusinessTestOverallResult;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.server.businesstest.BusinessTestLogger;
import org.nuclos.server.businesstest.BusinessTestManagementBean;
import org.nuclos.server.businesstest.codegeneration.BusinessTestScriptGeneratorBean;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptWriterBean;
import org.nuclos.server.businesstest.execution.BusinessTestExecutorBean;
import org.nuclos.server.rest.CrossOriginResourceSharingFilter;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.WebContext;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/businesstests")
public class BusinessTestRestService extends WebContext {

	private static final Logger LOG = LogManager.getLogger(BusinessTestRestService.class);

	@Autowired
	private BusinessTestExecutorBean executorBean;

	@Autowired
	private BusinessTestManagementBean managementBean;

	@Autowired
	private BusinessTestScriptGeneratorBean generatorBean;

	@Autowired
	private BusinessTestScriptWriterBean scriptWriter;

	private void addDefaultResponseHeaders(final HttpServletRequest request, final HttpServletResponse response) {
		response.setContentType("application/json");
		response.setHeader("Connection", "keep-alive");

		CrossOriginResourceSharingFilter.setCORSHeadersForResponse(request, response);
	}

	@GET
	@Path("/")
	@RestServiceInfo(description = "Lists all available tests")
	public List<BusinessTestVO> listTests() {
		checkSuperUser();

		return managementBean.getAll();
	}

	@POST
	@Path("/")
	@RestServiceInfo(description = "Create a new test")
	@Consumes({MediaType.APPLICATION_JSON})
	public BusinessTestVO createTest(BusinessTestVO test) {
		checkSuperUser();

		try {
			managementBean.create(test);
			scriptWriter.writeScriptIncremental(test);
		} catch (Exception ex) {
			throw new NuclosWebException(ex, null);
		}

		return test;
	}

	@DELETE
	@Path("/")
	@RestServiceInfo(description = "Deletes all business tests")
	@Consumes({MediaType.APPLICATION_JSON})
	public void deleteAllTests() {
		checkSuperUser();

		try {
			managementBean.deleteAll();
			scriptWriter.deleteScripts();
		} catch (Exception ex) {
			throw new NuclosWebException(ex, null);
		}
	}

	@GET
	@Path("/{testId}")
	@RestServiceInfo(description = "Get one test by ID")
	public BusinessTestVO getTest(@PathParam("testId") String testId) {
		checkSuperUser();

		BusinessTestVO businessTestVO = managementBean.get(new UID(testId));

		if (businessTestVO == null) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}

		return businessTestVO;
	}

	@PUT
	@Path("/{testId}")
	@RestServiceInfo(description = "Update one test by ID")
	@Consumes({MediaType.APPLICATION_JSON})
	public BusinessTestVO putTest(@PathParam("testId") String testId, BusinessTestVO test) {
		checkSuperUser();

		try {
			managementBean.modify(test);
			scriptWriter.writeScriptIncremental(test);
		} catch (Exception ex) {
			throw new NuclosWebException(ex, null);
		}
		return test;
	}

	@DELETE
	@Path("/{testId}")
	@RestServiceInfo(description = "Delete one test by ID")
	public Response deleteTest(@PathParam("testId") String testId) {
		checkSuperUser();

		try {
			BusinessTestVO test = managementBean.get(new UID(testId));
			managementBean.delete(test);
			scriptWriter.deleteScript(test);
		} catch (Exception e) {
			throw new NuclosWebException(e, null);
		}

		return Response.ok().build();
	}

	@POST
	@Path("/generate")
	@RestServiceInfo(description = "Creates basic test cases")
	public void generateTests(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response
	) throws IOException {
		checkSuperUser();

		try (final BusinessTestLogger logger = initLogger(request, response)) {
			generatorBean.generateAllTests(logger);
		}
	}

	@POST
	@Path("/run")
	@RestServiceInfo(description = "Starts all tests immediately")
	public void runTests(
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response
	) throws IOException {
		checkSuperUser();

		try (final BusinessTestLogger logger = initLogger(request, response)) {
			executorBean.runAllTests(logger);
		}
	}

	@POST
	@Path("/run/{testId}")
	@RestServiceInfo(description = "Starts a single test immediately")
	public void runTest(
			@PathParam("testId") final String testId,
			@Context final HttpServletRequest request,
			@Context final HttpServletResponse response
	) throws IOException {
		checkSuperUser();

		try (final BusinessTestLogger logger = initLogger(request, response)) {
			executorBean.runTest(new UID(testId), logger);
		}
	}

	@GET
	@Path("/status")
	@RestServiceInfo(description = "Returns the overall test results")
	@Produces({MediaType.APPLICATION_JSON})
	public BusinessTestOverallResult overall() {
		checkSuperUser();

		return managementBean.getOverallResult();
	}

	@GET
	@Path("/status/simple")
	@RestServiceInfo(description = "Returns the overall test status")
	public String status() {
		checkSuperUser();

		BusinessTestVO.STATE state = managementBean.getOverallResult().getState();
		return state == null ? "NULL" : state.name();
	}

	/**
	 * Creates a new BusinessTestLogWriter and inits the response streaming.
	 */
	private BusinessTestLogger initLogger(
			final HttpServletRequest request,
			final HttpServletResponse response
	) throws IOException {
		final BusinessTestLogWriter writer = new BusinessTestLogWriter(response);

		response.setStatus(Status.OK.getStatusCode());
		addDefaultResponseHeaders(request, response);
		response.flushBuffer();

		return new BusinessTestLogger(writer);
	}

	/**
	 * A Writer that writes to and flushes the HttpServletResponse.
	 */
	private class FlushingResponseWriter extends Writer {
		private final HttpServletResponse response;
		private final Writer writer;

		FlushingResponseWriter(final HttpServletResponse response) throws IOException {
			this.response = response;
			this.writer = response.getWriter();
		}

		@Override
		public void write(char[] cbuf, int off, int len) throws IOException {
			writer.write(cbuf, off, len);
		}

		@Override
		public void flush() throws IOException {
			writer.flush();
			response.flushBuffer();
		}

		@Override
		public void close() throws IOException {
			writer.close();
		}
	}

	/**
	 * A {@link FlushingResponseWriter} that writes logs to the client during the execution of business tests.
	 */
	public class BusinessTestLogWriter extends FlushingResponseWriter {
		private boolean responseStarted = false;
		private int messageCount = 0;

		BusinessTestLogWriter(HttpServletResponse response) throws IOException {
			super(response);
		}

		/**
		 * Writes the opening JSON part.
		 */
		private void startResponse() {
			try {
				write("[\n");
				responseStarted = true;
			} catch (IOException e) {
				LOG.warn("Could not write response", e);
			}
		}

		/**
		 * Writes the ending JSON part.
		 */
		private void endResponse() {
			if (!responseStarted) {
				return;
			}

			try {
				write("]");
				responseStarted = false;
			} catch (IOException e) {
				LOG.warn("Could not write response", e);
			}
		}

		@Override
		public void close() throws IOException {
			endResponse();
			super.close();
		}

		/**
		 * Prints the current progress.
		 *
		 * @param progress, decimal value between 0 and 1.
		 */
		public void printProgress(final BigDecimal progress) {
			JSONObject object = new JSONObject();
			object.put("progress", "" + progress);
			doPrint(object);
		}

		public void println(final String message) {
			print(message + "\n");
		}

		/**
		 * Writes the given message.
		 */
		public void print(final String message) {
			LOG.info(message);
			JSONObject jsonMessage = new JSONObject();
			jsonMessage.put("message", message);
			doPrint(jsonMessage);
		}

		private void doPrint(final JSONObject object) {
			if (!responseStarted) {
				startResponse();
			}
			try {
				if (messageCount > 0) {
					write(",\n");
				}
				write("\t" + object.toString());
				messageCount++;
				flush();
			} catch (IOException e) {
				LOG.warn("Could not print message", e);
			}
		}

		/**
		 * Writes the given message and the Stacktrace of the given Throwable.
		 */
		public void logError(final String message, final Throwable t) {
			println("[ERROR]   " + message);
			if (t != null) {
				println(ExceptionUtils.getFullStackTrace(t));
			}
		}
	}
}
