package org.nuclos.server.rest.services.rvo;

import org.nuclos.common.UID;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class RoleRVO {
	private final UID id;
	private final String name;

	public RoleRVO(final UID id, final String name) {
		this.id = id;
		this.name = name;
	}

	public UID getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
