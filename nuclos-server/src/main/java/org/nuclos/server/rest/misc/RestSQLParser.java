package org.nuclos.server.rest.misc;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithOtherField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.rest.ejb3.Rest;

import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.InverseExpression;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.Union;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;

public class RestSQLParser implements ExpressionVisitor, StatementVisitor,
		SelectVisitor, FromItemVisitor, SelectItemVisitor, ItemsListVisitor {

	private static final String INTID_SUFFIX = ".id";

	public CollectableSearchCondition getCondition() {
		return condition;
	}

	private CollectableSearchCondition condition;

	private FieldMeta<?> fieldColumn;

	private EntityMeta<?> entityTable;

	private Object value;

	@Override
	public void visit(AndExpression andExpression) {
		RestSQLParser leftParser = new RestSQLParser();
		andExpression.getLeftExpression().accept(leftParser);
		RestSQLParser rightParser = new RestSQLParser();
		andExpression.getRightExpression().accept(rightParser);
		if (leftParser.condition != null && rightParser.condition != null) {
			condition = SearchConditionUtils.and(leftParser.condition, rightParser.condition);
		} else if (leftParser.condition != null) {
			condition = leftParser.condition;
		} else if (rightParser.condition != null) {
			condition = rightParser.condition;
		}
	}

	@Override
	public void visit(OrExpression orExpression) {
		RestSQLParser leftParser = new RestSQLParser();
		orExpression.getLeftExpression().accept(leftParser);
		RestSQLParser rightParser = new RestSQLParser();
		orExpression.getRightExpression().accept(rightParser);
		if (leftParser.condition != null && rightParser.condition != null) {
			condition = SearchConditionUtils.or(leftParser.condition, rightParser.condition);
		} else if (leftParser.condition != null) {
			condition = leftParser.condition;
		} else if (rightParser.condition != null) {
			condition = rightParser.condition;
		}
	}

	@Override
	public void visit(InExpression inExpression) {
		RestSQLParser leftParser = new RestSQLParser();
		inExpression.getLeftExpression().accept(leftParser);

		RestSQLParser inParser = new RestSQLParser();
		// for date values:
		inParser.fieldColumn = leftParser.fieldColumn;
		inExpression.getItemsList().accept(inParser);

		if (leftParser.fieldColumn != null) {
			if (inParser.value instanceof List) {
				List inComparands = (List) inParser.value;
				// NUCLOS-6809: On reference-fields do an "InId-Comparison"
				if (leftParser.fieldColumn.hasAnyForeignEntity()) {
					condition = new CollectableInIdCondition<>(SearchConditionUtils.newEntityField(leftParser.fieldColumn), inComparands);
				} else {
					condition = new CollectableInCondition(SearchConditionUtils.newEntityField(leftParser.fieldColumn), inComparands);
				}

			} else if (inParser.condition != null) {
				if (inParser.condition instanceof AtomicCollectableSearchCondition) {
					CollectableEntityField entityField = ((AtomicCollectableSearchCondition) inParser.condition).getEntityField();
					if (!inParser.fieldColumn.getEntity().equals(entityField.getEntityUID())) {
						throw new NuclosFatalException("Invalid IN-condition: field entity does not match column entity");
					}
				}

				if (SF.PK_ID.checkField(leftParser.fieldColumn.getEntity(), leftParser.fieldColumn.getUID())) {
					// special handling for .INTID IN (..)
					condition = new CollectableSubCondition(
							inParser.fieldColumn.getEntity(),
							inParser.fieldColumn.getUID(),
							inParser.condition);
				} else {
					condition = new CollectableSubCondition(
							leftParser.fieldColumn.getUID(),
							inParser.fieldColumn.getEntity(),
							inParser.fieldColumn.getUID(),
							inParser.condition);
				}
			}
		}

		if (inExpression.isNot() && condition != null) {
			condition = SearchConditionUtils.not(condition);
		}
	}

	public void visit(SubSelect subSelect) {
		subSelect.getSelectBody().accept(this);
	}

	public void visit(ExpressionList expressionList) {
		List items = new ArrayList();
		for (Object object : expressionList.getExpressions()) {
			if (object instanceof Expression) {
				RestSQLParser itemParser = new RestSQLParser();
				itemParser.fieldColumn = fieldColumn;
				((Expression) object).accept(itemParser);
				if (itemParser.value != null) {
					items.add(itemParser.value);
				}
			}
		}
		value = items;
	}

	@Override
	public void visit(ExistsExpression existsExpression) {
	}

	@Override
	public void visit(Parenthesis parenthesis) {
		RestSQLParser expParser = new RestSQLParser();
		parenthesis.getExpression().accept(expParser);
		if (expParser.condition != null) {
			if (parenthesis.isNot()) {
				condition = SearchConditionUtils.not(expParser.condition);
			} else {
				condition = expParser.condition;
			}
		}
	}

	@Override
	public void visit(Column tableColumn) {
		if (tableColumn.getTable() != null && tableColumn.getWholeColumnName().endsWith(INTID_SUFFIX)) {
			String entityFqn = tableColumn.getTable().getName();
			UID entityUid = Rest.translateFqn(E.ENTITY, entityFqn);
			fieldColumn = Rest.getEntity(entityUid).isUidEntity() ? SF.PK_UID.getMetaData(entityUid) : SF.PK_ID.getMetaData(entityUid);
		} else {
			UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, tableColumn.getColumnName());
			fieldColumn = Rest.getEntityField(fieldUid);
		}
	}

	private static class BinaryParser {

		FieldMeta<?> fieldLeft;
		FieldMeta<?> fieldRight;
		Object valueLeft;
		Object valueRight;
		CollectableSearchCondition conditionLeft;
		CollectableSearchCondition conditionRight;

		private BinaryParser parse(BinaryExpression exp) {
			RestSQLParser leftParser = new RestSQLParser();
			exp.getLeftExpression().accept(leftParser);
			RestSQLParser rightParser = new RestSQLParser();
			exp.getRightExpression().accept(rightParser);

			conditionLeft = leftParser.condition;
			conditionRight = rightParser.condition;

			if (leftParser.fieldColumn != null) {
				fieldLeft = leftParser.fieldColumn;
			} else {
				// for date values:
				leftParser.fieldColumn = rightParser.fieldColumn;
				exp.getRightExpression().accept(leftParser);
				valueLeft = leftParser.value;
			}
			if (rightParser.fieldColumn != null) {
				fieldRight = rightParser.fieldColumn;
			} else {
				// for date values:
				rightParser.fieldColumn = leftParser.fieldColumn;
				exp.getRightExpression().accept(rightParser);
				valueRight = rightParser.value;
			}
			return this;
		}

	}

	private void setFieldComparison(BinaryExpression exp, ComparisonOperator op) {
		BinaryParser p = new BinaryParser().parse(exp);

		if (p.fieldLeft != null && p.fieldRight != null) {
			if (p.fieldLeft.getForeignEntity() != null) {
				EntityMeta<?> foreignEntityMeta = MetaProvider.getInstance().getEntity(p.fieldLeft.getForeignEntity());
				condition = new CollectableSubCondition(
						p.fieldLeft.getUID(),
						foreignEntityMeta.getUID(),
						foreignEntityMeta.getPk().getUID(foreignEntityMeta),
						p.conditionRight
				);
			} else {
				condition = new CollectableComparisonWithOtherField(
						SearchConditionUtils.newEntityField(p.fieldLeft),
						op,
						SearchConditionUtils.newEntityField(p.fieldRight));
			}
		} else if (p.fieldLeft != null && p.valueRight != null) {
			if (op == ComparisonOperator.LIKE) {
				condition = SearchConditionUtils.newLikeCondition(p.fieldLeft, (String) p.valueRight);
			} else {
				if (p.fieldLeft.getForeignEntity() != null) {
					EntityMeta<Object> foreignEntityMeta = MetaProvider.getInstance().getEntity(p.fieldLeft.getForeignEntity());
					if (foreignEntityMeta.isUidEntity()) {
						UID refUid = Rest.translateFqn(foreignEntityMeta, p.valueRight.toString());
						condition = SearchConditionUtils.newUidComparison(p.fieldLeft.getUID(), op, refUid);
					} else if (p.valueRight instanceof Long) {
						condition = SearchConditionUtils.newIdComparison(p.fieldLeft.getUID(), op, (Long) p.valueRight);
					}
				} else {
					condition = SearchConditionUtils.newComparison(p.fieldLeft, op, p.valueRight);
				}
			}
		} else if (p.fieldRight != null && p.valueLeft != null) {
			if (op == ComparisonOperator.LIKE) {
				condition = SearchConditionUtils.newLikeCondition(p.fieldRight, (String) p.valueLeft);
			} else {
				if (p.fieldRight.getForeignEntity() != null) {
					EntityMeta<Object> foreignEntityMeta = MetaProvider.getInstance().getEntity(p.fieldRight.getForeignEntity());
					if (foreignEntityMeta.isUidEntity()) {
						UID refUid = Rest.translateFqn(foreignEntityMeta, p.valueLeft.toString());
						condition = SearchConditionUtils.newUidComparison(p.fieldRight.getUID(), op, refUid);
					} else if (p.valueLeft instanceof Long) {
						condition = SearchConditionUtils.newIdComparison(p.fieldRight.getUID(), op, (Long) p.valueLeft);
					}
				} else {
					condition = SearchConditionUtils.newComparison(p.fieldRight, op, p.valueLeft);
				}
			}
		} else if (p.fieldLeft != null && p.valueRight == null) {
			condition = SearchConditionUtils.newIsNullCondition(p.fieldLeft);
		} else if (p.fieldRight != null && p.valueLeft == null) {
			condition = SearchConditionUtils.newIsNullCondition(p.fieldRight);
		}

		if (exp.isNot() && condition != null) {
			condition = SearchConditionUtils.not(condition);
		}
	}

	@Override
	public void visit(EqualsTo equalsTo) {
		setFieldComparison(equalsTo, ComparisonOperator.EQUAL);
	}

	@Override
	public void visit(NotEqualsTo notEqualsTo) {
		setFieldComparison(notEqualsTo, ComparisonOperator.NOT_EQUAL);
	}

	@Override
	public void visit(LikeExpression likeExpression) {
		setFieldComparison(likeExpression, ComparisonOperator.LIKE);
	}

	@Override
	public void visit(GreaterThan greaterThan) {
		setFieldComparison(greaterThan, ComparisonOperator.GREATER);
	}

	@Override
	public void visit(GreaterThanEquals greaterThanEquals) {
		setFieldComparison(greaterThanEquals, ComparisonOperator.GREATER_OR_EQUAL);
	}

	@Override
	public void visit(MinorThan minorThan) {
		setFieldComparison(minorThan, ComparisonOperator.LESS);
	}

	@Override
	public void visit(MinorThanEquals minorThanEquals) {
		setFieldComparison(minorThanEquals, ComparisonOperator.LESS_OR_EQUAL);
	}

	@Override
	public void visit(IsNullExpression isNullExpression) {
		RestSQLParser p = new RestSQLParser();
		isNullExpression.getLeftExpression().accept(p);

		if (p.fieldColumn != null) {
			condition = SearchConditionUtils.newIsNullCondition(p.fieldColumn);
		}

		if (isNullExpression.isNot() && condition != null) {
			condition = SearchConditionUtils.not(condition);
		}
	}

	@Override
	public void visit(StringValue stringValue) {
		value = stringValue.getValue();

		if (fieldColumn == null || value == null) {
			return;
		}

		if (java.util.Date.class.isAssignableFrom(fieldColumn.getJavaClass())) {
			// If this is a Timestamp attribute, try to parse the given value as a Timestamp first
			if (Timestamp.class.isAssignableFrom(fieldColumn.getJavaClass())) {
				try {
					value = Timestamp.from(OffsetDateTime.parse(stringValue.getValue()).toInstant());
					return;
				} catch (DateTimeException e) {
					// Ignore, maybe the given value was just a simple Date
				}
			}

			try {
				value = new SimpleDateFormat("yyyy-MM-dd").parse(stringValue.getValue());
			} catch (ParseException e) {
				throw new NuclosFatalException("Date value " + stringValue.getValue() + " is not parseable with yyyy-MM-dd: " + e.getMessage());
			}
		} else if (java.lang.Boolean.class.equals(fieldColumn.getJavaClass())) {
			value = Boolean.parseBoolean(stringValue.getValue());
		}
	}

	@Override
	public void visit(DoubleValue doubleValue) {
		value = doubleValue.getValue();
	}

	@Override
	public void visit(LongValue longValue) {
		value = longValue.getValue();
	}

	public void visit(NullValue nullValue) {
	}

	public void visit(Function function) {
	}

	public void visit(InverseExpression inverseExpression) {
	}

	public void visit(JdbcParameter jdbcParameter) {
	}

	public void visit(DateValue dateValue) {
	}

	public void visit(TimeValue timeValue) {
	}

	public void visit(TimestampValue timestampValue) {
	}

	public void visit(Addition addition) {
	}

	public void visit(Division division) {
	}

	public void visit(Multiplication multiplication) {
	}

	public void visit(Subtraction subtraction) {
	}

	public void visit(Between between) {
	}

	public void visit(CaseExpression caseExpression) {
	}

	public void visit(WhenClause whenClause) {
	}

	public void visit(AllComparisonExpression allComparisonExpression) {
	}

	public void visit(AnyComparisonExpression anyComparisonExpression) {
	}

	public void visit(Concat concat) {
	}

	public void visit(Matches matches) {
	}

	public void visit(BitwiseAnd bitwiseAnd) {
	}

	public void visit(BitwiseOr bitwiseOr) {
	}

	public void visit(BitwiseXor bitwiseXor) {
	}

	public void visit(Table tableName) {
		String entityFqn = tableName.getName();
		UID entityUid = Rest.translateFqn(E.ENTITY, entityFqn);
		entityTable = Rest.getEntity(entityUid);
	}

	public void visit(SubJoin subjoin) {
	}

	private void visitSelectItems(List selectItems) {
		if (selectItems.size() > 0) {
			SelectItem select = (SelectItem) selectItems.get(0);
			select.accept(this);
		}
	}

	@Override
	public void visit(PlainSelect plainSelect) {
		plainSelect.getWhere().accept(this);
		FromItem from = plainSelect.getFromItem();
		from.accept(this);
		visitSelectItems(plainSelect.getSelectItems());
	}

	public void visit(Union union) {
	}

	public void visit(AllColumns allColumns) {
	}

	public void visit(AllTableColumns allTableColumns) {
	}

	public void visit(SelectExpressionItem selectExpressionItem) {
		selectExpressionItem.getExpression().accept(this);
	}

	@Override
	public void visit(Select select) {
		select.getSelectBody().accept(this);
	}

	public void visit(CreateTable createTable) {
	}

	public void visit(Truncate truncate) {
	}

	public void visit(Drop drop) {
	}

	public void visit(Replace replace) {
	}

	public void visit(Insert insert) {
	}

	public void visit(Update update) {
	}

	public void visit(Delete delete) {
	}
}
