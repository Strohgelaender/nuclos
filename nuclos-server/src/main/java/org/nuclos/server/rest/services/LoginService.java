package org.nuclos.server.rest.services;

import java.util.List;
import java.util.Locale;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.schema.rest.LegalDisclaimer;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.LoginServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;

@Path("/")
public class LoginService extends LoginServiceHelper {	
	@GET
	@RestServiceInfo(identifier="login", description="Returns JsonObject with SessionID and link for BO-Metalist.")
	@Consumes({MediaType.APPLICATION_JSON})
	public JsonObject login(@Context HttpServletRequest request) {
		return getLoginInfo().build();
	}

	@POST
	@RestServiceInfo(identifier="login", validateSession=false, isFinalized=true, description="User-Login. Returns JsonObject with SessionID and link for BO-Metalist.", examplePostData = "{\"username\": \"nuclos\", \"password\": \"\"}")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response login(JsonObject data, @Context HttpServletRequest request) {
		LoginResult result = doLogin(data, request);
		return Response.ok(result.getResponseJSON().build(), MediaType.APPLICATION_JSON).cookie(result.getCookies()).build();
	}

	@GET
	@Path("locale")
	@RestServiceInfo(identifier="getLocale", description="Returns the server-side locale for the current session.")
	@Produces({MediaType.APPLICATION_JSON})
	public JsonObject doGetLocale() {
		Locale locale = getLocale();

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("locale", locale.toString());
		return json.build();
	}

	@PUT
	@Path("locale")
	@RestServiceInfo(identifier="setLocale", description="Sets the server-side locale for the current session.", examplePostData = "{\"locale\": \"de\"}")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response setLocale(
			JsonObject data
	) {
		String localeString = data.getString("locale");
		setLocale(localeString);
		return Response.ok().build();
	}

	@POST
	@Path("chooseMandator/{mandatorId}")
	@RestServiceInfo(identifier="chooseMandator", isFinalized=true, description="Choose the mandator after a user logs in. The Result of the login contains a list of possible mandators to choose.")
	public Response chooseMandator(@PathParam("mandatorId") String mandatorId) {
		return doChooseMandator(mandatorId);
	}
	
	//FIXME: HATEOAS Standard dictates that @DELETE-Method must delete persistent object on the DB. See also BoService @Path("/{boMetaId}/query")
	@DELETE
	@RestServiceInfo(identifier="logout", isFinalized=true, description="User-Logout. Returns HTTP-Status 200, if successful")
	public Response logout(@Context HttpServletRequest request) {
		return doLogout(request);
	}
	
	@GET
	@Path("serverstatus")
	@RestServiceInfo(identifier="serverstatus", validateSession=false, isFinalized=true, description="Status of the nuclos server")
	public JsonObject serverstatus() {
		boolean ready = SpringApplicationContextHolder.isNuclosReady();

		if (!ready) {
			throw new NuclosWebException(Response.Status.SERVICE_UNAVAILABLE);
		}

		JsonObjectBuilder json = Json.createObjectBuilder();
		json.add("ready", ready);
		return json.build();
	}

	@GET
	@Path("version")
	@RestServiceInfo(identifier="version", validateSession=false, isFinalized=true, description="Version of the nuclos server")
	public String version() {
		return getVersion();
	}

	@GET
	@Path("version/db")
	@RestServiceInfo(identifier="dbversion", validateSession=false, isFinalized=true, description="Version of DB schema of the nuclos server. Actually accesses the DB.")
	public String dbversion() {
		return getDbVersion();
	}

	@GET
	@Path("version/legaldisclaimer")
	@Produces(MediaType.APPLICATION_JSON)
	@RestServiceInfo(identifier = "legaldisclaimer", validateSession = false, description = "Legal disclaimer about Nuclos and this application.")
	public List<LegalDisclaimer> getLegalDisclaimers() {
		return super.getLegalDisclaimers();
	}
}