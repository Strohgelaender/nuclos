package org.nuclos.server.rest.ejb3;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;

public class BoGenerationResult {

	private final GeneratorActionVO generatorAction;
	private final EntityObjectVO<?> generatedEo;
	private final String businessError;
	
	public BoGenerationResult(GeneratorActionVO generatorAction,
			EntityObjectVO<?> generatedEo, String businessError) {
		super();
		this.generatorAction = generatorAction;
		this.generatedEo = generatedEo;
		this.businessError = businessError;
	}

	public GeneratorActionVO getGeneratorAction() {
		return generatorAction;
	}

	public EntityObjectVO<?> getGeneratedEo() {
		return generatedEo;
	}

	public String getBusinessError() {
		return businessError;
	}
	
}
