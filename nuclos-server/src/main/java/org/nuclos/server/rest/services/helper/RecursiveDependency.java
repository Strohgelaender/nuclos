package org.nuclos.server.rest.services.helper;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class RecursiveDependency {
	private final RecursiveDependency parent;
	private final String referenceAttributeFqn;
	private final String recordId;

	public static RecursiveDependency forRoot(final String recordId) {
		return new RecursiveDependency(
				null,
				null,
				recordId
		);
	}

	private RecursiveDependency(
			final RecursiveDependency parent,
			final String referenceAttributeFqn,
			final String recordId
	) {
		this.parent = parent;
		this.referenceAttributeFqn = referenceAttributeFqn;
		this.recordId = recordId;
	}

	public RecursiveDependency dependency(
			final String referenceAttributeFqn
	) {
		return dependency(referenceAttributeFqn, null);
	}

	public RecursiveDependency dependency(
			final String referenceAttributeFqn,
			final String recordId
	) {
		return new RecursiveDependency(
				this,
				referenceAttributeFqn,
				recordId
		);
	}

	public RecursiveDependency getParent() {
		return parent;
	}

	public String getReferenceAttributeFqn() {
		return referenceAttributeFqn;
	}

	String getRecordId() {
		return recordId;
	}

	String getRootRecordId() {
		if (parent != null) {
			return parent.getRootRecordId();
		}

		return recordId;
	}
}
