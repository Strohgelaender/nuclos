//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.ldap.ejb3;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVOWrapper;
import org.nuclos.server.security.NuclosLdapBindAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.control.PagedResultsCookie;
import org.springframework.ldap.control.PagedResultsDirContextProcessor;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.core.support.SingleContextSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for all LDAP data management functions.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class LDAPDataFacadeBean extends NuclosFacadeBean implements LDAPDataFacadeLocal, LDAPDataFacadeRemote {

	private final static int PAGESIZE = 1000;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	public LDAPDataFacadeBean() {
	}

	public MasterDataVO<UID> create(MasterDataVO<UID> vo, IDependentDataMap mpDependants) throws CommonBusinessException {
		checkWriteAllowed(E.LDAPSERVER);
		validate(vo, mpDependants);
		vo.setDependents(mpDependants);
		return masterDataFacade.create(vo, null);
	}

	public MasterDataVO<UID> modify(MasterDataVO<UID> vo, IDependentDataMap mpDependants) throws CommonBusinessException {
		checkWriteAllowed(E.LDAPSERVER);
		validate(vo, mpDependants);

		vo.setDependents(mpDependants);
		
		UID id = (UID) masterDataFacade.modify(vo, null);
		return masterDataFacade.get(E.LDAPSERVER.getUID(), id);
	}

	/**
	 * Find all LDAP users.
	 * @param filterExpr ldap filter expression, i.e. (sAMAccountName={0})
	 * @param filterArgs filter parameters, i.e. username
	 * @return a collection containing the search result for the given search expression.
	 * TODO restrict permissions
	 */
	@SuppressWarnings("deprecation")
	@RolesAllowed("Login")
	//TODO MULTINUCLET check the refactoring of this method
	public Collection<MasterDataVOWrapper<UID>> getUsers(String filterExpr, Object[] filterArgs) throws CommonBusinessException {
		final List<EntityObjectVO<UID>> servers = nucletDalProvider
.getEntityObjectProcessor(E.LDAPSERVER).getAll();

		final Collection<MasterDataVOWrapper<UID>> searchResult = new ArrayList<MasterDataVOWrapper<UID>>();

		for (final EntityObjectVO<UID> server : servers) {
			if (server.getFieldValue(E.LDAPSERVER.active) == null
					|| !server.getFieldValue(E.LDAPSERVER.active)
					|| StringUtils.isNullOrEmpty(server.getFieldValue(E.LDAPSERVER.serversearchfilter))) {
					continue;
			}
			String url = server.getFieldValue(E.LDAPSERVER.serverurl);
	    	String context = server.getFieldValue(E.LDAPSERVER.serversearchcontext);
	    	Integer searchscope = server.getFieldValue(E.LDAPSERVER.serversearchscope);
	    	String bindDN = server.getFieldValue(E.LDAPSERVER.binddn);
			String bindCredential = server.getFieldValue(E.LDAPSERVER.bindcredential);
			String filter = server.getFieldValue(E.LDAPSERVER.serversearchfilter);

			final LdapContextSource contextSource = new LdapContextSource();
			contextSource.setUrl(url);
			contextSource.setBase(context);
			if (!StringUtils.isNullOrEmpty(bindDN)) {
				contextSource.setUserDn(bindDN);
				contextSource.setPassword(bindCredential);
			}
			else {
				contextSource.setAnonymousReadOnly(true);
			}
			contextSource.setReferral("ignore");
			try {
				contextSource.afterPropertiesSet();
			}
			catch(Exception e) {
				throw new NuclosFatalException(e);
			}

			final Map<String, String> attributeMapping = new HashMap<String, String>();

			final CollectableSearchCondition attrcond = SearchConditionUtils.newUidComparison(E.LDAPMAPPING.ldapserver, ComparisonOperator.EQUAL, server.getPrimaryKey());
			final List<EntityObjectVO<UID>> mappings = nucletDalProvider
.getEntityObjectProcessor(E.LDAPMAPPING).getBySearchExpression(new CollectableSearchExpression(attrcond));

			if (mappings != null && mappings.size() > 0) {
				for (final EntityObjectVO<UID> mapping : mappings) {
					attributeMapping.put(mapping.getFieldValue(E.LDAPMAPPING.fieldNucleus), mapping.getFieldValue(E.LDAPMAPPING.fieldLDAP));
				}

				if (!StringUtils.isNullOrEmpty(filterExpr)) {
					filter = "(&" + filterExpr + filter + ")";
				}

				SearchControls constraints = new SearchControls();
				constraints.setSearchScope(searchscope);
				constraints.setReturningAttributes(attributeMapping.values().toArray(new String[attributeMapping.values().size()]));

				SingleContextSource singleContextSource = new SingleContextSource(contextSource.getReadOnlyContext());

				try {
					LdapTemplate template = new LdapTemplate(singleContextSource);
					template.setIgnorePartialResultException(true);

					PagedResultsCookie cookie;
					PagedResultsDirContextProcessor pagingProcessor = new PagedResultsDirContextProcessor(PAGESIZE);
					
					// Map fields by name (fieldname => UID)
					final Map<String, UID> mpFieldsByName = new HashMap<String, UID>();
					
					for (final FieldMeta<?> fieldMeta : E.USER.getFields()) {
						mpFieldsByName.put(fieldMeta.getFieldName(), fieldMeta.getUID());
					}
					
					do {
						template.search("", MessageFormat.format(filter, filterArgs), constraints, new ContextMapper() {
							@Override
							public Object mapFromContext(Object ctx) {
								DirContextOperations dirctx = (DirContextOperations) ctx;
								//final MasterDataVO result = new MasterDataVO(E.USER, null, 
										//null, "INITIAL", null, "INITIAL", new Integer(1), null);
								final MasterDataVO<UID> result = new MasterDataVO<UID>(E.USER.getUID(), null, null, "INITIAL", null, "INITIAl", new Integer(1), null, null, null,false);
								
								HashMap<UID, Object> wrapperFields = new HashMap<UID, Object>();
								wrapperFields.put(E.USER.superuser.getUID(), false);
								Iterator<String> keysIt = attributeMapping.keySet().iterator();
								Attributes attrs =  dirctx.getAttributes();
								String reqKey = null;
								while (keysIt.hasNext()) {
									reqKey = keysIt.next();
									Object value = null;
									try {
										if (attrs.get(attributeMapping.get(reqKey)) != null && attrs.get(attributeMapping.get(reqKey)).get(0) != null) {
											value = attrs.get(attributeMapping.get(reqKey)).get(0);
										}
									}
									catch (NamingException ex) {
										throw new NuclosFatalException(ex);
									}
									final UID reqKeyUid = mpFieldsByName.get(reqKey);
									wrapperFields.put(reqKeyUid, value);
									result.setFieldValue(reqKeyUid, value);
								}
								final MasterDataVOWrapper<UID> wrapper = new MasterDataVOWrapper<UID>(result, new ArrayList<UID>(wrapperFields.keySet()), wrapperFields);
								wrapper.setIsWrapped();
								searchResult.add(wrapper);
								return wrapper;
							}
						}, pagingProcessor);

						cookie = pagingProcessor.getCookie();
						pagingProcessor = new PagedResultsDirContextProcessor(PAGESIZE, cookie);
					} while (cookie != null && cookie.getCookie() != null);
				}
				finally {
					singleContextSource.destroy();
				}
			}
		}

		final Collection<MasterDataVO<UID>> nucleususers = ServerServiceLocator.getInstance().getFacade(
				MasterDataFacadeLocal.class).getMasterData(E.USER, null);

		final Map<String, MasterDataVOWrapper<UID>> map = new HashMap<String, MasterDataVOWrapper<UID>>();
		for (final MasterDataVOWrapper<UID> wrapper : searchResult) {
			map.put(wrapper.getFieldValue(E.USER.name).toLowerCase(), wrapper);
		}

		for (MasterDataVO<UID> nucleususer : nucleususers) {
			MasterDataVOWrapper<UID> wrapper;
			String key = nucleususer.getFieldValue(E.USER.name).toLowerCase();
			if (map.containsKey(key)) {
				wrapper = new MasterDataVOWrapper<UID>(nucleususer, new ArrayList<UID>(map.get(key).getMappedFields()));
				wrapper.setWrapperFields(map.get(key).getWrapperFields());
				wrapper.setIsMapped();
				map.remove(key);
			}
			else {
				wrapper = new MasterDataVOWrapper<UID>(nucleususer, new ArrayList<UID>());
				wrapper.setIsNative();
			}
			map.put(key, wrapper);
		}

		return new ArrayList<MasterDataVOWrapper<UID>>(map.values());
    }

	@RolesAllowed("Login")
	public boolean tryAuthentication(String ldapserver, String username, String password) throws CommonPermissionException {
		checkWriteAllowed(E.LDAPSERVER);

		final CollectableSearchCondition cond = SearchConditionUtils.newComparison(E.LDAPSERVER.servername, ComparisonOperator.EQUAL, ldapserver);
		final List<EntityObjectVO<UID>> servers = nucletDalProvider
				.getEntityObjectProcessor(E.LDAPSERVER).getBySearchExpression(new CollectableSearchExpression(cond));

		if (servers == null || servers.isEmpty()) {
			throw new NuclosFatalException("ldap.exception.servernotfound");
		}

		try {
			final EntityObjectVO<UID> server = servers.get(0);

			String url = server.getFieldValue(E.LDAPSERVER.serverurl);
			String baseDN = server.getFieldValue(E.LDAPSERVER.serversearchcontext);
			String bindDN = server.getFieldValue(E.LDAPSERVER.binddn);
			String bindCredential = server.getFieldValue(E.LDAPSERVER.bindcredential);
			String userSearchFilter = server.getFieldValue(E.LDAPSERVER.userfilter);
			Integer scope = server.getFieldValue(E.LDAPSERVER.serversearchscope);

			NuclosLdapBindAuthenticator authenticator = new NuclosLdapBindAuthenticator(url, baseDN, bindDN, bindCredential, userSearchFilter, scope);
			return authenticator.authenticate(new UsernamePasswordAuthenticationToken(username, password));

		} catch (org.springframework.ldap.NamingSecurityException nse) {
			throw new CommonPermissionException(nse.getMessage());
		} catch (org.springframework.ldap.NamingException ne) {
			throw new NuclosFatalException(ne.getMessage());
		}
	}

	private void validate(MasterDataVO<UID> vo, IDependentDataMap dependants) throws CommonValidationException {
		if (!StringUtils.isNullOrEmpty(vo.getFieldValue(E.LDAPSERVER.binddn))
			&& StringUtils.isNullOrEmpty(vo.getFieldValue(E.LDAPSERVER.bindcredential))) {
			throw new CommonValidationException("ldap.validation.password");
		}

		final Boolean active = vo.getFieldValue(E.LDAPSERVER.active);
		final String sync = vo.getFieldValue(E.LDAPSERVER.serversearchfilter);
		
		if (active != null && active && !StringUtils.isNullOrEmpty(sync)) {
			final List<String> nuclosfields = new ArrayList<String>(Arrays.asList("name", "firstname", "lastname"));
			for (final EntityObjectVO<UID> mdvo : dependants.<UID>getDataPk(E.LDAPMAPPING.ldapserver)) {
				String na = mdvo.getFieldValue(E.LDAPMAPPING.fieldNucleus);
				if (!StringUtils.isNullOrEmpty(na) && !mdvo.isFlagRemoved()) {
					nuclosfields.remove(na);
				}
			}
			if (nuclosfields.size() > 0) {
				throw new CommonValidationException("ldap.validation.mapping");
			}
		}
	}
}
