//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.common.statemodel.Statemodel;
import org.nuclos.common.statemodel.StatemodelClosure;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.NuclosUpdateException;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.common.StateCache.FindKey;
import org.nuclos.server.common.StateCache.IFindTransition;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.masterdata.valueobject.RoleTransitionVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.NuclosSubsequentStateNotLegalException;
import org.nuclos.server.statemodel.StatemodelObjectBuilder;
import org.nuclos.server.statemodel.valueobject.AttributegroupPermissionVO;
import org.nuclos.server.statemodel.valueobject.MandatoryColumnVO;
import org.nuclos.server.statemodel.valueobject.MandatoryFieldVO;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateHistoryVO;
import org.nuclos.server.statemodel.valueobject.StateModelLayout;
import org.nuclos.server.statemodel.valueobject.StateModelUsages;
import org.nuclos.server.statemodel.valueobject.StateModelUsagesCache;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.statemodel.valueobject.SubformGroupPermissionVO;
import org.nuclos.server.statemodel.valueobject.SubformPermissionVO;
import org.nuclos.server.validation.ValidationSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


/**
 * Facade bean for state management and state machine issues.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * §todo restrict
 */
@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("Login")
public class StateFacadeBean extends NuclosFacadeBean implements StateFacadeLocal, StateFacadeRemote, IFindTransition {

	private static final Logger LOG = LoggerFactory.getLogger(StateFacadeBean.class);

	@Autowired
	private LocaleFacadeLocal localeFacade;

	@Autowired
	private SessionUtils utils;

	@Autowired
	private StateCache stateCache;

	@Autowired
	private GenericObjectFacadeLocal genericObjectFacade;

	@Autowired
	private EventSupportFacadeLocal eventSupportFacade;
	
	@Autowired
	private ValidationSupport validationSupport;

	@Autowired
	private StatemodelObjectBuilder statemodelObjectBuilder;
		
	@Autowired
	private EventSupportCache esCache;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	@Autowired
	private MasterDataFacadeLocal _masterDataFacade;
	
	@Autowired
	private GenericObjectFacadeLocal _genericObjectFacadeLocal;
	
	@Autowired
	private AnnotationJaxb2Marshaller jaxb2Marshaller;
	
	public StateFacadeBean() {
	}
	
	private final MasterDataFacadeLocal getMasterDataFacade() {
		/*
		if (_masterDataFacade == null) {
			_masterDataFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
		}
		 */
		return _masterDataFacade;
	}

	private final GenericObjectFacadeLocal getGenericObjectFacade() {
		return _genericObjectFacadeLocal;
	}


	/**
	 * gets a complete state graph for a state model
	 * <p>
	 * TODO: cache this (tp).
	 * <p>
	 * @param stateModelUid UID of state model to get graph for
	 * @return state graph cvo containing the state graph information for the model with the given id
	 * @throws CommonPermissionException
	 */
	public StateGraphVO getStateGraph(final UID stateModelUid) throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		checkReadAllowed(E.STATEMODEL);
		StateGraphVO result;

		//get state model
		final StateModelVO stateModel = MasterDataWrapper.getStateModelVO(getMasterDataFacade().get(E.STATEMODEL, stateModelUid), null);
		result = new StateGraphVO(stateModel);
		
		//get states (with attributegroup permissions) and transitions for state model
		Set<StateVO> ststatevo = new HashSet<StateVO>();
		Set<StateTransitionVO> sttransitionvo = new HashSet<StateTransitionVO>();
		for (final StateVO statevo : stateCache.getStatesByModel(stateModelUid)) {
			StateVO.UserRights userRights = new StateVO.UserRights();
			for (MasterDataVO<UID> mdPermission : findAttributegroupPermissionsByState(statevo.getPrimaryKey())) {
				AttributegroupPermissionVO permission = MasterDataWrapper.getAttributegroupPermissionVO(mdPermission);
				userRights.addValue(permission.getRoleUID(), permission);
			}
			statevo.setUserRights(userRights);

			StateVO.UserSubformRights userSubformRights = new StateVO.UserSubformRights();
			for (MasterDataVO<UID> mdPermission : findSubformPermissionsByState(statevo.getPrimaryKey())) {
				SubformPermissionVO permission = MasterDataWrapper.getSubformPermissionVO(mdPermission);
				Set<SubformGroupPermissionVO> setPermissions = new HashSet<SubformGroupPermissionVO>();
				for (MasterDataVO<UID> mdCol : findSubformGroupPermissionsBySubformPermission(permission.getPrimaryKey())) {
					setPermissions.add(MasterDataWrapper.getSubformColumnPermissionVO(mdCol));
				}
				permission.setGroupPermissions(setPermissions);
				userSubformRights.addValue(permission.getRole(), permission);
			}
			statevo.setUserSubformRights(userSubformRights);

			statevo.setMandatoryFields(findMandatoryFieldsByStateUID(statevo.getPrimaryKey()));
			statevo.setMandatoryColumns(findMandatoryColumnsByStateUID(statevo.getPrimaryKey()));

			ststatevo.add(statevo);

			final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.STATETRANSITION.state2, ComparisonOperator.EQUAL, statevo.getPrimaryKey());
			final Collection<MasterDataVO<UID>> mdList = getMasterDataFacade().getMasterData(E.STATETRANSITION, cond);

			for (final MasterDataVO<UID> mdVO : mdList) {
				sttransitionvo.add(MasterDataWrapper.getStateTransitionVO(getMasterDataFacade().getWithDependants(E.STATETRANSITION.getUID(), mdVO.getPrimaryKey(), null)));
			}
		}
		result.setStates(ststatevo);
		result.setTransitions(sttransitionvo);
		
		if (stateModel.getLayout() == null)
			stateModel.setLayout(StateGraphVO.newLayoutInfo(result));

		return result;
	}

	/**
	 * method to insert, update or remove a complete state model in the database at once
	 * @param stategraphcvo state graph representation
	 * @return state model UID
	 * @throws CommonPermissionException
	 */
	public UID setStateGraph(StateGraphVO stategraphcvo, IDependentDataMap mpDependants) 
			throws CommonCreateException, CommonFinderException, CommonRemoveException, CommonValidationException, 
			CommonStaleVersionException, CommonPermissionException, NuclosBusinessException {

		UID result;

		// check state graph for validity:
		StateModelVO statemodelvo = stategraphcvo.getStateModel();
		if (!statemodelvo.isRemoved()) {
			stategraphcvo.validate();	// throws CommonValidationException
		}

		// set state model:
		if (statemodelvo.getPrimaryKey() == null) {
			checkWriteAllowed(E.STATEMODEL);
			result = createStateGraph(stategraphcvo);
			if (mpDependants != null) {
				for (IDependentKey dependentKey : mpDependants.getKeySet()) {
					for (EntityObjectVO<?> mdvoDependant : mpDependants.getData(dependentKey)) {
						mdvoDependant.setFieldUid(E.STATEMODELUSAGE.statemodel, (UID) mdvoDependant.getPrimaryKey());						
					}
				}
				getMasterDataFacade().modifyDependants(
						result, mpDependants, null);
			}
		}
		else {
			StateModelVO dbStateModel = findStateModelByUID(statemodelvo.getPrimaryKey());
			result = dbStateModel.getPrimaryKey();

			checkForStaleVersion(dbStateModel, statemodelvo);

			if (statemodelvo.isRemoved()) {
				// remove state model graph:
				checkDeleteAllowed(E.STATEMODEL);
				removeStateGraph(stategraphcvo, dbStateModel);
			} else {
				// update state model graph:
				checkWriteAllowed(E.STATEMODEL);
				updateStateGraph(stategraphcvo, dbStateModel);
			}

			if (mpDependants != null) {
				getMasterDataFacade().modifyDependants(
						stategraphcvo.getStateModel().getPrimaryKey(), mpDependants, null);
			}
		}

		localeFacade.flushInternalCaches(true);
		StateModelUsagesCache.getInstance().invalidateCache(true, true);
		stateCache.invalidateCache(true, true);
		SecurityCache.getInstance().invalidate();
		SecurityCache.getInstance().invalidateCache(true, true);
		esCache.invalidate(E.STATEMODEL);

		updateInitialStatesByStatemodel(result);
		
		return result;
	}

	private UID createStateGraph(StateGraphVO stategraphvo) throws NuclosBusinessRuleException, CommonPermissionException, CommonCreateException, CommonFinderException {
		StateModelVO statemodelvo = stategraphvo.getStateModel();
		MasterDataVO<UID> mdVO = getMasterDataFacade().create(MasterDataWrapper.wrapStateModelVO(statemodelvo), null);
		StateModelVO dbStateModel = MasterDataWrapper.getStateModelVO(mdVO, stategraphvo);

		final Map<UID, UID> mpStates = new HashMap<UID, UID>();
		final Map<UID, UID> mpTransitions = new HashMap<UID, UID>();
		StateModelLayout layoutinfo = statemodelvo.getLayout();
		for (StateVO statevo : stategraphvo.getStates()) {
			if(!statevo.isRemoved()) {
				statevo.setModelId(mdVO.getPrimaryKey());
				
				MasterDataVO<UID> newState = MasterDataWrapper.wrapStateVO(statevo);
				
				String labelResId = localeFacade.setResourceForLocale(null, LocaleInfo.parseTag(Locale.ENGLISH), "");
				String descriptionResId = localeFacade.setResourceForLocale(null, LocaleInfo.parseTag(Locale.ENGLISH), "");
				String buttonLabelResId = localeFacade.setResourceForLocale(null, LocaleInfo.parseTag(Locale.ENGLISH), "");
				
				newState.setFieldValue(E.STATE.labelres, labelResId);
				newState.setFieldValue(E.STATE.descriptionres, descriptionResId);
				newState.setFieldValue(E.STATE.buttonRes, buttonLabelResId);
				
				localeFacade.setResourceForLocale(labelResId, LocaleInfo.parseTag(Locale.GERMAN), statevo.getStatename(Locale.GERMAN));
				localeFacade.setResourceForLocale(labelResId, LocaleInfo.parseTag(Locale.ENGLISH), statevo.getStatename(Locale.ENGLISH));
				localeFacade.setResourceForLocale(descriptionResId, LocaleInfo.parseTag(Locale.GERMAN), StringUtils.defaultIfNull(statevo.getDescription(Locale.GERMAN), ""));
				localeFacade.setResourceForLocale(descriptionResId, LocaleInfo.parseTag(Locale.ENGLISH), StringUtils.defaultIfNull(statevo.getDescription(Locale.ENGLISH), ""));
				localeFacade.setResourceForLocale(buttonLabelResId, LocaleInfo.parseTag(Locale.GERMAN), StringUtils.defaultIfNull(statevo.getButtonLabel(Locale.GERMAN), ""));
				localeFacade.setResourceForLocale(buttonLabelResId, LocaleInfo.parseTag(Locale.ENGLISH), StringUtils.defaultIfNull(statevo.getButtonLabel(Locale.ENGLISH), ""));	
				
				MasterDataVO<UID> createdState = getMasterDataFacade().create(newState, null);
				mpStates.put(statevo.getClientUID(), createdState.getPrimaryKey());		//prepare mapping table for state transition inserts/updates
				statevo.setPrimaryKey(createdState.getPrimaryKey());
				layoutinfo.updateStateId(statevo.getClientUID(), createdState.getPrimaryKey());
				
				createUserRights(statevo);
				createUserSubformRights(statevo);
				createMandatoryFields(statevo);
				createMandatoryColumns(statevo);
			}
		}
		for (StateTransitionVO statetransitionvo : stategraphvo.getTransitions()) {
			if(!statetransitionvo.isRemoved()) {
				StateTransitionVO newVO = createStateTransition(statetransitionvo,mpStates);
				//prepare mapping table for state transition inserts/updates
				mpTransitions.put(statetransitionvo.getClientUID(),newVO.getPrimaryKey());
				layoutinfo.updateTransitionId(statetransitionvo.getClientUID(), mpTransitions.get(statetransitionvo.getClientUID()));
			}
		}
		
		// Speichern der korrekten State- und Transition-IDs der Layouts
		dbStateModel.setLayout(layoutinfo);

		try {
			getMasterDataFacade().modify(MasterDataWrapper.wrapStateModelVO(dbStateModel),null);
		}
		catch (CommonValidationException e) {
			throw new CommonFatalException(e);
		}
		catch (CommonStaleVersionException e) {
			throw new CommonFatalException(e);
		}
		catch (CommonRemoveException e) {
			throw new CommonFatalException(e);
		}
		return dbStateModel.getPrimaryKey();
	}

	private void removeStateGraph(StateGraphVO stateGraphVO, StateModelVO stateModelVO) 
			throws CommonRemoveException, CommonFinderException, CommonPermissionException, 
			NuclosBusinessRuleException, CommonStaleVersionException {
		
		// remove usage
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.STATEMODELUSAGE.statemodel, ComparisonOperator.EQUAL, stateModelVO.getPrimaryKey());
		final Collection<MasterDataVO<UID>> usages = getMasterDataFacade().getMasterData(E.STATEMODELUSAGE, cond);
		for (MasterDataVO<UID> usage : usages) {
			getMasterDataFacade().remove(E.STATEMODELUSAGE.getUID(), usage.getPrimaryKey(), false, null);
		}

		// remove the transitions
		for (StateTransitionVO statetransitionvo : stateGraphVO.getTransitions()) {
			if (statetransitionvo.getPrimaryKey() != null) {
				// (SERVERCODETRANSITION removed by cascade on delete)
				// (ROLETRANSITION removed by cascade on delete)
				getMasterDataFacade().remove(E.STATETRANSITION.getUID(), statetransitionvo.getPrimaryKey(), false, null);				
			}
		}

		// remove the states
		for (StateVO statevo : stateGraphVO.getStates()) {
			if (statevo.getPrimaryKey() != null) {
				removeState(statevo.getPrimaryKey());
			}
		}

		// remove the model
		if (stateModelVO.getPrimaryKey() != null)
			getMasterDataFacade().remove(E.STATEMODEL.getUID(), stateModelVO.getPrimaryKey(), false, null);
	}

	private void updateStateGraph(StateGraphVO stategraphcvo, StateModelVO dbStateModel) 
			throws CommonFinderException, CommonCreateException,
			NuclosBusinessRuleException, CommonPermissionException, CommonValidationException, 
			CommonStaleVersionException, CommonRemoveException, NuclosBusinessException
	{
		StateModelVO statemodelvo = stategraphcvo.getStateModel();
		StateModelLayout layoutinfo = statemodelvo.getLayout();

		validateUniqueConstraint(statemodelvo, dbStateModel);
		
		boolean forceStatemodelUpdate = false;

		// update (and delete) transitions part I ...
		for (StateTransitionVO statetransitionvo : stategraphcvo.getTransitions()) {
			
			if (statetransitionvo.isRemoved() && statetransitionvo.getPrimaryKey() != null) {
				// (SERVERCODETRANSITION removed by cascade on delete)
				// (ROLETRANSITION removed by cascade on delete)
				getMasterDataFacade().remove(E.STATETRANSITION.getUID(), statetransitionvo.getPrimaryKey(), true, null);
				layoutinfo.removeTransition(statetransitionvo.getPrimaryKey());
				forceStatemodelUpdate = true;
			}
			else if (statetransitionvo.getPrimaryKey() != null) {
				// As old states must be deleted before new states can be
				// inserted due to unique constraints on name an numeral, we
				// have to delete the state references from the transitions first.
				
				MasterDataVO<UID> transitionDb = getMasterDataFacade().get(E.STATETRANSITION, statetransitionvo.getPrimaryKey());
				
				boolean sourceStateRemoved = false;
				boolean targetStateRemoved = false;
				for (final StateVO statevo : stategraphcvo.getStates()) {
					final UID stateUid = statevo.getPrimaryKey();
					
					if (statevo.isRemoved() && stateUid != null) {
						if (stateUid.equals(transitionDb.getFieldUid(E.STATETRANSITION.state1))) {
							sourceStateRemoved = true;
						}
						if (stateUid.equals(transitionDb.getFieldUid(E.STATETRANSITION.state2))) {
							targetStateRemoved = true;
						}
					}
					if (sourceStateRemoved || targetStateRemoved) {
						MasterDataVO<UID> mdTransition = getMasterDataFacade().get(E.STATETRANSITION, statetransitionvo.getPrimaryKey());
						if (sourceStateRemoved) {
							mdTransition.getEntityObject().removeFieldUid(E.STATETRANSITION.state1.getUID());
						}
						if (targetStateRemoved) {
							mdTransition.getEntityObject().removeFieldUid(E.STATETRANSITION.state2.getUID());
						}
						getMasterDataFacade().modify(mdTransition, null);
						forceStatemodelUpdate = true;
					}
				}
			}
		}
		
		// delete states ...
		for (final StateVO statevo : stategraphcvo.getStates()) {
			final UID stateUid = statevo.getPrimaryKey();

			if (statevo.isRemoved() && stateUid != null) {
				removeState(stateUid);
				layoutinfo.removeState(stateUid);
				forceStatemodelUpdate = true;
			}
		}
		
		final Map<UID, UID> mpStates = new HashMap<UID, UID>();
		
		// update states ...
		for (final StateVO statevo : stategraphcvo.getStates()) {
			final UID stateUid = statevo.getPrimaryKey();

			if (!statevo.isRemoved() && stateUid != null) {
				mpStates.put(statevo.getClientUID(), stateUid);	//prepare mapping table for state transition inserts/updates
				
				boolean forceStateUpdate = false;
				updateUserRights(statevo);
				updateUserSubformRights(statevo);
				
				if (updateMandatoryFields(statevo)) {
					forceStateUpdate = true;
				}
				if (updateMandatoryColumns(statevo)) {
					forceStateUpdate = true;
				}
				if (updateState(statevo, statemodelvo, forceStateUpdate)) {
					forceStatemodelUpdate = true;
				}
			}				
		}
		
		// insert states ...
		for (final StateVO statevo : stategraphcvo.getStates()) {
			final UID stateUid = statevo.getPrimaryKey();

			if (!statevo.isRemoved() && stateUid == null) {
				statevo.setModelId(statemodelvo.getPrimaryKey());
				
				MasterDataVO<UID> newState = MasterDataWrapper.wrapStateVO(statevo);
				
				LocaleInfo localeInfoGerman = LocaleInfo.parseTag(Locale.GERMAN);
				LocaleInfo localeInfoEnglish = LocaleInfo.parseTag(Locale.ENGLISH);

				String labelResId = localeFacade.setResourceForLocale(null, localeInfoEnglish, "");
				String descriptionResId = localeFacade.setResourceForLocale(null, localeInfoEnglish, "");
				String buttonLabelResId = localeFacade.setResourceForLocale(null, localeInfoEnglish, "");
				
				newState.setFieldValue(E.STATE.labelres, labelResId);
				newState.setFieldValue(E.STATE.descriptionres, descriptionResId);
				newState.setFieldValue(E.STATE.buttonRes, buttonLabelResId);
				
				updateResourceByLocaleAndId(labelResId, localeInfoGerman, statevo.getStatename(Locale.GERMAN));
				updateResourceByLocaleAndId(labelResId, localeInfoEnglish, statevo.getStatename(Locale.ENGLISH));
				
				updateResourceByLocaleAndId(descriptionResId, localeInfoGerman, statevo.getDescription(Locale.GERMAN));
				updateResourceByLocaleAndId(descriptionResId, localeInfoEnglish, statevo.getDescription(Locale.ENGLISH));
				
				updateResourceByLocaleAndId(buttonLabelResId, localeInfoGerman, statevo.getButtonLabel(Locale.GERMAN));
				updateResourceByLocaleAndId(buttonLabelResId, localeInfoEnglish, statevo.getButtonLabel(Locale.ENGLISH));	
				
				
				MasterDataVO<UID> createdState = getMasterDataFacade().create(newState, null);
				mpStates.put(statevo.getClientUID(), createdState.getPrimaryKey());		//prepare mapping table for state transition inserts/updates
				statevo.setPrimaryKey(createdState.getPrimaryKey());
				layoutinfo.updateStateId(statevo.getClientUID(), mpStates.get(statevo.getClientUID()));
				
				createUserRights(statevo);
				createUserSubformRights(statevo);
				createMandatoryFields(statevo);
				createMandatoryColumns(statevo);
				
				forceStatemodelUpdate = true;
			}
		}
		
		// update (and insert) transitions part II ...
		final Map<UID, UID> mpTransitions = new HashMap<UID, UID>();
		for (StateTransitionVO statetransitionvo : stategraphcvo.getTransitions()) {
			
			if (!statetransitionvo.isRemoved()) {
				if (statetransitionvo.getPrimaryKey() == null) {
					// insert transition
					StateTransitionVO newVO = createStateTransition(statetransitionvo, mpStates);
					mpTransitions.put(statetransitionvo.getClientUID(), newVO.getPrimaryKey());		//prepare mapping table for state transition inserts/updates
					layoutinfo.updateTransitionId(statetransitionvo.getClientUID(), mpTransitions.get(statetransitionvo.getClientUID()));
					forceStatemodelUpdate = true;
				}
				else {
					// update transition
					final MasterDataVO<UID> transitionDb = getMasterDataFacade().get(E.STATETRANSITION, statetransitionvo.getPrimaryKey());
					final StateTransitionVO transitionDbVo = MasterDataWrapper.getStateTransitionVOWithoutDependants(transitionDb);

					final UID stateSourceUid = statetransitionvo.getStateSourceUID();
					if (stateSourceUid != null) {	//newly inserted state referenced?
						statetransitionvo.setStateSource(mpStates.get(stateSourceUid));				//map newly inserted state temp id to real state id
					}
					final UID stateTargetUid = statetransitionvo.getStateTargetUID();
					if (stateTargetUid != null) {
						statetransitionvo.setStateTarget(mpStates.get(stateTargetUid));
					}

					transitionDbVo.setAutomatic(statetransitionvo.isAutomatic());
					transitionDbVo.setDefault(statetransitionvo.isDefault());
					transitionDbVo.setNonstop(statetransitionvo.isNonstop());
					transitionDbVo.setDescription(statetransitionvo.getDescription());
					transitionDbVo.setStateSource(statetransitionvo.getStateSourceUID());
					transitionDbVo.setStateTarget(statetransitionvo.getStateTargetUID());
					
					MasterDataVO<UID> wrapStateTransitionVO = MasterDataWrapper.wrapStateTransitionVO(transitionDbVo);
					boolean updateTransition = false;
					if (!transitionDb.getEntityObject().equalFields(wrapStateTransitionVO.getEntityObject(), false)) {
						updateTransition = true;
					}
					updateStateTransitionRoles(statetransitionvo);
					if (updateStateTransitionRules(statetransitionvo)) {
						updateTransition = true;
					}
					if (updateTransition) {
						getMasterDataFacade().modify(wrapStateTransitionVO, null);
						forceStatemodelUpdate = true;
					}
				}
			}
		}

		MasterDataVO<UID> mdStatemodel = MasterDataWrapper.wrapStateModelVO(dbStateModel);
		
		dbStateModel.setName(statemodelvo.getName());
		dbStateModel.setDescription(statemodelvo.getDescription());
		dbStateModel.setLayout(layoutinfo);
		
		MasterDataVO<UID> newStatemodel = MasterDataWrapper.wrapStateModelVO(dbStateModel);
		
		if (forceStatemodelUpdate || !mdStatemodel.getEntityObject().equalFields(newStatemodel.getEntityObject(), false)) {
			getMasterDataFacade().modify(newStatemodel, null);
		}
	}
	
	public void updateInitialStatesByEntity(final UID entityUid) throws CommonCreateException {
		for (UID statemodelUid : StateModelUsagesCache.getInstance().getStateUsages().getStateModelUIDsByEntity(entityUid)) {
			updateInitialStatesByStatemodel(statemodelUid);
		}
	}
	
	/**
	 * set initial states for all objects
	 * @param statemodelUid entity UID
	 * @throws CommonCreateException
	 */
	private void updateInitialStatesByStatemodel(final UID statemodelUid) throws CommonCreateException {
		final Set<UsageCriteria> stateModelUsage = StateModelUsagesCache.getInstance().getStateUsages().getUsageCriteriasByStateModelUID(statemodelUid);
		for (UsageCriteria uc : stateModelUsage) {
			if (Modules.getInstance().isModule(uc.getEntityUID())) {
				final StateVO stateVO = stateCache.getState(StateModelUsagesCache.getInstance().getStateUsages().getInitialStateUID(uc));
				if (stateVO != null) {
					
					CollectableSearchCondition cond = SearchConditionUtils.newIsNullCondition(uc.getEntityUID(), SF.STATE);
					final List<Long> genvos = genericObjectFacade.getGenericObjectIdsNoCheck(uc.getEntityUID(), new CollectableSearchExpression(cond));
					for (Long id : genvos) {
						try {
							EntityObjectVO<Long> dalVO = DalSupportForGO.getEntityObjectProcessor(uc.getEntityUID()).getByPrimaryKey(id);
						
							dalVO.setFieldUid(SF.STATE_UID, stateVO.getPrimaryKey());
							dalVO.setFieldValue(E.STATE.icon, stateVO.getIcon());
							dalVO.setFieldValue(E.STATE.numeral, stateVO.getNumeral());

							dalVO.flagUpdate();
							DalSupportForGO.getEntityObjectProcessor(uc.getEntityUID()).insertOrUpdate(dalVO);
						}
						catch (DbException e) {
							throw new CommonCreateException(e);
						}
					}
				}
			}
		}
	}

	private boolean updateState(StateVO clientStateVO, StateModelVO modelVO, boolean forceUpdate) 
			throws CommonFinderException, CommonPermissionException, NuclosBusinessRuleException, 
			CommonCreateException, CommonRemoveException, CommonStaleVersionException, 
			CommonValidationException {
		
		MasterDataVO<UID> stateDb = getMasterDataFacade().get(E.STATE, clientStateVO.getId());
		String labelResId = stateDb.getFieldValue(E.STATE.labelres);
		String descriptionResId = stateDb.getFieldValue(E.STATE.descriptionres);
		String buttonLabelResId = stateDb.getFieldValue(E.STATE.buttonRes);
		
		StateVO dbStateVO = MasterDataWrapper.getStateVO(stateDb);
		// wrap-wrap cause of locale resource mapping
		stateDb = MasterDataWrapper.wrapStateVO(dbStateVO);
		stateDb.setFieldValue(E.STATE.labelres, labelResId);
		stateDb.setFieldValue(E.STATE.descriptionres, descriptionResId);
		stateDb.setFieldValue(E.STATE.buttonRes, buttonLabelResId);
		
		if (!RigidUtils.equal(dbStateVO.getStatename(Locale.GERMAN), clientStateVO.getStatename(Locale.GERMAN)) || 
			!RigidUtils.equal(dbStateVO.getStatename(Locale.ENGLISH), clientStateVO.getStatename(Locale.ENGLISH)) || 
			!RigidUtils.equal(dbStateVO.getDescription(Locale.GERMAN), clientStateVO.getDescription(Locale.GERMAN)) || 
			!RigidUtils.equal(dbStateVO.getDescription(Locale.ENGLISH), clientStateVO.getDescription(Locale.ENGLISH)) || 
			!RigidUtils.equal(dbStateVO.getButtonLabel(Locale.GERMAN), clientStateVO.getButtonLabel(Locale.GERMAN)) || 
			!RigidUtils.equal(dbStateVO.getButtonLabel(Locale.ENGLISH), clientStateVO.getButtonLabel(Locale.ENGLISH))) {
			forceUpdate = true;
		}

		dbStateVO.setNumeral(clientStateVO.getNumeral());
		dbStateVO.setStatename(Locale.GERMAN, clientStateVO.getStatename(Locale.GERMAN));
		dbStateVO.setStatename(Locale.ENGLISH, clientStateVO.getStatename(Locale.ENGLISH));
		
		dbStateVO.setIcon(clientStateVO.getIcon());
		dbStateVO.setDescription(Locale.GERMAN, clientStateVO.getDescription(Locale.GERMAN));
		dbStateVO.setDescription(Locale.ENGLISH, clientStateVO.getDescription(Locale.ENGLISH));
		
		dbStateVO.setModelId(modelVO.getPrimaryKey());
		dbStateVO.setTabbedPaneName(clientStateVO.getTabbedPaneName());
		dbStateVO.setColor(clientStateVO.getColor());
		dbStateVO.setButtonIcon(clientStateVO.getButtonIcon());
		
		LocaleInfo localeInfoGerman = LocaleInfo.parseTag(Locale.GERMAN);
		LocaleInfo localeInfoEnglish = LocaleInfo.parseTag(Locale.ENGLISH);

		if (labelResId == null) {
			labelResId = localeFacade.setResourceForLocale(null, localeInfoEnglish, "");
		}
		
		if (descriptionResId == null) {
			descriptionResId = localeFacade.setResourceForLocale(null, localeInfoEnglish, "");
		}
		
		if (buttonLabelResId == null) {
			buttonLabelResId = localeFacade.setResourceForLocale(null, localeInfoEnglish, "");
		}
		
		updateResourceByLocaleAndId(labelResId, localeInfoGerman, dbStateVO.getStatename(Locale.GERMAN));
		updateResourceByLocaleAndId(labelResId, localeInfoEnglish, dbStateVO.getStatename(Locale.ENGLISH));
		
		updateResourceByLocaleAndId(descriptionResId, localeInfoGerman, dbStateVO.getDescription(Locale.GERMAN));
		updateResourceByLocaleAndId(descriptionResId, localeInfoEnglish, dbStateVO.getDescription(Locale.ENGLISH));
		
		updateResourceByLocaleAndId(buttonLabelResId, localeInfoGerman, clientStateVO.getButtonLabel(Locale.GERMAN));
		updateResourceByLocaleAndId(buttonLabelResId, localeInfoEnglish, clientStateVO.getButtonLabel(Locale.ENGLISH));	
		
		
		final MasterDataVO<UID> mdvo = MasterDataWrapper.wrapStateVO(dbStateVO);
		
		mdvo.setFieldValue(E.STATE.labelres, labelResId);
		mdvo.setFieldValue(E.STATE.descriptionres, descriptionResId);
		mdvo.setFieldValue(E.STATE.buttonRes, buttonLabelResId);
		
		if (forceUpdate || !stateDb.getEntityObject().equalFields(mdvo.getEntityObject(), false)) {
			getMasterDataFacade().modify(mdvo, null);
			return true;
		}
		return false;
	}
	
	private void updateResourceByLocaleAndId(String sResId, LocaleInfo localeInfo, String text) {
		String textNotNull = StringUtils.defaultIfNull(text, "");
		
		if (textNotNull.isEmpty()) {
			String containing = localeFacade.getResourceById(localeInfo, sResId);
			if (containing != null && !containing.isEmpty()) {
				localeFacade.deleteResourceFromLocale(sResId, localeInfo);
			}
			
		} else {
			localeFacade.setResourceForLocale(sResId, localeInfo, textNotNull);
			
		}
		
	}

	private void validateUniqueConstraint(StateModelVO statemodelvo, StateModelVO dbStateModel) throws CommonValidationException, CommonPermissionException, NuclosBusinessException {
		try {
			final StateModelVO vo =
					MasterDataWrapper.getStateModelVO(getMasterDataFacade().get(E.STATEMODEL, dbStateModel.getPrimaryKey()), null);
			if(!ObjectUtils.equals(statemodelvo.getPrimaryKey(), vo.getPrimaryKey())){
				throw new CommonValidationException(
						StringUtils.getParameterizedExceptionMessage("validation.unique.constraint", "Name", "Status model"));
			}
		} catch (CommonFinderException e) {
			// No element found -> validation O.K.
		}
	}

	private void createUserRights(final StateVO statevo) throws NuclosBusinessRuleException, CommonPermissionException, CommonCreateException {
		for (final UID roleUid : statevo.getUserRights().keySet()) {
			for (AttributegroupPermissionVO attrgrouppermissionvo : statevo.getUserRights().getValues(roleUid)) {
				final AttributegroupPermissionVO permissionVO = new AttributegroupPermissionVO(
						attrgrouppermissionvo.getAttributegroupUID(), roleUid, statevo.getPrimaryKey(), attrgrouppermissionvo.isWritable());
				getMasterDataFacade().create(MasterDataWrapper.wrapAttributegroupPermissionVO(permissionVO),  null);
			}
		}
	}
	
	private boolean updateUserRights(final StateVO statevo) throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonValidationException, CommonPermissionException {
		// ROLEATTRIBUTEGROUP insert, update, delete only if needed
		boolean updated = false;
		Collection<MasterDataVO<UID>> attributegroupsDb = findAttributegroupPermissionsByState(statevo.getPrimaryKey());
		Collection<AttributegroupPermissionVO> attributegroupsDbVo = new ArrayList<AttributegroupPermissionVO>();
		for (MasterDataVO<UID> attributegroupDb : attributegroupsDb) {
			AttributegroupPermissionVO found = null;
			final UID roleUid = attributegroupDb.getFieldUid(E.ROLEATTRIBUTEGROUP.role);
			for (AttributegroupPermissionVO attrgrouppermissionvo : statevo.getUserRights().getValues(roleUid)) {
				if (RigidUtils.equal(attrgrouppermissionvo.getAttributegroupUID(), attributegroupDb.getFieldUid(E.ROLEATTRIBUTEGROUP.attributegroup))) {
					found = attrgrouppermissionvo;
					break;
				}
			}
			if (found != null) {
				// update
				attributegroupsDbVo.add(found);
				MasterDataVO<UID> wrapAttributegroupPermissionVO = MasterDataWrapper.wrapAttributegroupPermissionVO(found);
				Boolean readwrite = wrapAttributegroupPermissionVO.getFieldValue(E.ROLEATTRIBUTEGROUP.readwrite);
				if (!RigidUtils.equal(readwrite, attributegroupDb.getFieldValue(E.ROLEATTRIBUTEGROUP.readwrite))) {
					attributegroupDb.setFieldValue(E.ROLEATTRIBUTEGROUP.readwrite, readwrite);
					getMasterDataFacade().modify(attributegroupDb, null);
					updated = true;
				}
			} else {
				// delete
				getMasterDataFacade().remove(E.ROLEATTRIBUTEGROUP.getUID(), attributegroupDb.getPrimaryKey(), false, null);
				updated = true;
			}
		}
		for (AttributegroupPermissionVO attrgrouppermissionvo : statevo.getUserRights().getAllValues()) {
			if (attributegroupsDbVo.contains(attrgrouppermissionvo)) {
				continue;
			}
			// insert
			final AttributegroupPermissionVO newAttrgrouppermissionvo = new AttributegroupPermissionVO(
					attrgrouppermissionvo.getAttributegroupUID(), attrgrouppermissionvo.getRoleUID(), statevo.getPrimaryKey(), attrgrouppermissionvo.isWritable());
			getMasterDataFacade().create(MasterDataWrapper.wrapAttributegroupPermissionVO(newAttrgrouppermissionvo),  null);
			updated = true;
		}
		return updated;
	}

	private void createMandatoryFields(StateVO statevo) throws NuclosBusinessRuleException, CommonPermissionException, CommonCreateException {
		for (MandatoryFieldVO mandatoryVO : statevo.getMandatoryFields()) {
			getMasterDataFacade().create(MasterDataWrapper.wrapMandatoryFieldVO(
					new MandatoryFieldVO(mandatoryVO.getField(), statevo.getPrimaryKey())), null);
		}
	}
	
	private boolean updateMandatoryFields(final StateVO statevo) throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonValidationException, CommonPermissionException {
		// STATEMANDATORYFIELD insert, update, delete only if needed
		boolean updated = false;
		Collection<MasterDataVO<UID>> mandatoryFieldsDb = findMandatoryFieldsMDByStateUID(statevo.getPrimaryKey());
		Collection<MandatoryFieldVO> mandatoryFieldsDbVo = new ArrayList<MandatoryFieldVO>();
		for (MasterDataVO<UID> mandatoryFieldDb : mandatoryFieldsDb) {
			MandatoryFieldVO found = null;
			for (MandatoryFieldVO mandatoryfieldvo : statevo.getMandatoryFields()) {
				if (RigidUtils.equal(mandatoryfieldvo.getField(), mandatoryFieldDb.getFieldUid(E.STATEMANDATORYFIELD.entityfield))) {
					found = mandatoryfieldvo;
					break;
				}
			}
			if (found != null) {
				// (update) - nothing to do
				mandatoryFieldsDbVo.add(found);
			} else {
				// delete
				getMasterDataFacade().remove(E.STATEMANDATORYFIELD.getUID(), mandatoryFieldDb.getPrimaryKey(), false, null);
				updated = true;
			}
		}
		for (MandatoryFieldVO mandatoryfieldvo : statevo.getMandatoryFields()) {
			if (mandatoryFieldsDbVo.contains(mandatoryfieldvo)) {
				continue;
			}
			// insert
			final MandatoryFieldVO newMandatoryfieldvo = new MandatoryFieldVO(mandatoryfieldvo.getField(), statevo.getPrimaryKey());
			getMasterDataFacade().create(MasterDataWrapper.wrapMandatoryFieldVO(newMandatoryfieldvo),  null);
			updated = true;
		}
		return updated;
	}

	private void createMandatoryColumns(final StateVO statevo) throws NuclosBusinessRuleException, CommonPermissionException, CommonCreateException {
		for (MandatoryColumnVO mandatoryVO : statevo.getMandatoryColumns()) {
			getMasterDataFacade().create(MasterDataWrapper.wrapMandatoryColumnVO(
					new MandatoryColumnVO(mandatoryVO.getEntity(), mandatoryVO.getColumn(), statevo.getPrimaryKey())), null);
		}
	}
	
	private boolean updateMandatoryColumns(final StateVO statevo) throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonValidationException, CommonPermissionException {
		// STATEMANDATORYCOLUMN insert, update, delete only if needed
		boolean updated = false;
		Collection<MasterDataVO<UID>> mandatoryColumnsDb = findMandatoryColumnsMDByStateUID(statevo.getPrimaryKey());
		Collection<MandatoryColumnVO> mandatoryColumnsDbVo = new ArrayList<MandatoryColumnVO>();
		for (MasterDataVO<UID> mandatoryColumnDb : mandatoryColumnsDb) {
			MandatoryColumnVO found = null;
			for (MandatoryColumnVO mandatorycolumnvo : statevo.getMandatoryColumns()) {
				if (RigidUtils.equal(mandatorycolumnvo.getEntity(), mandatoryColumnDb.getFieldUid(E.STATEMANDATORYCOLUMN.entity)) &&
					RigidUtils.equal(mandatorycolumnvo.getColumn(), mandatoryColumnDb.getFieldUid(E.STATEMANDATORYCOLUMN.column))) {
					found = mandatorycolumnvo;
					break;
				}
			}
			if (found != null) {
				// (update) - nothing to do
				mandatoryColumnsDbVo.add(found);
			} else {
				// delete
				getMasterDataFacade().remove(E.STATEMANDATORYCOLUMN.getUID(), mandatoryColumnDb.getPrimaryKey(), false, null);
				updated = true;
			}
		}
		for (MandatoryColumnVO mandatorycolumnvo : statevo.getMandatoryColumns()) {
			if (mandatoryColumnsDbVo.contains(mandatorycolumnvo)) {
				continue;
			}
			// insert
			final MandatoryColumnVO newMandatorycolumnvo = new MandatoryColumnVO(mandatorycolumnvo.getEntity(), mandatorycolumnvo.getColumn(), statevo.getPrimaryKey());
			getMasterDataFacade().create(MasterDataWrapper.wrapMandatoryColumnVO(newMandatorycolumnvo),  null);
			updated = true;
		}
		return updated;
	}

	private void createUserSubformRights(final StateVO statevo) throws NuclosBusinessRuleException, CommonPermissionException, CommonCreateException {
		for(final UID roleUid : statevo.getUserSubformRights().keySet()) {
			for(SubformPermissionVO subformpermissionvo : statevo.getUserSubformRights().getValues(roleUid)) {
				final SubformPermissionVO permissionVO = new SubformPermissionVO(subformpermissionvo.getSubform(),
						roleUid, statevo.getPrimaryKey(), subformpermissionvo.canCreate(), subformpermissionvo.canDelete(),
						subformpermissionvo.getGroupPermissions());
				MasterDataVO<UID> createdRoleSubformPermission = getMasterDataFacade().create(
						MasterDataWrapper.wrapSubformPermissionVO(permissionVO), null);
				for (SubformGroupPermissionVO subformgrouppermissionvo : subformpermissionvo.getGroupPermissions()) {
					getMasterDataFacade().create(MasterDataWrapper.wrapSubformGroupPermissionVO(
							new SubformGroupPermissionVO(createdRoleSubformPermission.getPrimaryKey(),
										subformgrouppermissionvo.getGroup(), subformgrouppermissionvo.isWriteable())), null);
				}
			}
		}
	}
	
	private boolean updateUserSubformGroupRights(MasterDataVO<UID> subformDb, SubformPermissionVO found) throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonValidationException, CommonPermissionException {
		// ROLESUBFORMGROUP insert, update, delete only if needed
		boolean updated = false;
		Collection<MasterDataVO<UID>> groupsDb = findSubformGroupPermissionsBySubformPermission(subformDb.getPrimaryKey());
		Collection<SubformGroupPermissionVO> updatedGroupDbVo = new ArrayList<SubformGroupPermissionVO>();
		for (MasterDataVO<UID> groupDb : groupsDb) {
			SubformGroupPermissionVO foundCol = null;
			for (SubformGroupPermissionVO subformgrouppermissionvo : found.getGroupPermissions()) {
				if (RigidUtils.equal(subformgrouppermissionvo.getGroup(), groupDb.getFieldUid(E.ROLESUBFORMGROUP.group))) {
					foundCol = subformgrouppermissionvo;
					break;
				}
			}
			if (foundCol != null) {
				// update col
				MasterDataVO<UID> wrapSubformGroupPermissionVO = MasterDataWrapper.wrapSubformGroupPermissionVO(foundCol);
				Boolean readwriteCol = wrapSubformGroupPermissionVO.getFieldValue(E.ROLESUBFORMGROUP.readwrite);
				if (!RigidUtils.equal(readwriteCol, groupDb.getFieldValue(E.ROLESUBFORMGROUP.readwrite))) {
					groupDb.setFieldValue(E.ROLESUBFORMGROUP.readwrite, readwriteCol);
					getMasterDataFacade().modify(groupDb, null);
					updated = true;
				}
				updatedGroupDbVo.add(foundCol);
			} else {
				// delete col
				getMasterDataFacade().remove(E.ROLESUBFORMGROUP.getUID(), groupDb.getPrimaryKey(), false, null);
				updated = true;
			}
		}
		for (SubformGroupPermissionVO subformgrouppermissionvo : found.getGroupPermissions()) {
			if (updatedGroupDbVo.contains(subformgrouppermissionvo)) {
				continue;
			}
			// insert col
			final SubformGroupPermissionVO newSubformgrouppermissionvo = new SubformGroupPermissionVO(
					subformDb.getPrimaryKey(), subformgrouppermissionvo.getGroup(), subformgrouppermissionvo.isWriteable());
			getMasterDataFacade().create(MasterDataWrapper.wrapSubformGroupPermissionVO(newSubformgrouppermissionvo), null);
			updated = true;
		}
		return updated;
	}
	
	private boolean updateUserSubformRights(final StateVO statevo) throws NuclosBusinessRuleException, CommonCreateException, CommonPermissionException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonValidationException {
		// ROLESUBFORM(COLUMN) insert, update, delete only if needed
		boolean updated = false;
		Collection<MasterDataVO<UID>> subformsDb = findSubformPermissionsByState(statevo.getPrimaryKey());
		Collection<SubformPermissionVO> subformsDbVo = new ArrayList<SubformPermissionVO>();
		for (MasterDataVO<UID> subformDb : subformsDb) {
			SubformPermissionVO found = null;
			final UID roleUid = subformDb.getFieldUid(E.ROLESUBFORM.role);
			for (SubformPermissionVO subformpermissionvo : statevo.getUserSubformRights().getValues(roleUid)) {
				if (RigidUtils.equal(subformpermissionvo.getSubform(), subformDb.getFieldUid(E.ROLESUBFORM.entity))) {
					found = subformpermissionvo;
					break;
				}
			}
			if (found != null) {
				// update
				subformsDbVo.add(found);
				MasterDataVO<UID> wrapSubformPermissionVO = MasterDataWrapper.wrapSubformPermissionVO(found);
				Boolean create = wrapSubformPermissionVO.getFieldValue(E.ROLESUBFORM.create);
				Boolean delete = wrapSubformPermissionVO.getFieldValue(E.ROLESUBFORM.delete);
				boolean updatedSubformColumns = updateUserSubformGroupRights(subformDb, found);
				if (!RigidUtils.equal(create, subformDb.getFieldValue(E.ROLESUBFORM.create))
					|| !RigidUtils.equal(delete, subformDb.getFieldValue(E.ROLESUBFORM.delete))
					|| updatedSubformColumns) {
					subformDb.setFieldValue(E.ROLESUBFORM.create, create);
					subformDb.setFieldValue(E.ROLESUBFORM.delete, delete);
					getMasterDataFacade().modify(subformDb, null);
					updated = true;
				}
			} else {
				// delete (with cols)
				for (MasterDataVO<UID> colDb : findSubformGroupPermissionsBySubformPermission(subformDb.getPrimaryKey())) {
					getMasterDataFacade().remove(E.ROLESUBFORMGROUP.getUID(), colDb.getPrimaryKey(), false, null);
				}
				getMasterDataFacade().remove(E.ROLESUBFORM.getUID(), subformDb.getPrimaryKey(), false, null);
				updated = true;
			}
		}
		for (SubformPermissionVO subformpermissionvo : statevo.getUserSubformRights().getAllValues()) {
			if (subformsDbVo.contains(subformpermissionvo)) {
				continue;
			}
			// insert (with cols)
			final SubformPermissionVO newSubformpermissionvo = new SubformPermissionVO(subformpermissionvo.getSubform(),
					subformpermissionvo.getRole(), statevo.getPrimaryKey(), subformpermissionvo.canCreate(),
					subformpermissionvo.canDelete(), subformpermissionvo.getGroupPermissions());
			MasterDataVO<UID> subformDb = getMasterDataFacade().create(MasterDataWrapper.wrapSubformPermissionVO(newSubformpermissionvo), null);
			for (SubformGroupPermissionVO subformgrouppermissionvo : subformpermissionvo.getGroupPermissions()) {
				final SubformGroupPermissionVO newSubformgrouppermissionvo = new SubformGroupPermissionVO(
						subformDb.getPrimaryKey(), subformgrouppermissionvo.getGroup(), subformgrouppermissionvo.isWriteable());
				getMasterDataFacade().create(MasterDataWrapper.wrapSubformGroupPermissionVO(newSubformgrouppermissionvo), null);
				updated = true;
			}
		}
		return updated;
	}

	/**
	 * method to remove a complete state model with all usages in the database at once
	 * @param statemodelvo state model value object
	 * @throws CommonPermissionException
	 */
	public void removeStateGraph(StateModelVO statemodelvo) 
			throws CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException, 
			NuclosBusinessRuleException, NuclosBusinessException {

		checkDeleteAllowed(E.STATEMODEL);
		try {
			StateModelVO dbStateModel = findStateModelByUID(statemodelvo.getPrimaryKey());

			checkForStaleVersion(dbStateModel, statemodelvo);

			// get and delete state graph:
			StateGraphVO stategraphcvo = getStateGraph(statemodelvo.getPrimaryKey());
			stategraphcvo.getStateModel().remove();
			setStateGraph(stategraphcvo, null);
		}
		catch (CommonCreateException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (CommonValidationException ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	private void removeState(final UID stateUid) throws NuclosBusinessRuleException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException {
		for (MasterDataVO<UID> vo : findMandatoryFieldsMDByStateUID(stateUid)) {
			getMasterDataFacade().remove(E.STATEMANDATORYFIELD.getUID(), vo.getPrimaryKey(), false, null);
		}
		for (MasterDataVO<UID> vo : findMandatoryColumnsMDByStateUID(stateUid)) {
			getMasterDataFacade().remove(E.STATEMANDATORYCOLUMN.getUID(), vo.getPrimaryKey(), false, null);
		}
		for (MasterDataVO<UID> vo : findAttributegroupPermissionsByState(stateUid)) {
			getMasterDataFacade().remove(E.ROLEATTRIBUTEGROUP.getUID(), vo.getPrimaryKey(), false, null);					
		}
		for (MasterDataVO<UID> vo : findSubformPermissionsByState(stateUid)) {
			for (MasterDataVO<UID> groupVO : findSubformGroupPermissionsBySubformPermission(vo.getPrimaryKey())) {
				getMasterDataFacade().remove(E.ROLESUBFORMGROUP.getUID(), groupVO.getPrimaryKey(), false, null);
			}
			getMasterDataFacade().remove(E.ROLESUBFORM.getUID(), vo.getPrimaryKey(), false, null);
		}
		getMasterDataFacade().remove(E.STATE.getUID(), stateUid, false, null);
		Map<FieldMeta<?>, String> mpResIDs = getResourceSIdsForStateUid(stateUid);
		localeFacade.deleteResource(mpResIDs.get(E.STATE.labelres));
		localeFacade.deleteResource(mpResIDs.get(E.STATE.descriptionres));
		localeFacade.deleteResource(mpResIDs.get(E.STATE.buttonRes));
	}

	private void checkForStaleVersion(StateModelVO dbStateModel, StateModelVO clientStateModel) throws CommonStaleVersionException {
		if (dbStateModel.getVersion() != clientStateModel.getVersion()) {
			MasterDataVO<UID> clientModel = MasterDataWrapper.wrapStateModelVO(clientStateModel);
			MasterDataVO<UID> dbModel = MasterDataWrapper.wrapStateModelVO(dbStateModel);
			throw new CommonStaleVersionException(getVersionConflictMessages("status model", clientModel.getEntityObject(), dbModel.getEntityObject()));
		}
	}

	/**
	 * @param usagecriteria
	 * @return the initial state of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	public StateVO getInitialState(UsageCriteria usagecriteria) {
		return stateCache.getState(getInitialStateId(usagecriteria));
	}

	/**
	 * method to return the initial state for a given generic object
	 * @param genericObjectId id of leased object to get initial state for
	 * @return state id of initial state for given generic object
	 */
	public StateVO getInitialState(UID entity, final Long genericObjectId) throws NuclosFatalException {
		UsageCriteria usagecriteria = getRelevantStateUsageCritera(entity, genericObjectId);
		return getInitialState(usagecriteria);
		// @todo We need to make sure there is exactly one initial state for each state model. This must be verified when storing a state model.
	}

	/**
	 * @param usagecriteria
	 * @return the id of the initial state of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	@RolesAllowed("Login")
	public UID getInitialStateId(UsageCriteria usagecriteria) {
		return StateModelUsagesCache.getInstance().getStateUsages().getInitialStateUID(usagecriteria);
	}

	/**
	 * @param usagecriteria
	 * @return the id of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	@RolesAllowed("Login")
	public UID getStateModelId(UsageCriteria usagecriteria) {
		return StateModelUsagesCache.getInstance().getStateUsages().getStateModel(usagecriteria);
	}

	public StateVO getState(final UID stateUid) {
		return stateCache.getState(stateUid);
	}
	
	/**
	 * @return the id of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	@RolesAllowed("Login")
	public Collection<StateVO> getStatesByModel(final UID stateModelUid) {
		return stateCache.getStatesByModel(stateModelUid);
	}

	/**
	 * method to get all state models
	 * @return collection of state model vo
	 */
	public Collection<StateModelVO> getStateModels() throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		final Collection<StateModelVO> result = new HashSet<StateModelVO>();
		final Collection<MasterDataVO<UID>> mdVOList = getMasterDataFacade().getMasterData(E.STATEMODEL, null);
		for (MasterDataVO<UID> mdVO : mdVOList) {
			result.add(MasterDataWrapper.getStateModelVO(mdVO, getStateGraph(mdVO.getPrimaryKey())));
		}
		return result;
	}

	/**
	 * method to return the sorted list of state history entries for a given leased object
	 * 
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param moduleUid id of module for plausibility check
	 * @param genericObjectId id of leased object
	 * @return set of state history entries
	 */
	@RolesAllowed("Login")
	public Collection<StateHistoryVO> getStateHistory(final UID moduleUid, final Long genericObjectId)
			throws CommonFinderException, CommonPermissionException {

		if (!genericObjectFacade.isGenericObjectInModule(moduleUid, genericObjectId)) {
			throw new IllegalArgumentException();
		}
		checkReadAllowedForModule(moduleUid);

		return findStateHistoryByGenericObjectId(genericObjectId);
	}

	private UsageCriteria getRelevantStateUsageCritera(UID entity, final Long genericObjectId) {
		GenericObjectVO govo;
		try {
			govo = getGenericObjectFacade().get(entity, genericObjectId);
		} catch (CommonFinderException ex) {
			throw new NuclosFatalException(ex);
		} catch (CommonPermissionException ex) {
			throw new NuclosFatalException(ex);
		}
		return govo.getUsageCriteria(null);
	}

	/**
	 * Retrieve all states in all state models for the module with the given id
	 * @param moduleUid UID of module to retrieve states for
	 * @return Collection of all states for the given module
	 */
	@RolesAllowed("Login")
	public Collection<StateVO> getStatesByModule(final UID moduleUid) {
		return stateCache.getStatesByModule(moduleUid);
	}

	/**
	 * method to return the actual state for a given leased object
	 * 
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param moduleUid module id for plausibility check
	 * @param genericObjectId id of leased object to get actual state for
	 * @return state id of actual state for given leased object
	 */
	@RolesAllowed("Login")
	public StateVO getCurrentState(final UID moduleUid, final Long genericObjectId) throws CommonFinderException {
		final UID actualStateUid = DalSupportForGO.getEntityObject(genericObjectId, moduleUid).getFieldUid(SF.STATE_UID);
		return (actualStateUid == null) ? null : stateCache.getState(actualStateUid);
	}

	/**
	 * method to return the possible subsequent states for a given leased object
	 * 
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param iModuleId module id for plausibility check
	 * @param iGenericObjectId id of leased object to get subsequent states for
	 * @param bGetAutomaticStatesAlso should method also return states for automatic only transitions? false for returning subsequent states to client, which generates buttons for manual state changes
	 * @return set of possible subsequent states for given leased object
	 */
	@RolesAllowed("Login")
	public Collection<StateVO> getSubsequentStates(UID iModuleId, Long iGenericObjectId, 
			boolean bGetAutomaticStatesAlso) throws CommonFinderException {
    	return getSubsequentStates(iModuleId, iGenericObjectId, bGetAutomaticStatesAlso, null);
	}
	
	private void addSubsequentStates(StateVO statevoCurrent, boolean bGetAutomaticStatesAlso, Collection<StateVO> result) {
		final Collection<StateTransitionVO> collStates = bGetAutomaticStatesAlso ?
				findStateTransitionBySourceState(statevoCurrent.getId()) :
					findStateTransitionBySourceStateNonAutomatic(statevoCurrent.getPrimaryKey());

		final Collection<UID> collTransitionIds = SecurityCache.getInstance().getTransitionUids(utils.getCurrentUserName(), getCurrentMandatorUID());
		for (StateTransitionVO stateTransition : collStates) {
			if (bGetAutomaticStatesAlso || collTransitionIds.contains(stateTransition.getPrimaryKey()))
			{ //may transition be retrieved by actual user?
				StateVO state = stateCache.getState(stateTransition.getStateTargetUID());
				if (stateTransition.isNonstop()) {
					state.addNonstopSource(statevoCurrent.getId());
				}
				result.add(state);
			}
		}		
	}

	public Collection<StateVO> getSubsequentStates(UID iModuleId, Long genericObjectId, boolean bGetAutomaticStatesAlso, UID iCurrentStateId) throws CommonFinderException{

		Collection<StateVO> result = new HashSet<StateVO>();	
		StateVO statevoCurrent = iCurrentStateId == null ? getCurrentState(iModuleId, genericObjectId) : stateCache.getState(iCurrentStateId);
		
		if (statevoCurrent == null) {
			//if there is no current state, then subsequent state equals to initial state
			result.add(stateCache.getState(getInitialState(iModuleId, genericObjectId).getPrimaryKey()));
		} else {
			addSubsequentStates(statevoCurrent, bGetAutomaticStatesAlso, result);
		}
		
		return result;
	}
	
	public Collection<StateVO> getSubsequentStates(UsageCriteria usagecriteria, boolean bGetAutomaticStatesAlso, UID iCurrentStateId) throws CommonFinderException {
		Collection<StateVO> result = new HashSet<StateVO>();
		//TODO: Check: Within usagecriteria there is actually the current state, why set to null?
		StateVO statevoCurrent = iCurrentStateId == null ? null : stateCache.getState(iCurrentStateId);
		
		if (statevoCurrent == null) {
			//if there is no current state, then subsequent state equals to initial state
			result.add(stateCache.getState(getInitialState(usagecriteria).getPrimaryKey()));
		} else {
			addSubsequentStates(statevoCurrent, bGetAutomaticStatesAlso, result);
		}
		
		return result;
	}

	/**
	 * method to change the status of a given leased object (called by server only)
	 * @param moduleUid module UID for plausibility check
	 * @param genericObjectId leased object id to change status for
	 * @param iNumeral legal subsequent status numeral to set for given leased object
	 */
	public void changeStateByRule(final UID moduleUid, final Long genericObjectId, int iNumeral, String customUsage)
			throws NuclosBusinessException, CommonFinderException, CommonPermissionException, CommonCreateException {

		try {
			changeState(moduleUid, genericObjectId, getTargetStateId(moduleUid, genericObjectId, iNumeral), customUsage);
		} catch (NuclosCompileException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonValidationException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonStaleVersionException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonRemoveException e) {
			throw new NuclosUpdateException(e);
		}
	}

	/**
	 * method to change the status of a given leased object (called by server only)
	 * @param moduleUid module UID for plausibility check
	 * @param genericObjectId leased object id to change status for
	 * @param targetStateUid legal subsequent status UID to set for given leased object
	 */
	public void changeStateByRule(final UID moduleUid, final Long genericObjectId, final UID targetStateUid, final String customUsage)
			throws NuclosBusinessException, CommonFinderException, CommonPermissionException, CommonCreateException {

		try {
			changeState(moduleUid, genericObjectId, targetStateUid, true, customUsage);
		} catch (NuclosCompileException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonValidationException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonStaleVersionException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonRemoveException e) {
			throw new NuclosUpdateException(e);
		}
	}

	public void enterInitialState(GenericObjectWithDependantsVO goVO, String customUsage)
			throws NuclosBusinessException, CommonCreateException, CommonPermissionException {

		final UID initialStateUid = getInitialState(goVO.getModule(), goVO.getPrimaryKey()).getPrimaryKey();
		try {
			changeState(goVO.getModule(), goVO, initialStateUid, customUsage);
		} catch (NuclosCompileException e) {
			throw new CommonCreateException(e);
		} catch (CommonValidationException e) {
			throw new CommonCreateException(e);
		} catch (CommonStaleVersionException e) {
			throw new CommonCreateException(e);
		} catch (CommonRemoveException e) {
			throw new CommonCreateException(e);
		} catch (CommonFinderException e) {
			throw new CommonCreateException(e);
		}
	}

	/**
	 * method to change the status of a given leased object
	 * 
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param moduleUid module id for plausibility check
	 * @param genericObjectId leased object id to change status for
	 * @param targetStateUid legal subsequent status id to set for given leased object
	 */
	@RolesAllowed("Login")
	public void changeStateByUser(final UID moduleUid, final Long genericObjectId, final UID targetStateUid, String customUsage)
			throws NuclosBusinessException, CommonPermissionException, CommonPermissionException, CommonCreateException,
			NuclosSubsequentStateNotLegalException, CommonFinderException {

		checkWriteAllowedForModule(moduleUid);
		try {
			changeState(moduleUid, genericObjectId, targetStateUid, true, customUsage);
		} catch (NuclosCompileException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonValidationException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonStaleVersionException e) {
			throw new NuclosUpdateException(e);
		} catch (CommonRemoveException e) {
			throw new NuclosUpdateException(e);
		}
	}

	/**
	 * method to modify and change state of a given object
	 * 
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param moduleUid module UID for plausibility check
	 * @param gowdvo object to change status for
	 * @param targetStateUid legal subsequent status UID to set for given object
	 * @throws NuclosCompileException 
	 */
	@RolesAllowed("Login")
	public void changeStateAndModifyByUser(final UID moduleUid,
			final GenericObjectWithDependantsVO gowdvo, final UID targetStateUid, final String customUsage)
					throws NuclosBusinessException, CommonPermissionException, CommonPermissionException, CommonCreateException,
					NuclosSubsequentStateNotLegalException, CommonFinderException, CommonRemoveException,
					CommonStaleVersionException, CommonValidationException, CommonFatalException, NuclosCompileException {

		checkWriteAllowedForModule(moduleUid);
		genericObjectFacade.modify(gowdvo, customUsage);
		changeState(moduleUid, gowdvo.getId(), targetStateUid, false, customUsage);
	}

	private StateTransitionVO createStateTransition(StateTransitionVO vo, final Map<UID, UID> states) throws CommonPermissionException, CommonFinderException, NuclosBusinessRuleException, CommonCreateException {
		if (vo.getStateSourceUID() != null)								//newly inserted state referenced?
			vo.setStateSource(states.get(vo.getStateSourceUID()));		//map newly inserted state temp id to real state id
		if (vo.getStateTargetUID() != null)								//newly inserted state referenced?
			vo.setStateTarget(states.get(vo.getStateTargetUID()));		//map newly inserted state temp id to real state id

		MasterDataVO<UID> wrapStateTransitionVO = MasterDataWrapper.wrapStateTransitionVO(vo);
		MasterDataVO<UID> mdVO = getMasterDataFacade().create(wrapStateTransitionVO, null);
		vo.setPrimaryKey(mdVO.getPrimaryKey());
		
		createStateTransitionDependents(vo);
		
		return vo;
	}

	private void createStateTransitionDependents(StateTransitionVO vo) throws NuclosBusinessRuleException, CommonCreateException, CommonPermissionException {
		for (EventSupportTransitionVO retVO : vo.getRules()) {
			MasterDataVO<UID> mdVO = MasterDataWrapper.wrapEventSupportTransitionVO(
					new EventSupportTransitionVO(new NuclosValueObject<UID>(), retVO.getEventSupportClass(), retVO.getEventSupportClassType(), vo.getPrimaryKey(), retVO.getOrder()));
			getMasterDataFacade().create(mdVO, null);
		}
		for (final UID roleuid : vo.getRoleUIDs()) {
			MasterDataVO<UID> mdVO = MasterDataWrapper.wrapRoleTransitionVO(
					new RoleTransitionVO(new NuclosValueObject<UID>(), vo.getPrimaryKey(), roleuid));
			getMasterDataFacade().create(mdVO, null);
		}
	}
	
	private boolean updateStateTransitionRoles(StateTransitionVO transitionvo) throws NuclosBusinessRuleException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException, CommonCreateException {
		boolean updated = false;
		
		// ROLETRANSITION insert, update, delete only if needed
		Collection<MasterDataVO<UID>> rolesDb = findTransitionRolesByTransition(transitionvo.getPrimaryKey());
		Set<UID> rolesDbVo = new HashSet<UID>();
		for (MasterDataVO<UID> roleDb : rolesDb) {
			UID found = null;
			for (UID roleuid : transitionvo.getRoleUIDs()) {
				if (RigidUtils.equal(roleuid, roleDb.getFieldUid(E.ROLETRANSITION.role))) {
					found = roleuid;
					break;
				}
			}
			if (found != null) {
				// (update) - nothing to do
				rolesDbVo.add(found);
			} else {
				// delete
				getMasterDataFacade().remove(E.ROLETRANSITION.getUID(), roleDb.getPrimaryKey(), false, null);
				updated = true;
			}
		}
		for (UID roleuid : transitionvo.getRoleUIDs()) {
			if (rolesDbVo.contains(roleuid)) {
				continue;
			}
			// insert
			RoleTransitionVO newRoletransitionvo = new RoleTransitionVO(new NuclosValueObject<UID>(), transitionvo.getPrimaryKey(), roleuid);
			getMasterDataFacade().create(MasterDataWrapper.wrapRoleTransitionVO(newRoletransitionvo),  null);
			updated = true;
		}

		return updated;
	}
	
	private boolean updateStateTransitionRules(StateTransitionVO transitionvo) throws NuclosBusinessRuleException, CommonCreateException, CommonPermissionException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonValidationException {
		boolean updated = false;
		
		// SERVERCODETRANSITION insert, update, delete only if needed
		Collection<MasterDataVO<UID>> rulesDb = findTransitionRulesByTransition(transitionvo.getPrimaryKey());
		Collection<EventSupportTransitionVO> rulesDbVo = new ArrayList<EventSupportTransitionVO>();
		for (MasterDataVO<UID> ruleDb : rulesDb) {
			EventSupportTransitionVO found = null;
			for (EventSupportTransitionVO eventsupporttransitionvo : transitionvo.getRules()) {
				if (RigidUtils.equal(eventsupporttransitionvo.getEventSupportClassType(), ruleDb.getFieldValue(E.SERVERCODETRANSITION.type)) &&
					RigidUtils.equal(eventsupporttransitionvo.getEventSupportClass(), ruleDb.getFieldValue(E.SERVERCODETRANSITION.classField))) {
					found = eventsupporttransitionvo;
					break;
				}
			}
			if (found != null) {
				// update
				rulesDbVo.add(found);
				MasterDataVO<UID> wrapEventSupportTransitionVO = MasterDataWrapper.wrapEventSupportTransitionVO(found);
				Integer order = wrapEventSupportTransitionVO.getFieldValue(E.SERVERCODETRANSITION.order);
				if (!RigidUtils.equal(order, ruleDb.getFieldValue(E.SERVERCODETRANSITION.order))) {
					ruleDb.setFieldValue(E.SERVERCODETRANSITION.order, order);
					getMasterDataFacade().modify(ruleDb, null);
					updated = true;
				}
			} else {
				// delete
				getMasterDataFacade().remove(E.SERVERCODETRANSITION.getUID(), ruleDb.getPrimaryKey(), false, null);
				updated = true;
			}
		}
		for (EventSupportTransitionVO eventsupporttransitionvo : transitionvo.getRules()) {
			if (rulesDbVo.contains(eventsupporttransitionvo)) {
				continue;
			}
			// insert
			EventSupportTransitionVO newEventsupporttransitionvo = new EventSupportTransitionVO(new NuclosValueObject<UID>(), 
					eventsupporttransitionvo.getEventSupportClass(), eventsupporttransitionvo.getEventSupportClassType(), 
					transitionvo.getPrimaryKey(), eventsupporttransitionvo.getOrder());
			getMasterDataFacade().create(MasterDataWrapper.wrapEventSupportTransitionVO(newEventsupporttransitionvo),  null);
			updated = true;
		}
		
		return updated;
	}

	private Collection<RoleTransitionVO> getAllRoleTransitionsForTransitionId(final UID stateTransitionUid) throws CommonPermissionException {
		checkReadAllowed(E.ROLE);
		Collection<RoleTransitionVO> result = new HashSet<RoleTransitionVO>();

		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.ROLETRANSITION.transition, ComparisonOperator.EQUAL, stateTransitionUid);
		final Collection<MasterDataVO<UID>> mdVOList = getMasterDataFacade().getMasterData(E.ROLETRANSITION, cond);

		for (MasterDataVO<UID> mdVO : mdVOList)
			result.add(MasterDataWrapper.getRoleTransitionVO(mdVO));

		return result;
	}

	/**
	 * returns a StateTransitionVO for the given stateTransition UID
	 */
	@Deprecated
	public StateTransitionVO findStateTransitionByUID(final UID stateTransitionUid) {
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.STATETRANSITION.getUID(), SF.PK_UID, ComparisonOperator.EQUAL, stateTransitionUid);

		return findStateTransition(cond,true,false).iterator().next();
	}

	/**
	 * returns a StateTransitionVO for the given sourceStateUid
	 */
	public Collection<StateTransitionVO> findStateTransitionBySourceState(final UID sourceStateUid) {
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.STATETRANSITION.state1, ComparisonOperator.EQUAL, sourceStateUid);

		return findStateTransition(cond,false,false);
	}

	/**
	 * returns a StateTransitionVO for the given sourceStateUid without automatic
	 */
	public Collection<StateTransitionVO> findStateTransitionBySourceStateNonAutomatic(final UID sourceStateUid) {
		final CollectableComparison condSource = SearchConditionUtils.newUidComparison(E.STATETRANSITION.state1, ComparisonOperator.EQUAL, sourceStateUid);
		final CollectableComparison condAuto = SearchConditionUtils.newComparison(E.STATETRANSITION.automatic, ComparisonOperator.EQUAL, false);

		return findStateTransition(new CompositeCollectableSearchCondition(LogicalOperator.AND, CollectionUtils.asSet(condSource,condAuto)),false,false);
	}

	/**
	 * returns a StateTransitionVO for the given targetStateId without a targetStateUid
	 */
	public StateTransitionVO findStateTransitionByNullAndTargetState(final UID targetStateUid) {
		final CollectableComparison condTarget = SearchConditionUtils.newUidComparison(E.STATETRANSITION.state2, ComparisonOperator.EQUAL, targetStateUid);

		final Set<CollectableSearchCondition> conditions = new HashSet<CollectableSearchCondition>();
		conditions.add(SearchConditionUtils.newIsNullCondition(E.STATETRANSITION.state1));
		conditions.add(condTarget);

		final Collection<StateTransitionVO> stateTransitions =  findStateTransition(
				new CompositeCollectableSearchCondition(LogicalOperator.AND,conditions),true,true);

		return stateTransitions.isEmpty() ? null : stateTransitions.iterator().next();
	}

	/**
	 * returns a StateTransitionVO for the given sourceStateId and targetStateId
	 */
	public StateTransitionVO findStateTransitionBySourceAndTargetState(final UID sourceStateUid, final UID targetStateUid) {
		final CollectableComparison condSource = SearchConditionUtils.newUidComparison(E.STATETRANSITION.state1, ComparisonOperator.EQUAL, sourceStateUid);
		final CollectableComparison condTarget = SearchConditionUtils.newUidComparison(E.STATETRANSITION.state2, ComparisonOperator.EQUAL, targetStateUid);

		final Collection<StateTransitionVO> stateTransitions =  findStateTransition(
				new CompositeCollectableSearchCondition(LogicalOperator.AND,CollectionUtils.asSet(condSource,condTarget)),true,false);

		return stateTransitions.isEmpty() ? null : stateTransitions.iterator().next();
	}
	
	
	private List<StateTransitionVO> findStateTransition(CollectableSearchCondition cond, boolean checkUniqueness, boolean withDependants) {
		return stateCache.getTransitionsByFindKey(new FindKey(cond, checkUniqueness, withDependants), this);
	}

	@Override
	public List<StateTransitionVO> lookupStateTransition(CollectableSearchCondition cond, boolean checkUniqueness, boolean withDependants) {
		
		List<StateTransitionVO> result = new ArrayList<StateTransitionVO>();
		if (withDependants) {
			final Collection<MasterDataVO<UID>> mdList = getMasterDataFacade().getWithDependantsByCondition(E.STATETRANSITION.getUID(), cond, null);

			if (mdList != null && !mdList.isEmpty()) {
				if (checkUniqueness && mdList.size() > 1)
					throw new CommonFatalException("statemodel.error.notunique.transition");

				for (final MasterDataVO<UID> mdVO : mdList) {
					result.add(MasterDataWrapper.getStateTransitionVO(mdVO));
				}
			}
		} else {
			final Collection<MasterDataVO<UID>> mdList = getMasterDataFacade().getMasterData(E.STATETRANSITION, cond);

			if (mdList != null && !mdList.isEmpty()) {
				if (checkUniqueness && mdList.size() > 1)
					throw new CommonFatalException("statemodel.error.notunique.transition");

				for (final MasterDataVO<UID> mdVO : mdList) {
					result.add(MasterDataWrapper.getStateTransitionVOWithoutDependants(mdVO));
				}
			}
		}
		return result;
	}

	/**
	 * returns the StateModelVO for the given UID
	 */
	public StateModelVO findStateModelByUID(final UID stateModelUid) throws CommonPermissionException, CommonFinderException, NuclosBusinessException {
		return MasterDataWrapper.getStateModelVO(getMasterDataFacade().get(E.STATEMODEL, stateModelUid), getStateGraph(stateModelUid));
	}

	/**
	 * returns a Collection of StateModelVO which contains rule-transitions with the given ruleId
	 */
	public Collection<StateModelVO> findStateModelsByRule(final String ruleClass) throws CommonPermissionException {
		
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<UID> query = builder.createQuery(UID.class);
		final DbFrom s = query.from(E.STATE, "s");
		final DbFrom t = s.join(E.STATETRANSITION, JoinType.INNER, "t").on(SF.PK_UID, E.STATETRANSITION.state2);
		final DbFrom rt = t.join(E.SERVERCODETRANSITION, JoinType.INNER, "rt").on(SF.PK_UID, E.SERVERCODETRANSITION.transition);
		query.select(s.baseColumn(E.STATE.model));
		query.where(builder.equalValue(rt.baseColumn(E.SERVERCODETRANSITION.classField), ruleClass));

		final List<StateModelVO> statemodels = new ArrayList<StateModelVO>();

		try {
			for (final UID stateUid : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				statemodels.add(findStateModelByUID(stateUid));
			}
		} catch (CommonFinderException ex) {
			// Dateninkonsistenz?
			throw new CommonFatalException(ex);
		} catch (NuclosBusinessException ex) {
			// Dateninkonsistenz?
			throw new CommonFatalException(ex);
		}
		
		return statemodels;
	}

	/**
	 * returns a Collection of StateHistories for the given genericObjectId
	 */
	public Collection<StateHistoryVO> findStateHistoryByGenericObjectId(final Long genericObjectId)	{
		final List<StateHistoryVO> histories = new ArrayList<StateHistoryVO>();
		final CollectableComparison cond = SearchConditionUtils.newIdComparison(E.STATEHISTORY.genericObject, ComparisonOperator.EQUAL, genericObjectId);
		final Collection<MasterDataVO<Long>> stateHistories = getMasterDataFacade().getMasterData(E.STATEHISTORY, cond);

		for (final MasterDataVO<Long> mdVO : stateHistories) {
			final StateVO stateVO = StateCache.getInstance().getState(mdVO.getFieldUid(E.STATEHISTORY.state));
			histories.add(MasterDataWrapper.getStateHistoryVO(mdVO, stateVO.getNumeral() + " " + stateVO.getStatename(Locale.ENGLISH)));
		}

		return histories;
	}

	private void changeState(final UID moduleUid, final Long genericObjectId, final UID targetStateUid,
			boolean bGetAutomaticStatesAlso, final String customUsage)
			throws NuclosSubsequentStateNotLegalException,
			NuclosBusinessException, CommonFinderException, CommonPermissionException, CommonCreateException,
			NuclosCompileException, CommonValidationException, CommonStaleVersionException, CommonRemoveException {
		
		// @todo OPTIMIZE: Must this be done here?
		checkTargetState(moduleUid, genericObjectId, bGetAutomaticStatesAlso, targetStateUid);

		changeState(moduleUid, genericObjectId, targetStateUid, customUsage);
	}

	/**
	 * method to change the status of a given leased object
	 * @param moduleUid module id for plausibility check
	 * @param genericObjectId leased object id to change status for
	 * @param targetStateUid legal subsequent status id to set for given leased object
	 * @throws CommonRemoveException 
	 * @throws CommonStaleVersionException 
	 * @throws CommonValidationException 
	 * @throws NuclosCompileException 
	 */
	private void changeState(final UID moduleUid, final Long genericObjectId, final UID targetStateUid,
			final String customUsage)
			throws NuclosBusinessException, CommonFinderException, CommonCreateException, CommonPermissionException,
			NuclosCompileException, CommonValidationException, CommonStaleVersionException, CommonRemoveException {

		GenericObjectVO loadedGoVo = getGenericObjectFacade().get(moduleUid, genericObjectId);
		GenericObjectWithDependantsVO goVo = new GenericObjectWithDependantsVO(loadedGoVo, 
//				getGenericObjectFacade().reloadDependants(loadedGoVo, null, true, customUsage), loadedGoVo.getDataLanguageMap());
				new DependentDataMap(), loadedGoVo.getDataLanguageMap());
		changeState(moduleUid, goVo, targetStateUid, customUsage);
	}

	private GenericObjectWithDependantsVO changeState(final UID moduleUid, GenericObjectWithDependantsVO govo,
			final UID targetStateUid, final String customUsage)
			throws NuclosBusinessException, CommonCreateException, CommonPermissionException, CommonFinderException,
			NuclosCompileException, CommonValidationException, CommonStaleVersionException, CommonRemoveException {

		final EntityMeta<?> entity = metaProvider.getEntity(moduleUid);
		
		final UID iAttributeStateId = SF.STATE.getUID(entity);
		final UID iAttributeStatenumberId =  SF.STATENUMBER.getUID(entity);
	
		DynamicAttributeVO attrState = govo.getAttribute(iAttributeStateId);
		DynamicAttributeVO attrStatenumber = govo.getAttribute(iAttributeStatenumberId);
		final UID sourceStateUid = attrState == null? null: attrState.getValueUid();

		StateVO stateVO = stateCache.getState(targetStateUid);

		// copy status name to leased object attributes:
		try {
			final GenericObjectMetaDataCache prov = GenericObjectMetaDataCache.getInstance();

			if (attrState == null) {
				attrState = new DynamicAttributeVO(iAttributeStateId, null, null, null);
				govo.addAndSetAttribute(attrState);
			}
			if (attrStatenumber == null) {
				attrStatenumber = new DynamicAttributeVO(iAttributeStatenumberId, null, null, null);
				govo.addAndSetAttribute(attrStatenumber);
			}
	
			attrState.setParsedValue(stateVO.getStatename(Locale.ENGLISH), prov);
			attrState.setValueUid(stateVO.getPrimaryKey());
			attrStatenumber.setParsedValue(stateVO.getNumeral(), prov);
			attrStatenumber.setValueUid(stateVO.getPrimaryKey());
			
			fireStateChangedEvent(govo, sourceStateUid, targetStateUid, customUsage);
		}
		catch (CommonValidationException e) {
			throw new NuclosFatalException(e);
		}
		
		return govo;
	}

	public GenericObjectWithDependantsVO changeState(final UID moduleUid, EntityObjectVO<Long> eovo,
			final UID targetStateUid, final String customUsage)
			throws NuclosBusinessException, CommonCreateException, CommonPermissionException, CommonFinderException,
			NuclosCompileException, CommonValidationException, CommonStaleVersionException, CommonRemoveException {

		GenericObjectWithDependantsVO govo = 
				DalSupportForGO.getGenericObjectWithDependants(eovo);
		
		final EntityMeta<?> entity = metaProvider.getEntity(moduleUid);
		final GenericObjectMetaDataCache prov = GenericObjectMetaDataCache.getInstance();
		
		StateVO stateVO = stateCache.getState(targetStateUid);
		
		final UID iAttributeStateId = SF.STATE.getUID(entity);
		final UID iAttributeStatenumberId =  SF.STATENUMBER.getUID(entity);
	
		DynamicAttributeVO attrState = govo.getAttribute(iAttributeStateId);
		DynamicAttributeVO attrStatenumber = govo.getAttribute(iAttributeStatenumberId);
		
		attrState.setParsedValue(stateVO.getStatename(Locale.ENGLISH), prov);
		attrState.setValueUid(stateVO.getPrimaryKey());
		attrStatenumber.setParsedValue(stateVO.getNumeral(), prov);
		attrStatenumber.setValueUid(stateVO.getPrimaryKey());
		
		govo = genericObjectFacade.modify(govo, false);
		
		fireStateChangedEvent(govo, attrState.getValueUid(), targetStateUid, customUsage);
		
		return govo;
	}
	
	
	/**
	 * checks if the given target state id is contained in the list of subsequent states for the given leased objects:
	 * @param moduleUid
	 * @param genericObjectId
	 * @param targetStateUid
	 * @throws NuclosNoAdequateStatemodelException
	 */
	public boolean checkTargetState(final UID moduleUid, final Long genericObjectId, final UID targetStateUid)
			throws CommonFinderException, CommonPermissionException {
		try {
			checkTargetState(moduleUid, genericObjectId, false, targetStateUid);
		} catch (NuclosSubsequentStateNotLegalException e) {
			return false;
		}
		return true;
	}

	/**
	 * checks if the given target state UID is contained in the list of subsequent states for the given leased objects:
	 * @param moduleUid
	 * @param genericObjectId
	 * @param bGetAutomaticStatesAlso
	 * @param targetStateUid
	 * @throws NuclosNoAdequateStatemodelException
	 * @throws NuclosSubsequentStateNotLegalException
	 */
	private void checkTargetState(final UID moduleUid, final Long genericObjectId, boolean bGetAutomaticStatesAlso,
			final UID targetStateUid)
					throws NuclosSubsequentStateNotLegalException, CommonFinderException, CommonPermissionException {

		if (!bGetAutomaticStatesAlso) {
			// no automatic states... this is a external user call for a state change
			// and we have to check the possible grants here. 
			grantUtils.checkStateChange(moduleUid, genericObjectId);
			lockUtils.checkLockedByCurrentUser(moduleUid, genericObjectId);
		}
		
		boolean bLegal = false;
		
		for (StateVO statevo : getSubsequentStates(moduleUid, genericObjectId, bGetAutomaticStatesAlso)) {
			final UID stateUid = statevo.getPrimaryKey();
			if (ObjectUtils.equals(stateUid,targetStateUid)) {
				bLegal = true;
				break;
			}
		}
		if (!bLegal) {
			StateVO statevoCurrent = getCurrentState(moduleUid, genericObjectId); 
			StateVO statevoTarget = getState(targetStateUid);

			if(statevoCurrent.getId().equals(statevoTarget.getId())){
				throw new NuclosSubsequentStateNotLegalException("Das Objekt befindet sich bereits in dem  Status \"" + statevoTarget.getStatename(Locale.GERMANY)+"\"" );
			}
			else{				
				throw new NuclosSubsequentStateNotLegalException("Das Objekt konnte nicht in den Status \"" + statevoTarget.getStatename(Locale.GERMANY) + "\" überführt werden, da dieser kein gültiger Folgestatus von \" "+statevoCurrent.getStatename(Locale.GERMANY) +"\" ist, in welchem sich das Objekt mit der id "+ genericObjectId+" befindet.");
			}
		}
	}

	private UID getTargetStateId(final UID moduleUid, final Long genericObjectId, int iNumeral)
			throws NuclosSubsequentStateNotLegalException, CommonFinderException, CommonPermissionException {

		UID result = null;

		for (StateVO statevo : getSubsequentStates(moduleUid, genericObjectId, true)) {
			UID stateUid = statevo.getPrimaryKey();
			if ((statevo.getNumeral() != null) && (statevo.getNumeral().equals(new Integer(iNumeral)))) {
				result = stateUid;
				break;
			}
		}
		if (result == null) {
			throw new NuclosSubsequentStateNotLegalException("Requested target status numeral " + iNumeral + " is not a legal subsequent status for genericobject id " + genericObjectId);
		}
		return result;
	}

	private void fireStateChangedEvent(GenericObjectWithDependantsVO govo, final UID sourceStateUid,
			final UID targetStateUid, final String customUsage)
			throws CommonFinderException, CommonPermissionException,
			NuclosCompileException, CommonValidationException, CommonStaleVersionException, NuclosBusinessException,
			CommonCreateException, CommonRemoveException {

		EntityObjectVO<Long> validation = getEventSupportFacade().fireStateTransitionEventSupport(
				sourceStateUid, targetStateUid, DalSupportForGO.wrapGenericObjectVO(govo),
				StateChangeRule.class.getCanonicalName());

		// check mandatory fields and subform columns
		validation.setFieldUid(SF.STATE.getUID(validation.getDalEntity()), targetStateUid);
		validationSupport.validate(validation, null);
		govo = DalSupportForGO.getGenericObjectWithDependantsVO(validation);

		// Write back the possibly changed GenericObjectVO along with its dependants.
		// We deliberately allow changing the GenericObjectVO from the rule in "state change" events
		// so the rule writer doesn't have to distinguish them from other events (like "save").
		// Note that the modification of the leased object does not fire another save event here:
		GenericObjectWithDependantsVO modifiedGoVO = getGenericObjectFacade().modify(govo, false, customUsage);

		getEventSupportFacade().fireStateTransitionEventSupport(
				sourceStateUid, targetStateUid, DalSupportForGO.wrapGenericObjectVO(modifiedGoVO),
				StateChangeFinalRule.class.getCanonicalName());
	}
	
	private Collection<MasterDataVO<UID>> findTransitionRulesByTransition(final UID transitionUid) {
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.SERVERCODETRANSITION.transition, ComparisonOperator.EQUAL, transitionUid);
		final Collection<MasterDataVO<UID>> mdRules = getMasterDataFacade().getMasterData(E.SERVERCODETRANSITION, cond);
		return mdRules;
	}
	
	private Collection<MasterDataVO<UID>> findTransitionRolesByTransition(final UID transitionUid) {
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.ROLETRANSITION.transition, ComparisonOperator.EQUAL, transitionUid);
		final Collection<MasterDataVO<UID>> mdRoles = getMasterDataFacade().getMasterData(E.ROLETRANSITION, cond);
		return mdRoles;
	}

	private Collection<MasterDataVO<UID>> findAttributegroupPermissionsByState(final UID stateUid) {
//		List<AttributegroupPermissionVO> permissions = new ArrayList<AttributegroupPermissionVO>();

		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.ROLEATTRIBUTEGROUP.state, ComparisonOperator.EQUAL, stateUid);
		final Collection<MasterDataVO<UID>> mdPermissions = getMasterDataFacade().getMasterData(E.ROLEATTRIBUTEGROUP, cond);

		return mdPermissions;
//		for (MasterDataVO<UID> mdVO : mdPermissions)
//			permissions.add(MasterDataWrapper.getAttributegroupPermissionVO(mdVO));
//
//		return permissions;
	}

	private List<UID> findRulesByTransition(UID transUID) throws CommonFinderException, CommonPermissionException {
		
		List<UID> retVal = new ArrayList<UID>();
		
		final MasterDataVO<UID> mdTransitionToDelete = 
				getMasterDataFacade().get(E.STATETRANSITION, transUID);
		
		final Collection<MasterDataVO<UID>> mdTransitionRulesToDelete = getMasterDataFacade().getMasterData(E.SERVERCODETRANSITION, 
				SearchConditionUtils.newUidComparison(E.SERVERCODETRANSITION.transition, ComparisonOperator.EQUAL, mdTransitionToDelete.getPrimaryKey()));
		for (MasterDataVO<UID> rule : mdTransitionRulesToDelete) {
			if (!retVal.contains(rule.getPrimaryKey()))
				retVal.add(rule.getPrimaryKey());
		}
		
		return retVal;
	}
	
	private Collection<MasterDataVO<UID>> findSubformPermissionsByState(final UID stateUid) {

		final Collection<MasterDataVO<UID>> mdPermissions = getMasterDataFacade().getMasterData(E.ROLESUBFORM, 
				SearchConditionUtils.newUidComparison(E.ROLESUBFORM.state, ComparisonOperator.EQUAL, stateUid));
		return mdPermissions;
	}

	private Collection<MasterDataVO<UID>> findSubformGroupPermissionsBySubformPermission(final UID roleSubformUid) {
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.ROLESUBFORMGROUP.rolesubform, ComparisonOperator.EQUAL, roleSubformUid);
		final Collection<MasterDataVO<UID>> mdPermissions = getMasterDataFacade().getMasterData(E.ROLESUBFORMGROUP, cond);
		return mdPermissions;
	}

	public Set<MandatoryFieldVO> findMandatoryFieldsByStateUID(final UID stateUid) {
		final Set<MandatoryFieldVO> mandatory = new HashSet<MandatoryFieldVO>();

		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.STATEMANDATORYFIELD.state, ComparisonOperator.EQUAL, stateUid);
		final Collection<MasterDataVO<UID>> mdPermissions = getMasterDataFacade().getMasterData(E.STATEMANDATORYFIELD, cond);
		for (MasterDataVO<UID> mdVO : mdPermissions)
			mandatory.add(MasterDataWrapper.getMandatoryFieldVO(mdVO));

		return mandatory;
	}
	
	public Collection<MasterDataVO<UID>> findMandatoryFieldsMDByStateUID(final UID stateUid) {
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.STATEMANDATORYFIELD.state, ComparisonOperator.EQUAL, stateUid);
		final Collection<MasterDataVO<UID>> mdPermissions = getMasterDataFacade().getMasterData(E.STATEMANDATORYFIELD, cond);
		return mdPermissions;
	}

	public Set<MandatoryColumnVO> findMandatoryColumnsByStateUID(final UID stateUid) {
		final Set<MandatoryColumnVO> mandatory = new HashSet<MandatoryColumnVO>();
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.STATEMANDATORYCOLUMN.state, ComparisonOperator.EQUAL, stateUid);
		final Collection<MasterDataVO<UID>> mdPermissions = getMasterDataFacade().getMasterData(E.STATEMANDATORYCOLUMN, cond);
		for (MasterDataVO<UID> mdVO : mdPermissions)
			mandatory.add(MasterDataWrapper.getMandatoryColumnVO(mdVO));

		return mandatory;
	}
	
	public Collection<MasterDataVO<UID>> findMandatoryColumnsMDByStateUID(final UID stateUid) {
		final CollectableComparison cond = SearchConditionUtils.newUidComparison(E.STATEMANDATORYCOLUMN.state, ComparisonOperator.EQUAL, stateUid);
		final Collection<MasterDataVO<UID>> mdPermissions = getMasterDataFacade().getMasterData(E.STATEMANDATORYCOLUMN, cond);
		return mdPermissions;
	}

	public void invalidateCache() {
		LOG.debug("invalidatesCache: status cache and state model usages cache invalidated/revalidated");
		StateModelUsagesCache.getInstance().invalidateCache(true, true);
		stateCache.invalidateCache(true, true);

		LOG.debug("JMS send: notify clients that status models changed: {}", this);
		NuclosJMSUtils.sendOnceAfterCommitDelayed(null, JMSConstants.TOPICNAME_STATEMODEL);
	}
	
	@Override
	public Map<FieldMeta<?>, String> getResourceSIdsForStateUid(UID stateUid) {
		return stateCache.getResourceSIdsForStateUid(stateUid);
	}

	private List<Statemodel> getStatemodels(Set<UsageCriteria> usageCriterias) {
		List<Statemodel> lstStateModels = new ArrayList<Statemodel>();
		
		for (UsageCriteria uc : usageCriterias) {
			Statemodel res = new Statemodel(uc);
			res.setAllStates(stateCache.getStatesByModule(uc.getEntityUID()));
			res.setInitialState(getInitialStateId(uc));
			
			for(final UID stateUid : res.getStateUIDs()) {
				res.setTransitionsForState(stateUid, findStateTransitionBySourceState(stateUid));
				Map<FieldMeta<?>, String> mpResourceIds = getResourceSIdsForStateUid(stateUid);
				res.setResourceSIDsForState(stateUid, mpResourceIds.get(E.STATE.labelres), mpResourceIds.get(E.STATE.descriptionres));
			}
			res.setUserTransitionIDs(SecurityCache.getInstance().getTransitionUids(utils.getCurrentUserName(), getCurrentMandatorUID()));
			assert(res.isComplete());
			
			lstStateModels.add(res);
			
		}
		return lstStateModels;
	}

	private void setStatusModelIdAndName(final StatemodelClosure closure) {
		closure.getAllStates().forEach(state -> {
			StateModelVO statusModel;
			try {
				statusModel = stateCache.getModelByState(state);
				closure.getStatusModelNameById().put(
						statusModel.getId(),
						statusModel.getName()
				);
			} catch (CommonBusinessException e) {
				LOG.warn(e.getMessage(), e);
			}
		});
	}

	/**
	 * TODO: MULTINUCLET statemodel closure for state??
	 * TODO: cache this (tp).
	 */
	@Override
	public StatemodelClosure getStatemodelClosureForEntity(final UID entityUid) {
	
		Set<UsageCriteria> usageCrits = new HashSet<UsageCriteria>();
		
		final StateModelUsages stateUsages = StateModelUsagesCache.getInstance().getStateUsages();
		final Set<UID> stateModelsForEntity = stateUsages.getStateModelUIDsByEntity(entityUid);
		for(UID modelUid : stateModelsForEntity) {
			Set<UsageCriteria> ucs = stateUsages.getUsageCriteriasByStateModelUID(modelUid);
			for (UsageCriteria uc : ucs) {
				if (uc.getEntityUID().equals(entityUid)) {
					usageCrits.add(uc);
				}
			}
		}
		
		final StatemodelClosure res = new StatemodelClosure(entityUid);
		res.addStatemodels(getStatemodels(usageCrits));
		setStatusModelIdAndName(res);
		
		return res;
	}

	public List<StateTransitionVO> getOrderedStateTransitionsByStatemodel(final UID stateModelUid) {
		return this.stateCache.getOrderedTransitionsByStatemodel(stateModelUid);
	}
	
	public void createStatemodelObjects()
		throws NuclosCompileException, CommonBusinessException, InterruptedException {
		// create statemodel classes
		this.statemodelObjectBuilder.createObjects();
	}

	/**
	 * copies the permissions of an statemodel from one given role.
	 * @param roleToCopy
	 * @param roleToCopyTo
	 */ 
	public void copyRolePermissions(UID roleToCopy, UID roleToCopyTo) throws NuclosBusinessException {
		try {
			for (StateModelVO stateModel : getStateModels()) {
				boolean bChanged = false;
				
				final StateGraphVO stateGraphVO = getStateGraph(stateModel.getId());
				for (StateVO stateVO : stateGraphVO.getStates()) {
					final List<AttributegroupPermissionVO> userRights = stateVO.getUserRights().getValues(roleToCopy);
					if (userRights != null) {
						stateVO.getUserRights().addAllValues(roleToCopyTo, new ArrayList<AttributegroupPermissionVO>(userRights));
						bChanged = true;
					}
					final List<SubformPermissionVO> userSubformRights = stateVO.getUserSubformRights().getValues(roleToCopy);
					if (userSubformRights != null) {
						stateVO.getUserSubformRights().addAllValues(roleToCopyTo, new ArrayList<SubformPermissionVO>(userSubformRights));
						bChanged = true;
					}
				}
				
				for(StateTransitionVO stateTransitionVO : stateGraphVO.getTransitions()) {
					if (stateTransitionVO.getRoleUIDs().contains(roleToCopy)) {
						stateTransitionVO.getRoleUIDs().add(roleToCopyTo);
						bChanged = true;
					}
				}
				
				if (bChanged) 
					updateStateGraph(stateGraphVO, stateModel);
			}

			// just to be sure.
			SecurityCache.getInstance().invalidate();
			StateCache.getInstance().invalidateCache(true, true);
			StateModelUsagesCache.getInstance().invalidateCache(true, true);
			esCache.invalidate(E.STATEMODEL);
			
		} catch (CommonRemoveException e) {
			throw new NuclosBusinessException(e);
		} catch (CommonStaleVersionException e) {
			throw new NuclosBusinessException(e);
		} catch (CommonValidationException e) {
			throw new NuclosBusinessException(e);
		} catch (CommonPermissionException e) {
			throw new NuclosBusinessException(e);
		} catch (CommonCreateException e) {
			throw new NuclosBusinessException(e);
		} catch (CommonFinderException e) {
			throw new NuclosBusinessException(e);
		}
	}
}
