package org.nuclos.server.statemodel;

import java.util.Collection;
import java.util.List;

import org.nuclos.api.businessobject.facade.thin.Stateful;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.StatemodelService;
import org.nuclos.api.statemodel.State;
import org.nuclos.common.E;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.nuclos.server.nbo.EOBOBridge;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.nuclos.server.statemodel.valueobject.StateModelUsages.StateModelUsage;
import org.nuclos.server.statemodel.valueobject.StateModelUsagesCache;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.stereotype.Component;

@Component("StatemodelServiceProvider")
public class StatemodelServerServiceProvider implements
		StatemodelService {

	@SuppressWarnings("deprecation")
	@Override
	public <T extends Stateful> void changeState(T t, int iNumeral) throws Exception {
		
		AbstractBusinessObject obj = (AbstractBusinessObject) t;
		
		GenericObjectVO genericObjectVO = DalSupportForGO.getGenericObjectVO(EOBOBridge.getEO(obj));
		StateFacadeLocal facade = ServerServiceLocator.getInstance().getFacade(StateFacadeLocal.class);
		
		// Find statemodels for this entity with fitting state and process
		UsageCriteria crit = 
				new UsageCriteria(genericObjectVO.getModule(), genericObjectVO.getProcess(), genericObjectVO.getStatus(), null);
		// Find state of this statemodel
		StateModelUsage stateModelUsage = StateModelUsagesCache.getInstance().getStateUsages().getStateModelUsage(crit);
		Collection<StateVO> statesByModel = facade.getStatesByModel(stateModelUsage.getStateModelUID());
		
		for (StateVO s : statesByModel) {
			// if this is our state - execute statechange
			if (s.getNumeral() != null && s.getNumeral().equals(iNumeral)) {
				facade.changeStateByRule(genericObjectVO.getModule(), genericObjectVO.getId(), s.getId(), null);
				DalSupportForGO.wrapGenericObjectVO(genericObjectVO);
				break;
			}
		}
		
	}

	@Override
	public <T extends Stateful> void changeState(T t, State sState) throws BusinessException {
		
		if (t == null) {
			throw new BusinessException("BusinessObject must not be null.");
		}
		if (sState == null) {
			throw new BusinessException("Status must not be null.");
		}		
		
		// Check if the given state is a valid state of the statemodel of the given BusinessObject
		if (!checkState(t, sState)) {
			throw new BusinessException("Status '" + sState.getName() + "' is not a valid target status of BusinessObject '" + t.getClass().getCanonicalName() + "'.");
		}
		
		AbstractBusinessObject obj = (AbstractBusinessObject) t;
		
		GenericObjectVO genericObjectVO = DalSupportForGO.getGenericObjectVO(EOBOBridge.getEO(obj));
		StateFacadeLocal facade = ServerServiceLocator.getInstance().getFacade(StateFacadeLocal.class);
		
		try {
			// Change state
			facade.changeStateByRule(genericObjectVO.getModule(), genericObjectVO.getId(), (UID) sState.getId(), null);
			
			// store new state in BusinessObject
			EOBOBridge.setEO(obj, DalSupportForGO.wrapGenericObjectVO(genericObjectVO));
		} catch (Exception e) {
			if (e.getCause() != null) {
				throw new BusinessException(e.getCause());
			} else {
				throw new BusinessException(e);
			}
		}
		
	}
	
	private <T extends Stateful> boolean checkState(T t, State sState) {
		
		boolean retVal = false;
		
		final EntityObjectVO<Long> eo = EOBOBridge.getEO((AbstractBusinessObject) t);
		
		final UID nuclosEntityUid = MetaProvider.getInstance().getEntity(eo.getDalEntity()).getUID();
		final UID statemodelUid =  (UID) sState.getModelId();
		final UID processUid = eo.getFieldUid(SF.PROCESS_UID);
		final UID stateSourceUid = eo.getFieldUid(SF.STATE_UID);
		final UID stateTargetUid = (UID) sState.getId();
		
		// now check if this statemodel is used with this entity in a given process
		CompositeCollectableSearchCondition andCond = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		// entityId
		andCond.addOperand(SearchConditionUtils.newUidComparison(E.STATEMODELUSAGE.nuclos_module, ComparisonOperator.EQUAL, nuclosEntityUid));
		// processId
		if (processUid != null) {
			andCond.addOperand(SearchConditionUtils.newUidComparison(E.STATEMODELUSAGE.process, ComparisonOperator.EQUAL, processUid));			
		}
		else {
			andCond.addOperand(SearchConditionUtils.newIsNullCondition(E.STATEMODELUSAGE.process));
		}
		// statemodelId
		andCond.addOperand(SearchConditionUtils.newUidComparison(E.STATEMODELUSAGE.statemodel, ComparisonOperator.EQUAL, statemodelUid));
		
		List<EntityObjectVO<UID>> results = NucletDalProvider.getInstance().getEntityObjectProcessor(E.STATEMODELUSAGE).getBySearchExpression(
				new CollectableSearchExpression(andCond));
		
		if ((results == null || results.size() == 0) && processUid != null) {
			// search for generic statemodel if no statemodel with process is found
			andCond = new CompositeCollectableSearchCondition(LogicalOperator.AND);
			andCond.addOperand(SearchConditionUtils.newUidComparison(E.STATEMODELUSAGE.nuclos_module, ComparisonOperator.EQUAL, nuclosEntityUid));
			andCond.addOperand(SearchConditionUtils.newIsNullCondition(E.STATEMODELUSAGE.process));
			andCond.addOperand(SearchConditionUtils.newUidComparison(E.STATEMODELUSAGE.statemodel, ComparisonOperator.EQUAL, statemodelUid));
			results = NucletDalProvider.getInstance().getEntityObjectProcessor(E.STATEMODELUSAGE).getBySearchExpression(new CollectableSearchExpression(andCond));
		}
		
		if (results != null && results.size() == 1) {
			// now check wether this is an valid statetransition
			CompositeCollectableSearchCondition andCondTransitions = new CompositeCollectableSearchCondition(LogicalOperator.AND);
			
			// target state must not be null
			andCondTransitions.addOperand(SearchConditionUtils.newUidComparison(E.STATETRANSITION.state2, ComparisonOperator.EQUAL, stateTargetUid));
			
			// target state may be null
			if (stateSourceUid != null) {
				andCondTransitions.addOperand(SearchConditionUtils.newUidComparison(E.STATETRANSITION.state1, ComparisonOperator.EQUAL, stateSourceUid));	
			}
			else {
				andCondTransitions.addOperand(SearchConditionUtils.newIsNullCondition(E.STATETRANSITION.state1));
			}
			
			final List<EntityObjectVO<UID>> transitions = NucletDalProvider.getInstance().getEntityObjectProcessor(E.STATETRANSITION).getBySearchExpression(
					new CollectableSearchExpression(andCondTransitions));

			if (transitions != null && transitions.size() == 1) {
				retVal = true;				
			}
		}
		
		return retVal;
	}

}
