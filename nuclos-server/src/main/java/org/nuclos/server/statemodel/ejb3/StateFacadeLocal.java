//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.ejb3;

import java.util.Collection;
import java.util.Set;

import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.statemodel.NuclosSubsequentStateNotLegalException;
import org.nuclos.server.statemodel.valueobject.MandatoryColumnVO;
import org.nuclos.server.statemodel.valueobject.MandatoryFieldVO;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateHistoryVO;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

// @Local
public interface StateFacadeLocal {

	/**
	 * gets a complete state graph for a state model
	 * @param stateModelUid id of state model to get graph for
	 * @return state graph cvo containing the state graph information for the model with the given id
	 * @throws CommonPermissionException
	 */
	StateGraphVO getStateGraph(final UID stateModelUid)
		throws CommonFinderException, CommonPermissionException,
		NuclosBusinessException;

	/**
	 * @param usagecriteria
	 * @return the initial state of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	StateVO getInitialState(UsageCriteria usagecriteria);

	/**
	 * @param stateModelUid
	 * @return the id of the statemodel corresponding to <code>usagecriteria</code>.
	 */
	Collection<StateVO> getStatesByModel(final UID stateModelUid);

	/**
	 * method to get all state models
	 * @return collection of state model vo
	 */
	Collection<StateModelVO> getStateModels() throws CommonFinderException, CommonPermissionException, NuclosBusinessException;

	/**
	 * Retrieve all states in all state models for the module with the given UID
	 * @param moduleUid id of module to retrieve states for
	 * @return Collection of all states for the given module
	 */
	Collection<StateVO> getStatesByModule(final UID moduleUid);

	/**
	 * method to return the actual state for a given leased object
	 * 
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param moduleUid module id for plausibility check
	 * @param genericObjectId id of leased object to get actual state for
	 * @return state id of actual state for given leased object
	 */
	StateVO getCurrentState(final UID moduleUid,
		final Long genericObjectId) throws CommonFinderException;

	/**
	 * method to return the possible subsequent states for a given leased object
	 * 
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param moduleUid module id for plausibility check
	 * @param genericObjectId id of leased object to get subsequent states for
	 * @param bGetAutomaticStatesAlso should method also return states for automatic only transitions? false for returning subsequent states to client, which generates buttons for manual state changes
	 * @return set of possible subsequent states for given leased object
	 */
	Collection<StateVO> getSubsequentStates(final UID moduleUid,
		final Long genericObjectId, boolean bGetAutomaticStatesAlso)
		throws CommonFinderException;

	/**
	 * method to change the status of a given leased object (called by server only)
	 * @param moduleUid module id for plausibility check
	 * @param genericObjectId leased object id to change status for
	 * @param iNumeral legal subsequent status numeral to set for given leased object
	 */
	void changeStateByRule(final UID moduleUid,
		final Long genericObjectId, int iNumeral, final String customUsage)
		throws NuclosSubsequentStateNotLegalException, NuclosBusinessException,
		CommonFinderException, CommonPermissionException,
		CommonCreateException;

	/**
	 * method to change the status of a given leased object (called by server only)
	 * @param moduleUid module id for plausibility check
	 * @param genericObjectId leased object id to change status for
	 * @param targetStateUid legal subsequent status UID to set for given leased object
	 */
	void changeStateByRule(final UID moduleUid,
		final Long genericObjectId, final UID targetStateUid, final String customUsage)
		throws NuclosSubsequentStateNotLegalException, NuclosBusinessException,
		CommonFinderException, CommonPermissionException,
		CommonCreateException;

	/**
	 * method to change the status of a given leased object
	 * 
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param moduleUid module id for plausibility check
	 * @param genericObjectId leased object id to change status for
	 * @param targetStateUid legal subsequent status id to set for given leased object
	 */
	void changeStateByUser(final UID moduleUid,
		final Long genericObjectId, final UID targetStateUid, final String customUsage)
		throws NuclosBusinessException, CommonPermissionException,
		CommonPermissionException, CommonCreateException,
		NuclosSubsequentStateNotLegalException, CommonFinderException;

	
	public GenericObjectWithDependantsVO changeState(final UID moduleUid, EntityObjectVO<Long> eovo,
			final UID targetStateUid, final String customUsage)
			throws NuclosBusinessException, CommonCreateException, CommonPermissionException, CommonFinderException,
			NuclosCompileException, CommonValidationException, CommonStaleVersionException, CommonRemoveException;
	
	/**
	 * 
	 * @param goVO
	 * @throws NuclosBusinessException
	 * @throws NuclosNoAdequateStatemodelException
	 * @throws CommonCreateException
	 * @throws CommonPermissionException
	 */
	void enterInitialState(GenericObjectWithDependantsVO goVO, String customUsage) 
			throws NuclosBusinessException, CommonCreateException, CommonPermissionException;
	
	/**
	 * returns a StateTransitionVO for the given transitionUid
	 */
	StateTransitionVO findStateTransitionByUID(
		final UID transitionUid);

	/**
	 * returns a StateTransitionVO for the given sourceStateId
	 */
	Collection<StateTransitionVO> findStateTransitionBySourceState(
		final UID sourceStateUid);

	/**
	 * returns a StateTransitionVO for the given sourceStateId without automatic
	 */
	Collection<StateTransitionVO> findStateTransitionBySourceStateNonAutomatic(
		final UID sourceStateUid);

	/**
	 * returns a StateTransitionVO for the given targetStateId without a sourceStateId
	 */
	StateTransitionVO findStateTransitionByNullAndTargetState(
		final UID targetStateUid);

	/**
	 * returns a StateTransitionVO for the given sourceStateId and targetStateId
	 */
	StateTransitionVO findStateTransitionBySourceAndTargetState(
		final UID sourceStateUid, final UID targetStateUid);

	/**
	 * returns the StateModelVO for the given UID
	 */
	StateModelVO findStateModelByUID(final UID stateModelUid)
		throws CommonPermissionException, CommonFinderException, NuclosBusinessException;

	
	/**
	 * returns a Collection of StateHistories for the given genericObjectId
	 */
	Collection<StateHistoryVO> findStateHistoryByGenericObjectId(
		final Long genericObjectId);

	/**
	 * 
	 * @param StateUid
	 * @return
	 */
	Set<MandatoryFieldVO> findMandatoryFieldsByStateUID(final UID StateUid);
	
	/**
	 * 
	 * @param StateUid
	 * @return
	 */
	Set<MandatoryColumnVO> findMandatoryColumnsByStateUID(final UID StateUid);

	/**
	 * set initial states for all objects
	 * @param entityUid entityuid
	 * @throws CommonCreateException
	 */
	void updateInitialStatesByEntity(final UID entityUid) throws CommonCreateException;
	
	/**
	 * Creates all statemodells
	 * @throws CommonBusinessException
	 */
	void createStatemodelObjects() throws NuclosCompileException, CommonBusinessException, InterruptedException;
	

	/**
	 * 
	 */
	void invalidateCache();
	
	StateVO getState(UID stateId);
	
	Collection<StateVO> getSubsequentStates(UID iModuleId,
			Long iGenericObjectId, boolean bGetAutomaticStatesAlso, UID iCurrentStateId)
			throws CommonFinderException;
	
	public Collection<StateVO> getSubsequentStates(UsageCriteria usagecriteria, boolean bGetAutomaticStatesAlso, UID iCurrentStateId) 
			throws CommonFinderException;
}
