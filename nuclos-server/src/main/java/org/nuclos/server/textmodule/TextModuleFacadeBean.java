//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.textmodule;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.textmodule.TextModule;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Facade bean for textmodules. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class TextModuleFacadeBean extends NuclosFacadeBean implements TextModuleFacadeLocal, TextModuleFacadeRemote {

	private static final int TEXT_MODULE_MENU_SIZE = 100;
	
	private MasterDataFacadeLocal mdFacade;
	
	@Autowired
	public void setMasterDataFacadeBean(MasterDataFacadeLocal mdFacade) {
		this.mdFacade = mdFacade;
	}

	/**
	 * find text module
	 *
	 * @param tms	{@link TextModuleSettings}
	 */
	public List<TextModule> findTextModules(final TextModuleSettings tms) {
		
		final List<TextModule> lstResult = new ArrayList<TextModule>();

		EntityMeta<?> entity = metaProvider.getEntity(tms.getEntity());
		List<MasterDataVO<?>> colMdvo = new ArrayList<>(mdFacade.getMasterData(entity, TrueCondition.TRUE));
		
		// FIXME should be sorted by backend
		colMdvo = org.nuclos.common.collection.CollectionUtils.sorted(colMdvo, new Comparator<MasterDataVO>() {

			@Override
			public int compare(MasterDataVO o1, MasterDataVO o2) {
				UID fieldSort = tms.getEntityFieldSort();
				if (null == fieldSort || UID.UID_NULL.equals(fieldSort)) {
					// use entity field if no sort field was defined
					fieldSort = tms.getEntityField();
				}
				final Object f1 =  o1.getFieldValue(fieldSort);
				final Object f2 =  o2.getFieldValue(fieldSort);
				return LangUtils.compare(f1, f2);
			}
		});
		for (final MasterDataVO mdvo : colMdvo) {
			final String name = (String) ((tms.getEntityFieldName() != null) ? mdvo.getFieldValue(tms.getEntityFieldName(), String.class) : null);
			final String fullText = (String) mdvo.getFieldValue(tms.getEntityField(), String.class);
			String label = null;
			if (null != name) {
				label = name;
			} else {
				label = StringUtils.cutString(fullText, "...", TEXT_MODULE_MENU_SIZE);
				
			}
			// avoid null values
			if (null != label && null != fullText) {
				lstResult.add(new TextModule(fullText, label));
			}
		}
		
		return lstResult;
	}
	
}
