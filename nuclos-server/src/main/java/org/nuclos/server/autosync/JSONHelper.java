//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.MasterDataToEntityObjectTransformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JSONHelper {
	
	private static final Logger LOG = LoggerFactory.getLogger(JSONHelper.class);
	
	private static Map<EntityMeta<?>, Integer> idCounter = new HashMap<EntityMeta<?>, Integer>();

	public static MasterDataVO<UID> makeMasterDataVO(Object json, EntityMeta<UID> entity) {
		Map<?, ?> jsonProperties = (Map<?, ?>) json;

		if (entity == null) {
			return null;
		}
		
		EntityObjectVO<UID> eovo = new EntityObjectVO<UID>(entity);
		UID uid = coerce(jsonProperties.get("uid"), UID.class, null);
		if (uid == null) {
			Integer id = nextId(entity);
			uid = new UID(id+"");
		}
		eovo.setPrimaryKey(uid);
		eovo.setComplete(true);

		for (FieldMeta<?> field : entity.getFields()) {
			Class<?> javaClass = field.getJavaClass();
			Object value = jsonProperties.get(field.getFieldName());
			eovo.setFieldValue(field.getUID(), coerce(value, javaClass));
			if (field.getUnreferencedForeignEntity() != null) {
				if (field.getUnreferencedForeignEntity().equals(E.ENTITY.getUID())) {
					String foreignEntityName = coerce(jsonProperties.get(field.getFieldName()), String.class);
					EntityMeta<?> foreignEntityMeta = E.getByName(foreignEntityName);
					if (foreignEntityMeta == null) {
						LOG.error("Foreign entity {} does not exist, but field {}.{} is referencing on!",
								foreignEntityName, entity.getEntityName(), field.getFieldName());
						continue;
					}
					UID foreignEntityUID = foreignEntityMeta.getUID();
					eovo.setFieldUid(field.getUID(), foreignEntityUID);
				} else {
					Object valueUid = jsonProperties.get(field.getFieldName());
					eovo.setFieldUid(field.getUID(), coerce(valueUid, UID.class));
				}
			}
		}

		IDependentDataMap dependantMap = null;
		for (Map.Entry<?, ?> e : jsonProperties.entrySet()) {
			String name = e.getKey().toString();
			EntityMeta<UID> dependentMeta = E.<UID>getByName(name);
			Object value = e.getValue();
			// Dependant data
			if (value instanceof List<?> && dependentMeta != null) {
				// find reffield...
				FieldMeta<?> refField = null;
				for (FieldMeta<?> fMeta : dependentMeta.getFields()) {
					if (LangUtils.equal(fMeta.getUnreferencedForeignEntity(), entity.getUID()) ||
							LangUtils.equal(fMeta.getForeignEntity(), entity.getUID())) {
						if (refField != null) {
							throw new NuclosFatalException(String.format("Field references from %s to %s is not unique", 
									dependentMeta.getEntityName(), entity.getEntityName()));
						}
						refField = fMeta;
					}
				}
				if (refField == null) {
					throw new NuclosFatalException(String.format("Field references from %s to %s does not exist", 
							dependentMeta.getEntityName(), entity.getEntityName()));
				}
				if (dependantMap == null)
					dependantMap = new DependentDataMap();
				for (Object dependant : (List<?>) value) {
					MasterDataVO<UID> dependantVO = makeMasterDataVO(dependant, dependentMeta);
					if (dependantVO != null) {
						dependantVO.setFieldUid(refField.getUID(), uid);
						dependantMap.addData(refField, new MasterDataToEntityObjectTransformer().transform(dependantVO));
					}
				}
			} else {
				// field, was already handled
			}
		}
		
		MasterDataVO<UID> mdvo = new MasterDataVO<UID>(eovo);
		if (dependantMap != null)
			mdvo.setDependents(dependantMap);
		return mdvo;
	}
	
	private static Integer nextId(EntityMeta<?> entity) {
		Integer lastId = idCounter.get(entity);
		Integer nextId = (lastId != null) ? lastId - 1 : -1;
		idCounter.put(entity, nextId);
		return nextId;
	}

	public static <T> T coerce(Object value, Class<T> clazz) {
		return coerce(value, clazz, null);
	}

	public static <T> T coerce(Object value, Class<T> clazz, T def) {
		if (value == null) {
			return def;
		} else if (clazz == UID.class) {
			if (value instanceof String) {
				return (T) new UID((String) value);
			} else {
				return (T) clazz.cast(value);
			}
		} else if (clazz == Integer.class) {
			return (T) Integer.valueOf(((Number) value).intValue());
		} else if (clazz == Double.class) {
			return (T) Double.valueOf(((Number) value).doubleValue());
		} else if (clazz.isInstance(value)) {
			return clazz.cast(value);
		}
		throw new ClassCastException("Cannot cast " + value + " to " + clazz);
	}

}
