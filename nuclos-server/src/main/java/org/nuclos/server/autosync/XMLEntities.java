//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import static org.nuclos.common.collection.CollectionUtils.transform;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.EntityObjectToMasterDataTransformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.XMLUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XMLEntities {

	private static final Logger LOG = LoggerFactory.getLogger(XMLEntities.class);

	public static boolean DEV = true;

	private static Map<EntityMeta<?>, SystemDataCache> internalDataCaches;

	static {
		try {
			init();
		} catch (Exception e) {
			throw new CommonFatalException(e);
		}
	}

	private static void init() throws IOException, XMLStreamException, ParseException {
		internalDataCaches = new HashMap<EntityMeta<?>, SystemDataCache>();

		initInternalDataJSON(E.DBTYPE, "dbtype");
		initInternalDataJSON(E.DBOBJECTTYPE, "dbobjecttype");
		initInternalDataJSON(E.LOCALE, "locale");
		initInternalDataJSON(E.LOCALERESOURCE, "localeresource");
		initInternalDataJSON(E.ACTION, "action");
		initInternalDataJSON(E.LAYOUT, "layout");
		initInternalDataJSON(E.DATATYPE, "datatype");
		initInternalDataJSON(E.RELATIONTYPE, "relationtype");

		// Add Layout ML strings from resources
		// TODO: Why is layoutML a String field?  It's a physical XML (i.e. it contains an encoding declaration(!))
		for (MasterDataVO<?> mdvo : getOrCreate(E.LAYOUT).getAll()) {
			final String layoutMLResource = "resources/layoutml/" + mdvo.getFieldValue(E.LAYOUT.name) + ".layoutml";
			final InputStream is = org.nuclos.common2.LangUtils.getClassLoaderThatWorksForWebStart().getResourceAsStream(layoutMLResource);
			if (is != null) {
				final BufferedInputStream bis = new BufferedInputStream(is);
				Charset cs = IOUtils.guessXmlEncoding(bis);
				if (cs == null) {
					cs = XMLUtils.DEFAULT_LAYOUTML;
				}
				final String layoutML = IOUtils.readFromTextStream(bis, cs.name());
				mdvo.setFieldValue(E.LAYOUT.layoutML, layoutML);
			}
		}
	}
	
	public static SystemDataCache getData(EntityMeta<?> entity) {
		return internalDataCaches.get(entity);
	}

	private static SystemDataCache getData(UID entityUID) {
		return internalDataCaches.get(E.getByUID(entityUID));
	}

	public static boolean hasSystemData(UID entityUID) {
		EntityMeta<?> nuclosEntity = E.getByUID(entityUID);
		return nuclosEntity != null && internalDataCaches.containsKey(nuclosEntity);
	}

	public static List<MasterDataVO<UID>> getSystemObjects(UID entityUID, CollectableSearchCondition cond) {
		if (!XMLEntities.hasSystemData(entityUID))
			return Collections.emptyList();
		return getData(entityUID).findAllVO(cond);
	}

	public static Collection<MasterDataVO<UID>> getSystemObjectsWith(UID entityUID, UID fieldUID, Object value) {
		if (!XMLEntities.hasSystemData(entityUID))
			return Collections.emptyList();
		return getData(entityUID).findAllVO(fieldUID, value);
	}

	public static Collection<MasterDataVO<UID>> getSystemObjectsWith(UID entityUID, UID fieldUID, Object...values) {
		if (!XMLEntities.hasSystemData(entityUID))
			return Collections.emptyList();
		return getData(entityUID).findAllVOIn(fieldUID, Arrays.asList(values));
	}

	public static <PK> Collection<PK> getSystemObjectIds(UID entityUID, CollectableSearchCondition cond) {
		return transform(getSystemObjects(entityUID, cond), new MasterDataVO.GetId());
	}

	public static <PK> Collection<PK> getSystemObjectIdsWith(UID entityUID, UID fieldUID, Object value) {
		return transform(getSystemObjectsWith(entityUID, fieldUID, value), new MasterDataVO.GetId());
	}

	public static MasterDataVO<?> getSystemObjectById(UID entityUID, Object id) {
		if (!XMLEntities.hasSystemData(entityUID))
			return null;
		return getData(entityUID).getById(id);
	}

	private static void registerData(EntityMeta<UID> entity, Collection<MasterDataVO<UID>> mdvos) {
		for (MasterDataVO<UID> mdvo : mdvos) {
			IDependentDataMap dependents = mdvo.getDependents();
			if (dependents != null) {
				for (IDependentKey dependentKey : dependents.getKeySet()) {
					boolean registered = false;
					for (EntityMeta<?> dependentEntityMeta : E.getAllEntities()) {
						if (registered) {
							break;
						}
						for (FieldMeta<?> fieldMeta : dependentEntityMeta.getFields()) {
							if (fieldMeta.getUID().equals(dependentKey.getDependentRefFieldUID())) {
								Collection<EntityObjectVO<?>> dependantVOs = dependents.getData(dependentKey);
								Collection<MasterDataVO<UID>> colVO = CollectionUtils.transform(dependantVOs, new EntityObjectToMasterDataTransformer());
								registerData((EntityMeta<UID>) dependentEntityMeta, colVO);
								registered = true;
								break;
							}
						}
					}
					
				}
			}
		}

		SystemDataCache cache = getOrCreate(entity);
		cache.addAll(mdvos);
	}

	private static SystemDataCache getOrCreate(EntityMeta<?> entity) {
		SystemDataCache cache = internalDataCaches.get(entity);
		if (cache == null) {
			cache = new SystemDataCache(entity);
			internalDataCaches.put(entity, cache);
		}
		return cache;
	}

	private static void initInternalDataJSON(EntityMeta<UID> entity, String file) throws IOException, ParseException {
		registerData(entity, parseJSON(entity, file));
	}

	public static Object readJSON(String file) throws IOException, ParseException {
		InputStream is = org.nuclos.common2.LangUtils.getClassLoaderThatWorksForWebStart()
				.getResourceAsStream("resources/data/" + file + ".json");
		if (is == null) {
			return null;
		}
		return JSONValue.parseWithException(new BufferedReader(new InputStreamReader(is, "utf-8")));
	}

	public static List<MasterDataVO<UID>> parseJSON(EntityMeta<UID> entity, String file) throws IOException, ParseException {
		return mdvoListFromJSON(entity, (List<?>) readJSON(file));
	}

	public static List<MasterDataVO<UID>> mdvoListFromJSON(EntityMeta<UID> entity, List<?> list) {
		List<MasterDataVO<UID>> mdvos = new ArrayList<MasterDataVO<UID>>();
		for (Object obj : list) {
			MasterDataVO<UID> mdvo = mdvo(entity, (Map<?, ?>) obj);
			if (mdvo != null) {
				mdvos.add(mdvo);
			}
		}
		return mdvos;
	}

	public static MasterDataVO<UID> mdvo(EntityMeta<UID> entity, Map<?, ?> map) {
		return JSONHelper.makeMasterDataVO(map, entity);
	}

	private static interface MasterDataVOHandler {

		public void handle(EntityMeta<UID> entity, MasterDataVO<?> mdvo);
	}

}
