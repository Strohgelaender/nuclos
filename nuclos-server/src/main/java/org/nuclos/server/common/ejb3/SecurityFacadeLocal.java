//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.security.auth.login.LoginException;

import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common.LoginResult;
import org.nuclos.server.common.MasterDataPermissions;
import org.nuclos.server.common.ModulePermissions;

// @Local
public interface SecurityFacadeLocal {

	/**
	 * @return information about the current version of the application installed on the server.
	 */
	@RolesAllowed("Login")
	String getUserName();

	/**
	 * Get all actions that are allowed for the current user.
	 * @return set that contains the Actions objects (no duplicates).
	 */
	@RolesAllowed("Login")
	Set<String> getAllowedActions();

	/**
	 * determine the permission of an attribute regarding the state of a module for the current user
	 * @param attributeUid
	 * @param entityUid
	 * @param stateUid
	 * @return Permission
	 */
	Permission getAttributePermission(final UID entityUid, final UID attributeUid,
		final UID stateUid);

	@RolesAllowed("Login")
	Map<UID, Permission> getAttributePermissionsByEntity(final UID entityUid, final UID stateUid);

	@RolesAllowed("Login")
	ModulePermissions getModulePermissions();

	@RolesAllowed("Login")
	MasterDataPermissions getMasterDataPermissions();

	/**
	 * logs the current user in.
	 * @return session id for the current user
	 */
	LoginResult login();

	/**
	 * logs the current user out.
	 * @param iSessionId session id for the current user
	 */
	void logout(Long iSessionId) throws LoginException;

	/**
	 * logs the current user out.
	 * @param sessionId session id of the container for the current user
	 */
	void logout(final String sessionId) throws LoginException;

	//TODO MULTINUCLET refactor: this would return multiple userids (by different nuclets)
	@RolesAllowed("Login")
	UID getUserUid(final String sUserName) throws CommonFatalException;
	
	List<CollectableField> getUserCommunicationAccounts(UID userUID, Class<? extends RequestContext<?>> requestContextClass);

}
