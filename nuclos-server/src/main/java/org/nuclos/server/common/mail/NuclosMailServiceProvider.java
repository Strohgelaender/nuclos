package org.nuclos.server.common.mail;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.mail.NuclosMail;
import org.nuclos.api.service.MailService;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common2.exception.MailReceiveException;
import org.nuclos.common2.exception.MailSendException;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.mail.properties.IMAPConnectionProperties;
import org.nuclos.server.common.mail.properties.MailConnectionProperties;
import org.nuclos.server.common.mail.properties.POP3ConnectionProperties;
import org.nuclos.server.common.mail.properties.SMTPConnectionProperties;
import org.springframework.stereotype.Component;

@Component("mailServiceProvider")
public class NuclosMailServiceProvider implements MailService {

	private final ServerParameterProvider serverParameterProvider;

	public NuclosMailServiceProvider(final ServerParameterProvider serverParameterProvider) {
		this.serverParameterProvider = serverParameterProvider;
	}

	private MailConnectionProperties getConnectionPropertiesForReceiving() {
		String authIMAP = serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_PROTOCOL);
		boolean usingIMAP = (authIMAP != null && (authIMAP.equalsIgnoreCase("Y") || authIMAP.equalsIgnoreCase("Yes"))) ? true : false;

		MailConnectionProperties properties = null;

		if (!usingIMAP) {
			properties = new POP3ConnectionProperties();
			properties.setUser(serverParameterProvider.getValue(ParameterProvider.KEY_POP3_USERNAME));
			properties.setPassword(serverParameterProvider.getValue(ParameterProvider.KEY_POP3_PASSWORD));
			properties.setHost(serverParameterProvider.getValue(ParameterProvider.KEY_POP3_SERVER));
			properties.setPort(serverParameterProvider.getIntValue(ParameterProvider.KEY_POP3_PORT, 110));
		}
		else {
			properties = new IMAPConnectionProperties();
			properties.setUser(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_USERNAME));
			properties.setPassword(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_PASSWORD));
			properties.setHost(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_SERVER));
			properties.setPort(serverParameterProvider.getIntValue(ParameterProvider.KEY_IMAP_PORT, 143));
			properties.setFolderFrom(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_FOLDER_FROM));
			properties.setFolderTo(serverParameterProvider.getValue(ParameterProvider.KEY_IMAP_FOLDER_TO));
		}
		
		properties.setSsl(serverParameterProvider.isEnabled(ParameterProvider.KEY_MAILGET_SSL));

		return properties;
	}

	private MailConnectionProperties getConnectionPropertiesForSending() {
		boolean auth = serverParameterProvider.isEnabled(ParameterProvider.KEY_SMTP_AUTHENTICATION);

		MailConnectionProperties properties = new SMTPConnectionProperties();
		properties.setSsl(serverParameterProvider.isEnabled(ParameterProvider.KEY_SMTP_SSL));
		properties.setHost(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_SERVER));
		properties.setPort(serverParameterProvider.getIntValue(ParameterProvider.KEY_SMTP_PORT, 25));

		if (auth) {
			properties.setUser(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_USERNAME));
			properties.setPassword(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_PASSWORD));
		}

		return properties;
	}

	@Override
	public void send(NuclosMail mail) throws BusinessException {
		try {
			send(new org.nuclos.common.mail.NuclosMail(mail));
		} catch (MailSendException e) {
			throw new BusinessException(e);
		}
	}

	public void send(org.nuclos.common.mail.NuclosMail mail) throws MailSendException {
			final NuclosMailSender nuclosMailSender = new NuclosMailSender(getConnectionPropertiesForSending());
			final String sender = serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_SENDER);
			final String emailSignature = ServerParameterProvider.getInstance().getValue(ParameterProvider.EMAIL_SIGNATURE);

			nuclosMailSender.sendMail(mail, sender, emailSignature);
	}

	@Override
	public List<NuclosMail> receive(boolean bDeleteMails)
			throws BusinessException {
		return receive(null, bDeleteMails);
	}

	@Override
	public List<NuclosMail> receive(String folderFrom, boolean bDeleteMails) throws BusinessException {
		try {
			final NuclosMailHandler nuclosMailHandler = new NuclosMailHandler(getConnectionPropertiesForReceiving());
			return nuclosMailHandler.receiveMails(folderFrom, bDeleteMails);
		} catch (MailReceiveException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * TODO: This does not guarantee that the configuration is really valid and usable.
	 * Ideally there should be a real connection test whenever the relevant parameters are changed.
	 *
	 * @return true, if the necessary SMTP parameters for sending emails are set.
	 */
	public boolean isConfiguredForSending() {
		return StringUtils.isNotBlank(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_SERVER))
				&& StringUtils.isNotBlank(serverParameterProvider.getValue(ParameterProvider.KEY_SMTP_SENDER));
	}
}
