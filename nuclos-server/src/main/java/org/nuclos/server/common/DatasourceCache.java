//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.CalcAttributeVO;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.DynamicTasklistVO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

/**
 * Cache for all Datasources.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 *
 * TODO: Re-check if all methods must be synchronized! (tp)
 */
@Component
@DependsOn({"metaDataProvider", "recordGrantUtils"})
public class DatasourceCache implements ClusterCache{

	private static final Logger LOG = LoggerFactory.getLogger(DatasourceCache.class);

	private static DatasourceCache INSTANCE;

	//

	/** map which contains all datasources */
	private final Map<UID, DatasourceVO> mpDatasourcesByUID
		= new ConcurrentHashMap<>();

	/** map which contains the datasources where the user has at least read permission */
	private final Map<String, List<DatasourceVO>> mpDatasourcesByCreator
		= new ConcurrentHashMap<>();

	/** map which contains all datasources */
	private final Map<UID, ValuelistProviderVO> mpValuelistProviderById
		= new ConcurrentHashMap<>();

	/** map which contains all record grants */
	private final Map<UID, RecordGrantVO> mpRecordGrantById
		= new ConcurrentHashMap<>();

	/** map which contains all datasources */
	private final Map<UID, DynamicEntityVO> mpDynamicEntitiesById
		= new ConcurrentHashMap<>();

	private final Map<UID, DynamicTasklistVO> mpDynamicTasklistById
		= new ConcurrentHashMap<>();
	
	private final Map<UID, ChartVO> mpChartById
	= new ConcurrentHashMap<>();
	
	private final Map<UID, CalcAttributeVO> mpCalcAttributesById
	= new ConcurrentHashMap<>();
	
	private DatasourceServerUtils datasourceServerUtils;

	private SpringDataBaseHelper dataBaseHelper;

	private SecurityCache _securityCache;

	private NucletDalProvider nucletDalProvider;


	DatasourceCache() {
		INSTANCE = this;
	}

	public static DatasourceCache getInstance() {
		return INSTANCE;
	}

	@PostConstruct
	public final void init() {
		initializeCache(false);
		// MBeanAgent.invokeCacheMethodAsMBean(INSTANCE.getClass(), "initDatasourcesCachedSQL", null, null);
		initDatasourcesCachedSQL();
	}

	@ManagedOperation
	@ManagedOperationParameters
    public void initDatasourcesCachedSQL() {
	   // invoke this method only via MBean Server. As we do this, running a new Thread is allowed here.
	   final Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				buildDatasourcesCachedSQL();
			}
		});
	   t.setDaemon(true);
	   t.setPriority(Thread.MIN_PRIORITY);
	   
	   t.start();
    }

	@Autowired
	void setDatasourceServerUtils(DatasourceServerUtils datasourceServerUtils) {
		this.datasourceServerUtils = datasourceServerUtils;
	}

	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	private SecurityCache getSecurityCache() {
		if (_securityCache == null) {
			_securityCache = SpringApplicationContextHolder.getBean(SecurityCache.class);
		}
		return _securityCache;
	}

	@Autowired
	void setNucletDalProvider(NucletDalProvider nucletDalProvider) {
		this.nucletDalProvider = nucletDalProvider;
	}

	private void initializeCache(boolean initValueListProviderCache) {
		LOG.debug("Initializing DatasourceCache");
		// Check if it is 'too early'
		NucletDalProvider.getInstance();
		for (final EntityObjectVO<UID> eoVO :
			nucletDalProvider.getEntityObjectProcessor(E.DATASOURCE).getAll()) {
			mpDatasourcesByUID.put(eoVO.getPrimaryKey(), 
					MasterDataWrapper.getDatasourceVO(DalSupportForMD.wrapEntityObjectVO(eoVO), "INITIAL", null));
		}
		if (initValueListProviderCache) {
			for (final EntityObjectVO<UID> eoVO :
				nucletDalProvider.getEntityObjectProcessor(E.VALUELISTPROVIDER).getAll()) {
				mpValuelistProviderById.put(eoVO.getPrimaryKey(), 
						MasterDataWrapper.getValuelistProviderVO(DalSupportForMD.wrapEntityObjectVO(eoVO)));
			}
		}
		for (final EntityObjectVO<UID> eoVO :
			nucletDalProvider.getEntityObjectProcessor(E.DYNAMICENTITY).getAll()) {
			mpDynamicEntitiesById.put(eoVO.getPrimaryKey(), 
					MasterDataWrapper.getDynamicEntityVO(DalSupportForMD.wrapEntityObjectVO(eoVO)));
		}
		for (final EntityObjectVO<UID> eoVO :
			nucletDalProvider.getEntityObjectProcessor(E.RECORDGRANT).getAll()) {
			mpRecordGrantById.put(eoVO.getPrimaryKey(), 
					MasterDataWrapper.getRecordGrantVO(DalSupportForMD.wrapEntityObjectVO(eoVO)));
		}
		for (final EntityObjectVO<UID> eoVO :
			nucletDalProvider.getEntityObjectProcessor(E.DYNAMICTASKLIST).getAll()) {
			mpDynamicTasklistById.put(eoVO.getPrimaryKey(), 
					MasterDataWrapper.getDynamicTasklistVO(DalSupportForMD.wrapEntityObjectVO(eoVO)));
		}
		for (final EntityObjectVO<UID> eoVO :
			nucletDalProvider.getEntityObjectProcessor(E.CHART).getAll()) {
			mpChartById.put(eoVO.getPrimaryKey(), 
					MasterDataWrapper.getChartVO(DalSupportForMD.wrapEntityObjectVO(eoVO)));
		}
		for (final EntityObjectVO<UID> eoVO :
			nucletDalProvider.getEntityObjectProcessor(E.CALCATTRIBUTE).getAll()) {
			mpCalcAttributesById.put(eoVO.getPrimaryKey(), 
					MasterDataWrapper.getCalcAttributeVO(DalSupportForMD.wrapEntityObjectVO(eoVO)));
		}
		LOG.debug("Finished initializing DatasourceCache.");
	}


	private void buildDatasourcesCachedSQL() {
		final Map[] cacheMaps = new Map[] {
				mpDatasourcesByUID, mpValuelistProviderById, mpDynamicEntitiesById,
				mpDynamicTasklistById, mpChartById, mpCalcAttributesById
		};
		
		for (int i = 0; i < cacheMaps.length; i++) {
			for (Iterator iterator = cacheMaps[i].values().iterator(); iterator.hasNext();) {
				DatasourceVO datasourceVO = (DatasourceVO) iterator.next();
				try {
					datasourceServerUtils.createSQL(datasourceVO);
				} catch (Exception e) {
					//e.printStackTrace();
					// ignore. just try to cache.
				}
			}
		}
	}
	
	/**
	 * initialize the map of datasources by creator
	 * @param sCreator
	 */
	private void findDataSourcesByCreator(final String sCreator, UID mandator) {
		LOG.debug("Initializing DatasourceCacheByCreator");
		try {
			MasterDataFacadeLocal mdFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
			List<DatasourceVO> datasources = new ArrayList<DatasourceVO>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom<UID> t = query.from(E.DATASOURCE);
			query.select(t.baseColumn(SF.PK_UID));
			query.where(builder.equalValue(t.baseColumn(SF.CREATEDBY), sCreator));

			for (final UID datasourceUid : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				final MasterDataVO<UID> mdVO = mdFacade.get(E.DATASOURCE, datasourceUid);
				if (getPermission(mdVO.getPrimaryKey(), sCreator, mandator) != DatasourceVO.PERMISSION_NONE) {
					datasources.add(MasterDataWrapper.getDatasourceVO(mdVO, sCreator, mandator));
				}
			}
			mpDatasourcesByCreator.put(sCreator,datasources);
		}
		catch (CommonPermissionException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (CommonFinderException ex) {
		}
		LOG.debug("Finished initializing DatasourceCacheByCreator.");
	}

		
	/**
	 * Invalidate the cache
	 */
	@ManagedOperation
	@ManagedOperationParameters
	public void invalidate() {
		invalidate(true);
	}
	/**
	 * Invalidate the cache
	 */
	public void invalidate(boolean initCachedSQL) {
		LOG.debug("Invalidating DatasourceCache");
		mpDatasourcesByUID.clear();
		mpDatasourcesByCreator.clear();
		mpValuelistProviderById.clear();
		mpRecordGrantById.clear();
		mpDynamicEntitiesById.clear();
		mpDynamicTasklistById.clear();
		mpChartById.clear();
		mpCalcAttributesById.clear();

		datasourceServerUtils.invalidateCache();
		
		initializeCache(true);

		if (initCachedSQL) {
			// MBeanAgent.invokeCacheMethodAsMBean(INSTANCE.getClass(), "initDatasourcesCachedSQL", null, null);
			initDatasourcesCachedSQL();
		}
	}
	

	/**
	 * get a collection of all Datasources where the given user has at least read permission
	 * 
	 * @return Collection&lt;DatasourceVO&gt;
	 */
	public Collection<DatasourceVO> getAll() {
		List<DatasourceVO> result = new ArrayList<DatasourceVO>();
		result.addAll(getAllCharts());
		result.addAll(getAllDatasources());
		result.addAll(getAllDynamicEntities());
		result.addAll(getAllDynamicTasklists());
		result.addAll(getAllRecordGrant());
		result.addAll(getAllValuelistProvider(true));
		return result;
	}


	/**
	 * get a collection of all Datasources where the given user has at least read permission
	 * 
	 * @return Collection&lt;DatasourceVO&gt;
	 */
	public Collection<DatasourceVO> getAllDatasources(final String sUser, UID mandator) {
		List<DatasourceVO>result = new ArrayList<DatasourceVO>();

		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		for (DatasourceVO datasourceVO : mpDatasourcesByUID.values()) {
			if(getPermission(datasourceVO.getId(), sUser, mandator) != DatasourceVO.PERMISSION_NONE) {
				result.add(datasourceVO);
			}
		}
		return result;
	}

	/**
	 * get all Datasources without checking user permissions
	 * @return
	 */
	public Collection<DatasourceVO> getAllDatasources() {
		List<DatasourceVO>result = new ArrayList<DatasourceVO>();

		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpDatasourcesByUID.values());

		return result;
	}

	//TODO MULTINUCLET eliminate this method
	/**
	 * Get the base entity name of a dynamic entity.
	 *
	 * @param dynamicEntityUID The name of the dynamic entity.
	 * @return Returns the base entity name. Returns the original entity name if there is no dynamic entity with the given name.
	public String getBaseEntity(String dynamicEntityUID) {
		if (mapDynamicEntities.containsKey(dynamicEntityUID)) {
			return mapDynamicEntities.get(dynamicEntityUID).getEntity();
		}
		else {
			return dynamicEntityUID;
		}
	}
	 */

	/**
	 * get all valuelist provider
	 * @return
	 */
	public Collection<ValuelistProviderVO> getAllValuelistProvider(boolean useCache) {
		List<ValuelistProviderVO> result = new ArrayList<ValuelistProviderVO>();

		if (mpValuelistProviderById.isEmpty() && useCache)
			initializeCache(true);;//findDatasourcesById();

		result.addAll(mpValuelistProviderById.values());

		return result;
	}

	/**
	 * get a ValuelistProvider
	 */
	public ValuelistProviderVO getValuelistProvider(final UID valuelistProviderUID, boolean useCache) {
		if (valuelistProviderUID == null) {
			return null;
		}
		if (mpValuelistProviderById.isEmpty() && useCache) {
			initializeCache(true);;//findDatasourcesById();
			
		}
		return mpValuelistProviderById.get(valuelistProviderUID);
	}

	/**
	 * get all record grant
	 * @return
	 */
	public Collection<RecordGrantVO> getAllRecordGrant() {
		List<RecordGrantVO> result = new ArrayList<RecordGrantVO>();

		if (mpRecordGrantById.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpRecordGrantById.values());

		return result;
	}

	/**
	 * get a record grant
	 * @param recordGrantUID record grant UID
	 * @return
	 */
	public RecordGrantVO getRecordGrant(final UID recordGrantUID) {
		if (mpRecordGrantById.isEmpty())
			;//findDatasourcesById();

		return mpRecordGrantById.get(recordGrantUID);
	}

	/**
	 * get all dynamic entities
	 * @return
	 */
	public Collection<DynamicEntityVO> getAllDynamicEntities() {
		List<DynamicEntityVO>result = new ArrayList<DynamicEntityVO>();

		if (mpDynamicEntitiesById.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpDynamicEntitiesById.values());

		return result;
	}

	/**
	 * get all dynamic task lists
	 * @return
	 */
	public Collection<DynamicTasklistVO> getAllDynamicTasklists() {
		List<DynamicTasklistVO> result = new ArrayList<DynamicTasklistVO>();

		if (mpDynamicTasklistById.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpDynamicTasklistById.values());

		return result;
	}

	/**
	 * get a  dynamic task lists
	 * @param dynamicTaskListUID
	 * @return
	 */
	public DynamicTasklistVO getDynamicTasklist(final UID dynamicTaskListUID) {
		if (mpDynamicTasklistById.isEmpty())
			;//findDatasourcesById();

		return mpDynamicTasklistById.get(dynamicTaskListUID);
	}

	/**
	 * get all charts
	 * @return
	 */
	public Collection<ChartVO> getAllCharts() {
		List<ChartVO>result = new ArrayList<ChartVO>();

		if (mpChartById.isEmpty())
			;//findDatasourcesById();

		result.addAll(mpChartById.values());

		return result;
	}
	
	
	/**
	 * get all calc attributes
	 * @return
	 */
	public Collection<CalcAttributeVO> getAllCalcAttributes() {
		List<CalcAttributeVO> result = new ArrayList<CalcAttributeVO>();

		result.addAll(mpCalcAttributesById.values());

		return result;
	}
	
	/**
	 * get a CalcAttribute
	 */
	public CalcAttributeVO getCalcAttribute(final UID calcAttributeUID) {
		if (calcAttributeUID == null) {
			return null;
		}
		return mpCalcAttributesById.get(calcAttributeUID);
	}


	/**
	 * get a Datasource by UID regardless of permisssions
	 */
	public DatasourceVO get(final UID dataSourceUID) {
		return mpDatasourcesByUID.get(dataSourceUID);
	}

	/**
	 * get a Datasource by id where the given user has at least read permission
	 * @param dataSourceUID datasource UID
	 * @param sUserName username
	 * @return
	 * @throws CommonPermissionException
	 */
	public DatasourceVO getDatasourcesByUser(final UID dataSourceUID, final String sUserName, UID mandator) throws CommonPermissionException {
		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		if (getPermission(dataSourceUID, sUserName, mandator) == DatasourceVO.PERMISSION_NONE) {
			throw new CommonPermissionException("datasource.cach.missing.permission");
		}
		return mpDatasourcesByUID.get(dataSourceUID);
	}

	/**
	 * get datasources by creator
	 * @param sUserName
	 * @return
	 */
	public Collection<DatasourceVO> getDatasourcesByCreator(String sUserName, UID mandator) {
		if(mpDatasourcesByCreator.isEmpty() || mpDatasourcesByCreator.get(sUserName) == null) {
			findDataSourcesByCreator(sUserName, mandator);
		}
		return CollectionUtils.emptyIfNull(mpDatasourcesByCreator.get(sUserName));
	}

	/**
	 * get a datasource by name (used to check if different datasource exists with the same name).
	 * @param dataSourceUID
	 * @return DatasourceVO (may be null)
	 */
	public DatasourceVO getDatasource(final UID dataSourceUID) {
		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		DatasourceVO result = null;
		for(final DatasourceVO dsvo : mpDatasourcesByUID.values()) {
			if(dsvo.getPrimaryKey().equals(dataSourceUID)) {
				result = dsvo;
				break;
			}
		}

		return result;
	}

	/**
	 * get a valuelist provider by name.
	 * @param valuelistProviderUID
	 * @return ValuelistProviderVO (may be null)
	 */
	public ValuelistProviderVO getValuelistProviderByUID(final UID valuelistProviderUID) {
		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		ValuelistProviderVO result = null;
		for(ValuelistProviderVO vpvo : mpValuelistProviderById.values()) {
			if(vpvo.getPrimaryKey().equals(valuelistProviderUID)) {
				result = vpvo;
				break;
			}
		}

		return result;
	}

	/**
	 * get a record grant by name.
	 * @param recordGrantUID recordgrant UID
	 * @return RecordGrantVO (may be null)
	 */
	public RecordGrantVO getRecordGrantByName(UID recordGrantUID) {
		if (mpRecordGrantById.isEmpty())
			;//findDatasourcesById();

		RecordGrantVO result = null;
		for(RecordGrantVO vpvo : mpRecordGrantById.values()) {
			if(vpvo.getPrimaryKey().equals(recordGrantUID)) {
				result = vpvo;
				break;
			}
		}

		return result;
	}

	/**
	 * get a dynamic entity
	 * 
	 * @param entityUID	dynamic entity UID
	 * @return DynamicEntityVO (may be null)
	 */
	public DynamicEntityVO getDynamicEntity(final UID entityUID) {
		if (mpDatasourcesByUID.isEmpty())
			;//findDatasourcesById();

		DynamicEntityVO result = null;
		for(final DynamicEntityVO devo : mpDynamicEntitiesById.values()) {
			if(ObjectUtils.equals(devo.getId(), entityUID)) {
				result = devo;
				break;
			}
		}

		return result;
	}

	/**
	 * get a chart
	 * 
	 * @param entityUID entity UID
	 * @return ChartVO (may be null)
	 */
	public ChartVO getChart(final UID entityUID) {
		if (mpChartById.isEmpty())
			;//findDatasourcesById();

		ChartVO result = null;
		for(ChartVO cvo : mpChartById.values()) {
			if(ObjectUtils.equals(cvo.getPrimaryKey(), entityUID)) {
				result = cvo;
				break;
			}
		}

		return result;
	}

	/**
	 * helper function to get the permission for a given user from the SecurityCache
	 * @param dataSourceUID datasource UID
	 * @param sUserName
	 * @return
	 */
	public int getPermission(final UID dataSourceUID, final String sUserName, UID mandator) {
		final int result;

		if (getSecurityCache().getWritableDataSourceUids(sUserName, mandator).contains(dataSourceUID)) {
			result = DatasourceVO.PERMISSION_READWRITE;
		}
		else if (getSecurityCache().getReadableDataSources(sUserName, mandator).contains(dataSourceUID)) {
			result = DatasourceVO.PERMISSION_READONLY;
		}
		else {
			result = DatasourceVO.PERMISSION_NONE;
		}
		return result;
		//return DatasourceVO.PERMISSION_READWRITE;
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.DATASOURCE_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);			
	}

	@Override
	public void registerCache() {	
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public void invalidateCache(boolean notifyClients, boolean notifyClusterCloud) {
		invalidate(true);
		if(notifyClusterCloud) {
			notifyClusterCloud();
		}
	}

	@ManagedAttribute(description="get the size (number of data sources) of mpDatasourcesByUID")
	public int getNumberOfDatasourcesInCache() {
		return mpDatasourcesByUID.size();
	}

	@ManagedAttribute(description="get the size (number of VLP) of mpValuelistProviderById")
	public int getNumberOfValuelistProviderInCache() {
		return mpValuelistProviderById.size();
	}

	@ManagedAttribute(description="get the size (number of data record grant) of mpRecordGrantById")
	public int getNumberOfRecordGrantInCache() {
		return mpRecordGrantById.size();
	}

	@ManagedAttribute(description="get the size (number of dynamic entities) of mpDynamicEntitiesById")
	public int getNumberOfDynamicEntitiesInCache() {
		return mpDynamicEntitiesById.size();
	}

	@ManagedAttribute(description="get the size (number of dynamic task lists) of mpDynamicTasklistById")
	public int getNumberOfDynamicTaskListsInCache() {
		return mpDynamicTasklistById.size();
	}

	@ManagedAttribute(description="get the size (number of charts) of mpChartById")
	public int getNumberOfChartsInCache() {
		return mpChartById.size();
	}
	
	@ManagedAttribute(description="get the size (number of calc attributes) of mpCalcAttributesById")
	public int getNumberOfCalcAttributesInCache() {
		return mpCalcAttributesById.size();
	}

}
