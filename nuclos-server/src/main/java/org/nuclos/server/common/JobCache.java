package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@DependsOn("metaDataProvider")
public class JobCache {

	private List<JobVO> lstAllJobs;
	
	// Spring injection
	
	@Autowired
	private NucletDalProvider nucletDalProvider;
	
	// end of Spring injection
		
	JobCache() {
	}
	
	@PostConstruct
	final void init() {
		loadJobs();
	}
	
	private void loadJobs() {
		lstAllJobs = new ArrayList<JobVO>();
		
		for (Object eoVO : nucletDalProvider.getEntityObjectProcessor(E.JOBCONTROLLER).getAll()) {
				lstAllJobs.add(MasterDataWrapper.getJobVO((EntityObjectVO)eoVO));
		}
	}
	
	@Cacheable(value="jobcontroller", key="#uid", condition="#uid != null")
	public JobVO getJob(UID uid) {
		JobVO retVal = null;
		
		for (JobVO curjob : getJobs()) {
			if (uid.equals(curjob.getId())) {
				retVal = curjob;
				break;
			}
		}
		
		return retVal;
	}
	
	public List<JobVO> getJobs() {
		if (lstAllJobs.isEmpty())
			loadJobs();
		
		return this.lstAllJobs;
	}

	@CacheEvict(value="eoNuclets", allEntries=true) 
	public void evict() {}
	
	public void invalidate() {
		evict();
		this.lstAllJobs.clear();
	}
}
