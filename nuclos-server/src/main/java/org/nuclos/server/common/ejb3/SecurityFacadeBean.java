//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.security.RolesAllowed;
import javax.security.auth.login.LoginException;

import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LoginResult;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidE;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.Version;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.transport.GzipMap;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MasterDataPermissions;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ModulePermissions;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.communication.CommunicationInterfaceLocal;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.web.ClientLifecycle;
import org.nuclos.server.web.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for searchfilter. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*/
@Transactional(noRollbackFor= {Exception.class})
public class SecurityFacadeBean implements SecurityFacadeLocal, SecurityFacadeRemote {
	
	private static final Logger LOG = LoggerFactory.getLogger(SecurityFacadeBean.class);
	
	// Spring injection
	
	@Autowired
	private ServerParameterProvider serverParameterProvider;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private ApplicationProperties applicationProperties;
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	@Autowired
	private SecurityCache securityCache;
	
	@Autowired
	private ClientLifecycle clientLifecycle;
	
	@Autowired
	private Session session;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	// end of Spring injection
	
	public SecurityFacadeBean() {
	}

	@PostConstruct
	public void postConstruct() {
		LOG.info("Authentication successful.");
		getCurrentApplicationInfoOnServer(); // prefill md5 info string.

		// If there are still "active" sessions (e.g. because the server was killed), mark them as logged out
		writeLogoutProtocol();
	}

	@PreDestroy
	public void preDestroy() throws Exception {
		// Server is shutting down -> mark all active sessions as logged out
		writeLogoutProtocol();
	}

	/**
	 * @deprecated Use {@link #getUserName()}
	 */
	private final String getCurrentUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}

	/**
	 * logs the current user in.
	 * @return session id for the current user
	 */
	@RolesAllowed("Login")
	public LoginResult login() {
		final String username = getUserName();
		try {
			session.setUsername(username);
		} catch (BeanCreationException e) {
			// sometimes we are NOT in a session here, e.g. job run
		}

		String jSessionId = null;
		try {
			jSessionId = session.getHttpSession().getId();
		}
		catch (Exception ex) {
			// Might not be available yet during server startup
		}

		final Long sessionId = writeLoginProtocol(username, jSessionId);
		clientLifecycle.clientLogin(username);

		/**
		 * If this was a remote login, save it as a web session,
		 * so the session can be also used via REST, even if the login
		 * was done via Rich Client remote invocation.
		 */
		if (jSessionId != null) {
			setWebSessionContext(jSessionId, sessionId);
		}

		return new LoginResult(sessionId, jSessionId);
	}

	private void setWebSessionContext(final String jSessionId, final Long sessionId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		securityCache.setSessionContext(
				auth,
				LocaleContextHolder.getLocale(),
				jSessionId,
				sessionId
		);
	}

	/**
	 * logs the current user out.
	 * @param sessionId session id for the current user
	 */
	@RolesAllowed("Login")
	public void logout(Long sessionId) {
		writeLogoutProtocol(sessionId);
		performLogout();
	}

	/**
	 * logs the current user out.
	 *
	 * @param sessionId session id of the container for the current user
	 */
	@RolesAllowed("Login")
	public void logout(final String sessionId) throws LoginException {
		writeLogoutProtocol(sessionId);
		performLogout();
	}

	/**
	 * Performs the logout, i.e. invalidates sessions.
	 */
	private void performLogout() {
		final String username = getUserName();
		LOG.info("User {} logged out.", username);
		clientLifecycle.clientLogout(username);
		session.invalidate();
	}

	/**
	 * @return information about the current version of the application installed on the server.
	 */
    @RolesAllowed("Login")
	public Version getCurrentApplicationVersionOnServer() {
		return applicationProperties.getNuclosVersion();
	}

    private String sApplicationInfo = null;
    public String getCurrentApplicationInfoOnServer() {
    	if (sApplicationInfo == null) {
	    	try {
	        	//@todo mixup ip of current server. - not only database. @see NUCLOS-1223
	        	String info = UUID.randomUUID().toString();//SpringDataBaseHelper.getInstance().getCurrentConnectionInfo();
	        	sApplicationInfo = new String(Hex.encode(MessageDigest.getInstance("MD5").digest(info.getBytes())));
			} catch (Exception e) {
				throw new NuclosFatalException(e);
			}
    	}
    	return sApplicationInfo;
    }
    
	/**
	 * @return information about the current version of the application installed on the server.
	 */
    @RolesAllowed("Login")
	public String getUserName() {
		return getCurrentUserName();
	}

	/**
	 * Get all actions that are allowed for the current user.
	 * @return set that contains the Actions objects (no duplicates).
	 */
    @RolesAllowed("Login")
	public Set<String> getAllowedActions() {
		return securityCache.getAllowedActions(getCurrentUserName(), getCurrentMandatorUID());
	}
    
    /**
	 * Get all actions that are allowed for the current user.
	 * @return set that contains the Actions objects (no duplicates).
	 */
    @RolesAllowed("Login")
	public Set<UID> getAllowedCustomActions() {
		return securityCache.getAllowedCustomActions(getCurrentUserName(), getCurrentMandatorUID());
	}

	/**
	 * @return the module permissions for the current user.
	 */
    @RolesAllowed("Login")
	public ModulePermissions getModulePermissions() {
		return securityCache.getModulePermissions(getCurrentUserName(), getCurrentMandatorUID());
	}

	/**
	 * @return the masterdata permissions for the current user.
	 */
    @RolesAllowed("Login")
	public MasterDataPermissions getMasterDataPermissions() {
		return securityCache.getMasterDataPermissions(getCurrentUserName(), getCurrentMandatorUID());
	}
    
    /**
     * @return all accessible mandators for the current user by level.
     * @throws CommonPermissionException 
     */
    @RolesAllowed("Login")
    public Map<UID, List<MandatorVO>> getAccessibleMandatorsByLevel() throws CommonPermissionException {
    	Map<UID, List<MandatorVO>> result = new HashMap<UID, List<MandatorVO>>();
    	SecurityCache secCache = SecurityCache.getInstance();
    	secCache.checkMandatorLoginPermission(getCurrentUserName(), getCurrentMandatorUID());
    	for (UID mandatorUID : secCache.getAccessibleMandators(this.getCurrentMandatorUID())) {
    		MandatorVO mandator = secCache.getMandator(mandatorUID);
    		if (!result.containsKey(mandator.getLevelUID())) {
    			result.put(mandator.getLevelUID(), new ArrayList<MandatorVO>());
    		}
    		result.get(mandator.getLevelUID()).add(mandator);
    	}
    	for (Map.Entry<UID, List<MandatorVO>> entry : result.entrySet()) {
    		Collections.sort(entry.getValue());
    		entry.setValue(Collections.unmodifiableList(entry.getValue()));
    	}
    	return Collections.unmodifiableMap(result);
    }

	/**
	 * get a String representation of the users session context
	 */
    @RolesAllowed("Login")
	public String getSessionContextAsString() {
		return getCurrentUserName();
	}

	/**
	 * write the successful login into the protocol table
	 */
	private Long writeLoginProtocol(final String sUserName, final String sessionId) {
		try {
			final String sApplicationName = applicationProperties.getName();

			IEntityObjectProcessor<Long> eoProcessor = nucletDalProvider.getEntityObjectProcessor(RigidE.SESSION);

			EntityObjectVO<Long> eo = new EntityObjectVO<Long>(E.SESSION);
			eo.flagNew();
			eo.setPrimaryKey(dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE));
			eo.setFieldValue(E.SESSION.SESSION_ID, sessionId);
			eo.setFieldValue(E.SESSION.USER_ID, sUserName);
			eo.setFieldValue(E.SESSION.APPLICATION, sApplicationName);
			eo.setFieldValue(E.SESSION.LOGON, InternalTimestamp.toInternalTimestamp(new Date()));
			eo.setFieldValue(SF.CREATEDBY, "nuclos");
			eo.setFieldValue(SF.CHANGEDBY, "nuclos");
			eo.setFieldValue(SF.CREATEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
			eo.setFieldValue(SF.CHANGEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
			eo.setFieldValue(SF.VERSION, 1);

			eoProcessor.insertOrUpdate(eo);

			return eo.getPrimaryKey();
		}
		catch (Exception ex) {
			throw new CommonFatalException("Could not write login protocol", ex);//"Konnte Login nicht protokollieren."
		}
	}

	/**
	 * write the successful logout into the protocol table
	 */
	private void writeLogoutProtocol(final Long intid) {
		dataBaseHelper.execute(DbStatementUtils.updateValues(E.SESSION,
				E.SESSION.LOGOFF, InternalTimestamp.toInternalTimestamp(new Date())).where(E.SESSION.getPk(), intid));
	}

	/**
	 * write the successful logout into the protocol table
	 */
	private void writeLogoutProtocol(final String sessionId) {
		dataBaseHelper.execute(
				DbStatementUtils.updateValues(
						E.SESSION,
						E.SESSION.LOGOFF,
						InternalTimestamp.toInternalTimestamp(new Date())
				).where(E.SESSION.SESSION_ID, sessionId)
		);
	}

	/**
	 * Writes the current time as logout time for all currently active sessions,
	 * only if the server is not started in cluster mode as the session could still
	 * be alive on other cluster servers.
	 */
	private void writeLogoutProtocol() {
		if (NuclosClusterRegisterHelper.runInClusterMode()) {
			LOG.info("Server is started in cluster mode, therefore a logout is not recorded automatically.");
			return;
		}

		dataBaseHelper.execute(
				DbStatementUtils.updateValues(
						E.SESSION,
						E.SESSION.LOGOFF,
						InternalTimestamp.toInternalTimestamp(new Date())
				).where(E.SESSION.LOGOFF, null));
	}

	/**
	 * get the user id from the database
	 * @param sUserName the user name in lower case characters
	 * @return
	 * @throws CommonFatalException
	 */
	public UID getUserUid(final String sUserName) throws CommonFatalException {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.USER);
		query.select(t.basePk());
		query.where(builder.equal(builder.upper(t.baseColumn(E.USER.name)), builder.upper(builder.literal(sUserName))));
		UID executeQuerySingleResult = null;
		try{
			executeQuerySingleResult = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		} catch (DbInvalidResultSizeException e){
			throw new CommonFatalException("Could not find user "+sUserName);
		}
		return executeQuerySingleResult;
	}

	/**
	 * @return the readable subforms
	 */
    public Map<UID, SubformPermission> getSubFormPermission(final UID entityUid) {
		return securityCache.getSubForm(getCurrentUserName(), entityUid, getCurrentMandatorUID());
	}

	/**
	 * determine the permission of an attribute regarding the state of a module for the current user
	 */
    public Permission getAttributePermission(UID entityUid, UID attributeUid, UID stateUid) {
		// special behaviour for modules which have no statusnumeral (e.g. Rechnungsabschnitte)
		  if (stateUid == null) {
		   return Permission.READWRITE;
		  }
		  final UID attributeGroupUid = AttributeCache.getInstance().getAttribute(attributeUid).getAttributegroupUID();
		  final Map<UID, Permission> mpAttributePermission = securityCache.getAttributeGroup(getCurrentUserName(), entityUid, attributeGroupUid, getCurrentMandatorUID());

		  return mpAttributePermission.get(stateUid);
	}

	public Map<UID, Permission> getAttributePermissionsByEntity(UID entityUid, UID stateUid) {
		HashMap<UID, Permission> res = new HashMap<UID, Permission>();
		MetaProvider mdProv = MetaProvider.getInstance();
		String user = getCurrentUserName();

		Map<UID, FieldMeta<?>> fields = mdProv.getAllEntityFieldsByEntity(entityUid);
		for(Map.Entry<UID, FieldMeta<?>> e : fields.entrySet()) {
			Permission p;
			if(stateUid == null) {
				p = Permission.READWRITE;
			}
			else {
				p = securityCache.getAttributeGroup(
						user,
						entityUid,
						e.getValue().getFieldGroup(), getCurrentMandatorUID())
					.get(stateUid);
			}
			res.put(e.getKey(), p);
		}
		return res;
	}

    @RolesAllowed("Login")
	public Boolean isSuperUser() {
		return securityCache.isSuperUser(getCurrentUserName());
	}

    @RolesAllowed("Login")
	public void invalidateCache(){
		SecurityCache.getInstance().invalidateCache(true, true);
	}

    @RolesAllowed("Login")
    public Map<String, Object> getInitialSecurityData() {
		GzipMap<String, Object> res = new GzipMap<String, Object>();
		String userName = getCurrentUserName();
		UID mandatorUid = getCurrentMandatorUID();
		res.put(USERNAME, userName);
		res.put(USERUID, securityCache.getUserUid(userName));
		res.put(IS_SUPER_USER, isSuperUser());
		res.put(HAS_COMMUNICATION_ACCOUNT_PHONE, Boolean.valueOf(securityCache.getUserCommunicationPhoneAccount(userName) != null));
		res.put(ALLOWED_ACTIONS, getAllowedActions());
		res.put(ALLOWED_CUSTOM_ACTIONS, getAllowedCustomActions());
		res.put(MODULE_PERMISSIONS, getModulePermissions());
		res.put(MASTERDATA_PERMISSIONS, getMasterDataPermissions());
		res.put(DYNAMIC_TASKLISTS, securityCache.getDynamicTasklists(userName, mandatorUid));
		res.put(USER_ROLES, securityCache.getUserRolesWithName(userName, mandatorUid));
		try {
			res.put(SecurityFacadeRemote.MANDATORS_BY_LEVEL, getAccessibleMandatorsByLevel());
		} catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}
		return res;
    }

	public Boolean isLdapAuthenticationActive() {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.LDAPSERVER);
		query.select(t.basePk());
		query.where(builder.and(builder.equalValue(t.baseColumn(E.LDAPSERVER.active), true), builder.isNotNull(t.baseColumn(E.LDAPSERVER.userfilter))));

		return dataBaseHelper.getDbAccess().executeQuery(query).size() > 0;
	}

	public Boolean isLdapSynchronizationActive() {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.LDAPSERVER);
		query.select(t.basePk());
		query.where(builder.and(builder.equalValue(t.baseColumn(E.LDAPSERVER.active), true), builder.isNotNull(t.baseColumn(E.LDAPSERVER.serversearchfilter))));

		return dataBaseHelper.getDbAccess().executeQuery(query).size() > 0;
	}

	public Date getPasswordExpiration() {
		Integer interval = 0;
		if (serverParameterProvider.getValue(ParameterProvider.KEY_SECURITY_PASSWORD_INTERVAL) != null) {
			interval = serverParameterProvider.getIntValue(ParameterProvider.KEY_SECURITY_PASSWORD_INTERVAL, 0);
		}

		if (interval == 0) {
			return null;
		}

		String username = getCurrentUserName();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.USER);
		query.multiselect(t.baseColumn(E.USER.passwordchanged), t.baseColumn(E.USER.superuser));
		query.where(builder.equal(builder.upper(t.baseColumn(E.USER.name)), builder.upper(builder.literal(username))));
		DbTuple tuple = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);

		Date d = tuple.get(0, Date.class);
		Boolean isSuperUser = Boolean.TRUE.equals(tuple.get(1, Boolean.class));
		if (d != null && (!isLdapAuthenticationActive() || isSuperUser)) {
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			c.add(Calendar.DAY_OF_MONTH, interval);
			return c.getTime();
		}
		return null;
	}

	@Override
	public void clientWaitingForLogin() {
		clientLifecycle.clientWaitingForLogin();
	}

	@Override
	public void clientReady() {
		clientLifecycle.clientReady();
	}
	
	private final UID getCurrentMandatorUID() {
		return userCtx.getMandatorUID();
	}
	
	@Override
	public List<MandatorVO> getMandators() {
		return SecurityCache.getInstance().getAssignedMandators(getCurrentUserName());
	}
	
	@Override
	public void checkMandatorPermission() throws CommonPermissionException {
		try {
			SecurityCache.getInstance().checkMandatorLoginPermission(getCurrentUserName(), getCurrentMandatorUID());
		} catch (CommonPermissionException permex) {
			// too early for injection
			String localizedError = SpringLocaleDelegate.getInstance().getText(permex.getMessage());
			throw new CommonPermissionException(localizedError);
		}
	}
	
	@Override
	public List<MandatorVO> getMandatorsByLevel(UID levelUID) {
		return SecurityCache.getInstance().getMandatorsByLevel(levelUID);
	}
	
	@Override
	public List<CollectableField> getUserCommunicationAccounts(UID userUID, Class<? extends RequestContext<?>> requestContextClass) {
		List<CollectableField> result = new ArrayList<>();
		
		if(securityCache.isWriteAllowedForMasterData(getUserName(), E.USER.getUID(), userCtx.getMandatorUID())) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom<UID> t = query.from(E.USER_COMMUNICATION_ACCOUNT);
			query.multiselect(t.basePk(), t.baseColumn(E.USER_COMMUNICATION_ACCOUNT.account), t.baseColumn(E.USER_COMMUNICATION_ACCOUNT.communicationPort));
			query.where(builder.equalValue(t.baseColumn(E.USER_COMMUNICATION_ACCOUNT.user), userUID));
			
			CommunicationInterfaceLocal interfaces = SpringApplicationContextHolder.getBean(CommunicationInterfaceLocal.class);
			
			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
				UID accountUID = tuple.get(E.USER_COMMUNICATION_ACCOUNT.getPk());
				String accountLogin = tuple.get(E.USER_COMMUNICATION_ACCOUNT.account);
				UID portUID = tuple.get(E.USER_COMMUNICATION_ACCOUNT.communicationPort);
				for (Class<? extends RequestContext<?>> supportedClass : interfaces.getSupportedRequestContextClasses(portUID)) {
					if (supportedClass.equals(requestContextClass)) {
						
						DbQuery<String> q2 = builder.createQuery(String.class);
						DbFrom<UID> t2 = q2.from(E.COMMUNICATION_PORT);
						q2.select(t2.baseColumn(E.COMMUNICATION_PORT.portName));
						q2.where(builder.equalValue(t2.basePk(), portUID));
						String portName = dataBaseHelper.getDbAccess().executeQuerySingleResult(q2);
						
						CollectableValueIdField clctValue = new CollectableValueIdField(accountUID, portName + " (" + accountLogin + ")");
						result.add(clctValue);
					}
				}
			}
		}
		
		return result;
	}
}
