package org.nuclos.server.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.print.PrintService;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.api.service.FileService;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.printservice.PrintServiceLocator;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.exception.CommonPrintException;
import org.nuclos.server.printservice.PrintServiceFacadeLocal;
import org.nuclos.server.printservice.PrintServiceRepository;
import org.nuclos.server.printservice.PrintUtils;
import org.springframework.stereotype.Component;

@Component("fileServiceProvider")
public class FileServiceProvider implements FileService {

	@Override
	public void print(NuclosFile file) throws BusinessException {
		this.printOnPrinter(file, null);
	}

	@Override
	public void print(NuclosFile file, String printerName)
			throws BusinessException {
		this.printOnPrinter(file, printerName);
	}
	
	@Override
	public void print(NuclosFile file, PrintProperties printProperties)
			throws BusinessException {
		this.printWithProperties(file, printProperties);
	}
	
	private void printOnPrinter(NuclosFile file, String printerName)
				throws BusinessException {
		try {
			final PrintServiceFacadeLocal psFacade = SpringApplicationContextHolder.getBean(PrintServiceFacadeLocal.class);
			final PrintServiceRepository printServiceRep = SpringApplicationContextHolder.getBean(PrintServiceRepository.class);
			if (printerName != null) {
				final PrintServiceTO printServiceTO = printServiceRep.printServiceByName(printerName);
				if (printServiceTO != null) {
					psFacade.printFile(file, printServiceTO.getId());
				} else {
					final PrintService printService = PrintServiceLocator.lookupPrintServiceByPrinterName(printerName);
					psFacade.printFile(file, printService);
				}
			} else {
				final PrintService printService = printServiceRep.lookupDefaultPrintService();
				psFacade.printFile(file, printService);
			}
			
//			final PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
//			aset.add(new Copies(1));
//			aset.remove(MediaPrintableArea.class); 
			//reportFacade.printViaPrintService(
			//		(NuclosReportRemotePrintService) prserv, new PDFPrintJob(), aset, file.getContent());
			
		} catch (CommonPrintException e) {
			throw new BusinessException(e);
		}	
	}
	
	private void printWithProperties(NuclosFile file, PrintProperties printProperties)
			throws BusinessException {
		final PrintServiceFacadeLocal psFacade = SpringApplicationContextHolder.getBean(PrintServiceFacadeLocal.class);
		try {
			psFacade.printFile(file, printProperties);
		} catch (CommonPrintException e) {
			throw new BusinessException(e);
		}	
	}
	
	@Override
	public NuclosFile toPdf(NuclosFile file) throws BusinessException {
		try {
			return PrintUtils.toPdf(file);
		} catch (CommonPrintException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void save(NuclosFile file, String directory)
			throws BusinessException  {
		
		if (directory == null) {
			throw new BusinessException("directory must not be null");
		}

		if (file == null) {
			throw new BusinessException("file must not be null");
		}
		
		File f = new File(directory);
		
		if (!f.exists() || !f.isDirectory()) {
			throw new BusinessException("directory " + f.getName() + " is not valid or does not exist");
		}
		
		File savedFile = new File (f, file.getName());
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(savedFile);
			fos.write(file.getContent());
			fos.close();
		} catch (FileNotFoundException e) {
			throw new BusinessException(e);
		}catch (IOException e) {
			throw new BusinessException(e);
		} finally {
			try {
				if(fos != null)
					fos.close();				
			} catch (Exception e2) {}
		}
	}

	@Override
	public NuclosFile newFile(File file) throws BusinessException {
		
		NuclosFile retVal = null;
		
		if (file == null)
			throw new BusinessException ("File must not be null");
		
		if (!file.isFile())
			throw new BusinessException ("Argument file is not a file");

		if (!file.exists())
			throw new BusinessException ("File cannot be found");

		try {
			byte[] readFromBinaryFile = IOUtils.readFromBinaryFile(file);
			retVal =  new org.nuclos.common.NuclosFile(file.getName(), readFromBinaryFile);
		} catch (IOException e) {
			throw new BusinessException(e);
		}
		
		return retVal;
	}

	@Override
	public NuclosFile newFile(String completeFilePath) throws BusinessException {
		if (completeFilePath == null)
			throw new BusinessException ("completeFilePath must not be null");
		return this.newFile(new File(completeFilePath));
	}

}
