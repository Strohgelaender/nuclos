//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.nuclos.server.statemodel.valueobject.MandatoryColumnVO;
import org.nuclos.server.statemodel.valueobject.MandatoryFieldVO;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateModelUsagesCache;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;

/**
 * A cache for States.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:lars.rueckemann@novabit.de">Lars R\u00fcckemann</a>
 * @version 00.01.000
 */
public class StateCache implements ClusterCache, BeanFactoryAware {
	
	private static final Logger LOG = LoggerFactory.getLogger(StateCache.class);

	/**
	 * Supporting getInstance().
	 */
	private static StateCache INSTANCE;
	
	// Spring injection
	
	// BeanFactoryAware
	private BeanFactory beanFactory;
	
	private StateFacadeLocal statefacade;
	
	private MasterDataFacadeLocal masterDataFacade;
	
	private SpringDataBaseHelper dataBaseHelper;
	
	private NucletDalProvider nucletDalProvider;
	
	// end of Spring injection

	private final Map<UID, StateVO> mpStatessByUid
		= new ConcurrentHashMap<UID, StateVO>();
	
	private final Map<UID, List<StateVO>> mpStatesBymodelUid
		= new ConcurrentHashMap<UID, List<StateVO>>();
	
	private final Map<UID, StateVO> mpStateByModelAndInitialStateUid
		= new ConcurrentHashMap<UID, StateVO>();
	
	private final Map<UID, StateTransitionVO> mpInitialTransitionBymodelUid
		= new ConcurrentHashMap<UID, StateTransitionVO>();
	
	private final Map<UID, Collection<StateVO>> mpStatesByModuleUid
		= new ConcurrentHashMap<UID, Collection<StateVO>>();
	
	private final List<StateModelVO> lstStatemodels = new ArrayList<StateModelVO>();
	
	private final Map<UID, StateGraphVO> mpStatesGraphsByModel
		= new ConcurrentHashMap<UID, StateGraphVO>();

	private final Map<UID, Collection<StateVO>> mpStatesByTransition
		= new ConcurrentHashMap<UID, Collection<StateVO>>();
	
	private final Map<UID, List<StateTransitionVO>> mpTransitionsByState
	= new ConcurrentHashMap<UID, List<StateTransitionVO>>();
	
	private final Map<FindKey, List<StateTransitionVO>> mpTransitionsByFindKey
	= new ConcurrentHashMap<FindKey, List<StateTransitionVO>>();
	
	private final Map<UID, Map<FieldMeta<?>, String>> mpResourceSIdsByStateUid
	= new ConcurrentHashMap<UID, Map<FieldMeta<?>, String>>();		

	StateCache() {
		INSTANCE = this;
	}
	
	/** 
	 * From BeanFactoryAware.
	 */
	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}
	
	/*
	 * @Autowired is not possible as StateFacadeBean uses StateCache (and vice versa).
	 * We escape to on-demand loading. 
	@Autowired 
	final void setStateFacadeLocal(StateFacadeLocal stateFacadeLocal) {
		this.statefacade = stateFacadeLocal;
	}
	 */
	
	StateFacadeLocal getStateFacadeLocal() {
		if (statefacade == null) {
			statefacade = beanFactory.getBean(StateFacadeLocal.class);
		}
		return statefacade;
	}
	
	@Autowired
	final void setMasterDataFacadeLocal(MasterDataFacadeLocal masterDataFacadeLocal) {
		this.masterDataFacade = masterDataFacadeLocal;
	}
	
	@Autowired
	final void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}
	
	@Autowired
	final void setNucletDalProvider(NucletDalProvider nucletDalProvider) {
		this.nucletDalProvider = nucletDalProvider;
	}
	
	/**
	 * @deprecated Use Spring injection.
	 */
	public static StateCache getInstance() {
		return INSTANCE;
	}

	/**
	 * get a single State by UID
	 * @param stateUid
	 * @return StateVO with given UID
	 */
	public StateVO getState(final UID stateUid) {
		try {
			if (!mpStatessByUid.containsKey(stateUid)) {
				LOG.info("Initializing StateCache for StateId {}", stateUid);

				final StateVO stateVO = MasterDataWrapper.getStateVO(
						masterDataFacade.get(E.STATE, stateUid));
				stateVO.setMandatoryFields(getStateFacadeLocal().findMandatoryFieldsByStateUID(stateVO.getId()));
				stateVO.setMandatoryColumns(getStateFacadeLocal().findMandatoryColumnsByStateUID(stateVO.getId()));
				mpStatessByUid.put(stateVO.getId(), stateVO);

				LOG.debug("FINISHED initializing status cache for StateId {}", stateUid);
			}
		}
		catch (CommonFinderException | CommonPermissionException ex) {
			throw new NuclosFatalException(ex);
		}
		return mpStatessByUid.get(stateUid);
	}
	
	public boolean hasState(final UID stateUid) {
		boolean retVal = false;
		
		if (mpStatessByUid.containsKey(stateUid)) {
			retVal = true;
		}
		return retVal;
	}
	
	public StateGraphVO getStateGraph(final UID stateModelUid) throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		if (!mpStatesGraphsByModel.containsKey(stateModelUid)) {
			mpStatesGraphsByModel.put(stateModelUid, getStateFacadeLocal().getStateGraph(stateModelUid));
		}
		return mpStatesGraphsByModel.get(stateModelUid);
	}
	
	public List<StateModelVO> getStatemodels() throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		if (lstStatemodels.isEmpty()) {
			for (MasterDataVO<UID> eoVO : masterDataFacade.getMasterData(E.STATEMODEL, null)){
				lstStatemodels.add(MasterDataWrapper.getStateModelVO(eoVO, getStateGraph(eoVO.getPrimaryKey())));	
			}
		}
		return lstStatemodels;
	}
	
	public StateModelVO getStatemodelById(UID uid) throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		for (StateModelVO smvo : getStatemodels()) {
			if (uid.equals(smvo.getId())) {
				return smvo;
			}
		}
		return null;
	}
	
	public StateModelVO getModelByState(StateVO state) throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		for (UID curCollection : this.mpStatesBymodelUid.keySet()) {
			Collection<StateVO> coll = this.mpStatesBymodelUid.get(curCollection);
			if (coll.contains(state)) {
				return getStatemodelById(curCollection);
			}
		}
		return null;
	}
	
	public Collection<StateVO> getStatesByTransition(UID transUID) {
		if (this.mpStatesByTransition.isEmpty()) {
			for (EntityObjectVO<UID> eoVO : nucletDalProvider.getEntityObjectProcessor(E.STATETRANSITION).getAll()) {
				final StateTransitionVO stateTransitionVO = MasterDataWrapper.getStateTransitionVO(
					new MasterDataVO<UID>(new MasterDataVO<UID>(eoVO, false), eoVO.getDependents()));
					
				StateVO stateSource = stateTransitionVO.getStateSourceUID() == null ? null : this.getState(stateTransitionVO.getStateSourceUID());
				StateVO stateTarget = getState(stateTransitionVO.getStateTargetUID());
				
				mpStatesByTransition.put(
					stateTransitionVO.getId(), Lists.newArrayList(stateSource, stateTarget));
			}
		}
		return mpStatesByTransition.get(transUID);		
	}

	/**
	 * get a Collection of StateVO by modelUid
	 * 
	 * @param modelUid
	 * @return Collection&lt;StateVO&gt; of all states in model with given UID
	 */
	public synchronized Collection<StateVO> getStatesByModel(final UID modelUid) {
		List<StateVO> retVal = new ArrayList<StateVO>();
		if (mpStatesBymodelUid.isEmpty()) {
			
			Collection<MasterDataVO<UID>> allMandatorFields = masterDataFacade.getMasterData(E.STATEMANDATORYFIELD, null);
			Collection<MasterDataVO<UID>> allMandatorCols = masterDataFacade.getMasterData(E.STATEMANDATORYCOLUMN, null);
			
			for (MasterDataVO<UID> eoVO :  masterDataFacade.getMasterData(E.STATE, null)) {
				final StateVO stateVO = MasterDataWrapper.getStateVO(eoVO);
				Set<MandatoryFieldVO> setMandatoryFields = new HashSet<MandatoryFieldVO>();
				Set<MandatoryColumnVO> setMandatoryCols = new HashSet<MandatoryColumnVO>();
				for (MasterDataVO<UID> md : allMandatorFields) {
					if (RigidUtils.equal(md.getFieldUid(E.STATEMANDATORYFIELD.state), eoVO.getPrimaryKey())) {
						setMandatoryFields.add(MasterDataWrapper.getMandatoryFieldVO(md));
					}
				}
				for (MasterDataVO<UID> md : allMandatorCols) {
					if (RigidUtils.equal(md.getFieldUid(E.STATEMANDATORYCOLUMN.state), eoVO.getPrimaryKey())) {
						setMandatoryCols.add(MasterDataWrapper.getMandatoryColumnVO(md));
					}
				}
				stateVO.setMandatoryFields(setMandatoryFields);
				stateVO.setMandatoryColumns(setMandatoryCols);
				
				if (!mpStatesBymodelUid.containsKey(stateVO.getModelUID()))
					mpStatesBymodelUid.put(stateVO.getModelUID(), new ArrayList<StateVO>());
				
				mpStatesBymodelUid.get(stateVO.getModelUID()).add(stateVO);
			}
		}
		
		if (mpStatesBymodelUid.containsKey(modelUid)) 
			retVal = mpStatesBymodelUid.get(modelUid);
		
		return retVal;
	}
	
	public StateTransitionVO getInitialTransistionByModel(UID modelUid) {
		if (!mpInitialTransitionBymodelUid.containsKey(modelUid)) {
			StateTransitionVO initialTransition = getStateFacadeLocal().findStateTransitionByNullAndTargetState(
					getStateByModelAndInitialState(modelUid).getPrimaryKey());
			
			if (initialTransition == null)
				throw new NuclosFatalException("getInitialTransistionByModel failed for modelId = " + modelUid);

			mpInitialTransitionBymodelUid.put(modelUid, initialTransition);
			LOG.debug("FINISHED initializing Transistion cache for ModelId {}", modelUid);
		}	
		return mpInitialTransitionBymodelUid.get(modelUid);
	}
	
	public List<StateTransitionVO> getOrderedTransitionsByStatemodel(UID modelUID) {
		List<StateTransitionVO> retVal = new ArrayList<StateTransitionVO>();
		for (StateVO svo : getStatesByModel(modelUID)) {
			for (StateTransitionVO trans : getTransitionsByState(svo.getPrimaryKey())) {
				if (!retVal.contains(trans))
					retVal.add(trans);
			}
		}			
		return retVal;
	}
	
	public List<StateTransitionVO> getTransitionsByState(UID stateUID) {		
		 List<StateTransitionVO> retVal = new ArrayList<StateTransitionVO>(); 
		 if (mpTransitionsByState.isEmpty()) {
			 for (MasterDataVO<UID> eoVO :  masterDataFacade.getMasterData(E.STATETRANSITION, null)) {
				
				StateTransitionVO trans = MasterDataWrapper.getStateTransitionVOWithoutDependants(eoVO);
				
				if (trans.getStateSourceUID() != null) {
					if (!mpTransitionsByState.containsKey(trans.getStateSourceUID()))
						mpTransitionsByState.put(trans.getStateSourceUID(), new ArrayList<StateTransitionVO>());
					
					mpTransitionsByState.get(trans.getStateSourceUID()).add(trans);
				}
				
				if (!mpTransitionsByState.containsKey(trans.getStateTargetUID()))
					mpTransitionsByState.put(trans.getStateTargetUID(), new ArrayList<StateTransitionVO>());
				mpTransitionsByState.get(trans.getStateTargetUID()).add(trans);				
			}
		 }
		 
		 if (mpTransitionsByState.containsKey(stateUID))
			 retVal = mpTransitionsByState.get(stateUID);
		 
		 return retVal;
	}
	
	public List<StateTransitionVO> getTransitionsByFindKey(FindKey key, IFindTransition finder) {
		if (!mpTransitionsByFindKey.containsKey(key)) {
			mpTransitionsByFindKey.put(key, finder.lookupStateTransition(key.cond, key.checkUniqueness, key.withDependants));
		}
		return mpTransitionsByFindKey.get(key);
	}
	
	public Map<FieldMeta<?>, String> getResourceSIdsForStateUid(UID stateUid) {
		if (!mpResourceSIdsByStateUid.containsKey(stateUid)) {
			List<FieldMeta<String>> lstFields = new ArrayList<FieldMeta<String>>();
			lstFields.add(E.STATE.labelres);
			lstFields.add(E.STATE.descriptionres);	
			lstFields.add(E.STATE.buttonRes);
			
			String[] resIds = LocaleUtils.getResourceIdsForFields(E.STATE, stateUid, lstFields);
			
			Map<FieldMeta<?>, String> mpResIds = new HashMap<FieldMeta<?>, String>();
			if(resIds != null && resIds.length >= 3) {
				mpResIds.put(E.STATE.labelres, resIds[0]);
				mpResIds.put(E.STATE.descriptionres, resIds[1]);
				mpResIds.put(E.STATE.buttonRes, resIds[2]);
				
			}
			mpResourceSIdsByStateUid.put(stateUid, mpResIds);
		}
		
		return mpResourceSIdsByStateUid.get(stateUid);
	}

	public StateVO getStateByModelAndInitialState(final UID modelUid) {
		if (!mpStateByModelAndInitialStateUid.containsKey(modelUid)) {
			LOG.info("Initializing StateCache for ModelID {}", modelUid);

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom s = query.from(E.STATE);
			DbFrom st = s.join(E.STATETRANSITION, JoinType.INNER, E.STATETRANSITION.state2, "t2");
			query.select(s.baseColumn(SF.PK_UID));
			query.where(builder.and(
				builder.equalValue(s.baseColumn(E.STATE.model), modelUid),
				st.baseColumn(E.STATETRANSITION.state1).isNull()));

			final List<UID> stateIds = dataBaseHelper.getDbAccess().executeQuery(query.distinct(true));

			if (stateIds.size() == 0)
				throw new NuclosFatalException("getStateByModelAndInitialState failed for modelId = "+modelUid);

			mpStateByModelAndInitialStateUid.put(modelUid,getState(stateIds.iterator().next()));
			LOG.debug("FINISHED initializing status cache for ModelId {}", modelUid);
		}
		return mpStateByModelAndInitialStateUid.get(modelUid);
	}

	public Collection<StateVO> getStatesByModule(UID moduleUid) {
		// special case for general search (all states for all modules)
		if (moduleUid == null) {
			moduleUid = UID.UID_NULL;
		}
		if (!mpStatesByModuleUid.containsKey(moduleUid)) {
			if (UID.UID_NULL.equals(moduleUid)) {
				final List<StateVO> lstStates = new ArrayList<StateVO>();
				final Collection<MasterDataVO<UID>> colStates = masterDataFacade.getMasterData(E.STATE, null);
				for (final MasterDataVO<UID> mdVO : colStates) {
					StateVO stateVO = MasterDataWrapper.getStateVO(mdVO);
					stateVO.setMandatoryFields(getStateFacadeLocal().findMandatoryFieldsByStateUID(stateVO.getId()));
					stateVO.setMandatoryColumns(getStateFacadeLocal().findMandatoryColumnsByStateUID(stateVO.getId()));
					lstStates.add(stateVO);
				}
				mpStatesByModuleUid.put(moduleUid, lstStates);
			}
			else {
				LOG.info("Initializing StateCache for ModuleID {}", moduleUid);
				// First get all models for a given module

				final Collection<StateVO> collstatevo = new ArrayList<>();
				for (final UID modelUid : StateModelUsagesCache.getInstance().getStateUsages().getStateModelUIDsByEntity(moduleUid))
				{
					collstatevo.addAll(this.getStatesByModel(modelUid));
				}
				mpStatesByModuleUid.put(moduleUid, collstatevo);
				LOG.debug("FINISHED initializing status cache for ModuleId {}", moduleUid);
			}
		}
		return mpStatesByModuleUid.get(moduleUid);
	}
	
	public interface IFindTransition {
		List<StateTransitionVO> lookupStateTransition(CollectableSearchCondition cond, boolean checkUniqueness, boolean withDependants);
	}
	
	public static class FindKey {
		private final CollectableSearchCondition cond;
		private final boolean checkUniqueness;
		private final boolean withDependants;
		
		public FindKey(CollectableSearchCondition cond,
				boolean checkUniqueness, boolean withDependants) {
			this.cond = cond;
			this.checkUniqueness = checkUniqueness;
			this.withDependants = withDependants;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof FindKey) {
				FindKey that = (FindKey) obj;
				return LangUtils.equal(cond, that.cond) && LangUtils.equal(checkUniqueness, that.checkUniqueness) && LangUtils.equal(withDependants, that.withDependants);
			}
			
			return false;
		}
		
		@Override
		public int hashCode() {
			return LangUtils.hash(cond, checkUniqueness, withDependants);
		}
		
	}

	/**
	 * Invalidate the whole cache
	 */
	private void invalidate() {
		LOG.debug("Invalidating StateCache");
		mpStatessByUid.clear();
		mpStatesBymodelUid.clear();
		mpStateByModelAndInitialStateUid.clear();
		mpInitialTransitionBymodelUid.clear();
		mpStatesByModuleUid.clear();
		lstStatemodels.clear();
		mpStatesByTransition.clear();
		mpTransitionsByState.clear();
		mpTransitionsByFindKey.clear();
		mpStatesGraphsByModel.clear();
		mpResourceSIdsByStateUid.clear();
	}

	@Override
	public void invalidateCache(boolean notifyClients, boolean notifyClusterCloud) {
		invalidate();
		if(notifyClusterCloud) {
			notifyClusterCloud();
		}
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.STATE_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);		
	}

	@Override
	public void registerCache() {
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {
		return null;
	}	

}	// class StateCache
