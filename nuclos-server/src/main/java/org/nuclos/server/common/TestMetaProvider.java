//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.nuclos.common2.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

/**
 * An extension of {@link MetaProvider} useful for (non-trivial) unit tests.
 * <p>
 * It enables entity/field meta data retrieval from XML dumps produced with
 * JMX by {@link MetaProvider#dumpAllEntities(boolean)}. This way it is 
 * possible to unit test Nuclos extension code without much hassle.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.0.8
 */
public class TestMetaProvider extends MetaProvider implements ResourceLoaderAware {
	
	private static final Logger LOG = LoggerFactory.getLogger(TestMetaProvider.class);
	
	private final String xmlEntityFile;
	
	private ResourceLoader resourceLoader;
	
	/**
	 * Constructor for creating the unit test server meta data provider.
	 * <p>
	 * Normally this constructor is invoked from within a XML spring bean
	 * definition file.
	 * 
	 * @param xmlEntityFile location of the XML entity/field meta data dump 
	 * 		to use. This will be converted to the Spring {@link Resource} 
	 * 		abstraction. 
	 */
	public TestMetaProvider(String xmlEntityFile) {
		if (StringUtils.isNullOrEmpty(xmlEntityFile)) {
			throw new NullPointerException();
		}
		this.xmlEntityFile = xmlEntityFile;
	}
	
	@Override
	@PostConstruct
	void init() {
		dataCache = new TestDataCache();
		dataCache.buildMaps(true);
	}

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
	
	class TestDataCache extends DataCache {
		
		@Override
		public void buildMaps(boolean forceReload) {
			File f;
			try {
				f = resourceLoader.getResource(xmlEntityFile).getFile();
				recoverEntities(f.getAbsolutePath(), true);
			} catch (IOException e) {
				LOG.error("TestMetaProvider.buildMaps failed: ", e);
			}
		}
		
	}

}
