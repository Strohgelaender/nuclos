//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.nuclos.common.AbstractParameterProvider;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.i18n.language.data.DataLanguageVO;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * <code>ParameterProvider</code> for the server side.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph Radig</a>
 * @version 00.01.000
 */
public class ServerParameterProvider extends AbstractParameterProvider implements ClusterCache, InitializingBean {


	private static final Logger LOG = LoggerFactory.getLogger(ServerParameterProvider.class);
	
	private static final String NULL = "<null>";
	
	private static ServerParameterProvider INSTANCE;
	
	//

	/**
	 * Map<String sName, String sValue>
	 */
	private final Map<String, String> mpParameters = new ConcurrentHashMap<String, String>();

	/**
	 * Map<UID PK, String sValue>
	 */
	private final Map<UID, String> mpPK2ParameterName = new ConcurrentHashMap<UID, String>();


	//private final ClientNotifier clientnotifier = new ClientNotifier(JMSConstants.TOPICNAME_PARAMETERPROVIDER);
	
	private SpringDataBaseHelper dataBaseHelper;

	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static ServerParameterProvider getInstance() {
		return INSTANCE;
	}

	protected ServerParameterProvider() { 
		INSTANCE = this;
	}
	
	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (mpParameters.isEmpty()) {
			clearAndloadParameters();
		}
	}

	/**
	 * @return Map&lt;String sName, String sValue&gt;
	 */
	@ManagedAttribute
	public Map<String, String> getAllParameters() {
		return Collections.unmodifiableMap(mpParameters);
	}

	@ManagedOperation(description="Get the value of the given parameter")
	@ManagedOperationParameters({
			@ManagedOperationParameter(name="sParameter", description="name of the parameter")
	})
	@Override
	public String getValue(String sParameter) {
		final String result = mpParameters.get(sParameter);
		if (NULL.equals(result)) {
			return null;
		}
		return result;
	}

	public String getValueForPK(UID pk) throws CommonFinderException {
		if (!mpPK2ParameterName.containsKey(pk)) {
			throw new CommonFinderException();
		}
		String sParameter = mpPK2ParameterName.get(pk);
		if (sParameter != null) {
			return getValue(sParameter);
		}
		return null;
	}

	/**
	 * gets all parameters
	 * @return Pair<Map<String,String>, Map<String,String>>
	 */
	private void clearAndloadParameters() {
		Map<String,String> rawParameters = new HashMap<String, String>();
		Map<String, String> properties = getParameterDefaults();
		if (properties != null) {
			rawParameters.putAll(properties);
		}

		Pair<Map<String,String>, Map<UID,String>> paramsFromDB = getParametersFromDB();

		rawParameters.putAll(paramsFromDB.x);

		final Map<String,String> locMpParameters = new HashMap<String,String>();
		for(String sParameter : rawParameters.keySet()) {
			String value = rawParameters.get(sParameter);			
			if(value != null && !value.isEmpty()) {
				locMpParameters.put(sParameter, replaceParameter(rawParameters, value));
			}
			else {
				locMpParameters.put(sParameter, NULL);
			}

			String ovr = System.getProperty("override." + sParameter.replace(' ', '_'));
			if(ovr != null)
				locMpParameters.put(sParameter, ovr);
		}

		mpParameters.clear();
		mpParameters.putAll(locMpParameters);
		mpPK2ParameterName.clear();
		mpPK2ParameterName.putAll(paramsFromDB.y);

	}

	public Collection<DataLanguageVO> getDataLanguages() {
		
		List<DataLanguageVO> retVal = new ArrayList<DataLanguageVO>();
		
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.DATA_LANGUAGE);
		query.multiselect(t.baseColumn(E.DATA_LANGUAGE.language),
						  t.baseColumn(E.DATA_LANGUAGE.country),
						  t.baseColumn(E.DATA_LANGUAGE.primaryLanguage),
						  t.baseColumn(E.DATA_LANGUAGE.order));
		
		query.orderBy(
				builder.desc(t.baseColumn(E.DATA_LANGUAGE.primaryLanguage)),
				builder.asc(t.baseColumn(E.DATA_LANGUAGE.order)));
		
		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
			DataLanguageVO part = new DataLanguageVO(
					tuple.get(0, String.class),	
					tuple.get(1, String.class),
					tuple.get(2, Boolean.class), 
					tuple.get(3, Integer.class));
			retVal.add(part);
		}

		return retVal;
	}
	
	/**
	 * gets all parameter entries from the database.
	 * @return Pair<Map<String,String>, Map<String,String>>
	 */
	private Pair<Map<String, String>, Map<UID, String>> getParametersFromDB() {
		LOG.debug("START building parameter cache.");

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.PARAMETER);
		query.multiselect(t.basePk(), t.baseColumn(E.PARAMETER.name), t.baseColumn(E.PARAMETER.value));

		Map<String, String> result = new HashMap<String, String>();
		Map<UID, String> paramMap = new HashMap<UID, String>();

		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
			UID pk = tuple.get(0, UID.class);
			String name = tuple.get(1, String.class);
			String value = tuple.get(2, String.class);
			paramMap.put(pk, name);
			result.put(name, value);
		}

		LOG.debug("FINISHED building parameter cache.");
		return new Pair<>(result, paramMap);
	}

	/**
	 * updates existing system parameters
	 */
	public void updateSystemParameters(Map<String, String> parameters) {

		IEntityObjectProcessor<UID> eop = NucletDalProvider.getInstance().getEntityObjectProcessor(E.PARAMETER);
		List<EntityObjectVO<UID>> parameterEVOs = eop.getAll();

		for (EntityObjectVO<UID> eovo: parameterEVOs) {
			String key = eovo.getFieldValue(E.PARAMETER.name);
			String value = parameters.get(key);
			if (value != null) {
				eovo.setFieldValue(E.PARAMETER.value, value);
				eovo.flagUpdate();
				eop.insertOrUpdate(eovo);
			}
		}
		invalidateCache(false, true);
	}
	
	private static Map<String, String> getParameterDefaults() {
		Properties defaults = new Properties();
		InputStream is = LangUtils.getClassLoaderThatWorksForWebStart().getResourceAsStream("resources/parameter-defaults.properties");
		if (is == null) {
			throw new NuclosFatalException("Missing server resource parameter-defaults.properties");
		}
		try {
			defaults.load(is);
			is.close();
		} catch (IOException ex) {
			throw new NuclosFatalException("Error loading server resource parameter-defaults.properties", ex);
		}
		return (Map) defaults;
	}

	/**
	 * replace the user defined pattern with the parameter values
	 * @param mpParameters
	 * @param sParameterValue
	 * @return
	 */
    private static String replaceParameter(Map<String, String> mpParameters,String sParameterValue) {
       int sidx = 0;
       while ((sidx = sParameterValue.indexOf("${", sidx)) >= 0) {
           int eidx = sParameterValue.indexOf("}", sidx);
           String sParameter = sParameterValue.substring(sidx + 2, eidx);

           String rep = findReplacement(mpParameters, sParameter);
           sParameterValue = sParameterValue.substring(0, sidx) + rep + sParameterValue.substring(eidx + 1);
           if (rep != null) // nullpointer
         	  sidx = sidx + rep.length();
      }
       // check if there is more than one variable to replace (e.g. ${NuclosCodeGenerator Output Path}/wsdl), if so, do it again
       if (sParameterValue.indexOf("${") != -1)
      	 sParameterValue = replaceParameter(mpParameters, sParameterValue);

      return sParameterValue;
  }

	/**
	 * replace a single parameter pattern with the value
	 * @param mpParameters
	 * @param sParameter
	 * @return parameter value or <code>IllegalArgumentException</code> if parameter has no value
	 */
    private static String findReplacement(Map<String, String> mpParameters, String sParameter) {

       if(mpParameters.containsKey(sParameter))
    	   return mpParameters.get(sParameter);
       else if(getSystemParameters().containsKey(sParameter))
    	   return getSystemParameters().get(sParameter);
       else
    	   throw new IllegalArgumentException("sParameter");

    }	

	private void notifyClients(String sMessage, boolean bAfterCommit) {
		LOG.debug("JMS send: notify clients that parameters changed: {}", this);
		if(bAfterCommit) {
			NuclosJMSUtils.sendOnceAfterCommitDelayed(JMS_MESSAGE_ALL_PARAMETERS_ARE_REVALIDATED, JMSConstants.TOPICNAME_PARAMETERPROVIDER);
		}
		else {
			NuclosJMSUtils.sendMessage(JMS_MESSAGE_ALL_PARAMETERS_ARE_REVALIDATED, JMSConstants.TOPICNAME_PARAMETERPROVIDER, null);
		}
	}
	
	/**
	 * @return current system environment
	 */
	public static Map<String, String> getSystemParameters() {
		return System.getenv();
	}

	@ManagedAttribute
	public Collection<String> getParameterNames() {
		// defensive copy is needed for JMX
		return new ArrayList<String>(mpParameters.keySet());
	}

	@Override
	public void invalidateCache(boolean notifyClients,	boolean notifyClusterCloud) {
		clearAndloadParameters();
		if(notifyClients) {
			notifyClients(JMS_MESSAGE_ALL_PARAMETERS_ARE_REVALIDATED, notifyClusterCloud);
		}
		if(notifyClusterCloud) {
			notifyClusterCloud();
		}
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.PARAMTER_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);		
	}

	@Override
	public void registerCache() {
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {
		return null;
	}

	@ManagedAttribute(description="get the size (number of parameters) of mpParameters")
	public int getNumberOfParameters() {
		return mpParameters.size();
	}

}	// class ServerParameterProvider
