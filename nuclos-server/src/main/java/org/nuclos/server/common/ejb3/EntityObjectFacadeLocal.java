package org.nuclos.server.common.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.common.CommonEntityObjectFacade;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;

public interface EntityObjectFacadeLocal extends CommonEntityObjectFacade {

	<PK> void createOrUpdatePlainWithoutPermissionCheck(EntityObjectVO<PK> entity) throws CommonPermissionException;

	List<CollectableValueIdField> getReferenceList(
			FieldMeta<?> efMeta,
			String search,
			UID vlpUID, Map<String, Object> vlpParameter,
			String defaultValueColumn,
			Long iMaxRowCount,
			UID mandator
	) throws CommonBusinessException;

	String getFieldGroupName(UID groupID);

	void evictGroupNamesCache();

	List<CollectableValueIdField> getProcessByEntity(UID entityUid, boolean bSearchMode);

	<PK> UsageCriteria getUsageCriteriaForPK(PK pk, UID entityUID, String customUsage) throws CommonBusinessException;

	<PK> void lockInNewTransaction(UID entityUID, PK pk, UID userUID) throws CommonPermissionException;

	<PK> UID lockInNewTransaction(UID entityUID, PK pk) throws CommonPermissionException;

	<PK> void unlockInNewTransaction(UID entityUID, PK pk) throws CommonPermissionException;

	<PK> void unlockInNewTransactionAfterCommit(UID entityUID, PK pk) throws CommonPermissionException;

	<PK> ProxyList<PK, EntityObjectVO<PK>> getEntityObjectProxyListNoCheck(
			UID entity,
			CollectableSearchExpression clctexpr,
			Collection<UID> fields,
			String customUsage
	);

	<PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunkNoCheck(
			UID entity,
			CollectableSearchExpression clctexpr,
			ResultParams resultParams,
			String customUsage
	);

	Long countEntityObjectRowsNoCheck(UID entity, CollectableSearchExpression clctexpr);

	<PK> Collection<EntityObjectVO<PK>> getDependentEntityObjectsNoCheck(UID entity, UID oreignKeyField, PK oRelatedId);

	<PK> void setDefaultColumnValue(EntityObjectVO<PK> eo, FieldMeta<?> fMeta);

	boolean clearUserPreferences(String userName) throws CommonFinderException;

}
