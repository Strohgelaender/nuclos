//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.PlainSubCondition;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.database.query.SelectQuery;
import org.nuclos.common.database.query.definition.Column;
import org.nuclos.common.database.query.definition.Join;
import org.nuclos.common.database.query.definition.Join.JoinType;
import org.nuclos.common.database.query.definition.Schema;
import org.nuclos.common.database.query.definition.Table;
import org.nuclos.common.dblayer.IFieldRef;
import org.nuclos.common.querybuilder.DatasourceParameterParser;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.querybuilder.DatasourceXMLParser;
import org.nuclos.common.querybuilder.DatasourceXMLParser.XMLConnector;
import org.nuclos.common.querybuilder.DatasourceXMLParser.XMLTable;
import org.nuclos.common.querybuilder.NuclosDatasourceException;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.ChartVO;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceParameterValuelistproviderVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.valuelistprovider.VLPQuery;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.incubator.DbExecutor.LimitedResultSetRunner;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.report.WhereConditionParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class DatasourceServerUtils {
	
	private static final Logger LOG	= LoggerFactory.getLogger(DatasourceServerUtils.class);

	private final SQLCache sqlCache = new SQLCache();	
	//
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	@Autowired
	private SessionUtils utils;
	
	@Autowired
	private SchemaCache schemaCache;
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;

	@Autowired
	private MandatorUtils mandatorUtils;
	
	public DatasourceServerUtils() {
	}
	
	public void invalidateCache() {
		sqlCache.invalidate();
	}

	/**
	 * get sql string for datasource definition
	 *
	 * @param dataSourceUID id of datasource
	 * @return string containing sql
	 */
	public String createSQL(final UID dataSourceUID,
	    final Map<String, Object> mpParams, UID mandator, UID language) throws NuclosDatasourceException {
		try {
			final DatasourceVO vo = DatasourceCache.getInstance().getDatasourcesByUser(
			    dataSourceUID, utils.getCurrentUserName(), userCtx.getMandatorUID());
			return createSQL(vo, mpParams, mandator, language);
		}
		catch(CommonPermissionException e) {
			throw new NuclosDatasourceException("createSQL failed: " + e, e);
		}
	}

	/**
	 * get sql string for report execution.
	 * There is no authorization as the authorization is applied to the report.
	 *
	 * @param dataSourceUID data source UID
	 * @return string containing sql
	 */
	public String createSQLForReportExecution(final UID dataSourceUID, Map<String, Object> mpParams, UID language) throws NuclosDatasourceException {
		DatasourceVO vo = DatasourceCache.getInstance().getDatasource(dataSourceUID);
		return createSQL(vo, mpParams, null, language);
	}

	/**
	 * get sql string for datasource definition without parameter definition
	 *
	 * @param sDatasourceXML xml of datasource
	 * @return string containing sql
	 */
	public String createSQL(DatasourceVO datasourcevo) throws NuclosDatasourceException {
		return sqlCache.getSQL(datasourcevo);
	}

	/**
	 *
	 * @param sDatasourceXML
	 * @param mpParams
	 * @return new PlainSubCondition with '(SELECT intid FROM ( ... ) ds )' condition
	 * @throws NuclosDatasourceException
	 */
	public PlainSubCondition getConditionWithIdForInClause(DatasourceVO datasourcevo, Map<String, Object> mpParams, UID mandator, UID language) throws NuclosDatasourceException {
		return new PlainSubCondition(getSqlWithIdForInClause(datasourcevo, mpParams, mandator, language));
	}
	/**
	 *
	 * @param sDatasourceXML
	 * @param mpParams
	 * @return
	 * @throws NuclosDatasourceException
	 */
	public String getSqlLikeWithIdForInClause(DatasourceVO datasourcevo, Map<String, Object> mpParams, String search, UID mandator, UID language) throws NuclosDatasourceException {
		//@see NUCLOSINT-1347
		String sqlSelect = null;
		String sql = createSQL(datasourcevo, mpParams, mandator, language);
		
		final String sIdField = (String)mpParams.get(ValuelistProviderVO.DATASOURCE_IDFIELD);
		final String sNameField = (String)mpParams.get(ValuelistProviderVO.DATASOURCE_NAMEFIELD);
		if (StringUtils.isNullOrEmpty(sIdField) || StringUtils.isNullOrEmpty(sNameField)) {
			throw new IllegalArgumentException("id-field or name-field is needed to construct the sql clause here.");
		}
		
		//NOAINT-616. Get correct case and quotes for the fields
		
		String sIdField2Use = DataSourceCaseSensivity.getFieldWithCorrectCaseAndQuotation(sIdField, sql);
		String sNameField2Use = DataSourceCaseSensivity.getFieldWithCorrectCaseAndQuotation(sNameField, sql);
		
		sqlSelect = "(SELECT ds." + sIdField2Use + " FROM (" + sql + ") ds" +
				" where UPPER(" + sNameField2Use + ") LIKE '" + search + "')";
		
		return sqlSelect;
	}

	/**
	 *
	 * @param sDatasourceXML
	 * @param mpParams
	 * @return
	 * @throws NuclosDatasourceException
	 */
	public String getSqlWithIdForInClause(DatasourceVO datasourcevo, Map<String, Object> mpParams, UID mandator, UID language) throws NuclosDatasourceException {
		//@see NUCLOSINT-1347
		String sqlSelect = null;
		String sql = createSQL(datasourcevo, mpParams, mandator, language);
		String sIdField = (String)mpParams.get(ValuelistProviderVO.DATASOURCE_IDFIELD);
		if (StringUtils.isNullOrEmpty(sIdField)) {
			int idxIntid = sql.toLowerCase().indexOf("intid");
			if (idxIntid != -1) {
				sIdField = sql.substring(idxIntid, idxIntid+5);
			}
		}
		if (StringUtils.isNullOrEmpty(sIdField)) {
			throw new IllegalArgumentException("id-field or name-field is needed to construct the sql clause here.");
 		}
		sqlSelect = "(SELECT ds." + (!sql.contains("\"" + sIdField + "\"") ? sIdField : "\"" + sIdField + "\"") + " FROM (" + sql + ") ds)";
 		return sqlSelect;
	}

	/**
	 *
	 * @param sDatasourceXML
	 * @param mpParams
	 * @return
	 * @throws NuclosDatasourceException
	 */
	public String getSqlQueryForId(DatasourceVO datasourcevo, Map<String, Object> mpParams, Object id) throws NuclosDatasourceException {
		if (id == null) {
			throw new NullPointerException();
		}
		
		// Avoid SQL injection (tp)
		if (!(id instanceof Number) && !(id instanceof UID)) {
			throw new IllegalArgumentException("id is " + id + " of " + id.getClass().getName());
		}
		
		//@see NUCLOSINT-1347
		String sqlSelect = null;
		String sql = createSQL(datasourcevo, mpParams, null, null);

		String sIdField = (String)mpParams.get(ValuelistProviderVO.DATASOURCE_IDFIELD);
		if (StringUtils.isNullOrEmpty(sIdField)) {
			int idxIntid = sql.toLowerCase().indexOf("intid");
			if (idxIntid != -1) {
				sIdField = sql.substring(idxIntid, idxIntid+5);
			}
 		}
		if (StringUtils.isNullOrEmpty(sIdField)) {
			throw new IllegalArgumentException("id-field or name-field is needed to construct the sql clause here.");
 		}
		sqlSelect = "SELECT * FROM (" + sql + ") ds WHERE ds."
				+ (!sql.contains("\"" + sIdField + "\"") ? sIdField : "\"" + sIdField + "\"") + "= " + (id instanceof UID ? ((UID)id).getString() : String.valueOf(id));
		return sqlSelect;
	}

	public String createSQL(DatasourceVO datasourcevo, Map<String, Object> mpParams, UID dataMandator) throws NuclosDatasourceException {
		return createSQL(datasourcevo, mpParams, dataMandator, getDataLanguage());
	}
	/**
	 * get sql string for datasource definition
	 *
	 * @param datasourcevo contains xml of datasource
	 * @return string containing sql
	 */
	public String createSQL(DatasourceVO datasourcevo, Map<String, Object> mpParams, UID dataMandator, UID language) throws NuclosDatasourceException {
		String result = sqlCache.getSQL(datasourcevo);
		if (mpParams.get("username") == null) {
			// username is already set. seems to be from preview of the datasource. @see NUCLOSINT-1560
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null && auth.getPrincipal() != null) {
				mpParams.put("username", auth.getPrincipal());
			}
		}
		if (language != null) {
			mpParams.put("locale", language.toString());
		}
		if (mpParams.get("mandator") == null) {
			final StringBuffer sbMandator = new StringBuffer();
			if (SecurityCache.getInstance().isMandatorPresent()) {
				if (SystemFields.isMandatorColumn(dataMandator)) {
					result = result.replaceAll(Matcher.quoteReplacement("'$mandator'"), Matcher.quoteReplacement("$mandator"));
					String alias = "nuclos_" + E.MANDATOR_ACCESSIBLE.getUID().getString();
					sbMandator.append("SELECT " + alias + "." + E.MANDATOR_ACCESSIBLE.accessible.getDbColumn());
					sbMandator.append(" FROM " + E.MANDATOR_ACCESSIBLE.getDbTable() + " " + alias);
					sbMandator.append(" WHERE " + alias + "." + E.MANDATOR_ACCESSIBLE.mandator.getDbColumn() + " = " + dataMandator.getString());
				} else {
					final Set<UID> mandators;
					if (dataMandator == null) {
						mandators = SecurityCache.getInstance().getAccessibleMandators(userCtx.getMandatorUID());
					} else {
						mandators = MandatorUtils.getAccessibleMandatorsByLogin(dataMandator, userCtx.getMandatorUID());
					}
					
					final Iterator<UID> it = mandators.iterator(); 
					boolean isFirst = true;
					while (it.hasNext()) {
						if (isFirst) {
							isFirst = false;
						} else {
							sbMandator.append('\'');
						}
						sbMandator.append(it.next());
						if (it.hasNext()) {
							sbMandator.append('\'');
							sbMandator.append(',');
						}
					}
				}
			}
			mpParams.put("mandator", sbMandator.toString());
		}
		result = replaceParameters(result, mpParams, getParameterDefinitionMap(datasourcevo));
		LOG.debug(result);

		return result;
	}

	/**
	 *
	 * @param sDatasourceXML
	 * @return sql with original parameter placeholders
	 * @throws NuclosDatasourceException
	 */
	public String createSQLOriginalParameter(DatasourceVO datasourcevo) throws NuclosDatasourceException {
		return sqlCache.getSQL(datasourcevo);
	}

	/**
	 * get sql string for datasource definition
	 *
	 * @param datasourcevo
	 * @return string containing sql
	 */
	private String createSQLForCaching(DatasourceVO datasourcevo) throws NuclosDatasourceException {
		final String sDatasourceXML = datasourcevo.getSource();
		final Object objLOCK = new Object();
		synchronized (objLOCK) {

			String result = null;
			final SelectQuery query = new SelectQuery(utils.getCurrentUserName());

			final DatasourceXMLParser.Result parseresult = parseDatasourceXML(sDatasourceXML);

			if(parseresult.isModelUsed()) {
				final Schema schema = schemaCache.getCurrentSchema();

				// create from clause
				final Map<String, Table> fromTables = new HashMap<String, Table>();
				for (XMLTable xmlTable : parseresult.getMapXMLTables().values()) {
					Table t = createTable(schema, xmlTable, datasourcevo);
					fromTables.put(t.getAlias(), t);
					query.addToFromClause(t);
				}

				for(XMLConnector xmlconnector : parseresult.getLstConnectors()) {
					if (fromTables.get(xmlconnector.getSrcTable()) == null) {
						continue;
					}
					final Table srcTable = (Table) (fromTables.get(xmlconnector.getSrcTable())).clone();
					final Column srcColumn = new Column(srcTable.getColumn(xmlconnector.getSrcColumn()));
					if (fromTables.get(xmlconnector.getDstTable()) == null) {
						continue;
					}
					final Table dstTable = (Table) (fromTables.get(xmlconnector.getDstTable())).clone();
					final Column dstColumn = new Column(dstTable.getColumn(xmlconnector.getDstColumn()));
					final JoinType joinType = xmlconnector.getJoinType() == null ? JoinType.INNER_JOIN :
						KeyEnum.Utils.findEnum(JoinType.class, xmlconnector.getJoinType());
					Join join = new Join(srcColumn, joinType == null ? JoinType.INNER_JOIN : joinType, dstColumn);
					query.addJoin(join);
				}

				// create select, where, group by, order by claues
				final boolean bGroupBy = checkGroupBy(parseresult.getLstColumns());
				final List<String> lstColumnConditions = new ArrayList<String>();

				for(DatasourceXMLParser.XMLColumn xmlcolumn : parseresult.getLstColumns()) {
					final Column column = createColumn(fromTables, xmlcolumn);
					if (column == null) {
						continue;
					}
					
					if(xmlcolumn.isVisible()) {
						query.addToSelectClause(column, xmlcolumn.getAlias());
					}
					if(bGroupBy) {
						if(xmlcolumn.getGroup() == null || xmlcolumn.getGroup().length() == 0) {
							xmlcolumn.setGroup(DatasourceVO.GroupBy.MAX.getLabel());
						}
						else if(xmlcolumn.getGroup().equals(DatasourceVO.GroupBy.GROUP.getLabel())) {
							query.addToGroupByClause(column, xmlcolumn.getGroup());
						}
						else {
							query.addToGroupByMap(column, xmlcolumn.getGroup());
						}
					}
					lstColumnConditions.clear();
					final WhereConditionParser whereConditionParser = new WhereConditionParser();
					for(DatasourceXMLParser.XMLCondition xmlcondition : xmlcolumn.getLstConditions()) {
						if(xmlcondition.getCondition() != null && xmlcondition.getCondition().length() > 0) {
							final String sCondition = whereConditionParser.parseCondition(column, xmlcondition.getCondition());
							lstColumnConditions.add(sCondition);
						}
					}

					if(lstColumnConditions.size() > 0) {
						query.addColumnWhereClauses(lstColumnConditions);
					}
					if(xmlcolumn.getSort() != null && xmlcolumn.getSort().length() > 0) {
						query.addToOrderByClause(column, xmlcolumn.getSort().equals(DatasourceVO.OrderBy.ASCENDING.getLabel()));
					}
				}
				result = query.getSelectStatement(parseresult.isEntityOptionDynamic(),
							SpringDataBaseHelper.getInstance().getDbAccess().getDbType().equals(DbType.MSSQL));
			}
			else {
				result = parseresult.getQueryStringFromXml();
			}

			LOG.debug(result);
			return result;
		}
	}

	/**
	 * parses the datasource XML
	 *
	 * @param sDatasourceXML
	 * @throws NuclosFatalException
	 * @throws NuclosDatasourceException
	 */
	public DatasourceXMLParser.Result parseDatasourceXML(
	    String sDatasourceXML) throws NuclosFatalException,
	    NuclosDatasourceException {
		return DatasourceXMLParser.parse(sDatasourceXML);
	}

	/**
	 * return a copy of a Table in Schema because more than one instance is
	 * possible in the Datasource
	 *
	 * @param schema
	 * @param xmltable
	 * @return a copy of the specified table from the schema.
	 */
	private Table createTable(Schema schema,
	    DatasourceXMLParser.XMLTable xmltable,
	    DatasourceVO datasourcevo) {
		final Table table = schema.getTable(xmltable.getEntity());
		if(table != null) {
			// cache the columns in schema
			schemaCache.getColumns(table);
			final Table resultTable = (Table) table.clone();
			resultTable.setAlias(xmltable.getId());	

			try {
				EntityMeta<?> eMeta = MetaProvider.getInstance().getByTablename(xmltable.getEntity());

				if (!(datasourcevo instanceof DynamicEntityVO) && !(datasourcevo instanceof ChartVO)
						&& SecurityCache.getInstance().isMandatorPresent()) {
					if (eMeta.isMandator()) {
						String tableLocalReplacement = null;
						if (eMeta.IsLocalized()) {
							tableLocalReplacement = createLocalTableJoinSQL(eMeta, resultTable);
						}
						String tableNameReplacement = "(SELECT * FROM " + (tableLocalReplacement != null ? tableLocalReplacement : resultTable.getName()) + " WHERE " + SF.MANDATOR_UID.getDbColumn() + " in ('$mandator'))";
						resultTable.setRestriction(tableNameReplacement);
					} else {
						if (eMeta.IsLocalized()) {
							resultTable.setRestriction(
									createLocalTableJoinSQL(eMeta, resultTable));
						}
					}
				} else {
					if (eMeta.IsLocalized()) {
						resultTable.setRestriction(
								createLocalTableJoinSQL(eMeta, resultTable));
					}
				}
			} catch (CommonFatalException cfe) {
				// not an entity table; typically it's another data source.
			}
			return resultTable;
		}
		return new Table(schema.getTimestamp(), "");
	}

	private String createLocalTableJoinSQL(EntityMeta<?> eMeta,
			Table resultTable) {
		
		String tableName = resultTable.getName();
		
		NucletEntityMeta langEntityMeta = DataLanguageServerUtils.createEntityLanguageMeta(
				new EntityMetaVO(eMeta, true), SF.PK_ID.getMetaData(eMeta));
		UID entityRefField = DataLanguageServerUtils.extractForeignEntityReference(eMeta.getUID());
		UID entityRefDataLang = DataLanguageServerUtils.extractDataLanguageReference(eMeta.getUID());
		
		
		String tableLangName = langEntityMeta.getDbTable();
		
		List<String> columnsToSelect = new ArrayList<String>();
		List<String> langColumnsToSelect = new ArrayList<String>();
		
		for (Column c : resultTable.getColumns()) {
			columnsToSelect.add(c.getName().toLowerCase());
		}
	
		for (FieldMeta fm : eMeta.getFields()) {
			if (!fm.isCalculated()) {
				if (fm.isLocalized() && fm.getCalcFunction() == null) {		
					String dbColumn = fm.getDbColumn().toLowerCase();
					if (columnsToSelect.contains(dbColumn)) {
						columnsToSelect.remove(dbColumn);
					}
					UID langfield = DataLanguageServerUtils.extractFieldUID(fm.getUID());
					FieldMeta<?> fieldLang = langEntityMeta.getField(langfield);
					langColumnsToSelect.add(fieldLang.getDbColumn() + " as " + dbColumn);
				} 											
			}
		}
		
		String columnsToSelectAsString = "";
		
		for (int idx = 0; idx < columnsToSelect.size(); idx++) {
			columnsToSelectAsString += "TNL1." + columnsToSelect.get(idx);
			if (idx != columnsToSelect.size() -1) {
				columnsToSelectAsString += ",";
			}
		}
		for (int idx = 0; idx < langColumnsToSelect.size(); idx++) {
			columnsToSelectAsString += 
					columnsToSelectAsString.length() > 0 ? ", " + "TNL2." + langColumnsToSelect.get(idx) : "TNL2." + langColumnsToSelect.get(idx);
		}
		
		return "(SELECT " + columnsToSelectAsString + " FROM " + tableName + " TNL1, " + tableLangName + " TNL2 " + 
		" WHERE TNL1.intid = TNL2." + langEntityMeta.getField(entityRefField).getDbColumn() + 
		" AND TNL2." + langEntityMeta.getField(entityRefDataLang).getDbColumn()+ " = '$locale')";
	}
	
	public UID getDataLanguage() {
		return userCtx.getDataLocal();
	}
	
	public UID getReportLanguageToUse(Collection<MasterDataVO<UID>> lstModules, UID language, Long pk) 
			throws NuclosReportException {
		
		UID retVal = language; 
		EntityMeta<?> entityMeta = null;
		
		String pathToDataLangTable = null;
		
		for (MasterDataVO<UID> mdvo : lstModules) {
			UID entity = mdvo.getFieldUid(E.REPORTUSAGE.module);
			if (entity != null && MetaProvider.getInstance().getEntity(entity).getDataLangRefPath() != null) {
				String foundRef = MetaProvider.getInstance().getEntity(entity).getDataLangRefPath();
				if (pathToDataLangTable == null) {
					pathToDataLangTable = foundRef;
					entityMeta = MetaProvider.getInstance().getEntity(entity); 
				} else {
					if (!foundRef.equals(pathToDataLangTable)) {
						throw new NuclosReportException("This report uses more than one business object that contains a path to the data language table. Only one is allowed");
					}
				}
			}
		}
		
		if (entityMeta == null) {
			return language;
		}
		
		EntityMeta<?> langEntityMeta = MetaProvider.getInstance().getEntity(NucletEntityMeta.getEntityLanguageUID(entityMeta));
		UID entityRefField = DataLanguageServerUtils.extractForeignEntityReference(entityMeta.getUID());
		
		
		String dbNameReferencingEntity = entityMeta.getDbTable();			
		String[] pathToDLTable = entityMeta.getDataLangRefPath().split(";");
		
		String sql = "SELECT T1.intid, T2.struid_t_md_data_language FROM " + dbNameReferencingEntity + " T1 JOIN " + langEntityMeta.getDbTable() + " T2 \n"
				+ "ON T1.intid = " + pk + " AND T1.intid = " + langEntityMeta.getField(entityRefField).getDbColumn() + "\n";
		
		if (pathToDataLangTable != null) {
			
			StringBuilder s = new StringBuilder();
			String upperTableName = "T1";		
			String upperTableRefName = "intid";
		
			
			for(int idx=0; idx < pathToDLTable.length; idx++) {
				s.setLength(0);
				
				String reference = pathToDLTable[idx];
				UID referenceUID = new UID(reference);
				
				FieldMeta<?> referencingFieldMeta = MetaProvider.getInstance().getEntityField(referenceUID);
				
				EntityMeta<?> referencingEntityMeta = MetaProvider.getInstance().getEntity(referencingFieldMeta.getEntity());
				EntityMeta<?> referencedEntityMeta = MetaProvider.getInstance().getEntity(referencingFieldMeta.getForeignEntity());
				
				String dbNameReferencedEntityPKPrefix = referencedEntityMeta.isUidEntity() ? "struid" : "intid";
				String dbNameReferencingEntityPKPrefix = referencingEntityMeta.isUidEntity() ? "struid" : "intid";
				
				String dbNameReferencingField = referencingFieldMeta.getDbColumn().replace("STRVALUE", dbNameReferencedEntityPKPrefix);
				
				String dbNameReferencedEntity = referencedEntityMeta.getDbTable();
				String joinName = "aDLJoin" + (idx + 1);
				
				
				s.append("\n\tINNER JOIN(\n");
				s.append("\tSELECT referencingEntity." + dbNameReferencingEntityPKPrefix + ", referencedEntity." + dbNameReferencedEntityPKPrefix + " as " + dbNameReferencedEntityPKPrefix + "_" + dbNameReferencedEntity + "\n");
				s.append("\tFROM " + dbNameReferencingEntity + " referencingEntity, " + dbNameReferencedEntity + " referencedEntity\n");
				s.append("\tWHERE referencingEntity." + dbNameReferencingField + " = referencedEntity." + dbNameReferencedEntityPKPrefix + ") " + joinName + "\n");
				s.append("\tON " + joinName + "." + dbNameReferencingEntityPKPrefix + " = " + upperTableName + "." + upperTableRefName + "\n");
				
				if (E.DATA_LANGUAGE.getUID().equals(referencedEntityMeta.getUID())) {
					s.append("\tAND " + joinName + "." + dbNameReferencedEntityPKPrefix + "_" + dbNameReferencedEntity+ "= T2.struid_t_md_data_language\n");
				}
				
				dbNameReferencingEntity = dbNameReferencedEntity;
				upperTableName = joinName;
				upperTableRefName = dbNameReferencedEntityPKPrefix + "_" + dbNameReferencedEntity;
				
				sql+= s.toString();
			}
	
			ResultVO result = 
				dataBaseHelper.getDbAccess().executePlainQueryAsResultVO(sql, LimitedResultSetRunner.MAXFETCHSIZE, false);
			
			if (result != null && result.getRows().size() > 0) {
				Object object = result.getRows().get(0)[1];
				if (object != null) retVal = new UID(object.toString());
				
			}
			
		}
		
		return retVal;
	}

	private Column createColumn(Map<String, Table> mpTables,
	    DatasourceXMLParser.XMLColumn column) {
		final Table table = mpTables.get(column.getTable());
		if (table == null) {
			return null;
		}
		
		final Column result = new Column(table.getColumn(column.getColumn()));
		result.setTable(mpTables.get(column.getTable()));
		result.setAlias(column.getAlias());

		return result;
	}

	private boolean checkGroupBy(
	    List<DatasourceXMLParser.XMLColumn> lstColumns) {
		for(DatasourceXMLParser.XMLColumn xmlcolumn : lstColumns) {
			if((xmlcolumn != null && xmlcolumn.getGroup() != null)
			    || (xmlcolumn != null && xmlcolumn.getGroup() != null && xmlcolumn.getGroup().length() > 0)) {
				return true;
			}
		}
		return false;
	}

	private Map<String, DatasourceParameterVO> getParameterDefinitionMap(final DatasourceVO datasourceVO) throws NuclosDatasourceException {
		final Map<String, DatasourceParameterVO> result = new HashMap<>();
		final List<DatasourceParameterVO> parameters = getParameters(datasourceVO.getSource());
		for (DatasourceParameterVO parameterVO : parameters) {
			final String param = parameterVO.getParameter();
			result.put(param, parameterVO);
		}
		return result;
	}

	/**
	 * replace all parameters in sSql with the values from mpParams
	 *
	 * @param sSql
	 * @param mpParams
	 * @param parameterDefinitionMap
	 * @return sql string with parameters replaced.
	 */
	private String replaceParameters(String sSql, Map<String, ?> mpParams, final Map<String, DatasourceParameterVO> parameterDefinitionMap) throws NuclosDatasourceException {
		final StringBuilder result = new StringBuilder(sSql.length());
		final DatasourceParameterParser p = new DatasourceParameterParser(sSql);
		for (IFieldRef token: p) {
			final String content = token.getContent();
			if (token.isConstant()) {
				result.append(content);
			}
			else {
				final DatasourceParameterVO parameterVO = parameterDefinitionMap.get(content);
				result.append(getParamValueForReplacement("$" + content, parameterVO, mpParams));
			}
		}
		return result.toString();
	}

	/**
	 * replace get the value for sParameter from mapParams
	 *
	 * @param sParameter
	 * @param parameterVO
	 * @param mapParams
	 * @return the value for sParameter from the map, or sParameter if no value
	 */
	private String getParamValueForReplacement(String sParameter,
											   DatasourceParameterVO parameterVO,
											   Map<String, ?> mapParams) throws NuclosDatasourceException {
		final String sDatatype = parameterVO==null?null:parameterVO.getDatatype();
		final String sKey = sParameter.replace('$', ' ').trim();
		if (!mapParams.containsKey(sKey)) {
			return sParameter;
		}

		boolean bEscapeResult = parameterVO!=null; // ==null ? internal system parameter like mandator or locale (do not escape)
		Object param = mapParams.get(sKey);
		if (param instanceof Boolean) {
			param = dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL((Boolean)param);
			bEscapeResult = false;
		} else if (param instanceof Date) {
			// ISO-format 'yyyy-mm-dd', matches JDBC escape syntax
			param = String.format("%tF", param);
			bEscapeResult = false;
		} else if (param instanceof String) {
			if ("java.util.List<String>".equals(sDatatype)) {
				// e.g. test string from query editor
				String sParam = (String) param;
				sParam = sParam.trim();
				if (sParam.startsWith("(") || sParam.startsWith("[")) {
					sParam = sParam.substring(1);
				}
				if (sParam.endsWith(")") || sParam.endsWith("]")) {
					sParam = sParam.substring(0, sParam.length()-1);
				}
				List<String> list = new ArrayList<>();
				for (String s : sParam.split(",")) {
					if (s.startsWith("'") || s.startsWith("\"")) {
						s = s.substring(1);
					}
					if (s.endsWith("'") || s.endsWith("\"")) {
						s = s.substring(0, s.length()-1);
					}
					list.add(s);
				}
				param = list;
			}
		}

		if (param instanceof List) {
			if ("java.util.List<String>".equals(sDatatype)) {
				StringBuilder s = new StringBuilder();
				for (Object o : (List)param) {
					if (o != null) {
						s.append(s.length()==0?'(':',').append('\'');
						try {
							s.append(dataBaseHelper.getDbAccess().escapeLiteral(o.toString()));
						} catch (SQLException e) {
							throw new NuclosDatasourceException(e);
						}
						s.append('\'');
					}
				}
				s.append(s.length()==0?"(null)":")");
				param = s;
				bEscapeResult = false;
			} else if ("java.util.List<Integer>".equals(sDatatype)) {
				StringBuilder s = new StringBuilder();
				for (Object o : (List)param) {
					if (o instanceof Number) {
						s.append(s.length()==0?'(':',').append(String.valueOf(o));
					}
				}
				s.append(s.length()==0?"(null)":")");
				param = s;
				bEscapeResult = false;
			} else {
				String s = param.toString();
				param = "(" + s.substring(1, s.length() - 1) + ")";
			}
		}
		if (param != null) {
			String result = param.toString();

			if ("SQL".equals(sDatatype)) {
				if (DatasourceUtils.getTestValueForParameter(parameterVO, dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(false)).equals(result)) {
					// for datasource validation test... no whitelist check... we escape
				} else {
					final DatasourceParameterValuelistproviderVO parameterWhitelistVLP = parameterVO.getValueListProvider();
					if (parameterWhitelistVLP == null) {
						throw new NuclosDatasourceException("Valuelist provider of datasource parameter \"" + sParameter + "\" (SQL type) is null.");
					}
					final UID whitelistVLPUID = UID.parseUID(parameterWhitelistVLP.getValue());
					try {
						final ValuelistProviderVO whitelistProviderVO = getValuelistProvider(whitelistVLPUID);
						final Map<String, DatasourceParameterVO> whitelistParameterDefinitionMap = getParameterDefinitionMap(whitelistProviderVO);
						VLPQuery q = new VLPQuery(whitelistVLPUID);

						final Map<String, Object> parsedValuesMap = DatasourceUtils.parseAllParameters(parameterWhitelistVLP.getParameters(), whitelistParameterDefinitionMap,
								dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(true),
								dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL(false));
						q.setQueryParams(parsedValuesMap);
						q.setIdFieldName(parameterWhitelistVLP.getParameters().get(ValuelistProviderVO.DATASOURCE_IDFIELD));
						final String whitelistColumnName = parameterWhitelistVLP.getParameters().get(ValuelistProviderVO.DATASOURCE_NAMEFIELD);
						q.setNameField(whitelistColumnName);
						final ResultVO whitelistResultVO = executeQueryForVLP(q);
						final List<ResultColumnVO> columns = whitelistResultVO.getColumns();
						int whitelistColumnIndex = -1;
						for (int iIndex = 0; iIndex < columns.size(); iIndex++) {
							final ResultColumnVO rcvo = columns.get(iIndex);
							final String label = rcvo.getColumnLabel();
							if (label.equalsIgnoreCase(whitelistColumnName)) {
								whitelistColumnIndex = iIndex;
							}
						}
						if (whitelistColumnIndex == -1) {
							throw new NuclosDatasourceException("Column \"" + whitelistColumnName + "\" not found in whitelist result of datasource parameter \"" + sParameter + "\" (SQL type)");
						}
						boolean bValidWhitelistEntry = false;
						for (Object[] row : whitelistResultVO.getRows()) {
							if (result.equals(row[whitelistColumnIndex])) {
								bValidWhitelistEntry = true;
								break;
							}
						}
						if (!bValidWhitelistEntry) {
							throw new NuclosDatasourceException("Value \"" + result + "\" is not a valid whitelist entry of datasource parameter \"" + sParameter + "\" (SQL type)");
						}
						bEscapeResult = false;
					} catch (CommonBusinessException e) {
						throw new NuclosDatasourceException(e);
					}
				}
			}

			if (bEscapeResult) {
				try {
					result = dataBaseHelper.getDbAccess().escapeLiteral(result);
				} catch (SQLException e) {
					throw new NuclosDatasourceException(e);
				}
			}

			return result;
		}
		else {
			return "null";
		}
	}

	/**
	    * Retrieve the parameters a datasource accepts.
	    * @param sDatasourceXML
	    * @return
	    * @throws NuclosFatalException
	    * @throws NuclosDatasourceException
	    */
	   public List<DatasourceParameterVO> getParameters(String sDatasourceXML) throws NuclosFatalException, NuclosDatasourceException {
			// @todo we need a getParameters(Integer iDatasourceId) method for the
			// remote interface to prevent that every user needs the right to
			// execute the get method
			final List<DatasourceParameterVO> result = new ArrayList<DatasourceParameterVO>();
			final DatasourceXMLParser.Result parseresult = parseDatasourceXML(sDatasourceXML);
			result.addAll(parseresult.getLstParameters());
			return result;
	   }

	   /**
	    * Retrieve the parameters a datasource accepts.
	    * @param dataSourceUID
	    * @return
	    * @throws NuclosFatalException
	    * @throws NuclosDatasourceException
	    */
	   public List<DatasourceParameterVO> getParameters(final UID dataSourceUID) throws NuclosFatalException, NuclosDatasourceException {
	 		final List<DatasourceParameterVO> result = new ArrayList<DatasourceParameterVO>();
	 		final DatasourceVO dsvo = DatasourceCache.getInstance().get(dataSourceUID);
	 		final DatasourceXMLParser.Result parseresult = parseDatasourceXML(dsvo.getSource());
	 		result.addAll(parseresult.getLstParameters());
	 		return result;
	   }
	   
	   public List<DatasourceParameterVO> getCalcAttributeParameters(final UID calcAttributeUID) throws NuclosFatalException, NuclosDatasourceException {
	 		final List<DatasourceParameterVO> result = new ArrayList<DatasourceParameterVO>();
	 		final DatasourceVO dsvo = DatasourceCache.getInstance().getCalcAttribute(calcAttributeUID);
	 		final DatasourceXMLParser.Result parseresult = parseDatasourceXML(dsvo.getSource());
	 		result.addAll(parseresult.getLstParameters());
	 		return result;
	   }

	public ResultVO executeQueryForVLP(VLPQuery query) throws CommonBusinessException {

		String sql = createSQLForVLP(query);

		if (query.getBaseEntityUID() != null) {
			final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(query.getBaseEntityUID());
			if (eMeta.isMandator()) {
				final String baseTable = eMeta.getDbSelect();
				final Set<UID> mandators = mandatorUtils.getAccessibleMandators(query.getMandatorUID());
				final StringBuffer sMandators = new StringBuffer();
				for (Iterator<UID> iterator = mandators.iterator(); iterator.hasNext(); ) {
					sMandators.append('\'');
					sMandators.append(iterator.next().getString());
					sMandators.append('\'');
					if (iterator.hasNext()) {
						sMandators.append(",");
					}

				}
				sql = "SELECT * FROM (" + sql + ") tmp345 WHERE tmp345.\"" + query.getIdFieldName() + "\" IN (SELECT INTID FROM " + baseTable + " WHERE STRUID_NUCLOSMANDATOR IN (" + sMandators.toString() + "))";
			}
		}

		if (org.apache.commons.lang.StringUtils.isNotBlank(query.getNameField()) &&
				org.apache.commons.lang.StringUtils.isNotBlank(query.getQuickSearchInput())
				) {
			String quickSearchSql = null;
			try {
				quickSearchSql = dataBaseHelper.getDbAccess().escapeLiteral(query.getQuickSearchInput().toUpperCase())
						.replace('*', '%')
						.replace('?', '_');
			} catch (SQLException e) {
				throw new CommonValidationException(e);
			}
			sql = "SELECT * FROM (" + sql + ") tmp8329 WHERE UPPER(tmp8329.\"" + query.getNameField() + "\") LIKE '%" + quickSearchSql + "%'";
		}

		return executePlainQueryAsResultVO(sql, query.getMaxRowCount());
	}

	private String createSQLForVLP(VLPQuery query) throws CommonBusinessException {
		ValuelistProviderVO vlpVO = getValuelistProvider(query.getValuelistProviderUid());
		if (vlpVO == null) {
			throw new CommonFinderException("Could Not Find VLP with UID=" + query.getValuelistProviderUid());
		}

		//NUCLOS-5496
		for (DatasourceParameterVO param : getParameters(vlpVO.getSource())) {
			if (!query.getQueryParams().containsKey(param.getLabel())) {
				query.getQueryParams().put(param.getLabel(), null);
			}
		}

		return createSQL(
				vlpVO,
				query.getQueryParams(),
				query.getMandatorUID(),
				query.getLanguageUID()
		);
	}

	public ValuelistProviderVO getValuelistProvider(final UID valuelistProviderUID) throws CommonFinderException, CommonPermissionException {
		return DatasourceCache.getInstance().getValuelistProvider(valuelistProviderUID, true);
	}

	public ResultVO executePlainQueryAsResultVO(String sql, Integer iMaxRowCount) {
		if (iMaxRowCount == null) {
			iMaxRowCount = utils.isCalledRemotely() ? LimitedResultSetRunner.MAXFETCHSIZE : -1;
		}
		return dataBaseHelper.getDbAccess().executePlainQueryAsResultVO(sql, iMaxRowCount,
				ServerParameterProvider.getInstance().isEnabled(ParameterProvider.DEPRECATED_2017_ALLOWED, false));
	}

	   /**
	    * SQL Cache
	    */
	   private final class SQLCache {

		   private final Map<String, String> mpDatasourcesSQLByXML 
		   	= new ConcurrentHashMap<String, String>();

		   private String getSQL(DatasourceVO datasourcevo) throws NuclosDatasourceException {
			   if (!mpDatasourcesSQLByXML.containsKey(datasourcevo.getSource())) {
				   // alreadyProcessed. to avoid circular calls. @see SelectQuery.appendFromClause().
				   StackTraceElement[] stackTraces = new Exception().getStackTrace();
				   for (int i = 1; i < stackTraces.length; i++) {
					   StackTraceElement stackTraceElement = stackTraces[i];
						if (stackTraceElement.getClassName().equals("org.nuclos.server.common.DatasourceServerUtils$SQLCache")
								&& stackTraceElement.getMethodName().equals("getSQL")) {
							throw new StackOverflowError("alreadyProcessed " + datasourcevo.getSource());
						}
				   }
				   mpDatasourcesSQLByXML.put(datasourcevo.getSource(), createSQLForCaching(datasourcevo));
			   }

			   return mpDatasourcesSQLByXML.get(datasourcevo.getSource());
		   }

		   public void invalidate() {
			   mpDatasourcesSQLByXML.clear();
		   }
	   }

}
