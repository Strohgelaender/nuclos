package org.nuclos.server.common;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.nuclos.api.annotation.RuleType;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.rule.CommunicationRule;
import org.nuclos.api.rule.CustomRestRule;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.DeleteFinalRule;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.JobRule;
import org.nuclos.api.rule.PrintFinalRule;
import org.nuclos.api.rule.PrintRule;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.api.rule.TransactionalJobRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.businessentity.rule.annotation.SystemRuleUsage;
import org.nuclos.businessentity.utils.BusinessObjectBuilderForInternalUse;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.customcode.CustomCodeRuleScanner;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTypeVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.nbo.NuclosBusinessObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

@Component
@DependsOn("metaDataProvider")
public class EventSupportCache {

	private static final Logger LOG = LoggerFactory.getLogger(EventSupportCache.class);

	/**
	 * All external (Extension) or core (Nuclos) from jars
	 */
	private final List<EventSupportSourceVO> externalOrSystemEventSupports = new ArrayList<EventSupportSourceVO>();
	
	/**
	 * All instances from NuclosEntity.SERVERCODE that also implements
	 * event support specific interfaces.
	 */
	private List<EventSupportSourceVO> internalEventSupports;
	
	/**
	 * All instances from NuclosEntity.SERVERCODE that <em>do not</em> implement
	 * event support specific Interfaces.
	 * <p>
	 * This classes could be used as 'libraries' in other event support specific 
	 * code and hence must be detected for the nuclos IDE plugin.
	 * </p>
	 * @author Thomas Pasch
	 */
	private List<EventSupportSourceVO> internalEventLibs;	
	
	private List<EventSupportTypeVO> eventSupportTypes;

	private ServerCodeEventSupportStore entityStore;
	private DefaultEventSupportStore<EventSupportTransitionVO> transitionStore;
	private DefaultEventSupportStore<EventSupportGenerationVO> generationStore;
	private DefaultEventSupportStore<EventSupportJobVO> jobStore;
	private DefaultEventSupportStore<EventSupportCommunicationPortVO> comPortStore;
	
	private Map<UID, Pair<UID, String>> mapEntityNucletPackages;
	
	private Map<UID, String> mapEntityProxyImpl;
	private Map<UID, String> mapEntityMandatorProviderImpl;

	private Map<UID, List<UID>> mapEntityIntegrationPoints;

	private final List<String> registeredEvenTypes = Lists.newArrayList(
			CustomRule.class.getCanonicalName(),
			CustomRestRule.class.getCanonicalName(),
			DeleteFinalRule.class.getCanonicalName(), DeleteRule.class.getCanonicalName(),
			GenerateFinalRule.class.getCanonicalName(), GenerateRule.class.getCanonicalName(), 
			InsertFinalRule.class.getCanonicalName(), InsertRule.class.getCanonicalName(), 
			StateChangeFinalRule.class.getCanonicalName(), StateChangeRule.class.getCanonicalName(),
			UpdateFinalRule.class.getCanonicalName(), UpdateRule.class.getCanonicalName(),
			TransactionalJobRule.class.getCanonicalName(), JobRule.class.getCanonicalName(),
			PrintRule.class.getCanonicalName(), PrintFinalRule.class.getCanonicalName(),
			CommunicationRule.class.getCanonicalName()
			);
	
	// Spring injection
	
	@Autowired
	private CustomCodeManager ccm;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private MetaProvider provider;
	
	@Autowired
	private GenerationCache genCache;
	
	@Autowired
	private JobCache jobCache;
	
	@Autowired
	private NucletDalProvider nucletDalProvider;
	
	// end of Spring injection
	
	@PostConstruct
	final void init() {
		loadNuclets();
		entityStore = new ServerCodeEventSupportStore(nucletDalProvider);

		transitionStore = new DefaultEventSupportStore<EventSupportTransitionVO>(nucletDalProvider, E.SERVERCODETRANSITION) {
			@Override
			EventSupportTransitionVO transformEO(EntityObjectVO<UID> eoVO) {
				return MasterDataWrapper.getEventSupportTransitionVO(eoVO);
			}
			@Override
			UID getRef(EventSupportTransitionVO esVO) {
				return esVO.getTransition();
			}
		};
		generationStore = new DefaultEventSupportStore<EventSupportGenerationVO>(nucletDalProvider, E.SERVERCODEGENERATION) {
			@Override
			EventSupportGenerationVO transformEO(EntityObjectVO<UID> eoVO) {
				return MasterDataWrapper.getEventSupportGenerationVO(eoVO);
			}
			@Override
			UID getRef(EventSupportGenerationVO esVO) {
				return esVO.getGeneration();
			}
		};
		jobStore = new DefaultEventSupportStore<EventSupportJobVO>(nucletDalProvider, E.SERVERCODEJOB) {
			@Override
			EventSupportJobVO transformEO(EntityObjectVO<UID> eoVO) {
				return MasterDataWrapper.getEventSupportJobVO(eoVO);
			}
			@Override
			UID getRef(EventSupportJobVO esVO) {
				return esVO.getJobControllerUID();
			}
		};
		comPortStore = new DefaultEventSupportStore<EventSupportCommunicationPortVO>(nucletDalProvider, E.SERVERCODECOMMUNICATIONPORT) {
			@Override
			EventSupportCommunicationPortVO transformEO(EntityObjectVO<UID> eoVO) {
				return MasterDataWrapper.getEventSupportCommunicationPortVO(eoVO);
			}
			@Override
			UID getRef(EventSupportCommunicationPortVO esVO) {
				return esVO.getPort();
			}
		};

		loadExternalOrSystemEventSupportFiles();
	}
	
	private void loadNuclets() {
		mapEntityNucletPackages = new HashMap<UID, Pair<UID,String>>();
		
		Collection<EntityMeta<?>> lstAllEntities = MetaProvider.getInstance().getAllEntities();

		for (EntityMeta<?> eoVo : lstAllEntities) {
			Pair<UID, String> newPair = null;
			if (eoVo.getNuclet() != null) {
				EntityObjectVO<UID> nuclet = provider.getNuclet(eoVo.getNuclet());
				UID nuclId = (UID)nuclet.getId();
				String packageName = nuclet.getFieldValue(E.NUCLET.packagefield);
				newPair = new Pair<UID, String>(nuclId, packageName);
			}
			mapEntityNucletPackages.put(eoVo.getUID(), newPair);			
		}
	}
	
	public synchronized List<EventSupportSourceVO> getInternExecutableEventSupportFiles() {
		// Get all internal rules from database
		getInternalExecutableEventSupportFiles(); 
		return internalEventSupports;
	}
	
	public synchronized List<EventSupportSourceVO> getInternEventLibs() {
		// Get all internal rules from database
		getInternalExecutableEventSupportFiles(); 
		return internalEventLibs;
	}

	public synchronized void loadExternalOrSystemEventSupportFiles() {
		// Load all rules that are stored in external jars, e.g. Extensions,
		// or from nuclos core
		final ClassLoader classLoader = LangUtils.getClassLoaderThatWorksForWebStart();
		externalOrSystemEventSupports.addAll(new CustomCodeRuleScanner(classLoader, this.servletContext).
				getExecutableRulesFromClasspath(registeredEvenTypes).values());

		for (EventSupportSourceVO externalOrSystemEventSupport : externalOrSystemEventSupports) {
			try {
				final Class<?> systemRuleClass = classLoader.loadClass(externalOrSystemEventSupport.getClassname());
				externalOrSystemEventSupport.setSystem(entityStore.registerSystemRule(systemRuleClass));
			} catch (Exception e) {
				LOG.error("Core rule class {} not loadable: {}", externalOrSystemEventSupport.getClassname(), e.getMessage());
				throw new NuclosFatalException(e);
			}
		}
	}
	
	public synchronized List<EventSupportSourceVO> getExecutableEventSupportFiles() {
		List<EventSupportSourceVO> retVal = new ArrayList<EventSupportSourceVO>();

		// All external (Extension) or core (Nuclos) from jars
		retVal.addAll(this.externalOrSystemEventSupports);
		
		// Get all internal rules from database
		getInternalExecutableEventSupportFiles();
		
		retVal.addAll(this.internalEventSupports);

		return retVal;
	}
	
	private synchronized void getInternalExecutableEventSupportFiles() {		
		if (internalEventSupports == null || internalEventSupports.isEmpty()) {
			mapEntityProxyImpl = new HashMap<UID, String>();
			mapEntityMandatorProviderImpl = new HashMap<UID, String>();
			final List<EventSupportSourceVO> ies = new ArrayList<EventSupportSourceVO>();
			final List<EventSupportSourceVO> iel = new ArrayList<EventSupportSourceVO>();
			final List<EntityObjectVO<UID>> lstAllRules = nucletDalProvider.getEntityObjectProcessor(E.SERVERCODE).getAll();
			final Map<String, UID> proxyInterfaces = new HashMap<String, UID>();
			final Map<String, UID> mandatorProviderInterfaces = new HashMap<String, UID>();
			for (EntityMeta<?> eMeta : MetaProvider.getInstance().getAllEntities()) {
				mandatorProviderInterfaces.put(getEntityMandatorProviderInterfaceQualifiedName(eMeta), eMeta.getUID());

				if (eMeta.isProxy()||eMeta.isWriteProxy()) {				
					proxyInterfaces.put(getEntityProxyInterfaceQualifiedName(eMeta), eMeta.getUID());
				}
			}
			for (EntityObjectVO<UID> eoVO : lstAllRules) {
					String sName = extractSimplename(eoVO.getFieldValue(E.SERVERCODE.name.getUID(), String.class));
					String sDescription = eoVO.getFieldValue(E.SERVERCODE.description.getUID(), String.class);
					String sClassname = eoVO.getFieldValue(E.SERVERCODE.name.getUID(), String.class);
					String sPackage = extractPackage(eoVO.getFieldValue(E.SERVERCODE.name.getUID(), String.class));
					Date   dDateOfCompilation = eoVO.getChangedAt() != null ? eoVO.getChangedAt() : eoVO.getCreatedAt();
					UID   lNucletId = eoVO.getFieldUid(E.SERVERCODE.nuclet.getUID());
					Boolean isActive = eoVO.getFieldValue(E.SERVERCODE.active.getUID(), Boolean.class);
					
					Mutable<UID> proxyEntity = new Mutable<UID>();
					Mutable<UID> mandatorProviderEntity = new Mutable<UID>();
					
					List<String> lstInterfaces = getInterfacesForEventSupportSource(
							eoVO.getFieldValue(E.SERVERCODE.source.getUID(), String.class), proxyInterfaces, proxyEntity, mandatorProviderInterfaces, mandatorProviderEntity);
					if (proxyEntity.getValue() != null) {
						if (mapEntityProxyImpl.containsKey(proxyEntity.getValue())) {
							LOG.error("More than one implementation found for proxy bo {}",
							          proxyEntity.getValue());
						}
						mapEntityProxyImpl.put(proxyEntity.getValue(), sClassname);
					}
					
					if (mandatorProviderEntity.getValue() != null) {
						if (mapEntityMandatorProviderImpl.containsKey(mandatorProviderEntity.getValue())) {
							LOG.error("More than one implementation found for mandator provider of bo {}",
							          mandatorProviderEntity.getValue());
						}
						mapEntityMandatorProviderImpl.put(mandatorProviderEntity.getValue(), sClassname);
					}
					
					
					final EventSupportSourceVO ess = new EventSupportSourceVO(
							new NuclosValueObject<UID>(eoVO.getPrimaryKey(), eoVO.getCreatedAt(), eoVO.getCreatedBy(), eoVO.getChangedAt(), eoVO.getChangedBy(), eoVO.getVersion()), 
							sName, sDescription, sClassname, lstInterfaces, sPackage, dDateOfCompilation, lNucletId, isActive);
					if (lstInterfaces.size() > 0) {
						ies.add(ess);
					} else {
						iel.add(ess);
					}
			}
			internalEventSupports = ies;
			internalEventLibs = iel;
		}
	}

	private String extractPackage(String field) {
		String retVal = null;
		
		if (field != null && field.lastIndexOf(".") > 0) {
			retVal = field.substring(0, field.lastIndexOf("."));
		}
		
		return retVal;
	}

	private String extractSimplename(String field) {
		String retVal = null;
		
		if (field != null && field.lastIndexOf(".") > 0) {
			retVal = field.substring(field.lastIndexOf(".")+1);
		}
		
		return retVal;
	}
	
	public synchronized List<EventSupportTypeVO> getEventSupportTypes() {
		
		if (this.eventSupportTypes == null) {
			
			this.eventSupportTypes = new ArrayList<EventSupportTypeVO>();
			
			for(String eventType : this.getRegisteredSupportEventTypes()) {
				try {
					Class<?> evenTypeClass = Class.forName(eventType);
					List<String> lstOfImports = new ArrayList<String>();
					String sName = evenTypeClass.getSimpleName();
					String sBeschreibung = evenTypeClass.getSimpleName();
					lstOfImports.add(evenTypeClass.getName());
					
					Annotation[] annotations = evenTypeClass.getAnnotations();
					if (annotations.length > 0
							&& annotations[0].annotationType().equals(
									RuleType.class)) {
						sBeschreibung = evenTypeClass.getAnnotation(RuleType.class)
								.description();
						sName = evenTypeClass.getAnnotation(RuleType.class).name();
					}
					List<String> lstMethods = new ArrayList<String>();
					for (Method m: evenTypeClass.getMethods()) {
						String sParameter = "(";
						if (m.getParameterTypes().length > 0) {
							for (int idx=0; idx < m.getParameterTypes().length; idx++) {
								Class<?> cls = m.getParameterTypes()[idx];
								lstOfImports.add(cls.getName());
								sParameter += cls.getSimpleName() + " context" + (idx==0?"":idx);
								if (idx < m.getParameterTypes().length - 1)
									sParameter += ",";
							}
						}
						sParameter += ")";
						lstMethods.add("public " + m.getReturnType().getSimpleName() + " " + m.getName() + sParameter);
					}
					
					String pkgName = evenTypeClass.getPackage() != null ? evenTypeClass.getPackage().getName() : null;
					
					this.eventSupportTypes.add(new EventSupportTypeVO(sName, sBeschreibung,
							evenTypeClass.getName(), lstMethods, lstOfImports, pkgName, null));
					
				} catch (Exception e) {
					LOG.error("Cannot read class information for {}", eventType);
				}
			}
		}
		
		return this.eventSupportTypes;
	}
	
	public List<String> getRegisteredSupportEventTypes() {
		return this.registeredEvenTypes;
	}
	
	public EventSupportSourceVO getEventSupportSourceByName(String name) throws IllegalArgumentException{
		EventSupportSourceVO retVal =null;
		
		if (name == null)
			throw new IllegalArgumentException("name of rule must not be null");
		
		for (EventSupportSourceVO source : getExecutableEventSupportFiles()) {
			if (name.equals(source.getName()) || name.equals(source.getClassname())) {
				retVal= source;
				break;
			}
		}			
		
		return retVal;
	}
	
	public EventSupportSourceVO getEventSupportSourceByClassname(String classname) throws IllegalArgumentException{
		EventSupportSourceVO retVal =null;
		
		if (classname == null)
			throw new IllegalArgumentException("classname of rule must not be null");

		for (EventSupportSourceVO source : getExecutableEventSupportFiles()) {
			if (source.getClassname().equals(classname)) {
				retVal= source;
				break;
			}
		}			
		
		
		return retVal;
	}

	public List<UID> getIntegrationPointsByEntity(UID entityUID) {
		Map<UID, List<UID>> mapEntityIntegrationPoints = this.mapEntityIntegrationPoints;
		if (mapEntityIntegrationPoints == null) {
			mapEntityIntegrationPoints = new HashMap<>();
			final List<EntityObjectVO<UID>> allIntegrationPoints = nucletDalProvider.getEntityObjectProcessor(E.NUCLET_INTEGRATION_POINT).getAll();
			Collections.sort(allIntegrationPoints, new Comparator<EntityObjectVO<UID>>() {
				@Override
				public int compare(final EntityObjectVO<UID> eo1, final EntityObjectVO<UID> eo2) {
					final String package1 = MetaProvider.getInstance().getNuclet(eo1.getFieldUid(E.NUCLET_INTEGRATION_POINT.nuclet)).getFieldValue(E.NUCLET.packagefield);
					final String package2 = MetaProvider.getInstance().getNuclet(eo2.getFieldUid(E.NUCLET_INTEGRATION_POINT.nuclet)).getFieldValue(E.NUCLET.packagefield);
					return StringUtils.compare(package1 + eo1.getFieldValue(E.NUCLET_INTEGRATION_POINT.name),
							package2 + eo2.getFieldValue(E.NUCLET_INTEGRATION_POINT.name));
				}
			});
			for (EntityObjectVO<UID> eoIp : allIntegrationPoints) {
				final UID targetEntityUID = eoIp.getFieldUid(E.NUCLET_INTEGRATION_POINT.targetEntity);
				if (targetEntityUID != null) {
					List<UID> integrationPointLst = mapEntityIntegrationPoints.get(targetEntityUID);
					if (integrationPointLst == null) {
						integrationPointLst = new ArrayList<>();
						mapEntityIntegrationPoints.put(targetEntityUID, integrationPointLst);
					}
					integrationPointLst.add(eoIp.getPrimaryKey());
				}
			}
			this.mapEntityIntegrationPoints = Collections.unmodifiableMap(mapEntityIntegrationPoints);
		}

		List<UID> result = mapEntityIntegrationPoints.get(entityUID);
		return result;
	}
	
	public List<EventSupportJobVO> getEventSupportJobs() {
		return new ArrayList<EventSupportJobVO>(jobStore.getByIdMap().values());
	}
	public List<EventSupportGenerationVO> getEventSupportGenerations() {
		return new ArrayList<EventSupportGenerationVO>(generationStore.getByIdMap().values());
	}
	public List<EventSupportEventVO> getEventSupportEntities() {
		return new ArrayList<EventSupportEventVO>(entityStore.getByIdMap().values());
	}
	public List<EventSupportTransitionVO> getEventSupportTransitions() {
		return new ArrayList<EventSupportTransitionVO>(transitionStore.getByIdMap().values());
	}
	public List<EventSupportCommunicationPortVO> getEventSupportCommunicationPorts() {
		return new ArrayList<EventSupportCommunicationPortVO>(comPortStore.getByIdMap().values());
	}
	
	private List<String> getInterfacesForEventSupportSource(String source, 
			Map<String, UID> proxyInterfaces, Mutable<UID> proxyEntity, 
			Map<String, UID> mandatorProviderInterfaces, Mutable<UID> mandatorProviderEntity) {
		List<String> retVal = new ArrayList<String>();
		
		if (source.indexOf("implements") > 0) {
			String startString = source.substring(source.indexOf("implements") + 10).trim();
			String substring = startString.substring(0, startString.indexOf("{")).trim();
			
			String[] split = substring.split(",");
			for (String elm : split) {
				if (proxyInterfaces.containsKey(elm.trim())) {
					retVal.add(elm.trim());
					proxyEntity.setValue(proxyInterfaces.get(elm.trim()));
				}
				if (mandatorProviderInterfaces.containsKey(elm.trim())) {
					retVal.add(elm.trim());
					mandatorProviderEntity.setValue(mandatorProviderInterfaces.get(elm.trim()));
				}
				Class<?> clzzForName = null;
				try {
					String sInterfaceName = elm.trim();
					if (sInterfaceName.indexOf('<') > 0) {
						sInterfaceName = sInterfaceName.substring(0, sInterfaceName.indexOf('<'));
					}
					clzzForName = Class.forName(elm.trim().indexOf(".") < 0 ? "org.nuclos.api.rule." + sInterfaceName : sInterfaceName);
					if (this.registeredEvenTypes.contains(clzzForName.getCanonicalName())) {
						retVal.add(clzzForName.getCanonicalName());						
					}
				} catch (Exception e) {} 
			}
		}
		
		return retVal;
	}
	
	public List<EventSupportGenerationVO> getEventSupportsByGenerationUID(UID genUID) {
		List<EventSupportGenerationVO> retVal = new ArrayList<EventSupportGenerationVO>();
		List<EventSupportGenerationVO> lstStore = generationStore.getByRefIdMap().get(genUID);
		if (lstStore != null && !lstStore.isEmpty()) {
			retVal.addAll(lstStore);
		}
		return retVal;
	}
	
	public List<EventSupportTransitionVO> getEventSupportsByTransitionUID(UID transUID) {
		List<EventSupportTransitionVO> retVal = new ArrayList<EventSupportTransitionVO>();
		List<EventSupportTransitionVO> lstStore = transitionStore.getByRefIdMap().get(transUID);
		if (lstStore != null && !lstStore.isEmpty()) {
			retVal.addAll(lstStore);
		}
		return retVal;
	}
	
	public List<EventSupportEventVO> getEventSupportsByEntityUID(UID entityUID, boolean bIncludeIntegrationPointsInResult) {
		List<EventSupportEventVO> retVal = new ArrayList<EventSupportEventVO>();
		List<EventSupportEventVO> lstStore = entityStore.getByRefIdMap().get(entityUID);
		if (lstStore != null && !lstStore.isEmpty()) {
			retVal.addAll(lstStore);
		}
		if (bIncludeIntegrationPointsInResult) {
			final List<UID> integrationPointLst = getIntegrationPointsByEntity(entityUID);
			if (integrationPointLst != null) {
				for (UID integrationPointUID : integrationPointLst) {
					final List<EventSupportEventVO> lstIpStore = entityStore.getByRefIdMap().get(integrationPointUID);
					if (lstIpStore != null && !lstIpStore.isEmpty()) {
						retVal.addAll(lstIpStore);
					}
				}
			}
		}
		return retVal;
	}

	public List<EventSupportJobVO> getEventSupportsByJobUID(UID jobUID) {
		List<EventSupportJobVO> retVal = new ArrayList<EventSupportJobVO>();
		List<EventSupportJobVO> lstStore = jobStore.getByRefIdMap().get(jobUID);
		if (lstStore != null && !lstStore.isEmpty()) {
			retVal.addAll(lstStore);
		}
		return retVal;
	}
	
	public List<EventSupportCommunicationPortVO> getEventSupportsByCommunicationPortUID(UID portUID) {
		List<EventSupportCommunicationPortVO> retVal = new ArrayList<EventSupportCommunicationPortVO>();
		List<EventSupportCommunicationPortVO> lstStore = comPortStore.getByRefIdMap().get(portUID);
		if (lstStore != null && !lstStore.isEmpty()) {
			retVal.addAll(lstStore);
		}
		return retVal;
	}
	
	public void removeEventSupportCache(EventSupportSourceVO eseVO) {
		if (this.internalEventSupports.contains(eseVO)) {
			this.internalEventSupports.remove(eseVO);
		}
	}
	public void removeEventSupportJobCache(UID jobId, UID eseId) {
		jobStore.removeFromCache(jobId, eseId);
	}
	
	public void removeEventSupportEntityCache(UID entityId, UID eventIdToRemove) {
		entityStore.removeFromCache(entityId, eventIdToRemove);
	}
	
	public void removeEventSupportGenerationCache(UID genId, UID eseId) {
		generationStore.removeFromCache(genId, eseId);
	}
	
	public void removeEventSupportCommunicationPortCache(UID portId, UID eseId) {
		comPortStore.removeFromCache(portId, eseId);
	}
	
	public void removeEventSupportTransitionCache(UID transId, UID eseId) {
		transitionStore.removeFromCache(transId, eseId);
		
		StateCache.getInstance().invalidateCache(true, true);
	}
	
	public void insertEventSupportTransitionCache(EventSupportTransitionVO eseTVO) {
		transitionStore.insertIntoCache(eseTVO);
	}
	
	public void insertEventSupportEntityCache(EventSupportEventVO eseTVO) {
		entityStore.insertIntoCache(eseTVO);
	}
	
	public void insertEventSupportGenerationCache(EventSupportGenerationVO eseTVO) {
		generationStore.insertIntoCache(eseTVO);
	}
	
	public void insertEventSupportCommunicationPortCache(EventSupportCommunicationPortVO eseTVO) {
		comPortStore.insertIntoCache(eseTVO);
	}
	
	public void insertEventSupportJobCache(EventSupportJobVO eseTVO) {
		jobStore.insertIntoCache(eseTVO);
	}
	
	public String getNucletPackageForEntity(UID entity) {
		String retVal = null;
		
		if (mapEntityNucletPackages == null || mapEntityNucletPackages.isEmpty())
			loadNuclets();
			
		if (this.mapEntityNucletPackages.containsKey(entity)) {
			
			if (this.mapEntityNucletPackages.get(entity) != null && 
					this.mapEntityNucletPackages.get(entity).getY() != null) {
				retVal = this.mapEntityNucletPackages.get(entity).getY();
			}
		}
		
		return retVal;
	}
	
	public synchronized String getEntityMandatorProviderImpl(UID entityUID) {
		getInternalExecutableEventSupportFiles();
		return mapEntityMandatorProviderImpl.get(entityUID);
	}

	public String getEntityMandatorProviderInterfaceQualifiedName(EntityMeta<?> emdVO) {
		final String sPackage = NuclosBusinessObjectBuilder.getNucletPackageStatic(emdVO, provider);
		final String sName = NuclosBusinessObjectBuilder.getMandatorProviderInterfaceName(emdVO);
		return sPackage + "." + sName;
	}
	
	public String getEntityMandatorProviderInterfaceQualifiedName(String entityName, UID entityUID, UID nucletUID) {
		final String sPackage = NuclosBusinessObjectBuilder.getNucletPackageStatic(entityUID, nucletUID, provider);
		final String sName = NuclosBusinessObjectBuilder.getMandatorProviderInterfaceName(entityName);
		return sPackage + "." + sName;
	}
	
	public synchronized String getEntityProxyImpl(UID entityUID) {
		getInternalExecutableEventSupportFiles();
		return mapEntityProxyImpl.get(entityUID);
	}
	
	public String getEntityProxyInterfaceQualifiedName(EntityMeta<?> emdVO) {
		final String sPackage = NuclosBusinessObjectBuilder.getNucletPackageStatic(emdVO, provider);
		final boolean bForInternalUseOnly = E.isNuclosEntity(emdVO.getUID()) && BusinessObjectBuilderForInternalUse.getEntityMetas().contains(emdVO);
		return NuclosEntityValidator.getEntityProxyInterfaceQualifiedName(sPackage, emdVO, bForInternalUseOnly);
	}
	
	public String getEntityProxyInterfaceQualifiedName(UID entityUID) {
		return getEntityProxyInterfaceQualifiedName(provider.getEntity(entityUID));
	}

	public String getEntityProxyInterfaceQualifiedName(String entity, UID nucletUID) {
		final String sPackage = NuclosBusinessObjectBuilder.getNucletPackageStatic(null, nucletUID, provider);
		return NuclosEntityValidator.getEntityProxyInterfaceQualifiedName(sPackage, entity, false);
	}
	
	public void invalidate() {
		invalidate(null);
	}
	
	public void invalidate(EntityMeta<UID> entity) {
		// Just reload internal rules
		this.internalEventSupports = null;
		this.mapEntityIntegrationPoints = null;
		
		if (entity == null || E.JOBCONTROLLER.checkEntityUID(entity.getUID())) {
			jobStore.invalidate();
			jobCache.invalidate();
		}
		if (entity == null || E.GENERATION.checkEntityUID(entity.getUID())) {
			generationStore.invalidate();
			genCache.invalidate();
		}
		if (entity == null || E.STATEMODEL.checkEntityUID(entity.getUID())) {
			transitionStore.invalidate();
			StateCache.getInstance().invalidateCache(true, true);
		}
		if (entity == null || E.ENTITY.checkEntityUID(entity.getUID())) {
			entityStore.invalidate();
			this.mapEntityProxyImpl = null;
		}
		if (entity == null || E.COMMUNICATION_PORT.checkEntityUID(entity.getUID())) {
			comPortStore.invalidate();
		}
		if (entity == null || E.NUCLET.checkEntityUID(entity.getUID())) {
			this.mapEntityNucletPackages = null;
			provider.invalidateNucletCache();
		}
	}

	private static final class ServerCodeEventSupportStore extends DefaultEventSupportStore<EventSupportEventVO> {

		private Map<UID, EventSupportEventVO> mapById;
		private Map<UID, List<EventSupportEventVO>> mapByRefId;

		private final Map<UID, EventSupportEventVO> systemRulesByIdMap = CollectionUtils.newHashMap();
		private final Map<UID, List<EventSupportEventVO>> systemRulesByEntityIdMap = CollectionUtils.newHashMap();

		public ServerCodeEventSupportStore(final NucletDalProvider nucletDalProvider) {
			super(nucletDalProvider, E.SERVERCODEENTITY);
		}

		@Override
		EventSupportEventVO transformEO(EntityObjectVO<UID> eoVO) {
			return MasterDataWrapper.getEventSupportEventVO(eoVO);
		}

		@Override
		UID getRef(EventSupportEventVO esVO) {
			return RigidUtils.defaultIfNull(esVO.getIntegrationPointUID(), esVO.getEntityUID());
		}

		public synchronized boolean registerSystemRule(Class systemRuleClass) {
			LOG.info("register core rule usages: {}", systemRuleClass.getCanonicalName());
			if (systemRuleClass.isAnnotationPresent(SystemRuleUsage.class)) {
				final SystemRuleUsage systemRuleUsage = (SystemRuleUsage) systemRuleClass.getAnnotation(SystemRuleUsage.class);
				if (InsertRule.class.isAssignableFrom(systemRuleClass)) {
					addNewSystemEventSupportVO(systemRulesByIdMap, systemRulesByEntityIdMap, systemRuleClass, systemRuleUsage, InsertRule.class);
				}
				if (InsertFinalRule.class.isAssignableFrom(systemRuleClass)) {
					addNewSystemEventSupportVO(systemRulesByIdMap, systemRulesByEntityIdMap, systemRuleClass, systemRuleUsage, InsertFinalRule.class);
				}
				if (UpdateRule.class.isAssignableFrom(systemRuleClass)) {
					addNewSystemEventSupportVO(systemRulesByIdMap, systemRulesByEntityIdMap, systemRuleClass, systemRuleUsage, UpdateRule.class);
				}
				if (UpdateFinalRule.class.isAssignableFrom(systemRuleClass)) {
					addNewSystemEventSupportVO(systemRulesByIdMap, systemRulesByEntityIdMap, systemRuleClass, systemRuleUsage, UpdateFinalRule.class);
				}
				if (DeleteRule.class.isAssignableFrom(systemRuleClass)) {
					addNewSystemEventSupportVO(systemRulesByIdMap, systemRulesByEntityIdMap, systemRuleClass, systemRuleUsage, DeleteRule.class);
				}
				if (DeleteFinalRule.class.isAssignableFrom(systemRuleClass)) {
					addNewSystemEventSupportVO(systemRulesByIdMap, systemRulesByEntityIdMap, systemRuleClass, systemRuleUsage, DeleteFinalRule.class);
				}
				if (CustomRule.class.isAssignableFrom(systemRuleClass)) {
					addNewSystemEventSupportVO(systemRulesByIdMap, systemRulesByEntityIdMap, systemRuleClass, systemRuleUsage, CustomRule.class);
				}
				return true;
			}
			return false;
		}

		private static void addNewSystemEventSupportVO(Map<UID, EventSupportEventVO> systemRulesByIdMap,
													   Map<UID, List<EventSupportEventVO>> systemRulesByEntityIdMap,
													   Class systemRuleClass,
													   SystemRuleUsage systemRuleUsage,
													   Class eventSupportTypeClass) {
			final EventSupportEventVO eventSupportEventVO = newSystemEventSupportVO(systemRuleClass, systemRuleUsage, eventSupportTypeClass);
			final UID entityUID = eventSupportEventVO.getEntityUID();
			systemRulesByIdMap.put(new UID(), eventSupportEventVO);
			if (!systemRulesByEntityIdMap.containsKey(entityUID)) {
				systemRulesByEntityIdMap.put(entityUID, new ArrayList<EventSupportEventVO>());
			}
			systemRulesByEntityIdMap.get(entityUID).add(eventSupportEventVO);
		}

		private static EventSupportEventVO newSystemEventSupportVO(Class systemRuleClass, SystemRuleUsage systemRuleUsage, Class eventSupportTypeClass) {
			UID entityUID = null;
			try {
				final BusinessObject<?> bo = systemRuleUsage.boClass().newInstance();
				entityUID = (UID) bo.getEntityUid();
			} catch (Exception e) {
				throw new NuclosFatalException("Business object of type " + systemRuleUsage.boClass().getCanonicalName() + " could not be instantiated: " + e.getMessage(), e);
			}
			return new EventSupportEventVO(systemRuleClass.getCanonicalName(), eventSupportTypeClass.getCanonicalName(), entityUID, null,
					null, null, null, null, systemRuleUsage.order());
		}

		@Override
		public void invalidate() {
			super.invalidate();
			this.invalidateLocalOnly();
		}

		private void invalidateLocalOnly() {
			synchronized (this) {
				this.mapById = null;
				this.mapByRefId = null;
			}
		}

		@Override
		public void insertIntoCache(final EventSupportEventVO esVO) {
			super.insertIntoCache(esVO);
			this.invalidateLocalOnly();
		}

		@Override
		public void removeFromCache(final UID refId, final UID esId) {
			super.removeFromCache(refId, esId);
			this.invalidateLocalOnly();
		}

		@Override
		public Map<UID, EventSupportEventVO> getByIdMap() {
			Map<UID, EventSupportEventVO> result = this.mapById;
			if (result == null) {
				synchronized (this) {
					result = CollectionUtils.newHashMap();
					result.putAll(super.getByIdMap());
					result.putAll(systemRulesByIdMap);
					this.mapById = result;
				}
			}
			return result;
		}

		@Override
		public Map<UID, List<EventSupportEventVO>> getByRefIdMap() {
			Map<UID, List<EventSupportEventVO>> result = this.mapByRefId;
			if (result == null) {
				synchronized (this) {
					result = CollectionUtils.newHashMap();
					this.addAllWithNewList(result, super.getByRefIdMap());
					this.addAllWithNewList(result, systemRulesByEntityIdMap);
					for (UID refId : result.keySet()) {
						order(result.get(refId));
					}
					this.mapByRefId = result;
				}
			}
			return result;
		}

		private void addAllWithNewList(Map<UID, List<EventSupportEventVO>> getByRefIdMap, Map<UID, List<EventSupportEventVO>> toAdd) {
			if (toAdd == null) {
				return;
			}
			for (Map.Entry<UID, List<EventSupportEventVO>> entry : toAdd.entrySet()) {
				this.addWithNewList(getByRefIdMap, entry);
			}
		}

		private void addWithNewList(Map<UID, List<EventSupportEventVO>> getByRefIdMap, Map.Entry<UID, List<EventSupportEventVO>> newEntry) {
			List<EventSupportEventVO> eventSupportEventVOS = getByRefIdMap.get(newEntry.getKey());
			if (eventSupportEventVOS == null) {
				eventSupportEventVOS = new ArrayList<>();
				getByRefIdMap.put(newEntry.getKey(), eventSupportEventVOS);
			}
			eventSupportEventVOS.addAll(newEntry.getValue());
		}

	};
	
	private static abstract class DefaultEventSupportStore<T extends EventSupportVO> {
		private final NucletDalProvider nucletDalProvider;
		private final EntityMeta<UID> entityMeta;
		
		private Map<UID, T> mapById;
		private Map<UID, List<T>> mapByRefId;
		
		public DefaultEventSupportStore(NucletDalProvider nucletDalProvider, EntityMeta<UID> entityMeta) {
			super();
			this.nucletDalProvider = nucletDalProvider;
			this.entityMeta = entityMeta;
		}

		private void fillMaps() {
			final Map<UID, T> mapById = CollectionUtils.newHashMap(); 
			final Map<UID, List<T>> mapByRefId = CollectionUtils.newHashMap();
			final List<EntityObjectVO<UID>> lst = nucletDalProvider.getEntityObjectProcessor(entityMeta).getAll();
			for (EntityObjectVO<UID> eoVO : lst) {
				T esVO = transformEO(eoVO);
				UID refId = getRef(esVO);
				if (refId == null) {
					// is dirty, ignore it
					continue;
				}
				mapById.put(eoVO.getPrimaryKey(), esVO);
				if (!mapByRefId.containsKey(refId)) {
					mapByRefId.put(refId, new ArrayList<T>());
				}
				mapByRefId.get(refId).add(esVO);
			}
			for (UID refId : mapByRefId.keySet()) {
				order(mapByRefId.get(refId));
			}
			this.mapById = mapById;
			this.mapByRefId = mapByRefId;
		}
		
		public Map<UID, T> getByIdMap() {
			Map<UID, T> result = this.mapById; 
			if (result == null) {
				synchronized (this) {
					// check synchronized again
					result = this.mapById;
					if (result == null) {
						this.fillMaps();
						result = this.mapById;
					}
				}
			}
			if (result == null) {
				throw new NuclosFatalException("Rule cache must not be null (NUCLOS-6190)");
			}
			return result;
		}
		
		public Map<UID, List<T>> getByRefIdMap() {
			Map<UID, List<T>> result = this.mapByRefId;
			if (result == null) {
				synchronized (this) {
					// check synchronized again
					result = this.mapByRefId;
					if (result == null) {
						this.fillMaps();
						result = this.mapByRefId;
					}
				}
			}
			if (result == null) {
				throw new NuclosFatalException("Rule cache must not be null (NUCLOS-6190)");
			}
			return result;
		}
		
		public void removeFromCache(UID refId, UID esId) {
			synchronized (this) {
				Map<UID, T> mapById = new HashMap<UID, T>(getByIdMap());
				for (UID key : mapById.keySet()) {
					T esVO = mapById.get(key);
					UID esVOrefId = getRef(esVO);
					if (LangUtils.equal(esVO.getId(), esId) && LangUtils.equal(esVOrefId, refId)) {
						mapById.remove(key);
						break;
					}
				}
				this.mapById = mapById;
				if (refId != null) {
					Map<UID, List<T>> mapByRefId = new HashMap<UID, List<T>>(getByRefIdMap());
					if (mapByRefId.containsKey(refId)) {
						List<T> lstES = mapByRefId.get(refId);
						for (T esVO : lstES) {
							UID esVOrefId = getRef(esVO);
							if (LangUtils.equal(esVO.getId(), esId) && LangUtils.equal(esVOrefId, refId)) {
								lstES.remove(esVO);
								if (lstES.size() == 0) {
									mapByRefId.remove(refId);
								}
								break;
							}
						};
					}
					this.mapByRefId = mapByRefId;
				}
			}
		}
		
		public void insertIntoCache(T esVO) {
			synchronized (this) {
				UID refId = getRef(esVO);
				if (refId != null) {
					Map<UID, T> mapById = new HashMap<UID, T>(getByIdMap());
					mapById.put(esVO.getId(), esVO);
					this.mapById = mapById;
					Map<UID, List<T>> mapByRefId = new HashMap<UID, List<T>>(getByRefIdMap());
					if (!mapByRefId.containsKey(refId)) {
						mapByRefId.put(refId, new ArrayList<T>());
					}
					mapByRefId.get(refId).add(esVO);
					order(mapByRefId.get(refId));
					this.mapByRefId = mapByRefId;
				}
			}
		}
		
		public List<T> order(List<T> lst) {
			Collections.sort(lst,
					new Comparator<T>() {
						@Override
						public int compare(T o1, T o2) {
							int result = 0;
							if (o1.getOrder() != null && o2.getOrder() != null) {
								result = o1.getOrder().compareTo(o2.getOrder());
							}
							if (result == 0 && o1.getEventSupportClass() != null && o2.getEventSupportClass() != null) {
								result = o1.getEventSupportClass().compareTo(o2.getEventSupportClass());
							}
							return result;
						}
					});
			return lst;
		}
		
		public void invalidate() {
			synchronized (this) {
				this.mapById = null;
				this.mapByRefId = null;
			}
		}
		
		abstract T transformEO(EntityObjectVO<UID> eoVO);
		abstract UID getRef(T esVO);
	}
}
