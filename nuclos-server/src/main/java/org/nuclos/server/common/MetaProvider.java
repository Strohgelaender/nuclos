//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.PostConstruct;

import org.nuclos.common.CommonMetaDataServerProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityLafParameterVO;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaUtils;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterHashMap;
import org.nuclos.common.LafParameterMap;
import org.nuclos.common.MetaTablesContentFromDb;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.util.DalTransformations;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.metadata.NotifyObject;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.transport.GzipMap;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.report.SchemaCache;
import org.nuclos.server.resource.ResourceCache;
import org.nuclos.server.statemodel.valueobject.StateModelUsagesCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

/**
 * An caching singleton for accessing the meta data information
 * on the server side.
 */
@Component("metaDataProvider")
@Qualifier("metaDataProvider")
public class MetaProvider implements IMetaProvider, CommonMetaDataServerProvider, ClusterCache, INucletCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(MetaProvider.class);
	
	private static final long TIMEOUT_SECONDS = 100L;

	private static MetaProvider INSTANCE;
	
	//

	DataCache dataCache;
	
	// Spring injection
	
	@Autowired
	private NucletDalProvider nucletDalProvider;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private XStreamSupport xstreamSupport;
	
	// end of Spring injection
	
	MetaProvider(){
		INSTANCE = this;
	}
	
	@PostConstruct
	void init() {
		loadNuclets();
		dataCache = new DataCache();
		dataCache.buildMaps(false);
	}
	
	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static MetaProvider getInstance() {
		return INSTANCE;
	}

	@Override
    public Collection<EntityMeta<?>> getAllEntities() {
		return new ArrayList<>(dataCache.getMapEntityMetaData().values());
	}
	
	@Override
    public Collection<EntityMeta<?>> getAllLanguageEntities() {
		Collection<EntityMeta<?>> result = new ArrayList<EntityMeta<?>>();
		Map<UID, EntityMeta<?>> mapEntityMetaData = dataCache.getMapEntityMetaData();
		for (UID entityUID : mapEntityMetaData.keySet()) {
			if (NucletEntityMeta.isEntityLanguageUID(entityUID)) {
				result.add(mapEntityMetaData.get(entityUID));
			}
		}
		return result;
	}
	
	public Set<UID> getAllEntityUids() {
		Set<UID> allEntities = new HashSet<UID>();
		for (EntityMeta<?> eMeta : getAllEntities()) {
			allEntities.add(eMeta.getUID());
		}
		return allEntities;
	}

	@Override
	public boolean checkEntity(UID entityUID) {
		EntityMeta<?> result = dataCache.getMapEntityMetaData().get(entityUID);
		return result != null;
	}

	@Override
    public <PK> EntityMeta<PK> getEntity(UID entityUID) {
		EntityMeta<PK> result = (EntityMeta<PK>) dataCache.getMapEntityMetaData().get(entityUID);
		if (result == null) {
			throw new CommonFatalException("Entity with UID " + entityUID + " does not exist.");
		}
		return result;
	}
	
	@Override
	public EntityMeta<?> getByTablename(String sTableName) {
		assert sTableName != null;

		// Search for a normal entity
		for(EntityMeta<?> meta : getAllEntities()) {
			if(meta.getDbTable().equalsIgnoreCase(sTableName))
				return meta;
		}

		// Search for a virtual entity
		for(EntityMeta<?> meta : getAllEntities()) {
			if(meta.getVirtualEntity() != null && meta.getVirtualEntity().equalsIgnoreCase(sTableName))
				return meta;
		}

		throw new CommonFatalException("Table for " + sTableName + " not found!");		
	}
	
	@Override
    public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entityUID) {
		final EntityMeta<?> entityMeta = dataCache.getMapEntityMetaData().get(entityUID);
		if (entityMeta == null) {
			return Collections.emptyMap();
		}
		final Collection<FieldMeta<?>> fields = entityMeta.getFields();
		return DalTransformations.transformFieldMap(fields);
	}

	public Collection<UID> getAllFieldsByEntityFilteredByGroups(UID entityUID, Collection<UID> groups, boolean bHaving, boolean bWithSystemFields) {
		final EntityMeta<?> entityMeta = dataCache.getMapEntityMetaData().get(entityUID);
		if (entityMeta == null) {
			return Collections.emptySet();
		}
		Collection<UID> ret = new HashSet<>();
		for (FieldMeta<?> fm : entityMeta.getFields()) {
			if (SF.isEOField(fm.getFieldName())) {
				if (bWithSystemFields) {
					ret.add(fm.getUID());
				}
				continue;
			}
			if (groups.contains(fm.getFieldGroup())) {
				if (bHaving) {
					ret.add(fm.getUID());
				}
			} else if (!bHaving) {
				ret.add(fm.getUID());
			}
		}
		return ret;
	}

	public Map<UID, EntityMeta<?>> getMapEntityMetaData() {
		return this.dataCache.getMapEntityMetaData();
	}
	
	@Override
	public List<String> getPossibleIdFactories() {
		return new ArrayList<String>(dataBaseHelper.getDbAccess().getCallableNames());
	}

	private static boolean isBlank(Object o) {
		if (o == null) return true;
		if (o instanceof String) {
			return "".equals(o);
		}
		return false;
	}

	public Map<UID, Map<UID, FieldMeta<?>>> getAllEntityFieldsByEntitiesGz(List<UID> entities) {
		// We can simply iterate most inefficiently over the single get results,
		// as these depend on caches themselves. All in all, the only thing
		// happening here is an in-memory cache transformation
	    GzipMap<UID, Map<UID, FieldMeta<?>>> res = new GzipMap<UID, Map<UID,FieldMeta<?>>>();
	    for (UID entityUID : entities) {
	    	res.put(entityUID, getAllEntityFieldsByEntity(entityUID));	    	
	    }
	    return res;
    }

    public boolean checkEntityField(UID fieldUID) {
		final FieldMeta<?> fieldMeta = getEntityField(fieldUID, true);
		return fieldMeta != null;
	}

	@Override
	public FieldMeta<?> getEntityField(UID fieldUID) {
		return getEntityField(fieldUID, false);
	}

	public FieldMeta<?> getEntityField(UID fieldUID, boolean checkOnly) {
		if (fieldUID == null || dataCache == null) {
			return null;
		}
		FieldMeta<?> fieldMeta = dataCache.getMapFieldMetaData().get(fieldUID);
		if (fieldMeta != null) {
			return fieldMeta;
		} else {
			if (CalcAttributeUtils.isCalcAttributeCustomization(fieldUID)) {
				LocaleFacadeLocal locFacade = SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class);
				
				EntityObjectVO<UID> calcFieldEO = nucletDalProvider.getEntityObjectProcessor(E.ENTITYFIELDCUSTOMIZATION).getByPrimaryKey(fieldUID);
				if (calcFieldEO == null) {
					throw new CommonFatalException("customized entity field " + fieldUID + " does not exist any more.");
				}
				UID calcAttributeDS = calcFieldEO.getFieldUid(E.ENTITYFIELDCUSTOMIZATION.calcAttributeDS);
				UID baseFieldUID = calcFieldEO.getFieldUid(E.ENTITYFIELDCUSTOMIZATION.entityfield);
				String paramValues = calcFieldEO.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.calcAttributeParamValues);
				String resIdLabel = calcFieldEO.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.localeresourcel);
				String resIdDesc = calcFieldEO.getFieldValue(E.ENTITYFIELDCUSTOMIZATION.localeresourced);
				
				FieldMeta<?> baseField = getEntityField(baseFieldUID);
				
				if (!LangUtils.equal(calcAttributeDS, baseField.getCalcAttributeDS())) {
					nucletDalProvider.getEntityObjectProcessor(E.ENTITYFIELDCUSTOMIZATION).delete(new Delete<UID>(fieldUID));
					throw new CommonFatalException("customized entity field " + fieldUID + " is not up to date.");
				}
				
				FieldMetaVO result = new FieldMetaVO(baseField);
				result.setUID(fieldUID);
				result.setCalcBaseFieldUID(baseFieldUID);
				result.setDbColumn(CalcAttributeUtils.getColumnName(fieldUID));
				result.setCalcAttributeParamValues(paramValues);
				Set<Pair<String, String>> resToExtend = new HashSet<Pair<String, String>>();
				if (resIdLabel != null) {
					result.setLocaleResourceIdForLabel(resIdLabel);
					resToExtend.add(new Pair<String, String>(resIdLabel, baseField.getLocaleResourceIdForLabel()));
				}
				if (resIdDesc != null) {
					result.setLocaleResourceIdForDescription(resIdDesc);
					resToExtend.add(new Pair<String, String>(resIdDesc, baseField.getLocaleResourceIdForDescription()));
				}
				for (Pair<String, String> resPair : resToExtend) {
					for (LocaleInfo li : locFacade.getAllLocales(false)) {
						String res = locFacade.getResourceById(li, resPair.y);
						Map<String, Object> paramValuesMap = CalcAttributeUtils.stringToMap(paramValues);
						Set<String> keys = new TreeSet<String>(paramValuesMap.keySet());
						StringBuffer resCalc = new StringBuffer();
						for (String key : keys) {
							Object nextValue = paramValuesMap.get(key);
							if (nextValue != null) {
								if (key.endsWith("Id")) {
									if (keys.contains(key.substring(0, key.length()-2))) {
										// id from from a value list provider, ignore
										continue;
									}
								}
								if (nextValue instanceof Date) {
									nextValue = DateFormat.getDateInstance(DateFormat.SHORT, li.toLocale()).format(nextValue);
								}
								if (nextValue instanceof Double) {
									nextValue = NumberFormat.getNumberInstance(li.toLocale()).format(nextValue);
								}
								if (resCalc.length() > 0) {
									resCalc.append(", ");
								}
								resCalc.append(nextValue);
							}
						}
						res = resCalc.length() == 0 ? res : resCalc.toString();
						locFacade.setResourceForLocale(resPair.x, li, res);
					}
				}
				
				dataCache.getMapFieldMetaData().put(fieldUID, result);
				return result;
			}
			if (checkOnly) {
				return null;
			}
			throw new CommonFatalException("entity field " + fieldUID + " does not exist.");
		}
	}
	
	@Override
	public Map<UID, LafParameterMap> getAllLafParameters() {
		final Map<UID, LafParameterMap> result = dataCache.getMapLafParameters();
		if (result == null) {
			return Collections.emptyMap();
		}
		return result;
	}

	@Override
	public LafParameterMap getLafParameters(UID entityUID) {
		return getAllLafParameters().get(entityUID);
	}
	
	public void rebuildMapsUnsafe() {
		LOG.info("Building Meta-Maps unsafe");
		dataCache.buildMapsImpl(true);
		nucletDalProvider.revalidate();
		invalidateWebLayoutCache();
	}

	public void revalidate(boolean otherCaches, boolean refreshMenus) {
		invalidateNucletCache();
		dataCache.buildMaps(true);
		nucletDalProvider.revalidate();
		invalidateWebLayoutCache();
		tryToInvalideFqnCaches();

		if (otherCaches) {
			/** re-/invalidate old caches */
			Modules.getInstance().invalidate();
			StateCache.getInstance().invalidateCache(true, true);
			StateModelUsagesCache.getInstance().invalidateCache(true, true);
			SecurityCache.getInstance().invalidate(false); // refresh menus will be done later on.
			SchemaCache.getInstance().invalidate();
			DatasourceCache.getInstance().invalidate();
			AttributeCache.getInstance().invalidateCache(true, true);
			GenericObjectMetaDataCache.getInstance().revalidate();
			ResourceCache.getInstance().invalidateCache(true, true);
			ServerParameterProvider.getInstance().invalidateCache(true, true);
			ServerServiceLocator.getInstance().getFacade(LocaleFacadeLocal.class).flushInternalCaches(true);
			//JobCache ->eventsupport.
			//GenerationCache ->eventsupport.
			//NucletCache ->eventsupport. 
			//EntityCache ->eventsupport.
			//EventSupportCache
			ServerServiceLocator.getInstance().getFacade(EventSupportFacadeLocal.class).invalidateCaches();
		}
		
		notifyClusterCloud();
		
		LOG.debug("JMS send: notify clients that meta data changed: {}", this);
		NuclosJMSUtils.sendOnceAfterCommitDelayed(new NotifyObject(null, refreshMenus), JMSConstants.TOPICNAME_METADATACACHE);
	}
	
	private void tryToInvalideFqnCaches() {
		try {
			SpringApplicationContextHolder.getBean(MasterDataRestFqnCache.class).invalidateCacheForEntity(E.ENTITY.getUID());
		} catch (Exception ex) {
			LOG.warn(ex.getMessage());
		}
	}

	@Override
	protected void finalize() throws Throwable {
		//this.clientnotifier.close();
		super.finalize();
	}

	class DataCache {
		
		private final ReadWriteLock lock = new ReentrantReadWriteLock();

		private abstract class ReaderWithLocking<T> {

			public ReaderWithLocking(final String problemMsg) {
				this.problemMsg = problemMsg;
			}

			private final String problemMsg;

			abstract T getInternal();

			public T get() {
				final Lock l = lock.readLock();
				try {
					if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
						return getInternal();
					} else {
						handleProblem(problemMsg);
					}
				} catch (InterruptedException e) {
					handleProblem(problemMsg, e);
				} catch (NuclosFatalException e) {
					throw e;
				} catch (Exception e) {
					handleProblem(problemMsg, e);
				} finally {
					l.unlock();
				}
				// Never reached because method returns or NuclosFatalException
				return null;
			}
		}

		private Map<UID, List<UID>> mapEntitiesByNuclets = null;

		private Map<UID, EntityMeta<?>> mapEntityMetaData = null;
		
		private Map<UID, FieldMeta<?>> mapFieldMetaData = null;
		
		private Map<UID, LafParameterMap> mapLafParameter = null;

		private MultiListMap<UID, EntityObjectVO<UID>> mapGenericImplementationsByGeneric = null;

		private MultiListMap<UID, EntityObjectVO<UID>> mapGenericFieldMappingByImplementation = null;
		
		DataCache() {
		}

		private void handleProblem(String msg, Exception e) throws NuclosFatalException {
			msg = "Could not aquire lock : " + msg + ": " + e;
			LOG.error(msg, e);
			throw new NuclosFatalException(msg, e);
		}

		private void handleProblem(String msg) throws NuclosFatalException {
			msg = "Could not aquire lock : " + msg;
			LOG.error(msg);
			throw new NuclosFatalException(msg);
		}

		public Map<UID, List<UID>> getMapEntitiesByNuclets() {
			return new ReaderWithLocking<Map<UID, List<UID>>>("getMapEntitiesByNuclets") {
				Map<UID, List<UID>> getInternal() {
					return mapEntitiesByNuclets;
				}
			}.get();
		}

		public Map<UID, EntityMeta<?>> getMapEntityMetaData() {
			return new ReaderWithLocking<Map<UID, EntityMeta<?>>>("getMapEntityMetaData") {
				Map<UID, EntityMeta<?>> getInternal() {
					return mapEntityMetaData;
				}
			}.get();
		}
		
		/**
		 * Set additional entity/field meta data.
		 * <p>
		 * ATTENTION: This method is only intended to use with tests!
		 * <p>
		 * TODO: At present using this is a little bit dangerous as no attempt
		 * is made to <em>delete</em> entity/field meta data that are not present
		 * any more.
		 * 
		 * @param metas collection of entity meta data to add to this cache
		 * @param overwrite weather to overwrite already present entity/field 
		 * 		meta data
		 * @return number of entities changed/processed
		 * 
		 * @author Thomas Pasch
		 * @since Nuclos 4.0.8
		 */
		int setMapEntityMetaData(Collection<EntityMeta<?>> metas, boolean overwrite) {
			final Lock l = lock.writeLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					if (mapEntityMetaData == null) {
						mapEntityMetaData = new HashMap<UID, EntityMeta<?>>();
					}
					if (mapFieldMetaData == null) {
						mapFieldMetaData = new HashMap<UID, FieldMeta<?>>();
					}					
					int result = 0;
					for (EntityMeta<?> em: metas) {
						final UID euid = em.getUID();
						if (overwrite || !mapEntityMetaData.containsKey(euid)) {
							mapEntityMetaData.put(euid, em);
							for (FieldMeta<?> f: em.getFields()) {
								final UID fuid = f.getUID();
								if (overwrite || !mapFieldMetaData.containsKey(fuid)) {
									mapFieldMetaData.put(fuid, f);
									FieldMetaUtils.putPkField(mapFieldMetaData, em);
								}
							}
							++result;
						}
					}
					return result;
				} else {
					handleProblem("setMapEntityMetaData");
				}
			} catch (InterruptedException e) {
				handleProblem("setMapEntityMetaData", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("setMapEntityMetaData", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return -9999;
		}

		public Map<UID, FieldMeta<?>> getMapFieldMetaData() {
			return new ReaderWithLocking<Map<UID, FieldMeta<?>>>("getMapFieldMetaData") {
				Map<UID, FieldMeta<?>> getInternal() {
					return mapFieldMetaData;
				}
			}.get();
		}
		
		/**
		 * Attention: must be called with acquired write lock! (tp)
		 */
		private Map<UID, EntityMeta<?>> buildMapEntityMetaData(MetaTablesContentFromDb metaTablesContent) {
			Map<UID, EntityMeta<?>> result = new HashMap<>();

			/*
			 * Nuclos Entities
			 */
			for (EntityMeta<?> systemMeta : E.getAllEntities()) {
				result.put(systemMeta.getUID(), systemMeta);
			}

			/*
			 * Nuclet Entities
			 */
			for (NucletEntityMeta eMeta : metaTablesContent.getNucletEntities()) {
				result.put(eMeta.getUID(), eMeta);
			}
			
			/*
			 * Data Language Entities
			 */
			for (NucletEntityMeta eMeta : metaTablesContent.getDataLangEntities()) {
				result.put(eMeta.getUID(), eMeta);
			}

			/*
			 * Dynamic Entities
			 */
			for(NucletEntityMeta meta : metaTablesContent.getDynamicEntities()) {
				result.put(meta.getUID(), meta);
			}

			/*
			 * Chart Entities
			 */
			for(NucletEntityMeta meta : metaTablesContent.getChartEntities()) {
				result.put(meta.getUID(), meta);
			}

			/*
			 * Dynamic Tasklist Entities
			 */
			for(NucletEntityMeta meta : metaTablesContent.getDynamicListEntities()) {
				result.put(meta.getUID(), meta);
			}
			
			return result;
		}

		private boolean hasDataLanguageEntity(Collection<FieldMeta<?>> fields) {
			boolean retVal = false;
			
			for (FieldMeta fm : fields) {
				if (fm.isLocalized()) {
					retVal = true;
					break;
				}
			}
			return retVal;
		}

		public Map<UID, LafParameterMap> getMapLafParameters() {
			return new ReaderWithLocking<Map<UID, LafParameterMap>>("getMapLafParameters") {
				Map<UID, LafParameterMap> getInternal() {
					return mapLafParameter;
				}
			}.get();
		}

		/**
		 * Attention: must be called with acquired write lock! (tp)
		 */
		private Map<UID, List<UID>> buildMapEntitiesByNuclets(MetaTablesContentFromDb metaTablesContent) {
			HashMap<UID, List<UID>> entitiesByNuclets = new HashMap<>();

			for (EntityMeta<?> meta : metaTablesContent.getNucletEntities()) {
				if (!entitiesByNuclets.containsKey(meta.getNuclet())) {
					entitiesByNuclets.put(meta.getNuclet(), new ArrayList<UID>());
				}
				if (entitiesByNuclets.containsKey(meta.getNuclet())) {
					entitiesByNuclets.get(meta.getNuclet()).add(meta.getUID());
				}
			}

			for (UID nuclet : entitiesByNuclets.keySet()) {
				entitiesByNuclets.put(nuclet, Collections.unmodifiableList(entitiesByNuclets.get(nuclet)));
			}

			return entitiesByNuclets;
		}
		
		/**
		 * Attention: must be called with acquired write lock! (tp)
		 */
		private <T> Map<UID, LafParameterMap<T,String>> buildMapLafParameters() {
			Map<UID, LafParameterMap<T,String>> result = new HashMap<UID, LafParameterMap<T,String>>();
			for (EntityLafParameterVO vo : nucletDalProvider.getEntityLafParameterProcessor().getAll()) {
				final UID uid = vo.getEntity();
				if (LafParameter.PARAMETERS.containsKey(vo.getParameter())) {
					final LafParameter<T,String> lafParameter = (LafParameter<T,String>) LafParameter.PARAMETERS.get(vo.getParameter());					
					if (!result.containsKey(uid)) {
						result.put(uid, new LafParameterHashMap<T,String>());
					}
					T value;
					try {
						value = lafParameter.convertFromStore(vo.getValue());
					} catch (ParseException e) {
						LOG.warn("Can't convert LafParameter {} (entityId={}) value {}: {}",
								vo.getParameter(), vo.getEntity(), vo.getValue(), e.toString(), e);
						value = lafParameter.getDefault();
					} catch (Exception e) {
						LOG.warn("Can't read LafParameter {} (entityId={}) value {}: {}",
								vo.getParameter(), vo.getEntity(), vo.getValue(), e.toString(), e);
						value = lafParameter.getDefault();
					}
					result.get(uid).putValue(lafParameter, value);
				} else {
					LOG.warn("LafParameter {} unknown. (entityId={})",
					         vo.getParameter(), vo.getEntity());
				}
			}
			return result;
		}
		
		private Map _buildMapLafParameters() {
			return buildMapLafParameters();
		}

		public MultiListMap<UID, EntityObjectVO<UID>> getMapGenericImplementationsByGeneric() {
			return new ReaderWithLocking<MultiListMap<UID, EntityObjectVO<UID>>>("getMapGenericImplementationsByGeneric") {
				MultiListMap<UID, EntityObjectVO<UID>> getInternal() {
					return mapGenericImplementationsByGeneric;
				}
			}.get();
		}

		private MultiListMap<UID, EntityObjectVO<UID>> buildMapGenericImplementationsByGeneric() {
			MultiListMap<UID, EntityObjectVO<UID>> result = new MultiListHashMap<>();
			final List<EntityObjectVO<UID>> all = nucletDalProvider.getEntityObjectProcessor(E.ENTITY_GENERIC_IMPLEMENTATION).getAll();
			for (EntityObjectVO<UID> eo : all) {
				result.addValue(eo.getFieldUid(E.ENTITY_GENERIC_IMPLEMENTATION.genericEntity), eo);
			}
			return result;
		}

		public MultiListMap<UID, EntityObjectVO<UID>> getMapGenericFieldMappingByImplementation() {
			return new ReaderWithLocking<MultiListMap<UID, EntityObjectVO<UID>>>("getMapGenericFieldMappingByImplementation") {
				MultiListMap<UID, EntityObjectVO<UID>> getInternal() {
					return mapGenericFieldMappingByImplementation;
				}
			}.get();
		}

		private MultiListMap<UID, EntityObjectVO<UID>> buildMapGenericFieldMappingByImplementation() {
			MultiListMap<UID, EntityObjectVO<UID>> result = new MultiListHashMap<>();
			final List<EntityObjectVO<UID>> all = nucletDalProvider.getEntityObjectProcessor(E.ENTITY_GENERIC_FIELDMAPPING).getAll();
			for (EntityObjectVO<UID> eo : all) {
				result.addValue(eo.getFieldUid(E.ENTITY_GENERIC_FIELDMAPPING.genericImplementation), eo);
			}
			return result;
		}
		
		private void buildMapsImpl(boolean forceReload) {
			MetaTablesContentFromDb metaTablesContent = nucletDalProvider.loadMetaTablesFromDb(forceReload);

			mapEntitiesByNuclets = Collections.unmodifiableMap(buildMapEntitiesByNuclets(metaTablesContent));
			mapEntityMetaData = Collections.unmodifiableMap(buildMapEntityMetaData(metaTablesContent));
			mapFieldMetaData = new ConcurrentHashMap<>(DalTransformations.flattenEntityMetaMap(mapEntityMetaData));
			FieldMetaUtils.putPkFields(mapFieldMetaData, mapEntityMetaData.values());
			mapLafParameter = Collections.unmodifiableMap(_buildMapLafParameters());
			mapGenericImplementationsByGeneric = buildMapGenericImplementationsByGeneric();
			mapGenericFieldMappingByImplementation = buildMapGenericFieldMappingByImplementation();
		}

		public void buildMaps(boolean forceReload) {
			LOG.debug("buildMaps: begin");
			final Lock l = lock.writeLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					LOG.debug("buildMaps: aquired lock");
					buildMapsImpl(forceReload);
				} else {
					handleProblem("buildMaps");
				}
			} catch (InterruptedException e) {
				handleProblem("buildMaps", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("buildMaps", e);
			} finally {
				l.unlock();
				LOG.info("buildMaps: end");
			}
		}

	} // class DataCache
		
	@Override
	public List<UID> getEntities(UID nuclet) {
		return dataCache.getMapEntitiesByNuclets().get(nuclet);
	}

	@Override
	public List<EntityObjectVO<UID>> getEntityMenus() {
		return nucletDalProvider.getEntityObjectProcessor(E.ENTITYMENU).getAll();
	}

	@Override
	public boolean isNuclosEntity(UID entityUID) {
		return E.isNuclosEntity(entityUID);
	}

	@Override
	public Collection<FieldMeta<?>> getAllReferencingFields(UID masterUID) {
		return getAllOrFirstReferencingFields(masterUID, true);
	}

	public boolean isReferenced(UID masterUID) {
		return !getAllOrFirstReferencingFields(masterUID, false).isEmpty();
	}

	private Collection<FieldMeta<?>> getAllOrFirstReferencingFields(UID masterUID, boolean all) {
		Collection<FieldMeta<?>> fields = new HashSet<FieldMeta<?>>();
		
		for (EntityMeta<?> ent : dataCache.getMapEntityMetaData().values()) {			
			for (FieldMeta<?> fm : ent.getFields()) {
				if (masterUID.equals(fm.getForeignEntity())) {
					fields.add(fm);
					if (!all) {
						return fields;
					}
				}
			}
		}
		return fields;
	}
	
	@ManagedOperation(description="dump entity meta data as XML to temporary file")
	@ManagedOperationParameter(name="includeSystemEntities", 
		description="weather to include system entity meta data in the XML dump")
	public String dumpAllEntities(boolean includeSystemEntities) {
		try {
			final File tmp = File.createTempFile("EntityMeta-", ".xml");
			tmp.deleteOnExit();
			final OutputStream out = new BufferedOutputStream(new FileOutputStream(tmp));
			return dumpAllEntities(out, includeSystemEntities) + " to " + tmp;
		} catch (IOException e) {
			return "dump failed: " + e;
		}
	}

	private String dumpAllEntities(OutputStream out, boolean includeSystemEntities) throws IOException {
		try (CloseableXStream closeable = xstreamSupport.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			final List<EntityMeta<?>> result = new ArrayList<EntityMeta<?>>();
			for (EntityMeta<?> em : dataCache.getMapEntityMetaData().values()) {
				if (includeSystemEntities || !E.isNuclosEntity(em.getUID())) {
					result.add(em);
				}
			}
			
			final Writer w = new OutputStreamWriter(out, "UTF-8");
			final HierarchicalStreamWriter writer = new PrettyPrintWriter(w);
			xstream.marshal(result, writer);
			
			// xstream.toXML(result, out);
			
			return "successfully dumped";
		}
	}

	@ManagedOperation(description="recover entity meta data from XML dump")
	@ManagedOperationParameters({
		@ManagedOperationParameter(name="f",
				description="XML file to recover entity meta data from"),
		@ManagedOperationParameter(name="overwrite", 
				description="weather to overwrite already existing entity meta data")
	})
	public String recoverEntities(String f, boolean overwrite) {
		try {
			final File file = new File(f);
			if (!file.exists()) {
				return "recover failed: file does not exist: " + f;
			}
			if (!file.canRead()) {
				return "recover failed: can read " + f;
			}
			final InputStream in = new BufferedInputStream(new FileInputStream(file));
			final Collection<EntityMeta<?>> result = recoverEntities(in);
			final int count = dataCache.setMapEntityMetaData(result, overwrite);
			return "successfully recovered from " + file.getAbsolutePath()
					+ ": " + count + " entities";
		} catch (IOException e) {
			return "recover entities failed: " + e;
		}
	}

	private Collection<EntityMeta<?>> recoverEntities(InputStream in) {
		try (CloseableXStream closeable = xstreamSupport.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return (Collection<EntityMeta<?>>) xstream.fromXML(in);
		}
	}

	@ManagedAttribute(description="get the size (number of entities) of dataCache.getMapEntityMetaData()")
	public int getNumberOfEntitiesInCache() {
		return dataCache.getMapEntityMetaData().size();
	}

	@ManagedAttribute(description="get the size (number of fields) of dataCache.getMapFieldMetaData()")
	public int getNumberOfFieldsInCache() {
		return dataCache.getMapFieldMetaData().size();
	}

	@ManagedAttribute(description="get the size (number of lap parameters) of dataCache.getMapLafParameters()")
	public int getNumberOfLafParametersInCache() {
		return dataCache.getMapLafParameters().size();
	}

	@Override
	public FieldMeta<?> getCalcAttributeCustomization(UID fieldUID, String paramValues) {
		if (paramValues == null) {
			throw new IllegalArgumentException("paramValues must not be null");
		}
		if (fieldUID == null) {
			throw new IllegalArgumentException("fieldUID must not be null");
		}
		
		FieldMeta<?> baseField = getEntityField(fieldUID);
		if (baseField.getCalcAttributeDS() == null) {
			throw new IllegalArgumentException("Field " + fieldUID + " is not calculated");
		}
		
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.ENTITYFIELDCUSTOMIZATION);
		query.multiselect(
				t.baseColumn(E.ENTITYFIELDCUSTOMIZATION.getPk()),
				t.baseColumn(E.ENTITYFIELDCUSTOMIZATION.localeresourcel),
				t.baseColumn(E.ENTITYFIELDCUSTOMIZATION.localeresourced));
		query.where(builder.equalValue(t.baseColumn(E.ENTITYFIELDCUSTOMIZATION.entityfield), fieldUID));
		query.addToWhereAsAnd(builder.equalValue(t.baseColumn(E.ENTITYFIELDCUSTOMIZATION.calcAttributeDS), baseField.getCalcAttributeDS()));
		query.addToWhereAsAnd(builder.equalValue(t.baseColumn(E.ENTITYFIELDCUSTOMIZATION.calcAttributeParamValues), paramValues));

		UID calcFieldUID = null;
		String resIdLabel = null;
		String resIdDesc = null;
		
		List<DbTuple> existing = dataBaseHelper.getDbAccess().executeQuery(query);
		if (!existing.isEmpty()) {
			DbTuple row = existing.get(0);
			calcFieldUID = row.get(E.ENTITYFIELDCUSTOMIZATION.getPk());
			resIdLabel = row.get(E.ENTITYFIELDCUSTOMIZATION.localeresourcel);
			resIdDesc = row.get(E.ENTITYFIELDCUSTOMIZATION.localeresourced);
		}
		
		if (calcFieldUID == null) {
			LocaleFacadeLocal locFacade = SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class);
			calcFieldUID = CalcAttributeUtils.createNewCalcAttributeCustomizationUID();
			if (baseField.getLocaleResourceIdForLabel() != null) {
				resIdLabel = locFacade.createResource(baseField.getLocaleResourceIdForLabel());
			}
			if (baseField.getLocaleResourceIdForDescription() != null) {
				resIdDesc = locFacade.createResource(baseField.getLocaleResourceIdForDescription());
			}
			DbMap values = new DbMap();
			values.put(SF.PK_UID, calcFieldUID);
			values.put(E.ENTITYFIELDCUSTOMIZATION.entityfield, fieldUID);
			values.put(E.ENTITYFIELDCUSTOMIZATION.calcAttributeDS, baseField.getCalcAttributeDS());
			values.put(E.ENTITYFIELDCUSTOMIZATION.calcAttributeParamValues, paramValues);
			if (resIdLabel != null) {
			values.put(E.ENTITYFIELDCUSTOMIZATION.localeresourcel, resIdLabel);
			}
			if (resIdDesc != null) {
				values.put(E.ENTITYFIELDCUSTOMIZATION.localeresourced, resIdDesc);
			}
			values.put(SF.CREATEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
			values.put(SF.CREATEDBY, "metaProvider");
			values.put(SF.CHANGEDAT, values.get(SF.CREATEDAT));
			values.put(SF.CHANGEDBY, values.get(SF.CREATEDBY));
			values.put(SF.VERSION, 1);
			dataBaseHelper.getDbAccess().execute(new DbInsertStatement<UID>(E.ENTITYFIELDCUSTOMIZATION, values));
		}
		
		return getEntityField(calcFieldUID);
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.METAPROVIDER_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);			
	}

	@Override
	public void registerCache() {		
	}

	@Override
	public void deregisterCache() {		
	}

	@Override
	public String getName() {		
		return null;
	}

	@Override
	public void invalidateCache(boolean notifyClients,	boolean notifyClusterCloud) {
	}
	
	private List<EntityObjectVO<UID>> lstAllNuclets;
	
	private void loadNuclets() {
		lstAllNuclets = nucletDalProvider.getEntityObjectProcessor(E.NUCLET).getAll();
	}
	
	@Cacheable(value="eoNuclets", key="#nucletUID", condition="#nucletUID != null")
	public EntityObjectVO<UID> getNuclet(UID nucletUID) {
		
		EntityObjectVO<UID> retVal = null;
		
		for (EntityObjectVO<UID> eoVO : getNuclets()) {
			if (nucletUID.equals(eoVO.getPrimaryKey())) {
				retVal = eoVO;
				break;
			}
		}
		
		return retVal;
	}
	
	@Override
	public String getFullQualifiedNucletName(UID nucletUID) {
		return getNuclet(nucletUID).getFieldValue(E.NUCLET.packagefield);
	}
	
	public List<EntityObjectVO<UID>> getNuclets() {
		if (lstAllNuclets.isEmpty()) {
			loadNuclets();			
		}
		
		return this.lstAllNuclets;
	}

	@CacheEvict(value="eoNuclets", allEntries=true) 
	private void evictNucletCache() {}
	
	public void invalidateNucletCache() {
		evictNucletCache();
		this.lstAllNuclets.clear();
	}

	@Caching(evict = {
			@CacheEvict(value = "webLayoutFixed", allEntries = true),
			@CacheEvict(value = "webLayoutCalculated", allEntries = true),
			@CacheEvict(value = "webLayoutResponsive", allEntries = true),
			@CacheEvict(value = "webLayoutRules", allEntries = true)
	})
	public void invalidateWebLayoutCache() {
	}

	public boolean hasEntity(UID entityUID) {
		
		boolean retVal = false;
		
		try {
			getEntity(entityUID);
			retVal = true;
		} catch (Exception e) {}
		
		return retVal;
	}
	
	public static boolean isDataEntity(EntityMeta<?> eMeta) {
		if (eMeta == null || eMeta.isDynamic() || eMeta.isProxy() || eMeta.isGeneric() || eMeta.isChart() || eMeta.getVirtualEntity() != null) {
			return false;
		}
		
		return !E.isNuclosEntity(eMeta.getUID()) || E.GENERALSEARCHDOCUMENT.checkEntityUID(eMeta.getUID()) || E.USER.checkEntityUID(eMeta.getUID()) || E.DOCUMENTFILE.checkEntityUID(eMeta.getUID());
	}
	
	public Collection<EntityMeta<?>> getDataEntities(Collection<String> virtualDataEntities) {
		Collection<EntityMeta<?>> ret = new HashSet<EntityMeta<?>>();
		
		for (EntityMeta<?> em : dataCache.getMapEntityMetaData().values()) {
			if (isDataEntity(em) || virtualDataEntities.contains(em.getVirtualEntity())) {
				ret.add(em);
			}
		}
		
		return ret;
	}

	public Collection<EntityMeta<?>> getVirtualEntities() {
		Collection<EntityMeta<?>> ret = new HashSet<EntityMeta<?>>();
		
		for (EntityMeta<?> em : dataCache.getMapEntityMetaData().values()) {
			if (em.getVirtualEntity() != null) {
				ret.add(em);
			}
		}
		
		return ret;
	}

	@Override
	public List<EntityObjectVO<UID>> getImplementingEntityDetails(UID genericEntityUID) {
		return dataCache.getMapGenericImplementationsByGeneric().getValues(genericEntityUID);
	}

	@Override
	public List<EntityObjectVO<UID>> getImplementingFieldMapping(UID genericImplementationUID) {
		return dataCache.getMapGenericFieldMappingByImplementation().getValues(genericImplementationUID);
	}

	@Override
	public Set<UID> getImplementingEntities(UID genericEntityUID) {
		final List<EntityObjectVO<UID>> allImplementations = dataCache.getMapGenericImplementationsByGeneric().getValues(genericEntityUID);
		final Set<UID> result = new HashSet<>();
		for (EntityObjectVO<?> eo : allImplementations) {
			result.add(eo.getFieldUid(E.ENTITY_GENERIC_IMPLEMENTATION.implementingEntity));
		}
		return result;
	}

	public final <PK> List<String> getVersionConflictMessages(String type, EntityObjectVO<PK> newVo, EntityObjectVO<PK> oldVo) {
		List<String> msgs = new ArrayList<>();
		EntityMeta<?> eMeta = getEntity(newVo.getDalEntity());
		msgs.add("version: " + newVo.getVersion() + " (this) != " + oldVo.getVersion());
		for (UID f : newVo.getFieldsWithDifferentValue(oldVo)) {
			msgs.add(getEntityField(f) + ": " + newVo.getFieldValue(f) + " != " + oldVo.getFieldValue(f));
		}
		Collections.sort(msgs);
		String rec = type != null? type : "record";
		msgs.add(0, "This " + rec + " " + eMeta + "/" + newVo.getPrimaryKey() + " has been");
		msgs.add(1, "updated by " + oldVo.getChangedBy() + " at " + oldVo.getChangedAt());
		if (!RigidUtils.equal(newVo.getDependents(), oldVo.getDependents())) {
			msgs.add("dependents: " + newVo.getDependents() + " != " + oldVo.getDependents());
		}
		return msgs;
	}
}
