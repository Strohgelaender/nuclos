//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.cxf.management.annotation.ManagedOperation;
import org.nuclos.common.Actions;
import org.nuclos.common.CommonSecurityCache;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.MandatorLevelVO;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common.security.AttributePermissionKey;
import org.nuclos.common.security.NotifyObject;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.autosync.XMLEntities;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.common.ejb3.SecurityFacadeBean;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.security.SessionContext;
import org.nuclos.server.statemodel.valueobject.StateModelUsages.StateModelUsage;
import org.nuclos.server.statemodel.valueobject.StateModelUsagesCache;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.tasklist.TasklistFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Singleton class for getting permissions.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * TODO: Name is misleading. This class does much more than just caching...
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 00.01.000
 */
@Component
public class SecurityCache implements ClusterCache, CommonSecurityCache {

	private static final Logger LOG = LoggerFactory.getLogger(SecurityCache.class);

	private static SecurityCache INSTANCE;
	
	//
	
	private int masterDataPermissionsMinSize;

	private int masterDataPermissionsMaxSize;

	private SpringDataBaseHelper dataBaseHelper;
	
	private StateCache stateCache;
	
	private MetaProvider metaProvider;
	
	private StateModelUsagesCache stateModelUsagesCache;

	private final Map<AttributePermissionKey, Permission> mpAttributePermission
		= new ConcurrentHashMap<>();

	private final Map<UID, String> mpUserName = new ConcurrentHashMap<>();
	
	private final Map<String, Map<UID, UserRights>> mpUserRights = new ConcurrentHashMap<>();

	private final Map<UserAttributeGroup, Map<UID, Permission>> mpAttributeGroups
		= new ConcurrentHashMap<>();
	
	private final Map<UserSubForm, Map<UID, SubformPermission>> mpSubForms = new ConcurrentHashMap<>();
	
	private boolean mapRoleNamesFilled = false;
	private final Map<UID, String> mapRoleNames
		= new ConcurrentHashMap<>();
	
	/**
	 * Sorted list of mandators
	 */
	private final List<MandatorVO> lstMandators = new ArrayList<>();
	/**
	 * Key: parentUID with 
	 * UID.UID_NULL for the roots
	 */
	private final Map<UID, Set<UID>> mandatorChildren = new ConcurrentHashMap<UID, Set<UID>>();
	/**
	 * the accessible mandators for the given mandator
	 * 		accessible= all childrens + direct parents up to root
	 */
	private final Map<UID, Set<UID>> mandatorAccessible = new ConcurrentHashMap<UID, Set<UID>>();
	/**
	 * mandator by pk
	 */
	private final Map<UID, MandatorVO> mapMandators = new ConcurrentHashMap<UID, MandatorVO>();
	/**
	 * mandators by level
	 */
	private final Map<UID, List<MandatorVO>> mandatorsByLevel = new ConcurrentHashMap<UID, List<MandatorVO>>();
	/**
	 * level by pk
	 */
	private final Map<UID, MandatorLevelVO> mapLevels = new ConcurrentHashMap<UID, MandatorLevelVO>();
	/**
	 * Sorted list of mandator levels
	 */
	private final List<MandatorLevelVO> lstMandatorLevels = new ArrayList<MandatorLevelVO>();
	
	private boolean mandatorsloaded = false;

	private final Map<String, SessionContext> mapSessionContexts = new HashMap<String, SessionContext>();
	private final Map<String, Map<String, Authentication>> mapFatClientContexts = new HashMap<String, Map<String, Authentication>>();

	private final TransactionSynchronization invalidate = new TransactionSynchronizationAdapter() {
		@Override
		public synchronized void afterCommit() {
			LOG.info("afterCommit: Invalidating security cache...");

			mpUserRights.clear();
			mpAttributeGroups.clear();
			mpSubForms.clear();
			mpAttributePermission.clear();
			mapFatClientContexts.clear();
			clearMapRoleNames();
			synchronized (lstMandators) {
				lstMandators.clear();
				lstMandatorLevels.clear();
				mandatorChildren.clear();
				mandatorAccessible.clear();
				mapMandators.clear();
				mandatorsByLevel.clear();
				mapLevels.clear();
				mandatorsloaded = false;
				cleanDbCache();
			}
		}
	};

	/**
	 * Determines whether the given entity is a dynamic task list and the given user is allowed to read it.
	 */
	public boolean isReadAllowedForDynamicTaskList(
			final UID entity,
			final String user,
			final UID mandator
	) {
		EntityMeta<Object> meta = metaProvider.getEntity(entity);
		return meta != null && meta.isDynamicTasklist()
				// TODO: Ugly String comparison here, because "entity" UID has a prefix for dynamic task lists,
				// but getDynamicTasklistDatasources returns only UIDs without prefix...
				&& getDynamicTasklistDatasources(user, mandator).stream().anyMatch(
						taskListUid -> entity.toString().endsWith(taskListUid.toString())
		);
	}

	private class UserRights {

		private final String sUserName;
		private final UID mandator;

		private ModulePermissions modulepermissions;
		private MasterDataPermissions masterdatapermissions;
		private Collection<UID> collTransitionUids;
		private Map<ReportType,Collection<UID>> result;
		private Collection<UID> collWritableReportUids;
		private Map<UID, String> collAllowedCustomRestRules;
		private Collection<UID> collAssignedGeneratorUids;
		private Collection<UID> collAssignedRecordgrantUids;
		private Collection<UID> collReadableDataSourceUids;
		private Collection<UID> collWritableDataSourceUids;
		private Collection<CompulsorySearchFilter> collCompulsorySearchFilters;
		private Set<String> actions;
		private Set<UID> customActions;
		private Set<UID> roleUids;
		private Set<UID> dynamicTasklistUids;
		private Set<UID> dynamicTasklistDatasourceUids;
		private Set<UID> mandatorUids;
		
		private UID userUid;
		private Boolean isSuperUser;
		private UID communicationAccountPhone = UID.UID_NULL;

		UserRights(String sUserName, UID mandator) {
			this.sUserName = sUserName;
			this.mandator = mandator;
		}

		public synchronized boolean isSuperUser() {
			if (isSuperUser == null) {
				readUserData();
			}
			return isSuperUser;
		}

		private synchronized UID getUserUid() {
			if (userUid == null) {
				readUserData();
			}
			return userUid;
		}
		
		public synchronized UID getCommunicationAccountPhone() {
			if (UID.UID_NULL.equals(communicationAccountPhone)) {
				readUserData();
			}
			return communicationAccountPhone;
		}

		public synchronized Set<String> getAllowedActions() {
			if (actions == null) {
				actions = readActions();
			}
			return actions;
		}
		
		public synchronized Set<UID> getAllowedCustomActions() {
			if (customActions == null) {
				customActions = readCustomActions();
			}
			return customActions;
		}

		public synchronized ModulePermissions getModulePermissions() {
			if (modulepermissions == null) {
				modulepermissions = readModulePermissions();
			}
			return modulepermissions;
		}

		public synchronized MasterDataPermissions getMasterDataPermissions() {
			if (masterdatapermissions == null) {
				masterdatapermissions = readMasterDataPermissions();
				final int newSize = masterdatapermissions.size();
				masterDataPermissionsMaxSize = Math.max(masterDataPermissionsMaxSize, newSize);
				if (masterDataPermissionsMinSize <= 0) {
					masterDataPermissionsMinSize = newSize;
				} else {
					masterDataPermissionsMinSize = Math.min(masterDataPermissionsMinSize, newSize);
				}
			}
			return masterdatapermissions;
		}

		public synchronized Collection<UID> getTransitionUids() {
			if (collTransitionUids == null) {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<UID> query = builder.createQuery(UID.class);

				if (isSuperUser()) {
					DbFrom t = query.from(E.STATETRANSITION);
					query.select(t.baseColumn(E.STATETRANSITION.getPk()));
				} else {
					DbFrom t = query.from(E.ROLETRANSITION);
					query.select(t.baseColumn(E.ROLETRANSITION.transition));
					query.where(t.baseColumn(E.ROLETRANSITION.role).in(getRoleUids()));
				}

				collTransitionUids = dataBaseHelper.getDbAccess().executeQuery(query.distinct(true));
			}
			return collTransitionUids;
		}

		public synchronized Map<ReportType,Collection<UID>> getReadableReports() {
			if (result == null) {
				result = readReports(false);
			}
			return result;
		}

		public synchronized Collection<UID> getWritableReportUids() {
			// @todo refactor: It doesn't make sense to calculate the writable and readable reports in two independent queries.
			if (collWritableReportUids == null) {
				Map<ReportType, Collection<UID>> reports = readReports(true);
				collWritableReportUids = CollectionUtils.concatAll(reports.values());
			}
			return collWritableReportUids;
		}

		public synchronized Map<UID, String> getAllowedCustomRestRules() {
			if (collAllowedCustomRestRules == null) {
				collAllowedCustomRestRules = readCustomRestRules();
			}
			return collAllowedCustomRestRules;
		}

		public synchronized Collection<UID> getAssignedGeneratorUids() {
			if (collAssignedGeneratorUids == null) {
				collAssignedGeneratorUids = readGenerations();
			}
			return collAssignedGeneratorUids;
		}

		public synchronized Collection<UID> getAssignedRecordgrantUids() {
			if (collAssignedRecordgrantUids == null) {
				collAssignedRecordgrantUids = readRecordgrants();
			}
			return collAssignedRecordgrantUids;
		}

		private synchronized Map<ReportType,Collection<UID>> readReports(boolean readWrite) {
			Map<ReportType,Collection<UID>> result = new HashMap<ReportType,Collection<UID>>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.REPORT);
			query.multiselect(t.baseColumn(E.REPORT.getPk()), t.baseColumn(E.REPORT.type));
			if (!isSuperUser()) {
				DbFrom rr = t.join(E.ROLEREPORT, JoinType.LEFT, "rr").on(E.REPORT.getPk(), E.ROLEREPORT.report);
				DbColumnExpression<UID> rrc = rr.baseColumn(E.ROLEREPORT.role);
				DbCondition readWriteCond = readWrite
					? builder.equalValue(rr.baseColumn(E.ROLEREPORT.readwrite), true)
					: builder.alwaysTrue();
				query.where(builder.or(
					getNameCondition(query, t, SF.CREATEDBY),
					builder.and(rrc.in(getRoleUids()), readWriteCond)));
			}

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				UID uid = tuple.get(0, UID.class);
				ReportType type = KeyEnum.Utils.findEnum(ReportType.class, tuple.get(1, Integer.class));
				if (result.containsKey(type))
					result.get(type).add(uid);
				else {
					Collection<UID> uids = new HashSet<UID>();
					uids.add(uid);
					result.put(type, uids);
				}
			}

			return result;
		}

		private synchronized Map<UID, String> readCustomRestRules() {
			Map<UID, String> result = new HashMap<>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.SERVERCODE);
			query.multiselect(t.baseColumn(E.SERVERCODE.getPk()), t.baseColumn(E.SERVERCODE.name));
			DbFrom rr = t.join(E.ROLESERVERCODE, JoinType.LEFT, "rr").on(E.SERVERCODE.getPk(), E.ROLESERVERCODE.servercode);
			DbColumnExpression<UID> rrc = rr.baseColumn(E.ROLESERVERCODE.role);

			if (!isSuperUser()) {
				query.where(rrc.in(getRoleUids()));
			} else {
				query.where(rrc.isNotNull());
			}

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				UID uid = tuple.get(0, UID.class);
				String name = tuple.get(1, String.class);
				result.put(uid, name);
			}

			return result;
		}

		private synchronized Collection<UID> readGenerations() {
			Collection<UID> result = new ArrayList<UID>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.GENERATION);
			query.multiselect(t.baseColumn(E.GENERATION.getPk()), t.baseColumn(E.GENERATION.name));
			if (!isSuperUser()) {
				DbFrom rr = t.join(E.ROLEGENERATION, JoinType.LEFT, "rr").on(E.GENERATION.getPk(), E.ROLEGENERATION.generation);
				DbColumnExpression<UID> rrc = rr.baseColumn(E.ROLEGENERATION.role);
				query.where(
					rrc.in(getRoleUids()));
			}

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				UID uid = tuple.get(0, UID.class);
				result.add(uid);
			}

			return result;
		}

		private synchronized Collection<UID> readRecordgrants() {
			Collection<UID> result = new ArrayList<UID>();

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.RECORDGRANT);
			query.multiselect(t.baseColumn(E.RECORDGRANT.getPk()), t.baseColumn(E.RECORDGRANT.name));
			if (!isSuperUser()) {
				DbFrom rr = t.join(E.ROLERECORDGRANT, JoinType.LEFT, "rr").on(E.RECORDGRANT.getPk(), E.ROLERECORDGRANT.recordgrant);
				DbColumnExpression<UID> rrc = rr.baseColumn(E.ROLERECORDGRANT.role);
				query.where(
					rrc.in(getRoleUids()));
			}

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
				UID uid = tuple.get(0, UID.class);
				result.add(uid);
			}

			return result;
		}

		public synchronized Collection<UID> getReadableDataSourceUids() {
			if (collReadableDataSourceUids == null) {
				collReadableDataSourceUids = readDataSourceUids(false);
			}
			return collReadableDataSourceUids;
		}

		public synchronized Collection<UID> getWritableDataSourceUids() {
			if (collWritableDataSourceUids == null) {
				collWritableDataSourceUids = readDataSourceUids(true);
			}
			return collWritableDataSourceUids;
		}

		public synchronized Set<UID> getCompulsorySearchFilterUids(UID entityUID) {
			if (collCompulsorySearchFilters == null) {
				collCompulsorySearchFilters = readCompulsorySearchFilterUids();
			}
			Date now = DateUtils.getPureDate(DateUtils.now());
			Set<UID> result = new HashSet<UID>();
			for (CompulsorySearchFilter f : collCompulsorySearchFilters) {
				if (f.entityUID.equals(entityUID) && f.isValid(now))
					result.add(f.uid);
			}
			return result;
		}

		private synchronized Collection<UID> readDataSourceUids(boolean readWrite) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom t = query.from(E.DATASOURCE);
			query.select(t.baseColumn(E.DATASOURCE.getPk()));

			if (!isSuperUser()) {
				DbFrom ro = t.join(E.REPORTOUTPUT, JoinType.LEFT, "ro").on(E.DATASOURCE.getPk(), E.REPORTOUTPUT.datasource);
				List<UID> reportUids = CollectionUtils.concatAll(readReports(readWrite).values());
				query.where(builder.or(
					getNameCondition(query, t, SF.CREATEDBY),
					ro.baseColumn(E.REPORTOUTPUT.parent).in(reportUids)));
			}

			return dataBaseHelper.getDbAccess().executeQuery(query.distinct(true));
		}

		private DbCondition getNameCondition(DbQuery<?> q, DbFrom t, DbField<String> usercolumn) {
			DbQueryBuilder builder = q.getBuilder();
			return builder.equal(builder.upper(t.baseColumn(usercolumn)), builder.upper(builder.literal(sUserName)));
		}

		public synchronized Set<UID> getRoleUids() {
			if (roleUids == null) {
				roleUids = readUserRoleUids();
			}
			return roleUids;
		}
		
		public synchronized Set<UID> getDynamicTasklistUids() {
			if (dynamicTasklistUids == null) {
				dynamicTasklistUids = readDynamicTasklistUids();
			}
			return dynamicTasklistUids;
		}
		
		public synchronized Set<UID> getDynamicTasklistDatasourceUids() {
			if (dynamicTasklistDatasourceUids == null) {
				dynamicTasklistDatasourceUids = readDynamicTasklistDatasourceUids();
			}
			return dynamicTasklistDatasourceUids;
		}

		private ModulePermissions readModulePermissions() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

			final Map<UID, ModulePermission> mpByModuleUid
				= new ConcurrentHashMap<>();
			final Map<UID, Boolean> mpNewAllowedByModuleUid 
				= new ConcurrentHashMap<>();
			final Map<UID, Set<UID>> mpNewAllowedProcessesByModuleUid 
				= new ConcurrentHashMap<>();

			if (isSuperUser()) {
				DbQuery<UID> query = builder.createQuery(UID.class);
				DbFrom t = query.from(E.ENTITY);
				query.select(t.baseColumn(E.ENTITY));
				query.where(builder.equalValue(t.baseColumn(E.ENTITY.usesstatemodel), true));
				for (UID entityUID : dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))) {
					ModulePermission permission = ModulePermission.DELETE_PHYSICALLY;

					mpByModuleUid.put(entityUID, permission);
					mpNewAllowedByModuleUid.put(entityUID, Boolean.TRUE);

					DbQuery<UID> processQuery = builder.createQuery(UID.class);
					DbFrom p = processQuery.from(E.PROCESS);
					processQuery.select(p.baseColumn(E.PROCESS));
					processQuery.where(builder.equalValue(p.baseColumn(E.PROCESS.module), entityUID));
					List<UID> allProcesses = dataBaseHelper.getDbAccess().executeQuery(processQuery);
					
					Set<UID> newAllowedProcesses = new HashSet<UID>();
					for (UID stateModelUID : stateModelUsagesCache.getStateUsages().getStateModelUIDsByEntity(entityUID)) {
						StateTransitionVO initialTransitionVO = getStateCache().getInitialTransistionByModel(stateModelUID);
						if (stateModelUID != null && initialTransitionVO != null) {
							for (UsageCriteria uc : stateModelUsagesCache.getStateUsages().getUsageCriteriasByStateModelUID(stateModelUID)) {
								newAllowedProcesses.add(uc.getProcessUID());
								if (uc.getProcessUID() == null) {
									newAllowedProcesses.addAll(allProcesses);
								}
							}
						}
					}
					mpNewAllowedProcessesByModuleUid.put(entityUID, newAllowedProcesses);
				}
			} else {
				DbQuery<DbTuple> query = builder.createTupleQuery();
				DbFrom m = query.from(E.ENTITY, "m");
				DbFrom re = m.join(E.ROLEMODULE, JoinType.INNER, E.ROLEMODULE.module, "re");
				query.multiselect(
					m.baseColumn(E.ENTITY),
					re.baseColumn(E.ROLEMODULE.modulepermission));
				query.where(builder.and(
					re.baseColumn(E.ROLEMODULE.role).in(getRoleUids()),
					builder.equalValue(m.baseColumn(E.ENTITY.usesstatemodel), true)));
				for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
					UID entityUID = tuple.get(0, UID.class);
					Integer ipermission = tuple.get(1, Integer.class);
					ModulePermission permission = ModulePermission.getInstance(ipermission);
					permission = permission.max(mpByModuleUid.get(entityUID));

					mpByModuleUid.put(entityUID, permission);

					/** is new allowed is defined in initial state model. */
					if (!mpNewAllowedByModuleUid.containsKey(entityUID)) {
						StateModelUsage smu = stateModelUsagesCache.getStateUsages().getStateModelUsage(new UsageCriteria(entityUID, null, null, null));
						Boolean isNewAllowed = false;
						if (smu != null) {
							UID stateModelUid = smu.getStateModelUID();
							StateTransitionVO initialTransitionVO = stateModelUid!=null? getStateCache().getInitialTransistionByModel(stateModelUid) : null;
							isNewAllowed = stateModelUid != null && initialTransitionVO != null &&
								!CollectionUtils.intersection(initialTransitionVO.getRoleUIDs(), getRoleUids()).isEmpty();
						}
						mpNewAllowedByModuleUid.put(entityUID, isNewAllowed);
					}

					DbQuery<UID> processQuery = builder.createQuery(UID.class);
					DbFrom p = processQuery.from(E.PROCESS);
					processQuery.select(p.baseColumn(E.PROCESS));
					processQuery.where(builder.equalValue(p.baseColumn(E.PROCESS.module), entityUID));
					List<UID> allProcesses = dataBaseHelper.getDbAccess().executeQuery(processQuery);
					
					boolean isNewAllowedForUndefinedProcess = false;
					
					/** is new process allowed is defined in state model. */
					if (!mpNewAllowedProcessesByModuleUid.containsKey(entityUID)) {
						Set<UID> newAllowedProcesses = new HashSet<UID>();

						for (UID stateModelUid : stateModelUsagesCache.getStateUsages().getStateModelUIDsByEntity(entityUID)) {
							StateTransitionVO initialTransitionVO = getStateCache().getInitialTransistionByModel(stateModelUid);
							final Boolean isNewAllowed = stateModelUid != null && initialTransitionVO != null &&
								!CollectionUtils.intersection(initialTransitionVO.getRoleUIDs(), getRoleUids()).isEmpty();
							if (isNewAllowed) {
								for (UsageCriteria uc : stateModelUsagesCache.getStateUsages().getUsageCriteriasByStateModelUID(stateModelUid)) {
									newAllowedProcesses.add(uc.getProcessUID());
									if (uc.getProcessUID() == null) {
										isNewAllowedForUndefinedProcess = true;
									}
								}
							}
							else {
								for (UsageCriteria uc : stateModelUsagesCache.getStateUsages().getUsageCriteriasByStateModelUID(stateModelUid)) {
									if (uc.getProcessUID() != null) {
										allProcesses.remove(uc.getProcessUID());
									}
								}
							}
						}
						
						if (isNewAllowedForUndefinedProcess) {
							newAllowedProcesses.addAll(allProcesses);
						}
						
						mpNewAllowedProcessesByModuleUid.put(entityUID, newAllowedProcesses);
					}
				}
			}

			return new ModulePermissions(mpByModuleUid, mpNewAllowedByModuleUid, mpNewAllowedProcessesByModuleUid);
		}

		private MasterDataPermissions readMasterDataPermissions() {
			final Map<UID, MasterDataPermission> mpByEntityUid = new HashMap<UID, MasterDataPermission>();

			if (isSuperUser()) {
				for (EntityMeta metaVO : metaProvider.getAllEntities()) {
					mpByEntityUid.put(metaVO.getUID(), MasterDataPermission.DELETE);
				}
			} else {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<DbTuple> query = builder.createTupleQuery();
				DbFrom t = query.from(E.ROLEMASTERDATA);
				query.multiselect(t.baseColumn(E.ROLEMASTERDATA.entity), t.baseColumn(E.ROLEMASTERDATA.masterdatapermission));
				query.where(t.baseColumn(E.ROLEMASTERDATA.role).in(getRoleUids()));
				for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
					UID entityUID = tuple.get(0, UID.class);
					MasterDataPermission mp = MasterDataPermission.getInstance(tuple.get(1, Integer.class));
					mpByEntityUid.put(entityUID, mp.max(mpByEntityUid.get(entityUID)));
				}
			}

			return new MasterDataPermissions(mpByEntityUid);
		}

		private Set<UID> readUserRoleUids() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom t = query.from(E.ROLEUSER);
			query.select(t.baseColumn(E.ROLEUSER.role));
			query.where(builder.equalValue(t.baseColumn(E.ROLEUSER.user), getUserUid()));
			Set<UID> result = new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true)));
			
			if (mandator != null) {
				builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				query = builder.createQuery(UID.class);
				t = query.from(E.MANDATOR_ROLE_USER);
				query.select(t.baseColumn(E.MANDATOR_ROLE_USER.role));
				DbJoin mu = t.joinOnJoinedPk(E.MANDATOR_USER, JoinType.INNER, E.MANDATOR_ROLE_USER.mandatorUser, "mu");
				query.where(builder.equalValue(mu.baseColumn(E.MANDATOR_USER.user), getUserUid()));
				query.addToWhereAsAnd(builder.equalValue(mu.baseColumn(E.MANDATOR_USER.mandator), mandator));
				result.addAll(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true)));
			}
			
			return Collections.unmodifiableSet(result);
		}
		
		private Set<UID> readDynamicTasklistUids() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom<UID> t1 = query.from(E.TASKLIST);
			DbJoin<?> t2 = t1.join(E.TASKLISTROLE, JoinType.INNER, "T2").on(E.TASKLIST.getPk(), E.TASKLISTROLE.tasklist);
			//DbJoin<?> t3 = t2.join(E.ROLEUSER, JoinType.INNER, "T3").on(E.TASKLISTROLE.role, E.ROLEUSER.role);
			query.select(t1.baseColumn(E.TASKLIST));
			//query.where(builder.and(builder.equalValue(t3.baseColumn(E.ROLEUSER.user), getUserUid()),
			query.where(builder.and(t2.baseColumn(E.TASKLISTROLE.role).in(getRoleUids()),
					builder.isNotNull(t1.baseColumn(E.TASKLIST.datasource))));
			return Collections.unmodifiableSet(new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))));
		}
		
		private Set<UID> readDynamicTasklistDatasourceUids() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom<UID> t1 = query.from(E.TASKLIST);
			DbJoin<?> t2 = t1.join(E.TASKLISTROLE, JoinType.INNER, "T2").on(E.TASKLIST.getPk(), E.TASKLISTROLE.tasklist);
			//DbJoin<?> t3 = t2.join(E.ROLEUSER, JoinType.INNER, "T3").on(E.TASKLISTROLE.role, E.ROLEUSER.role);
			query.select(t1.baseColumn(E.TASKLIST.datasource));
			//query.where(builder.and(builder.equalValue(t3.baseColumn(E.ROLEUSER.user), getUserUid()),
			query.where(builder.and(t2.baseColumn(E.TASKLISTROLE.role).in(getRoleUids()), 
					builder.isNotNull(t1.baseColumn(E.TASKLIST.datasource))));
			
			return Collections.unmodifiableSet(new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true))));
		}

		private Set<String> readActions() {
			if (isSuperUser()) {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<String> query = builder.createQuery(String.class);
				DbFrom t = query.from(E.ACTION);
				query.select(t.baseColumn(E.ACTION.action));
				Set<String> actions = new HashSet<String>(dataBaseHelper.getDbAccess().executeQuery(query));

				for(MasterDataVO<UID> mdvo : XMLEntities.getData(E.ACTION).getAll()) {
					actions.add(mdvo.getFieldValue(E.ACTION.action));
				}
				return Collections.unmodifiableSet(actions);
			}

			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<String> query = builder.createQuery(String.class);
			DbFrom t = query.from(E.ROLEACTION);
			query.select(t.baseColumn(E.ROLEACTION.action));
			query.where(t.baseColumn(E.ROLEACTION.role).in(getRoleUids()));
			return Collections.unmodifiableSet(new HashSet<String>(dataBaseHelper.getDbAccess().executeQuery(query)));
		}
		
		private Set<UID> readCustomActions() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom t = query.from(E.ROLEACTION);
			t.innerJoin(E.ACTION, "taction").on(E.ROLEACTION.action, E.ACTION.action);
			query.select(t.column("taction", E.ACTION.getPk()));
			query.where(t.baseColumn(E.ROLEACTION.role).in(getRoleUids()));
			return Collections.unmodifiableSet(new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query)));
		}

		public synchronized Map<UID, SubformPermission> readSubFormPermissions(UID entityUID) {
			final Map<UID, SubformPermission> result = new HashMap<>();
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

			if (isSuperUser()) {
				DbQuery<UID> query = builder.createQuery(UID.class);
				DbFrom t = query.from(E.STATE);
				query.select(t.baseColumn(E.STATE));
				for (UID stateUID : dataBaseHelper.getDbAccess().executeQuery(query)) {
					result.put(stateUID, SubformPermission.ALL);
				}
				return result;
			}

			DbQuery<DbTuple> query = builder.createQuery(DbTuple.class);
			DbFrom t = query.from(E.ROLESUBFORM);
			query.multiselect(t.basePk(),
				t.baseColumn(E.ROLESUBFORM.state),
				t.baseColumn(E.ROLESUBFORM.create),
				t.baseColumn(E.ROLESUBFORM.delete));
			query.where(builder.and(
				t.baseColumn(E.ROLESUBFORM.role).in(getRoleUids()),
				builder.equalValue(t.baseColumn(E.ROLESUBFORM.entity), entityUID)));

			Map<UID, UID> mpRoleSf2StateUids = new HashMap<>();

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
				UID roleSfUID = tuple.get(0, UID.class);
				UID stateUID = tuple.get(1, UID.class);
				Boolean create = tuple.get(2, Boolean.class);
				Boolean delete = tuple.get(3, Boolean.class);

				mpRoleSf2StateUids.put(roleSfUID, stateUID);

				SubformPermission permission = new SubformPermission(create, delete);

				result.put(stateUID, permission.max(result.get(stateUID)));
			}

			query = builder.createQuery(DbTuple.class);
			t = query.from(E.ROLESUBFORMGROUP);
			query.multiselect(
					t.baseColumn(E.ROLESUBFORMGROUP.rolesubform),
					t.baseColumn(E.ROLESUBFORMGROUP.group),
					t.baseColumn(E.ROLESUBFORMGROUP.readwrite));
			query.where(t.baseColumn(E.ROLESUBFORMGROUP.rolesubform).in(mpRoleSf2StateUids.keySet()));

			for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
				UID roleSfUID = tuple.get(0, UID.class);
				UID groupUID = tuple.get(1, UID.class);
				Boolean readwrite = tuple.get(2, Boolean.class);

				Permission permission = Permission.get(readwrite);
				UID stateUID = mpRoleSf2StateUids.get(roleSfUID);

				SubformPermission subformPermission = result.get(stateUID);
				subformPermission.addGroupPermission(groupUID, permission);

			}

			return result;
		}

		public synchronized Map<UID, Permission> getAttributeGroupPermissions(UID attributeGroupUid) {
			final Map<UID, Permission> result = new HashMap<UID, Permission>();
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

			if (isSuperUser()) {
				DbQuery<UID> query = builder.createQuery(UID.class);
				DbFrom t = query.from(E.STATE);
				query.select(t.baseColumn(E.STATE));
				for (UID stateUID : dataBaseHelper.getDbAccess().executeQuery(query)) {
					result.put(stateUID, Permission.READWRITE);
				}
			} else {
				DbQuery<DbTuple> query = builder.createQuery(DbTuple.class);
				DbFrom t = query.from(E.ROLEATTRIBUTEGROUP);
				query.multiselect(
					t.baseColumn(E.ROLEATTRIBUTEGROUP.state),
					t.baseColumn(E.ROLEATTRIBUTEGROUP.readwrite));
				query.where(builder.and(
					t.baseColumn(E.ROLEATTRIBUTEGROUP.role).in(getRoleUids()),
					builder.equalValue(t.baseColumn(E.ROLEATTRIBUTEGROUP.attributegroup), attributeGroupUid)));
				for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
					UID stateUID = tuple.get(0, UID.class);
					Boolean readwrite = tuple.get(1, Boolean.class);
					Permission permission = Permission.get(readwrite);
					result.put(stateUID, permission.max(result.get(stateUID)));
				}
			}
			return result;
		}

		private Collection<CompulsorySearchFilter> readCompulsorySearchFilterUids() {
			if (isSuperUser())
				return Collections.emptySet();
			List<CompulsorySearchFilter> list = new ArrayList<CompulsorySearchFilter>();
			list.addAll(readCompulsorySearchFilterIdsImpl(
					E.SEARCHFILTERUSER, E.SEARCHFILTERUSER.searchfilter, E.SEARCHFILTERUSER.validFrom, E.SEARCHFILTERUSER.validUntil,
					E.SEARCHFILTERUSER.compulsoryFilter, E.SEARCHFILTERUSER.user, Collections.singleton(getUserUid())));
			list.addAll(readCompulsorySearchFilterIdsImpl(
					E.SEARCHFILTERROLE, E.SEARCHFILTERROLE.searchfilter, E.SEARCHFILTERROLE.validFrom, E.SEARCHFILTERROLE.validUntil,
					E.SEARCHFILTERROLE.compulsoryFilter, E.SEARCHFILTERROLE.role, getRoleUids()));
			return list;
		}

		private List<CompulsorySearchFilter> readCompulsorySearchFilterIdsImpl(EntityMeta<UID> subtable, 
				FieldMeta<UID> refcolumn, FieldMeta<Date> fromcolumn, FieldMeta<Date> untilcolumn, FieldMeta<Boolean> compulcolumn, FieldMeta<UID> condcolumn, 
				Collection<UID> conduids) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();

			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.SEARCHFILTER);
			DbFrom u = t.join(subtable, JoinType.INNER, refcolumn, "u");
			query.multiselect(
				t.baseColumn(E.SEARCHFILTER), t.baseColumn(E.SEARCHFILTER.entity),
				u.baseColumn(fromcolumn), u.baseColumn(untilcolumn));
			query.where(builder.and(
				builder.equalValue(u.baseColumn(compulcolumn), true),
				u.baseColumn(condcolumn).in(conduids)));
			query.distinct(true);
			return dataBaseHelper.getDbAccess().executeQuery(query, new Transformer<DbTuple, CompulsorySearchFilter>() {
				@Override
				public CompulsorySearchFilter transform(DbTuple t) {
					return new CompulsorySearchFilter(
						t.get(0, UID.class), t.get(1, UID.class), t.get(2, Date.class), t.get(3, Date.class));
				}
			});
		}

		private void readUserData() {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<DbTuple> query = builder.createTupleQuery();
			DbFrom t = query.from(E.USER);
			query.multiselect(
				t.baseColumn(E.USER),
				t.baseColumn(E.USER.superuser),
				t.baseColumn(E.USER.communicationAccountPhone));
			query.where(getNameCondition(query, t, E.USER.name));
			DbTuple tuple = CollectionUtils.getFirst(dataBaseHelper.getDbAccess().executeQuery(query));
			if (tuple != null) {
				userUid = tuple.get(0, UID.class);
				isSuperUser = Boolean.TRUE.equals(tuple.get(1, Boolean.class));
				communicationAccountPhone = tuple.get(E.USER.communicationAccountPhone);
			} else {
				// No user found
				userUid = null;
				isSuperUser = false;
				communicationAccountPhone = null;
			}
		}
		
		public synchronized Set<UID> getMandators() {
			if (mandatorUids == null) {
				mandatorUids = readMandators();
			}
			return mandatorUids;
		}
		
		private Set<UID> readMandators() {
			Transformer<MandatorVO, UID> toUidTransformer = new Transformer<MandatorVO, UID>() {
				@Override
				public UID transform(MandatorVO i) {
					return i.getUID();
				}
			};
			if (isSuperUser()) {
				return Collections.unmodifiableSet(CollectionUtils.transformIntoSet(getAllMandators(), toUidTransformer));
			} else {
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<UID> query = builder.createQuery(UID.class);
				DbFrom t = query.from(E.MANDATOR_USER);
				query.select(t.baseColumn(E.MANDATOR_USER.mandator));
				query.where(builder.equalValue(t.baseColumn(E.MANDATOR_USER.user), getUserUid()));
				Set<UID> result = new HashSet<UID>();
				for (UID mandatorUID : dataBaseHelper.getDbAccess().executeQuery(query)) {
					result.add(mandatorUID);
					addChildMandators(result, mandatorUID);
				}
				return Collections.unmodifiableSet(result);
			}
		}
		
		private void addChildMandators(Set<UID> result, UID parent) {
			for (UID child : getChildMandators(parent)) {
				result.add(child);
				addChildMandators(result, child);
			}
		}
	}	// class UserRights


	/**
	 * defines a unique key to get the permission for an attributegroup
	 */
	private static class UserAttributeGroup {
		private String sUserName;
		private UID attributeGroupUID;
		private UID mandatorUID;

		UserAttributeGroup(String sUserName, UID attributeGroupUID, UID mandatorUID) {
			this.sUserName = sUserName;
			this.attributeGroupUID = attributeGroupUID;
			this.mandatorUID = mandatorUID;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || (this.getClass() != o.getClass())) {
				return false;
			}
			final UserAttributeGroup that = (UserAttributeGroup) o;
			return LangUtils.equal(this.sUserName, that.sUserName) && LangUtils.equal(this.attributeGroupUID, that.attributeGroupUID) && LangUtils.equal(this.mandatorUID, that.mandatorUID);
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(this.sUserName) ^ LangUtils.hashCode(this.attributeGroupUID) ^ LangUtils.hash(this.mandatorUID);
		}

	}	// class UserAttributeGroup

	/**
	 * defines a unique key to get the permission for a subform used in an entity
	 */
	private static class UserSubForm {
		private String sUserName;
		private UID entityUID;
		private UID mandatorUID;

		UserSubForm(String sUserName, UID entityUID, UID mandatorUID) {
			this.sUserName = sUserName;
			this.entityUID = entityUID;
			this.mandatorUID = mandatorUID;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || (this.getClass() != o.getClass())) {
				return false;
			}
			final UserSubForm that = (UserSubForm) o;
			return LangUtils.equal(this.sUserName, that.sUserName) && LangUtils.equal(this.entityUID, that.entityUID) && LangUtils.equal(this.mandatorUID, that.mandatorUID);
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(this.sUserName) ^ LangUtils.hashCode(this.entityUID) ^ LangUtils.hashCode(this.mandatorUID);
		}
	} // class UserSubForm

	private static class CompulsorySearchFilter {
		private UID uid;
		private UID entityUID;
		private Date validFrom;
		private Date validUntil;

		CompulsorySearchFilter(UID uid, UID entityUID, Date validFrom, Date validUntil) {
			this.entityUID = entityUID;
			this.uid = uid;
			this.validFrom = validFrom;
			this.validUntil = validUntil;
		}

		boolean isValid(Date date) {
			return (validFrom == null || validFrom.before(date))
				&& (validUntil == null || validUntil.after(date));
		}
	}

	public static SecurityCache getInstance() {
		return INSTANCE;
	}

	SecurityCache() {
		INSTANCE = this;
	}

	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}
	
	@Autowired
	void setMetaProvider(MetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}
	
	@Autowired
	void setStateModelUsagesCache(StateModelUsagesCache stateModelUsagesCache) {
		this.stateModelUsagesCache = stateModelUsagesCache;
	}
	
	private StateCache getStateCache() {
		if (stateCache == null) {
			stateCache = StateCache.getInstance();
		}
		return stateCache;
	}

	public Set<String> getAllowedActions(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAllowedActions();
	}
	
	public Set<UID> getAllowedCustomActions(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAllowedCustomActions();
	}

	public ModulePermissions getModulePermissions(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getModulePermissions();
	}

	public MasterDataPermissions getMasterDataPermissions(String sUserName, UID mandator){
		return getUserRights(sUserName, mandator).getMasterDataPermissions();
	}

	@Override
	public boolean isActionAllowed(final String sUserName, final String action, UID mandator) {
		return getUserRights(sUserName, mandator).getAllowedActions().contains(action);
	}

	@Override
	public boolean isReadAllowedForEntity(String user, UID entityUID, UID mandator) {
		return isReadAllowedForMasterData(user, entityUID, mandator) || isReadAllowedForModule(user, entityUID, mandator);
	}

	public boolean isReadAllowedForModule(String sUserName, UID entityUID, UID mandator) {
		return getUserRights(sUserName, mandator).isSuperUser() || ModulePermission.includesReading(this.getModulePermissions(sUserName, mandator).getMaxPermissionForGenericObject(entityUID));
	}

	public boolean isReadAllowedForMasterData(String sUserName, UID entityUID, UID mandator) {
		if (E.REPORTEXECUTION.checkEntityUID(entityUID)) {
			return getUserRights(sUserName, mandator).getAllowedActions().contains(Actions.ACTION_EXECUTE_REPORTS);
		}
		return getUserRights(sUserName, null).isSuperUser() 
				|| MasterDataPermission.includesReading(this.getMasterDataPermissions(sUserName, mandator).get(entityUID));
	}

	public boolean isNewAllowedForModule(String sUserName, UID entityUID, UID mandator) {
		Boolean bNewAllowed = getModulePermissions(sUserName, mandator).getNewAllowedByModuleUid().get(entityUID);
		return bNewAllowed != null ? bNewAllowed : false;
	}

	public boolean isWriteAllowedForModule(String sUserName, UID entityUID, UID mandator) {
		return ModulePermission.includesWriting(
				getModulePermissions(sUserName, mandator).getMaxPermissionForGenericObject(entityUID));
	}

	public boolean isWriteAllowedForMasterData(String sUserName, UID entityUID, UID mandator) {
		return MasterDataPermission.includesWriting(getMasterDataPermissions(sUserName, mandator).get(entityUID));
	}

	public boolean isDeleteAllowedForModule(String sUserName, UID entityUID, boolean bPhysically, UID mandator) {
		if (getUserRights(sUserName, mandator).isSuperUser())
			return true;
		final ModulePermission modulepermission = getModulePermissions(sUserName, mandator).getMaxPermissionForGenericObject(entityUID);
		return bPhysically ?
				ModulePermission.includesDeletingPhysically(modulepermission) :
				ModulePermission.includesDeletingLogically(modulepermission);

	}

	public boolean isDeleteAllowedForMasterData(String sUserName, UID entityUID, UID mandator) {
		return getUserRights(sUserName, mandator).isSuperUser() 
				|| MasterDataPermission.includesDeleting(getMasterDataPermissions(sUserName, mandator).get(entityUID));
	}

	public boolean isSuperUser(String sUserName) {
		return getUserRights(sUserName, null).isSuperUser();
	}
	
	public boolean isMaintenanceUser(String sUserName) {
		return getAllowedActions(sUserName, null).contains(Actions.ACTION_MAINTENANCE_MODE);
	}
	
	public boolean isNucletExportUser(String sUserName) {
		return getAllowedActions(sUserName, null).contains(Actions.ACTION_NUCLET_EXPORT);
	}
	
	public boolean isNucletImportUser(String sUserName) {
		return getAllowedActions(sUserName, null).contains(Actions.ACTION_NUCLET_IMPORT);
	}
	
	public Boolean isNucletAssignUser(String sUserName) {
		return getAllowedActions(sUserName, null).contains(Actions.ACTION_NUCLET_ASSIGN);
	}

	/**
	 * @param sUserName
	 * @return Set&lt;UID&gt; the ids of all modules 
	 * 		that can be read by the user with the given name.
	 */
	public Set<UID> getReadableModuleUids(String sUserName, UID mandator) {
		final Set<UID> result = new HashSet<UID>();
		for (Entry<UID, ModulePermission> userRights : this.getModulePermissions(sUserName, mandator).getEntries()) {
			if (ModulePermission.includesReading(userRights.getValue())) {
				result.add(userRights.getKey());
			}
		}
		return result;
	}

	public Collection<UID> getTransitionUids(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getTransitionUids();
	}

	/**
	 * Get the ids of all reports/forms the user has the right to read.
	 * Read rights are given when the user
	 * - has created the report/form himself or
	 * - has access to it via the roles he belongs to.
	 * 
	 * @param sUserName
	 * @return Map&lt;ReportType,Collection&lt;UID&gt;&gt; contains a map of all reports/forms 
	 * 		that may be read by the given user.
	 */
	public Map<ReportType,Collection<UID>> getReadableReports(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getReadableReports();
	}

	/**
	 * Get the ids of all custom rest rules the user is allowed to execute.
	 * - has access to it via the roles he belongs to. Superuser gets any rule that has an assignment to roles.
	 * If a rule isn't assign to a rule, it won't be delivered for anybody by this methode.
	 *
	 * @param sUserName
	 * @param mandator
	 * @return Map&lt;UID, String&gt; contains a map of all custom rest rules
	 * 		that may be allowed for the given user. UID and Name in a map
	 */
	public Map<UID, String> getAllowedCustomRestRules(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAllowedCustomRestRules();
	}

	/**
	 * Get the ids of all generations the user has assigned.
	 * - has access to it via the roles he belongs to. Superuser gets all.
	 * 
	 * @param sUserName
	 * @return Collection&lt;UID&gt; contains a map of all generations 
	 * 		that may be assigned to the given user.
	 */
	public Collection<UID> getAssignedGenerations(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAssignedGeneratorUids();
	}

	/**
	 * Get the ids of all recordgrants the user has assigned.
	 * - has access to it via the roles he belongs to. Superuser gets all.
	 * 
	 * @param sUserName
	 * @return Collection&lt;UID&gt; contains a map of all recordgrants 
	 * 		that may be assigned to the given user.
	 */
	public Collection<UID> getAssignedRecordgrants(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getAssignedRecordgrantUids();
	}

	/**
	 * Get the ids of all reports/forms the user has the right to write (and read).
	 * Write rights are given when the user
	 * - has created the report/form himself or
	 * - has writable access to it via the roles he belongs to.
	 * 
	 * @param sUserName
	 * @return Collection&lt;UID&gt; contains the ids of all reports/forms 
	 * 		that may be (read and) written by the given user.
	 */
	public Collection<UID> getWritableReportIds(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getWritableReportUids();
	}

	/**
	 * Get the ids of all datasources the user has the right to read.
	 * A user has right to read a datasource, when
	 * - the user has created it himself or
	 * - it is used in a report the user has access to (via the user's roles)
	 * 
	 * @param sUserName
	 * @return Collection&lt;UID&gt; contains the ids of all datasources 
	 * 		that may be read by the given user.
	 */
	public Collection<UID> getReadableDataSourceIds(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getReadableDataSourceUids();
	}

	/**
	 * Get the ids of all datasources the user has the right to read.
	 * A user has right to read a datasource, when
	 * - the user has created it himself or
	 * - it is used in a report the user has access to (via the user's roles)
	 * 
	 * @param sUserName
	 * @return Collection&lt;Integer&gt; contains the ids of all datasources 
	 * 		that may be read by the given user.
	 */
	public Collection<UID> getReadableDataSources(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getReadableDataSourceUids();
	}

	/**
	 * Get the ids of all datasources the user has the right to write (and read).
	 * A user has right to write a datasource when
	 * - the user created it himself or
	 * - it is used in a report the user has writable access to (via the user's roles)
	 * @param sUserName
	 * @return Collection&lt;Integer&gt; contains the ids of all datasources 
	 * 		that may be (read and) written by the given user.
	 */
	public Collection<UID> getWritableDataSourceUids(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getWritableDataSourceUids();
	}

	/**
	 * @param sUserName
	 * @param entityUID
	 * @param attributeGroupUID
	 * @return maps a state id to a permission.
	 */
	public Map<UID, Permission> getAttributeGroup(String sUserName, UID entityUID, UID attributeGroupUID, UID mandator) {
		final UserAttributeGroup userattrgroup = new UserAttributeGroup(sUserName, attributeGroupUID, mandator);
		if (!mpAttributeGroups.containsKey(userattrgroup)) {
			mpAttributeGroups.put(userattrgroup, newAttributeGroup(sUserName, entityUID, attributeGroupUID, mandator));
		}

		/* 
		 * �todo It's not necessary to return a copy here - an unmodifiable wrapper would be better (see below).
		 * We want to go sure for now and optimize later. 
		 */
		return new HashMap<UID, Permission>(mpAttributeGroups.get(userattrgroup));
	}

	/**
	 * @param sUserName
	 * @param entityUID
	 * @return maps a state id to a permission
	 */
	public Map<UID, SubformPermission> getSubForm(String sUserName, UID entityUID, UID mandator) {
		final UserSubForm usersubform = new UserSubForm(sUserName, entityUID, mandator);
		if(!mpSubForms.containsKey(usersubform)) {
			mpSubForms.put(usersubform, getUserRights(sUserName, mandator).readSubFormPermissions(entityUID));
		}
		return new HashMap<>(mpSubForms.get(usersubform));
	}

	private Map<UID, Permission> newAttributeGroup(String sUserName, UID entityUID, UID attributeGroupUid, UID mandator) {
		if (SF.SYSTEMIDENTIFIER.getMetaData(entityUID).getFieldGroup().equals(attributeGroupUid)) {
			Map<UID, Permission> systemFieldPermissions = new HashMap<UID, Permission>();

			MasterDataFacadeLocal mdFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
			for (MasterDataVO<UID> state : mdFacade.getMasterData(E.STATE, null)) {
				systemFieldPermissions.put(state.getId(), Permission.READONLY);
			}

			return systemFieldPermissions;
		} else if (SF.PROCESS.getMetaData(entityUID).getFieldGroup().equals(attributeGroupUid)) {
			Map<UID, Permission> systemFieldPermissions = new HashMap<UID, Permission>();

			MasterDataFacadeLocal mdFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
			for (MasterDataVO<UID> state : mdFacade.getMasterData(E.STATE, null)) {
				systemFieldPermissions.put(state.getId(), Permission.READWRITE);
			}

			return systemFieldPermissions;
		}


		return getUserRights(sUserName, mandator).getAttributeGroupPermissions(attributeGroupUid);
	}

	public Set<UID> getCompulsorySearchFilterUids(String userName, UID entityUID, UID mandator) {
		return getUserRights(userName, mandator).getCompulsorySearchFilterUids(entityUID);
	}
	
	@ManagedOperation(description="invalidate/clear cache, fill it again, and refresh menus")
	public void invalidate() {
		invalidate(true);
		SpringApplicationContextHolder.getBean(TasklistFacadeLocal.class).invalidateCaches();
	}

	@CacheEvict(value="userData", allEntries=true)
	public void invalidate(boolean refreshMenus) {
		if (TransactionSynchronizationManager.getSynchronizations().contains(invalidate)) {
			return;
		}
		
		notifyUser(null, refreshMenus);
		TransactionSynchronizationManager.registerSynchronization(invalidate);
	}
	
	@CacheEvict(value="userData", key="#p0")
	public void invalidate(final String username, boolean refreshMenus, String customUsage) {
		if (username != null) {
			//Note that this method is explicitly called by MasterDataFacadeHelper.invalidateCaches, when the User is altered in T_MD_USER.
			//NUCLOS-3795
			final boolean removeSessionContextsForUser = !NuclosConstants.WEB_LAYOUT_CUSTOM_USAGE.equals(customUsage);
			if (removeSessionContextsForUser) {
				removeSessionContextsForUser(username, null);				
			} 
			
			// If we find the invalidate transaction synchronization the complete
			// cache is invalidated. Hence, there is no need to invalidate for 
			// a individual user. (tp)
			if (TransactionSynchronizationManager.getSynchronizations().contains(invalidate)) {
				return;
			}
			
			notifyUser(username, refreshMenus);
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public synchronized void afterCommit() {
					LOG.info("afterCommit: Invalidating security cache for user {} ...", username);
					if (mpUserRights.containsKey(username)) {
						mpUserRights.remove(username);
					}
					for (UserSubForm usf : new ArrayList<UserSubForm>(mpSubForms.keySet())) {
						if (username.equals(usf.sUserName)) {
							mpSubForms.remove(usf);
						}
					}
					for (UserAttributeGroup uag : new ArrayList<UserAttributeGroup>(mpAttributeGroups.keySet())) {
						if (username.equals(uag.sUserName)) {
							mpAttributeGroups.remove(uag);
						}
					}
					
					if (removeSessionContextsForUser) {
						setAuthentication2NullForUser(username, null);
					}
					
					mapFatClientContexts.remove(username);
					clearMapRoleNames();
				}
			});
		}
		else {
			invalidateCache(true, true);
		}
	}
	
	private void clearMapRoleNames() {
		synchronized (mapRoleNames) {
			mapRoleNamesFilled = false;
			mapRoleNames.clear();
		}
	}

	@Override
	public UID getUserUid(String sUserName){
		UserRights userRights = getUserRights(sUserName, null);
		return userRights != null ? userRights.getUserUid() : null;
	}

	public Set<UID> getUserRoles(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getRoleUids();
	}

	public Set<UID> getUserRolesForUid(UID userUID, UID mandator) {
		return getUserRoles(getUserName(userUID), mandator);
	}
	
	public String getUserName(UID userUID) {
		String sUserName = mpUserName.get(userUID);
		if (sUserName == null) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<String> query = builder.createQuery(String.class);
			DbFrom t = query.from(E.USER);
			query.select(t.baseColumn(E.USER.name));
			query.where(builder.equalValue(t.basePk(), userUID));
			sUserName = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			mpUserName.put(userUID, sUserName);
		}
		return sUserName;
	}
	
	public Map<UID, String> getAllRolesWithName() {
		synchronized (mapRoleNames) {
			if (!mapRoleNamesFilled) {
				mapRoleNames.clear();
				DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
				DbQuery<DbTuple> query = builder.createTupleQuery();
				DbFrom<UID> t = query.from(E.ROLE, SystemFields.BASE_ALIAS);
				query.multiselect(t.baseColumn(E.ROLE.getPk()), t.baseColumn(E.ROLE.name));
				List<DbTuple> dbRoles = dataBaseHelper.getDbAccess().executeQuery(query);
				for (DbTuple dbRole : dbRoles) {
					mapRoleNames.put(dbRole.get(0, UID.class), dbRole.get(1, String.class));
				}
				mapRoleNamesFilled = true;
			}
			return Collections.unmodifiableMap(mapRoleNames);
		}
	}
	
	public Map<UID, String> getUserRolesWithName(String sUserName, UID mandator) {
		Map<UID, String> result = new HashMap<UID, String>();

		Set<UID> roles = getUserRoles(sUserName, mandator);
		if (roles.isEmpty()) {
			return result;
		}
				
		Map<UID, String> allRolesWithName = getAllRolesWithName();
		for (UID role : roles) {
			if (allRolesWithName.containsKey(role)) {
				result.put(role, allRolesWithName.get(role));
			}
		}
		
		return result;
	}

	public String findOtherSimilarRoleName(UID roleUid, String roleName) {
		String roleNameEscaped = NuclosEntityValidator.escapeJavaIdentifier(roleName);
		Map<UID, String> allRoles = getAllRolesWithName();
		for (UID uid : allRoles.keySet()) {
			if (roleUid != null && roleUid.equals(uid)) {
				continue;
			}
			String otherRoleName = allRoles.get(uid);
			if (LangUtils.equal(roleNameEscaped, NuclosEntityValidator.escapeJavaIdentifier(otherRoleName))) {
				return otherRoleName;
			}
		}
		return null;
	}

	public Set<UID> getDynamicTasklists(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getDynamicTasklistUids();
	}
	
	public Set<UID> getDynamicTasklistDatasources(String sUserName, UID mandator) {
		return getUserRights(sUserName, mandator).getDynamicTasklistDatasourceUids();
	}

	private UserRights getUserRights(String sUserName, UID mandator) {
		Map<UID, UserRights> perMandator = mpUserRights.get(sUserName);
		if (perMandator == null) {
			perMandator = new ConcurrentHashMap<UID, SecurityCache.UserRights>();
			mpUserRights.put(sUserName, perMandator);
		}
		UID mandatorCacheKey = mandator == null ? UID.UID_NULL : mandator;
		UserRights result = perMandator.get(mandatorCacheKey);
		if (result == null) {
			result = new UserRights(sUserName, mandator);
			perMandator.put(mandatorCacheKey, result);
		}
		return result;
	}

	/**
	 * get the permission for an attribute within the given status numeral
	 */
	public Permission getAttributePermission(UID entityUID, UID attributeUID, UID stateUID) {
		AttributePermissionKey attributePermissionKey = new AttributePermissionKey(entityUID, attributeUID, stateUID);
		if (!mpAttributePermission.containsKey(attributePermissionKey)) {
			// TODO: Get rid of SecurityFacade here to break dependency cycle
			Permission permission = SpringApplicationContextHolder.getBean(SecurityFacadeBean.class)
					.getAttributePermission(entityUID, attributeUID, stateUID);
			if (permission == null) {
				permission = Permission.NONE;
			}
			mpAttributePermission.put(attributePermissionKey, permission);
		}
		final Permission result = mpAttributePermission.get(attributePermissionKey);
		if (Permission.NONE.equals(result)) {
			return null;
		}
		return result;
	}

	@ManagedAttribute
	public int getAttributeGroupsCount() {
		return mpAttributeGroups != null ? mpAttributeGroups.size() : 0;
	}

	@ManagedAttribute
	public int getSubFormCount() {
		return mpSubForms != null ? mpSubForms.size() : 0;
	}

	@ManagedAttribute
	public int getUserRightsCount() {
		return mpUserRights != null ? mpUserRights.size() : 0;
	}

	/**
	 * notifies clients that the leased object meta data has changed, so they can invalidate their local caches.
	 */
	private void notifyUser(String username, boolean refreshMenus) {
		LOG.debug("JMS send: notify user {} that security data has changed: {}",
		          username, this);
		NuclosJMSUtils.sendOnceAfterCommitDelayed(new NotifyObject(username, refreshMenus), JMSConstants.TOPICNAME_SECURITYCACHE);
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.SECURITY_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);			
	}

	@Override
	public void registerCache() {
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public void invalidateCache(boolean notifyClients, boolean notifyClusterCloud) {		
		if(notifyClusterCloud) {
			notifyClusterCloud();
		}
		invalidate(true);
	}
	
	private Collection<String> searchSessionUIDForUser(String username) {
		Collection<String> retVal = new HashSet<String>();
		for (String suid : mapSessionContexts.keySet()) {
			SessionContext sc = mapSessionContexts.get(suid);
			if (sc.getUsername().equals(username)) {
				retVal.add(sc.getJSessionId());
			}
		}
		return retVal;
	}

	public SessionContext setSessionContext(
			final Authentication auth,
			final Locale locale,
			final String jSessionId,
			final Long sessionId
	) {
		final SessionContext sessionContext = new SessionContext(
				sessionId,
				jSessionId,
				auth,
				locale
		);
		return setSessionContext(jSessionId, sessionContext);
	}

	public SessionContext setSessionContext(
			final String jSessionId,
			final SessionContext sessionContext
	) {
		mapSessionContexts.put(jSessionId, sessionContext);
		return sessionContext;
	}

	public SessionContext getSessionContext(String webSessionUID) {
		return mapSessionContexts.get(webSessionUID);
	}
	
	public void removeSessionContext(String webSessionUID) {
		mapSessionContexts.remove(webSessionUID);
	}
	
	public void removeSessionContextsForUser(String username, String webSessionID2Keep) {
		Collection<String> sessions = searchSessionUIDForUser(username);
		sessions.remove(webSessionID2Keep);
		for (String s : sessions) {
			mapSessionContexts.remove(s);
		}
	}
	
	public void setAuthentication2NullForUser(String username, String webSessionID2Keep) {
		for (String webSessionId : mapSessionContexts.keySet()) {
			SessionContext context = mapSessionContexts.get(webSessionId);
			if (context.getJSessionId().equals(webSessionID2Keep)) {
				continue;
			}
			if (username.equals(context.getUsername())) {
				context.setAuthentication(null);
			}
		}
	}

	public void setAuthentificationFromFatClientContext(String username, String password, Authentication auth) {
		Map<String, Authentication> mp2 = new HashMap<String, Authentication>();
		mp2.put(password, auth);
		mapFatClientContexts.put(username, mp2);
	}

	public void removeAuthentificationFromFatClientContext(String username) {
		mapFatClientContexts.remove(username);
	}

	public Authentication getAuthentificationFromFatClientContext(String username, String password) {
		Map<String, Authentication> mp2 = mapFatClientContexts.get(username);
		if (mp2 != null) {
			return mp2.get(password);
		}
		return null;
	}
	
	public List<MandatorVO> getAssignedMandators(String sUserName) {
		fillMandatorsIfNecessary();
		List<MandatorVO> result = new ArrayList<MandatorVO>();
		Set<UID> grantedMandators = getUserRights(sUserName, null).getMandators();
		for (MandatorVO mandator : lstMandators) {
			if (grantedMandators.contains(mandator.getUID())) {
				result.add(mandator);
			}
		}
		return Collections.unmodifiableList(result);
	}
	
	public boolean isMandatorPresent() {
		fillMandatorsIfNecessary();
		return !lstMandators.isEmpty();
	}
	
	public List<MandatorLevelVO> getAllMandatorLevels() {
		fillMandatorsIfNecessary();
		return Collections.unmodifiableList(lstMandatorLevels);
	}
	
	public List<MandatorVO> getAllMandators() {
		fillMandatorsIfNecessary();
		return Collections.unmodifiableList(lstMandators);
	}
	
	public Set<UID> getChildMandators(UID parentUID) {
		fillMandatorsIfNecessary();
		Set<UID> result = mandatorChildren.get(parentUID);
		if (result == null) {
			result = Collections.unmodifiableSet(new HashSet<UID>());
			mandatorChildren.put(parentUID, result);
		}
		return result;
	}
	
	public MandatorVO getMandator(UID mandatorUID) {
		fillMandatorsIfNecessary();
		return mapMandators.get(mandatorUID);
	}
	
	public MandatorLevelVO getMandatorLevel(UID levelUID) {
		fillMandatorsIfNecessary();
		return mapLevels.get(levelUID);
	}
	
	public List<MandatorVO> getMandatorsByLevel(UID levelUID) {
		fillMandatorsIfNecessary();
		List<MandatorVO> result = mandatorsByLevel.get(levelUID);
		if (result == null) {
			result = Collections.unmodifiableList(new ArrayList<MandatorVO>());
			mandatorsByLevel.put(levelUID, result);
		}
		return result;
	}
	
	/**
	 * 
	 * @param mandatorUID
	 * @return the accessible mandators for the given mandator.
	 * 		accessible= all childrens + direct parents up to root
	 */
	public Set<UID> getAccessibleMandators(UID mandatorUID) {
		fillMandatorsIfNecessary();
		return mandatorAccessible.get(mandatorUID==null?UID.UID_NULL:mandatorUID);
	}
	
	private void fillMandatorsIfNecessary() {
		synchronized (lstMandators) {
			if (!mandatorsloaded) {
				lstMandators.clear();
				lstMandatorLevels.clear();
				mandatorChildren.clear();
				mandatorAccessible.clear();
				mapMandators.clear();
				mandatorsByLevel.clear();
				mapLevels.clear();
				readAllMandators(lstMandators, mandatorChildren, mapMandators, mandatorsByLevel, new HashSet<UID>(), null);
				for (Map.Entry<UID, Set<UID>> entry : mandatorChildren.entrySet()) {
					entry.setValue(Collections.unmodifiableSet(entry.getValue()));
				}
				for (Map.Entry<UID, List<MandatorVO>> entry : mandatorsByLevel.entrySet()) {
					Collections.sort(entry.getValue());
					entry.setValue(Collections.unmodifiableList(entry.getValue()));
				}
				readAllMandatorLevels(mapLevels);
				for (MandatorVO mandator : lstMandators) {
					Set<UID> set = new HashSet<UID>();
					set.add(mandator.getUID());
					addChildMandators(set, mandator.getUID(), mandatorChildren);
					addParentMandators(set, mandator.getUID(), mapMandators);
					mandatorAccessible.put(mandator.getUID(), Collections.unmodifiableSet(set));
				}
				lstMandatorLevels.addAll(mapLevels.values());
				MandatorLevelVO.sort(lstMandatorLevels);
				mandatorAccessible.put(UID.UID_NULL, Collections.unmodifiableSet(CollectionUtils.transformIntoSet(lstMandators, new MandatorVO.ToUid())));
				Collections.sort(lstMandators);
				mandatorsloaded = true;
				cleanDbCache();
				fillDbCache();
			}
		}
	}
	
	private static void addChildMandators(Set<UID> result, UID parent, Map<UID, Set<UID>> mandatorChildren) {
		if (mandatorChildren.containsKey(parent)) {
			for (UID child : mandatorChildren.get(parent)) {
				result.add(child);
				addChildMandators(result, child, mandatorChildren);
			}
		}
	}
	
	private static void addParentMandators(Set<UID> result, UID child, Map<UID, MandatorVO> mapMandators) {
		MandatorVO childMandator = mapMandators.get(child);
		if (childMandator.getParentUID() != null) {
			result.add(childMandator.getParentUID());
			addParentMandators(result, childMandator.getParentUID(), mapMandators);
		}
	}
	
	private void readAllMandatorLevels( Map<UID, MandatorLevelVO> mapLevels) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.MANDATOR_LEVEL);
		query.multiselect(
			t.baseColumn(E.MANDATOR_LEVEL),
			t.baseColumn(E.MANDATOR_LEVEL.name),
			t.baseColumn(E.MANDATOR_LEVEL.parentLevel),
			t.baseColumn(E.MANDATOR_LEVEL.showName));
		for (DbTuple child : dataBaseHelper.getDbAccess().executeQuery(query)) {
			UID pk = child.get(E.MANDATOR_LEVEL.getPk());
			String name = child.get(E.MANDATOR_LEVEL.name);
			UID parent = child.get(E.MANDATOR_LEVEL.parentLevel);
			Boolean showName = child.get(E.MANDATOR_LEVEL.showName);
			MandatorLevelVO vo = new MandatorLevelVO(pk, name, parent, showName);
			mapLevels.put(pk, vo);
		}
	}
	
	private void readAllMandators(
			Collection<MandatorVO> result, 
			Map<UID, Set<UID>> mandatorChildren, 
			Map<UID, MandatorVO> mapMandators, 
			Map<UID, List<MandatorVO>> mandatorsByLevel, 
			Set<UID> readPk, UID parentUID) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.MANDATOR);
		query.multiselect(
			t.baseColumn(E.MANDATOR),
			t.baseColumn(E.MANDATOR.name),
			t.baseColumn(E.MANDATOR.path),
			t.baseColumn(E.MANDATOR.parentMandator),
			t.baseColumn(E.MANDATOR.level),
			t.baseColumn(E.MANDATOR.color));
		if (parentUID == null) {
			query.where(builder.isNull(E.MANDATOR.parentMandator));
		} else {
			query.where(builder.equalValue(t.baseColumn(E.MANDATOR.parentMandator), parentUID));
		}
		for (DbTuple child : dataBaseHelper.getDbAccess().executeQuery(query)) {
			UID pk = child.get(E.MANDATOR.getPk());
			String name = child.get(E.MANDATOR.name);
			String path = child.get(E.MANDATOR.path);
			UID parent = child.get(E.MANDATOR.parentMandator);
			UID level = child.get(E.MANDATOR.level);
			String color = child.get(E.MANDATOR.color);
			if (readPk.contains(pk)) {
				continue;
			}
			readPk.add(pk);
			MandatorVO vo = new MandatorVO(pk, name, path, parent, level, color);
			result.add(vo);
			mapMandators.put(pk, vo);
			
			final UID key = parentUID==null?UID.UID_NULL:parentUID;
			if (!mandatorChildren.containsKey(key)) {
				mandatorChildren.put(key, new HashSet<UID>());
			}
			if (!mandatorsByLevel.containsKey(level)) {
				mandatorsByLevel.put(level, new ArrayList<MandatorVO>());
			}
			mandatorChildren.get(key).add(pk);
			mandatorsByLevel.get(level).add(vo);
			
			readAllMandators(result, mandatorChildren, mapMandators, mandatorsByLevel, readPk, pk);
		}
	}
	
	public void checkMandatorLoginPermission(String sUserName, UID mandatorUID) throws CommonPermissionException {
		if (isMandatorPresent()) {
			if (mandatorUID != null) {
				if (!getUserRights(sUserName, null).getMandators().contains(mandatorUID)) {
					MandatorVO mandator = getMandator(mandatorUID);
					if (mandator == null) {
						throw new CommonPermissionException("mandator.permission.not.found");
					} else {
						throw new CommonPermissionException("mandator.permission.not.assigned");
					}
				}
			} else {
				if (!isSuperUser(sUserName)) {
					throw new CommonPermissionException("mandator.permission.no.superuser");
				}
			}
		}
	}
	
	@ManagedAttribute(description="get the size (number of users/UserRights) of mpUserRights")
	public int getNumberOfUserRightsInCache() {
		return mpUserRights.size();
	}
	
	@ManagedAttribute(description="get the minimal size of MasterDataPermissions in mpUserRights")
	public int getMasterDataPermissionsMinSize() {
		return masterDataPermissionsMinSize;
	}

	@ManagedAttribute(description="get the maximal size of MasterDataPermissions in mpUserRights")
	public int getMasterDataPermissionsMaxSize() {
		return masterDataPermissionsMaxSize;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	private synchronized void cleanDbCache() {
		dataBaseHelper.execute(new DbDeleteStatement<Long>(E.MANDATOR_ACCESSIBLE, new DbMap()));
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	private synchronized void fillDbCache() {
		int pk = 1000;
		for (MandatorVO mandator : lstMandators) {
			for (UID accessible : mandatorAccessible.get(mandator.getUID())) {
				DbMap values = new DbMap();
				values.put(E.MANDATOR_ACCESSIBLE.getPk(), new Long(pk++));
				values.put(E.MANDATOR_ACCESSIBLE.mandator, mandator.getUID());
				values.put(E.MANDATOR_ACCESSIBLE.accessible, accessible);
				values.put(SF.CREATEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
				values.put(SF.CREATEDBY, "securityCache");
				values.put(SF.CHANGEDAT, values.get(SF.CREATEDAT));
				values.put(SF.CHANGEDBY, values.get(SF.CREATEDBY));
				values.put(SF.VERSION, 1);
				dataBaseHelper.execute(new DbInsertStatement<Long>(E.MANDATOR_ACCESSIBLE, values));
			}
		}
	}
	
	public UID getUserCommunicationPhoneAccount(String sUserName) {
		return getUserRights(sUserName, null).getCommunicationAccountPhone();
	}

	/**
	 * TODO: Do not use flag arguments (bWrite) - split this into 2 methods instead (isReadAllowedForField / isWriteAllowedForField)
	 */
	@Deprecated
	public boolean isReadOrWriteAllowedForField(
			FieldMeta<?> fm,
			UsageCriteria usage,
			boolean bWrite,
			String user,
			UID mandator
	) {

		//TODO: For Fields without state there aren't any right checks for fields yet, so we return always true.
		if (usage.getStatusUID() == null) {
			return true;
		}

		Map<UID, Permission> mpPermission = getAttributeGroup(user, usage.getEntityUID(), fm.getFieldGroup(), mandator);
		Permission perm = mpPermission.get(usage.getStatusUID());
		if (perm == null) {
			return false;
		}
		return bWrite ? perm.includesWriting() : perm.includesReading();
	}

	/**
	 * Checks if the given field is readable either directly or indirectly via a stringified reference.
	 *
	 * TODO: Cache result
	 */
	public boolean isReadAllowedForFieldViaStringifiedReference(FieldMeta<?> fieldMeta, String user, UID mandator) {
		// Direct read allowed?
		boolean readAllowed = isReadAllowedForEntity(user, fieldMeta.getEntity(), mandator)
				&& isReadOrWriteAllowedForField(
				fieldMeta,
				new UsageCriteria(fieldMeta.getEntity(), null, null, null),
				false,
				user,
				mandator
		);

		if (readAllowed) {
			return true;
		}

		// Direct read not allowed -> check for implicit permission via stringified reference
		Collection<EntityMeta<?>> allEntities = metaProvider.getAllEntities();
		for (EntityMeta<?> entityMeta : allEntities) {

			// Find reference fields whose foreign entity is the entity of the field to check
			Collection<FieldMeta<?>> referenceFieldMetas = entityMeta.getFields().stream().filter(
					meta -> RigidUtils.equal(meta.getForeignEntity(), fieldMeta.getEntity())
			).collect(Collectors.toSet());

			for (FieldMeta<?> referenceFieldMeta : referenceFieldMetas) {
				boolean readAllowedForReferenceField = isReadOrWriteAllowedForField(
						referenceFieldMeta,
						new UsageCriteria(referenceFieldMeta.getEntity(), null, null, null),
						false,
						user,
						mandator);
				if (readAllowedForReferenceField) {
					// Check if the field to check is part of the stringified reference
					String stringifiedReference = referenceFieldMeta.getForeignEntityField();
					if (UID.parseAllUIDs(stringifiedReference).contains(fieldMeta.getUID())) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * TODO: Do not use flag arguments (bWrite) - split this into 2 methods instead (isReadAllowedForSubform / isWriteAllowedForSubform)
	 */
	@Deprecated
	public boolean isReadOrWriteAllowedForSubform(
			UID subform,
			UsageCriteria usage,
			boolean bWrite,
			String user,
			UID mandator
	) {
		SubformPermission perm = getSubformPermission(subform, usage, user, mandator);
		if (perm == null) {
			return false;
		}

		return bWrite ? perm.includesWriting() : perm.includesReading();
	}

	public SubformPermission getSubformPermission(UID subform, UsageCriteria usage, String user, UID mandator) {
		// No statemodel, all rights for subforms
		if (usage.getStatusUID() == null) {
			return SubformPermission.ALL;
		}

		Map<UID, SubformPermission> mpPermission = getSubForm(user, subform, mandator);
		return mpPermission.get(usage.getStatusUID());
	}

}
