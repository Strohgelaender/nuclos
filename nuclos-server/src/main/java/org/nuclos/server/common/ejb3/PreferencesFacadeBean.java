//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.StringUtils;
import org.nuclos.api.Settings;
import org.nuclos.api.SettingsImpl;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.GenericObjectMetaDataProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescription2.Frame;
import org.nuclos.common.WorkspaceDescription2.MutableContent;
import org.nuclos.common.WorkspaceDescription2.NestedContent;
import org.nuclos.common.WorkspaceDescription2.Split;
import org.nuclos.common.WorkspaceDescription2.Tab;
import org.nuclos.common.WorkspaceDescription2.Tabbed;
import org.nuclos.common.WorkspaceDescriptionDefaultsFactory;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.PreferenceVO;
import org.nuclos.common.preferences.Preference;
import org.nuclos.common.preferences.PreferenceShareVO;
import org.nuclos.common.preferences.PreferencesConverter;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.exception.WorkspaceException;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.valueobject.PreferencesVO;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.nuclet.IPreferenceProcessor;
import org.nuclos.server.dal.processor.nuclet.IWorkspaceProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbTableStatement;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.thoughtworks.xstream.XStream;

/**
 * Facade bean for storing userUID preferences.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class PreferencesFacadeBean extends NuclosFacadeBean implements PreferencesFacadeLocal, PreferencesFacadeRemote {
	
	private static final Logger LOG = LoggerFactory.getLogger(PreferencesFacadeBean.class);
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	/**
	 * new (web) preferences (grantable, nucletable, etc...)
	 */
	@Autowired
	private IPreferenceProcessor prefsProc;

	@Autowired
	private GenericObjectMetaDataProvider goMetaProvider;

	public PreferencesFacadeBean() {
	}

	@RolesAllowed("Login")
	public PreferencesVO getUserPreferences() throws CommonFinderException {
		// print out the default encoding on this platform:
		LOG.debug("PreferencesFacadeBean.getUserPreferences: "
		          + "Default character encoding on this platform:");
		LOG.debug("  System.getProperty(\"file.encoding\") = {}",
		          System.getProperty("file.encoding"));
		// debug("  sun.io.Converters.getDefaultEncodingName() = " + sun.io.Converters.getDefaultEncodingName());
		LOG.debug("  java.nio.charset.Charset.defaultCharset().name() = {}",
		          java.nio.charset.Charset.defaultCharset().name());

		return getUserPreferences(this.getCurrentUserName().toLowerCase());
	}

	@RolesAllowed("Login")
	public PreferencesVO getTemplateUserPreferences() throws CommonFinderException{
		PreferencesVO templateUserPrefs = null;
		String templateUser = ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TEMPLATE_USER);
		if (templateUser != null) {
			templateUserPrefs = getUserPreferences(templateUser);
		}
		return templateUserPrefs;
	}

	@RolesAllowed("Login")
	public void modifyUserPreferences(PreferencesVO prefsvo) throws CommonFinderException {
		setPreferencesForUser(getCurrentUserName(),prefsvo);
	}

	@RolesAllowed("UseManagementConsole")
	public void setPreferencesForUser(String sUserName, PreferencesVO prefsvo) throws CommonFinderException {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.USER);
		query.select(t.basePk());
		query.where(builder.equal(builder.upper(t.baseColumn(E.USER.name)), builder.upper(builder.literal(sUserName))));

		final UID userUid = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		if (userUid != null) {
			byte[] data = prefsvo == null ? new byte[]{} : convertToBadBytes(prefsvo.getPreferencesBytes());
			dataBaseHelper.getDbAccess().execute(DbStatementUtils.updateValues(E.USER,
				E.USER.preferences, data).where(E.USER.getPk(), userUid));
		}
	}

	@RolesAllowed("UseManagementConsole")
	public PreferencesVO getPreferencesForUser(String sUserName) throws CommonFinderException {
		return getUserPreferences(sUserName);
	}

	@RolesAllowed("UseManagementConsole")
	public void mergePreferencesForUser(String targetUser, Map<String, Map<String, String>> preferencesToMerge) throws CommonFinderException {
		try {
			NavigableMap<String, Map<String, String>> preferencesMap;
			PreferencesVO preferencesVO = getUserPreferences(targetUser);
			if (preferencesVO != null && preferencesVO.getPreferencesBytes().length > 0) {
				preferencesMap = PreferencesConverter.loadPreferences(
					new ByteArrayInputStream(preferencesVO.getPreferencesBytes()));
			} else {
				preferencesMap = new TreeMap<String, Map<String, String>>();
			}

			// Merge preferences
			preferencesMap.putAll(preferencesToMerge);

			// Save merged preferences
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PreferencesConverter.writePreferences(baos, preferencesMap, true);
			setPreferencesForUser(targetUser, new PreferencesVO(baos.toByteArray()));
		} catch (IOException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	private void handleWorkspaceBusinessExceptions(DbException ex) throws PreferencesException {
		boolean throwBusiness = false;
		if ("Workspace.name.in.use".equals(ex.getMessage())) {
			throwBusiness = true;
		} else if ("Workspace.not.found".equals(ex.getMessage())) {
			throwBusiness = true;
		}
		if (throwBusiness) {
			throw new PreferencesException(ex.getMessage());
		} else {
			throw ex;
		}
	}

	public Collection<WorkspaceVO> getWorkspaceHeaderOnly() {
		
		synchronized(PreferencesFacadeBean.class) {
		
			final String user = getCurrentUserName();
			List<WorkspaceVO> result = new ArrayList<WorkspaceVO>();
			List<WorkspaceVO> lstAssignedByRole = getWorkspaceProcessor().getByAssignedByRole(user);
			List<WorkspaceVO> lstToRemove = new ArrayList<WorkspaceVO>();
			boolean isAssigner = SecurityCache.getInstance().getAllowedActions(user, null).contains(Actions.ACTION_WORKSPACE_ASSIGN);
			boolean isMaintenanceAllowed = SecurityCache.getInstance().isMaintenanceUser(user);
			
			for (WorkspaceVO wsvo : getWorkspaceProcessor().getByUser(user)) {
				if (isAssigner) {
					result.add(wsvo); 
				} else {
					if (wsvo.getAssignedWorkspace() == null) {
						result.add(wsvo);
					} else {
						boolean assigned = false;
						for (WorkspaceVO wsvoAssigned : lstAssignedByRole) {
							if (wsvo.getAssignedWorkspace().equals(wsvoAssigned.getPrimaryKey())) {
								wsvo.setName(wsvoAssigned.getName());
								wsvo.getWoDesc().setName(wsvo.getName());
								assigned = true;
								break;
							}
						}
						if (assigned) {
							result.add(wsvo);
						} else {
							lstToRemove.add(wsvo);
						}
					}
				} 				
			}
			
			for (WorkspaceVO wovo : lstToRemove) {
				getWorkspaceProcessor().delete(new Delete(wovo.getPrimaryKey()));
			}
			
			List<WorkspaceVO> newAssigned = getWorkspaceProcessor().getNewAssigned(getCurrentUserName());
	
			// remove first userUID workspace if:
			// 1. only one userUID workspace exists
			// 2. workspace is not assigned
			// 3. userUID has no right to create new workspaces
			// 1+2+3 = Initial start workspace
			// 4. at least one assigned workspace is new
			if (result.size() == 1
			 && result.get(0).getAssignedWorkspace() == null
			 && !SecurityCache.getInstance().getAllowedActions(getCurrentUserName(), null).contains(Actions.ACTION_WORKSPACE_CREATE_NEW)
			 && !newAssigned.isEmpty()) {
				removeWorkspace(result.get(0).getPrimaryKey());
				result.clear();
			}
	
			for (WorkspaceVO assignedWorkspace : newAssigned) {
				WorkspaceVO customizedWorkspace = new WorkspaceVO();
				customizedWorkspace.importHeader(assignedWorkspace.getWoDesc());
				customizedWorkspace.setClbworkspace(assignedWorkspace.getClbworkspace());
				customizedWorkspace.setAssignedWorkspace(assignedWorkspace.getPrimaryKey());
				try {
					result.add(storeWorkspace(customizedWorkspace));
				} catch (CommonBusinessException e) {
					// ignore. may be workspace name is in use by private workspace.
				}
			}
	
			// add maintenance workspace for superuser if not already exists
			if(SecurityCache.getInstance().isSuperUser(getCurrentUserName()) || isMaintenanceAllowed) {
				boolean hasMaintenanceWorkspace = false;
				for (WorkspaceVO wovo : result) {
					if(wovo.isMaintenance() != null && wovo.isMaintenance()) {
						hasMaintenanceWorkspace = true;
						break;
					}
				}
				if(!hasMaintenanceWorkspace) {
					
					if(result.size() == 0) { // create default workspace (initial install on empty DB)
						WorkspaceVO vo = new WorkspaceVO();
						vo.setWoDesc(WorkspaceDescriptionDefaultsFactory.createOldMdiStyle());
						vo.setUser(SecurityCache.getInstance().getUserUid(getCurrentUserName()));
						vo.getWoDesc().setName(vo.getName());
						vo.getWoDesc().setHideName(true);
						vo.getWoDesc().setNuclosResource("org.nuclos.client.resource.icon.glyphish.174-imac.png");
						try {
							storeWorkspace(vo);
						} catch (CommonBusinessException e) {
							LOG.error("Unable to create default workspace.", e);
						};
						result.add(vo);
					}
					
					WorkspaceVO vo = new WorkspaceVO();
					vo.setName(SpringLocaleDelegate.getInstance().getText("maintenancemode"));
					vo.setWoDesc(WorkspaceDescriptionDefaultsFactory.createMaintenanceModeStyle());
					vo.setUser(SecurityCache.getInstance().getUserUid(getCurrentUserName()));
					vo.getWoDesc().setName(vo.getName());
					vo.getWoDesc().setNuclosResource("org.nuclos.client.resource.icon.glyphish.184-warning.png");
					vo.setMaintenance(true);
					try {
						storeWorkspace(vo);
					} catch (CommonBusinessException e) {
						LOG.error("Unable to create maintenance mode workspace.", e);
					};
					result.add(vo);
				}
			}
	
			// header only
			for (WorkspaceVO wovo : result) {
				wovo.getWoDesc().getFrames().clear();
				wovo.getWoDesc().removeAllEntityPreferences();
			}
	
			return result;
			
		}
	}

	public void storeWorkspaceHeaderOnly(WorkspaceVO wovo) throws PreferencesException {
		if (wovo.getAssignedWorkspace() != null && !isAssignWorkspaceAllowed()) {
			throw new CommonFatalException("Edit of assignable workspaces is not allowed!");
		}
		WorkspaceVO wovoDB = getWorkspaceProcessor().getByPrimaryKey(wovo.getPrimaryKey());

		wovoDB.importHeader(wovo.getWoDesc());

		DalUtils.updateVersionInformation(wovoDB, getCurrentUserName());
		wovoDB.flagUpdate();
		try {
			getWorkspaceProcessor().insertOrUpdate(wovoDB);
		} catch (DbException ex) {
			handleWorkspaceBusinessExceptions(ex);
		}
	}

	@Override
	public WorkspaceVO getWorkspace(final UID workspaceUid) throws WorkspaceException {
		WorkspaceVO wovo = getWorkspaceProcessor().getByPrimaryKey(workspaceUid);
		if (wovo == null) {
			throw new WorkspaceException("Workspace.not.found");
		}
		if (wovo.getAssignedWorkspace() != null) {
			WorkspaceVO assignedWovo = getWorkspaceProcessor().getByPrimaryKey(wovo.getAssignedWorkspace());
			
			assignedWovo = getWorkspaceProcessor().getByPrimaryKey(wovo.getAssignedWorkspace());
			
			if (!SecurityCache.getInstance().getAllowedActions(getCurrentUserName(), null).contains(Actions.ACTION_WORKSPACE_ASSIGN)) {
				if (assignedWovo.getWoDesc().isAlwaysReset()) {
					removeAllTabs(wovo.getWoDesc().getMainFrame().getContent().getContent(), true);
				}
				return mergeWorkspaces(assignedWovo, wovo);
			} else {
				if (wovo.getWoDesc().isAlwaysReset()) {
					removeAllTabs(wovo.getWoDesc().getMainFrame().getContent().getContent(), false);
				}
			}
		}
		return wovo;
	}
	
	private void removeAllTabs(NestedContent nc, boolean all) {
		if (nc instanceof MutableContent) {
			removeAllTabs(((MutableContent) nc).getContent(), all);
		} else if (nc instanceof Split) {
			removeAllTabs(((Split) nc).getContentA(), all);
			removeAllTabs(((Split) nc).getContentB(), all);
		} else if (nc instanceof Tabbed) {
			if (all) {
				((Tabbed) nc).clearTabs(getCurrentMandatorUID());
			} else {
				for (Tab t : ((Tabbed) nc).getTabs(getCurrentMandatorUID())) {
					if (!t.isNeverClose()) {
						((Tabbed) nc).removeTab(getCurrentMandatorUID(), t);
					}
				}
			}
		} else {
			throw new UnsupportedOperationException("Unknown NestedContent type: " + nc.getClass());
		}
	}

	public WorkspaceVO storeWorkspace(WorkspaceVO wovo) throws PreferencesException {
		final String user = getCurrentUserName();
		DalUtils.updateVersionInformation(wovo, user);
		if (wovo.getPrimaryKey() == null) {
			wovo.setUser(SecurityCache.getInstance().getUserUid(user));
			wovo.flagNew();
			wovo.setPrimaryKey(new UID());
			//wovo.setPrimaryKey(DalUtils.getNextId());
		} else {
			wovo.flagUpdate();
		}
		try {
			getWorkspaceProcessor().insertOrUpdate(wovo);
		} catch (DbException ex) {
			handleWorkspaceBusinessExceptions(ex);
		}
		return wovo;
	}

	private WorkspaceVO storeAssignableWorkspace(WorkspaceVO wovo) throws PreferencesException {
		DalUtils.updateVersionInformation(wovo, getCurrentUserName());
		wovo.setUser(null);
		if (wovo.getPrimaryKey() == null) {
			wovo.flagNew();
			wovo.setPrimaryKey(new UID());
		} else {
			wovo.flagUpdate();
		}
		try {
			getWorkspaceProcessor().insertOrUpdate(wovo);
		} catch (DbException ex) {
			handleWorkspaceBusinessExceptions(ex);
		}
		return wovo;
	}

	public void removeWorkspace(final UID workspaceUid) {
		try {
			WorkspaceVO wovo = getWorkspace(workspaceUid);
			if (wovo.isAssigned() || wovo.getUser() == null) {
				if (!isAssignWorkspaceAllowed()) {
					throw new CommonFatalException("Remove of assignable workspaces is not allowed!");
				}
				if (wovo.isAssigned()) {
					getWorkspaceProcessor().deleteAllByAssigned(wovo.getAssignedWorkspace());
					getWorkspaceProcessor().modifyWorkspaceAssignments(new ArrayList<UID>(), wovo.getAssignedWorkspace(), getCurrentUserName());
				}
				if (wovo.getUser() == null) {
					getWorkspaceProcessor().deleteAllByAssigned(workspaceUid);
					getWorkspaceProcessor().modifyWorkspaceAssignments(new ArrayList<UID>(), workspaceUid, getCurrentUserName());
				}
			}
		} catch (WorkspaceException e) {
			throw new CommonFatalException(e);
		}
		getWorkspaceProcessor().delete(new Delete<UID>(workspaceUid));
	}

	public WorkspaceVO assignWorkspace(WorkspaceVO wovo, Collection<UID> roleUids) throws WorkspaceException {
		if (!isAssignWorkspaceAllowed()) {
			throw new CommonFatalException("Edit of assignable workspaces is not allowed!");
		}

		if (wovo.getAssignedWorkspace() == null) {
			// workspace is private, make it assignable

			if (!roleUids.isEmpty()) {
				WorkspaceVO assignableWovo = getWorkspace(wovo.getPrimaryKey());
				WorkspaceVO customizedWovo = splitWorkspace(assignableWovo);

				storeAssignableWorkspace(assignableWovo);
				storeWorkspace(customizedWovo);

				getWorkspaceProcessor().modifyWorkspaceAssignments(roleUids, assignableWovo.getPrimaryKey(), getCurrentUserName());

				// transfer header only
				WorkspaceDescription2 customizedWoDesc = customizedWovo.getWoDesc();
				customizedWovo.setWoDesc(new WorkspaceDescription2());
				customizedWovo.importHeader(customizedWoDesc);

				return customizedWovo;
			} else {
				return wovo;
			}

		} else {
			// assigned workspace
			if (roleUids.isEmpty()) {
				// revert assignment, make it private again

				final UID assignableWovoUid = wovo.getAssignedWorkspace();

				WorkspaceVO privateWovo = getWorkspace(wovo.getPrimaryKey());
				privateWovo.setAssignedWorkspace(null);
				storeWorkspace(privateWovo);

				getWorkspaceProcessor().modifyWorkspaceAssignments(roleUids, assignableWovoUid, getCurrentUserName());
				removeWorkspace(assignableWovoUid);

				// transfer header only
				WorkspaceDescription2 privateWoDesc = privateWovo.getWoDesc();
				privateWovo.setWoDesc(new WorkspaceDescription2());
				privateWovo.importHeader(privateWoDesc);

				return privateWovo;
			} else {
				// modify assignments only
				getWorkspaceProcessor().modifyWorkspaceAssignments(roleUids, wovo.getAssignedWorkspace(), getCurrentUserName());

				return wovo; // workspace is customized
			}
		}
	}
	
	public Collection<EntityObjectVO<UID>> getAssignableRoles() {
		return nucletDalProvider.getEntityObjectProcessor(E.ROLE).getAll();
	}

	@Override
	public Collection<UID> getAssignedRoleIds(UID assignedWorkspaceUid) {
		return getWorkspaceProcessor().getAssignedRoleIds(assignedWorkspaceUid);
	}

	private WorkspaceVO splitWorkspace(WorkspaceVO assignableWovo) throws WorkspaceException {
		WorkspaceVO customizedWorkspace = new WorkspaceVO();

		transferWorkspaceContent(assignableWovo, customizedWorkspace);
		removeNonMainFrames(assignableWovo);
		removeClosableTabs(assignableWovo.getWoDesc());
		flagTabsAssigned(assignableWovo.getWoDesc());

		customizedWorkspace.setAssignedWorkspace(assignableWovo.getPrimaryKey());
		assignableWovo.setUser(null);

		return customizedWorkspace;
	}

	public void publishWorkspaceChanges(WorkspaceVO customizedWovo, boolean isPublishStructureChange, boolean isPublishStructureUpdate, 
			boolean isPublishStarttabConfiguration, boolean isPublishToolbarConfiguration) throws WorkspaceException {
		
		if (!isAssignWorkspaceAllowed()) {
			throw new CommonFatalException("Edit of assignable workspaces is not allowed!");
		}

		if (customizedWovo.getWoDesc().getEntityPreferences().isEmpty() 
				&& customizedWovo.getWoDesc().getFrames().isEmpty()) {
			// header only
			WorkspaceVO customizedWovoFromDb = getWorkspace(customizedWovo.getPrimaryKey());
			customizedWovo = customizedWovoFromDb;
		}
		WorkspaceVO assignableWovo = getWorkspace(customizedWovo.getAssignedWorkspace());

		/**
		 * STRUCTURE CHANGE - implements STRUCTURE UPDATE, STARTTAB CONFIGURATIONs
		 */
		if (isPublishStructureChange) {
			transferWorkspaceContent(customizedWovo, assignableWovo);
			removeNonMainFrames(assignableWovo);
			removeClosableTabs(assignableWovo.getWoDesc());
			flagTabsAssigned(assignableWovo.getWoDesc());
		} else {
			// check before publish
			if (isStructureChanged(assignableWovo.getWoDesc().getMainFrame().getContent(), customizedWovo.getWoDesc().getMainFrame().getContent())) {
				throw new CommonFatalException("Workspace structure differs");
			}

			WorkspaceVO assignableBackupWovo = getWorkspace(customizedWovo.getAssignedWorkspace());

			/**
			 * STRUCTURE UPDATE - implements STARTTAB CONFIGURATIONs
			 */
			if (isPublishStructureUpdate) {
				transferWorkspaceContent(customizedWovo, assignableWovo);
				removeNonMainFrames(assignableWovo);
				removeClosableTabs(assignableWovo.getWoDesc());
				flagTabsAssigned(assignableWovo.getWoDesc());

				if (!isPublishStarttabConfiguration) {
					/**
					 * NO STARTTAB CONFIGURATIONs - restore from backup
					 */
					transferStarttabConfiguration(assignableBackupWovo.getWoDesc().getMainFrame().getContent(), assignableWovo.getWoDesc().getMainFrame().getContent());
				}
			} else {
				/**
				 * STARTTAB CONFIGURATIONs without STRUCTURE UPDATE
				 */
				if (isPublishStarttabConfiguration) {
					transferStarttabConfiguration(customizedWovo.getWoDesc().getMainFrame().getContent(), assignableWovo.getWoDesc().getMainFrame().getContent());
				}
			}
		}

		/**
		 * TOOLBARs
		 */
		if (isPublishToolbarConfiguration) {
			// TODO implement
		}

		storeWorkspace(assignableWovo);
	}

	private void transferStarttabConfiguration(NestedContent ncSource, NestedContent ncTarget) {
		if (ncSource instanceof MutableContent) {
			transferStarttabConfiguration(((MutableContent) ncSource).getContent(), ((MutableContent) ncTarget).getContent());
		} else if (ncSource instanceof Split) {
			transferStarttabConfiguration(((Split) ncSource).getContentA(), ((Split) ncTarget).getContentA());
			transferStarttabConfiguration(((Split) ncSource).getContentB(), ((Split) ncTarget).getContentB());
		} else if (ncSource instanceof Tabbed) {
			final Tabbed tbbSource = (Tabbed) ncSource;
			final Tabbed tbbTarget = (Tabbed) ncTarget;

			tbbTarget.getPredefinedEntityOpenLocations().clear();
			tbbTarget.addAllPredefinedEntityOpenLocations(new ArrayList<UID>(tbbSource.getPredefinedEntityOpenLocations()));

			tbbTarget.getReducedStartmenus().clear();
			tbbTarget.addAllReducedStartmenus(tbbSource.getReducedStartmenus());

			tbbTarget.setAlwaysHideBookmark(tbbSource.isAlwaysHideBookmark());
			tbbTarget.setAlwaysHideHistory(tbbSource.isAlwaysHideHistory());
			tbbTarget.setAlwaysHideStartmenu(tbbSource.isAlwaysHideStartmenu());
			tbbTarget.setNeverHideBookmark(tbbSource.isNeverHideBookmark());
			tbbTarget.setNeverHideHistory(tbbSource.isNeverHideHistory());
			tbbTarget.setNeverHideStartmenu(tbbSource.isNeverHideStartmenu());
			tbbTarget.setShowAdministration(tbbSource.isShowAdministration());
			tbbTarget.setShowConfiguration(tbbSource.isShowConfiguration());
			tbbTarget.setShowEntity(tbbSource.isShowEntity());

			tbbTarget.setDesktop(tbbSource.getDesktop());
			tbbTarget.setDesktopActive(tbbSource.isDesktopActive());
			tbbTarget.setHideStartTab(tbbSource.isHideStartTab());
		} else {
			throw new UnsupportedOperationException("Unknown NestedContent type: " + ncSource.getClass());
		}
	}

	public WorkspaceVO restoreWorkspace(WorkspaceVO customizedWovo) throws WorkspaceException {
		if (customizedWovo.getAssignedWorkspace() == null) {
			throw new CommonFatalException("Workspace is not assigned!");
		}

		WorkspaceVO assignableWovo = getWorkspace(customizedWovo.getAssignedWorkspace());

		transferWorkspaceContent(assignableWovo, customizedWovo);
		storeWorkspace(customizedWovo);

		// only header
		customizedWovo.getWoDesc().getFrames().clear();
		customizedWovo.getWoDesc().removeAllEntityPreferences();

		return customizedWovo;
	}

	void transferWorkspaceContent(WorkspaceVO sourceWovo, WorkspaceVO targetWovo) {
		targetWovo.importHeader(sourceWovo.getWoDesc());

		// clone WorkspaceDescription
		targetWovo.setClbworkspace(sourceWovo.getClbworkspace());
	}

	private void removeNonMainFrames(WorkspaceVO wovo) throws WorkspaceException {
		// only main frame in target
		Frame mainFrame = wovo.getWoDesc().getMainFrame();
		wovo.getWoDesc().getFrames().clear();
		wovo.getWoDesc().addFrame(mainFrame);

		// check for home and tree tabbed in main frame
		wovo.getWoDesc().getHomeTabbed();
		wovo.getWoDesc().getHomeTreeTabbed();
	}

	private void flagTabsAssigned(WorkspaceDescription2 wd) {
		for (Frame frame : wd.getFrames()) {
			for (Tab tab : getTabs(frame.getContent())) {
				tab.setFromAssigned(true);
			}
		}
	}

	private void removeClosableTabs(WorkspaceDescription2 wd) {
		for (Frame frame : wd.getFrames()) {
			removeClosableTabs(frame.getContent());
		}
	}

	private void removeClosableTabs(NestedContent nc) {
		if (nc instanceof MutableContent) {
			removeClosableTabs(((MutableContent) nc).getContent());
		} else if (nc instanceof Split) {
			removeClosableTabs(((Split) nc).getContentA());
			removeClosableTabs(((Split) nc).getContentB());
		} else if (nc instanceof Tabbed) {
			for (Tab tab : ((Tabbed) nc).getTabs(getCurrentMandatorUID())) {
				if (!tab.isNeverClose()) {
					((Tabbed) nc).removeTab(getCurrentMandatorUID(), tab);
				}
			}
		} else {
			throw new UnsupportedOperationException("Unknown NestedContent type: " + nc.getClass());
		}
	}

	private void mergeTabbeds(NestedContent ncAssigned, NestedContent ncCustomized) {
		if (ncAssigned instanceof MutableContent) {
			mergeTabbeds(((MutableContent) ncAssigned).getContent(), ((MutableContent) ncCustomized).getContent());
		} else if (ncAssigned instanceof Split) {
			mergeTabbeds(((Split) ncAssigned).getContentA(), ((Split) ncCustomized).getContentA());
			mergeTabbeds(((Split) ncAssigned).getContentB(), ((Split) ncCustomized).getContentB());
		} else if (ncAssigned instanceof Tabbed) {
			final Tabbed tbbAssigned = (Tabbed) ncAssigned;
			final Tabbed tbbCustomized = (Tabbed) ncCustomized;

			// add never closable tabs
			final List<Tab> tabsAssigned = tbbAssigned.getTabs(getCurrentMandatorUID());
			final List<Tab> tabsCustomized = tbbCustomized.getTabs(getCurrentMandatorUID());
			for (Tab tab : tabsAssigned) {
				if (!tabsCustomized.contains(tab)) {
					((Tabbed) ncCustomized).addTab(getCurrentMandatorUID(), tab);
				}
			}

			// start tab configuration
			if (!SecurityCache.getInstance().getAllowedActions(getCurrentUserName(), null).contains(Actions.ACTION_WORKSPACE_CUSTOMIZE_STARTTAB)) {
				tbbCustomized.getPredefinedEntityOpenLocations().clear();
				tbbCustomized.addAllPredefinedEntityOpenLocations(new ArrayList<UID>(tbbAssigned.getPredefinedEntityOpenLocations()));

				tbbCustomized.getReducedStartmenus().clear();
				tbbCustomized.addAllReducedStartmenus(tbbAssigned.getReducedStartmenus());

				tbbCustomized.setAlwaysHideBookmark(tbbAssigned.isAlwaysHideBookmark());
				tbbCustomized.setAlwaysHideHistory(tbbAssigned.isAlwaysHideHistory());
				tbbCustomized.setAlwaysHideStartmenu(tbbAssigned.isAlwaysHideStartmenu());
				tbbCustomized.setNeverHideBookmark(tbbAssigned.isNeverHideBookmark());
				tbbCustomized.setNeverHideHistory(tbbAssigned.isNeverHideHistory());
				tbbCustomized.setNeverHideStartmenu(tbbAssigned.isNeverHideStartmenu());
				tbbCustomized.setShowAdministration(tbbAssigned.isShowAdministration());
				tbbCustomized.setShowConfiguration(tbbAssigned.isShowConfiguration());
				tbbCustomized.setShowEntity(tbbAssigned.isShowEntity());

				tbbCustomized.setDesktop(tbbAssigned.getDesktop());
			}
		} else {
			throw new UnsupportedOperationException("Unknown NestedContent type: " + ncAssigned.getClass());
		}
	}
	
	private WorkspaceVO mergeWorkspaces(WorkspaceVO assignableWovo, WorkspaceVO customizedWovo) throws WorkspaceException {
		final WorkspaceVO mergedWovo = customizedWovo;
		final WorkspaceDescription2 wdm = mergedWovo.getWoDesc();
		final WorkspaceDescription2 wda = assignableWovo.getWoDesc();
		wdm.importHeader(wda);

		// list of frames is ordered by use (last on top)
		// --> merge only mainframe
		if (isStructureChanged(wdm.getMainFrame(), wda.getMainFrame())) {

			// backup open tabs
			final List<Tab> tabsToMove = getTabs(wdm.getMainFrame().getContent());
			final List<Tab> tabsSource = getTabs(wda.getMainFrame().getContent());

			// transfer structure
			wdm.getMainFrame().getContent().setContent(wda.getMainFrame().getContent().getContent());
			final Tabbed homeTabbed = wdm.getHomeTabbed();

			// move tabs
			for (Tab ttm : tabsToMove) {
				if (!tabsSource.contains(ttm)) {
					homeTabbed.addTab(getCurrentMandatorUID(), ttm);
				}
			}

			// remove home tabbed in other frames
			for (Frame f : wdm.getFrames()) {
				if (!f.isMainFrame()) {
					removeHomeTabbedFlags(f.getContent());
				}
			}
		}

		// same structure in merged and assigned
		// transfer assigned never closable tabs, start tab configuration...
		mergeTabbeds(wda.getMainFrame().getContent(), wdm.getMainFrame().getContent());
			
		wdm.removeAllLayoutPreferences();
		wdm.addAllLayoutPreferences(wda.getLayoutPreferences());
		
		return mergedWovo;
	}
	
	public boolean isWorkspaceStructureChanged(UID id1, UID id2) throws WorkspaceException {
		return isStructureChanged(
				getWorkspaceProcessor().getByPrimaryKey(id1).getWoDesc().getMainFrame(),
				getWorkspaceProcessor().getByPrimaryKey(id2).getWoDesc().getMainFrame());
	}

	private boolean isStructureChanged(Frame f1, Frame f2) {
		return isStructureChanged(f1.getContent(), f2.getContent());
	}

	private boolean isStructureChanged(NestedContent nc1, NestedContent nc2) {
		if (equalsContent(nc1, nc2)) {
			if (nc1 instanceof MutableContent) {
				return isStructureChanged(((MutableContent) nc1).getContent(), ((MutableContent) nc2).getContent());
			} else if (nc1 instanceof Split) {
				return isStructureChanged(((Split) nc1).getContentA(), ((Split) nc2).getContentA())
						|| isStructureChanged(((Split) nc1).getContentB(), ((Split) nc2).getContentB());
			} else if (nc1 instanceof Tabbed) {
				return false;
			} else {
				throw new UnsupportedOperationException("Unknown NestedContent type: " + nc1.getClass());
			}
		} else {
			return true;
		}
	}

	private boolean equalsContent(NestedContent nc1, NestedContent nc2) {
		if (nc1 instanceof MutableContent) {
			return (nc2 instanceof MutableContent);
		} else if (nc1 instanceof Split) {
			return (nc2 instanceof Split);
		} else if (nc1 instanceof Tabbed) {
			return (nc2 instanceof Tabbed);
		} else {
			throw new UnsupportedOperationException("Unknown NestedContent type: " + nc1.getClass());
		}
	}

	private List<Tab> getTabs(NestedContent nc) {
		List<Tab> result = new ArrayList<Tab>();
		if (nc instanceof MutableContent) {
			result.addAll(getTabs(((MutableContent) nc).getContent()));
		} else if (nc instanceof Split) {
			result.addAll(getTabs(((Split) nc).getContentA()));
			result.addAll(getTabs(((Split) nc).getContentB()));
		} else if (nc instanceof Tabbed) {
			result.addAll(((Tabbed) nc).getTabs(getCurrentMandatorUID()));
		}
		return result;
	}

	private void removeHomeTabbedFlags(NestedContent nc) {
		if (nc instanceof MutableContent) {
			removeHomeTabbedFlags(((MutableContent) nc).getContent());
		} else if (nc instanceof Split) {
			removeHomeTabbedFlags(((Split) nc).getContentA());
			removeHomeTabbedFlags(((Split) nc).getContentB());
		} else if (nc instanceof Tabbed) {
			((Tabbed) nc).setHome(false);
			((Tabbed) nc).setHomeTree(false);
		}
	}

	private boolean isAssignWorkspaceAllowed() {
		return SecurityCache.getInstance().getAllowedActions(getCurrentUserName(), null).contains(Actions.ACTION_WORKSPACE_ASSIGN);
	}
	
	private boolean isPublishProfileAllowed() {
		return isAssignWorkspaceAllowed();
	}
	
	private boolean isCustomizeProfileAllowed() {
		return SecurityCache.getInstance().getAllowedActions(getCurrentUserName(), null).contains(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS);
	}

	private PreferencesVO getUserPreferences(String sUserName) throws CommonFinderException {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<byte[]> query = builder.createQuery(byte[].class);
		DbFrom<UID> t = query.from(E.USER);
		query.select(t.baseColumn(E.USER.preferences));
		query.where(builder.equal(builder.upper(t.baseColumn(E.USER.name)), builder.upper(builder.literal(sUserName))));

		try {
			byte[] b = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			if (b != null) {
				return new PreferencesVO(convertFromBadBytes(b));
			}
		} catch (DbInvalidResultSizeException e) {
			final String msg = "There are no stored preferences for the current userUID (" + sUserName + ").";
			LOG.info(msg + e.toString(), e);
			throw new CommonFinderException(msg);
		}
		return null;
	}

	// Previously, the preferences were transferred as Strings.  This is obviously wrong because they
	// are bytes (Java's Preferences export/import API works with byte streams).  Moreover, client and
	// servers used different charsets to encode/decode the bytes (UTF-8 on the client side, and the
	// server's native encoding in this Facade)
	// => the stored BLOBs are broken (invalid XML w.r.t encoding)!
	//
	// The following code imitates the old behavior to work around these issues.  If DB migrations are
	// possible, please migrate all BLOBs once using convertFromBadBytes (from native->UTF-8)
	//
	// For a test case, use preferences with some umlauts!

	@Deprecated
	private static byte[] convertFromBadBytes(byte[] b) {
		// interpret bytes as native encoded string and convert them back using UTF-8
		return new String(b).getBytes(UTF_8);
	}

	@Deprecated
	private static byte[] convertToBadBytes(byte[] b) {
		// interpret bytes as UTF-8 string and convert them back using the native encoding
		return new String(b, UTF_8).getBytes();
	}

	private final static Charset UTF_8 = Charset.forName("UTF-8");

	private IWorkspaceProcessor getWorkspaceProcessor() {
		return nucletDalProvider.getWorkspaceProcessor();
	}
	
	@Override
	public Map<String, Settings> getApiUserSettings() {
		final UID nuclosUserUid = SecurityCache.getInstance().getUserUid(getCurrentUserName());
		
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		final DbFrom<UID> t = query.from(E.USERSETTING);
		query.multiselect(
				t.baseColumn(E.USERSETTING.settingKey),
				t.baseColumn(E.USERSETTING.setting));
		query.where(builder.equalValue(t.baseColumn(E.USERSETTING.user), nuclosUserUid));

		
		final Map<String, Settings> result = new HashMap<String, Settings>();
		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
			final String sKey = tuple.get(0, String.class);
			final String sSettings = tuple.get(1, String.class);
			try {
				final XStreamSupport xs = XStreamSupport.getInstance();
				try (CloseableXStream closeable = xs.getCloseableStream()) {
					final XStream xstream = closeable.getStream();
					Settings userSettings = (Settings) xstream.fromXML(sSettings);
					result.put(sKey, userSettings);
				}
			} catch (Exception ex) {
				LOG.error("UserSettings (Key={}) not restored! Error={}", sKey, ex.getMessage(), ex);
			}
		}
		return result;
	}
	
	@Override
	public Settings getApiUserSettings(final String key) {
		final UID nuclosUserUid = SecurityCache.getInstance().getUserUid(getCurrentUserName());
		
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<String> query = builder.createQuery(String.class);
		final DbFrom<UID> t = query.from(E.USERSETTING);
		query.select(t.baseColumn(E.USERSETTING.setting));
		query.where(builder.and(
				builder.equalValue(t.baseColumn(E.USERSETTING.user), nuclosUserUid),
				builder.equalValue(t.baseColumn(E.USERSETTING.settingKey), key)));

		try {
			try {
				final XStreamSupport xs = XStreamSupport.getInstance();
				try (CloseableXStream closeable = xs.getCloseableStream()) {
					final XStream xstream = closeable.getStream();
					Settings userSettings = (Settings) xstream.fromXML(dataBaseHelper.getDbAccess().executeQuerySingleResult(query));
					return userSettings;
				}
			} catch (DbInvalidResultSizeException ex) {
				return new SettingsImpl();
			} 
		} catch (Exception e1) {
			LOG.error("UserSettings (Key={}) not restored! Error={}", key, e1.getMessage(), e1);
			throw new NuclosFatalException(e1);
		} 
	}
	
	@Override
	public void setApiUserSettings(Map<String, Settings> userSettings) {
		if (userSettings == null) {
			return;
		}
		
		for (String key : userSettings.keySet()) {
			setApiUserSettings(key, userSettings.get(key));
		}
	}
	
	@Override
	public void setApiUserSettings(String key, Settings userSettings) {
		if (userSettings == null) {
			return;
		}
		
		final String user = getCurrentUserName();
		final UID nuclosUserUid = SecurityCache.getInstance().getUserUid(user);
		
		/*
		final Map<String, Object> conditions = new HashMap<String, Object>(1);
		conditions.put("INTID_T_MD_USER", nuclosUserUid);
		conditions.put("STRSETTINGKEY", key);
		*/
		final DbMap dbMapConditions = new DbMap();
		dbMapConditions.put(E.USERSETTING.user, nuclosUserUid);
		dbMapConditions.put(E.USERSETTING.settingKey, key);
		
		DbTableStatement<UID> stmt = new DbDeleteStatement<UID>(E.USERSETTING, dbMapConditions);
		try {
			dataBaseHelper.getDbAccess().execute(stmt);
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}

		final XStreamSupport xs = XStreamSupport.getInstance();
		final String sSettings;
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			sSettings = xstream.toXML(userSettings);
		}

		/*
		final Map<String, Object> values = new HashMap<String, Object>();
		values.put("INTID_T_MD_USER", nuclosUserUid);
		values.put("STRSETTINGKEY", key);
		values.put("CLBSETTING", sSettings);
		values.put("INTID", IdUtils.toLongId(dataBaseHelper.getNextIdAsInteger(SpringDataBaseHelper.DEFAULT_SEQUENCE)));
		values.put("STRCREATED", userUID);
		values.put("STRCHANGED", userUID);
		values.put("DATCREATED", new Date());
		values.put("DATCHANGED", new Date());
		values.put("INTVERSION", 1);
		*/
		final DbMap dbMapValues = new DbMap();
		dbMapValues.put(E.USERSETTING.user, nuclosUserUid);
		dbMapValues.put(E.USERSETTING.settingKey, key);
		dbMapValues.put(E.USERSETTING.setting, sSettings);
		dbMapValues.put(E.USERSETTING.getPk(), new UID());
		dbMapValues.put(SF.CREATEDBY, user);
		dbMapValues.put(SF.CHANGEDBY, user);
		dbMapValues.put(SF.CREATEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
		dbMapValues.put(SF.CHANGEDAT, InternalTimestamp.toInternalTimestamp(new Date()));
		dbMapValues.put(SF.VERSION, 1);
		
		
		stmt = new DbInsertStatement<UID>(E.USERSETTING, dbMapValues);
		try {
			dataBaseHelper.getDbAccess().execute(stmt);
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}
	
	public WorkspaceVO getDefaultWorkspace(String user) throws CommonBusinessException {
		return getWorkspaceProcessor().getDefaultWorkspace(user, this);
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * List of preferences for the current user.
	 *
	 * @param app
	 * @param type
	 * @param entityUID
	 * @param layoutUID
	 * 			return layout dependent preferences.
	 * 			if null and returnDepEntities is true, all layouts are used.
	 * @param orLayoutIsNull
	 *  		return preferences without layout definition (for all layouts)
	 * @param menuRelevant
	 * @param returnDepEntities
	 * 			return dependent entities instead of "entityUID" direct. Uses layouts for determination.
	 * @return a list of PreferenceVOs (not null)
	 */
	public List<Preference> getPreferences(String app, String type, UID entityUID, UID layoutUID, boolean orLayoutIsNull, Boolean menuRelevant, Boolean returnDepEntities) {
		return getPreferences(app, type, entityUID, layoutUID, orLayoutIsNull, SecurityCache.getInstance().getUserUid(getCurrentUserName()), menuRelevant, returnDepEntities);
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * List of preferences for the given user.
	 *
	 * @param app
	 * @param type
	 * @param entityUID
	 * @param layoutUID
	 * 			return layout dependent preferences.
	 * 			if null and returnDepEntities is true, all layouts are used.
	 * @param orLayoutIsNull
	 *  		return preferences without layout definition (for all layouts)
	 * @param userUID
	 * @param menuRelevant
	 * @param returnDepEntities
	 * 			return dependent entities instead of "entityUID" direct. Uses layouts for determination.
	 * @return a list of PreferenceVOs (not null)
	 */
	public List<Preference> getPreferences(
			String app,
			String type,
			UID entityUID,
			UID layoutUID,
			boolean orLayoutIsNull,
			UID userUID,
			Boolean menuRelevant,
			Boolean returnDepEntities
	) {

		// TODO: Refactoring: Method has too many parameters and is too complex.

		final List<Preference> prefs;

		Set<UID> entityUIDs = new HashSet<>();
		if (entityUID != null) {
			if (Boolean.TRUE.equals(returnDepEntities)) {
				if (layoutUID != null) {
					entityUIDs.addAll(goMetaProvider.getSubFormEntityByLayout(layoutUID));
				} else {
					entityUIDs.addAll(goMetaProvider.getAllSubFormEntitiesByEntity(entityUID));
				}

				if (entityUIDs.isEmpty()) {
					// Abort if no Subform-BOs were found
					return new ArrayList<>();
				}
			} else {
				entityUIDs.add(entityUID);
			}
		}

		if (!entityUIDs.isEmpty() && type != null) {
			if (menuRelevant != null) {
				throw new IllegalArgumentException("get by menuRelevant not supported. Use it with userUID");
			}
			prefs = Preference.transformVOs(prefsProc.getByAppTypeUserAndEntity(app, type, userUID, entityUIDs, layoutUID, orLayoutIsNull));
		} else if (!entityUIDs.isEmpty()) {
			if (menuRelevant != null) {
				throw new IllegalArgumentException("get by menuRelevant not supported. Use it with userUID");
			}
			prefs = Preference.transformVOs(prefsProc.getByAppUserAndEntity(app, userUID, entityUIDs, layoutUID, orLayoutIsNull));
		} else if (type != null) {
			if (menuRelevant != null) {
				prefs = Preference.transformVOs(prefsProc.getByAppTypeUserAndMenuRelevance(app, type, userUID, menuRelevant));
			} else {
				prefs = Preference.transformVOs(prefsProc.getByAppTypeAndUser(app, type, userUID));
			}
		} else {
			if (menuRelevant != null) {
				prefs = Preference.transformVOs(prefsProc.getByAppUserAndMenuRelevance(app, userUID, menuRelevant));
			} else {
				prefs = Preference.transformVOs(prefsProc.getByAppAndUser(app, userUID));
			}
		}
		return prefs;
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Un-share the preference for the given user role
	 *
	 * @param prefUID
	 * @param roleUID
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@Override
	public void unSharePreference(UID prefUID, UID roleUID) throws CommonPermissionException, CommonFinderException {
		final UID mandator = getCurrentMandatorUID();
		final String userName = getCurrentUserName();
		final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		if (!SecurityCache.getInstance().getAllowedActions(userName, mandator).contains(Actions.ACTION_SHARE_PREFERENCES)) {
			throw new CommonPermissionException();
		}

		PreferenceVO prefDb = prefsProc.getByPrimaryKey(prefUID);
		if (prefDb == null) {
			throw new CommonFinderException();
		}

		if (prefDb.getSharedPreference() != null) {
			// is customized, use the shared UID
			prefUID = prefDb.getSharedPreference();
		}

		DbMap conditions = new DbMap();
		conditions.put(E.ROLEPREFERENCE.preference, prefUID);
		conditions.put(E.ROLEPREFERENCE.role, roleUID);
		DbDeleteStatement<UID> delete = new DbDeleteStatement<UID>(E.ROLEPREFERENCE, conditions);
		int result = SpringDataBaseHelper.getInstance().execute(delete);
		if (result != 1) {
			LOG.error("Un-share preference {} for role {} failed. DbResult={}",
					prefUID, roleUID, result);
			throw new CommonFinderException();
		} else {

			boolean isNotSharedAnyMore = getSharedUserRoles(prefUID).isEmpty();
			// delete customizations for users who lost their grant completely
			prefsProc.batchDeleteCustomizations(prefUID, isNotSharedAnyMore);

			if (isNotSharedAnyMore) {
				// make it private again...
				if (!prefDb.getPrimaryKey().equals(prefUID)) {
					// use cutomization instead of the shared one
					// recreate with old UID
					prefDb.flagNew();
					// and remove the shared one
					prefsProc.delete(new Delete<UID>(prefUID));
				} else {
					// no customization
					prefDb = prefsProc.getByPrimaryKey(prefUID);
					prefDb.flagUpdate();
				}
				prefDb.setSharedPreference(null);
				prefDb.setUser(userUID);
				prefDb.setNuclet(null);
				try {
					DalUtils.updateVersionInformation(prefDb, userName);
					prefsProc.insertOrUpdate(prefDb);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					throw new NuclosFatalException(e);
				}
			}
		}
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Share the preference with the given user role
	 */
	@Override
	public void sharePreference(UID prefUID, UID roleUID) throws CommonPermissionException, CommonFinderException {
		final UID mandator = getCurrentMandatorUID();
		final String userName = getCurrentUserName();
		//final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		if (!SecurityCache.getInstance().getAllowedActions(userName, mandator).contains(Actions.ACTION_SHARE_PREFERENCES)) {
			throw new CommonPermissionException();
		}

		PreferenceVO prefDb = prefsProc.getByPrimaryKey(prefUID);
		if (prefDb == null) {
			throw new CommonFinderException();
		}

		if (prefDb.getSharedPreference() != null) {
			// is customized, use the shared UID
			prefUID = prefDb.getSharedPreference();
		}

		EntityObjectVO<UID> shareVO = new EntityObjectVO<UID>(E.ROLEPREFERENCE);
		shareVO.setFieldUid(E.ROLEPREFERENCE.role, roleUID);
		shareVO.setFieldUid(E.ROLEPREFERENCE.preference, prefUID);

		try {
			DalUtils.updateVersionInformation(shareVO, userName);
			shareVO.setPrimaryKey(new UID());
			shareVO.flagNew();
			nucletDalProvider.getEntityObjectProcessor(E.ROLEPREFERENCE).insertOrUpdate(shareVO);
			if (prefDb.getSharedPreference() == null && prefDb.getUser() != null) {
				// must be first sharing
				prefDb.setUser(null);

				// add to nuclet ?
				// TODO: entity UID should be null if not set, instead of UID containing a blank string
				if (StringUtils.isNotBlank(prefDb.getEntity().toString())) {
					final UID nuclet = metaProvider.getEntity(prefDb.getEntity()).getNuclet();
					if (nuclet != null) {
						final boolean isNucletSource = metaProvider.getNuclet(nuclet).getFieldValue(E.NUCLET.source);
						if (isNucletSource) {
							// Only auto-add to Nuclet if this Nuclos instance is a source system for this Nuclet.
							// Otherwise the user will add something which will be removed on next Nuclet import.
							prefDb.setNuclet(nuclet);
						}
					}
				}

				prefDb.flagUpdate();
				DalUtils.updateVersionInformation(prefDb, userName);
				prefsProc.insertOrUpdate(prefDb);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Get a list of usergroups to share prefernces with
	 *
	 * @param prefUID
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@Override
	public List<PreferenceShareVO> getPreferenceShares(UID prefUID) throws CommonPermissionException, CommonFinderException {
		final UID mandator = getCurrentMandatorUID();
		final String userName = getCurrentUserName();
		final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		if (!SecurityCache.getInstance().getAllowedActions(userName, mandator).contains(Actions.ACTION_SHARE_PREFERENCES)) {
			throw new CommonPermissionException();
		}

		PreferenceVO prefDb = prefsProc.getByPrimaryKey(prefUID);
		if (prefDb == null) {
			throw new CommonFinderException();
		}

		if (prefDb.getSharedPreference() != null) {
			// is customized, use the shared UID
			prefUID = prefDb.getSharedPreference();
		}


		boolean isSuperuser = SecurityCache.getInstance().isSuperUser(userName);
		Map<UID, String> allRolesWithName = SecurityCache.getInstance().getAllRolesWithName();
		Set<UID> userRoles = SecurityCache.getInstance().getUserRoles(userName, mandator);
		List<EntityObjectVO<UID>> alreadyShared = getSharedUserRoles(prefUID);

		List<PreferenceShareVO> result = new ArrayList<>();
		for (Map.Entry<UID, String> rwm : allRolesWithName.entrySet()) {
			if (!isSuperuser) {
				if (!userRoles.contains(rwm.getKey())) {
					// No super user, do not allow sharing with stranger roles.
					// Otherwise sharing would hide the shared preference, ugh.
					continue;
				}
			}

			boolean shared = false;
			for (EntityObjectVO<UID> share : alreadyShared) {
				if (share.getFieldUid(E.ROLEPREFERENCE.role).equals(rwm.getKey())) {
					shared = true;
					break;
				}
			}
			result.add(new PreferenceShareVO(userUID, rwm.getKey(), rwm.getValue(), shared));
		}

		Collections.sort(result);
		return result;
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Get a single preference of the current user.
	 *
	 * @param prefUID
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@Override
	public Preference getPreference(UID prefUID) throws CommonPermissionException, CommonFinderException {
		final String userName = getCurrentUserName();
		final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		PreferenceVO voDb = prefsProc.getByPrimaryKeyWithSelected(prefUID, userUID);
		if (voDb == null) {
			throw new CommonFinderException();
		}

		final Preference prefDb = new Preference(voDb);
		final boolean isShared = prefDb.isShared();
		Preference customPrefDb = null;

		if (isShared) {
			// search for customization
			PreferenceVO customVoDb = prefsProc.getBySharedKeyAndUser(prefUID, userUID);
			if (customVoDb != null) {
				// set nuclet from shared preference
				customVoDb.setNuclet(voDb.getNuclet());
				customPrefDb = new Preference(customVoDb);
			}
		} else {
			// personal unshared pref
			if (!voDb.getUser().equals(userUID)) {
				throw new CommonPermissionException("You does not own these preference");
			}
		}

		try {
			return RigidUtils.defaultIfNull(customPrefDb, prefDb);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Insert a single preference for the current user.
	 * App is optional. Would be 'nuclos' if not set
	 * Type is mandatory. In case of app-'nuclos' it must be registered in org.nuclos.common.NuclosPreferenceType
	 *
	 * @param wpref
	 * @return
	 * @throws CommonPermissionException
	 */
	@Override
	public Preference insertPreference(Preference.WritablePreference wpref) throws CommonPermissionException {
		final SecurityCache securityCache = SecurityCache.getInstance();
		final String userName = getCurrentUserName();
		final UID userUID = securityCache.getUserUid(userName);
		final Boolean selected = wpref.getSelected();

		checkConfigureAllowed(wpref, true);

		try {
			PreferenceVO newVo = new PreferenceVO();
			wpref.transferWritableAttributes(newVo);
			newVo.setPrimaryKey(wpref.getUID());
			if (newVo.getPrimaryKey() == null) {
				newVo.setPrimaryKey(new UID());
			}

			newVo.setUser(userUID);
			if (newVo.getNuclet() != null) {
				// insert == personal (not shared) -> no nuclet assignment
				newVo.setNuclet(null);
			}
			newVo.flagNew();
			DalUtils.updateVersionInformation(newVo, userName);
			prefsProc.insertOrUpdate(newVo);
			newVo = prefsProc.getByPrimaryKey(newVo.getPrimaryKey());

			if (Boolean.TRUE.equals(selected)) {
				newVo.setSelected(prefsProc.selectPreferenceForUser(newVo, userUID, wpref.getLayout()));
			}

			return new Preference(newVo);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosFatalException();
		}
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Update a single preference of the current user.
	 *
	 * @param wpref
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@Override
	public void updatePreference(Preference.WritablePreference wpref) throws CommonPermissionException, CommonFinderException {
		final SecurityCache securityCache = SecurityCache.getInstance();
		final String userName = getCurrentUserName();
		final UID userUID = securityCache.getUserUid(userName);
		final PreferenceVO voDb;
		if (wpref.getSelected() != null) {
			voDb = prefsProc.getByPrimaryKeyWithSelected(wpref.getUID(), userUID);
		} else {
			voDb = prefsProc.getByPrimaryKey(wpref.getUID());
		}
		if (voDb == null) {
			throw new CommonFinderException();
		}

		checkConfigureAllowed(voDb, false);
		PreferenceVO customVoDb = null;

		final Preference prefDb = new Preference(voDb);
		final boolean isShared = prefDb.isShared();
		final boolean isContentEqual = prefDb.equalContents(wpref);
		if (isShared) {
			// search for customization
			customVoDb = prefsProc.getBySharedKeyAndUser(wpref.getUID(), userUID);
		} else {
			// personal unshared pref
			if (!voDb.getUser().equals(userUID)) {
				throw new CommonPermissionException("You does not own these preference");
			}
		}

		final boolean isCustomized = customVoDb != null;

		final PreferenceVO targetVo;

		if (isCustomized) {
			final Preference customPrefDb = new Preference(customVoDb);
			final boolean isCustomEqual = customPrefDb.equalContents(wpref);
			if (isCustomEqual) { // no need to update
				targetVo = null;
			} else {
				if (isContentEqual) { // new content equals the shared content, delete the customization
					targetVo = customVoDb;
					targetVo.flagRemove();
				} else {
					targetVo = customVoDb;
					targetVo.flagUpdate();
				}
			}
		} else {
			// not customized
			if (isShared) {
				if (isContentEqual) { // no need for a new customization
					targetVo = null;
				} else {
					// new customization
					targetVo = new PreferenceVO();
					targetVo.setPrimaryKey(new UID());
					targetVo.setSharedPreference(voDb.getPrimaryKey());
					targetVo.setUser(userUID);
					targetVo.flagNew();
				}
			} else {
				// personal unshared prefs
				if (isContentEqual) { // no need to update
					targetVo = null;
				} else {
					targetVo = voDb;
					targetVo.flagUpdate();
				}
			}
		}

		if (isShared && !RigidUtils.equal(wpref.getNuclet(), voDb.getNuclet())) {
			// Nuclet assignment change found...
			if (securityCache.isSuperUser(userName)) {
				voDb.setNuclet(wpref.getNuclet());
				try {
					DalUtils.updateVersionInformation(voDb, userName);
					prefsProc.insertOrUpdate(voDb);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					throw new NuclosFatalException(e);
				}
			}
		}

		try {
			if (targetVo != null && !targetVo.isFlagUnchanged()) {
				wpref.transferWritableAttributes(targetVo);

				// never update a shared preference here
				assert targetVo.getUser() != null;
				// private or customized prefs are not Nuclet assignable
				targetVo.setNuclet(null);

				if (targetVo.isFlagUpdated() || targetVo.isFlagNew()) {
					DalUtils.updateVersionInformation(targetVo, userName);
					prefsProc.insertOrUpdate(targetVo);
				}
				if (targetVo.isFlagRemoved()) {
					prefsProc.delete(new Delete<UID>(targetVo.getPrimaryKey(), E.PREFERENCE.getUID()));
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosFatalException(e);
		}

		if (wpref.getSelected() != null) {
			if (!voDb.isSelected() && wpref.getSelected()) {
				wpref.setSelected(prefsProc.selectPreferenceForUser(voDb, userUID, prefDb.getLayout()));
			} else if (voDb.isSelected() && !wpref.getSelected()) {
				prefsProc.deselectPreferenceForUser(voDb.getPrimaryKey(), userUID, prefDb.getLayout());
			}
		}
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Update the shared preference with the given customization. (User publish changes.)
	 *
	 * @param wpref
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@Override
	public void updatePreferenceShare(Preference.WritablePreference wpref) throws CommonPermissionException, CommonFinderException {
		final UID mandator = getCurrentMandatorUID();
		final String userName = getCurrentUserName();
		//final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		UID prefUID = wpref.getUID();
		if (prefUID == null) {
			throw new IllegalArgumentException("prefId must not be null");
		}
		if (!SecurityCache.getInstance().getAllowedActions(userName, mandator).contains(Actions.ACTION_SHARE_PREFERENCES)) {
			throw new CommonPermissionException();
		}

		checkConfigureAllowed(wpref, false);

		PreferenceVO voDb = prefsProc.getByPrimaryKey(prefUID);
		Preference prefDb = new Preference(voDb);
		if (voDb == null) {
			throw new CommonFinderException();
		}

		if (!prefDb.isShared()) {
			throw new IllegalArgumentException("Preference is not shared");
		}

		// backup Nuclet
		final UID nuclet = voDb.getNuclet();
		wpref.transferWritableAttributes(voDb);
		voDb.setNuclet(nuclet);
		voDb.flagUpdate();
		try {
			DalUtils.updateVersionInformation(voDb, userName);
			prefsProc.insertOrUpdate(voDb);
			// reset user customizations, if any
			prefsProc.batchResetUserCustomizations(voDb);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Delete multiple preference items.
	 * Shared preferences will be unshared.
	 *
	 * @param prefUIDs
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@Override
	public void deletePreference(UID ...prefUIDs) throws CommonPermissionException, CommonFinderException {

		for (UID prefUID : prefUIDs) {

			final String userName = getCurrentUserName();
			final UID userUID = SecurityCache.getInstance().getUserUid(userName);
			PreferenceVO voDb = prefsProc.getByPrimaryKey(prefUID);
			if (voDb == null) {
				throw new CommonFinderException();
			}

			checkConfigureAllowed(voDb, false);

			final Preference prefDb = getPreference(prefUID);

			final UID prefUIDtoDelete;
			if (prefDb.isShared()) {
				if (prefDb.isCustomized()) {
					prefUIDtoDelete = prefDb.getCustomized();
				} else {
					// preference is shared -> unshare it
					prefUIDtoDelete = prefDb.getUID();
					for (EntityObjectVO<UID> userRole : this.getSharedUserRoles(prefDb.getUID())) {
						this.unSharePreference(prefDb.getUID(), userRole.getFieldUid(E.ROLEPREFERENCE.role));
					}
				}
			} else {
				prefUIDtoDelete = prefDb.getUID();
			}

			try {
				prefsProc.delete(new Delete<UID>(prefUIDtoDelete));
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new NuclosFatalException(e);
			}
		}

	}

	@Override
	public Preference[] findReferencingPreferences(UID... prefUIDs) throws CommonPermissionException, CommonFinderException {

		for (UID prefUID : prefUIDs) {

			final String userName = getCurrentUserName();
			final UID userUID = SecurityCache.getInstance().getUserUid(userName);
			PreferenceVO voDb = prefsProc.getByPrimaryKey(prefUID);
			if (voDb == null) {
				throw new CommonFinderException();
			}

			checkConfigureAllowed(voDb, false);
			final Preference prefDb = getPreference(prefUID);
			final UID prefUIDtoDelete = prefDb.getUID();

			SpringDataBaseHelper dataBaseHelper = SpringDataBaseHelper.getInstance();
			final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			final DbQuery<UID> query = builder.createQuery(UID.class);
			final DbFrom<UID> from = query.from(E.PREFERENCE);
			query.select(from.baseColumn(E.PREFERENCE.getPk()));

			query.addToWhereAsAnd(
				builder.likeUnsafe(from.baseColumn(E.PREFERENCE.json), builder.literal("%" + prefUIDtoDelete + "%"))
			);

			dataBaseHelper.getDbAccess().executeQuery(query);
			final Set<UID> prefUids = new HashSet<>(dataBaseHelper.getDbAccess().executeQuery(query));

			List<Preference> prefList = new ArrayList<>();
			for (UID p : prefUids) {
				prefList.add(new Preference(prefsProc.getByPrimaryKeyWithSelected(p, userUID)));
			}
			Preference[] result = new Preference[prefUids.size()];
			prefList.toArray(result);
			return result;
		}

		return new Preference[0];
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Delete all preferences of the current user.
	 *
	 */
	@Override
	public void deleteMyPreferences() {
		final String userName = getCurrentUserName();
		final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		deletePreferencesForUser(userUID);
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Delete all preferences of the given user.
	 *
	 */
	public void deletePreferencesForUser(UID userUID) {
		try {
			prefsProc.deleteUserPreferences(userUID);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Selects the preference for the current user and layout.
	 *
	 * @param prefUID
	 * @param layoutUID
	 */
	@Override
	public void selectPreference(UID prefUID, UID layoutUID) {
		final String userName = getCurrentUserName();
		final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		prefsProc.selectPreferenceForUser(prefUID, userUID, layoutUID);
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * De-Select the preference for the current user.
	 *
	 * @param prefUID
	 * @param layoutUID
	 */
	@Override
	public void deselectPreference(UID prefUID, final UID layoutUID) {
		final String userName = getCurrentUserName();
		final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		prefsProc.deselectPreferenceForUser(prefUID, userUID, layoutUID);
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Resets all customized preferences of these types and entity for the current user.
	 *
	 * @param entity
	 * @param type
	 */
	@Override
	public void resetCustomizedPreferences(final UID entity, final String... type) {
		final String userName = getCurrentUserName();
		final UID userUID = SecurityCache.getInstance().getUserUid(userName);
		prefsProc.batchDeleteAllCustomizationsForUser(userUID, entity, type);
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * @param prefUID
	 * @return
	 */
	private List<EntityObjectVO<UID>> getSharedUserRoles(UID prefUID) {
		return nucletDalProvider.getEntityObjectProcessor(E.ROLEPREFERENCE).getBySearchExpression(new CollectableSearchExpression(
				SearchConditionUtils.newUidComparison(E.ROLEPREFERENCE.preference, ComparisonOperator.EQUAL, prefUID)));
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Checks if the current user is allowed to modify the given pref.
	 *
	 * @throws CommonPermissionException
	 */
	private void checkConfigureAllowed(PreferenceVO vo, boolean isInsert) throws CommonPermissionException {
		this.checkConfigureAllowed(vo.getApp(), vo.getType(), isInsert);
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Checks if the current user is allowed to modify the given pref.
	 *
	 * @throws CommonPermissionException
	 */
	private void checkConfigureAllowed(Preference.WritablePreference wp, boolean isInsert) throws CommonPermissionException {
		this.checkConfigureAllowed(wp.getApp(), wp.getType(), isInsert);
	}

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Checks if the current user is allowed to modify the given pref.
	 *
	 * @throws CommonPermissionException
	 */
	private void checkConfigureAllowed(String app, String type, boolean isInsert) throws CommonPermissionException {
		if (PreferenceVO.APP_NUCLOS.equals(app)) {
			if (NuclosPreferenceType.CHART.getType().equals(type)) {
				final String userName = getCurrentUserName();
				if (!SecurityCache.getInstance().getAllowedActions(userName, null).contains(Actions.ACTION_CONFIGURE_CHARTS)) {
					throw new CommonPermissionException();
				}
			}
			// New perspectives are only allowed if the user is allowed to configure perspectives
			else if (NuclosPreferenceType.PERSPECTIVE.getType().equals(type) && isInsert) {
				final String userName = getCurrentUserName();
				if (!SecurityCache.getInstance().getAllowedActions(userName, null).contains(Actions.ACTION_CONFIGURE_PERSPECTIVES)) {
					throw new CommonPermissionException();
				}
			}
		}
	}

}
