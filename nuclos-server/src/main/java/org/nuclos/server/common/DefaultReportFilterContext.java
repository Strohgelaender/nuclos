//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.io.Serializable;

import org.nuclos.common.UID;


/**
 * default implementation for {@link ReportFilterContext}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class DefaultReportFilterContext implements ReportFilterContext, Serializable {

	private final String username;
	private final UID entityId;
	private boolean readWrite;

	public DefaultReportFilterContext(final String username) {
		// this reflects the former default when there was no ReportFilterContext
		this(username, null);
		this.readWrite = false;
	}
	
	public DefaultReportFilterContext(final String username, final UID entityId) {
		this.username = username;
		this.entityId = entityId;
	}

	@Override
	public boolean readWrite() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public UID assignedEntityId() {
		return entityId;
	}

	@Override
	public String getUser() {
		return username;
	}

	@Override
	public void setReadWrite(boolean readWrite) {
		this.readWrite = readWrite;
	}

	@Override
	public String toString() {
		return "DefaultReportFilterContext [username=" + username
				+ ", entityId=" + entityId + ", readWrite=" + readWrite + "]";
	}

}
