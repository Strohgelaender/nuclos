package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorUsageVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@DependsOn({"metaDataProvider", "resourceCache"})
public class GenerationCache {

	private boolean generationsLoaded = false;
	
	private List<GeneratorActionVO> lstAllGenerationActions;
	
	// Spring injection
	
	@Autowired
	private NucletDalProvider nucletDalProvider;
	
	// end of Spring injection
	
	GenerationCache() {
	}
	
	@PostConstruct
	final void init() {
		loadGenerations();
	}
	
	private void loadGenerations() {
		List<GeneratorActionVO> lstAllGenerationActionsTemp = new ArrayList<GeneratorActionVO>();
		List<EntityObjectVO<UID>> allUsages = nucletDalProvider.getEntityObjectProcessor(E.GENERATIONUSAGE).getAll();
		for (EntityObjectVO<UID> genAction : nucletDalProvider.getEntityObjectProcessor(E.GENERATION).getAll()) {			
				List<GeneratorUsageVO> genUsages = new ArrayList<GeneratorUsageVO>();
				for (EntityObjectVO<UID> genUsage : allUsages) {
					if (genAction.getPrimaryKey().equals(genUsage.getFieldUid(E.GENERATIONUSAGE.generation))) {
						genUsages.add(
							MasterDataWrapper.getGeneratorUsageVO(new MasterDataVO<UID>(genUsage, false)));
					}
				}
				lstAllGenerationActionsTemp.add(
						MasterDataWrapper.getGeneratorActionVO(new MasterDataVO<UID>(genAction, false), genUsages));
		}
		lstAllGenerationActions = lstAllGenerationActionsTemp;
		generationsLoaded = true;
	}
	
	@Cacheable(value="generationActions", key="#uid", condition="#uid != null")
	public GeneratorActionVO getGenerationAction(UID uid) {
		GeneratorActionVO retVal = null;
		
		for (GeneratorActionVO curjob : getGeneratorActions()) {
			if (uid.equals(curjob.getId())) {
				retVal = curjob;
				break;
			}
		}
		
		return retVal;
	}
	
	public List<GeneratorActionVO> getGeneratorActions() {
		if (!generationsLoaded) {
			loadGenerations();
		}
		return new ArrayList<GeneratorActionVO>(this.lstAllGenerationActions);
	}

	
	@CacheEvict(value="generationActions", allEntries=true) 
	public void evict() {}
	
	public void invalidate() {
		evict();
		this.lstAllGenerationActions.clear();
		this.generationsLoaded = false;
	}
	
}
