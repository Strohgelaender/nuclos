//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.Priority;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.RuleNotification;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DataSourceCaseSensivity;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.LockUtils;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.RecordGrantUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dal.processor.jdbc.impl.EOSearchExpressionUnparser;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.impl.SQLUtils2;
import org.nuclos.server.dblayer.query.DbCompoundColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbOrder;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.statements.DbStructureChange.Type;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbUniqueConstraint;
import org.nuclos.server.dblayer.structure.DbIndex;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class NuclosFacadeBean implements NuclosFacadeLocal {
	
	protected static final Logger LOG = LoggerFactory.getLogger(NuclosFacadeBean.class);

	// Spring injected

	@Autowired
	protected SecurityCache securityCache;
	
	@Autowired
	protected MetaProvider metaProvider;
	
	@Autowired
	protected NucletDalProvider nucletDalProvider;

	@Autowired
	protected RecordGrantUtils grantUtils;
	
	@Autowired
	protected LockUtils lockUtils;

	@Autowired
	protected AnnotationJaxb2Marshaller jaxb2Marshaller;

	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	@Autowired
	private NuclosRemoteContextHolder remoteCtx;
	
	@Autowired
	private Modules modules;
	
	@Autowired
	private DataLanguageCache dlCache;

	// end Spring injected
	
	public NuclosFacadeBean() {
	}
	
	/**
	 * @return the name of the current user. Shortcut for <code>this.getSessionContext().getCallerPrincipal().getName()</code>.
	 */
	public final String getCurrentUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}

	/**
	 * No @Autowired, must be lazy.
	 */
	private EventSupportFacadeLocal eventSupportFacade;

	protected final EventSupportFacadeLocal getEventSupportFacade() {
		if (eventSupportFacade == null) {
			eventSupportFacade = SpringApplicationContextHolder.getBean(EventSupportFacadeLocal.class);
		}
		return eventSupportFacade;
	}

	public boolean isCalledRemotely() {
		Boolean peek = remoteCtx.peek();
		return peek != null ? remoteCtx.peek() : Boolean.FALSE;
	}

	public Object getSavePoint() {
		return userCtx.getSavePoint();
	}

	public InternalTimestamp getSavePointTime() {
		return userCtx.getSavePointTime();
	}
	
	public UID getCurrentMandatorUID() {
		return userCtx.getMandatorUID();
	}
	
	public void setCurrentMandatorUID(UID mandatorUID) {
		userCtx.setMandatorUID(mandatorUID);
	}

	/**
	 * checks if it is allowed to read a genericobject
	 *
	 * @throws CommonPermissionException if reading of the entity and the special genericobject is not allowed for the current user.
	 */
	protected void checkReadAllowedForModule(UID iModuleId) throws CommonPermissionException {
		if (isCalledRemotely()) {
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			if (!securityCache.isReadAllowedForModule(user, iModuleId, userCtx.getMandatorUID())) {
				throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.1", 
					user, "", modules.getLabel(iModuleId)));
			}
		}
	}

	/**
	 * checks if it is allowed to write a genericobject
	 *
	 * @throws CommonPermissionException if writing of the entity and the special genericobject is not allowed for the current user.
	 */
	protected void checkWriteAllowedForModule(UID iModuleId) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			if (!securityCache.isWriteAllowedForModule(user, iModuleId, userCtx.getMandatorUID())) {
				throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.2",
					user, "", modules.getLabel(iModuleId)));
			}
		}
	}

	/**
	 * checks if it is allowed to delete a genericobject
	 *
	 * @throws CommonPermissionException if deleting of the entity and the special genericobject is not allowed for the current user.
	 */
	protected void checkDeleteAllowedForModule(UID iModuleId, Long iGenericObjectId, boolean bDeletePhysically) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			if (iGenericObjectId == null) {
				throw new NullArgumentException("iGenericObjectId");
			}
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			String sMessage;
			if (!securityCache.isDeleteAllowedForModule(user, iModuleId, bDeletePhysically, userCtx.getMandatorUID())) {
				if (!bDeletePhysically) {
					sMessage = StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.5",
						user, "", modules.getLabel(iModuleId));
				}
				else {
					sMessage = StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.6",
						user, "", modules.getLabel(iModuleId));
				}
				throw new CommonPermissionException(sMessage);
			}
		}
	}

	/**
	 * Checks if the current user is allowed to read ALL of the given entities (masterdata or genericobject or dynamic task list)
	 *
	 * @throws CommonPermissionException if reading of any of the given entities is not allowed for the current user.
	 */
	protected void checkReadAllowed(EntityMeta<?>...entities) throws CommonPermissionException {
		checkReadAllowed(toUidArray(entities));
	}
	
	/**
	 * Checks if the current user is allowed to read ALL of the given entities (masterdata or genericobject or dynamic task list)
	 *
	 * @throws CommonPermissionException if reading of any of the given entities is not allowed for the current user.
	 */
	protected void checkReadAllowed(UID...entities) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());

			for (final UID entity : entities) {
				String sEntityLabel;
				if (modules.isModule(entity)) {
					sEntityLabel = modules.getLabel(entity);
					if (securityCache.isReadAllowedForModule(user, entity, userCtx.getMandatorUID())) {
						continue;
					}
				} else {
					sEntityLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(metaProvider.getEntity(entity));
					if (securityCache.isReadAllowedForMasterData(user, entity, userCtx.getMandatorUID())) {
						continue;
					}
				}

				if (securityCache.isReadAllowedForDynamicTaskList(entity, user, getCurrentMandatorUID())) {
					continue;
				}

				throw new CommonPermissionException(
						StringUtils.getParameterizedExceptionMessage(
								"nucleus.facade.permission.exception.7",
								user,
								sEntityLabel
						)
				);
			}

		}
	}

	protected void checkWriteAllowed(EntityMeta<?>...entities) throws CommonPermissionException {
		checkWriteAllowed(toUidArray(entities));
	}
	
	protected void checkWriteAllowed(UID...entities) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			boolean isWriteAllowed = false;
			for (int i = 0; i < entities.length; i++) {
				final UID entity = entities[i];
				String sEntityLabel;
				if (modules.isModule(entity)) {
					sEntityLabel = modules.getLabel(entity);
					if(securityCache.isWriteAllowedForModule(user, entity, userCtx.getMandatorUID())) {
					  isWriteAllowed = true;
					  break;
					}
				}
				else {
					sEntityLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(metaProvider.getEntity(entity));
					if(securityCache.isWriteAllowedForMasterData(user, entity, userCtx.getMandatorUID())) {
					  isWriteAllowed = true;
					  break;
					}
				}
				if (!isWriteAllowed) {
					throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage(
							"nucleus.facade.permission.exception.8", user, sEntityLabel));
				}
			}
		}
	}
	
	protected void checkDeleteAllowed(EntityMeta<?>...entities) throws CommonPermissionException {
		checkDeleteAllowed(toUidArray(entities));
	}

	protected void checkDeleteAllowed(UID...entities) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			boolean isDeleteAllowed = false;
			for (int i = 0; i < entities.length; i++) {
				final UID entity = entities[i];
				String sEntityLabel;
				if(modules.isModule(entity)) {
					sEntityLabel = modules.getLabel(entity);
					if (securityCache.isDeleteAllowedForModule(user, entity, true, userCtx.getMandatorUID())) {
					  isDeleteAllowed = true;
					  break;
					}
				}
				else {
					sEntityLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(metaProvider.getEntity(entity));
					if (securityCache.isDeleteAllowedForMasterData(user, entity, userCtx.getMandatorUID())) {
					  isDeleteAllowed = true;
					  break;
					}
				}
				if (!isDeleteAllowed) {
					throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.9", user, sEntityLabel));
				}
			}
		}
	}
	
	private static UID[] toUidArray(EntityMeta<?>...entities) {
		return CollectionUtils.transformArray(entities, UID.class, new Transformer<EntityMeta<?>, UID>(){
			@Override
			public UID transform(EntityMeta<?> e) {
				return e.getUID();
			}
		});
	}

	protected boolean isInRole(String sRoleName) {
		for (GrantedAuthority ga : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
			if (ga.getAuthority().equals(sRoleName)) {
				return true;
			}
		}
		return false;
	}

	protected List<DbConstraint> getConstraints(DbAccess dbAccess) {
		MetaDbHelper helper = new MetaDbHelper(E.getSchemaHelperVersion(), dbAccess, metaProvider, null, null);
		
		List<DbConstraint> result = new ArrayList<DbConstraint>();
		for (EntityMeta<?> eMeta : metaProvider.getAllEntities()) {
			DbTable dbtable = helper.getDbTable(eMeta);
			if (dbtable != null) {
				result.addAll(dbtable.getTableArtifacts(DbForeignKeyConstraint.class));
				result.addAll(dbtable.getTableArtifacts(DbUniqueConstraint.class));
			}
		}
		return result;
	}
	
	protected void createOrDropConstraints(List<DbConstraint> lstConstraints, boolean bCreate, DbAccess dbAccess, List<String> result) {
		String action = bCreate ? "CREATE" : "DROP";
		LOG.info("Starting {} of {} constraints.", action, lstConstraints.size());
		
		int iFailed = 0;
		for (DbConstraint c : lstConstraints) {
			String name = c.getConstraintName();
			String details = c.toString();
			if (!bCreate) {
				try {
					dbAccess.execute(new DbStructureChange(Type.DROP, c));
					if (result != null) {
						LOG.info("{} successfully dropped.", name );
						result.add(name + " successfully dropped.");						
					} else {
						LOG.debug("{} successfully dropped.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not dropped! {}", name, details, ex);
						result.add(name + " not dropped! " + details + " (" + ex.getMessage() + ")");						
					} else {
						//Failed droppings from import/exportDB  are logged without complete Stack-Trace
						LOG.warn("{} not dropped! {}", name, details);
					}
					iFailed++;
				}
				
			} else {
				try {
					dbAccess.execute(new DbStructureChange(Type.CREATE, c));
					if (result != null) {
						LOG.info("{} successfully created.", name);
						result.add(name + " successfully created.");						
					} else {
						LOG.debug("{} successfully created.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not created! {}", name, details, ex);
						result.add(name + " not created! " + details + " (" + ex.getMessage() + ")");						
					} else {
						LOG.warn("{} not created! {}", name, details, ex);
					}
					iFailed++;
				}			
				
			}
		}
		LOG.info("Finished {} of {} constraints. Failed: {}",
		         action, lstConstraints.size(), iFailed);
	}
	
	protected void createOrDropConstraints(List<DbConstraint> lstConstraints, boolean bCreate, PersistentDbAccess dbAccess, List<String> result) {
		String action = bCreate ? "CREATE" : "DROP";
		LOG.info("Starting {} of {} constraints.", action, lstConstraints.size());
		
		int iFailed = 0;
		for (DbConstraint c : lstConstraints) {
			String name = c.getConstraintName();
			String details = c.toString();
			if (!bCreate) {
				try {
					dbAccess.execute(new DbStructureChange(Type.DROP, c));
					if (result != null) {
						LOG.info("{} successfully dropped.", name);
						result.add(name + " successfully dropped.");						
					} else {
						LOG.debug("{} successfully dropped.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not dropped! {}", name, details, ex);
						result.add(name + " not dropped! " + details + " (" + ex.getMessage() + ")");						
					} else {
						//Failed droppings from import/exportDB  are logged without complete Stack-Trace
						LOG.warn("{} not dropped! {}", name, details);
					}
					iFailed++;
				}
				
			} else {
				try {
					dbAccess.execute(new DbStructureChange(Type.CREATE, c));
					if (result != null) {
						LOG.info("{} successfully created.", name);
						result.add(name + " successfully created.");						
					} else {
						LOG.debug("{} successfully created.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not created! {}", name, details, ex);
						result.add(name + " not created! " + details + " (" + ex.getMessage() + ")");						
					} else {
						LOG.warn("{} not created! {}", name, details, ex);
					}
					iFailed++;
				}			
				
			}
		}
		LOG.info("Finished {} of {} constraints. Failed: {}", action, lstConstraints.size(), iFailed);
	}
	
	protected List<DbIndex> getIndexes(DbAccess dbAccess) {
		MetaDbHelper helper = new MetaDbHelper(E.getSchemaHelperVersion(), dbAccess, metaProvider, null, null);
		
		List<DbIndex> result = new ArrayList<DbIndex>();
		for (EntityMeta<?> eMeta : metaProvider.getAllEntities()) {
			DbTable dbtable = helper.getDbTable(eMeta);
			if (dbtable != null) {
				result.addAll(dbtable.getTableArtifacts(DbIndex.class));
			}
		}
		return result;
	}
	
	protected void createOrDropIndexes(List<DbIndex> indexes, boolean bCreate, PersistentDbAccess dbAccess, List<String> result) {
		String action = bCreate ? "CREATE" : "DROP";
		LOG.info("Starting {} of {} indexes.", action, indexes.size());
		
		int iFailed = 0;
		for (DbIndex index : indexes) {
			String name = index.getIndexName();
			String details = index.toString();
			if (!bCreate) {
				try {
					dbAccess.execute(new DbStructureChange(Type.DROP, index));
					if (result != null) {
						LOG.info("{} successfully dropped.", name);
						result.add(name + " successfully dropped.");						
					} else {
						LOG.debug("{} successfully dropped.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not dropped! {}", name, details, ex);
						result.add(name + " not dropped! " + details + " (" + ex.getMessage() + ")");						
					} else {
						//Failed droppings from import/exportDB  are logged without complete Stack-Trace
						LOG.warn("{} not dropped! {}", name, details);
					}
					iFailed++;
				}
			} else {
				try {
					dbAccess.execute(new DbStructureChange(Type.CREATE, index));
					if (result != null) {
						LOG.info("{} successfully created.", name);
						result.add(name + " successfully created.");						
					} else {
						LOG.debug("{} successfully created.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not created! {}", name, details, ex);
						result.add(name + " not created! " + details + " (" + ex.getMessage() + ")");						
					} else {
						LOG.warn("{} not created! {}", name, details, ex);
					}
					iFailed++;
				}			
			}
		}
		LOG.info("Finished {} of {} indexes. Failed:{}",
		         action, indexes.size(), iFailed);
	}

	/**
	 * TODO: Too many parameters!
	 * TODO: Method is much too complex - split it up appropriately
	 */
	protected List<CollectableValueIdField> getReferenceList(
			SpringDataBaseHelper dataBaseHelper,
			DatasourceServerUtils datasourceServerUtils,
			SessionUtils sessionUtils,
			FieldMeta<?> efMeta,
			String search,
			UID vlpUID,
			Map<String, Object> vlpParameter,
			String defaultValueColumn,
			Long iMaxRowCount,
			UID mandator,
			boolean bComesFromQuickSearch
	) throws CommonBusinessException {
		List<CollectableValueIdField> result = new ArrayList<>();

		//NUCLOS-4687 the root cause is fixed. This is a fallback.
		if (efMeta != null && efMeta.getEntity() == null) {
			efMeta = metaProvider.getEntityField(efMeta.getUID());
		}

		if (efMeta != null) {
			UsageCriteria usage = new UsageCriteria(efMeta.getEntity(), null, null, null);
			boolean readAllowed = securityCache.isReadOrWriteAllowedForField(efMeta, usage, false, getCurrentUserName(), getCurrentMandatorUID());
			if (!readAllowed) {
				throw new CommonPermissionException();
			}
		}

		final DbAccess access = dataBaseHelper.getDbAccess();
		final DbQueryBuilder builder = access.getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<?> from = null;

		final UID foreignEntityUID = efMeta != null ? (efMeta.getForeignEntity() != null ? efMeta.getForeignEntity() : efMeta.getLookupEntity()) : null;
		EntityMeta eForeignMeta = null;
		if (metaProvider.checkEntity(foreignEntityUID)) {
			eForeignMeta = metaProvider.getEntity(foreignEntityUID);
		}

		final DatasourceVO dsvo = vlpUID != null ? DatasourceCache.getInstance().getValuelistProvider(vlpUID, true) : null;
		final String vlpIdField = vlpParameter != null ? (String) vlpParameter.get(ValuelistProviderVO.DATASOURCE_IDFIELD) : null;
		final String vlpNameField = vlpParameter != null ? (String) vlpParameter.get(ValuelistProviderVO.DATASOURCE_NAMEFIELD) : null;
		final boolean bWithVlpNameField = !StringUtils.isNullOrEmpty(vlpNameField);
		final boolean bIsUidPk = (eForeignMeta != null && eForeignMeta.isUidEntity()) || SF.PK_UID.getDbColumn().equalsIgnoreCase(vlpIdField);

		// Localized stringified ref, if localization is active for the entity.
		DbExpression stringifiedRef = null;
		boolean hasDataLanguage = false;

		String vlpIdColumn2Use = null;
		String defaultColumn2Use = null;
		boolean bUseDefaultOrder = true;
		DbExpression defaultValueExpr = null;
		if (dsvo != null && dsvo.getValid()) {
			// Ein VLP muss immer verwendet werden und wird hier per INNER Join eingebunden (NUCLOS-6567)

			for (DatasourceParameterVO paramVO : datasourceServerUtils.getParameters(dsvo.getSource())) {
				if (!vlpParameter.containsKey(paramVO.getParameter())) {
					// Fehlende Parameter vom Client werden so zumindest mit 'null' initiert und im SQL korrekt ersetzt!
					vlpParameter.put(paramVO.getParameter(), null);
				}
				if (paramVO.getDatatype().equals("java.lang.Integer")) {
					Object intValue = vlpParameter.get(paramVO.getParameter());
					if (intValue == null) {
						// NUCLOS-5010 (VLPs erzeugen unterschiedliche SQLs für Webclient, RichclientComboBox und RichClientLOV)
						// Abwärtskompatibilität: Integer Parameter müssen wenn leer mit 0 befüllt sein. Das stellen wir hier sicher...
						vlpParameter.put(paramVO.getParameter(), 0);
					}
				}
			}

			final String sql = datasourceServerUtils.createSQL(dsvo, vlpParameter, mandator, userCtx.getDataLocal());
			vlpIdColumn2Use = RigidUtils.looksEmpty(vlpIdField) ?null: DataSourceCaseSensivity.getFieldWithCorrectCaseAndQuotation(vlpIdField, sql);

			from = query.from(sql, "VLP");
			if (vlpIdColumn2Use != null) {
				if (bIsUidPk) {
					query.addSelection(from.baseColumn(SimpleDbField.create(vlpIdColumn2Use, UID.class)));
				} else {
					query.addSelection(from.baseColumn(SimpleDbField.create(vlpIdColumn2Use, Object.class)));
				}
			}

			if (bWithVlpNameField) {
				String nameColumn2Use = DataSourceCaseSensivity.getFieldWithCorrectCaseAndQuotation(vlpNameField, sql);
				final DbField<String> nameDbField = SimpleDbField.create(nameColumn2Use, String.class);
				stringifiedRef = from.baseColumn(nameDbField);
				query.addSelection(stringifiedRef);
			} else {
				stringifiedRef = new DbCompoundColumnExpression(from, efMeta, false, bComesFromQuickSearch);
				query.addSelection(stringifiedRef);
			}
			// VLP Sortierung kann nur gewährleistet werden, wenn dieser ein 'vlpNameField' hat, weil dann die Tabelle nicht zusätzlich gejoint wird.
			// SQL bedingte Einschränkung, geht leider
			final boolean bUseCustomOrder = bWithVlpNameField &&
					Pattern.compile("(?i)ORDER\\s+(?i)BY").matcher(sql).find();
			if (bUseCustomOrder) {
				bUseDefaultOrder = false;
				// Sortierung wird vom VLP übernommen
			}

			if (defaultValueColumn != null) {
				defaultColumn2Use = DataSourceCaseSensivity.getFieldWithCorrectCaseAndQuotation(defaultValueColumn, sql);
				final DbField<String> defaultDbField = SimpleDbField.create(defaultColumn2Use, String.class);
				defaultValueExpr = from.baseColumn(defaultDbField);
			}
		}
		final boolean bVlpInUse = from != null;

		if (eForeignMeta != null) {
			final TableAliasSingleton tas = TableAliasSingleton.getInstance();
			final String alias = tas.getAlias(efMeta);

			final boolean bTableInUse;
			DbJoin tableJoin = null;
			if (bVlpInUse) {
				if (!bWithVlpNameField) {
					// join table
					tableJoin = from.join(eForeignMeta, JoinType.INNER, alias).on(
							SimpleDbField.create(vlpIdColumn2Use, Object.class),
							SimpleDbField.create(bIsUidPk ? SF.PK_UID.getDbColumn() : SF.PK_ID.getDbColumn(), Object.class));
					bTableInUse = true;
				} else {
					bTableInUse = false;
				}
			} else {
				from = query.from(eForeignMeta, alias);
				query.addSelection(from.baseColumn(eForeignMeta.getPk()));
				bTableInUse = true;
			}

			if (bTableInUse) {
				final EOSearchExpressionUnparser unparser = new EOSearchExpressionUnparser(query, eForeignMeta, sessionUtils, dlCache);
				for (RefJoinCondition rjc : TableAliasSingleton.getInstance().getRefJoinCondition(efMeta)) {
					unparser.addRefJoinCondition(rjc);
				}
				final FieldMeta<?> efDeleted = SF.LOGICALDELETED.getMetaData(eForeignMeta.getUID());
				if (efDeleted != null && metaProvider.getEntity(eForeignMeta.getUID()).getFields().contains(efDeleted)) {
					final CollectableSearchCondition condSearchDeleted = SearchConditionUtils.newComparison(efDeleted, ComparisonOperator.EQUAL, Boolean.FALSE);
					unparser.unparseSearchCondition(condSearchDeleted);
				}

				DbCompoundColumnExpression untranslatedStringifiedRef = new DbCompoundColumnExpression(
						tableJoin != null ? tableJoin : from,
						efMeta,
						String.class,
						false,
						bComesFromQuickSearch
				);

				// TODO: It is not safe to rely on the size of the selection list in order
				// to determine wether the data language feature is in use.
				if (query.getSelections().size() > 1) {
					// data language column added from unparser ...
					hasDataLanguage = true;
					stringifiedRef = (DbExpression) query.getSelections().get(1);
				} else {
					stringifiedRef = untranslatedStringifiedRef;
				}

				// Always add the unlocalized stringified reference as a fallback (in case the localization is incomplete)
				// TODO: This could be COALESCE'd with the data language selection (if it exists), so that we always fetch only a single column
				query.addSelection(untranslatedStringifiedRef);
			}

			if (!bVlpInUse && bTableInUse && eForeignMeta.isMandator()) {
				final Set<UID> mandators;
				if (mandator == null) {
					mandators = securityCache.getAccessibleMandators(getCurrentMandatorUID());
				} else {
					mandators = MandatorUtils.getAccessibleMandatorsByLogin(mandator, getCurrentMandatorUID());
				}
				query.addToWhereAsAnd(builder.in(from.baseColumn(SF.MANDATOR_UID), mandators));
			}
		}
		
		if (iMaxRowCount != null) {
			query.limit(iMaxRowCount);	
		} else {
			query.limit(10000L);
		}

		if (stringifiedRef != null && search != null && search.trim().length() > 0) {
			final String wildcard = access.getWildcardLikeSearchChar();
			search = search.replace("*", wildcard);
			search = search.replace("?", "_");
			search = wildcard + SQLUtils2.escape(StringUtils.toUpperCase(search)) + wildcard;
			final DbCondition condLike = builder.like(builder.upper(stringifiedRef), search);
			query.addToWhereAsAnd(condLike);
		}

		if (defaultValueExpr != null) {
			query.addSelection(defaultValueExpr);
		}

		if (bUseDefaultOrder) {
			final DbOrder order = builder.asc(stringifiedRef);
			query.orderBy(order);
		}


		
		for (DbTuple tuple : access.executeQuery(query)) {
			int iValueIndex = 0;
			final Object id;
			if (!bVlpInUse || vlpIdColumn2Use != null) {
				iValueIndex++;
				if (bIsUidPk) {
					id = tuple.get(0, UID.class);
				} else {
					Object oId = tuple.get(0, Object.class);
					if (oId instanceof Long) {
						id = oId;
					} else if (oId instanceof BigDecimal) {
						id = ((BigDecimal) oId).longValue();
					} else if (oId instanceof Integer) {
						id = ((Integer) oId).longValue();
					} else if (oId instanceof Double) {
						id = ((Double) oId).longValue();
					} else {
						if (eForeignMeta == null) {
							// VLP auf einem Textfeld, dann ist die ID nicht relevant (könnte auch eine UID sein)
							id = oId;
						} else {
							id = Long.parseLong(oId.toString());
						}
					}
				}
			} else {
				id = null;
			}

			Object value = tuple.get(iValueIndex);
			if (value == null && hasDataLanguage) {
				// The next column is the fallback.
				value = tuple.get(iValueIndex + 1);
			}

			final CollectableValueIdField field = new CollectableValueIdField(id, value);
			if (defaultValueExpr != null) {
				field.setSelected(RigidUtils.parseBoolean(tuple.get(defaultColumn2Use)));
			}
			result.add(field);
		}
		
		return result;
	}

	// NUCLOS-6134, NUCLOS-6140
	public <PK> void removeValuesFromForbiddenSubformColumns(UID iStatus, UID subentity, Collection<EntityObjectVO<PK>> eosVos) {
		if (iStatus == null) {
			return;
		}

		Map<UID, SubformPermission> permissions = securityCache.getSubForm(getCurrentUserName(), subentity, getCurrentMandatorUID());
		SubformPermission subformPermission = permissions.get(iStatus);

		Collection<UID> fields2Clear;

		if (subformPermission == null || !subformPermission.includesReading()) {
			fields2Clear = null;
		} else {
			Collection<UID> readAllowedGroups = subformPermission.getReadAllowedGroups();
			fields2Clear = readAllowedGroups == null ? Collections.<UID>emptySet() :
					metaProvider.getAllFieldsByEntityFilteredByGroups(subentity, readAllowedGroups, false, false);
		}

		for (EntityObjectVO<PK> eovo : eosVos) {
			eovo.clearFields(fields2Clear);
		}
	}

	public void validateDefaultMandatory(NucletFieldMeta<?> fieldMeta,
										 FieldMetaVO fieldMetaVOToAdjust,
										 NucletFieldMeta<?> fieldMetaToAdjust,
										 SpringDataBaseHelper dataBaseHelper, DatasourceServerUtils datasourceServerUtils, SessionUtils sessionUtils) {
		if (!metaProvider.checkEntity(fieldMeta.getForeignEntity())) {
			// foreignEntity is being created... ignore validation this time!
			return;
		}
		String refValue;
		String oldDefaultMandatory = fieldMeta.getDefaultMandatory();
		try {
			EntityMeta<?> foreignEntity = metaProvider.getEntity(fieldMeta.getForeignEntity());
			// NUCLOS-6599 There are actually UID-foreign keys, too
			EntityObjectVO<?> eo;
			if (foreignEntity.isUidEntity()) {
				UID defaultMandatoryUid = UID.parseUID(oldDefaultMandatory);
				eo = nucletDalProvider.getEntityObjectProcessor(fieldMeta.getForeignEntity()).getByPrimaryKey(defaultMandatoryUid);
			} else {
				Long defaultMandatoryId = Long.parseLong(oldDefaultMandatory);
				eo = nucletDalProvider.getEntityObjectProcessor(fieldMeta.getForeignEntity()).getByPrimaryKey(defaultMandatoryId);				
			}
			
			if (eo != null) {
				refValue = RefValueExtractor.get(eo, fieldMeta.getForeignEntityField(), null);
				fieldMeta.setDefaultMandatory(refValue);
				fieldMeta.flagUpdate();
			}
		} catch (NumberFormatException | CommonFatalException nfe) {
			LOG.debug(nfe.getMessage(), nfe);
		}

		List<CollectableValueIdField> listFound = new ArrayList<>();
		try {
			listFound = getReferenceList(
					dataBaseHelper,
					datasourceServerUtils,
					sessionUtils,
					fieldMeta,
					fieldMeta.getDefaultMandatory(),
					null,
					null,
					null,
					1L,
					null,
					false
			);
		} catch (CommonBusinessException | CommonFatalException be) {
			LOG.error(be.getMessage());
		}
		if (listFound.size() > 0) {
			if (fieldMetaToAdjust != null) {
				fieldMetaToAdjust.setDefaultMandatory(listFound.get(0).getValueId().toString());
			}
			if (fieldMetaVOToAdjust != null) {
				fieldMetaVOToAdjust.setDefaultMandatory(listFound.get(0).getValueId().toString());
			}
		} else {
			fieldMeta.setDefaultMandatory(oldDefaultMandatory);
			fieldMeta.flagUpdate();
			String sBusinessObject = "UID=" + fieldMeta.getEntity().getString();
			try {
				sBusinessObject = metaProvider.getEntity(fieldMeta.getEntity()).getEntityName();
			} catch (Exception ex) {
				LOG.debug(ex.getMessage(), ex);
			}
			String message = "Default mandatory value '" + fieldMeta.getDefaultMandatory() + "' for attribute '" + fieldMeta.getFieldName() + "' of business object '" + sBusinessObject + "' not found.";
			LOG.error(message);
			NuclosJMSUtils.sendObjectMessageAfterCommit(new RuleNotification(Priority.HIGH, message, "Nuclos Server"), JMSConstants.TOPICNAME_RULENOTIFICATION, SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
		}
	}

	protected final <PK> List<String> getVersionConflictMessages(String type, EntityObjectVO<PK> newVo, EntityObjectVO<PK> oldVo) {
		return metaProvider.getVersionConflictMessages(type, newVo, oldVo);
	}

	protected final void createSavepoint() {
		userCtx.createSavepoint();
	}
}
