package org.nuclos.server.common.mail;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.mail.NuclosMail;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.MailSendException;
import org.nuclos.server.common.mail.properties.MailConnectionProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NuclosMailSender {
	private final static Logger LOG = LoggerFactory.getLogger(NuclosMailHandler.class);

	final MailConnectionProperties properties;

	public NuclosMailSender(final MailConnectionProperties properties) {
		this.properties = properties;
	}

	/**
	 *
	 * @param mail
	 * @param sender
	 * @throws MailSendException
	 */
	public void sendMail(final NuclosMail mail, final String sender) throws MailSendException {
		sendMail(mail, sender, null);
	}

	/**
	 *
	 * @param mail
	 * @param sender
	 * @param emailSignature
	 * @throws MailSendException
	 */
	public void sendMail(final NuclosMail mail, final String sender, final String emailSignature) throws MailSendException {
		if (StringUtils.isBlank(mail.getFrom())) {
			mail.setFrom(sender);
		}

		sendMail(
				properties.getHost(),
				properties.getPort(),
				properties.isSsl(),
				properties.getUser(),
				properties.getPassword(),
				mail,
				emailSignature
		);
	}

	/**
	 *
	 * @param smtpHost
	 * @param smtpPort
	 * @param login
	 * @param password
	 * @param mail
	 * @throws CommonBusinessException
	 */
	public void sendMail(final String smtpHost, final Integer smtpPort, final boolean isSsl, final String login, final String password, final NuclosMail mail, final String emailSignature) throws MailSendException {
		try{
			if (smtpHost == null) {
				throw new MailSendException("mailsender.error.3");
			}
			Properties properties = new Properties();
			properties.put("mail.smtp.host", smtpHost);
			if (smtpPort != null) {
				properties.put("mail.smtp.port", smtpPort.intValue());
			}
			
			if (isSsl) {
				properties.put("mail.smtp.ssl.enable", "true");
			}

			Authenticator auth = null;
			if (login != null) {
				properties.put("mail.smtp.user", login);
				properties.put("mail.smtp.starttls.enable", "false");
				properties.put("mail.smtp.auth", "true");
				auth = new Authenticator() {

					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(login, password);
					}
				};
			}
			Session session = Session.getInstance(properties, auth);

			MimeMultipart content = new MimeMultipart();
			MimeBodyPart text = getBodyPart(mail.getMessage(), mail);
			content.addBodyPart(text);

			if (!StringUtils.isBlank(emailSignature)) {
				MimeBodyPart sig = getBodyPart(emailSignature, mail);
				content.addBodyPart(sig);
			}

			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(mail.getFrom()));

			List<String> to = mail.getRecipients();
			InternetAddress[] recipients = new InternetAddress[to.size()];
			for (int i = 0; i < to.size(); i++) {
				recipients[i] = new InternetAddress(to.get(i));
			}
			msg.setRecipients(Message.RecipientType.TO, recipients);

			List<String> toCC = mail.getRecipientsCC();
			InternetAddress[] recipientsCC = new InternetAddress[toCC.size()];
			for (int i = 0; i < toCC.size(); i++) {
				recipientsCC[i] = new InternetAddress(toCC.get(i));
			}
			msg.setRecipients(Message.RecipientType.CC, recipientsCC);

			List<String> toBCC = mail.getRecipientsBCC();
			InternetAddress[] recipientsBCC = new InternetAddress[toBCC.size()];
			for (int i = 0; i < toBCC.size(); i++) {
				recipientsBCC[i] = new InternetAddress(toBCC.get(i));
			}
			msg.setRecipients(Message.RecipientType.BCC, recipientsBCC);

			if (mail.getReplyTo() != null) {
				InternetAddress[] replies = new InternetAddress[1];
				replies[0] = new InternetAddress(mail.getReplyTo());
				msg.setReplyTo(replies);
			}
			else {
				InternetAddress[] replies = new InternetAddress[1];
				replies[0] = new InternetAddress(mail.getFrom());
				msg.setReplyTo(replies);
			}

			msg.setSubject(mail.getSubject());
			msg.setContent(content);
			msg.setHeader("MIME-Version", "1.0");
			msg.setHeader("Content-Type", content.getContentType());

			for (org.nuclos.api.common.NuclosFile attachment : mail.getAttachments()) {
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				DataSource dataSource = new ByteArrayDataSource(attachment.getContent(), "application/octet-stream");
				messageBodyPart.setDataHandler(new DataHandler(dataSource));
				messageBodyPart.addHeader("Content-Type", "application/octet-stream ; charset= UTF-8");
				try {
					messageBodyPart.setFileName(MimeUtility.encodeText(
							attachment.getName(),
							StandardCharsets.UTF_8.name(),
							"Q"
					));
				} catch (UnsupportedEncodingException e) {
					LOG.warn("Failed to encode filename as UTF-8: " + attachment.getName());
					messageBodyPart.setFileName(attachment.getName());
				}
				content.addBodyPart(messageBodyPart);
			}


			Transport.send(msg);
		} catch (MessagingException | RuntimeException e) {
			LOG.error("Sending email failed.", e);
			throw new MailSendException(
					org.nuclos.common2.StringUtils.getParameterizedExceptionMessage(
							"mailsender.error.1",
							e.getMessage()
					)
			);
		}
	}

	private static MimeBodyPart getBodyPart(String body, NuclosMail mail) throws MessagingException {
		MimeBodyPart text = new MimeBodyPart();

		text.setText(body, mail.getCharset() != null ? mail.getCharset() : "UTF-8");
		text.setHeader("MIME-Version","1.0");

		// set Content-Type
		String contentType = mail.getHeader("Content-Type");
		if (null == contentType) {
			// default text/plain
			contentType = "text/plain; charset=UTF-8";
		}
		if (mail.getCharset() != null && mail.getType() != null) {
			contentType = mail.getType() + "; charset=" + mail.getCharset();
		}

		text.setHeader("Content-Type", contentType);

		return text;
	}
}
