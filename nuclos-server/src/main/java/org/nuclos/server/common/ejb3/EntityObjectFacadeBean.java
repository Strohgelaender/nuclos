//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.UnlockMode;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.LockUtils;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.nuclos.server.common.RecordGrantUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityObjectProcessor;
import org.nuclos.server.dal.processor.proxy.impl.ProxyEntityObjectProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.entityobject.EntityObjectProxyList;
import org.nuclos.server.eventsupport.ejb3.EventSupportCustomResult;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.ProxyListProvider;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Server implementation of the EntityObjectFacadeRemote interface.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("Login")
public class EntityObjectFacadeBean extends NuclosFacadeBean implements EntityObjectFacadeLocal, EntityObjectFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(EntityObjectFacadeBean.class);

	private LayoutFacadeLocal layoutFacade;

	@Autowired
	@Qualifier("masterDataService")
	private MasterDataFacadeRemote masterDataFacade;

	@Autowired
	private MasterDataFacadeLocal masterDataFacadeLocal;

	@Autowired
	private GenericObjectFacadeRemote genericObjectFacade;

	@Autowired
	private SecurityCache securityCache;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private RecordGrantUtils grantUtils;
	
	@Autowired
	private LockUtils lockUtils;
	
	@Autowired
	private DatasourceServerUtils datasourceServerUtils;
	
	@Autowired
	private SessionUtils sessionUtils;
	
	@Autowired
	private ServerParameterProvider serverParameter;
	
	@Autowired
	private MandatorUtils mandatorUtils;

	@Autowired
	private NuclosRemoteContextHolder remoteContext;
	
	public EntityObjectFacadeBean() {
	}
	
	private final MasterDataFacadeRemote getMasterDataFacade() {
		return masterDataFacade;
	}
	
	private void appendMandator(CollectableSearchExpression cse, EntityMeta<?> entity) {
		cse.setSearchCondition(mandatorUtils.append(cse, entity));
	}
	
	@Override
	public void cancelRunningStatements(UID entity) {
		nucletDalProvider.getEntityObjectProcessor(metaProvider.getEntity(entity)).cancelRunningStatements();
	}
	
	@Override
	public <PK> EntityObjectVO<PK> get(UID entity, PK id) throws CommonPermissionException {
		EntityMeta<PK> meta = metaProvider.getEntity(entity);
		if (meta.isStateModel()) {
			checkReadAllowedForModule(meta.getUID());
		} else {
			checkReadAllowed(meta);
		}

		grantUtils.checkInternal(entity, id);
		IEntityObjectProcessor<PK> eop = nucletDalProvider.getEntityObjectProcessor(entity);
		EntityObjectVO<PK> result = eop.getByPrimaryKey(id);
		if (result != null) {
			mandatorUtils.checkReadAllowed(result, meta);
		}
		return result;
	}

	@Override
	public <PK> EntityObjectVO<PK> getReferenced(UID referencingEntity, UID referencingEntityField, PK id) {
		FieldMeta<?> fieldmeta = metaProvider.getEntityField(referencingEntityField);
		if (fieldmeta.getForeignEntity() == null && fieldmeta.getLookupEntity() == null) {
			throw new NuclosFatalException("Field " + referencingEntity + "." + referencingEntityField + " is not a reference or lookup field.");
		} else {
			final IEntityObjectProcessor<PK> eop = nucletDalProvider.getEntityObjectProcessor(
					fieldmeta.getForeignEntity() != null ? fieldmeta.getForeignEntity() : fieldmeta.getLookupEntity());
			final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
			try {
				eop.setIgnoreRecordGrantsAndOthers(true);
				return eop.getByPrimaryKey(id);
			} finally {
				eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
			}
		}
	}

	public List<CollectableValueIdField> getReferenceList(
			FieldMeta<?> efMeta,
			String search,
			UID vlpUID,
			Map<String, Object> vlpParameter,
			String defaultValueColumn,
			Long iMaxRowCount,
			UID mandator
	) throws CommonBusinessException {
		return super.getReferenceList(
				dataBaseHelper,
				datasourceServerUtils,
				sessionUtils,
				efMeta,
				StringUtils.defaultIfNull(search, ""),
				vlpUID,
				vlpParameter,
				defaultValueColumn,
				iMaxRowCount,
				mandator,
				false
		);
	}

	@Override
	public <PK> List<PK> getEntityObjectIds(UID entity, CollectableSearchExpression cse) throws CommonPermissionException {
		final List<PK> ids;
		final String user = getCurrentUserName();
		boolean allowed = true;
		if (isCalledRemotely()) {
			allowed = securityCache.isReadAllowedForMasterData(user, entity, getCurrentMandatorUID());
		}
		if (allowed) {				
			final EntityMeta<?> eMeta = metaProvider.getEntity(entity);
			appendMandator(cse, eMeta);
			ids = nucletDalProvider.<PK>getEntityObjectProcessor(entity).getIdsBySearchExpression(cse);
		} else {
			throw new CommonPermissionException("User " + user + " has not access to " + entity.debugString());
		}
		return ids;
	}

	@Override
	public <PK> EntityObjectVO<PK> getByIdWithDependents(
			UID entity,
			PK id,
			Collection<UID> fields,
			String customUsage
	) throws CommonPermissionException {
		checkReadAllowed(entity);

		final EntityMeta<PK> eMeta = metaProvider.getEntity(entity);
		final EntityObjectVO<PK> eo = nucletDalProvider.getEntityObjectProcessor(eMeta).getByPrimaryKey(id);

		if (eo != null && fields != null) {
			masterDataFacadeLocal.fillDependentsForSubformColumns(Collections.singletonList(eo), fields, entity, customUsage);
		}

		return eo;
	}

	@Override
	public <PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunk(
			UID entity,
			CollectableSearchExpression clctexpr,
			ResultParams resultParams,
			String customUsage
	) throws CommonPermissionException {
		if (!E.NUCLETRELEASENOTE.checkEntityUID(entity)) {
			checkReadAllowed(entity);
		}
		return getEntityObjectsChunkNoCheck(entity, clctexpr, resultParams, customUsage);
	}

	public <PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunkNoCheck(
			UID entity,
			CollectableSearchExpression clctexpr,
			ResultParams resultParams,
			String customUsage
	) {
		
		final EntityMeta<PK> eMeta = metaProvider.getEntity(entity);
		
		IEntityObjectProcessor ieop=nucletDalProvider.getEntityObjectProcessor(eMeta);
		
		if(ieop instanceof JdbcEntityObjectProcessor)
		{
			JdbcEntityObjectProcessor<PK> eop = (JdbcEntityObjectProcessor<PK>) ieop;
			appendMandator(clctexpr, eMeta);
			
			final List<EntityObjectVO<PK>> eos = eop.getChunkBySearchExpressionImpl(clctexpr, resultParams);
			
			if (resultParams.getFields() != null) {
				masterDataFacadeLocal.fillDependentsForSubformColumns(
						eos,
						resultParams.getFields(),
						entity,
						customUsage
				);
			}
			
			return eos;
		} else if(ieop instanceof ProxyEntityObjectProcessor) {
			throw new NotImplementedException("ProxyEntityObjectProcessor unterstützt keine Chunks");
			
		}
		throw new NotImplementedException("EntityObjectProcessor wird nicht unterstützt");
		
	}

	@Override
	public Long countEntityObjectRows(UID entity, CollectableSearchExpression clctexpr) throws CommonPermissionException{
		checkReadAllowed(entity);
		return countEntityObjectRowsNoCheck(entity, clctexpr);
	}

	public Long countEntityObjectRowsNoCheck(UID entity, CollectableSearchExpression clctexpr) {
		final EntityMeta<?> eMeta = metaProvider.getEntity(entity);
		IEntityObjectProcessor<?> eop = nucletDalProvider.getEntityObjectProcessor(eMeta);
		appendMandator(clctexpr, eMeta);
		return eop.count(clctexpr);
	}

	public List<CollectableValueIdField> getProcessByEntity(UID entityUid, boolean bSearchMode) {
		return getMasterDataFacade().getProcessByEntity(entityUid, bSearchMode);
	}

	/**
	 */
	@RolesAllowed("Login")
	@Override
	public <PK> ProxyList<PK,EntityObjectVO<PK>> getEntityObjectProxyList(UID entity,
			CollectableSearchExpression clctexpr, Collection<UID> fields, String customUsage)
		throws CommonPermissionException {
		checkReadAllowed(entity);
		return getEntityObjectProxyListNoCheck(entity, clctexpr, fields, customUsage);
	}

	public <PK> ProxyList<PK,EntityObjectVO<PK>> getEntityObjectProxyListNoCheck(UID entity,
																	 CollectableSearchExpression clctexpr,
																	 Collection<UID> fields, String customUsage) {
		
		final EntityMeta<PK> eMeta = metaProvider.getEntity(entity);
		final ProxyListProvider plProvider = new ProxyListProvider(serverParameter, metaProvider);
		
		final CollectableSearchCondition search = getSearchCondition(clctexpr.getSearchCondition());
		clctexpr.setSearchCondition(search);

		// Do not add record grants here! (NUCLOS-3517)
		
		EntityObjectProxyList<PK> result = new EntityObjectProxyList<>(eMeta.getUID(), clctexpr,
				fields, plProvider, customUsage);
		
		return result;
	}
	
	private CollectableSearchCondition getSearchCondition(CollectableSearchCondition constrain) {
		if (constrain == null) {
			return TrueCondition.TRUE;
		}
		
		return constrain;
	}

	@RolesAllowed("Login")
	@Override
	public <PK> Collection<EntityObjectVO<PK>> getDependentEntityObjects(UID subform, UID field, PK relatedId)
		throws CommonPermissionException {
		UID referencedEntity = metaProvider.getEntityField(field).getEntity();
		checkReadAllowed(referencedEntity);
		return getDependentEntityObjectsNoCheck(subform, field, relatedId);
	}

	public <PK> Collection<EntityObjectVO<PK>> getDependentEntityObjectsNoCheck(UID subform, UID field, PK relatedId) {
		final CollectableSearchCondition cond;
		if (relatedId instanceof Long) {
			cond = SearchConditionUtils.newIdComparison(
					field, ComparisonOperator.EQUAL, (Long)relatedId);
		} else {
			cond = SearchConditionUtils.newUidComparison(
					field, ComparisonOperator.EQUAL, (UID)relatedId);
		}
		CollectableSearchExpression cse = new CollectableSearchExpression(cond);
		final EntityMeta<?> eMeta = metaProvider.getEntity(subform);
		appendMandator(cse, eMeta);
		return nucletDalProvider.<PK>getEntityObjectProcessor(subform).getBySearchExprResultParams(
				cse, ResultParams.DEFAULT_RESULT_PARAMS);
	}

	@Override
	public <PK> void delete(UID entity, PK pk, boolean logicalDeletion, boolean bRemoveDependents) throws CommonFinderException,
		CommonRemoveException, CommonStaleVersionException, NuclosBusinessException,
		CommonPermissionException, CommonCreateException {

		final EntityMeta<PK> mdEntity = metaProvider.getEntity(entity);
		String customUsage = serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY);
		if (mdEntity.isStateModel()) {
			genericObjectFacade.remove(entity, (Long)pk, !logicalDeletion, customUsage);
		} else {
			masterDataFacade.remove(entity, pk, bRemoveDependents, customUsage);
		}
	}
	
	//TODO: Ambigous method name. The cache key only work, if all keys are from one ID-Factory
	@Override
	@CacheEvict(value="goMetaFields", key="#p1.intValue()")
	public void removeEntity(UID entity, Object id) throws CommonPermissionException {
		final EntityMeta<?> mdEntity = metaProvider.getEntity(entity);

        checkDeleteAllowed(mdEntity);

		final IEntityObjectProcessor<Object> processor = nucletDalProvider.getEntityObjectProcessor(entity);
		processor.delete(new Delete<>(id));
	}

	@Override
	public void remove(EntityObjectVO<?> entity) throws CommonPermissionException {
		removeEntity(entity.getDalEntity(), entity.getPrimaryKey());
	}

	@Override
	public <PK> EntityObjectVO<PK> insert(EntityObjectVO<PK> eoVO) throws CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException {

		final UID uid = eoVO.getDalEntity();
		String customUsage = serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY);

		EntityObjectVO<PK> retVal;
		if (metaProvider.getEntity(uid).isStateModel()) {
			GenericObjectVO go = DalSupportForGO.getGenericObjectVO((EntityObjectVO<Long>) eoVO);
			GenericObjectWithDependantsVO goWithDeps =
					new GenericObjectWithDependantsVO(go, eoVO.getDependents(), eoVO.getDataLanguageMap());
			go = genericObjectFacade.create(goWithDeps, customUsage);
			retVal = (EntityObjectVO<PK>) DalSupportForGO.wrapGenericObjectVO(go);
			retVal.setDependents(goWithDeps.getDependents());
		} else {
			MasterDataVO<PK> md = DalSupportForMD.wrapEntityObjectVO(eoVO);
			md = getMasterDataFacade().create(md, customUsage);
			retVal = md.getEntityObject();
		}

		return retVal;

	}

	// NUCLOS-5995
	public <PK> EntityObjectVO<PK> executeBusinessRules(List<EventSupportSourceVO> lstRuleVO, EntityObjectVO<PK> eoVO,
														String customUsage, boolean bCollectiveProcessing)
				throws CommonBusinessException {
		boolean bSaveAfterRuleExecution = false;

		for (EventSupportSourceVO eseVO : lstRuleVO) {
			EventSupportCustomResult<PK> result = getEventSupportFacade().fireCustomEventSupport(eoVO, eseVO, false);
			eoVO = result.getEntityObjectVO();
			if (!bSaveAfterRuleExecution) {
				bSaveAfterRuleExecution = result.getUpdateAfterExecution();
			}
		}

		if (bSaveAfterRuleExecution) {
			if (eoVO.isFlagNew()) {
				eoVO = this.insert(eoVO);
			} else {
				eoVO = this.update(eoVO, bCollectiveProcessing);
			}
		}

		return eoVO;
	}

	public <PK> void executeBusinessRuleForPks(EventSupportSourceVO ruleVO, UID entity, Collection<PK> pks) throws CommonBusinessException {
		for (PK pk : pks) {
			EntityObjectVO<PK> eo = get(entity, pk);
			executeBusinessRules(Collections.singletonList(ruleVO), eo, null, false);
		}
	}

	@Override
	public <PK> EntityObjectVO<PK> update(EntityObjectVO<PK> eoVO, boolean bCollectiveProcessing) throws CommonCreateException, CommonFinderException, CommonRemoveException,
		NuclosBusinessException, NoSuchElementException, CommonPermissionException, CommonStaleVersionException, CommonValidationException {

		EntityObjectVO<PK> retVal;
		final UID uid = eoVO.getDalEntity();
		String customUsage = serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY);
		
		if (metaProvider.getEntity(uid).isStateModel()) {
			GenericObjectVO go = DalSupportForGO.getGenericObjectVO((EntityObjectVO<Long>) eoVO);
			GenericObjectWithDependantsVO modify = genericObjectFacade.modify(uid,
					new GenericObjectWithDependantsVO(go, eoVO.getDependents(), eoVO.getDataLanguageMap()),
					customUsage, bCollectiveProcessing);
			retVal = (EntityObjectVO<PK>) DalSupportForGO.wrapGenericObjectVO(modify);
			retVal.setDependents(modify.getDependents());
		}
		else {
			MasterDataVO<PK> md = DalSupportForMD.wrapEntityObjectVO(eoVO);
			PK pk = masterDataFacade.modify(md, customUsage, bCollectiveProcessing);
			md.setPrimaryKey(pk);
			retVal = md.getEntityObject();
		}
		
		return retVal;
	}
	
	@Override
	public <PK> void createOrUpdatePlain(EntityObjectVO<PK> entity) throws CommonPermissionException {
		final UID uid = entity.getDalEntity();
		final String user = getCurrentUserName();
		final PK id = entity.getPrimaryKey();
		final EntityMeta<PK> mdEntity = metaProvider.getEntity(uid);
		
		checkPermissions(mdEntity, user);
		grantUtils.checkWriteInternal(uid, entity.getPrimaryKey());
		lockUtils.checkLockedByCurrentUserInternal(uid, entity.getPrimaryKey());
		if (entity.isFlagNew()) {
			mandatorUtils.checkWriteAllowed(entity, mdEntity);
		} else {
			mandatorUtils.checkWriteAllowedFromDb(id, mdEntity);
		}
		setFlagsAndId(entity, mdEntity);
		setSystemFields(entity, user);

		final IEntityObjectProcessor<PK> processor = nucletDalProvider.getEntityObjectProcessor(uid);
		processor.insertOrUpdate(entity);
	}
	
	public <PK> void createOrUpdatePlainWithoutPermissionCheck(EntityObjectVO<PK> entity) throws CommonPermissionException {
		final UID uid = entity.getDalEntity();
		final String user = getCurrentUserName();
		final MetaProvider mdProv = metaProvider;
		final EntityMeta<PK> mdEntity = mdProv.getEntity(uid);
		
		setFlagsAndId(entity, mdEntity);
		setSystemFields(entity, user);

		final IEntityObjectProcessor<PK> processor = nucletDalProvider.getEntityObjectProcessor(uid);
		processor.insertOrUpdate(entity);
	}
	
	private <PK> void setId(EntityObjectVO<PK> entity, EntityMeta<?> mdEntity) {
		final boolean isInsertWoId = entity.isbInsertWoId();
		final PK intid = entity.getPrimaryKey();
		final PK result;
		if (intid != null || isInsertWoId) {
			result = intid;
		} else {
			final String idFactory = mdEntity.getIdFactory();
			if (idFactory == null) {
				if (mdEntity.getPkClass().equals(UID.class)) {
					result = (PK) new UID();
				} else {
					result = (PK) dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE);
				}
			} else {
				result = (PK) dataBaseHelper.getDbAccess().executeFunction(idFactory, Long.class);
			}
		}
		if (!isInsertWoId) {
			entity.setPrimaryKey(result);
		}
	}
	
	private void setSystemFields(EntityObjectVO entity, String user) {
		final InternalTimestamp now = new InternalTimestamp(System.currentTimeMillis());
		if (entity.getCreatedBy() == null) {
			entity.setCreatedBy(user);
		}
		entity.setChangedBy(user);
		if (entity.getCreatedAt() == null) {
			entity.setCreatedAt(now);
		}
		entity.setChangedAt(now);
	}
	
	private <PK> void setFlagsAndId(EntityObjectVO<PK> entity, EntityMeta<?> mdEntity) throws CommonPermissionException {
		final PK intid = entity.getPrimaryKey();
		if (intid != null) {
			entity.flagUpdate();
		}
		else {
			entity.flagNew();
			setId(entity, mdEntity);
			entity.setVersion(1);
		}
	}

	private void checkPermissions(EntityMeta<?> mdEntity, String user) throws CommonPermissionException {
		securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
        checkWriteAllowed(mdEntity);
	}
	
	@Override
	public Integer getVersion(UID entity, Object id) throws CommonPermissionException {
		checkReadAllowed(entity);
		return nucletDalProvider.getEntityObjectProcessor(entity).getVersion(id);
	}
	
	@Cacheable(value="fieldGroupNames", key="#p0")
	public String getFieldGroupName(UID groupID) {
		EntityObjectVO<?> eo = nucletDalProvider.getEntityObjectProcessor(E.ENTITYFIELDGROUP.getUID()).getByPrimaryKey(groupID);
		if (eo == null) {
			return null;
		}
		return eo.getFieldValue(E.ENTITYFIELDGROUP.name);
	}
	
	@CacheEvict(value="fieldGroupNames", allEntries=true)
	public void evictGroupNamesCache() {
	}
	
	public <PK> UsageCriteria getUsageCriteriaForPK(PK pk, UID entityUID, String customUsage) throws CommonBusinessException {
		EntityMeta<PK> eMeta = metaProvider.getEntity(entityUID);
		if (eMeta.isStateModel()) {
			return genericObjectFacade.getGOMeta((Long)pk, entityUID, customUsage);
		}
		
		return new UsageCriteria(entityUID, null, null, customUsage);
	}
	
	/*
	 * taken from 
	 * - org.nuclos.client.masterdata.ClientEnumeratedDefaultValueProvider
	 * - org.nuclos.client.common.Utils.setDefaultValues(Collectable<PK>, CollectableEntity)
	 */
	public <PK> void setDefaultValues(EntityObjectVO<PK> eo) {
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(eo.getDalEntity());
		for (FieldMeta<?> fMeta : eMeta.getFields()) {
			// fill the map with null values
			eo.setFieldValue(fMeta.getUID(), null);
			// set default value
			setDefaultColumnValue(eo, fMeta);
		} 
	}
	
	public <PK> void setDefaultColumnValue(EntityObjectVO<PK> eo, FieldMeta<?> fMeta) {
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(eo.getDalEntity());
		String sDefault = fMeta.getDefaultValue();
		Long defaultId = fMeta.getDefaultForeignId();
		UID defauUID = fMeta.getDefaultForeignUid();
		try {
			if (fMeta.getForeignEntity() != null || fMeta.getLookupEntity() != null) {
				if ((defaultId != null || defauUID != null)) {
					EntityObjectVO<?> referencedEO = getReferenced(null, fMeta.getUID(), RigidUtils.defaultIfNull(defaultId, defauUID));
					if (referencedEO != null) {
						if (defaultId != null) {
							eo.setFieldId(fMeta.getUID(), defaultId);
						}
						if (defauUID != null) {
							eo.setFieldUid(fMeta.getUID(), defauUID);
						}
						String stringifiedValue = RefValueExtractor.get(referencedEO, fMeta.getUID(), null, metaProvider);
						eo.setFieldValue(fMeta.getUID(), stringifiedValue);
					}
				}
			} else {
				if (StringUtils.looksEmpty(sDefault)) {
					//NUCLOS-5488
					if (fMeta.getJavaClass() == Boolean.class && !fMeta.isNullable()) {
						eo.setFieldValue(fMeta.getUID(), Boolean.FALSE);
					}
				} else {
					if (fMeta.getJavaClass() == Double.class) {
						eo.setFieldValue(fMeta.getUID(), Double.parseDouble(sDefault.replace(',', '.')));
					}
					else if (fMeta.getJavaClass() == Integer.class) {
						eo.setFieldValue(fMeta.getUID(), Integer.parseInt(sDefault));
					}
					else if (fMeta.getJavaClass() == Boolean.class) {						
						if("ja".equals(sDefault)) {
							eo.setFieldValue(fMeta.getUID(), Boolean.TRUE);
						} else {
							eo.setFieldValue(fMeta.getUID(), Boolean.FALSE);
						}
					}
					else if (fMeta.getJavaClass() == Date.class) {
						if (RelativeDate.today().toString().equals(sDefault)) {
							eo.setFieldValue(fMeta.getUID(), DateUtils.today());
						}
						else {
							// NUCLOS-1914
							final String format = fMeta.getFormatInput();
							final DateFormat formatter;
							if (format != null) {
								formatter = new SimpleDateFormat(format);
							} else {
								formatter = SpringLocaleDelegate.getInstance().getDateFormat();
							}
							eo.setFieldValue(fMeta.getUID(), formatter.parse(sDefault));
						}
					}
					else {
						eo.setFieldValue(fMeta.getUID(), sDefault);
					}
				}
			}
		} catch (Exception ex) {
			String s = "[sDefault=" + sDefault + ", defaultId=" + defaultId +
			           ", defauUID=" + (defauUID==null?null:defauUID.getString()) + "]";
			LOG.warn("Setting default {} for field {}.{} failed:",
			         s, eMeta.getEntityName(), fMeta.getFieldName(),
			         ex);
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public <PK> UID lockInNewTransaction(UID entityUID, PK pk) throws CommonPermissionException {
		return this.lock(entityUID, pk);
	}
	
	public <PK> UID lock(UID entityUID, PK pk) throws CommonPermissionException {
		UID userUid = securityCache.getUserUid(getCurrentUserName());
		lockImpl(entityUID, pk, userUid);
		return userUid;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public <PK> void lockInNewTransaction(UID entityUID, PK pk, UID userUID) throws CommonPermissionException {
		lockImpl(entityUID, pk, userUID);
	}
	
	private <PK> void lockImpl(UID entityUID, PK pk, UID userUID) throws CommonPermissionException {
		if (pk == null) {
			throw new NuclosFatalException("Primary key must not be null");
		}
		EntityMeta<Object> eMeta = metaProvider.getEntity(entityUID);
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not owner enabled");
		}
		boolean lock;
		if (securityCache.isSuperUser(getCurrentUserName())) {
			lock = true;
		} else {
			lock = !sessionUtils.isCalledRemotely();
		}
		if (lock) {
			lockUtils.lock(entityUID, pk, userUID);
		} else {
			throw new CommonPermissionException("Locking is not allowed");
		}
	}
	
	public <PK> void unlockInNewTransactionAfterCommit(final UID entityUID, final PK pk) throws CommonPermissionException {
		this.unlockImpl(entityUID, pk, true);
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
			@Override public void suspend() {}
			@Override public void resume() {}
			@Override public void flush() {}
			@Override public void beforeCompletion() {}
			@Override public void beforeCommit(boolean arg0) {}
			@Override public void afterCompletion(int arg0) {}
			@Override public void afterCommit() {
				try {
					unlockInNewTransaction(entityUID, pk);
				} catch (CommonPermissionException e) {
					// permission changed during call?
					LOG.error(e.getMessage(), e);
				}
			}
		});
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public <PK> void unlockInNewTransaction(UID entityUID, PK pk) throws CommonPermissionException {
		this.unlockImpl(entityUID, pk, false);
	}
	
	@Override
	public <PK> void unlock(UID entityUID, PK pk) throws CommonPermissionException {
		this.unlockImpl(entityUID, pk, false);
	}
	
	private <PK> void unlockImpl(UID entityUID, PK pk, boolean checkPermissionOnly) throws CommonPermissionException {
		if (pk == null) {
			throw new NuclosFatalException("Primary key must not be null");
		}
		EntityMeta<PK> eMeta = metaProvider.getEntity(entityUID);
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not owner enabled");
		}
		boolean unlock = false;
		if (securityCache.isSuperUser(getCurrentUserName())) {
			unlock = true;
		} else {
			if (sessionUtils.isCalledRemotely()) {
				if (eMeta.getUnlockMode() == UnlockMode.ALL_USERS_MANUALLY) {
					unlock = true;
				}
			} else {
				unlock = true;
			}
		}
		if (unlock) {
			if (!checkPermissionOnly) {
				lockUtils.unlock(entityUID, pk);
			}
		} else {
			throw new CommonPermissionException("Unlocking is not allowed");
		}
	}

	/**
	 *
	 * @param username User which preferences are to be cleared.
	 * @return boolean is something has been cleared
	 */
	public boolean clearUserPreferences(String username) throws CommonFinderException {
		IEntityObjectProcessor<UID> proc = nucletDalProvider.getEntityObjectProcessor(E.USER);
		CollectableSearchCondition csc = SearchConditionUtils.newComparison(E.USER.name, ComparisonOperator.EQUAL, username);
		List<EntityObjectVO<UID>> users = proc.getBySearchExpression(new CollectableSearchExpression(csc));
		if (users.size() != 1) {
			throw new CommonFinderException("Not exact one user for name=" + username + " found, but:" + users.size());
		}
		EntityObjectVO<UID> userEo = users.get(0);

		UID user = userEo.getPrimaryKey();

		boolean b = purgePrefsFromSystemEntity(E.PREFERENCE.user, user)
				|| purgePrefsFromSystemEntity(E.PREFERENCE_SELECTED.user, user)
				|| purgePrefsFromSystemEntity(E.USERSETTING.user, user)
				|| purgePrefsFromSystemEntity(E.WORKSPACE.user, user);

		if (userEo.getFieldValue(E.USER.preferences) != null) {
			userEo.setFieldValue(E.USER.preferences, null);
			userEo.flagUpdate();
			proc.insertOrUpdate(userEo);
			b = true;
		}

		if (b) {
			securityCache.invalidate(username, true, null);
		}

		return b;
	}

	private boolean purgePrefsFromSystemEntity(FieldMeta<UID> userField, UID user) {

		IEntityObjectProcessor<UID> proc = nucletDalProvider.getEntityObjectProcessor(userField.getEntity());
		CollectableSearchCondition csc = SearchConditionUtils.newUidComparison(userField, ComparisonOperator.EQUAL, user);
		Collection<UID> uids = proc.getIdsBySearchExpression(new CollectableSearchExpression(csc));

		for (UID uid : uids) {
			proc.delete(new Delete<>(uid));
		}

		return !uids.isEmpty();
	}
}