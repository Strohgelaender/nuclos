package org.nuclos.server.websocket;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.atmosphere.config.managed.Decoder;
import org.atmosphere.config.managed.Encoder;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.SerializableString;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Default object encoder/decoder.
 *
 * Dates are formatted in the ISO 8601 format, including milliseconds.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class DefaultCodec implements Encoder<Object, String>, Decoder<String, Object> {
	private final ObjectMapper mapper = new ObjectMapper();
	private final static String dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

	public DefaultCodec() {
		mapper.getFactory().setCharacterEscapes(new AtmosphereCharacterEscapes());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
		mapper.setDateFormat(new SimpleDateFormat(dateFormat));

//		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
	}

	@Override
	public Object decode(final String s) {
		try {
			return mapper.readValue(s, Object.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static Date decodeDate(String s) {
		s = StringUtils.trimLeadingCharacter(s, '"');
		s = StringUtils.trimTrailingCharacter(s, '"');
		try {
			return new SimpleDateFormat(dateFormat).parse(s);
		} catch (ParseException e) {
			return null;
		}
	}

	@Override
	public String encode(final Object o) {
		try {
			return mapper.writeValueAsString(o);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Escape special character for Atmohsphere.
	 * E.g. the pipe symbol '|' is the default message delimiter.
	 */
	class AtmosphereCharacterEscapes extends CharacterEscapes {
		private final int[] asciiEscapes;

		public AtmosphereCharacterEscapes() {
			// start with set of characters known to require escaping (double-quote, backslash etc)
			int[] esc = CharacterEscapes.standardAsciiEscapesForJSON();
			// and force escaping of a few others:
			esc['|'] = CharacterEscapes.ESCAPE_STANDARD;
			asciiEscapes = esc;
		}

		// this method gets called for character codes 0 - 127
		@Override
		public int[] getEscapeCodesForAscii() {
			return asciiEscapes;
		}

		// and this for others; we don't need anything special here
		@Override
		public SerializableString getEscapeSequence(int ch) {
			// no further escaping (beyond ASCII chars) needed:
			return null;
		}
	}
}
