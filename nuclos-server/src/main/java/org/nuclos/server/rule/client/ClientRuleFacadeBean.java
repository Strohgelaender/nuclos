package org.nuclos.server.rule.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rule.client.vo.ClientRuleSourceVO;
import org.nuclos.server.rule.client.vo.ClientRuleUsageVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClientRuleFacadeBean implements ClientRuleFacadeRemote {
	
	@Autowired
	MasterDataFacadeLocal mdFacade;
	
	@RolesAllowed("Login")
	@Override
	public UID createClientRule(ClientRuleSourceVO crSource) 
			throws CommonCreateException, CommonPermissionException, NuclosBusinessRuleException {
		
		if (crSource == null)
			throw new CommonCreateException("Cannot create client rule because source is null");
		
		if (crSource.getPrimaryKey() != null)
			throw new CommonCreateException("Cannot create client rule because it already has a primaryKey");
		
		MasterDataVO<UID> mdVOSaved = 
				mdFacade.create(MasterDataWrapper.wrapClientRuleSourceVO(crSource), null);
				
		return mdVOSaved.getPrimaryKey();
	}
	
	@RolesAllowed("Login")
	@Override
	public void removeClientRule(ClientRuleSourceVO crSource) throws CommonRemoveException, 
		NuclosBusinessRuleException, CommonFinderException, CommonStaleVersionException, CommonPermissionException {
		if (crSource == null) {
			throw new CommonRemoveException("Cannot remove client rule because source is null");
		}
		if (crSource.getPrimaryKey() == null) {
			throw new CommonRemoveException("Cannot remove client rule because it does not have a primaryKey");
		}
		mdFacade.remove(E.CLIENTCODE.getUID(), crSource.getPrimaryKey(), true);		
	}
	
	@RolesAllowed("Login")
	@Override
	public void insertFieldCalculationRule(UID ruleId, UID fieldId) throws CommonCreateException{
		
		try {
			MasterDataVO<UID> mdvo = mdFacade.get(E.ENTITYFIELD.getUID(), fieldId);
			
			if (mdvo != null) {
				mdvo.setFieldUid(E.ENTITYFIELD.ruleBackgroundColor.getUID(), ruleId);
				mdFacade.modify(mdvo, null);
			}			
		} catch (Exception e) {
			throw new CommonCreateException(e);
		}		
	}
	
	@RolesAllowed("Login")
	@Override
	public void insertFieldBackgroundColorRule(UID ruleId, UID fieldId) throws CommonCreateException{
		
		try {
			MasterDataVO<UID> mdvo = mdFacade.get(E.ENTITYFIELD.getUID(), fieldId);
			
			if (mdvo != null) {
				mdvo.setFieldUid(E.ENTITYFIELD.ruleBackgroundColor.getUID(), ruleId);
				mdFacade.modify(mdvo, null);
			}			
		} catch (Exception e) {
			throw new CommonCreateException(e);
		}		
	}
	
	public List<ClientRuleSourceVO> getRulesByUsage(ClientRuleUsageVO usage) throws CommonFinderException, CommonPermissionException {
		
		List<ClientRuleSourceVO> retVal = new ArrayList<>();
		
		// get all fields for this entity		
		Collection<MasterDataVO<UID>> boFieldRules = mdFacade.getMasterData(E.ENTITYFIELD,
				SearchConditionUtils.and(
						SearchConditionUtils.newUidComparison(E.ENTITYFIELD.entity, ComparisonOperator.EQUAL, usage.getBusinessObjectId()),
						SearchConditionUtils.or(
								SearchConditionUtils.newIsNotNullCondition(E.ENTITYFIELD.ruleBackgroundColor.getUID()), 
								SearchConditionUtils.newIsNotNullCondition(E.ENTITYFIELD.ruleCalculation.getUID()))));
		
		List<UID> listOfRuleUIDs = new ArrayList<UID>();
		
		for (UID curId : listOfRuleUIDs) {
			retVal.add(MasterDataWrapper.getClientRuleSourceVO(mdFacade.get(E.CLIENTCODE, curId).getEntityObject()));
		}
		
		// find layout rules

		return retVal;
	}

	public String createContextSource() {
		return null;
	}
}
