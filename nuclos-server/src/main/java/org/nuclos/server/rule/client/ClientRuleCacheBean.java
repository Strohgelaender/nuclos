package org.nuclos.server.rule.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.EntityAndField;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@DependsOn("metaDataProvider")
public class ClientRuleCacheBean implements ClientRuleCacheRemote {
	
	@Autowired
	LayoutFacadeLocal layoutFacade;
	
	
	private final Map<UID, List<EntityObjectVO<UID>>> bosByNuclet
	 	= new HashMap<UID, List<EntityObjectVO<UID>>>();
	private final Map<UID, List<FieldMeta<?>>> fieldsByBo
 		= new HashMap<UID, List<FieldMeta<?>>>();	
	private final Map<UID, Map<Pair<UID, UID>, List<FieldMeta<?>>>> referencesByBo
		= new HashMap<UID, Map<Pair<UID, UID>, List<FieldMeta<?>>>>();
	private final Map<UID, Map<UID, List<FieldMeta<?>>>> dependentsByBo
		= new HashMap<UID, Map<UID, List<FieldMeta<?>>>>();
	
	public synchronized List<EntityObjectVO<UID>> getBOsByNuclet(UID nucletUid) {
		if (!bosByNuclet.containsKey(nucletUid)) {
			List<EntityObjectVO<UID>> results = NucletDalProvider.getInstance().getEntityObjectProcessor(E.ENTITY).getBySearchExpression(
					new CollectableSearchExpression(SearchConditionUtils.newUidComparison(E.ENTITY.nuclet, ComparisonOperator.EQUAL, nucletUid)));
			bosByNuclet.put(nucletUid, results);
		}
		
		return bosByNuclet.get(nucletUid);
	}
	
	public synchronized Map<Pair<UID, UID>, List<FieldMeta<?>>> getReferencesByBO(UID boUid) {
		if (!referencesByBo.containsKey(boUid)) {
			// get fields of subform is possible
			referencesByBo.put(boUid, getReferencedBOFields(boUid));
		}
		
		return referencesByBo.get(boUid);
		
	}
	
	public synchronized Map<UID, List<FieldMeta<?>>> getDependentsByBO(UID boUid) {
		if (!dependentsByBo.containsKey(boUid)) {
			// get fields of parent is possible		
			dependentsByBo.put(boUid, getSubformEntityFields(boUid));		
		}
		
		return dependentsByBo.get(boUid);
		
	}
	
	public synchronized List<FieldMeta<?>> getFieldsByBO(UID boUid) {
		
		if (!fieldsByBo.containsKey(boUid)) {
			
			ArrayList<FieldMeta<?>> arrayList = new ArrayList<FieldMeta<?>>();
			
			for (FieldMeta fm : MetaProvider.getInstance().getAllEntityFieldsByEntity(boUid).values()) {
				// Do not show system fields or referencing fields
				if (!SF.isEOField(fm.getEntity(), fm.getUID()) && fm.getForeignEntity() == null)
					arrayList.add(fm);
			}
			
			Collections.sort(arrayList, new FieldSorter());
			
			if (MetaProvider.getInstance().getEntity(boUid).isUidEntity()) {
				arrayList.add(0, SF.PK_UID.getMetaData(boUid));				
			}
			else {
				arrayList.add(0, SF.PK_ID.getMetaData(boUid));
			}
			
			fieldsByBo.put(boUid, arrayList);	
		}
		
		return fieldsByBo.get(boUid);
	}

	private Map<UID, List<FieldMeta<?>>> getSubformEntityFields(UID boUid) {
		Map<UID, List<FieldMeta<?>>> retVal = new HashMap<UID, List<FieldMeta<?>>>();
		
		IEntityObjectProcessor<UID> layoutUsageProcessor = 
				NucletDalProvider.getInstance().getEntityObjectProcessor(E.LAYOUTUSAGE);
		
		List<EntityObjectVO<UID>> layoutUsages = layoutUsageProcessor.getBySearchExpression(new CollectableSearchExpression(
				SearchConditionUtils.and(		
						SearchConditionUtils.newComparison(E.LAYOUTUSAGE.entity, ComparisonOperator.EQUAL, boUid), 
						SearchConditionUtils.newComparison(E.LAYOUTUSAGE.searchScreen, ComparisonOperator.EQUAL, Boolean.FALSE))));
		
		Map<UID, Integer> subFormsAndUsage = new HashMap<UID, Integer>();
		
		int numbOfAppearance = layoutUsages.size();
		
		//  get all layouts that work with that our boUID
		for (EntityObjectVO<UID> layoutUsage : layoutUsages) {
			Map<EntityAndField, UID> subFormsOfLayout = 
					layoutFacade.getSubFormEntityAndParentSubFormEntityNamesById(layoutUsage.getFieldUid(E.LAYOUTUSAGE.layout));
			for (EntityAndField subform : subFormsOfLayout.keySet()) {
				UID entity = subform.getEntity();
				if (NucletDalProvider.getInstance().getEntityMetaDataProcessor().getByPrimaryKey(entity) != null) {
					if (!subFormsAndUsage.containsKey(entity)) {
						subFormsAndUsage.put(entity, 1);
					}
					else {
						subFormsAndUsage.put(entity, subFormsAndUsage.get(entity).intValue() + 1);
					}					
				}
			}
		}
		
		for (UID elem : subFormsAndUsage.keySet()) {
			if (subFormsAndUsage.get(elem).intValue() == numbOfAppearance) {
					ArrayList<FieldMeta<?>> arrayList = new ArrayList<FieldMeta<?>>();
					
					// Add UID of primaryKey manually
					if (MetaProvider.getInstance().getEntity(elem).isUidEntity()) {
						arrayList.add(SF.PK_UID.getMetaData(elem));				
					}
					else {
						arrayList.add(SF.PK_ID.getMetaData(elem));
					}
					
					for (FieldMeta<?> fm : MetaProvider.getInstance().getAllEntityFieldsByEntity(elem).values()) {
						if (!SF.isEOField(fm.getEntity(), fm.getUID()))
							arrayList.add(fm);
					}
						
					Collections.sort(arrayList, new FieldSorter());
				retVal.put(elem, arrayList);
			}	
		}
		
		return retVal;
	}
	
	private Map<Pair<UID, UID>, List<FieldMeta<?>>> getReferencedBOFields(UID boUid) {
		Map<Pair<UID, UID>, List<FieldMeta<?>>> retVal = new HashMap<Pair<UID, UID>, List<FieldMeta<?>>>();
		
		// first get all entities that might use boUID as subform in their layout
		List<Pair<UID, UID>> referencedEntities = new ArrayList<Pair<UID, UID>>();
		for (FieldMeta field : MetaProvider.getInstance().getAllEntityFieldsByEntity(boUid).values()) {
			if (field.getForeignEntity() != null && !MetaProvider.getInstance().getEntity(field.getForeignEntity()).isUidEntity())
				referencedEntities.add(new Pair(field.getUID(), field.getForeignEntity()));
		}
		
		for (Pair<UID, UID> pair : referencedEntities) {
			retVal.put(pair, new ArrayList<FieldMeta<?>>());
			
			// Add UID of primaryKey manually
			if (MetaProvider.getInstance().getEntity(pair.getY()).isUidEntity()) {
				retVal.get(pair).add(SF.PK_UID.getMetaData(pair.getY()));				
			}
			else {
				retVal.get(pair).add(SF.PK_ID.getMetaData(pair.getY()));
			}
			
			for (FieldMeta field : MetaProvider.getInstance().getAllEntityFieldsByEntity(pair.getY()).values()) {
				if (!SF.isEOField(field.getEntity(), field.getUID()))
					retVal.get(pair).add(field);
			}
		}
		
		return retVal;
	}
	
	class FieldSorter implements Comparator<FieldMeta<?>> {

		@Override
		public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
			if (o1.getOrder() != null && o2.getOrder() != null)
				return o1.getOrder().compareTo(o2.getOrder());
			
			return 0;
		}
		
	}
}
