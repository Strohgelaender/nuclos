package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if the system parameters require a recompile.
 */
@Component
public class SystemParameterRecompileChecker extends CompositeRecompileChecker {


    public SystemParameterRecompileChecker() {
        super(new PKRecompileChecker());
    }

	@Override
	public boolean isRecompileRequiredOnUpdate(final MasterDataVO<?> oldMdvo, final MasterDataVO<?> updatedMdvo) {
    	String paramOld = oldMdvo.getFieldValue(E.PARAMETER.name);
    	String paramNew = updatedMdvo.getFieldValue(E.PARAMETER.name);
    	if (RigidUtils.equal(paramNew, paramOld)) {
    		return false;
		}
    	return isRecompileRequiredForParam(paramOld) || isRecompileRequiredForParam(paramNew);
  	}

	@Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
		return isRecompileRequiredForParam(mdvo.getFieldValue(E.PARAMETER.name));
    }

	// NUCLOS-6918 when a new system parameter is created or deleted we don't have to recompile it for fix system parameters
    private static boolean isRecompileRequiredForParam(String param) {
		if (param == null || param.startsWith("nuclos_LAF_")) {
			return false;
		}
		return !ParameterProvider.listSystemParameter().contains(param);
	}

}
