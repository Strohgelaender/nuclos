package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Generates recompile checker.
 */
@Component
public class RecompileCheckerRegistry {

    @Autowired
    private NoRecompileRequiredChecker noRecompileRequiredChecker;


    @Autowired
    private RoleRecompileChecker roleRecompileChecker;

    @Autowired
    private NucletRecompileChecker nucletRecompileChecker;

    @Autowired
    private NucletParameterRecompileChecker nucletParameterRecompileChecker;

    @Autowired
    private SystemParameterRecompileChecker systemParameterRecompileChecker;

    @Autowired
    private XMLImportStructureDefinitionRecompileChecker
        xmlImportStructureDefinitionRecompileChecker;

    @Autowired
    private ImportStructureDefinitionRecompileChecker importStructureDefinitionRecompileChecker;

    @Autowired
    private WebServiceRecompileChecker webServiceRecompileChecker;

    @Autowired
    private ReportRecompileChecker reportRecompileChecker;

    @Autowired
    private FormRecompileChecker formRecompileChecker;

    @Autowired
    private GeneratorRecompileChecker generatorRecompileChecker;

    @Autowired
    private DataSourceRecompileChecker dataSourceRecompileChecker;

    @Autowired
    private StatemodelRecompileChecker statemodelRecompileChecker;

    @Autowired
    private StateRecompileChecker stateRecompileChecker;

    @Autowired
    private CommunicationPortRecompileChecker communicationPortRecompileChecker;

    @Autowired
    private ProcessRecompileChecker processRecompileChecker;

    @Autowired
	private GenericImplementationRecompileChecker genericImplementationRecompileChecker;

	@Autowired
	private GenericFieldMappingRecompileChecker genericFieldMappingRecompileChecker;

	@Autowired
	private NucletIntegrationPointRecompileChecker nucletIntegrationPointRecompileChecker;

	@Autowired
	private DynamicEntityRecompileChecker dynamicEntityRecompileChecker;

    /**
     * Gets a recompile checker for the given entity.
     *
     * @param entityUid The UID of the entity to check if a modification on such an entity instance
     *                  requires a recompile.
     * @return Never {@code null}. If no matching checker for the given entity was found then a
     * checker is returned that does not required a recompile.
     */
    public IRecompileChecker getRecompileChecker(UID entityUid) {

        if (E.PARAMETER.checkEntityUID(entityUid)) {
            return systemParameterRecompileChecker;
        } else if (E.NUCLETPARAMETER.checkEntityUID(entityUid)) {
            return nucletParameterRecompileChecker;
        } else if (E.NUCLET.checkEntityUID(entityUid)) {
            return nucletRecompileChecker;
        } else if (E.ROLE.checkEntityUID(entityUid)) {
            return roleRecompileChecker;
        } else if (E.WEBSERVICE.checkEntityUID(entityUid)) {
            return webServiceRecompileChecker;
        } else if (E.IMPORT.checkEntityUID(entityUid)) {
            return importStructureDefinitionRecompileChecker;
        } else if (E.XML_IMPORT.checkEntityUID(entityUid)) {
            return xmlImportStructureDefinitionRecompileChecker;
        } else if (E.REPORT.checkEntityUID(entityUid)) {
            return reportRecompileChecker;
        } else if (E.FORM.checkEntityUID(entityUid)) {
            return formRecompileChecker;
        } else if (E.GENERATION.checkEntityUID(entityUid)) {
            return generatorRecompileChecker;
        } else if (E.DATASOURCE.checkEntityUID(entityUid)) {
            return dataSourceRecompileChecker;
        } else if (E.STATEMODEL.checkEntityUID(entityUid)) {
            return statemodelRecompileChecker;
        } else if (E.STATE.checkEntityUID(entityUid)) {
            return stateRecompileChecker;
        } else if (E.COMMUNICATION_PORT.checkEntityUID(entityUid)) {
            return communicationPortRecompileChecker;
        } else if (E.PROCESS.checkEntityUID(entityUid)) {
            return processRecompileChecker;
        } else if (E.ENTITY_GENERIC_IMPLEMENTATION.checkEntityUID(entityUid)) {
			return genericImplementationRecompileChecker;
		} else if (E.ENTITY_GENERIC_FIELDMAPPING.checkEntityUID(entityUid)) {
			return genericFieldMappingRecompileChecker;
		} else if (E.NUCLET_INTEGRATION_POINT.checkEntityUID(entityUid)) {
        	return nucletIntegrationPointRecompileChecker;
		} else if (E.DYNAMICENTITY.checkEntityUID(entityUid)) {
        	return dynamicEntityRecompileChecker;
		}

        return noRecompileRequiredChecker;
    }
}
