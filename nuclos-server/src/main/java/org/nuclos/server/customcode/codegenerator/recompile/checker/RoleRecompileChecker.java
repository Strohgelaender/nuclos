package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a role must cause a recompile.
 */
@Component
public class RoleRecompileChecker extends CompositeRecompileChecker {

    public RoleRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.ROLE.name),
              new UIDFieldRecompileChecker(E.ROLE.nuclet));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new roles and deleted roles must force recompile:
        return true;
    }

}
