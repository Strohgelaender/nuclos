package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a process must cause a recompile.
 */
@Component
public class ProcessRecompileChecker extends CompositeRecompileChecker {

    public ProcessRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.PROCESS.name),
              new UIDFieldRecompileChecker(E.PROCESS.module));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new roles and deleted processes must force recompile:
        return true;
    }

}
