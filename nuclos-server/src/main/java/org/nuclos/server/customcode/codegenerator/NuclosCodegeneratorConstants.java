//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.nuclos.server.nbo.AbstractOutputFormat;
import org.nuclos.server.nbo.AbstractPrintout;

public class NuclosCodegeneratorConstants {

	public static final String JAVA_SRC_ENCODING = "UTF-8";

	/** Base directory where the generated code is based on (e.g. data/codegenerator/) */
	public static final File GENERATOR_FOLDER;
	
	static {
		try {
			GENERATOR_FOLDER = NuclosSystemParameters.getDirectory(NuclosSystemParameters.GENERATOR_OUTPUT_PATH).getCanonicalFile();
		} catch (IOException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	public static final File BUILD_OUTPUT_PATH = new File(GENERATOR_FOLDER, "build");

	public static final File WSDL_OUTPUT_PATH = new File(GENERATOR_FOLDER, "wsdl");

	public static final String SRC_DIR_NAME = "src";

	public static final File SOURCE_OUTPUT_PATH = new File(GENERATOR_FOLDER, SRC_DIR_NAME);

	public static final String JAR_NAME = "Nuclet.jar";

	public static final File JAR_SRC_FOLDER = new File(GENERATOR_FOLDER, SRC_DIR_NAME);

	public static final File JARFILE = new File(GENERATOR_FOLDER, JAR_NAME);
	
	public static final String BO_NAME =  "BOEntities.jar";

	public static final String BO_SRC_DIR_NAME = "bosrc";
	public static final File BO_SRC_FOLDER = new File(GENERATOR_FOLDER, BO_SRC_DIR_NAME);

	public static final File BOJARFILE = new File(GENERATOR_FOLDER, BO_NAME);
	
	public static final String DATASOURCEREPORT_NAME = "ReportDSEntities.jar";

	public static final String DATASOURCEREPORT_SRC_DIR_NAME = "dsrsrc";
	public static final File DATASOURCEREPORT_SRC_FOLDER = new File(GENERATOR_FOLDER,
	                                                                DATASOURCEREPORT_SRC_DIR_NAME);
	
	public static final File DATASOURCEREPORTJARFILE = new File(GENERATOR_FOLDER, DATASOURCEREPORT_NAME);

	public static final String REPORT_NAME = "ReportEntities.jar";

	public static final String REPORT_SRC_DIR_NAME = "repsrc";

	public static final File REPORT_SRC_FOLDER = new File(GENERATOR_FOLDER, REPORT_SRC_DIR_NAME);
	
	public static final File REPORTJARFILE = new File(GENERATOR_FOLDER, REPORT_NAME);
	
	public static final String WEBSERVICE_NAME = "WebServices.jar";

	public static final String WEBSERVICE_SRC_DIR_NAME = "wssrc";
	public static final File WEBSERVICE_SRC_FOLDER = new File(GENERATOR_FOLDER,
	                                                          WEBSERVICE_SRC_DIR_NAME);
	
	public static final File WEBSERVICEJARFILE = new File(GENERATOR_FOLDER, WEBSERVICE_NAME);
	
	public static final String IMPORTSTRUCTUREDEFS_NAME = "ImportStructDefsEntities.jar";

	public static final String IMPORTSTRUCTUREDEFS_SRC_DIR_NAME = "isdsrc";
	public static final File IMPORTSTRUCTUREDEFS_SRC_FOLDER = new File(GENERATOR_FOLDER,
	                                                                   IMPORTSTRUCTUREDEFS_SRC_DIR_NAME);
	
	public static final File IMPORTSTRUCTUREDEFSJARFILE = new File(GENERATOR_FOLDER, IMPORTSTRUCTUREDEFS_NAME);
	
	public static final String PRINTOUT_NAME = "PrintoutEntities.jar";

	public static final String PRINTOUT_SRC_DIR_NAME = "posrc";
	public static final File PRINTOUT_SRC_FOLDER = new File(GENERATOR_FOLDER, PRINTOUT_SRC_DIR_NAME);
	
	public static final File PRINTOUTJARFILE = new File(GENERATOR_FOLDER, PRINTOUT_NAME);

	public static final String USERROLE_NAME = "UserRoleEntities.jar";

	public static final String USERROLE_SRC_DIR_NAME = "ugsrc";
	public static final File USERROLE_SRC_FOLDER = new File(GENERATOR_FOLDER, USERROLE_SRC_DIR_NAME);
	
	public static final File USERROLEJARFILE = new File(GENERATOR_FOLDER, USERROLE_NAME);
	
	public static final String GENERATION_NAME = "Generation.jar";

	public static final String GENERATION_SRC_DIR_NAME = "gensrc";
	public static final File GENERATION_SRC_FOLDER = new File(GENERATOR_FOLDER,
	                                                          GENERATION_SRC_DIR_NAME);
	
	public static final File GENERATIONJARFILE = new File(GENERATOR_FOLDER, GENERATION_NAME);
	
	public static final String PARAMETER_NAME = "Parameter.jar";

	public static final String PARAMETER_SRC_DIR_NAME = "paramsrc";
	public static final File PARAMETER_SRC_FOLDER = new File(GENERATOR_FOLDER,
	                                                         PARAMETER_SRC_DIR_NAME);
	
	public static final File PARAMETERJARFILE = new File(GENERATOR_FOLDER, PARAMETER_NAME);
	
	public static final String COMMUNICATION_NAME = "Communication.jar";

	public static final String COMMUNICATION_SRC_DIR_NAME = "comsrc";
	public static final File COMMUNICATION_SRC_FOLDER = new File(GENERATOR_FOLDER,
	                                                             COMMUNICATION_SRC_DIR_NAME);
	
	public static final File COMMUNICATIONJARFILE = new File(GENERATOR_FOLDER, COMMUNICATION_NAME);
	
	public static final String JAR_OLD_NAME = "Nuclet.jar.old";
	
	static final File JARFILE_OLD = new File(GENERATOR_FOLDER, JAR_OLD_NAME);
	
	public static final String CCCE_NAME = "CCCE.jar";
	
	public static final File CCCEJARFILE = new File(GENERATOR_FOLDER, CCCE_NAME);

	public static final String STATEMODEL_SRC_DIR_NAME = "smsrc";
	public static final File STATEMODEL_SRC_FOLDER = new File(GENERATOR_FOLDER,
	                                                          STATEMODEL_SRC_DIR_NAME);
	
	public static final String STATEMODEL_NAME = "statemodels.jar";
	
	public static final File STATEMODELJARFILE = new File(GENERATOR_FOLDER, STATEMODEL_NAME);

	public static final String TEMP_SRC_DIR_NAME = "temp";
	public static final File TEMP_FOLDER = new File(GENERATOR_FOLDER, TEMP_SRC_DIR_NAME);

	public static final String BUSINESS_TEST_SRC_DIR_NAME = "btsrc";
	public static final File BUSINESS_TEST_SRC_FOLDER = new File(GENERATOR_FOLDER, BUSINESS_TEST_SRC_DIR_NAME);
	// Source folder for business test entity classes etc.
	public static final File BUSINESS_TEST_SRC_MAIN_FOLDER = new File(BUSINESS_TEST_SRC_FOLDER, "main");
	// Source folder for the actual test scripts
	public static final File BUSINESS_TEST_SRC_TEST_FOLDER = new File(BUSINESS_TEST_SRC_FOLDER, "test");

	public static final File[] SRC_FOLDERS = new File[] {
		JAR_SRC_FOLDER, BO_SRC_FOLDER, IMPORTSTRUCTUREDEFS_SRC_FOLDER, PARAMETER_SRC_FOLDER,
		COMMUNICATION_SRC_FOLDER, DATASOURCEREPORT_SRC_FOLDER, PRINTOUT_SRC_FOLDER,
		USERROLE_SRC_FOLDER, REPORT_SRC_FOLDER, GENERATION_SRC_FOLDER, STATEMODEL_SRC_FOLDER,
		WEBSERVICE_SRC_FOLDER
	};
	
	public static final List<File> SRC_FOLDERS_LIST;
	
	public static final File[] JAR_FILES = new File[] {
		JARFILE, BOJARFILE, IMPORTSTRUCTUREDEFSJARFILE, PARAMETERJARFILE, COMMUNICATIONJARFILE,
		USERROLEJARFILE, DATASOURCEREPORTJARFILE, GENERATIONJARFILE, PRINTOUTJARFILE, REPORTJARFILE,
		WEBSERVICEJARFILE, STATEMODELJARFILE, CCCEJARFILE
	};
	
	public static final List<File> JAR_FILES_LIST;
	
	public static final String[] JAR_NAMES = new String[] {
		JAR_NAME, BO_NAME, IMPORTSTRUCTUREDEFS_NAME, DATASOURCEREPORT_NAME, PRINTOUT_NAME,
		PARAMETER_NAME, COMMUNICATION_NAME, USERROLE_NAME, GENERATION_NAME, REPORT_NAME,
		WEBSERVICE_NAME, STATEMODEL_NAME, CCCE_NAME
	};
	
	public static final List<String> JAR_NAMES_LIST;
	
	static final Class<?>[] CODE_COMPILER_CLASSPATH_EXTENSION = new Class<?>[] {
		AbstractBusinessObject.class, AbstractBusinessObject.NuclosMandatorProvider.class, UID.class, 
		AbstractOutputFormat.class, AbstractPrintout.class
	};
	
	static {
		try {
			GENERATOR_FOLDER.mkdirs();
			if (!GENERATOR_FOLDER.isDirectory()) {
				throw new IOException(GENERATOR_FOLDER.toString());
			}
			
			SRC_FOLDERS_LIST = Arrays.asList(SRC_FOLDERS);
			JAR_FILES_LIST = Arrays.asList(JAR_FILES);
			JAR_NAMES_LIST = Arrays.asList(JAR_NAMES);
		} catch (IOException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

}
