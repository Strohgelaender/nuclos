//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import static org.nuclos.server.customcode.codegenerator.GeneratedFile.COMMENT_END;
import static org.nuclos.server.customcode.codegenerator.GeneratedFile.COMMENT_START;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.security.MessageDigestInputStream;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.eventsupport.ejb3.SourceCache;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.security.NuclosLocalServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author Thomas Pasch
 */
@Configurable
class SourceScannerTask extends TimerTask {

	private static final Logger LOG = LoggerFactory.getLogger(SourceScannerTask.class);

	private static final Pattern PROP_PAT = Pattern.compile("^//\\s*(\\p{Alnum}+)=(.*)$");

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private EntityObjectFacadeLocal entityObjectFacade;

	@Autowired
	private NuclosLocalServerSession nuclosLocalServerSession;

	@Autowired
	private NuclosJavaCompilerComponent nuclosJavaCompilerComponent;

	@Autowired
	private SourceCache sourceCache;

	@Autowired
	private CustomCodeManager customCodeManager;

	private boolean forceReadingSources;

	SourceScannerTask() {
		LOG.info("Created scanner");
	}

	public void setForceReadingSourcesOn() {
		this.forceReadingSources = true;
	}

	@Override
	public void run() {
		try {
			_run();
		} catch (Exception e) {
			LOG.warn("scanner failed: ", e);
		}
	}

	private void _run() {
		if (!SpringApplicationContextHolder.isSpringReady(true)) {
			return;
		}
		if (nuclosJavaCompilerComponent.isCurrentlyWritingSources()) {
			return;
		}

		final long modified = nuclosJavaCompilerComponent.getLastSrcCompileTime();
		final File srcDir = nuclosJavaCompilerComponent.getSourceOutputPath();
		final File wsdlDir = nuclosJavaCompilerComponent.getWsdlOutputPath();

		if (!forceReadingSources && !NuclosCodegeneratorConstants.JARFILE.exists()) {
			LOG.debug("No Nuclet.jar - no action");
			return;
		}

		// Find modified files on disk
		final List<File> javaSrc = new ArrayList<>();
		scanDir(javaSrc, srcDir, modified, ".java");
		final List<File> wsdlSrc = new ArrayList<>();
		scanDir(wsdlSrc, wsdlDir, modified, ".wsdl");

		if (javaSrc.isEmpty() && wsdlSrc.isEmpty()) {
			return;
		}

		// Parse files on disk (to get type and id)
		final List<GeneratedFile> java = new ArrayList<GeneratedFile>();
		for (File f : javaSrc) {
			try {
				final GeneratedFile gf = parseFile(f);
				// only compile if file has changed (tp)
				final String hashValue = sourceCache.getHashValue(gf.getName());
				if (hashValue == null) {
					// ignore...
				} else if (!hashValue.equals(gf.getHashValue())) {
					java.add(gf);
				} else {
					LOG.debug("file {} has not changed (hashValue), not compiling...", f);
				}
			} catch (IOException e) {
				LOG.warn("Can't parse file {}: ", f, e);
			} catch (NumberFormatException e) {
				LOG.warn("Can't parse file {}", f, e);
			}
		}
		final List<GeneratedFile> wsdl = new ArrayList<GeneratedFile>();
		for (File f : wsdlSrc) {
			try {
				wsdl.add(parseFile(f));
			} catch (IOException e) {
				LOG.warn("Can't parse file {}: ", f, e);
			} catch (NumberFormatException e) {
				LOG.warn("Can't parse file {}", f, e);
			}
		}
		if (java.isEmpty() && wsdl.isEmpty()) {
			LOG.debug("Changes on disk, but nothing changed");
			return;
		}

		// Change data in DB based on file changes on disk
		nuclosLocalServerSession.loginAsUser(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER));
		try {
			for (GeneratedFile gf : java) {
				// USE API RULES !!! No help for deprecated classes any more...
				if (gf.getEntity() != E.SERVERCODE) {
					LOG.warn("USE API RULES !!! No help for deprecated classes any more ... {}",
							gf.getFile());
					continue;
				}
				try {
					if ("org.nuclos.server.customcode.valueobject.CodeVO".equals(gf.getType())) {
						updatePlainCode(gf);
					} else {
						LOG.warn("Don't know how to write object type {} to DB", gf.getType());
					}
					// update hash
					sourceCache.updateHashValue(gf.getName(), gf.getHashValue());
				} catch (Exception e) {
					LOG.warn("Can't save modified file {} to DB: ", gf.getFile(), e);
				}
			}
			for (GeneratedFile gf : wsdl) {
				LOG.warn("Modified file {} from WSDL, but support for change is not implemented",
						gf.getFile());
			}
		} finally {
			nuclosLocalServerSession.logout();
			// New way: compile and do NOT write to disk
			try {
				customCodeManager.getClassLoaderAndCompileIfNeeded(false);
			} catch (NuclosCompileException ncex) {
				// ignore compile exceptions here!
				LOG.error(ncex.getMessage(), ncex);
			}
		}
	}

	public void updatePlainCode(GeneratedFile gf) throws CommonFinderException, CommonPermissionException,
			NuclosBusinessRuleException, CommonCreateException, CommonRemoveException,
			CommonStaleVersionException, CommonValidationException {

		final MasterDataVO<UID> vo = getCodeAsMd(gf.getEntity(), gf.getPrimaryKey());

		if (!gf.getName().equals(vo.getFieldValue(E.SERVERCODE.name))) {
			throw new IllegalStateException();
		}

		vo.setFieldValue(E.SERVERCODE.source, new String(gf.getContent()));
		masterDataFacade.modify(vo, null);

		LOG.info("Update code in db: name={} uid={} from {}",
				gf.getName(), gf.getPrimaryKey(), gf.getFile());
	}

	public EntityObjectVO createEventSupportRule(GeneratedFile gf) throws IOException, CommonPermissionException {
		if (!E.SERVERCODE.equals(gf.getEntity())) {
			throw new IllegalStateException();
		}
		final String entity = gf.getEntity().getEntityName();
		final byte[] content = IOUtils.readFromBinaryFile(gf.getFile());

		final EntityObjectVO<UID> evo = new EntityObjectVO<UID>(E.SERVERCODE.getUID());
		evo.setFieldValue(E.SERVERCODE.source, new String(content));
		evo.setFieldValue(E.SERVERCODE.name, gf.getName());
		evo.setFieldValue(E.SERVERCODE.description, "created by nuclos eclipse plugin");
		evo.setFieldValue(E.SERVERCODE.debug, false);
		evo.setFieldValue(E.SERVERCODE.active, true);

		// search for nuclet
		UID nucletUID = null;
		String foundNucletPackage = null;
		for (EntityObjectVO<UID> nucletVO : MetaProvider.getInstance().getNuclets()) {
			String sNucletPackage = nucletVO.getFieldValue(E.NUCLET.packagefield);
			if (gf.getName().startsWith(sNucletPackage)) {
				if (nucletUID == null) {
					nucletUID = nucletVO.getPrimaryKey();
					foundNucletPackage = sNucletPackage;
				} else {
					if (foundNucletPackage.length() < sNucletPackage.length()) {
						nucletUID = nucletVO.getPrimaryKey();
						foundNucletPackage = sNucletPackage;
					}
				}
			}
		}
		evo.setFieldUid(E.SERVERCODE.nuclet, nucletUID);

		entityObjectFacade.createOrUpdatePlainWithoutPermissionCheck(evo);
		LOG.info("Created event support rule in db: name={} uid={} from {}",
				gf.getName(), gf.getPrimaryKey(), gf.getFile());

		// Update GeneratedFile and re-write it to include prefix
		gf.setPrimaryKey(evo.getPrimaryKey());
		gf.setType("org.nuclos.server.customcode.valueobject.CodeVO");
		final Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(gf.getFile()), "UTF-8"));
		try {
			out.write(gf.getContent());
			out.write(gf.getPropertiesAsComment());
		} finally {
			out.close();
		}

		return evo;
	}

	private MasterDataVO<UID> getCodeAsMd(EntityMeta<?> entity, UID pk) throws CommonFinderException, CommonPermissionException {
		return masterDataFacade.get(entity.getUID(), pk);
	}

	private void scanDir(List<File> result, File dir, long modified, String ext) {
		final File[] files = dir.listFiles();
		for (File f : files) {
			if (f.isDirectory()) {
				scanDir(result, f, modified, ext);
			} else if (f.isFile()) {
				if (f.getName().endsWith(ext) && f.lastModified() > modified) {
					result.add(f);
				}
			}
		}
	}

	GeneratedFile parseFile(File file) throws IOException {
		final GeneratedFile result = new GeneratedFile(NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT));
		result.setFile(file);
		final MessageDigestInputStream in = new MessageDigestInputStream(new FileInputStream(file), SourceCache.DIGEST);

		try (
				final BufferedReader reader = new BufferedReader(
						new InputStreamReader(in, NuclosCodegeneratorConstants.JAVA_SRC_ENCODING)
				);
				final CharArrayWriter out = new CharArrayWriter()
		) {
			Map<String, String> properties = null;
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.equals(COMMENT_START)) {
					properties = readPropertiesComment(reader);
					properties.forEach((field, value) -> {
						if ("name".equals(field)) {
							result.setName(value);
						} else if ("classname".equals(field)) {
							result.setTargetClassName(value);
						} else if ("type".equals(field)) {
							result.setType(value);
						} else if ("entity".equals(field)) {
							result.setEntity(E.getByName(value));
						} else if ("class".equals(field)) {
							result.setGeneratorClass(value);
						} else if ("uid".equals(field)) {
							result.setPrimaryKey(new UID(value));
						} else {
							LOG.info("Unknown field '{}' with value '{}' in {}", field, value, file);
						}
					});
				} else {
					out.append(line);
					out.append("\n");
				}
			}
			if (properties == null || properties.isEmpty()) {
				throw new IllegalStateException("Parse rule: Can't find CodeGenerator properties in " + file);
			}

			if ("org.nuclos.server.ruleengine.valueobject.RuleVO".equals(result.getType())) {
				copyRule(reader, out);
			} else {
				copy(reader, out);
			}

			if (out.size() <= 0) {
				throw new IllegalStateException();
			}

			// Trim additional empty lines, leave only 1 line break at the end.
			final String trimmedContent = StringUtils.trimToEmpty(out.toString()) + "\n";

			result.setContent(trimmedContent.toCharArray());
		}
		result.setHashValue(in.digestAsBase64());
		return result;
	}

	private Map<String, String> readPropertiesComment(BufferedReader reader) throws IOException {
		Map<String, String> result = new TreeMap<>();

		String line = reader.readLine();
		while (StringUtils.startsWith(line, "//") && !StringUtils.startsWith(line, COMMENT_END)) {
			final Matcher m = PROP_PAT.matcher(line);
			if (m.matches()) {
				String field = m.group(1);
				String value = m.group(2);
				result.put(field, value);
			}
			line = reader.readLine();
		}

		return result;
	}

	private void copy(Reader r, Writer w) throws IOException {
		final char[] buffer = new char[4092];
		int size;
		while ((size = r.read(buffer)) >= 0) {
			w.write(buffer, 0, size);
		}
	}

	private void copyRule(BufferedReader r, Writer w) throws IOException {
		String line;
		boolean begin = false;
		// only copy the 'rule' part of the source
		while ((line = r.readLine()) != null) {
			if (line.trim().equals("// BEGIN RULE")) {
				begin = true;
				break;
			}
		}
		boolean end = false;
		while ((line = r.readLine()) != null) {
			if (line.trim().equals("// END RULE")) {
				end = true;
				break;
			}
			w.write(line, 0, line.length());
			w.write("\n");
		}
		if (!(begin && end)) {
			throw new IllegalStateException("Parse rule: Can't find begin and end");
		}
	}

}
