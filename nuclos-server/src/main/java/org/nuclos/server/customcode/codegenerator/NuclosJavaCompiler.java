//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.customcode.codegenerator;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;

import org.apache.commons.collections.CollectionUtils;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.nuclos.common2.security.MessageDigestOutputStream;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.customcode.codegenerator.CodeGenerator.JavaSourceAsString;
import org.nuclos.server.eventsupport.ejb3.SourceCache;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class NuclosJavaCompiler implements Closeable {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosJavaCompiler.class);

	// Spring injection

	@Autowired
	private NuclosJavaCompilerComponent nuclosJavaCompilerComponent;

	@Autowired
	private GeneratorClasspathComponent generatorClasspathComponent;

	@Autowired
	private NuclosJavaCompilerExceptionCache compilExepCache;

	@Autowired
	private SourceCache sourceCache;

	// End of Spring injection 

	private final Locale locale;

	private JavaCompiler javac;

	private CodeGeneratorDiagnosticListener diagnosticListener;

	private StandardJavaFileManager stdFileManager;
	private StandardJavaFileManager stdFileManagerForApiOnly;

	private static final String SAVE_SRC_LOCK = "saveSrcLock";

	NuclosJavaCompiler() {
		this(null);
	}

	NuclosJavaCompiler(Locale locale) {
		this.locale = locale;
	}

	@PostConstruct
	final void init() {
		// We use Java 7's compiler API...
		javac = NuclosJavaCompilerComponent.getJavaCompilerTool();
		if (javac == null) {
			throw new NuclosFatalException("No registered system Java compiler found");
		}
		this.diagnosticListener = new CodeGeneratorDiagnosticListener(locale);
		this.stdFileManager = javac.getStandardFileManager(diagnosticListener, locale, null);
		this.stdFileManagerForApiOnly = javac.getStandardFileManager(diagnosticListener, locale, null);
		try {
			// add libs from tomcat
			String catalinaHome = (String) System.getProperties().get("catalina.home");
			List<File> classpath = new ArrayList<File>();
			if (catalinaHome != null) {
				catalinaHome = catalinaHome + File.separator + "lib";
				classpath.addAll(generatorClasspathComponent.getLibs(new File(catalinaHome), false));
			}
			classpath.addAll(generatorClasspathComponent.getLibs(
					NuclosSystemParameters.getDirectory(NuclosSystemParameters.WSDL_GENERATOR_LIB_PATH), false));
			classpath.add(NuclosCodegeneratorConstants.JARFILE);
			classpath.add(NuclosCodegeneratorConstants.BOJARFILE);
			classpath.add(NuclosCodegeneratorConstants.STATEMODELJARFILE);
			classpath.add(NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE);
			classpath.add(NuclosCodegeneratorConstants.PARAMETERJARFILE);
			classpath.add(NuclosCodegeneratorConstants.COMMUNICATIONJARFILE);
			classpath.add(NuclosCodegeneratorConstants.GENERATIONJARFILE);
			classpath.add(NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFSJARFILE);
			classpath.add(NuclosCodegeneratorConstants.REPORTJARFILE);
			classpath.add(NuclosCodegeneratorConstants.PRINTOUTJARFILE);
			classpath.add(NuclosCodegeneratorConstants.USERROLEJARFILE);
			classpath.add(NuclosCodegeneratorConstants.WEBSERVICEJARFILE);
			stdFileManager.setLocation(StandardLocation.CLASS_PATH, CollectionUtils.union(classpath,
					generatorClasspathComponent.getExpandedSystemParameterClassPath(false)));
			stdFileManagerForApiOnly.setLocation(StandardLocation.CLASS_PATH, CollectionUtils.union(classpath,
					generatorClasspathComponent.getExpandedSystemParameterClassPath(true)));

			addLocations(stdFileManager);
			addLocations(stdFileManagerForApiOnly);

		} catch (IOException e) {
			throw new NuclosFatalException(e);
		}
	}

	private void addLocations(StandardJavaFileManager sjfm) throws IOException {
		sjfm.setLocation(StandardLocation.SOURCE_PATH,
				Collections.singleton(nuclosJavaCompilerComponent.getSourceOutputPath()));
		sjfm.setLocation(StandardLocation.SOURCE_OUTPUT,
				Collections.singleton(nuclosJavaCompilerComponent.getSourceOutputPath()));
		sjfm.setLocation(StandardLocation.CLASS_OUTPUT,
				Collections.singleton(getBuildOutputPath()));
	}

	private File getBuildOutputPath() {
		File dir = new File(nuclosJavaCompilerComponent.getBuildOutputPath(), "src");
		if (!dir.exists()) {
			dir.mkdir();
		}
		return dir;
	}

	synchronized Map<String, byte[]> javac(List<CodeGenerator> generators, boolean saveSrc) throws NuclosCompileException {
		final Map<JavaFileObject, CodeGenerator> sources = new HashMap<JavaFileObject, CodeGenerator>();
		for (CodeGenerator generator : generators) {
			if (generator.isRecompileNecessary()) {
				for (JavaFileObject jfo : generator.getSourceFiles()) {
					final CodeGenerator old = sources.put(jfo, generator);
					if (old != null) {
						if (jfo instanceof JavaSourceAsString) {
							LOG.warn("Duplicate class: {} from 2 CodeGenerators: "
											+ "old={} and new={}, saveSrc={}",
									((JavaSourceAsString) jfo).getFQName(), old, generator, saveSrc);
						} else {
							LOG.warn("Duplicate class: {} from 2 CodeGenerators: "
											+ "old={} and new={}, saveSrc={}",
									jfo.getName(), old, generator, saveSrc);
						}
						throw new NuclosCompileException("nuclos.compiler.duplicateclasses");
					}
				}
			}
		}
		LOG.info("Execute Java compiler for source files: {}", sources);

		if (saveSrc) {
			try {
				File directory = NuclosSystemParameters.getDirectory(NuclosSystemParameters.GENERATOR_OUTPUT_PATH);
				File f = new File(directory, "/src/");
				if (f.exists()) {
					AbstractNuclosObjectCompiler.removeDirectory(f);
				}

				f.mkdir();

				saveSrc(generators);
			} catch (IOException e1) {
				// Source is saved to disk just for debugging purposes
				LOG.warn("javac failed: {}", e1);
			}
		}

		if (sources.size() > 0) {
			List<String> options = new ArrayList<>();
			options.add("-g");
			options.add("-encoding");
			options.add("UTF-8");

			ByteArrayOutputFileManager byteFileManagerApiOnly = new ByteArrayOutputFileManager(stdFileManagerForApiOnly);

			Set<JavaFileObject> sourcesApiOnly = new HashSet<>();
			for (JavaFileObject jfo : sources.keySet()) {
				sourcesApiOnly.add(jfo);
			}

			boolean success = true;
			if (!sourcesApiOnly.isEmpty()) {
				LOG.info("api compile of {}", sourcesApiOnly);
				CompilationTask task = javac.getTask(null, byteFileManagerApiOnly, diagnosticListener, options, null, sourcesApiOnly);
				task.setLocale(locale);
				if (!task.call()) {
					success = false;
				}
			}
			List<ErrorMessage> errors = diagnosticListener.clearErrors();

			compilExepCache.clear();

			if (!success) {
				LOG.info("Compile failed with {} errors:", errors.size());
				for (ErrorMessage em : errors) {
					LOG.info("{}", em);
				}

				// Store exceptions in cache for later fixing
				compilExepCache.add(errors);

				throw new NuclosCompileException(errors);
			}

			Map<String, byte[]> output = new HashMap<>();
			try {
				if (!sourcesApiOnly.isEmpty()) {
					output.putAll(byteFileManagerApiOnly.getOutput());
					byteFileManagerApiOnly.flush();
				}
			} catch (IOException e) {
				throw new NuclosCompileException(e);
			}
			nuclosJavaCompilerComponent.setLastSrcCompileTime(System.currentTimeMillis());
			return output;
		} else {
			nuclosJavaCompilerComponent.setLastSrcCompileTime(System.currentTimeMillis());
			return new HashMap<>();
		}
	}

	private synchronized void saveSrc(List<CodeGenerator> generators) throws IOException {
		nuclosJavaCompilerComponent.setCurrentlyWritingSources(true);
		try {
			for (CodeGenerator generator : generators) {
				saveSrc(generator, false);
			}
		} finally {
			nuclosJavaCompilerComponent.setCurrentlyWritingSources(false);
		}
	}

	void saveSrc(CodeGenerator generator, boolean remove) throws IOException {
		final boolean writingSources = nuclosJavaCompilerComponent.isCurrentlyWritingSources();
		if (!writingSources) {
			nuclosJavaCompilerComponent.setCurrentlyWritingSources(true);
		}
		try {
			if (generator.isRecompileNecessary()) {
				for (JavaSourceAsString srcobject : generator.getSourceFiles()) {
					synchronized (SAVE_SRC_LOCK) {
						final File f = generator.getJavaSrcFile(srcobject);
						if (remove) {
							final boolean success = f.delete();
							if (!success) {
								LOG.warn("Unable to delete " + f);
							}
						} else {
							if (!f.exists()) {
								f.getParentFile().mkdirs();
								f.createNewFile();
							}
							try (
									final MessageDigestOutputStream mdos = new MessageDigestOutputStream(new FileOutputStream(f), SourceCache.DIGEST);
									final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(mdos,
											NuclosCodegeneratorConstants.JAVA_SRC_ENCODING))
							) {
								generator.writeSource(out, srcobject);
							} finally {
								sourceCache.updateHashValue(srcobject.getFQName(), generator.hashForManifest());
							}
						}
					}
				}
			}
		} finally {
			if (!writingSources) {
				nuclosJavaCompilerComponent.setCurrentlyWritingSources(false);
			}
		}
	}

	@Override
	public void close() throws IOException {
		if (stdFileManager != null) {
			stdFileManager.close();
		}
		if (stdFileManagerForApiOnly != null) {
			stdFileManagerForApiOnly.close();
		}
	}

	/**
	 * A diagnostic listener which collects the error messages.  It recognizes <code>GeneratedJavaFileObject</code>s
	 * and automatically adjust the line and position offsets.
	 */
	static class CodeGeneratorDiagnosticListener implements DiagnosticListener<JavaFileObject> {

		private final Locale locale;
		private final List<NuclosCompileException.ErrorMessage> errors;

		CodeGeneratorDiagnosticListener(Locale locale) {
			this.locale = locale;
			this.errors = new ArrayList<>();
		}

		@Override
		public synchronized void report(Diagnostic<? extends JavaFileObject> diag) {
			if (diag.getKind() == Diagnostic.Kind.ERROR) {
				JavaFileObject source = diag.getSource();
				String packageName = null;
				try {
					String content = source.getCharContent(true).toString();
					if (content != null && content.indexOf("package") >= 0) {
						packageName = content.substring(content.indexOf("package") + 7, content.indexOf(";")).trim();
					}
				} catch (Exception e) {
					// nothing can be done.
				}
				String message = getMessageWithoutPath(diag);
				if (message == null || message.isEmpty())
					message = "Unknown error";

				if (source != null && source.getClass().getName().equals("com.sun.tools.javac.api.ClientCodeWrapper$WrappedJavaFileObject")) {
					try {
						Field clientFileObject = source.getClass().getSuperclass().getDeclaredField("clientFileObject");
						clientFileObject.setAccessible(true);
						Object fieldValue = clientFileObject.get(source);
						if (source instanceof JavaFileObject)
							source = (JavaFileObject) fieldValue;
					} catch (Exception e) {
						// nothing can be done.
					}
				}

				if (source instanceof JavaSourceAsString) {
					final JavaSourceAsString t = (JavaSourceAsString) source;
					long line = diag.getLineNumber();
					if (line != Diagnostic.NOPOS && source.getKind() == JavaFileObject.Kind.SOURCE) {
						if (message.startsWith(line + ":"))
							message = message.substring((line + ":").length());
					}
				}

				String sourcename = source == null ? "" : source.getName(); // physical or symbolic source name
				UID entityUid = null;

				if (source instanceof JavaSourceAsString) {
					JavaSourceAsString jas = (JavaSourceAsString) source;
					sourcename = jas.getLabel();
					entityUid = jas.getEntityUid();

				}

				errors.add(
						new ErrorMessage(
								diag.getKind(),
								sourcename,
								message,
								entityUid,
								packageName,
								diag.getLineNumber(),
								diag.getColumnNumber(),
								diag.getPosition(),
								diag.getStartPosition(),
								diag.getEndPosition()
						)
				);
			}
		}

		public synchronized List<NuclosCompileException.ErrorMessage> clearErrors() {
			List<NuclosCompileException.ErrorMessage> result = new ArrayList<NuclosCompileException.ErrorMessage>(errors);
			errors.clear();
			return result;
		}

		private String getMessageWithoutPath(Diagnostic<? extends JavaFileObject> diag) {
			String message = diag.getMessage(locale);
			JavaFileObject source = diag.getSource();
			if (source != null && message != null) {
				String path = source.toUri().getPath();
				if (path != null && message.startsWith(path + ":"))
					message = message.substring(path.length() + 1);
				String lineNumber = "" + diag.getLineNumber();
				if (message.startsWith(diag.getLineNumber() + ":"))
					message = message.substring(lineNumber.length() + 1);
				message = message.trim();
			}
			return message;
		}

		private static long shift(long pos, long delta) {
			if (pos != Diagnostic.NOPOS) {
				pos -= delta;
				if (pos < 0)
					pos = 0;
			}
			return pos;
		}
	}

}
