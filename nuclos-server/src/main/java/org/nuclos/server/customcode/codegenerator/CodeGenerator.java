//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import static org.nuclos.server.customcode.codegenerator.GeneratedFile.COMMENT_END;
import static org.nuclos.server.customcode.codegenerator.GeneratedFile.COMMENT_START;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.util.Map;

import javax.tools.SimpleJavaFileObject;

import org.nuclos.common.UID;

/**
 * Interface to be implemented for generating code from anything.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public interface CodeGenerator {

	boolean isRecompileNecessary();

	Iterable<? extends JavaSourceAsString> getSourceFiles();

	byte[] postCompile(String name, byte[] bytecode);

	/**
	 * Additional properties for the code scanner, to be appended to the file as comments.
	 */
	Map<String, String> getProperties();

	File getJavaSrcFile(JavaSourceAsString src);

	void writeSource(Writer writer, JavaSourceAsString src) throws IOException;

	/**
	 * hashForManifest() *must* return the same value for the same content of
	 * source file(s) only. This is in contrast to hashCode()!!! (tp)
	 */
	String hashForManifest();

	/**
	 * hashCode() *must* return the same value for same (fully-qualified) class name to ensure
	 * that generator is changed when doing on disk source scanning. (tp)
	 */
	int hashCode();

	/**
	 * equals() *must* return true for same (fully-qualified) class name to ensure
	 * that generator is changed when doing on disk source scanning. (tp)
	 */
	boolean equals(Object other);

	static String propertiesToComment(final Map<String, String> properties) {
		StringBuilder sb = new StringBuilder();

		if (!properties.isEmpty()) {
			sb.append("\n\n");
			sb.append(COMMENT_START);

			properties.forEach((key, value) -> {
				sb.append("\n// ");
				sb.append(key);
				sb.append("=");
				sb.append(value);
			});

			sb.append("\n");
			sb.append(COMMENT_END);
			sb.append("\n");
		}

		return sb.toString();
	}

	class JavaSourceAsString extends SimpleJavaFileObject {
		private final String name;
		private final String source;
		private final String postfix;
		private final UID entityUid;

		// TODO: Use a builder.
		JavaSourceAsString(
				String name,
				String source,
				Map<String, String> properties,
				UID entityUID
		) {

			super(URI.create(name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
			this.name = name;
			this.source = source;
			this.postfix = propertiesToComment(properties);
			this.entityUid = entityUID;
		}

		public String getSource() {
			return source;
		}

		public String getPostfix() {
			return postfix;
		}

		@Override
		public CharSequence getCharContent(boolean ignoreEncodingErrors) {
			return source + postfix;
		}

		@Override
		public String getName() {
			String filename;
			if (name.contains(".")) {
				if (name.endsWith(".wsdl")) {
					return name;
				} else {
					filename = name.substring(name.lastIndexOf('.') + 1);
				}
			} else {
				filename = name;
			}
			return filename + Kind.SOURCE.extension;
		}

		public String getFQName() {
			return name;
		}

		// Note: toString() should be meaningless because it's not part of the public JSR199 Compiler API;
		// However, toString() is misused by Sun's javac for deriving the classfile's [SourceFile]
		// attribute (cf. JVM spec 4.8.10 and com.sun.tools.javac.jvm.ClassWriter#writeClassFile) which
		// in turn is used by debuggers for source code lookup.  Hence, a wrong value will confuse debuggers.
		// This misusing behaviour is prevalent in JDK6 (at least up to 6u21), but seems to be fixed in
		// (Open)JDK's head:
		// http://hg.openjdk.java.net/jdk6/jdk6/langtools/diff/5c2858bccb3f/src/share/classes/com/sun/tools/javac/jvm/ClassWriter.java
		@Override
		public String toString() {
			return getName();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			JavaSourceAsString other = (JavaSourceAsString) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		public String getPath() {
			return name.replace('.', '/') + Kind.SOURCE.extension;
		}

		public String getLabel() {
			return getName();
		}

		public UID getEntityUid() {
			return this.entityUid;
		}
	}
}
