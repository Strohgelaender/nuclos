//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.io.IOException;
import java.util.Timer;

import javax.annotation.PostConstruct;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Thomas Pasch
 */
@Component
public class SourceScannerComponent {
	
	private final long INTERVAL_MILLIS = 1000 * 20;
	
	//
	
	private SourceScannerTask instance;
	
	private boolean scheduled = false;
	
	private boolean canceled = false;
	
	// Spring injection
	
	private Timer timer;
	
	// End of Spring injection
	
	SourceScannerComponent() {
	}
	
	@Autowired
	void setTimer(Timer timer) {
		this.timer = timer;
	}
	
	@PostConstruct
	synchronized final void init() {
		instance = new SourceScannerTask();
		schedule();
	}
	
	private synchronized SourceScannerTask get() {
		return instance;
	}
	
	public GeneratedFile parseFile(File file) throws IOException {
		return get().parseFile(file);
	}
	
	public void updatePlainCode(GeneratedFile file) throws NuclosBusinessRuleException, CommonFinderException, 
		CommonPermissionException, CommonCreateException, CommonRemoveException, CommonStaleVersionException, 
		CommonValidationException, NuclosCompileException {
		
		get().updatePlainCode(file);
	}
	
	public EntityObjectVO createEventSupportRule(GeneratedFile gf) throws IOException, CommonPermissionException {
		return get().createEventSupportRule(gf);
	}
	
	public synchronized void cancel() {
		if (scheduled) {
			instance.cancel();
			scheduled = false;
			canceled = true;
		}
	}
	
	public synchronized void schedule() {
		if (canceled) {
			instance = new SourceScannerTask();
			canceled = false;
		}
		if (!scheduled && NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			timer.schedule(instance, INTERVAL_MILLIS, INTERVAL_MILLIS);
			scheduled = true;
		}
	}
	
	public synchronized void runOnce() {
		if (!scheduled) {
			SourceScannerTask tmpInstance = new SourceScannerTask();
			tmpInstance.setForceReadingSourcesOn();
			timer.schedule(tmpInstance, 0);
		}
	}

	public boolean isScheduled() {
		return scheduled;
	}
	
}
