//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.nuclos.common.NuclosFatalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * CodeGenerator used by source scanner task
 * <p>
 * Deprecated: Not used any more in SourceScannerTask
 *
 * @author Thomas Pasch
 * @see SourceScannerTask
 * @deprecated
 */
@Configurable
@Deprecated
public class OnDiskCodeGenerator implements CodeGenerator {

	private NuclosJavaCompilerComponent nuclosJavaCompilerComponent;

	private final GeneratedFile gf;

	private JavaSourceAsString src;

	public OnDiskCodeGenerator(GeneratedFile gf) {
		this.gf = gf;
	}

	@Autowired
	final void setNuclosJavaCompilerComponent(NuclosJavaCompilerComponent nuclosJavaCompilerComponent) {
		this.nuclosJavaCompilerComponent = nuclosJavaCompilerComponent;
	}

	@PostConstruct
	final void init() {
		src = new JavaSourceAsString(
				gf.getName(),
				new String(gf.getContent()),
				getProperties(),
				gf.getPrimaryKey()
		);
	}

	@Override
	public boolean isRecompileNecessary() {
		return true;
	}

	@Override
	public Iterable<? extends JavaSourceAsString> getSourceFiles() {
		return Collections.singletonList(src);
	}

	@Override
	public byte[] postCompile(String name, byte[] bytecode) {
		return bytecode;
	}

	@Override
	public Map<String, String> getProperties() {
		return gf.getProperties();
	}

	@Override
	public File getJavaSrcFile(JavaSourceAsString src) {
		return new File(nuclosJavaCompilerComponent.getSourceOutputPath(), src.getPath());
	}

	@Override
	public void writeSource(Writer writer, JavaSourceAsString src) throws IOException {
		writer.write(src.getSource());
		writer.write(src.getPostfix());
	}

	@Override
	public String hashForManifest() {
		String sourceFileContent = src.getSource() + src.getPostfix();
		try {
			return PlainCodeGenerator.hashForManifest(sourceFileContent.getBytes(NuclosCodegeneratorConstants.JAVA_SRC_ENCODING));
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}

	@Override
	public int hashCode() {
		return src.getName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof CodeGenerator)) {
			return false;
		}
		final CodeGenerator other = (CodeGenerator) obj;
		final JavaSourceAsString firstOtherSrc;
		if (!other.isRecompileNecessary()) {
			return false;
		}
		firstOtherSrc = other.getSourceFiles().iterator().next();
		return src.getFQName().equals(firstOtherSrc.getFQName());
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder("OnDiskCG[code=");
		result.append(gf.getName());
		result.append(",file=").append(gf.getFile());
		result.append("]");
		return result.toString();
	}

}
