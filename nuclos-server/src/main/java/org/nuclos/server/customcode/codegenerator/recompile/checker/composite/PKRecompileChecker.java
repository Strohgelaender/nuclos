package org.nuclos.server.customcode.codegenerator.recompile.checker.composite;

import org.nuclos.common.UID;
import org.nuclos.server.customcode.codegenerator.recompile.checker.IRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Checks if the PK was modified.
 */
public class PKRecompileChecker implements IRecompileChecker {

    private boolean isPKEqual(
        MasterDataVO<?> oldEntity,
        MasterDataVO<?> updatedEntity) {

        UID oldFormPK = (UID) oldEntity.getPrimaryKey();
        UID newFormPK = (UID) updatedEntity.getPrimaryKey();
        return oldFormPK.equals(newFormPK);
    }

    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        // nop
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(
        MasterDataVO<?> oldEntity,
        MasterDataVO<?> updatedEntity) {

        return !isPKEqual(oldEntity, updatedEntity);
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        return false;
    }

}
