//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.customcode.ejb3;

import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.EventSupportNotification;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent;
import org.nuclos.server.customcode.codegenerator.PlainCodeGenerator;
import org.nuclos.server.customcode.valueobject.CodeVO;
import org.nuclos.server.eventsupport.ejb3.SourceCache;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseException;
import com.github.javaparser.ParseProblemException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.expr.Name;

@RolesAllowed("Login")
@Transactional(noRollbackFor= {Exception.class})
public class CodeFacadeBean extends NuclosFacadeBean implements CodeFacadeLocal, CodeFacadeRemote {
	
	// Spring injection

	@Autowired
	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private NuclosJavaCompilerComponent nuclosJavaCompilerComponent;

	@Autowired
	private CustomCodeManager customCodeManager;

	@Autowired
	private EventSupportCache esCache;
	
	@Autowired
	private SourceCache sourceCache;

	@Autowired
	private SpringLocaleDelegate localeDelegate;

	// end of Spring injection
	
	public CodeFacadeBean() {
	}

	public MasterDataVO<UID> create(MasterDataVO<UID> vo) throws CommonBusinessException {
		checkWriteAllowed(vo.getEntityObject().getDalEntity());

		CodeVO codevo = MasterDataWrapper.getServerCodeVO(vo);

		try {
			check(vo, codevo.isActive());			
		} catch (NuclosCompileException e) {
			// When classes cannot be build we have to mark them in RuleManagement
			NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILERRORS, JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION);
			NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILERRORS, JMSConstants.TOPICNAME_RULECOMPILATION);
			throw e;
		}
		
		getNameFromSource(codevo);
		codevo.validate();

		vo = masterDataFacade.create(MasterDataWrapper.wrapCodeVO(codevo), null);
		try {
			// check again to generate source files with valid header (otherwise no UID is set)
			check(vo, codevo.isActive());			
			NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILED, JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION);
		} catch (NuclosCompileException e) {
			throw new NuclosFatalException(e);
		}
	
		sourceCache.addOrUpdate(codevo);
		sourceCache.updateHashValue(codevo.getName(), PlainCodeGenerator.hashForManifest(codevo));
		esCache.invalidate(E.SERVERCODE);
		
		notifyEventSupportManagement();
		
		return vo;
	}

	private void notifyEventSupportManagement() {	
		//NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILED, JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION);
		forceCompilationInClusterCloud();
		NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILED, JMSConstants.TOPICNAME_RULECOMPILATION);
	}
	
	public MasterDataVO modify(MasterDataVO vo) throws CommonBusinessException {
		checkWriteAllowed(vo.getEntityObject().getDalEntity());

		NuclosCodegeneratorConstants.JARFILE.delete();
		
		CodeVO codevo = MasterDataWrapper.getServerCodeVO(vo);
		
		try {
			check(vo, codevo.isActive());	
		} catch (NuclosCompileException e) {
			// When classes (not the current changed) cannot be build we have to mark them in RuleManagement
			NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILERRORS, JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION);
			NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILERRORS, JMSConstants.TOPICNAME_RULECOMPILATION);
			throw e;
		}

		getNameFromSource(codevo);
		codevo.validate();
		
		UID uid = masterDataFacade.modify(MasterDataWrapper.wrapCodeVO(codevo), null);
		MasterDataVO<UID> mdVO = masterDataFacade.get(vo.getEntityObject().getDalEntity(), uid);
		
		sourceCache.updateHashValue(codevo.getName(), PlainCodeGenerator.hashForManifest(codevo));
		esCache.invalidate(E.SERVERCODE);
		
		notifyEventSupportManagement();
		
		return mdVO;
	}
	
	private void forceCompilationInClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.RULE_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);	
	}

	public void remove(MasterDataVO<UID> vo) throws CommonBusinessException {
		checkWriteAllowed(vo.getEntityObject().getDalEntity());

		NuclosCodegeneratorConstants.JARFILE.delete();
		
		CodeVO cvo = MasterDataWrapper.getServerCodeVO(vo);
		if (cvo.isActive()) {
			try {
				nuclosJavaCompilerComponent.check(new PlainCodeGenerator(cvo), true);
			} catch (NuclosCompileException e) {
				// When classes cannot be build we have to mark them in RuleManagement
				NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILERRORS, JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION);
				NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILERRORS, JMSConstants.TOPICNAME_RULECOMPILATION);
				throw e;
			}
		}
		
		masterDataFacade.remove(vo.getEntityObject().getDalEntity(), vo.getPrimaryKey(), true);

		NuclosCodegeneratorConstants.JARFILE.delete();
		
		esCache.invalidate(E.SERVERCODE);
		
		notifyEventSupportManagement();
	}

	public void check(MasterDataVO<UID> vo) throws CommonBusinessException {
		checkWriteAllowed((vo.getEntityObject().getDalEntity()));
		check(vo, false);
	}

	private void check(MasterDataVO<UID> vo, boolean active) throws CommonBusinessException, NuclosCompileException {
		CodeVO codevo = MasterDataWrapper.getServerCodeVO(vo);
		getNameFromSource(codevo);
		nuclosJavaCompilerComponent.check(new PlainCodeGenerator(codevo), !active);
	}

	private void getNameFromSource(CodeVO vo) throws CommonValidationException, NuclosCompileException {
		String fullName = "";
		CompilationUnit cu;
		try {
			cu = JavaParser.parse(new ByteArrayInputStream(vo.getSource().getBytes()));

			Optional<PackageDeclaration> packageDeclaration = cu.getPackageDeclaration();
			if (packageDeclaration.isPresent()) {
				Name name = packageDeclaration.get().getName();
				if (name != null) {
					fullName = name.toString() + ".";
				}
			}

			if (cu.getTypes() != null && cu.getTypes().size() > 0) {
				fullName += cu.getTypes().get(0).getName();
			}
			else {
				if (StringUtils.isNullOrEmpty(fullName)) {
					throw new ParseException("CodeFacadeBean.exception.extractname");
				}
				else {
					fullName += "package-info";
				}
			}
			vo.setName(fullName);
		}
		catch (ParseException | ParseProblemException e) {
			// vo.setName("temp");

			// throw exception from compiler with more details
			nuclosJavaCompilerComponent.compile(false);

			// If compiler does not throw an exception: our parser has failed!
			// If we already have an name, we use it.
			// We throw an exception otherwise. (tp)
			if (StringUtils.isNullOrEmpty(fullName)) {
				vo.setName("temp");
				String errorReason = StringUtils.firstLine(e.getMessage());
				String message = localeDelegate.getMessage(
						"CodeFacadeBean.exception.extractname",
						errorReason,
						errorReason
				);
				throw new CommonValidationException(message);
			}
		}
	}

	public List<CodeVO> getAll(UID entity) throws CommonPermissionException {
		try {
			checkReadAllowed(entity);		
		} catch (CommonPermissionException e) {
			return Collections.EMPTY_LIST;
		}
		return CollectionUtils.transform(masterDataFacade.getMasterData(
				metaProvider.<UID>getEntity(entity), null),
					(MasterDataVO<UID> i) -> MasterDataWrapper.getServerCodeVO(i));
	}

	public Object invokeFunction(String functionname, Object[] args) {
		return customCodeManager.invokeFunction(functionname, args);
	}

	@Override
	@RolesAllowed("login")
	public boolean isAutomaticRuleCompilation() {
		return nuclosJavaCompilerComponent.isAutomaticRuleCompilation();
	}
	
	@Override
	@RolesAllowed("login")
	public void setAutomaticRuleCompilation(boolean ruleCompilation) {
		nuclosJavaCompilerComponent.setAutomaticRuleCompilation(ruleCompilation);
	}
	
}
