package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if the nuclet parameters require a recompile.
 */
@Component
public class NucletParameterRecompileChecker extends CompositeRecompileChecker {

    public NucletParameterRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.NUCLETPARAMETER.name),
              new UIDFieldRecompileChecker(E.NUCLETPARAMETER.nuclet));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted nuclet params force a recompile
        return true;
    }

}
