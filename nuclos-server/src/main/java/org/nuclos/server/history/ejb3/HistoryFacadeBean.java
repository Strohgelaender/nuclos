//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.history.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.history.HistoryVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for all master data and modul data management functions.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor = {Exception.class})
public class HistoryFacadeBean extends NuclosFacadeBean implements HistoryFacadeLocal, HistoryFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(HistoryFacadeBean.class);

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;

	private LayoutFacadeLocal layoutFacade;

	private MasterDataFacadeLocal masterDataFacade;

	private MasterDataFacadeLocal getMasterDataFacade() {
		if (masterDataFacade == null)
			masterDataFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
		return masterDataFacade;
	}

	private LayoutFacadeLocal getLayoutFacade() {
		if (layoutFacade == null)
			layoutFacade = ServerServiceLocator.getInstance().getFacade(LayoutFacadeLocal.class);
		return layoutFacade;
	}

	public void removeHistoryEntries(Object govoId) {
		DbStatement stmt = null;
		if (govoId instanceof Number)
			stmt = DbStatementUtils.deleteFrom(E.HISTORY, E.HISTORY.entityobjectid, Long.valueOf(govoId.toString()));
		if (govoId instanceof UID)
			stmt = DbStatementUtils.deleteFrom(E.HISTORY, E.HISTORY.entityobjectuid, (UID) govoId);

		if (stmt != null)
			dataBaseHelper.getDbAccess().execute(stmt);
	}

	/**
	 * creates a HistoryEntry
	 * <p>
	 * TODO: Too many parameters.
	 */
	private void createHistoryEntry(
			Object savePoint,
			String username,
			Object lObjectId,
			InternalTimestamp dValidUntil,
			UID entityUID,
			UID entityFieldUID,
			Object iRefId,
			Object oValue
	) {
		FieldMeta<?> efMeta = metaProvider.getAllEntityFieldsByEntity(entityUID).get(entityFieldUID);
		if (efMeta == null) {
			if (entityFieldUID.equals(SF.VERSION.getUID(entityUID))) {
				efMeta = SF.VERSION.getMetaData(entityUID);
			} else
				return; // if field equals 'version' we can handle that. skip others.
		}
		final boolean bShouldStoreAsByte = shouldStoreAsByte(efMeta);
		if (SF.isEOField(efMeta.getEntity(), efMeta.getUID()) && oValue == null) {
			return; // skip if for example status is null. 
		}
		final MasterDataVO<Long> mdvo = MasterDataWrapper.wrapHistoryVO(new HistoryVO(new NuclosValueObject<>(),
				(lObjectId instanceof Number ? Long.valueOf(lObjectId.toString()) : null),
				(lObjectId instanceof UID ? (UID) lObjectId : null),
				entityUID,
				SF.isEOField(efMeta.getFieldName()) ? null : entityFieldUID,
				!SF.isEOField(efMeta.getFieldName()) ? null : entityFieldUID,
				savePoint.toString(),
				username,
				dValidUntil,
				bShouldStoreAsByte ? null : HistoryVO.getObjectValueAsString(efMeta, oValue),
				!bShouldStoreAsByte ? null : HistoryVO.getObjectValueAsByteArray(oValue),
				(iRefId instanceof Number ? Long.valueOf(iRefId.toString()) : null),
				(iRefId instanceof UID ? (UID) iRefId : null)));

		try {
			getMasterDataFacade().create(mdvo, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		} catch (NuclosBusinessException | CommonCreateException | CommonPermissionException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	public void trackChangesToLogbook(EntityObjectVO<?> eoOld, EntityObjectVO<?> eoNew) {
		if (!isLogbookTrackingActive(eoNew)) {
			return;
		}

		createSavepoint();

		Set<UID> setExcludedAttributeIds = new HashSet<>();
		setExcludedAttributeIds.add(SF.SYSTEMIDENTIFIER.getUID(eoOld.getDalEntity()));
		setExcludedAttributeIds.add(SF.CHANGEDAT.getUID(eoOld.getDalEntity()));
		setExcludedAttributeIds.add(SF.CHANGEDBY.getUID(eoOld.getDalEntity()));
		setExcludedAttributeIds.add(SF.CREATEDAT.getUID(eoOld.getDalEntity()));
		setExcludedAttributeIds.add(SF.CREATEDBY.getUID(eoOld.getDalEntity()));
		setExcludedAttributeIds.add(SF.VERSION.getUID(eoOld.getDalEntity()));

		final List<UID> setWrittenFields = new ArrayList<>();
		for (UID field : eoNew.getFieldValues().keySet()) {
			if (setWrittenFields.contains(field))
				continue;
			// track fields.
			final FieldMeta<?> efMeta = metaProvider.getAllEntityFieldsByEntity(eoNew.getDalEntity()).get(field);
			if (efMeta == null)
				continue; // we have problems with field like 'version'

			if (!setExcludedAttributeIds.contains(efMeta.getUID())) {
				if (efMeta.isLogBookTracking()) {
					if (efMeta.getForeignEntity() == null) { // is not an id field.
						if (!LangUtils.equal(eoOld.getFieldValue(field), eoNew.getFieldValue(field))) {
							setWrittenFields.add(field); // remember field has been written.
							createHistoryEntry(
									getSavePoint(),
									getCurrentUserName(),
									eoOld.getPrimaryKey(),
									getSavePointTime(),
									eoOld.getDalEntity(),
									field,
									null,
									eoOld.getFieldValue(field)
							);
						}
					} else {
						final EntityMeta<?> refEntityMeta = metaProvider.getEntity(efMeta.getForeignEntity());
						if (refEntityMeta.isUidEntity()) {
							if (!LangUtils.equal(eoOld.getFieldUid(field), eoNew.getFieldUid(field))) {
								setWrittenFields.add(field); // remember field has been written.
								createHistoryEntry(
										getSavePoint(),
										getCurrentUserName(),
										eoOld.getPrimaryKey(),
										getSavePointTime(),
										eoOld.getDalEntity(),
										field,
										eoOld.getFieldUid(field),
										eoOld.getFieldValue(field)
								);
							}
						} else {
							if (!LangUtils.equal(eoOld.getFieldId(field), eoNew.getFieldId(field))) {
								setWrittenFields.add(field); // remember field has been written.
								createHistoryEntry(
										getSavePoint(),
										getCurrentUserName(),
										eoOld.getPrimaryKey(),
										getSavePointTime(),
										eoOld.getDalEntity(),
										field,
										eoOld.getFieldId(field),
										eoOld.getFieldValue(field)
								);
							}
						}
					}
				}
			}
		}
	}

	public void trackRemoveToLogbook(EntityObjectVO<?> eo) {
		if (!isLogbookTrackingActive(eo)) {
			return;
		}

		createSavepoint();

		final List<UID> setWrittenFields = new ArrayList<>();
		for (UID field : eo.getFieldValues().keySet()) {
			if (setWrittenFields.contains(field))
				continue;
			// track fields.
			FieldMeta<?> efMeta = metaProvider.getAllEntityFieldsByEntity(eo.getDalEntity()).get(field);
			if (efMeta == null) {
				if (field.equals(SF.VERSION.getUID(eo.getDalEntity()))) {
					efMeta = SF.VERSION.getMetaData(eo.getDalEntity());
				} else
					continue; // we have problems with field like 'version' - skip others.
			}
			if (efMeta.isLogBookTracking()) {
				setWrittenFields.add(field); // remember field has been written.
				createHistoryEntry(
						getSavePoint(),
						getCurrentUserName(),
						eo.getPrimaryKey(),
						getSavePointTime(),
						eo.getDalEntity(),
						field,
						eo.getFieldId(field),
						eo.getFieldValue(field)
				);
			}
		}
	}

	private static boolean shouldStoreAsByte(FieldMeta<?> efMeta) {
		if (efMeta == null)
			return false;

		final DbColumnType.DbGenericType type
				= DbUtils.getDbColumnType(efMeta).getGenericType();
		switch (type) {
			case CLOB:
			case NCLOB:
			case BLOB:
				return true;
			default:
				return false;
		}
	}

	public Collection<MasterDataVO<Long>> getHistoryData(UID entityUID, Object iObjectId) throws CommonPermissionException {
		checkReadAllowed(entityUID);
		return getHistoryDataWithoutCheck(entityUID, iObjectId, 0);
	}

	private Collection<MasterDataVO<Long>> getHistoryDataWithoutCheck(
			UID entityUID,
			Object iObjectId,
			int recursion
	) {
		try {
			// get all entries with entity and objectid.

			final CollectableComparison condEntity = SearchConditionUtils.newComparison(E.HISTORY.entity, ComparisonOperator.EQUAL, entityUID);
			final CollectableComparison condEntityObject = SearchConditionUtils.newComparison(
					(iObjectId instanceof UID ? E.HISTORY.entityobjectuid : E.HISTORY.entityobjectid),
					ComparisonOperator.EQUAL,
					iObjectId
			);
			Collection<MasterDataVO<Long>> result = getMasterDataFacade().getMasterData(
					E.HISTORY,
					SearchConditionUtils.and(condEntity, condEntityObject)
			);
			// get all referencing entries			
			final CollectableComparison condRefValue = SearchConditionUtils.newComparison(
					(iObjectId instanceof UID ? E.HISTORY.value_refuid : E.HISTORY.value_refid),
					ComparisonOperator.EQUAL,
					iObjectId
			);
			for (MasterDataVO<Long> mdvo : getMasterDataFacade().getMasterData(E.HISTORY, condRefValue)) {
				final UID uidRefEntityField = mdvo.getFieldUid(E.HISTORY.entityfield);
				final UID uidForeignRefEntity = metaProvider.getEntityField(uidRefEntityField).getEntity();
				if (uidForeignRefEntity.equals(entityUID)) {
					result.add(mdvo);
				}
			}

			// get all possible current foreign entity entries
			Map<EntityAndField, UID> mpEntityAndParentEntityName = Collections.EMPTY_MAP;
			try {
				mpEntityAndParentEntityName = getLayoutFacade().getSubFormEntityAndParentSubFormEntities(
						entityUID,
						iObjectId,
						false,
						ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY)
				);
			} catch (NuclosFatalException e) {
				LOG.warn("error getting dependant fields from layout", e);
			} catch (CommonFatalException e) {
				LOG.warn("error getting dependant fields from layout", e);
			} catch (Exception e) {
				LOG.warn("error getting dependant fields from layout", e);
			}

			// Stop at first recursion level
			if (recursion < 1) {
				for (EntityAndField eaf: mpEntityAndParentEntityName.keySet()) {
					for (EntityObjectVO<?> mdvo: getMasterDataFacade().getDependantMasterData(eaf.getField(), iObjectId)) {
						// get all entries for the foreign entity
						// TODO: Recursive call within double nested loop :(
						// TODO: Do we really need the history data of dependents?
						result.addAll(getHistoryDataWithoutCheck(
								mdvo.getDalEntity(),
								mdvo.getPrimaryKey(),
								recursion + 1
						));
					}
				}
			}

			// get all entries with same savepoint
			final List<String> lstSavepoints = new ArrayList<>();
			for (MasterDataVO<Long> mdvo : new ArrayList<>(result)) {
				final String sSavePoint = mdvo.getFieldValue(E.HISTORY.savepoint.getUID(), String.class);
				if (!lstSavepoints.contains(sSavePoint)) {
					lstSavepoints.add(sSavePoint);
					final CollectableComparison cond = SearchConditionUtils.newComparison(E.HISTORY.savepoint, ComparisonOperator.EQUAL, sSavePoint);
					result.addAll(getMasterDataFacade().getMasterData(E.HISTORY, cond));
				}
			}

			// return distinct result list.
			return CollectionUtils.distinct(
					result,
					(t1, t2) -> LangUtils.equal(t1.getPrimaryKey(), t2.getPrimaryKey())
			);
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * TODO: Useless method.
	 */
	public void trackChangesToLogbookIfPossible(EntityObjectVO<?> eoOld, EntityObjectVO<?> eoNew) {
		trackChangesToLogbook(eoOld, eoNew);
	}

	/**
	 * TODO: Useless method.
	 */
	public void trackRemoveToLogbookIfPossible(EntityObjectVO<?> eo) {
		trackRemoveToLogbook(eo);
	}

	private boolean isLogbookTrackingActive(EntityObjectVO<?> eo) {
		return metaProvider.getEntity(eo.getDalEntity()).isLogBookTracking();
	}
}