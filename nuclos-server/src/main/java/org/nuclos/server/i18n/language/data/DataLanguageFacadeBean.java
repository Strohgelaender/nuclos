package org.nuclos.server.i18n.language.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.nuclos.api.locale.NuclosLocale;
import org.nuclos.api.service.DataLocaleService;
import org.nuclos.common.DbField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.processor.jdbc.impl.EntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional(noRollbackFor = { Exception.class })
public class DataLanguageFacadeBean extends NuclosFacadeBean implements DataLanguageFacadeRemote, DataLocaleService {

	private static final Logger LOG = LoggerFactory.getLogger(DataLanguageFacadeBean.class);
	
	@Autowired
	private NucletDalProvider nucletDalProvider;
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;	
	@Autowired
	private DataLanguageCache dataLangCache;
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	public Collection<DataLanguageVO> getDataLanguages() {
		return dataLangCache.getDataLanguages();
	}
	
	public UID getLanguageToUse() {
		return dataLangCache.getLanguageToUse();
	}
	
	public Collection<DataLanguageVO> getAllDataLanguages() {
		
		List<DataLanguageVO> retVal = new ArrayList<DataLanguageVO>();
		
		for (Locale l : Locale.getAvailableLocales()) {
			if (l.getCountry() != null && l.getCountry().trim().length() > 0) {
				DataLanguageVO dataLanguageVO = new DataLanguageVO(l.getLanguage(), l.getCountry(), false, 0);
				if (!retVal.contains(dataLanguageVO))
					retVal.add(dataLanguageVO);				
			}
		}
		
		return retVal;
	}

	public void setPrimaryDataLanguage(DataLanguageVO dlvo) {
		for (EntityObjectVO<UID> eovo : nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).getAll()) {
			DataLanguageVO currentdlvo = new DataLanguageVO(eovo);
			if (currentdlvo.getLocale().equals(dlvo.getLocale())) {
				currentdlvo.setIsPrimary(Boolean.TRUE);
				nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).insertOrUpdate(currentdlvo);
			} else {
				if (currentdlvo.isPrimary()) {
					currentdlvo.setIsPrimary(Boolean.FALSE);
					nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).insertOrUpdate(currentdlvo);					
				}
			}		
		}
		
		this.dataLangCache.invalidate();
	}

	@Override
	public void removeDataLanguage(DataLanguageVO dlvo) throws 
		NuclosBusinessRuleException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException {
	
		List<EntityObjectVO<UID>> bySearchExpression = nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).getBySearchExpression(
				new CollectableSearchExpression(
						SearchConditionUtils.and(
								SearchConditionUtils.newComparison(E.DATA_LANGUAGE.language, ComparisonOperator.EQUAL, dlvo.getLanguage()), 
								SearchConditionUtils.newComparison(E.DATA_LANGUAGE.country , ComparisonOperator.EQUAL, dlvo.getCountry()))));
		
		UID toDelete = bySearchExpression.get(0).getPrimaryKey();
		masterDataFacade.remove(E.DATA_LANGUAGE.getUID(), toDelete, false);
		this.dataLangCache.invalidate();
	}

	@Override
	public void addDataLanguage(DataLanguageVO dlvo) {
		
		if (dlvo.isPrimary()) {
			List<EntityObjectVO<UID>> bySearchExpression = nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).getBySearchExpression(
					new CollectableSearchExpression(SearchConditionUtils.newComparison(E.DATA_LANGUAGE.primaryLanguage, ComparisonOperator.EQUAL, Boolean.TRUE)));
			
			for (EntityObjectVO<UID> eovo : bySearchExpression) {
				DataLanguageVO toUpdate = new DataLanguageVO(eovo);
				toUpdate.setIsPrimary(Boolean.FALSE);
				nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).insertOrUpdate(toUpdate);
			}
		}
		
		nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).insertOrUpdate(dlvo);		
		this.dataLangCache.invalidate();
	}

	@Override
	public void save(List<DataLanguageVO> selectDataLanguages) throws 
			NuclosBusinessRuleException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, CommonPermissionException, CommonCreateException, CommonValidationException {
		
		if (selectDataLanguages == null || selectDataLanguages.isEmpty()) {
			// No localization anymore
			removeAllDataLanguageTables();
		} 
		else {
			
			UID systemLanguage = null;
			boolean systemLanguageIsNew = false;
			
			// First remove all existing entries from all language tables that are no needed any more
			for (EntityMeta<?> em : metaProvider.getAllLanguageEntities()) {
				List<CollectableComparison> uidcomps = new ArrayList<CollectableComparison>();
				UID dataLangReference = DataLanguageServerUtils.extractDataLanguageReference(
						EntityMetaVO.getEntityUIDByLanguageUID(em.getUID()));
				FieldMeta dataLangReferenceMeta = em.getField(dataLangReference);
				
				IEntityObjectProcessor dlProcessor = nucletDalProvider.getEntityObjectProcessor(em);
				DbQueryBuilder queryBuilder = 
						dataBaseHelper.getDbAccess().getQueryBuilder();
				
				DbDelete createDelete = queryBuilder.createDelete(em);					
				for (DataLanguageVO dlvo : selectDataLanguages) {
					createDelete.addToWhereAsAnd(
							queryBuilder.not(queryBuilder.equalValue(
									createDelete.baseColumn(dataLangReferenceMeta), dlvo.getPrimaryKey().toString())));				
				}				
				dataBaseHelper.getDbAccess().executeDelete(createDelete);
				
			}
						
			// create new data languages and remove old languages
			List<DataLanguageVO> existingAndValid = new ArrayList<DataLanguageVO>();
			
			for (EntityObjectVO<UID> eovo : nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).getAll()) {
				DataLanguageVO dlvo = new DataLanguageVO(eovo);
				if (!selectDataLanguages.contains(dlvo)) {
					masterDataFacade.remove(E.DATA_LANGUAGE.getUID(), dlvo.getPrimaryKey(), false);	
				} else {
					DataLanguageVO existDlvo = selectDataLanguages.get(selectDataLanguages.indexOf(dlvo));
					dlvo.setOrder(existDlvo.getOrder());
					dlvo.setIsPrimary(existDlvo.isPrimary());
					masterDataFacade.modify(new MasterDataVO(dlvo));	
					existingAndValid.add(dlvo);
				}
			}			
			
			for (DataLanguageVO dlvo : selectDataLanguages) {				
				if (dlvo.isPrimary()) {
					systemLanguage = dlvo.getPrimaryKey();				
				}		
				if (!existingAndValid.contains(dlvo)) {
					if (dlvo.isPrimary()) {
						systemLanguageIsNew = true;
					}
				}
			}
			
			int iCountLangs = 0;
			for (DataLanguageVO dlvo : selectDataLanguages) {				
				if (!existingAndValid.contains(dlvo)) {
					iCountLangs++;
				}
			}
			int iLang = 1;
			for (DataLanguageVO dlvo : selectDataLanguages) {				
				if (!existingAndValid.contains(dlvo)) {
					masterDataFacade.create(new MasterDataVO<UID>(dlvo), ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
					// create new default entries (copy of primary language) in languages tables
					fillLocalizedDataIntoLanguageTable(dlvo.getPrimaryKey(),
							systemLanguageIsNew ? getPrimaryDataLanguageUID() : systemLanguage, iLang, iCountLangs);
					iLang++;
				}
			}
			
			boolean systemLangHasChanged = !systemLanguage.equals(getPrimaryDataLanguageUID());
			
			this.dataLangCache.invalidate();			
			
			// If the primary language has been changed we have to ensure that all entity tables
			// offer a value for this primary language
			if (systemLangHasChanged) {
				if (!systemLanguageIsNew) {
					for (EntityMeta em : metaProvider.getAllLanguageEntities()) {
						UID entityUID = NucletEntityMeta.getEntityUIDByLanguageUID(em.getUID());
						fillLocalizedDataIntoEntityTable(
								entityUID, metaProvider.getAllEntityFieldsByEntity(entityUID).values(), 
								em.getUID(), systemLanguage, systemLanguageIsNew);
					}					
				}
			}
			
			LOG.info("DATA_LANGs ready!");
		}
	}
	
	private void fillLocalizedDataIntoLanguageTable(UID language, UID systemLanguage, int iLang, int iCountLangs) throws CommonFinderException {
		
		EntityObjectProcessor dlProcessor = null;
		
		LOG.info("DATA_LANG {} ({}/{}) fill tables with default values ...",
		         language.getString(), iLang, iCountLangs);
		
		int iCountTables = 0;
		for (EntityMeta dataLanguageMeta : metaProvider.getAllLanguageEntities()) {
			if (metaProvider.hasEntity(dataLanguageMeta.getUID())) {
				iCountTables++;
			}
		}
		
		int iTable = 1;
		for (EntityMeta dataLanguageMeta : metaProvider.getAllLanguageEntities()) {
			
			EntityMeta entityMeta = metaProvider.getEntity(
					NucletEntityMeta.getEntityUIDByLanguageUID(dataLanguageMeta.getUID()));
			
			if (metaProvider.hasEntity(dataLanguageMeta.getUID())) {
				
				dlProcessor = (EntityObjectProcessor)
					nucletDalProvider.getEntityObjectProcessor(dataLanguageMeta.getUID());
				
				LOG.info("DATA_LANG {} ({}/{}) filling {} ({}/{})",
				         language.getString(), iLang, iCountLangs,
				         dlProcessor.getMetaData().getDbTable(),
				         iTable, iCountTables);
				dlProcessor.insertWithSelect(
					createFieldsValueMap(new NucletEntityMeta(entityMeta, true),
					                     dataLanguageMeta, language.equals(systemLanguage),
					                     language), null, entityMeta);
				iTable++;
			} else {
				throw new CommonFinderException("Cannot find data language table for entity '" + entityMeta.getEntityName() + "' though there are localized fields.");
			}
		}					
	}
	
	private void removeAllDataLanguageTables() throws 
		NuclosBusinessRuleException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, 
		CommonPermissionException {
		
		// This is a complete cleanup, because without data languages there is no localization at all
		
		// remove data language references in entities (for report, etc.)
		IEntityObjectProcessor<UID> eoProcessor = nucletDalProvider.getEntityObjectProcessor(E.ENTITY);
		for (EntityObjectVO<UID> eovo : eoProcessor.getBySearchExpression(
				new CollectableSearchExpression(SearchConditionUtils.newIsNotNullComparison(E.ENTITY.dataLangRefPath)))) {
			eovo.setFieldValue(E.ENTITY.dataLangRefPath, null);
			eovo.flagUpdate();
			eoProcessor.insertOrUpdate(eovo);
		}
		
		// remove localized flags on entity fields
		eoProcessor = nucletDalProvider.getEntityObjectProcessor(E.ENTITYFIELD);
		for (EntityObjectVO<UID> eovo : eoProcessor.getBySearchExpression(
				new CollectableSearchExpression(
						SearchConditionUtils.newComparison(E.ENTITYFIELD.isLocalized, ComparisonOperator.EQUAL, Boolean.TRUE)))) {
			eovo.setFieldValue(E.ENTITYFIELD.isLocalized, Boolean.FALSE);
			eovo.flagUpdate();
			eoProcessor.insertOrUpdate(eovo);
		}	
		
		// remove language tables
		for (EntityMeta<?> em : metaProvider.getAllLanguageEntities()) {
			nucletDalProvider.getDataLanguageMetaDataProcessor().remove(em); 
		}		
		
		// remove entries from data language tables
		for (EntityObjectVO<UID> eovo : nucletDalProvider.getEntityObjectProcessor(E.DATA_LANGUAGE).getAll()) {
			masterDataFacade.remove(E.DATA_LANGUAGE.getUID(), eovo.getPrimaryKey(), false);				
		}
		
		metaProvider.revalidate(true, true);
		this.dataLangCache.invalidate();
	}
	
	@Override
	public UID getCurrentUserDataLanguageUID() {
		return (UID) this.dataLangCache.getLanguageToUse();
	}
	
	@Override
	public UID getPrimaryDataLanguageUID() {
		return (UID) this.dataLangCache.getNuclosPrimaryDataLanguage();
	}
	
	public void fillLocalizedDataIntoEntityTable(UID entityObjectUID, Collection<? extends FieldMeta> entityObjectMetaFields, 
			UID dataLanguageUID, UID systemLanguage, boolean systemLanguageIsNew) {
	
		final UID fieldMetaEntityReference = DataLanguageServerUtils.extractForeignEntityReference(entityObjectUID);
		final UID fieldMetaDataLangReference = DataLanguageServerUtils.extractDataLanguageReference(entityObjectUID);
		
		// maintained data of new, but existing system language must be copied
		// into the main data tables
		IEntityObjectProcessor<Object> eoProcessor = 
				nucletDalProvider.getEntityObjectProcessor(entityObjectUID);
		IEntityObjectProcessor dlProcessor = 
				nucletDalProvider.getEntityObjectProcessor(dataLanguageUID);
		
		for (EntityObjectVO eovo : eoProcessor.getAll()) {			
			
			Map<UID, EntityObjectVO> foundDLEntries = CollectionUtils.transformIntoMap(
					dlProcessor.getBySearchExpression(new CollectableSearchExpression(
					SearchConditionUtils.newIdComparison(
							fieldMetaEntityReference, ComparisonOperator.EQUAL, (Long) eovo.getPrimaryKey()))),
					new Transformer<Object, UID>() {						
				@Override
				public UID transform(Object i) {
					return ((EntityObjectVO) i).getFieldUid(fieldMetaDataLangReference);
				}
			});
			
			for (FieldMeta fieldMeta : entityObjectMetaFields) {
				if (fieldMeta.isLocalized()) {
					
					UID dlValueField = DataLanguageServerUtils.extractFieldUID(fieldMeta.getUID());
					
					String newSystemLangValue = 
							(String) foundDLEntries.get(systemLanguage).getFieldValue(dlValueField);
					
					eovo.setFieldValue(fieldMeta.getUID(), newSystemLangValue);
					eovo.flagUpdate();
				}
			}					
			eoProcessor.insertOrUpdate(eovo);
		}			
	}

	public void fillLocalizedDataIntoLanguageTable(boolean isNewDL, 
			NucletEntityMeta entityObjectMeta, List<FieldMeta<?>> fields, EntityMeta dataLanguageMeta) {
		
		EntityObjectProcessor<Long> dlProcessor = 
				(EntityObjectProcessor<Long>) nucletDalProvider.getEntityObjectProcessor(dataLanguageMeta);
		
		entityObjectMeta.setFields(fields);
		
		if (isNewDL) {
			// Data Language table is new, there is no data stored yet; copy from entity table for all
			// languages			
			for (DataLanguageVO dlvo : this.dataLangCache.getDataLanguages()) {
				LOG.debug("Copying values into data table {} for language {}",
				          dataLanguageMeta.getDbTable(), dlvo);
				dlProcessor.insertWithSelect(
						createFieldsValueMap(entityObjectMeta, dataLanguageMeta, dlvo.isPrimary(), dlvo.getPrimaryKey()), null, entityObjectMeta);
			}
		} else {
			for (DataLanguageVO dlvo : this.dataLangCache.getDataLanguages()) {
				LOG.debug("Copying values into data table {} for language {}",
				          dataLanguageMeta.getDbTable(), dlvo);
				DbMap fieldsToUpdate = 
						createFieldMap(fields, dataLanguageMeta, dlvo.isPrimary());
				if (!fieldsToUpdate.isEmpty()) {
					StringBuilder builder = new StringBuilder();
					builder.append("UPDATE ").append(dataLanguageMeta.getDbTable()).append(" d1 SET ");
					
					Object obj;
					for (DbField field : fieldsToUpdate.getFields()) {
						obj = fieldsToUpdate.get(field);
						if (obj instanceof NucletFieldMeta) {
							NucletFieldMeta fieldMeta = (NucletFieldMeta) obj;
							FieldMeta dlFieldMeta = dataLanguageMeta.getField(
									DataLanguageServerUtils.extractForeignEntityReference(entityObjectMeta.getPrimaryKey()));
							builder.append(field.getDbColumn()).append(" = (SELECT ");
							builder.append(entityObjectMeta.getDbTable()).append(".");
							builder.append(fieldMeta.getDbColumn()).append(" FROM ");
							builder.append(entityObjectMeta.getDbTable()).append(" WHERE d1.");
							builder.append(dlFieldMeta.getDbColumn()).append(" = ");
							builder.append(entityObjectMeta.getDbTable()).append(".");
							builder.append(SF.PK_ID.getMetaData(entityObjectMeta).getDbColumn());
							builder.append("),");
						}
						else {
							NucletFieldMeta fieldMeta = (NucletFieldMeta) field;
							Object value = dataBaseHelper.getDbAccess().getBooleanRepresentationInSQL((Boolean)obj);
							builder.append(fieldMeta.getDbColumn()).append("=").append(value).append(",");
						}
					}
					// letzes , abschneiden
					builder.deleteCharAt(builder.length() - 1);
					
					builder.append(" WHERE d1.").append(dataLanguageMeta.getField(
							DataLanguageServerUtils.extractDataLanguageReference(entityObjectMeta.getPrimaryKey())).getDbColumn());
					builder.append(" = '").append(dlvo.getPrimaryKey()).append("'");
					String sql = builder.toString();
					
					dataBaseHelper.getDbAccess().executePlainUpdate(sql);
				}
			}
		}
	}
	
	public void fillLocalizedDataIntoLanguageTable(boolean isNewDL, 
			NucletEntityMeta entityObjectMeta, EntityMeta dataLanguageMeta) {
		
		fillLocalizedDataIntoLanguageTable(isNewDL, entityObjectMeta, new ArrayList(entityObjectMeta.getFields()), dataLanguageMeta);
	}

	private DbMap createFieldMap(Collection<FieldMeta<?>> fields, EntityMeta dataLanguageMeta, boolean isPrimaryLang) {
		
		DbMap retVal = new DbMap();

		for (FieldMeta meta : fields) {
			if (meta.isLocalized() && meta instanceof NucletFieldMeta) {
				NucletFieldMeta nfm = (NucletFieldMeta) meta;
				if (nfm.isFlagUpdated() || nfm.isFlagNew()) {
					UID localizedFieldUID = DataLanguageServerUtils.extractFieldUID(nfm.getUID());
					UID localizedFieldModifiedUID = DataLanguageServerUtils.extractFieldUID(nfm.getUID(), true);
					FieldMeta localizedFieldMeta = dataLanguageMeta.getField(localizedFieldUID);
					FieldMeta localizedFieldModifiedMeta = dataLanguageMeta.getField(localizedFieldModifiedUID);					
					retVal.putField(localizedFieldMeta, nfm);
					retVal.put(localizedFieldModifiedMeta, isPrimaryLang ? Boolean.FALSE : Boolean.TRUE);
				}
			}
		}
		
		return retVal;
	}
	
	private DbMap createFieldConditionMap(NucletEntityMeta entityObjectMeta, EntityMeta dataLanguageMeta, UID language) {
		DbMap retVal = new DbMap();
		
		retVal.putField(dataLanguageMeta.getField(
					DataLanguageServerUtils.extractForeignEntityReference(entityObjectMeta.getPrimaryKey())),
				SF.PK_ID.getMetaData(entityObjectMeta));
		retVal.put(dataLanguageMeta.getField(
					DataLanguageServerUtils.extractDataLanguageReference(entityObjectMeta.getPrimaryKey())),
				language.toString());
		
		return retVal;
	}
	
	private DbMap createFieldsValueMap(
			NucletEntityMeta entityObjectMeta, EntityMeta dataLanguageMeta, boolean isSystemLanguage, UID language) {
		
		DbMap retVal = new DbMap();
		
		// add nuclos standard fields
		retVal.putUnsafe(SF.PK_ID.getMetaData(dataLanguageMeta), new DbId());
		retVal.put(SF.CHANGEDAT.getMetaData(dataLanguageMeta), InternalTimestamp.toInternalTimestamp(new Date()));
		retVal.put(SF.CHANGEDBY.getMetaData(dataLanguageMeta), "nuclos");
		retVal.put(SF.CREATEDAT.getMetaData(dataLanguageMeta), InternalTimestamp.toInternalTimestamp(new Date()));
		retVal.put(SF.CREATEDBY.getMetaData(dataLanguageMeta), "nuclos");
		retVal.put(SF.VERSION.getMetaData(dataLanguageMeta), 1);
		
		// add localized fields
		for (FieldMeta field : entityObjectMeta.getFields()) {
			if (field.isLocalized() && !field.isCalculated()) {
				UID localizedFieldUID = DataLanguageServerUtils.extractFieldUID(field.getUID());
				UID localizedFieldModifiedUID = DataLanguageServerUtils.extractFieldUID(field.getUID(), true);
				FieldMeta localizedFieldMeta = dataLanguageMeta.getField(localizedFieldUID);
				FieldMeta localizedFieldModifiedMeta = dataLanguageMeta.getField(localizedFieldModifiedUID);
				retVal.putField(localizedFieldMeta,field);
				retVal.put(localizedFieldModifiedMeta, isSystemLanguage ? Boolean.FALSE : Boolean.TRUE);				
			}
		}
		
		// add references for language and main entity entry
		UID entityRefField = DataLanguageServerUtils.extractForeignEntityReference(entityObjectMeta.getUID());
		UID entityRefDataLang = DataLanguageServerUtils.extractDataLanguageReference(entityObjectMeta.getUID());
		FieldMeta localizedRefFieldMeta = dataLanguageMeta.getField(entityRefField);
		FieldMeta localizedRefDataFieldMeta = dataLanguageMeta.getField(entityRefDataLang);
		retVal.putField(localizedRefFieldMeta, SF.PK_ID.getMetaData(entityObjectMeta));
		retVal.put(localizedRefDataFieldMeta,language.toString());	
		
	return retVal;
}

	@Override
	public List<NuclosLocale> getDataLocales() {
		
		List<NuclosLocale> transformIntoSet = CollectionUtils.transform(this.dataLangCache.getDataLanguages(), new Transformer<DataLanguageVO, NuclosLocale>() {
			@Override
			public NuclosLocale transform(DataLanguageVO i) {
				return i.getNuclosLocale();
			}
		});
		
		return transformIntoSet;
	}
}
