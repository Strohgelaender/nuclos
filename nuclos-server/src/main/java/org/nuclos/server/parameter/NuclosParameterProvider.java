package org.nuclos.server.parameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.parameter.NucletParameter;
import org.nuclos.api.parameter.SystemParameter;
import org.nuclos.api.service.ParameterService;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

@Component("parameterServiceProvider")
public class NuclosParameterProvider implements ParameterService {
	
	public static final String WEBCLIENT_RESTRICTIONS_MUST_IGNORE_GROOVY_RULES = "WEBCLIENT_RESTRICTIONS_MUST_IGNORE_GROOVY_RULES";
	public static final String NUCLOS_SERVERCODE_CLASSPATH_EXTENSION = "NUCLOS_SERVERCODE_CLASSPATH_EXTENSION";

	@Autowired
	private ServerParameterProvider parameterProvider;

	@Override
	public String getSystemParameter(SystemParameter parameter) throws BusinessException {
	
		if (parameter == null) {
			throw new BusinessException("parameter must not be null");
		}

		try {
			return parameterProvider.getValueForPK(new UID(parameter.getId()));
		} catch (CommonFinderException cfe) {
			throw new BusinessException("Parameter id=" + parameter.getId() + " not found");
		}
	}

	@Override
	public String getNucletParameter(NucletParameter parameter)
			throws BusinessException {
		if (NucletDalProvider.getInstance().isRunningJobSet()) {
			return getNucletParameterByJob(parameter, NucletDalProvider.getInstance().getRunningJob());
		} else if (SecurityCache.getInstance().isMandatorPresent()) {
			Set<UID> accessibleMandators = NucletDalProvider.getInstance().getAccessibleMandators();
			return getNucletParameterByMandators(parameter, accessibleMandators);
		} else {
			return getNucletParameterDefault(parameter);
		}
	}
	
	@Cacheable(value="nucletParameterDefault", key="#p0.getId()")
	private String getNucletParameterDefault(NucletParameter parameter) throws BusinessException{
		String retVal = null;
		
		if (parameter == null)
			throw new BusinessException("parameter must not be null");
		
		EntityObjectVO<UID> eovo = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLETPARAMETER).
				getByPrimaryKey(new UID(parameter.getId()));
		
		if (eovo != null) {
			retVal = eovo.getFieldValue(E.NUCLETPARAMETER.value);
		}
		
		return retVal;
	}
	
	public String getNucletParameterByMandators(NucletParameter parameter, Set<UID> accessibleMandators) throws BusinessException{
		String retVal = null;
		
		if (parameter == null)
			throw new BusinessException("parameter must not be null");
		
		retVal = getNucletParameterDefault(parameter);
		if (retVal != null) {
			if (SecurityCache.getInstance().isMandatorPresent()) {
				if (accessibleMandators != null) {
					String mandatorVal = getNucletParameterByMandatorsCached(parameter, accessibleMandators);
					if (mandatorVal != null) {
						retVal = mandatorVal;
					}
				}
			}
		}
			
		return retVal;
	}
	
	@Cacheable(value="nucletParameterByAccessibleMandators", key="#p0.getId() + #p1.toString()")
	private String getNucletParameterByMandatorsCached(NucletParameter parameter, Set<UID> accessibleMandators) throws BusinessException{
		String retVal = null;
			
		// Suche nach mandantenspezifischen Werten?
		CollectableSearchCondition cond = SearchConditionUtils.newKeyComparison(E.MANDATOR_PARAMETERVALUE.nucletParameter, ComparisonOperator.EQUAL, new UID(parameter.getId()));
		List<EntityObjectVO<UID>> nucletParamMandatorValues = NucletDalProvider.getInstance().getEntityObjectProcessor(E.MANDATOR_PARAMETERVALUE).getBySearchExpression(new CollectableSearchExpression(cond));
		
		retVal = MandatorUtils.getByAccessibleMandators(nucletParamMandatorValues, E.MANDATOR_PARAMETERVALUE.value, E.MANDATOR_PARAMETERVALUE.mandator, accessibleMandators);
		
		return retVal;
	}
	
	public String getNucletParameterByJob(NucletParameter parameter, UID jobController) throws BusinessException{
		String retVal = null;
		
		if (parameter == null)
			throw new BusinessException("parameter must not be null");
		
		retVal = getNucletParameterDefault(parameter);
		if (retVal != null) {
			if (SecurityCache.getInstance().isMandatorPresent()) {
				Set<UID> accessibleMandators = NucletDalProvider.getInstance().getAccessibleMandators();
				String mandatorVal = getNucletParameterByMandators(parameter, accessibleMandators);
				if (mandatorVal != null) {
					retVal = mandatorVal;
				}
			}
			if (NucletDalProvider.getInstance().isRunningJobSet()) {
				if (jobController != null) {
					String jobVal = getNucletParameterByJobCached(parameter, jobController);
					if (jobVal != null) {
						retVal = jobVal;
					}
				}
			}
		}
			
		return retVal;
	}
	
	@Cacheable(value="nucletParameterByJob", key="#p0.getId() + #p1.toString()")
	private String getNucletParameterByJobCached(NucletParameter parameter, UID jobController) throws BusinessException{
		String retVal = null;
		
		// Suche nach jobspezifischem Wert?
		CollectableSearchCondition condParam = SearchConditionUtils.newKeyComparison(E.JOBCONTROLLER_PARAMETERVALUE.nucletParameter, ComparisonOperator.EQUAL, new UID(parameter.getId()));
		CollectableSearchCondition condJobcontroller = SearchConditionUtils.newKeyComparison(E.JOBCONTROLLER_PARAMETERVALUE.jobcontroller, ComparisonOperator.EQUAL, jobController);
		
		List<EntityObjectVO<UID>> nucletParamJobValues = NucletDalProvider.getInstance().getEntityObjectProcessor(E.JOBCONTROLLER_PARAMETERVALUE).getBySearchExpression(
				new CollectableSearchExpression(new CompositeCollectableSearchCondition(LogicalOperator.AND, condParam, condJobcontroller)));
		if (nucletParamJobValues.size() == 1) {
			retVal = nucletParamJobValues.get(0).getFieldValue(E.JOBCONTROLLER_PARAMETERVALUE.value);
		}
		
		return retVal;
	}
	
	public String getNucletParameterByName(UID nucletUID, String parameter){
		if (nucletUID == null || parameter == null) {
			return null;
		}
		return _getNucletParameterByName(nucletUID, parameter);
	}
	
	@Cacheable(value="nucletParameterByName", key="#p0.getString() + #p1")
	public String _getNucletParameterByName(UID nucletUID, String parameter){
		String retVal = null;
		
		CollectableComparison cond1 = SearchConditionUtils.newUidComparison(E.NUCLETPARAMETER.nuclet, ComparisonOperator.EQUAL, nucletUID);
		CollectableComparison cond2 = SearchConditionUtils.newComparison(E.NUCLETPARAMETER.name, ComparisonOperator.EQUAL, parameter);
		
		List<EntityObjectVO<UID>> eovos = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLETPARAMETER).
				getBySearchExpression(new CollectableSearchExpression(SearchConditionUtils.and(cond1, cond2)));
		
		if (eovos != null && eovos.size() == 1) {
			retVal = eovos.get(0).getFieldValue(E.NUCLETPARAMETER.value);
		}
		
		return retVal;
	}
	
	public Collection<NucletParameter> getNucletParametersByNuclet(UID nucletUID){
		if (nucletUID == null) {
			return new ArrayList<NucletParameter>();
		}
		return getNucletParametersByNucletCached(nucletUID);
	}
	
	@Cacheable(value="nucletParameterByNuclet", key="#p0.getString()") 
	private Collection<NucletParameter> getNucletParametersByNucletCached(UID nucletUID){
		Collection<NucletParameter> retVal = new ArrayList<NucletParameter>();
		
		CollectableComparison cond1 = SearchConditionUtils.newUidComparison(E.NUCLETPARAMETER.nuclet, ComparisonOperator.EQUAL, nucletUID);
		
		List<EntityObjectVO<UID>> eovos = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLETPARAMETER).
				getBySearchExpression(new CollectableSearchExpression(cond1));
		
		for (EntityObjectVO<UID> eo : eovos) {
			retVal.add(new NucletParameter(eo.getPrimaryKey().getString()));
		}
		
		return retVal;
	}
	
	@Cacheable(value="nucletParameterNames") 
	public Map<UID, String> getNucletParameterNames(){
		Map<UID, String> retVal = new HashMap<UID, String>();
		
		List<EntityObjectVO<UID>> eovos = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLETPARAMETER).getAll();
		
		for (EntityObjectVO<UID> eo : eovos) {
			retVal.put(eo.getPrimaryKey(), eo.getFieldValue(E.NUCLETPARAMETER.name));
		}
		
		return retVal;
	}
	
	@Caching(evict= {
			@CacheEvict(value="nucletParameterDefault", allEntries=true),
			@CacheEvict(value="nucletParameterByAccessibleMandators", allEntries=true),
			@CacheEvict(value="nucletParameterByJob", allEntries=true),
			@CacheEvict(value="nucletParameterByName", allEntries=true),
			@CacheEvict(value="nucletParameterByNuclet", allEntries=true),
			@CacheEvict(value="nucletParameterNames", allEntries=true)
	})
	public void evictNucletParameterCaches() {}

}
