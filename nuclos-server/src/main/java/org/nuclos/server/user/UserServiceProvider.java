package org.nuclos.server.user;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Date;

import org.nuclos.api.common.NuclosMandator;
import org.nuclos.api.common.NuclosRole;
import org.nuclos.api.common.NuclosUser;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.UserService;
import org.nuclos.common.E;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.security.UserVO;
import org.nuclos.common.valueobject.MandatorUserVO;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.mandator.MandatorFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.security.UserFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("userServiceProvider")
public class UserServiceProvider implements UserService {

	@Autowired
	private MasterDataFacadeLocal mdFacade;
	@Autowired
	private UserFacadeLocal userFacade;
	@Autowired
	private MandatorFacadeLocal mandatorFacade;
	
	@Override
	public org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, Boolean passwordChangeRequired) throws BusinessException {
		return insert(username, firstname, lastname, email, createUserPassword(), passwordChangeRequired);
	}
	
	@Override
	public org.nuclos.api.UID insert(String username, String firstname, String lastname, String email, String password, Boolean passwordChangeRequired) throws BusinessException {
		
		if (username == null)
			throw new BusinessException("username must not be null");
		if (firstname == null)
			throw new BusinessException("firstname must not be null");
		if (lastname == null)
			throw new BusinessException("lastname must not be null");
		if (email == null)
			throw new BusinessException("email must not be null");
		if (passwordChangeRequired == null)
			throw new BusinessException("passwordChangeRequired must not be null");
		
		org.nuclos.api.UID retVal = null;
		
		try {
			EntityObjectVO<UID> eoUser = new EntityObjectVO<UID>(E.USER);
			UserVO userVO = new UserVO(new MasterDataVO<UID>(eoUser));
			
			userVO.setLocked(Boolean.FALSE);
			userVO.setEmail(email);
			userVO.setLastname(lastname);
			userVO.setFirstname(firstname);
			userVO.setName(username);
			userVO.setRequirePasswordChange(passwordChangeRequired);
			userVO.setSetPassword(Boolean.TRUE);
			userVO.setNewPassword(password);
			userVO.setNotifyUser(Boolean.TRUE);
			userVO.setSuperuser(Boolean.FALSE);
			userVO.setPrivacyConsent(Boolean.FALSE);
			
			UserVO create = userFacade.create(userVO, eoUser.getDependents());	
			retVal = create.getPrimaryKey();
			
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		
		return retVal;
	}
	
	private static String createUserPassword() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32).replaceAll("", "").substring(1, 8);
	}
	
	
	@Override
	public void revokeRole(Class<? extends NuclosRole> role, NuclosUser user) 
			throws BusinessException {
		
		if (user == null)
			throw new BusinessException("user must not be null");
		if (role == null)
			throw new BusinessException("userrole must not be null");
		
		try {

			Collection<MasterDataVO<UID>> results = this.mdFacade.getMasterData(E.ROLEUSER,
					SearchConditionUtils.and(SearchConditionUtils.newComparison(E.ROLEUSER.role, ComparisonOperator.EQUAL, (UID) role.newInstance().getId()),
											 SearchConditionUtils.newComparison(E.ROLEUSER.user, ComparisonOperator.EQUAL, (UID) user.getId())));
			if (results.size() == 0)
				throw new BusinessException("User does not have this role");
			
			if (results.size() > 1)
				throw new BusinessException("Incorrect number of elements found for this combination");
			
			
			MasterDataVO<UID> mdvo = results.iterator().next();
			this.mdFacade.remove(mdvo.getEntityObject().getDalEntity(), mdvo.getPrimaryKey(), false);		
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		
	}
	
	@Override
	public org.nuclos.api.UID grantRole(Class<? extends NuclosRole> role, NuclosUser user)
			throws BusinessException  {
		
		if (user == null)
			throw new BusinessException("user must not be null");
		if (role == null)
			throw new BusinessException("userrole must not be null");
		
		MasterDataVO<UID> create = null;
		
		try {
			UID roleId = (UID) role.newInstance().getId();
			UID userId = (UID) user.getId();
			
			EntityObjectVO<UID> newRoleUser = new EntityObjectVO<UID>(E.ROLEUSER);
			newRoleUser.setFieldUid(E.ROLEUSER.user, userId);
			newRoleUser.setFieldUid(E.ROLEUSER.role, roleId);
			
			
			try {
				create = mdFacade.create(new MasterDataVO<UID> (newRoleUser), null);
			} catch (Exception e) {
				throw new BusinessException(e);
			}
			
			if (create == null)
				throw new BusinessException("user cannot be assigned to this role");
			
			
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		
		return create.getPrimaryKey();
	}

	@Override
	public void expire(NuclosUser user, Date date) throws BusinessException {
		try {
			UserVO userVO = userFacade.getByUID((UID) user.getId());
			userVO.setExpirationDate(date);
			userFacade.modify(userVO, null, null);
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public org.nuclos.api.UID grantMandator(NuclosMandator mandator,NuclosUser user) throws BusinessException {
		if (mandator == null || user == null)
			throw new BusinessException("Mandant und User müssen übergeben werden");
		MandatorVO mandatorVO = null;
		UserVO userVO = null;
		try {
			mandatorVO = mandatorFacade.getByUID((UID) mandator.getId());
			userVO = userFacade.getByUID((UID) user.getId());
			return userFacade.grantMandator(new MandatorUserVO(mandatorVO, userVO));
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public boolean isMandatorGranted(NuclosMandator mandator,NuclosUser user) throws BusinessException {
		MandatorVO mandatorVO = null;
		UserVO userVO = null;
		try {
			mandatorVO = mandatorFacade.getByUID((UID) mandator.getId());
			userVO = userFacade.getByUID((UID) user.getId());
			return userFacade.isMandatorGranted(userVO, mandatorVO);
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public void revokeMandator(NuclosMandator mandator, NuclosUser user) throws BusinessException {
		MandatorVO mandatorVO = null;
		UserVO userVO = null;
		try {
			mandatorVO = mandatorFacade.getByUID((UID) mandator.getId());
			userVO = userFacade.getByUID((UID) user.getId());
			userFacade.revokeMandator(userVO,mandatorVO	);
		} catch (CommonBusinessException e) {
			throw new BusinessException(e);
		}
	}

}
