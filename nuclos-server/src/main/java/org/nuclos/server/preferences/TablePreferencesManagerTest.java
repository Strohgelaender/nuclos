//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.preferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.preferences.ColumnPreferences;
import org.nuclos.common.preferences.PreferencesProvider;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.server.test.IntegrationTest;

public class TablePreferencesManagerTest implements IntegrationTest {

	@Override
	public Map<String, Object> run(final String sTestcase, final Map<String, Object> mapParams) throws Exception {
		if ("ColumnOrder".equals(sTestcase)) {
			final TablePreferencesManager prefsMngr = PreferencesProvider.getInstance().getTablePreferencesManagerForEntity(new UID("sYFUgWuHvxtv8MDrpgV2"), "nuclos", null);
			final TablePreferences prefs = prefsMngr.createDefault();
			try {
				prefs.setName(TablePreferencesManagerTest.class.getCanonicalName());
				final List<ColumnPreferences> columnPreferences = new ArrayList<>(prefs.getSelectedColumnPreferences());
				if (columnPreferences.size() < 6) {
					throw new Exception("Missing column preferences");
				}
				if (!columnPreferences.get(0).getColumn().getString().equals("yRrNxQuuEubF3geNfE0A")) {
					throw new Exception("Column 1 is not: customerNumber");
				}
				if (!columnPreferences.get(1).getColumn().getString().equals("Tsq9sMXXXuAAVFnKLKNr")) {
					throw new Exception("Column 2 is not: name");
				}
				if (!columnPreferences.get(2).getColumn().getString().equals("PI0aFHtZRMXLtFNVPFT0")) {
					throw new Exception("Column 3 is not: birthday");
				}
				if (!columnPreferences.get(3).getColumn().getString().equals("pGwFv9qaL7PCAwdMIWti")) {
					throw new Exception("Column 4 is not: active");
				}
				if (!columnPreferences.get(4).getColumn().getString().equals("imCuENAlyLJTKzq5Tkgs")) {
					throw new Exception("Column 5 is not: order");
				}
				if (!columnPreferences.get(5).getColumn().getString().equals("fc30VdYmTR9xbnMFA9Hq")) {
					throw new Exception("Column 6 is not: turnover");
				}
				final ColumnPreferences colTurnover = columnPreferences.get(5);
				prefs.removeSelectedColumnPreferences(colTurnover);
				prefs.addSelectedColumnPreferencesInFront(colTurnover);
				prefsMngr.insert(prefs);

				final TablePreferencesManager prefsMngr2 = PreferencesProvider.getInstance().getTablePreferencesManagerForEntity(new UID("sYFUgWuHvxtv8MDrpgV2"), "nuclos", null);
				TablePreferences prefs2 = null;
				for (TablePreferences tp : prefsMngr2.getPrivate()) {
					if (prefs.getUID().equals(tp.getUID())) {
						prefs2 = tp;
						break;
					}
				}
				if (prefs2 == null) {
					throw new Exception("Prefs not stored correctly!");
				}
				if (!prefs2.getSelectedColumnPreferences().get(0).getColumn().getString().equals("fc30VdYmTR9xbnMFA9Hq")) {
					throw new Exception("Column ordering not stored!");
				}
			} finally {
				prefsMngr.delete(prefs);
			}
		}
		return mapParams;
	}
}
