package org.nuclos.server.businesstest.sampledata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Generates new sample text data by analyzing the chars that occur per position in the sample values.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SampleTextDataGenerator extends SampleDataGenerator<String> {
	/**
	 * Holds a set with allowed chars for each position a new sample to be generated.
	 */
	private final List<LinkedHashMap<Character, Integer>> allowedChars = new ArrayList<>();

	private final Random random = new Random();

	SampleTextDataGenerator() {
		super(String.class);
	}

	@Override
	public void addSampleValues(final Collection<String> values) {
		for (String value : values) {
			addAllowedChars(value);
		}
	}

	private void addAllowedChars(final String value) {
		if (value == null) {
			return;
		}

		for (int i = 0; i < value.length(); i++) {
			final Character chr = value.charAt(i);
			incrementCharCount(i, chr);
		}
	}

	private void incrementCharCount(final int i, final Character chr) {
		while (allowedChars.size() <= i) {
			allowedChars.add(new LinkedHashMap<>());
		}
		Map<Character, Integer> counts = allowedChars.get(i);
		if (!counts.containsKey(chr)) {
			counts.put(chr, 0);
		}
		counts.put(chr, counts.get(chr) + 1);
	}

	@Override
	public String newValue() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < allowedChars.size(); i++) {
			sb.append(getRandomAllowedChar(i));
		}
		return sb.toString();
	}

	private Character getRandomAllowedChar(int index) {
		final LinkedHashMap<Character, Integer> charsForIndex = allowedChars.get(index);
		final int i = random.nextInt(charsForIndex.size());
		return getEntry(charsForIndex, i);
	}

	private Character getEntry(LinkedHashMap chars, int index) {
		Iterator<Character> iterator = chars.keySet().iterator();
		int n = 0;
		while (iterator.hasNext()) {
			char entry = iterator.next();
			if (n == index) {
				return entry;
			}
			n++;
		}
		return null;
	}
}
