package org.nuclos.server.businesstest.codegeneration;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.NucletVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Service
public class BusinessTestNucletCacheBean implements IBusinessTestNucletCache {

	@Qualifier("masterDataService")
	private final MasterDataFacadeBean masterDataFacade;

	/**
	 * Maps Nuclet UIDs to the corresponding NucletVO.
	 */
	private final Map<UID, NucletVO> nucletCache = new HashMap<>();

	/**
	 * Maps Nuclet packages to the corresponding NucletVO.
	 */
	private final Map<String, NucletVO> nucletPackageCache = new HashMap<>();

	public BusinessTestNucletCacheBean(
			@Qualifier("masterDataService") final MasterDataFacadeBean masterDataFacade
	) {
		this.masterDataFacade = masterDataFacade;
	}

	public static IBusinessTestNucletCache getInstance() {
		return SpringApplicationContextHolder.getBean(BusinessTestNucletCacheBean.class);
	}

	@Override
	public synchronized NucletVO getNuclet(UID nucletUID) {
		if (!nucletCache.containsKey(nucletUID)) {
			MasterDataVO<UID> mdvo;
			try {
				mdvo = masterDataFacade.get(E.NUCLET.getUID(), nucletUID);
				NucletVO nucletVO = new NucletVO(mdvo);
				cacheNuclet(nucletVO);
			} catch (CommonFinderException | CommonPermissionException e) {
				e.printStackTrace();
			}
		}

		return nucletCache.get(nucletUID);
	}

	@Override
	public synchronized NucletVO getNucletByPackage(final String pkg) {
		if (!nucletPackageCache.containsKey(pkg)) {
			final CollectableComparison search = SearchConditionUtils.newComparison(
					E.NUCLET.packagefield,
					ComparisonOperator.EQUAL,
					pkg
			);
			final Collection<MasterDataVO<UID>> data = masterDataFacade.getMasterData(E.NUCLET, search);
			if (!data.isEmpty()) {
				NucletVO nucletVO = new NucletVO(data.iterator().next());
				cacheNuclet(nucletVO);
			}
		}

		return nucletPackageCache.get(pkg);
	}

	private synchronized void cacheNuclet(NucletVO nucletVO) {
		nucletCache.put(nucletVO.getUid(), nucletVO);
		nucletPackageCache.put(nucletVO.getPackage(), nucletVO);
	}
}
