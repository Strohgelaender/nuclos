package org.nuclos.server.businesstest.codegeneration;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.provider.BusinessObjectProvider;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletLanguageEntityMeta;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.businesstest.codegeneration.source.AbstractGroovyClassSource;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestEntitySource;
import org.nuclos.server.businesstest.execution.BusinessTestEO;
import org.nuclos.server.businesstest.execution.BusinessTestEODependents;
import org.nuclos.server.businesstest.execution.BusinessTestRESTFacadeBean;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generates the entity classes to be used in business test scripts.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
public class BusinessTestClassGenerator {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestClassGenerator.class);

	private final BusinessTestGenerationContext context;

	public BusinessTestClassGenerator(final BusinessTestGenerationContext context) {
		this.context = context;
	}

	/**
	 * Creates Groovy classes for all business entities in the context.
	 */
	public Map<UID, BusinessTestEntitySource> generateClasses() {
		final Map<UID, BusinessTestEntitySource> result = new HashMap<>();

		for (EntityMeta<Long> entity : context.getBusinessEntities()) {
			final BusinessTestEntitySource source = generateEntitySource(entity);
			result.put(entity.getUID(), source);
		}

		return result;
	}

	private String escapeIdentifier(final String identifier) {
		return NuclosEntityValidator.escapeJavaIdentifier(identifier);
	}

	/**
	 * Generates the Groovy source for the given entity.
	 */
	private BusinessTestEntitySource generateEntitySource(final EntityMeta<?> entity) {
		final String pkg = context.getBusinessTestEOPackage(entity.getUID());
		final String entityMetaId = context.getEntityMetaID(entity.getUID());
		final String className = context.getBusinessTestEOClassName(entity.getUID());

		final BusinessTestEntitySource source = new BusinessTestEntitySource(pkg, className);
		addImports(source);

		source.setBaseClass(BusinessTestEO.class);
		source.addField("final static META_ID = '" + entityMetaId + "'");

		addStates(entity, source);
		addBaseMethods(entity, className, source);
		addFields(entity, source);
		addDependents(context, entity, source);
		addCustomRules(context, entity, source);
		addProcesses(context, entity, source);

		// TODO: Forms enum

		return source;
	}

	/**
	 * TODO: Add only necessary imports.
	 */
	private void addImports(final BusinessTestEntitySource source) {
		source.addImport(BusinessTestEO.class);
		source.addImport(BusinessTestEODependents.class);
		source.addImport(BusinessTestRESTFacadeBean.class);
		source.addImport(BusinessTestRESTFacadeBean.class);
		source.addImport(JSONObject.class);
		source.addImport(UID.class);
		source.addImport(EntityObjectVO.class);
		source.addImport(Flag.class);
		source.addImport(NuclosFile.class);
	}

	private void addProcesses(
			final BusinessTestGenerationContext context,
			final EntityMeta<?> entity,
			final BusinessTestEntitySource source
	) {
		final Set<BusinessTestGenerationContext.Process> processes = context.getEntityProcesses(entity.getUID());

		if (processes.isEmpty()) {
			return;
		}

		final UID processFieldUID = SF.PROCESS.getUID(entity);

		// .unsetProcess()
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("unsetProcess")
						.setComment("Sets the Nuclos Process to null.")
						.setReturnType("void")
						.setSource("setAttributeUid('" + processFieldUID + "', (UID) null)")
						.build()
		);

		for (BusinessTestGenerationContext.Process process : processes) {
			final String escapedName = NuclosEntityValidator.escapeJavaIdentifierPart(process.getName());

			// change process: changeProcess<process name>
			source.addMethod(
					new AbstractGroovyClassSource.MethodBuilder("changeProcess" + escapedName)
							.setComment("Changes the Nuclos Process to \"" + process.getName() + "\".")
							.setReturnType("void")
							.setSource("setAttributeUid('" + processFieldUID + "', '" + process.getUID() + "')")
							.build()
			);
		}
	}

	private void addFields(final EntityMeta<?> entity, final BusinessTestEntitySource source) {
		for (FieldMeta<?> field : entity.getFields()) {
			final String fieldName = StringUtils.capitalize(
					NuclosEntityValidator.escapeJavaIdentifierPart(field.getFieldName())
			);

			addFieldGetter(source, field, fieldName);

			if (shouldHaveIdGetter(field)) {
				addFieldIdGetter(source, field, fieldName);
			}

			if (field.isSystemField()) {
				continue;
			}

			if (field.isFileDataType()) {
				addFieldFileSetter(source, field, fieldName);
			} else {
				// Getter/Setter for reference fields: get<field>Id, set<field>Id
				if (field.getForeignEntity() != null) {
					addFieldIdSetter(source, field, fieldName);
				} else {
					addFieldValueSetter(source, field, fieldName);
				}
			}
		}
	}

	private boolean shouldHaveIdGetter(FieldMeta<?> field) {
		return field.getForeignEntity() != null
				&& !field.isFileDataType();
	}

	private void addFieldValueSetter(
			final BusinessTestEntitySource source,
			final FieldMeta<?> field,
			final String fieldName
	) {
		final AbstractGroovyClassSource.MethodBuilder builder = new AbstractGroovyClassSource.MethodBuilder(
				"set" + fieldName
		);

		builder.setComment("Sets the value of the attribute \"" + field.getFieldName() + "\".")
				.setReturnType("void")
				.addParam("def", "value")
				.setSource("setAttribute('" + field.getUID() + "', value)");

		source.addMethod(
				builder.build()
		);
	}

	private void addFieldFileSetter(
			final BusinessTestEntitySource source,
			final FieldMeta<?> field,
			final String fieldName
	) {
		final AbstractGroovyClassSource.MethodBuilder builder = new AbstractGroovyClassSource.MethodBuilder(
				"set" + fieldName
		);

		builder.setComment("Sets the value of the attribute \"" + field.getFieldName() + "\".")
				.setTypeParameter("<T extends org.nuclos.api.common.NuclosFileBase>")
				.setReturnType("void")
				.addParam("T", "file")
				.setSource("setNuclosFile('" + field.getUID() + "', file)");

		source.addMethod(
				builder.build()
		);
	}

	private void addFieldIdSetter(final BusinessTestEntitySource source, final FieldMeta<?> field, final String fieldName) {
		AbstractGroovyClassSource.MethodBuilder builder = new AbstractGroovyClassSource.MethodBuilder("set" + fieldName + "Id")
				.setComment("Sets the ID value of the reference attribute \"" + field.getFieldName() + "\".")
				.setReturnType("void");

		if (isUIDRef(field)) {
			builder.
					addParam("UID", "uid")
					.setSource("setAttributeUid('" + field.getUID() + "', uid)");
		} else {
			builder.
					addParam("Long", "id")
					.setSource("setAttributeId('" + field.getUID() + "', id)");
		}

		source.addMethod(builder.build());
	}

	private void addFieldIdGetter(final BusinessTestEntitySource source, final FieldMeta<?> field, final String fieldName) {
		AbstractGroovyClassSource.MethodBuilder builder = new AbstractGroovyClassSource.MethodBuilder("get" + fieldName + "Id")
				.setComment("Returns the ID value of the reference attribute \"" + field.getFieldName() + "\".");

		if (isUIDRef(field)) {
			builder
					.setReturnType("UID")
					.setSource("getAttributeUid('" + field.getUID() + "')");
		} else {
			builder
					.setReturnType("Long")
					.setSource("getAttributeId('" + field.getUID() + "')");
		}

		source.addMethod(builder.build());
	}

	/**
	 * Adds a getter method for the given field.
	 */
	private void addFieldGetter(final BusinessTestEntitySource source, final FieldMeta<?> field, final String fieldName) {
		// Generate Getter/Setter: get<field>, set<field>
		AbstractGroovyClassSource.MethodBuilder builder = new AbstractGroovyClassSource.MethodBuilder("get" + fieldName)
				.setComment("Returns the value of the attribute \"" + field.getFieldName() + "\".");

		Class<?> returnType = field.getJavaClass();

		// Reference fields are returned "stringified" - there are separate methods to get the ID ("get...Id()")
		// and complete referenced object ("get...BO()")
		if (field.getForeignEntity() != null && !field.isFileDataType()) {
			returnType = String.class;
		}

		final String className = StringUtils.removeStart(
				returnType.getCanonicalName(),
				"java.lang."
		);
		builder.setReturnType(className);

		if (field.isFileDataType()) {
			builder.setSource("getNuclosFile('" + field.getUID() + "') as " + className);
		} else {
			builder.setSource("getAttribute('" + field.getUID() + "') as " + className);
		}

		source.addMethod(
				builder.build()
		);
	}

	private boolean isUIDRef(final FieldMeta<?> field) {
		final EntityMeta<Object> foreignEntity = E.getByUID(field.getForeignEntity());
		return foreignEntity != null && foreignEntity.isUidEntity();
	}

	private void addBaseMethods(final EntityMeta<?> entity, final String className, final BusinessTestEntitySource source) {
		// Constructor
		source.addMethod(
				new AbstractGroovyClassSource.ConstructorBuilder(className)
						.setComment("Constructor using an EntityObjectVO (for internal use).")
						.addParam("EntityObjectVO<Long>", "wrappedEO")
						.setSource("super(wrappedEO, " + className + ".class)")
						.build()
		);
		source.addMethod(
				new AbstractGroovyClassSource.ConstructorBuilder(className)
						.setComment("Public constructor.")
						.setSource("super(new UID('" + entity.getUID() + "'), " + className + ".class)")
						.build()
		);

		// .get(Long id)
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("get")
						.setComment("Fetches the " + className + " with the given ID.\nThis is equivalent to {@link " + QueryProvider.class.getName() + "#getById(Class, Object)}.")
						.addModifier(Modifier.STATIC)
						.setReturnType(className)
						.addParam("Long", "id")
						.setSource("return get(new UID('" + entity.getUID() + "'), " + className + ".class, id)")
						.build()
		);

		// .save()
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("save")
						.setComment("Saves this " + className + ".\nThis is equivalent to \n" +
								"\t{@link " + BusinessObjectProvider.class.getName() + "#insert(" + BusinessObject.class.getName() + ")}\n" +
								"\tor {@link " + BusinessObjectProvider.class.getName() + "#update(" + BusinessObject.class.getName() + ")}\n" +
								"depending on wether this " + className + " is new.")
						.setReturnType(className)
						.setSource("super.save()\n"
								+ "return this")
						.build()
		);

		// .findByState(int numeral)
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("findByState")
						.setComment("Finds 10 instances of " + className + " in the given state.\n" +
								"The result is not sorted, i.e. returns the first 10 results as returned by the underlying database.")
						.addModifier(Modifier.STATIC)
						.setReturnType("List<" + className + ">")
						.addParam("int", "numeral")
						.setSource("findByState(numeral, 10)")
						.build()
		);

		// .findByState(int numeral, int limit)
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("findByState")
						.setComment("Finds <i>limit</i> instances of " + className + " in the given state.\n" +
								"The result is not sorted, i.e. returns the first <i>limit</i> results as returned by the underlying database.")
						.addModifier(Modifier.STATIC)
						.setReturnType("List<" + className + ">")
						.addParam("int", "numeral").addParam("int", "limit")
						.setSource("String whereCondition = \"${META_ID}_nuclosStateNumber like '$numeral'\"\n"
								+ "return query(whereCondition, limit)")
						.build()
		);

		// .query(String where)
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("query")
						.setComment("Finds 100 instances of " + className + " by the given custom query.\n" +
								"See {@link query(String, int)}")
						.addModifier(Modifier.STATIC)
						.setReturnType("List<" + className + ">")
						.addParam("String", "whereCondition")
						.setSource("return query(new UID('" + entity.getUID() + "'), " + className + ".class, whereCondition, 100)")
						.build()
		);

		// .query(String where, int limit)
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("query")
						.setComment("Finds <i>limit</i> instances of " + className + " by the given custom query.\n" +
								"The query can be a SQL-like query as for the REST service, e.g.:\n" +
								"\n" +
								"    example_rest_Order_customer = 40000294\n" +
								"AND example_rest_Order_orderDate >= '2014-06-05'\n" +
								"AND example_rest_Order.id IN (\n" +
								"        SELECT example_rest_OrderPosition_order \n" +
								"        FROM example_rest_OrderPosition\n" +
								"        WHERE example_rest_OrderPosition_price > 800\n" +
								"    )")
						.addModifier(Modifier.STATIC)
						.setReturnType("List<" + className + ">")
						.addParam("String", "whereCondition").addParam("int", "limit")
						.setSource("return query(new UID('" + entity.getUID() + "'), " + className + ".class, whereCondition, limit)")
						.build());

		// .first()
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("first")
						.setComment("Returns the first instance of " + className + " as returned by the underlying database.")
						.addModifier(Modifier.STATIC)
						.setReturnType(className)
						.setSource("return first(new UID('" + entity.getUID() + "'), " + className + ".class)")
						.build()
		);

		// .list()
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("list")
						.setComment("Returns the first 100 instances of " + className + " as returned by the underlying database.")
						.addModifier(Modifier.STATIC)
						.setReturnType("List<" + className + ">")
						.setSource("return list(new UID('" + entity.getUID() + "'), " + className + ".class, 100)")
						.build()
		);

		// .list(int limit)
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("list")
						.setComment("Returns the first <i>limit</i> instances of " + className + " as returned by the underlying database.")
						.addModifier(Modifier.STATIC)
						.setReturnType("List<" + className + ">")
						.addParam("int", "limit")
						.setSource("return list(new UID('" + entity.getUID() + "'), " + className + ".class, limit)")
						.build()
		);

		// .toString()
		source.addMethod(
				new AbstractGroovyClassSource.MethodBuilder("toString")
						.setComment("Returns the entity name and ID.")
						.setReturnType("String")
						.setSource("'" + className + " ' + id")
						.build()
		);
	}

	private void addStates(final EntityMeta<?> entity, final BusinessTestEntitySource source) {
		if (entity.isStateModel()) {
			Collection<StateVO> states = context.getEntityStates(entity.getUID());
			SortedSet<String> stateSet = new TreeSet<>();
			for (StateVO state : states) {
				stateSet.add("STATE_" + state.getNumeral());
			}
			source.addField("enum STATES {\n		" + StringUtils.join(stateSet, ",\n		") + "\n	}");
		}
	}

	/**
	 * Adds accessor methods for all dependents of the entity.
	 */
	private void addDependents(final BusinessTestGenerationContext context, final EntityMeta<?> entity, final BusinessTestEntitySource source) {
		List<BusinessTestGenerationContext.Dependency> dependencies = context.getDependencies(entity.getUID());
		for (BusinessTestGenerationContext.Dependency dep : dependencies) {
			final EntityMeta<?> dependentEntity = context.getEntity(dep.getSourceEntity());
			final FieldMeta<?> fieldMeta = context.getField(dep.getFieldUID());

			if (dependentEntity instanceof NucletLanguageEntityMeta) {
				LOG.warn("Skipping generation of dependency on language entity \"{}\" for entity \"{}\"", dependentEntity.getEntityName(), entity.getEntityName());
				continue;
			} else if (dependentEntity.getNuclet() == null) {
				LOG.warn("Skipping generation of dependency on entity without nuclet \"{}\" for entity \"{}\"", dependentEntity.getEntityName(), entity.getEntityName());
				continue;
			}

			String dependencyName = dependentEntity.getEntityName();
			if (context.isMultiDependency(dep)) {
				dependencyName += "By" + StringUtils.capitalize(fieldMeta.getFieldName());
			}

			final String methodName = escapeIdentifier(
					"get" + StringUtils.capitalize(dependencyName)
			);

			final String dependentPackage = context.getBusinessTestEOPackage(dep.getSourceEntity());
			final String dependentClassName = context.getBusinessTestEOClassName(dep.getSourceEntity());
			final String dependentFullName = dependentPackage + "." + dependentClassName;

			// .get<DependentName>()
			source.addMethod(
					new AbstractGroovyClassSource.MethodBuilder(methodName)
							.setComment("Returns a modifiable Collection of the dependent " + dependentEntity.getEntityName() + "\n" +
									"by the reference attribute " + fieldMeta.getFieldName() + ".")
							.setReturnType("BusinessTestEODependents<" + dependentFullName + ">")
							.setSource("return getDependents(new UID('" + dependentEntity.getUID() + "'), new UID('" + dep.getFieldUID() + "'), " + dependentFullName + ".class)")
							.build()
			);

			// .get<DependentName>(Flag... flags)
			source.addMethod(
					new AbstractGroovyClassSource.MethodBuilder(methodName)
							.setComment("Returns a modifiable Collection of the dependent " + dependentEntity.getEntityName() + "\n" +
									"by the reference attribute " + fieldMeta.getFieldName() + "\n" +
									"filtered by the given {@link " + Flag.class.getName() + "}s.")
							.setReturnType("BusinessTestEODependents<" + dependentFullName + ">")
							.addParam("Flag...", "flags")
							.setSource("return getDependents(new UID('" + dependentEntity.getUID() + "'), new UID('" + dep.getFieldUID() + "'), " + dependentFullName + ".class, flags)")
							.build()
			);
		}
	}

	private void addCustomRules(final BusinessTestGenerationContext context, final EntityMeta<?> entity, final BusinessTestEntitySource source) {
		for (BusinessTestGenerationContext.CustomRule customRule : context.getCustomRules(entity.getUID())) {
			final String methodName = "execute" + StringUtils.capitalize(BusinessTestGenerationContext.escapeIdentifier(customRule.getSimpleName()));
			source.addMethod(
					new AbstractGroovyClassSource.MethodBuilder(methodName)
							.setComment("Executes the CustomRule " + customRule.getSimpleName() + " on this " + source.getClassName() + ".")
							.setReturnType(source.getClassName())
							.setSource("(" + source.getClassName() + ") super.executeCustomRule('" + customRule.getFullName() + "')")
							.build()
			);
		}
	}
}
