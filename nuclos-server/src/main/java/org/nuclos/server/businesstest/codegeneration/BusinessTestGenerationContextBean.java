package org.nuclos.server.businesstest.codegeneration;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.NucletVO;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeBean;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataRestFqnCache;
import org.nuclos.server.masterdata.ejb3.MetaDataFacadeBean;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.statemodel.ejb3.StateFacadeBean;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Provides new BusinessTestGenerationContexts to business test class and script generators.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Service
public class BusinessTestGenerationContextBean {

	private final MetaDataFacadeBean metaDataFacadeBean;
	@Qualifier("masterDataService")
	private final MasterDataFacadeBean masterDataFacade;
	private final StateFacadeBean stateFacadeBean;
	private final MasterDataRestFqnCache fqnCache;
	private final EventSupportFacadeBean eventSupportFacadeBean;

	public BusinessTestGenerationContextBean(
			final MetaDataFacadeBean metaDataFacadeBean,
			@Qualifier("masterDataService") final MasterDataFacadeBean masterDataFacade,
			final StateFacadeBean stateFacadeBean,
			final MasterDataRestFqnCache fqnCache,
			final EventSupportFacadeBean eventSupportFacadeBean
	) {
		this.metaDataFacadeBean = metaDataFacadeBean;
		this.masterDataFacade = masterDataFacade;
		this.stateFacadeBean = stateFacadeBean;
		this.fqnCache = fqnCache;
		this.eventSupportFacadeBean = eventSupportFacadeBean;
	}

	BusinessTestGenerationContext getNewContext() throws CommonPermissionException {
		final Collection<EntityMeta<?>> entities = metaDataFacadeBean.getAllEntities();
		final List<EventSupportEventVO> eventSupportEntities = eventSupportFacadeBean.getEventSupportEntities();
		final Set<BusinessTestGenerationContext.Process> processes = getProcesses();

		final Map<UID, NucletVO> nucletCache = new HashMap<>();
		return new BusinessTestGenerationContext(entities, eventSupportEntities, processes) {
			@Override
			protected NucletVO getNuclet(final UID nucletUID) {
				if (!nucletCache.containsKey(nucletUID)) {
					MasterDataVO<UID> mdvo;
					try {
						mdvo = masterDataFacade.get(E.NUCLET.getUID(), nucletUID);
						NucletVO nucletVO = new NucletVO(mdvo);
						nucletCache.put(nucletUID, nucletVO);
					} catch (CommonFinderException | CommonPermissionException e) {
						e.printStackTrace();
					}
				}

				return nucletCache.get(nucletUID);
			}

			@Override
			public String getEntityMetaID(final UID entityUID) {
				return fqnCache.translateUid(E.ENTITY, entityUID);
			}

			@Override
			public Collection<StateVO> getEntityStates(final UID entityUID) {
				return stateFacadeBean.getStatesByModule(entityUID);
			}
		};
	}

	private Set<BusinessTestGenerationContext.Process> getProcesses() {
		final Collection<MasterDataVO<UID>> processVOs = masterDataFacade.getMasterData(E.PROCESS, TrueCondition.TRUE);

		final Set<BusinessTestGenerationContext.Process> processes = new HashSet<>();
		for (MasterDataVO<UID> processVO: processVOs) {
			final BusinessTestGenerationContext.Process process = new BusinessTestGenerationContext.Process(
					processVO.getPrimaryKey(),
					processVO.getFieldValue(E.PROCESS.name),
					processVO.getFieldUid(E.PROCESS.module)
			);
			processes.add(process);
		}

		return processes;
	}
}