package org.nuclos.server.businesstest.codegeneration.script;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.codegeneration.BusinessTestClassLoaderBean;
import org.nuclos.server.businesstest.codegeneration.BusinessTestGenerationContext;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestSampleDataProvider;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract base class for different INSERT script generators.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
abstract class BusinessTestInsertScriptGenerator extends AbstractBusinessTestScriptGenerator {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestClassLoaderBean.class);

	private final IBusinessTestSampleDataProvider sampleDataProvider;

	BusinessTestInsertScriptGenerator(
			final EntityMeta<?> meta,
			final IBusinessTestNucletCache nucletCache,
			final IBusinessTestLogger logger,
			final BusinessTestGenerationContext context, final IBusinessTestSampleDataProvider sampleDataProvider
	) {
		super(meta, nucletCache, logger, context);
		this.sampleDataProvider = sampleDataProvider;
	}

	abstract Collection<FieldMeta<?>> getFields();

	private <T> T generateSampleValue(FieldMeta<T> field, T defaultValue) {
		T sampleValue = sampleDataProvider.getSampleData(field);

		if (sampleValue == null) {
			getLogger().print("   no sample data for field " + field.getFieldName() + " - using default value \"" + defaultValue + "\"... ");
			sampleValue = defaultValue;
		}

		return sampleValue;
	}

	@Override
	public BusinessTestScriptSource generateScriptSource() {
		final String className = getClassName();
		final BusinessTestScriptSource script = createDefaultScript();

		script.addScriptLine(className + " bo = new " + className + "()");

		for (FieldMeta<?> field : getFields()) {
			// TODO: lookup constraints?

			final String fieldName = BusinessTestGenerationContext.escapeIdentifier(field.getFieldName());
			if (field.getForeignEntity() != null) {
				final UID foreignEntityUID = field.getForeignEntity();
				final EntityMeta<?> foreignEntity = getContext().getEntity(foreignEntityUID);
				if (foreignEntity.isUidEntity()) {
					getLogger().println("References to system entities cannot be auto-generated yet");
					script.addScriptLine("bo." + StringUtils.uncapitalize(fieldName) + "Id = null // TODO: Set a valid UID here");
				} else {
					importNucletPackage(script, foreignEntity);
					final String foreignEntityName = BusinessTestGenerationContext.escapeIdentifier(foreignEntity.getEntityName());
					script.addScriptLine("bo." + StringUtils.uncapitalize(fieldName) + "Id = " + foreignEntityName + ".first()?.id");
				}
			} else if (String.class.isAssignableFrom(field.getJavaClass())) {
				String sampleValue = generateSampleValue((FieldMeta<String>) field, RandomStringUtils.randomAlphabetic(10));
				script.addScriptLine("bo." + StringUtils.uncapitalize(fieldName) + " = '" + StringEscapeUtils.escapeJava(sampleValue) + "'");
			} else if (Boolean.class.isAssignableFrom(field.getJavaClass())) {
				Boolean sampleValue = generateSampleValue((FieldMeta<Boolean>) field, true);
				script.addScriptLine("bo." + StringUtils.uncapitalize(fieldName) + " = " + Boolean.toString(sampleValue));
			} else if (Date.class.isAssignableFrom(field.getJavaClass())) {
				Date sampleValue = generateSampleValue((FieldMeta<Date>) field, DateUtils.truncate(new Date(), Calendar.DATE));
				DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
				script.addScriptLine("bo." + StringUtils.uncapitalize(fieldName) + " = new Date().parse('dd.MM.yyyy', '" + df.format(sampleValue) + "')");
			} else if (Integer.class.isAssignableFrom(field.getJavaClass())) {
				Integer sampleValue = generateSampleValue((FieldMeta<Integer>) field, 0);
				script.addScriptLine("bo." + StringUtils.uncapitalize(fieldName) + " = " + sampleValue);
			} else if (Double.class.isAssignableFrom(field.getJavaClass())) {
				Double sampleValue = generateSampleValue((FieldMeta<Double>) field, 0.0);
				script.addScriptLine("bo." + StringUtils.uncapitalize(fieldName) + " = " + sampleValue);
			} else if (BigDecimal.class.isAssignableFrom(field.getJavaClass())) {
				BigDecimal sampleValue = generateSampleValue((FieldMeta<BigDecimal>) field, BigDecimal.ZERO);
				script.addScriptLine("bo." + StringUtils.uncapitalize(fieldName) + " = " + sampleValue);
			} else {
				// TODO: Handle other data types
				LOG.warn("Could not generate sample data for attribute type {}", field.getJavaClass());
			}
		}

		script.addScriptLine("bo.save()");

		return script;
	}

	private void importNucletPackage(final BusinessTestScriptSource script, final EntityMeta<?> foreignEntity) {
		if (foreignEntity.getNuclet() != null) {
			String pkg = nucletCache.getNuclet(foreignEntity.getNuclet()).getPackage();
			if (pkg != null) {
				script.addImport(pkg + ".*");
			}
		}
	}
}
