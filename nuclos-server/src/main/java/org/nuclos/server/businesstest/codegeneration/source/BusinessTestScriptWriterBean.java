package org.nuclos.server.businesstest.codegeneration.source;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.common.file.FileNameMapper;
import org.nuclos.common2.IOUtils;
import org.nuclos.server.businesstest.BusinessTestManagementBean;
import org.nuclos.server.businesstest.NucletVO;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Compiles the business test class sources.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Component
public class BusinessTestScriptWriterBean {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestScriptWriterBean.class);

	private final BusinessTestManagementBean managementBean;
	private final BusinessTestScriptScannerBean scannerBean;
	private final IBusinessTestNucletCache nucletCache;

	private final File outputDir;

	public BusinessTestScriptWriterBean(
			final BusinessTestManagementBean managementBean,
			final BusinessTestScriptScannerBean scannerBean,
			final IBusinessTestNucletCache nucletCache
	) {
		this.managementBean = managementBean;
		this.scannerBean = scannerBean;
		this.nucletCache = nucletCache;

		outputDir = NuclosCodegeneratorConstants.BUSINESS_TEST_SRC_TEST_FOLDER;
	}

	@PostConstruct
	public void init() {
		try {
			writeScripts(managementBean.getAll());
		} catch (Exception e) {
			LOG.error("Could not write business test scripts", e);
		}
	}

	/**
	 * Deletes the output directory and writes all test scripts anew.
	 */
	private void writeScripts(final Collection<BusinessTestVO> tests) throws IOException {
		LOG.info("Writing business test scripts to file system");
		deleteScripts();
		writeScriptsIncremental(tests);
		writePomXml();
	}

	/**
	 * Generates and writes a pom.xml to the business test source folder.
	 */
	private void writePomXml() throws IOException {
		final String nuclosVersion = ApplicationProperties.getInstance().getNuclosVersion().getVersionNumber();
		final String groovyVersion = "2.4.7";    // TODO: Get Groovy version dynamically

		final BusinessTestMavenGenerator mavenGenerator = new BusinessTestMavenGenerator(nuclosVersion, groovyVersion);
		final String pomXml = mavenGenerator.generatePomXml();
		final File pomXmlFile = new File(NuclosCodegeneratorConstants.BUSINESS_TEST_SRC_FOLDER, "pom.xml");

		FileUtils.writeStringToFile(pomXmlFile, pomXml);
	}

	/**
	 * Truncates the business test script output directory (btsrc/test/).
	 */
	public void deleteScripts() {
		try {
			scannerBean.setEnabled(false);
			File[] files = outputDir.listFiles();
			if (files != null) {
				for (File file : files) {
					FileUtils.forceDelete(file);
				}
			}
		} catch (Exception e) {
			LOG.debug("Could not delete business test script directory", e);
		} finally {
			scannerBean.setEnabled(true);
		}
	}

	/**
	 * See {@link #writeScriptsIncremental(Collection)}
	 */
	public void writeScriptIncremental(BusinessTestVO test) throws IOException {
		writeScriptsIncremental(Collections.singletonList(test));
	}

	/**
	 * Writes changed or new test scripts to the output directory.
	 */
	public void writeScriptsIncremental(final Collection<BusinessTestVO> tests) throws IOException {
		try {
			scannerBean.setEnabled(false);

			for (BusinessTestVO test : tests) {
				final String pkg = getPackage(test);
				final String fileName = test.getName() + ".groovy";
				final File file = getFile(pkg, fileName);

				if (!file.exists() || file.lastModified() < test.getChangedAt().getTime()) {
					IOUtils.writeToTextFile(file, test.getSource(), "UTF-8");
				}
			}
		} finally {
			scannerBean.setEnabled(true);
		}
	}

	/**
	 * Physically deletes the given test script.
	 */
	public void deleteScript(BusinessTestVO test) {
		deleteScripts(Collections.singletonList(test));
	}

	/**
	 * Physically deletes the given test scripts.
	 */
	private void deleteScripts(final Collection<BusinessTestVO> tests) {
		try {
			scannerBean.setEnabled(false);

			for (BusinessTestVO test : tests) {
				final String pkg = getPackage(test);
				final String fileName = test.getName() + ".groovy";
				final File file = getFile(pkg, fileName);

				if (file.exists()) {
					if (file.delete()) {
						LOG.debug("Deleted: {}", file);
					}
				}
			}
		} finally {
			scannerBean.setEnabled(true);
		}
	}

	private String getPackage(BusinessTestVO test) {
		if (test.getNuclet() == null) {
			return null;
		}
		final NucletVO nuclet = nucletCache.getNuclet(test.getNuclet());
		if (nuclet == null) {
			return null;
		}

		return nuclet.getPackage();
	}

	/**
	 * Returns the corresponding File for the given package and filename.
	 * Creates the package directory if it does not yet exist.
	 */
	private File getFile(final String pkg, String fileName) {
		final File result;

		fileName = FileNameMapper.escape(fileName);

		if (StringUtils.isNotBlank(pkg)) {
			fileName = pkg.replaceAll("\\.", Matcher.quoteReplacement(File.separator)) + File.separator + fileName;
		}

		result = new File(outputDir, fileName);
		if (!result.getParentFile().exists()) {
			if (!result.getParentFile().mkdirs()) {
				LOG.warn("Could not create parent directory for {}", result);
			}
		}

		return result;
	}
}
