package org.nuclos.server.businesstest.codegeneration;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.server.businesstest.BusinessTestManagementBean;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.codegeneration.script.AbstractBusinessTestScriptGenerator;
import org.nuclos.server.businesstest.codegeneration.script.BusinessTestScriptGeneratorFactory;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptSource;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptWriterBean;
import org.nuclos.server.statemodel.ejb3.StateFacadeBean;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Generates test scripts for entities, e.g. scripts for CRUD operations, state changes, and so on.
 * <p>
 * TODO: Too many dependencies.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Service
public class BusinessTestScriptGeneratorBean {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestScriptGeneratorBean.class);

	private final StateFacadeBean stateFacadeBean;
	private final BusinessTestManagementBean businessTestManagementBean;
	private final IBusinessTestNucletCache nucletCache;
	private final BusinessTestGenerationContextBean generationContext;
	private final BusinessTestSampleDataBean sampleDataBean;
	private final BusinessTestScriptWriterBean scriptWriter;

	public BusinessTestScriptGeneratorBean(
			final StateFacadeBean stateFacadeBean,
			final BusinessTestManagementBean businessTestManagementBean,
			final IBusinessTestNucletCache nucletCache,
			final BusinessTestGenerationContextBean generationContext,
			final BusinessTestSampleDataBean sampleDataBean,
			final BusinessTestScriptWriterBean scriptWriter
	) {
		this.stateFacadeBean = stateFacadeBean;
		this.businessTestManagementBean = businessTestManagementBean;
		this.nucletCache = nucletCache;
		this.generationContext = generationContext;
		this.sampleDataBean = sampleDataBean;
		this.scriptWriter = scriptWriter;
	}

	private BusinessTestVO createTest(EntityMeta<?> entity, String name) {
		final BusinessTestVO test = new BusinessTestVO();
		test.setName(name);
		test.setNuclet(entity.getNuclet());

		test.setDescription("");
		test.setVersion(-1);

		test.setNuclet(entity.getNuclet());

		return test;
	}

	private BusinessTestVO generateTest(
			final AbstractBusinessTestScriptGenerator generator,
			final IBusinessTestLogger logger
	) {
		final String name = generator.getTestName();
		logger.print("\tGenerating Test " + name + " for entity " + generator.getMeta().getEntityName() + "...");

		final BusinessTestVO test = createTest(generator.getMeta(), name);
		final BusinessTestScriptSource script = generator.generateScriptSource();

		test.setSource(script.getGroovySource());

		try {
			businessTestManagementBean.create(test);
			logger.println("   OK");
			return test;
		} catch (RuntimeException e) {
			LOG.warn("Failed to create business test", e);
			logger.println("   FAILED: " + e.getMessage());
		}

		return null;
	}

	private BusinessTestVO generateInsertThinTest(
			final BusinessTestScriptGeneratorFactory factory
	) {
		return generateTest(factory.newInsertThinScriptGenerator(), factory.getLogger());
	}

	private BusinessTestVO generateInsertFullTest(
			final BusinessTestScriptGeneratorFactory factory
	) {
		return generateTest(factory.newInsertFullScriptGenerator(), factory.getLogger());
	}

	private BusinessTestVO generateUpdateTest(
			final BusinessTestScriptGeneratorFactory factory
	) {
		return generateTest(factory.newUpdateScriptGenerator(), factory.getLogger());
	}

	private BusinessTestVO generateDeleteTest(
			final BusinessTestScriptGeneratorFactory factory
	) {
		return generateTest(factory.newDeleteScriptGenerator(), factory.getLogger());
	}

	private Set<BusinessTestVO> generateStateChangeTests(
			final BusinessTestScriptGeneratorFactory factory
	) {
		final Set<BusinessTestVO> result = new HashSet<>();

		final EntityMeta<?> entity = factory.getEntity();
		final IBusinessTestLogger logger = factory.getLogger();

		if (!entity.isStateModel()) {
			throw new IllegalArgumentException("The given entity is not stateful");
		}

		logger.println("Generating StateChange-Tests for entity " + entity.getEntityName() + "...");

		final Collection<StateVO> states = stateFacadeBean.getStatesByModule(entity.getUID());
		for (StateVO sourceState : states) {
			Collection<StateTransitionVO> transitions = stateFacadeBean.findStateTransitionBySourceStateNonAutomatic(sourceState.getId());
			for (StateTransitionVO transition : transitions) {
				StateVO targetState = stateFacadeBean.getState(transition.getStateTargetUID());

				final BusinessTestVO test = generateTest(factory.newStateChangeScriptGenerator(sourceState, targetState), logger);
				result.add(test);
			}
		}

		return result;
	}

	private Set<BusinessTestVO> generateCustomRuleTests(
			final BusinessTestScriptGeneratorFactory factory,
			final BusinessTestGenerationContext context
	) {
		final Set<BusinessTestVO> result = new HashSet<>();
		final Set<BusinessTestGenerationContext.CustomRule> customRules = context.getCustomRules(factory.getEntity().getUID());
		for (BusinessTestGenerationContext.CustomRule customRule : customRules) {
			final BusinessTestVO test = generateTest(factory.newCustomRuleScriptGenerator(customRule.getSimpleName()), factory.getLogger());
			result.add(test);
		}
		return result;
	}

	/**
	 * Generates basic test cases.
	 */
	public void generateAllTests(final IBusinessTestLogger logger) {
		try {
			logger.println("Generating all business tests...");
			final BusinessTestGenerationContext context = generationContext.getNewContext();
			final Set<BusinessTestVO> tests = new HashSet<>();

			int count = 0;
			for (EntityMeta<?> entity : context.getBusinessEntities()) {
				count++;

				if (entity.isDatasourceBased()) {
					logger.println("Skipping dynamic entity \"" + entity.getEntityName() + "\"");
					continue;
				} else if (entity.isGeneric()) {
					logger.println("Skipping generic entity \"" + entity.getEntityName() + "\"");
					continue;
				} else if (entity.isProxy()) {
					logger.println("Skipping proxy entity \"" + entity.getEntityName() + "\"");
					continue;
				}

				final BusinessTestScriptGeneratorFactory factory = new BusinessTestScriptGeneratorFactory(entity, nucletCache, logger, sampleDataBean, context);

				tests.add(generateInsertThinTest(factory));
				tests.add(generateInsertFullTest(factory));
				tests.add(generateUpdateTest(factory));
				tests.add(generateDeleteTest(factory));

				tests.addAll(generateCustomRuleTests(factory, context));

				if (entity.isStateModel()) {
					tests.addAll(generateStateChangeTests(factory));
				}

				logger.printProgress(count, context.getBusinessEntities().size());
			}
			logger.println("Generating all business tests: Done");

			tests.remove(null);

			scriptWriter.writeScriptsIncremental(tests);
		} catch (Exception ex) {
			logger.printError("Failed to generate business tests", ex);
		}
	}
}
