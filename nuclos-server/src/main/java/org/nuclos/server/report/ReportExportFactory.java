//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.report;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.server.report.export.CsvExport;
import org.nuclos.server.report.export.ExcelExport;
import org.nuclos.server.report.export.JasperExport;
import org.nuclos.server.report.export.WordExport;

public abstract class ReportExportFactory {	
	
	public static Export getExport(String searchCondition, ReportOutputVO.Format format, ReportOutputVO.PageOrientation orientation, boolean columnScaled) {
			switch (format) {
			case PDF:
				JasperExport export = new JasperExport(searchCondition, orientation, columnScaled);				
				return export;
			case CSV:
				return new CsvExport();
			case TSV:
				return new CsvExport('\t', 0, ' ', false);
			case XLS:
			case XLSX:
				return new ExcelExport(format);
			case DOC:
			case DOCX:
				return new WordExport(format);
			default:
				throw new NuclosFatalException("Format " + format + " is not supported for server-side execution.");
			}
	}

}
