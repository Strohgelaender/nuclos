//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ReportFieldDefinition;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.StringUtils;

public abstract class Export {

	public abstract NuclosFile test(DefaultReportOutputVO output, UID mandatorUID) throws NuclosReportException;
	
	public abstract NuclosFile export(DefaultReportOutputVO output, Map<String, Object> params, Locale locale, int maxrows, UID language, UID mandatorUID) throws NuclosReportException;
	
	public abstract NuclosFile export(ReportOutputVO output, ResultVO result, List<ReportFieldDefinition> fields) throws NuclosReportException;

	protected static String getPath(String path, final ResultVO resultVO, final List<ReportFieldDefinition> fields) {
		return ReportFieldDefinition.getPath(path, resultVO, fields);
	}

}
