package org.nuclos.server.report;

import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.nbo.NuclosObjectBuilder;

public class ChartDatasourceObjectBuilder extends NuclosObjectBuilder {

	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.datasource.chart";
	private static final String DEFFAULT_ENTITY_PREFIX = "DSCRT";
	private static final String DEFFAULT_ENTITY_POSTFIX = "DSCRT";
	
	public ChartDatasourceObjectBuilder() {}
	
	public static String getNameForFqn(String sName) {
		return formatMethodName(NuclosEntityValidator.escapeJavaIdentifier(sName, DEFFAULT_ENTITY_PREFIX)) + DEFFAULT_ENTITY_POSTFIX;
	}
	
	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		String retVal = DEFFAULT_PACKAGE_NUCLET;

		if (nucletUID != null) {
			String sFqn = nucletCache.getFullQualifiedNucletName(nucletUID);
			if (!StringUtils.looksEmpty(sFqn)) {
				retVal = sFqn;
			}
		}

		return retVal;
	}

	@Override
	protected void createObjects() throws CommonBusinessException {}
	
}
