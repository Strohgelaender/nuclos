//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.eventsupport.ejb3;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.nuclos.api.ide.SourceItemFacadeRemote;
import org.nuclos.api.ide.valueobject.INuclosApi;
import org.nuclos.api.ide.valueobject.ISourceItem;
import org.nuclos.api.ide.valueobject.NuclosApi;
import org.nuclos.api.ide.valueobject.SourceItem;
import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.api.parameter.NucletParameter;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.EventSupportNotification;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.security.MessageDigest;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.customcode.codegenerator.GeneratedFile;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent;
import org.nuclos.server.customcode.codegenerator.SourceScannerComponent;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.parameter.NuclosParameterProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(noRollbackFor = { Exception.class })
@DependsOn("eventSupportService")
public class SourceItemFacadeBean implements SourceItemFacadeLocal, SourceItemFacadeRemote {
	
	private static final Logger LOG = LoggerFactory.getLogger(SourceItemFacadeBean.class);
	
	//
	
	// begin of Spring injection
	
	@Autowired
	private CustomCodeManager customCodeManager;
	
	@Autowired
	private EventSupportCache esCache;
	
	@Autowired
	private NuclosJavaCompilerComponent nuclosJavaCompilerComponent;
	
	@Autowired
	private SourceScannerComponent sourceScannerComponent;
	
	@Autowired
	private SourceCache sourceCache;
	
	@Autowired
	private NuclosParameterProvider paramProv;
	
	// end of Spring injection
	
	public SourceItemFacadeBean() {
	}
	
	private void scanJar(SourceType type, File jar, File srcDir, 
			Map<String, SourceInfo> map, Set<String> allServerCodeClasses) throws IOException {
		
		if (jar.isFile()) {
			final JarFile jarFile = new JarFile(jar);
			final URL jarContext = new URL("jar:" + jar.toURI().toURL() + "!/");
			final URL srcContext = srcDir.toURI().toURL();
			
			// scan jar
			for (Enumeration<JarEntry> enu = jarFile.entries(); enu.hasMoreElements();) {
				final JarEntry entry = enu.nextElement();
				if (!entry.isDirectory()) {
					final String name = entry.getName();
					final String qname = name.substring(0, name.length() - 6).replace('/', '.');
					// Filter out 'old rules'
					// if (name.endsWith(".class") && !name.startsWith("Rule_") && !name.startsWith("TimelimitRule_")) {
					if (name.endsWith(".class") && isNewRule(type, qname, allServerCodeClasses)) {
						map.put(qname, new SourceInfo(qname, type, 
								new URL(srcContext, name.substring(0, name.length() - 6) + ".java"), 
								new URL(jarContext, name)));
					}
				}
			}
		}
		
		// sometimes there is no jar because of compile problems
		// hence scan source folders as well
		scanSourceDir("", type, jar, srcDir, map, allServerCodeClasses);
	}
	
	/**
	 * TODO: Fixme, see https://nuclets.atlassian.net/browse/EPR-6
	 */
	private boolean isNewRule(SourceType type, String qualifiedName, Set<String> allServerCodeClasses) { 
		if (qualifiedName.startsWith("Rule_") || qualifiedName.startsWith("TimelimitRule_")) {
			return false;
		}
		if (SourceType.EVENT_SUPPORT_CLASS.equals(type)) {
			return allServerCodeClasses.contains(qualifiedName);
		}
		return true;
	}
	
	private void scanSourceDir(String path, SourceType type, File jar, File srcDir, 
			Map<String, SourceInfo> map, Set<String> allServerCodeClasses) throws IOException {
		
		final URL jarContext = new URL("jar:" + jar.toURI().toURL() + "!/");
		final URL srcContext = srcDir.toURI().toURL();
		class JavaFileFilter implements FileFilter {

			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory() || pathname.getName().endsWith(".java");
			}
			
		}
		final File dir = new File(srcDir, path);
		// if dir is not a directory listFile(..) will return null (tp)
		if (dir.isDirectory()) {
			for (File f: dir.listFiles(new JavaFileFilter())) {
				final String name = f.getName();
				if (!f.isDirectory()) {
					final String srcFile = path + File.separator + name;
					final String qname = path.replace(File.separatorChar, '.') + "." + name.substring(0, name.length() - 5);
					if (!map.containsKey(qname) && isNewRule(type, qname, allServerCodeClasses)) {
						map.put(qname, new SourceInfo(qname, type, 
								new URL(srcContext, srcFile), 
								new URL(jarContext, srcFile.substring(0, srcFile.length() - 5) + ".class")));					
					}
				} else {
					if (path == null || "".equals(path)) {
						path = f.getName();
					} else {
						path += File.separator + f.getName();
					}
					scanSourceDir(path, type, jar, srcDir, map, allServerCodeClasses);
				}
			}
		}
	}	

	@Override
	public URL getDownloadURL(String qualifiedClassname, boolean src) throws IllegalAccessException {
		if (!NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			throw new IllegalAccessException("Server is not configured for developing");
		}
		final URL result;
		if (qualifiedClassname.endsWith(".jar")) {
			if (NuclosCodegeneratorConstants.JAR_NAMES_LIST.contains(qualifiedClassname)) {
				try {
					result = new File(NuclosCodegeneratorConstants.GENERATOR_FOLDER, qualifiedClassname).toURI().toURL();
				} catch (MalformedURLException e) {
					throw new NuclosFatalException(e);
				}
				return result;
			}
			/*
			if (NuclosCodegeneratorConstants.CCCE_NAME.equals(qualifiedClassname)) {
				try {
					result = NuclosCodegeneratorConstants.CCCEJARFILE.toURI().toURL();
				} catch (MalformedURLException e) {
					throw new NuclosFatalException(e);
				}
				return result;
			}
			 */
		}
		final Map<String, SourceInfo> qname2Info = getQname2Info();
		final SourceInfo info = qname2Info.get(qualifiedClassname);
		if (info == null || info.getType() == SourceType.PACKAGE) {
			result = null;
		} else if (src) {
			result = info.getSourceUrl();
			sourceCache.addOrUpdate(info);
		} else {
			result = info.getClassUrl();
			sourceCache.addOrUpdate(info);
		}
		return result;
	}
	
	private URI getServerLibsDir() {
		/*
		String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		String decodedPath = URLDecoder.decode(path, "UTF-8");
		return new URI("file://"+decodedPath.substring(0, decodedPath.lastIndexOf("/"))+"/");
		 */
		// see http://maven.apache.org/plugin-developers/common-bugs.html#Converting_between_URLs_and_Filesystem_Paths
		final URL url = getClass().getProtectionDomain().getCodeSource().getLocation();
		final File file = FileUtils.toFile(url).getParentFile();
		return file.toURI();
	}
	
	private Collection<File> getNuclosApiJars() {
		Collection<File> nuclosApiJars = new ArrayList<File>();

		File serverLibs = new File(getServerLibsDir()); 
		
		Map<UID, String> nucletParameterNames = paramProv.getNucletParameterNames();
		final Set<String> extensions = new HashSet<String>();
		for (Entry<UID, String> eParam : nucletParameterNames.entrySet()) {
			if (NuclosParameterProvider.NUCLOS_SERVERCODE_CLASSPATH_EXTENSION.equals(eParam.getValue())) {
				UID nucletParamUid = eParam.getKey();
				try {
					String value = paramProv.getNucletParameter(new NucletParameter(nucletParamUid.getString()));
					if (value.indexOf(";") >= 0 || value.indexOf(":") >= 0) {
						String[] split = value.split("(:|;)");
						for (String s : split) {
							extensions.add(s.trim());
						}
					} else {
						extensions.add(value.trim());
					}
				} catch (Exception e) {
					LOG.warn(NuclosParameterProvider.NUCLOS_SERVERCODE_CLASSPATH_EXTENSION + 
							" for name=" + eParam.getValue() + 
							" ,id=" + eParam.getKey() + 
							" failed: " + 
							e.getMessage(), e);
				}
			}
		}
		
		for (File serverLibEntry : serverLibs.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.startsWith("nuclos-") && name.contains("-api-") && name.endsWith(".jar")) {
					if (!name.contains("ide")) {
						return true;
					}
				}
				for (String ext : extensions) {
					if (ext.equals(name)) {
						return true;
					}
					if (ext.contains("{v}")) {
						try {
							String regex = ext.replace("{v}", "(\\d|\\.|-)*");
							Pattern pattern = Pattern.compile(regex);
							Matcher matcher = pattern.matcher(name);
							while (matcher.find()) {
								return true;
							}
						} catch (Exception e) {
							LOG.warn(NuclosParameterProvider.NUCLOS_SERVERCODE_CLASSPATH_EXTENSION + 
									" for extension=" + ext + 
									" failed: " + 
									e.getMessage(), e);
						}
					}
				}
				return false;
			}
		})) {
			nuclosApiJars.add(serverLibEntry);
		}
		return nuclosApiJars;
	}
	
	@Override
	public URL getNuclosApiDownloadURL(String jarFileName) throws IllegalAccessException {
		try {
			if (jarFileName == null) {
				return getServerLibsDir().toURL();
			}
			if (!NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
				throw new IllegalAccessException("Server is not configured for developing");
			}
			final URL libContext = getServerLibsDir().toURL();
			for (File jar : getNuclosApiJars()) {
				if (jar.getName().equals(jarFileName)) {
					return new URL(libContext, jar.getName());
				}
			}
		} catch (IOException e) {
			throw new NuclosFatalException(e);
		}
		return null;
	}
	
	@Override
	public INuclosApi getNuclosApi() throws IllegalAccessException {
		if (!NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			throw new IllegalAccessException("Server is not configured for developing");
		}
		try {
			Set<String> nuclosApiJarNames = new HashSet<String>();
			for (File jar : getNuclosApiJars()) {
				nuclosApiJarNames.add(jar.getName());
			}
			return new NuclosApi(
					String.format("%s (%s)", 
							ApplicationProperties.getInstance().getNuclosVersion().getVersionNumber(),
							ApplicationProperties.getInstance().getNuclosVersion().getVersionDateString()), 
					nuclosApiJarNames);
		} catch (RuntimeException e) {
			throw new NuclosFatalException(e);
		}
	}

	/**
	 * TODO: honor includeOldApiRules (tp)
	 */
	@Override
	public ISourceItem getTree(SourceType type, String qualifiedPackagenameAsRoot, boolean includeOldApiRules) throws IllegalAccessException {
		if (!NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			throw new IllegalAccessException("Server is not configured for developing");
		}
		if (includeOldApiRules) {
			throw new UnsupportedOperationException();
		}
		final SortedSet<SourceInfo> filtered = filter(type, qualifiedPackagenameAsRoot);
		if (qualifiedPackagenameAsRoot != null) {
			throw new UnsupportedOperationException("TODO");
		}
		if (qualifiedPackagenameAsRoot == null) {
			// default package
			qualifiedPackagenameAsRoot = "";
		}
		
		// add packages
		final SortedSet<SourceInfo> pas = new TreeSet<SourceInfo>(SourceInfo.QNAME_COMPARATOR);
		for (SourceInfo si: filtered) {
			int begin = 0;
			int idx = -1;
			do {
				idx = si.getQualifiedName().indexOf('.', begin);
				if (idx >= 0) {
					begin = idx + 1;
					final String pa = si.getQualifiedName().substring(0, idx);
					pas.add(new SourceInfo(pa, SourceType.PACKAGE, null, null));
				}
			} while (idx >= 0);
		}
		// add default package
		pas.add(new SourceInfo("", SourceType.PACKAGE, null, null));
		filtered.addAll(pas);
		
		final SourceInfo fsi = filtered.first();
		//filtered.remove(fsi);
		final ISourceItem root = sourceCache.getOrCreateSourceItem(fsi);
		assert qualifiedPackagenameAsRoot.equals(root.getQualifiedName());
		
		// build hierachy
		connect(root, fsi, filtered);
		
		return root;
	}
	
	@Override
	public ISourceItem uploadSourceItem(ISourceItem item, boolean isOldRule, byte[] content) throws IllegalAccessException {
		if (!NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			throw new IllegalAccessException("Server is not configured for developing");
		}
		if (isOldRule) {
			throw new UnsupportedOperationException();
		}
		final String hashValue = MessageDigest.digestAsBase64(SourceCache.DIGEST, content);
		if (!hashValue.equals(item.getHashValue())) {
			LOG.warn("{} hash differs: from ide: {} server: {}",
			         item.getQualifiedName(), item.getHashValue(), hashValue);
		}
		final SourceType type = item.getType();
		final String qualifiedName = item.getQualifiedName();
		final Map<String, SourceInfo> qname2Info = getQname2Info();
		SourceInfo si = qname2Info.get(qualifiedName);
		if (si == null) {
			// throw new IllegalArgumentException("Unknown source item " + qualifiedName);
			
			// A newly-created 'new rule'
			final String path = qualifiedName.replace('.', '/');
			try {
				final URL source = new URL(NuclosCodegeneratorConstants.JAR_SRC_FOLDER.toURI().toURL(), path + ".java");
				final URL clazz = new URL("jar:" + NuclosCodegeneratorConstants.JARFILE.toURI().toURL() + "!/" + path + ".class");
				si = new SourceInfo(qualifiedName, SourceType.EVENT_SUPPORT_CLASS, source, clazz);
			} catch (MalformedURLException e) {
				throw new IllegalArgumentException(e);
			}
		}
		if (si.getType() != type) {
			throw new IllegalArgumentException("Source item " + qualifiedName + " is of type " + si.getType() + ", not " + type);
		}
		
		// check if source has changed
		final ISourceItem sourceItem = sourceCache.getOrCreateSourceItem(si);
		if (hashValue.equals(sourceItem.getHashValue())) {
			return sourceItem;
		}
		
		sourceScannerComponent.cancel();
		try {
			final File file = FileUtils.toFile(si.getSourceUrl());
			// create directory structure
			file.getParentFile().mkdirs();
			
			final OutputStream fos = new FileOutputStream(file);
			try {
				fos.write(content);
			} finally {
				fos.close();
			}
			
			ISourceItem result = item;
			GeneratedFile gf = null;
			
			gf  = sourceScannerComponent.parseFile(file);
			if (gf.getPrimaryKey() == null) {
				// newly-created rule 'new rule'
				gf.setEntity(E.SERVERCODE);
				gf.setName(qualifiedName);
				final EntityObjectVO evo = sourceScannerComponent.createEventSupportRule(gf);
				
				// Create new source item
				result = new SourceItem();
				// no UID in ISourceItems! (tp)
				result.setId(gf.getPrimaryKey().getString());
				result.setType(SourceType.EVENT_SUPPORT_CLASS);
				result.setQualifiedName(qualifiedName);
				result.setHashValue(hashValue);
				
				// We just create a new rule (event object).
				// Hence it is necessary to clear the cache in order to 
				// get this rule with the next getTree(). (tp)
				esCache.invalidate(E.SERVERCODE);
				NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILED, JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION);
				NuclosJMSUtils.sendMessage(EventSupportNotification.COMPILED, JMSConstants.TOPICNAME_RULECOMPILATION);
			} else {
				sourceScannerComponent.updatePlainCode(gf);
			}
			sourceCache.addOrUpdate(gf);
			result.setHashValue(hashValue);
			sourceCache.addOrUpdate(result, si);
			return result;
		} catch (IOException e) {
			LOG.warn("IDE plugin uploadSourceItem failed: ", e);
			throw new IllegalStateException(e);
		} catch (CommonBusinessException e) {
			// Exception will be propagated to IDE - but CommonBusinessException is not know there
			LOG.warn("IDE plugin uploadSourceItem failed: ", e);
			throw new IllegalStateException(e.toString());			
		}
	}
	
	private SortedSet<SourceInfo> filter(SourceType type, String qualifiedPackagenameAsRoot) {
		if (qualifiedPackagenameAsRoot != null) {
			throw new UnsupportedOperationException("TODO");
		}
		final Map<String, SourceInfo> qname2Info = getQname2Info();
		final TreeSet<SourceInfo> result = new TreeSet<SourceInfo>(SourceInfo.QNAME_COMPARATOR);
		if (type == null) {
			result.addAll(qname2Info.values());
			return result;
		}
		for (String qname: qname2Info.keySet()) {
			final SourceInfo si = qname2Info.get(qname);
			sourceCache.addOrUpdate(si);
			if (si.getType() == type) {
				result.add(si);
			}
		}
		return result;
	}
	
	@Override
	public void forceRuleCompilation() {
		nuclosJavaCompilerComponent.forceCompile();
		NuclosCodegeneratorConstants.JARFILE.delete();
		sourceScannerComponent.schedule();
		try {
			customCodeManager.getClassLoaderAndCompileIfNeeded();
			customCodeManager.getClassLoaderAndCompileIfNeeded();
		}
		catch (NuclosCompileException e) {
			LOG.warn("Compilation failed: ", e);
		}
	}
	
	private void connect(ISourceItem root, SourceInfo si, SortedSet<SourceInfo> set) {
		/*
		 * Attention:
		 * As we cache the node, the children are normally NOT up-to-date.
		 * As we re-create the tree struction here, is ok to remove the
		 * (old) children and re-add them (now with the right hashValues).
		 * (tp)
		 */
		root.getChildren().clear();
		
		final int nop = si.getNop();
		final SortedSet<SourceInfo> tail = set.tailSet(si);
		for (SourceInfo s: tail) {
			final int snop = s.getNop();
			// find children
			if (nop + 1 == snop && (snop == 0 || s.getQualifiedName().startsWith(si.getQualifiedName() + "."))) {
				// child node
				if (s.getQualifiedName().contains("$")) {
					// inner class
					continue;
				}
				final ISourceItem child = sourceCache.getOrCreateSourceItem(s);
				root.addChild(child);
				
				if (child.getType() == SourceType.PACKAGE) {
					connect(child, s, tail);
				}
			}
			// break only if we got more 'dots' (nop) and the name is the same
			else if (nop + 1 < snop && s.getQualifiedName().startsWith(si.getQualifiedName() + ".")) {
				break;
			}
		}
	}
	
	/**
	 * TODO: cache map and invalidate on changes...
	 */
	private Map<String, SourceInfo> getQname2Info() {
		final Set<String> allServerCodeClasses = new HashSet<String>();
		for (EventSupportSourceVO ess: esCache.getInternExecutableEventSupportFiles()) {
			allServerCodeClasses.add(ess.getClassname());
		}
		for (EventSupportSourceVO ess: esCache.getInternEventLibs()) {
			allServerCodeClasses.add(ess.getClassname());
		}
		
		final Map<String, SourceInfo> qname2Info = new ConcurrentHashMap<String, SourceInfo>();
		final ClassLoader cl;
		try {
			cl = customCodeManager.getClassLoaderAndCompileIfNeeded();
			for (final EventSupportSourceVO src: esCache.getExecutableEventSupportFiles()) {
				addToQname2Info(qname2Info, allServerCodeClasses, cl, src);
			}
			for (final EventSupportSourceVO src: esCache.getInternEventLibs()) {
				addToQname2Info(qname2Info, allServerCodeClasses, cl, src);
			}
		} catch (NuclosCompileException e) {
			LOG.error("getQname2Info failed: unable to compile rules: ", e);
		}
			
		try {			
			scanJar(SourceType.BO_ENTITIES, NuclosCodegeneratorConstants.BOJARFILE, 
					NuclosCodegeneratorConstants.BO_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.DATASOURCE_REPORT, NuclosCodegeneratorConstants.DATASOURCEREPORTJARFILE, 
					NuclosCodegeneratorConstants.DATASOURCEREPORT_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.IMPORTSTRUCTUREDEFINITIONS, NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFSJARFILE, 
					NuclosCodegeneratorConstants.IMPORTSTRUCTUREDEFS_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.PARAMETER, NuclosCodegeneratorConstants.PARAMETERJARFILE, 
					NuclosCodegeneratorConstants.PARAMETER_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.COMMUNICATION, NuclosCodegeneratorConstants.COMMUNICATIONJARFILE, 
					NuclosCodegeneratorConstants.COMMUNICATION_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.STATEMODEL, NuclosCodegeneratorConstants.STATEMODELJARFILE, 
					NuclosCodegeneratorConstants.STATEMODEL_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.REPORT, NuclosCodegeneratorConstants.REPORTJARFILE, 
					NuclosCodegeneratorConstants.REPORT_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.WEBSERVICE, NuclosCodegeneratorConstants.WEBSERVICEJARFILE, 
					NuclosCodegeneratorConstants.WEBSERVICE_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.PRINTOUT, NuclosCodegeneratorConstants.PRINTOUTJARFILE, 
					NuclosCodegeneratorConstants.PRINTOUT_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.USERROLE, NuclosCodegeneratorConstants.USERROLEJARFILE, 
					NuclosCodegeneratorConstants.USERROLE_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.GENERATION, NuclosCodegeneratorConstants.GENERATIONJARFILE, 
					NuclosCodegeneratorConstants.GENERATION_SRC_FOLDER, qname2Info, allServerCodeClasses);
			scanJar(SourceType.EVENT_SUPPORT_CLASS, NuclosCodegeneratorConstants.JARFILE, 
					NuclosCodegeneratorConstants.JAR_SRC_FOLDER, qname2Info, allServerCodeClasses);
		} catch (IOException e) {
			LOG.error("getQname2Info: can't scan JAR: ", e);
		}
		return qname2Info;
	}
	
	private void addToQname2Info(final Map<String, SourceInfo> qname2Info, 
			final Set<String> allServerCodeClasses, final ClassLoader cl, final EventSupportSourceVO src) {
		
		if (src == null) {
			return;
		}
		try {
			if (src.isActive()) {
				final Class<?> clazz = cl.loadClass(src.getClassname());
				if (isNewRule(SourceType.EVENT_SUPPORT_CLASS, clazz.getName(), allServerCodeClasses)) {
					final SourceInfo old = qname2Info.put(clazz.getName(), 
							new SourceInfo(clazz.getName(), SourceType.EVENT_SUPPORT_CLASS, 
									null, clazz.getProtectionDomain().getCodeSource().getLocation()));
					if (old != null) {
						throw new IllegalArgumentException(clazz.getName());
					}
				}
			} else {
				LOG.debug("class is not active: {}", src.getClassname());
			}
		} catch (ClassNotFoundException e1) {
			LOG.warn("Unable to get Location for class {}", src.getClassname(), e1);
		}	
	}

}
