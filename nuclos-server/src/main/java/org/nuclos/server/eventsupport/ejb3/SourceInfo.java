//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.eventsupport.ejb3;

import java.net.URL;
import java.util.Comparator;

import org.nuclos.api.ide.valueobject.SourceType;

/**
 * Information about java code compiled by Nuclos
 * (rules, business objects, business support objects) derived from 
 * the place where the source and/or class is stored on the 
 * filesystem (URL based).
 *  
 * @author Thomas Pasch
 * @since Nuclos 3.14.18, 3.15.18, 4.0.15
 */
public class SourceInfo {
	
	public static final Comparator<SourceInfo> QNAME_COMPARATOR = new QNameComparator();
	
	/**
	 * Comparator for SourceInfo objects that is consistent with an tree 
	 * representation of java classes and packages.
	 */
	private static class QNameComparator implements Comparator<SourceInfo> {
		
		private QNameComparator() {
		}

		@Override
		public int compare(SourceInfo o1, SourceInfo o2) {
			int result;
			if (o1 == null) {
				if (o2 == null) {
					result = 0;
				}
				else {
					result = 1;
				}
			}
			else {
				if (o2 == null) {
					result = -1;
				}
				else {
					result = 0;
					
					if (result == 0) {
						result = (o1.getNop() > o2.getNop()) ? 1 : (o1.getNop() == o2.getNop() ? 0 : -1); 
					}
					// make sure, packages are always at the end of same nops entries
					if (result == 0) {
						if (o1.getType() != o2.getType()) {
							if (o1.getType() == SourceType.PACKAGE)
								return 1;
							if (o2.getType() == SourceType.PACKAGE)
								return -1;
						}
					}
					
					if (result == 0) {
						result = o1.getQualifiedName().compareTo(o2.getQualifiedName());
					}
				}
			}
			return result;
		}
		
	}
	
	//
	
	private final String qualifiedName;
	
	/**
	 * Number of '.' in qualified name.
	 */
	private final int nop;
	
	private final SourceType type;
	
	private final URL sourceUrl;
	
	private final URL classUrl;
	
	public SourceInfo(String qualifiedName, SourceType type, URL source, URL clazz) {
		if (qualifiedName == null || type == null) {
			throw new NullPointerException();
		}
		this.qualifiedName = qualifiedName;
		this.nop = count(qualifiedName);
		this.type = type;
		this.sourceUrl = source;
		this.classUrl = clazz;
	}
	
	public String getQualifiedName() {
		return qualifiedName;
	}
	
	public int getNop() {
		return nop;
	}

	public SourceType getType() {
		return type;
	}

	public URL getSourceUrl() {
		return sourceUrl;
	}

	public URL getClassUrl() {
		return classUrl;
	}
	
	/**
	 * Equals is based on {@link #getQualifiedName()}.
	 */
	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (!(other instanceof SourceInfo)) return false;
		final SourceInfo o = (SourceInfo) other;
		return qualifiedName.equals(o.qualifiedName);
	}
	
	/**
	 * Hashcode is based on {@link #getQualifiedName()}.
	 */
	@Override
	public int hashCode() {
		return 9367 + 23 * qualifiedName.hashCode();
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("SourceInfo[");
		result.append(type).append(",");
		result.append(qualifiedName);
		result.append("]");
		return result.toString();
	}
	
	private static int count(String s) {
		if (s == null) throw new NullPointerException();
		if ("".equals(s)) return -1;
		
		int result = 0;
		int begin = 0;
		int idx = -1;
		do {
			idx = s.indexOf('.', begin);
			if (idx >= 0) {
				begin = idx + 1;
				++result;
			}
		} while (idx >= 0);
		return result;
	}

}
