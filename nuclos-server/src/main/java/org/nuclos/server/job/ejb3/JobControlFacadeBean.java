//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.job.ejb3;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.job.IntervalUnit;
import org.nuclos.common.job.JobResult;
import org.nuclos.common.job.JobUtils;
import org.nuclos.common.job.LogLevel;
import org.nuclos.common.mail.NuclosMail;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.ValueValidationHelper;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.MailSendException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.JobCache;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.mail.NuclosMailServiceProvider;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.ejb3.SchedulerControlFacadeLocal;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.quartz.CronExpression;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for nucleus quartz job controller.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class JobControlFacadeBean extends MasterDataFacadeBean implements JobControlFacadeLocal, JobControlFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(JobControlFacadeBean.class);
	
	private SchedulerControlFacadeLocal scheduler;
	
	// Spring injection

	@Autowired
	private SpringLocaleDelegate localeDelegate;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	@Autowired
	private EntityObjectFacadeLocal entityObjectFacade;
	
	@Autowired
	private EventSupportCache esCache;
	
	@Autowired
	private JobCache jobCache;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	@Autowired
	private ServerParameterProvider parameterProvider;

	@Autowired
	private NuclosMailServiceProvider mailServiceProvider;
	
	// end of Spring injection
	
	public JobControlFacadeBean() {
	}

	protected SpringLocaleDelegate getLocaleDelegate() {
		return localeDelegate;
	}
	
	private SchedulerControlFacadeLocal getScheduler() {
		if (scheduler == null) {
			scheduler = ServerServiceLocator.getInstance().getFacade(SchedulerControlFacadeLocal.class);
		}
		return scheduler;
	}

	@RolesAllowed("Login")
	public MasterDataVO create(JobVO job) throws CommonBusinessException {
		checkWriteAllowed(E.JOBCONTROLLER);

		// try to generate cron expression and validate
		migrateToCron(job);

		MasterDataVO<UID> mdVO = job.toMasterDataVO();
		mdVO.setFieldValue(E.JOBCONTROLLER.laststate, JobControlFacadeLocal.NOT_ACTIVATED);
		mdVO.setFieldValue(E.JOBCONTROLLER.running, false);
		mdVO.setFieldValue(E.JOBCONTROLLER.lastfiretime, null);
		mdVO.setFieldValue(E.JOBCONTROLLER.result, null);
		mdVO.setFieldValue(E.JOBCONTROLLER.resultdetails, null);
		mdVO.setFieldValue(E.JOBCONTROLLER.nextfiretime, null);
		mdVO.setDependents(job.getDependants());
		
		MasterDataVO<UID> result = this.masterDataFacade.create(mdVO, null);
		getScheduler().addJob(new JobVO(result, result.getDependents()));
		
		esCache.invalidate(E.JOBCONTROLLER);
		jobCache.invalidate();
			
		return result;
	}

	@RolesAllowed("Login")
	public Object modify(JobVO job) throws CommonBusinessException {
		checkWriteAllowed(E.JOBCONTROLLER);

		// try to generate cron expression and validate
		migrateToCron(job);

		// check if name has changed. If it has changed, remove the quartz job with the old name.
		final MasterDataVO<UID> jobFromDb = (MasterDataVO<UID>) this.masterDataFacade.get(E.JOBCONTROLLER.getUID(), job.getId());
		final String name = jobFromDb.getFieldValue(E.JOBCONTROLLER.name.getUID(), String.class);
		final JobKey jk = JobKey.jobKey(name, Scheduler.DEFAULT_GROUP);
		final boolean isScheduled = getScheduler().isScheduled(jk);
		if (!job.getName().equals(name)) {
			getScheduler().deleteJob(jk);
		}
		MasterDataVO<UID> masterDataVO = job.toMasterDataVO();
		masterDataVO.setDependents(job.getDependants());
		
		// update job		
		Object result = this.masterDataFacade.modify(masterDataVO, null);

		if (isScheduled) {
			Trigger trigger = getScheduler().scheduleJob(job);

			Date nextFireTime = trigger.getFireTimeAfter(Calendar.getInstance().getTime());
			if (nextFireTime != null) {
				MasterDataVO mdvo = this.masterDataFacade.get(E.JOBCONTROLLER.getUID(), job.getId());
				mdvo.setFieldValue(E.JOBCONTROLLER.laststate, "Aktiviert");
				mdvo.setFieldValue(E.JOBCONTROLLER.nextfiretime, DateUtils.getDateAndTime(nextFireTime));

				this.masterDataFacade.modify(mdvo, null);
			}
		}
		
		esCache.invalidate(E.JOBCONTROLLER);
		jobCache.invalidate();
		
		return result;
	}

	@RolesAllowed("Login")
    public void remove(JobVO job) throws CommonBusinessException {
		checkWriteAllowed(E.JOBCONTROLLER);

		for (JobKey jk : getScheduler()._getJobKeys()) {
			if (jk.equals(job.getName())) {
				// job is scheduled with quartz, try to delete
				getScheduler().deleteJob(jk);
			}
		}

		MasterDataFacadeLocal mdFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
		mdFacade.remove(E.JOBCONTROLLER.getUID(), job.getId(), true, null);
		
		esCache.invalidate(E.JOBCONTROLLER);
		jobCache.invalidate();
		
    }

	/**
	 * Validates a <code>JobVO</code>.
	 * If the job is defined by interval, the corresponding cron expression is generated and validated.
	 *
	 * TODO: 
	 * Exception messages spit out here are propagated to client and are at 
	 * present totally ill-formatted. Even the resource translation did not 
	 * work! (tp)
	 *
	 * @param  job The <code>JobVO</code> to validate.
	 * @return <code>true</code> if the given job as been modified.
	 * @throws CommonValidationException
	 */
	public static boolean migrateToCron(JobVO job) throws CommonValidationException {
		final boolean changed;
		if (job.getUseCronExpression() == null || !job.getUseCronExpression()) {
			Date startdate = job.getStartdate();
			String starttime = job.getStarttime();
			Integer interval = job.getInterval();
			String unit = job.getUnit();

			if (!ValueValidationHelper.validateInputFormat(
					starttime, E.JOBCONTROLLER.startdate.getFormatInput())) {
				throw new CommonValidationException("job.validation.starttime");
			}

			if (startdate == null) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("masterdata.error.validation.value",
					E.JOBCONTROLLER.getLocaleResourceIdForLabel(),
					E.JOBCONTROLLER.startdate.getLocaleResourceIdForLabel()));
			}

			if (interval == null) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("masterdata.error.validation.value",
					E.JOBCONTROLLER.getLocaleResourceIdForLabel(),
					E.JOBCONTROLLER.interval.getLocaleResourceIdForLabel()));
			}

			if (unit == null) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("masterdata.error.validation.value",
					E.JOBCONTROLLER.getLocaleResourceIdForLabel(),
					E.JOBCONTROLLER.unit.getLocaleResourceIdForLabel()));
			}

			IntervalUnit intervalUnit = org.nuclos.common2.KeyEnum.Utils.findEnum(IntervalUnit.class, unit);
			Calendar c = Calendar.getInstance(ServerServiceLocator.getInstance().getFacade(LocaleFacadeLocal.class).getUserLocale().toLocale());
			c.setTime(startdate);
			int iHour = Integer.parseInt(starttime.split(":")[0]);
			int iMinute = Integer.parseInt(starttime.split(":")[1]);
			c.set(Calendar.HOUR_OF_DAY, iHour);
			c.set(Calendar.MINUTE, iMinute);

			job.setCronExpression(JobUtils.getCronExpressionFromInterval(intervalUnit, interval, c));
			changed = true;
		} else {
			changed = false;
		}
		if (!CronExpression.isValidExpression(job.getCronExpression())) {
			throw new CommonValidationException("scheduler.error.cronexpression");
		}
		return changed;
	}
	
	/**
	 * prepare job execution: clean protocol table, set 'running' true, create jobrun data record
	 *
	 * Note to transaction management:
	 * Execute this method in a separate transaction (TransactionAttributeType.REQUIRES_NEW)
	 * to avoid transaction isolation locks that last for the complete job execution.
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
    public Pair<JobVO, MasterDataVO<Long>> prepare(UID jobUID) {
		try {
			//get the JobVO
			JobVO jobVO = findJobVOWithNotifications(jobUID);

			//clean protocol table
			deleteOldLogInformation(jobVO);

			//job is running - set 'running' field true and modify mdvo
			jobVO.setRunning(true);
			makeMasterDataVO(jobVO);

			//create jobrun data record (starttime)
			MasterDataVO jobRun = createNewJobRun(jobUID);

			Pair<JobVO, MasterDataVO<Long>> pair = new Pair<JobVO, MasterDataVO<Long>>(jobVO, jobRun);

			return pair;
		}
		catch (Exception ex) {
			throw new RuntimeException(ex);
		}
    }

	/**
	 * delete Job entries older than delete value in days, 0 - delete all, null - delete nothing
	 * @param jobVO the job which log entries has to be deleted
	 * @throws CommonPermissionException
	 * @throws CommonStaleVersionException
	 * @throws CommonRemoveException
	 * @throws CommonFinderException
	 * @throws CommonCreateException
	 * @throws NuclosBusinessRuleException
	 * @throws NuclosCompileException 
	 */
	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	private void deleteOldLogInformation(JobVO jobVO) 
			throws CommonFinderException, CommonRemoveException, CommonStaleVersionException, 
			CommonPermissionException, NuclosBusinessRuleException, CommonCreateException {
		
		if((jobVO.getDeleteInDays() != null)) {
			final long deleteDate = System.currentTimeMillis() - ((((((((long)jobVO.getDeleteInDays().intValue() * 24)) * 60)) * 60)) * 1000);
			
			for(EntityObjectVO<Long> mdvo: this.masterDataFacade.<Long, UID>getDependantMd4FieldMeta(E.JOBRUN.parent, jobVO.getPrimaryKey())) {
				long createdAt = mdvo.getCreatedAt().getTime();
				if (createdAt < deleteDate) {
					this.masterDataFacade.remove(mdvo.getDalEntity(), mdvo.getPrimaryKey(), false, null);
				}
			}
		}
	}

	private void removeJobRunMessages(MasterDataVO<UID> mdvo) 
			throws NuclosBusinessRuleException, CommonFinderException, CommonRemoveException, 
			CommonStaleVersionException, CommonPermissionException {
		final CollectableComparison comp = 
				SearchConditionUtils.newComparison(E.JOBRUNMESSAGES.parent, ComparisonOperator.EQUAL, mdvo.getPrimaryKey());
		for(MasterDataVO<Long> vo : this.masterDataFacade.getMasterData(E.JOBRUNMESSAGES, comp)) {
			masterDataFacade.remove(vo.getEntityObject().getDalEntity(), vo.getPrimaryKey(), false, null);
		}
	}

	/**
	 * @param iParentId
	 * @throws NuclosBusinessRuleException
	 */
	private MasterDataVO createNewJobRun(UID iParentId) throws CommonCreateException, CommonPermissionException, NuclosBusinessRuleException{

		Map<UID, Object> mpFields = new HashMap<UID, Object>();
		Map<UID, Object> mpFieldUids = new HashMap<UID, Object>();
		
		mpFields.put(E.JOBRUN.startdate.getUID(), new DateTime(System.currentTimeMillis()));
		mpFieldUids.put(E.JOBRUN.parent.getUID(), iParentId);

		MasterDataVO<Long> mdvo = this.masterDataFacade.create(
				new MasterDataVO(E.JOBRUN.getUID(), null, new Date(), getCurrentUserName(), new Date(),
				getCurrentUserName(), new Integer(1), mpFields, null, mpFieldUids, true), null);

		return mdvo;
	}

	/**
	 * @param mdvo
	 * @throws CommonPermissionException
	 * @throws CommonValidationException
	 * @throws CommonStaleVersionException
	 * @throws CommonRemoveException
	 * @throws CommonFinderException
	 * @throws CommonCreateException
	 * @throws NuclosBusinessRuleException
	 * @throws NuclosCompileException 
	 */
	private Object modifyJobRun(MasterDataVO mdvo) 
			throws CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, 
			CommonValidationException, CommonPermissionException, NuclosBusinessRuleException {
		
		return this.masterDataFacade.modify(mdvo, null);
	}

	/**
	 * @param mdvo
	 * @return
	 * @throws CommonCreateException
	 * @throws CommonFinderException
	 * @throws CommonRemoveException
	 * @throws CommonStaleVersionException
	 * @throws CommonValidationException
	 * @throws CommonPermissionException
	 * @throws NuclosBusinessRuleException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = {Exception.class})
	private Object modifyJobRunInNewTransaction(MasterDataVO mdvo)
			throws CommonCreateException, CommonFinderException, CommonRemoveException, CommonStaleVersionException,
			CommonValidationException, CommonPermissionException, NuclosBusinessRuleException {

		return modifyJobRun(mdvo);
	}

	@RolesAllowed("Login")
	public void scheduleJob(UID jobUID) throws CommonBusinessException {
		checkWriteAllowed(E.JOBCONTROLLER);

		final MasterDataVO<UID> mdVO = this.masterDataFacade.get(E.JOBCONTROLLER.getUID(), jobUID);
		final JobVO jobVO = new JobVO(mdVO, mdVO.getDependents());
		final Trigger jobTrigger = getScheduler().scheduleJob(jobVO);
		Date nextFireTime = jobTrigger.getFireTimeAfter(Calendar.getInstance().getTime());
		if (nextFireTime != null) {
			LOG.info("Scheduled Job '{}' for {}", jobVO.getName(), nextFireTime);
			MasterDataVO<UID> mdvo = this.masterDataFacade.get(E.JOBCONTROLLER.getUID(), mdVO.getPrimaryKey());
			mdvo.setFieldValue(E.JOBCONTROLLER.laststate, "Aktiviert");
			mdvo.setFieldValue(E.JOBCONTROLLER.nextfiretime, DateUtils.getDateAndTime(nextFireTime));
			
			this.masterDataFacade.modify(mdvo, null);
			
			MasterDataVO<Long> run = createNewJobRun(jobUID);
			run.setFieldValue(E.JOBRUN.state, "Schedule activated by " + getCurrentUserName());
			modifyJobRun(run);
		}
		else {
			LOG.error("Scheduling Job '{}' failed", jobVO.getName());
		}
		
	}
	
	private void startJobImmediatelyRemote(Object oId) throws CommonBusinessException {
		
	}
	
	private void unscheduleRemoteJob(Object oId) throws CommonBusinessException {
		
	}
	
	private void scheduleRemoteJob(Object oId) throws CommonBusinessException {
		
	}
	
	private boolean isSchedulerRunning() {
		if(NuclosClusterRegisterHelper.runInClusterMode() && NuclosClusterRegisterHelper.iAmSlave())
			return false;
		return true;
	}

	@RolesAllowed("Login")
	public void unscheduleJob(UID jobUID) throws CommonBusinessException {
		checkWriteAllowed(E.JOBCONTROLLER);
		final MasterDataVO<UID> mdVO = this.masterDataFacade.get(E.JOBCONTROLLER.getUID(), jobUID);

		getScheduler().unscheduleJob(new JobVO(mdVO, mdVO.getDependents()));
		mdVO.setFieldValue(E.JOBCONTROLLER.laststate, JobControlFacadeLocal.NOT_ACTIVATED);
		mdVO.setFieldValue(E.JOBCONTROLLER.running, false);
		mdVO.setFieldValue(E.JOBCONTROLLER.nextfiretime, null);
		this.masterDataFacade.modify(mdVO, null);
		
		MasterDataVO<Long> run = createNewJobRun(jobUID);
		run.setFieldValue(E.JOBRUN.state, "Schedule deactivated by " + getCurrentUserName());
		modifyJobRun(run);
	}

	/**
	 * @param jobUID - id of job to execute
	 */
	@RolesAllowed("Login")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, noRollbackFor= {Exception.class})
	public void startJobImmediately(UID jobUID) throws CommonBusinessException {
		if(isSchedulerRunning()) {
		checkWriteAllowed(E.JOBCONTROLLER);
		try {
			final MasterDataVO<UID> md = masterDataFacade.get(E.JOBCONTROLLER.getUID(), jobUID);
			JobVO job = new JobVO(md, md.getDependents());
			
			MasterDataVO<Long> run = createNewJobRun(jobUID);
			run.setFieldValue(E.JOBRUN.state, "Schedule immediate by " + getCurrentUserName());
			modifyJobRun(run);
			
			getScheduler().triggerJob(job);
		} catch (CommonFinderException e) {
			throw new NuclosFatalException(e);
		}
		}
		else {
			startJobImmediatelyRemote(jobUID);
		}
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public void writeToJobRunMessages(Long iSessionId, String sLevel, String sMessage, String sRuleName) {
		try {
			MasterDataVO<UID> mdvo = new MasterDataVO(E.JOBRUNMESSAGES.getUID(), 
					null, new Date(), getCurrentUserName(), new Date(),
					getCurrentUserName(), new Integer(1), null, null,null, true);

			// NUCLOS-5852 2)
			sMessage = StringUtils.reduceMultilineText(sMessage, E.JOBRUNMESSAGES.message.getScale());

			mdvo.setFieldId(E.JOBRUNMESSAGES.parent, iSessionId);
			mdvo.setFieldValue(E.JOBRUNMESSAGES.level, sLevel);
			mdvo.setFieldValue(E.JOBRUNMESSAGES.message, sMessage);
			mdvo.setFieldValue(E.JOBRUNMESSAGES.rule, sRuleName);
			
			this.masterDataFacade.create(mdvo, null);
		}
		catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void sendMessage(UID sRecipient, String sSubject, String sMessage) throws MailSendException {
		String sEmail = null;
		if (sRecipient != null) {
			EntityObjectVO<UID> recipient = 
					NucletDalProvider.getInstance().getEntityObjectProcessor(E.USER).getByPrimaryKey(sRecipient);
		
			sEmail = recipient.getFieldValue(E.USER.email);
		}

		if (sRecipient != null && sEmail != null) {
			mailServiceProvider.send(new NuclosMail(sEmail, sSubject, sMessage));
		}
	}

	/**
	 * transforms a JobVO to a MasterDataVO
	 * @param jobvo
	 * @return MasterDataVO
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 * @throws CommonValidationException
	 * @throws CommonStaleVersionException
	 * @throws CommonRemoveException
	 * @throws CommonCreateException
	 * @throws NuclosBusinessRuleException
	 * @throws NuclosCompileException 
	 */
	private void makeMasterDataVO(JobVO jobvo) throws CommonFinderException, CommonPermissionException,
				CommonCreateException, CommonRemoveException, CommonStaleVersionException, CommonValidationException, 
				NuclosBusinessRuleException {
		this.modifyVO(jobvo.toMasterDataVO(), null, false);
	}

	/**
	 * @param oUid
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	private JobVO findJobVOWithNotifications(UID oUid) throws CommonFinderException, CommonPermissionException {
		final MasterDataVO<UID> md = masterDataFacade.get(E.JOBCONTROLLER.getUID(), oUid);
		final Collection<EntityObjectVO<UID>> notificationDeps = entityObjectFacade.getDependentEntityObjectsNoCheck(
				E.JOBNOTIFICATION.getUID(), E.JOBNOTIFICATION.jobController.getUID(), md.getPrimaryKey());
		final Collection<EntityObjectVO<UID>> mandatorDeps = entityObjectFacade.getDependentEntityObjectsNoCheck(
				E.JOBMANDATOR.getUID(), E.JOBMANDATOR.parent.getUID(), md.getPrimaryKey());
		final IDependentDataMap deps = new DependentDataMap();
		deps.addAllData(E.JOBNOTIFICATION.jobController, notificationDeps);
		deps.addAllData(E.JOBMANDATOR.parent, mandatorDeps);
		return new JobVO(md, deps);
	}

	/**
	 * get job procedures/functions
	 */
	@RolesAllowed("Login")
	public Collection<String> getDBObjects()  throws CommonPermissionException {
		checkReadAllowed(E.JOBCONTROLLER);

		return CollectionUtils.applyFilter(dataBaseHelper.getDbAccess().getCallableNames(), new Predicate<String>() {
			@Override public boolean evaluate(String name) {
				return StringUtils.toUpperCase(name).startsWith("JOB_");
			}
		});
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
    public void setJobExecutionResult(Object oResult, Date dFireTime, Date dNextFireTime, JobVO jobVO, MasterDataVO<Long> jobRun) {
	    try {
	    	if (jobRun != null) {
				jobVO = findJobVOWithNotifications(jobVO.getId());
				StringBuilder mailBuffer = new StringBuilder();
				int iInfoCount = 0;
				int iWarningCount = 0;
				int iErrorCount = 0;
				int iTotalcount = 0;
				for (EntityObjectVO<Long> mdvo: masterDataFacade.<Long, Long>getDependantMd4FieldMeta(E.JOBRUNMESSAGES.parent, jobRun.getPrimaryKey())) {
					final String level = mdvo.getFieldValue(E.JOBRUNMESSAGES.level);
					final String msg = mdvo.getFieldValue(E.JOBRUNMESSAGES.message);
					final LogLevel logLevel;
					if (StringUtils.looksEmpty(level)) {
						logLevel = null;
					} else {
						logLevel = LogLevel.valueOf(level);
					}
					if (logLevel == LogLevel.INFO) {
						iInfoCount++;
					}
					else if (logLevel == LogLevel.WARNING) {
						iWarningCount++;
					}
					else if (logLevel == LogLevel.ERROR) {
						iErrorCount++;
					}
					if (iTotalcount < 100) {
						mailBuffer.append(logLevel.toString()).append(" ");
						mailBuffer.append(msg).append("\n");
						iTotalcount++;
					}
				}
				
				String sState = "INFO: " + iInfoCount + " entries\nWARNING: "+ iWarningCount + " entries\nERROR: " + iErrorCount + " entries";
				if (oResult != null) {
					sState = (String)oResult;
					if (sState.length() > 250) {
						sState = sState.substring(0, 250);
					}
				}
				jobRun.setFieldValue(E.JOBRUN.enddate, new DateTime(System.currentTimeMillis()));
				jobRun.setFieldValue(E.JOBRUN.state, sState);
				modifyJobRunInNewTransaction(jobRun);

				jobVO.setLastFireTime(DateUtils.getDateAndTime(dFireTime));
				if (dNextFireTime != null) {
					jobVO.setNextFireTime(DateUtils.getDateAndTime(dNextFireTime));
				}
				
				final JobResult jobResult = JobResult.getResult(iWarningCount, iErrorCount);
				jobVO.setRunning(false);
				jobVO.setResultAsEnum(jobResult);
				jobVO.setResultDetails(oResult == null ? sState : (String)oResult);
				if (jobVO.getResultDetails().length() > E.JOBCONTROLLER.resultdetails.getScale()) {
					jobVO.setResultDetails(jobVO.getResultDetails().substring(0, E.JOBCONTROLLER.resultdetails.getScale()));
				}
				makeMasterDataVO(jobVO);
				
				// create mail message
				String instanceName = parameterProvider.getValue(ParameterProvider.KEY_NUCLOS_INSTANCE_NAME);
				StringBuilder mailBuilder = new StringBuilder();
				mailBuilder.append("Nuclos-Instanz: ").append(instanceName).append("\n");
				mailBuilder.append(sState).append("\n\n");
				mailBuilder.append(mailBuffer);
				mailBuilder.append("\n\n");
				mailBuilder.append("(This message was generated by Nuclos / Diese Benachrichtigung wurde von Nuclos generiert)");
				
				try {
					sendMessage(jobResult, jobVO, mailBuilder.toString());
				} catch (Exception ex) {
					LOG.error("Unable to send message:", ex);
					writeToJobRunMessages(jobRun.getPrimaryKey(), "ERROR",
					                      "Error while trying to send email: " + ex.getMessage(),
					                      null);
				}
			
			}
	    }
	    catch (Exception ex) {
	    	throw new RuntimeException(ex.getMessage(), ex);
	    }
    }

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
    public void setJobExecutionResultError(UID oId, Date dFireTime, Date sNextFireTime, Long lSessionId, Exception e) {
	    try {
			JobVO jobVO = findJobVOWithNotifications(oId);
			jobVO.setRunning(false);
			jobVO.setResultAsEnum(JobResult.ERROR);
			jobVO.setLastFireTime(DateUtils.getDateAndTime(dFireTime));
			if (sNextFireTime != null) {
				jobVO.setNextFireTime(DateUtils.getDateAndTime(sNextFireTime));
			}
			StringBuilder sb = new StringBuilder(e.getClass().getName() + ": ");
			sb.append(getLocaleDelegate().getMessageFromResource(e.getMessage()));
			jobVO.setResultDetails(sb.toString());
			makeMasterDataVO(jobVO);

			if (lSessionId != null) {
				MasterDataVO<Long> jobRun = this.masterDataFacade.get(E.JOBRUN.getUID(), lSessionId);
				jobRun.setFieldValue(E.JOBRUN.enddate, new DateTime(System.currentTimeMillis()));
				jobRun.setFieldValue(E.JOBRUN.state, sb.toString());
				modifyJobRun(jobRun);
			}
			
			try {
				sendMessage(JobResult.ERROR, jobVO, sb.toString());
			} catch (Exception ex) {
				LOG.error("Unable to send message:", ex);
				writeToJobRunMessages(lSessionId, "ERROR",
				                      "Error while trying to send email: " + ex.getMessage(), null);
			}
	    } catch (Exception ex) {
	    	throw new RuntimeException(ex.getMessage(), ex);
	    }
    }

	private void sendMessage(JobResult sResult, JobVO jobVO, String sMessage) throws MailSendException {
		final String subject = "Nuclos: Job " + jobVO.getName();
		
		// TODO: old notifications, obsolete
		if (jobVO.getLevel() != null) {
			final LogLevel level = LogLevel.Utils.findEnum(LogLevel.class, jobVO.getLevel());
			if (LogLevel.mustNotify(level, sResult)) {
				sendMessage(jobVO.getUser(), subject, sMessage);
			}
		}
		
		// new notifications
		final Collection<EntityObjectVO<UID>> notifications = jobVO.getDependants().<UID>getDataPk(E.JOBNOTIFICATION.jobController);
		for (EntityObjectVO<UID> eo: notifications) {
			final UID user = eo.getFieldUid(E.JOBNOTIFICATION.userRef);
			final String level = eo.getFieldValue(E.JOBNOTIFICATION.level);
			final String email = eo.getFieldValue(E.JOBNOTIFICATION.email);
			if (!StringUtils.looksEmpty(level)) {
				final LogLevel logLevel = LogLevel.Utils.findEnum(LogLevel.class, level);
				if (LogLevel.mustNotify(logLevel, sResult)) {
					if (user != null) {
						sendMessage(user, subject, sMessage);
					}
					if (!StringUtils.looksEmpty(email)) {
						mailServiceProvider.send(new NuclosMail(email, subject, sMessage));
					}
				}
			}
		}
	}
	
	public void setMandatorInUserContext(UID mandator) {
		userCtx.setMandatorUID(mandator);
	}
}
