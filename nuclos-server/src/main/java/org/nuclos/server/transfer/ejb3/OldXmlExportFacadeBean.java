//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.transfer.ejb3;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for all XML Transfer functions. <br>
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class OldXmlExportFacadeBean extends NuclosFacadeBean implements OldXmlExportFacadeLocal, OldXmlExportFacadeRemote {

	private OldXmlExportImportProtocolFacadeLocal protocolFacade;
	
	private LocaleFacadeLocal localeFacade;

	private Integer iProtocolId;

	private Modules modules;
	private GenericObjectMetaDataCache genericObjectMetaDataCache;
	private List<Pair<String, Integer>> exportedIds;
	private Map<Integer, Map<EntityAndField, String>> mpGoSubFormsWithForeignKeys;
	private Map<String, Map<EntityAndField, String>> mpMdSubFormsWithForeignKeys;
	private Date today;
	private SimpleDateFormat dateFormat;

	private Integer iProcessedEntities = 0;

	private Integer iActionNumber = 1;
	
	private GenericObjectFacadeLocal genericObjectFacade;
	
	private MasterDataFacadeLocal masterDataFacade;
	
	public OldXmlExportFacadeBean() {
	}

	public org.nuclos.common2.File xmlExport(Map<Long, UID> exportEntities, boolean deepexport, String sFileName) 
			throws CommonFinderException, CommonPermissionException, IOException, CommonCreateException, NuclosBusinessRuleException {
		throw new NotImplementedException();
	}

	/**
	 * §jboss.method-attributes read-only = "true"
	 *
	 * @param exportEntities map of ids and entitynames to export
	 * @return Zip File with Xml output and Document files
	 */
	public org.nuclos.common2.File xmlExport(Map<Integer, String> exportEntities, boolean deepexport, boolean withDependants, String sFileName) 
			throws CommonFinderException, CommonPermissionException, IOException, CommonCreateException, NuclosBusinessRuleException {
		throw new NotImplementedException();
	}

	/**
	 * get the count of the processed exported entities
	 */
	public Integer getProcessedEntities() {
		throw new NotImplementedException();
	}
	
}
