//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.transfer.ejb3;

import java.util.Date;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for XML Transfer Protocol functions.<br>
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor={Exception.class})
public class OldXmlExportImportProtocolFacadeBean extends NuclosFacadeBean implements OldXmlExportImportProtocolFacadeLocal, OldXmlExportImportProtocolFacadeRemote {
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	public OldXmlExportImportProtocolFacadeBean() {
	}
	
	/**
	 * creates the protocol header for an export/import transaction
	 */
	public Integer writeExportImportLogHeader(String sType, String sUserName, Date dImportDate, String sImportEntityName, String sImportFileName) 
			throws CommonCreateException, CommonPermissionException, NuclosBusinessRuleException {
		throw new NotImplementedException();
		/*
		MasterDataMetaVO mdmvo = getMasterDataFacade().getMetaData(E.IMPORTEXPORT.getEntityUID());
		MasterDataVO mdvo = new MasterDataVO(mdmvo, false);

		mdvo.setFieldValue("type", sType);
		mdvo.setFieldValue("user", sUserName);
		mdvo.setFieldValue("date", dImportDate);
		mdvo.setFieldValue("entity", sImportEntityName);
		mdvo.setFieldValue("filename", sImportFileName);

		return getMasterDataFacade().create(E.IMPORTEXPORT.getEntityUID(), mdvo, null, null).getIntId();
		*/
	}

	/**
	 * creates a protocol entry for the given protocol header id
	 */
	public void writeExportImportLogEntry(Integer iParentId, String sMessageLevel, String sImportAction, String sImportMessage, Integer iActionNumber) 
			throws CommonCreateException, CommonPermissionException, NuclosBusinessRuleException {
		if (iParentId == null) {
			return;
		}
		throw new NotImplementedException();
		/*
		MasterDataMetaVO mdmvo = getMasterDataFacade().getMetaData(E.IMPORTEXPORTMESSAGES.getEntityUID());
		MasterDataVO mdvo = new MasterDataVO(mdmvo, false);

		mdvo.setFieldValue("importexportId", iParentId);
		mdvo.setFieldValue("messagelevel", sMessageLevel);
		mdvo.setFieldValue("action", sImportAction);
		mdvo.setFieldValue("message", sImportMessage);
		mdvo.setFieldValue("actionnumber", iActionNumber);

		getMasterDataFacade().create(E.IMPORTEXPORTMESSAGES.getEntityUID(), mdvo, null, null);
		*/
	}

	/**
	 * add content of xml file to protocol header of given id
	 */
	public void addFile(Integer iParentId, byte[] ba) {
		throw new NotImplementedException();
	}

	/**
	 * returns the content of the xml file for the given protocol id
	 *
	 * @param iParentId
	 * @return org.nuclos.common2.File
	 */
	public org.nuclos.common2.File getFile(Integer iParentId) {
		throw new NotImplementedException();
	}
}
