//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.webservice.ejb3;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.WebService;
import org.nuclos.api.ws.WebServiceObject;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.customcode.CustomCodeManager;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("webserviceProvider")
public class WebServiceFacadeBean implements WebService{

	@Autowired
	private MasterDataFacadeLocal mdFacade;
	@Autowired
	private CustomCodeManager ccm;
	
	@Override
	public <T> T getStub(Class<T> stubClass, WebServiceObject webServiceUID) throws BusinessException{
		
		T stub = null;
	
		try {
			MasterDataVO<UID> service = mdFacade.get(E.WEBSERVICE.getUID(), (UID) webServiceUID.getUID());
			stub = (T) ccm.getCurrentClassLoader().loadClass(stubClass.getCanonicalName()).newInstance();
			
			String username = service.getFieldValue(E.WEBSERVICE.user);
			String password = service.getFieldValue(E.WEBSERVICE.password);
			String url = service.getFieldValue(E.WEBSERVICE.host);
			Boolean useProxy = service.getFieldValue(E.WEBSERVICE.useproxy);
			String authentificationType = service.getFieldValue(E.WEBSERVICE.authtype);
			String encoding = service.getFieldValue(E.WEBSERVICE.encoding);
			
        	Class<?> clzzEndpointReference = 
        			stubClass.getClassLoader().loadClass("org.apache.axis2.addressing.EndpointReference");
        	Object serviceClient = stubClass.getMethod("_getServiceClient").invoke(stub);
        	
        	Object serviceClientOptions = serviceClient.getClass().getMethod("getOptions").invoke(serviceClient);
			serviceClientOptions.getClass().getMethod("setTo",
				clzzEndpointReference).invoke(serviceClientOptions, clzzEndpointReference.getConstructor(url.getClass()).newInstance(url));
			Method mthdSetProperty = serviceClientOptions.getClass().getMethod("setProperty", String.class, Object.class);

			//PROXY
			if (useProxy != null) {
				if (useProxy) {
					String proxyName = service.getFieldValue(E.WEBSERVICE.proxyname);
					Integer proxyPort = service.getFieldValue(E.WEBSERVICE.proxyport);
					String proxyusername = service.getFieldValue(E.WEBSERVICE.proxyuser);
					String proxypasswort = service.getFieldValue(E.WEBSERVICE.proxypassword);
					String proxydomain = service.getFieldValue(E.WEBSERVICE.proxydomain);

					// PROXY Configuration
					Class<?> clzzProxyProperties = stub.getClass().getClassLoader().loadClass("org.apache.axis2.transport.http.HttpTransportProperties$ProxyProperties");
					Object proxyConfig = clzzProxyProperties.newInstance();

					if (!StringUtils.isNullOrEmpty(proxyName))
						// proxyConfig.setProxyName(proxyName);
						clzzProxyProperties.getMethod("setProxyName", String.class).invoke(proxyConfig, proxyName);
					if (proxyPort != null)
						//	proxyConfig.setProxyPort(proxyPort);
						clzzProxyProperties.getMethod("setProxyPort", int.class).invoke(proxyConfig, proxyPort);
					if (!StringUtils.isNullOrEmpty(proxyusername))
						//	proxyConfig.setUserName(proxyusername);
						clzzProxyProperties.getMethod("setUserName", String.class).invoke(proxyConfig, proxyusername);
					if (!StringUtils.isNullOrEmpty(proxypasswort))
						//proxyConfig.setPassWord(proxypasswort);
						clzzProxyProperties.getMethod("setPassWord", String.class).invoke(proxyConfig, proxypasswort);
					if (!StringUtils.isNullOrEmpty(proxydomain))
						//	proxyConfig.setDomain(proxydomain);
						clzzProxyProperties.getMethod("setDomain", String.class).invoke(proxyConfig, proxydomain);

					mthdSetProperty.invoke(serviceClientOptions, "PROXY", proxyConfig);
					mthdSetProperty.invoke(serviceClientOptions, "__HTTP_PROTOCOL_VERSION__", "HTTP/1.0");
				}
			}

			// AUTHENTIFICATION
			Class<?> clzzAuthenticator = stub.getClass().getClassLoader().loadClass("org.apache.axis2.transport.http.HttpTransportProperties$Authenticator");
			Object auth = null;
			if (!"NONE".equals(authentificationType)) {
				auth = clzzAuthenticator.newInstance();
				clzzAuthenticator.getMethod("setPreemptiveAuthentication", boolean.class).invoke(auth, true);

				if (password != null) {
					clzzAuthenticator.getMethod("setPassword", String.class).invoke(auth, password);
				}
				if (username != null) {
					clzzAuthenticator.getMethod("setUsername", String.class).invoke(auth, username);
				}
				mthdSetProperty.invoke(serviceClientOptions, "_NTLM_DIGEST_BASIC_AUTHENTICATION_", auth);
			}

			if ("BASIC".equals(authentificationType)) {
				List<String> authSchema = new ArrayList<String>();
				authSchema.add("Basic");
				clzzAuthenticator.getMethod("setAuthSchemes", List.class).invoke(auth, authSchema);
			} else if ("NTLM".equals(authentificationType)) {
				List<String> authSchema = new ArrayList<String>();
				authSchema.add("NTLM");
				clzzAuthenticator.getMethod("setAuthSchemes", List.class).invoke(auth, authSchema);
			} else if ("DIGEST".equals(authentificationType)) {
				List<String> authSchema = new ArrayList<String>();
				authSchema.add("Digest");
				clzzAuthenticator.getMethod("setAuthSchemes", List.class).invoke(auth, authSchema);
			}

			// ENCODING
			if ("UTF-8".equals(encoding))
				mthdSetProperty.invoke(serviceClientOptions, "CHARACTER_SET_ENCODING", "UTF-8");
			else if ("UTF-16".equals(encoding))
				mthdSetProperty.invoke(serviceClientOptions, "CHARACTER_SET_ENCODING", "UTF-16");
			else if ("ISO-8859".equals(encoding))
				mthdSetProperty.invoke(serviceClientOptions, "CHARACTER_SET_ENCODING", "ISO-8859");
			else if ("ISO-8859-1".equals(encoding))
				mthdSetProperty.invoke(serviceClientOptions, "CHARACTER_SET_ENCODING", "ISO-8859-1");

			
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		
		return stub;
	}
	
}
