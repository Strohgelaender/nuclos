//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.List;

import org.nuclos.api.context.communication.CommunicationContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

@Configurable
public class CommunicationServiceExecuter {
	
	private static final Logger LOG = LoggerFactory.getLogger(CommunicationServiceExecuter.class);

	@Autowired
	private NuclosUserDetailsContextHolder userContext;
	
	@Autowired
	private SecurityFacadeLocal securityFacadeLocal;
	
	private Long sessionId;
	
	private final CommunicationContext context;
	
	public CommunicationServiceExecuter(CommunicationContext context) {
		this.context = context;
	}
	
	public void execute() throws BusinessException {
		
		loginAsSuperUser();
		userContext.createSavepoint();
		
		try {
			SpringApplicationContextHolder.getBean(EventSupportFacadeLocal.class).fireCommunicationEventSupport(context);
		} catch (NuclosBusinessRuleException e) {
			throw new BusinessException(e);
		} catch (NuclosCompileException e) {
			throw new RuntimeException(e);
		} finally {
			userContext.clear();
			logout();
		}
	}
	
	public void loginAsSuperUser() {
		String username = ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_TIMELIMIT_RULE_USER);
		Authentication auth = new NuclosLocalServerAuthenticationToken(username, "", getSuperUserAuthorities());
		SecurityContextHolder.getContext().setAuthentication(auth);
		sessionId = securityFacadeLocal.login().getSessionId();
	}
	
	private List<GrantedAuthority> getSuperUserAuthorities() {
		return CollectionUtils.transform(NuclosUserDetailsService.getSuperUserActions(), new Transformer<String, GrantedAuthority>() {
			@Override
			public GrantedAuthority transform(String i) {
				return new SimpleGrantedAuthority(i);
			}
		});
	}
	
	public void logout() {
		try {
			securityFacadeLocal.logout(sessionId);
			SecurityContextHolder.getContext().setAuthentication(null);
		} catch (BeanCreationException e) {
			final String s = e.toString();
			if (s.indexOf("scopedTarget.") >= 0) {
				LOG.debug("Logout failed: {}", s);
			} else {
				LOG.error("Logout failed: {}", e);
			}
		} catch (Exception e) {
			LOG.error("Logout failed: {}", e);
		}
	}
	
}
