//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * AuthenticationProvider for Nuclos.
 *
 * TODO add support for caching (ldap servers and users)
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class NuclosAuthenticationProvider implements AuthenticationProvider, MessageListener {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosAuthenticationProvider.class);
	
	public static final String ANONYMOUS_USER_NAME = "anonymous";

	private UserDetailsService userDetailsService;

	private List<NuclosLdapBindAuthenticator> ldapAuthenticators;

	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		if (authentication instanceof NuclosLocalServerAuthenticationToken) {
			LOG.debug("Local server authentication: ...");
			return authentication;
		}

		boolean authenticated = false;

		final String username = authentication.getPrincipal().toString();
		final String password = authentication.getCredentials().toString();
		final String authName = authentication.getName().toUpperCase();
		final boolean anonymousUserAccess = ServerParameterProvider.getInstance().isEnabled(ParameterProvider.KEY_ANONYMOUS_USER_ACCESS_ENABLED);
		
		SecurityCache securityCache = SecurityCache.getInstance();
		Authentication authCached = securityCache.getAuthentificationFromFatClientContext(username, password);
		if (authCached != null) {
			SecurityContextHolder.getContext().setAuthentication(authCached);
			return authCached;
		}
		
		UserDetails userDetails = userDetailsService.loadUserByUsername(authName);
	
		LOG.debug("NuclosAuthenticationProvider.authenticate({}), locale: {}",
		          userDetails.getUsername(), LocaleContextHolder.getLocale().toString());

		if (!userDetails.isAccountNonLocked()) {
			throw new LockedException("nuclos.security.authentication.locked");
		}

		if (!userDetails.isAccountNonExpired()) {
			throw new AccountExpiredException("nuclos.security.authentication.accountexpired");
		}

		try	{
			for (NuclosLdapBindAuthenticator authenticator : getLdapBindAuthenticators()) {
				try {
					if (authenticator.authenticate(authentication)) {
						authenticated = true;
						break;
					}
				}
				catch (BadCredentialsException ex) {
					// authentication not successful, continue
				}
				catch (Exception ex) {
					LOG.error("Unable to authenticate:", ex);
				}
			}
		}
		catch (Exception ex) {
			LOG.error("Configuration of ldap authenticators failed.", ex);
		}

		// if ldap authentication is not active or the user is a superuser, try authentication against db
		if ((getLdapBindAuthenticators() == null || getLdapBindAuthenticators().size() == 0) || SecurityCache.getInstance().isSuperUser(username)) {
			// fallback, if password was deleted in database
			if (StringUtils.isNullOrEmpty(userDetails.getPassword()) && StringUtils.isNullOrEmpty(password)) {
				authenticated = true;
			}

			final String sPasswordFromUser = StringUtils.encryptPw(username, password);
			if(sPasswordFromUser.equals(userDetails.getPassword()) || (username.equals(ANONYMOUS_USER_NAME) && anonymousUserAccess)) {
				authenticated = true;
			}

			// Allow user to change password if credential is expired
			if (authenticated && !userDetails.isCredentialsNonExpired()) {
				
				// Authenticate the user for ChangeOwnPassword only.
				final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				authorities.add(new SimpleGrantedAuthority("Login"));
				// authorities.add(new SimpleGrantedAuthority("ChangeOwnPassword"));
				final UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
						userDetails.getUsername(), userDetails.getPassword(), authorities);
				SecurityContextHolder.getContext().setAuthentication(auth);
				LOG.info("User {} gets authenticated only for ChangeOwnPassword: {}",
				         userDetails.getUsername(), auth.isAuthenticated());
				
				// This exception trigger the change password dialog on login.
				throw new CredentialsExpiredException("nuclos.security.authentication.credentialsexpired");
			}
		}


		final UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
				userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
		// ???
		auth.setDetails(userDetails);
		if (authenticated) {
			securityCache.setAuthentificationFromFatClientContext(username, password, auth);
		} else {
			securityCache.removeAuthentificationFromFatClientContext(username);
			throw new BadCredentialsException("invalid.login.exception");//"Benutzername/Kennwort ung\u00fcltig.");
		}

		SecurityContextHolder.getContext().setAuthentication(auth);
		LOG.info("User {} gets authenticated: {}",
		         userDetails.getUsername(), auth.isAuthenticated());
		return auth;
	}

	private List<NuclosLdapBindAuthenticator> getLdapBindAuthenticators() {
		if (ldapAuthenticators == null) {
			ldapAuthenticators = new ArrayList<>();

			final List<EntityObjectVO<UID>> servers = CollectionUtils.applyFilter(
					NucletDalProvider.getInstance().getEntityObjectProcessor(E.LDAPSERVER).getAll(), 
					new Predicate<EntityObjectVO<UID>>() {
				@Override
				public boolean evaluate(EntityObjectVO<UID> t) {
					final Boolean active = t.getFieldValue(E.LDAPSERVER.active);
					final String authfilter = t.getFieldValue(E.LDAPSERVER.userfilter);
					return active != null && active && !StringUtils.isNullOrEmpty(authfilter);
				}
			});

			ldapAuthenticators.addAll(CollectionUtils.transform(servers, new Transformer<EntityObjectVO<UID>, NuclosLdapBindAuthenticator>() {
				@Override
				public NuclosLdapBindAuthenticator transform(EntityObjectVO<UID> i) {
					String url = i.getFieldValue(E.LDAPSERVER.serverurl);
					String baseDN = i.getFieldValue(E.LDAPSERVER.serversearchcontext);
					String bindDN = i.getFieldValue(E.LDAPSERVER.binddn);
					String bindCredential = i.getFieldValue(E.LDAPSERVER.bindcredential);
					String userSearchFilter = i.getFieldValue(E.LDAPSERVER.userfilter);
					Integer scope = i.getFieldValue(E.LDAPSERVER.serversearchscope);
					return new NuclosLdapBindAuthenticator(url, baseDN, bindDN, bindCredential, userSearchFilter, scope);
				}
			}));
		}
		return ldapAuthenticators;
	}
	
	public boolean hasActiveLdapBindAuthenticator() {
		return !getLdapBindAuthenticators().isEmpty();
	}

	public void invalidateLdapAuthenticators() {
		ldapAuthenticators = null;
	}

	@Override
	public boolean supports(Class<? extends Object> clazz) {
		if (UsernamePasswordAuthenticationToken.class.isAssignableFrom(clazz)) {
			return true;
		} else if (clazz == NuclosLocalServerAuthenticationToken.class) {
			return true;
		}
		return false;
	}

	@Override
	public void onMessage(Message message) {
		if(message instanceof TextMessage) {
			try {
				String text = ((TextMessage) message).getText();
				if (StringUtils.isNullOrEmpty(text) || text.equals(E.LDAPSERVER.getEntityName())) {
					LOG.info("onMessage: Invalidate ldap servers.");
					this.ldapAuthenticators = null;
				}
			}
			catch(JMSException e) {
				LOG.error("onMessage failed: {}", e, e);
			}
		}
	}

}
