//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.web.activemq;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.Connection;
import org.apache.activemq.broker.TransportConnection;
import org.apache.activemq.broker.TransportConnector;
import org.apache.activemq.thread.TaskRunnerFactory;
import org.apache.activemq.transport.Transport;
import org.apache.activemq.transport.TransportAcceptListener;
import org.apache.activemq.transport.http.HttpSpringEmbeddedTunnelServlet;
import org.apache.activemq.transport.http.HttpTransportFactory;
import org.apache.activemq.transport.http.HttpTunnelServlet;
import org.apache.activemq.util.ServiceSupport;
import org.apache.activemq.xbean.XBeanBrokerService;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.activemq.NuclosHttpTransportFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * NuclosJMSBrokerTunnelServlet is a servlet for making the server JMS server broker (ActiveMQ)
 * accessible over http(s). This is needed for nuclos client to communicate with the server
 * via JMS (i.e. asynchronous communication).
 * <p>
 * This version prevents any dependencies to jetty.
 * </p><p>
 * TODO: There must be a extremely more convenient way to do this... (tp)
 * </p>
 * @author Thomas Pasch
 */
public class NuclosJMSBrokerTunnelServlet extends HttpSpringEmbeddedTunnelServlet {
	
	private static final Logger LOG = LoggerFactory.getLogger(NuclosJMSBrokerTunnelServlet.class);
	
	private static long lastRecentUserSessionCleanup;
	private static final int LAST_RECENT_USER_MAX_PING_INTERVAL = 30000;

	// contains a table of session id / timestamp for active sessions
	private static Hashtable<String, Long> recentUserSessions = new Hashtable<String, Long>(); 
	
	public static void invalidateRecentUserSession(String sessionId) {
		recentUserSessions.remove(sessionId);
	}
	
	/*
	 * <p>
	 * ActiveMQ HTTP seems to always need an jetty. See http://activemq.apache.org/http-and-https-transports-reference.html
	 * for details. Alternative transport are available as well, see http://activemq.apache.org/configuring-transports.html.
	 * Websocket http://activemq.apache.org/websockets.html would be fine, but can we use that with tomcat?
	 * </p>
	 * <p>
	 * Here are some links for using Servlets with Spring:
	 * <ul>
	 *   <li>http://andykayley.blogspot.com/2007/11/how-to-inject-spring-beans-into.html</li>
	 *   <li>http://javageek.org/2005/09/23/accessing_a_spring_bean_from_a_servlet.html</li>
	 *   <li>http://static.springsource.org/spring/docs/2.5.x/reference/mvc.html</li>
	 * </ul>
	 * </p>
	 */
	
	private NuclosHttpTransportServer transportConnector;
	
	public NuclosJMSBrokerTunnelServlet() {
	}
	
	/**
	 * Get the broker from our (existing) Spring context.
	 */
	@Override
	protected BrokerService createBroker() throws Exception {
		if (broker == null) {
			while (!SpringApplicationContextHolder.isSpringReady()) {
				Thread.sleep(500);
			}
			broker = (XBeanBrokerService) SpringApplicationContextHolder.getBean("broker");
		}
		return broker;
	}
	
	@Override
	public void init() throws ServletException {
		// -------------------------------------------------------------------------		
		// copied from org.apache.activemq.transport.http.HttpEmbeddedTunnelServlet (5.5.0)
        // lets initialize the ActiveMQ broker
        try {
            if (broker == null) {
                broker = createBroker();

                // Add the servlet connector
                String url = getConnectorURL();
                // HttpTransportFactory factory = new HttpTransportFactory();
                // transportConnector = (HttpTransportServer) factory.doBind(new URI(url));
                transportConnector = new NuclosHttpTransportServer(new URI(url), new HttpTransportFactory());
                broker.addConnector(transportConnector);

                String brokerURL = getServletContext().getInitParameter("org.apache.activemq.brokerURL");
                if (brokerURL != null) {
                    log("Listening for internal communication on: " + brokerURL);
                }
            }
            broker.start();
        } catch (Exception e) {
            throw new ServletException("Failed to start embedded broker: " + e, e);
        }
        // now lets register the listener
        TransportAcceptListener listener = transportConnector.getAcceptListener();
        getServletContext().setAttribute("transportChannelListener", listener);
		// end of copied from org.apache.activemq.transport.http.HttpEmbeddedTunnelServlet (5.5.0)
		// -------------------------------------------------------------------------		
        
		// -------------------------------------------------------------------------
        // fix to set required parameter of HttpTunnelServlet
        // listener is always null here...
        if (listener == null) {
        	listener = new TransportAcceptListener() {
				
				@Override
				public void onAcceptError(Exception error) {
					LOG.error("accept failed: {}", error, error);
				}
				
				@Override
				public void onAccept(final Transport transport) {
					try {
						// Starting the connection could block due to
						// wireformat negotiation, so start it in an async thread.
						Thread startThread = new Thread("NuclosJMSBrokerTunnelServlet: ActiveMQ Transport Initiator: " + transport.getRemoteAddress()) {
							@Override
							public void run() {
								try {
									Connection connection = createConnection(transport);
									connection.start();
								} 
								catch (Exception e) {
									ServiceSupport.dispose(transport);
									onAcceptError(e);
								}
							}
						};
						startThread.start();
					}
					catch (Exception error) {
						ServiceSupport.dispose(transport);
						onAcceptError(error);
					}
				}
        	};
            getServletContext().setAttribute("transportChannelListener", listener);
        }
        getServletContext().setAttribute("acceptListener", listener);
        final HttpTransportFactory htf = new NuclosHttpTransportFactory();
        getServletContext().setAttribute("transportFactory", htf);
		// -------------------------------------------------------------------------
        
		// -------------------------------------------------------------------------
        // copied from org.apache.activemq.transport.http.HttpTunnelServlet (5.5.0)
        //
        // The following is looked up in the servlet context:
        // context_name					type						variable				optional?
        // -------------------------------------------------------------------------------------------
        // acceptListener				TransportAcceptListener		listener				no
        // transportFactory				HttpTransportFactory		transportFactory		no
        // transportOptions				HashMap						transportOptions		yes
        // wireFormat					TextWireFormat				wireFormat				yes
        //
        /*
        this.listener = (TransportAcceptListener)getServletContext().getAttribute("acceptListener");
        if (this.listener == null) {
            throw new ServletException("No such attribute 'acceptListener' available in the ServletContext");
        }
        transportFactory = (HttpTransportFactory)getServletContext().getAttribute("transportFactory");
        if (transportFactory == null) {
            throw new ServletException("No such attribute 'transportFactory' available in the ServletContext");    
        }
        transportOptions = (HashMap)getServletContext().getAttribute("transportOptions");
        wireFormat = (TextWireFormat)getServletContext().getAttribute("wireFormat");
        if (wireFormat == null) {
            wireFormat = createWireFormat();
        }
         */
        // end of copied from org.apache.activemq.transport.http.HttpTunnelServlet (5.5.0)
		// -------------------------------------------------------------------------
        
        // set stuff from org.apache.activemq.transport.http.HttpTunnelServlet 
        // using reflection (to access private fields)
        setField("listener", (TransportAcceptListener)getServletContext().getAttribute("acceptListener"));
        setField("transportFactory", (HttpTransportFactory)getServletContext().getAttribute("transportFactory"));
        Map to = (HashMap)getServletContext().getAttribute("transportOptions");
        if (to == null) {
        	to = new HashMap();
        }
        setField("transportOptions", to);
        // setField("wireFormat", (TextWireFormat)getServletContext().getAttribute("wireFormat"));
        setField("wireFormat", createWireFormat());
	}
	
	/**
	 * remove entries from recentUserSessions if no JMS ping arrived in the last view seconds
	 */
	private static void cleanupRecentUserSessions() {
		if(System.currentTimeMillis() - lastRecentUserSessionCleanup < 1000)
			return;
		
		for(Iterator<Entry<String, Long>> esIt = recentUserSessions.entrySet().iterator();esIt.hasNext();) {
			Entry<String, Long> es = esIt.next();
			if(es.getValue()<System.currentTimeMillis() - LAST_RECENT_USER_MAX_PING_INTERVAL) { 
				esIt.remove();
			}
		}
		lastRecentUserSessionCleanup = System.currentTimeMillis();
	}
	
	public static int getNrOfRecentUserSessions() {
		cleanupRecentUserSessions();
		return recentUserSessions.size();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(recentUserSessions.containsKey(request.getSession().getId())) {
			recentUserSessions.remove(request.getSession().getId());
		}
		recentUserSessions.put(request.getSession().getId(), System.currentTimeMillis());
		super.doGet(request, response);
		logging(true, request);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);
		logging(false, request);
	}
	
	private void logging(boolean get, HttpServletRequest request) {
		if(LOG.isDebugEnabled()) {
			StringBuffer requstUrl = request.getRequestURL();
			if(get) {				
				LOG.debug("get request {}", requstUrl);
			} 
			else {
				LOG.debug("post request {}", requstUrl);
			}			
		}
	}
	
	private Connection createConnection(Transport transport) {
		final Broker b;
		try {
			b = broker.getBroker();
		}
		catch (Exception e) {
			throw new NuclosFatalException(e);
		}
		final TransportConnector con = new TransportConnector(transportConnector);
		con.setBrokerService(broker);
		// ActiveMQ 5.7 - but beware of http://support.nuclos.de/browse/NUCLOS-1565
		final TaskRunnerFactory trf = new TaskRunnerFactory();
		final TransportConnection result = new TransportConnection(con, transport, b, trf, trf);
		// ActiveMQ 5.6
		// final TransportConnection result = new TransportConnection(con, transport, b, null);
		result.getStatistics().setEnabled(true);
		return result;
	}
	
	private void setField(String name, Object value) throws ServletException {
		try {
			final Field field = HttpTunnelServlet.class.getDeclaredField(name);
			field.setAccessible(true);
			field.set(this, value);
		} 
		catch (SecurityException e) {
			throw new ServletException(e);
		} 
		catch (NoSuchFieldException e) {
			throw new ServletException(e);
		} 
		catch (IllegalArgumentException e) {
			throw new ServletException(e);
		} 
		catch (IllegalAccessException e) {
			throw new ServletException(e);
		}
	}
	
	/**
	 * Tidy up.
	 */
	@Override
	public void destroy() {
		super.destroy();
		try {
			transportConnector.stop();
		}
		catch (Exception e) {
			// ignore
		}
		transportConnector = null;
		
		if (broker != null) {
			try {
				broker.stop();
				broker.waitUntilStopped();
				((XBeanBrokerService) broker).destroy();
			}
			catch (Exception e) {
				// ignore
			}
		}
		broker = null;
		broker = null;
	}

	@Override
	protected String getConnectorURL() {
		return "http://localhost/" + "nuclos";
	}

	@Override
	public void service(ServletRequest req, ServletResponse res)
			throws ServletException, IOException {
		super.service(req, res);
	}

}
