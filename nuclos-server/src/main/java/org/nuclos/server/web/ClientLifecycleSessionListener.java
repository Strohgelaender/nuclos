//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.web;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.nuclos.server.spring.NuclosWebApplicationInitializer;
import org.nuclos.server.web.activemq.NuclosJMSBrokerTunnelServlet;

/**
 * A (servlet) session listener used to track client http session lifecycle.
 * <p>
 * As there is no spring support for session listeners, it runs outside of spring.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.0.10
 */
@WebListener
public class ClientLifecycleSessionListener implements HttpSessionListener {
	
	public ClientLifecycleSessionListener() {
	}

	@Override
	public void sessionCreated(HttpSessionEvent se) {
//		final ClientLifecycle clientLifecycle = ClientLifecycle.getInstance();
//		clientLifecycle.newSession(se.getSession());
		se.getSession().setMaxInactiveInterval(NuclosWebApplicationInitializer.DEFAULT_SESSION_TIMEOUT);
//		se.getSession().setMaxInactiveInterval(60);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
//		final ClientLifecycle clientLifecycle = ClientLifecycle.getInstance();
//		clientLifecycle.destroySession(se.getSession());
		NuclosJMSBrokerTunnelServlet.invalidateRecentUserSession(se.getSession().getId());
	}

}
