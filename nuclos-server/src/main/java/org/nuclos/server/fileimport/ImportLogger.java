//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.AppenderRef;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.nuclos.common.HashResourceBundle;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.fileimport.ejb3.ImportFacadeRemote;

/**
 * Utility class for logging in file imports.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class ImportLogger {

	private final Logger logger;
	private final File file;

	private final HashResourceBundle bundle;

	private int errorcount = 0;

	public int getErrorcount() {
		return errorcount;
	}
	
	public ImportLogger(File f, HashResourceBundle bundle) throws IOException {

		file = f;
		if (!file.exists()) {
			file.createNewFile();
		}

		LoggerContext context= (LoggerContext) LogManager.getContext();
        Configuration config= context.getConfiguration();

        PatternLayout layout= PatternLayout.createLayout("%m%n", null, null, null, Charset.defaultCharset(),false,false,null,null);
        Appender appender=ConsoleAppender.createAppender(layout, null, null, "CONSOLE_APPENDER", null, null);
        // appender = FileAppender.createAppender(file.getAbsolutePath(), null, null, file.getName(), "true", null, null, null, layout, null, null, null, null); // does not work
        
        appender.start();
        AppenderRef ref= AppenderRef.createAppenderRef("FILE_APPENDER",null,null);
        AppenderRef[] refs = new AppenderRef[] {ref};
        LoggerConfig loggerConfig= LoggerConfig.createLogger("false", Level.INFO,"FILE_LOGGER","ImportLogger",refs,null,config,null);
        loggerConfig.addAppender(appender,null,null);

        config.addAppender(appender);
        config.addLogger("ImportLogger", loggerConfig);
        context.updateLoggers(config);

        logger = (Logger) LogManager.getContext().getLogger("ImportLogger");
		
		this.bundle = bundle;
	}
	
	public File getFile() {
		return file;
	}

	
	private void writeToFile(String message) {
		writeToFile(message);
	}
	private void writeToFile(String message, boolean isError) {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
		    out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + (isError ? "ERROR:" : "") + " - " + message);
		    out.flush();
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String info(String message) {
		String result = localize(message);
		writeToFile(result);
		logger.info(result);
		return result;
	}

	public void error(String message) {
		errorcount++;
		writeToFile(localize(message), true);
		logger.error(localize(message));
	}

	public void error(String message, Throwable ex) {
		errorcount++;
		writeToFile(localize(message, ex), true);
		logger.error(localize(message, ex), ex);
	}

	public void error(int line, String message) {
		errorcount++;
		writeToFile(localize(StringUtils.getParameterizedExceptionMessage("import.logging.line", line, localize(message))), true);
		logger.error(localize(StringUtils.getParameterizedExceptionMessage("import.logging.line", line, localize(message))));
	}

	public void error(int line, String message, Throwable ex) {
		errorcount++;
		writeToFile(localize(StringUtils.getParameterizedExceptionMessage("import.logging.line", line, localize(message, ex))), true);
		logger.error(localize(StringUtils.getParameterizedExceptionMessage("import.logging.line", line, localize(message, ex))), ex);
	}

	protected String localize(String message, Throwable ex) {
		return localize(message) + "; " + localizeException(ex);
	}

	private String localizeException(Throwable ex) {
		StringBuilder result = new StringBuilder();
		if (ex.getLocalizedMessage() != null) {
			result.append(localize(ex.getLocalizedMessage()));
		}
		else if (ex.getMessage() != null) {
			result.append(localize(ex.getMessage()));
		}
		else {
			result.append(ex.toString());
		}
		return result.toString();
	}

	public String localize(String message) {
		return ImportFacadeRemote.localize(message, bundle);
	}
}
