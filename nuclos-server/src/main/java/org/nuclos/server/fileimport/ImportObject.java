//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;

/**
 * Value object for an imported object.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class ImportObject {

	private UID entityUid;
	private ImportObjectKey key;
	private Map<UID, Object> attributes;
	private Map<UID, ImportObject> references;
	private EntityObjectVO<?> valueObject;

	private int lineNumber;

	public ImportObject(final UID entityUid, final ImportObjectKey key, final Map<UID, Object> attributes, int lineNumber) {
		this.entityUid = entityUid;
		this.key = key;
		this.attributes = attributes;
		this.lineNumber = lineNumber;
	}

	public ImportObject(final UID entityUid, final ImportObjectKey key, final Map<UID, Object> attributes, int lineNumber, final Map<UID, ImportObject> references) {
		this(entityUid, key, attributes, lineNumber);
		this.references = references;
	}

	public UID getEntityUID() {
		return this.entityUid;
	}

	public ImportObjectKey getKey() {
		return this.key;
	}

	public Map<UID, Object> getAttributes() {
		return this.attributes;
	}

	public void setAttributes(final Map<UID, Object> attributes) {
		this.attributes = attributes;
	}

	public Map<UID, ImportObject> getReferences() {
		return this.references;
	}

	public void setValueObject(EntityObjectVO<?> valueObject) {
		this.valueObject = valueObject;
	}

	public EntityObjectVO getValueObject() {
		return valueObject;
	}

	public int getLineNumber() {
		return this.lineNumber;
	}

	public boolean isEmpty() {
		for (Object o : attributes.values()) {
			if (o != null) {
				return false;
			}
		}
		if (references != null) {
			for (ImportObject o : references.values()) {
				if (o != null && !o.isEmpty()) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return (key.toString()).replace("{", "").replace("}", "");
	}

	@Override
	public int hashCode() {
		return key.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ImportObject) {
			return key.equals(((ImportObject)obj).key);
		}
		return false;
	}

}
