//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIsNullCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.csvparser.CSVParser;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.entityobject.MakeEntityObjectValueIdField;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Utility class for various static import helper operations.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class ImportUtils {

	private ImportUtils() {
	}

	public static void validateCsvImportStructure(final MasterDataVO<UID> importstructure)
			throws CommonValidationException {
		final UID entityUid = importstructure.getFieldUid(E.IMPORT.entity);
		if (entityUid == null) {
			throw new CommonValidationException("import.validation.structure.entity.mandatory");
		}

		if (importstructure.getFieldValue(E.IMPORT.mode) == null) {
			throw new CommonValidationException("import.validation.structure.mode.mandatory");
		}
		final ImportMode mode = KeyEnum.Utils.findEnum(ImportMode.class, importstructure.getFieldValue(E.IMPORT.mode));

		for (EntityObjectVO<?> attribute : importstructure.getDependents().getData(E.IMPORTATTRIBUTE.importfield)) {
			if (attribute.getFieldUid(E.IMPORTATTRIBUTE.attribute) == null || attribute.isFlagRemoved()) {
				continue;
			}

			final UID attributeUid = attribute.getFieldUid(E.IMPORTATTRIBUTE.attribute);
			if (attributeUid.equals(SF.STATE.getUID(entityUid)) && mode.equals(ImportMode.NUCLOSIMPORT)) {
				throw new CommonValidationException("import.validation.structure.statenotallowed");
			}

			boolean hasScript = attribute.getFieldValue(E.IMPORTATTRIBUTE.script) != null;
			boolean hasColumn = attribute.getFieldValue(E.IMPORTATTRIBUTE.fieldcolumn) != null;

			if (hasScript == hasColumn) {
				throw new CommonValidationException("import.validation.structure.column.or.script");
			}

			final UID foreignentityUid = MetaProvider.getInstance().getEntityField(attributeUid).getForeignEntity();
			if (foreignentityUid != null) {
				for (final EntityObjectVO<?> fei : attribute.getDependents().getData(E.IMPORTFEIDENTIFIER.importattribute)) {
					if (fei.getFieldUid(E.IMPORTFEIDENTIFIER.attribute) == null || fei.isFlagRemoved()) {
						continue;
					}

					boolean exists = false;
					for (FieldMeta<?> foreignfield : MetaProvider.getInstance()
							.getAllEntityFieldsByEntity(foreignentityUid).values()) {
						if (foreignfield.getUID().equals(fei.getFieldUid(E.IMPORTFEIDENTIFIER.attribute))) {
							exists = true;
						}
					}
					if (!exists) {
						throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
								"import.validation.structure.foreignfielddoesnotexist",
								fei.getFieldUid(E.IMPORTFEIDENTIFIER.attribute), foreignentityUid, attributeUid));
					}
				}
			}
		}

		int count = 0;
		for (final EntityObjectVO<?> identifier : importstructure.getDependents().getData(E.IMPORTIDENTIFIER.importfield)) {
			if (identifier.isFlagRemoved()) {
				continue;
			}
			boolean isImported = false;
			UID field = null;

			if (identifier.getFieldUid(E.IMPORTIDENTIFIER.attribute) == null) {
				continue;
			}
			else {
				field = identifier.getFieldUid(E.IMPORTIDENTIFIER.attribute);
			}
			count++;

			for (EntityObjectVO<?> attribute : importstructure.getDependents().getData(E.IMPORTATTRIBUTE.importfield)) {
				if (attribute.isFlagRemoved() || attribute.getFieldUid(E.IMPORTATTRIBUTE.attribute) == null) {
					continue;
				}
				else if (field.equals(attribute.getFieldUid(E.IMPORTATTRIBUTE.attribute))) {
					isImported = true;
				}
			}

			if (!isImported) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
						"import.validation.structure.identifiernotimported", field));
			}
		}
		boolean updateOrDelete = (importstructure.getFieldValue(E.IMPORT.update) == null ? false : importstructure
				.getFieldValue(E.IMPORT.update))
				|| (importstructure.getFieldValue(E.IMPORT.delete) == null ? false : importstructure
						.getFieldValue(E.IMPORT.delete));
		if (updateOrDelete && count == 0) {
			throw new CommonValidationException("import.validation.structure.identifierrequired");
		}
	}

	public static void validateCsvFileImport(final MasterDataVO<UID> fileimport) throws CommonValidationException {
		if (fileimport.getFieldValue(E.IMPORTFILE.mode) == null) {
			throw new CommonValidationException("import.validation.importfile.mode");
		}
		ImportMode mode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class,
				(String) fileimport.getFieldValue(E.IMPORTFILE.mode));

		final GenericObjectDocumentFile file = (GenericObjectDocumentFile) fileimport.getFieldValue(E.IMPORTFILE.documentfile.getUID());
		if (file == null) {
			throw new CommonValidationException("import.validation.importfile.filemissing");
		}
		if (!file.getFilename().toUpperCase().endsWith(".CSV")) {
			throw new CommonValidationException("import.validation.importfile.csv");
		}

		List<CsvImportStructure> importDefinitions = new ArrayList<CsvImportStructure>();
		for (final EntityObjectVO<?> usage : fileimport.getDependents().getData(E.IMPORTUSAGE.importfile)) {
			if (usage.isFlagRemoved() || usage.getFieldUid(E.IMPORTUSAGE.importfield) == null) {
				continue;
			}

			final UID importstructureUid = usage.getFieldUid(E.IMPORTUSAGE.importfield);
			assert importstructureUid != null;

			MasterDataFacadeLocal mdFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
			MasterDataVO<UID> importstructure = null;
			try {
				importstructure = mdFacade.getWithDependants(E.IMPORT.getUID(), importstructureUid, null);
				importDefinitions.add(CsvImportStructure.newCsvImportStructure(importstructureUid));
			}
			catch (Exception e) {
				throw new NuclosFatalException(e);
			}

			try {
				ImportUtils.validateCsvImportStructure(importstructure);
			}
			catch (CommonValidationException ex) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
						"import.validation.fileimport.invalidstructure", importstructure.getFieldValue(E.IMPORT.name)),
						ex);
			}

			ImportMode strucutremode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class,
					importstructure.getFieldValue(E.IMPORT.mode));

			if (!mode.equals(strucutremode)) {
				throw new CommonValidationException(
						StringUtils.getParameterizedExceptionMessage("import.validation.fileimport.structuremodewrong",
								importstructure.getFieldValue(E.IMPORT.name)));
			}
		}

		final Map<UID, Integer> importSettings = new HashMap<UID, Integer>();
		final Map<UID, Set<UID>> keyDefinitions = new HashMap<UID, Set<UID>>();

		for (CsvImportStructure is : importDefinitions) {
			if (!importSettings.containsKey(is.getEntityUid())) {
				importSettings.put(is.getEntityUid(), is.getImportSettings());
			}
			else if ((importSettings.get(is.getEntityUid()) & is.getImportSettings()) != is.getImportSettings()) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
						"import.validation.importfile.differentsettings", is.getEntityUid()));
			}

			if (!keyDefinitions.containsKey(is.getEntityUid())) {
				keyDefinitions.put(is.getEntityUid(), is.getIdentifiers());
			}
			else if (!keyDefinitions.get(is.getEntityUid()).equals(is.getIdentifiers())) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
						"import.validation.importfile.differentkeydefinitions", is.getEntityUid()));
			}

			for (final Item item : is.getItems().values()) {
				if (item.isReferencing()) {
					final Set<UID> foreignkey = new HashSet<UID>();

					for (ForeignEntityIdentifier fei : item.getForeignEntityIdentifiers()) {
						foreignkey.add(fei.getFieldUId());
					}

					if (!keyDefinitions.containsKey(item.getForeignEntityUID())) {
						keyDefinitions.put(item.getForeignEntityUID(), foreignkey);
					}
					else if (!keyDefinitions.get(item.getForeignEntityUID()).equals(foreignkey)) {
						throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
								"import.validation.importfile.differentkeydefinitions", item.getForeignEntityUID()));
					}
				}
			}
		}
	}

	public static void validateXmlImportStructure(final MasterDataVO<UID> importstructure)
			throws CommonValidationException {
		final UID entityUid = importstructure.getFieldUid(E.XML_IMPORT.entity);
		if (entityUid == null) {
			throw new CommonValidationException("import.validation.structure.entity.mandatory");
		}

		if (importstructure.getFieldValue(E.XML_IMPORT.mode) == null) {
			throw new CommonValidationException("import.validation.structure.mode.mandatory");
		}
		final ImportMode mode = KeyEnum.Utils.findEnum(ImportMode.class, importstructure.getFieldValue(E.IMPORT.mode));

		for (EntityObjectVO<?> attribute : importstructure.getDependents().getData(E.XMLIMPORTATTRIBUTE.importfield)) {
			if (attribute.getFieldUid(E.XMLIMPORTATTRIBUTE.attribute) == null || attribute.isFlagRemoved()) {
				continue;
			}

			final UID attributeUid = attribute.getFieldUid(E.XMLIMPORTATTRIBUTE.attribute);
			if (attributeUid.equals(SF.STATE.getUID(entityUid)) && mode.equals(ImportMode.NUCLOSIMPORT)) {
				throw new CommonValidationException("import.validation.structure.statenotallowed");
			}
			/*
			boolean hasScript = attribute.getFieldValue(E.XMLIMPORTATTRIBUTE.script) != null;
			boolean hasColumn = attribute.getFieldValue(E.XMLIMPORTATTRIBUTE.match) != null;
			if (hasScript == hasColumn) {
				throw new CommonValidationException("import.validation.structure.column.or.script");
			}
			 */

			final UID foreignentityUid = MetaProvider.getInstance().getEntityField(attributeUid).getForeignEntity();
			if (foreignentityUid != null) {
				for (final EntityObjectVO<?> fei : attribute.getDependents().getData(E.XMLIMPORTFEIDENTIFIER.importattribute)) {
					if (fei.getFieldUid(E.XMLIMPORTFEIDENTIFIER.attribute) == null || fei.isFlagRemoved()) {
						continue;
					}

					boolean exists = false;
					for (FieldMeta<?> foreignfield : MetaProvider.getInstance()
							.getAllEntityFieldsByEntity(foreignentityUid).values()) {
						if (foreignfield.getUID().equals(fei.getFieldUid(E.XMLIMPORTFEIDENTIFIER.attribute))) {
							exists = true;
						}
					}
					if (!exists) {
						throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
								"import.validation.structure.foreignfielddoesnotexist",
								fei.getFieldUid(E.XMLIMPORTFEIDENTIFIER.attribute), foreignentityUid, attributeUid));
					}
				}
			}
		}

		int count = 0;
		for (final EntityObjectVO<?> identifier : importstructure.getDependents().getData(E.IMPORTIDENTIFIER.xmlimportfield)) {
			if (identifier.isFlagRemoved()) {
				continue;
			}
			boolean isImported = false;
			UID field = null;

			if (identifier.getFieldUid(E.IMPORTIDENTIFIER.attribute) == null) {
				continue;
			}
			else {
				field = identifier.getFieldUid(E.IMPORTIDENTIFIER.attribute);
			}
			count++;

			for (EntityObjectVO<?> attribute : importstructure.getDependents().getData(E.XMLIMPORTATTRIBUTE.importfield)) {
				if (attribute.isFlagRemoved() || attribute.getFieldUid(E.XMLIMPORTATTRIBUTE.attribute) == null) {
					continue;
				}
				else if (field.equals(attribute.getFieldUid(E.XMLIMPORTATTRIBUTE.attribute))) {
					isImported = true;
				}
			}

			if (!isImported) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
						"import.validation.structure.identifiernotimported", field));
			}
		}
		boolean updateOrDelete = (importstructure.getFieldValue(E.XML_IMPORT.update) == null ? false : importstructure
				.getFieldValue(E.XML_IMPORT.update))
				|| (importstructure.getFieldValue(E.XML_IMPORT.delete) == null ? false : importstructure
						.getFieldValue(E.XML_IMPORT.delete));
		if (updateOrDelete && count == 0) {
			throw new CommonValidationException("import.validation.structure.identifierrequired");
		}
	}

	public static void validateXmlFileImport(final MasterDataVO<UID> fileimport) throws CommonValidationException {
		if (fileimport.getFieldValue(E.XMLIMPORTFILE.mode) == null) {
			throw new CommonValidationException("import.validation.importfile.mode");
		}
		ImportMode mode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class,
				(String) fileimport.getFieldValue(E.XMLIMPORTFILE.mode));

		final GenericObjectDocumentFile file = (GenericObjectDocumentFile) fileimport
				.getFieldValue(E.XMLIMPORTFILE.documentfile.getUID());
		if (file == null) {
			throw new CommonValidationException("import.validation.importfile.filemissing");
		}
		if (!file.getFilename().toUpperCase().endsWith(".XML")) {
			throw new CommonValidationException("import.validation.importfile.xml");
		}

		List<XmlImportStructure> importDefinitions = new ArrayList<XmlImportStructure>();
		for (final EntityObjectVO<?> usage : fileimport.getDependents().getData(E.IMPORTUSAGE.xmlimportfile)) {
			if (usage.isFlagRemoved() || usage.getFieldUid(E.IMPORTUSAGE.xmlimportfield) == null) {
				continue;
			}

			final UID importstructureUid = usage.getFieldUid(E.IMPORTUSAGE.xmlimportfield);
			assert importstructureUid != null;

			MasterDataFacadeLocal mdFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
			MasterDataVO<UID> importstructure = null;
			try {
				importstructure = mdFacade.getWithDependants(E.XML_IMPORT.getUID(), importstructureUid, null);
				importDefinitions.add(XmlImportStructure.newXmlImportStructure(importstructureUid));
			}
			catch (Exception e) {
				throw new NuclosFatalException(e);
			}

			try {
				ImportUtils.validateXmlImportStructure(importstructure);
			}
			catch (CommonValidationException ex) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
						"import.validation.fileimport.invalidstructure", importstructure.getFieldValue(E.XML_IMPORT.name)),
						ex);
			}

			ImportMode strucutremode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class,
					importstructure.getFieldValue(E.XML_IMPORT.mode));

			if (!mode.equals(strucutremode)) {
				throw new CommonValidationException(
						StringUtils.getParameterizedExceptionMessage("import.validation.fileimport.structuremodewrong",
								importstructure.getFieldValue(E.XML_IMPORT.name)));
			}
		}

		final Map<UID, Integer> importSettings = new HashMap<UID, Integer>();
		final Map<UID, Set<UID>> keyDefinitions = new HashMap<UID, Set<UID>>();

		for (XmlImportStructure is : importDefinitions) {
			if (!importSettings.containsKey(is.getEntityUid())) {
				importSettings.put(is.getEntityUid(), is.getImportSettings());
			}
			else if ((importSettings.get(is.getEntityUid()) & is.getImportSettings()) != is.getImportSettings()) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
						"import.validation.importfile.differentsettings", is.getEntityUid()));
			}

			if (!keyDefinitions.containsKey(is.getEntityUid())) {
				keyDefinitions.put(is.getEntityUid(), is.getIdentifiers());
			}
			else if (!keyDefinitions.get(is.getEntityUid()).equals(is.getIdentifiers())) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
						"import.validation.importfile.differentkeydefinitions", is.getEntityUid()));
			}

			for (final Item item : is.getItems().values()) {
				if (item.isReferencing()) {
					final Set<UID> foreignkey = new HashSet<UID>();

					for (ForeignEntityIdentifier fei : item.getForeignEntityIdentifiers()) {
						foreignkey.add(fei.getFieldUId());
					}

					if (!keyDefinitions.containsKey(item.getForeignEntityUID())) {
						keyDefinitions.put(item.getForeignEntityUID(), foreignkey);
					}
					else if (!keyDefinitions.get(item.getForeignEntityUID()).equals(foreignkey)) {
						throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(
								"import.validation.importfile.differentkeydefinitions", item.getForeignEntityUID()));
					}
				}
			}
		}
	}

	public static int skipHeaderLines(CSVParser parser, int headerlines) throws IOException {
		for (int iLine = 0; iLine < headerlines; iLine++) {
			parser.getLine();
		}
		return headerlines;
	}

	public static int countLines(GenericObjectDocumentFile importfile, String delimiter) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(
				importfile.getContents())));

		try {
			// we have to use the csv parser to respect quotes and so on...
			CSVParser parser;
			if (!StringUtils.looksEmpty(delimiter)) {
				parser = new CSVParser(reader, delimiter.charAt(0));
			}
			else {
				parser = new CSVParser(reader);
			}

			int result = 0;
			while (parser.getLine() != null) {
				result++;
			}
			return result;
		}
		finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	public static CollectableSearchCondition getSearchCondition(final UID entityUid,
			final Map<UID, Set<UID>> definitions, final ImportObject io) throws NuclosBusinessException {
		if (definitions == null || !definitions.containsKey(entityUid) || definitions.get(entityUid) == null
				|| definitions.get(entityUid).size() == 0) {
			throw new NuclosBusinessException("import.utils.exception.1");//"Keine Schl\u00fcsseldefinition vorhanden.");
		}

		List<CollectableSearchCondition> conditions = new ArrayList<CollectableSearchCondition>();

		boolean isModule = Modules.getInstance().isModule(entityUid);

		for (final UID attributeUid : definitions.get(entityUid)) {
			CollectableEntityField cef;

			if (isModule) {
				FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(attributeUid);
				Map<UID, Permission> mpPermissions = null;
				if (fMeta.isCalcAttributeAllowCustomization()) {
					mpPermissions = AttributeCache.getInstance().getAttribute(LangUtils.defaultIfNull(fMeta.getCalcBaseFieldUID(), fMeta.getUID())).getPermissions();
				} else {
					mpPermissions = AttributeCache.getInstance().getAttribute(attributeUid).getPermissions();
				}
				cef = new CollectableGenericObjectEntityField(mpPermissions, fMeta, entityUid);
			}
			else {

				cef = (new CollectableMasterDataEntity(MetaProvider.getInstance().getEntity(entityUid)))
						.getEntityField(attributeUid);
			}

			CollectableSearchCondition cond;
			if (cef.isReferencing()) {
				if (io.getReferences() != null) {
					if (io.getReferences().get(attributeUid) != null) {
						if (io.getReferences().get(attributeUid).getValueObject() != null
								&& io.getReferences().get(attributeUid).getValueObject().getPrimaryKey() != null) {
							Object referencedId = io.getReferences().get(attributeUid).getValueObject().getPrimaryKey();

							cond = new CollectableComparison(cef, ComparisonOperator.EQUAL,
									new CollectableValueIdField(referencedId, null));
						}
						else if (io.getAttributes().get(attributeUid) != null) {
							cond = new CollectableComparison(cef, ComparisonOperator.EQUAL,
									new CollectableValueIdField(io.getAttributes().get(attributeUid), ""));
						}
						else {
							cond = new CollectableIdCondition(0);
						}
					}
					else {
						cond = new CollectableIsNullCondition(cef);
					}
				}
				else if (io.getAttributes().get(attributeUid) != null) {
					cond = new CollectableComparison(cef, ComparisonOperator.EQUAL, new CollectableValueField(io
							.getAttributes().get(attributeUid)));
				}
				else {
					cond = new CollectableIsNullCondition(cef);
				}
			}
			else {
				if (io.getAttributes().get(attributeUid) != null) {
					cond = new CollectableComparison(cef, ComparisonOperator.EQUAL, new CollectableValueField(io
							.getAttributes().get(attributeUid)));
				}
				else {
					cond = new CollectableIsNullCondition(cef);
				}
			}
			conditions.add(cond);
		}

		if (conditions.size() > 1) {
			CompositeCollectableSearchCondition composite = new CompositeCollectableSearchCondition(LogicalOperator.AND);
			composite.addAllOperands(conditions);
			return composite;
		}
		else {
			return conditions.get(0);
		}

	}

	public static EntityObjectVO<UID> getNewObject(final UID entityUid) {
		final EntityObjectVO<UID> result = new EntityObjectVO<UID>(entityUid);
		// result.initFields(1, 1);
		// result.setEntity(entityUid);
		result.setFieldValue(SF.LOGICALDELETED.getUID(entityUid), Boolean.FALSE);
		return result;
	}

	public static List<Long> getObjectIdsByEntity(final UID entityUid) {
		if (Modules.getInstance().isModule(entityUid)) {
			GenericObjectFacadeLocal goFacade = ServerServiceLocator.getInstance().getFacade(
					GenericObjectFacadeLocal.class);
			return goFacade.getGenericObjectIdsNoCheck(entityUid, CollectableSearchExpression.TRUE_SEARCH_EXPR);
			/*
				//TODO MULTINUCLET
			return CollectionUtils.transform(goFacade.getGenericObjectIds(Modules.getInstance().getModuleIdByEntityName(entityUid), (CollectableSearchCondition)null), new Transformer<Integer, Long>() {
				@Override
				public Long transform(Integer i) {
					return IdUtils.toLongId(i);
				}
			});
			*/

		}
		else {
			//TODO MULTINUCLET
			MasterDataFacadeLocal mdFacade = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
			return CollectionUtils.transform(mdFacade.getMasterDataIds(entityUid), new Transformer<Object, Long>() {
				@Override
				public Long transform(Object i) {
					return (Long) i;
				}
			});
		}
	}

	public static <PK> PK getReferenceValue(String stringifiedForeignentityFieldDefinition, EntityObjectVO<PK> vo) {
		MakeEntityObjectValueIdField<PK> transformer;
		if (!StringUtils.isNullOrEmpty(stringifiedForeignentityFieldDefinition)) {
			transformer = new MakeEntityObjectValueIdField<PK>(stringifiedForeignentityFieldDefinition);
		}
		else {
			transformer = new MakeEntityObjectValueIdField<PK>();
		}
		return (PK) transformer.transform(vo).getValue();
	}
}
