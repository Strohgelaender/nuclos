//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport.ejb3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import javax.annotation.security.RolesAllowed;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.objectimport.ImportResult;
import org.nuclos.api.objectimport.ImportStructureDefinition;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.HashResourceBundle;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.ejb3.LocaleFacadeBean;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.fileimport.CsvImportJob;
import org.nuclos.server.fileimport.CsvImportStructure;
import org.nuclos.server.fileimport.Import;
import org.nuclos.server.fileimport.ImportContext;
import org.nuclos.server.fileimport.ImportFactory;
import org.nuclos.server.fileimport.ImportLogger;
import org.nuclos.server.fileimport.ImportStatus;
import org.nuclos.server.fileimport.ImportUtils;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.report.NuclosQuartzJob;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.NoTransactionException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * Facade for managing and executing imports.
 * Takes care of executing an import with respect to its transaction settings.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@RolesAllowed("Login")
@Transactional(noRollbackFor = { Exception.class })
@Qualifier("CsvImportService")
public class CsvImportFacadeBean extends AbstractImportFacadeBean implements CsvImportFacadeLocal, CsvImportFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(CsvImportFacadeBean.class);

	// Spring injection

	@Autowired
	private CsvImportFacadeLocal importFacade;

	// end of Spring injection

	public CsvImportFacadeBean() {
	}

	@Override
	EntityMeta<UID> getImportStructureEntityMeta() {
		return E.IMPORT;
	}
	
	@Override
	EntityMeta<UID> getImportFileEntityMeta() {
		return E.IMPORTFILE;
	}

	@Transactional(propagation = Propagation.SUPPORTS, noRollbackFor = { Exception.class })
	public ImportResult doImport(NuclosFile file, boolean isAtomic,
			Class<? extends ImportStructureDefinition>... structureDefClasses) throws BusinessException {

		ImportResult retVal = null;
		ImportMode mode = null;

		GenericObjectDocumentFile goFile = new GenericObjectDocumentFile(file.getName(), file.getContent());
		LocaleInfo info = localeFacade.getDefaultLocale();
		HashResourceBundle bundle = localeFacade.getResourceBundle(info);

		List<CsvImportStructure> structures = new ArrayList<CsvImportStructure>();

		for (Class<? extends ImportStructureDefinition> defClass : structureDefClasses) {
			try {
				CsvImportStructure istr = CsvImportStructure.newCsvImportStructure((UID) defClass.newInstance().getId());

				if (mode == null) {
					if (istr.getMode() != null)
						mode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class, istr.getMode());
				}
				else {
					if (istr.getMode() != null) {
						if (!mode.equals(org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class, istr.getMode())))
							throw new BusinessException(
									"ImportStructureDefinitions contain different import modes. " +
									"Do not mix 'Direct import' and 'Standardimport'. " +
									"This feature is not implemented yet.");
					}
				}
				structures.add(istr);
			}
			catch (Exception e) {
				throw new BusinessException(e);
			}
		}
		java.io.File fileLogger = new java.io.File(
				NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH),
				"ruleImport." + file.getName().substring(0, file.getName().lastIndexOf(".")) + ".log");

		// create import instance
		try {
			ImportLogger logger = new ImportLogger(fileLogger, bundle);

			Import instance = ImportFactory.newCsvImport(
					mode, goFile, null, structures, logger, isAtomic);

			ImportStatus result;
			if (isAtomic) {
				result = doAtomicImport(instance);
			}
			else {
				result = doNonAtomicImport(instance);
			}

			final org.nuclos.common.fileimport.ImportResult ir = result.getImportResult();
			final boolean isSuccessful = ir.getValue() != null ? ir.getValue().equals(org.nuclos.common.fileimport.ImportResult.OK) ? true
					: false
					: false;
			org.nuclos.common.NuclosFile logNuclosFile =
					new org.nuclos.common.NuclosFile(file.getName().substring(0, file.getName().lastIndexOf("."))
							+ ".log", IOUtils.readFromBinaryFile(logger.getFile()));

			retVal = new ImportResult(!isSuccessful, result.getMessage(), logNuclosFile);
		}
		catch (NuclosFileImportException e) {
			throw new BusinessException(e);
		}
		catch (IOException e) {
			throw new BusinessException(e);
		}
		return retVal;
	}

	@Transactional(propagation = Propagation.SUPPORTS, noRollbackFor = { Exception.class })
	public void doImport(ImportContext context) throws NuclosFileImportException {
		MasterDataVO<UID> importfilevo;
		try {
			importfilevo = masterDataFacade.getWithDependants(E.IMPORTFILE.getUID(), context.getImportfileId(), null);
		}
		catch (Exception e) {
			throw new NuclosFatalException(e);
		}

		GenericObjectDocumentFile file = (GenericObjectDocumentFile) importfilevo.getFieldValue(E.IMPORTFILE.documentfile.getUID());

		// initialize import logger
		ImportLogger logger;
		try {
			UID logFileUID = importfilevo.getFieldUid(E.IMPORTFILE.logfile);
			if (logFileUID == null) {
				logFileUID = DocumentFileBase.newFileUID();
			}
			GenericObjectDocumentFile logFile = new GenericObjectDocumentFile(file.getFilename().substring(0,
					file.getFilename().lastIndexOf("."))
					+ ".log", logFileUID, new byte[] {});
			importfilevo.setFieldUid(E.IMPORTFILE.logfile, logFileUID);
			importfilevo.setFieldValue(E.IMPORTFILE.logfile.getUID(), logFile);
			masterDataFacade.modify(importfilevo, null);

			// get path for log file
			java.io.File f = new java.io.File(
					NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH),
					logFileUID.getString() + ".log");

			// get translations
			LocaleInfo info = null;
			for (LocaleInfo i: localeFacade.getAllLocales(true)) {
				if (i.getLocale().equals(context.getLocaleId())) {
					info = i;
				}
			}
			if (info == null) {
				info = localeFacade.getDefaultLocale();
			}
			HashResourceBundle bundle = localeFacade.getResourceBundle(info);

			logger = new ImportLogger(f, bundle);
		}
		catch (Exception ex) {
			throw new NuclosFatalException();
		}

		// prepare import structures		
		List<EntityObjectVO<UID>> order = CollectionUtils.sorted(importfilevo.getDependents().<UID>getDataPk(E.IMPORTUSAGE.importfile),
				new Comparator<EntityObjectVO<UID>>() {
					@Override
					public int compare(EntityObjectVO<UID> o1, EntityObjectVO<UID> o2) {
						Integer index1 = o1.getFieldValue(E.IMPORTUSAGE.order.getUID(), Integer.class);
						Integer index2 = o2.getFieldValue(E.IMPORTUSAGE.order.getUID(), Integer.class);
						return index1.compareTo(index2);
					}
				});

		List<CsvImportStructure> structures = new ArrayList<CsvImportStructure>();
		for (EntityObjectVO<UID> importusage : order) {
			final UID iu = importusage.getFieldUid(E.IMPORTUSAGE.importfield);
			assert iu != null;
			structures.add(CsvImportStructure.newCsvImportStructure(iu));
		}

		ImportMode mode = org.nuclos.common2.KeyEnum.Utils.findEnum(ImportMode.class,
				(String) importfilevo.getFieldValue(E.IMPORTFILE.mode));

		boolean atomic = importfilevo.getFieldValue(E.IMPORTFILE.atomic) == null ? false : importfilevo
				.getFieldValue(E.IMPORTFILE.atomic);

		// create import instance
		Import instance = ImportFactory.newCsvImport(mode, file, context, structures, logger, atomic);

		ImportStatus result;
		if (atomic) {
			result = importFacade.doAtomicImport(instance);
		}
		else {
			result = importFacade.doNonAtomicImport(instance);
		}

		importfilevo.setFieldValue(E.IMPORTFILE.laststate, result.getImportResult().getValue());
		importfilevo.setFieldValue(E.IMPORTFILE.result, result.getMessage());

		// @see NUCLOS-1522
		try {
			final GenericObjectDocumentFile logFile = (GenericObjectDocumentFile) importfilevo
					.getFieldValue(E.IMPORTFILE.logfile.getUID());
			UID docFileUID = DocumentFileBase.newFileUID();
			importfilevo.setFieldUid(E.IMPORTFILE.logfile.getUID(), docFileUID);
			importfilevo.setFieldValue(E.IMPORTFILE.logfile.getUID(), new GenericObjectDocumentFile(
					logFile.getFilename(),
					docFileUID,
					IOUtils.readFromBinaryFile(logger.getFile())));
		}
		catch (Exception e) {
			// ignore.
		}

		try {
			masterDataFacade.modify(importfilevo, null);
		}
		catch (Exception e) {
			throw new NuclosFatalException(e);
		}
		finally {
			result.cleanUp(); // trigger finish.
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = { Exception.class })
	public ImportStatus doAtomicImport(Import instance) throws NuclosFileImportException {
		try {
			ImportStatus result = doImportInternal(instance);
			if (result.getImportResult().equals(org.nuclos.common.fileimport.ImportResult.ERROR)) {
				try {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				}
				catch (NoTransactionException e) {
					LOG.error("Unable to set rollback only:", e);
				}
			}
			return result;
		}
		catch (NuclosFileImportException ex) {
			try {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
			catch (NoTransactionException e) {
				LOG.error("Unable to set rollback only:", e);
			}
			throw ex;
		}
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, noRollbackFor = { Exception.class })
	public ImportStatus doNonAtomicImport(Import instance) throws NuclosFileImportException {
		return doImportInternal(instance);
	}

	private ImportStatus doImportInternal(Import instance) throws NuclosFileImportException {
		return instance.doImport();
	}

	public void stopImport(UID importfileId) throws NuclosFileImportException {
		try {
			final JobKey jk = JobKey.jobKey(importfileId.toString(), NuclosQuartzJob.JOBGROUP_IMPORT);
			nuclosScheduler.interrupt(jk);
		}
		catch (UnableToInterruptJobException e) {
			throw new NuclosFileImportException("import.exception.stop");
		}
	}

	public String doImport(UID importfileId) throws NuclosFileImportException {
		String correlationId = getImportCorrelationId(importfileId);

		try {
			MasterDataVO<UID> importfile = masterDataFacade
					.getWithDependants(E.IMPORTFILE.getUID(), importfileId, null);
			ImportUtils.validateCsvFileImport(importfile);

			// reset state
			UID newDocFileUID = DocumentFileBase.newFileUID();
			GenericObjectDocumentFile file = (GenericObjectDocumentFile) importfile.getFieldValue(E.IMPORTFILE.documentfile.getUID());
			GenericObjectDocumentFile logFile = new GenericObjectDocumentFile(file.getFilename().substring(0,
					file.getFilename().lastIndexOf("."))
					+ ".log", newDocFileUID, new byte[] {});
			importfile.setFieldUid(E.IMPORTFILE.logfile, newDocFileUID);
			importfile.setFieldValue(E.IMPORTFILE.logfile.getUID(), logFile);
			importfile.setFieldValue(E.IMPORTFILE.laststate, null);
			importfile.setFieldValue(E.IMPORTFILE.result, null);
			masterDataFacade.modify(importfile, null);
		}
		catch (Exception ex) {
			throw new NuclosFileImportException(ex.getMessage(), ex);
		}

		if (StringUtils.isNullOrEmpty(correlationId)) {
			correlationId = UUID.randomUUID().toString();

			try {
				final JobKey jk = JobKey.jobKey(importfileId.getString(), NuclosQuartzJob.JOBGROUP_IMPORT);
				if (nuclosScheduler.getJobDetail(jk) != null) {
					nuclosScheduler.deleteJob(jk);
				}
				final JobDataMap jdm = new JobDataMap();
				jdm.put("ProcessId", correlationId);
				jdm.put("LocaleId",
						SpringApplicationContextHolder.getBean(LocaleFacadeBean.class).getUserLocale()
						.getLocale().getString());
				jdm.put("User", getCurrentUserName());
				final JobBuilder jb = JobBuilder.newJob(CsvImportJob.class).withIdentity(jk).setJobData(jdm)
						.storeDurably();
				final JobDetail jobDetail = jb.build();

				nuclosScheduler.addJob(jobDetail, false);
				nuclosScheduler.triggerJob(jk);
			}
			catch (SchedulerException ex) {
				throw new NuclosFileImportException("import.exception.start");
			}
		}
		return correlationId;
	}

	@Override
	void validateImportStructure(MasterDataVO<UID> importStructure) throws CommonValidationException {
		ImportUtils.validateCsvImportStructure(importStructure);
	}

	@Override
	void validateFileImport(MasterDataVO<UID> fileImport) throws CommonValidationException {
		ImportUtils.validateCsvFileImport(fileImport);
	}

}
