//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.objectimport.ImportResult;
import org.nuclos.api.objectimport.ImportStructureDefinition;
import org.nuclos.api.service.ImportService;
import org.nuclos.server.fileimport.ejb3.CsvImportFacadeLocal;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

@Component("csvImportObjectProvider")
public class CsvImportObjectProvider implements ImportService, BeanFactoryAware {
	
	CsvImportObjectProvider() {
	}
	
	// Spring injection
	
	/**
	 * From BeanFactoryAware. (tp) 
	 */
	private BeanFactory beanFactory;

	/**
	 * Can't get @Autowired as it must be resolved _very_ late. (tp)
	 */
	private CsvImportFacadeLocal importFacade;
	
	// end of Spring injection
	
	@Override
	public ImportResult run(NuclosFile importFile, boolean isTransactional,
			Class<? extends ImportStructureDefinition>... structureDefClasses)
			throws BusinessException {
	
		if (importFile == null)
			throw new BusinessException ("ImportFile must not be null");
		
		if (structureDefClasses == null || structureDefClasses.length == 0)
			throw new BusinessException ("Number of structure definitions must not be null or zero");
		
		if (importFacade == null) {
			importFacade = beanFactory.getBean(CsvImportFacadeLocal.class);
		}
		return importFacade.doImport(importFile, isTransactional, structureDefClasses);
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

}
