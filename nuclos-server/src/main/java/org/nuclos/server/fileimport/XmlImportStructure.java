//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.genericobject.GenericObjectImportUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Class representing an invoice structure definition.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version	00.01.000
 */
@Configurable
public class XmlImportStructure extends AbstractImportStructure {

	private final String match;
	
	// Spring injection
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	@Autowired
	private AttributeCache attributeCache;
	
	@Autowired
	private MetaProvider metaProvider;
	
	@Autowired
	Modules modules;
	
	// end of Spring injection
	
	public static XmlImportStructure newXmlImportStructure(final UID importStructureUid) {
		try {
			final MasterDataFacadeLocal mdfacade = ServerServiceLocator.getInstance().getFacade(
					MasterDataFacadeLocal.class);

			final MasterDataVO<UID> mdcvoStructure = mdfacade.get(E.XML_IMPORT, importStructureUid);

			final String name = (String) mdcvoStructure.getFieldValue(E.XML_IMPORT.name);
			final String matching = (String) mdcvoStructure.getFieldValue(E.XML_IMPORT.match);
			final String sEncoding = (String) mdcvoStructure.getFieldValue(E.XML_IMPORT.encoding);
			final Boolean bInsert = (Boolean) mdcvoStructure.getFieldValue(E.XML_IMPORT.insert);
			final Boolean bUpdate = (Boolean) mdcvoStructure.getFieldValue(E.XML_IMPORT.update);
			final Boolean bDelete = (Boolean) mdcvoStructure.getFieldValue(E.XML_IMPORT.delete);
			final String mode = (String) mdcvoStructure.getFieldValue(E.XML_IMPORT.mode);

			final UID entityUid = mdcvoStructure.getFieldUid(E.XML_IMPORT.entity);

			final XmlImportStructure result = new XmlImportStructure(
					importStructureUid, entityUid, name, mode,
					sEncoding,bInsert, bUpdate, bDelete,
					matching);
			return result;
		}
		catch (CommonPermissionException e) {
			throw new NuclosFatalException(e);
		}
		catch (CommonFinderException e) {
			throw new NuclosFatalException(e);
		}
	}

	public XmlImportStructure(UID importStructureUid, 
			UID entityUid, String name, String mode, String encoding,
			boolean insert, boolean update, boolean delete, String match) 
			throws CommonPermissionException {

		super(importStructureUid, entityUid, name, mode, encoding, insert, update, delete);
		this.match = match;
	}
	
	@PostConstruct
	void init() {
		final UID entityUid = getEntityUid();
		final UID importStructureUid = getImportStructureUid();
		
		final Collection<EntityObjectVO<UID>> collAttributes = masterDataFacade.getDependantMd4FieldMeta(
				E.XMLIMPORTATTRIBUTE.importfield, importStructureUid);

		final boolean isModule = modules.isModule(entityUid);

		for (final EntityObjectVO<UID> mdcvo : collAttributes) {
			final String matching = mdcvo.getFieldValue(E.XMLIMPORTATTRIBUTE.match);
			final UID attributeUid = mdcvo.getFieldUid(E.XMLIMPORTATTRIBUTE.attribute);
			final String format = mdcvo.getFieldValue(E.XMLIMPORTATTRIBUTE.parsestring);
			final boolean preserve = mdcvo.getFieldValue(E.XMLIMPORTATTRIBUTE.preserve);
			final NuclosScript script = mdcvo.getFieldValue(E.XMLIMPORTATTRIBUTE.script);

			final Class<?> clazz;

			UID foreignEntityUid = null;
			if (isModule) {
				final AttributeCVO meta = attributeCache.getAttribute(attributeUid);
				clazz = meta.getJavaClass();
				foreignEntityUid = meta.getExternalEntity();
			}
			else {
				final FieldMeta<?> meta = metaProvider.getEntityField(attributeUid);
				try {
					clazz = Class.forName(meta.getDataType());
				}
				catch (final ClassNotFoundException ex) {
					throw new NuclosFatalException(ex);
				}
				foreignEntityUid = meta.getForeignEntity();
			}

			final Set<ForeignEntityIdentifier> foreignentityattributes = 
					new HashSet<ForeignEntityIdentifier>();

			if (foreignEntityUid != null
					&& !attributeUid.equals(SF.PROCESS.getMetaData(entityUid).getUID())
					&& !attributeUid.equals(SF.STATE.getMetaData(entityUid).getUID())) {
				final boolean isFeModule = modules.isModule(foreignEntityUid);

				final Collection<EntityObjectVO<UID>> collForeignEntityIdentifiers = masterDataFacade.getDependantMd4FieldMeta(
						E.XMLIMPORTFEIDENTIFIER.importattribute, mdcvo.getPrimaryKey());
				for (final EntityObjectVO<UID> mdvo : collForeignEntityIdentifiers) {
					// final Integer iColumn = mdvo.getFieldValue(E.XMLIMPORTFEIDENTIFIER.fieldcolumn);
					final UID feattributeUid = mdvo.getFieldUid(E.XMLIMPORTFEIDENTIFIER.attribute);
					final String feformat = mdvo.getFieldValue(E.XMLIMPORTFEIDENTIFIER.parsestring);
					final Class<?> feclazz;

					if (isFeModule) {
						AttributeCVO meta = attributeCache.getAttribute(feattributeUid);
						feclazz = meta.getJavaClass();
					}
					else {
						FieldMeta<?> meta = metaProvider.getEntityField(feattributeUid);
						try {
							feclazz = Class.forName(meta.getDataType());
						}
						catch (final ClassNotFoundException ex) {
							throw new NuclosFatalException(ex);
						}
					}
					foreignentityattributes.add(new XmlForeignEntityIdentifierImpl(matching, foreignEntityUid, feattributeUid,
							feclazz, feformat));
				}
			}
			else {
				foreignEntityUid = null;
			}
			getItems().put(attributeUid, (XmlItem)
					new XmlItemImpl(entityUid, attributeUid, clazz, format, preserve,
							foreignEntityUid, foreignentityattributes, script, matching));
		}

		final Collection<EntityObjectVO<UID>> collIdentifiers = masterDataFacade.getDependantMd4FieldMeta(
				E.IMPORTIDENTIFIER.xmlimportfield, importStructureUid);
		
		for (final EntityObjectVO<UID> mdcvo : collIdentifiers) {
			getIdentifiers().add(mdcvo.getFieldUid(E.IMPORTIDENTIFIER.attribute));
		}
	}

	public String getMatch() {
		return match;
	}

	public Set<String> getForbiddenAttributeNames(ParameterProvider paramProvider) {
		return GenericObjectImportUtils.getForbiddenAttributeNames(paramProvider, isUpdate());
	}

	@Configurable
	static class XmlItemImpl extends AbstractItem implements XmlItem {
		
		private final String match;
		
		XmlItemImpl(final UID entityUid, 
				final UID fieldUid, final Class<?> cls, final String sFormat, 
				boolean bPreserve, final UID foreignentityUid, 
				final Set<ForeignEntityIdentifier> feIdentifier, final NuclosScript script, final String match) {
			
			super(entityUid, fieldUid, cls, sFormat, bPreserve, foreignentityUid, feIdentifier, script);
			this.match = match;
		}

		@Override
		public String getMatch() {
			return match;
		}

	}	// inner class CsvItemImpl

	@Configurable
	static class XmlForeignEntityIdentifierImpl extends ForeignEntityIdentifierImpl implements XmlForeignEntityIdentifier {
		
		private final String match;
		
		XmlForeignEntityIdentifierImpl(String match, final UID entityUid, final UID fieldUid, 
				Class<?> cls, String sFormat) {
			
			super(entityUid, fieldUid, cls, sFormat);
			this.match = match;
		}

		public String getMatch() {
			return match;
		}

	} // class XmlForeignEntityIdentifierImpl
	
}  // class ImportStructure
