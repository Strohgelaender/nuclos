//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.livesearch.ejb3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.PostConstruct;

import org.apache.lucene.search.ScoreDoc;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.searchcondition.CollectableDbCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDalReadVO;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.livesearch.ejb3.LiveSearchFacadeRemote;
import org.nuclos.common.lucene.CompleteTransaction;
import org.nuclos.common.lucene.IndexVO;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common.transport.GzipList;
import org.nuclos.common2.DocumentFile;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.common.ejb3.EntityObjectFacadeRemote;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.processor.jdbc.impl.EntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.documentfile.DocumentFileFacadeLocal;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.parameter.NuclosParameterProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Live search server bean.
 * Extension point: if - for any business reason - additional filtering is
 * is required for the search results, a pluggable module architecture exists
 * via the server-side configuration and implementations of LiveSearchAddFilter. 
 */
@Transactional(noRollbackFor= {Exception.class})
public class LiveSearchFacadeBean extends NuclosFacadeBean implements LiveSearchFacadeLocal, LiveSearchFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(LiveSearchFacadeBean.class);
	
	// Spring injection
	@Autowired
	private SessionUtils utils;
	
	@Autowired
	private EntityObjectFacadeRemote entityObjectFacade;
	
	@Autowired
	private GenericObjectFacadeLocal goFacade;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private LayoutFacadeLocal layoutFacade;
	
	@Autowired
	private NuclosRemoteContextHolder remoteContext;
	
	@Autowired
	private NuclosParameterProvider nuclosParameterProvider;
	
	@Autowired
	private NucletDalProvider nucletDalProvider;
	
	@Autowired
	private DocumentFileFacadeLocal documentFileFacade;
	// end of Spring injection
	
	public LiveSearchFacadeBean() {
	}
	
	@PostConstruct
	final void init() {
		createIndexDirAndFillIfNeeded(false);			
	}
	
	private ThreadLocal<IndexerHelper> threadLocalIndexerHelper = new ThreadLocal<IndexerHelper>();
	
	private IndexerHelper getIndexerHelper() {
		if (threadLocalIndexerHelper.get() == null) {
			threadLocalIndexerHelper.set(new IndexerHelper());
		}
		
		return threadLocalIndexerHelper.get();
	}
	
	public <PK> void sendVOintoQueue(final IDalReadVO<PK> idrVO, final String transactId) {
		if (IndexTools.INSTANCE().switchedOff()) {
			return;
		}
		
		if (!IndexTools.INSTANCE().checkIndexDir()) {
			createIndexDirAndFillIfNeeded(true);
		}
		
		try {
			Map<UID, Object> mpValues = new HashMap<>();
			if (E.DOCUMENTFILE.checkEntityUID(idrVO.getDalEntity()) && 
				idrVO.getFlag() != IDalVO.STATE_REMOVED) {
				NuclosFile content = documentFileFacade.loadContent((UID) idrVO.getPrimaryKey());
				String text = DocumentFile.extractText(content);
				if (text != null) {
					mpValues.put(E.DOCUMENTFILE.indexContent.getUID(), text);
				}
			}
			IndexVO<PK> toBeIndexed = new IndexVO<>(idrVO, transactId, mpValues);
			getIndexerHelper().addToQueue(toBeIndexed);
		} catch (RuntimeException re) {
			if (re.getCause() instanceof IOException) {
				//NUCLOS-7267 Typically documents are missing. Why is this wrapped within a runtime-exception??
				LOG.error(re.getMessage());
			} else {
				LOG.error(re.getMessage(), re);
			}
		}
	}
	
	public void transactionComplete(final String transactId, final boolean success, final boolean close) {
		if (IndexTools.INSTANCE().switchedOff()) {
			return;
		}
		
		try {
			CompleteTransaction completeTransaction = new CompleteTransaction(transactId, success, close);
			getIndexerHelper().addToQueue(completeTransaction);
		} catch (RuntimeException re) {
			LOG.error(re.getMessage(), re);
		}
	}
	
	private <PK> void deleteNonExistingIndices(List<Delete<PK>> toDelete) {
		if (toDelete == null || toDelete.isEmpty()) {
			return;
		}
				
		LOG.info("Deleting " + toDelete.size() + " non-existing Indices. First: " + toDelete.get(0));
		
		for (Delete<PK> delete : toDelete) {
			sendVOintoQueue(delete, "search");			
		}
		
		transactionComplete("search", true, true);
	}

	public void setIndexerEnabled(boolean enabled, boolean synchronous) {
		IndexTools.INSTANCE().setEnabled(enabled, synchronous);
		if (enabled) {
			createIndexDirAndFillIfNeeded(false);
		}
	}

	public void rebuildLuceneIndex() {
		createIndexDirAndFillIfNeeded(true);
	}
	
	private void createIndexDirAndFillIfNeeded(boolean forceRebuild) {
		if (IndexTools.INSTANCE().switchedOff()) {
			return;
		}
		
		try {
			
			if (!forceRebuild && IndexTools.INSTANCE().versionFileExists()) {
				return;
			}

			IndexTools.INSTANCE().removeIndexDir();
			IndexTools.INSTANCE().touchVersionFile();

			startRebuildingIndexDirectory();

			Thread.sleep(IndexTools.INSTANCE().isSynchronous() ? 250 : 1500);

		} catch (IOException | InterruptedException e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	private void startRebuildingIndexDirectory() {

		Runnable r = () -> {
			Map<EntityMeta<?>, Collection<UID>> mpEntity = getEntitiesToBeIndexed();

			for (EntityMeta<?> entity : mpEntity.keySet()) {

				try {
					indexEntity(entity, mpEntity.get(entity));
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			}
		};

		if (IndexTools.INSTANCE().isSynchronous()) {
			r.run();
		} else {
			new Thread(r).start();
		}
	}

	// NUCLOS-6855 In order to avoid illegal SQLs because of NUCLOS-6857, preindexing of entities can be switched off by
	// setting nucletparamer. For example: "Datalangunterpos=skipindex"
	private Map<EntityMeta<?>, Collection<UID>> getEntitiesToBeIndexed() {
		Map<EntityMeta<?>, Collection<UID>> ret = new HashMap<>();
		Map<String, Collection<String>> virtualEntities2Index = getVirtualEntities2Index();

		for (EntityMeta<?> entity : metaProvider.getDataEntities(virtualEntities2Index.keySet())) {
			String np = nuclosParameterProvider.getNucletParameterByName(entity.getNuclet(), entity.getEntityName());

			if ("skipindex".equalsIgnoreCase(np)) {
				continue;
			}

			ret.put(entity, getFields2Index(entity, virtualEntities2Index));
		}

		return ret;
	}

	// NUCLOS-5182 1) for virtual entities if fields are definied within the nuclet parameter, only those will be indexed.
	private Collection<UID> getFields2Index(EntityMeta<?> entity, Map<String, Collection<String>> virtualEntities2Index) {
		Collection<String> fields = virtualEntities2Index.get(entity.getVirtualEntity());
		
		Collection<UID> ret = new HashSet<>();
		
		for (FieldMeta<?> fm : entity.getFields()) {
			if (fm.isCalculated()) {
				continue; // NUCLOS-6371
			}
			if (fields == null || fields.contains(fm.getFieldName())) {
				ret.add(fm.getUID());
			}
		}
				
		return ret;		
	}

	// NUCLOS-5182 1) NucletParameter for (lucene)indixing virtual entities
	// Example for indexing VirtualRwWord, fields name and times: "VirtualRwWord=name,times"
	private Map<String, Collection<String>> getVirtualEntities2Index() {
		Map<String, Collection<String>> res = new HashMap<String, Collection<String>>();
		
		for (EntityMeta<?> ve : metaProvider.getVirtualEntities()) {
			String np = nuclosParameterProvider.getNucletParameterByName(ve.getNuclet(), ve.getVirtualEntity());
			
			if (np != null) {
				res.put(ve.getVirtualEntity(), new HashSet<>(Arrays.asList(np.split(";"))));
			}
		}
		
		return res;
	}
	
	private <PK> void indexEntity(EntityMeta<PK> entity, Collection<UID> fields) {
		final long CHUNKSIZE = 10000;
		ResultParams resultParams = new ResultParams(fields, 0L, CHUNKSIZE, true);
		
		fields.add(SF.PK_ID.getUID(entity));
		String sPkCompare = entity.isUidEntity() ? "t.STRUID > '$1'" : "t.INTID > $1";
		Object lastID = entity.isUidEntity() ? "" : 0L;
		
		EntityObjectProcessor<PK> eop = (EntityObjectProcessor<PK>)nucletDalProvider.getEntityObjectProcessor(entity);
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		String transactId = null;
		
		for (;;) {
			DbCondition cond = builder.plainCondition(sPkCompare.replace("$1", lastID.toString()));
			
			CollectableSearchCondition csc = new CollectableDbCondition(cond);
			CollectableSearchExpression cse = new CollectableSearchExpression(csc);
			
		    Collection<EntityObjectVO<PK>> lstEOs = eop.getSlimChunk(cse, resultParams);
		    
		    if (lstEOs.isEmpty()) {
		    	if (transactId != null) {
		    		transactionComplete(transactId, true, true);
		    	}
		    	return;
		    }
		    
			transactId = entity.getEntityName() + lastID;
			
		    for (EntityObjectVO<PK> eo : lstEOs) {
		    	eo.flagNew();
		    	sendVOintoQueue(eo, transactId);
		    	lastID = eo.getPrimaryKey();
		    }
		    
	    	transactionComplete(transactId, true, false);
		}
	    
	}
	
	private void searchImpl(List<LuceneSearchResult> result, String search, int max, final boolean referenceSearch, final boolean documentFileSearch, String originalSearch) {
		List<Delete<Object>> toDelete = new ArrayList<>();
		List<LuceneSearchResult> searchList = new ArrayList<>();
		final AtomicReference<ScoreDoc> lastScoreDoc = new AtomicReference<>();
		final boolean references = referenceSearch || documentFileSearch;

		for (;;) {
			List<Pair<String, String>> lstRes = IndexTools.INSTANCE().search(search, max, references, lastScoreDoc);
			if (lstRes.isEmpty()) {
				break;
			}

			for (Pair<String, String> sres : lstRes) {

				try {
					FieldMeta<?> fm = metaProvider.getEntityField(new UID(sres.y));
					UID entity = fm.getEntity();

					Object pk = metaProvider.getEntity(entity).isUidEntity() ? new UID(sres.x) : Long.parseLong(sres.x);
					EntityObjectVO<?> eo = null;

					//Subform-Documents or Document-File
					if (!referenceSearch && (E.GENERALSEARCHDOCUMENT.checkEntityUID(entity)
							|| E.DOCUMENTFILE.checkEntityUID(entity))) {

						//No security checks here for Generalsearchdocument or Document-File
						remoteContext.setRemotly(false);
						eo = entityObjectFacade.get(entity, pk);
						remoteContext.pop();

						if (eo == null) {
							toDelete.add(new Delete<>(pk, entity));
							continue;
						}

						if (E.GENERALSEARCHDOCUMENT.checkEntityUID(entity)) {
							pk = eo.getFieldId(E.GENERALSEARCHDOCUMENT.genericObject);
							entity = goFacade.getModuleContainingGenericObject((Long) pk);
							eo = entityObjectFacade.get(entity, pk);
						}
					}

					LuceneSearchResult searchResult = new LuceneSearchResult(entity, pk, fm, originalSearch);
					searchResult.setEoIfNotSet(eo);

					if (!result.contains(searchResult) && !searchList.contains(searchResult)) {
						searchList.add(searchResult);
					}

				} catch (NumberFormatException | CommonFatalException | CommonBusinessException e) {
					LOG.warn(e.getMessage(), e);
				}
			}


			//Now get the found objects for each entity block-wise.
			Map<UID, Map<Object, EntityObjectVO<Object>>> mpAllEos = loadEntityObjectsInBlocks(searchList);


			//Add the found objects to the result by the original order.
			for (LuceneSearchResult searchResult : searchList) {
				try {
					if (result.contains(searchResult)) {
						continue;
					}

					EntityObjectVO<?> eo = searchResult.getEo();

					if (eo == null) {
						Map<Object, EntityObjectVO<Object>> mpEOs = mpAllEos.get(searchResult.getEntity());
						if (mpEOs == null) {
							continue; //No privilege for this entity at all
						}

						eo = mpEOs.get(searchResult.getPk());
					}

					if (eo == null) {
						//Note that inside this call all permission will be checked (remote Interface)
						eo = entityObjectFacade.get(searchResult.getEntity(), searchResult.getPk());
					}

					//No CommonBusinessException and still null => Object does not exist. Delete it from index.
					if (eo == null) {
						toDelete.add(new Delete<>(searchResult.getPk(), searchResult.getEntity()));
						continue;
					}

					UsageCriteria usage = UsageCriteria.createUsageCriteriaFromEO(eo, null);

					if (layoutFacade.isDetailLayoutAvailable(usage)) {
						boolean bSubform = searchResult.getField() == E.GENERALSEARCHDOCUMENT.documentfile;
						boolean hasRights = bSubform ? securityCache.isReadOrWriteAllowedForSubform(E.GENERALSEARCHDOCUMENT.getUID(),
								usage, false, getCurrentUserName(), getCurrentMandatorUID()) :
								securityCache.isReadOrWriteAllowedForField(searchResult.getField(), usage, false,
										getCurrentUserName(), getCurrentMandatorUID());

						if (hasRights) {
							searchResult.setEoIfNotSet(eo);
							result.add(searchResult);
						}
					}

					//TODO: If there is no layout, try to find a layout that contains this result as sub-form

					if ((!referenceSearch || documentFileSearch) && metaProvider.isReferenced(searchResult.getEntity())) {
						Collection<FieldMeta<?>> collReferencing = metaProvider.getAllReferencingFields(searchResult.getEntity());

						if (!documentFileSearch && E.DOCUMENTFILE.indexContent.equals(searchResult.getField())) {
							//Search for documentfile references
							searchImpl(result, searchResult.getPk().toString(), max, false, true, originalSearch);
						}
						if (ForeignEntityFieldUIDParser.isWithinStringifiedReferences(searchResult.getField(), collReferencing)) {
							//Search for ReferenzFields
							searchImpl(result, searchResult.getPk().toString(), max, true, false, originalSearch);
						}
					}

					if (result.size() >= max) {
						break;
					}

				} catch (CommonPermissionException e) {
					//Standard-Case: User has not the privilege for this very object
					LOG.debug(e.getMessage(), e);
				}
			}

			if (result.size() >= max || lstRes.size() < max) {
				break;
			}
		}
		
		deleteNonExistingIndices(toDelete);
	}

	/**
	 * 
	 * @param searchList
	 * @return
	 */
	private Map<UID, Map<Object, EntityObjectVO<Object>>> loadEntityObjectsInBlocks(List<LuceneSearchResult> searchList) {
		//Now get the found objects for each entity block-wise.
		Map<UID, List<LuceneSearchResult>> searchMap = new HashMap<>();
		
		for (LuceneSearchResult searchResult : searchList) {
			if (!searchMap.containsKey(searchResult.getEntity())) {
				searchMap.put(searchResult.getEntity(), new ArrayList<>());
			}				
			searchMap.get(searchResult.getEntity()).add(searchResult);
		}

		Map<UID, Map<Object, EntityObjectVO<Object>>> mpAllEos = new HashMap<>();
		
		for (UID entity : searchMap.keySet()) try {
			mpAllEos.put(entity, loadEntityObjectsBlock(entity, searchMap.get(entity)));
			
		} catch (CommonPermissionException e) {
			//Standard-Case: User has not the privilege for this entity
			LOG.debug(e.getMessage(), e);
		}

		return mpAllEos;
	}
	
	/**
	 * 
	 * @param entity
	 * @param lstSearchResult
	 * @return
	 * @throws CommonPermissionException
	 */
	private Map<Object, EntityObjectVO<Object>> loadEntityObjectsBlock(UID entity, Collection<LuceneSearchResult> lstSearchResult) throws CommonPermissionException {
		Map<Object, EntityObjectVO<Object>> mpRes = new HashMap<>();
		
		if (lstSearchResult.size() <= 1) {
			return mpRes;
		}
		
		EntityMeta<Object> eMeta = metaProvider.getEntity(entity);
		checkReadAllowed(eMeta);
 		
		List<Object> pks = new ArrayList<>();
		for (LuceneSearchResult searchResult : lstSearchResult) {
			pks.add(searchResult.getPk());
		}
		
		JdbcEntityObjectProcessor<Object> eop = (JdbcEntityObjectProcessor<Object>) nucletDalProvider.getEntityObjectProcessor(eMeta);
		
		//Note that inside this call record grants will be considered.
		final List<EntityObjectVO<Object>> eos = eop.getByPrimaryKeys(pks);
		
		for (EntityObjectVO<Object> eo : eos) {
			mpRes.put(eo.getPrimaryKey(), eo);
		}
		
		return mpRes;
	}
	
	/**
	 * Perform live-search for one entity and search string.
	 * Returns a list of matching entity vos along with a set of attribute
	 * names to hide from the enduser. This is convenience, as we have to check
	 * the attribute permissions anyway when searching, and thus, can simply
	 * include this data in the search result.
	 * 
	 */

	@Override
	public List<LuceneSearchResult> nuclosIndexSearch(String search) {
		List<LuceneSearchResult> result = new ArrayList<>();
		
		searchImpl(result, search, 32, false, false, search);
		
		return new GzipList<>(result);
	}

}
