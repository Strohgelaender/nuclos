package org.nuclos.server.livesearch.ejb3;

import org.nuclos.common.dal.vo.IDalReadVO;

public interface LiveSearchFacadeLocal {
	
	<PK> void sendVOintoQueue(final IDalReadVO<PK> idrVO, final String transaction);
	
	void transactionComplete(final String transaction, final boolean success, final boolean close);

	void setIndexerEnabled(boolean enabled, boolean synchronous);

	void rebuildLuceneIndex();
	
}
