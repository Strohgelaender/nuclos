//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.maintenance;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;

import org.nuclos.api.businessobject.Query;
import org.nuclos.api.provider.QueryProvider;
import org.nuclos.businessentity.NucletIntegrationPoint;
import org.nuclos.common.Actions;
import org.nuclos.common.CommandMessage;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.web.activemq.NuclosJMSBrokerTunnelServlet;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.transaction.annotation.Transactional;


@Transactional(noRollbackFor = {Exception.class})
public class MaintenanceFacadeBean extends NuclosFacadeBean implements MaintenanceFacadeLocal, MaintenanceFacadeRemote {

	private static String maintenanceMode = MaintenanceConstants.MAINTENANCE_MODE_OFF;

	private static String maintenanceModeMarkerFileName = "maintenance-mode-on";
	
	@Autowired
	private ParameterProvider paramProvider;
	
	private String maintenanceSuperUserName;

	private Scheduler jobScheduler;

	private Scheduler getJobScheduler() {
		if (jobScheduler == null) {
			jobScheduler = (Scheduler) SpringApplicationContextHolder.getBean("nuclosScheduler");
		}
		return jobScheduler;
	}

	@PostConstruct
	private void checkMaintanenceModeMarkerFile() {
		if (paramProvider.isEnabled(ParameterProvider.KEY_MAINTENANCE_MODE_SURVIVE_RESTART)) {
			if (existMaintenanceModeMarkerFile()) {
				deleteMaintenanceModeMarkerFile();
				maintenanceMode = MAINTENANCE_MODE_ON;
			}
		}
	}

	@Override
	public String getMaintenanceSuperUserName() {
		return maintenanceSuperUserName;
	}



	private boolean checkUserAllowed() {
		String user = getCurrentUserName();
		boolean isMaintenanceAllowed = SecurityCache.getInstance().getAllowedActions(user, null).contains(Actions.ACTION_MAINTENANCE_MODE);
		boolean isSuperuser = SecurityCache.getInstance().isSuperUser(user);
		if(isMaintenanceAllowed || isSuperuser) {
			return true;
		} else {
			throw new AccessDeniedException("user not have the super user flag or the access for the action maintenance mode");
		}
	}
	
	@RolesAllowed("Login")
	@Override
	public String enterMaintenanceMode(String maintenanceSuperUserName) {
		checkUserAllowed();

		this.maintenanceSuperUserName = maintenanceSuperUserName;

		if (!MAINTENANCE_MODE_OFF.equals(maintenanceMode)) {
			return maintenanceMode;
		}

		maintenanceMode = MAINTENANCE_MODE_INITIALIZED;
		maintenanceModeInitializeRequestedAt = new Date();

		new Thread(() -> initShutdownSessions()).start();

		pauseJobs();

		try {
			enterMaintenanceModeAfterWaitingPeriod(getCompleteWaittimeInSeconds());
		} catch (IllegalStateException e) {
			LOG.error(e.getMessage(), e);
			maintenanceMode = MAINTENANCE_MODE_OFF;
			this.maintenanceSuperUserName = null;
			resumeJobs();
		}
		
		if (paramProvider.isEnabled(ParameterProvider.KEY_MAINTENANCE_MODE_SURVIVE_RESTART)) {
			writeMaintenanceModeMarkerFile();
		}
		

		String result = getMaintenanceMode();
		LOG.info("enterMaintenanceMode=" + result);
		return result;
	}

	
	private boolean existMaintenanceModeMarkerFile() {
		try {
			File systemPath = NuclosSystemParameters.getDirectory(NuclosSystemParameters.SYSTEM_PATH).getCanonicalFile();
			if (systemPath.exists()) {
				File maintenanceModeMarkerFile = new File(systemPath, maintenanceModeMarkerFileName);
				if (maintenanceModeMarkerFile.exists()) {
					return true;
				}
			}
		} catch (Exception e) {
			LOG.error("Error while checking maintenance mode marker file.", e);
		}
		return false;
	}
	
	private void writeMaintenanceModeMarkerFile() {
		try {
			File systemPath = NuclosSystemParameters.getDirectory(NuclosSystemParameters.SYSTEM_PATH).getCanonicalFile();
			if (!systemPath.exists()) {
				systemPath.mkdirs();
			}
			File maintenanceModeMarkerFile = new File(systemPath, maintenanceModeMarkerFileName);
			if (maintenanceModeMarkerFile.exists()) {
				throw new IllegalStateException("maintenance marker file already exist");
			}
			maintenanceModeMarkerFile.createNewFile();
		} catch (Exception e) {
			LOG.error("Error while writing maintenance mode marker file.", e);
		}
	}
	
	private void deleteMaintenanceModeMarkerFile() {
		try {
			File systemPath = NuclosSystemParameters.getDirectory(NuclosSystemParameters.SYSTEM_PATH).getCanonicalFile();
			if (systemPath.exists()) {
				File maintenanceModeMarkerFile = new File(systemPath, maintenanceModeMarkerFileName);
				if (maintenanceModeMarkerFile.exists()) {
					maintenanceModeMarkerFile.delete();
				}
			}
		} catch (Exception e) {
			LOG.error("Error while deleting maintenance mode marker file.", e);
		}
	}

	/**
	 * wait until all jobs are done and all users are logged out
	 *
	 * @param waitMaxSeconds max time to wait in minutes
	 */
	private void enterMaintenanceModeAfterWaitingPeriod(int waitMaxSeconds) {

		long timestamp = new Date().getTime();
		long endTimestamp = timestamp + waitMaxSeconds * 1000;
		try {

			while (maintenanceMode == MaintenanceConstants.MAINTENANCE_MODE_INITIALIZED &&
					(getJobScheduler().getCurrentlyExecutingJobs().size() > 0 || getNumberOfOpenSessions() > 1)
					&& new Date().getTime() < endTimestamp) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					LOG.error("Unable to sleep.", e);
				}

				LOG.info("Running jobs: {}, open sessions: {}",
						getJobScheduler().getCurrentlyExecutingJobs().size(),
						getNumberOfOpenSessions());
			}

			if (maintenanceMode == MAINTENANCE_MODE_INITIALIZED) {
				maintenanceMode = MAINTENANCE_MODE_ON;
				this.maintenanceModeInitializeRequestedAt = null;
			}

			if (getJobScheduler().getCurrentlyExecutingJobs().size() > 0) {
				throw new IllegalStateException("There are still " + getJobScheduler().getCurrentlyExecutingJobs().size() + " running job(s).");
			}

		} catch (SchedulerException e) {
			LOG.error("Unable to get JobScheduler information.", e);
		}
	}


	private Date maintenanceModeInitializeRequestedAt = null;

	/**
	 * @return the remaining time until the maintenance mode will entered in seconds
	 */
	@Override
	public synchronized Integer getWaittimeInSeconds() {
		if (MaintenanceConstants.MAINTENANCE_MODE_ON.equals(maintenanceMode)) {
			return 0;
		}
		if (maintenanceModeInitializeRequestedAt == null) {
			return null;
		}
		Calendar waitUntilCal = Calendar.getInstance();
		waitUntilCal.setTime(maintenanceModeInitializeRequestedAt);
		waitUntilCal.add(Calendar.SECOND, getCompleteWaittimeInSeconds());
		return ((int) (waitUntilCal.getTime().getTime() - new Date().getTime())) / 1000;
	}

	@Override
	public void throwRecompileOutsideMaintenanceIfNecessary() throws CommonPermissionException {
		final boolean isProductionEnvironment = NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_PRODUCTION);
		if (isProductionEnvironment) {
			if (!MaintenanceConstants.MAINTENANCE_MODE_ON.equals(getMaintenanceMode())) {
				throw new CommonPermissionException("recompile.outside.maintenance");
			}
		}
	}

	@Override
	@RolesAllowed("Login")
	public void exitMaintenanceMode() throws CommonValidationException {
		checkUserAllowed();
		final boolean isProductionEnvironment = NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_PRODUCTION);
		if (isProductionEnvironment) {
			final Query<NucletIntegrationPoint> qIp = QueryProvider.create(NucletIntegrationPoint.class);
			qIp.where(NucletIntegrationPoint.Problem.eq(true));
			if (!QueryProvider.execute(qIp).isEmpty()) {
				throw new CommonValidationException("integration.point.with.problem");
			}
		}
		maintenanceMode = MAINTENANCE_MODE_OFF;
		maintenanceSuperUserName = null;
		cancelShutdownSessions();
		if (paramProvider.isEnabled(ParameterProvider.KEY_MAINTENANCE_MODE_SURVIVE_RESTART)) {
			deleteMaintenanceModeMarkerFile();
		}
		resumeJobs();
		LOG.info("exitMaintenanceMode");
	}


	@Override
	public String getMaintenanceMode() {
		return maintenanceMode;
	}

	private void resumeJobs() {
		try {
			getJobScheduler().start();
		} catch (SchedulerException e) {
			LOG.error("Error while trying to resume jobs.", e);
		}
	}

	private void pauseJobs() {
		try {
			getJobScheduler().standby();
		} catch (SchedulerException e) {
			LOG.error("Unable to pause JobScheduler.", e);
		}
	}


	private void initShutdownSessions() {
		LOG.info("JMS send killSession to all users.");
		final CommandMessage cm = new CommandMessage(CommandMessage.CMD_SHUTDOWN);
		cm.setShutdownWaitTimeInSeconds(getCompleteWaittimeInSeconds());

		try {
			NuclosJMSUtils.sendObjectMessage(cm, JMSConstants.TOPICNAME_RULENOTIFICATION, null);
		} catch (IllegalStateException e) {
			LOG.warn("Error while calling initShutdownSessions.");
		}
	}

	private void cancelShutdownSessions() {
		LOG.info("cancel JMS send killSession to all users.");
		final CommandMessage cm = new CommandMessage(CommandMessage.CMD_CANCEL_SHUTDOWN);
		try {
			NuclosJMSUtils.sendObjectMessage(cm, JMSConstants.TOPICNAME_RULENOTIFICATION, null);
		} catch (IllegalStateException e) {
			LOG.warn("Error while calling cancelShutdownSessions.");
		}
	}


	/**
	 * @return true if the server is in maintenance mode
	 * and the given user is not a superuser
	 * or another superuser has already access to the system
	 */
	@Override
	public boolean blockUserLogin(String username) {
		if ("anonymousUser".equals(username)) { // before client login
			return false;
		}

		if (MAINTENANCE_MODE_OFF.equals(maintenanceMode)) {
			return false;
		}

		if (SecurityCache.getInstance().isSuperUser(username) || SecurityCache.getInstance().isMaintenanceUser(username)) {
			return false;
		}

		return !LangUtils.equal(maintenanceSuperUserName, username);
	}


	@Override
	public Integer getNumberOfOpenSessions() {
		return NuclosJMSBrokerTunnelServlet.getNrOfRecentUserSessions();
	}


	@Override
	public Integer getNumberOfRunningJobs() {
		try {
			return getJobScheduler().getCurrentlyExecutingJobs().size();
		} catch (SchedulerException e) {
			LOG.error("Unable to get JobScheduler information.", e);
		}
		return null;
	}

	public boolean isMaintenanceOff() {
		return MaintenanceConstants.MAINTENANCE_MODE_OFF.equals(getMaintenanceMode());
	}

	@Override
	public boolean isProductionEnvironment() {
		return NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_PRODUCTION);
	}

	@Override
	public boolean isDevelopmentEnvironment() {
		return NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT);
	}
}
