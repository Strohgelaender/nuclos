//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.documentfile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.utils.NuclosFileUtils;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Helper class for managing DocumentFile objects.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:kai.fibr@nuclos.de">Kai Fibr</a>
 * @version 01.00.00
 */
public abstract class DocumentFileUtils {
	
	private static DocumentFileFacadeLocal documentFileFacade;
	
	/**
	 * Get name of physical file, consisting of an UID 
	 * and the file extension of uploaded file (e.g. YiX8IM7a2FkyWc6r1hPV.png)
	 * @param documentFileUID
	 * @param filename
	 * @return name of physical file
	 */
	public static String getDocumentFileName(UID documentFileUID, String filename) {
		String documentFileName = documentFileUID.toString();
		String fileExtension = org.nuclos.common2.File.getExtension(filename);
		if (fileExtension != null)
			documentFileName += "." + fileExtension;
		return documentFileName;
	}
	
	/**
	 * Get java.io.File for a specific documentFileUID and filename 
	 * @param documentFileUID
	 * @param filename
	 * @return java.io.File of physical file
	 */
	public static File getDocumentFile(UID documentFileUID, String filename) {
		final java.io.File documentDir = new File(NuclosSystemParameters.getString(NuclosSystemParameters.DOCUMENT_PATH));
		java.io.File file = new java.io.File(documentDir, DocumentFileUtils.getDocumentFileName(documentFileUID, filename));
		return file;
	}
	
	/**
	 * Get a map of all entity fields and existing document file UIDs of an EntityObject
	 * @param eoVO EntityObject to check
	 * @return map of all entity fields and existing document file UIDs
	 */
	public static Map<UID, UID> getExistingDocumentFiles(EntityObjectVO<?> eoVO) {
		Map<UID, UID> existingDocumentFileMap = new HashMap<>();
		for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(eoVO.getDalEntity()).values()) {
			if (efMeta.isFileDataType()) {
				UID documentFileUID = getDocumentFileFacade().findDocumentFileUID(efMeta.getUID(), eoVO.getPrimaryKey());
				if (documentFileUID != null) {
					existingDocumentFileMap.put(efMeta.getUID(), documentFileUID);
				}
			}
		}
		return existingDocumentFileMap;
	}
	
	/**
	 * Remove links of deleted files
	 * and the physical files as well, 
	 * if it was the last link to document file 
	 * @param eoVO EntityObject to check
	 * @param existingDocumentFiles a map of existing document files before update/remove
	 */
	public static void removeDocumentFiles(EntityObjectVO<?> eoVO, Map<UID, UID> existingDocumentFiles) {
		for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(eoVO.getDalEntity()).values()) {
			if (efMeta.isFileDataType()) {
				UID documentFileUID = eoVO.getFieldUid(efMeta.getUID());
				// field is now empty
				if (documentFileUID == null) {
					UID existingDocumentFileUID = existingDocumentFiles.get(efMeta.getUID());
					// but was set before 
					if (existingDocumentFileUID != null) {
						getDocumentFileFacade().deleteDocumentFileLink(existingDocumentFileUID, efMeta.getUID(), eoVO.getPrimaryKey());
					}
				}
			}
		}
	}
	
	/**
	 * Remove all links to document files
	 * and the physical files as well, 
	 * if it was the last link to document file 
	 * @param eoVO EntityObject to check
	 */
	public static void removeDocumentFiles(EntityObjectVO<?> eoVO) {
		for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(eoVO.getDalEntity()).values()) {
			if (efMeta.isFileDataType()) {
				UID documentFileUID = eoVO.getFieldUid(efMeta.getUID());
				if (documentFileUID != null) {
					getDocumentFileFacade().deleteDocumentFileLink(documentFileUID,	efMeta.getUID(), eoVO.getPrimaryKey());
				}
			}
		}
	}
	
	/**
	 * Change content of physical file (transactional)
	 * @param documentFile new file content
	 * @param dir document path
	 * @param documentFileUID document file UID
	 */
	public static void updateDocumentFile(GenericObjectDocumentFile documentFile, final File dir, UID documentFileUID) {
		// delete exiting file
		String fileName = getDocumentFileFacade().getFileName(documentFileUID);
		fileName = DocumentFileUtils.getDocumentFileName(documentFileUID, fileName);
		final String existingFileName = fileName;
		final File existingFile = new File(dir, existingFileName);
		final File oldFile = new File(dir, existingFileName + ".old");
		// rename file to fileName + ".old"
		existingFile.renameTo(oldFile);
		// store new file
		storeDocumentFile(documentFile, documentFileUID);
		// register transaction handler
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCompletion(int status) {
				if (status == TransactionSynchronization.STATUS_COMMITTED) {
					// delete .old-File
					if (oldFile.exists())
						oldFile.delete();
				}
				else if (status == TransactionSynchronization.STATUS_ROLLED_BACK) {
					// rename .old-File back
					oldFile.renameTo(new File(dir, existingFileName));
				}
			}
		});
	}
	
	/**
	 * Store new document files or update existing document files
	 * @param eoVO EntityObject to check
	 * @param existingDocumentFiles a map of existing document files before update/remove
	 */
	public static void storeDocumentFiles(EntityObjectVO<?> eoVO, Map<UID, UID> existingDocumentFiles) {
		File dir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH);
		
		for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(eoVO.getDalEntity()).values()) {
			if (efMeta.isFileDataType()) {
				final UID documentFileDbBackupContentField = efMeta.getDocumentFileDbBackupContentField();
				final boolean bIsWithDbBackup = documentFileDbBackupContentField != null;
				boolean bWriteDbBackup = false;
				UID documentFileUID = eoVO.getFieldUid(efMeta.getUID());
				GenericObjectDocumentFile documentFile = (GenericObjectDocumentFile) eoVO.getFieldValue(efMeta.getUID());
				
				if (documentFile != null) {
						
					if (eoVO.isFlagNew()) { // create bo

						if (documentFileUID == null) {
							documentFileUID = (UID) documentFile.getDocumentFilePk();
						}
						final boolean fileExists = getDocumentFile(documentFileUID, documentFile.getFilename()).exists();

						if (fileExists) {
							// new bo with existing file and not a link? -> must be a clone -> new UID (see NUCLOS-5464)

							if (!documentFile.getContentsChanged()) {
								// load contents
								documentFile.getContents();
							}

							documentFileUID = DocumentFileBase.newFileUID();
							documentFile.setDocumentFilePk(documentFileUID);
							eoVO.setFieldUid(efMeta.getUID(), documentFileUID);
						}

						storeDocumentFile(documentFile, documentFileUID);
						bWriteDbBackup = true;
						getDocumentFileFacade().createDocumentFile(documentFileUID,	documentFile.getFilename());
						getDocumentFileFacade().createDocumentFileLink(documentFileUID, efMeta.getUID(), eoVO.getPrimaryKey());

					} else if (eoVO.isFlagUpdated()) { // update bo

						if (documentFile.getContentsChanged()) {
							UID existingFileUID = existingDocumentFiles.get(efMeta.getUID());
							
							if (existingFileUID != null) { // DocumentFile in Field is updated
								eoVO.setFieldUid(efMeta.getUID(), existingFileUID); 
								updateDocumentFile(documentFile, dir, existingFileUID);
								getDocumentFileFacade().updateDocumentFile(existingFileUID,	documentFile.getFilename());
								
							} else { // DocumentFile in Field is new
								if (documentFileUID == null) {
									documentFileUID = (UID) documentFile.getDocumentFilePk();
								}
								// NUCLOS-7310
								if (UID.UID_NULL.equals(documentFileUID)) {
									documentFileUID = DocumentFileBase.newFileUID();
								}
								storeDocumentFile(documentFile, documentFileUID);
								bWriteDbBackup = true;
								getDocumentFileFacade().createDocumentFile(documentFileUID, documentFile.getFilename());
								getDocumentFileFacade().createDocumentFileLink(documentFileUID, efMeta.getUID(), eoVO.getPrimaryKey());
							}
						}
					}

					if (bIsWithDbBackup && bWriteDbBackup) {
						eoVO.setFieldValue(documentFileDbBackupContentField, documentFile.getContents());
					}

					documentFile.setContentsStored();
					
				} else { // documentFile == null
					
					if (documentFileUID != null) {
						if (eoVO.isFlagNew() || eoVO.isFlagUpdated()) {
							getDocumentFileFacade().createDocumentFileLink(documentFileUID, efMeta.getUID(), eoVO.getPrimaryKey());
							String sFileName = getDocumentFileFacade().getFileName(documentFileUID);
							// mark as linked:
							eoVO.setFieldValue(efMeta.getUID(), new GenericObjectDocumentFile(sFileName, documentFileUID));
						}
					}
				}
			}
		}
	}
	
	/**
	 * Check all document files and remove duplicates
	 * @return Information about how many duplicates removed
	 * 		   and how many bytes saved.
	 */
	public static String cleanupDuplicateDocuments() {
		long foundDuplicates = 0;
		long savedBytes = 0;
		long chunksize = 25;
		long documentcount = getDocumentFileFacade().countDocumentFiles();
		boolean oneFetch = documentcount < chunksize;
		Map<String, UID> checksumMap = new HashMap<>();
		List<UID> documentsUIDsToDelete = new ArrayList<>();
		for (long i = 0; i < documentcount; i+= chunksize) {
			Long istart = i;
			Long iend = i + chunksize - 1;
			if (iend >= documentcount) {
				iend = documentcount - 1;
			}
			if (oneFetch) {
				istart = iend = -1L;
			} 
			List<DocumentFileChecksum> chunk = getDocumentFileFacade().getDocumentFileChecksums(istart, iend);
			for (DocumentFileChecksum checksum : chunk) {
				String key = checksum.getFilename() + "#" + checksum.getChecksum();
				if (!checksumMap.containsKey(key)) {
					checksumMap.put(key, checksum.getDocumentUID());
				}
				else {	// duplicate found
					UID oldDocumentFileUID = checksum.getDocumentUID();
					UID newDocumentFileUID = checksumMap.get(key);
					// update file links
					getDocumentFileFacade().updateDocumentFileLinks(oldDocumentFileUID, newDocumentFileUID);
					// register old document UID to delete
					documentsUIDsToDelete.add(oldDocumentFileUID);
					foundDuplicates++;
					savedBytes += checksum.getFilelength();
				}
			}
		}
		// delete duplicates
		if (!documentsUIDsToDelete.isEmpty()) {
			for (UID documentUID : documentsUIDsToDelete) {
				getDocumentFileFacade().deleteDocumentFile(documentUID);
			}
		}
		
		return foundDuplicates + " duplicates found.\n" + savedBytes + " bytes saved.";
	}
	
	/**
	 * Store physical file (transactional)
	 * @param documentFile doument file content
	 * @param documentFileUID document file UID
	 */
	public static void storeDocumentFile(GenericObjectDocumentFile documentFile, UID documentFileUID) {
		try {
			final String filePath = NuclosFileUtils.getPathNameForDocument(documentFile, documentFileUID);
			IOUtils.writeToBinaryFile(new File(filePath), documentFile.getContents());
			// register rollback handler
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public void afterCompletion(int status) {
					if (status == TransactionSynchronization.STATUS_ROLLED_BACK) {
						File file = new File(filePath);
						if (file.exists())
							file.delete();
					}
				}
			});
		} catch (IOException e) {
			throw new NuclosFatalException(
					"File content cannot be updated for new file ("
							+ e.getMessage() + ").");
		}
	}
	
	private static DocumentFileFacadeLocal getDocumentFileFacade() {
		if (documentFileFacade == null) {
			documentFileFacade = SpringApplicationContextHolder.getBean(DocumentFileFacadeLocal.class);
		}
		return documentFileFacade;
	}
}
