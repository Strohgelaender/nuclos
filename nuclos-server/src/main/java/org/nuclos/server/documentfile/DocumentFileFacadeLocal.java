//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.documentfile;

import java.util.List;

import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;

public interface DocumentFileFacadeLocal {
	
	/**
	 * Get name of uploaded file by documentFileUID
	 * @param documentFileUID UID of DocumentFile
	 * @return name of uploaded file
	 */
	public String getFileName(UID documentFileUID);
	
	/**
	 * Find DocumentFile UID for a specific entity field of a business object
	 * @param entityFieldUID UID of entity field witch contains a DocumentFile
	 * @param pk primary key of business object
	 * @return UID of DocumentFile or null if no DocumentFile exist for the field
	 */
	public UID findDocumentFileUID(UID entityFieldUID, Object pk);
	
	/**
	 * Create DocumentFile entry
	 * @param documentFileUID UID of DocumentFile
	 * @param fileName name of uploaded file
	 */
	public void createDocumentFile(UID documentFileUID, String fileName);
	
	/**
	 * Update file name and version of DocumentFile entry
	 * @param documentFileUID UID of DocumentFile
	 * @param fileName name of uploaded file
	 */
	public void updateDocumentFile(UID documentFileUID, String fileName);
	
	/**
	 * Create a DocumentFile link entry.
	 * For every reference to a document, a DocumentFileLink entry is created,
	 * which contains the DocumentFile UID, entity field UID and the business objects primary key
	 * @param documentFileUID UID of DocumentFile
	 * @param entityFieldUID UID of entity field witch contains a DocumentFile
	 * @param pk primary key of business object
	 */
	public void createDocumentFileLink(UID documentFileUID, UID entityFieldUID, Object pk);
	
	/**
	 * Delete an DocumentFileLink entry.
	 * If that entry was the last reference, DocumentFile entry and the physical file are deleted as well
	 * @param documentFileUID UID of DocumentFile
	 * @param entityFieldUID UID of entity field witch contains a DocumentFile
	 * @param pk primary key of business object
	 */
	public void deleteDocumentFileLink(UID documentFileUID, UID entityFieldUID, Object pk);
	
	/**
	 * Delete an document file entry and the physical file (transactional)
	 * @param documentFileUID UID of DocumentFile
	 */
	public void deleteDocumentFile(final UID documentFileUID);
	
	/**
	 * Count all document file entries
	 * @return number of document file entries
	 */
	public Long countDocumentFiles();
	
	/**
	 * Get a chunk of DocumentFileChecksum objects, that contain
	 * meta information about document files
	 * @param startIndex start index of chunk
	 * @param endIndex end index of chunk
	 * @return a chunk of DocumentFileChecksum object
	 */
	public List<DocumentFileChecksum> getDocumentFileChecksums(long startIndex, long endIndex);
	
	/**
	 * Updates document file links from an old document UID to an new one.
	 * This is used in the "cleanup duplicate documents" console command.
	 * @param oldDocumentFileUID UID of old DocumentFile
	 * @param newDocumentFileUID UID of new DocumentFile 
	 */
	public void updateDocumentFileLinks(UID oldDocumentFileUID, UID newDocumentFileUID);
	
	/**
	 * Load file content and return as NuclosFile
	 * @param documentFileUID UID of DocumentFile
	 * @return NuclosFile 
	 */
	public NuclosFile loadContent(UID documentFileUID);
}
