//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.validation.annotation.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopInfrastructureBean;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class ValidationSupport implements BeanPostProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(ValidationSupport.class);
	
	private final MultiListMap<EntityMeta<?>, Pair<Integer, Validator>> validators = new MultiListHashMap<EntityMeta<?>, Pair<Integer, Validator>>();
	private final List<Pair<Integer, Validator>> genericValidators= new ArrayList<Pair<Integer, Validator>>();
	
	// Spring injection
	
	@Autowired
	private MetaProvider metaProvider;
	
	@Autowired
	private LocaleFacadeLocal localeFacade;
	
	// end of Spring injection

	ValidationSupport() {
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (bean instanceof AopInfrastructureBean) {
			// Ignore AOP infrastructure such as scoped proxies.
			return bean;
		}
		Class<?> targetClass = AopUtils.getTargetClass(bean);
		if (targetClass.isAnnotationPresent(Validation.class) && bean instanceof Validator) {
			final Validation a = targetClass.getAnnotation(Validation.class);
			final Validator v = (Validator) bean;
			final Integer order = a.order();

			LOG.info("Processing Validator {}[entity={};entities={};order={}]",
			         bean.getClass(), a.entity(), a.entities(), order);

			final Pair<Integer, Validator> entry = new Pair<Integer, Validator>(order, v);
			if (StringUtils.isNullOrEmpty(a.entity()) 
					&& (a.entities() == null || a.entities().length == 0)
					&& (a.entityUids() == null || a.entityUids().length == 0)) {
				genericValidators.add(entry);
				// return early
				return bean;
			}
			
			final boolean hasEntities = a.entities() != null && a.entities().length > 0;
			final boolean hasEntity = !StringUtils.isNullOrEmpty(a.entity());
			if (hasEntities && hasEntity) {
				throw new IllegalArgumentException("In @Validation, only one of entity and entities should be set");
			}
			if (hasEntities) {
				// deprecated
				for (final String strEntity : a.entities()) {
					final EntityMeta<?> entity =  E.getByName(strEntity);
					if (entity == null) {
						throw new IllegalArgumentException("In @Validation: Unknown entity " + strEntity);
					}
					validators.addValue(entity, entry);
				}
			} else if (hasEntity) {
				// deprecated
				final EntityMeta<?> entity =  E.getByName(a.entity()); 
				if (entity == null) {
					throw new IllegalArgumentException("In @Validation: In entities: unknown entity " + a.entity());
				}
				validators.addValue(entity, entry);
			} else {
				// new and valid stuff
				for (final String u: a.entityUids()) {
					final UID uid = new UID(u);
					final EntityMeta<?> entity =  metaProvider.getEntity(uid);
					if (entity == null) {
						throw new IllegalArgumentException("In @Validation: Unknown entity uid" + u);
					}
					validators.addValue(entity, entry);
				}
			}
		}
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	public Validator[] getValidators(final EntityMeta<?> entity) {
		List<Pair<Integer, Validator>> result = new ArrayList<Pair<Integer,Validator>>();
		/*
		if (validators.containsKey(__GENERIC)) {
			result.addAll(validators.getValues(__GENERIC));
		}
		*/
		result.addAll(genericValidators);
		if (validators.containsKey(entity)) {
			result.addAll(validators.getValues(entity));
		}
		Collections.sort(result, new Comparator<Pair<Integer, Validator>>() {
			@Override
			public int compare(Pair<Integer, Validator> o1, Pair<Integer, Validator> o2) {
				return LangUtils.compare(o1.x, o2.x);
			}
		});
		return CollectionUtils.transform(result, new Transformer<Pair<Integer, Validator>, Validator>() {
			@Override
			public Validator transform(Pair<Integer, Validator> i) {
				return i.y;
			}
		}).toArray(new Validator[result.size()]);
	}

	public void validate(EntityObjectVO<?> eo, IDependentDataMap dependants) throws CommonValidationException {
		final ValidationContext c = new ValidationContext(localeFacade.getUserLocale().toLocale());
		validate(eo, dependants, c);
		if (c.hasErrors()) {
			CommonValidationException cve = new CommonValidationException(c.getErrors(), c.getFieldErrors());
			cve.setFullMessage(SpringLocaleDelegate.getInstance(), metaProvider, eo.getDalEntity());
			throw cve;
		}
	}

	private void validate(EntityObjectVO<?> eo, IDependentDataMap dependants, ValidationContext c) {
		for (Validator v : getValidators(MetaProvider.getInstance().getEntity(eo.getDalEntity()))) {
			LOG.debug("Processing validator  for object [{}]",
			          v.getClass(), eo.getDebugInfo());
			v.validate(eo, c);
		}
		if (dependants != null) {
			for (final IDependentKey dependentKey : dependants.getKeySet()) {
				final FieldMeta<?> refFieldMeta = metaProvider.getEntityField(dependentKey.getDependentRefFieldUID());
				final EntityMeta<?> entity = MetaProvider.getInstance().getEntity(refFieldMeta.getEntity());
				if (entity.isDatasourceBased()){
					continue;
				}
				for (final EntityObjectVO<?> eo2 : dependants.getData(dependentKey)) {
					if (eo2.isFlagRemoved() || eo2.isFlagUnchanged()) {
						continue;
					}
					final UID oldParent = c.getParent();
					c.setParent(eo.getDalEntity());
					try {
						validate(eo2, eo2.getDependents(), c);
					} finally {
						c.setParent(oldParent);
					}
				}
			}
		}
	}
}
