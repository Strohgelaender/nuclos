//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.validation;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.statemodel.valueobject.MandatoryFieldVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.validation.annotation.Validation;
import org.springframework.beans.factory.annotation.Autowired;

@Validation(order = 1)
public class MandatoryFieldValidator implements Validator {

	private IMetaProvider metaProvider;

	private StateCache stateCache;

	@Autowired
	public void setMetaDataProvider(IMetaProvider metaDataProvider) {
		this.metaProvider = metaDataProvider;
	}

	@Autowired
	final void setStateCache(StateCache stateCache) {
		this.stateCache = stateCache;
	}

	@Override
	public void validate(EntityObjectVO<?> object, ValidationContext c) {
		final EntityMeta<?> meta = metaProvider.getEntity(object.getDalEntity());
		if (meta.isStateModel() && c.getParent() != null) {
			// No dependent validation necessary (see NUCLOS-7095)
			return;
		}
		for (FieldMeta<?> fieldmeta : metaProvider.getAllEntityFieldsByEntity(object.getDalEntity()).values()) {
			if (fieldmeta.isNullable() || fieldmeta.isCalculated()) {
				continue;
			}
			checkMandatoryField(object, c, meta, fieldmeta);
		}

		if (meta.isStateModel()) {
			if (object.getFieldUids().containsKey(SF.STATE.getUID(object.getDalEntity()))) {
				UID stateUID = object.getFieldUid(SF.STATE.getUID(object.getDalEntity()));
				if (stateUID != null) {
					StateVO state = stateCache.getState(stateUID);
					if (!state.getMandatoryFields().isEmpty()) {
						for (MandatoryFieldVO mandatoryField : state.getMandatoryFields()) {
							final FieldMeta<?> fieldmeta = metaProvider.getEntityField(mandatoryField.getField());
							if (!LangUtils.equal(fieldmeta.getEntity(), object.getDalEntity())) {
								continue; // its not a field from current entity. statemodel seems to be valid in more than only this entity.
							}
							checkMandatoryField(object, c, meta, fieldmeta);
						}
					}
				}
			}
		}
	}
	
	private void checkMandatoryField(EntityObjectVO<?> object, ValidationContext c, EntityMeta<?> meta, FieldMeta<?> fieldmeta) {
		final Object value;
		if (fieldmeta.isFileDataType()) {
			value = object.getFieldUid(fieldmeta.getUID());
		} else if (fieldmeta.getForeignEntity() != null || fieldmeta.getUnreferencedForeignEntity() != null) {
			final EntityMeta<?> refMeta = metaProvider.getEntity(LangUtils.defaultIfNull(fieldmeta.getForeignEntity(), fieldmeta.getUnreferencedForeignEntity()));
			if (fieldmeta.getForeignEntity() != null
					&& LangUtils.equal(fieldmeta.getForeignEntity(), c.getParent())) {
				return;
			}
			if (fieldmeta.getUnreferencedForeignEntity() != null
					&& LangUtils.equal(fieldmeta.getUnreferencedForeignEntity(), c.getParent())) {
				return;
			}
			else if (LangUtils.equal(fieldmeta.getForeignEntity(), E.GENERICOBJECT.getUID())) {
				return;
			}
			if (refMeta.isUidEntity()) {
				value = object.getFieldUid(fieldmeta.getUID());
			} else {
				value = object.getFieldId(fieldmeta.getUID());
			}
		}
		else {
			value = object.getFieldValue(fieldmeta.getUID());
		}
		if (value == null) {
			String error = StringUtils.getParameterizedExceptionMessage("CollectableUtils.3", fieldmeta.getLocaleResourceIdForLabel());
			c.addFieldError(meta.getUID(), fieldmeta.getUID(), error, FieldValidationError.ValidationErrorType.MANDATORY_FIELD_ERROR);
		}
	}
}
