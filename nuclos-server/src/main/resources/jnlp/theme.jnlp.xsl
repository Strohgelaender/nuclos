<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" />

	<xsl:param name="codebase" />
	<xsl:param name="href" />
	<xsl:param name="jnlp.packEnabled" />
	<xsl:param name="version" />

	<xsl:template match="/">
		<jnlp spec="7.0+" codebase="{$codebase}" href="{$href}" version="{$version}">
			<information>
				<title>Nuclos theme</title>
				<vendor>Novabit Informationssysteme GmbH</vendor>
			</information>
			<security>
				<all-permissions />
			</security>
			<resources>
				<j2se version="1.8+" />
				<xsl:for-each select="jar">
					<jar href="{text()}" version="{@version}" download="{@download}" />
				</xsl:for-each>
				<property name="jnlp.versionEnabled" value="true"/>
				<property name="jnlp.packEnabled" value="{$jnlp.packEnabled}" />
			</resources>
			<component-desc/>
		</jnlp>
	</xsl:template>
</xsl:stylesheet>
