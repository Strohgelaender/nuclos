package org.nuclos.server;

import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

import org.junit.runner.RunWith;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchIgnore;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.thirdparty.com.google.common.collect.Iterators;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(
		packages = "org.nuclos.server",
		importOptions = ImportOption.DontIncludeTests.class
)
public class ArchitectureTest {
	@ArchTest
	public static void checkClassCount(JavaClasses classes) {
		// There are ~8k Nuclos server classes at the moment
		assert Iterators.size(classes.iterator()) > 5000 : "Only " + Iterators.size(classes.iterator()) + " classes found";
	}

	@ArchTest
	@ArchIgnore    // TODO: Currently impossible to execute this test, because of too much complexity (too many cycles?!)
	public static final ArchRule noCycles = slices().matching("org.nuclos.server.(*)..").should().beFreeOfCycles();
}
