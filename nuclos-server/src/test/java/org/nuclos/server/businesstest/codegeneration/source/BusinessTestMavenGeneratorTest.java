package org.nuclos.server.businesstest.codegeneration.source;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestMavenGeneratorTest {
	@Test
	public void testCreatePomXML() {
		final String nuclosVersion = "4.11.0-SNAPSHOT";
		final String groovyVersion = "2.4.7";

		final BusinessTestMavenGenerator generator = new BusinessTestMavenGenerator(nuclosVersion, groovyVersion);
		final String pomXml = generator.generatePomXml();

		assert pomXml.contains(nuclosVersion);
		assert pomXml.contains(groovyVersion);

		assert !pomXml.contains("$");
	}
}