package org.nuclos.server.businesstest.codegeneration.script;

import java.io.IOException;

import org.junit.Test;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestInsertThinScriptGeneratorTest extends AbstractBusinessTestScriptGeneratorTest {

	@Test
	public void testGenerator() throws CommonFinderException, IOException, CommonPermissionException {
		assert "INSERT THIN A".equals(generator.getTestName());

		ScriptResult script = parseScript();

		final String groovySource = script.getGroovySource();

		assert groovySource.contains(".name = 'Sample Data'");

		checkMandatoryFieldsOfA(groovySource);

		assert !groovySource.contains(".bByRefA1");
		assert !groovySource.contains(".refUser");

		assert groovySource.contains(".save()");
	}

	/**
	 * Asserts that all mandatory fields of A are set in the given Groovy source.
	 */
	static void checkMandatoryFieldsOfA(final String groovySource) {
		assert groovySource.contains(".integerField = 42");
		assert groovySource.contains(".doubleField = 42.0");
		assert groovySource.contains(".bigDecimalField = 0");
		assert groovySource.contains(".dateField = new Date().parse('dd.MM.yyyy', '01.01.2016')");
		assert groovySource.contains(".refFieldId = B.first()?.id");
	}

	@Override
	protected AbstractBusinessTestScriptGenerator newGenerator() {
		return getFactory(new A()).newInsertThinScriptGenerator();
	}
}