//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.dbtransfer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.log4j.Logger;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.remote.NoConnectionTimeoutRunner;
import org.nuclos.client.remote.NuclosHttpInvokerAttributeContext;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.wizard.WizardFrame;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dbtransfer.TransferNuclet;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common.dbtransfer.ZipOutput;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.dbtransfer.TransferFacadeRemote;
import org.nuclos.server.navigation.ejb3.TreeNodeFacadeRemote;
import org.pietschy.wizard.I18n;
import org.pietschy.wizard.PanelWizardStep;
import org.pietschy.wizard.WizardEvent;
import org.pietschy.wizard.WizardListener;
import org.pietschy.wizard.models.StaticModel;

import info.clearthought.layout.TableLayout;

public class DBTransferExport {
	
	private static final Logger LOG = Logger.getLogger(DBTransferExport.class);
	
	
	private final UID nucletUID;
	
	private final DBTransferUtils utils = new DBTransferUtils();
	
	private DBTransferWizard wizard;
	private StaticModel model;
	private PanelWizardStep step1, step2;
	
	private Exception exportException;
	
	private MainFrameTab ifrm;
	
	// former Spring injection
	
	private SpringLocaleDelegate localeDelegate;
	
	private TransferFacadeRemote transferFacadeRemote;
	
	private NuclosHttpInvokerAttributeContext ctx;
	
	private final Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node("DBTransferExport");
	
	private final static String PREF_LAST_USED_FILE_TYPE = "lastUsedFileType";
	
	// end of former Spring injection
	
	public DBTransferExport(UID nucletUID) {
		if (nucletUID == null) {
			throw new IllegalArgumentException("nucletUID must not be null");
		}
		this.nucletUID = nucletUID;
		
		setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
		setTransferFacadeRemote(SpringApplicationContextHolder.getBean(TransferFacadeRemote.class));
		setNuclosHttpInvokerAttributeContext(SpringApplicationContextHolder.getBean(NuclosHttpInvokerAttributeContext.class));
		init();
	}
	
	final void init() {
		I18n.setBundle(DBTransferWizard.getResourceBundle());
		
		ifrm = Main.getInstance().getMainController().newMainFrameTab(
				null, getSpringLocaleDelegate().getMessage("dbtransfer.export.title", "Konfiguration exportieren"));
		ifrm.setTabIconFromNuclos("getDefaultFrameIcon");
		
		step1 = newStep1(ifrm);
		step2 = newStep2(ifrm);

		model = new StaticModel(){
			@Override
			public boolean isLastVisible() {
				return false;
			}

			@Override
			public void refreshModelState() {
				super.refreshModelState();
				if (wizard != null) {

				}
			}
		};
		
		model.add(step1);
		model.add(step2);

		wizard = new DBTransferWizard(model);
      
      wizard.addWizardListener(new WizardListener() {
            @Override
            public void wizardClosed(WizardEvent e) {
				ifrm.close();
            }
            @Override
            public void wizardCancelled(WizardEvent e) {
				ifrm.close();             
            }
      });
	}
	
	final void setNuclosHttpInvokerAttributeContext(NuclosHttpInvokerAttributeContext ctx) {
		this.ctx = ctx;
	}
	
	final void setSpringLocaleDelegate(SpringLocaleDelegate cld) {
		this.localeDelegate = cld;
	}
	
	final SpringLocaleDelegate getSpringLocaleDelegate() {
		return localeDelegate;
	}
	
	final void setTransferFacadeRemote(TransferFacadeRemote transferFacadeRemote) {
		this.transferFacadeRemote = transferFacadeRemote;
	}
	
	final TransferFacadeRemote getTransferFacadeRemote() {
		return transferFacadeRemote;
	}
	
	public void showWizard(MainFrameTabbedPane homePane) {
      ifrm.setLayeredComponent(WizardFrame.createFrameInScrollPane(wizard));
      
      homePane.add(ifrm);
      
      ifrm.setVisible(true);
	}
	
	final JComboBox comboNuclet = new JComboBox(utils.getAvailableNuclets());
	final JTextField tfTransferFile = new JTextField(50);
	final JLabel lbMessage = new JLabel();
	final JRadioButton radFile = new JRadioButton();
	final JRadioButton radDir = new JRadioButton();
	final JButton btnNuclonExport = new JButton();
	final JButton btnNucletExport = new JButton();

	private PanelWizardStep newStep1(final MainFrameTab ifrm) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final PanelWizardStep step = new PanelWizardStep(localeDelegate.getMessage(
				"dbtransfer.export.step1.1", "Export"), 
				localeDelegate.getMessage("dbtransfer.export.step1.2", 
						"Bitte w\u00e4hlen Sie den Speicherort f\u00fcr die Konfigurationsdatei aus und die Optionen."));
		utils.initJPanel(step,
			new double[] {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.FILL},
			new double[] {	TableLayout.PREFERRED,
							TableLayout.PREFERRED,
							32,
							TableLayout.PREFERRED,
							TableLayout.PREFERRED});
		
		final JLabel lbNuclet = new JLabel("Nuclet");
		final ButtonGroup fileType = new ButtonGroup();
		radFile.setText(localeDelegate.getMessage("dbtransfer.import.step1.3", "Datei"));
		radDir.setText(localeDelegate.getMessage("dbtransfer.import.step1.22", "Verzeichnis"));
		
		fileType.add(radFile);
		fileType.add(radDir);
		switch (prefs.getInt(PREF_LAST_USED_FILE_TYPE, 0)) {
			case 0:	radFile.setSelected(true); break;
			case 1: radDir.setSelected(true); break;
		}
		
		final JButton btnBrowse = new JButton("...");
		btnNuclonExport.setText(localeDelegate.getMessage("configuration.transfer.export.option.nuclon", "Nuclon Export"));
		btnNuclonExport.setIcon(Icons.getInstance().getIconNuclon());
		btnNucletExport.setText(localeDelegate.getMessage("configuration.transfer.export.option.nuclet", "Nuclet Export"));
		btnNucletExport.setIcon(Icons.getInstance().getIconNuclet());

		tfTransferFile.getDocument().addDocumentListener(new DocumentListener() {

			ActionListener alUpdateButtons = new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					updateExportButtons();
					modTimer.stop();
				}
			};

			Timer modTimer = new Timer(20, alUpdateButtons);

			@Override
			public void insertUpdate(DocumentEvent e) {
				modTimer.restart();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				modTimer.restart();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				modTimer.restart();
			}
		});

		step.add(lbNuclet, "0,0");
		step.add(comboNuclet, "1,0");
		step.add(tfTransferFile, "1,1");
		step.add(radFile, "2,1");
		step.add(radDir, "3,1");
		step.add(btnBrowse, "4,1");
		step.add(lbMessage, "1,2");
		step.add(btnNucletExport, "1,3");
		step.add(btnNuclonExport, "1,4");
		
		ActionListener alLastUsedFileType = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (radFile.isSelected()) {
					prefs.putInt(PREF_LAST_USED_FILE_TYPE, 0);
				} else if (radDir.isSelected()) {
					prefs.putInt(PREF_LAST_USED_FILE_TYPE, 1);
				}
				tfTransferFile.setText("");
				updateExportButtons();
			}
		};
		radFile.addActionListener(alLastUsedFileType);
		radDir.addActionListener(alLastUsedFileType);
		
		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateExportButtons();
			}
		};
		comboNuclet.setSelectedItem(new TransferNuclet(nucletUID, null, null, false, false));
		al.actionPerformed(new ActionEvent(comboNuclet, 0, "init"));
		comboNuclet.addActionListener(al);
		
		btnBrowse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				if (radFile.isSelected()) {
					final JFileChooser filechooser = utils.getFileChooser(localeDelegate.getMessage(
							"configuration.transfer.file.nuclet", "Nuclet-Dateien"), ".nuclet", false);
					TransferNuclet tn = (TransferNuclet) comboNuclet.getSelectedItem();
					final String filename = (tn.getLabel() + (tn.getVersion()==null?"":String.format("-v%s", tn.getVersion())));
					filechooser.setSelectedFile(new File(filename + ".nuclet"));
					final int iBtn = filechooser.showSaveDialog(ifrm);
	
					if (iBtn == JFileChooser.APPROVE_OPTION) {
						final File file = filechooser.getSelectedFile();
						if (file.exists()) {
							if (JOptionPane.showConfirmDialog(ifrm, 
									localeDelegate.getMessage("general.overwrite.file", "general.overwrite.file", file.getName()),
									localeDelegate.getMessage("general.overwrite.file.title", "general.overwrite.file.title"), JOptionPane.OK_CANCEL_OPTION) != JOptionPane.YES_OPTION) {
								tfTransferFile.setText("");
								updateExportButtons();
								return;
							}
						}
						if (file != null) {
							String fileName = file.getPath();
	
							if (!fileName.toLowerCase().endsWith(".nuclet")) {
								fileName += ".nuclet";
							}
							tfTransferFile.setText(fileName);
							updateExportButtons();
						}
					}
				} else if (radDir.isSelected()) {
					final JFileChooser filechooser = utils.getDirChooser();
					final int iBtn = filechooser.showSaveDialog(ifrm);
	
					if (iBtn == JFileChooser.APPROVE_OPTION) {
						final File file = filechooser.getSelectedFile();
						if (file != null) {
							String fileName = file.getPath();
							tfTransferFile.setText(fileName);
							updateExportButtons();
						}
					}
				}
			}
		});
		updateExportButtons();
		btnNucletExport.addActionListener(new ExportAction(step, btnNucletExport, false));
		btnNuclonExport.addActionListener(new ExportAction(step, btnNuclonExport, true));
	
		return step;
	}
	
	private void updateExportButtons() {
		boolean nucletEnabledTmp = false;
		boolean nuclonEnabled = false;
		
		if (!StringUtils.looksEmpty(tfTransferFile.getText()) && 
				comboNuclet.getSelectedIndex()>=0) {
			
			try {
				TransferNuclet tn = (TransferNuclet) comboNuclet.getSelectedItem();
				validateExportLocation(tn, new File(tfTransferFile.getText()));
				if (tn.isNuclon()) {
					nuclonEnabled = true;
				} else {
					nucletEnabledTmp = true;
					nuclonEnabled = true;
				}
				SwingUtilities.invokeLater(() -> {
					lbMessage.setVisible(false);
				});
			} catch (Exception e) {
				SwingUtilities.invokeLater(() -> {
					lbMessage.setText(e.getMessage());
					lbMessage.setVisible(true);
				});
				//JOptionPane.showMessageDialog(tfTransferFile, e.getMessage(), "Location Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		//Wird spaeter aktiviert werden...
		//btnNuclonExport.setEnabled(nuclonEnabled);
		final boolean nucletEnabled = nucletEnabledTmp;
		SwingUtilities.invokeLater(() -> {
			btnNuclonExport.setEnabled(false);
			btnNucletExport.setEnabled(nucletEnabled);
		});
	}
	
	private void validateExportLocation(TransferNuclet tn, File fExport) throws IOException, NuclosBusinessException {
		if (radDir.isSelected()) {
			if (!fExport.exists() || !fExport.isDirectory()) {
				throw new NuclosBusinessException(translate("Export directory does not exist or is no directory."));
			} 
			
			File fNucletXml = new File(fExport.getCanonicalPath() + File.separatorChar + "nuclet.xml");
			if (fNucletXml.exists()) {
				final ByteArrayOutputStream out = new ByteArrayOutputStream();
				try {
					ZipOutput zout = new ZipOutput(out);
					DBTransferUtils.writeToZip(zout, fNucletXml, fExport);
					byte[] metaDataRoot = out.toByteArray();
					UID nucletUID = getTransferFacadeRemote().getNucletUIDFromMetaDataRoot(metaDataRoot, false);
					if (!RigidUtils.equal(nucletUID, tn.getUID())) {
						throw new NuclosBusinessException(translate("Export directory does not contain the selected Nuclet."));
					}
				} finally {
					out.close();
				}
			} else {
				if (fExport.list().length > 0) {
					throw new NuclosBusinessException(translate("Export directory does not contain a Nuclet and is not empty."));
				}
			}
		} else {
			if (fExport.exists() && fExport.isDirectory()) {
				throw new NuclosBusinessException(translate("Export path is a directory."));
			}
		}
	}
	
	private String translate(String s) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		return localeDelegate.getMsg(s);
	}
	
	private class ExportAction implements ActionListener {

		private final PanelWizardStep step;
		private final JButton btnExport;
		private final boolean bNuclon;

		public ExportAction(PanelWizardStep step, JButton btnExport, boolean bNuclon) {
			super();
			this.step = step;
			this.btnExport = btnExport;
			this.bNuclon = bNuclon;
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			final LayerLock lock = ifrm.lockLayerWithProgress();

			Thread t = new Thread("DBTransferExport.actionListener.startExport") {
				@Override
				public void run() {
					//are there any unassigned configuration parts?
					if (isAnyConfigurationUnassigned()) {
						if (JOptionPane.showConfirmDialog(ifrm,
							SpringLocaleDelegate.getInstance().getMessage("dbtransfer.export.unassigned.message", "There are parts of the configuration which are not assigned to any nuclet. Would you nevertheless like to proceed with the export?"),
							SpringLocaleDelegate.getInstance().getMessage("dbtransfer.export.unassigned.title", ""),
							JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
								doExport(lock);
							} else {
								ifrm.unlockLayer(lock);

								btnExport.setEnabled(true);
							};
					} else {
						doExport(lock);
					}
				}
			};
			t.start();
		}

		private void doExport(LayerLock lock) {
			try {
				step.setComplete(false);
				btnExport.setEnabled(false);
				ctx.setMessageReceiver(ifrm.getId());

				final Map<TransferOption, Serializable> exportOptions = new HashMap<TransferOption, Serializable>();
				if (bNuclon) {
					exportOptions.put(TransferOption.IS_NUCLON, null);
				}
				if (radDir.isSelected()) {
					exportOptions.put(TransferOption.IS_DIRECTORY_MODE, null);
				}

				byte[] transferFile = NoConnectionTimeoutRunner.runSynchronized(
						() ->  getTransferFacadeRemote().createTransferFile(
								((TransferNuclet) comboNuclet.getSelectedItem()).getUID(),
								exportOptions
						)
				);
				File f = new File(tfTransferFile.getText());
				if (radFile.isSelected()) {
					final OutputStream fout = new BufferedOutputStream(new FileOutputStream(f));
					try {
						fout.write(transferFile);
						LOG.info("written nuclet to " + f);
					} finally {
						fout.close();
					}
				} else if (radDir.isSelected()) {
					DBTransferUtils.writeToDir(f, transferFile);
				}
				exportException = null;
			} catch (Exception e) {
				exportException = e;
				LOG.warn("nuclet export failed: " + e, e);
			} finally {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						ifrm.unlockLayer(lock);

						btnExport.setEnabled(true);
						step.setComplete(true);
						model.nextStep();
					}
				});
			}
		}
	}

	private boolean isAnyConfigurationUnassigned() {
		TreeNodeFacadeRemote treeNodeFacadeRemote = SpringApplicationContextHolder.getBean(TreeNodeFacadeRemote.class);
		return treeNodeFacadeRemote.getAvailableNucletContents().size() > 0;
	}

	private PanelWizardStep newStep2(final MainFrameTab ifrm) {
		final JLabel lbResult = new JLabel();
		final JTextArea taLog = new JTextArea();
		final JScrollPane scrollLog = new JScrollPane(taLog);
		
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final PanelWizardStep step = new PanelWizardStep(localeDelegate.getMessage(
				"dbtransfer.import.step5.4", "Ergebnis"), 
				localeDelegate.getMessage("dbtransfer.export.step2.1", "Hier wird Ihnen das Ergebnis des Exports angezeigt.")){

				@Override
				public void prepare() {
					if (exportException == null) {
						setComplete(true);
						wizard.setCancelEnabled(false);
						lbResult.setText(localeDelegate.getMessage(
								"dbtransfer.export.step2.2", "Die Konfigurationsdaten wurden erfolgreich exportiert."));
						scrollLog.setVisible(false);
					} else {
						setComplete(false);
						wizard.setCancelEnabled(true);
						lbResult.setText(localeDelegate.getMessage(
								"dbtransfer.import.step5.7", "Ein Problem ist aufgetreten!"));
						scrollLog.setVisible(true);
						if (exportException instanceof FileNotFoundException){
							taLog.setText(localeDelegate.getMessage(
									"dbtransfer.export.step2.3", "Die ausgew\u00e4hlte Datei konnte nicht geschrieben bzw. ersetzt werden.\n" +
									"M\u00f6glicherweise ist diese noch ge\u00f6ffnet oder Ihnen fehlt eine Berechtigung."));
						} else {
							StringWriter sw = new StringWriter();
					      PrintWriter pw = new PrintWriter(sw, true);
					      LOG.warn("DB transfer export failed: " + exportException, exportException);
					      pw.flush();
					      sw.flush();
					      taLog.setText(sw.toString());
						}
					}
				}
		};
		
		utils.initJPanel(step,
			new double[] {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.FILL},
			new double[] {20,
							  TableLayout.FILL});
		
		step.add(lbResult, "0,0,3,0");
		step.add(scrollLog, "0,1,3,1");
		
		return step;
	}
}
