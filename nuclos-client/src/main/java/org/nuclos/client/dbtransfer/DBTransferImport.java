//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.dbtransfer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.Writer;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.TreePath;

import org.apache.commons.collections15.Closure;
import org.apache.commons.lang.mutable.MutableInt;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.TreeTableNode;
import org.jfree.util.Log;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.GeneratorActions;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.maintenance.MaintenanceUtils;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.remote.NoConnectionTimeoutRunner;
import org.nuclos.client.remote.NuclosHttpInvokerAttributeContext;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.wizard.WizardFrame;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.ParameterComparison;
import org.nuclos.common.dbtransfer.PreviewPart;
import org.nuclos.common.dbtransfer.Transfer;
import org.nuclos.common.dbtransfer.TransferOption;
import org.nuclos.common.dbtransfer.TransferTreeNode;
import org.nuclos.common.dbtransfer.ZipOutput;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.dbtransfer.TransferFacadeRemote;
import org.pietschy.wizard.I18n;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.PanelWizardStep;
import org.pietschy.wizard.WizardEvent;
import org.pietschy.wizard.WizardListener;
import org.pietschy.wizard.models.StaticModel;

import info.clearthought.layout.TableLayout;

public class DBTransferImport {

	public static final String IMPORT_EXECUTED = "import_executed";

	//
	
	// former Spring injection
	
	private SpringLocaleDelegate localeDelegate;
	
	private TransferFacadeRemote transferFacadeRemote;
	
	private NuclosHttpInvokerAttributeContext ctx;
	
	// end of former Spring injection

	private final ActionListener notifyParent;

	private Transfer importTransferObject = null;
	private boolean blnImportStarted, blnSaveOfScriptRecommend, blnSaveOfLogRecommend = false;
	private Transfer.Result importTransferResult = null;

	private final DBTransferUtils utils = new DBTransferUtils();
	
	private DBTransferWizard wizard;
	private StaticModel model;
	private PanelWizardStep step1, step2, step3, step4, step5;
	private MainFrameTab ifrm;
	
	public DBTransferImport(ActionListener notifyParent) {
		if (notifyParent == null) {
			throw new IllegalArgumentException("notifyParent must not be null");
		}
		this.notifyParent = notifyParent;
		
		setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
		setTransferFacadeRemote(SpringApplicationContextHolder.getBean(TransferFacadeRemote.class));
		setNuclosHttpInvokerAttributeContext(SpringApplicationContextHolder.getBean(NuclosHttpInvokerAttributeContext.class));
		init();
	}
	
	final void init() {
		ifrm = Main.getInstance().getMainController().newMainFrameTab(
				null, getSpringLocaleDelegate().getMessage("dbtransfer.import.title", "Konfiguration importieren"));
		I18n.setBundle(DBTransferWizard.getResourceBundle());

		ifrm.setTabIconFromNuclos("getDefaultFrameIcon");

		step1 = newStep1(ifrm);
		step2 = newStep2(ifrm);
		step3 = newStep3(ifrm);
		step4 = newStep4(ifrm);
		step5 = newStep5(ifrm);
		model = new StaticModel(){
			@Override
			public boolean isLastVisible() {
				return false;
			}

			@Override
			public void refreshModelState() {
				super.refreshModelState();
				if (wizard != null) {
					wizard.setCancelEnabled(!blnImportStarted);
					super.setPreviousAvailable(!blnImportStarted &&  !step1.equals(getActiveStep()));
				}
			}
		};

		model.add(step1);
		model.add(step2);
		model.add(step3);
		model.add(step4);
		model.add(step5);
		wizard = new DBTransferWizard(model);

      wizard.addWizardListener(new WizardListener() {
            @Override
            public void wizardClosed(WizardEvent e) {
            	closeWizard();
            }
            @Override
            public void wizardCancelled(WizardEvent e) {
            	closeWizard();
            }
      });
	}
	
	final void setSpringLocaleDelegate(SpringLocaleDelegate cld) {
		this.localeDelegate = cld;
	}
	
	final SpringLocaleDelegate getSpringLocaleDelegate() {
		return localeDelegate;
	}
	
	final void setNuclosHttpInvokerAttributeContext(NuclosHttpInvokerAttributeContext ctx) {
		this.ctx = ctx;
	}
	
	final void setTransferFacadeRemote(TransferFacadeRemote transferFacadeRemote) {
		this.transferFacadeRemote = transferFacadeRemote;
	}

	final TransferFacadeRemote getTransferFacadeRemote() {
		return transferFacadeRemote;
	}

	private void closeWizard(){
		if (blnImportStarted) {
			if (model.allStepsComplete()) {
				ifrm.close();
			} else {
				if (blnSaveOfLogRecommend || blnSaveOfScriptRecommend) {
					final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
					String titel = localeDelegate.getMessage("dbtransfer.import.closewizard.title", "Wizard kann noch nicht geschlossen werden");
					String message = "";
					if (blnSaveOfLogRecommend) {
						message = localeDelegate.getMessage("dbtransfer.import.closewizard.log", "Es ist ein Problem beim \u00c4ndern des Datenbankschemas aufgetreten.\n" +
								"Bitte speichern Sie die Log-Datei und analysieren Sie die Probleme!");
					} else if (blnSaveOfScriptRecommend) {
						message = localeDelegate.getMessage("dbtransfer.import.closewizard.script", "Da die \u00c4nderungen am Datenbankschema nicht automatisch durchgef\u00fchrt wurden\n" +
								"m\u00fcssen Sie diese noch manuell nachholen.\n" +
								"Bitte speichern Sie das Script!");
					}
					JOptionPane.showMessageDialog(ifrm, message, titel, JOptionPane.INFORMATION_MESSAGE);
				}
			}
		} else {
			ifrm.close();
		}
	}

	public void showWizard(MainFrameTabbedPane homePane) {
      ifrm.setLayeredComponent(WizardFrame.createFrameInScrollPane(wizard));
      int x = homePane.getComponentPanel().getWidth()/2-wizard.getPreferredSize().width/2;
      int y = homePane.getComponentPanel().getHeight()/2-wizard.getPreferredSize().height/2;
      x = x<0?0:x;
      y = y<0?0:y;
      ifrm.setBounds(x, y, wizard.getWidth(), wizard.getHeight());
//      ifrm.pack();

      homePane.add(ifrm);

      ifrm.setVisible(true);
	}

	/*
	 * Begin Step 1
	 */
	private final JTextField tfTransferFile = new JTextField(50);
	final JButton btnNuclonImport = new JButton();
	final JButton btnNucletImport = new JButton();
	final JButton btnNucletImportOld = new JButton();
	boolean bUseOldNucletImport = false;
	
	private PanelWizardStep newStep1(final MainFrameTab ifrm) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final PanelWizardStep step = new PanelWizardStep(localeDelegate.getMessage(
				"dbtransfer.import.step1.1", "Konfigurationsdatei"), 
				localeDelegate.getMessage("dbtransfer.import.step1.2", "Bitte w\u00e4hlen Sie eine Konfigurationsdatei aus."));
		
		final JLabel lbFile = new JLabel(localeDelegate.getMessage("dbtransfer.import.step1.23", "Datei / Verzeichnis"));
		final JLabel lbImportOutsideMaintenance = new JLabel(localeDelegate.getMsg("nuclet.import.outside.maintenance"));
		UIUtils.setFontStyleBold(lbImportOutsideMaintenance, true);
		
		utils.initJPanel(step,
			new double[] {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.FILL},
			new double[] {	  TableLayout.PREFERRED,
					          TableLayout.PREFERRED,
							  32,
							  TableLayout.PREFERRED,
							  TableLayout.PREFERRED,
							  TableLayout.FILL,
							  TableLayout.PREFERRED,
							  16});
		
		final JButton btnBrowse = new JButton("...");
		
		// TODO implement nuclon import (replace UIDs)
		btnNuclonImport.setVisible(false);
		
		btnNuclonImport.setText(localeDelegate.getMessage("configuration.transfer.import.option.nuclon", "Nuclon Import Analyse und Vorschau"));
		btnNuclonImport.setIcon(Icons.getInstance().getIconNuclon());
		btnNuclonImport.setEnabled(false);
		btnNucletImport.setText(localeDelegate.getMessage("configuration.transfer.import.option.nuclet.new", "Nuclet Import Analyse und Vorschau (NEUE Version)"));
		btnNucletImport.setIcon(Icons.getInstance().getIconNuclet());
		btnNucletImport.setEnabled(false);
		btnNucletImportOld.setText(localeDelegate.getMessage("configuration.transfer.import.option.nuclet", "Nuclet Import Analyse und Vorschau"));
		btnNucletImportOld.setEnabled(false);

		step.add(lbImportOutsideMaintenance, "0,0 3,0");
		step.add(lbFile, "0,1");
		step.add(tfTransferFile, "1,1");
		step.add(btnBrowse, "2,1");
		step.add(btnNucletImport, "1,3");
		step.add(btnNuclonImport, "1,4");
		step.add(btnNucletImportOld, "1,6,c,c");
		
		tfTransferFile.setEditable(true);
		tfTransferFile.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(final DocumentEvent e) {
				textChanged();
			}

			@Override
			public void removeUpdate(final DocumentEvent e) {
				textChanged();
			}

			@Override
			public void changedUpdate(final DocumentEvent e) {
				textChanged();
			}

			private void textChanged() {
				boolean enabled = false;
				String sFile = tfTransferFile.getText();
				if (!StringUtils.isNullOrEmpty(sFile)) {
					try {
						File f = new File(sFile);
						if (f.isDirectory()) {
							File fNucletXml = new File(f.getCanonicalPath() + File.separatorChar + "nuclet.xml");
							enabled = fNucletXml.exists();
						} else {
							enabled = f.getName().endsWith(".nuclet") && f.exists();
						}
					} catch (IOException io) {
						// enabled = false
					}
				}
				btnNucletImport.setEnabled(enabled);
				btnNucletImportOld.setEnabled(enabled);
			}
		});
		
		final ActionListener browseAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				final JFileChooser filechooser = utils.getFileChooser(localeDelegate.getMessage(
						"configuration.transfer.file.both", "Nuclet-Dateien / Verzeichnis"), ".nuclet", true);
				final int iBtn = filechooser.showOpenDialog(ifrm);

				if (iBtn == JFileChooser.APPROVE_OPTION) {
					final File file = filechooser.getSelectedFile();
					if (file != null) {
						tfTransferFile.setText("");
						btnBrowse.setEnabled(false);
							
						String fileName = file.getPath();
						if (StringUtils.isNullOrEmpty(fileName)) {
							return;
						}
						tfTransferFile.setText(fileName);
						
						final LayerLock lock = ifrm.lockLayerWithProgress();
						Thread t = new Thread(){
							@Override
							public void run() {
								step.setComplete(false);
								btnNuclonImport.setEnabled(false);
								btnNucletImport.setEnabled(false);
								btnNucletImportOld.setEnabled(false);

								ctx.setMessageReceiver(ifrm.getId());
								try {								
									String fileName = tfTransferFile.getText();
									if (StringUtils.isNullOrEmpty(fileName)) {
										return;
									}
									File f = new File(fileName);
									long size = f.length();
			
									final byte[] transferFile;
									if (f.isDirectory()) {
										File fNucletXml = new File(f.getCanonicalPath() + File.separatorChar + "nuclet.xml");
										if (fNucletXml.exists()) {
											final ByteArrayOutputStream out = new ByteArrayOutputStream();
											try {
												ZipOutput zout = new ZipOutput(out);
												DBTransferUtils.writeToZip(zout, f, f);
												transferFile = out.toByteArray();
											} finally {
												out.close();
											}
										} else {
											throw new NuclosBusinessException("Import of nuclet failed: File format is not supported.");
										}
									} else {
										final InputStream fin = new BufferedInputStream(new FileInputStream(f));
										try {
											transferFile = utils.getBytes(fin, (int) size);
										} finally {
											fin.close();
										}
									}
									final Map<TransferOption, Serializable> transferOptions = NoConnectionTimeoutRunner.runSynchronized(
											() -> getTransferFacadeRemote().getOptions(transferFile)
									);
									boolean nuclon = transferOptions.containsKey(TransferOption.IS_NUCLON);
									btnNuclonImport.setEnabled(nuclon);
									btnNucletImport.setEnabled(!nuclon);
									btnNucletImportOld.setEnabled(!nuclon);
								} catch (Exception e) {
									Errors.getInstance().showExceptionDialog(ifrm, e);
								} finally {
									btnBrowse.setEnabled(true);
									ifrm.unlockLayer(lock);
								}
							}
						};
						t.start();
					}
				}
			}
		};
		btnBrowse.addActionListener(browseAction);
		btnNuclonImport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				step.setComplete(true);
				model.nextStep();
			}
		});
		btnNucletImport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bUseOldNucletImport = false;
				step.setComplete(true);
				model.nextStep();
			}
		});
		btnNucletImportOld.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				bUseOldNucletImport = true;
				step.setComplete(true);
				model.nextStep();
			}
		});

		if (MaintenanceUtils.isImportPossible()) {
			lbImportOutsideMaintenance.setVisible(false);
		} else {
			lbFile.setVisible(false);
			tfTransferFile.setVisible(false);
			btnBrowse.setVisible(false);
			btnNucletImport.setVisible(false);
			btnNucletImportOld.setVisible(false);
			btnNuclonImport.setVisible(false);
		}

		return step;
	}
	
	/*
	 * Begin Step 2
	 */
	private JXTreeTable jttNucletChanges = new JXTreeTable() {
		@Override
		public String getToolTipText(final MouseEvent evt) {
			int row = jttNucletChanges.rowAtPoint(evt.getPoint());
			int col = jttNucletChanges.columnAtPoint(evt.getPoint());
			if (row >= 0) {
				TreePath selPath = jttNucletChanges.getPathForRow(row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				return tttn.getToolTipText(col);
			}
			return super.getToolTipText(evt);
		}
		@Override
		public Point getPopupLocation(MouseEvent evt) {
			setPopupTriggerLocation(evt);
			return super.getPopupLocation(evt);
		}
		protected void setPopupTriggerLocation(MouseEvent evt) {
			putClientProperty("popupTriggerLocation", evt != null ? evt.getPoint() : null);
		}
	};
	private boolean jttNucletChangesInitiated = false;
	private NucletChangeActionListener nucletChangeActionListener;

	private JTabbedPane tabbedPane;

	private JScrollPane scrollWarn;
	private JScrollPane scrollChanges;

	private JPanel jpnPreviewContent = new JPanel();
	private JPanel jpnPreviewHeader = new JPanel();
	private JPanel jpnPreviewFooter = new JPanel();
	
	private PanelWizardStep newStep2(final MainFrameTab ifrm) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();

		tabbedPane = new JTabbedPane();
		final JEditorPane editWarnings = new JEditorPane();
		final String sDefaultPreparePreviewTabText = localeDelegate.getMessage(
				"configuration.transfer.prepare.preview.tab", "Vorschau der Schema Aenderungen");

		final PanelWizardStep step = new PanelWizardStep(localeDelegate.getMessage(
				"dbtransfer.import.step2.1", "Analyse"), 
				localeDelegate.getMessage("dbtransfer.import.step2.2", "Nuclet Import Analyse und Vorschau.")) {
			@Override
			public void prepare() {
				final LayerLock lock = ifrm.lockLayerWithProgress();
				
				Thread t = new Thread(){
					@Override
					public void run() {
						setComplete(false);
						boolean blnTransferWithWarnings = false;

						ctx.setMessageReceiver(ifrm.getId());
						try {								
							String fileName = tfTransferFile.getText();
							if (StringUtils.isNullOrEmpty(fileName)) {
								return;
							}
							File f = new File(fileName);
							long size = f.length();
	
							final byte[] transferFile;
							if (f.isDirectory()) {
								final ByteArrayOutputStream out = new ByteArrayOutputStream();
								try {
									ZipOutput zout = new ZipOutput(out);
									DBTransferUtils.writeToZip(zout, f, f);
									transferFile = out.toByteArray();
								} finally {
									out.close();
								}
							} else {
								final InputStream fin = new BufferedInputStream(new FileInputStream(f));
								try {
									transferFile = utils.getBytes(fin, (int) size);
								} finally {
									fin.close();
								}
							}
							importTransferObject = NoConnectionTimeoutRunner.runSynchronized(
									() -> getTransferFacadeRemote().prepareTransfer(transferFile, f.getName(), isNucletChangesAvaiable())
							);

							setComplete(!importTransferObject.result.hasCriticals());
							
							if (!importTransferObject.result.hasCriticals() && !importTransferObject.result.hasWarnings()) {
								editWarnings.setText(localeDelegate.getMessage(
										"configuration.transfer.prepare.no.warnings", "Keine Änderungen"));
							} else {
								editWarnings.setText(
									"<html><body><font color=\"#800000\">" + importTransferObject.result.getCriticals() + "</font>" +
									(importTransferObject.result.hasCriticals()?"<br />":"") + importTransferObject.result.getWarnings() + 
									"</body></html>");
							}
							
							int iPreviewSize = importTransferObject.getPreviewParts().size();
							blnTransferWithWarnings = setupPreviewPanel(importTransferObject.getPreviewParts(), importTransferObject.getNucletTree());
							showNucletInfoOrChangesTab(importTransferObject.result.hasCriticals());
							tabbedPane.setTitleAt(1, sDefaultPreparePreviewTabText + (iPreviewSize==0?"":" ("+iPreviewSize+")"));
							setupNucletChangePanel(importTransferObject.getNucletTree(), blnTransferWithWarnings || importTransferObject.result.hasCriticals());

							nucletChangeActionListener = new NucletChangeActionListener() {
								@Override
								public void nucletChangeActionChanged(final TransferTreeTableNode tttn) {
									setupPreviewPanel(importTransferObject.getPreviewParts(), importTransferObject.getNucletTree());
								}
							};
						} catch (Exception e) {
							Errors.getInstance().showExceptionDialog(ifrm, e);
						} finally {
							ifrm.unlockLayer(lock);
						}
						if(blnTransferWithWarnings) {
							JOptionPane.showMessageDialog(jpnPreviewContent, localeDelegate.getMessage(
									"dbtransfer.import.step1.19", "Nicht alle Statements können durchgeführt werden!\nBitte kontrollieren Sie die mit rot markierten Einträge!"), "Warning", JOptionPane.WARNING_MESSAGE);
						}
					}
				};
				t.start();
			}
		};
		
		utils.initJPanel(step,
			new double[] {TableLayout.FILL},
			new double[] {  TableLayout.FILL});
		
		editWarnings.setContentType("text/html");
		editWarnings.setEditable(false);
		editWarnings.setBackground(Color.WHITE);
		
		scrollWarn = new JScrollPane(editWarnings);
		scrollWarn.setPreferredSize(new Dimension(680, 250));
		scrollWarn.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		scrollWarn.getVerticalScrollBar().setUnitIncrement(20);
		scrollWarn.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollWarn.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollChanges = new JScrollPane(jttNucletChanges);
		scrollChanges.setPreferredSize(new Dimension(680, 250));
		scrollChanges.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		scrollChanges.getVerticalScrollBar().setUnitIncrement(20);
		scrollChanges.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollChanges.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		final JScrollPane scrollPrev = new JScrollPane(jpnPreviewContent);
		scrollPrev.setPreferredSize(new Dimension(680, 250));
		scrollPrev.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));
		scrollPrev.getVerticalScrollBar().setUnitIncrement(20);
		scrollPrev.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPrev.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		final JPanel jpnPreview = new JPanel(new BorderLayout());
		jpnPreview.add(jpnPreviewHeader, BorderLayout.NORTH);
		jpnPreview.add(scrollPrev, BorderLayout.CENTER);
		jpnPreview.add(jpnPreviewFooter, BorderLayout.SOUTH);
		jpnPreview.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		jpnPreview.setBackground(Color.WHITE);
		jpnPreviewHeader.setBackground(Color.WHITE);
		jpnPreviewContent.setBackground(Color.WHITE);
		jpnPreviewFooter.setBackground(Color.WHITE);
		tabbedPane.addTab(sDefaultPreparePreviewTabText, jpnPreview);

		step.add(tabbedPane, "0,0");
		
		return step;
	}

	private void showNucletInfoOrChangesTab(boolean bShowLog) {
		if (tabbedPane.getTabCount() == 3) {
			// with Changes and LOG tab
			tabbedPane.removeTabAt(2);
		}
		if (tabbedPane.getTabCount() == 2) {
			// remove first tab: Changes or LOG
			tabbedPane.removeTabAt(0);
		}
		MutableInt countImportChanges = new MutableInt();
		MutableInt countLocalChanges = new MutableInt();
		if (isNucletChangesAvaiable() && importTransferObject.getNucletTree() != null) {
			importTransferObject.getNucletTree().countChanges(countImportChanges, countLocalChanges);
		}
		String tabTitle = localeDelegate.getMessage("dbtransfer.import.step2.9", "Nuclet Änderungen");
		if (countImportChanges != null && countLocalChanges != null &&
				countImportChanges.intValue() > 0 || countLocalChanges.intValue() > 0) {
			final NumberFormat intFrmt = NumberFormat.getIntegerInstance();
			tabTitle += " (" + intFrmt.format(countImportChanges.intValue()) + " | " + intFrmt.format(countLocalChanges.intValue()) + ")";
		}
		final int iChangesIndex = isNucletChangesAvaiable() ? 0 : -1;
		final int iLogIndex;
		if (bShowLog || !isNucletChangesAvaiable()) {
			if (isNucletChangesAvaiable()) {
				iLogIndex = 2;
			} else {
				iLogIndex = 0;
			}
		} else {
			iLogIndex = -1;
		}
		if (iChangesIndex >= 0) {
			tabbedPane.insertTab(tabTitle, null, scrollChanges, null, iChangesIndex);
		}
		if (iLogIndex >= 0) {
			if (iLogIndex == 2) {
				tabTitle = "LOG";
			}
			tabbedPane.insertTab(tabTitle, null, scrollWarn, null, iLogIndex);
		}
		int iSelectionIndex = 0;
		if (iLogIndex == 2) {
			iSelectionIndex = iLogIndex;
		}
		tabbedPane.setSelectedIndex(iSelectionIndex);
	}

	private boolean isNucletChangesAvaiable() {
		return !bUseOldNucletImport; // ApplicationProperties.getInstance().isFunctionBlockDev();
	}

	private boolean isNucletChangeAllowOnlyLocalChanges() {
		return true;
	}

	private void setupNucletChangePanel(TransferTreeNode nucletTree, final boolean blnTransferWithWarnings) {
		if (nucletTree != null && isNucletChangesAvaiable()) {
			TransferTreeTableNode root = new TransferTreeTableNode(nucletTree, isNucletChangeAllowOnlyLocalChanges());
			List<Object> columns = new ArrayList<>();
			columns.add(localeDelegate.getMessage("dbtransfer.import.step2.10", "Objekt"));
			columns.add(localeDelegate.getMessage("dbtransfer.import.step2.11", "Import Datei"));
			columns.add(localeDelegate.getMessage("dbtransfer.import.step2.12", "Aktion"));
			columns.add(localeDelegate.getMessage("dbtransfer.import.step2.13", "Dieses System"));
			columns.add(localeDelegate.getMessage("dbtransfer.import.step2.14", "Information"));

			NucletChangesTreeTableModel ttModel = new NucletChangesTreeTableModel(root, columns);
			jttNucletChanges.setTreeTableModel(ttModel);
			jttNucletChanges.getColumnModel().getColumn(0).setPreferredWidth(240);
			jttNucletChanges.getColumnModel().getColumn(1).setMinWidth(80);
			jttNucletChanges.getColumnModel().getColumn(1).setMaxWidth(80);
			jttNucletChanges.getColumnModel().getColumn(2).setMinWidth(80);
			jttNucletChanges.getColumnModel().getColumn(2).setMaxWidth(80);
			jttNucletChanges.getColumnModel().getColumn(3).setMinWidth(80);
			jttNucletChanges.getColumnModel().getColumn(3).setMaxWidth(80);

			if (!jttNucletChangesInitiated) {
				jttNucletChanges.setToggleClickCount(1);
				jttNucletChanges.setRowSelectionAllowed(false);
				jttNucletChanges.setColumnSelectionAllowed(false);
				jttNucletChanges.setCellSelectionEnabled(false);
				jttNucletChanges.setShowGrid(true, true);
				jttNucletChanges.setRowHeight(32);
				jttNucletChanges.getTableHeader().setReorderingAllowed(false);
				jttNucletChanges.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
				jttNucletChanges.setTreeCellRenderer(new TransferTreeTableNode.TransferTreeCellRenderer());
				jttNucletChanges.addMouseListener(nucletChangesMouseListener);
				final JPopupMenu popupMenu = new JPopupMenu();
				JMenuItem copyIdItem = new JMenuItem(localeDelegate.getMessage("dbtransfer.import.step2.17", "Kopiere ID"));
				copyIdItem.addActionListener(nucletChangesCopyIdListener);
				popupMenu.add(copyIdItem);
				jttNucletChanges.setComponentPopupMenu(popupMenu);
				jttNucletChanges.addHighlighter(new ColorHighlighter(nucletChangesHlPredYellow, new Color(253, 245, 199), Color.BLACK));
				jttNucletChanges.addHighlighter(new ColorHighlighter(nucletChangesHlPredGreen, new Color(197, 231, 187), Color.BLACK));
				jttNucletChanges.addHighlighter(new ColorHighlighter(nucletChangesHlPredBlue, new Color(205, 236, 254), Color.BLACK));
				jttNucletChanges.addHighlighter(new ColorHighlighter(nucletChangesHlPredRed, new Color(232, 182, 182), Color.BLACK));
				jttNucletChanges.addHighlighter(new ColorHighlighter(nucletChangesHlPredGrey, new Color(213, 213, 213), Color.BLACK));
				jttNucletChangesInitiated = true;
			}

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					// expand first 'nuclet' level
					for (Enumeration e = ((TreeTableNode) ttModel.getRoot()).children(); e.hasMoreElements();) {
						TreeTableNode childNode = (TreeTableNode) e.nextElement();
						jttNucletChanges.expandPath(new TreePath(ttModel.getPathToRoot(childNode)));
					}
				}
			});

			if (blnTransferWithWarnings) {
				// show LOG tab with warnings or critical errors
				showNucletInfoOrChangesTab(true);
			}
		}
	}

	HighlightPredicate nucletChangesHlPredYellow = new HighlightPredicate() {
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			if (adapter.column == 1 || adapter.column == 2 || adapter.column == 3) {
				TreePath selPath = jttNucletChanges.getPathForRow(adapter.row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				final TransferTreeNode ttn = (TransferTreeNode) tttn.getUserObject();
				if (ttn.getChangeTypeImport() != null && ttn.getChangeTypeLocal() != null) {
					switch (adapter.column) {
						case 1: return !ttn.isIgnoreChange();
						case 2: return true;
						case 3: return ttn.isIgnoreChange();
						default: return false;
					}
				}
			}
			return false;
		}
	};
	HighlightPredicate nucletChangesHlPredGreen = new HighlightPredicate() {
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			if (adapter.column == 1 || adapter.column == 2 || adapter.column == 3) {
				TreePath selPath = jttNucletChanges.getPathForRow(adapter.row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				final TransferTreeNode ttn = (TransferTreeNode) tttn.getUserObject();
				if (!ttn.isIgnoreChange() && ttn.getChangeTypeLocal() == null && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.INSERT)) {
					return adapter.column == 1 || (!tttn.isAllowOnlyLocalChanges() && adapter.column == 2);
				}
				if (ttn.isIgnoreChange() && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_ADDED) && ttn.getChangeTypeImport() == null) {
					return adapter.column == 2 || adapter.column == 3;
				}
			}
			return false;
		}
	};
	HighlightPredicate nucletChangesHlPredBlue = new HighlightPredicate() {
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			if (adapter.column == 1 || adapter.column == 2 || adapter.column == 3) {
				TreePath selPath = jttNucletChanges.getPathForRow(adapter.row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				final TransferTreeNode ttn = (TransferTreeNode) tttn.getUserObject();
				if (!ttn.isIgnoreChange() && ttn.getChangeTypeLocal() == null && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.UPDATE)) {
					return adapter.column == 1 || (!tttn.isAllowOnlyLocalChanges() && adapter.column == 2);
				}
				if (ttn.isIgnoreChange() && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_CHANGES) && ttn.getChangeTypeImport() == null) {
					return adapter.column == 2 || adapter.column == 3;
				}
			}
			return false;
		}
	};
	HighlightPredicate nucletChangesHlPredRed = new HighlightPredicate() {
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			if (adapter.column == 1 || adapter.column == 2) {
				TreePath selPath = jttNucletChanges.getPathForRow(adapter.row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				final TransferTreeNode ttn = (TransferTreeNode) tttn.getUserObject();
				if (!ttn.isIgnoreChange() && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.DELETE)) {
					return adapter.column == 1 || (adapter.column == 2 &&
							(ttn.getChangeTypeLocal() != null || ttn.getChangeTypeLocal() == null && !tttn.isAllowOnlyLocalChanges()));
				}
				if (!ttn.isIgnoreChange() && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_ADDED)) {
					return adapter.column == 1 || adapter.column == 2;
				}
			}
			return false;
		}
	};
	HighlightPredicate nucletChangesHlPredGrey = new HighlightPredicate() {
		@Override
		public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
			if (adapter.column == 1 || adapter.column == 2 || adapter.column == 3) {
				TreePath selPath = jttNucletChanges.getPathForRow(adapter.row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				final TransferTreeNode ttn = (TransferTreeNode) tttn.getUserObject();
				if (ttn.isIgnoreChange() && ttn.getChangeTypeLocal() == null && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.DELETE)) {
					return (!tttn.isAllowOnlyLocalChanges() && adapter.column == 2) || adapter.column == 3;
				}
				if (ttn.isIgnoreChange() && ttn.getChangeTypeLocal() == null && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_IMPORT.UPDATE)) {
					return (!tttn.isAllowOnlyLocalChanges() && adapter.column == 2) || adapter.column == 3;
				}
				if (!ttn.isIgnoreChange() && ttn.isChangeType(TransferTreeNode.CHANGE_TYPE_LOCAL.LOCAL_CHANGES) && ttn.getChangeTypeImport() == null) {
					return adapter.column == 1 || adapter.column == 2;
				}
			}
			return false;
		}
	};

	private class NucletChangesTreeTableModel extends DefaultTreeTableModel {
		public NucletChangesTreeTableModel(final TreeTableNode root, final List<?> columnNames) {
			super(root, columnNames);
		}
		@Override
		public Class<?> getColumnClass(final int column) {
			if (column == 1 || column == 2 || column == 3) {
				return ImageIcon.class;
			}
			return super.getColumnClass(column);
		}
		public void fireUpdate(TreePath path) {
			modelSupport.firePathChanged(path);
		}
		public void fireUpdateForParentNodes(TreePath path) {
			TreeTableNode lastNode = (TreeTableNode) path.getLastPathComponent();
			if (lastNode.getParent() != null) {
				TreePath parentPath = new TreePath(getPathToRoot(lastNode.getParent()));
				fireUpdate(parentPath);
				fireUpdateForParentNodes(parentPath);
			}
		}
		public void fireUpdateForAllChildren(TreePath path) {
			TreeTableNode lastNodeFromPath = (TreeTableNode) path.getLastPathComponent();
			for (Enumeration e = lastNodeFromPath.children(); e.hasMoreElements();) {
				TreeTableNode childNode = (TreeTableNode) e.nextElement();
				TreePath childPath = new TreePath(getPathToRoot(childNode));
				fireUpdate(childPath);
				fireUpdateForAllChildren(childPath);
			}
		}
	};

	private MouseListener nucletChangesMouseListener = new MouseAdapter() {
		@Override
		public void mouseReleased(final MouseEvent evt) {
			if (evt.isConsumed()) {
				return;
			}
			NucletChangesTreeTableModel ttModel = (NucletChangesTreeTableModel) jttNucletChanges.getTreeTableModel();
			int row = jttNucletChanges.rowAtPoint(evt.getPoint());
			int col = jttNucletChanges.columnAtPoint(evt.getPoint());
			if (row >= 0 && (col == 2)) {
				TreePath selPath = jttNucletChanges.getPathForRow(row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				final TransferTreeNode ttn = (TransferTreeNode) tttn.getUserObject();
				if (!SwingUtilities.isLeftMouseButton(evt)) {
					return;
				}
				if (evt.isAltDown() && evt.getClickCount() == 1) {
					tttn.setAllowOnlyLocalChanges(!tttn.isAllowOnlyLocalChanges());
					ttModel.fireUpdate(selPath);
					ttModel.fireUpdateForAllChildren(selPath);
					evt.consume();
					return;
				}
				if (ttn.getChangeTypeLocal() == null && tttn.isAllowOnlyLocalChanges()) {
					return;
				}
				ttn.setIgnoreChange(!ttn.isIgnoreChange());
				ttModel.fireUpdate(selPath);
				ttModel.fireUpdateForParentNodes(selPath);
				ttModel.fireUpdateForAllChildren(selPath);
				evt.consume();
				if (nucletChangeActionListener != null) {
					nucletChangeActionListener.nucletChangeActionChanged(tttn);
				}
			}
			if (row >= 0 && col == 0) {
				if (!SwingUtilities.isLeftMouseButton(evt)) {
					return;
				}
				TreePath selPath = jttNucletChanges.getPathForRow(row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				if (jttNucletChanges.isExpanded(selPath)) {
					jttNucletChanges.collapsePath(selPath);
				} else {
					jttNucletChanges.expandPath(selPath);
				}
				evt.consume();
			}
		}
	};

	private ActionListener nucletChangesCopyIdListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Point p = (Point) jttNucletChanges.getClientProperty("popupTriggerLocation");
			int row = jttNucletChanges.rowAtPoint(p);
			int col = jttNucletChanges.columnAtPoint(p);
			if (row >= 0 && col == 0) {
				TreePath selPath = jttNucletChanges.getPathForRow(row);
				final TransferTreeTableNode tttn = (TransferTreeTableNode) selPath.getLastPathComponent();
				final TransferTreeNode ttn = (TransferTreeNode) tttn.getUserObject();
				if (ttn.getTransferEO() != null) {
					StringSelection stringSelection = new StringSelection(ttn.getTransferEO().getUID().getString());
					Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
					clpbrd.setContents(stringSelection, null);
				}
			}
		}
	};

	private interface NucletChangeActionListener {
		void nucletChangeActionChanged(TransferTreeTableNode tttn);
	}
	
	private boolean setupPreviewPanel(List<PreviewPart> previewParts, final TransferTreeNode nucletTree) {
		boolean blnTransferWithWarnings = false;
		jpnPreviewHeader.removeAll();
		jpnPreviewFooter.removeAll();
		
		// setup parameter scroll pane
		jpnPreviewContent.removeAll();
		
		double[] rowContraints = new double[previewParts.size()];
		for (int i = 0; i < previewParts.size(); i++)
			rowContraints[i] = TableLayout.PREFERRED;
		final int iWidthBeginnigSpace = 3;
		final int iWidthSeparator = 6;
		
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		JLabel lbPreviewHeaderEntity = new JLabel(localeDelegate.getMessage("dbtransfer.import.step1.11", "Entit\u00e4t"));
		JLabel lbPreviewHeaderTable = new JLabel(localeDelegate.getMessage("dbtransfer.import.step1.12", "Tabellenname"));
		JLabel lbPreviewHeaderRecords = new JLabel(localeDelegate.getMessage("dbtransfer.import.step1.4", "Datens\u00e4tze"));
		lbPreviewHeaderRecords.setToolTipText(localeDelegate.getMessage("dbtransfer.import.step1.5", "Anzahl der betroffenen Datens\u00e4tze"));
		utils.initJPanel(jpnPreviewContent, 
			new double[] {iWidthBeginnigSpace, TableLayout.PREFERRED, iWidthSeparator, TableLayout.PREFERRED, iWidthSeparator, TableLayout.PREFERRED, iWidthSeparator, TableLayout.PREFERRED, TableLayout.PREFERRED, iWidthSeparator, TableLayout.PREFERRED},
			rowContraints);
		
		int iWidthEntityLabelSize = 0;
		int iWidthTableLabelSize = 0;
		int iWidthRecordsLabelSize = 0;
		
		int iCountNew = 0;
		int iCountDeleted = 0;
		int iCountChanged = 0;
		int iCountIgnored = 0;
		
		int iRow = 0;
		for (final PreviewPart pp : previewParts) {
			final UID entityUID = pp.getEntityUID();
			boolean bChangeIgnored = false;
			if (entityUID != null && nucletTree != null && isNucletChangesAvaiable()) {
				// search table in tree
				final TransferTreeNode entityNode = nucletTree.getChild(E.ENTITY.getUID(), entityUID);
				if (entityNode.isIgnoreChange()) {
					bChangeIgnored = true;
				}
				if (!bChangeIgnored) {
					for (int i = 0; i < entityNode.getChildCount(); i++) {
						final TransferTreeNode entityChildNode = entityNode.getChildAt(i);
						if (entityChildNode.isIgnoreChange() &&
								entityChildNode.getTransferEO().eo.getDalEntity().equals(E.ENTITYFIELD.getUID())) {
							bChangeIgnored = true;
							break;
						}
					}
				}
			}
			String tooltip = "";
			JLabel lbEntity = new JLabel(pp.getEntity());
			JLabel lbTable = new JLabel(pp.getTable());
			JLabel lbRecords = new JLabel(String.valueOf(pp.getDataRecords()));
			lbRecords.setHorizontalAlignment(SwingConstants.RIGHT);
			if (lbEntity.getPreferredSize().width<lbPreviewHeaderEntity.getPreferredSize().width)
				lbEntity.setPreferredSize(lbPreviewHeaderEntity.getPreferredSize());
			if (lbTable.getPreferredSize().width<lbPreviewHeaderTable.getPreferredSize().width)
				lbTable.setPreferredSize(lbPreviewHeaderTable.getPreferredSize());
			if (lbRecords.getPreferredSize().width<lbPreviewHeaderRecords.getPreferredSize().width)
				lbRecords.setPreferredSize(lbPreviewHeaderRecords.getPreferredSize());
			iWidthEntityLabelSize = iWidthEntityLabelSize<lbEntity.getPreferredSize().width?lbEntity.getPreferredSize().width:iWidthEntityLabelSize;
			iWidthTableLabelSize = iWidthTableLabelSize<lbTable.getPreferredSize().width?lbTable.getPreferredSize().width:iWidthTableLabelSize;
			iWidthRecordsLabelSize = iWidthRecordsLabelSize<lbRecords.getPreferredSize().width?lbRecords.getPreferredSize().width:iWidthRecordsLabelSize;
			
			Icon icoStatement = null;
			switch (pp.getType()) {
			case PreviewPart.NEW:
				tooltip = localeDelegate.getMessage("dbtransfer.import.step1.6", "Entit\u00e4t wird hinzugef\u00fcgt");
				icoStatement = Icons.getInstance().getIconNucletChangeContentAdded16(); //ParameterEditor.COMPARE_ICON_NEW;
				if (!bChangeIgnored) {
					iCountNew++;
				}
				break;
			case PreviewPart.CHANGE:
				tooltip = localeDelegate.getMessage("dbtransfer.import.step1.7", "Entit\u00e4t wird ge\u00e4ndert");
				icoStatement = Icons.getInstance().getIconNucletChangeContentModified16(); //ParameterEditor.COMPARE_ICON_VALUE_CHANGED;
				if (!bChangeIgnored) {
					iCountChanged++;
				}
				break;
			case PreviewPart.DELETE:
				tooltip = localeDelegate.getMessage("dbtransfer.import.step1.8", "Entit\u00e4t wird gel\u00f6scht");
				icoStatement = Icons.getInstance().getIconNucletChangeContentDeleted16(); //ParameterEditor.COMPARE_ICON_DELETED;
				if (!bChangeIgnored) {
					iCountDeleted++;
				}
				break;
			}

			if (bChangeIgnored) {
				iCountIgnored++;
			}
			
			JLabel lbIcon = new JLabel(icoStatement);
			JLabel lbStatemnts = new JLabel("<html><u>" + localeDelegate.getMessage("dbtransfer.import.step1.9", "Script anzeigen") + "...</u></html>");
			lbStatemnts.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));			
			lbStatemnts.addMouseListener(new MouseListener() {
				@Override
				public void mouseReleased(MouseEvent e) {}
				@Override
				public void mousePressed(MouseEvent e) {}
				@Override
				public void mouseExited(MouseEvent e) {}
				@Override
				public void mouseEntered(MouseEvent e) {}
				@Override
				public void mouseClicked(MouseEvent e) {
					String statements = "";
					for (String statement : pp.getStatements()) {
						statements = statements+statement+";\n\n";
					}
					JTextArea txtArea = new JTextArea(statements);
					
					txtArea.setEditable(false);
					txtArea.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
					JScrollPane scroll = new JScrollPane(txtArea);
					scroll.getVerticalScrollBar().setUnitIncrement(20);
					scroll.setPreferredSize(new Dimension(600, 300));
					scroll.setBorder(BorderFactory.createEmptyBorder());
					MainFrameTab overlayFrame = new MainFrameTab(localeDelegate.getMessage(
							"dbtransfer.import.step1.10", "Script f\u00fcr") + " " + pp.getEntity() + " (" + pp.getTable() + ")");
					overlayFrame.setLayeredComponent(scroll);
					ifrm.add(overlayFrame);
				}
			});

			lbIcon.setToolTipText(tooltip);
			lbStatemnts.setToolTipText(tooltip);
			lbEntity.setToolTipText(tooltip);
			lbTable.setToolTipText(tooltip);

			if (bChangeIgnored) {
				lbStatemnts = new JLabel(localeDelegate.getMessage(
						"dbtransfer.import.step2.15", "Nicht akzeptierte Änderungen"));
			}
			
			jpnPreviewContent.add(lbEntity, "1," + iRow + ",l,c");
			jpnPreviewContent.add(lbTable, "3," + iRow + ",l,c");
			jpnPreviewContent.add(lbRecords, "5," + iRow + ",r,c");
			jpnPreviewContent.add(lbIcon, "7," + iRow + ",l,c");
			jpnPreviewContent.add(lbStatemnts, "8," + iRow + ",l,c");

			if(pp.getWarning() > 0) {
				lbIcon.setIcon(Icons.getInstance().getIconPriorityCancel16());
				blnTransferWithWarnings = true;
			}
			iRow++;
		}
		
		jpnPreviewContent.add(new JSeparator(JSeparator.VERTICAL), "2,0,2,"+(iRow-1));
		jpnPreviewContent.add(new JSeparator(JSeparator.VERTICAL), "4,0,4,"+(iRow-1));
		jpnPreviewContent.add(new JSeparator(JSeparator.VERTICAL), "6,0,6,"+(iRow-1));
		
		// setup preview header	
		utils.initJPanel(jpnPreviewHeader, 
			new double[] {iWidthBeginnigSpace, iWidthEntityLabelSize, iWidthSeparator, iWidthTableLabelSize, iWidthSeparator, iWidthRecordsLabelSize, iWidthSeparator, TableLayout.PREFERRED, iWidthSeparator, TableLayout.PREFERRED, TableLayout.PREFERRED}, 
			new double[] {TableLayout.PREFERRED});
		
		if (previewParts.isEmpty()) {
			jpnPreviewHeader.add(new JLabel(localeDelegate.getMessage(
					"dbtransfer.import.step1.18", "Keine Struktur\u00e4nderungen am Datenbankschema.")), "0,0,8,0");
			return blnTransferWithWarnings;
		}
		
		jpnPreviewHeader.add(lbPreviewHeaderEntity, "1,0");
		jpnPreviewHeader.add(lbPreviewHeaderTable, "3,0");
		jpnPreviewHeader.add(lbPreviewHeaderRecords, "5,0");		
		
		jpnPreviewHeader.add(new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.step1.13", "\u00c4nderung")), "7,0");

		// setup preview footer
		utils.initJPanel(jpnPreviewFooter, 
			new double[] {TableLayout.FILL, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED},
			new double[] {TableLayout.PREFERRED});
		
		final JLabel lbCompare = new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.step1.14", "\u00c4nderungen")+":");
		final JLabel lbCompareNew = new JLabel(iCountNew+"");
		final JLabel lbCompareDeleted = new JLabel(iCountDeleted+"");
		final JLabel lbCompareValueChanged = new JLabel(iCountChanged+"");
		final JLabel lbCompareIgnored = new JLabel(iCountIgnored+"");
		
		lbCompareNew.setIcon(Icons.getInstance().getIconNucletChangeContentAdded16()); //ParameterEditor.COMPARE_ICON_NEW);
		lbCompareDeleted.setIcon(Icons.getInstance().getIconNucletChangeContentDeleted16()); //ParameterEditor.COMPARE_ICON_DELETED);
		lbCompareValueChanged.setIcon(Icons.getInstance().getIconNucletChangeContentModified16()); //ParameterEditor.COMPARE_ICON_VALUE_CHANGED);
		lbCompareIgnored.setIcon(Icons.getInstance().getIconNucletChangeActionIgnore16());
		
		lbCompareNew.setToolTipText(localeDelegate.getMessage(
				"dbtransfer.import.step1.15", "Neue Entit\u00e4ten"));
		lbCompareDeleted.setToolTipText(localeDelegate.getMessage(
				"dbtransfer.import.step1.17", "Gel\u00f6schte Entit\u00e4ten"));
		lbCompareValueChanged.setToolTipText(localeDelegate.getMessage(
				"dbtransfer.import.step1.16", "Ge\u00e4nderte Entit\u00e4ten"));
		lbCompareIgnored.setToolTipText(localeDelegate.getMessage(
				"dbtransfer.import.step2.16", "Nicht akzeptierte Änderungen"));
		
		jpnPreviewFooter.add(lbCompare, "1,0,r,c");
		jpnPreviewFooter.add(lbCompareNew, "2,0,r,c");
		jpnPreviewFooter.add(lbCompareValueChanged, "3,0,r,c");
		jpnPreviewFooter.add(lbCompareDeleted, "4,0,r,c");
		jpnPreviewFooter.add(lbCompareIgnored, "5,0,r,c");
		
		return blnTransferWithWarnings;		
	}
	
	/*
	 * Begin Step 3
	 */
	private final JPanel jpnParameter = new JPanel();
	private final JPanel jpnParameterHeader = new JPanel();
	private final JPanel jpnParameterFooter = new JPanel();
	private final JRadioButton rbCurrentAll = new JRadioButton();
	private final JRadioButton rbIncomingAll = new JRadioButton();
	
	private final Set<String> setSelectedIncomingParameter = new HashSet<String>();
	private final Set<String> setSelectedCurrentParameter = new HashSet<String>();
	private final Map<String, String> mapSelectedOtherParameter = new HashMap<String, String>();
	
	private final List<ParameterEditor> lstParameterEditors = new ArrayList<ParameterEditor>();
	
	private PanelWizardStep newStep3(final MainFrameTab ifrm) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final PanelWizardStep step = new PanelWizardStep(localeDelegate.getMessage(
				"dbtransfer.import.step3.1", "Nuclet Parameter"), 
				localeDelegate.getMessage(
						"dbtransfer.import.step3.2", "Bestimmen Sie die Parameter für dieses Nuclet. Sie k\u00f6nnen w\u00e4hlen zwischen dem aktuellen Zustand und dem aus der Konfigurationsdatei importierten Zustand (default). Sollte keine der beiden Vorgaben stimmen, so k\u00f6nnen Sie auch einen anderen Wert setzen.")){

				@Override
				public void prepare() {
					setupParameterPanel(importTransferObject.getExistingNucletUIDs(), importTransferObject.getParameter());
				}
				
				@Override
				public void applyState() throws InvalidStateException {
					setSelectedIncomingParameter.clear();
					setSelectedCurrentParameter.clear();
					mapSelectedOtherParameter.clear();
					
					for (ParameterEditor pe : lstParameterEditors) {
						if (pe.isCurrentValue())
							setSelectedCurrentParameter.add(pe.getName());
						if (pe.isIncomingValue())
							setSelectedIncomingParameter.add(pe.getName());
						if (pe.isOtherValue())
							mapSelectedOtherParameter.put(pe.getName(), pe.getValue());
					}
				}
		};
		step.setComplete(true);
		
		utils.initJPanel(step,
			new double[] {TableLayout.FILL},
			new double[] {TableLayout.PREFERRED,
							  TableLayout.FILL,
							  TableLayout.PREFERRED});
		
		rbCurrentAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resetParameterEditors(true);
			}
		});
		rbIncomingAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				resetParameterEditors(false);
			}
		});
		
		final JScrollPane scroll = new JScrollPane(jpnParameter);
		scroll.setPreferredSize(new Dimension(680, 250));
		scroll.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));
		scroll.getVerticalScrollBar().setUnitIncrement(20);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.getViewport().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				Dimension parameterPanelSize = new Dimension(scroll.getViewport().getWidth(), jpnParameter.getPreferredSize().height);
				jpnParameter.setPreferredSize(parameterPanelSize);
			}
		});
		
		step.add(jpnParameterHeader, "0,0");
		step.add(scroll, "0,1");
		step.add(jpnParameterFooter, "0,2");
		
		return step;
	}
	
	private void setupParameterPanel(Set<UID> existingNucletUIDs, Collection<EntityObjectVO<UID>> incomingParameterVOs) {
		List<EntityObjectVO<UID>> currentParameterVOs = new ArrayList<EntityObjectVO<UID>>();
		if (existingNucletUIDs != null) {
			for (UID nucletUID : existingNucletUIDs) {
				currentParameterVOs.addAll(CollectionUtils.transform(
						MasterDataDelegate.getInstance().<UID>getMasterData(E.NUCLETPARAMETER.getUID(), 
								SearchConditionUtils.newUidComparison(E.NUCLETPARAMETER.nuclet, ComparisonOperator.EQUAL, nucletUID)), 
						DalSupportForMD.<UID>getTransformerToEntityObjectVO()));
			}
		}
		
		int iCountNew = 0;
		int iCountValueChanged = 0;
		
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		
		// find new parameter and existing
		List<ParameterComparison> allParameter = new ArrayList<ParameterComparison>();
		for (EntityObjectVO<UID> incomingParameterVO : incomingParameterVOs) {
			String nucletPackage = incomingParameterVO.getFieldValue(E.NUCLETPARAMETER.nuclet.getUID(), String.class);
			boolean blnParamFound = false;
			String sCurrentValue = null;
			for (EntityObjectVO<UID> currentParameterVO : currentParameterVOs) {
				if (incomingParameterVO.getPrimaryKey().equals(currentParameterVO.getPrimaryKey())) {
					blnParamFound = true;
					sCurrentValue = currentParameterVO.getFieldValue(E.NUCLETPARAMETER.value);
					break;
				}
			}
			if (!blnParamFound) {
				allParameter.add(new ParameterComparison(incomingParameterVO, true, null, nucletPackage));
				iCountNew++;
			} else {
				ParameterComparison parameter = new ParameterComparison(incomingParameterVO, false, sCurrentValue, nucletPackage);
				allParameter.add(parameter);
				if (parameter.isValueChanged())
					iCountValueChanged++;
			}
		}
		
		// sort parameter VOs
		List<ParameterComparison> allParameterSorted = new ArrayList<ParameterComparison>(allParameter);
		Collections.sort(allParameterSorted, new Comparator<ParameterComparison>() {
			@Override
			public int compare(ParameterComparison o1, ParameterComparison o2) {
				return o1.getTitle().compareTo(o2.getTitle());
			}});
		
		jpnParameterHeader.removeAll();
		jpnParameterFooter.removeAll();
		
		// setup parameter scroll pane
		jpnParameter.removeAll();
		
		double[] rowContraints = new double[allParameterSorted.size()];
		for (int i = 0; i < allParameterSorted.size(); i++)
			rowContraints[i] = TableLayout.PREFERRED;
		final int iWidthBeginnigSpace = 3;
		final int iWidthSeparator = 6;
		final int iWidthRadioButton = 20;
		
		utils.initJPanel(jpnParameter, 
			new double[] {iWidthBeginnigSpace, TableLayout.PREFERRED, iWidthSeparator, iWidthRadioButton, iWidthRadioButton, iWidthRadioButton, TableLayout.FILL},
			rowContraints);
		
		int iWidthLabelSize = 0;
		
		int iRow = 0;
		lstParameterEditors.clear();
		for (ParameterComparison parameter : allParameterSorted) {
			final ParameterEditor pe = new ParameterEditor(parameter);
			
			// init last state
			if (setSelectedCurrentParameter.contains(pe.getName()))
				pe.setCurrentValue();
			if (setSelectedIncomingParameter.contains(pe.getName()))
				pe.setIncomingValue();
			if (mapSelectedOtherParameter.containsKey(pe.getName()))
				pe.setOtherValue(mapSelectedOtherParameter.get(pe.getName()));
			
			// add change listener to update the radio buttons in footer
			pe.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					updateParameterAllSelection();
				}
			});
			
			pe.addToStepContent(jpnParameter, iRow);
			lstParameterEditors.add(pe);
			
			if (pe.getLabelPrefferedSize().width > iWidthLabelSize)
				iWidthLabelSize = pe.getLabelPrefferedSize().width;
			iRow++;
		}
		updateParameterAllSelection();
		jpnParameter.add(new JSeparator(JSeparator.VERTICAL), "2,0,2,"+(iRow-1));
		
		// setup parameter header	
		utils.initJPanel(jpnParameterHeader, 
			new double[] {iWidthBeginnigSpace, iWidthLabelSize+iWidthSeparator+(iWidthRadioButton/2)+utils.TABLE_LAYOUT_H_GAP+1/*Border*/, iWidthRadioButton, iWidthRadioButton, TableLayout.FILL}, 
			new double[] {TableLayout.PREFERRED,
							  TableLayout.PREFERRED,
							  TableLayout.PREFERRED,
							  4});
		
		if (allParameterSorted.isEmpty()) {
			jpnParameterHeader.add(new JLabel(localeDelegate.getMessage(
					"dbtransfer.import.parameterpanel.18", "Keine Parameter vorhanden.")), "1,2,4,2");
			return;
		}
		
		jpnParameterHeader.add(new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.parameterpanel.1", "Parameter")), "1,2");
		jpnParameterHeader.add(new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.parameterpanel.2", "aktuellen Zustand beibehalten")), "2,0,4,0");
		jpnParameterHeader.add(new JSeparator(JSeparator.VERTICAL), "2,1,2,3");
		jpnParameterHeader.add(new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.parameterpanel.3", "importierten Zustand \u00fcbernehmen")), "3,1,4,1");
		jpnParameterHeader.add(new JSeparator(JSeparator.VERTICAL), "3,2,3,3");
		jpnParameterHeader.add(new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.parameterpanel.4", "anderen Wert setzen")), "4,2");
		jpnParameterHeader.add(new JSeparator(JSeparator.VERTICAL), "4,3");
		
		// setup parameter footer		
		utils.initJPanel(jpnParameterFooter, 
			new double[] {iWidthBeginnigSpace, iWidthLabelSize+iWidthSeparator+utils.TABLE_LAYOUT_H_GAP+1/*Border*/, iWidthRadioButton, iWidthRadioButton, TableLayout.FILL, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED}, 
			new double[] {TableLayout.PREFERRED});
		
		final JLabel lbCompare = new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.parameterpanel.5", "\u00c4nderungen")+":");
		final JLabel lbCompareNew = new JLabel(iCountNew+"");
		final JLabel lbCompareValueChanged = new JLabel(iCountValueChanged+"");
		
		lbCompareNew.setIcon(ParameterEditor.COMPARE_ICON_NEW);
		lbCompareValueChanged.setIcon(ParameterEditor.COMPARE_ICON_VALUE_CHANGED);
		
		lbCompare.setToolTipText(localeDelegate.getMessage(
				"dbtransfer.import.parameterpanel.6", "Vergleich von Aktueller- und Importkonfiguration"));
		lbCompareNew.setToolTipText(ParameterEditor.COMPARE_DESCRIPTION_NEW);
		lbCompareValueChanged.setToolTipText(ParameterEditor.COMPARE_DESCRIPTION_VALUE_CHANGED);
		
		jpnParameterFooter.add(new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.parameterpanel.7", "auf alle Parameter anwenden")), "1,0,r,c");
		jpnParameterFooter.add(rbCurrentAll, "2,0,l,c");
		jpnParameterFooter.add(rbIncomingAll, "3,0,l,c");
		jpnParameterFooter.add(lbCompare, "4,0,r,c");
		jpnParameterFooter.add(lbCompareNew, "5,0,r,c");
		jpnParameterFooter.add(lbCompareValueChanged, "6,0,r,c");
	}
	
	private void resetParameterEditors(boolean toCurrent) {
		for (ParameterEditor pe : lstParameterEditors) {
			pe.reset(toCurrent);
		}
	}
	
	private void updateParameterAllSelection() {
		boolean blnIsCurrentAll = true;
		boolean blnIsIncomingAll = true;
		boolean blnIsNothingChanged = true;
		
		for (ParameterEditor pe : lstParameterEditors) {
			if (pe.isOtherValue()) {
				blnIsCurrentAll = false;
				blnIsIncomingAll = false;
				break;
			}
			if (pe.isCurrentValue() && pe.getParameter().isValueChanged())
				blnIsIncomingAll = false;
			if (pe.isIncomingValue() && pe.getParameter().isValueChanged() && !pe.getParameter().isNew())
				blnIsCurrentAll = false;
			
			if (pe.getParameter().isValueChanged())
				blnIsNothingChanged = false;
		}
		
		//zeige nur einen Button wenn Auswahl f\u00fcr beide gleich
		if (blnIsCurrentAll && blnIsIncomingAll) {
			if (blnIsNothingChanged) {
				rbIncomingAll.setVisible(false);
			} else {
				rbCurrentAll.setVisible(false);
			}
		}

		rbCurrentAll.setSelected(blnIsCurrentAll);
		rbIncomingAll.setSelected(blnIsIncomingAll);
	}
	
	/*
	 * Begin Step 4
	 */
	private PanelWizardStep newStep4(final MainFrameTab ifrm) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final PanelWizardStep step = new PanelWizardStep(localeDelegate.getMessage(
				"dbtransfer.import.step4.1", "Import"), 
				localeDelegate.getMessage(
						"dbtransfer.import.step4.2", "Der Import der Konfiguration kann nun gestartet werden."));
		
		utils.initJPanel(step,
			new double[] {180, TableLayout.FILL},
			new double[] {TableLayout.PREFERRED,
							  10,
							  TableLayout.PREFERRED,
							  TableLayout.PREFERRED});
		
		final JLabel lbWarning = new JLabel("<html>"+ localeDelegate.getMessage(
				"dbtransfer.import.step4.3", " <b>Achtung</b>: Durch den Konfigurationsimport werden Teile der Konfiguration auf diesem System \u00fcberschrieben!<br>" +
			"Vor dem Import wird dringend empfohlen, ein Backup der Datenbank durchzuf\u00fchren.<br><br>" +
			"Wollen Sie fortfahren und den Konfigurationsimport starten?")+"</html>");
		final JButton btnStartImport = new JButton(localeDelegate.getMessage(
				"dbtransfer.import.step4.4", "importieren")+"...");
//		final JProgressBar progressBar = new JProgressBar(0, 200);
		
		step.add(lbWarning, "0,0,1,0");
		step.add(btnStartImport, "0,2");
//		step.add(progressBar, "0,3");
		
		btnStartImport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				final LayerLock lock = ifrm.lockLayerWithProgress();
				
				final Thread t = new Thread(){
					@Override
					public void run() {
						try {
							btnStartImport.setEnabled(false);
							blnImportStarted = true;
							wizard.getModel().refreshModelState();
							ctx.setMessageReceiver(ifrm.getId());
							
							// start configuration import
							
							// create new parameter list
							Collection<EntityObjectVO<UID>> parameterSelection = new ArrayList<EntityObjectVO<UID>>();
							for (ParameterEditor pe : lstParameterEditors) {
								pe.getParameter().setFieldValue(E.NUCLETPARAMETER.value, pe.getValue());
								parameterSelection.add(pe.getParameter());
							}
							
							// copy transfer to add parameter list.
							// only temporary in case we go back one step, then we need the old list. 
							final Transfer transfer = new Transfer(importTransferObject);
							transfer.setParameter(parameterSelection);
							
							importTransferResult = NoConnectionTimeoutRunner.runSynchronized(
									() -> getTransferFacadeRemote().runTransfer(transfer)
							);
							
							// Nicht invalidateAllClientCaches() aufrufen! Nach einem Aufruf sind die Menus solange deaktiviert, 
							// bis alle NovabitInternalFrames geschlossen wurden... BUG?
							// 
							// Main.getMainController().invalidateAllClientCaches();
							
							// NUCLOS-3544:
							// At least do the same as when finishing the entity wizard in
							// org.nuclos.client.wizard.steps.NuclosEntitySQLLayoutStep.createOrModifyEntity().
							// (tp)
							EntityUtils.clearAllEntityCaches();
							
							// Create BusinessObjects; show errors but don't stop import
							try {
								EventSupportDelegate.getInstance().invalidateCaches();
								GeneratorActions.invalidateCache();
								EventSupportDelegate.getInstance().createBusinessObjects();	
							} catch (CommonBusinessException | InterruptedException e) {
								Errors.getInstance().showExceptionDialog(ifrm, e);
							}						

							notifyParent.actionPerformed(new ActionEvent(DBTransferImport.this, 0, IMPORT_EXECUTED));
							step.setComplete(true);
							model.nextStep();
						} catch (Exception e) {
							btnStartImport.setEnabled(true);
							blnImportStarted = false;
							wizard.getModel().refreshModelState();
							Errors.getInstance().showExceptionDialog(ifrm, e);
						} finally {
							ifrm.unlockLayer(lock);
						}
					}
				};
				t.start();				
			}
		});
		return step;
	}
	
	/*
	 * Begin Step 5
	 */
	private PanelWizardStep newStep5(final MainFrameTab ifrm) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final JLabel lbResult = new JLabel();
		final JEditorPane editLog = new JEditorPane();
		final JScrollPane scrollLog = new JScrollPane(editLog);
		final JLabel lbStructureChangeResult = new JLabel(localeDelegate.getMessage(
				"dbtransfer.import.step5.1", "\u00c4nderungen am Datenbankschema"));
		final JButton btnSaveStructureChangeScript = new JButton(localeDelegate.getMessage(
				"dbtransfer.import.step5.2", "Script speichern")+"...");
		final JButton btnSaveStructureChangeLog = new JButton(localeDelegate.getMessage(
				"dbtransfer.import.step5.3", "Log speichern")+"...");
		
		editLog.setContentType("text/html");
		editLog.setEditable(false);
		
		final PanelWizardStep step = new PanelWizardStep(localeDelegate.getMessage("dbtransfer.import.step5.4", "Ergebnis"), 
				localeDelegate.getMessage("dbtransfer.import.step5.5", "Hier wird Ihnen das Ergebnis des Imports angezeigt.")){

				@Override
				public void prepare() {
					if (!importTransferResult.hasCriticals()){
						lbResult.setText(localeDelegate.getMessage("dbtransfer.import.step5.6", "Import erfolgreich!"));
						this.setComplete(true);	
					} else {
						lbResult.setText(localeDelegate.getMessage("dbtransfer.import.step5.7", "Ein Problem ist aufgetreten!"));
						blnSaveOfLogRecommend = true;
					}
					SwingUtilities.invokeLater(new Runnable() {
						
						@Override
						public void run() {
							StringBuffer sbLog = new StringBuffer();
							sbLog.append("<html><body>");
							if (!importTransferResult.foundReferences.isEmpty()) {
								sbLog.append(localeDelegate.getMessage(
										"dbtransfer.import.step5.8", "Folgende Konfigurationsobjekte sollten entfernt werden, werden aber noch verwendet") + ":<br />");
								for (Pair<UID, String> reference : importTransferResult.foundReferences) {
									String sEntity = reference.x.getStringifiedDefinition();
									try {
										sEntity = localeDelegate.getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(reference.x));
									} catch (Exception ex) {
										// cache invalidation
										Log.error(ex.getMessage(), ex);
									}
									sbLog.append("- " + reference.y + " (" 
											+ sEntity + ")<br />");
								}
								sbLog.append("<br />" + localeDelegate.getMessage(
										"dbtransfer.import.step5.9", "Passen Sie Ihre Konfiguration dahingehend an oder bearbeiten Sie die Daten,\nwelche noch auf die Konfigurationsobjekte verweisen."));
								sbLog.append("<br />");
							}
							sbLog.append("<font color=\"#800000\">" + importTransferObject.result.getCriticals() + "</font>" +
								(importTransferObject.result.hasCriticals()?"<br />":""));
							sbLog.append(importTransferResult.getWarnings());
							sbLog.append("</body></html>");
							editLog.setText(sbLog.toString());
						}
					});
				}
		};
		
		utils.initJPanel(step,
			new double[] {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.FILL},
			new double[] {20,
							  TableLayout.FILL,
							  TableLayout.PREFERRED});
		
		step.add(lbResult, "0,0,3,0");
		step.add(scrollLog, "0,1,3,1");
		step.add(lbStructureChangeResult, "0,2");
		step.add(btnSaveStructureChangeScript, "1,2");
		step.add(btnSaveStructureChangeLog, "2,2");
		
		btnSaveStructureChangeScript.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				Future<LayerLock> lock = null;
				try {
					lock = UIUtils.lockFrame(ifrm);
					
					final StringBuffer sbSql = new StringBuffer();
					org.apache.commons.collections15.CollectionUtils.forAllDo(importTransferResult.script, new Closure<Object>() {
						@Override
						public void execute(Object element) {
							sbSql.append("<DDL>" + element + "</DDL>\n\n");
						}
					});
					
					final JFileChooser filechooser = utils.getFileChooser(localeDelegate.getMessage(
							"configuration.transfer.file.sql", "SQL-Dateien"), ".import-sql.txt", false);
					filechooser.setSelectedFile(new File(tfTransferFile.getText() + ".import-sql.txt"));
					final int iBtn = filechooser.showSaveDialog(step);

					if (iBtn == JFileChooser.APPROVE_OPTION) {
						final File file = filechooser.getSelectedFile();
						if (file != null) {
							String fileName = file.getPath();

							if (!fileName.toLowerCase().endsWith(".import-sql.txt")) {
								fileName += ".import-sql.txt";
							}

							File outFile = new File(fileName);
							final Writer out = new BufferedWriter(new FileWriter(outFile));
							try {
								out.write(sbSql.toString());
							}
							finally {
								out.close();
							}
							if (blnSaveOfScriptRecommend) {
								step.setComplete(true);
							}
						}
					}
				}
				catch (Exception e) {
					Errors.getInstance().showExceptionDialog(ifrm, e);
				}
				finally {
					UIUtils.unLockFrame(ifrm, lock);
				}
			}
		});
		btnSaveStructureChangeLog.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				Future<LayerLock> lock = null;
				try {
					lock = UIUtils.lockFrame(ifrm);
					
					final JFileChooser filechooser = utils.getFileChooser(localeDelegate.getMessage(
							"configuration.transfer.file.log", "Log-Dateien"), ".import-log.html", false);
					filechooser.setSelectedFile(new File(tfTransferFile.getText() + ".import-log.html"));
					final int iBtn = filechooser.showSaveDialog(step);

					if (iBtn == JFileChooser.APPROVE_OPTION) {
						final File file = filechooser.getSelectedFile();
						if (file != null) {
							String fileName = file.getPath();

							if (!fileName.toLowerCase().endsWith(".import-log.html")) {
								fileName += ".import-log.html";
							}

							File outFile = new File(fileName);
							final Writer out = new BufferedWriter(new FileWriter(outFile));
							try {
								out.write(editLog.getText());
							}
							finally {
								out.close();
							}
							if (blnSaveOfLogRecommend) {
								step.setComplete(true);
							}
						}
					}					
				}
				catch (Exception e) {
					Errors.getInstance().showExceptionDialog(ifrm, e);
				}
				finally {
					UIUtils.unLockFrame(ifrm, lock);
				}
			}
		});
		
		return step;
	}
	
	
	
}
