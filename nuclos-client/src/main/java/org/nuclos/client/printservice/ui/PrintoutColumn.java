//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice.ui;

import org.nuclos.common.UID;

/**
 * {@link PrintoutColumn}
 * 
 * column for printservice model
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface PrintoutColumn {
	/**
	 * is field editable 
	 * 
	 * @return is editable true/false
	 */
	public boolean isEditable();
	
	/**
	 * column name
	 * 
	 * @return name of column
	 */
	public String getName();
	
	/**
	 * column description
	 * 
	 * @return description of column
	 */
	public String getDescription();
	
	/**
	 * get column java data type
	 * 
	 * @return {@link Class} of column data type
	 */
	public Class<?> getJavaClass();
	
	/**
	 * get column id
	 * 
	 * @return id of column
	 */
	public UID getId();
}
