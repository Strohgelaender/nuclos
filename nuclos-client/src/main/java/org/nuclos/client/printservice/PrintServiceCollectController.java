//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice;

import java.awt.event.ActionEvent;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.common.E;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.printservice.PrintServiceFacadeRemote;

public class PrintServiceCollectController extends MasterDataCollectController {

	private static final Logger LOG = Logger.getLogger(PrintServiceCollectController.class);
	
	private final JButton btnAutosetupResult;
	private final JButton btnAutosetupSearch;

	public PrintServiceCollectController(final MainFrameTab tabIfAny) {
		super(E.PRINTSERVICE, tabIfAny, null);
		
		Action actAutosetup = new AbstractAction("Autosetup", Icons.getInstance().getIconExecuteRule16()) {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Integer newPrintServicesCreated = SpringApplicationContextHolder.getBean(PrintServiceFacadeRemote.class).autoSetupPrintServices();
					PrintServiceCollectController.this.setCollectState(CollectState.OUTERSTATE_RESULT, CollectState.RESULTMODE_NOSELECTION);
					PrintServiceCollectController.this.getSearchStrategy().search();
					JOptionPane.showMessageDialog(tabIfAny, getSpringLocaleDelegate().getMsg("newPrintServicesCreated", newPrintServicesCreated.toString()));
				} catch (Exception ex) {
					Errors.getInstance().showExceptionDialog(tabIfAny, ex);
				}
			}
		};
	
		btnAutosetupResult = new JButton(actAutosetup);
		btnAutosetupResult.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.PRINTSERVICE));
		btnAutosetupSearch = new JButton(actAutosetup);
		btnAutosetupSearch.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.PRINTSERVICE));
		
	}
	
	@Override
	protected void setupSearchToolBar() {
		super.setupSearchToolBar();
		MasterDataSearchPanel sp = getSearchPanel();
		sp.addToolBarComponent(btnAutosetupSearch);
	}
	
	@Override
	protected void setupResultToolBar() {
		super.setupResultToolBar();
		final NuclosResultPanel<UID, CollectableMasterDataWithDependants<UID>> rp = getResultPanel();
		rp.addToolBarComponent(btnAutosetupResult);
	}
	
}
