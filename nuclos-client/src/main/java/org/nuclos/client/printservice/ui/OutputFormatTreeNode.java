//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice.ui;

import java.util.Enumeration;

import javax.swing.tree.TreeNode;

import org.nuclos.api.UID;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintPropertiesTO;

/**
 * {@link OutputFormatTreeNode} is {@link TreeNode} for {@link OutputFormatTO}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class OutputFormatTreeNode implements TreeNode {

	private final TreeNode parentNode;
	private final OutputFormatTO outputFormat;
	private boolean choose;
	private boolean edited;
	
	public OutputFormatTreeNode(final TreeNode parentNode, final OutputFormatTO outputFormat) {
		this.outputFormat = outputFormat;
		this.parentNode = parentNode;
		this.choose = false;
		this.edited = false;
	}
	
	/**
	 * get {@link OutputFormatTO}
	 * 
	 * @return {@link OutputFormatTO}
	 */
	public OutputFormatTO getOutputFormat() {
		return outputFormat;
	}

	/**
	 * get id of {@link OutputFormat}
	 * 
	 * @return {@link UID} of {@link OutputFormat}
	 */
	public UID getId() {
		return outputFormat.getId();
	}

	/**
	 * get {@link PrintPropertiesTO}
	 * 
	 * @return {@link PrintPropertiesTO}
	 * 
	 */
	public PrintPropertiesTO getProperties() {
		return outputFormat.getProperties();
	}

	/**
	 * set {@link PrintPropertiesTO}
	 * 
	 * @param properties {@link PrintPropertiesTO}
	 */
	public void setProperties(PrintPropertiesTO properties) {
		outputFormat.setProperties(properties);
	}

	/**
	 * is mandatory {@link OutputFormat}
	 * 
	 * @return is mandatory true/false
	 */
	public boolean isMandatory() {
		return outputFormat.isMandatory();
	}

	/**
	 * get description
	 * 
	 * @return description
	 */
	public String getDescription() {
		return outputFormat.getDescription();
	}

	/**
	 * set description
	 * 
	 * @param description description 
	 */
	public void setDescription(String description) {
		outputFormat.setDescription(description);
	}

	/**
	 * to string
	 */
	public String toString() {
		return outputFormat.getDescription();
	}
	

	/**
	 * is selected by user
	 * 
	 * @return is selected true/false
	 */
	public boolean isChoose() {
		return choose;
	}

	/**
	 * set is selected by user
	 * 
	 * @param choose is selected by user
	 */
	public void setChoose(boolean choose) {
		this.choose = choose;
	}

	/**
	 * get child at index
	 * 
	 * @param childIndex index
	 * 
	 * @return {@link TreeNode}
	 */
	@Override
	public TreeNode getChildAt(int childIndex) {
		return null;
	}

	/**
	 * get count of children
	 * 
	 * @return child count
	 */
	@Override
	public int getChildCount() {
		return 0;
	}

	/**
	 * get parent {@link TreeNode}
	 * 
	 * @return parent {@link TreeNode} or null!
	 */
	@Override
	public TreeNode getParent() {
		return this.parentNode;
	}

	/**
	 * get index of {@link TreeNode}
	 * 
	 * @return index of {@link TreeNode}
	 */
	@Override
	public int getIndex(TreeNode node) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * is {@link TreeNode} allowed to provider children
	 * 
	 * @return allowed true/false
	 */
	@Override
	public boolean getAllowsChildren() {
		return false;
	}

	/**
	 * is node a leaf {@link TreeNode}
	 * 
	 * @return is leaf true/false
	 */
	@Override
	public boolean isLeaf() {
		return true;
	}

	/**
	 * get {@link Enumeration} of children
	 * 
	 * @return null!!
	 */
	@Override
	public Enumeration<? extends TreeNode> children() {
		return null;
	}

	public boolean isEdited() {
		return edited;
	}

	public void setEdited(boolean edited) {
		this.edited = edited;
	}
	
	

}
