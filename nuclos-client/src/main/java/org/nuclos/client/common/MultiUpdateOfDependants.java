//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.SymmetricBinaryPredicate;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntity;

/**
 * Provides support for multi-update of dependant masterdata. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class MultiUpdateOfDependants<PK> implements IMultiUpdateOfDependants {

	private final Map<UID, SubFormCollectableMap<PK>> subFormCollectableMaps = new HashMap<>();;

	/**
	 * the color to be used as background for multi editable subforms that don't
	 * share a common value.
	 */
	public final static Color COLOR_NO_COMMON_VALUES = Utils.translateColorFromParameter(ParameterProvider.KEY_HISTORICAL_STATE_CHANGED_COLOR);// new
																																				// Color(246,229,255);

	/**
	 * initiates the multi-editing of dependants. Calculates the objects that
	 * are common in the dependants of all the given collectables and puts these
	 * in the given subform controllers, for each subentity.
	 * 
	 * @param collsubformctl
	 * @param collclctwd
	 */
	public MultiUpdateOfDependants(Collection<? extends DetailsSubFormController<PK,CollectableEntityObject<PK>>> collsubformctl, Collection<? extends CollectableWithDependants<PK>> collclctwd) {
		prepareSubFormsForMultiEdit(collsubformctl, collclctwd);
	}

	/**
	 * §todo we should return a DependantMasterDataMap here
	 * 
	 * @param collsubformctl
	 *            the subform controllers containing the data entered by the
	 *            user during multi-edit.
	 * @param clctwd
	 *            the collectable to update
	 * @return the dependant collectables needed for update of the given
	 *         collectable
	 */
	public DependantCollectableMasterDataMap getDependantCollectableMapForUpdate(
			Collection<? extends DetailsSubFormController<PK,CollectableEntityObject<PK>>> collsubformctl, CollectableWithDependants<PK> clctwd) {
		final IDependentDataMap mpDependants = new DependentDataMap();

		// iterate all enabled subforms:
		for (DetailsSubFormController<PK,CollectableEntityObject<PK>> subformctl : collsubformctl) {
			if (subformctl.getSubForm().isEnabled()) {

				final UID uidSubEntityName = subformctl.getSubForm().getEntityUID();
				final UID uidForeignKeyField = subformctl.getForeignKeyFieldUID();
				final Object oParentId = clctwd.getId();

				UID parentSubform = subformctl.getSubForm().getParentSubForm();

				// special handling for child subforms
				if (parentSubform != null) {
					// to be done
					continue;
				}

				Collection<EntityObjectVO<Object>> collresult = new Vector<>();

				SubFormCollectableMap<PK> map = subFormCollectableMaps.get(uidSubEntityName);
				for (Map<PK, CollectableEntityObject<PK>> mapForCurrentPrototype : map.values()) {
					CollectableEntityObject<PK> dependantData = mapForCurrentPrototype.get(oParentId);
					if (dependantData != null) {
						collresult.add((EntityObjectVO<Object>) dependantData.getEntityObjectVO());
					}
				}

				// 7. Correct the parent ids of those objects:
				setParentIds(collresult, uidForeignKeyField, oParentId);

				// and put them to the result:
				IDependentKey dependentKey = DependentDataMap.createDependentKey(uidForeignKeyField);
				mpDependants.addAllData(dependentKey, collresult);
			}
		}
		
		// LOCC.updateCollectable expects a DependantCollectableMap:
		DependantCollectableMasterDataMap dcmdp = new DependantCollectableMasterDataMap(mpDependants);
		
		Utils.mapNullBooleansToFalseInDependents(dcmdp);
		return dcmdp;
	}

	/**
	 * prepares the subforms for multi edit. Creates the id mapping for multi
	 * edit.
	 * 
	 * @param collsubformctl
	 * @param collclctwd
	 * @return the id mapping for multi edit, which is needed when updating is
	 *         started.
	 */
	private void prepareSubFormsForMultiEdit(
			Collection<? extends DetailsSubFormController<PK,CollectableEntityObject<PK>>> collsubformctl, 
			Collection<? extends CollectableWithDependants<PK>> collclctwd) {
		
		for (DetailsSubFormController<PK,CollectableEntityObject<PK>> subformctl : collsubformctl) {
			subformctl.clear();
			final UID sSubEntityName = subformctl.getSubForm().getEntityUID();
			final UID sParentFieldName = subformctl.getForeignKeyFieldUID();

			UID parentSubform = subformctl.getSubForm().getParentSubForm();

			final Collection<CollectableEntityObject<PK>> collclctCommon;
			
			// load data of subforms of the first hierarchy
			if (parentSubform == null && 
				MetaProvider.getInstance().getEntity(subformctl.getEntityAndForeignKeyField().getEntity()).isEditable()) {
				SubFormCollectableMap<PK> subFormCollectableMap = getCommonSubCollectables(collclctwd, sSubEntityName, 
						sParentFieldName, subformctl);
				subFormCollectableMaps.put(sSubEntityName, subFormCollectableMap);
				collclctCommon = subFormCollectableMap.keySet();
			}
			// load data of child subforms
			else {
				SubFormCollectableMap<PK> subFormCollectableMap = getCommonSubCollectables(collclctwd, sSubEntityName, 
						sParentFieldName, subformctl);
				subFormCollectableMaps.put(sSubEntityName, subFormCollectableMap);
				collclctCommon = subFormCollectableMap.keySet();
			}
			
			//NUCLOS-5686 Register itself when subFormCollectableMaps has been initialized, not before
			subformctl.setMultiUpdateOfDependants(this);

			// instead only...
			if (!collclctCommon.isEmpty()) {
				//@todo try to use MasterDataSubFormController.fillSubFor instead
				subformctl.updateTableModel(new ArrayList<>(collclctCommon));
			}
		
		}
	}

	/**
	 * @param collclct
	 * @return dependant data records of the given sub entity that are identical
	 *         for all given Collectables
	 */
	private static <PK2> SubFormCollectableMap<PK2> getCommonSubCollectables(
			Collection<? extends CollectableWithDependants<PK2>> collclct, final UID subEntity, final UID parentField,
			DetailsSubFormController<PK2,CollectableEntityObject<PK2>> subformctl) {

		// compare all fields except the parent field:
		final Collection<EntityObjectVO<PK2>> collmdcvo;
		SubForm subform = subformctl.getSubForm();

		if (subform.isMultieditable()) {
			// reload subform data from the database instead of using the tablemodel
			// data like it was made before
			List<PK2> lstPKs = new ArrayList<>();
			
			for (CollectableWithDependants<PK2> clctWithDependants : collclct) {
				final PK2 pk = clctWithDependants.getPrimaryKey();
				
				if (pk != null && !lstPKs.contains(pk)) {
					lstPKs.add(pk);
				}											
			}
			
			PK2[] pks = RigidUtils.uncheckedCast(lstPKs.toArray());
			collmdcvo = MasterDataDelegate.getInstance().getDependentDataCollectionWithLimit(parentField,
                    null, subform.getMapParams(), subformctl.getCurrentLayoutUid(), null, pks);
		} else {
			collmdcvo = new ArrayList<>();
		}
		
		CollectableEOEntity eo = (CollectableEOEntity) CollectableEOEntityClientProvider.getInstance().getCollectableEntity(subEntity);
		Collection<CollectableEntityObject<PK2>> dependants = CollectionUtils.transform(collmdcvo, new CollectableEntityObject.MakeCollectable<PK2>(eo));

		Collection<UID> combination = new ArrayList<>();

		// determine field combination
		final EntityMeta<?> entity = MetaProvider.getInstance().getEntity(subEntity);
		Map<UID, FieldMeta<?>> entityfields = MetaProvider.getInstance().getAllEntityFieldsByEntity(subEntity); 
		
		if (entity.getFieldsForEquality() != null) {
			for (UID field : entity.getFieldsForEquality()) {
				if (entityfields.containsKey(field)) {
					combination.add(field);
				}
			}
		}
		
		if (combination.size() == 0) {
			for (FieldMeta<?> field : entityfields.values()) {
				if (field.isUnique()) {
					combination.add(field.getUID());
				}
			}
		}
		
		if (combination.size() == 0) {
			combination.addAll(entityfields.keySet());
		}
		
		CollectionUtils.retainAll(combination, (UID t) -> {
			if (SF.isEOField(entity.getUID(), t)) {
				return false;
			}
			else if (parentField.equals(t)) {
				return false;
			}
			return true;
		});
		
		List<Set<CollectableEntityObject<PK2>>> equivalenceClasses =
				IMultiUpdateOfDependants.getEquivalenceClasses(dependants, new AreFieldsEqual(combination));
		return new SubFormCollectableMap<>(equivalenceClasses, subEntity, parentField,
				collclct, subformctl, combination);
	}

	private static void setParentIds(Collection<EntityObjectVO<Object>> collmdvo, UID uidForeignKeyField, Object iParentId) {
		for (EntityObjectVO<?> mdvo : collmdvo) {
			mdvo.setFieldValue(uidForeignKeyField, iParentId);

			if (iParentId instanceof Integer) {
				Long id = new Long((Integer) iParentId);
				mdvo.setFieldId(uidForeignKeyField, id);
			}
			else if (iParentId instanceof Long){
				mdvo.setFieldId(uidForeignKeyField, (Long) iParentId);
			}
			else {
				mdvo.setFieldUid(uidForeignKeyField, (UID) iParentId);
			}
		}
	}

	private static class AreFieldsEqual<PK2> implements SymmetricBinaryPredicate<Collectable<PK2>> {
		private Collection<UID> collFieldNames;

		AreFieldsEqual(Collection<UID> collFieldNames) {
			this.collFieldNames = collFieldNames;
		}

		@Override
		public boolean evaluate(Collectable<PK2> clct1, Collectable<PK2> clct2) {
			for (UID sFieldName : collFieldNames) {
				if (!clct1.getField(sFieldName).equals(clct2.getField(sFieldName))) {
					return false;
				}
			}
			return true;
		}

	} // inner class AreFieldsEqual

	@Override
	public TableCellRenderer getTableCellRenderer(CollectableEntityField clctefTarget) {
		if (subFormCollectableMaps == null) {
			// call during init!
			return new DefaultTableCellRenderer();
		} else {
			UID entity = clctefTarget.getEntityUID();
			SubFormCollectableMap<PK> map = subFormCollectableMaps.get(entity);
			return new SubFormTableCellRenderer<>(map);
		}
	}

	public void transfer(SubForm sf, DetailsSubFormController dsfCtl) {
		int[] selectedRows = sf.getJTable().getSelectedRows();
		SubFormCollectableMap<PK> map = subFormCollectableMaps.get(sf.getEntityUID());
		for (int row : selectedRows) {
			if (!map.allEntitiesHaveDataInRow(row)) {
				CollectableEntityObject prototype = (CollectableEntityObject<?>) map.getPrototype(row);
				map.transferDataToAllEntities(prototype);
			}
		}
		sf.getJTable().repaint();
		sf.fireStateChanged(new ChangeEvent(sf));
	}

	public boolean isTransferPossible(SubForm sf) {
		if (sf.isEnabled()) {
			SubFormCollectableMap<PK> map = subFormCollectableMaps.get(sf.getEntityUID());
			int[] selectedRows = sf.getJTable().getSelectedRows();
			for (int row : selectedRows) {
				if (!map.allEntitiesHaveDataInRow(row)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void close(SubFormController sfCtl) {
		for (SubFormCollectableMap<PK> map : subFormCollectableMaps.values()) {
			if (map.getSubformController() == sfCtl) {
				map.close();
			}
		}
	}
} // class MultiUpdateOfDependants
