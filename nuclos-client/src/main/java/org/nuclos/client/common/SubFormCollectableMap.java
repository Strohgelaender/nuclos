//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.event.TableModelEvent;

import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.ui.model.AbstractListTableModel;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.dal.vo.EntityObjectVO;

@SuppressWarnings("serial")
public class SubFormCollectableMap<PK> extends HashMap<CollectableEntityObject<PK>, Map<PK, CollectableEntityObject<PK>>> 
	implements ISubFormCollectableMap<PK> {

	private UID sSubEntityName;
	private UID sParentFieldName;
	private SubFormController subformctl;
	private Collection<? extends CollectableWithDependants<PK>> collclct;
	private AbstractListTableModel<Collectable<PK>> tableModel;
	private Collection<UID> combination;
	
	@SuppressWarnings("unchecked")
	public SubFormCollectableMap(List<Set<CollectableEntityObject<PK>>> equivalenceClasses, 
			UID sSubEntityName, UID sParentFieldName, Collection<? extends CollectableWithDependants<PK>> collclct, 
			SubFormController subformctl, Collection<UID> combination) {
		super();
		this.sSubEntityName = sSubEntityName;
		this.sParentFieldName = sParentFieldName;
		this.collclct = collclct;
		this.subformctl = subformctl;
		this.combination = combination;
		
		tableModel = (AbstractListTableModel<Collectable<PK>>) subformctl.getJTable().getModel();
		tableModel.addTableModelListener(this);

		// compute the prototype for each equivalenceClass (= set of data with
		// the same key)
		for (Set<CollectableEntityObject<PK>> equivalenceClass : equivalenceClasses) {
			CollectableEntityObject<PK> prototype = computePrototype(equivalenceClass);
			Map<PK, CollectableEntityObject<PK>> idToCollectableMap = new HashMap<PK, CollectableEntityObject<PK>>();
			for (CollectableEntityObject<PK> collectable : equivalenceClass) {
				idToCollectableMap.put((PK) collectable.getField(sParentFieldName).getValueId(), collectable);
			}
			put(prototype, idToCollectableMap);
		}
	}

	// compute the prototype for each equivalenceClass (= set of data with the
	// same key)
	private CollectableEntityObject<PK> computePrototype(Set<CollectableEntityObject<PK>> equivalenceClass) {
		Collection<UID> nonUniqueFields = getNonUniqueFieldsWithoutParent(sSubEntityName, sParentFieldName);

		CollectableEntityObject<PK> firstEntry = equivalenceClass.iterator().next();
		CollectableEntityObject<PK> representative = new CollectableEntityObject<PK>(
				firstEntry.getCollectableEntity(), firstEntry.getEntityObjectVO().copy());

		for (CollectableEntityObject<PK> ceo : equivalenceClass) {
			for (UID field : nonUniqueFields) {
				CollectableField representativeField = representative.getField(field);
				CollectableField currentField = ceo.getField(field);
				if (!representativeField.equals(currentField)) {
					representative.setField(field, new CollectableValueField(null));
				}
			}
		}
		return representative;
	}
	
	private Collection<UID> getNonUniqueFieldsWithoutParent(UID sSubEntityName, UID sParentFieldName) {
		Collection<UID> result = new Vector<UID>();
		for (UID fieldName : MetaProvider.getInstance().getAllEntityFieldsByEntity(sSubEntityName).keySet()) {
			if (SF.isEOField(sSubEntityName, sParentFieldName) || sParentFieldName.equals(fieldName) || combination.contains(fieldName)) {
				continue;
			}
			result.add(fieldName);
		}
		return result;
	}

	// transfer the data in the given field from the prototype to its
	// equivalence class
	public void transferField(CollectableEntityObject<PK> prototype, UID fieldName) {
		Collection<CollectableEntityObject<PK>> equivalenceClass = get(prototype).values();

		for (CollectableEntityObject<PK> object : equivalenceClass) {
			object.setField(fieldName, prototype.getField(fieldName));
		}
	}

	// add a new prototype to the mapping. The mapping will contain a copy of
	// the prototype for
	// each parent collectable
	public void addPrototype(CollectableEntityObject<PK> prototype) {
		Map<PK, CollectableEntityObject<PK>> idToCollectableMap = new HashMap<PK, CollectableEntityObject<PK>>();
		for (CollectableWithDependants<PK> clctWithDependants : collclct) {
			CollectableEntityObject<PK> object = new CollectableEntityObject<PK>(
					prototype.getCollectableEntity(), prototype.getEntityObjectVO().copy());
			idToCollectableMap.put((PK)clctWithDependants.getId(), object);
		}
		put(prototype, idToCollectableMap);
	}

	// mark all data associated with the prototype as removed
	public void removePrototype(CollectableEntityObject<PK> prototype) {
		Collection<CollectableEntityObject<PK>> equivalenceClass = get(prototype).values();
		for (CollectableEntityObject<PK> object : equivalenceClass) {
			object.markRemoved();
		}

	}

	// transfer the data of the prototyp to all parent collectables. If a parent
	// already has the
	// data, it will be changed. If it does not, the data will be created.
	public Map<PK, CollectableEntityObject<PK>> transferDataToAllEntities(CollectableEntityObject<PK> representative) {
		Map<PK, CollectableEntityObject<PK>> newCeos = new HashMap<PK, CollectableEntityObject<PK>>();
		Map<PK, CollectableEntityObject<PK>> map = get(representative);
		for (CollectableWithDependants<PK> clctWithDependants : collclct) {
			PK parentId = (PK) clctWithDependants.getId();
			CollectableEntityObject<PK> object = map.get(parentId);
			if (object == null) {
				CollectableEntityObject<PK> copyCeo = copyCollectableEO(representative);
				map.put(parentId, copyCeo);
				newCeos.put(parentId, copyCeo);
			}
			else {
				copyAllFields(representative, object);
			}
		}
		return newCeos;
	}

	private void copyAllFields(CollectableEntityObject<PK> source, CollectableEntityObject<PK> target) {
		for (UID fieldName : MetaProvider.getInstance().getAllEntityFieldsByEntity(sSubEntityName).keySet()) {
			if (SF.isEOField(sSubEntityName, fieldName) || sParentFieldName.equals(fieldName)) {
				continue;
			}
			target.setField(fieldName, source.getField(fieldName));
		}
	}

	private CollectableEntityObject<PK> copyCollectableEO(CollectableEntityObject<PK> original) {
		EntityObjectVO<PK> eoNew = original.getEntityObjectVO().copy();
		eoNew.flagNew();
		return new CollectableEntityObject<PK>(original.getCollectableEntity(), eoNew);
	}

	public Collectable<PK> getPrototype(int row) {
		return tableModel.getRow(row);
	}

	/**
	 * Checks whether all parent collectables have data in the given row
	 * @param row
	 * @return
	 */
	public boolean allEntitiesHaveDataInRow(int row) {
		Collectable<PK> collectable = tableModel.getRow(row);
		Map<PK, CollectableEntityObject<PK>> mapCollEntityObject = get(collectable);
		if (mapCollEntityObject == null) return false;
		Collection<CollectableEntityObject<PK>> equivalenceClass = mapCollEntityObject.values();
		return equivalenceClass.size() == collclct.size();
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		int firstRow = e.getFirstRow();
		int lastRow = e.getLastRow();
		int column = e.getColumn();

		if (e.getType() == TableModelEvent.UPDATE && column != -1) {
			// change existing cell
			UID field = subformctl.getTableColumns().get(column).getUID();
			for (int i = firstRow; i <= lastRow; i++) {
				transferField((CollectableEntityObject) tableModel.getRow(i), field);
			}
		}
		else if (e.getType() == TableModelEvent.INSERT && column == -1) {
			// insert new row
			CollectableEntityObject<PK> representative = (CollectableEntityObject<PK>) tableModel.getRow(firstRow);
			addPrototype(representative);
		}
		else if (e.getType() == TableModelEvent.DELETE && e.getColumn() == -1) {
			// delete a row
			// The table model has already been changed. So we cannot find the
			// deleted Collectable in it. Hence, we
			// search through the table. The deleted Collectables are the ones
			// which are still in the map but not in the
			// table anymore.
			Set<CollectableEntityObject<PK>> representatives = new HashSet<CollectableEntityObject<PK>>(keySet());
			for (int currentRow = 0; currentRow < subformctl.getJTable().getRowCount(); currentRow++) {
				CollectableEntityObject<PK> representative = (CollectableEntityObject<PK>) tableModel.getRow(currentRow);
				representatives.remove(representative);
			}
			for (CollectableEntityObject<PK> representative : representatives) {
				removePrototype(representative);
			}
		}
	}
	
	public int getParentCollectablesSize() {
		return collclct.size();
	}

	public SubFormController getSubformController() {
		return subformctl;
	}

	public Collection<CollectableWithDependants<PK>> getParentCollectables() {
		return Collections.unmodifiableCollection(collclct);
	}
	
	public void close() {
		tableModel.removeTableModelListener(this);
	}
	
	public Map<PK, CollectableEntityObject<PK>> get(Collectable<PK> clct) {
		return super.get(clct);
	}
}
