//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;

/**
 * <code>MultiMap&lt;String sEntityName, Collectable&gt;</code>. A map containing the dependent <code>Collectable</code>s by entity.
 * §todo the key should be (sEntityName, sForeignKeyFieldToParent) - refactor, then consider moving this class to common.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 * 
 * @deprecated Multinuclet: Get rid of this in favor of {@link IDependentDataMap}.
 */
public class DependantCollectableMasterDataMap {

	private final MultiListMap<IDependentKey, CollectableEntityObject<?>> mmp = new MultiListHashMap<IDependentKey, CollectableEntityObject<?>>();

	/**
	 * creates an empty map.
	 */
	public DependantCollectableMasterDataMap() {
	}

	public DependantCollectableMasterDataMap(FieldMeta<?> referencingField, Collection<? extends CollectableEntityObject<?>> collclctDependants) {
		this(DependentDataMap.createDependentKey(referencingField), collclctDependants);
	}
	
	/**
	 * §precondition collclctDependants != null
	 * @param dependantKey
	 * @param collclctDependants Collection&lt;Collectable&gt;
	 */
	public DependantCollectableMasterDataMap(IDependentKey dependantKey, Collection<? extends CollectableEntityObject<?>> collclctDependants) {
		this();
		addValues(dependantKey, collclctDependants);
	}
	
	public DependantCollectableMasterDataMap(IDependentDataMap mpDependants) {
		this();
		setAllValues(mpDependants);
	}

	public void setAllValues(IDependentDataMap mpDependants) {
		this.clear();
		for (IDependentKey dependentKey : mpDependants.getKeySet()) {
			FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(dependentKey.getDependentRefFieldUID());
			CollectableEntity ce = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(fMeta.getEntity());
			if (ce instanceof CollectableEOEntity) {
				final CollectableEOEntity clctmde = (CollectableEOEntity) ce;
				addValues(dependentKey, CollectionUtils.transform(mpDependants.getData(fMeta), new CollectableEntityObject.MakeCollectable(clctmde)));
			}
			else {
				final CollectableMasterDataEntity clctmde = (CollectableMasterDataEntity)ce;
				final CollectableEOEntity cee = Utils.transformCollectableMasterDataEntityTOCollectableEOEntity(clctmde);
				addValues(dependentKey, CollectionUtils.transform(mpDependants.getData(fMeta), new CollectableEntityObject.MakeCollectable(cee)));
			}
		}
	}
	

	/**
	 * transforms a <code>DependantMasterDataMap</code> into a <code>DependantCollectableMap</code>.
	 * @param mpDependants
	 * @return DependantCollectableMap
	 */
	public static DependantCollectableMasterDataMap newDependantCollectableMap(DependentDataMap mpDependants) {
		return new DependantCollectableMasterDataMap(mpDependants);
	}

	/**
	 * §postcondition result != null
	 * @param dependantKey
	 * @return the dependants belonging to the given entity, if any.
	 */
	public Collection<CollectableEntityObject<?>> getValues(IDependentKey dependentKey) {
		return this.mmp.getValues(dependentKey);
	}
	
	/**
	 * §postcondition result != null
	 * @param referencingField
	 * @return the dependants belonging to the given referencingField, if any.
	 */
	public Collection<CollectableEntityObject<?>>  getValues(FieldMeta<?> referencingField) {
		IDependentKey dependentKey = DependentDataMap.createDependentKey(referencingField);
		return this.getValues(dependentKey);
	}
	
	/**
	 * get single dependant object by enitiyUID and PK
	 * @param dependantKey 
	 * @param id PK
	 * @return dependant object or null if no such object exist
	 */
	public CollectableEntityObject<?> getValue(IDependentKey dependantKey, Object id) {
		for (CollectableEntityObject<?> value : getValues(dependantKey)) {
			if (value.getId().equals(id)) {
				return value;
			}
		}
		return null;
	}

	/**
	 * puts the given <code>Collectable</code> into this map.
	 * @param dependantKey
	 * @param clctDependant
	 */
	public <PK> void addValue(IDependentKey dependantKey, CollectableEntityObject<PK> clctDependant) {
		this.mmp.addValue(dependantKey, clctDependant);
	}

	/**
	 * puts all elements of <code>collclctDependants</code> into this map.
	 * Note that if the given <code>collclctDependants</code> is empty, nothing will be added.
	 * §precondition collclctDependants != null
	 * @param dependantKey
	 * @param collclctDependants
	 */
	public void addValues(IDependentKey dependantKey, Collection<? extends CollectableEntityObject<?>> collclctDependants) {
		this.mmp.addAllValues(dependantKey, collclctDependants);
	}
	
	public Set<IDependentKey> getKeySet() {
		return this.mmp.keySet();
	}

	/**
	 * @return the uids of referencing field that this map contains values for.
	 */
	public Set<UID> getReferencingFieldUIDs() {
		Set<UID> result = new HashSet<UID>();
		for (IDependentKey key : this.mmp.keySet()) {
			result.add(key.getDependentRefFieldUID());
		}
		return result;
	}

	/**
	 * transforms <code>this</code> into a <code>DependantMasterDataMap</code>.
	 * @return DependantMasterDataMap
	 */
	public IDependentDataMap toDependentDataMap() {
		final IDependentDataMap result = new DependentDataMap();
		for (IDependentKey key : this.mmp.keySet()) {
			result.addAllData(key, CollectionUtils.transform(this.getValues(key), new ExtractEntityObjectVO()));
		}
		return result;
	}
	
	// clears dependant map
	public void clear() {
		this.mmp.clear();
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("keys=").append(mmp.keySet());
		result.append("]");
		return result.toString();
	}
	
	public static class ExtractEntityObjectVO<PK> implements Transformer<CollectableEntityObject<PK>, EntityObjectVO<PK>> {
		@Override
		public EntityObjectVO<PK> transform(CollectableEntityObject<PK> clctmd) {
			IDependentDataMap depmdmp = clctmd.getDependantMasterDataMap();
			EntityObjectVO<PK> mdVO = clctmd.getEntityObjectVO();
			mdVO.setDependents(depmdmp);
			return mdVO;
		}
	}

}	// class DependantCollectableMasterDataMap
