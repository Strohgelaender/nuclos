package org.nuclos.client.common;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.ui.collect.EditView;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;

public interface IEntityCollectController<PK,Clct extends Collectable<PK>> extends ICmdGenerateObject {

	List<GeneratorActionVO> getGeneratorActions();
	
	List<GeneratorActionVO> getGeneratorActions(Collection<Clct> selectedCollectablesFromResult);
	
	EditView newSearchEditView(LayoutRoot<PK> layoutroot);
	
	Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> getDetailsSubforms();

}
