//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.*;

import org.apache.log4j.Logger;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectController.CollectableEventListener;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.WeakCollectableEventListener;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentEvent;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Default listener for <code>CollectableListOfValues.Event</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class NuclosLOVListener<PK> implements CollectableListOfValues.LOVListener<PK> {
	private static NuclosLOVListener singleton;

	public static synchronized NuclosLOVListener getInstance() {
		if (singleton == null) {
			singleton = new NuclosLOVListener();
		}
		return singleton;
	}

	private NuclosLOVListener() {
	}

	@Override
	public void viewSearchResults(final CollectableListOfValues.Event ev) {
		final CollectableComponent clctcomp = ev.getCollectableComponent();
		UIUtils.runCommandLater(UIUtils.getTabOrWindowForComponent(clctcomp.getJComponent()), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final UID sReferencedEntityName = clctcomp.getEntityField().getReferencedEntityUID();
				final CollectController<?,?> ctl = newCollectController(sReferencedEntityName);
				ctl.runViewResults(ev.getCollectableListOfValues());
				ctl.getCollectPanel().setTabbedPaneEnabledAt(CollectState.OUTERSTATE_SEARCH, false);
			}
		});
	}

	@Override
	public void lookup(final CollectableListOfValues.Event ev) {
		final CollectableComponent clctcomp = ev.getCollectableComponent();
		final MainFrameTab tab = getTab(clctcomp);

		UIUtils.runCommandLater(tab, new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final UID sReferencedEntityName = clctcomp.getEntityField().getReferencedEntityUID();	
				CollectController<Object, ?> ctlAlt = Main.getInstance().getMainController().findCollectControllerDisplaying(clctcomp.getEntityField().getEntityUID());				

				final MainFrameTab overlay = new MainFrameTab();
				String customUsage = (String)((org.nuclos.client.common.NuclosCollectableListOfValues)ev.getSource()).getCustomUsageSearch();
				if (customUsage == null) {
					customUsage = ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY);
				}
				final CollectController<?,?> ctl = NuclosCollectControllerFactory.getInstance().newCollectController(sReferencedEntityName, overlay, customUsage);
				
				if(ctlAlt!=null)
					ctl.setShowInvalidMasterData(ctlAlt.useInvalidMasterDataEnabled());
				
				Main.getInstance().getMainController().initMainFrameTab(ctl, overlay);
				tab.add(overlay);
				ctl.runLookupCollectable(ev.getCollectableListOfValues());
			}
		});
	}

	@Override
	public void showDetails(final CollectableComponentEvent ev) {
		final CollectableComponent clctcomp = ev.getCollectableComponent();
		UIUtils.runCommandLater(Main.getInstance().getMainFrame(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final Main main = Main.getInstance();
				final MainController mc = main.getMainController();
				final UID sReferencedEntityName = clctcomp.getEntityField().getReferencedEntityUID();
				
//				CollectController<?> controller = mc.getControllerForTab((MainFrameTab) 
//						main.getMainFrame().getHomePane().getSelectedComponent());
				// Not good! Can result in assertion error if source is not on home pane ;-(
				// But new solution currently not works with source fields out of subform. How we can solv a parent from a cell editor?
				// Or add Parent to CollectableComponent / CollectableComponentEvent?
				CollectController<PK,?> controller = (CollectController<PK, ?>) mc.getControllerForTab(UIUtils.getTabForComponent(clctcomp.getJComponent()));
				
				final PK oId = (PK) clctcomp.getField().getValueId();
				
				CollectableEventListener clctEventListener = null;
				
				if (clctcomp instanceof CollectableEventListener) {
					clctEventListener = (CollectableEventListener) clctcomp;
				}
				if (clctcomp instanceof CollectableListOfValues) {
					clctEventListener = ((CollectableListOfValues)clctcomp).getCollectableEventListenerForDetails();
				}

				MainFrameTab tab = null;
				try {
					tab = getTab(clctcomp);
					
				} catch (Exception e) {
					Logger.getLogger(NuclosLOVListener.class).warn(e.getMessage());
				}

				if (clctEventListener == null) {
					mc.showDetails(sReferencedEntityName, oId, true, tab, controller);
				} else {
					mc.showDetails(sReferencedEntityName, oId, true, tab, controller, new WeakCollectableEventListener(clctEventListener));					
				}			
			}
		});
	}

	private static CollectController<?,? extends Collectable<?>> newCollectController(UID sEntityName) throws CommonBusinessException {
		return NuclosCollectControllerFactory.getInstance().newCollectController(sEntityName, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
	}

	@Override
	public void createNew(CollectableComponentEvent ev) {
		final CollectableComponent clctcomp = ev.getCollectableComponent();

		CollectableEventListener clctEventListener = null;
		
		if (clctcomp instanceof CollectableEventListener) {
			clctEventListener = (CollectableEventListener) clctcomp;
		}
		if (clctcomp instanceof CollectableListOfValues) {
			clctEventListener = ((CollectableListOfValues)clctcomp).getCollectableEventListenerForNew();
		}

		if (clctEventListener == null) {
			throw new IllegalStateException();
		}

		final MainFrameTab tab = getTab(clctcomp);

		final CollectableEventListener listener = new WeakCollectableEventListener(clctEventListener);
		
		UIUtils.runCommandLater(tab, new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final UID sReferencedEntityName = clctcomp.getEntityField().getReferencedEntityUID();
				Main.getInstance().getMainController().showNew(sReferencedEntityName, tab, listener);
			}
		});
	}

	private MainFrameTab getTab(CollectableComponent comp) {
		Component c = UIUtils.getTabOrWindowForComponent(comp.getControlComponent());
		final MainFrameTab tab;
		if (c instanceof MainFrameTab) {
			tab = (MainFrameTab) c;
		} else {
			c = UIUtils.getTabOrWindowForComponent(comp.getJComponent());
			if (c instanceof MainFrameTab) {
				tab = (MainFrameTab) c;
			} else {
				MainFrameTab selectedTab = null;
				try {
					selectedTab = MainFrame.getSelectedTab(comp.getControlComponent().getLocationOnScreen());
				} catch (IllegalComponentStateException e) {
					//
				} finally {
					tab = selectedTab;
				}
			}
		};

		if (tab == null) {
			throw new NuclosFatalException("Tab from parent could not be determinded");
		}
		return tab;
	}

}	// class NuclosLOVListener
