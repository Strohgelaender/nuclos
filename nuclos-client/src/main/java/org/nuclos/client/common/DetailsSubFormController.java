//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.JViewport;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.autonumber.AutonumberUiUtils;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.genericobject.GenericObjectSplitViewCollectController;
import org.nuclos.client.genericobject.datatransfer.GenericObjectIdModuleProcess;
import org.nuclos.client.genericobject.datatransfer.TransferableGenericObjects;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.masterdata.MasterDataSplitViewCollectController;
import org.nuclos.client.masterdata.datatransfer.MasterDataIdAndEntity;
import org.nuclos.client.masterdata.datatransfer.MasterDataVORow;
import org.nuclos.client.masterdata.datatransfer.TransferableMasterDatas;
import org.nuclos.client.scripting.ScriptEvaluator;
import org.nuclos.client.scripting.context.CollectControllerScriptContext;
import org.nuclos.client.scripting.context.SubformControllerScriptContext;
import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.BubbleUtils;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController.CollectableEventListener;
import org.nuclos.client.ui.collect.CollectController.MessageType;
import org.nuclos.client.ui.collect.ISubFormCtrlLateInit;
import org.nuclos.client.ui.collect.ITabSelectListener;
import org.nuclos.client.ui.collect.LayoutNavigationManager;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.ToolTipsTableHeader;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.subform.ClearAction;
import org.nuclos.client.ui.collect.subform.FixedColumnRowHeader;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubForm.SubFormToolListener;
import org.nuclos.client.ui.collect.subform.SubFormFilter;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.collect.subform.SubFormTools;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.collect.subform.ToolbarFunctionState;
import org.nuclos.client.ui.collect.subform.TransferLookedUpValueAction;
import org.nuclos.client.ui.dnd.DragAndDropContext;
import org.nuclos.client.ui.dnd.TableRowCopyTransferHandler;
import org.nuclos.client.ui.dnd.TableRowCopyTransferHandler.RowTransferable;
import org.nuclos.client.ui.dnd.TransferableWrapper;
import org.nuclos.client.ui.event.ITablePopupMenuListener;
import org.nuclos.client.ui.event.Mouse2TableHeaderPopupMenuAdapter;
import org.nuclos.client.ui.event.TablePopupMenuEvent;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.ui.profile.ProfileMenu;
import org.nuclos.client.ui.profile.ProfileSupport;
import org.nuclos.client.ui.profile.SubformProfilesController;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.ui.util.EntityFieldUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PointerException;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.PredicateUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.collection.ValueObjectList;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common.profile.ProfileItem;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

/**
 * Controller for collecting dependant data (in a one-to-many relationship) in a
 * subform. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public abstract class DetailsSubFormController<PK, Clct extends Collectable<PK>> extends AbstractDetailsSubFormController<PK, Clct> 
	implements NuclosDropTargetVisitor, ITabSelectListener, EnabledListener {

	private static final Logger LOG = Logger
			.getLogger(DetailsSubFormController.class);

	protected final NuclosCollectControllerCommonState state;
	
	/**
	 * the id of the (current) parent object.
	 */
	private Object oParentId;

	private FixedColumnRowHeader fixedcolumnheader;

	private EntityCollectController<?, ?> cltctl;

	private boolean multiEdit = false;

	/**
	 * required for determination of editable rows (and in the future probably
	 * for editing sub-subforms)
	 */
	private IMultiUpdateOfDependants<PK> multiUpdateOfDependants;

	private final SubForm.SubFormToolListener multieditToolListener = new SubForm.SubFormToolListener() {
		@Override
		public void toolbarAction(String actionCommand) {
			ToolbarFunction cmd = ToolbarFunction
					.fromCommandString(actionCommand);
			if (ToolbarFunction.TRANSFER.equals(cmd)
					&& getMultiUpdateOfDependants() != null) {
				getMultiUpdateOfDependants().transfer(getSubForm(),
						DetailsSubFormController.this);
			}
		}
	};

	private final ListSelectionListener multieditSelectionListener = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (getMultiUpdateOfDependants() != null
					&& getMultiUpdateOfDependants().isTransferPossible(
							getSubForm())) {
				getSubForm().setToolbarFunctionState(ToolbarFunction.TRANSFER,
						ToolbarFunctionState.ACTIVE);
			} else {
				getSubForm().setToolbarFunctionState(ToolbarFunction.TRANSFER,
						ToolbarFunctionState.DISABLED);
			}
		}
	};

	private TableRowIndicator rowIndicator;

	private volatile DragAndDropContext dragAndDropContext;

	private SubformProfilesController profilesCtl;

	private LayoutNavigationSupport lns;

	/**
	 * @param clctcompmodelproviderParent
	 *            provides the enclosing <code>CollectController</code>'s
	 *            <code>CollectableComponentModel</code>s. This avoids handing
	 *            the whole <code>CollectController</code> to the
	 *            <code>SubFormController</code>. May be <code>null</code> if
	 *            there are no dependencies from subform columns to fields of
	 *            the main form.
	 * @param subform
	 * @param prefsUserParent
	 *            the preferences of the parent controller
	 */
	public DetailsSubFormController(CollectableEntity clcte, MainFrameTab tab,
			CollectableComponentModelProvider clctcompmodelproviderParent, UID parentEntity, final SubForm subform,
			Preferences prefsUserParent, EntityPreferences entityPrefs, CollectableFieldsProviderFactory clctfproviderfactory,
			NuclosCollectControllerCommonState state, EntityCollectController<?, ?> eoController) {
		
		super(clcte, tab, clctcompmodelproviderParent, parentEntity, subform,
				prefsUserParent, entityPrefs, clctfproviderfactory);
		this.state = state;
		this.cltctl = eoController;

		if (subform.isWithinTabbedPane()) {
			subform.setTabSelectListener(this);
		}
		
		if (subform.isWithinTabbedPane() && !E.isNuclosEntity(subform.getEntityUID())) {
			
			ISubFormCtrlLateInit iSubFormCtrlLateInit = () -> this.lateInit();
			subform.setSubFormCtrlLateInit(iSubFormCtrlLateInit);
			
		} else {
			this.lateInit(); //
			
		}
	}
	
	private void lateInit() {
		this.initModelAndKeys();
		this.initFixedColumnHeader();
		this.postCreate();
		this.setupDragDrop();
	}

	private void initFixedColumnHeader() {
		if (this.isColumnSelectionAllowed(getParentEntityUID())) {
			this.fixedcolumnheader = new FixedColumnRowHeader(this.getSubForm(), this.getTablePreferencesManager());
			this.fixedcolumnheader.initializeFieldsFromPreferences(this.getTablePreferencesManager());
			getSubForm().setTableRowHeader(this.fixedcolumnheader);
			this.fixedcolumnheader.getHeaderTable().addMouseListener(getSubForm().new SubFormPopupMenuMouseAdapter(this.fixedcolumnheader.getHeaderTable()));
		}
	}
	
	@Override
	protected void postCreate() {
		final SubForm sf = getSubForm();
		setupHeaderToolTips(getJTable());
		this.profilesCtl = new SubformProfilesController(getTablePreferencesManager(),
				new SubFormProfileSupport(), this);
		
		sf.initializeParametersForSubformFilter(this, getParentEntityUID(), getCollectableFieldsProviderFactory(), getIntidForVLP(), getMandatorUID());

		this.rowIndicator = new TableRowIndicator(
				this.fixedcolumnheader.getHeaderTable(),
				TableRowIndicator.RESIZE_ALL_ROWS, this.getPrefs(), this);
		rowIndicator.addJTableToSynch(getJTable());

		super.postCreate();
		boolean blnSetDynamicRowHeight = this.getTablePreferencesManager()
				.getSelected().isDynamicRowHeight();
		if (blnSetDynamicRowHeight || sf.isDynamicRowHeightsDefault()) {
			// this.getSubForm().setRowHeight(this.getPrefs().getInt(TableRowIndicator.SUBFORM_ROW_HEIGHT,SubForm.DYNAMIC_ROW_HEIGHTS));
			sf.setRowHeight(SubForm.DYNAMIC_ROW_HEIGHTS);
		} else {
			// this.getSubForm().setRowHeight(this.getPrefs().getInt(TableRowIndicator.SUBFORM_ROW_HEIGHT,
			// this.getSubForm().getMinRowHeight()));
			sf.setRowHeight(sf.getMinRowHeight());
		}

		final JTable table = sf.getJTable();
		table.getTableHeader().addMouseListener(new MouseAdapter() {
					public void mouseClicked(final MouseEvent event) {
						if (SwingUtilities.isRightMouseButton(event)) {
							final JPopupMenu pop = new JPopupMenu();
							final JTableHeader header = sf.getJTable().getTableHeader();
							int iColumn = header.columnAtPoint(event.getPoint());
							iColumn = header.getColumnModel().getColumn(iColumn).getModelIndex();
							final Mouse2TableHeaderPopupMenuAdapter adapter = new Mouse2TableHeaderPopupMenuAdapter(table);
							adapter.mouseClicked(event);
							final List<JComponent> lstColumnActions = getColumnIndicatorActions(adapter, iColumn);
							if (lstColumnActions != null) {
								for (JComponent action : lstColumnActions) {
									pop.add(action);
								}
							}
							pop.setLocation(event.getLocationOnScreen());
							pop.show(getJTable(), event.getX(), event.getY());
						}
					}
				});

		fixedcolumnheader.getHeaderTable().getColumnModel().addColumnModelListener(
				newSubFormTablePreferencesUpdateListener());

		List<UID> documentFields = new ArrayList<UID>();
		for (UID field : getCollectableEntity().getFieldUIDs()) {
			CollectableEntityField cef = getCollectableEntity().getEntityField(
					field);
			if (DocumentFileBase.class.isAssignableFrom(cef.getJavaClass())) {
				documentFields.add(field);
			}
		}

		if (documentFields.size() == 1
				&& isColumnEnabled(documentFields.get(0))) {
			sf.setToolbarFunctionState(ToolbarFunction.DOCUMENTIMPORT,
							ToolbarFunctionState.ACTIVE);
		} else {
			sf.setToolbarFunctionState(ToolbarFunction.DOCUMENTIMPORT,
							ToolbarFunctionState.HIDDEN);
		}

		ListenerUtil.registerSubFormToolListener(sf, this,
				new SubFormToolListener() {
					@Override
					public void toolbarAction(String actionCommand) {
						if (ToolbarFunction
								.fromCommandString(actionCommand) == ToolbarFunction.DOCUMENTIMPORT) {
							cmdImportDocuments();
						}
					}
				});

		initAutoNumber();
		initSubformDetailsView();
		initSubformChangeState();

		// Copy & Paste
		ListenerUtil.registerSubFormToolListener(getSubForm(), this,
				new SubFormToolListener() {
					@Override
					public void toolbarAction(String actionCommand) {
						final ToolbarFunction cmd = ToolbarFunction
								.fromCommandString(actionCommand);
						if (cmd != null) {
							switch (cmd) {
							case COPY_ROW:
								if (!cmdCopySelectedRows()) {
									LOG.error("copy row failed");
								}
								break;
							case PASTE_ROW:
								if (!cmdPasteRows()) {
									LOG.error("paste row failed");
								}
								break;
							case CUT_ROW:
								if (!cmdCutSelectedRows()) {
									LOG.error("cut row failed");
								}
								break;
							case FILTER:
								toggleAutoNumberButtons(true);
							default:
								return;
							}
						}
					}
				});

		getFixedColumnHeader().getHeaderTable().addKeyListener(
				new KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent ke) {
						if (ke.isControlDown()
								&& (ke.getKeyCode() == KeyEvent.VK_C)) {
							cmdCopySelectedRows();
						} else if (ke.isControlDown()
								&& (ke.getKeyCode() == KeyEvent.VK_Y)) {
							// may be locked by autonumber
							if (!cmdCutSelectedRows()) {
								LOG.error("cut row failed");
							}

						} else if (ke.isControlDown()
								&& (ke.getKeyCode() == KeyEvent.VK_V)) {
							// may be locked by autonumber
							if (!cmdPasteRows()) {
								LOG.error("paste row failed");
							}

						}
						return;
					}
				});

		this.profilesCtl = new SubformProfilesController(getTablePreferencesManager(),
				new SubFormProfileSupport(), this);
		if (this.profilesCtl.isAvailable()) {
			if (getSubForm().getToolbar().getOrientation() == JToolBar.HORIZONTAL) {
				getSubForm().getToolbar().add(ProfileMenu.createPopupButton(profilesCtl, state));
			}
		}
		
		init();	

		// setup layout navigation support
		this.lns = new LayoutNavigationManager(this);
		getSubForm().setLayoutNavigationSupport(lns);
	}

	protected void cmdOpenSubformDetailsView() {
		if (getCollectController().changesArePending()) {
			JOptionPane.showMessageDialog(getTab(), SpringLocaleDelegate.getInstance().getMessage("DetailsSubFormController.11", null));
			return;
		}
		String detailTitle = RefValueExtractor.get(getCollectController().getSelectedCollectables().get(0), getForeignKeyFieldUID(), true, MetaProvider.getInstance());
		final UID entityUid = this.getCollectableEntity().getUID();
		if (MetaProvider.getInstance().getEntity(entityUid).isStateModel()) {
			try {
				final MainFrameTab tab = new MainFrameTab();
				GenericObjectSplitViewCollectController clct = NuclosCollectControllerFactory
						.getInstance()
						.newGenericObjectCollectControllerSplitView(entityUid, tab, null);
				clct.refreshOnCloseIfNecessary(getCollectController());
				clct.runViewResults(getForeignKeyFieldUID(),
						getCollectController().getSelectedCollectableId(), detailTitle);

				getTab().getTopOverlayOfTab().add(tab);
			} catch (CommonBusinessException e) {
				throw new NuclosFatalException(e);
			}
		} else {
			try {
				MainFrameTab tab = new MainFrameTab();
				MasterDataSplitViewCollectController clct = NuclosCollectControllerFactory
						.getInstance().newMasterDataCollectControllerSplitView(entityUid, tab, null);
				clct.refreshOnCloseIfNecessary(getCollectController());
				clct.runViewResults(getForeignKeyFieldUID(),
						 getCollectController().getSelectedCollectableId(), detailTitle);

				getTab().getTopOverlayOfTab().add(tab);

			} catch (CommonBusinessException e) {
				throw new NuclosFatalException(e);
			}
		}

	}

	/**
	 * @return an list of jmenuitems. if no boolean entityfield available
	 *         returns an empty list.
	 */
	List<JComponent> getRowIndicatorActions() {
		final List<JComponent> result = new LinkedList<JComponent>();

		getSubForm().addToolbarMenuItems(result);
		
		result.add(new JSeparator());

		final Collectable<PK> clct = this.getSelectedCollectable();
		if (clct == null) {
			return result;
		}

		final List<UID> lstFieldNames = new LinkedList<UID>();
		final CollectableEntity clcte = this.getCollectableEntity();
		for (Iterator<UID> iterator = clcte.getFieldUIDs().iterator(); iterator.hasNext();) {
			final UID fieldUid = iterator.next();
			final CollectableEntityField clctef = clcte.getEntityField(fieldUid);
			if (clctef.getJavaClass() == Boolean.class
					&& getSubForm().isColumnVisible(fieldUid)) {
				lstFieldNames.add(fieldUid);
			}
		}

		if (!lstFieldNames.isEmpty()) { // if empty, return an empty list.
			JMenuItem mi1 = new JMenuItem(getSpringLocaleDelegate().getMessage(
					"DetailsSubFormController.1", "Alle setzen"));
			mi1.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					boolean blnChanged = false;
					for (Collectable<PK> clct : DetailsSubFormController.this
							.getSelectedCollectables()) {
						if (clct != null) {
							for (Iterator<UID> iterator = lstFieldNames
									.iterator(); iterator.hasNext();) {
								UID fieldUid = iterator.next();
								if (DetailsSubFormController.this.getSubForm()
										.isColumnEnabled(fieldUid)) {
									int i = getCollectables().indexOf(clct);
									if (i > -1 && !isRowEditable(i)) {
										continue;
									}
									clct.setField(fieldUid,
											new CollectableValueField(
													Boolean.TRUE));
									blnChanged = true;
								}
							}
						}
					}
					if (blnChanged) {
						final AbstractTableModel tblmodel = ((AbstractTableModel) DetailsSubFormController.this
								.getSubForm().getJTable().getModel());
						for (Iterator<UID> iterator = lstFieldNames.iterator(); iterator
								.hasNext();) {
							UID sFieldName = iterator.next();
							if (DetailsSubFormController.this.getSubForm()
									.isColumnEnabled(sFieldName)) {
								tblmodel.fireTableChanged(new TableModelEvent(
										tblmodel, 0,
										DetailsSubFormController.this
												.getSubForm().getJTable()
												.getRowCount() - 1,
										DetailsSubFormController.this
												.getSubForm().getSubformTable()
												.getColumnModel()
												.getColumnIndex(sFieldName),
										TableModelEvent.UPDATE));
							}
						}
					}
				}
			});
			mi1.setEnabled(!getSubForm().isReadOnly() && getSubForm().isEnabled());
			JMenuItem mi2 = new JMenuItem(getSpringLocaleDelegate().getMessage(
					"DetailsSubFormController.2", "Alle zurücksetzen"));
			mi2.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					boolean blnChanged = false;
					for (Collectable<PK> clct : DetailsSubFormController.this
							.getSelectedCollectables()) {
						if (clct != null) {
							for (Iterator<UID> iterator = lstFieldNames
									.iterator(); iterator.hasNext();) {
								UID fieldUid = iterator.next();
								if (DetailsSubFormController.this.getSubForm()
										.isColumnEnabled(fieldUid)) {
									int i = getCollectables().indexOf(clct);
									if (i > -1 && !isRowEditable(i)) {
										continue;
									}
									clct.setField(fieldUid,
											new CollectableValueField(
													Boolean.FALSE));
									blnChanged = true;
								}
							}
						}
					}
					if (blnChanged) {
						final SubForm sf = DetailsSubFormController.this
								.getSubForm();
						final AbstractTableModel tblmodel = ((AbstractTableModel) sf
								.getJTable().getModel());
						for (Iterator<UID> iterator = lstFieldNames.iterator(); iterator
								.hasNext();) {
							UID sFieldName = iterator.next();
							if (DetailsSubFormController.this.getSubForm()
									.isColumnEnabled(sFieldName)) {
								tblmodel.fireTableChanged(new TableModelEvent(
										tblmodel, 0, sf.getJTable()
												.getRowCount() - 1, sf
												.getSubformTable()
												.getColumnModel()
												.getColumnIndex(sFieldName),
										TableModelEvent.UPDATE));
							}
						}
					}
				}
			});
			mi2.setEnabled(!getSubForm().isReadOnly() && getSubForm().isEnabled());
			
			result.add(mi1);
			result.add(mi2);
			result.add(new JSeparator());
		}
		return result;
	}

	/**
	 * @param event MouseEvent on the <em>JTable</em> leading to popup menu request.
	 * @param iColumn JTable column index leading to popup menu request.
	 * @return an list of JMenuItems. if no boolean entityfield available
	 *         returns an empty list.
	 */
	protected List<JComponent> getColumnIndicatorActions(final Mouse2TableHeaderPopupMenuAdapter adapter, final int iColumn) {
		
		final SubForm sf = getSubForm();
		final List<JComponent> result = new LinkedList<JComponent>();
		if (iColumn < 0) {
			return result;
		}
		final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
		final JMenuItem miPopupSortThisColumnAsc = new JMenuItem(sld.getMessage(
				"DetailsSubFormController.6", "Aufsteigen sortieren"));
		miPopupSortThisColumnAsc.setIcon(Icons.getInstance().getIconUp16());
		adapter.register(miPopupSortThisColumnAsc, new ITablePopupMenuListener() {
			
			@Override
			public void tablePopupMenu(TablePopupMenuEvent e) {
				final SortableCollectableTableModel<PK,Clct> tableModel = getCollectableTableModel();
				final int colIndex = e.getColumnIndex();
				final List<SortKey> sortKeys = new ArrayList<SortKey>(tableModel.getSortKeys());
				boolean found = false;
				for (SortKey sortKey : tableModel.getSortKeys()) {
					final int idx = tableModel.getSortKeys().indexOf(sortKey);
					if (sortKey.getColumn() == colIndex) {
						sortKeys.remove(sortKey);
						sortKeys.add(idx, new SortKey(sortKey.getColumn(), SortOrder.ASCENDING));
						found = true;
					}
				}
				if (!found) {
					sortKeys.add(new SortKey(colIndex, SortOrder.ASCENDING));
				}
				tableModel.setSortKeys(sortKeys, true);
			}
		});
		result.add(miPopupSortThisColumnAsc);

		final JMenuItem miPopupSortThisColumnDec = new JMenuItem(sld.getMessage(
				"DetailsSubFormController.7", "Absteigen sortieren"));
		miPopupSortThisColumnDec.setIcon(Icons.getInstance().getIconDown16());
		adapter.register(miPopupSortThisColumnDec, new ITablePopupMenuListener() {
			
			@Override
			public void tablePopupMenu(TablePopupMenuEvent e) {
				final SortableCollectableTableModel<PK,Clct> tableModel = getCollectableTableModel();
				final int colIndex = e.getColumnIndex();
				final List<SortKey> sortKeys = new ArrayList<SortKey>(tableModel.getSortKeys());
				boolean found = false;
				for (SortKey sortKey : tableModel.getSortKeys()) {
					final int idx = tableModel.getSortKeys().indexOf(sortKey);
					if (sortKey.getColumn() == colIndex) {
						sortKeys.remove(sortKey);
						sortKeys.add(idx, new SortKey(sortKey.getColumn(), SortOrder.DESCENDING));
						found = true;
					}
				}
				if (!found) {
					sortKeys.add(new SortKey(colIndex, SortOrder.DESCENDING));
				}
				tableModel.setSortKeys(sortKeys, true);
			}
		});
		result.add(miPopupSortThisColumnDec);

		final JMenuItem miPopupSortThisColumnNone = new JMenuItem(sld.getMessage(
				"DetailsSubFormController.8", "Sortierung aufheben"));
		miPopupSortThisColumnNone.setIcon(Icons.getInstance().getIconUndo16());
		adapter.register(miPopupSortThisColumnNone, new ITablePopupMenuListener() {
			
			@Override
			public void tablePopupMenu(TablePopupMenuEvent e) {
				final SortableCollectableTableModel<PK,Clct> tableModel = getCollectableTableModel();
				final int colIndex = e.getColumnIndex();
				List<SortKey> sortKeys = new ArrayList<SortKey>(tableModel.getSortKeys());
				for (SortKey sortKey : tableModel.getSortKeys()) {
					if (sortKey.getColumn() == colIndex) {
						sortKeys.remove(sortKey);
					}
				}
				tableModel.setSortKeys(sortKeys, true);
				// seems to be needed (tp)
				getJTable().getTableHeader().repaint();
			}
		});
		result.add(miPopupSortThisColumnNone);

		final JMenuItem miPopupSortAllColumnsNone = new JMenuItem(sld.getMessage(
				"DetailsSubFormController.10", "Alle Sortierungen zurücksetzen"));
		miPopupSortAllColumnsNone.setIcon(Icons.getInstance()
				.getIconInsertTable16());
		adapter.register(miPopupSortAllColumnsNone, new ITablePopupMenuListener() {
			
			@Override
			public void tablePopupMenu(TablePopupMenuEvent e) {
				final SortableCollectableTableModel<PK,Clct> tableModel = getCollectableTableModel();
				List<SortKey> sortKeys = new ArrayList<SortKey>(tableModel.getSortKeys());
				for (SortKey sortKey : tableModel.getSortKeys()) {
					sortKeys.remove(sortKey);
				}
				tableModel.setSortKeys(sortKeys, true);
				// seems to be needed (tp)
				getJTable().getTableHeader().repaint();
			}
		});
		result.add(miPopupSortAllColumnsNone);
		result.add(new JSeparator());

		final CollectableEntityField clctef = getCollectableTableModel().getCollectableEntityField(iColumn);
		if (clctef.getJavaClass() == Boolean.class && sf.isColumnVisible(clctef.getUID())) {

			JMenuItem mi1 = new JMenuItem(getSpringLocaleDelegate().getMessage(
					"DetailsSubFormController.1", "Alle setzen"));
			adapter.register(mi1, new ITablePopupMenuListener() {
				
				@Override
				public void tablePopupMenu(TablePopupMenuEvent e) {
					final CollectableEntityField field = clctef;
					final List<Clct> clcts = DetailsSubFormController.this.getCollectables();
					boolean blnChanged = false;
					for (Clct clct : clcts) {
						int i = getCollectables().indexOf(clct);
						if (i > -1 && !isRowEditable(i)) {
							continue;
						}
						clct.setField(field.getUID(),
								new CollectableValueField(Boolean.TRUE));
						blnChanged = true;
					}
					if (blnChanged) {
						final AbstractTableModel tblmodel = ((AbstractTableModel) sf.getJTable().getModel());
						tblmodel.fireTableChanged(new TableModelEvent(tblmodel,
								0, sf.getJTable().getRowCount() - 1, sf
										.getSubformTable().getColumnModel()
										.getColumnIndex(field.getUID()),
								TableModelEvent.UPDATE));
					}
				}
			});
			final SubFormFilter sfFilter = sf.getSubFormFilter();
			mi1.setEnabled(!sf.isReadOnly() && sf.isEnabled()
					&& !sfFilter.isFilteringActive());
			JMenuItem mi2 = new JMenuItem(getSpringLocaleDelegate().getMessage(
					"DetailsSubFormController.2", "Alle zurücksetzen"));
			adapter.register(mi2, new ITablePopupMenuListener() {
				
				@Override
				public void tablePopupMenu(TablePopupMenuEvent e) {
					final CollectableEntityField field = clctef;
					boolean blnChanged = false;
					final List<Clct> clcts = DetailsSubFormController.this.getCollectables();
					for (Clct clct : clcts) {
						int i = getCollectables().indexOf(clct);
						if (i > -1 && !isRowEditable(i)) {
							continue;
						}
						clct.setField(field.getUID(),
								new CollectableValueField(Boolean.FALSE));
						blnChanged = true;
					}
					if (blnChanged) {
						final AbstractTableModel tblmodel = ((AbstractTableModel) sf
								.getJTable().getModel());
						tblmodel.fireTableChanged(new TableModelEvent(tblmodel,
								0, sf.getJTable().getRowCount() - 1, sf
										.getSubformTable().getColumnModel()
										.getColumnIndex(field.getUID()),
								TableModelEvent.UPDATE));
					}
				}
			});
			mi2.setEnabled(!sf.isReadOnly() && sf.isEnabled()
					&& !sfFilter.isFilteringActive());

			result.add(mi1);
			result.add(mi2);
			result.add(new JSeparator());

			JMenuItem mi3 = new JMenuItem(getSpringLocaleDelegate().getMessage(
					"DetailsSubFormController.setselected",
					"Selektierte Zeilen setzen"));
			adapter.register(mi3, new ITablePopupMenuListener() {
				
				@Override
				public void tablePopupMenu(TablePopupMenuEvent e) {
					final CollectableEntityField field = getCollectableTableModel().getCollectableEntityField(
							e.getColumnIndex());
					final List<Clct> clcts = DetailsSubFormController.this.getSelectedCollectables();
					boolean blnChanged = false;
					for (Clct clct : clcts) {
						clct.setField(field.getUID(),
								new CollectableValueField(Boolean.TRUE));
						blnChanged = true;
					}
					if (blnChanged) {
						fireDataUpdatedForSelectedRows();
					}
				}
			});
			mi3.setEnabled(!sf.isReadOnly() && sf.isEnabled()
					&& !sfFilter.isFilteringActive() && getSelectedCollectables().size() > 0);
			JMenuItem mi4 = new JMenuItem(getSpringLocaleDelegate().getMessage(
					"DetailsSubFormController.resetselected",
					"Selektierte Zeilen zurücksetzen"));
			adapter.register(mi4, new ITablePopupMenuListener() {
				
				@Override
				public void tablePopupMenu(TablePopupMenuEvent e) {
					final CollectableEntityField field = getCollectableTableModel().getCollectableEntityField(
							e.getColumnIndex());
					boolean blnChanged = false;
					final List<Clct> clcts = DetailsSubFormController.this.getSelectedCollectables();
					for (Clct clct : clcts) {
						clct.setField(field.getUID(),
								new CollectableValueField(Boolean.FALSE));
						blnChanged = true;
					}
					if (blnChanged) {
						fireDataUpdatedForSelectedRows();
					}
				}
			});
			mi4.setEnabled(!sf.isReadOnly() && sf.isEnabled()
				&& !sfFilter.isFilteringActive() && getSelectedCollectables().size() > 0);

			result.add(mi3);
			result.add(mi4);
		}
		if (!result.isEmpty()) {
			result.add(new JSeparator());
		}
		if (SecurityCache.getInstance().isActionAllowed(
				Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS)
				|| !Main.getInstance().getMainFrame().getWorkspace()
						.isAssigned()) {
			final JMenuItem miPopupHideThisColumn = new JMenuItem(
					SpringLocaleDelegate.getInstance().getMessage(
							"DetailsSubFormController.5",
							"Diese Spalte ausblenden"));
			miPopupHideThisColumn.setIcon(Icons.getInstance()
					.getIconRemoveColumn16());
			adapter.register(miPopupHideThisColumn, new ITablePopupMenuListener() {
				
				@Override
				public void tablePopupMenu(TablePopupMenuEvent e) {
					final int colIndex = e.getColumnIndex();					
					final CollectableEntityField field = getTableColumns().get(colIndex);
									
					fixedcolumnheader.hideCollectableEntityFieldColumn(field,
							new AbstractAction() {
								@Override
								public void actionPerformed(ActionEvent e) {
									List<? extends SortKey> sortKeys = new ArrayList<SortKey>(
											getCollectableTableModel().getSortKeys());
									for (SortKey sortKey : getCollectableTableModel().getSortKeys()) {
										if (sortKey.getColumn() == colIndex) {
											sortKeys.remove(sortKey);
										}
									}
									getCollectableTableModel().setSortKeys(sortKeys, true);
								}
							});
				}
			});
			result.add(miPopupHideThisColumn);
		}
		// NUCLOS-1479
		if (profilesCtl.isAvailable()) {
			result.add(new JSeparator());
			result.add(ProfileMenu.createMenu(profilesCtl, state));
		}
		return result;
	}

	private void fireDataUpdatedForSelectedRows() {
		int first = DetailsSubFormController.this.getSubForm().getJTable()
				.getSelectionModel().getMinSelectionIndex();
		int last = DetailsSubFormController.this.getSubForm().getJTable()
				.getSelectionModel().getMinSelectionIndex();
		((AbstractTableModel) DetailsSubFormController.this.getSubForm()
				.getJTable().getModel()).fireTableRowsUpdated(first, last);
	}

	/**
	 * sets the responsible CollectController for this SubForm
	 */
	public void setCollectController(
			EntityCollectController<?, ?> masterDataCollectController) {
		this.cltctl = masterDataCollectController;
	}

	/**
	 * @return the responsible CollectController of this SubForm
	 */
	@Override
	public EntityCollectController<?, ?> getCollectController() {
		return this.cltctl;
	}

	protected final UID getCurrentLayoutUid() {
		return this.cltctl != null ? this.cltctl.getCurrentLayoutUid() : null;
	}

	/**
	 * releases resources, removes listeners.
	 */
	@Override
	public void close() {
		if (getSubForm() != null) {
			final JTable table = getJTable();
			if (table != null) {
				TableUtils.removeMouseListenersForSortingFromTableHeader(table);
			}
		}

		this.removeColumnModelListener();
		this.removeTableModelListener();

		if (getSubForm() != null) {
			/*
			getSubForm().getSubFormFilter().storeTableFilter(
					MetaProvider.getInstance().getEntity(getParentEntityUID()).getUID());
			 */
			getSubForm().getSubFormFilter().close();
		}

		fixedcolumnheader = null;
		lns = null;
		rowIndicator = null;
		dragAndDropContext = null;
		profilesCtl = null;

		// cleanup clipboard listeners (FIXME this erases all flavor listeners)
		final Clipboard sysClip = Toolkit.getDefaultToolkit().getSystemClipboard();
		for (final FlavorListener listener : sysClip.getFlavorListeners()) {
			sysClip.removeFlavorListener(listener);
		}
		super.close();
	}

	/**
	 * @param lstclct List&lt;Collectable&gt;
	 * @return a new <code>ValueObjectList</code> containing the given
	 *         <code>Collectable</code>s.
	 */
	protected abstract ValueObjectList<Clct> newValueObjectList(
			List<Clct> lstclct);

	@Override
	protected List<Clct> newCollectableList(List<Clct> lstclct) {
		return this.newValueObjectList(lstclct);
	}

	public Object getParentId() {
		return oParentId;
	}

	public void setParentId(Object oParentId) {
		this.oParentId = oParentId;
	}

	/**
	 * validates the Collectables in this subform and the childsubforms if any,
	 * sets the given parent id in the foreign key field of each Collectable and
	 * returns them.
	 * 
	 * §postcondition result != null
	 * @return All collectables, even the removed ones.
	 */
	public List<Clct> getAllCollectables(Object oParentId,
			Collection<DetailsSubFormController<PK, Clct>> collSubForms,
			boolean bSetParent, Clct clct) throws CommonValidationException {
		final EntityAndField entityAndField = getEntityAndForeignKeyField();
		final List<Clct> result;

		if (bSetParent) {
			result = getCollectables(oParentId, true, true, true);
		} else {
			if (clct != null && clct instanceof CollectableMasterData) {
				// don't get collectables from table model for dependant data of
				// subform childs
				final DependantCollectableMasterDataMap dcmdm = ((CollectableMasterData<PK>) clct)
						.getDependantCollectableMasterDataMap();
				result = prepareAndValidateCollectables(
						(List) dcmdm.getValues(entityAndField.getDependentKey()), true, false, true);
			} else {
				result = getCollectables(true, true, true);
			}
		}

		if (!entityAndField.getEntity().equals(getParentEntityUID())) { // @see RSWORGA-104
			for (DetailsSubFormController<PK, Clct> subFormController : CollectionUtils
					.emptyIfNull(collSubForms)) {
				final UID pentity = subFormController.getParentEntityUID();
				if (entityAndField.getEntity().equals(pentity)) {
					// dependant subform case
					for (Clct clct1 : result) {
						subFormController.setCollectables(subFormController
								.getAllCollectables(clct1.getId(), collSubForms,
										false, clct1));
					}
				}
			}
		}
		return result;
	}

	/**
	 * @return the ids of all <code>Collectable</code>s in this subform,
	 *         including incomplete ones, excluding removed ones.
	 */
	public List<PK> getCollectableIds() {
		return CollectionUtils.transform(this.getCollectables(),
				new Collectable.GetId<PK>());
	}

	/**
	 * sets the parent id on the Collectables in this subform and returns them.
	 * 
	 * §postcondition result != null
	 * @param bIncludeIncompleteOnes
	 *            Include incompletes <code>Collectable</code>s in the
	 *            <code>result</code>?
	 * @param bIncludeRemovedOnes
	 *            Include removed <code>Collectable</code>s in the
	 *            <code>result</code>?
	 * @param bPrepareForSavingAndValidate
	 *            prepare for saving and validate? In this case, empty rows are
	 *            removed, booleans that are <code>null</code> are mapped to
	 *            <code>false</code>.
	 * @return the <code>Collectable</code>s in this subform
	 */
	protected List<Clct> getCollectables(Object oParentId,
			boolean bIncludeIncompleteOnes, boolean bIncludeRemovedOnes,
			boolean bPrepareForSavingAndValidate)
			throws CommonValidationException {
		final List<Clct> result = this.getCollectables(bIncludeIncompleteOnes,
				bIncludeRemovedOnes, bPrepareForSavingAndValidate);
		// set parent id on those Collectables:
		/** @todo this doesn't belong here */
		for (Collectable<PK> clct : result) {
			this.setParentId(clct, oParentId);
		}
		assert result != null;
		return result;
	}

	/**
	 * §postcondition result != null
	 * @param bIncludeIncompleteOnes
	 *            Include incomplete <code>Collectable</code>s in the
	 *            <code>result</code>?
	 * @param bIncludeRemovedOnes
	 *            Include removed <code>Collectable</code>s in the
	 *            <code>result</code>?
	 * @param bPrepareForSavingAndValidate
	 *            prepare for saving and validate? In this case, empty rows are
	 *            removed, booleans that are <code>null</code> are mapped to
	 *            <code>false</code>.
	 * @return the <code>Collectable</code>s in this subform
	 */
	public List<Clct> getCollectables(boolean bIncludeIncompleteOnes,
			boolean bIncludeRemovedOnes, boolean bPrepareForSavingAndValidate)
			throws CommonValidationException {
		if (!this.stopEditing()) {
			throw new CommonValidationException(getSpringLocaleDelegate()
					.getMessage("details.subform.controller",
							"Ung\u00fcltige Eingabe im Unterformular ''{0}''",
							getCollectableEntity().getLabel()));
		}

		final List<Clct> result = bIncludeIncompleteOnes ? new ArrayList<Clct>(
				this.getCollectables()) : CollectionUtils.select(
				this.getCollectables(), new Collectable.IsComplete());

		if (bPrepareForSavingAndValidate) {
			this.prepareForSaving(result);
			this.validate(result);
		}

		if (bIncludeRemovedOnes) {
			List<Clct> lst = this.getModifiableListOfCollectables();
			if (lst instanceof ValueObjectList) {
				result.addAll(((ValueObjectList<Clct>)lst).getRemovedObjects());
			}
		}
		assert result != null;
		return result;
	}

	public List<Clct> prepareAndValidateCollectables(List result,
			boolean bIncludeIncompleteOnes, boolean bIncludeRemovedOnes,
			boolean bPrepareForSavingAndValidate)
			throws CommonValidationException {
		if (!this.stopEditing()) {
			throw new CommonValidationException(getSpringLocaleDelegate()
					.getMessage("details.subform.controller",
							"Ung\u00fcltige Eingabe im Unterformular ''{0}''",
							getCollectableEntity().getLabel()));
		}

		result = bIncludeIncompleteOnes ? result : CollectionUtils.select(
				result, new Collectable.IsComplete());

		if (bPrepareForSavingAndValidate) {
			this.prepareForSaving(result);
			this.validate(result);
		}

		if (bIncludeRemovedOnes) {
			List<Clct> lst = this.getModifiableListOfCollectables();
			if (lst instanceof ValueObjectList) {
				result.addAll(((ValueObjectList<Clct>)lst).getRemovedObjects());
			}
		}
		assert result != null;
		return result;
	}

	/**
	 * prepares the <code>Collectable</code>s in this subform for saving: Empty
	 * rows are removed, when they do not contain Booleans. Else, Booleans that
	 * are <code>null</code> are mapped to <code>false</code>.
	 */
	private void prepareForSaving(Collection<Clct> collclct) {
		final CollectableEntity clcte = this.getCollectableEntity();
		final Collection<UID> collFieldUIDs = getNonForeignKeyFieldUids(clcte,
				getForeignKeyFieldUID());

		for (Clct clct : new ArrayList<Clct>(collclct)) {
			if (containsOnlyNullFields(clct, collFieldUIDs)) {
				collclct.remove(clct);
			} else {
				// Note that the foreign key field needn't be treated specially,
				// as it is never a Boolean.
				Utils.mapNullBooleansToFalseInCollectable(clct, clcte);
			}
		}
	}

	/**
	 * validates the given <code>Collectable</code>s (excluding the foreign key
	 * field):
	 * 
	 * @throws CommonValidationException
	 * @param collclct
	 */
	private void validate(Collection<? extends Collectable<PK>> collclct)
			throws CommonValidationException {
		// no validation against metadata in client.
	}

	private static Collection<UID> getNonForeignKeyFieldUids(
			CollectableEntity clcte, UID sForeignKeyFieldUid) {
		return CollectionUtils.select(clcte.getFieldUIDs(), PredicateUtils
				.not(PredicateUtils.<UID> isEqual(sForeignKeyFieldUid)));
	}

	protected final void setupHeaderToolTips(JTable tbl) {
		tbl.setTableHeader(new ToolTipsTableHeader(this
				.getCollectableTableModel(), tbl.getColumnModel()));
	}

	/**
	 * sets the parent id of the given <code>Collectable</code>. The
	 * corresponding field in <code>clct</code> is not changed if the parent id
	 * is already equal to the given parent id.
	 * 
	 * @param clct
	 * @param oParentId
	 */
	protected void setParentId(Collectable<PK> clct, Object oParentId) {
		final UID foreignKeyFieldUid = getForeignKeyFieldUID();
		if (!LangUtils.equal(clct.getField(foreignKeyFieldUid).getValueId(),
				oParentId)) {
			clct.setField(foreignKeyFieldUid, new CollectableValueIdField(
					oParentId, null));
		}
	}

	/**
	 * inserts a new row. The foreign key field to the parent entity will be set
	 * accordingly.
	 */
	@Override
	public Clct insertNewRow() throws NuclosBusinessException {
		final Clct clctNew = this.newCollectable();
		this.setParentId(clctNew, this.getParentId());
		Utils.setDefaultValues(clctNew, this.getCollectableEntity());
		return insertExistingRow(clctNew);
	}

	@Override
	public Clct insertNewRow(int idx) throws NuclosBusinessException {
		final Clct clctNew = this.newCollectable();
		this.setParentId(clctNew, this.getParentId());
		Utils.setDefaultValues(clctNew, this.getCollectableEntity());
		return insertExistingRow(clctNew, idx);
	}

	protected void setupLocalizedFields(Clct clctNew) {
		// implement if needed
	}
	
	public Clct insertExistingRow(Clct clct) throws NuclosBusinessException {
		// FIXME remove invalid index -1
		return insertExistingRow(clct, -1);
	}

	public Clct insertExistingRow(Clct clct, int idx)
			throws NuclosBusinessException {
		
		final SubForm sf = getSubForm();
		final SubFormFilter sfFilter = sf.getSubFormFilter();
		if (sfFilter.isFilteringActive()) {
			// inserted collectable
			final Map<UID, CollectableComponent> comps = sfFilter.getAllFilterComponents();
			if (comps != null) {
				for (UID key : comps.keySet()) {
					CollectableComponent clctComp = comps.get(key);
					if (clctComp != null) {
						try {
							CollectableField cf = clctComp.getField();
							if (!cf.isNull()) {
								clct.setField(clctComp.getEntityField().getUID(), cf);
							}
						} catch (CollectableFieldFormatException e) {
							throw new CommonFatalException(e);
						}
					}
				}
			}
		}

		if (this.isMultiEdit()) {
			sf.getJTable().setBackground(null);
		}
		// FIXME this was added to not touch the old behavior for add()
		if (idx == -1 || idx > this.getCollectableTableModel().getRowCount()) {
			this.getCollectableTableModel().add(clct);
		} else {
			this.getCollectableTableModel().addCollectable(idx, clct);
		}
		
		setupLocalizedFields(clct);
		
		return clct;
	}

	/**
	 * @return the selected collectable, if any.
	 */
	public final Clct getSelectedCollectable() {
		final int iSelectedRow = this.getJTable().getSelectedRow();
		// Note that iSelectedRow >= this.getTableModel().getRowCount() may
		// occur during deletion of the last element.
		// We take care of such a temporary inconsistency here.
		if (iSelectedRow == -1
				|| iSelectedRow >= this.getCollectableTableModel()
						.getRowCount())
			return null;

		try {
			int modelIndex = this.getJTable().convertRowIndexToModel(iSelectedRow);
			return (modelIndex == -1) ? null : this.getCollectableTableModel()
					.getCollectable(modelIndex);
		} catch (ArrayIndexOutOfBoundsException aioobex) {
			return null;
		}
	}

	public final List<Clct> getSelectedCollectables() {
		final List<Integer> lstSelectedRowNumbers = CollectionUtils.asList(this
				.getJTable().getSelectedRows());
		final List<Clct> result = CollectionUtils.transform(
				lstSelectedRowNumbers, new Transformer<Integer, Clct>() {
					@Override
					public Clct transform(Integer iRowNo) {
						try {
							int modelIndex = getJTable().convertRowIndexToModel(
									iRowNo);
							return getCollectableTableModel().getCollectable(
									modelIndex);
						} catch (ArrayIndexOutOfBoundsException aioobex) {
							return null;
						}
					}
				});
		assert result != null;
		return result;
	}

	/**
	 * removes all rows from this subform.
	 */
	public void clear() {
		this.setMultiUpdateOfDependants(null);
		this.updateTableModel(new ArrayList<Clct>());
	}

	@Override
	public ProfileItem getSelectedProfile() {
		return profilesCtl.getModel().getSelectedProfile();
	}

	@Override
	public void updateSelectedProfile() {
		getTablePreferencesManager().update(getSelectedProfile().getPreferences());
	}

	/**
	 * @param clct
	 * @param collFieldUids
	 * @return Does the given <code>Collectable</code> contain only null values
	 *         in the fields specified by <code>collFieldNames</code>?
	 */
	protected static <PK> boolean containsOnlyNullFields(Collectable<PK> clct,
			Collection<UID> collFieldUids) {
		boolean result = true;
		for (UID fieldUid : collFieldUids) {
			final CollectableField clctf = clct.getField(fieldUid);
			if (!clctf.isNull()) {
				result = false;
				break;
			}
		}
		return result;
	}

	/**
	 * copies the model from <code>subformctlSource</code> to
	 * <code>subformctlDest</code>.
	 * 
	 * @param subformctlSource
	 * @param subformctlDest
	 */
	public static <PK, Clct extends Collectable<PK>> void copyModel(
			DetailsSubFormController<PK, Clct> subformctlSource,
			DetailsSubFormController<PK, Clct> subformctlDest) {
		subformctlDest.setParentId(subformctlSource.getParentId());

		List<Clct> lst = subformctlSource.getModifiableListOfCollectables();
		if (lst instanceof ValueObjectList) {
			subformctlDest.setCollectables(((ValueObjectList<Clct>)lst).clone());
		} else {
			subformctlDest.setCollectables(new ArrayList<Clct>(lst));
		}
	}

	/**
	 * stores the order of the columns in the table
	 */
	@Override
	protected void storeColumnOrderAndWidths(JTable tbl) {
		super.storeColumnOrderAndWidths(tbl);
		try {
			if (fixedcolumnheader != null) {
				this.fixedcolumnheader
						.writeFieldToPreferences(getTablePreferencesManager());
			}
		} catch (PreferencesException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public void selectFirstRow() {
	}

	public void setMultiEdit(boolean multiEdit) {
		this.multiEdit = multiEdit;
		getSubForm().setMultiEdit(multiEdit);
	}

	public void blockSubForm(String text, boolean b) {
		if (b) {
			getSubForm().setLockedLayer(text);
		} else {
			getSubForm().forceUnlockFrame();
		}
	}

	public boolean isMultiEdit() {
		return this.multiEdit;
	}

	public IMultiUpdateOfDependants getMultiUpdateOfDependants() {
		return multiUpdateOfDependants;
	}

	public void setMultiUpdateOfDependants(
			IMultiUpdateOfDependants multiUpdateOfDependants) {
		if (this.multiUpdateOfDependants != null) {
			getSubForm().removeSubFormToolListener(this.multieditToolListener);
			getSubForm().getJTable().getSelectionModel()
					.removeListSelectionListener(multieditSelectionListener);
			this.multiUpdateOfDependants.close(this);
		}
		this.multiUpdateOfDependants = multiUpdateOfDependants;
		if (multiUpdateOfDependants != null) {
			getSubForm().setTableCellRendererProvider(multiUpdateOfDependants);
			getSubForm().addSubFormToolListener(multieditToolListener);
			getSubForm().getJTable().getSelectionModel()
					.addListSelectionListener(multieditSelectionListener);
			getSubForm().setToolbarFunctionState(ToolbarFunction.TRANSFER,
					ToolbarFunctionState.DISABLED);
		} else {
			getSubForm().setTableCellRendererProvider(this);
			getSubForm().setToolbarFunctionState(ToolbarFunction.TRANSFER,
					ToolbarFunctionState.HIDDEN);
		}
	}

	@Override
	public boolean isRowEditable(int row) {
		return true;
	}

	@Override
	public boolean isRowRemovable(int row) {
		boolean result = getSubForm().canDelete();

		// why?
		/*final ToolbarFunctionState state
			= getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.REMOVE);
		if (null != state && !ToolbarFunctionState.ACTIVE.equals(state)) {
			result = false;
		}*/

		if (result && getSubForm().getDeleteEnabledScript() != null) {
			DetailsSubFormController parentSubformController = getCollectController()
					.getDetailsController().getSubFormController(getSubForm().getParentSubForm());
			Collectable<PK> c = getCollectables().get(row);
			Object o = null;
			try {
				o = ScriptEvaluator.getInstance().eval(
						getSubForm().getDeleteEnabledScript(),
						new SubformControllerScriptContext(getCollectController(), parentSubformController, DetailsSubFormController.this, c));
			} catch (InvocationTargetException e) {
				LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
				o = false;
			} catch (Exception e) {
				LOG.warn("Failed to evaluate script expression: " + e, e);
				o = false;
			}
			if (o instanceof Boolean) {
				result = (Boolean) o;
			}
		}
		return result;
	}

	@Override
	public TableCellRenderer getTableCellRenderer(
			CollectableEntityField clctefTarget) {
		final TableCellRenderer result = super
				.getTableCellRenderer(clctefTarget);

		if (isMultiEdit()) {
			return new TableCellRenderer() {
				@Override
				public Component getTableCellRendererComponent(JTable table,
						Object value, boolean isSelected, boolean hasFocus,
						int row, int column) {

					/**
					 * TODO: Using a DefaultTableCellRenderer as a fallback is only a workaround.
					 * Instead of this, make sure that result is never null.
					 */
					Component c;
					if (result != null) {
						c = result.getTableCellRendererComponent(
								table,
								value,
								isSelected,
								hasFocus,
								row,
								column);
					} else {
						c = new DefaultTableCellRenderer().getTableCellRendererComponent(
								table,
								value,
								isSelected,
								hasFocus,
								row,
								column
						);
					}

					if (!isRowEditable(row) && !isSelected) {
						c.setBackground(NuclosThemeSettings.BACKGROUND_INACTIVEROW);
					}
					return c;
				}
			};
		} else {
			return result;
		}
	}

	protected void setupDragDrop() {
		DropTarget dropTargetSubform = new DropTarget(this.getSubForm()
				.getJTable(), new NuclosDropTargetListener(this));
		dropTargetSubform.setActive(true);

		DropTarget dropTargetButton = new DropTarget(this.getSubForm()
				.getToolbarButton("NEW"), new NuclosDropTargetListener(this));
		dropTargetButton.setActive(true);

		DropTarget dropTargetScroll = new DropTarget(this.getSubForm()
				.getSubformScrollPane().getViewport(),
				new NuclosDropTargetListener(this));
		dropTargetScroll.setActive(true);

		// NUCLOS-1477, NUCLOS-1478
		final JTable tblSubFormHeader = this.fixedcolumnheader.getHeaderTable();
		DropTarget dropTargetHeader = new DropTarget(tblSubFormHeader,
				new NuclosDropTargetListener(this));
		dropTargetHeader.setActive(true);

		tblSubFormHeader.setTransferHandler(new TableRowCopyTransferHandler(
				(SubFormTable) getJTable()));
		tblSubFormHeader.setDragEnabled(true);
		this.dragAndDropContext = new DragAndDropContext();

		getFixedColumnHeader().getHeaderTable().addMouseListener(
				new MouseAdapter() {
					@Override
					public void mousePressed(final MouseEvent e) {
						stopEditing();
						dragAndDropContext.setRowDragged(getFixedColumnHeader()
								.getHeaderTable().rowAtPoint(e.getPoint()));
					}
				});
	}

	protected void insertNewRowFromDrop(File file) throws IOException,
			NuclosBusinessException {
		final Clct clctNew = insertNewRow();

		final InputStream fis = new BufferedInputStream(new FileInputStream(
				file));
		final byte[] b;
		try {
			b = new byte[(int) file.length()];
			fis.read(b);
		} finally {
			fis.close();
		}

		UID newDocFileUID = DocumentFileBase.newFileUID();
		GenericObjectDocumentFile docfile = new GenericObjectDocumentFile(
				file.getName(), newDocFileUID, b);
		final UID sFieldDocument = getDocumentField();
		clctNew.setField(sFieldDocument, new CollectableValueIdField(newDocFileUID, docfile));
		// for multiedit...
		int column = getCollectableTableModel().getColumn(this.getCollectableEntity().getEntityField(sFieldDocument));
		for (int row = 0; row < getCollectableTableModel().getRowCount(); row++) {
			Clct clctRow = getCollectableTableModel().getRow(row);
			if (clctRow.equals(clctNew)) {
				((AbstractTableModel)getSubForm().getJTable().getModel()).fireTableCellUpdated(row, column);
				break;
			}
		}
	}

	private UID getDocumentField() {
		UID sField = null;
		for (UID sf : this.getCollectableEntity().getFieldUIDs()) {
			if (this.getCollectableEntity()
					.getEntityField(sf)
					.getJavaClass()
					.isAssignableFrom(
							org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile.class)) {
				sField = sf;
				break;
			}
		}
		return sField;
	}

	protected void updateRowFromDrop(int hereRow, List<File> files)
			throws FileNotFoundException, IOException {
		for (Iterator<File> it = files.iterator(); it.hasNext();) {
			File file = it.next();
			CollectableMasterData<PK> clma = (CollectableMasterData<PK>) DetailsSubFormController.this
					.getCollectables().get(hereRow);

			final InputStream fis = new BufferedInputStream(
					new FileInputStream(file));
			final byte[] b;
			try {
				b = new byte[(int) file.length()];
				fis.read(b);
			} finally {
				fis.close();
			}

			final UID sFieldDocument = getDocumentField();
			CollectableField cf = clma.getField(sFieldDocument);
			UID newDocFileUID;
			if (cf != null && cf.getValue() instanceof GenericObjectDocumentFile) {
				newDocFileUID = (UID)((GenericObjectDocumentFile)cf.getValue()).getDocumentFilePk();
			} else {
				newDocFileUID = DocumentFileBase.newFileUID();
			}
			GenericObjectDocumentFile docfile = new GenericObjectDocumentFile(
					file.getName(), newDocFileUID, b);
			clma.setField(sFieldDocument, new CollectableValueIdField(newDocFileUID, docfile));
			SortableCollectableTableModel<PK, CollectableMasterData<PK>> model = (SortableCollectableTableModel<PK, CollectableMasterData<PK>>) DetailsSubFormController.this
					.getCollectableTableModel();
			model.setCollectable(hereRow, clma);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					DetailsSubFormController.this.getJTable().repaint();
				}
			});
		}
	}

	@Override
	public void visitDragEnter(DropTargetDragEvent dtde) {
	}

	@Override
	public void visitDragExit(DropTargetEvent dte) {
	}

	@Override
	public void visitDragOver(DropTargetDragEvent dtde) {
		if ((dtde.getSourceActions() & DnDConstants.ACTION_COPY_OR_MOVE) == 0
				|| this.getSubForm().isReadOnly()
				|| this.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.NEW) != ToolbarFunctionState.ACTIVE) {
			dtde.rejectDrag();
			return;
		}

		// check for TransferableGenericObjects or MasterDataVO support
		if (dtde.isDataFlavorSupported(TransferableGenericObjects.dataFlavor)
				|| dtde.isDataFlavorSupported(MasterDataIdAndEntity.dataFlavor)) {
			dtde.acceptDrag(dtde.getDropAction());
			return;
		}

		// NUCLOS-1477, NUCLOS-1478
		if (dtde.isDataFlavorSupported(TableRowCopyTransferHandler.RowTransferable
				.getDataFlavor())) {
			try {
				if (isRowMovingAllowed()) {
					dtde.acceptDrag(dtde.getDropAction());
				} else {
					LOG.error("move or copy rows rejected.");
					dtde.rejectDrag();
				}

				final TransferableWrapper<SubFormTable, RowTransferable> wrapper = (TransferableWrapper<SubFormTable, RowTransferable>) dtde
						.getTransferable().getTransferData(
								TableRowCopyTransferHandler.RowTransferable
										.getDataFlavor());

				final SubFormTable sfTable = (SubFormTable) getJTable();
				if (sfTable == wrapper.getCustomObject()) {
					// source table == target table => move(reorder) rows
					// TODO
					int iRowDropped = sfTable.rowAtPoint(dtde.getLocation());
					boolean bRemoveSelection = true;

					final int[] arrRows = CollectionUtils
							.asPrimitiveIntArray(wrapper.getTransferable()
									.getRows().keySet());

					// intersection with moving rows
					for (int iRow : arrRows) {
						if (dragAndDropContext.getRowLastPointed() == iRow) {
							bRemoveSelection = false;
							break;
						}
					}
					if (bRemoveSelection) {
						sfTable.getSelectionModel().removeSelectionInterval(
								dragAndDropContext.getRowLastPointed(),
								dragAndDropContext.getRowLastPointed());
					}
					sfTable.getSelectionModel().addSelectionInterval(
							iRowDropped, iRowDropped);
					dragAndDropContext.setRowLastPointed(iRowDropped);

					// scroll table if necessary:
					if (sfTable.getAutoscrolls()) {
						final int iRowIndex = (iRowDropped > dragAndDropContext
								.getRowDragged()) ? iRowDropped + 1
								: iRowDropped - 1;
						final int iColumnIndex = 0;

						final Rectangle cellRect = sfTable.getCellRect(
								iRowIndex, iColumnIndex != -1 ? iColumnIndex
										: 0, true);
						if (cellRect != null) {
							sfTable.scrollRectToVisible(cellRect);
						}
					}
				} else {
					// source table != target table => copy rows
					// hilight hovered rows
					int iRow = getSubForm().getJTable().rowAtPoint(
							dtde.getLocation());
					getSubForm().getJTable().getSelectionModel()
							.setSelectionInterval(iRow, iRow);

				}
			} catch (final Exception ex) {
				LOG.error("move or copy rows failed.", ex);
				throw new NuclosFatalException(ex);
			}

			return;
		}

		final Transferable trans = dtde.getTransferable();
		final DataFlavor flavors[] = trans.getTransferDataFlavors();

		final Point here = dtde.getLocation();
		final int hereRow = DetailsSubFormController.this.getSubForm()
				.getJTable().rowAtPoint(here);

		for (int i = 0; i < flavors.length; i++) {
			try {
				if (flavors[i].isFlavorJavaFileListType()) {
					final List<File> files = (List<File>) trans
							.getTransferData(flavors[i]);
					if (files.size() == 1 && hereRow >= 0) {
						;
					} else if (hereRow == -1) {
						;
					} else {
						dtde.rejectDrag();
						return;
					}
				}
			} catch (Exception e) {
				// ignore.
			}
		}

		// check for File support
		boolean blnAcceptFileChosser = false;
		boolean blnAcceptFileList = false;
		boolean blnAcceptEmail = false;

		// check if there is an DocumentFileBase in the subform, otherwise don't
		// let the user drop files
		CollectableEntity entity = DetailsSubFormController.this
				.getCollectableEntity();
		Set<UID> setEntities = entity.getFieldUIDs();
		for (UID entityUid : setEntities) {
			CollectableEntityField field = entity.getEntityField(entityUid);
			Class<?> clazz = field.getJavaClass();
			if (DocumentFileBase.class.isAssignableFrom(clazz)) {
				blnAcceptFileChosser = true;
				break;
			}
		}

		for (int i = 0; i < flavors.length; i++) {
			if (flavors[i].isFlavorJavaFileListType()) {
				blnAcceptFileList = true;
				break;
			}
		}

		// check if one or more outlook email want to be dropped
		if (flavors != null && flavors.length > 0) {
			int count = flavors.length;
			for (int i = 0; i < count; i++) {
				try {
					Object obj = trans.getTransferData(flavors[i]);
					if (obj instanceof String) {
						String strRow = (String) obj;
						if (strRow.indexOf("Betreff") != -1) {
							blnAcceptEmail = true;
						}
					}
				} catch (Exception e) {
					// do nothing here
					LOG.warn("visitDragOver fails on Betreff to " + flavors[i] + ": " + e);
				}
			}
		}

		if (blnAcceptFileChosser && (blnAcceptFileList || blnAcceptEmail)) {
			dtde.acceptDrag(dtde.getDropAction());
		} else {
			dtde.rejectDrag();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void visitDrop(DropTargetDropEvent dtde) {
		if ((dtde.getSourceActions() & DnDConstants.ACTION_COPY_OR_MOVE) == 0) {
			dtde.rejectDrop();
			return;
		}

		try {
			boolean blnViewPort = false;
			if (dtde.getSource() instanceof DropTarget) {
				if (((DropTarget) dtde.getSource()).getComponent() instanceof JViewport) {
					blnViewPort = true;
				}
			}
			// NUCLOS-1477, NUCLOS-1478
			if (dtde.getCurrentDataFlavorsAsList()
					.contains(
							TableRowCopyTransferHandler.RowTransferable
									.getDataFlavor())) {
				final TransferableWrapper<SubFormTable, RowTransferable> wrapper = (TransferableWrapper<SubFormTable, RowTransferable>) dtde
						.getTransferable().getTransferData(
								TableRowCopyTransferHandler.RowTransferable
										.getDataFlavor());
				final int idxInsert = getSubForm().getJTable().rowAtPoint(
						dtde.getLocation());
				if (getSubForm().getJTable() == wrapper.getCustomObject()) {
					if (!processMoveDraggedRows(idxInsert, wrapper)) {
						LOG.error("move rows failed");
					} else {
						LOG.error("moved "
								+ wrapper.getTransferable().getRows().size()
								+ " rows");
					}
				} else {
					if (!processCopyDraggedRows(idxInsert, wrapper)) {
						LOG.error("copy rows failed");
					} else {
						LOG.debug("copied "
								+ wrapper.getTransferable().getRows().size()
								+ " rows");
					}
				}
				return;
			}
			dtde.acceptDrop(dtde.getDropAction());
			Transferable trans = dtde.getTransferable();

			// check for TransferableGenericObjects or MasterDataVO support
			boolean reference = false;

			List<?> lstloim = null;
			if (dtde.isDataFlavorSupported(TransferableGenericObjects.dataFlavor)) {
				reference = true;
				lstloim = (List<?>) trans
						.getTransferData(TransferableGenericObjects.dataFlavor);
			} else if (dtde
					.isDataFlavorSupported(TransferableMasterDatas.dataFlavor)) {
				reference = true;
				lstloim = (List<?>) trans
						.getTransferData(TransferableMasterDatas.dataFlavor);
			} else if (dtde.isDataFlavorSupported(MasterDataIdAndEntity.dataFlavor)) {
				reference = true;
				Object o = trans.getTransferData(MasterDataIdAndEntity.dataFlavor);
				if (o instanceof Collection) {
					lstloim = new ArrayList((Collection)o);
				} else {
					lstloim = Collections.singletonList(o);
				}
			}

			if (reference) {
				int countNotImported = 0;
				int countImported = 0;
				boolean noReferenceFound = false;
				UID entityId = null;
				String entityLabel = null;

				for (Object o : lstloim) {
					Collectable<?> clct = null;
					if (o instanceof GenericObjectIdModuleProcess) {
						GenericObjectIdModuleProcess goimp = (GenericObjectIdModuleProcess) o;
						entityId = goimp.getModuleUid();

						try {
							clct = new CollectableGenericObjectWithDependants(
									GenericObjectDelegate
											.getInstance()
											.getWithDependants(goimp.getModuleUid(),
													goimp.getGenericObjectId(),
													ClientParameterProvider
															.getInstance()
															.getValue(
																	ParameterProvider.KEY_LAYOUT_CUSTOM_KEY)));
						} catch (Exception e) {
							LOG.error("visitDrop failed: " + e, e);
						}
					} else if (o instanceof MasterDataIdAndEntity) {
						MasterDataIdAndEntity<PK> mdiae = (MasterDataIdAndEntity<PK>) o;
						entityId = mdiae.getEntityUid();
						try {
							clct = new CollectableMasterData<PK>(
									new CollectableMasterDataEntity(
											MasterDataDelegate
													.getInstance()
													.getMetaData(
															mdiae.getEntityUid())),
									MasterDataDelegate.getInstance()
											.get(mdiae.getEntityUid(),
													mdiae.getId()));
						} catch (CommonBusinessException e) {
							LOG.error("visitDrop failed: " + e, e);
						}
					}

					entityLabel = SpringLocaleDelegate.getInstance()
							.getLabelFromMetaDataVO(
									MetaProvider.getInstance().getEntity(
											entityId));

					if (clct != null) {
						try {
							if (!insertNewRowWithReference(entityId, clct, true)) {
								countNotImported++;
							} else {
								countImported++;
							}
						} catch (NuclosBusinessException e) {
							noReferenceFound = true;
							LOG.debug("visitDrop: No reference found: " + e);
						}
					}
				}

				if (noReferenceFound) {
					String bubbleInfo = getSpringLocaleDelegate()
							.getMessage(
									"MasterDataSubFormController.4",
									"Dieses Unterformular enthält keine Referenzspalte zur Entität ${entity}.",
									entityLabel);
					new Bubble(getSubForm().getJTable(), bubbleInfo, 10,
							BubbleUtils.Position.CENTER).setVisible(true);
				} else {
					String sNotImported = getSpringLocaleDelegate()
							.getMessage(
									"MasterDataSubFormController.5",
									"Der Valuelist Provider verhindert das Anlegen von ${count} Unterformular Datensätzen.",
									countNotImported);

					getCollectController()
							.getDetailsPanel()
							.setStatusBarText(
									getSpringLocaleDelegate()
											.getMessage(
													"MasterDataSubFormController.6",
													"${count} Unterformular Datensätze angelegt.",
													countImported)
											+ (countNotImported == 0 ? "" : " "
													+ sNotImported));
					if (countImported > 0) {
						cmdAutoNumberPush(ToolbarFunction.NEW);
					}
					if (countNotImported != 0) {
						new Bubble(
								getCollectController().getDetailsPanel().tfStatusBar,
								sNotImported, 10, BubbleUtils.Position.NW)
								.setVisible(true);
					}
				}
				return;
			}

			final Point here = dtde.getLocation();
			final int hereRow = DetailsSubFormController.this.getSubForm()
					.getJTable().rowAtPoint(here);

			final DataFlavor flavors[] = trans.getTransferDataFlavors();
			for (int i = 0; i < flavors.length; i++) {
				try {
					if (flavors[i].isFlavorJavaFileListType()) {
						final List<File> files = (List<File>) trans
								.getTransferData(flavors[i]);
						if (files.size() == 1 && hereRow >= 0 && !blnViewPort) {
							updateRowFromDrop(hereRow, files);
						} else if (hereRow == -1 && !blnViewPort) {
							for (Iterator<File> it = files.iterator(); it
									.hasNext();) {
								File file = it.next();
								insertNewRowFromDrop(file);
							}
						}
						dtde.dropComplete(true);
						return;
					}
				} catch (Exception e) {
					// ignore.
				}
			}

			final List<File> lstFile = DragAndDropUtils.mailHandlingWithJacob();
			if (lstFile.size() > 0) {
				if (lstFile.size() == 1 && hereRow > 0 && !blnViewPort) {
					updateRowFromDrop(hereRow, lstFile);
				} else {
					for (File file : lstFile) {
						insertNewRowFromDrop(file);
					}
				}
				dtde.rejectDrop();
				return;
			}
			dtde.dropComplete(false);
		} catch (PointerException e) {
			LOG.warn("visitDrop fails with PointerException: " + e);
			Bubble bubble = new Bubble(
					DetailsSubFormController.this.getJTable(),
					getSpringLocaleDelegate()
							.getMessage("details.subform.controller.2",
									"Diese Funktion wird nur unter Microsoft Windows 32bit unterstützt!"),
					5, BubbleUtils.Position.NE);
			bubble.setVisible(true);
		} catch (Exception e) {
			LOG.warn("visitDrop fails: " + e, e);
		}
	}

	@Override
	public void visitDropActionChanged(DropTargetDragEvent dtde) {
	}

	public abstract boolean insertNewRowWithReference(UID entityUid,
			Collectable<?> collectable, boolean b)
			throws NuclosBusinessException;
	
	public abstract boolean insertNewRowWithAdditionalCollectable(CollectableEntityObject<PK> oldClct, Collectable<?> additionalCollectable, UID referencingEntityField)
			throws NuclosBusinessException;

	public void cmdImportDocuments() {
		UIUtils.runCommandForTabbedPane(this.getMainFrameTabbedPane(),
				new Runnable() {
					@Override
					public void run() {
						final String sLastDir = (getPrefs() == null) ? null
								: getPrefs()
										.node(CollectableDocumentFileChooserBase.PREFS_NODE_COLLECTABLEFILECHOOSER)
										.get(CollectableDocumentFileChooserBase.PREFS_KEY_LAST_DIRECTORY,
												null);
						final JFileChooser filechooser = new JFileChooser(
								sLastDir);
						filechooser.setMultiSelectionEnabled(true);
						filechooser
								.setFileSelectionMode(JFileChooser.FILES_ONLY);
						// Customer's wish (B1149) / UA
						filechooser.setFileHidingEnabled(false);

						final int iBtn = filechooser
								.showOpenDialog(getParent());
						if (iBtn == JFileChooser.APPROVE_OPTION) {
							final File[] files = filechooser.getSelectedFiles();
							if (files != null) {
								for (File file : files) {
									try {
										insertNewRowFromDrop(file);
									} catch (Exception e) {
										Errors.getInstance()
												.showExceptionDialog(
														getParent(), e);
									}
								}
							}
							if (getPrefs() != null) {
								getPrefs()
										.node(CollectableDocumentFileChooserBase.PREFS_NODE_COLLECTABLEFILECHOOSER)
										.put(CollectableDocumentFileChooserBase.PREFS_KEY_LAST_DIRECTORY,
												filechooser
														.getCurrentDirectory()
														.getAbsolutePath());
							}
						}
					}
				});
	}

	/**
	 * handle autonumber push up/down
	 * 
	 * @param fToolBar
	 *            toolbar function
	 */
	private final void cmdAutoNumberPush(final ToolbarFunction fToolBar) {
		final SubFormTable sfTable = (SubFormTable) getJTable();

		int iSelectedRow = sfTable.getSelectedRow();
		int iRowFrom = -1;
		int iRowTo = -1;

		// prevent losing unsaved data
		stopEditing();
		boolean fixSorting = true;
		switch (fToolBar) {
		case AUTONUMBER_PUSH_UP:
		case AUTONUMBER_PUSH_DOWN:
			if (ToolbarFunction.AUTONUMBER_PUSH_UP.equals(fToolBar)) {
				iRowFrom = iSelectedRow;
				iRowTo = iSelectedRow - 1;
			} else {
				iRowFrom = iSelectedRow;
				iRowTo = iSelectedRow + 1;
			}
			int[] arrRows = sfTable.getSelectedRows();
			int iRowTgt = iRowTo;
			int iRowSrc = iRowFrom;

			if (!AutonumberUiUtils.moveRows(iRowSrc, iRowTgt, arrRows, sfTable)) {
				return;
			}
			break;
		case REMOVE:
		case NEW:
		case NEW_AT_POSITION:
			break;
		default:
			fixSorting = false;
		}
		if (fixSorting) {
			// refresh sorting
			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					AutonumberUiUtils.fixSubFormOrdering(sfTable);
				}
			});
		}
	}

	private final boolean cmdCutSelectedRows() {
		boolean result = false;

		if (isRowMovingAllowed()) {
			result = true;
			// copy
			if (!cmdCopySelectedRows()) {
				result = false;
			} else {
				// cut (remove rows from source)
				removeSelectedRows();
				// refresh sorting
				final SubFormTable sfTable = (SubFormTable) getJTable();
				UIUtils.invokeOnDispatchThread(new Runnable() {
					@Override
					public void run() {
						AutonumberUiUtils.fixSubFormOrdering(sfTable);
					}
				});
			}
		}
		return result;

	}

	protected boolean cmdCopySelectedRows() {
		final Clipboard sysClip = Toolkit.getDefaultToolkit()
				.getSystemClipboard();

		final TableRowCopyTransferHandler.RowTransferable transferable = new TableRowCopyTransferHandler.RowTransferable(
				getSubForm().getEntityUID(), (SubFormTable) getJTable());

		final SubFormTableModel model = getSubFormTableModel();
		for (int iRow : getJTable().getSelectedRows()) {
			assert iRow > -1;
			// catch the whole row
			transferable.newRow(new Integer(iRow), Utils.copyRow(getJTable().convertRowIndexToModel(iRow), model));
		}

		sysClip.setContents(transferable, null);
		// refresh sorting
		final SubFormTable sfTable = (SubFormTable) getJTable();
		UIUtils.invokeOnDispatchThread(new Runnable() {
			@Override
			public void run() {
				AutonumberUiUtils.fixSubFormOrdering(sfTable);
			}
		});
		return true;
	}

	private final boolean cmdPasteRows() {
		boolean result = false;

		if (!isRowMovingAllowed()) {
			result = false;
		} else {

			final Clipboard sysClip = Toolkit.getDefaultToolkit()
					.getSystemClipboard();
			try {
				final Transferable transfer = sysClip.getContents(null);
				if (!transfer
						.isDataFlavorSupported(TableRowCopyTransferHandler.RowTransferable
								.getDataFlavor())) {
					LOG.error("paste row failed, DataFlavor "
							+ TableRowCopyTransferHandler.RowTransferable
									.getDataFlavor().getHumanPresentableName()
							+ " is not supported.");
					result = false;
				} else {
					final TransferableWrapper<SubFormTable, RowTransferable> wrapper = (TransferableWrapper<SubFormTable, RowTransferable>) transfer
							.getTransferData(TableRowCopyTransferHandler.RowTransferable
									.getDataFlavor());

					final SubFormTable sfTable = (SubFormTable) getJTable();
					int idx = sfTable.getSelectedRow();
					if (idx < 0) {
						// if nothing is selected- insert at last position.
						idx = sfTable.getRowCount();
					}
					result = processCopyDraggedRows(idx, wrapper);
				}
			} catch (UnsupportedFlavorException ex) {
				LOG.error("paste row failed, DataFlavor "
						+ TableRowCopyTransferHandler.RowTransferable
								.getDataFlavor().getHumanPresentableName()
						+ " is not supported.", ex);
			} catch (IOException ex) {
				LOG.error("paste row failed", ex);
			} catch (CommonBusinessException ex) {
				LOG.error("paste row failed", ex);
			}
		}
		return result;
	}

	/**
	 * Handels autonumber actions for sub form
	 * 
	 * NUCLOS-1477
	 */
	private final void initAutoNumber() {
		// Action Listener: Toolbar
		ListenerUtil.registerSubFormToolListener(getSubForm(), this,
				new SubFormToolListener() {
					@Override
					public void toolbarAction(String actionCommand) {
						final ToolbarFunction cmd = ToolbarFunction
								.fromCommandString(actionCommand);
						if (null != cmd) {
							switch (cmd) {
							case AUTONUMBER_PUSH_UP:
							case AUTONUMBER_PUSH_DOWN:
							case NEW:
							case NEW_AT_POSITION:
							case CLONE:
							case PASTE_ROW:
							case REMOVE:
							case CUT_ROW:
								cmdAutoNumberPush(cmd);
								break;
							}
						}
					}
				});
		
		// Action Listener: List Selection
		getSubForm().getJTable().getSelectionModel()
				.addListSelectionListener(new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						// toggle buttons
						toggleAutoNumberButtons(true);
					}
				});

		toggleAutoNumberButtons(true);

		final SubFormTable sfTable = (SubFormTable) getJTable();
		sfTable.getColumnModel().addColumnModelListener(
			new TableColumnModelListener() {
				@Override
				public void columnSelectionChanged(ListSelectionEvent arg0) {
				}

				@Override
				public void columnRemoved(TableColumnModelEvent arg0) {
					toggleAutoNumberButtons(false);
				}

				@Override
				public void columnMoved(TableColumnModelEvent arg0) {
				}

				@Override
				public void columnMarginChanged(ChangeEvent arg0) {
				}

				@Override
				public void columnAdded(TableColumnModelEvent arg0) {
					toggleAutoNumberButtons(false);
				}
			});
	}

	private final void initSubformDetailsView() {
		// Action Listener: Toolbar
		ListenerUtil.registerSubFormToolListener(getSubForm(), this,
				new SubFormToolListener() {
					@Override
					public void toolbarAction(String actionCommand) {
						final ToolbarFunction cmd = ToolbarFunction
								.fromCommandString(actionCommand);
						if (null != cmd) {
							switch (cmd) {
							case SUBFORM_DETAILS_VIEW:
								cmdOpenSubformDetailsView();
								break;
							case SUBFORM_DETAILS_ZOOM:
								cmdEnterMultiViewMode();
							}
						}
					}
				});

		boolean hasScripts = 
				getSubForm().getDeleteEnabledScript() != null || 
				getSubForm().getNewEnabledScript() != null ||
				getSubForm().getCloneEnabledScript() != null ||
				getSubForm().getEditEnabledScript() != null;
		
		if (!hasScripts && !getSubForm().isIgnoreSubLayout() && SecurityCache.getInstance().isReadAllowedForEntity(this.getCollectableEntity().getUID())
					&& MasterDataLayoutHelper.isLayoutMLAvailable(this.getCollectableEntity().getUID(), false)) {
			if (getSubForm().isEnabled()) {
				getSubForm().setToolbarFunctionState(
						ToolbarFunction.SUBFORM_DETAILS_VIEW,
						ToolbarFunctionState.ACTIVE);
			} else {
				getSubForm().setToolbarFunctionState(
						ToolbarFunction.SUBFORM_DETAILS_VIEW,
						ToolbarFunctionState.DISABLED);
			}
		} else {
			getSubForm().setToolbarFunctionState(
					ToolbarFunction.SUBFORM_DETAILS_VIEW,
					ToolbarFunctionState.HIDDEN);
		}
	}

	private final void initSubformChangeState() {
		if (!isSearchable() && 
				MasterDataLayoutHelper.isLayoutMLAvailable(this.getCollectableEntity().getUID(), false)
				&& MetaProvider.getInstance().getEntity(this.getCollectableEntity().getUID()).isStateModel()) {
			//boolean bEnabled = !getSubForm().isReadOnly() && getSubForm().isEnabled();
			//if (bEnabled) {
				getSubForm().setToolbarFunctionState(
						ToolbarFunction.SUBFORM_CHANGE_STATE,
						ToolbarFunctionState.ACTIVE);
			//} else {
			//	getSubForm().setToolbarFunctionState(
			//		SubForm.ToolbarFunction.SUBFORM_CHANGE_STATE,
			//			ToolbarFunctionState.DISABLED);
			//}
		} else {
			getSubForm().setToolbarFunctionState(
					ToolbarFunction.SUBFORM_CHANGE_STATE,
					ToolbarFunctionState.HIDDEN);
		}
	}

	public boolean isRowMovingAllowed() {
		boolean result = false;
		boolean bHasAutoNumber = (AutonumberUiUtils.findAutoNumberField(getSubForm().getEntityUID())!= null);
		if (!bHasAutoNumber) {
			result = true;
		} else {
			result = AutonumberUiUtils.isSortedByAutonumber(DetailsSubFormController.this) 
					&& getSubForm().getSubFormFilter().isCollapsed();
		}
		return result;
	}

	private final void toggleAutoNumberButtons(final boolean revalidateNewState) {
		final EnumSet<ToolbarFunction> tfDependingOnActiveAutonumberSorting = EnumSet
				.of(ToolbarFunction.NEW, ToolbarFunction.NEW_AT_POSITION,
						ToolbarFunction.CLONE, ToolbarFunction.REMOVE,
						ToolbarFunction.PASTE_ROW, ToolbarFunction.CUT_ROW);

		boolean bHasAutoNumber = (null != AutonumberUiUtils
				.findAutoNumberField(getSubForm().getEntityUID())) && getSubForm().isAutonumberSorting();
		if (!bHasAutoNumber) {
			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					getSubForm().setToolbarFunctionState(
							ToolbarFunction.AUTONUMBER_PUSH_DOWN,
							ToolbarFunctionState.HIDDEN);
					getSubForm().setToolbarFunctionState(
							ToolbarFunction.AUTONUMBER_PUSH_UP,
							ToolbarFunctionState.HIDDEN);
				}

			});
			return;
		}

		ToolbarFunctionState toolbarStateUp = ToolbarFunctionState.DISABLED;
		ToolbarFunctionState toolbarStateDown = ToolbarFunctionState.DISABLED;

		final boolean bEnabled = DetailsSubFormController.this.isEnabled() && getSubForm().getSubformTable().getSelectedRowCount() > 0;
		if (bEnabled) {
			// check sorting by autonumber column
			if (!isRowMovingAllowed()) {
				for (final ToolbarFunction tf : tfDependingOnActiveAutonumberSorting) {
					getSubForm().setToolbarFunctionState(tf, ToolbarFunctionState.DISABLED);;
				}
			} else {
				for (ToolbarFunction tf : tfDependingOnActiveAutonumberSorting) {
					if(
						getSubForm().getCloneEnabledScript() != null && tf == ToolbarFunction.CLONE
						|| getSubForm().getNewEnabledScript() != null && tf == ToolbarFunction.NEW
						|| getSubForm().getDeleteEnabledScript() != null && tf == ToolbarFunction.REMOVE
						|| getSubForm().getEditEnabledScript() != null && tf == ToolbarFunction.MULTIEDIT
						) {
						// TODO call groovy scripts to get to know if this toolbar button should be shown
						// at the moment it is not possible to mix groovy scripts for toolbar buttons and autonumber  
					} else {
						getSubForm().setToolbarFunctionState(tf, ToolbarFunctionState.ACTIVE);
					}
				}

				final SubFormTable sfTable = (SubFormTable) getSubForm().getJTable();
				int iRowCount = sfTable.getRowCount();

				final int[] arrRows = getSubForm().getJTable()
						.getSelectedRows();
				if (arrRows.length > 0 && arrRows.length < iRowCount) {
					// check lower bound
					if (arrRows[0] > 0) {
						toolbarStateUp = ToolbarFunctionState.ACTIVE;
					}

					// check upper bound
					if (arrRows[arrRows.length - 1] < (iRowCount - 1)) {
						toolbarStateDown = ToolbarFunctionState.ACTIVE;
					}
				}
			}
		}
		final ToolbarFunctionState fToolbarStateUp = toolbarStateUp;
		final ToolbarFunctionState fToolbarStateDown = toolbarStateDown;		
		
		// set toolbar state
		UIUtils.invokeOnDispatchThread(new Runnable() {
			@Override
			public void run() {
				if (revalidateNewState) {
					LOG.debug("revalidateNewState...");
					DetailsSubFormController parentSubformController = null;
					if (getCollectController() != null &&
						getCollectController().getDetailsController() != null &&
						getSubForm() != null &&
						getSubForm().getParentSubForm() != null) {
						parentSubformController = getCollectController()
								.getDetailsController().getSubFormController(getSubForm().getParentSubForm());
					}
					getSubForm().setNewEnabled(
							new SubformControllerScriptContext(
									getCollectController(),
									parentSubformController,
									DetailsSubFormController.this,
									null
							)
					);
				}
				getSubForm().setToolbarFunctionState(
						ToolbarFunction.AUTONUMBER_PUSH_DOWN,
						fToolbarStateDown);
				getSubForm().setToolbarFunctionState(
						ToolbarFunction.AUTONUMBER_PUSH_UP,
						fToolbarStateUp);
			}
		});

	}

	public FixedColumnRowHeader getFixedColumnHeader() {
		return fixedcolumnheader;
	}

	private final boolean processMoveDraggedRows(int idxTgt,
			final TransferableWrapper<SubFormTable, RowTransferable> wrapper)
			throws CommonBusinessException {
		try {

			final SubFormTable sfTable = (SubFormTable) getJTable();

			final int[] arrRows = CollectionUtils.asPrimitiveIntArray(wrapper
					.getTransferable().getRows().keySet());

			int iRowTgt = idxTgt;
			int iRowSrc = dragAndDropContext.getRowDragged();

			if (!AutonumberUiUtils.moveRows(iRowSrc, iRowTgt, arrRows, sfTable)) {
				LOG.error("Row move rejected");
				return false;
			}

			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					AutonumberUiUtils.fixSubFormOrdering(sfTable);
				}
			});
		} finally {
			this.dragAndDropContext = new DragAndDropContext();
		}
		return true;
	}

	private final boolean processCopyDraggedRows(int idxInsert,
			final TransferableWrapper<SubFormTable, RowTransferable> wrapper)
			throws CommonBusinessException {
		final Map<Integer, MasterDataVORow<CollectableField>> mpData = wrapper
				.getTransferable().getRows();
		final SubFormTable srcTable = wrapper.getCustomObject();

		// field(src) -> field(tgt)
		final Map<Integer, Integer> mpFieldMatch = EntityFieldUtils
				.createFieldMatching(EntityFieldUtils
						.getFieldsFromModel(getSubFormTableModel()),
						EntityFieldUtils.getFieldsFromModel(srcTable
								.getSubFormModel()));

		int idxSelectStart = idxInsert;

		for (Iterator iterator = mpData.entrySet().iterator(); iterator.hasNext();) {
			final Entry<Integer, MasterDataVORow<CollectableField>> rowEntry
				= (Entry<Integer, MasterDataVORow<CollectableField>>) iterator.next();

			final CollectableEntityObject<PK> eoRow = (CollectableEntityObject<PK>) this
					.insertNewRow();
			// insertNewRow(idx) does not work for childsubforms.
			// so we had to remove and add new collectable at the right index.
			this.getCollectableTableModel().remove(eoRow);
			this.getCollectableTableModel().addCollectable(idxInsert,
					(Clct) eoRow);

			int idxInserted = idxInsert;
			final List<Integer> lstFieldsVLP = new ArrayList<Integer>();
			// new row
			for (final Entry<Integer, Integer> matchEntry : mpFieldMatch
					.entrySet()) {
				final UID tgtColumnUid = getSubFormTableModel()
						.getColumnFieldUid(matchEntry.getKey());
				final CollectableEntityField clctef = eoRow
						.getCollectableEntity().getEntityField(tgtColumnUid);
				final CollectableComponentTableCellEditor cellEditor = ((CollectableComponentTableCellEditor) getTableCellEditor(
						getJTable(), idxInsert, clctef));

				if (cellEditor == null) {
					// could happen, column is foreign column.
					continue;
				}

				final CollectableComponent clctcomp = cellEditor
						.getCollectableComponent();

				if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
					lstFieldsVLP.add(matchEntry.getKey());
					continue;
				} else {
					final Collection<ClearAction> clearActions = getSubForm()
							.getClearActions(tgtColumnUid);
					eoRow.setField(tgtColumnUid, rowEntry.getValue().getRow()
							.getFields().get(matchEntry.getValue()));
					SubFormTools.clearValues(getSubFormTableModel(),
							idxInserted, clearActions);
				}
			}

			// process layout rules(lookups) for reference fields (combobox/lov)
			for (final Integer fieldIdx : lstFieldsVLP) {

				final UID tgtColumnUid = getSubFormTableModel()
						.getColumnFieldUid(fieldIdx);
				final CollectableEntityField clctef = eoRow
						.getCollectableEntity().getEntityField(tgtColumnUid);
				final PK valueId = (PK) rowEntry.getValue().getRow()
						.getFields().get(mpFieldMatch.get(fieldIdx))
						.getValueId();
				final CollectableListOfValues<PK> clctlov = new CollectableListOfValues<PK>(
						eoRow.getCollectableEntity().getEntityField(
								tgtColumnUid));

				final Collectable<PK> c = Utils.getReferencedCollectable(
						clctef.getEntityUID(), clctef.getUID(), valueId);

				if (null == c) {
					continue;
				}

				final Collection<TransferLookedUpValueAction> valueActions = getSubForm()
						.getTransferLookedUpValueActions(tgtColumnUid);
				final Collection<ClearAction> clearActions = getSubForm()
						.getClearActions(tgtColumnUid);

				clctlov.addLookupListener(new SubFormTools.LookupClearListener(
						getSubFormTableModel(), idxInserted, clearActions));
				clctlov.addLookupListener(new SubFormTools.LookupValuesListener(
						(SubFormTable) getJTable(), isSearchable(),
						idxInserted, valueActions));

				getSubForm().getJTable().getSelectionModel()
						.setSelectionInterval(idxInserted, idxInserted);
				clctlov.acceptLookedUpCollectable(c);
				// eoRow.setField(clctlov.getFieldName(), clctlov.getField());
			}

			// only if we have more than one copied element. Otherwise selection will be expanded to more than the copied elements.
			if (iterator.hasNext()) {
				++idxInsert;
			}
		}

		// FIXME handle invalid idx
		// select new created rows
		final SubFormTable sfTable = (SubFormTable) getSubForm().getJTable();
		if (idxInsert > -1 && idxInsert < sfTable.getRowCount()
				&& idxSelectStart > -1
				&& idxSelectStart < sfTable.getRowCount()) {

			sfTable.getSelectionModel().setSelectionInterval(idxSelectStart,
					idxInsert);

			// fix autonumbers
			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					AutonumberUiUtils
							.fixSubFormOrdering((SubFormTable) getJTable());
				}
			});
		} else {
			LOG.error("illegal selection index");
		}
		return true;
	}

	public class SubFormProfileSupport implements ProfileSupport {

		@Override
		public void updateBySelectedProfile() {
			final boolean isIgnorePreferencesUpdateBefore = isIgnorePreferencesUpdate;
			isIgnorePreferencesUpdate = true;
			try {
				final TablePreferencesManager tblprefManager = DetailsSubFormController.this.getTablePreferencesManager();
				final TablePreferences tp = tblprefManager.getSelected();

				final List<Integer> widths = tblprefManager.getColumnWidths();
				final List<CollectableEntityField> allFields = fixedcolumnheader.getAllAvailableFields();
				final List<CollectableEntityField> selected = tblprefManager.getSelectedFields(allFields);
				final Set<CollectableEntityField> fixed = tblprefManager.getFixedFields(selected);

				makeSureSelectedFieldsAreNonEmpty(getCollectableEntity(), selected);
				LOG.info("updateBySelectedProfile: fixed=" + fixed + " other=" + selected);

				fixedcolumnheader.changeSelectedColumns(selected, fixed, widths,
						null, new AbstractAction() {
							@Override
							public void actionPerformed(ActionEvent e) {
								DetailsSubFormController.this.getSubForm().resetDefaultColumnWidths();
							}
						}, new AbstractAction() {
							@Override
							public void actionPerformed(ActionEvent e) {
								setColumnOrder();
							}
						});
				if (tp.isDynamicRowHeight()) {
					// DetailsSubFormController.this.getSubForm().setRowHeight(DetailsSubFormController.this.getPrefs().getInt(TableRowIndicator.SUBFORM_ROW_HEIGHT,
					// SubForm.DYNAMIC_ROW_HEIGHTS));
					DetailsSubFormController.this.getSubForm().setRowHeight(
							SubForm.DYNAMIC_ROW_HEIGHTS);
				} else {
					// DetailsSubFormController.this.getSubForm().setRowHeight(DetailsSubFormController.this.getPrefs().getInt(TableRowIndicator.SUBFORM_ROW_HEIGHT,
					// DetailsSubFormController.this.getSubForm().getMinRowHeight()));
					DetailsSubFormController.this.getSubForm().setRowHeight(
							DetailsSubFormController.this.getSubForm()
									.getMinRowHeight());
				}
				DetailsSubFormController.this.rowIndicator.updateRowHeights();
			} finally {
				isIgnorePreferencesUpdate = isIgnorePreferencesUpdateBefore;
			}
		}

	}

	public void cmdEnterMultiViewMode() {
		enterMultiViewMode();
	}

	/**
	 * filter new collectables
	 * 
	 * @param colCollectable
	 *            list of collectables
	 * @return
	 */
	private List<Clct> filterNew(final Collection<Clct> colCollectable) {
		final List<Clct> lstNew = new ArrayList<Clct>();
		for (final Clct clt : colCollectable) {
			if (null == clt.getId()) {
				lstNew.add(clt);
			}
		}
		return lstNew;
	}

	/**
	 * isMultiEditAllowed
	 * 
	 * @return true if multi edit is allowed
	 */
	public boolean isMultiEditAllowed() {
		boolean result = true;

		// amount of selected collectables must not be empty
		if (getSelectedCollectables().isEmpty()) {
			result = false;
		}

		// selected collectables must not contain any unsaved entry (without id)
		if (!filterNew(getSelectedCollectables()).isEmpty()) {
			result = false;
		}
		return result;
	}

	private void enterMultiViewMode() {
		if (getCollectController().changesArePending()) {
			JOptionPane.showMessageDialog(getTab(), SpringLocaleDelegate.getInstance().getMessage("DetailsSubFormController.11", null));
			return;
		}
		try {
			// check if multi edit is allowed
			if (!isMultiEditAllowed()) {
				throw new IllegalStateException("multi edit is not allowed");
			}
			// wenn nur ein Eintrag -> nicht in Sammelbearbeitung öffnen
			if (getSelectedCollectables().size() == 1) {
				Main.getInstance().getMainController().showDetails(getEntityAndForeignKeyField().getEntity(), 
						getSelectedCollectables().get(0).getId());
				return;
			}
			
			final NuclosCollectController ctl = NuclosCollectControllerFactory
					.getInstance().newCollectController(
							getEntityAndForeignKeyField().getEntity(), null,
							null);
			
			final List<Object> ids = CollectionUtils.transform(
					getSelectedCollectables(),
					new Transformer<Collectable, Object>() {
						@Override
						public Object transform(Collectable clct) {
							return clct.getId();
						}
					});

			ctl.runViewMultipleCollectablesWithIds(ids);

			// refresh parent collect controller on safe
			ctl.addCollectableEventListener(new CollectableEventListener() {

				@Override
				public void handleCollectableEvent(Collectable collectable,
						final MessageType messageType) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							try {
								if (MessageType.MULTIEDIT_DONE == messageType) {
									getCollectController()
											.refreshCurrentCollectable();
								}
							} catch (CommonBusinessException e) {
								throw new NuclosFatalException(e);
							}
						}
					});
				}
			});
		} catch (CommonBusinessException e) {
			Errors.getInstance().showExceptionDialog(
					Main.getInstance().getMainFrame(), e);
		}
	}

	public void setSubformPermission(SubformPermission permission) {
		if (getSubForm().setSubformPermission(permission)) {
			// NUCLOS-6562 Reinit Renderes and Editor when permissions have been changed
			setupTableCellRenderers(getSubFormTableModel());
			setupStaticTableCellEditors(getJTable());
		}
	}

	@Override
	public void enabledChanged(final boolean enabled) {
		getSubForm().setNewEnabled(new CollectControllerScriptContext<>(getCollectController(), new ArrayList<>(getCollectController().getDetailsSubforms().values())));

		getSubForm().setToolbarFunctionState(ToolbarFunction.COPY_ROW,
				enabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
		getSubForm().setToolbarFunctionState(ToolbarFunction.MULTIEDIT,
				getSubForm().getUniqueMasterColumnUid() != null
						? (enabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED)
						: ToolbarFunctionState.HIDDEN);
		getSubForm().setToolbarFunctionState(ToolbarFunction.TRANSFER,
				(getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.TRANSFER) == null
						|| getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.TRANSFER) != ToolbarFunctionState.HIDDEN)
						? (enabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED)
						: ToolbarFunctionState.HIDDEN);
		if (getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) == ToolbarFunctionState.ACTIVE) {
			if ((!enabled || getSubForm().isIgnoreSubLayout()) && !(getSubForm().isReadOnly() && getSubForm().isDetailViewReadOnlyAvailable())) {
				getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.DISABLED);
			}
		} else if (getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) == ToolbarFunctionState.DISABLED){
			if ((enabled && !getSubForm().isIgnoreSubLayout()) || (getSubForm().isReadOnly() && getSubForm().isDetailViewReadOnlyAvailable())) {
				getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.ACTIVE);
			}
		}
		getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE,
				(getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) == null
						|| getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != ToolbarFunctionState.HIDDEN)
						? (enabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED)
						: ToolbarFunctionState.HIDDEN);
	}
} // class DetailsSubFormController
