//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.RowSorter.SortKey;

import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.MainFrameTabController;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.matrix.JMatrixComponent;
import org.nuclos.client.ui.matrix.MatrixCollectable;
import org.nuclos.client.ui.matrix.MatrixTableModel;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.WorkspaceDescription2.MatrixPreferences;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.security.IPermission;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import com.thoughtworks.xstream.XStream;

public class MatrixController extends MainFrameTabController implements Closeable {
	
	protected JMatrixComponent matrix;
	protected UID matrixName;
	
	protected MatrixTableModel model;
	protected MatrixTableModel modelFixed;
	
	protected Object oParentId;
	protected Collectable parent;
	
	protected EntityPreferences entityPreferences;
	
	protected IPermission permissionYEntity;
	protected IPermission permissionMatrixEntity;
	
	protected final CollectController<Long,?> clct;

	public MatrixController(MainFrameTab parent, UID name, JMatrixComponent matrix, CollectController<Long,?> clct) {
		super(parent);
		this.matrixName = name;
		this.matrix = matrix;
		this.matrix.setController(this);
		this.clct = clct;
		prepareTableModel();
	}
	
	public void stopEditing() {
		matrix.stopEditing();
	}
	
	public <PK> CollectableMasterData<PK> getSelectedCollectable()  {
		return matrix.getSelectedCollectable();
	}
	
	public <PK> Collection<CollectableMasterData<PK>> getSelectedCollectables() {
		return matrix.getSelectedCollectables();
	}
	
	public JMatrixComponent getMatrixComponent() {
		return this.matrix;
	}
	
	public void setPermissions(IPermission permissionYEntity, IPermission permissionMatrixEntity) {
		this.permissionYEntity = permissionYEntity;
		this.permissionMatrixEntity = permissionMatrixEntity;
		matrix.setPermissionYEntity(permissionYEntity);
		matrix.setPermissionMatrixEntity(permissionMatrixEntity);
	}
	
	protected void prepareTableModel() {
		
		model = new MatrixTableModel(this);	
		model.setFixedModel(false);
		
		modelFixed = new MatrixTableModel(this);
		modelFixed.setFixedModel(true);
		
	}

	public void loadMatrixData() {
		clearRemovedObjects();

		Map<Collectable, Map<Collectable, List<Object>>> mpContent = new HashMap<Collectable, Map<Collectable, List<Object>>>();
		mpContent = ListOrderedMap.decorate(mpContent);
		
		Map<Collectable, Map<Collectable, List<Object>>> mpContentFixed = new HashMap<Collectable, Map<Collectable, List<Object>>>();
		mpContentFixed = ListOrderedMap.decorate(mpContentFixed);

		IDependentDataMap mpDep = _loadMatrixData();
		IDependentKey dependentKey = DependentDataMap.createDependentKey(matrix.getEntityYParentField());
		List<EntityObjectVO<?>> lstY = (List<EntityObjectVO<?>>)mpDep.getData(dependentKey);

		// y axis start
		setMatrixRowData(mpContentFixed, lstY);
		// y axis end
		
		dependentKey = DependentDataMap.createDependentKey(matrix.getMatrixEntityParentField());
		Collection<EntityObjectVO<?>> colMatrixData = mpDep.getData(dependentKey);
		
		// x axis start
		List<EntityObjectVO> lstX = loadXData();
		
		String sCategorieName = "";
		
		for(EntityObjectVO vo : lstX) {
			String sCat = (String) vo.getFieldValue(matrix.getFieldCategorie());
			if(sCat != null) {
				sCat = sCat.trim();
			}
			Collectable collectable = new MatrixCollectable(sCat);
			if (StringUtils.equals(sCategorieName, sCat)) {
				UID sFieldX = matrix.getFieldX();
				String sValue = " " + vo.getFieldValue(sFieldX);
				Collectable ctValue = new MatrixCollectable((Long) vo.getPrimaryKey(), sValue);
				mpContent.get(collectable).put(ctValue, initMatrix(colMatrixData, ctValue, lstY));
				continue;
			}
			sCategorieName = sCat;
			Map<Collectable, List<Object>> mp = new HashMap<>();
			mp = ListOrderedMap.decorate(mp);
			mpContent.put(collectable, mp);


			UID sFieldX = matrix.getFieldX();
			String sValue = " " + vo.getFieldValue(sFieldX);
			Collectable ctValue = new MatrixCollectable((Long) vo.getPrimaryKey(), sValue);
			mpContent.get(collectable).put(ctValue, initMatrix(colMatrixData, ctValue, lstY));
		}
		// x axis end
		
		model.setData(mpContent);
		modelFixed.setData(mpContentFixed);
		this.matrix.setTableModel(model, modelFixed);
	}

	protected void setMatrixRowData(Map<Collectable, Map<Collectable, List<Object>>> mpContentFixed, List<EntityObjectVO<?>> lstY) {

		UID sEntityYAxis = matrix.getEntityY();
		Map<UID, FieldMeta<?>> mpFieldsY = MetaProvider.getInstance().getAllEntityFieldsByEntity(sEntityYAxis);
		long minus = -1L;

		// row indicator
		Map<Collectable, List<Object>> mpRowIndicator = new HashMap<>();
		List<Object> mpDataFirst = new ArrayList<>();
		for (int i = 0; i < lstY.size(); i++) {
			MatrixCollectable mcRowIndicator = new MatrixCollectable("");
			mcRowIndicator.setField(UID.UID_NULL);
			mpDataFirst.add(mcRowIndicator);
		}

		MatrixCollectable mcRowIndicator = new MatrixCollectable("");
		mcRowIndicator.setField(UID.UID_NULL);
		mpRowIndicator.put(mcRowIndicator, mpDataFirst);
		MatrixCollectable mcMpRow = new MatrixCollectable(-100L, "");
		mcMpRow.setField(UID.UID_NULL);
		mpContentFixed.put(mcMpRow, mpRowIndicator);

		// This is a group key for y/rows
		Map<Long, MatrixCollectable.GroupKey> mpYGroupKeys= reGroupRows(matrix.getFieldY(), lstY);

		for (UID field : mpFieldsY.keySet()) {
			FieldMeta<?> fieldMeta = mpFieldsY.get(field);
			UID parent = matrix.getEntityYParentField();
			if (isSystemField(fieldMeta) || parent.equals(field)) {
				continue;
			}
			String sLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(fieldMeta);
			MatrixCollectable fixedGroup = new MatrixCollectable(-1L, sLabel);
			fixedGroup.setField(fieldMeta.getUID());
			fixedGroup.setEntityFieldMetaDataVO((FieldMeta<Object>) fieldMeta);

			Collectable group = new MatrixCollectable(minus--, "");

			Map<Collectable, List<Object>> mpCategorie = new HashMap<>();
			List<Object> mpData = new ArrayList<>();

			for (EntityObjectVO<?> vo : lstY) {
				MatrixCollectable matrixData = new MatrixCollectable((Long) vo.getPrimaryKey(), vo.getFieldValue(field), vo.getVersion());
				matrixData.setField(field);
				matrixData.setEntityFieldMetaDataVO((FieldMeta<Object>) fieldMeta);
				matrixData.setVO(vo);
				matrixData.setYgroupkey(mpYGroupKeys.get(vo.getPrimaryKey()));
				mpData.add(matrixData);
			}

			mpCategorie.put(group, mpData);
			mpContentFixed.put(fixedGroup, mpCategorie);
		}
	}

	public static Map<Long, MatrixCollectable.GroupKey> reGroupRows(UID ygroupfield, List<EntityObjectVO<?>> lstY) {
		if (ygroupfield == null) {
			return Collections.emptyMap();
		}
		Map<Object, List<EntityObjectVO<?>>> ygroups = new HashMap<>();
		Map<Long, MatrixCollectable.GroupKey> mpGroups = new HashMap<>();

		for (EntityObjectVO<?> vo : lstY) {
			Object val = vo.getFieldValue(ygroupfield);
			ygroups.putIfAbsent(val, new ArrayList<>());
			ygroups.get(val).add(vo);
		}

		List<EntityObjectVO<?>> copyY = new ArrayList<>(lstY);
		lstY.clear();
		int igroup = 0;
		// Reorder such that members of one group stay together
		for (EntityObjectVO<?> vo : copyY) {
			Object val = vo.getFieldValue(ygroupfield);
			List<EntityObjectVO<?>> lstInGroup = ygroups.get(val);
			if (lstInGroup.size() == 1) {
				// Just one element, not a group
				lstY.add(vo);
				continue;
			}

			int rankInGroup = lstInGroup.indexOf(vo);
			if (rankInGroup == 0) {
				lstY.addAll(lstInGroup);
				igroup++;

				for (EntityObjectVO<?> voInGroup : lstInGroup) {
					rankInGroup = lstInGroup.indexOf(voInGroup);
					MatrixCollectable.GroupKey groupKey = new MatrixCollectable.GroupKey(igroup, lstInGroup.size(), rankInGroup);
					mpGroups.put((Long)voInGroup.getPrimaryKey(), groupKey);

					if (rankInGroup > 0) {
						Map<UID, Object> fieldValues = voInGroup.getFieldValues();
						for (UID uid : fieldValues.keySet()) {
							if (RigidUtils.equal(fieldValues.get(uid), lstInGroup.get(0).getFieldValue(uid))) {
								groupKey.getIdenticalFields().add(uid);
							}
						}
					}
				}
			}

		}

		return mpGroups;
	}

	protected List<Object> initMatrix(Collection<EntityObjectVO<?>> colMatrixData, Collectable clctX, List<EntityObjectVO<?>> colY) {
		
		if(colMatrixData.isEmpty()) {
			return initEmptyMatrix(colY.size());
		}
		
		final Long xId = (Long) clctX.getId();
		
		final Collection<EntityObjectVO<?>> colMatrixXData = CollectionUtils.applyFilter(colMatrixData, new Predicate<EntityObjectVO<?>>() {
			
			@Override
			public boolean evaluate(EntityObjectVO<?> vo) {
				if(xId.equals(vo.getFieldId(matrix.getEntityXRefField())))
					return true;
				return false;
			}
			
		});
		
		List<Object> lstColumxData = new ArrayList<Object>();
		for(EntityObjectVO voY : colY) {
			final Long yId = (Long) voY.getPrimaryKey();
			
			EntityObjectVO voFind = CollectionUtils.findFirst(colMatrixXData, new Predicate<EntityObjectVO>() {

				@Override
				public boolean evaluate(EntityObjectVO vo) {
					if(yId.equals(vo.getFieldId(matrix.getMatrixEntityParentField())))
						return true;
					return false;
				}
				
			});
			
			if(voFind != null) {
				EntityMeta<?> em = MetaProvider.getInstance().getEntity(voFind.getDalEntity());
				FieldMeta<?> fm = em.getField(matrix.getEntityMatrixValueField());

				MatrixCollectable row11 = new MatrixCollectable((Long) voFind.getPrimaryKey(), voFind.getFieldValue(matrix.getEntityMatrixValueField()), voFind.getVersion(), fm.getDataType());
				lstColumxData.add(row11);
				row11.setYId(yId);
			}
			else {
				MatrixCollectable rowEmpty = new MatrixCollectable(null);
				rowEmpty.setYId(yId);
				lstColumxData.add(rowEmpty);
			}

		}
		if(lstColumxData.isEmpty()) {
			return initEmptyMatrix(colY.size());
		}
		if(lstColumxData.size() != colY.size()) {
			for(int i = lstColumxData.size(); i < colY.size(); i++) {
				Collectable rowEmpty = new MatrixCollectable(null);
				lstColumxData.add(rowEmpty);
			}
		}
		
		return lstColumxData;
	}
	
	private List<Object> initEmptyMatrix(int rowCount) {
		List<Object> lstEmpty = new ArrayList<Object>();
		for(int r = 0; r < rowCount; r++) {
			Collectable rowEmpty = new MatrixCollectable(null);
			lstEmpty.add(rowEmpty);
		}
		
		return lstEmpty;
	}
	

	protected IDependentDataMap _loadMatrixData() {
		if(oParentId == null) return new DependentDataMap();
		
		final UID sEntity = matrix.getEntityY();
		final UID sField = matrix.getEntityYParentField();
		Long iParentId = (Long) oParentId;
		
		List<EntityAndField> lstChildSubform = new ArrayList<EntityAndField>();
		EntityAndField adfn = new EntityAndField(matrix.getMatrixEntity(), matrix.getMatrixEntityParentField());
		lstChildSubform.add(adfn);

		UID layoutUID = ((EntityCollectController)clct).getCurrentLayoutUid();
	
		IDependentDataMap mp = MasterDataDelegate.getInstance().loadMatrixData(sField, iParentId, lstChildSubform, layoutUID);
		 
		IDependentKey dependentKey = DependentDataMap.createDependentKey(sField);
		Collection<EntityObjectVO<Long>> colVOY = mp.getDataPk(dependentKey);
				
		Collection<EntityObjectVO<Long>> lstSorted = CollectionUtils.sorted(colVOY, new Comparator<EntityObjectVO<Long>>() {

			@Override
			public int compare(EntityObjectVO<Long> o1, EntityObjectVO<Long> o2) {
				
				String field1 = (String) o1.getFieldValue(sField);
				String field2 = (String) o2.getFieldValue(sField);
				
				return ObjectUtils.compare(field1, field2);
			}
			
		});
		
		mp.setData(dependentKey, lstSorted);
		
		return mp;
	}
	
	protected List<EntityObjectVO> loadXData() {
		final UID sEntity = matrix.getEntityX();
		final UID sCategorieField = matrix.getFieldCategorie();
		final UID sFieldX = matrix.getFieldX();
		final UID layoutUID = ((EntityCollectController)clct).getCurrentLayoutUid();
		Collection<MasterDataVO<Long>> colX = MasterDataDelegate.getInstance().loadMatrixXAxis(sEntity, layoutUID);
		Collection<EntityObjectVO<Long>> colVOX = CollectionUtils.transform(colX, new Transformer<MasterDataVO<Long>, EntityObjectVO<Long>>() {

			@Override
			public EntityObjectVO<Long> transform(MasterDataVO<Long> i) {
				return i.getEntityObject();
			}
			
		});
		
		List<EntityObjectVO<Long>> lstSorted = CollectionUtils.sorted(colVOX, new Comparator<EntityObjectVO<Long>>() {

			@Override
			public int compare(EntityObjectVO<Long> o1, EntityObjectVO<Long> o2) {
				
				String field1 = (String) o1.getFieldValue(sCategorieField);
				String field2 = (String) o2.getFieldValue(sCategorieField);				
				return ObjectUtils.compare(field1, field2);
			}
			
		});
		
		MultiListMap<String, EntityObjectVO<Long>> mp = CollectionUtils.splitIntoMap(lstSorted, new Transformer<EntityObjectVO<Long>, String>() {

			@Override
			public String transform(EntityObjectVO<Long> i) {
				return (String)i.getFieldValue(sCategorieField);
			}
			
		});
		
		
		List<EntityObjectVO> newSorted = new ArrayList<EntityObjectVO>();
		
		Set<String> setKey = mp.keySet();
		
		List<String> lstKey = CollectionUtils.sorted(setKey, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return ObjectUtils.compare(o1, o2);
			}
			
		});
		
		for(String key : lstKey) {			
			List<EntityObjectVO<Long>> lst = mp.getValues(key);
			lst = CollectionUtils.sorted(lst, new Comparator<EntityObjectVO>() {

				@Override
				public int compare(EntityObjectVO o1, EntityObjectVO o2) {
					
					String field1 = (String) o1.getFieldValue(sFieldX);
					String field2 = (String) o2.getFieldValue(sFieldX);				
					return ObjectUtils.compare(field1, field2);
				}
				
			});			
			newSorted.addAll(lst);
		}		
		
		return newSorted;
		
	}
	
	public IDependentDataMap getAllData(Long iParentId) {
		IDependentDataMap mp = new DependentDataMap();
		List<EntityObjectVO<Long>> lstSubformData = new ArrayList<EntityObjectVO<Long>>();
		
		int rowCounter = modelFixed.getRowCount();
		for(int r = 0; r < rowCounter; r++) {
			EntityObjectVO voSubform = new EntityObjectVO(matrix.getEntityY());			
			for(Collectable clCategorie : modelFixed.getContent().keySet()) {
				MatrixCollectable mcCategorie = (MatrixCollectable)clCategorie;
				if(mcCategorie.getField().equals(UID.UID_NULL)) {
					continue;
				}
				for(Collectable colColumn : modelFixed.getContent().get(clCategorie).keySet()) {
					List<Object> lstColumn = modelFixed.getContent().get(clCategorie).get(colColumn);
					Object oValue = lstColumn.get(r);
					MatrixCollectable mctl = (MatrixCollectable)oValue;
					if(mctl.getVO() != null) {
						UID uField = mctl.getEntityFieldMetaDataVO().getUID();
						Map<UID, Long> mpTmp = mctl.getVO().getFieldIds();
						for(UID uid : mpTmp.keySet()) {
							if(uid.equals(uField)) {
								voSubform.setFieldId(uField, mpTmp.get(uField));
							}
							voSubform.setFieldId(uid, mpTmp.get(uid));
						}

						for(Object sField : mctl.getVO().getFieldValues().keySet()) {
							if(!voSubform.getFieldValues().containsKey(sField)) {
								Map<UID, Object> mpTmpValues = mctl.getVO().getFieldValues();
								for(UID uid : mpTmpValues.keySet()) {
									voSubform.setFieldValue(uid, mpTmpValues.get(uid));
								}								

							}
						}
					}
					voSubform.setFieldValue(mcCategorie.getField(), mctl.getValue(UID.UID_NULL));

					if(oParentId != null) {
						voSubform.setFieldId(matrix.getEntityYParentField(), (Long)oParentId);						
					}

					Long id = (Long) mctl.getId();
					
					voSubform.setPrimaryKey(id);
					voSubform.setVersion(mctl.getVersion());
					
					if (mctl.getVO() != null) {
						EntityMeta<?> em = MetaProvider.getInstance().getEntity(mctl.getVO().getDalEntity());
						if (em.isStateModel()) {
							UID state = mctl.getVO().getFieldUid(SF.STATE.getUID(em));
							voSubform.setFieldUid(SF.STATE.getUID(em), state);
						}
					}
					
					if (id == null) {
						voSubform.flagNew();						
					} else if (mctl.isModified()) {
						voSubform.flagUpdate();						
					} else if (mctl.isRemoved()) {
						voSubform.flagRemove();
					}
					break;
				}
			}
			for(Collectable clCategorie : model.getContent().keySet()) {
				for(Collectable colColumn : model.getContent().get(clCategorie).keySet()) {				
					List<Object> lstCell = model.getContent().get(clCategorie).get(colColumn);
					Object oValue = lstCell.get(r);
					MatrixCollectable mctl = (MatrixCollectable)oValue;
					
					Object value = mctl.getValue(UID.UID_NULL);
					Object id = mctl.getId();

					boolean emptyValue = value == null || value.equals(0);

					EntityObjectVO voToSave = new EntityObjectVO(matrix.getMatrixEntity());
					//value cell
					voToSave.setFieldValue(matrix.getEntityMatrixValueField(), mctl.getValue(UID.UID_NULL));
					// x axis
					voToSave.setFieldId(matrix.getEntityXRefField(), (Long) colColumn.getId());
					// y axis
					voToSave.setFieldId(matrix.getMatrixEntityParentField(), (Long)voSubform.getPrimaryKey());
					
					voToSave.setFieldValue(matrix.getMatrixEntityParentField(), voSubform.getPrimaryKey());
					
					voToSave.setPrimaryKey((Long) id);
					voToSave.setVersion(mctl.getVersion());
					if(id != null && mctl.isModified())
						voToSave.flagUpdate();
					else if(id == null) {
						voToSave.flagNew();
					}
					
					if (emptyValue || mctl.isRemoved()) {
						voToSave.flagRemove();
					}					
					IDependentKey dependentKey = DependentDataMap.createDependentKey(matrix.getMatrixEntityParentField());
					voSubform.getDependents().addData(dependentKey, voToSave);									
				}
			}
			
			lstSubformData.add(voSubform);
		}
		
		IDependentKey dependentKey = DependentDataMap.createDependentKey(matrix.getEntityYParentField());
		mp.addAllData(dependentKey, lstSubformData);		
		
		addRemovedObjects(mp);
		
		return mp;
	}
	
	private void addRemovedObjects(IDependentDataMap mp) {
		List<EntityObjectVO<Long>> lstSubformData = new ArrayList<EntityObjectVO<Long>>();
		Map<Collectable, List<Object>> mpRemovedFixed = modelFixed.getRemovedObjects();
		for(Collectable obj : mpRemovedFixed.keySet()) {			
			MatrixCollectable mctl = (MatrixCollectable)obj;
						
			EntityObjectVO voSubform = new EntityObjectVO(matrix.getEntityY());
			voSubform.setFieldValue(mctl.getEntityFieldMetaDataVO().getUID(), mctl.getValue(UID.UID_NULL));
			if(oParentId != null) {
				voSubform.setFieldId(matrix.getEntityYParentField(), (Long)oParentId);
			}
			voSubform.setPrimaryKey((Long) mctl.getId());
			voSubform.setVersion(mctl.getVersion());
			for(Object objField : mpRemovedFixed.get(obj)) {
				MatrixCollectable mc = (MatrixCollectable)objField;
				if(mc.getEntityFieldMetaDataVO() == null) {
					EntityObjectVO voToDel = new EntityObjectVO(matrix.getMatrixEntity());
					//value cell
					voToDel.setFieldValue(matrix.getEntityMatrixValueField(), mc.getValue(new UID()));
					// y axis
					voToDel.setFieldId(matrix.getMatrixEntityParentField(), (Long)voSubform.getPrimaryKey());
					voToDel.setFieldValue(matrix.getMatrixEntityParentField(), (Long)voSubform.getPrimaryKey());
					voToDel.setPrimaryKey((Long) mc.getId());
					voToDel.setVersion(mc.getVersion());
					
					voToDel.flagRemove();				
					
					IDependentKey dependentKey = DependentDataMap.createDependentKey(matrix.getMatrixEntityParentField());
					voSubform.getDependents().addData(dependentKey, voToDel);
					
				}
				else {
					voSubform.setFieldValue(mc.getEntityFieldMetaDataVO().getUID(), mc.getValue(UID.UID_NULL));
					
				}
			}			
			voSubform.flagRemove();
			lstSubformData.add(voSubform);			
		}		
		
		IDependentKey dependentKey = DependentDataMap.createDependentKey(matrix.getEntityYParentField());
		mp.addAllData(dependentKey, lstSubformData);
	}

	public void clearRemovedObjects() {
		if (modelFixed != null) {
			modelFixed.getRemovedObjects().clear();
			modelFixed.setMarked(Collections.emptySet());
			model.setMarked(Collections.emptySet());
		}
	}

	public void setMarked(Collection<Object> marked) {
		modelFixed.setMarked(marked);
		model.setMarked(marked);
	}

	public void setMatrixPreferences(String sPrefs) {
		final XStreamSupport xs = XStreamSupport.getInstance();

		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			WorkspaceDescription2.MatrixPreferences prefs = (WorkspaceDescription2.MatrixPreferences) xstream.fromXML(sPrefs);
			matrix.setMatrixPreferences(prefs);
			matrix.refresh(prefs, false);
		} catch (Exception e) {
			Log.warn("no Preferences");
		}
	}

	public String getMatrixPreferences() {
		
		WorkspaceDescription2.MatrixPreferences voPrefs = new WorkspaceDescription2.MatrixPreferences();
		voPrefs.setName(matrixName);
		voPrefs.setShowOnly(Boolean.FALSE);
		voPrefs.setSelected(new ArrayList<String>(matrix.getSelectedFields()));
		voPrefs.setAvailable(new TreeSet<String>(matrix.getAvailableFields()));	
		voPrefs.setAvailablefixed(new TreeSet<UID>(matrix.getAvailableFieldsFixed()));
		voPrefs.setSelectedfixed(new ArrayList<UID>(matrix.getSelectedFieldsFixed()));
		voPrefs.setFixedFieldWidths(new HashMap<UID, Integer>(matrix.getFixedFieldWidths()));
		voPrefs.setSelectedGroupElements(new HashMap<String, List<String>>(matrix.getSelectedGroupElements()));
		voPrefs.setAvailableGroupElements(new HashMap<String, SortedSet<String>>());
		
		voPrefs.setAvailableGroupElements(new HashMap<String, SortedSet<String>>(matrix.getAvailableGroupElements()));
		
		List<Pair<Integer, Integer>> lstSortKeys = new ArrayList<Pair<Integer,Integer>>();
		for(SortKey key : matrix.getSortKeys()) {
			int sortorder = 0;
			switch (key.getSortOrder()) {
			case ASCENDING:
				sortorder = 1;
				break;
			case DESCENDING:
				sortorder = 2;
				break;
			default:
				sortorder = 0;
				break;
			}
			lstSortKeys.add(new Pair<Integer, Integer>(key.getColumn(), sortorder));
		}
		voPrefs.setSortKeys(lstSortKeys);
		
		if(entityPreferences != null) {
			for(MatrixPreferences p : entityPreferences.getMatrixPreferences()) {
				entityPreferences.removeMatrixPreferences(p);
			}
			entityPreferences.addMatrixPreferences(voPrefs);
		}

		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(voPrefs);
		}
	}

	public UID getMatrixPreferencesField() {
		return matrix.getPreferencesField();
	}
	
	public void setParentId(Object oParentId) {
		this.oParentId = oParentId;
	}
	
	public void setParentObject(Collectable parent) {
		this.parent = parent;
	}
	
	public CollectController<Long, ?> getCollectController() {
		return this.clct;
	}
	
	public Collectable getParentObject() {
		return this.parent;
	}
	
	public void setEntityPreferences(EntityPreferences prefs) {
		if(prefs == null) {
			this.entityPreferences = new EntityPreferences();
		}
		this.entityPreferences = prefs;		
		if(prefs.getMatrixPreferences().isEmpty()){
			prefs.addMatrixPreferences(new MatrixPreferences());
		}
		for(MatrixPreferences mp : prefs.getMatrixPreferences()) {
			matrix.setMatrixPreferences(mp);
			matrix.refresh(mp, true);
		}
	}

	@Override
	public void close() {		
		this.parent = null;
		this.oParentId = null;
	}
	
	protected static boolean isSystemField(FieldMeta<?> vo) {
		return (vo.getClass().isAssignableFrom(SystemFields.class));
	}
	
	public void setParameterValueListProvider(UID uidField, CollectableField cf) throws CommonBusinessException {
		this.matrix.setParameterValueListProvider(uidField, cf);
	}

}
