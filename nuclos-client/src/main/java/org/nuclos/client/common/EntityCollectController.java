//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.IllegalComponentStateException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.FocusManager;
import javax.swing.Icon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.http.util.LangUtils;
import org.apache.log4j.Logger;
import org.nuclos.api.context.MultiContext;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.api.context.SingleContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.ui.dnd.DropContext;
import org.nuclos.api.ui.dnd.DropHandler;
import org.nuclos.api.ui.dnd.DropHandlerProvider;
import org.nuclos.api.ui.dnd.DropTarget;
import org.nuclos.api.ui.dnd.Flavor;
import org.nuclos.api.ui.dnd.InstallContext;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponentListener;
import org.nuclos.api.ui.layout.NewContext;
import org.nuclos.api.ui.layout.SearchContext;
import org.nuclos.client.command.CommonClientWorker;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.common.AbstractDetailsSubFormController.DetailsSubFormTableModel;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenerationController;
import org.nuclos.client.genericobject.GenerationController.GenerationListener;
import org.nuclos.client.genericobject.GeneratorActions;
import org.nuclos.client.genericobject.ReportController;
import org.nuclos.client.genericobject.datatransfer.GenericObjectIdModuleProcess;
import org.nuclos.client.genericobject.datatransfer.TransferableGenericObjects;
import org.nuclos.client.history.HistoryController;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.masterdata.datatransfer.MasterDataIdAndEntity;
import org.nuclos.client.masterdata.datatransfer.TransferableMasterDatas;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.nuclet.NucletComponentRepository;
import org.nuclos.client.report.PrintController;
import org.nuclos.client.report.PrintController.PrintControllerEventListener;
import org.nuclos.client.report.PrintController.PrintControllerEventType;
import org.nuclos.client.report.ReportDelegate;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.scripting.context.CollectControllerScriptContext;
import org.nuclos.client.searchfilter.SearchFilter;
import org.nuclos.client.ui.AskAndSaveResult;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.BubbleUtils;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.NuclosToolBar;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.CollectStateListener;
import org.nuclos.client.ui.collect.CollectStateModel;
import org.nuclos.client.ui.collect.RunCustomRuleSelectedCollectablesController;
import org.nuclos.client.ui.collect.SearchFilterBar;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentWithValueListProvider;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.component.model.EditModel;
import org.nuclos.client.ui.collect.component.model.SearchEditModel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModelImpl;
import org.nuclos.client.ui.collect.result.ResultActionCollection;
import org.nuclos.client.ui.collect.result.ResultActionCollection.ResultActionType;
import org.nuclos.client.ui.collect.result.ResultController;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.collect.search.ISearchStrategy;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.client.ui.labeled.LabeledComponent;
import org.nuclos.client.ui.labeled.LabeledTextField;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.client.valuelistprovider.cache.ManagedCollectableFieldsProvider;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LafParameter;
import org.nuclos.common.LafParameterStorage;
import org.nuclos.common.LockMode;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PointerCollection;
import org.nuclos.common.PointerException;
import org.nuclos.common.SF;
import org.nuclos.common.SplitViewSettings;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UnlockMode;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.WorkspaceDescription2.LayoutPreferences;
import org.nuclos.common.WorkspaceDescription2.Split;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.ValueListProviderType;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.report.valueobject.PrintResultTO;
import org.nuclos.common.security.IPermission;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonPrintException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.AbstractProxyList;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.ejb3.GenerationResult;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.ExecuteCustomRuleActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public abstract class EntityCollectController<PK,Clct extends Collectable<PK>> extends NuclosCollectController<PK,Clct>
		implements IEntityCollectController<PK,Clct>, ISplitViewController, ISubformHistoricalViewLookUp {

	private static final Logger LOG = Logger.getLogger(EntityCollectController.class);
	
	private EntityCollectController<PK,Clct> initOnlyDetailController;
	private MainFrameTab initOnlyDetailTab;
	
	private MainFrameTab masterTab;
	private WeakReference<EntityCollectController<PK,Clct>> detailController;
	private WeakReference<MainFrameTab> detailTab;
	
	private CollectableEventListener<PK> splitDetailCollectableEventListener;
	private CollectStateListener splitDetailCollectStateListener;
	// The following should be final but can't because they are
	// reset to null in close() to avoid memory leaks. (tp)
	public JCheckBoxMenuItem chkbxUseInvalidMasterData = new JCheckBoxMenuItem();
	
	private PK selectAfterFillResult;
	
	private boolean isSynchronizingToSplitDetails;
	private boolean isSynchronizingFromSplitDetails;
	
	private WeakReference<CollectController> refreshOnCloseIfNecessary;
	private boolean refreshOnClose = false;
	private Runnable cmdSwitchSplitViewMode;
	private UpdateSplitViewSettingsRunnable cmdUpdateSplitViewSettings;
	
	private boolean isClosing = false;

	public boolean bUseInvalidMasterData = false;
	
	private CollectableListOfValues<UID> searchOwnerClctLOV;

	private final static String loadingLabelText = SpringLocaleDelegate.getInstance().getMessage("entity.collect.controller.loading.label", "Ladevorgang...");
	private final static String notLoadingLabelText = "              ";

	private ResultPanel.StatusBarButton btnExportResults = new ResultPanel.StatusBarButton(
			SpringLocaleDelegate.getInstance().getTextFallback("EntityCollectController.44", "Export List"));

	public void refreshSubForm(SubFormController sfctrl, CollectableSearchCondition filter) {
		boolean detailsChangeIgnoredBefore = this.isDetailsChangedIgnored();
		this.setDetailsChangedIgnored(true);
		SubFormsInterruptableClientWorker worker = new SubFormsLoaderClientWorker<PK>(sfctrl, this, this.getSelectedCollectable(), filter, this.getSelectedCollectable());
		CommonMultiThreader.getInstance().execute(worker);
		this.setDetailsChangedIgnored(detailsChangeIgnoredBefore);
	}

	private interface UpdateSplitViewSettingsRunnable {
		public void run(LafParameterStorage storage);
	}
	
	private static class GenerateAction extends AbstractAction {
		
		private final List<GeneratorActionVO> generators = new ArrayList<GeneratorActionVO>();
		
		private final WeakReference<ICmdGenerateObject> cgo;
		
		private GenerateAction(ICmdGenerateObject cgo) {
			if (cgo == null) {
				throw new NullPointerException();
			}
			this.cgo = new WeakReference<ICmdGenerateObject>(cgo);
		}
		
		List<GeneratorActionVO> getGenerators() {
			return generators;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			final ICmdGenerateObject cmd = cgo.get();
			if (cmd != null) {
				final GeneratorActionVO selected = generators.get(e.getID());
				if (!(selected instanceof GeneratorDummy)) {
					try {
						cmd.cmdGenerateObject(selected);
					} finally {
						putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, null);
					}
				}
			} else {
				generators.clear();
			}
		}
	}
	
	protected JLabel loadingLabel;
	protected SubFormsLoader subFormsLoader;
	
	protected Map<UID, MatrixController> mpOfMatrixController;

	private PointerCollection pointerCollection = null;
	private NuclosBusinessRuleException pointerException = null;
	private List<ActionListener> lstPointerChangeListener = new ArrayList<ActionListener>();

	protected Map<UID, SearchConditionSubFormController> mpsubformctlSearch;
	
	private ResultActionsWorker runningResultActionsWorker;
	
	private String customUsage;
	
	private CollectableField mandator;
	
	private boolean bIsFillDuringClone;
	
	private NucletComponentRepository nucletComponentRepository = SpringApplicationContextHolder.getBean(NucletComponentRepository.class);
	private List<org.nuclos.api.ui.dnd.DropHandler<PK>> lstDropHandlers= new ArrayList<org.nuclos.api.ui.dnd.DropHandler<PK>>();
	private PointerAction pointerAction;
	
	private final GenerateAction actGenerate = (GenerateAction) NuclosToolBarActions.GENERATE.init(new GenerateAction(this));
	
	protected final Action actPrintDetails = NuclosToolBarActions.PRINT_DETAILS
			.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdPrintSelectedObjects();
				}
			});
	
	protected final Action actLock = NuclosToolBarActions.LOCK
			.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdLock();
				}
			});

	/**
	 * Don't make this public!
	 *
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	protected EntityCollectController(UID entityUid, MainFrameTab tabIfAny, String customUsage) {
		this(NuclosCollectableEntityProvider.getInstance().getCollectableEntity(entityUid), tabIfAny, customUsage);
	}

	/**
	 * Don't make this public!
	 *
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	protected EntityCollectController(CollectableEntity clcte, MainFrameTab tabIfAny, String customUsage) {
		super(clcte, tabIfAny, new NuclosCollectControllerCommonState());
		this.customUsage = customUsage;
		this.loadingLabel = new JLabel(notLoadingLabelText);
		this.loadingLabel.setName("loadingLabel");
		subFormsLoader = new SubFormsLoader();
		initPointOn();
		actGenerate.putValue(ToolBarItemAction.VISIBLE, Boolean.FALSE);
	}

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 * @param viewMode 
	 */
	protected EntityCollectController(UID entityUid, MainFrameTab tabIfAny, ResultController<PK,Clct> rc, String customUsage, ControllerPresentation viewMode) {
		this(NuclosCollectableEntityProvider.getInstance().getCollectableEntity(entityUid), tabIfAny, rc, customUsage, viewMode);
	}

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 */
	protected EntityCollectController(CollectableEntity clcte, MainFrameTab tabIfAny, ResultController<PK,Clct> rc, String customUsage, ControllerPresentation viewMode) {
		super(clcte, tabIfAny, rc);
		this.viewMode = viewMode;
		this.customUsage = customUsage;
		this.loadingLabel = new JLabel(notLoadingLabelText);
		this.loadingLabel.setName("loadingLabel");
		subFormsLoader = new SubFormsLoader();
		initPointOn();
		actGenerate.putValue(ToolBarItemAction.VISIBLE, Boolean.FALSE);
	}
	
	private void initPointOn() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final Component cPointOnResult = getPointOnComponent(false);
				final Component cPointOnDetails = getPointOnComponent(true);
				if (cPointOnResult instanceof AbstractButton) {
					cPointOnResult.addMouseListener(getPointerContextListener());
				}
				if (cPointOnDetails instanceof AbstractButton) {
					cPointOnDetails.addMouseListener(getPointerContextListener());
				}
				addPointerChangeListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						final Component cPointOnResult = getPointOnComponent(false);
						final Component cPointOnDetails = getPointOnComponent(true);
						if (cPointOnResult instanceof AbstractButton) {
							cPointOnResult.setEnabled(getPointerAction().isEnabled());
						}
						if (cPointOnDetails instanceof AbstractButton) {
							cPointOnDetails.setEnabled(getPointerAction().isEnabled());
						}
					}
				});
			}
		});
	}

	@Override
	public void close() {
		try {
			if (!isClosing && refreshOnCloseIfNecessary != null) {
				synchronized (this) {
					if (refreshOnClose) {
						refreshOnClose = false;
						@SuppressWarnings({ "rawtypes" })
						CollectController ccToRefresh = refreshOnCloseIfNecessary.get();
						if (ccToRefresh != null) {
							ccToRefresh.refreshCurrentCollectable();
						}
					}
				}
			}
		} catch (Exception ex) {
			LOG.error("Error during close: " + ex.getMessage(), ex);
		} 
		
		isClosing = true;

		btnExportResults = null;
		cmdSwitchSplitViewMode = null;
		cmdUpdateSplitViewSettings = null;
		EntityCollectController<PK, Clct> detailController = getSplitDetailController();
		if (detailController != null) {
			if (!detailController.isClosing) {
				detailController.removeCollectableEventListener(splitDetailCollectableEventListener);
				CollectStateModel<PK,Clct> clctstatemdl = detailController.getCollectStateModel();
				if (clctstatemdl != null) {
					clctstatemdl.removeCollectStateListener(splitDetailCollectStateListener);
				}
				detailController.close();
			}
		}
		
		super.close();
		if (mpsubformctlSearch != null) {
			for (SearchConditionSubFormController search: mpsubformctlSearch.values()) {
				search.close();
			}
			mpsubformctlSearch.clear();
		}
		mpsubformctlSearch = null;
		searchOwnerClctLOV = null;
	}
	
	@Override
	public void makeConsistent(boolean bSearchTab) throws CollectableFieldFormatException {
		makeConsistent(bSearchTab, false);
	}

	public void makeConsistent(boolean bSearchTab, boolean bRefreshOnly)
			throws CollectableFieldFormatException {
		super.makeConsistent(bSearchTab);

		if (!stopEditing(bSearchTab))
			/** @todo we need to give a better error message here. */
			throw new CollectableFieldFormatException(getSpringLocaleDelegate()
					.getMessage("GenericObjectCollectController.95",
							"Ung\u00fcltige Eingabe in Unterformular."));
		
		if (bSearchTab) {		
			SearchFilterBar bar = getResultPanel().getSearchFilterBar();
			SearchFilter searchFilter = null;
			if (bRefreshOnly) {
				if ((bar.getSelected() == null || getMainFilter() == null)) {
					searchFilter = getMainFilter();
					if (searchFilter == null) searchFilter = bar.getSelected();
					if (searchFilter == null) searchFilter = bar.getDefault();
				}
			} else {
				if (bar.getSelected() == null) {
					searchFilter = bar.getDefault();
				}
			}
			if (searchFilter != null) {
				setMainFilter(searchFilter, false);
			}
		}
	}

	/**
	 * §todo move to CollectController after renaming stopEditing() to
	 *       stopEditingInDetails()
	 * 
	 * @param bSearchTab
	 * @return Has the editing been stopped?
	 */
	protected boolean stopEditing(boolean bSearchTab) {
		return bSearchTab ? stopEditingInSearch() : stopEditingInDetails();
	}
	
	protected void showLoading(boolean loading){
		synchronized (getTab()) {
			String sTitle = getTab().getTitle();
			if(loading){
				this.loadingLabel.setText(loadingLabelText);
				this.loadingLabel.revalidate();
				//setTitle((sTitle != null ? sTitle+" " : "") +loadingLabelText);
			} else {
				this.loadingLabel.setText(notLoadingLabelText);
				this.loadingLabel.revalidate();
				if(sTitle != null){
					StringBuffer sbTitle = new StringBuffer(sTitle);
					int loadingLabelTextPosition = sbTitle.indexOf(loadingLabelText);
					if(loadingLabelTextPosition >=0){
						//setTitle(sbTitle.substring(0,loadingLabelTextPosition));
					}
				}
			}
		}
	}

	/**
	 * creates a searchable subform ctl for each subform. If the subform is disabled, the controller will be disabled.
	 * @param mpSubForms
	 */
	protected Map<UID, SearchConditionSubFormController> newSearchConditionSubFormControllers(Map<UID, SubForm> mpSubForms) {
		final SearchEditModel editmodelSearch = getSearchPanel().getEditModel();

		return CollectionUtils.transformMap(mpSubForms, new Transformer<SubForm, SearchConditionSubFormController>() {
			@Override
			public SearchConditionSubFormController transform(SubForm subform) {
				return newSearchConditionSubFormController(subform, getEntityUid(), editmodelSearch);
			}
		});
	}

	/**
	 * §todo maybe move to CollectController?
	 * 
	 * @param subform
	 * @param clctcompmodelprovider
	 */
	protected SearchConditionSubFormController newSearchConditionSubFormController(SubForm subform, UID parentEntityUid,
		CollectableComponentModelProvider clctcompmodelprovider) {

		final String sControllerType = subform.getControllerType();
		if (sControllerType != null && !sControllerType.equals("default"))
			LOG.warn("Kein spezieller SearchConditionSubFormController f?r Controllertyp " + sControllerType + " vorhanden.");
		return _newSearchConditionSubFormController(clctcompmodelprovider, parentEntityUid, subform);
	}

	protected SearchConditionSubFormController _newSearchConditionSubFormController(CollectableComponentModelProvider clctcompmodelprovider,
		UID parentEntityUid, SubForm subform) {

		// if parent of subform is another subform, change given parent entity name
		UID parentSubFormUid = subform.getParentSubForm();
		if (parentSubFormUid != null)
			parentEntityUid = parentSubFormUid;

		return new SearchConditionSubFormController(getTab(), clctcompmodelprovider, parentEntityUid, subform,
			getPreferences(), getEntityPreferences(), MasterDataCollectableFieldsProviderFactory.newFactory(subform.getEntityUID(), valueListProviderCache));
	}

	@Override
	protected void initialize(CollectPanel<PK,Clct> pnlCollect) {
		super.initialize(pnlCollect);
		
		this.getCollectStateModel().addCollectStateListener(new EntityCollectStateListener());
		this.addCollectableEventListener(new PointerResetChangeEventListener());
		this.getResultPanel().getResultTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					refreshResultActions();
				}
			}
		});
		
//		this.getDetailsPanel().getToolBar().add(cmbbxGeneratorActions);
		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.GENERATE, actGenerate);
//		final ListCellRenderer originalRenderer = cmbbxGeneratorActions.getRenderer();
//		cmbbxGeneratorActions.setRenderer(new DefaultListCellRenderer() {
//			@Override
//			public Component getListCellRendererComponent(JList jlst, Object oValue, int iIndex, boolean bSelected,
//				boolean bHasFocus) {
//				final JLabel result = (JLabel) originalRenderer.getListCellRendererComponent(jlst, oValue, iIndex, bSelected,
//					bHasFocus);
//				result.setOpaque(false);
//
//				if (oValue == null) {
//					result.setText(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.9","Objektgenerator"));
//					result.setToolTipText(getSpringLocaleDelegate().getMessage(
//							"GenericObjectCollectController.17","Bitte w\u00e4hlen Sie einen Objektgenerator aus."));
//				}
//				else {
//					assert oValue instanceof GeneratorActionVO;
//					final GeneratorActionVO actvo = (GeneratorActionVO) oValue;
//					final String sSourceModuleName = GenerationController.getModuleLabel(actvo.getSourceModuleId());
//					final String sTargetModuleName = GenerationController.getModuleLabel(actvo.getTargetModuleId());
//					result.setToolTipText(getSpringLocaleDelegate().getMessage(
//							"GenericObjectCollectController.46","Erzeugt einen {0} aus dem/der aktuellen {1}", sTargetModuleName, sSourceModuleName));
//				}
//				return result;
//			}
//		});
	}
	
	@Override
	protected void revalidateAdditionalSearchField() {
		super.revalidateAdditionalSearchField();
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(getEntityUid());
		if (isSearchPanelAvailable() && eMeta.isMandator()) {
			UID mandatorFieldUID = SF.MANDATOR.getUID(getEntityUid());
			CollectableComponentModel clctcompmodel = getSearchEditView().getModel().getCollectableComponentModelFor(mandatorFieldUID);
			if (clctcompmodel != null) {
				NuclosCollectableComboBox searchcomp = new NuclosCollectableComboBox(clctcompmodel.getEntityField(), true);
				searchcomp.setModel(clctcompmodel);
				searchcomp.setValueListProvider(getCollectableFieldsProviderFactoryForSearchEditor().newCustomCollectableFieldsProvider(ValueListProviderType.MANDATOR, mandatorFieldUID));
				searchcomp.refreshValueList(true, false, true);
				super.addAdditionalSearchComponent(searchcomp);
			}
		}
		if (isSearchPanelAvailable() && eMeta.isOwner()) {
			UID ownerFieldUID = SF.OWNER.getUID(getEntityUid());
			CollectableComponentModel clctcompmodel = getSearchEditView().getModel().getCollectableComponentModelFor(ownerFieldUID);
			if (clctcompmodel != null) {
				setSearchOwnerLOVModelFromEditView();
				CollectableListOfValues<UID> searchcomp = new CollectableListOfValues<UID>(getCollectableEntity().getEntityField(ownerFieldUID), true, true);
				searchcomp.setModel(clctcompmodel);
				super.addAdditionalSearchComponent(searchcomp);
			}
		}
	}
	
	public void refreshResultActions() {
		if(viewMode == ControllerPresentation.DEFAULT) {
			getResultPanel().loadingResultActions();
			Collection<Clct> selectedCollectablesFromResult = getResultController().getSelectedCollectablesFromTableModel();
			ResultActionsWorker worker = new ResultActionsWorker(selectedCollectablesFromResult);
			worker.execute();
		}
	}
	
	protected List<ResultActionCollection> getResultActionsMultiThreaded(Collection<Clct> selectedCollectablesFromResult) {
		List<ResultActionCollection> result = new ArrayList<ResultActionCollection>();
		ResultActionCollection rac = new ResultActionCollection(ResultActionType.GENERATE,
				SpringLocaleDelegate.getInstance().getMessage("ResultPanel.12","Objektgeneratoren"));
		
		List<GeneratorActionVO> actions = getGeneratorActions(selectedCollectablesFromResult);
		if (actions!=null && !actions.isEmpty()) {
			for(final GeneratorActionVO actionVO : getGeneratorActions(selectedCollectablesFromResult)) {
				AbstractAction act = new AbstractAction(actionVO.toString(), 
						actionVO.getButtonIcon()==null ? null : ResourceCache.getInstance().getIconResource(actionVO.getButtonIcon().getId())) {
							@Override
							public void actionPerformed(ActionEvent e) {
								cmdGenerateObject(actionVO);
							}
						};
				rac.addAction(act);
			}
		}
		
		if (!rac.getActions().isEmpty()) {
			result.add(rac);
		}
		
		return result;
	}
	
	protected void initMatrixControllers() {
		mpOfMatrixController = new HashMap<>();
		for(UID matrix : getLayoutRoot().getMapOfMatrix().keySet()) {
			MatrixController mc = new MatrixController(getParent(), matrix, getLayoutRoot().getMapOfMatrix().get(matrix), (CollectController<Long, ?>)this);
			mc.setParentId(this.getSelectedCollectableId());			
			mc.matrix.addChangeListener(getChangeListener(false));
			mpOfMatrixController.put(matrix, mc);
		}
	}
	
	protected void loadMatrices(Collectable<PK> clct, UID iState) {
		// initMatrixControllers();
		
		for (MatrixController mc : getMatrixControllers()) {
			// adjust matrix
			if (this.getCollectStateModel().getCollectState().isDetailsModeNew()) {
				mc.setParentId(null);
				mc.setParentObject(null);
			}
			else {
				mc.setParentId(clct.getPrimaryKey());
				mc.setParentObject(clct);
				mc.loadMatrixData();
			}
			
			IPermission permissionEntityY = SubformPermission.ALL;
			IPermission permissionMatrixEntity = SubformPermission.ALL;
			if (iState != null) {
				UID YEntity = mc.getMatrixComponent().getEntityY();
				permissionEntityY = SecurityCache.getInstance().getSubFormPermission(YEntity, iState);
				// TODO: Check the following: The folloing is some kind of Sub-Subform Permissions, referencing to the main status
				UID sMatrixEntity = mc.getMatrixComponent().getMatrixEntity();
				permissionMatrixEntity = SecurityCache.getInstance().getSubFormPermission(sMatrixEntity, iState);
			}
			mc.setPermissions(permissionEntityY, permissionMatrixEntity);

			UID sPrefsField = mc.getMatrixPreferencesField();
			if(sPrefsField != null) {
				String sPrefs = (String) clct.getField(sPrefsField).getValue();
				mc.setMatrixPreferences(sPrefs);						
			}					
			mc.setEntityPreferences(getEntityPreferences());
		}
	}
	
	protected void setParameterMatrixVLP(UID uidField, CollectableField cf) {
		Map<UID, MatrixController> mpController = this.getMatrixController();
		for(UID uid : mpController.keySet()) {
			MatrixController mc = mpController.get(uid);
			try {
				mc.setParameterValueListProvider(uidField, cf);
			} catch (CommonBusinessException e) {
				LOG.warn(e);
			}
		}
	}
	
	public Map<UID, MatrixController> getMatrixController() {
		if(mpOfMatrixController == null){
			return Collections.EMPTY_MAP;
		}
		return mpOfMatrixController;
	}
	
	protected Collection<MatrixController> getMatrixControllers() {
		if(this.mpOfMatrixController != null)
			return this.mpOfMatrixController.values();
		return Collections.emptyList();
	}

	protected void initSearchSubforms() {
		if(!this.isSearchPanelAvailable())
			return;
		final LayoutRoot layoutrootSearch = getInitialLayoutMLDefinitionForSearchPanel();
		layoutrootSearch.getRootComponent().setFocusCycleRoot(true);
		getSearchPanel().setEditView(newSearchEditView(layoutrootSearch));
		revalidateAdditionalSearchField();
		Map<UID, SubForm> mpSubForm = layoutrootSearch.getMapOfSubForms();
		mpsubformctlSearch = newSearchConditionSubFormControllers(mpSubForm);
		setupSubFormController(mpSubForm, mpsubformctlSearch);
	}

	protected Map<UID, SearchConditionSubFormController> getMapOfSubFormControllersInSearch() {
		return mpsubformctlSearch;
	}

	protected abstract void setupSubFormController(Map<UID, SubForm> mpSubForm, Map<UID, ? extends SubFormController> mpSubFormController);

	protected abstract LayoutRoot getInitialLayoutMLDefinitionForSearchPanel();

	public SubFormsLoader getSubFormsLoader() {
		return subFormsLoader;
	}

	protected void initSubFormsLoader(){
		getSubFormsLoader().initLoaderStates();
	}
	/**
	 * Needed for FDM.
	 */
	public DetailsSubFormController<PK, CollectableEntityObject<PK>> getSubFormController(UID uid) {
		final Collection<DetailsSubFormController<PK, CollectableEntityObject<PK>>> controllers = getSubFormControllersInDetails();
		for (DetailsSubFormController<PK, CollectableEntityObject<PK>> controller : controllers) {
			if (uid.equals(controller.getCollectableEntity().getUID())) {
				return controller;
			}
		}
		return null;
	}

	@Override
	protected void unsafeFillMultiEditDetailsPanel(Collection<Clct> collclct) throws CommonBusinessException {
		super.unsafeFillMultiEditDetailsPanel(collclct);

		UID dataMandator = getMandatorUID(collclct);
		
		for (DetailsSubFormController<PK,CollectableEntityObject<PK>> subformctl : getSubFormControllersInDetails()) {
			subformctl.setMandatorUID(dataMandator);
			subformctl.setIntidForVLP(null);
			
			if (subformctl instanceof MasterDataSubFormController) {
				for (MasterDataSubFormController<PK> child : ((MasterDataSubFormController<PK>) subformctl).getChildSubFormController()) {
					child.getSubForm().setEnabled(false);
				}
			}

			subformctl.setMultiEdit(true);
		}
	}

	@Override
	protected boolean isSaveAllowed() {
		return super.isSaveAllowed() && isNotLoadingSubForms();
	}

	protected boolean isNotLoadingSubForms() {
		return !getSubFormsLoader().isLoadingSubForms();
	}

	@Override
	protected boolean isDeleteSelectedCollectableAllowed() {
		return super.isDeleteSelectedCollectableAllowed() && isNotLoadingSubForms();
	}

	@Override
	protected boolean isCloneAllowed() {
		boolean result = super.isCloneAllowed() && isNotLoadingSubForms();
		final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(getEntityUid());
		if (eMeta.getCloneGenerator() == null)
			return result;
		else {
			final GeneratorActionVO genActionVO = GeneratorActions.getGeneratorAction(eMeta.getCloneGenerator());
			result = result && (genActionVO != null);
		}

		return result;
	}
	
	protected boolean isDetailsViewAvailable(MasterDataSubFormController mdsubformctl) {
		boolean hasScripts = 
				mdsubformctl.getSubForm().getDeleteEnabledScript() != null || 
						mdsubformctl.getSubForm().getNewEnabledScript() != null ||
								mdsubformctl.getSubForm().getCloneEnabledScript() != null ||
										mdsubformctl.getSubForm().getEditEnabledScript() != null;
		return !hasScripts && MasterDataLayoutHelper.isLayoutMLAvailable(mdsubformctl.getSubForm().getEntityUID(), false) &&
		(SecurityCache.getInstance().isReadAllowedForEntity(mdsubformctl.getSubForm().getEntityUID()));
		
	} 

	public MasterDataSubFormController<PK> newDetailsSubFormController(SubForm subform,
			UID parentEntityUid, CollectableComponentModelProvider clctcompmodelprovider,
			MainFrameTab tab, JComponent compDetails, Preferences prefs, EntityPreferences entityPrefs, EntityCollectController<?, ?> eoController) {
		//subform.setLockedLayer();
		MasterDataSubFormController<PK> controller = NuclosCollectControllerFactory.getInstance().newDetailsSubFormController(subform,
				parentEntityUid, clctcompmodelprovider, tab, compDetails, prefs, entityPrefs, valueListProviderCache, state, eoController);
		controller.setParentController((EntityCollectController<PK,CollectableEntityObject<PK>>) this);
		return controller;
	}

	public Collection<UID> getAdditionalLoaderNames(){
		List<UID> emptyList = Collections.emptyList();
		return emptyList;
	}

	/**
	 * switches to "New" mode and fills the Details panel with the contents of the selected Collectable.
	 * @throws CommonBusinessException
	 */
	@Override
	protected void cloneSelectedCollectable() throws CommonBusinessException {
		final Clct clctBackup = this.getCompleteSelectedCollectable(false, false);
		clctBackup.removeId();
		//all subform workers will be interrupted and subforms will be stay disabled. see EntityCollectStateListener.detailsModeLeft.
		this.enterNewChangedMode();

		final EntityMeta<PK> eMeta = (EntityMeta<PK>) MetaProvider.getInstance().getEntity(getEntityUid());
		if (eMeta.isMandator()) {
			setMandator(clctBackup.getField(SF.MANDATOR.getUID(eMeta)));
		}
		if (eMeta.getCloneGenerator() == null) {
			try {
				bIsFillDuringClone = true;
				this.unsafeFillDetailsPanel(clctBackup);
			} finally {
				bIsFillDuringClone = false;
			}
			setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_NEW_CHANGED);
			
			// @see NUCLOS-2126
			final EditModel modelDetails = getDetailsPanel().getEditModel();
			for (CollectableComponentModel clctmodel : modelDetails.getCollectableComponentModels()) {
				for (CollectableComponent clctcomp : getDetailCollectableComponentsFor(clctmodel.getFieldUID()))
					if (!clctcomp.isCloneable())
						clctmodel.clear();
			}
			getSubFormsLoader().addAfterLoadingRunnable(new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					for(DetailsSubFormController<?,?> sfctrl : getMapOfSubFormControllersInDetails().values()) {
						if (!sfctrl.getSubForm().isCloneable()) {
							sfctrl.clear();
						}
						else {
							for (Column column : sfctrl.getSubForm().getColumns()) {
								if (!column.isCloneable()) {
									final SubFormTableModel mdl = sfctrl.getSubForm().getSubformTable().getSubFormModel();
									final int col = mdl.findColumnByFieldUid(column.getUID());
									if (col != -1) {
										for (int i = 0; i < mdl.getRowCount(); i++) {
											mdl.setValueAt(null, i, col);
										}
									}
								}
							}
						}
					}
				}
			});
		}
		else {
			final GeneratorActionVO genActionVO = GeneratorActions.getGeneratorAction(eMeta.getCloneGenerator());
			if (genActionVO == null)
				return;
			
			final Map<Long, UsageCriteria> sources = new HashMap<Long, UsageCriteria>();
			
			sources.put((Long) clctBackup.getId(), null);
			GenerationController controller = new GenerationController(sources, genActionVO, this, getTab());
			controller.addGenerationListener(new GenerationListener() {
				@Override
				public void generatedWithException(GeneratorFailedException result) {
					Errors.getInstance().showExceptionDialog(getTab(), result.getMessage(), result.getCause());
				}
				
				@Override
				public void generatedSucessful(GenerationResult result) {
					Clct clct = null;
					EntityObjectVO<Long> eo = result.getGeneratedObject();
					if (clctBackup instanceof CollectableMasterDataWithDependants) {
						clct = (Clct) new CollectableMasterDataWithDependants(getCollectableEntity(), new MasterDataVO<Long>(eo));
					} else if (clctBackup instanceof CollectableGenericObjectWithDependants) try {
						for (List<EntityObjectVO<?>> lstDependantEO : eo.getDependents().getRoDataMap().values()) {
							for (EntityObjectVO<?> dependantEO : lstDependantEO) dependantEO.flagNew();
						}
						EntityMeta<?> em = MetaProvider.getInstance().getEntity(eo.getDalEntity());
						CollectableEOEntity meta = new CollectableEOEntity(em);
						GenericObjectWithDependantsVO gowd = DalSupportForGO.getGenericObjectWithDependantsVO(eo, meta);
						clct = (Clct) new CollectableGenericObjectWithDependants(gowd);
					} catch (CommonValidationException cve) {
						LOG.warn(cve.getMessage(), cve);
					}
					
					if (clct != null)
						try {
							unsafeFillDetailsPanel(clct);							
						} catch (Exception e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						}
				}
			});
			controller.setConfirmationEnabled(false);
			controller.generateGenericObject(true, null);
		}
	}

	/**
	 * switches to New changed mode
	 */
	protected void enterNewChangedMode() {
		if (this.stopEditingInDetails()) {
			try {
				this.setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_NEW_CHANGED);
			}
			catch (CommonBusinessException ex) {
				Errors.getInstance().showExceptionDialog(this.getTab(), ex);
			}
		}
	}

	/**
	 * save scroll position and selection of subforms an restore after reloading has finished (NUCLOSINT-851)
	 */
	@Override
	public void refreshCurrentCollectable() throws CommonBusinessException {
		this.refreshCurrentCollectable(true);
	}

	/**
	 * save scroll position and selection of subforms an restore after reloading has finished (NUCLOSINT-851)
	 */
	@Override
    public void refreshCurrentCollectable(boolean withMultiThreader) throws CommonBusinessException {
		saveAndRestoreSubformState();
	    super.refreshCurrentCollectable(withMultiThreader);
    }

    protected void saveAndRestoreSubformState() {
		final Map<UID, Rectangle> visibleRectangles = new HashMap<UID, Rectangle>();
		final Map<UID, List<PK>> selectedRows = new HashMap<UID, List<PK>>();

		for (DetailsSubFormController<PK,?> controller : getSubFormControllersInDetails()) {
			visibleRectangles.put(controller.getEntityAndForeignKeyField().getEntity(), controller.getSubForm().getJTable().getVisibleRect());
			if (!controller.getSelectedCollectables().isEmpty()) {
				List<PK> selectedIds = new ArrayList<>();
				for ( Collectable<PK> clct : controller.getSelectedCollectables()) {
					selectedIds.add(clct.getId());
				}
				selectedRows.put(controller.getEntityAndForeignKeyField().getEntity(), selectedIds);
			}
		}
		this.getSubFormsLoader().addAfterLoadingRunnable(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				for (UID entity : visibleRectangles.keySet()) {
					for (DetailsSubFormController<PK,?> controller : getSubFormControllersInDetails()) {
						if (entity.equals(controller.getEntityAndForeignKeyField().getEntity())) {
							boolean setDefaultIndex = true;
							if (selectedRows.containsKey(entity)) {
								DetailsSubFormTableModel<PK,?> subformtablemodel = (DetailsSubFormTableModel<PK,?>) controller.getSubFormTableModel();
								List<PK> selectedIds = selectedRows.get(entity);
								for (PK id : selectedIds) {
									setDefaultIndex = false;
									int index = subformtablemodel.findRowById(id);
									if (index > -1) {
										int viewindex = controller.getJTable().convertRowIndexToView(index);
										controller.getJTable().getSelectionModel().addSelectionInterval(viewindex, viewindex);
									}
								}
							}
							if (setDefaultIndex && controller.getCollectableTableModel().getRowCount() > 0) {
								controller.getJTable().getSelectionModel().addSelectionInterval(0, 0);
							}
							controller.getJTable().scrollRectToVisible(visibleRectangles.get(entity));
						}
					}
				}
			}
		});
	}

	// =============== Additional ChangeListeners for search ===============

	/**
	 * @deprecated Move to SearchController specialization.
	 */
	@Override
	public void addAdditionalChangeListenersForSearch() {
		this.addAdditionalChangeListeners(true);
	}

	/**
	 * @deprecated Move to SearchController specialization.
	 */
	protected void addAdditionalChangeListeners(boolean bSearchable) {
		for (SubFormController subformctl : getSubFormControllers(bSearchable)) {
			subformctl.getSubForm().addChangeListener(getChangeListener(bSearchable));
		}
	}

	protected abstract Collection<? extends SubFormController> getSubFormControllers(boolean bSearchTab);

	protected ChangeListener getChangeListener(boolean bSearchable) {
		return bSearchable ? this.changelistenerSearchChanged : this.changelistenerDetailsChanged;
	}

	// =============== inner classes ===============

	/* private inner class SubFormsLoader */
	protected class SubFormsLoader {
		private boolean loading;
		private boolean suspended;

		private Map<UID, Boolean> subFormsLoadState;
		private Map<UID, SubFormsInterruptableClientWorker> subFormsClientWorker;

		private Map<UID, Boolean> subFormsSuspendedState;

		private final List<CommonRunnable> afterLoadingRunnables;

		public SubFormsLoader(){
			this.loading = false;
			this.suspended = false;
			this.subFormsLoadState = new HashMap<UID, Boolean>();
			this.subFormsClientWorker = new HashMap<UID, SubFormsInterruptableClientWorker>();
			this.subFormsSuspendedState = new HashMap<UID, Boolean>();
			this.afterLoadingRunnables = new ArrayList<CommonRunnable>();
		}

		private void initLoaderStates() {
			if(!getCollectState().isDetailsModeMultiViewOrEdit()){
				synchronized (this) {
					// only a keys from MapOfSubFormControllersInDetails as entity name is not enough here because of m:n enities
						Map<UID, DetailsSubFormController<PK,CollectableEntityObject<PK>>> mapSFC = EntityCollectController.this.getMapOfSubFormControllersInDetails();
						if (mapSFC != null) {
							for (DetailsSubFormController<PK,CollectableEntityObject<PK>> subformctl : mapSFC.values()) {
								if (subformctl.getSubForm() == null || subformctl.getSubForm().getParentSubForm() == null) {
									this.subFormsLoadState.put(subformctl.getCollectableEntity().getUID(), Boolean.FALSE);
								}
							}
						}
					//additional loader (calculated attributes)
					for (UID addLoader : EntityCollectController.this.getAdditionalLoaderNames()) {
						this.subFormsLoadState.put(addLoader, new Boolean(false));
					}
					this.loading = false;
				}
			}
		}

		public void startLoading(){
			if(!getCollectState().isDetailsModeMultiViewOrEdit()){
				synchronized (this) {
					//if(!this.suspended){
					//	this.suspendedForms.clear();
					//}
					resetLoaderStates();
					if(!this.subFormsLoadState.isEmpty()){
						this.loading = true;
						EntityCollectController.this.disableToolbarButtons();
						EntityCollectController.this.showLoading(true);
						LOG.debug("loading started");
					}
				}
			}
		}
		
		public void startLoading(UID subFormUid){
			if(!getCollectState().isDetailsModeMultiViewOrEdit()){
				synchronized (this) {
					//if(!this.suspended){
					//	this.suspendedForms.clear();
					//}
					setSubFormLoaded(subFormUid, false);
					
					if(!this.subFormsLoadState.isEmpty()){
						this.loading = true;
						EntityCollectController.this.disableToolbarButtons();
						EntityCollectController.this.showLoading(true);
						LOG.debug("loading started");
					}
					
					SubFormsInterruptableClientWorker clientWorker = this.subFormsClientWorker.get(subFormUid);
					if (clientWorker != null) {
						addSubFormClientWorker(subFormUid, clientWorker);
					}
				}
			}
		}

		public void finishLoading() {
			if(!getCollectState().isDetailsModeMultiViewOrEdit()){
				if(!this.subFormsLoadState.isEmpty()){
					this.loading = false;
					EntityCollectController.this.showLoading(false);
					UIUtils.invokeOnDispatchThread(new Runnable() {
						@Override
						public void run() {
							if (isDetailsmodeNewChanged()) {
								EntityCollectController.this.enableToolbarButtonsForDetailsMode(CollectState.DETAILSMODE_NEW_CHANGED);
							} else {
								EntityCollectController.this.enableToolbarButtonsForDetailsMode(CollectState.DETAILSMODE_VIEW);
							}
						}
					});
					UIUtils.invokeOnDispatchThread(new Runnable() {
						@Override
						public void run() {
							ScriptContext ctx = new CollectControllerScriptContext(EntityCollectController.this, 
									new ArrayList<DetailsSubFormController<PK,?>>(getSubFormControllersInDetails()));
							if (getDetailsPanel() != null) {
								for (CollectableComponent c : getDetailsPanel().getEditView().getCollectableComponents()) {
									c.setComponentState(ctx, null);
									c.setBackgroundColor(ctx, null);
								}
							}
						}
					});
					synchronized (this.afterLoadingRunnables) {
						for (CommonRunnable cr : this.afterLoadingRunnables) {
							try {
								cr.run();
							} catch (Exception e) {
								LOG.info("finishedLoading failed: " + e, e);
							}
						}
						this.afterLoadingRunnables.clear();
					}
				}
			}
		}

		public void resetLoaderStates(){
			this.subFormsLoadState.clear();
			initLoaderStates();
		}

		public boolean isLoadingSubForms(){
			return this.loading;
		}

		public boolean haveUnloadedSubforms(){
			return this.subFormsLoadState.containsValue(new Boolean(false));
		}

		public boolean isSubFormLoaded(UID subFormUid){
			return this.subFormsLoadState.get(subFormUid);
		}

		public void setSubFormLoaded(UID subFormUid, boolean loaded){
			synchronized (this) {
				this.subFormsLoadState.put(subFormUid, loaded);
				/*if(loaded){ // we want to reuse the clientworkers.
					this.subFormsClientWorker.remove(subFormName);
				}*/
				if(!this.haveUnloadedSubforms()){
					this.finishLoading();
				}
			}
		}

		public void addSubFormClientWorkerInSuspendedMode(UID subFormUid, final SubFormsInterruptableClientWorker worker){
			if(this.subFormsSuspendedState.containsKey(subFormUid)){
				if(this.subFormsSuspendedState.get(subFormUid).booleanValue()){
					setSubFormLoaded(subFormUid, true);
				} else {
					startRunningWorker(subFormUid, worker);
				}
			} else {
				startRunningWorker(subFormUid, worker);
			}
		}

		public void addSubFormClientWorker(UID subFormUid, final SubFormsInterruptableClientWorker worker){
			if(!this.subFormsSuspendedState.isEmpty()){
				addSubFormClientWorkerInSuspendedMode(subFormUid, worker);
			} else {
				startRunningWorker(subFormUid, worker);
			}
		}

		private void startRunningWorker(UID subFormUid, final SubFormsInterruptableClientWorker worker) {
			this.subFormsClientWorker.put(subFormUid, worker);
			CommonMultiThreader.getInstance().execute(worker);
		}

		public void suspendRunningClientWorkers(){
			synchronized (this) {
				if(!this.suspended){
					this.suspended = true;
					List<UID> notLoadedFoms = getSubFormsWithState(false);
					for(UID notLoadedFom : notLoadedFoms){
						SubFormsInterruptableClientWorker notLoadedWorker = this.subFormsClientWorker.get(notLoadedFom);
						if(notLoadedWorker != null){
							notLoadedWorker.interrupt();
						}
					}
					this.subFormsSuspendedState.putAll(this.subFormsLoadState);
				}
			}
		}

		public void addSuspendedClientWorker(UID workerName){
			subFormsSuspendedState.put(workerName, false);
		}

		public boolean hasSuspendedClientWorkers(){
			synchronized (this) {
				return this.subFormsSuspendedState.containsValue(false);
			}
		}

		public void resume(){
			synchronized (this) {
				this.subFormsSuspendedState.clear();
				this.suspended = false;
			}
		}

		public void interruptAllClientWorkers(){
			synchronized (this) {
				if(this.suspended){
					return;
				}
				for (SubFormsInterruptableClientWorker worker : this.subFormsClientWorker.values()) {
					worker.interrupt();
				}
				this.subFormsClientWorker.clear();
				this.initLoaderStates();
				EntityCollectController.this.showLoading(false);
			}
		}

		public List<UID> getSubFormsWithState(boolean loaded){
			List<UID> subFormNames = new ArrayList<UID>();
			for (UID subformctlName : subFormsLoadState.keySet()) {
				if(this.subFormsLoadState.get(subformctlName).booleanValue() == loaded){
					subFormNames.add(subformctlName);
				}
			}
			return subFormNames;
		}

		public void addAfterLoadingRunnable(CommonRunnable pAfterLoadingRunnable) {
			synchronized (this.afterLoadingRunnables) {
				this.afterLoadingRunnables.add(pAfterLoadingRunnable);
			}
		}
		
		public void removeAfterLoadingRunnable(CommonRunnable pAfterLoadingRunnable) {
			synchronized (this.afterLoadingRunnables) {
				this.afterLoadingRunnables.remove(pAfterLoadingRunnable);
			}
		}

	} // inner class SubFormsLoader

	/* private abstract inner class SubFormsInterruptableClientWorker */
	public static abstract class SubFormsInterruptableClientWorker implements CommonClientWorker {
		
		protected volatile boolean interrupted = false;

		public void interrupt(){
			this.interrupted = true;
		}

		@Override
        public abstract void init() throws CommonBusinessException;

		@Override
        public abstract void work() throws CommonBusinessException;

		@Override
        public abstract void paint() throws CommonBusinessException;

		@Override
        public abstract JComponent getResultsComponent();

		@Override
        public abstract void handleError(Exception ex);
	} // inner class SubFormsInterruptableClientWorker

	/* private inner class EntityCollectStateListener */
	private class EntityCollectStateListener extends CollectStateAdapter {
		@Override
		public void resultModeEntered(CollectStateEvent ev) throws CommonBusinessException {
			//EntityCollectController.this.getSubFormsLoader().interruptAllClientWorkers();
			if(!(ev.getNewCollectState().isDetailsMode() && CollectState.isDetailsModeViewOrEdit(ev.getNewCollectState().getInnerState())) && !getCollectState().isDetailsModeMultiViewOrEdit()){
				EntityCollectController.this.getSubFormsLoader().interruptAllClientWorkers();
			}
		}
		
		

		@Override
		public void searchModeEntered(CollectStateEvent ev)	throws CommonBusinessException {
			final Collection<LayoutComponent<PK>> colComponents = getLayoutRoot().getLayoutComponents();
			for(LayoutComponent<PK> component : colComponents) {
				for(LayoutComponentListener<PK> li : component.getLayoutComponentListeners()) {
					li.searchEntered(new SearchContext() {
						
					});
				}
			}
		}

		@Override
		public void detailsModeLeft(final CollectStateEvent ev) {
			if(!(ev.getNewCollectState().isDetailsMode() && ((CollectState.isDetailsModeViewOrEdit(ev.getNewCollectState().getInnerState()) || (CollectState.DETAILSMODE_NEW_CHANGED == ev.getNewCollectState().getInnerState())))) && !getCollectState().isDetailsModeMultiViewOrEdit()){
			//if(!(ev.getNewCollectState().isDetailsMode() && CollectState.isDetailsModeEdit(ev.getNewCollectState().getInnerState())) && !getCollectState().isDetailsModeMultiViewOrEdit()){
				EntityCollectController.this.getSubFormsLoader().interruptAllClientWorkers();
			}

		// NUCLEUSINT-1047 reload selected collectable
			final int iDetailsModeOld = ev.getOldCollectState().getInnerState();
			final int iDetailsModeNew = ev.getNewCollectState().getInnerState();
			
			if((iDetailsModeOld == CollectState.DETAILSMODE_EDIT)
					&& !(iDetailsModeNew == CollectState.DETAILSMODE_EDIT || iDetailsModeNew == CollectState.DETAILSMODE_MULTIEDIT)) {
				
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							if (isClosed()) {
								return;
							}
							if (ev.getNewCollectState().equals(getCollectStateModel().getCollectState())) {
								// state changed during invocation (multiedit in splitview)
								return;
							}
							
							if (getResultTable().getSelectedRow() >= 0) {
								Clct clctCurrent = getSelectedCollectable();
								boolean bWasDetailsChangedIgnored = isDetailsChangedIgnored();
								setDetailsChangedIgnored(true);
								try {
									EntityCollectController.this.readValuesFromEditPanel(clctCurrent, false);
									getResultController().replaceCollectableInTableModel(clctCurrent);
								} finally {
									setDetailsChangedIgnored(bWasDetailsChangedIgnored);
								}
							}
						}
						catch(CommonBusinessException ex) {
							throw new CommonFatalException(ex);
						}
					}
				});
			}
		}

	}	// inner class DefaultCollectStateListener

	/**
	 *
	 * handle reset of <code>PointerCollection</code>
	 */
	private class PointerResetChangeEventListener implements CollectableEventListener<PK> {
		@Override
		public void handleCollectableEvent(Collectable<PK> collectable,	org.nuclos.client.ui.collect.CollectController.MessageType messageType) {
			switch(messageType) {
				case REFRESH_DONE_DIRECTLY :
				case EDIT_DONE :
				case STATECHANGE_DONE :
				case DELETE_DONE :
				case NEW_DONE :
				case CLCT_LEFT :
					setPointerInformation(null, null);
					break;
			}
		}
	}

	protected class PointerContextListener extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {

			if (SwingUtilities.isRightMouseButton(e)) {
				if (pointerException != null) {
					final Component cPointOn = getPointOnComponent();
					final JPopupMenu menu = new JPopupMenu();
					final String itemLabel = getSpringLocaleDelegate().getMessage("EntityCollectController.1", "Herkunft anzeigen") + "...";
					final JMenuItem originItem = new JMenuItem(itemLabel, NuclosToolBarActions.INFO.getIcon());
					originItem.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							Errors.getInstance().showDetailedExceptionDialog(EntityCollectController.this.getTab(), pointerException);
						}
					});
					menu.add(originItem);
					menu.show(cPointOn, 0, cPointOn.getPreferredSize().height);
				}
			}
		}
	}

	/**
	 *
	 * displays <code>PointerCollection</code> in a JPopupMenu
	 */
	protected class PointerAction extends AbstractAction {

		public PointerAction() {
			super(null, NuclosToolBarActions.INFO.getIcon());
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			final PointerCollection pc = EntityCollectController.this.pointerCollection;
			if (pc != null) {
				final JPopupMenu menu = new JPopupMenu();
				final JComponent cPointOn = getPointOnComponent();

				if (pc.getMainPointer().message != null) {
					final String itemLabel = getSpringLocaleDelegate().getMessage("EntityCollectController.2", "Hinweis");
					final JMenuItem mainItem = new JMenuItem(itemLabel, NuclosToolBarActions.INFO.getIcon());
					mainItem.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							showPointerBubble(cPointOn, pc.getLocalizedMainPointer());
						}
					});
					menu.add(mainItem);
					menu.addSeparator();
				}

				final String fieldNotFoundLabel = getSpringLocaleDelegate().getMessage("EntityCollectController.3", "unbekanntes Attribut");
				final List<JMenuItem> menuItems = new ArrayList<JMenuItem>();
				for (final UID field : pc.getFields()) {
					FieldMeta<?> efMeta;
					try {
						efMeta = MetaProvider.getInstance().getEntityField(field);
					} catch (Exception ex) {
						efMeta = null;
					}
					final boolean fieldNotFound = efMeta == null;
					final String fieldLabel = fieldNotFound ? (field + " (" + fieldNotFoundLabel + ")")
							: getSpringLocaleDelegate().getLabelFromMetaFieldDataVO(efMeta);
					final JMenuItem fieldItem = new JMenuItem(fieldLabel);

					fieldItem.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							if (fieldNotFound) {
								showPointerBubble(cPointOn, getHtmlList(pc.getLocalizedFieldPointers(field)));
							} else {
								boolean fieldNotInLayout = true;
								for (final CollectableComponent clctcomp : getDetailCollectableComponentsFor(field)) {
									if (getDetailsPanel().ensureComponentIsVisible(clctcomp.getControlComponent())) {
										fieldNotInLayout = false;
										if (pc.hasFieldPointers(field))
											showPointerBubble(clctcomp.getControlComponent(), getHtmlList(pc.getLocalizedFieldPointers(field)));
										else {
											getDetailsPanel().spotComponent(clctcomp.getControlComponent());
										}
										clctcomp.getControlComponent().requestFocusInWindow();
										break;
									}
								}
								if (fieldNotInLayout) {
									showPointerBubble(cPointOn, getSpringLocaleDelegate().getMessage(
											"EntityCollectController.4", "Attribut nicht im Layout gefunden!") +
										"<br/>" + getHtmlList(pc.getLocalizedFieldPointers(field)));
								}
							}
						}
					});
					menuItems.add(fieldItem);
				}
				for (JMenuItem menuItem : CollectionUtils.sorted(menuItems, new Comparator<JMenuItem>() {
					@Override
					public int compare(JMenuItem o1, JMenuItem o2) {
						return o1.getText().compareToIgnoreCase(o2.getText());
					}})) {
					menu.add(menuItem);
				}
				menu.show(cPointOn, 0, cPointOn.getPreferredSize().height);
			}
		}

		@Override
		public void setEnabled(boolean b) {
			// ignore
		}

		@Override
		public boolean isEnabled() {
			return EntityCollectController.this.pointerCollection != null;
		}

	} // inner class PointerAction

	/**
	 *
	 * @param comp
	 * @param htmlMessage
	 */
	protected void showPointerBubble(final JComponent comp, final String htmlMessage) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					(new Bubble(comp, "<html>"+htmlMessage.replaceAll("\n", "<br/>")+"</html>", 8, BubbleUtils.Position.SE, 1500L)).setVisible(true);
					
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							if (FocusManager.getCurrentManager().getFocusOwner() == null
									|| UIUtils.findJComponent(getDetailsPanel(), FocusManager.getCurrentManager().getFocusOwner()) == null)
								setInitialComponentFocusInDetailsTab();
						}
					});
				} catch (IllegalComponentStateException e) {
					// do nothing. it is not shown
				}
			}
		});
	}

	/**
	 *
	 * @param pc
	 */
	protected void showPointers(final PointerCollection pc) {
		if (pc != null) {
			if (pc.getMainPointer().message != null) {
				JComponent cPointOn = getPointOnComponent();
				showPointerBubble(cPointOn, pc.getLocalizedMainPointer());
			}
			for (final UID field : pc.getFields()) {
				if (pc.hasFieldPointers(field)) {
					for (final CollectableComponent clctcomp : getDetailCollectableComponentsFor(field)) {
						if (getDetailsPanel().ensureComponentIsVisible(clctcomp.getControlComponent())) {
							showPointerBubble(clctcomp.getControlComponent(), getHtmlList(pc.getLocalizedFieldPointers(field)));
							break;
						}
					}
				}
			}
		}
	}

	/**
	 *
	 * @param lstMessages
	 * @return
	 */
	private String getHtmlList(List<String> lstMessages) {
		String htmlMessage = "";

		for (int i = 0; i < lstMessages.size(); i++) {
			String message = lstMessages.get(i);
			if (lstMessages.size() > 1)
				htmlMessage = htmlMessage + "- ";
			htmlMessage = htmlMessage + message;
			if (i < lstMessages.size()-1)
				htmlMessage = htmlMessage + "<br/>";
		}

		return htmlMessage;
	}

	/**
	 *
	 * @return
	 */
	protected Action getPointerAction() {
		if (pointerAction == null) {
			pointerAction = new PointerAction();
		}
		return pointerAction;
	}
	
	protected JComponent getPointOnComponent() {
		return getPointOnComponent(getCollectState().isDetailsMode());
	}
	
	protected JComponent getPointOnComponent(boolean details) {
		NuclosToolBar toolBar = null;
		if (details && getDetailsPanel() != null) {
			toolBar = getDetailsPanel().getToolBar();
		} else if (!details && getResultPanel() != null) {
			toolBar = getResultPanel().getToolBar();
		}
		if (toolBar != null) {
			for (Component c : toolBar.getComponents()) {
				if (c instanceof AbstractButton) {
					if (getPointerAction().equals(((AbstractButton) c).getAction())) {
						return (JComponent) c;
					}
				}
			}
		}
		return toolBar;
	}

	/**
	 *
	 * @return
	 */
	protected MouseListener getPointerContextListener() {
		return new PointerContextListener();
	}

	/**
	 *
	 * @return
	 */
	public PointerCollection getPointerCollection() {
		return pointerCollection;
	}

	/**
	 *
	 * @param pc
	 */
	public void setPointerInformation(PointerCollection pc, NuclosBusinessRuleException nbrex) {
		this.pointerCollection = pc;
		this.pointerException = nbrex;

		showPointers(pc);

		for (ActionListener al : this.lstPointerChangeListener) {
			al.actionPerformed(new ActionEvent(EntityCollectController.this, 0, "setPointerCollection"));
		}
	}

	/**
	 *
	 * @param al
	 */
	public void addPointerChangeListener(ActionListener al) {
		this.lstPointerChangeListener.add(al);
	}

	/**
	 *
	 * @param al
	 */
	public void removePointerChangeListener(ActionListener al) {
		this.lstPointerChangeListener.remove(al);
	}
	
	public boolean handleException(Exception ex) {
		if (handlePointerException(ex))
			return true;
		if (handleStaleVersionException(ex))
			return true;
		return false;
	}

	/**
	 * shows <code>NuclosBusinessRuleException</code>
	 *   and <code>PointerException</code>
	 *    in "Pointer-Style"...
	 * @param ex
	 * @return true if exception handled
	 */
	public boolean handlePointerException(Exception ex) {
		if (getCollectState().isDetailsMode() || getCollectState().isResultMode()) {
			
			final PointerException pex = PointerException.extractPointerExceptionIfAny(ex);
			final NuclosBusinessRuleException nbrex = NuclosBusinessRuleException.extractNuclosBusinessRuleExceptionIfAny(ex);
			if (pex != null) {
				final PointerCollection pc = pex.getPointerCollection();

				if (ex.getMessage() != null && pc.getMainPointer() == null) { // if pointer collection has no main information set exception message
					pc.setMainPointer(ex.getMessage());
				}
				if (ex instanceof NuclosBusinessRuleException) {
					setPointerInformation(pc, (NuclosBusinessRuleException) ex);
				} else {
					setPointerInformation(pc, pex);
				}
				LOG.error(ex);
				return true;
			} else if (nbrex != null) {
				final String exceptionMessage;
				final BusinessException be = nbrex.getCausingBusinessException();
				if (be != null) {
					exceptionMessage = be.getMessage();
				} else {
					exceptionMessage = NuclosExceptions.getReasonableMessage(nbrex);
				}				
				if (exceptionMessage != null) {
					final PointerCollection pc = new PointerCollection(Errors.formatErrorForBubble(exceptionMessage));
					setPointerInformation(pc, nbrex);
					LOG.error(ex);
					return true;
				}
			}
		}

		return false;
	}
	
	protected void handleAfterException() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {			
				try {
					for(final DetailsSubFormController<PK,?> subCtl :  getSubFormControllersInDetails()) {
						if(subCtl.getSubForm().getParentSubForm() != null)
							continue;
							
						int selectedRow = subCtl.getJTable().getSelectedRow();
						if(selectedRow >= 0) {
							subCtl.getJTable().getSelectionModel().clearSelection();
							subCtl.getJTable().getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
						}
					}
				}
				catch(Exception e) {
					LOG.error(e);
				}
			}
		});
	}

	@Override
	protected boolean handleSpecialException(Exception ex) {
		if (handleCommonValidationException(ex)) {
			handleAfterException();
			return true;
		} else if (handlePointerException(ex)) {
			handleAfterException();			
			return true;
		}
		else {
			return super.handleSpecialException(ex);
		}
	}

	protected boolean handleCommonValidationException(Exception ex) {
		CommonValidationException cve = NuclosExceptions.getCause(ex, CommonValidationException.class);
		if (cve != null) {
			final PointerCollection pointerCollection = new PointerCollection("");
			cve.getFullMessage(getSpringLocaleDelegate(), MetaProvider.getInstance(), getEntityUid(), pointerCollection);

			setPointerInformation(pointerCollection, null);
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected void handleCollectableValidationException(CollectableValidationException ex, String sMessage1) {
		PointerCollection pc = null;
		final String exceptionMessage = NuclosExceptions.getReasonableMessage(ex);

		if (exceptionMessage != null) {
			pc = new PointerCollection(Errors.formatErrorForBubble(exceptionMessage));
		}

		final CollectableEntityField clctefInvalid = ex.getCollectableEntityField();
		if (clctefInvalid != null) {
			if (pc == null) pc = new PointerCollection(sMessage1);
			pc.addEmptyFieldPointer(clctefInvalid.getUID());
		}

		if (pc != null) {
			setPointerInformation(pc, null);
			final Collection<CollectableComponent> collclctcomp = getDetailsPanel().getEditView().getCollectableComponentsFor(
					clctefInvalid.getUID());
			if (!collclctcomp.isEmpty()) {
				final CollectableComponent clctcomp = collclctcomp.iterator().next();
					clctcomp.getControlComponent().requestFocusInWindow();
			}
		} else {
			super.handleCollectableValidationException(ex, sMessage1);
		}
	}
	
	private static class GeneratorDummy extends GeneratorActionVO implements ToolBarItemAction.ToolBarItemDummy {

		public GeneratorDummy() {
			super(null, null, SpringLocaleDelegate.getInstance().getMessage("GenericObjectCollectController.9","Objektgenerator"), 
					null, null, null, null, null,
					null, null, null, null);
		}
		
	}

	protected void setupGeneratorActions(boolean bEnableButtons) {
		final List<GeneratorActionVO> generators = actGenerate.getGenerators();
		generators.clear();
		generators.add(new GeneratorDummy());
		for (GeneratorActionVO generatorAction : getGeneratorActions()) {
			if (!generatorAction.isRuleOnly()) {
				generators.add(generatorAction);		
			}
		}
		
//		cmbbxGeneratorActions.setVisible(lstActions.size() > 0);
		actGenerate.putValue(ToolBarItemAction.VISIBLE, generators.size() > 1);
		if (generators.size() > 0) {
			
//			cmbbxGeneratorActions.setModel(new DefaultComboBoxModel(lstActions.toArray()));
//			cmbbxGeneratorActions.setSelectedItem(null);
			Object[] items = new Object[generators.size()];
			for (int i = 0; i < generators.size(); i++) {
				items[i] = generators.get(i);
			}
			actGenerate.putValue(ToolBarItemAction.ACTION_DISABLED, Boolean.TRUE);
			actGenerate.putValue(ToolBarItemAction.ITEM_ARRAY, items);
			actGenerate.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, null);
			if (bEnableButtons) {
				actGenerate.putValue(ToolBarItemAction.ACTION_DISABLED, Boolean.FALSE);
			}
//			cmbbxGeneratorActions.setEnabled(bEnableButtons);
			actGenerate.setEnabled(bEnableButtons);

			// workaround for cellrenderer
//			int w = cmbbxGeneratorActions.getPreferredSize().width;
//			if(w < 100)
//				w = 100;
//			cmbbxGeneratorActions.setPreferredSize(new Dimension(w, cmbbxGeneratorActions.getPreferredSize().height));
//			UIUtils.setMaximumSizeToPreferredSize(cmbbxGeneratorActions);
			
//			for (ActionListener l : cmbbxGeneratorActions.getActionListeners()) {
//				cmbbxGeneratorActions.removeActionListener(l);
//			}
//			if (cmbbxGeneratorActions.isEnabled())
//				cmbbxGeneratorActions.addActionListener(new ActionListener() {
//					@Override
//					public void actionPerformed(ActionEvent ev) {
//						UIUtils.runCommandLater(getTab(), new CommonRunnable() {
//							@Override
//							public void run() {
//								if (cmbbxGeneratorActions.getSelectedItem() instanceof GeneratorActionVO)
//									try {
//										cmdGenerateObject((GeneratorActionVO) cmbbxGeneratorActions.getSelectedItem());
//									}
//								finally {
//									cmbbxGeneratorActions.setSelectedItem(null);
//								}
//							}
//						});
//					}
//				});
		}
	}

	/**
	 * custom rule execution command
	 * 
	 * @param executeCustomRuleActionVO {@link ExecuteCustomRuleActionVO}
	 */
	public void cmdExecuteCustomRule(ExecuteCustomRuleActionVO executeCustomRuleActionVO) {
		
		final List<EventSupportSourceVO> lstRules = new ArrayList<EventSupportSourceVO>();
		for (final EventSupportSourceVO vo : getUserRules()) {
			if (vo.getClassname().equals(executeCustomRuleActionVO.getClassName())) {
		
				lstRules.add(vo);
				break;
			}
		}
		
		if (lstRules.isEmpty()) {
			throw new NuclosFatalException("expected at least one rule");
		}
		
		// selected row count
		int iCount = getResultTable().getSelectedRowCount();
		
		new RunCustomRuleSelectedCollectablesController<PK,Clct>(this, lstRules, null).run(getMultiActionProgressPanel(iCount));
	}

	/**
	 * get custom rule action commands
	 * @return list of {@link ExecuteCustomRuleActionVO}
	 */
	public List<ExecuteCustomRuleActionVO> getExecuteCustomRuleActions() {
		final List<ExecuteCustomRuleActionVO> lstResult = new ArrayList<ExecuteCustomRuleActionVO>();
		for (final EventSupportSourceVO userRule : getUserRules()) {
			final ExecuteCustomRuleActionVO ruleActionVO = new ExecuteCustomRuleActionVO(userRule.getName(), userRule.getName(), userRule.getClassname());
			// avoid duplicates, getUserRules() returns same rule twice!!
			if (lstResult.contains(ruleActionVO)) {
				continue;
			} 
			lstResult.add(ruleActionVO);
		}
		
		return lstResult;
	}  

	protected void setupResultContextMenuGeneration() {
		final ResultPanel<PK, Clct> rp = getResultPanel();
		final JPopupMenu rowWidget = rp.getRowWidget();
		final JMenuItem generationWidget = rp.getGenerationWidget();
		rowWidget.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				try {
					final List<GeneratorActionVO> lstActions = getGeneratorActions();
					generationWidget.setVisible(!lstActions.isEmpty());
					for(final GeneratorActionVO actionVO : lstActions) {
						JMenuItem action = new JMenuItem(new AbstractAction(actionVO.toString()) {

							@Override
							public void actionPerformed(ActionEvent e) {
								cmdGenerateObject(actionVO);
							}

						});
						generationWidget.add(action);
					}
				}
				catch (Exception e1) {
					generationWidget.setVisible(false);
					LOG.warn("popupMenuWillBecomeVisible failed: " + e1 + ", setting it invisible");
				}
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				clearGeneratorMenu();
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				clearGeneratorMenu();
			}

			private void clearGeneratorMenu() {
				generationWidget.removeAll();
			}
		});
	}
	
	/**
	 * setup context menu content for custom rule execution (in resultlist)
	 */
	protected void setupResultContextExecuteMenuCustomRule() {
		final ResultPanel<PK,Clct> rp = getResultPanel();
		final JPopupMenu rowWidget = rp.getRowWidget();
		final JMenuItem customRuleWidget = rp.getCustomRulesWidget();
		rowWidget.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				try {
					final List<ExecuteCustomRuleActionVO> lstActions = getExecuteCustomRuleActions();
					// hide menu entry if no rule was found
					customRuleWidget.setVisible(!lstActions.isEmpty());
					for(final ExecuteCustomRuleActionVO actionVO : lstActions) {
						JMenuItem action = new JMenuItem(new AbstractAction(actionVO.toString()) {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								cmdExecuteCustomRule(actionVO);
							}
							
						});
						customRuleWidget.add(action);
					}
					
				}
				catch (Exception e1) {
					customRuleWidget.setVisible(false);
					LOG.warn("popupMenuWillBecomeVisible failed: " + e1 + ", setting it invisible");
				}
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				clearMenu();
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				clearMenu();
			}

			private void clearMenu() {
				customRuleWidget.removeAll();
			}
		});
	}
	
	private class ResultActionsWorker extends SwingWorker<List<ResultActionCollection>, Object> {

		final Collection<Clct> selectedCollectablesFromResult;
		
		boolean cancelWorker = false;
		
		public ResultActionsWorker(Collection<Clct> selectedCollectablesFromResult) {
			if (runningResultActionsWorker != null) {
				try {
					runningResultActionsWorker.cancelWorker();
				} catch (Exception ex) {
					//ignore
				}
			}
			runningResultActionsWorker = this;
			this.selectedCollectablesFromResult = selectedCollectablesFromResult;
		}
		
		@Override
		protected List<ResultActionCollection> doInBackground() throws Exception {
			try {
				// looks better ;-)
				Thread.sleep(500);
				return getResultActionsMultiThreaded(selectedCollectablesFromResult);
			}
			catch (Exception ex) {
				// it's okay here?!
			}
			return null;
 		}
		
		public void cancelWorker() {
			cancelWorker = true;
			cancel(true);
		}
		
		@Override
		protected void done() {
			try {
				List<ResultActionCollection> result = get();
				if (!cancelWorker) {
					runningResultActionsWorker = null;
					getResultPanel().setResultActions(result);
				}
			} catch (Exception e) {
				if (!cancelWorker) {
					if (getResultPanel() != null)
						getResultPanel().setResultActions(null);
				}
			} 
		}
	}

	@Override
	public String getCustomUsage() {
		return customUsage;
	}	
	
	/**
	 *
	 * @param subFormEntityUid
	 * @param t
	 * @return count imported | count not imported
	 * @throws NuclosBusinessException
	 */
   public int[] dropOnSubForm(UID subFormEntityUid, Transferable t) throws NuclosBusinessException {

		boolean subFormFound = false;
		int[] result = new int[]{0,0};

		try {
			for (DetailsSubFormController<?,?> subFormCtrl : getSubFormControllersInDetails()) {
				if (subFormCtrl instanceof MasterDataSubFormController
					&& subFormCtrl.getEntityAndForeignKeyField().getEntity().equals(subFormEntityUid)) {
					subFormFound = true;

					if (!subFormCtrl.getSubForm().isEnabled()) {
						throw new NuclosBusinessException(
								getSpringLocaleDelegate().getMessage("EntityCollectController.102", "Unterformular ist nicht aktiv."));
					}

					String entityLabel = null;
					boolean noReferenceFound = false;
					if (t.isDataFlavorSupported(TransferableGenericObjects.dataFlavor)) {
						final List<?> lstloim = (List<?>) t.getTransferData(TransferableGenericObjects.dataFlavor);
		                for (Object o : lstloim) {
		                	if (o instanceof GenericObjectIdModuleProcess) {
		                		GenericObjectIdModuleProcess goimp = (GenericObjectIdModuleProcess) o;
		                		UID entityUid = goimp.getModuleUid();
		                		entityLabel = getSpringLocaleDelegate().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(
		                				entityUid));	
		                        try {
		                        	if (!subFormCtrl.insertNewRowWithReference(
		                        		entityUid, Utils.getCollectable(entityUid, goimp.getGenericObjectId()),
		                        		true)) {
		                        		result[1] = result[1]+1;
			                		} else {
			                			result[0] = result[0]+1;
			                		}
		                        }
		                        catch(NuclosBusinessException e) {
		        					LOG.error("dropOnSubForm failed: " + e, e);
		                        	noReferenceFound = true;
		                        }
		                        catch(CommonBusinessException e) {
		        					LOG.error("dropOnSubForm failed: " + e, e);
		                        }
		                	}
		                }
					} else if (t.isDataFlavorSupported(TransferableMasterDatas.dataFlavor)) {
						final List<?> lstloim = (List<?>) t.getTransferData(TransferableMasterDatas.dataFlavor);
		                for (Object o : lstloim) {
		                	if (o instanceof MasterDataIdAndEntity) {
		                		MasterDataIdAndEntity<?> mdiae = (MasterDataIdAndEntity<?>) o;
		                		UID entityUid = mdiae.getEntityUid();
		                		entityLabel = getSpringLocaleDelegate().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(
		                				mdiae.getEntityUid()));
	
		                        try {
		                        	if (!subFormCtrl.insertNewRowWithReference(
		                        		entityUid, Utils.getCollectable(entityUid, mdiae.getId()),
		                        		true)) {
		                        		result[1] = result[1]+1;
			                		} else {
			                			result[0] = result[0]+1;
			                		}
		                        }
		                        catch(NuclosBusinessException e) {
		        					LOG.error("dropOnSubForm failed: " + e, e);
		                        	noReferenceFound = true;
		                        }
		                        catch(CommonBusinessException e) {
		        					LOG.error("dropOnSubForm failed: " + e, e);
		                        }
		                	}
		                }
					}
					
	                if (noReferenceFound) {
	                	throw new NuclosBusinessException(
	                			getSpringLocaleDelegate().getMessage(
	                					"EntityCollectController.104", "Dieses Unterformular enthält keine Referenzspalte zur Entität {0}.", entityLabel));
	                }
				}
			}
		} catch(UnsupportedFlavorException e) {
			LOG.error("dropOnSubForm failed: " + e, e);
	    } catch(IOException e) {
			LOG.error("dropOnSubForm failed: " + e, e);
	    }

		if (!subFormFound) {
			throw new NuclosBusinessException(getSpringLocaleDelegate().getMessage(
					"EntityCollectController.103", "Unterformular ist nicht im Layout vorhanden."));
		}
		return result;
	}
   	
    protected void setInitialComponentFocusInSearchTab() {
    	// implement in subclasses if needed.
    }
   
  	protected void setInitialComponentFocusInDetailsTab() {
  		// implement in subclasses if needed.
  	}
  	
    /**
     * change the customUsage
     * @param customUsage
     */
	protected final void setCustomUsage(String customUsage) {
		this.customUsage = customUsage;
	}
	
	private Date dateHistorical;

	public final boolean isHistoricalView() {
		return dateHistorical != null;
	}

	protected void setHistoricalDate(Date dateHistorical) {
		this.dateHistorical = dateHistorical;
	}

	protected final Date getHistoricalDate() {
		return dateHistorical;
	}
	
	//@todo eliminate parameter boolean bUseNewHistory
	public void runViewSingleHistoricalCollectable(CollectableWithDependants<PK> clct, Date dateHistorical) {
		setHistoricalDate(dateHistorical);
		runViewSingleHistoricalCollectable(clct, dateHistorical, false);
	}
	public void runViewSingleHistoricalCollectable(CollectableWithDependants<PK> clct, Date dateHistorical, boolean bUseNewHistory) {
		setHistoricalDate(dateHistorical);
	}
	
	protected static final Color colorHistoricalNotTracked = Utils.translateColorFromParameter(ParameterProvider.KEY_HISTORICAL_STATE_NOT_TRACKED_COLOR);
	protected static final Color colorHistoricalChanged = Utils.translateColorFromParameter(ParameterProvider.KEY_HISTORICAL_STATE_CHANGED_COLOR);
	
	public static <PK2,Clct2 extends Collectable<PK2>> void markSubformInHistoricalView(AbstractDetailsSubFormController<PK2,Clct2> subformctl,
			CollectableWithDependants<PK2> clct, CollectableWithDependants<PK2> clctlowdCurrent) throws NuclosBusinessException {

		final UID entityUid = subformctl.getCollectableEntity().getUID();
		
		boolean hasLoggingFields = false;
		for(FieldMeta<?> fieldMeta :
				MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUid).values())
			hasLoggingFields |= fieldMeta.isLogBookTracking();

		if(!hasLoggingFields) {
			// if no field of the entity is logged, mark subform table as not logged
			subformctl.getSubForm().getJTable().setBackground(colorHistoricalChanged);
			subformctl.getSubForm().getJTable().getParent().setBackground(colorHistoricalChanged);
		} else {
			// final UID entityUid = subformctl.getCollectableEntity().getUID();
			UID foreignKeyFieldUID = subformctl.getForeignKeyFieldUID();
			IDependentKey dependentKey = DependentDataMap.createDependentKey(foreignKeyFieldUID);
			if (clct.getDependants(dependentKey).size() != clctlowdCurrent.getDependants(dependentKey).size()) {
				subformctl.getSubForm().getJTable().setBackground(colorHistoricalChanged);
				subformctl.getSubForm().getJTable().getParent().setBackground(colorHistoricalChanged);
			} else {
//				for (Collectable<PK2> clctDependant : clct.getDependants(entityUid)) {
//					CollectableEntityObject<PK2> clctEO = (CollectableEntityObject<PK2>) clctDependant;
//					if (clctEO.getEntityObjectVO().isFlagNew() || clctEO.getEntityObjectVO().isFlagUpdated()
//							|| clctEO.getEntityObjectVO().isFlagRemoved()) {
//						// if there is any entry in the logbook for this entity done
//						// after the date of the historical view, mark the subform
//						// table as changed
//						subformctl.getSubForm().getJTable().setBackground(colorHistoricalChanged);
//						subformctl.getSubForm().getJTable().getParent().setBackground(colorHistoricalChanged);
//						break;
//					}
//				}
			}
		}
	}

	public static <PK2> void markFieldInHistoricalView(LayoutRoot layoutrootDetails, CollectableWithDependants<PK2> clctlowdCurrent, 
			UID fieldUid, CollectableField clctfShown) {
		if (clctlowdCurrent != null) {
			final CollectableField clctf = clctlowdCurrent.getField(fieldUid);
			if (!clctfShown.equals(clctf)) {
				final Collection<CollectableComponent> collclctcomp = layoutrootDetails.getCollectableComponentsFor(fieldUid);
				if (!collclctcomp.isEmpty()) {
					final CollectableComponent clctcomp = collclctcomp.iterator().next();
					final JComponent compFocussable = clctcomp.getFocusableComponent();
					String initialToolTip = (String) compFocussable.getClientProperty("initialToolTip");
					if (initialToolTip == null) {
						initialToolTip = StringUtils.emptyIfNull(compFocussable.getToolTipText());
						compFocussable.putClientProperty("initialToolTip", initialToolTip);
					}
					if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
						LabeledCollectableComponentWithVLP clctcmbx = (LabeledCollectableComponentWithVLP) clctcomp;
						clctcmbx.setBackgroundColor(colorHistoricalChanged);
					} else {
						clctcomp.getJComponent().setBackground(colorHistoricalChanged);						
					}
					final String sToolTip = SpringLocaleDelegate.getInstance().getMessage("GenericObjectCollectController.4","{0} [Ge\u00e4ndert; aktueller Wert: \"{1}\"]", initialToolTip, clctf.toString());
					compFocussable.setToolTipText(sToolTip);
					clctcomp.getJComponent().setToolTipText(sToolTip);
					// todo: mark fields which are not tracked in logbook?
				}
			}
		}
	}
	
	@Override
	public void askAndSaveIfNecessary(ResultListener<AskAndSaveResult> rl) {
		if (isHistoricalView())
			rl.done(AskAndSaveResult.NOT_SAVED);
		else
			super.askAndSaveIfNecessary(rl);
	}
	
	protected UID getMatrixPreferencesField() {
		UID sField = null;
		for(UID sMatrix : mpOfMatrixController.keySet()) {
			MatrixController mc = mpOfMatrixController.get(sMatrix);
			if(sField == null) {
				sField = mc.getMatrixPreferencesField();
			}
		}
		return sField;
	}
	
	protected String getMatrixPreferences() {
		String sPrefs = null;
		for(UID sMatrix : mpOfMatrixController.keySet()) {
			MatrixController mc = mpOfMatrixController.get(sMatrix);
			sPrefs = mc.getMatrixPreferences();
		}
		
		return sPrefs;
	}
	
	
	
	protected IDependentDataMap getAllMatrixData(IDependentDataMap mpmdvoDependants) {		

		for(UID matrix : mpOfMatrixController.keySet()) {
			MatrixController mc = mpOfMatrixController.get(matrix);	
			mc.stopEditing();
			IDependentDataMap mpData = mc.getAllData(null);
			for(IDependentKey key : mpData.getKeySet()) {
				mpmdvoDependants.<Object>addAllData(key, mpData.getDataPk(key));
			}
		}
		
		return mpmdvoDependants;
	}

	protected final void clearRemovedMatrixData() {
		for (MatrixController mc : mpOfMatrixController.values()) {
			mc.clearRemovedObjects();
		}
	}
	
	@Override
	public boolean save() throws CommonBusinessException {
		addCollectableEventListener(new CollectableEventListener<PK>() {
			@Override
			public void handleCollectableEvent(Collectable<PK> collectable, MessageType messageType) {
				switch(messageType) {
				case EDIT_DONE:
					// close frame on sucessfull save.
					if (isHistoricalView()) {
						setHistoricalDate(null);
						getTab().close();
					}
					break;
				default:
					break;
				}
				removeCollectableEventListener(this);
			}
		});
//		Collection<? extends SubFormController> cltSfControllers = this.getSubFormControllers(false);
//		for (SubFormController sfCtl : cltSfControllers) {
//			if (sfCtl instanceof MasterDataSubFormController<?>) {
//				((MasterDataSubFormController<PK>)sfCtl).saveSelectedRows();
//			}
//		}
		saveAndRestoreSubformState();
		final boolean result = super.save();
		return result;
	}
	
	@Override
	protected boolean isRefreshSelectedCollectableAllowed() {
		return !isHistoricalView();
	}

	protected final void cmdShowHistory() {
		if (getCollectState().isDetailsMode() && getCollectState().getInnerState() == CollectState.DETAILSMODE_VIEW) {
			UIUtils.runCommand(getTab(), new Runnable() {
				@Override
				public void run() {
					final Collectable<PK> clctSelected = getResultController().getSelectedCollectableFromTableModel();
	
					assert clctSelected != null;
	
					try {
						/** @todo frame vs. parent */
						final HistoryController ctlr = new HistoryController(getTab(), getEntityUid(),
								clctSelected.getId(), getPreferences(), 
								SpringLocaleDelegate.getInstance().getIdentifierLabel(
										clctSelected, clctSelected.getEntityUID(), MetaProvider.getInstance(),
										getLanguage()));
						
						ctlr.getMainFrameTab().addMainFrameTabListener(new MainFrameTabAdapter() {
							@Override
							public void tabClosing(MainFrameTab tab, ResultListener<Boolean> rl) {
								super.tabClosing(tab, rl);
								
								try {
									refreshCurrentCollectable();
								}
								catch(CommonBusinessException e) {
									LOG.error("refresh failed: " + e, e);
								}
							}
							@Override
							public void tabClosed(MainFrameTab tab) {
								tab.removeMainFrameTabListener(this);
							}
						});
						ctlr.run();
					}
					catch (CommonBusinessException ex) {
						Errors.getInstance().showExceptionDialog(getTab(), ex);
					}
				}
			});
		}
	}
	
	protected void notifyLayoutComponentListeners(LayoutRoot layoutRoot, boolean isSingle) {
		final Collection<LayoutComponent<PK>> colComponents = layoutRoot.getLayoutComponents();
		LOG.debug("Found " + colComponents.size() + " layout component(s)");
		for (final LayoutComponent<PK> lmc : colComponents) {
			final Collection<LayoutComponentListener<PK>> colListeners = lmc.getLayoutComponentListeners();
			final Object[] listeners = colListeners.toArray();
			for (int i = listeners.length - 1 ; i >= 0 ; i--) {
				LayoutComponentListener<PK> listener = (LayoutComponentListener<PK>) listeners[i];
				if (isSingle) {
					if (this.getCollectStateModel().getCollectState().equals(new CollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_NEW))) {
						listener.newEntered(new NewContext() {});
					} else if (this.getCollectStateModel().getCollectState().equals(new CollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_VIEW))
							|| this.getCollectStateModel().getCollectState().equals(new CollectState(CollectState.OUTERSTATE_RESULT, CollectState.DETAILSMODE_EDIT))) {
						listener.singleViewEntered(new SingleContext<PK>() {
							
							@Override
							public PK getObjectId() {
								return getSelectedCollectableId();
							}
						});
					}
				} else {
					// open record (multi edit)
					final Collection<PK> lstIds = new ArrayList<PK>();
					for (final Collectable<PK> c : getSelectedCollectables()) {
						lstIds.add(c.getId());
					}
					if (this.getCollectStateModel().getCollectState().equals(new CollectState(CollectState.DETAILSMODE_EDIT, CollectState.DETAILSMODE_MULTIVIEW))) {
						listener.multiViewEntered(new MultiContext<PK>() {
							
							@Override
							public Collection<PK> getAllObjectIds() {
								return lstIds;
							}
						});
					}
					// TODO
				}
			}
		}
	}
	
	protected void setupDropListener(final JTable tbl) {
		this.lstDropHandlers = new ArrayList<org.nuclos.api.ui.dnd.DropHandler<PK>>();
		for (final DropHandlerProvider<PK> dhp : nucletComponentRepository.getDropHandlerProvider()) {
			for (final DropHandler<PK> dh : dhp.getDropHandler(DropTarget.RESULTLIST)) {
				if (dh.install(new InstallContext() {

					@Override
					public UID getEntityUid() {
						return getEntityUid();
					}

					@Override
					public DropTarget getDropTarget() {
						return DropTarget.RESULTLIST;
					}
				})) {
					lstDropHandlers.add(dh);
				}
			}
		}
		
		java.awt.dnd.DropTarget dt = new java.awt.dnd.DropTarget(tbl, new NuclosDropTargetListener(new NuclosDropTargetVisitor() {
			
			private DropContext<PK> createNewDropContext(final Object data, final Point location) {
				return new DropContext<PK>() {
					
					@Override
					public PK getTargetObjectId() {
						PK result = null;
						int row = getResultTable().rowAtPoint(location);
						if (row >= 0) {
							final AbstractCollectable<PK> mdwd = (AbstractCollectable<PK>) getResultTableModel().getRow(row);
							result = mdwd.getId();
						}
						return result;	
					}

					@Override
					public Object getData() {
						return data;
					}

				};
			}
			@Override
			public void visitDropActionChanged(DropTargetDragEvent dtde) {}
			
			@Override
			public void visitDrop(final DropTargetDropEvent dtde) {
				boolean result = false;
				for (final org.nuclos.api.ui.dnd.DropHandler<PK> dh : lstDropHandlers) {
					
					DataFlavor dfSupported = null;
					for (final Flavor flavor : dh.supportedFlavors()) {
						DataFlavor df = new DataFlavor(flavor.getRepresentationClass(), flavor.getMimeType());
						if (dtde.isDataFlavorSupported(df)) {
							dfSupported = df;
							break;
						}
					}
					if (null == dfSupported) {
						dtde.rejectDrop();
						break;
					}
					final DataFlavor df = dfSupported;

					dtde.acceptDrop(DnDConstants.ACTION_COPY);

					try {
						final Object data = dtde.getTransferable().getTransferData(df);

						// FIXME use SwingWorker
						result = dh.handle(createNewDropContext(data, dtde.getLocation()));
						// FIXME only the first handler is executed
						break;
					} catch (final UnsupportedFlavorException ex) {
						throw new NuclosFatalException(ex);
					} catch (final IOException ex) {
						throw new NuclosFatalException(ex);
					}
				}
				dtde.getDropTargetContext().dropComplete(result);
			}
			
			@Override
			public void visitDragOver(final DropTargetDragEvent dtde) {
				boolean result = false;
				
				int row = getResultTable().rowAtPoint(dtde.getLocation());
				getResultTable().getSelectionModel().setSelectionInterval(row, row);
				for (final org.nuclos.api.ui.dnd.DropHandler<PK> dh : lstDropHandlers) {
					
					DataFlavor dfSupported = null;
					for (final Flavor flavor : dh.supportedFlavors()) {
						DataFlavor df = new DataFlavor(flavor.getRepresentationClass(), flavor.getMimeType());
						if (dtde.isDataFlavorSupported(df)) {
							dfSupported = df;
							break;
						}
					}
					if (null == dfSupported) {
						dtde.rejectDrag();
						break;
					}
					final DataFlavor df = dfSupported;
					
					
					try {
						final Object data = dtde.getTransferable().getTransferData(df);

						result = dh.isAllowed(createNewDropContext(data, dtde.getLocation()));
						break;
					} catch (final UnsupportedFlavorException ex) {
						throw new NuclosFatalException(ex);
					} catch (final IOException ex) {
						throw new NuclosFatalException(ex);
					}
				}
				if (!result) {
					dtde.rejectDrag();
				}
			}
			
			@Override
			public void visitDragExit(DropTargetEvent dte) {}
			
			@Override
			public void visitDragEnter(DropTargetDragEvent dtde) {}
		}));
		dt.setActive(true);
	}
	
	protected void errorDuringInit(RuntimeException re) {
		MainFrameTab tab = getTab();
		if (tab != null) tab.errorWhileOpening();
		throw re;
	}
	
	public CollectableField getMandator() {
		return mandator;
	}

	public void setMandator(CollectableField mandator) {
		this.mandator = mandator;
		showMandator(mandator);
	}
	
	protected void showMandator(Clct clct) {
		EntityMeta<?> em = MetaProvider.getInstance().getEntity(entityUid);
		if (em.isMandator()) {
			CollectableField mandator = clct.getField(SF.MANDATOR.getUID(entityUid));
			showMandator(mandator);
		}
	}
	
	protected void showMandator(CollectableField mandator) {
		EntityMeta<?> em = MetaProvider.getInstance().getEntity(entityUid);
		if (em.isMandator()) {
			String sMandator = null;
			if (mandator != null) {
				 sMandator = (String) mandator.getValue();
			}
			String sLevel = SpringLocaleDelegate.getInstance().getText(SF.MANDATOR.getMetaData(this.entityUid).getLocaleResourceIdForLabel());
			getDetailsPanel().setStatusBarText2(sLevel + ": " + sMandator, " - ");
			
			Color color = null;
			if (mandator != null) {
				try {
					String sRes = SecurityCache.getInstance().getMandatorById((UID) mandator.getValueId()).getColor();
					if (sRes != null) {
						color = Color.decode(sRes);
					}
				} catch (Exception ex) {
					LOG.error(ex.getMessage(), ex);
				}
			} 
			getDetailsPanel().getToolBar().setGradientColor(color);
		}
	}

	@Override
	protected void clearSearchFields() {
		super.clearSearchFields();
		setMandatorSearchCondition();
		CollectableListOfValues<UID> owner = searchOwnerClctLOV;
		if (owner != null) {
			owner.clear();
		}
	}

	private void setMandatorSearchCondition() {
		if (getMandator() != null && isSearchPanelAvailable()) {
			CollectableComponentModel m = getSearchEditView().getModel().getCollectableComponentModelFor(SF.MANDATOR.getUID(this.entityUid));
			if (m != null) {
				m.setField(new CollectableValueIdField(getMandator().getValueId(), getMandator().getValue()));
			}
		}
	}
	
	@Override
	protected Clct newCollectableWithDefaultValues(boolean forInsert) {
		Clct result = super.newCollectableWithDefaultValues(forInsert);
		EntityMeta<?> em = MetaProvider.getInstance().getEntity(entityUid);
		if (em.isMandator()) {
			if (forInsert) {
				result.setField(SF.MANDATOR.getUID(em), getMandator());
			} else {
				List<MandatorVO> accessibleMandators = SecurityCache.getInstance().getMandatorsByLevel(em.getMandatorLevel());
				final Mutable<MandatorVO> selected = new Mutable<MandatorVO>();
				if (accessibleMandators.isEmpty()) {
					throw new NuclosFatalException("Accessible mandators must not be empty!");
				}
				if (accessibleMandators.size() == 1) {
					// only one mandator is accessible, select it...
					selected.setValue(accessibleMandators.get(0));
				} else {
					// show dialog...
					final JPanel optionPanel = new JPanel();
					optionPanel.setLayout(new BoxLayout(optionPanel, BoxLayout.Y_AXIS));
					optionPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
					JScrollPane scroller = new JScrollPane(optionPanel);
					scroller.setBorder(BorderFactory.createEmptyBorder());
					scroller.setPreferredSize(new Dimension(300, 150));
					Frame parentFrame = null;
					try {
						parentFrame = UIUtils.getFrameForComponent(MainFrame.getTabbedPane(getTab()).getComponentPanel());
					} catch (Exception ex) {
						// maybe not yet visible
						parentFrame = MainFrame.getFrames()[0];
					}
					final JDialog dialog = new JDialog(parentFrame, SpringLocaleDelegate.getInstance().getText("mandator"), true);
					dialog.add(scroller);
					
					ButtonGroup group = new ButtonGroup();
					for (final MandatorVO vo : accessibleMandators) {
						final JRadioButton button = new JRadioButton(new AbstractAction(vo.toString()) {
							@Override
							public void actionPerformed(ActionEvent e) {
								dialog.setVisible(false);
								selected.setValue(vo);
							}
						});
						button.addKeyListener(new KeyAdapter() {
							@Override
							public void keyReleased(KeyEvent e) {
								if (e.getKeyCode() == KeyEvent.VK_DOWN) {
									button.transferFocus();
								} else if (e.getKeyCode() == KeyEvent.VK_UP) {
									button.transferFocusBackward();
								} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
									button.doClick();
								} 
							}
						});
						group.add(button);
						optionPanel.add(button);
					}
					
					dialog.pack();
					UIUtils.center(dialog, parentFrame, true);
					dialog.setVisible(true);
					
					if (selected.getValue() == null) {
						selected.setValue(accessibleMandators.get(0));
					}
				}
				setMandator(new CollectableValueIdField(selected.getValue().getUID(), selected.getValue().toString()));
				result.setField(SF.MANDATOR.getUID(em), getMandator());
			}
		}
		return result;
	}
	
	@Override
	protected boolean isNewAllowed() {
		boolean result = super.isNewAllowed();
		if (result) {
			EntityMeta<?> em = MetaProvider.getInstance().getEntity(entityUid);
			if (em.isMandator()) {
				List<MandatorVO> accessibleMandators = SecurityCache.getInstance().getMandatorsByLevel(em.getMandatorLevel());
				result = !accessibleMandators.isEmpty();
			}
		}
		return result;
	}
	
	@Override
	protected void unsafeFillDetailsPanel(Clct clct) throws CommonBusinessException {
		super.unsafeFillDetailsPanel(clct);
		showMandator(clct);
		updateLockState(clct);
	}
	
	@Override
	protected void setupSearchToolBar() {
		super.setupSearchToolBar();

		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(getEntityUid());
		if (eMeta.isOwner()) {
			CollectableListOfValues<UID> clctSearchOwner = getSearchOwnerLOV();
			if (clctSearchOwner != null) {
				final LabeledComponent lovSearchOwner = clctSearchOwner.getLabeledComponent();
				lovSearchOwner.putClientProperty(ToolBarItem.COMPONENT_PROPERTY_KEY, NuclosToolBarItems.SEARCH_OWNER);
				this.getSearchPanel().addToolBarComponent(lovSearchOwner);
			}
		}
	}

	protected boolean isFillDuringClone() {
		return bIsFillDuringClone;
	}

	protected void setSplitDetailController(EntityCollectController<PK,Clct> detailController) {
		this.initOnlyDetailTab = detailController.getTab();
		if (initOnlyDetailTab == null) {
			throw new IllegalArgumentException("detailTab must not be null");
		}
		this.initOnlyDetailController = detailController;
	}
	
	protected EntityCollectController<PK, Clct> getSplitDetailController() {
		return detailController == null ? null : detailController.get();
	}

	protected MainFrameTab getSplitDetailTab() {
		return detailTab == null ? null : detailTab.get();
	}
	
	protected MainFrameTab getSplitMasterTab() {
		return masterTab;
	}

	protected void initSplitView() {
		detailController = new WeakReference<EntityCollectController<PK,Clct>>(initOnlyDetailController);
		detailTab = new WeakReference<MainFrameTab>(initOnlyDetailTab);
		masterTab = new MainFrameTab();
		SplitViewSettings settings = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Split_View_Settings, getEntityUid());
		getCollectPanel().addExternalPanelForSplitting(masterTab, getSplitDetailTab(), settings);
		
		getSplitDetailController().setCmdSwitchSplitViewMode(new Runnable() {
			@Override
			public void run() {
				switchSplitViewMode();
			}
		});
		getSplitDetailController().setCmdUpdateSplitViewSettings(new UpdateSplitViewSettingsRunnable() {
			@Override
			public void run(LafParameterStorage storage) {
				updateSplitViewSettings(storage);
			}
		});
		
		getResultTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting()) return;
				if (isClosing || isClosed()) return;
				if (isSynchronizingFromSplitDetails) return;
				cmdViewSelectedCollectables();
			}
		});
		
		splitDetailCollectStateListener = new CollectStateAdapter() {
			private LayerLock lockobject;
			@Override
			public void detailsModeLeft(CollectStateEvent ev) throws CommonBusinessException {
				if (ev.getOldCollectState().getInnerState() == CollectState.DETAILSMODE_EDIT ||
					ev.getOldCollectState().getInnerState() == CollectState.DETAILSMODE_MULTIEDIT ||
					ev.getOldCollectState().getInnerState() == CollectState.DETAILSMODE_NEW_CHANGED) {
					masterTab.unlockLayer(lockobject);
//					System.out.println("UN-lock master");	
				}
			}
			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				if (ev.getNewCollectState().getInnerState() == CollectState.DETAILSMODE_EDIT ||
					ev.getNewCollectState().getInnerState() == CollectState.DETAILSMODE_MULTIEDIT ||
					ev.getNewCollectState().getInnerState() == CollectState.DETAILSMODE_NEW_CHANGED) {
					if (lockobject != null) {
						masterTab.unlockLayer(lockobject);
					}
					lockobject = masterTab.lockLayer();
//					System.out.println("lock master");
				}
			}
		};
		splitDetailCollectableEventListener = new CollectableEventListener<PK>() {
			@Override
			public void handleCollectableEvent(Collectable<PK> clct,
					org.nuclos.client.ui.collect.CollectController.MessageType messageType) {
				if (isSynchronizingToSplitDetails) {
					return;
				}
				isSynchronizingFromSplitDetails = true;
				try {
					synchronized (getSplitDetailController()) {
						if (isSynchronizingToSplitDetails) {
//							System.out.println("isSynchronizingToDetails");
							return;
						}
						
						int row = -1;
						if (clct != null) {
							for (int i = 0; i < getResultTableModel().getRowCount(); i++) {
								if (LangUtils.equals(getResultTableModel().getRow(i).getPrimaryKey(), clct.getPrimaryKey())) {
									row = i;
									break;
								}
							}
						}
						switch (messageType) {
						case NEW_DONE:
							refreshOnClose = true;
							selectAfterFillResult = clct.getPrimaryKey();
							getResultController().getSearchResultStrategy().cmdSearch(true);
							break;
						case EDIT_DONE:
						case STATECHANGE_DONE:
						case REFRESH_DONE_DIRECTLY:
						case REFRESH_DONE:
							refreshOnClose = true;
							if (row > -1) {
								getResultTableModel().setCollectable(row, (Clct) clct);
							}
							break;
						case DELETE_DONE:
							refreshOnClose = true;
							if (row > -1) {
								getResultTableModel().remove(row);
							}
							break;
						case MULTIEDIT_DONE:
							refreshOnClose = true;
							try {
								refreshSelectedCollectablesInResult();
							} catch (CommonBusinessException e) {
								Errors.getInstance().showExceptionDialog(getTab(), e);
							}
							break;
						default:
						}
					}
				} finally {
					if (selectAfterFillResult == null) {
						isSynchronizingFromSplitDetails = false;
					}
				}
			}
		};
		
		getSplitDetailController().getCollectStateModel().addCollectStateListener(splitDetailCollectStateListener);
		getSplitDetailController().addCollectableEventListener(splitDetailCollectableEventListener);
		
		getSplitDetailTab().setVisible(false);
		initOnlyDetailController = null;
		initOnlyDetailTab = null;
	}
	
	@Override
	public void cmdViewSelectedCollectables() {
		if (getSplitDetailController() == null) {
			super.cmdViewSelectedCollectables();
			return;
		}
		
		isSynchronizingToSplitDetails = true;
		try {
			synchronized (getSplitDetailController()) {
				List<Clct> splitDetailCollectables = getSelectedCollectables();
				if (splitDetailCollectables.isEmpty()) {
					cmdEnterNewMode();
				} else {
					getSplitDetailController().setSelectedCollectables(splitDetailCollectables);
					getSplitDetailController().cmdViewSelectedCollectables();
				}
			}
		} finally {
			isSynchronizingToSplitDetails = false;
		}
		getSplitDetailTab().setVisible(true);
	}
	
	@Override
	protected void cmdEnterNewMode() {
		if (getSplitDetailController() == null) {
			super.cmdEnterNewMode();
			return;
		}
		
		synchronized (getSplitDetailController()) {
			getSplitDetailController().cmdEnterNewMode();
		}
	}

	@Override
	protected void cmdCloneSelectedCollectable() {
		if (getSplitDetailController() == null) {
			super.cmdCloneSelectedCollectable();
			return;
		}
		
		getSplitDetailController().cmdCloneSelectedCollectable();
		getSplitDetailTab().setVisible(true);
	}

	@Override
	public void runNew(final boolean selectTab) throws CommonBusinessException {
		if (getSplitDetailController() == null) {
			super.runNew(selectTab);
			return;
		}

		synchronized (getSplitDetailController()) {
			getSplitDetailController().runNew(selectTab);
		}
	}

	@Override
	public void runNewWith(final Clct clct) throws CommonBusinessException {
		if (getSplitDetailController() == null) {
			super.runNewWith(clct);
			return;
		}

		synchronized (getSplitDetailController()) {
			getSplitDetailController().runNewWith(clct);
		}
	}

	@Override
	public void runViewSingleCollectableWithId(final PK oId) throws CommonBusinessException {
		if (getSplitDetailController() == null) {
			super.runViewSingleCollectableWithId(oId);
			return;
		}

		synchronized (getSplitDetailController()) {
			getSplitDetailController().runViewSingleCollectableWithId(oId);
		}
	}

	@Override
	protected void enterNewMode() {
		super.enterNewMode();
		if (getViewMode() == ControllerPresentation.SPLIT_DETAIL) {
			getTab().setVisible(true);
		}
	}

	@Override
	protected void enterNewModeWithSearchValues() {
		super.enterNewModeWithSearchValues();
		if (getViewMode() == ControllerPresentation.SPLIT_DETAIL) {
			getTab().setVisible(true);
		}
	}

	@Override
	public void setParentRestriction(UID fieldToParent, Object parentId, String parentTitle) {
		super.setParentRestriction(fieldToParent, parentId, parentTitle);
		
		if (getSplitDetailController() != null) {
			getSplitDetailController().setParentRestriction(fieldToParent, parentId, parentTitle);
		}
	}
	
	@Override
	protected void selectAfterFillResult() {	
		if (getResultTable() == null) {
			return;	// NUCLOS-6853
		}
		final int rows = getResultTableModel().getRowCount();
	
		final boolean lafShowDetailsForSingleSearchResult = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Result_Show_Details_For_Single_Search_Result, entityUid);
		boolean viewDetailsOfSingleResult = rows == 1 && lafShowDetailsForSingleSearchResult;
		if (selectAfterFillResult != null) {
			try {
				for (int i = 0; i < rows; i++) {
					if (LangUtils.equals(getResultTableModel().getRow(i).getPrimaryKey(), selectAfterFillResult)) {
						getResultTable().getSelectionModel().setSelectionInterval(i, i);
						viewDetailsOfSingleResult = false;
						break;
					}
				}
			} finally {
				isSynchronizingFromSplitDetails = false;
				selectAfterFillResult = null;
			}
		
		}		

		if (viewDetailsOfSingleResult && !E.REPORTEXECUTION.checkEntityUID(this.getEntityUid()) && getForeignKeyMouseListenerForTableDoubleClick() ==null) {
			getResultTable().getSelectionModel().setSelectionInterval(0, 0);			
			cmdViewSelectedCollectables();			
		}			
	
	}
	
	private void setSelectedCollectables(List<Clct> selectedCollectables) {
		getResultTableModel().setCollectables(selectedCollectables);
		getResultTable().getSelectionModel().setSelectionInterval(0, selectedCollectables.size()-1);
	}
	
	@Override
	public void switchSplitViewMode() {
		if (getViewMode() == ControllerPresentation.SPLIT_RESULT) {
			getCollectPanel().switchSplitViewMode();
		} else if (getViewMode() == ControllerPresentation.SPLIT_DETAIL) {
			if (cmdSwitchSplitViewMode != null) {
				cmdSwitchSplitViewMode.run();
			}
		}
	}
	
	@Override
	public void updateSplitViewSettings(LafParameterStorage storage) {
		if (getViewMode() == ControllerPresentation.SPLIT_RESULT) {
			SplitViewSettings settings = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Split_View_Settings, getEntityUid(), storage, true);
			if (settings.isFixSize()) {
				// update divider location and horizontal state
				SplitViewSettings settingsFromView = getCollectPanel().getSplitViewSettings();
				settings.setFixSize(settingsFromView.getFixSize());
				settings.setHorizontal(settingsFromView.isHorizontal());
			} else {
				// reset to default
				settings = null;
			}
			LafParameterProvider.getInstance().setValue(LafParameter.nuclos_LAF_Split_View_Settings, getEntityUid(), storage, 
					LafParameter.nuclos_LAF_Split_View_Settings.convertToStore(settings));
			
			settings = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Split_View_Settings, getEntityUid(), true);
			getCollectPanel().setDividerResizing(settings);
		} else if (getViewMode() == ControllerPresentation.SPLIT_DETAIL) {
			if (cmdUpdateSplitViewSettings != null) {
				cmdUpdateSplitViewSettings.run(storage);
			}
		}
	}
	
	/**
	 * Currently only available for SplitView. Extend if needed.
	 */
	@Override
	public void refreshOnCloseIfNecessary(CollectController cc) {
		refreshOnCloseIfNecessary = new WeakReference<CollectController>(cc);
	}
	
	protected Runnable getCmdSwitchSplitViewMode() {
		return cmdSwitchSplitViewMode;
	}

	protected void setCmdSwitchSplitViewMode(Runnable cmdSwitchSplitViewMode) {
		this.cmdSwitchSplitViewMode = cmdSwitchSplitViewMode;
	}

	protected UpdateSplitViewSettingsRunnable getCmdUpdateSplitViewSettings() {
		return cmdUpdateSplitViewSettings;
	}

	protected void setCmdUpdateSplitViewSettings(UpdateSplitViewSettingsRunnable cmdUpdateSplitViewSettings) {
		this.cmdUpdateSplitViewSettings = cmdUpdateSplitViewSettings;
	}

	@Override
	protected boolean isNavigationAllowed() {
		if (getViewMode() == ControllerPresentation.SPLIT_DETAIL) {
			return false;
		}
		return super.isNavigationAllowed();
	}
	
	/**
	 * {@link UID} of current layout
	 * 
	 * @return uid
	 */
	public abstract UID getCurrentLayoutUid();
	
	
	protected void readSplitPaneStateFromPrefs(final UID uidLayout, final Preferences prefsParent, final Component rootComp) {
		if (null == uidLayout) {
			return;
		}
		
		LayoutPreferences lp = WorkspaceUtils.getInstance().getLayoutPreferences(WorkspaceUtils.getInstance().getWorkspace(), uidLayout);
		if (null != lp) {
				final Collection<JSplitPane> lstSplitPanes = UIUtils.findAllInstancesOf(rootComp, JSplitPane.class);
				for (final JSplitPane sp : lstSplitPanes) {
					final Split spp = WorkspaceUtils.getInstance().getSplitPanePreferences(lp, sp.getName());
					if (null == spp) {
						continue;
					}
					if (spp.getExpandedState() == Split.EXPANDED_STATE_NONE) {
						sp.setDividerLocation(spp.getPosition());
					} else if (spp.getExpandedState() == Split.EXPANDED_STATE_LEFT) {
						sp.getRightComponent().setMinimumSize(new Dimension());
						sp.setDividerLocation(1.0d);
					} else {
						sp.getLeftComponent().setMinimumSize(new Dimension());
						sp.setDividerLocation(0.0d);
					}
				}
		} else {
			UIUtils.readSplitPaneStateFromPrefs(getCurrentLayoutUid(), getPreferences(), getDetailsPanel());
		}
	}
	
	protected static void searchTabbedPanes(JComponent comp, List<JTabbedPane> lst) {
		if (comp instanceof JTabbedPane) {
			lst.add((JTabbedPane) comp);			
		}
		
		if (comp.getComponents().length == 0) {
			return;			
		}
		
		for (Component c : comp.getComponents()) {
			if (c instanceof JComponent) {
				searchTabbedPanes((JComponent) c, lst);				
			}
		}
	}
	
	protected List<CollectableEntityField> getSelectedFields() {
		// TODO: avoid unnecessary check.
		return CollectionUtils.typecheck(getFields().getSelectedFields(),
				CollectableEntityField.class);
	}
	
	/**
	 * Get the fixed part of the result table, if any.
	 */
	public JTable getFixedResultTable() {
		return null;
	}
	
	public List<Integer> getSelectedFieldsWidth() {
		List<Integer> lstWidth = new ArrayList<Integer>();
		
		JTable tblFixed = getFixedResultTable();
		JTable tblResult = getResultTable();
		
		UID stateIconUid = SF.STATEICON.getUID(this.getEntityUid());

		if (tblFixed != null) {
			final TableColumnModel columnmodelFixed = tblFixed.getTableHeader().getColumnModel();
			SortableCollectableTableModelImpl tm = (SortableCollectableTableModelImpl)tblFixed.getModel();

			// TODO: bei fixierten Spalten wird hier die Status-Spalte noch nicht korrekt berücksichtigt (diese wird beim Drucken ausgeblendet)
			// das hier liefert die Position im TableModel, aber nicht die Position in der Ansicht
			int stateColumn = tm.findColumnByFieldUid(stateIconUid);   
			for (int iColumn = 0; iColumn < columnmodelFixed.getColumnCount(); iColumn++) {
				int width = columnmodelFixed.getColumn(iColumn).getPreferredWidth();
				lstWidth.add(width);
			}
		}

		SortableCollectableTableModelImpl tm = (SortableCollectableTableModelImpl)tblResult.getModel();
		int stateColumn = tm.findColumnByFieldUid(stateIconUid);
		
		final TableColumnModel columnmodel = tblResult.getTableHeader().getColumnModel();
		for (int iColumn = 0; iColumn < columnmodel.getColumnCount(); iColumn++) {
			TableColumn tableColumn = columnmodel.getColumn(iColumn);
			int width = tableColumn.getPreferredWidth();
			if(iColumn != stateColumn) {
				lstWidth.add(width);
			}
		}
		
		return lstWidth;
	}
	
	/**
	 * invokes dialog for export of result table list. If no lines are selected,
	 * just exports the result list. If one or more lines are selected, asks
	 * whether to export result list or to print appropriate reports for
	 * selection
	 * 
	 * §precondition getCollectStateModel().getOuterState() ==
	 *               CollectState.OUTERSTATE_RESULT
	 */
	private void cmdExportResult() {
		assert getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_RESULT;

		final List<Clct> lstclctlo = getSelectedCollectables();
		final UsageCriteria usagecriteria = (lstclctlo.isEmpty() ? null	: getGreatestCommonUsageCriteriaFromCollectables(lstclctlo));

		//FIXME: NUCLOS-5588 Doesn't work this way, because even if only some lines are selected, the complete list will be exported.
		//int rowCount2Export = lstclctlo.isEmpty() ? getResultPanel().getResultTable().getRowCount() : lstclctlo.size();

		// NUCLOS-5582 Step 5)
		int rowCount2Export = getSearchStrategy().getCollectableProxyList().totalSize(false);
		if (rowCount2Export == 0x7FFFFFFF) {
			rowCount2Export = getSearchStrategy().getCollectableProxyList().totalSize(true);
			getResultPanel().setStatusBar(rowCount2Export);
		}

		int maxCount = AbstractProxyList.HUGELISTSIZE;

		if (rowCount2Export > maxCount) {
			String msg = getSpringLocaleDelegate().getMessage("EntityCollectController.Limit",
					"The list is too large for this action. Please limit to a max number of {0}!", maxCount);
			JOptionPane.showMessageDialog(MainController.getMainFrame(), msg);
			return;
		}

		int maxSpreedSheet = AbstractProxyList.LARGELISTSIZE;
		final Integer iMaxSpreedSheet = rowCount2Export >= maxSpreedSheet ? maxSpreedSheet : null;

		// NUCLOS-6193
		final String newText = SpringLocaleDelegate.getInstance().getTextFallback("EntityCollectController.46", "Preparing export...");
		final String oldText = btnExportResults.getText();
		btnExportResults.setText(newText);
		btnExportResults.setEnabled(false);

		final Runnable finishCallback = new Runnable() {
			@Override
			public void run() {
				if (btnExportResults != null) {
					btnExportResults.setEnabled(true);
					btnExportResults.setText(oldText);
				}
			}
		};

		UIUtils.runCommand(getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final ISearchStrategy<PK, Clct> ss = getSearchStrategy();
				final CollectableSearchExpression clctexprInternal = ss.getSearchExpressionForExport();
				final Runnable printRunnable = new Runnable() {
					@Override
					public void run() {
						cmdPrintSelectedObjects();
					}
				};
				final ReportController reportCtrl = new ReportController(getTab());
				final boolean exportLaunched = reportCtrl.export(getCollectableEntity(), clctexprInternal,
						getSelectedFields(), getSelectedFieldsWidth(), !lstclctlo.isEmpty(), usagecriteria,
						printRunnable, getCustomUsage(), iMaxSpreedSheet, finishCallback);

				if (!exportLaunched) {
					finishCallback.run();
				}
			}
		});
	}
	
	protected UsageCriteria getGreatestCommonUsageCriteriaFromCollectables(Collection<Clct> collclct) {
		for (Clct clct : collclct) {
			assert clct.getEntityUID().equals(entityUid);
		}
		return new UsageCriteria(entityUid, null, null, customUsage);
	}
	
	protected UsageCriteria getUsageCriteria(Collectable clct) {
		assert clct.getEntityUID().equals(entityUid);
		return new UsageCriteria(entityUid, null, null, customUsage);
	}
	
	/**
	 * get if the given object has assigned forms
	 * 
	 * @param clct
	 */
	protected boolean hasFormsAssigned(Clct clct) {
		return ReportDelegate.getInstance().hasReportsAssigned(getUsageCriteria(clct));
	}
	
	protected boolean hasFormsAssigned(Collection<Clct> collclct) {
		return ReportDelegate.getInstance().hasReportsAssigned(getGreatestCommonUsageCriteriaFromCollectables(collclct));
	}
	
	protected UID getDocumentSubformEntityUID() {
		return null;
	}
	
	protected UID[] getDocumentSubformColumns() {
		return null;
	}
	
	protected void cmdPrintSelectedObjects() {
		try {
			final List<Clct> collectables = getSelectedCollectables();
			if (collectables.isEmpty()) {
				return;
			}
			
			final PrintController printController = new PrintController(getTab());
			printController.show(getGreatestCommonUsageCriteriaFromCollectables(collectables), 
					collectables, getDocumentSubformEntityUID(), getDocumentSubformColumns(), 
					new PrintControllerEventListener() {

						@Override
						public void execute(final PrintControllerEventType event, final Object value) {
							final boolean isDetailsMode = getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_DETAILS;
							if (PrintControllerEventType.SUBMIT_SUCCESS == event) {
								try {
									if (isDetailsMode) {
										if (collectables.size() > 1) {
											// multi edit -> start a new one, perhaps the data was changed by rules
											setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_MULTIVIEW);
										} else {
											refreshCurrentCollectable();
										}
									} else {
										refreshSelectedCollectablesInResult();
									}
								} catch (final CommonBusinessException ex) {
									LOG.error(ex.getMessage(),ex);
								}
							} else if (PrintControllerEventType.SUBMIT_ERROR == event) {
								Collection col = (Collection)value;
								if (col != null && col.size() >= 1) {
									PrintResultTO printResult = (PrintResultTO)col.iterator().next();
									Exception ex = printResult.getException();
									String msg = ex.getMessage();
									if (isDetailsMode && msg != null && msg.contains("NuclosBusinessRuleException")) {
										// show NuclosBusinessRuleException
										showPointers(new PointerCollection(msg.substring(msg.indexOf("NuclosBusinessRuleException")+29), "Fehler"));
									} else if (ex != null) {
										if (ex instanceof CommonPrintException) {
											Errors.getInstance().showExceptionDialog(getParent(), new NuclosBusinessException(msg));
										}
									}
								}
							}
						}
					});

		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(getTab(), ex);
		}
	}
	
	private boolean isUnlockAllowed(Clct clct) {
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(getEntityUid());
		final UnlockMode unlockMode = eMeta.getUnlockMode();
		final boolean isUnlockByUserAllowed = unlockMode == UnlockMode.ALL_USERS_MANUALLY 
											  || Boolean.TRUE.equals(SecurityCache.getInstance().isSuperUser());
		
		if (eMeta.isOwner()) {
			CollectableField ownerField = clct.getField(SF.OWNER.getUID(getEntityUid()));
			if (ownerField != null) {
				final UID ownerUID = (UID) ownerField.getValueId();
				if (ownerUID != null) {
					// locked
					return isUnlockByUserAllowed;
				}
			}
		}
		return false;
	}

	protected void updateLockState(Clct clct) {
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(getEntityUid());
		if (eMeta.isOwner()) {
			final SpringLocaleDelegate locale = SpringLocaleDelegate.getInstance();
			final LockMode lockMode = eMeta.getLockMode();
			final UnlockMode unlockMode = eMeta.getUnlockMode();
			final boolean isUnlockByUserAllowed = isUnlockAllowed(clct);
			// currently only lock by API:
			final boolean isLockByUserAllowed = false;
			
			CollectableField ownerField = clct.getField(SF.OWNER.getUID(getEntityUid()));
			final boolean isLocked = ownerField != null && ownerField.getValueId() != null;
			actLock.putValue(Action.SELECTED_KEY, isLocked);
			String lockText = "";
			String tooltipText = "";
			Icon lockIcon = null;
			boolean visible = false;
			boolean enable = false;
			if (isLocked) {
				enable = isUnlockByUserAllowed;
				visible = true;
				
				final UID ownerUID = (UID) ownerField.getValueId();
				final boolean isOwner = org.nuclos.common2.LangUtils.equal(SecurityCache.getInstance().getUserUID(), ownerUID);
				
				if (isOwner) {
					lockText = locale.getMsg("locked.state.your.record");
				} else {
					lockText = locale.getMsg("locked.state.another.user", ownerField.getValue());
				}
				tooltipText = locale.getMsg("click.to.unlock");
				lockIcon = Icons.getInstance().getIconLockClose16();
				
			} else {
				// unlocked
				if (isLockByUserAllowed) {
					lockText = locale.getMsg("locked.action.lock.now");
					enable = true;
					visible = true;
				}
			}
			
			//actLock.putValue(ToolBarItemAction.ACTION_DISABLED, !enable);
			actLock.setEnabled(enable);
			actLock.putValue(ToolBarItemAction.VISIBLE, visible);
			actLock.putValue(Action.NAME, lockText);
			actLock.putValue(Action.SHORT_DESCRIPTION, tooltipText);
			actLock.putValue(Action.SMALL_ICON, lockIcon);
		}
		
	}
	
	protected void cmdLock() {
		final List<Clct> collectables = getSelectedCollectables();
		if (collectables.isEmpty()) {
			return;
		}
		
		for (Clct clct : collectables) {
			if (isUnlockAllowed(clct)) {
				Future<LayerLock> lock = null;
				try {
					lock = lockFrame();
					EntityObjectDelegate.getInstance().unlock(getEntityUid(), clct.getId());
					refreshCurrentCollectable();
				} catch (CommonPermissionException e) {
					Errors.getInstance().showExceptionDialog(getParent(), e);
					return;
				} catch (CommonBusinessException e) {
					Errors.getInstance().showExceptionDialog(getParent(), e);
					return;
				} finally {
					if (lock != null) {
						unLockFrame(lock);
					}
				}
			}
		}
	}
	
	protected synchronized CollectableListOfValues<UID> getSearchOwnerLOV() {
		if (searchOwnerClctLOV == null) {
			final CollectableEntityField clctef = getCollectableEntity().getEntityField(SF.OWNER.getUID(getEntityUid()));
			final FieldMeta<?> fieldMeta = MetaProvider.getInstance().getEntityField(clctef.getUID());
			searchOwnerClctLOV = new CollectableListOfValues<UID>(clctef, true, true);
			searchOwnerClctLOV.setLabelText(SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(fieldMeta));
			searchOwnerClctLOV.setColumns(12);
		}
		setSearchOwnerLOVModelFromEditView();
		return searchOwnerClctLOV;
	}
	
	private void setSearchOwnerLOVModelFromEditView() {
		if (searchOwnerClctLOV != null) {
			if (isSearchPanelAvailable()) {
				final CollectableComponentModel clctcompmodel = getSearchEditView().getModel().getCollectableComponentModelFor(SF.OWNER.getUID(getEntityUid()));
				if (clctcompmodel != null) {
					searchOwnerClctLOV.setModel(clctcompmodel);
				}
			}
		}
	}
	protected void addButtonInvalidMasterdata() {

		chkbxUseInvalidMasterData.setText(getSpringLocaleDelegate()
				.getMessage("GenericObjectCollectController.96",
						"Ung\u00fcltige Stammdaten anzeigen?"));
		chkbxUseInvalidMasterData.setOpaque(false);
		chkbxUseInvalidMasterData.setEnabled(true);
		chkbxUseInvalidMasterData.setState(true);

		// chkbxUseInvalidMasterData.setForeground(Color.white);
		chkbxUseInvalidMasterData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bUseInvalidMasterData = chkbxUseInvalidMasterData
						.isSelected();				
				// loadSpecializedLayoutForDetails();
				Collection<CollectableComponent> collectableComponents = getDetailsPanel()
						.getEditView().getCollectableComponents();
				for (CollectableComponent clcmp : collectableComponents) {
					if (clcmp instanceof CollectableComponentWithValueListProvider) {
						CollectableComponentWithValueListProvider clcmpWithVLP = (CollectableComponentWithValueListProvider) clcmp;
						CollectableFieldsProvider valueListProvider = clcmpWithVLP
								.getValueListProvider();
						if (valueListProvider instanceof ManagedCollectableFieldsProvider) {
							((ManagedCollectableFieldsProvider) valueListProvider)
									.setIgnoreValidity(bUseInvalidMasterData);
							clcmpWithVLP
									.setValueListProvider(valueListProvider);
							clcmpWithVLP.refreshValueList(false);
						}
					}
				}
			}
		});
		this.getDetailsPanel().addPopupExtraSeparator();
		this.getDetailsPanel().addPopupExtraMenuItem(
				chkbxUseInvalidMasterData);
		
		
	}
	
	public boolean useInvalidMasterDataEnabled() {
		return chkbxUseInvalidMasterData.getState();		
	}

	protected static void doAutoNumberStuff(Collection<CollectableComponent> clctComponents) {
		for (final CollectableComponent cltComponent : clctComponents) {
			cltComponent.setEnabled(false);
			// by default the auto number field is filled by
			// '<auto>'
			final LabeledTextField component = (LabeledTextField) cltComponent.getJComponent();
			if (StringUtils.isNullOrEmpty(component.getTextField().getText())) {
				component.getTextField().setText(SpringLocaleDelegate.getInstance().getText("AutoNumber.Ui.fieldDefault"));
			}
		}
	}

	protected boolean bUseNewHistory = false;
	// @todo eliminate me with old history

	protected final boolean isNewHistoricalView() {
		return isHistoricalView() && bUseNewHistory;
	}

	/**
	 * §postcondition result != null
	 *
	 * @param subform
	 * @param iStateId
	 * @return the permission (read/write) for the given subform in the state
	 *         with the given id
	 */
	private Permission getSubformPermission(SubForm subform, UID iStateId) {
		Map<UID, SubformPermission> mpPermissions = SecurityCache.getInstance().getSubFormPermission(subform.getEntityUID());
		if (mpPermissions.containsKey(iStateId)) {
			Permission permission = Permission.READWRITE;
			if (isHistoricalView() && !isNewHistoricalView()) {
				permission = Permission.READONLY;
			}

			return org.nuclos.common2.LangUtils.min(permission, mpPermissions.get(iStateId).getSimplePermission());
		} else {
			return Permission.NONE;
		}
	}

	/**
	 * §postcondition result != null
	 *
	 * @param subform
	 * @param collStateUids
	 * @return the least common permission for the subform
	 */
	private SubformPermission getLeastCommonSubformPermission(SubForm subform, Collection<UID> collStateUids) {
		if (CollectionUtils.isNullOrEmpty(collStateUids)) {
			throw new IllegalArgumentException("collStateIds");
		}

		Map<UID, SubformPermission> mpPermissions = SecurityCache.getInstance().getSubFormPermission(subform.getEntityUID());
		SubformPermission result = SubformPermission.ALL;

		for (UID stateUid : collStateUids) {
			result = result.min(mpPermissions.get(stateUid));
			if (result == null) {
				break;
			}
		}

		return result;
	}

	// adjust subforms for GenericObjects:
	protected final void adjustSubformForPermission(Collection<DetailsSubFormController<Long, CollectableEntityObject<Long>>> collsubformctls,
													Collection<UID> collStateUids, Long pk, boolean bSearchMode) {
		if (collsubformctls == null || collsubformctls.isEmpty()) {
			return;
		}

		boolean bForcedReadonly = (isHistoricalView() && !isNewHistoricalView()) || !MetaProvider.getInstance().getEntity(getEntityUid()).isEditable();

		for (DetailsSubFormController subformctl : collsubformctls) {
			SubForm subform = subformctl.getSubForm();
			SubformPermission permission = getLeastCommonSubformPermission(subform, collStateUids);

			if (subform != null && subform.getParent() != null) {
				subform.setVisible(permission != null && permission.includesReading());
			}

			final boolean isEditable = !bForcedReadonly
					&& permission != null && permission.includesWriting()
					&& SecurityCache.getInstance().isWriteAllowedForModule(getEntityUid(), pk)
					&& MetaProvider.getInstance().getEntity(subform.getEntityUID()).isEditable();

			if (!isEditable) {
				subform.setReadOnly(!bSearchMode);
				if (!bSearchMode) {
					subform.setCreate(false);
					subform.setDelete(false);
					subformctl.setSubformPermission(permission);
				}
				continue;
			}

			subform.setReadOnly(false);

			// NUCLOS-6134

			if (subform.setCreate(permission.canCreate())) {
				ScriptContext sc = new CollectControllerScriptContext<>(
						this,
						new ArrayList<>(
								getSubFormControllersInDetails()
						)
				);
				subform.setNewEnabled(sc);
			};

			subform.setDelete(permission.canDelete());

			subformctl.setSubformPermission(permission);

		}
	}

	protected void addAndSetupExportResultButton() {
		getResultPanel().addStatusBarComponent(btnExportResults);

		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_EXPORT_SEARCHRESULT)) {
			btnExportResults.setEnabled(true);
			// action: Export results
			btnExportResults.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					cmdExportResult();
				}
			});
		}
		else {
			btnExportResults.setEnabled(false);
			btnExportResults.setToolTipText(getSpringLocaleDelegate().getMessage("EntityCollectController.45",
					"Liste exportieren - Sie verf\u00fcgen nicht \u00fcber ausreichende Rechte."));
		}
	}

	private String selectedTabName;

	private boolean bSelectTabNameListenerEnabled = true;

	public boolean isSelectTabNameListenerEnabled() {
		return bSelectTabNameListenerEnabled;
	}

	public void setSelectTabNameListenerEnabled(final boolean bSelectTabNameListenerEnabled) {
		this.bSelectTabNameListenerEnabled = bSelectTabNameListenerEnabled;
	}

	protected final String getSelectedTabName() {
		return selectedTabName;
	}

	public class JTabbedPaneChangeListener implements ChangeListener {

		@Override
		public void stateChanged(ChangeEvent e) {
			if (!bSelectTabNameListenerEnabled) {
				return;
			}
			JTabbedPane pane = (JTabbedPane) e.getSource();
			int idx = pane.getSelectedIndex();
			if (idx != -1) {
				EntityCollectController.this.selectedTabName = pane.getComponentAt(idx).getName();
			}
		}
	}

	// To override and do things
	protected void handleBusinessExceptionAfterSafe(CommonBusinessException cbe) {
		// Implement when needed
	}

}