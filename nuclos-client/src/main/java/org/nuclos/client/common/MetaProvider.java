//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.customcomp.CustomComponentCache;
import org.nuclos.client.genericobject.CollectableGenericObjectEntity;
import org.nuclos.client.genericobject.GenericObjectLayoutCache;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.report.ReportDelegate;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.searchfilter.SearchFilterCache;
import org.nuclos.client.startup.AbstractLocalUserCache;
import org.nuclos.client.tasklist.TasklistCache;
import org.nuclos.common.CommonMetaDataClientProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaUtils;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.LafParameterMap;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.util.DalTransformations;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.metadata.NotifyObject;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * An caching singleton for remotely accessing the meta data information
 * from the client side.
 * <p>
 * For accessing the remote (server) side, this implementation uses
 * {@link org.nuclos.client.masterdata.MetaDataDelegate}.
 * </p>
 */
// @Component("metaDataProvider")
public class MetaProvider extends AbstractLocalUserCache
		implements IMetaProvider, CommonMetaDataClientProvider {

	private static final Logger LOG = Logger.getLogger(MetaProvider.class);

	private static final long TIMEOUT_SECONDS = 100L;

	private static MetaProvider INSTANCE;

	//

	private final DataCache dataCache = new DataCache();

	// set in afterPropertiesSet

	private transient MessageListener messageListener;

	// Spring injection

	private transient TopicNotificationReceiver tnr;

	private transient MetaDataDelegate metaDataDelegate;

	// end of Spring injection

	MetaProvider() {
		INSTANCE = this;
	}

	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static MetaProvider getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	public final void setTopicNotificationReceiver(TopicNotificationReceiver tnr) {
		this.tnr = tnr;
	}

	@Autowired
	final void setMetaDataDelegate(MetaDataDelegate metaDataDelegate) {
		this.metaDataDelegate = metaDataDelegate;
	}

	/**
	 * Will also be called on deserialized instances. (tp)
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		// Constructor might not be called - as this instance might be deserialized (tp)
		if (INSTANCE == null) {
			INSTANCE = this;
		}
		if (!wasDeserialized() || !isValid())
			dataCache.buildMaps();
		messageListener = new MessageListener() {
			@Override
			public void onMessage(Message msg) {
				LOG.info("onMessage " + this + " revalidate cache...");
				if (msg instanceof ObjectMessage) {
					try {
						Object oMsg = ((ObjectMessage) msg).getObject();
						if (oMsg instanceof NotifyObject) {
							if (E.REPORTUSAGE.checkEntityUID(((NotifyObject) oMsg).getEntity())
									|| E.FORMUSAGE.checkEntityUID(((NotifyObject) oMsg).getEntity())) {
								ReportDelegate.getInstance().invalidateCaches();
								return;
							}
						}
					} catch (JMSException e) {
						LOG.error(e.getMessage(), e);
					}
				}
				MetaProvider.this.revalidate();
			}
		};
		tnr.subscribe(getCachingTopic(), messageListener);
	}

	@Override
	public String getCachingTopic() {
		return JMSConstants.TOPICNAME_METADATACACHE;
	}

	@Override
	public Collection<EntityMeta<?>> getAllEntities() {
		return dataCache.getMapEntityMetaData().values();
	}

	public Collection<EntityMeta<?>> getAllDynEntities() {
		return dataCache.getMapDynamicEntities().values();
	}

	public String getRepresentativeEntityName(UID entityUID) {
		StringBuilder sb = new StringBuilder();

		sb.append(getEntity(entityUID).getEntityName());
		sb.append(" (");
		UID nucletUID = getEntity(entityUID).getNuclet();

		// @TODO Nuclet label caching
		sb.append(nucletUID.getStringifiedDefinition());

		sb.append(")");

		return sb.toString();
	}

	@Override
	public boolean checkEntity(final UID entityUID) {
		final EntityMeta<?> result = _getEntity(entityUID);
		return result != null;
	}

	@Override
	public EntityMeta<?> getEntity(UID entityUID) {
		final EntityMeta<?> result = _getEntity(entityUID);
		if (result == null) {
			final String msg = "entity " + entityUID + " does not exist.";
			final CommonFatalException e = new CommonFatalException(msg);
			LOG.warn(msg, e);
			throw e;
		}
		return result;
	}

	private EntityMeta<?> _getEntity(UID entityUID) {
		return dataCache.getMapEntityMetaData().get(entityUID);
	}

	@Override
	public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entityUID) {
		final Map<UID, FieldMeta<?>> result = dataCache.getMapEntityFieldMetaData().get(entityUID);
		if (result == null) {
			return Collections.emptyMap();
		}
		return result;
	}

	public Boolean isLocalizedField(UID fieldUID) {
		Boolean retVal = Boolean.FALSE;
		
		try {
			FieldMeta<?> entityField = getEntityField(fieldUID);
			if (entityField != null && Boolean.TRUE.equals(entityField.isLocalized())) {
				retVal = Boolean.TRUE;
			}
		} catch (Exception e) {}
		
		return retVal;
	}
	
	/**
	 * TODO:
	 * Get rid of non-existing fieldUIDs! (tp)
	 */
	@Override
	public FieldMeta<?> getEntityField(UID fieldUID) {
		if (fieldUID == null) {
			return null;
		}
		FieldMeta<?> fieldMeta = dataCache.getMapFieldMetaData().get(fieldUID);
		if (fieldMeta != null) {
			return fieldMeta;
		} else {
			if (CalcAttributeUtils.isCalcAttributeCustomization(fieldUID)) {
				try {
					fieldMeta = metaDataDelegate.getEntityField(fieldUID);
					if (fieldMeta != null) {
						dataCache.getMapFieldMetaData().put(fieldUID, fieldMeta);
					}
					return fieldMeta;
				} catch (Exception ex) {
					// ignore here
				}
			}
			final String msg = "entity field " + fieldUID + " does not exist.";
			final CommonFatalException e = new CommonFatalException(msg);
			LOG.debug(msg, e);
			throw e;
		}
	}
	
	/**
	 * Looks up a field by name.
	 * 
	 * @param entityUID The UID of the entity to which the field belongs
	 * @param fieldName The case-insensitive name of the field
	 * @return
	 */
	public FieldMeta<?> getEntityFieldByName(UID entityUID, String fieldName) {
		if (entityUID == null || fieldName == null) {
			return null;
		}
		
		EntityMeta<?> entityMeta = getEntity(entityUID);
		if (entityMeta == null) {
			return null;
		}
		
		for (FieldMeta<?> fieldMeta: entityMeta.getFields()) {
			if (StringUtils.equalsIgnoreCase(fieldName, fieldMeta.getFieldName())) {
				return fieldMeta;
			}
		}
		
		return null;
	}

	@Override
	public EntityMeta<?> getByTablename(String sTableName) {
		assert sTableName != null;
		for(EntityMeta<?> meta : getAllEntities()) {
			if(meta.getDbTable().equalsIgnoreCase(sTableName))
				return meta;
		}
		throw new CommonFatalException("Table for " + sTableName + " not found!");		
	}

	@Override
	public List<EntityObjectVO<UID>> getAllEntityMenus() {
		return dataCache.getListEntityMenus();
	}

	@Override
	public Map<UID, LafParameterMap> getAllLafParameters() {
		final Map<UID, LafParameterMap> result = dataCache.getMapLafParameters();
		if (result == null) {
			return Collections.emptyMap();
		}
		return result;
	}

	@Override
	public LafParameterMap getLafParameters(UID entityUID) {
		return getAllLafParameters().get(entityUID);
	}

	public void revalidate() {
		dataCache.buildMaps();
		LOG.info("Revalidated cache " + this);

		SecurityCache.getInstance().revalidate();
		// MetaDataCache.getInstance().invalidate();
		MasterDataCache.getInstance().invalidate();
		MasterDataDelegate.getInstance().invalidateCaches();
		MasterDataDelegate.getInstance().invalidateLayoutCache();
		AttributeCache.getInstance().revalidate();
		CollectableGenericObjectEntity.Cache.getInstance().invalidate();
		LocaleDelegate.getInstance().flush();
		GenericObjectLayoutCache.getInstance().invalidate();
		GenericObjectMetaDataCache.getInstance().revalidate();
		SearchFilterCache.getInstance().invalidate();
		TasklistCache.getInstance().revalidate();
		ResourceCache.getInstance().invalidate();
		CustomComponentCache.getInstance().revalidate();
		ReportDelegate.getInstance().invalidateCaches();
	}

	private final class DataCache implements Serializable {

		private final ReadWriteLock lock = new ReentrantReadWriteLock();

		private Map<UID, List<UID>> mapEntitiesByNuclets = null;

		private Map<UID, EntityMeta<?>> mapEntityMetaData = null;
		private Map<UID, EntityMeta<?>> mapLanguageEntityMetaData = null;
		
		private Map<UID, FieldMeta<?>> mapFieldMetaData = null;
		private Map<UID, Map<UID, FieldMeta<?>>> mapEntityFieldMetaData = null;
		private List<EntityObjectVO<UID>> lstEntityMenus = null;

		private Map<UID, EntityMeta<?>> mapDynamicEntities;

		private Map<UID, LafParameterMap> mapLafParameter = null;

		public Map<UID, List<UID>> getMapEntitiesByNuclets() {
			final Lock l = lock.readLock();
			try {
				if (l.tryLock(100, TimeUnit.SECONDS)) {
					return mapEntitiesByNuclets;
				} else {
					handleProblem("getMapEntitiesByNuclets");
				}
			} catch (InterruptedException e) {
				handleProblem("getMapEntitiesByNuclets", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("getMapEntitiesByNuclets", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return null;
		}

		private void handleProblem(String msg, Exception e) throws NuclosFatalException {
			msg = "Could not aquire lock : " + msg + ": " + e;
			LOG.error(msg, e);
			throw new NuclosFatalException(msg, e);
		}

		private void handleProblem(String msg) throws NuclosFatalException {
			msg = "Could not aquire lock : " + msg;
			LOG.error(msg);
			throw new NuclosFatalException(msg);
		}

		public Map<UID, EntityMeta<?>> getMapEntityMetaData() {
			final Lock l = lock.readLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					return mapEntityMetaData;
				} else {
					handleProblem("getMapEntityMetaData");
				}
			} catch (InterruptedException e) {
				handleProblem("getMapEntityMetaData", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("getMapEntityMetaData", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return null;
		}

		public Map<UID, EntityMeta<?>> getMapLocalizedEntityMetaData() {
			final Lock l = lock.readLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					return mapLanguageEntityMetaData;
				} else {
					handleProblem("getMapLocalizedEntityMetaData");
				}
			} catch (InterruptedException e) {
				handleProblem("getMapLocalizedEntityMetaData", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("getMapLocalizedEntityMetaData", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return null;
		}
		
		public Map<UID, Map<UID, FieldMeta<?>>> getMapEntityFieldMetaData() {
			final Lock l = lock.readLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					return mapEntityFieldMetaData;
				} else {
					handleProblem("getMapEntityFieldMetaData");
				}
			} catch (InterruptedException e) {
				handleProblem("getMapEntityFieldMetaData", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("getMapEntityFieldMetaData", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return null;
		}

		public Map<UID, FieldMeta<?>> getMapFieldMetaData() {
			final Lock l = lock.readLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					return mapFieldMetaData;
				} else {
					handleProblem("getMapFieldMetaData");
				}
			} catch (InterruptedException e) {
				handleProblem("getMapFieldMetaData", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("getMapFieldMetaData", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return null;
		}

		public Map<UID, EntityMeta<?>> getMapDynamicEntities() {
			final Lock l = lock.readLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					return mapDynamicEntities;
				} else {
					handleProblem("getMapDynamicEntities");
				}
			} catch (InterruptedException e) {
				handleProblem("getMapDynamicEntities", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("getMapDynamicEntities", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return null;
		}

		public List<EntityObjectVO<UID>> getListEntityMenus() {
			final Lock l = lock.readLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					return lstEntityMenus;
				} else {
					handleProblem("getListEntityMenus");
				}
			} catch (InterruptedException e) {
				handleProblem("getListEntityMenus", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("getListEntityMenus", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return null;
		}

		public Map<UID, LafParameterMap> getMapLafParameters() {
			final Lock l = lock.readLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					return mapLafParameter;
				} else {
					handleProblem("getMapLafParameters");
				}
			} catch (InterruptedException e) {
				handleProblem("getMapLafParameters", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("getMapLafParameters", e);
			} finally {
				l.unlock();
			}
			// Never reached because method returns or NuclosFatalException
			return null;
		}

		private Map<UID, Map<UID, FieldMeta<?>>> buildMapFieldMetaData(Collection<EntityMeta<?>> allEntities) {
			return metaDataDelegate.getAllEntityFieldsByEntitiesGz(
					CollectionUtils.transform(allEntities, DalTransformations.getEntity()));
		}

		public void buildMaps() {
			LOG.info("buildMaps: begin");
			final Lock l = lock.writeLock();
			try {
				if (l.tryLock(TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
					LOG.info("buildMaps: aquired lock");
					final Collection<EntityMeta<?>> allEntities = metaDataDelegate.getAllEntities();
					final List<EntityObjectVO<UID>> tmpLstEntityMenus = metaDataDelegate.getEntityMenus();
					final Map<UID, LafParameterMap> tmpMapLafParameter = metaDataDelegate.getLafParameters();
					
					final Map<UID, List<UID>> entitiesByNuclets = new HashMap<UID, List<UID>>();
					final Map<UID, EntityMeta<?>> mde = new HashMap<UID, EntityMeta<?>>();

					for (final EntityMeta<?> meta : allEntities) {
						final UID eUid = meta.getUID();
						
						// fill SF (tp)
						SF.CHANGEDAT.getUID(eUid);
						SF.CHANGEDBY.getUID(eUid);
						SF.CREATEDAT.getUID(eUid);
						SF.CREATEDBY.getUID(eUid);
						SF.IMPORTVERSION.getUID(eUid);
						SF.LOGICALDELETED.getUID(eUid);
						SF.ORIGIN.getUID(eUid);
						SF.ORIGINUID.getUID(eUid);
						if (!meta.isUidEntity()) {
							SF.PK_ID.getUID(eUid);
						} else {
							SF.PK_UID.getUID(eUid);
						}
						if (!meta.isStateModel()) {
							SF.PROCESS.getUID(eUid);
							SF.PROCESS_UID.getUID(eUid);
							SF.STATE.getUID(eUid);
							SF.STATE_UID.getUID(eUid);
							SF.STATEICON.getUID(eUid);
							SF.STATENUMBER.getUID(eUid);
						}
						SF.SYSTEMIDENTIFIER.getUID(eUid);
						SF.VERSION.getUID(eUid);
				
						if (meta.getNuclet() != null) {
							if (!entitiesByNuclets.containsKey(meta.getNuclet())) {
								entitiesByNuclets.put(meta.getNuclet(), new ArrayList<UID>());
							}
							if (entitiesByNuclets.containsKey(meta.getNuclet())) {
								entitiesByNuclets.get(meta.getNuclet()).add(meta.getUID());
							}
						}
						if (meta.isDynamic()) {
							mde.put(meta.getUID(), meta);
						}
					}

					for (UID nuclet : entitiesByNuclets.keySet()) {
						entitiesByNuclets.put(nuclet, Collections.unmodifiableList(entitiesByNuclets.get(nuclet)));
					}
					mapLanguageEntityMetaData = Collections.unmodifiableMap(
							CollectionUtils.generateLookupMap(metaDataDelegate.getAllLanguageEntities(),
							DalTransformations.getEntity()));
					mapEntitiesByNuclets = Collections.unmodifiableMap(entitiesByNuclets);
					mapEntityMetaData = Collections.unmodifiableMap(CollectionUtils.generateLookupMap(allEntities,
							DalTransformations.getEntity()));
					mapEntityFieldMetaData = Collections.unmodifiableMap(buildMapFieldMetaData(allEntities));
					mapFieldMetaData = new ConcurrentHashMap<UID, FieldMeta<?>>(DalTransformations.flattenFieldMetaMap(mapEntityFieldMetaData));
					FieldMetaUtils.putPkFields(mapFieldMetaData, mapEntityMetaData.values());
					mapDynamicEntities = Collections.unmodifiableMap(mde);

					lstEntityMenus = tmpLstEntityMenus;
					mapLafParameter = Collections.unmodifiableMap(tmpMapLafParameter);
				} else {
					handleProblem("buildMaps");
				}
			} catch (InterruptedException e) {
				handleProblem("buildMaps", e);
			} catch (NuclosFatalException e) {
				throw e;
			} catch (Exception e) {
				handleProblem("buildMaps", e);
			} finally {
				l.unlock();
				LOG.info("buildMaps: end");
			}
		}

	} //class DataCache

	public boolean isEntity(UID entityUID) {
		return _getEntity(entityUID) != null;
	}

	@Override
	public List<UID> getEntities(UID nucletUID) {
		return dataCache.getMapEntitiesByNuclets().get(nucletUID);
	}

	@Override
	public boolean isNuclosEntity(UID entityUID) {
		return E.isNuclosEntity(entityUID);
	}

	@Override
	public Collection<FieldMeta<?>> getAllReferencingFields(UID masterUID) {
		List<FieldMeta<?>> lstFields = new ArrayList<FieldMeta<?>>();
		Collection<EntityMeta<?>> ents = getAllEntities();
		for (EntityMeta<?> ent : ents) {
			Map<UID, FieldMeta<?>> fms = getAllEntityFieldsByEntity(ent.getUID());
			for (UID field : fms.keySet()) {
				FieldMeta<?> fm = fms.get(field);
				if (masterUID.equals(fm.getForeignEntity())) {
					if (!lstFields.contains(fm))
						lstFields.add(fm);
				}
			}
		}
		return lstFields;
	}

	@Override
	public FieldMeta<?> getCalcAttributeCustomization(UID fieldUID, String paramValues) {
		return metaDataDelegate.getCalcAttributeCustomization(fieldUID, paramValues);
	}

	@Override
	public Collection<EntityMeta<?>> getAllLanguageEntities() {
		return dataCache.mapLanguageEntityMetaData.values();
	}

	@Override
	public List<EntityObjectVO<UID>> getNuclets() {
		return metaDataDelegate.getNuclets();
	}

	@Override
	public Set<UID> getImplementingEntities(final UID genericEntityUID) {
		return metaDataDelegate.getImplementingEntities(genericEntityUID);
	}
}
