package org.nuclos.client.common;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JComponent;

import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.scripting.context.CollectControllerScriptContext;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.exception.CommonBusinessException;

public class SubFormsLoaderClientWorker<PK> extends EntityCollectController.SubFormsInterruptableClientWorker {

    private Collection<EntityObjectVO<PK>> collmdvo;
    private PK parentId;
    private MasterDataSubFormController<PK> mdsubformctl;
    private boolean limited = false;
    private EntityCollectController entityCollectController;
    private Collectable<PK> clct;
    private CollectableSearchCondition filterCondition;
    private Collectable<PK> mdwdCurrent;

    public SubFormsLoaderClientWorker(SubFormController subformctl, EntityCollectController entityCollectController, Collectable<PK> clct, CollectableSearchCondition filterCondition, Collectable<PK> mdwdCurrent) {
        this.mdsubformctl = (MasterDataSubFormController<PK>) subformctl;
        this.entityCollectController = entityCollectController;
        this.clct = clct;
        this.filterCondition = filterCondition;
        this.mdwdCurrent = mdwdCurrent;
    }

    @Override
    public void init() throws CommonBusinessException {
        if (!interrupted) {
            mdsubformctl.clear();
            mdsubformctl.fillSubForm(null, new ArrayList<EntityObjectVO<PK>>());
            mdsubformctl.getSubForm().setLockedLayer();
        }
    }
    @Override
    public void work() throws NuclosBusinessException {
        if (interrupted || entityCollectController.isClosed()) {
            return;
        }
        parentId = clct.getPrimaryKey();
        if (parentId == null) {
            collmdvo = new ArrayList<EntityObjectVO<PK>>();
        }
        else {
            boolean bLoad = false;
            if (clct instanceof CollectableMasterDataWithDependants)  {
                final IDependentDataMap dep = ((CollectableMasterDataWithDependants<PK>) clct)
                        .getMasterDataWithDependantsCVO().getDependents();
                IDependentKey dependentKey = DependentDataMap.createDependentKey(mdsubformctl.getForeignKeyFieldUID());
                if (dep.hasData(dependentKey)) {
                    collmdvo = dep.getDataPk(dependentKey);
                    bLoad = false;
                }
            }
            if (bLoad || collmdvo == null) {
                Integer maxEntries = mdsubformctl.getSubForm().getMaxEntries();
                limited = entityCollectController.isDetailsViewAvailable(mdsubformctl) && maxEntries != null && maxEntries >= 0;

                collmdvo = MasterDataDelegate.getInstance().getDependentDataCollectionWithLimit(
                        mdsubformctl.getForeignKeyFieldUID(), filterCondition, mdsubformctl.getSubForm().getMapParams(),
                        entityCollectController.getCurrentLayoutUid(), limited ? maxEntries : null, parentId);
            }
        }
    }
    @Override
    public void handleError(Exception ex) {
        if (!interrupted) {
            Errors.getInstance().showExceptionDialog(getResultsComponent(), ex);
        }
    }

    @Override
    public JComponent getResultsComponent() {
        return mdsubformctl.getSubForm();
    }

    @Override
    public void paint() throws CommonBusinessException {
        // if this worker is interrupted - it is a worker for an "old" subform/main object.
        // The data should not be published to sub form! otherwise we will see a sub form data of another object!
        if (interrupted || entityCollectController.isClosed()) {
            return;
        }

        synchronized (entityCollectController) {
            final boolean bWasDetailsChangedIgnored = entityCollectController.isDetailsChangedIgnored();
            entityCollectController.setDetailsChangedIgnored(true);
            try {
                mdsubformctl.getSubForm().getJTable().setBackground(Color.WHITE);

                int originSize = collmdvo.size();
                final Integer limit = mdsubformctl.getSubForm().getMaxEntries();
                final boolean truncated = limited && limit > 0 && originSize == limit + 1;

                if (truncated) {
                    mdsubformctl.getSubForm().setReadOnlyWithDetailView(true, true);

                    if (collmdvo instanceof ArrayList && collmdvo.size() > 0) {
                        ((ArrayList) collmdvo).remove(collmdvo.size() - 1);
                    }
                }

                mdsubformctl.fillSubForm(parentId, collmdvo);

                if (truncated) {
                    mdsubformctl.displayLimitBubble(limit);
                }

				mdsubformctl.getSubForm().setNewEnabled(
						new CollectControllerScriptContext<>(
								entityCollectController,
								new ArrayList<DetailsSubFormController<PK, ?>>(entityCollectController.getSubFormControllersInDetails())
						)
				);

                if (entityCollectController.isHistoricalView()) {
                    entityCollectController.markSubformInHistoricalView(mdsubformctl, (CollectableWithDependants) clct, (CollectableWithDependants) mdwdCurrent);
                }
            }
            finally {
                collmdvo = null;
                if (!bWasDetailsChangedIgnored) {
                    entityCollectController.setDetailsChangedIgnored(bWasDetailsChangedIgnored);
                }
            }

            entityCollectController.getSubFormsLoader().setSubFormLoaded(mdsubformctl.getCollectableEntity().getUID(), true);
            if (!mdsubformctl.isClosed()) {
                mdsubformctl.getSubForm().forceUnlockFrame();
            }
        }
    }
}
