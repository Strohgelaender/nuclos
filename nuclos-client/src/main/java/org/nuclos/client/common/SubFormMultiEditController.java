//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.DefaultSelectObjectsPanel;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.SelectObjectsController;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.model.CollectableTableModel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.model.ChoiceList;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableStringFieldComparator;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Controller for inserting/removing multiple rows in a subform.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

class SubFormMultiEditController<PK,Clct extends Collectable<PK>> extends SelectObjectsController<CollectableField> {
	private final Logger log = Logger.getLogger(this.getClass());

	private final SubForm subform;
	private final AbstractDetailsSubFormController<PK,Clct> controller;

	SubFormMultiEditController(SubForm subform, AbstractDetailsSubFormController<PK,Clct> ctl) {
		super(subform, new DefaultSelectObjectsPanel<CollectableField>());
		this.subform = subform;
		this.controller = ctl;
	}

	public void run() {
		final CollectableTableModel<PK,Clct> model = (CollectableTableModel<PK,Clct>) this.subform.getJTable().getModel();

		/** @todo remove those optimistic assumptions below */
		final UID columnName = this.subform.getUniqueMasterColumnUid();
		if (columnName == null) {
			throw new CommonFatalException(getSpringLocaleDelegate().getMessage(
					"SubFormMultiEditController.1", "Im Unterformular {0} wurde keine eindeutige Spalte (unique master column) definiert.", subform.getEntityUID()));
		}

		final int colIndex = model.findColumnByFieldUid(columnName);
		if (colIndex < 0) {
			throw new CommonFatalException(getSpringLocaleDelegate().getMessage(
					"SubFormMultiEditController.2", "Im Unterformular {0} ist keine eindeutige Spalte (unique master column) namens {1} vorhanden.", subform.getEntityUID(), columnName));
		}

		final int viewColumn = this.subform.getJTable().convertColumnIndexToView(colIndex);
		final CollectableComponentTableCellEditor celleditor = (CollectableComponentTableCellEditor)
				this.subform.getJTable().getCellEditor(0, viewColumn);

		final LabeledCollectableComponentWithVLP comboBox = (LabeledCollectableComponentWithVLP) celleditor.getCollectableComponent();

		final Comparator<CollectableField> comp = new CollectableStringFieldComparator();
		//@TODO use right comparator here!!!
		//CollectableFieldComparatorFactory.getInstance().newCollectableFieldComparator(comboBox.getEntityField());
		final SortedSet<CollectableField> oldAvailableObjects = getNonNullValues(comboBox, comp);
		final List<CollectableField> oldSelectedObjects = new ArrayList<CollectableField>();
		final Collection<CollectableField> fixed = new ArrayList<CollectableField>();

		final List<CollectableField> oldAvailableObjectsList = new ArrayList<CollectableField>(oldAvailableObjects);
		// iterate through the table and compute selected fields:
		for (int row = 0; row < model.getRowCount(); ++row) {
			final CollectableField value = model.getValueAt(row, colIndex);
			for (Iterator iterator = oldAvailableObjectsList.iterator(); iterator.hasNext();) {
				CollectableField collectableField = (CollectableField) iterator.next();
				// NUCLOS-6966 3) Don't do duplicates
				if (oldSelectedObjects.contains(collectableField)) {
					continue;
				}
				if (collectableField.isIdField() && value.isIdField()) {
					if (LangUtils.equal(collectableField.getValueId(), value.getValueId())) {
						oldSelectedObjects.add(collectableField);
						oldAvailableObjects.remove(collectableField);
						break;
					}
				} else {
					if (LangUtils.equal(collectableField.getValue(), value.getValue())) {
						oldSelectedObjects.add(collectableField);
						oldAvailableObjects.remove(collectableField);
						break;
					}
				}
			}

			if (!controller.isRowRemovable(row)) {
				fixed.add(value);
			}
		}

		// perform the dialog:
		ChoiceList<CollectableField> ro = new ChoiceList<CollectableField>();
		ro.set(oldAvailableObjects, oldSelectedObjects, comp);
		ro.setComparatorForSelected(comp);
		ro.setFixed(fixed);
		setModel(ro);
		final boolean bOK = run(
				getSpringLocaleDelegate().getMessage(
						"SubFormMultiEditController.3", "Mehrere Datens\u00e4tze in Unterformular einf\u00fcgen/l\u00f6schen"));

		if (bOK) {
			final List<?> lstNewSelectedObjects = this.getSelectedObjects();

			// 1. iterate through the table model and remove all rows that are not selected:
			for (int iRow = model.getRowCount() - 1; iRow >= 0; --iRow) {
				final CollectableField value = model.getValueAt(iRow, colIndex);
				// NUCLOS-6966 2) don't remove the NULLs as they cannot be selected.
				if (value.isIdField()) {
					if (value.getValueId() == null) {
						continue;
					}
				} else if (value.getValue() == null) {
					continue;
				}
				boolean blnRemove = true;
				for (Iterator iterator = lstNewSelectedObjects.iterator(); iterator.hasNext();) {
					CollectableField collectableField = (CollectableField) iterator.next();
					if (collectableField.isIdField() && value.isIdField()) {
						if (value.getValueId() != null && LangUtils.equal(collectableField.getValueId(), value.getValueId())) {
							blnRemove = false;
							break;
						}
					} else {
						if (LangUtils.equal(collectableField.getValue(), value.getValue())) {
							blnRemove = false;
							break;
						}
					}
				}
				if (blnRemove)
					model.remove(iRow);
				
			}

			
			List<Object> newFields = new ArrayList<Object>();
			// 2. iterate through the selected objects and add a row for each that is not contained in the table model already:
			for (Object oSelected : lstNewSelectedObjects) {
				
				if (!isContainedInTableModel(oSelected, model, colIndex)) {
					// add row
					newFields.add(oSelected);
					
				}
			}
			
			final Map<Clct, Object> mpNewFields = new HashMap<Clct, Object>();
			
			//NUCLOS-4890 First set create the new rows
			for (Object o : newFields) {
				try {
					mpNewFields.put(controller.insertNewRow(), o);					
				} catch (CommonBusinessException e) {
					Errors.getInstance().showExceptionDialog(getParent(), e);
				}
			}

			Runnable r = new Runnable() {
				@Override
				public void run() {
					setMultiEditValues(mpNewFields, model, controller.getCollectableEntity().getEntityField(columnName));
				}
			};
			
			//NUCLOS-4890 Then set the values and trigger Layout-ML Rules
			SwingUtilities.invokeLater(r);
					
		}
	}
	
	private void setMultiEditValues(final Map<Clct, Object> mpNewFields, final CollectableTableModel<PK, Clct> model, final CollectableEntityField clctefTarget) {
		
		for (Clct clct : mpNewFields.keySet()) {
			final int row = controller.getCollectables().indexOf(clct);
			final Object o = mpNewFields.get(clct);
			
			// try to set via editor to trigger layout rules (BMWFDM-758)
			CollectableComponentTableCellEditor tableCellEditor = (CollectableComponentTableCellEditor) controller.getTableCellEditor(controller.getJTable(), row, clctefTarget);
			if (tableCellEditor != null) {
				controller.getSubForm().getSubformTable().getSelectionModel().setSelectionInterval(row, row);
				tableCellEditor.getCollectableComponent().setField((CollectableField) o);						
			}
			
		}
		
		final int colIndex = controller.getTableColumns().indexOf(clctefTarget);
		
		for (Clct clct : mpNewFields.keySet()) {
			final int row = controller.getCollectables().indexOf(clct);
			final Object o = mpNewFields.get(clct);
			
			//clct.setField(columnName, (CollectableField) o);
			
			//BMWFDM-714: This could never work for Sammelbearbeitung, as we have to use the model, 
			//so the TableModelListeners are called and the SubFormCollectableMap transfers the values to the 
			//equivalence classes. Thus use the model:
			
			model.setValueAt(o, row, colIndex);
		}
		
		if (model instanceof SortableCollectableTableModel) {
			((SortableCollectableTableModel<PK, Clct>)model).sort();				
		}
	}

	private static SortedSet<CollectableField> getNonNullValues(LabeledCollectableComponentWithVLP clctcmbbx, Comparator<CollectableField> comp) {
		if (clctcmbbx.getValueListProvider() != null) {
			clctcmbbx.refreshValueList(false);
		}
		final List<CollectableField> result = clctcmbbx.getValueList();

		// remove the first entry, if it is null:
		if (!result.isEmpty()) {
			if (result.get(0).isNull()) {
				result.remove(0);
			}
		}
		final SortedSet<CollectableField> realResult = new TreeSet<CollectableField>(comp);
		realResult.addAll(result);
		return realResult;
	}

	private static <PK2,Clct2 extends Collectable<PK2>> boolean isContainedInTableModel(Object oSelected, 
			CollectableTableModel<PK2,Clct2> tblmdl, int iColumn) {
		boolean result = false;
		for (int iRow = 0; iRow < tblmdl.getRowCount(); ++iRow) {
			final CollectableField value = tblmdl.getValueAt(iRow, iColumn);
			final CollectableField collectableField = (CollectableField)oSelected;
			if (collectableField.isIdField() && value.isIdField()) {
				if (LangUtils.equal(collectableField.getValueId(), value.getValueId())) {
					result = true;
					break;
				}
			} else {
				if (LangUtils.equal(collectableField.getValue(), value.getValue())) {
					result = true;
					break;
				}
			}
		}
		return result;
	}

}	// class SubFormMultiEditController
