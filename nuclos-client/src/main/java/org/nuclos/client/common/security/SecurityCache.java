//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.swing.Action;

import org.apache.log4j.Logger;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.main.Main;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.common.Actions;
import org.nuclos.common.CommonSecurityCache;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common.security.NotifyObject;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.AttributePermissionKey;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.MasterDataPermission;
import org.nuclos.server.common.MasterDataPermissions;
import org.nuclos.server.common.ModulePermission;
import org.nuclos.server.common.ModulePermissions;
import org.nuclos.server.common.ejb3.SecurityFacadeRemote;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * caches client rights.
 * §todo move some code to the server side.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
// @Component
@ManagedResource(
		objectName="nuclos.client.cache:name=SecurityCache",
		description="Nuclos Client SecurityCache",
		currencyTimeLimit=15,
		log=false)
public class SecurityCache implements CommonSecurityCache, InitializingBean {
	
	private static final Logger LOG = Logger.getLogger(SecurityCache.class);
	
	private static SecurityCache INSTANCE;
	
	//
	
	private String username;
	private UID userUID;
	private Boolean superUser;
	private Boolean hasCommunicationAccountPhone;
	private Set<String> stAllowedActions;
	private Set<org.nuclos.api.UID> stAllowedCustomActions;

	private ModulePermissions modulepermissions;
	private MasterDataPermissions masterdatapermissions;
	
	private Map<UID, List<MandatorVO>> mandatorsByLevel;
	private Map<UID, MandatorVO> mandatorById;
	private Map<UID, String> userRoles;

	private final Map<AttributePermissionKey, Permission> mpAttributePermission
		= new ConcurrentHashMap<>();
	private final Map<UID, Map<UID, SubformPermission>> mpSubFormPermission
		= new ConcurrentHashMap<>();

	private final Map<UID, ModulePermission> modulePermissionsCache
		= new ConcurrentHashMap<>();
	private final Map<UID, MasterDataPermission> masterDataPermissionsCache
		= new ConcurrentHashMap<>();
	
	private final Map<String, List<Pair<String[], Action>>> mpMenuActions = new ConcurrentHashMap<>();
	
	private Set<UID> dynamicTasklists;
	
	// Spring injection
	
	private Timer timer;
	
	private TopicNotificationReceiver tnr;
	
	private SecurityDelegate securityDelegate;
	
	private AttributeCache attributeCache;
	
	
	// end of Spring injection

	private final MessageListener listener = new MessageListener() {
		@Override
        public void onMessage(Message msg) {
			boolean clearcache = false;
			boolean refreshMenus = true;
			if (msg instanceof ObjectMessage) {
				ObjectMessage om = (ObjectMessage) msg;
				try {
					final NotifyObject no = (NotifyObject) om.getObject();
					final String username = no.getUsername();
					if (StringUtils.isNullOrEmpty(username) || username.equals(username)) {
						clearcache = true;
					}
					refreshMenus = no.getRefreshMenus();
				} catch (JMSException e) {
					clearcache = true;
				}
			}
			if (clearcache) {
				LOG.info("onMessage: JMS trigger clearcache (security, attribute, menues) in " + this);
				SecurityCache.this.revalidate();
				attributeCache.revalidate();
				if (refreshMenus) {
					Main.getInstance().getMainController().refreshMenusLater();
				}
			}
		}
	};


	/**
	 * @return the one (and only) instance of SecurityCache
	 * 
	 * @deprecated Use Spring injection instead.
	 */
	public static SecurityCache getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("Too early");
		}
		return INSTANCE;
	}

	/**
	 * creates the cache. Fills in all the attributes from the database.
	 */
	SecurityCache() {
		INSTANCE = this;
	}
	
	@Autowired
	final void setTimer(Timer timer) {
		this.timer = timer;
	}
	
	// @PostConstruct
	public final void afterPropertiesSet() {
		tnr.subscribe(JMSConstants.TOPICNAME_SECURITYCACHE, listener);
		// do not validate before login!
//		final TimerTask task = new TimerTask() {
//			
//			@Override
//			public void run() {
//				try {
//					validate();
//				} catch (Exception e) {
//					LOG.error("initial validate failed: " + e, e);
//				}
//			}
//		};
//		timer.schedule(task, 300);
	}
	
	// @Autowired
	public final void setTopicNotificationReceiver(TopicNotificationReceiver tnr) {
		this.tnr = tnr;
	}
	
	// @Autowired
	public final void setSecurityDelegate(SecurityDelegate securityDelegate) {
		this.securityDelegate = securityDelegate;
	}
	
	// @Autowired
	// @Qualifier("attributeCache")
	public final void setAttributeCache(AttributeCache attributeCache) {
		this.attributeCache = attributeCache;
	}
	
	public synchronized boolean isActionAllowed(String sActionName) {
		if (stAllowedActions == null) {
			validate();
		}
		return stAllowedActions.contains(sActionName);
	}
	
	public synchronized boolean isCustomActionAllowed(UID sActionUID) {
		if (stAllowedCustomActions == null) {
			validate();
		}
		return stAllowedCustomActions.contains(sActionUID);
	}
	
	public synchronized Set<org.nuclos.api.UID> getCustomActionsAllowed() {
		if (stAllowedCustomActions == null) {
			validate();
		}
		return stAllowedCustomActions;
	}

	private ModulePermission getModulePermission(UID moduleUid, Long iGenericObjectId) {
		if (!modulePermissionsCache.containsKey(moduleUid)) {
			if (modulepermissions == null) {
				validate();
			}
			ModulePermission permission = modulepermissions.getMaxPermissionForGO(moduleUid);
			modulePermissionsCache.put(moduleUid, permission);
		}
		final ModulePermission result = modulePermissionsCache.get(moduleUid);
		if (result == ModulePermission.NO) {
			return null;
		}
		return result;
	}

	/**
	 * Must be synchronized because masterdatapermissions might be null.
	 */
	private synchronized MasterDataPermission getMasterDataPermission(UID entityUid) {
		if (!masterDataPermissionsCache.containsKey(entityUid)) {
			if (masterdatapermissions == null) {
				validate();
			}
			MasterDataPermission permission = masterdatapermissions.get(entityUid);
			if (permission == null) {
				permission = MasterDataPermission.NO;
			}
			masterDataPermissionsCache.put(entityUid, permission);
		}
		final MasterDataPermission result = masterDataPermissionsCache.get(entityUid);
		if (result == MasterDataPermission.NO) {
			return null;
		}
		return result;
	}

	public boolean isReadAllowedForModule(UID moduleUid, Long iGenericObjectId) {
		return ModulePermission.includesReading(getModulePermission(moduleUid, iGenericObjectId));
	}

	public boolean isReadAllowedForMasterData(UID entityUid) {
		if (E.REPORTEXECUTION.checkEntityUID(entityUid)) {
			return isActionAllowed(Actions.ACTION_EXECUTE_REPORTS);
		}
		return MasterDataPermission.includesReading(getMasterDataPermission(entityUid));
	}

	/**
	 * Check, whether reading is allowed for any type of entity
	 * @param entityUid  the name
	 * @return boolean
	 */
	public boolean isReadAllowedForEntity(UID entityUid) {
		return isReadAllowedForMasterData(entityUid)
		   ||  isReadAllowedForModule(entityUid, null);
	}

	public boolean isReadAllowedForMasterData(EntityMeta<?> entity) {
		return isReadAllowedForMasterData(entity.getUID());
	}

	public boolean isNewAllowedForModule(UID moduleUid) {
		if (modulepermissions == null) {
			validate();
		}
		return LangUtils.defaultIfNull(modulepermissions.getNewAllowedByModuleUid().get(moduleUid), false);
	}

	public boolean isNewAllowedForModuleAndProcess(UID moduleUid, UID processUid) {
		if (modulepermissions == null) {
			validate();
		}
		final Set<UID> processes = modulepermissions.getNewAllowedProcessesByModuleUid().get(moduleUid);
		return processes != null && processes.contains(processUid);
	}

	public boolean isWriteAllowedForModule(UID moduleUid, Long iGenericObjectId) {
		return ModulePermission.includesWriting(getModulePermission(moduleUid, iGenericObjectId));
	}

	public boolean isWriteAllowedForMasterData(UID entityUid) {
		return MasterDataPermission.includesWriting(getMasterDataPermission(entityUid));
	}

	public boolean isWriteAllowedForMasterData(EntityMeta<?> entity) {
		return isWriteAllowedForMasterData(entity.getUID());
	}

	public boolean isDeleteAllowedForModule(UID moduleUid, Long iGenericObjectId, boolean physically) {
		ModulePermission modulePermission = getModulePermission(moduleUid, iGenericObjectId);
		return physically
			? ModulePermission.includesDeletingPhysically(modulePermission)
			: ModulePermission.includesDeletingLogically(modulePermission);
	}

	public boolean isDeleteAllowedForMasterData(UID entityUid) {
		return MasterDataPermission.includesDeleting(getMasterDataPermission(entityUid));
	}

	/**
	 * revalidates this cache: clears it, then fills in all the attributes from the server again.
	 * <p>
	 * Must be synchronized because masterdatapermissions is set to null (via validate).
	 * </p>
	 */
	public synchronized void revalidate() {
		this.superUser = Boolean.FALSE;
		this.hasCommunicationAccountPhone = Boolean.FALSE;
		this.stAllowedActions = null;
		this.modulepermissions = null;
		this.masterdatapermissions = null;
		this.dynamicTasklists = null;
		this.mandatorsByLevel = null;
		this.mandatorById = null;
		this.userRoles = null;
		
		this.mpAttributePermission.clear();
		this.mpSubFormPermission.clear();
		modulePermissionsCache.clear();
		masterDataPermissionsCache.clear();
		mpMenuActions.clear();
		LOG.info("Cleared cache " + this);
		validate();
	}

	/**
	 * fills this cache.
	 * 
	 * @throws NuclosFatalException
	 */
    private void validate() throws NuclosFatalException {
		Map<String, Object> iniData = securityDelegate.getInitialSecurityData();
		this.username = (String) iniData.get(SecurityFacadeRemote.USERNAME);
		this.userUID = (UID) iniData.get(SecurityFacadeRemote.USERUID);
		this.superUser = (Boolean) iniData.get(SecurityFacadeRemote.IS_SUPER_USER);
		this.hasCommunicationAccountPhone = (Boolean) iniData.get(SecurityFacadeRemote.HAS_COMMUNICATION_ACCOUNT_PHONE);
		this.stAllowedActions = (Set<String>) iniData.get(SecurityFacadeRemote.ALLOWED_ACTIONS);
		this.stAllowedCustomActions = (Set<org.nuclos.api.UID>) iniData.get(SecurityFacadeRemote.ALLOWED_CUSTOM_ACTIONS);
		this.modulepermissions = (ModulePermissions) iniData.get(SecurityFacadeRemote.MODULE_PERMISSIONS);
		this.masterdatapermissions = (MasterDataPermissions) iniData.get(SecurityFacadeRemote.MASTERDATA_PERMISSIONS);
		this.dynamicTasklists = (Set<UID>) iniData.get(SecurityFacadeRemote.DYNAMIC_TASKLISTS);
		this.mandatorsByLevel = (Map<UID, List<MandatorVO>>) iniData.get(SecurityFacadeRemote.MANDATORS_BY_LEVEL);
		Map<UID, MandatorVO> tmpMandatorById = new HashMap<UID, MandatorVO>();
		for (List<MandatorVO> lstMandator : mandatorsByLevel.values()) {
			for (MandatorVO mandator : lstMandator) {
				tmpMandatorById.put(mandator.getUID(), mandator);
			}
		}
		this.mandatorById = Collections.unmodifiableMap(tmpMandatorById);
		this.userRoles= RigidUtils.uncheckedCast(iniData.get(SecurityFacadeRemote.USER_ROLES));
		LOG.info("Validated (filled) cache " + this);
	}

	/**
	 * get permissions for a subform
	 * NOTE: this method makes only sense for subforms placed in modules
	 * 
	 * @param subformUid
	 * @return a map of state id's and the corresponding permission
	 */
	public Map<UID, SubformPermission> getSubFormPermission(UID subformUid) {
		if (!mpSubFormPermission.containsKey(subformUid)) {
			Map<UID, SubformPermission> permission = securityDelegate.getSubFormPermission(subformUid);
			mpSubFormPermission.put(subformUid, permission);
		}
		return mpSubFormPermission.get(subformUid);
	}
	
	public Map<String, List<Pair<String[], Action>>> getMpMenuActions() {
		return mpMenuActions;
	}

	/**
	 * get the permission for a subform within the given state id
	 * NOTE: this method makes only sense for subforms placed in modules
	 * 
	 * @param entityUid
	 * @param iState
	 * @return Permission
	 */
	public SubformPermission getSubFormPermission(UID entityUid, UID iState) {
		return getSubFormPermission(entityUid).get(iState);
	}

	/**
	 * get the permission for an attribute within the given stateId
	 * @param entityUid         the entity
	 * @param attributeUid  the attribute
	 * @param stateId        the state
	 * @return Permission
	 */
	public Permission getAttributePermission(UID entityUid, UID attributeUid, UID stateId) {
		if (CalcAttributeUtils.isCalcAttributeCustomization(attributeUid)) {
			FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(attributeUid);
			attributeUid = LangUtils.defaultIfNull(fMeta.getCalcBaseFieldUID(), attributeUid);
		}
		AttributePermissionKey key = new AttributePermissionKey(entityUid, attributeUid, stateId);
		if(!mpAttributePermission.containsKey(key)) {
			Map<UID, Permission> attrPermissions
				= securityDelegate.getAttributePermissionsByEntity(entityUid, stateId);
			for(Map.Entry<UID, Permission> e : attrPermissions.entrySet()) {
				Permission perm = e.getValue();
				if (perm == null) {
					perm = Permission.NONE;
				}
				mpAttributePermission.put(
					new AttributePermissionKey(entityUid, e.getKey(), stateId),
					perm);
			}
		}
		final Permission result = mpAttributePermission.get(key);
		if (result == Permission.NONE) {
			return null;
		}
		return result;
	}

	public boolean isReadAllowedForDynamicTasklist(UID uid) {
		if (isSuperUser()) {
			return true;
		}
		if (dynamicTasklists == null) {
			validate();
		}
		if (dynamicTasklists == null) {
			return false;
		}
		return dynamicTasklists.contains(uid);
	}
	public Boolean isSuperUser() {
		return superUser;
	}
	
	public Boolean isMaintenanceUser() {
		return isActionAllowed(Actions.ACTION_MAINTENANCE_MODE);
	}
	
	public Boolean isNucletExportUser() {
		return isActionAllowed(Actions.ACTION_NUCLET_EXPORT);
	}
	
	public Boolean isNucletImportUser() {
		return isActionAllowed(Actions.ACTION_NUCLET_IMPORT);
	}
	
	public Boolean isNucletAssignUser() {
		return isActionAllowed(Actions.ACTION_NUCLET_ASSIGN);
	}
	
	public Boolean hasCommunicationAccountPhone() {
		return hasCommunicationAccountPhone;
	}
	
	/**
	 * 
	 * @param levelUID
	 * @return sorted list of mandator vo
	 */
	public List<MandatorVO> getMandatorsByLevel(UID levelUID) {
		if (mandatorsByLevel == null) {
			validate();
		}
		List<MandatorVO> result = mandatorsByLevel.get(levelUID);
		if (result == null) {
			result = Collections.unmodifiableList(new ArrayList<MandatorVO>());
		}
		return result;
	}
	
	public MandatorVO getMandatorById(UID mandatorUID) {
		if (mandatorById == null) {
			validate();
		}
		MandatorVO result = mandatorById.get(mandatorUID);
		if (result == null) {
			throw new NuclosFatalException("Mandator with ID " + mandatorUID.getString() + " not found!");
		}
		return result;
	}
	
	/**
	 * @deprecated SecurityCache is not a good place for this method.
	 */
	public String getUsername() {
		return username;
	}

	@Override
	public UID getUserUid(final String sUserName) {
		if (!this.username.equals(sUserName)) {
			throw new IllegalArgumentException("getUserUid only for username \"" + username + "\" allowed. (Param=" + sUserName + ")");
		}
		return userUID;
	}

	public UID getUserUID() {
		return userUID;
	}

	@ManagedAttribute(description="get the size of mpAttributePermission")
	public int getNumberOfAttributePermsInCache() {
		return mpAttributePermission.size();
	}
	
	@ManagedAttribute(description="get the size of mpSubFormPermission")
	public int getNumberOfSubFormPermsInCache() {
		return mpSubFormPermission.size();
	}
	
	@ManagedAttribute(description="get the size of modulePermissionsCache")
	public int getNumberOfModulePermsInCache() {
		return modulePermissionsCache.size();
	}
	
	@ManagedAttribute(description="get the size of masterDataPermissionsCache")
	public int getNumberOfMasterDataPermsInCache() {
		return masterDataPermissionsCache.size();
	}
	
	@ManagedAttribute(description="get the size of dynamicTasklists")
	public int getNumberOfDynamicTasklistsInCache() {
		if (dynamicTasklists == null) {
			return 0;
		}
		return dynamicTasklists.size();
	}

	@Override
	public boolean isReadAllowedForEntity(final String user, final UID entityUID, final UID mandator) {
		if (!StringUtils.equals(this.username, user)) {
			throw new IllegalArgumentException("this.user=" + this.username + ", arg.user=" + user);
		}
		if (!RigidUtils.equal(ClientMandatorContext.getMandatorUID(), mandator)) {
			throw new IllegalArgumentException("clientcontext.mandator=" + ClientMandatorContext.getMandatorUID() + ", arg.mandator=" + mandator);
		}
		return isReadAllowedForEntity(entityUID);
	}

	@Override
	public boolean isActionAllowed(final String user, final String action, final UID mandator) {
		if (!StringUtils.equals(this.username, user)) {
			throw new IllegalArgumentException("this.user=" + this.username + ", arg.user=" + user);
		}
		if (!RigidUtils.equal(ClientMandatorContext.getMandatorUID(), mandator)) {
			throw new IllegalArgumentException("clientcontext.mandator=" + ClientMandatorContext.getMandatorUID() + ", arg.mandator=" + mandator);
		}
		return isActionAllowed(action);
	}
	
	public boolean isInGroup(String sGroup, boolean bWithSuperUser) {
		if (userRoles == null) {
			validate();
		}
		if (bWithSuperUser && isSuperUser()) {
			return true;
		}
		return userRoles.containsValue(sGroup);
	}
}	// class SecurityCache
