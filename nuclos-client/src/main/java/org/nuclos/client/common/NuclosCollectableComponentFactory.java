//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import org.nuclos.client.genericobject.CollectableGenericObjectFileChooser;
import org.nuclos.client.ui.collect.component.CollectableCheckBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.client.ui.collect.component.CollectableDateChooser;
import org.nuclos.client.ui.collect.component.CollectableEmail;
import org.nuclos.client.ui.collect.component.CollectableHiddenField;
import org.nuclos.client.ui.collect.component.CollectableHyperlink;
import org.nuclos.client.ui.collect.component.CollectableHyperlinkRef;
import org.nuclos.client.ui.collect.component.CollectablePhoneNumber;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.DefaultCollectableComponentFactory;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;

/**
 * Factory that creates <code>CollectableComponent</code>s in Nucleus.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class NuclosCollectableComponentFactory extends DefaultCollectableComponentFactory {

	private Color colorFocusBackground = null;
	private FocusListener focuslistener = null;

	public CollectableComponent newCollectableComponent(CollectableEntity clcte, UID sFieldName, CollectableComponentType clctcomptype, boolean bSearchable) {
		final CollectableComponent result = this.newCollectableComponent(clcte.getEntityField(sFieldName), clctcomptype, bSearchable);
		result.setCollectableEntity(clcte);
		return result;
	}

	@Override
	public CollectableComponent newCollectableComponent(CollectableEntityField clctef,
			CollectableComponentType clctcomptype, boolean bSearchable) {

		final CollectableComponent result = super.newCollectableComponent(clctef, clctcomptype, bSearchable);

		this.setFocusListener(result);

		return result;
	}
	
	@Override
	protected CollectableComponent newCollectableComponentByEnumeratedControlType(CollectableEntityField clctef, int iiControlType, boolean bSearchable, boolean bRenderer, boolean bSubform) {
		final CollectableComponent result;
		
		if (bRenderer && !bSearchable) {
			switch (iiControlType) {
				case CollectableComponentTypes.TYPE_COMBOBOX:
				case CollectableComponentTypes.TYPE_DATECHOOSER:
					return super.newCollectableComponentByEnumeratedControlType(clctef, CollectableComponentTypes.TYPE_TEXTFIELD, false, true, bSubform);
			}
		}

		switch (iiControlType) {
			case CollectableComponentTypes.TYPE_COMBOBOX:
				if (enableLoveEngineForComoBox() && clctef.isReferencing() && !MetaProvider.getInstance().isNuclosEntity(clctef.getEntityUID())) {
					result = new NuclosDropDown(clctef, bSearchable);
				} else {
					result = new NuclosCollectableComboBox(clctef, bSearchable);					
				}
				break;

			case CollectableComponentTypes.TYPE_TEXTAREA:
				
				if (clctef.isLocalized() && !clctef.isCalculated()) {
					result = new NuclosCollectableLocalizedTextArea(clctef, bSearchable);
				} else {
					result = new NuclosCollectableTextArea(clctef, bSearchable);					
				}
				break;

			case CollectableComponentTypes.TYPE_DATECHOOSER:
				// NucleusCollectableDataChooser has set bTodayIsRelative=bSearchable
				result = new CollectableDateChooser(clctef, bSearchable, bSearchable);
				break;
				
			case CollectableComponentTypes.TYPE_HYPERLINK:
				if (clctef.isReferencing() || bSubform && clctef.getReferencedEntityUID() != null) {
					result = new CollectableHyperlinkRef(clctef, bSearchable);
				}
				else {
					result = new CollectableHyperlink(clctef, bSearchable);
				}
				break;
				
			case CollectableComponentTypes.TYPE_EMAIL:
				result = new CollectableEmail(clctef, bSearchable);
				break;
				
			case CollectableComponentTypes.TYPE_PHONENUMBER:
				result = new CollectablePhoneNumber(clctef, bSearchable);
				break;

			case CollectableComponentTypes.TYPE_IDTEXTFIELD:
				result = new NuclosCollectableIdTextField(clctef, bSearchable);
				break;

			case CollectableComponentTypes.TYPE_LISTOFVALUES:
				result = new NuclosCollectableListOfValues(clctef, bSearchable);
				break;

			case CollectableComponentTypes.TYPE_FILECHOOSER:
				if (bSearchable)
					result = new CollectableTextField(clctef, bSearchable);	
				else
					result = new CollectableGenericObjectFileChooser(clctef, bSearchable);
				break;
				
			case CollectableComponentTypes.TYPE_IMAGE:
				result = new NuclosCollectableImage(clctef, bSearchable);
				break;
				
			case CollectableComponentTypes.TYPE_PASSWORDFIELD:
				//NUCLEUSINT-1142
				result = new NuclosCollectablePasswordField(clctef, bSearchable);
				break;

			case CollectableComponentTypes.TYPE_HIDDEN:
				result = new CollectableHiddenField(clctef);
				break;

			default:
				result = super.newCollectableComponentByEnumeratedControlType(clctef, iiControlType, bSearchable, bRenderer, bSubform);
		}
		return result;
	}

	private void setFocusListener(CollectableComponent clctcomp) {
		if (colorFocusBackground == null) {
			this.initializeFocusListener();
		}

		/*
		 * CollectableDateChooser & CollectableCheckBox has own Focuslistener
		 */
		if (focuslistener != null && !(clctcomp instanceof CollectableDateChooser || clctcomp instanceof CollectableCheckBox)) {
			clctcomp.getFocusableComponent().addFocusListener(focuslistener);
		}
	}

	private void initializeFocusListener() {
		colorFocusBackground = Utils.translateColorFromParameter(ParameterProvider.KEY_FOCUSSED_ITEM_BACKGROUND_COLOR);

		focuslistener = new FocusListener() {
			private Color color;

			@Override
            public void focusLost(FocusEvent ev) {
				ev.getComponent().setBackground(color);
			}

			@Override
            public void focusGained(FocusEvent ev) {
				color = ev.getComponent().getBackground();
				ev.getComponent().setBackground(colorFocusBackground);
			}
		};
	}

}	// class NuclosCollectableComponentFactory
