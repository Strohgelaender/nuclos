//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.collect.CollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultEditView;
import org.nuclos.client.ui.collect.EditView;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.caching.NBCache;
import org.nuclos.common.caching.NBCache.LookupProvider;
import org.nuclos.common.caching.TimedCache;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.masterdata.MakeMasterDataValueIdField;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Utility methods for Nucleus client.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class Utils {

	private static final Logger LOG = Logger.getLogger(Utils.class);

	private static final String FIELDNAME_NAME = "name";
	
	private static final String FIELDNAME_ACTIVE = "active";
	private static final String FIELDNAME_VALIDFROM = "validFrom";
	private static final String FIELDNAME_VALIDUNTIL = "validUntil";

	private Utils() {
	}

	private static UID getFieldUidByName(UID entity, String fieldname) {
		for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity).values()) {
			if (field.getFieldName().equals(fieldname)) {
				return field.getUID();
			}
		}
		return null;
	}
	/**
	 * @deprecated use <code>EntityObjectDelegate.getCollectableFieldsByName</code>
	 *
	 * get masterdata for the given entity as collectable fields.
	 * @param entityUid
	 * @param collmdvo
	 * @param stringifiedFieldDefinition masterdata field name
	 * @param bCheckValidity Test for active sign and validFrom/validUntil
	 * @return list of collectable fields
	 */
	@Deprecated
	public static synchronized <PK> List<CollectableField> getCollectableFieldsByName(UID entityUid,
			Collection<MasterDataVO<PK>> collmdvo, String stringifiedFieldDefinition, boolean bCheckValidity) {

		final Collection<MasterDataVO<PK>> collmdvoFiltered = bCheckValidity ? selectValidAndActive(entityUid, collmdvo) : collmdvo;
		return CollectionUtils.transform(collmdvoFiltered, new MakeMasterDataValueIdField(entityUid, stringifiedFieldDefinition));
	}

	/**
	 * 
	 */
	public static UID getNameIdentifierField(UID entity) {
		return getFieldUidByName(entity, FIELDNAME_NAME);
	}

	/**
	 * Tests whether the entity has {@code validFrom}/{@code validTo} or {@code active} fields.
	 */
	public static boolean hasValidOrActiveField(UID entity) {
		final Collection<UID> collFieldNames = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(entity).getFieldUIDs();
		final boolean bContainsActive = collFieldNames.contains(getFieldUidByName(entity, FIELDNAME_ACTIVE));
		final boolean bContainsValidFromAndUntil = collFieldNames.contains(getFieldUidByName(entity, FIELDNAME_VALIDFROM)) &&
				collFieldNames.contains(getFieldUidByName(entity, FIELDNAME_VALIDUNTIL));
		return bContainsActive || bContainsValidFromAndUntil;
	}

	/**
	 * selects the valid and/or active (if applicable) entries from the given Collection.
	 * @param sEntity
	 * @param collmdvo
	 * @return Collection<MasterDataVO>
	 * @todo define Predicate MasterDataVO.IsValidAndActive
	 */
	private static <PK> Collection<MasterDataVO<PK>> selectValidAndActive(UID entity, Collection<MasterDataVO<PK>> collmdvo) {
		final List<MasterDataVO<PK>> result = new ArrayList<MasterDataVO<PK>>();

		final Collection<UID> collFieldNames = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(entity).getFieldUIDs();
		final boolean bContainsActive = collFieldNames.contains(getFieldUidByName(entity, FIELDNAME_ACTIVE));
		final boolean bContainsValidFromAndUntil = collFieldNames.contains(getFieldUidByName(entity, FIELDNAME_VALIDFROM)) &&
				collFieldNames.contains(getFieldUidByName(entity, FIELDNAME_VALIDUNTIL));
		final Date dateNow = new Date(System.currentTimeMillis());

		for (MasterDataVO<?> mdvo : collmdvo) {
			// separate valid entries...
			boolean bAddToResult = true;
			if (bContainsActive) {
				final Boolean bActive = (Boolean) mdvo.getFieldValue(getFieldUidByName(entity, FIELDNAME_ACTIVE));
				bAddToResult = (bActive != null) && bActive.booleanValue();
			}
			if (bAddToResult) {
				if (bContainsValidFromAndUntil) {
					final Date dateValidFrom = (Date) mdvo.getFieldValue(getFieldUidByName(entity, FIELDNAME_VALIDFROM));
					final Date dateValidUntil = (Date) mdvo.getFieldValue(getFieldUidByName(entity, FIELDNAME_VALIDUNTIL));

					bAddToResult =
							(dateValidFrom == null || dateValidFrom.before(dateNow)) &&
									(dateValidUntil == null || dateValidUntil.after(dateNow));
				}
			}
			if (bAddToResult) {
				result.add((MasterDataVO<PK>) mdvo);
			}
		}
		return result;
	}

	/**
	 * does for the given *dirty* <code>Collectable</code> this:
	 * Booleans that are <code>null</code> are mapped to <code>false</code>.
	 * 
	 * §precondition clct != null
	 * §precondition clct.isComplete()
	 * §precondition clcte != null
	 * 
	 * @param clct
	 * @param clcte
	 */
	public static void mapNullBooleansToFalseInCollectable(Collectable<?> clct, CollectableEntity clcte) {
		if (!clct.isDirty()) return;
		for (UID sFieldName : clcte.getFieldUIDs()) {
			final CollectableEntityField clctef = clcte.getEntityField(sFieldName);
			if (clctef.getJavaClass().equals(Boolean.class) && clct.getField(sFieldName).isNull()) {
				clct.setField(sFieldName, new CollectableValueField(Boolean.FALSE));
			}
		}
	}

	/**
	 * prepares the given dependents for saving:
	 * Booleans that are <code>null</code> are mapped to <code>false</code>.
	 */
	public static void mapNullBooleansToFalseInDependents(DependantCollectableMasterDataMap mpDependents) {
		for (IDependentKey dependentKey : mpDependents.getKeySet()) {
			FieldMeta<?> refFieldMeta = MetaProvider.getInstance().getEntityField(dependentKey.getDependentRefFieldUID());
			final CollectableEntity clcte = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(refFieldMeta.getEntity());
			for (CollectableMasterData<?> clctmd : mpDependents.getValues(dependentKey)) {
				mapNullBooleansToFalseInCollectable(clctmd, clcte);
			}
		}
	}

	/**
	 * §precondition coll != null
	 * 
	 * @param coll
	 * @return the common object, if any, based on equals. If all objects in the given Collection are equal, the first object is returned.
	 * Otherwise, <code>null</code> is returned.
	 */
	public static <E> E getCommonObject(Collection<E> coll) {
		if (coll == null) {
			throw new NullArgumentException("coll");
		}
		E result = null;
		for (E e : coll) {
			if (result == null) {
				result = e;
			}
			else if (!result.equals(e)) {
				result = null;
				break;
			}
		}
		return result;
	}

	/**
	 * sets the names for all fields in the given object that are <code>java.awt.Component</code>s.
	 * 
	 * §todo move to org.nuclos.client.ui.UIUtils eventually.
	 */
	public static void setComponentNames(ComponentNameSetter o) {
		setComponentNames(o, o.getClass().getDeclaredFields());
	}

	private static void setComponentNames(ComponentNameSetter o, Field[] aDeclaredFields) {
		for (Field field : aDeclaredFields) {
			if (Component.class.isAssignableFrom(field.getType())) {
				o.setComponentName(field);
			}
		}
	}

	/**
	 * Read a comma separated String (R,G,B) from the parameter provider and convert it into a color if possible.
	 * @param sParameter
	 * @return the newly created color or Color.BLACK if not possible to read the parameter
	 */
	public static Color translateColorFromParameter(String sParameter) {
		return translateColorFromParameter(sParameter, Color.BLACK);
	}
	
	/**
	 * Read a comma separated String (R,G,B) from the parameter provider and convert it into a color if possible.
	 * @param sParameter
	 * @return the newly created color or defaultIfNull if not possible to read the parameter
	 */
	public static Color translateColorFromParameter(String sParameter, Color defaultIfNull) {
		Color result = defaultIfNull;
		final String sColor = ClientParameterProvider.getInstance().getValue(sParameter);
		if (sColor != null) {
			final String[] saColors = sColor.split(",");
			if (saColors.length == 3) {
				result = new Color(
						Integer.parseInt(saColors[0]),
						Integer.parseInt(saColors[1]),
						Integer.parseInt(saColors[2]));
			}
		}
		return result;
	}

	public static String colorToHexString(Color c) {
		char[] buf = new char[7];
		buf[0] = '#';
		String s = Integer.toHexString(c.getRed());
		if (s.length() == 1) {
			buf[1] = '0';
			buf[2] = s.charAt(0);
		}
		else {
			buf[1] = s.charAt(0);
			buf[2] = s.charAt(1);
		}
		s = Integer.toHexString(c.getGreen());
		if (s.length() == 1) {
			buf[3] = '0';
			buf[4] = s.charAt(0);
		}
		else {
			buf[3] = s.charAt(0);
			buf[4] = s.charAt(1);
		}
		s = Integer.toHexString(c.getBlue());
		if (s.length() == 1) {
			buf[5] = '0';
			buf[6] = s.charAt(0);
		}
		else {
			buf[5] = s.charAt(0);
			buf[6] = s.charAt(1);
		}
		return String.valueOf(buf);
	}
	
	public static Color getBestForegroundColor(Color background) {
		int backgroundBrightness = (background.getBlue() + background.getRed()*2 + background.getGreen()*3) / 6;
		return backgroundBrightness > 160 ? Color.BLACK: Color.WHITE;
	}

	public static void setInitialComponentFocus(EditView editView, Map<UID, ? extends SubFormController> mpsubformctl) {
		if ((editView instanceof DefaultEditView) && ((DefaultEditView) editView).getInitialFocusField() != null) {
			// frame can be null if bShowWarnings is false
			setInitialComponentFocus(((DefaultEditView) editView).getInitialFocusField(), editView, mpsubformctl, null, false);
		}
	}

	/**
	 * sets the input focus to a certain collectable component in a LayoutML mask.
	 * 
	 * §precondition eafnInitialFocus != null
	 * 
	 * @param eafnInitialFocus entity name and field name of the component that is to receive to focus. The entity name is
	 * null, if the component is not in a subform.
	 * @param clctcompprovider map of all collectable components in the layout
	 * @param mpsubformctl map of all subformcontrollers
	 * @param frame frame of the layout (for possible warning dialogs only)
	 * @param bShowWarnings displays warnings for components not found for focussing
	 */
	public static void setInitialComponentFocus(EntityAndField eafnInitialFocus,
			final CollectableComponentsProvider clctcompprovider, final Map<UID, ? extends SubFormController> mpsubformctl,
			final MainFrameTab frame, final boolean bShowWarnings) {

		final UID sInitialFocusEntity = eafnInitialFocus.getEntity();
		final UID sInitialFocusField = eafnInitialFocus.getField();

		// Must be invoked later, else focus is not set with compound components like LOVs
		EventQueue.invokeLater(new Runnable() {
			@Override
            public void run() {
				try {
					if (sInitialFocusEntity == null) {
						if (sInitialFocusField != null) {
							final Collection<CollectableComponent> collclctcomp = clctcompprovider.getCollectableComponentsFor(sInitialFocusField);
							if (collclctcomp.isEmpty()) {
								if (bShowWarnings) {
									final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
											"ClientUtils.1", "Das angegebene Feld f\u00fcr den initialen Fokus existiert nicht.");
									JOptionPane.showMessageDialog(frame, sMessage, SpringLocaleDelegate.getInstance().getMessage("ClientUtils.2", "Hinweis"),
											JOptionPane.WARNING_MESSAGE);
								}
							}
							else {
								final CollectableComponent clctcomp = collclctcomp.iterator().next();
								final JComponent compFocus = clctcomp.getFocusableComponent();
								compFocus.requestFocusInWindow();
							}
						}
					}
					else {
						final SubFormController subformctl = mpsubformctl.get(sInitialFocusEntity);
						if (subformctl != null) {
							final SubFormTableModel subformtblmdl = (SubFormTableModel) subformctl.getSubForm().getJTable().getModel();

							final JTable tbl = subformctl.getSubForm().getJTable();
							final int iColumn = tbl.convertColumnIndexToView(subformtblmdl.findColumnByFieldUid(sInitialFocusField));
							if (iColumn != -1) {
								if (subformtblmdl.getRowCount() > 0) {
									tbl.editCellAt(0, iColumn);

									SwingUtilities.invokeLater(new Runnable() {
										@Override
										public void run() {
											Component editor = tbl.getEditorComponent();
											if (editor != null)
												editor.requestFocusInWindow();
										}
									});
								}
							}
							else {
								if (bShowWarnings) {
									final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
											"ClientUtils.3", "Das angegebene Feld in der Entit\u00e4t f\u00fcr den initialen Fokus existiert nicht.");
									JOptionPane.showMessageDialog(frame, sMessage,
											SpringLocaleDelegate.getInstance().getMessage("ClientUtils.2", "Hinweis"),
											JOptionPane.WARNING_MESSAGE);
								}
							}
						}
						else {
							if (bShowWarnings) {
								final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
										"ClientUtils.4", "Die angegebene Entit\u00e4t f\u00fcr den initialen Fokus existiert nicht.");
								JOptionPane.showMessageDialog(frame, sMessage,
										SpringLocaleDelegate.getInstance().getMessage("ClientUtils.2", "Hinweis"),
										JOptionPane.WARNING_MESSAGE);
							}
						}
					}
				}
				catch (Exception e) {
					LOG.error("setInitialComponentFocus failed: " + e, e);
				}
			}
		});
	}


	/**
	 * sets the input focus to a certain collectable component in a LayoutML mask.
	 * 
	 * §precondition eafnInitialFocus != null
	 * 
	 * @param focusFieldUid field name of the component that is to receive to focus.
	 * @param clctcompprovider map of all collectable components in the layout
	 * @param frame frame of the layout (for possible warning dialogs only)
	 * @param bShowWarnings displays warnings for components not found for focussing
	 */
	public static void setComponentFocus(final UID focusFieldUid,
			final CollectableComponentsProvider clctcompprovider,
			final MainFrameTab frame, final boolean bShowWarnings) {
		// Must be invoked later, else focus is not set with compound components like LOVs
		EventQueue.invokeLater(new Runnable() {
			@Override
            public void run() {
				try {
					if (focusFieldUid != null) {
						final Collection<CollectableComponent> collclctcomp = clctcompprovider.getCollectableComponentsFor(focusFieldUid);
						if (collclctcomp.isEmpty()) {
							if (bShowWarnings) {
								final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
										"ClientUtils.1", "Das angegebene Feld f\u00fcr den initialen Fokus existiert nicht.");
								JOptionPane.showMessageDialog(frame, sMessage, SpringLocaleDelegate.getInstance().getMessage("ClientUtils.2", "Hinweis"),
										JOptionPane.WARNING_MESSAGE);
							}
						}
						else {
							final CollectableComponent clctcomp = collclctcomp.iterator().next();
							final JComponent compFocus = clctcomp.getFocusableComponent();
							compFocus.requestFocusInWindow();
						}
					}
				}
				catch (Exception e) {
					LOG.error("setComponentFocus failed: " + e, e);
				}
			}
		});
	}

	/**
	 * Returns a {@code Collectable} object for the given entity and id.
	 * This method works for master data as well as generic objects.
	 * 
	 * @param entityUid UID (valid types are Integer or Long)
	 */
	public static <PK> Collectable<PK> getCollectable(UID entityUid, PK id) throws CommonBusinessException {
		if (entityUid == null)
			throw new IllegalArgumentException("entityName is null");
		if (!(id == null || id instanceof Integer || id instanceof Long)) {
			throw new IllegalArgumentException("id must be Integer or Long");
		}
		final Long intId = (id != null) ? ((Long) id) : null;
		Collectable<PK> clct = null;
		if (intId != null) {
			if (Modules.getInstance().isModule(entityUid)) {
				UID moduleId = Modules.getInstance().getModule(entityUid).getUID();
				if (moduleId != null) {
					GenericObjectVO govo = GenericObjectDelegate.getInstance().get(moduleId, intId);
					clct = (Collectable<PK>) CollectableGenericObjectWithDependants.newCollectableGenericObject(govo);
				}
			} else {
				EntityMeta<PK> metaData = (EntityMeta<PK>) MasterDataDelegate.getInstance().getMetaData(entityUid);
				CollectableMasterDataEntity clcte = new CollectableMasterDataEntity(metaData);
				MasterDataVO<PK> mdvo = (MasterDataVO<PK>)
						MasterDataDelegate.getInstance().getWithDependants(entityUid, intId);
				clct = new CollectableMasterDataWithDependants<PK>(clcte, mdvo);
			}
		}
		return clct;
	}

	public static <PK> Collectable<PK> getReferencedCollectable(UID referencingEntity, UID referencingEntityField, PK oId) throws CommonBusinessException {
		Collectable<PK> clct = null;
		if (oId != null) {
			EntityObjectVO<PK> eo = (EntityObjectVO<PK>) 
					EntityObjectDelegate.getInstance().getReferenced(referencingEntity, referencingEntityField, oId);
			if (eo != null) {
				FieldMeta<?> field = MetaProvider.getInstance().getEntityField(referencingEntityField);
				EntityMeta<PK> referencedMeta = (EntityMeta<PK>) 
						MetaProvider.getInstance().getEntity(field.getForeignEntity() != null 
						? field.getForeignEntity() 
						: field.getLookupEntity());
				if (referencedMeta.isStateModel()) {
					Map<UID, FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(referencedMeta.getUID());
					CollectableEOEntity clcte = new CollectableEOEntity(referencedMeta);
					GenericObjectVO govo = DalSupportForGO.getGenericObjectVO((EntityObjectVO<Long>) eo, clcte);
					clct = (Collectable<PK>) CollectableGenericObjectWithDependants.newCollectableGenericObjectWithDependants(govo);
				}
				else {
					EntityMeta<?> metaData = MasterDataDelegate.getInstance().getMetaData(referencedMeta.getUID());
					CollectableMasterDataEntity clcte = new CollectableMasterDataEntity(metaData);
					MasterDataVO<PK> mdvo = DalSupportForMD.wrapEntityObjectVO(eo);
					clct = new CollectableMasterData<PK>(clcte, mdvo);
				}
			}
		}
		return clct;
	}
	
	public static <PK> Collectable<PK> getCachedReferencedCollectable(UID referencingEntity, UID referencingEntityField, PK oId) throws CommonBusinessException {
		try {
			return (Collectable<PK>)TIMEDREFCACHE.get(new Pair<UID, Object>(referencingEntityField, oId));
			
		} catch (RuntimeException re) {
			if (re.getCause() instanceof CommonBusinessException) {
				throw ((CommonBusinessException)re.getCause());
			}			
			throw re;
		}
	}
	
	private final static TimedCache<Pair<UID, Object>, Collectable<Object>> TIMEDREFCACHE = 
			new TimedCache<Pair<UID, Object>, Collectable<Object>>(new GetReferencedCollectable<Object>(), 5);

	private static class GetReferencedCollectable<PK> implements LookupProvider<Pair<UID, PK>, Collectable<PK>>{		
		@Override
		public Collectable<PK> lookup(Pair<UID, PK> refInfo) {
			try {
				try {
					return Utils.getReferencedCollectable(null, refInfo.x, refInfo.y);					
				} catch (CommonBusinessException cbe) {
					throw new RuntimeException(cbe);
				}
			}
			catch(RuntimeException e) {
				throw new CommonFatalException(e);
			}
		}
	}

	public static String getRepresentation(final UID refAttributeUID, final Collectable<?> c) {
		String oForeignValue = null;
		try {
			oForeignValue = RefValueExtractor.get(c, refAttributeUID);
		}
		catch (Exception ex) {
			LOG.warn("acceptLookedUpCollectable: foreign value could not be found.");
		}
		return oForeignValue;
	}

	public static class CollectableLookupProvider implements NBCache.LookupProvider<Object, Collectable> {
		private final UID referencingEntity;
		private final UID referencingField;

		public CollectableLookupProvider(UID referencingEntity, UID referencingField) {
			this.referencingEntity = referencingEntity;
			this.referencingField = referencingField;
		}
		@Override
		public Collectable<?> lookup(Object key) {
			try {
				return getReferencedCollectable(referencingEntity, referencingField, key);
			} catch (CommonBusinessException e) {
				throw new NuclosFatalException(e);
			}
		}
	}

	public static CollectableEOEntity transformCollectableMasterDataEntityTOCollectableEOEntity(CollectableMasterDataEntity cmde) {
		final UID entity = cmde.getUID();
		final CollectableEOEntity cee = new CollectableEOEntity(MetaProvider.getInstance().getEntity(entity));
		return cee;
	}
	
	/**
	 * copy {@link SubFormTable} row
	 * 
	 * @param iRow row index
	 * @param model table model
	 */
	public static List<CollectableField> copyRow(int iRow, final SubFormTableModel model) {
		assert iRow > -1;
		final List<CollectableField> lstRowFields = new ArrayList<CollectableField>();
		for (int iColumn= 0; iColumn< model.getColumnCount(); iColumn++) {
			lstRowFields.add(model.getValueAsCollectableField(model.getValueAt(iRow, iColumn)));
		}
		
		return Collections.unmodifiableList(lstRowFields);
		
	}
	

	/**
	 * sets all fields in the given <code>Collectable</code> to their respective default values (according to the given <code>CollectableEntity</code>).
	 * 
	 * §precondition clct != null
	 * §precondition clct.isComplete()
	 * §precondition clcte != null
	 * 
	 * @param clct the <code>Collectable</code> to be changed.
	 * @param clcte the <code>CollectableEntity</code> containing the required information about the fields in <code>clct</code>.
	 */
	public static <PK> void setDefaultValues(Collectable<PK> clct, CollectableEntity clcte) {
		for (UID field : clcte.getFieldUIDs()) {
			setDefaultValue(clct, clcte, field);
		}
	}

	/**
	 * set the field in the given <code>Collectable</code> to their respective default values (according to the given <code>CollectableEntity</code>).
	 *
	 * @param clct the <code>Collectable</code> to be changed.
	 * @param clcte clcte the <code>CollectableEntity</code> containing the required information about the fields in <code>clct</code>.
	 * @param field the UID of CollectableField
	 *
	 * @return true when default is set, otherwise false
	 */
	public static <PK> boolean setDefaultValue(Collectable<PK> clct, CollectableEntity clcte, UID field) {
		final CollectableField clctf = clcte.getEntityField(field).getDefault();
		if (clctf.getValue() != null && clctf.getValue().toString().equals(RelativeDate.today().toString())) {
			clct.setField(field, new CollectableValueField(DateUtils.today()));
			return true;
		}
		else if (clcte.getEntityField(field).getJavaClass() == Boolean.class &&
				clctf.getValue() == null) {
			//null default values for boolean fields
		}
		else {
			CollectableField cf = clctf;
			if (clcte.getEntityField(field).isReferencing() && !cf.isNull()) {
				if (!MasterDataCache.getInstance().exist(clcte.getEntityField(field).getReferencedEntityUID(), ((CollectableValueIdField)clctf).getValueId())) {
					cf = clcte.getEntityField(field).getNullField();
				}
			}
			clct.setField(field, cf);
			return true;
		}
		return false;
	}

	/**
	 * §precondition collclct != null
	 * §precondition sFieldName != null
	 * 
	 * @return the common value (if any) of the field with the given name, in all Collectables in the given collection.
	 *         <code>null</code> if there is no common value.
	 */
	public static <PK> CollectableField getCommonValue(Collection<? extends Collectable<PK>> collclct, UID field) {
		CollectableField result = null;
		for (Collectable<PK> clct : collclct) {
			final CollectableField clctf = clct.getField(field);
			if (result == null) {
				result = clctf;
			} else {
				if (!result.equals(clctf)) {
					result = null;
					break;
				}
			}
		}
		return result;
	}
	
}	// class Utils
