//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General License for more details.
//
//You should have received a copy of the GNU Affero General License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;

public interface NuclosDropTargetVisitor {
	
	void visitDragEnter(DropTargetDragEvent dtde);

	void visitDragExit(DropTargetEvent dte);
	
	void visitDragOver(DropTargetDragEvent dtde);		
	
	void visitDrop(DropTargetDropEvent dtde);
	
	void visitDropActionChanged(DropTargetDragEvent dtde);		
	
}
