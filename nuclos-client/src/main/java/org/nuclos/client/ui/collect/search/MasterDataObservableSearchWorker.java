package org.nuclos.client.ui.collect.search;

import java.util.List;
import java.util.Observable;

import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.genericobject.ProxyList;

/**
 * A specialization of SearchWorker to use with MasterDataCollectController.
 * 
 * @since Nuclos 3.1.01 this is a top level class.
 * @author Thomas Pasch (refactoring)
 */
public class MasterDataObservableSearchWorker<PK> extends Observable implements
		SearchWorker<PK,CollectableMasterDataWithDependants<PK>> {
	
	private final MasterDataCollectController<PK> cc;

	public MasterDataObservableSearchWorker(MasterDataCollectController<PK> cc) {
		this.cc = cc;
	}
	
	@Override
	public void startSearch() throws CommonBusinessException {
		startSearch(false);
	}

	@Override
	public void startSearch(boolean bRefreshOnly) throws CommonBusinessException {
		final MasterDataCollectController cc = getMasterDataCollectController();
		cc.makeConsistent(true, bRefreshOnly);
		cc.removePreviousChangeListenersForResultTableVerticalScrollBar();
	}

	@Override
	public ProxyList<PK,CollectableMasterDataWithDependants<PK>> getResult()
			throws CommonBusinessException {
		return getMasterDataCollectController().getSearchStrategy().getSearchResult();
	}

	@Override
	public void finishSearch(List<CollectableMasterDataWithDependants<PK>> lstclctResult) {
		final MasterDataCollectController<PK> cc = getMasterDataCollectController();
		
		ProxyList<PK,CollectableMasterDataWithDependants<PK>> proxyList = (ProxyList<PK,CollectableMasterDataWithDependants<PK>>) lstclctResult;
		cc.getSearchStrategy().setCollectableProxyList(proxyList);
		cc.fillResultPanel(proxyList, proxyList.totalSize(false), false);
		cc.setupChangeListenerForResultTableVerticalScrollBar();
		super.setChanged();
		super.notifyObservers("search finished");
	}
	
	private MasterDataCollectController<PK> getMasterDataCollectController() {
		return cc;
	}
}
