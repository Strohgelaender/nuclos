//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.common.CollectableDocumentFileChooserBase;
import org.nuclos.client.ui.collect.DefaultLayoutNavigationSupportContext;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.LayoutNavigationSupport.ExecutionPoint;
import org.nuclos.client.ui.labeled.ILabeledComponentSupport;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;

public class HyperlinkTextFieldWithButton extends TextFieldWithButton implements LayoutNavigationProcessor {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = Logger.getLogger(HyperlinkTextFieldWithButton.class);
	
	protected static final int ENTER = KeyEvent.VK_ENTER;
	
	protected static final int CTRL = InputEvent.CTRL_MASK;
	
	private static final Cursor curHand = new Cursor(Cursor.HAND_CURSOR);
	
	private Font defaultFont;
	
	private Font underlineFont;
	
	protected final boolean bSearchable;
	
	private boolean buttonEnabled = true;
	
	private boolean openLink = true;

	private boolean selectAllOnGainFocus = false;
	
	private volatile boolean focusedByMouse = false;

	private LayoutNavigationCollectable lnc;

	private boolean selectOnlyFolders=false;
	
	private static final List<Icon> HYPERLINKBUTTONS = new ArrayList<Icon>();
	
	static {
		HYPERLINKBUTTONS.add(Icons.getInstance().getIconTextFieldButtonHyperlink());
		HYPERLINKBUTTONS.add(Icons.getInstance().getIconTextFieldButtonFile());
	}

	public HyperlinkTextFieldWithButton(ILabeledComponentSupport support, boolean bSearchable) {
		this(HYPERLINKBUTTONS, support, bSearchable);
	}
	
	protected HyperlinkTextFieldWithButton(List<Icon> paramLstIconButtons, ILabeledComponentSupport support, boolean bSearchable) {
		super(paramLstIconButtons, support);
		this.bSearchable = bSearchable;
		if (!bSearchable) {
			addMouseListener(new HyperlinkMouseListener());
			addFocusListener(new HyperlinkFocusListener());
			addKeyListener(new HyperlinkKeyListener());
		}
		setFont(getFont());
		setForeground(Color.BLUE);		
	}
	


	@Override
	public void setFont(Font f) {
		defaultFont = f;
		underlineFont = getUnderline(f);
		super.setFont(openLink?underlineFont:defaultFont);
	}

	private Font getUnderline(Font change) {
		Map<TextAttribute, Object> fontAttributes = new HashMap<TextAttribute, Object>(change.getAttributes());
		fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		return new Font(change.getName(), change.getStyle(), change.getSize()).deriveFont(fontAttributes);
	}
	
	public void setButtonEnabled(boolean enabled) {
		this.buttonEnabled = enabled;
	}

	@Override
	public boolean isButtonEnabled(int index) {
		return buttonEnabled;
	}
	
	public void setOpenLink(boolean openLink) {
		this.openLink = openLink;
		setButtonEnabled(openLink);
		super.setFont(openLink?underlineFont:defaultFont);
	}
	
	@Override
	public void setText(String text) {
		super.setText(text);
		setOpenLink(true);
	}

	@Override
	public void buttonClicked(MouseEvent me, int index) {
		if (SwingUtilities.isLeftMouseButton(me)) {
			switch (index) {
				case 0:
					setOpenLink(!openLink);
					break;
				case 1:
					cmdBrowse();
					break;
			}
		}
		selectAllOnGainFocus = false;
	}
	
	@Override
	public void textClicked(MouseEvent me) {
		if (!bSearchable && SwingUtilities.isLeftMouseButton(me) && !me.isControlDown() && 
				openLink && !StringUtils.looksEmpty(getText())) {
			openLink();
		}
	}
	
	protected void cmdBrowse() {
    	final Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node("hyperlinkfolder");
    	
		final String sLastDir = prefs.get(CollectableDocumentFileChooserBase.PREFS_KEY_LAST_DIRECTORY, null);
		final JFileChooser filechooser = new JFileChooser(sLastDir);

		// Customer's wish (B1149) / UA
		if(selectOnlyFolders==false)
		{
			filechooser.setFileHidingEnabled(false);
			filechooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		}else
		{
			//BNG-53 Optionales Feature wenn die erweiterte Eigentschaft "DIRECTORIES_ONLY" an dem Hyperlink gesetzt ist.
			filechooser.setFileHidingEnabled(true);
			filechooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}
		final int iBtn = filechooser.showOpenDialog(this);
		if (iBtn == JFileChooser.APPROVE_OPTION) {
			final java.io.File file = filechooser.getSelectedFile();
			if (file != null) {
				prefs.put(CollectableDocumentFileChooserBase.PREFS_KEY_LAST_DIRECTORY, filechooser.getCurrentDirectory().getAbsolutePath());
				
				try {
					String path = file.getAbsolutePath().replace("\\", "/");
					if (!path.startsWith("/")) {
						path = "/" + path;
					}
					URI uri = new URI("file", "", path, null);
					
					Set<Character> toIgnore = new HashSet<Character>();
					toIgnore.add(':');
					toIgnore.add('/');
					toIgnore.add('%');
					String link = StringUtils.encodeForUrl(uri.toString(), toIgnore);
					setText(link);
					
				} catch (Exception e) {
					log.warn(e.getMessage(), e);
				}
					
			}
		}
	}
	
	/**
	 * Click or Ctrl+Enter
	 */
	public void openLink() {
		try {
			if (!StringUtils.looksEmpty(getText())) {
				String url = getText();
				if (url.indexOf("://") == -1) {
					url = "http://"+url;
				}
				Desktop.getDesktop().browse(URI.create(url));
			}
		} catch (IllegalArgumentException e) {
			log.info(e.getMessage(), e);
		} catch (IOException e) {
			log.info(e.getMessage(),e);
		}
	}
	
	@Override
	protected Cursor getDefaultCursor(MouseEvent me) {
		if (!bSearchable && !me.isControlDown() &&
				openLink && !StringUtils.looksEmpty(getText())) {
			return curHand;
		} else {
			return super.getDefaultCursor(me);
		}
	}
	
	protected List<JComponent> getContextMenuItems() {
		List<JComponent> result = new ArrayList<JComponent>();
		result.add(getEditMenuItem());
		result.add(getOpenMenuItem());
		return result;
	}
	
	protected JMenuItem getOpenMenuItem() {
		JMenuItem miOpen = new JMenuItem(new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage("Hyperlink.open", "Öffnen")) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				openLink();
			}
		});
		miOpen.setAccelerator(KeyStroke.getKeyStroke(ENTER, CTRL));
		miOpen.setEnabled(!StringUtils.looksEmpty(getText()));
		return miOpen;
	}
	
	protected JMenuItem getEditMenuItem() {
		return new JMenuItem(new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage("Hyperlink.edit", "Bearbeiten"),
				Icons.getInstance().getIconEdit16()) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setOpenLink(false);
			}
		});
	}

	private class HyperlinkMouseListener extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent me) {
			if (SwingUtilities.isRightMouseButton(me)) {
				requestFocusInWindow();
				JPopupMenu popup = new JPopupMenu();
				for (JComponent c : getContextMenuItems()) {
					popup.add(c);
				}
				popup.show(HyperlinkTextFieldWithButton.this, me.getX(), me.getY());
			}
		}
		
		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			super.mousePressed(e);
			focusedByMouse = true;
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			focusedByMouse = false;
		}
		
	}
	
	private class HyperlinkFocusListener extends FocusAdapter {
		
		@Override
		public void focusGained(FocusEvent e) {
			if (openLink) {
				setOpenLink(false);
			}
			// FIXME move to LayoutNavigationManager
			if (focusedByMouse)  {
				selectAllOnGainFocus = false;
				moveCursorToEndOfDocument();
			} else {
				selectAllOnGainFocus = true;
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			if (!openLink) {
				setOpenLink(true);
			}
		}
				
	}
	
	private class HyperlinkKeyListener extends KeyAdapter {

		@Override
		public void keyReleased(KeyEvent e) {
			if (e.isControlDown()) {
				if (e.getKeyCode() == ENTER) {
					openLink();
				}
			}
		}
		
	}
	
	@Override
	protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e,
			final int condition, final boolean pressed) {
		
		boolean processed = false;
		if (null != lnc) {
			final LayoutNavigationSupport lns = lnc.getLayoutNavigationSupport();
			if (lns != null) {
				final DefaultLayoutNavigationSupportContext ctx = new DefaultLayoutNavigationSupportContext(
						pressed, ks, e, condition, HyperlinkTextFieldWithButton.this, lnc);
				processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.BEFORE);
				if (!processed) {
					processed = super.processKeyBinding(ks, e, condition, pressed);
					//if (!processed) {
						ctx.setProcessed(processed);
						processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.AFTER);
					//}
				}
			} else {
				processed = super.processKeyBinding(ks, e, condition, pressed);
			}
		} else {
			processed = super.processKeyBinding(ks, e, condition, pressed);
		}
		return processed;

	}

	public void moveCursorToEndOfDocument() {
		HyperlinkTextFieldWithButton.this.setCaretPosition(Math.max(HyperlinkTextFieldWithButton.this.getDocument().getEndPosition().getOffset() - 1,0));
	}

	public boolean selectAllOnGainFocus() {
		return this.selectAllOnGainFocus;
	}

	@Override
	public void setLayoutNavigationCollectable(LayoutNavigationCollectable lnc) {
		this.lnc = lnc;
	}

	public void setSelectFolderAllowed(boolean val) {
		this.selectOnlyFolders=val;
		
	}

	@Override
	public String getToolTipText(MouseEvent ev) {
		String sToolTipText = super.getToolTipText(ev);
		return StringUtils.isNullOrEmpty(sToolTipText) ? null : sToolTipText;
	}
}
