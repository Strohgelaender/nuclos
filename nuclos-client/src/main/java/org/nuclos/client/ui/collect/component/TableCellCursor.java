package org.nuclos.client.ui.collect.component;

import java.awt.*;

public interface TableCellCursor {

	Cursor getCursor(Object cellValue, int cellWidth, int x);
	
	void deriveFont(float size);

}
