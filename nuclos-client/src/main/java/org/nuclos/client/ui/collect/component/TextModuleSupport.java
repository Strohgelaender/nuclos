//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import javax.swing.text.JTextComponent;

import org.nuclos.common.textmodule.TextModule;
import org.nuclos.common.textmodule.TextModuleSettings;

public interface TextModuleSupport {

	/**
	 * get {@link TextModuleSettings}
	 * 
	 * @return {@link TextModuleSettings}
	 */
	TextModuleSettings getTextModuleSettings();
	
	/**
	 * set {@link TextModuleSettings}
	 * 
	 * @param tms	{@link TextModuleSettings}
	 */
	void setTextModuleSettings(TextModuleSettings tms);
	
	/**
	 * get text component where {@link TextModule} is inserted
	 * 
	 * @return {@link JTextComponent}
	 */
	JTextComponent getTargetTextComponent();

}