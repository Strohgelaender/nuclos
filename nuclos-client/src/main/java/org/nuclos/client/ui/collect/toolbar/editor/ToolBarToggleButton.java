//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar.editor;

import java.awt.event.ActionEvent;

import javax.swing.*;

class ToolBarToggleButton extends JToggleButton implements IToolBarComponent {
	private final ToolBarObject tbo;
	public ToolBarToggleButton(String name, ToolBarObject tbo) {
		super(new AbstractAction(name) {
			@Override
			public void actionPerformed(ActionEvent e) {
				// do nothing
			}
		});
		this.tbo = tbo;
		setFocusable(false);
	}
	@Override
	public ToolBarObject getObject() {
		return this.tbo;
	}
	@Override
	public void reset() {
		this.setBorderPainted(false);
		this.setSelected(false);
	}
	@Override
	public JComponent getJComponent() {
		return this;
	}
}