//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.releasenote;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.annotation.PostConstruct;
import javax.swing.JOptionPane;

import org.nuclos.client.command.ResultListener;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.MainFrameTabListener;
import org.nuclos.client.ui.model.AbstractListTableModel;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.preferences.PreferencesConstants;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.EntityObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

public class ReleaseNoteOverviewTableModel extends AbstractListTableModel<EntityObjectVO<UID>> {
	
	private static final UID RN_ENTITY = E.NUCLETRELEASENOTE.getUID();
	private static final UID NUCLET_FIELD = E.NUCLETRELEASENOTE.nuclet.getUID();
	private static final UID VERSION_FIELD = E.NUCLETRELEASENOTE.nucletVersion.getUID();
	private static final UID PUBLISHED_FIELD = E.NUCLETRELEASENOTE.published.getUID();
	
	public static final UID NEW_RN = new UID();
	
	// Spring injection
	
	@Autowired
	private SpringLocaleDelegate sld;
	
	@Autowired
	private EntityObjectFacadeRemote entityObjectFacade;
	
	@Autowired
	private ClientPreferences preferences;
	
	// end of Spring injection
	
	/**
	 * Highest nuclet version seen in user preferences.
	 * <p>
	 * (UID) nuclet -> (Integer) max version seen from preferences
	 */
	private Map<UID,Integer> maxVersion;
	
	/**
	 * New versions w.r.t user preferences <em>at client start</em>
	 * (i.e. before user read new release notes).
	 */
	private Multimap<UID,Integer> newVersionsAtStart;
	
	private DateFormat dateFormat;
	
	ReleaseNoteOverviewTableModel() {
	}
	
	@PostConstruct
	void init() {
		dateFormat = SpringLocaleDelegate.getInstance().getDateFormat();
		final List<CollectableSorting> sorting = new LinkedList<CollectableSorting>();
		sorting.add(new CollectableSorting(SystemFields.BASE_ALIAS, RN_ENTITY, true, NUCLET_FIELD, true));
		sorting.add(new CollectableSorting(SystemFields.BASE_ALIAS, RN_ENTITY, true, VERSION_FIELD, false));
		
		final CollectableSearchExpression expr = new CollectableSearchExpression();
		expr.setSearchCondition(TrueCondition.TRUE);
		expr.setSortingOrder(sorting);
		try {
			final Collection<EntityObjectVO<UID>> all = entityObjectFacade.getEntityObjectsChunk(
					RN_ENTITY, expr, ResultParams.DEFAULT_RESULT_PARAMS, null);
			addAll(all);
		} catch (CommonPermissionException cpe) {
			throw new NuclosFatalException(cpe);
		}
		refresh();
	}
	
	/**
	 * Adjust new release note flag. Before showing another/new MainFrameTab.
	 */
	public void refresh() {
		final Preferences rnPrefs = preferences.getUserPreferences().node(PreferencesConstants.RELEASENOTE_PREF_PATH);
		final Map<UID,Integer> maxVersion = new HashMap<UID,Integer>();
		// find last seen version
		for (EntityObjectVO<UID> eo: this) {
			final UID nuclet = eo.getFieldUid(NUCLET_FIELD);
			final int nucletVersion = rnPrefs.getInt(nuclet.getString(), Integer.MIN_VALUE);
			final Integer current = maxVersion.get(nuclet);
			if (current == null || current.intValue() < nucletVersion) {
				maxVersion.put(nuclet, nucletVersion);
			}
		}
		// tag entity object accordingly
		newVersionsAtStart = MultimapBuilder.hashKeys().treeSetValues().build();
		for (EntityObjectVO<UID> eo: this) {
			final UID nuclet = eo.getFieldUid(NUCLET_FIELD);
			final int lastSeen = maxVersion.get(nuclet).intValue();
			final Integer nucletVersion = eo.getFieldValue(VERSION_FIELD, Integer.class);
			final boolean isNew = nucletVersion.intValue() > lastSeen;
			eo.setFieldValue(NEW_RN, isNew);
			if (isNew) {
				newVersionsAtStart.put(nuclet, nucletVersion);
			}
		}
	}

	@Override
	public int getColumnCount() {
		return 4;
	}
	
	@Override
	public Class<?> getColumnClass(int col) {
		if (col == 2) {
			return Boolean.class;
		}
		return super.getColumnClass(col);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex >= getRowCount()) {
			return null;
		}
		final EntityObjectVO<UID> row = getRow(rowIndex);
		final Object result;
		switch (columnIndex) {
		case 0:
			result = row.getFieldValue(NUCLET_FIELD);
			break;
		case 1:
			result = row.getFieldValue(VERSION_FIELD);
			break;
		case 2:
			result = row.getFieldValue(NEW_RN);
			break;
		case 3:
			final Object publishDate = row.getFieldValue(PUBLISHED_FIELD);
			if (publishDate != null) {
				result = dateFormat.format(publishDate);
			}
			else {
				result = null;
			}
			break;
		default:
			throw new IllegalArgumentException();
		}
		return result;
	}
	
	@Override
    public String getColumnName(int column) {
		final String result;
		switch (column) {
		case 0:
			result = sld.getMessage("nuclet.release.note.column.1", "nuclet.release.note.column.1");
			break;
		case 1:
			result = sld.getMessage("nuclet.release.note.column.2", "nuclet.release.note.column.2");
			break;
		case 2:
			result = sld.getMessage("nuclet.release.note.column.3", "nuclet.release.note.column.3");
			break;
		case 3:
			result = sld.getMessage("nuclet.release.note.column.4", "nuclet.release.note.column.4");
			break;
		default:
			throw new IllegalStateException();
		}
		return result;
    }
	
	Map<UID,Integer> getMaxVersionFromPrefs() {
		return ImmutableMap.copyOf(maxVersion);
	}
	
	public Multimap<UID,Integer> getNewVersionsAtStart() {
		return ImmutableMultimap.copyOf(newVersionsAtStart);
	}
	
	public MainFrameTabListener createTabListener(MainFrameTab releaseNoteTab) {
		return new TabListener(releaseNoteTab);
	}
	
	public void storePrefs(Multimap<UID,Integer> toMark, Set<UID> exclude) {
		final Preferences rnPrefs = preferences.getUserPreferences().node(PreferencesConstants.RELEASENOTE_PREF_PATH);				
		for (UID nuclet: toMark.keys()) {
			if (!exclude.contains(nuclet)) {
				final List<Integer> versions = (List<Integer>) toMark.get(nuclet);
				final int size = versions.size();
				if (size > 0) {
					final Integer max = versions.get(versions.size() - 1);
					rnPrefs.putInt(nuclet.toString(), max);
				}
			}
		}
		
		// Also adjust model - as the tab may be re-opened
		// Consider that refresh() has not run yet - hence maxVersions 
		// and newVersionsAtStart are NOT valid.
		for (EntityObjectVO<UID> eo: ReleaseNoteOverviewTableModel.this) {
			final UID nuclet = eo.getFieldUid(E.NUCLETRELEASENOTE.nuclet.getUID());
			final Boolean isNew = eo.getFieldValue(NEW_RN, Boolean.class);
			final Integer version = eo.getFieldValue(E.NUCLETRELEASENOTE.nucletVersion.getUID(), Integer.class);
			final List<Integer> versions = (List<Integer>) toMark.get(nuclet);
			final boolean isMarked = versions.contains(version);
			final boolean isExcluded = exclude.contains(nuclet);
			if (isNew != null && isNew.booleanValue()) {
				if (!isExcluded && isMarked) {
					// As the tab is closing it is not necessary to fire a table even.
					eo.setFieldValue(NEW_RN, Boolean.FALSE);
				}
			} else {
				if (isExcluded && isMarked) {
					// As the tab is closing it is not necessary to fire a table even.
					eo.setFieldValue(NEW_RN, Boolean.TRUE);
				}
			}
		}
	}
	
	private class TabListener extends MainFrameTabAdapter {
		
		private final MainFrameTab releaseNoteTab;

		private TabListener(MainFrameTab releaseNoteTab) {
			if (releaseNoteTab == null) {
				throw new NullPointerException();
			}
			this.releaseNoteTab = releaseNoteTab;
		}

		@Override
		public void tabClosing(MainFrameTab tab, ResultListener<Boolean> rl) {
			final Multimap<UID,Integer> newVersions = getNewVersionsAtStart();
			final Set<UID> nucletsWithNewRn = new HashSet<UID>();
			final StringBuilder msg = new StringBuilder(
					sld.getMessage("nuclet.release.note.new.2", "nuclet.release.note.new.2"));
			msg.append("\n");
			
			int numEntry = 0;
			int numMoreEntries = 0;
			for (EntityObjectVO<UID> eo : ReleaseNoteOverviewTableModel.this) {
				final UID nucletUid = eo.getFieldUid(E.NUCLETRELEASENOTE.nuclet.getUID());
				final Integer version = eo.getFieldValue(E.NUCLETRELEASENOTE.nucletVersion.getUID(),
						Integer.class);
				final Boolean isNew = eo.getFieldValue(ReleaseNoteOverviewTableModel.NEW_RN, Boolean.class);
				if (isNew != null && isNew.booleanValue()) {
					if (numEntry < 10) {
						final String nuclet = eo.getFieldValue(E.NUCLETRELEASENOTE.nuclet.getUID(), String.class);
						msg.append("\n* ").append(nuclet).append(" Version ").append(version);
						nucletsWithNewRn.add(nucletUid);
						numEntry++;
					}
					else {
						numMoreEntries++;
					}
				}
			}
			
			if (numMoreEntries > 0) {
				msg.append("\n\n... und "+ numMoreEntries +" weitere");
			}
			
			if (!nucletsWithNewRn.isEmpty()) {
				msg.append("\n\n");
				msg.append(
						sld.getMessage("nuclet.release.note.new.3", "nuclet.release.note.new.3"));
				final String title = sld.getMessage("nuclet.release.note.new.1", "nuclet.release.note.new.1");
				final int ok = JOptionPane.showConfirmDialog(releaseNoteTab, msg.toString(),
						title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);				
				if (ok == JOptionPane.OK_OPTION) {
					storePrefs(newVersions, Collections.<UID>emptySet());
					rl.done(true);
				} else if (ok == JOptionPane.NO_OPTION) {
					storePrefs(newVersions, nucletsWithNewRn);
					rl.done(true);
				} else {
					// cancel - no new prefs to store
					rl.done(false);
				}
			} else {
				// newVersions is empty - no new prefs to store
				rl.done(true);
			}
		}
	}
}
