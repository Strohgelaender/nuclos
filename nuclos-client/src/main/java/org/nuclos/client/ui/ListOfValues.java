//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.IllegalComponentStateException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ComboBoxEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.apache.log4j.Logger;
import org.nuclos.client.command.CommonClientWorker;
import org.nuclos.client.ui.collect.DefaultLayoutNavigationSupportContext;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.LayoutNavigationSupport.ExecutionPoint;
import org.nuclos.client.ui.collect.component.ICollectableListOfValues;
import org.nuclos.client.ui.labeled.CheckCombo;
import org.nuclos.client.ui.labeled.ILabeledComponentSupport;
import org.nuclos.client.ui.labeled.MultiSelectComboBoxEditor;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * A List of Values (LOV), as in Oracle Forms.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public class ListOfValues extends JPanel implements LayoutNavigationProcessor {

	private static final Logger LOG = Logger.getLogger(ListOfValues.class);

	public static final int QUICKSEARCH_DELAY_TIME = 756;

	private static final int QUICKSEARCH_POPUP_ROWS = 16;

	private static final KeyStroke ENTER = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true);

	private static final KeyStroke TAB = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, true);

	private static final KeyStroke UP = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0, true);

	private static final KeyStroke DOWN = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, true);

	private static final KeyStroke ESC = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true);

	private static final KeyStroke DELETE = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0, true);

	private static final KeyStroke BACK_SPACE = KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0, true);

	private static final String QUICK_SEARCH = "quickSearch";

	private static final String QUICK_SEARCH_NAV_UP = "quickSearchNavUp";

	private static final String QUICK_SEARCH_NAV_DOWN = "quickSearchNavDown";

	private static final String QUICK_SEARCH_CANCEL = "quickSearchCancel";

	// 

	private ILabeledComponentSupport support;
	
	private ColorProvider colorProviderBackup;
	
	private Color defaultColorBackup;

	private QuickSearchResulting quickSearchResulting;

	private QuickSearchSelectedListener quickSearchSelectedListener;

	private ActionListener quickSearchCanceledListener;

	private boolean searchOnLostFocus = true;
	
	private boolean searchOnEnterFocus = false;

	private boolean transferQuickSearchValue = true;

	private boolean quickSearchEnabled = true;

	private boolean quickSearchOnly = false;

	private boolean alertRunning = false;

	private volatile boolean searchRunning = false;
	
	private volatile Integer searchInvokeCounter = 0;

	private boolean changesPending = false;

	private boolean clearOnEmptyInput = false;

	private boolean ignoreDocumentEvents = false;

	private final InputChanged inputChanged = new InputChanged();

	private final LovDocumentListener lovDocumentListener = new LovDocumentListener();

	private final QuickSearchAction actionSearch = new QuickSearchAction();

	private SearchingWorker lastSearchingWorker = null;

	private TimerTask lastTimerTask = null;
	
	private static final Icon LOV_ICON = Icons.getInstance().getIconTextFieldButtonLOV();
	private static final Icon COMBO_ICON = Icons.getInstance().getIconTextFieldButtonCombobox();
	
	private static List<Icon> getAllIcons() {
		List<Icon> lovbuttons = new ArrayList<Icon>();
		lovbuttons.add(LOV_ICON);
		lovbuttons.add(COMBO_ICON);
		return lovbuttons;
	}
	
	private final LovTextFieldWithButton tf;

	private class LovTextFieldWithButton extends TextFieldWithButton {
		/**  */
		private static final long serialVersionUID = 1L;

		private Boolean bLovButton;
		private Boolean bLovSearch;
		private Boolean bDropButton;
		private String customUsageSearch;
		private boolean bDropButtonEnabled;

		LovTextFieldWithButton(ILabeledComponentSupport support) {
			super(getAllIcons(), support);
			addOrRemoveIcons();
			bDropButtonEnabled = true;
			cbxQuickChooser.addPopupMenuListener(new PopupMenuListener() {
				
				@Override
				public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				}
				
				@Override
				public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
					enableDropButtonLater();
				}
				
				@Override
				public void popupMenuCanceled(PopupMenuEvent e) {
					enableDropButtonLater();
				}
			});
		}
		
		public void enableDropButtonLater() {
			SwingUtilities.invokeLater(new Runnable() {						
				@Override
				public void run() {
					bDropButtonEnabled = true;
				}
			});			
		}
		
		private void addOrRemoveIcons() {
			getButtonIcons().clear();
			if (hasLovBtn()) {
				getButtonIcons().add(LOV_ICON);
			}
			if (hasDropdownBtn()) {
				getButtonIcons().add(COMBO_ICON);
			}
		}
		
		//Magnifier (LOV-Button) is default-enabled, thus, return true, if null
		private boolean hasLovBtn() {
			if (bLovButton == null) {
				return true;
			}
			return bLovButton;
		}
		
		private boolean hasLovSearch() {
			if (bLovSearch == null) {
				return false;
			}
			return bLovSearch;
		}

		//Triangle (Dropdown-Button) is default-disable, thus, return false, if null
		private boolean hasDropdownBtn() {
			if (bDropButton == null) {
				return false;
			}
			return bDropButton;
		}

		private void setLovBtn(Boolean bLovBtn) {
			this.bLovButton = bLovBtn;
			addOrRemoveIcons();
		}
		
		private void setLovSearch(Boolean bLovSearch) {
			this.bLovSearch = bLovSearch;
		}

		private void setDropdownBtn(Boolean bDropdownBtn) {
			this.bDropButton = bDropdownBtn;
			addOrRemoveIcons();
		}

		private void setCustomUsageSearch(String customUsageSearch) {
			this.customUsageSearch = customUsageSearch;
			addOrRemoveIcons();
		}

		@Override
		public String getToolTipText(MouseEvent ev) {
			final ToolTipTextProvider provider = support.getToolTipTextProvider();
			String sToolTipText = (provider != null) ? provider.getDynamicToolTipText() : super.getToolTipText(ev);
			return StringUtils.isNullOrEmpty(sToolTipText) ? null : sToolTipText;
		}
		
		@Override
		public boolean isButtonEnabled(int index) {
			if (index == 0 && hasLovBtn()) {
				return ListOfValues.this.btnBrowse.isEnabled();				
			}
			return bDropButtonEnabled;
		}

		@Override
		public void buttonClicked(MouseEvent me, int index) {
			if (index == 0 && hasLovBtn()) {
				ListOfValues.this.btnBrowse.doClick();
			} else {
				if (isButtonEnabled(index)) {
					inputChanged.startSearch(true);
					bDropButtonEnabled = false;
				}
			}
		}

		@Override
		public boolean isEditable() {
			return super.isEditable() && quickSearchEnabled;
		}
		
		@Override
		public void setText(String sText) {			
			if (isMultiSelect() && !StringUtils.isNullOrEmpty(cbxQuickChooser.getMultiSelectComboBoxEditor().getMultiSelectItemsString())) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						ignoreDocumentEvents = true;
						LovTextFieldWithButton.super.setText(cbxQuickChooser.getMultiSelectComboBoxEditor().getMultiSelectItemsString());
						ignoreDocumentEvents = false;						
					}
				});
			} else {
				super.setText(sText);				
			}
		}

		private boolean processKeyBindingBase(final KeyStroke ks, final KeyEvent e,
				final int condition, final boolean pressed) {
			boolean retVal = true;
			if (e.getKeyCode() == ENTER.getKeyCode()
					|| e.getKeyCode() == TAB.getKeyCode()) {
				if (ks.getModifiers() == 0) {
					if (!pressed) {
						if (ListOfValues.this.cbxQuickChooser.isPopupVisible()) {
							actionPerformedQuickSearchSelected();
							((CollectableValueIdField) ListOfValues.this.cbxQuickChooser.getSelectedItem()).setSelected(!((CollectableValueIdField) ListOfValues.this.cbxQuickChooser.getSelectedItem()).isSelected());
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									Object selected = cbxQuickChooser.getSelectedItem();
									cbxQuickChooser.setSelectedItem(null);
									cbxQuickChooser.setSelectedItem(selected);
								}
							});
						} else {
							if (blnLookupEnabled || isSearchOnEnterFocus()) {
								SwingUtilities.notifyAction(new QuickSearchAction(), ks, e, ListOfValues.this.tf,
										e.getModifiers());
							}
						} 
					}
					else {
						if (e.getKeyCode() == TAB.getKeyCode()) {
							if (ListOfValues.this.cbxQuickChooser.isPopupVisible()) {
								actionPerformedQuickSearchSelected();
							}
							retVal = super.processKeyBinding(ks, e, condition, pressed); //@see NUCLOS-1488
						}
					}				
					super.processKeyBinding(ks, e, condition, pressed); //@see NUCLOS-1488
				} else {
					retVal = super.processKeyBinding(ks, e, condition, pressed);
				}
			} else if (e.getKeyCode() == UP.getKeyCode()) {
				if (ListOfValues.this.cbxQuickChooser.isPopupVisible()) {
					if (e.getID() != KeyEvent.KEY_RELEASED) {
						SwingUtilities.notifyAction(new QuickSearchNavigationAction(true), ks, e, ListOfValues.this.tf,
								e.getModifiers());
					}
					retVal = true;
				}
			} else if (e.getKeyCode() == DOWN.getKeyCode()) {
				if (ListOfValues.this.cbxQuickChooser.isPopupVisible()) {
					if (e.getID() != KeyEvent.KEY_RELEASED) {
						SwingUtilities.notifyAction(new QuickSearchNavigationAction(false), ks, e, ListOfValues.this.tf,
								e.getModifiers());
					}
					retVal = true;
				}
			} else if (e.getKeyCode() == ESC.getKeyCode()) {
				if (isQuickSearchPopupVisible()) {
					// quick search actions
					if (!pressed) {
						SwingUtilities.notifyAction(new QuickSearchCancelAction(), ks, e, ListOfValues.this.tf,
								e.getModifiers());
					}
					retVal = true;
				} else {
					// default actions
					// return super.processKeyBinding(ks, e, condition, pressed);		
				}
			} else if (e.getKeyCode() == DELETE.getKeyCode() || e.getKeyCode() == BACK_SPACE.getKeyCode()) {
				retVal = super.processKeyBinding(ks, e, condition, pressed);
				clearOnEmptyInput = true;
				if (ListOfValues.this.cbxQuickChooser.isPopupVisible()) {
					inputChanged.handleUpdate();
				}

			} else if (e.getKeyCode() != 0) {
				retVal = super.processKeyBinding(ks, e, condition, pressed);
				inputChanged.enable();

			} else {
				retVal = super.processKeyBinding(ks, e, condition, pressed);
			}
			
			//}
			return retVal;
		}
		
		@Override
		protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e,
				final int condition, final boolean pressed) {
			boolean processed = false;
			if (null != lnc) {
				final DefaultLayoutNavigationSupportContext ctx = new DefaultLayoutNavigationSupportContext(pressed,
						ks, e, condition, ListOfValues.this, lnc);
				final LayoutNavigationSupport lns = lnc.getLayoutNavigationSupport();
				if (lns != null) {
					processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.BEFORE);
					if (!processed) {
						processed = processKeyBindingBase(ks, e, condition, pressed);
						//if (!processed) {
						ctx.setProcessed(processed);
						processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.AFTER);
						//}
					}
				} else {
					processed = processKeyBindingBase(ks, e, condition, pressed);
				}
			} else {
				processed = processKeyBindingBase(ks, e, condition, pressed);
			}
			return processed;
		}

	}

	private final JButton btnBrowse = new JButton();

	private final Set<Object> selectedItems = new HashSet<>();

	private class lovComboBox extends CheckCombo {
		private final MultiSelectComboBoxEditor multiSelectComboBoxEditor;
		
		private lovComboBox(boolean opensAutomaticallyWithMultiselect) {
			super(opensAutomaticallyWithMultiselect);
			this.multiSelectComboBoxEditor = new MultiSelectComboBoxEditor(this, ListOfValues.this.selectedItems);
			super.setEditor(this.multiSelectComboBoxEditor);
		}
		
		@Override
		public void setSelectedItem(Object anObject) {
			super.setSelectedItem(anObject);
			if (anObject instanceof CollectableValueIdField) {
				CollectableValueIdField valueIdField = (CollectableValueIdField) anObject;
				if (valueIdField.isSelected()) {
					ListOfValues.this.selectedItems.add(valueIdField);
				} else {
					ListOfValues.this.selectedItems.remove(valueIdField);
				}
			}
		}

		@Override
		public Rectangle getBounds() {
			return ListOfValues.this.tf.getBounds();
		}

		@Override
		public Point getLocationOnScreen() {
			return ListOfValues.this.tf.getLocationOnScreen();
		}

		@Override
		public boolean isShowing() {
			return ListOfValues.this.tf.isShowing();
		}

		private boolean layingOut = false;
		private int widestLengh = 0;
		private boolean wide = false;

		public Dimension getSize() {
			Dimension dim = ListOfValues.this.tf.getSize();
			if (!layingOut)
				dim.width = Math.max(widestLengh, dim.width);
			return dim;
		}
		
		public MultiSelectComboBoxEditor getMultiSelectComboBoxEditor() {
			return multiSelectComboBoxEditor;
		}
	};

	private final lovComboBox cbxQuickChooser = new lovComboBox(true);

	private volatile boolean blnLookupEnabled;

	private LayoutNavigationCollectable lnc;

	@SuppressWarnings("unchecked")
	public ListOfValues(ILabeledComponentSupport support) {
		super(new BorderLayout(2, 0));
		if (support == null) {
			throw new NullPointerException();
		}		
		this.support = support;
		tf = new LovTextFieldWithButton(support);

		this.setOpaque(false);
		super.addFocusListener(new LovFocusListener());
		getJTextField().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (cbxQuickChooser.isPopupVisible()) {
					cbxQuickChooser.setMultiSelect(false);
					hideQuickSearch();
					cbxQuickChooser.setMultiSelect(true);
				}
			}
		});

		this.add(this.tf, BorderLayout.CENTER);
		this.tf.getDocument().addDocumentListener(lovDocumentListener);
		this.tf.addFocusListener(new LovTfFocusListener());
		this.tf.addKeyListener(new LovKeyListener());

		this.add(this.cbxQuickChooser, BorderLayout.SOUTH);
		this.cbxQuickChooser.setVisible(false);
		this.cbxQuickChooser.setMaximumRowCount(QUICKSEARCH_POPUP_ROWS);
		this.cbxQuickChooser.addActionListener(new QuickSearchActionListener());
		this.cbxQuickChooser.setRenderer(new DefaultListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				if (value instanceof String) {
					return new JLabel((String) value);
				}

				return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			}
		});

		this.tf.getActionMap().put(QUICK_SEARCH, new QuickSearchAction());
		this.tf.getActionMap().put(QUICK_SEARCH_NAV_UP, new QuickSearchNavigationAction(true));
		this.tf.getActionMap().put(QUICK_SEARCH_NAV_DOWN, new QuickSearchNavigationAction(false));
		this.tf.getActionMap().put(QUICK_SEARCH_CANCEL, new QuickSearchCancelAction());
	}

	/**
	 *
	 * @param enabled
	 */
	public void setQuickSearchEnabled(boolean enabled) {
		this.quickSearchEnabled = enabled;
	}

	public void setQuickSearchOnly(boolean qsonly) {
		this.quickSearchOnly = qsonly;
		if (isEnabled()) {
			btnBrowse.setEnabled(!qsonly);
		}
	}

	public java.awt.Font getFont() {
		return tf == null ? super.getFont() : tf.getFont();
	};

	public void setFont(java.awt.Font font) {
		if (tf != null && cbxQuickChooser != null) {
			tf.setFont(font);
			cbxQuickChooser.setFont(font);
		} else {
			super.setFont(font);
		}
	};

	public JTextField getJTextField() {
		return this.tf;
	}

	public JButton getBrowseButton() {
		return btnBrowse;
	}

	/**
	 * sets the number of columns of the textfield
	 * @param iColumns
	 */
	public void setColumns(int iColumns) {
		this.tf.setColumns(iColumns);
	}

	@Override
	public void setToolTipText(String sText) {
		this.tf.setToolTipText(sText);
	}

	void setToolTipTextProvider(ToolTipTextProvider tooltiptextprovider) {
		support.setToolTipTextProvider(tooltiptextprovider);
		if (tooltiptextprovider != null) {
			ToolTipManager.sharedInstance().registerComponent(this.getJTextField());
		}
	}

	@Override
	public void setEnabled(boolean bEnabled) {
		super.setEnabled(bEnabled);

		this.tf.setEditable(bEnabled);
		this.btnBrowse.setEnabled(!quickSearchOnly && bEnabled);
	}

	/*
	public void setBackgroundColorProviderForTextField(ColorProvider colorproviderBackground) {
		this.tf.setBackgroundColorProviderForTextField(colorproviderBackground);
	}
	 */

	@Override
	public void setName(String sName) {
		super.setName(sName);
		UIUtils.setCombinedName(this.tf, sName, "tf");
		UIUtils.setCombinedName(this.btnBrowse, sName, "btnBrowse");
	}

	@Override
	public boolean hasFocus() {
		return tf.hasFocus();
	}

	@Override
	public synchronized void addFocusListener(FocusListener l) {
		tf.addFocusListener(l);
	}

	@Override
	public synchronized void removeFocusListener(FocusListener l) {
		tf.removeFocusListener(l);
	}

	class LovFocusListener implements FocusListener {
		@Override
		public void focusGained(FocusEvent e) {
			if (!isSearchOnEnterFocus()) {
				blnLookupEnabled = false;
			}
			ListOfValues.this.tf.requestFocusInWindow();
			enableInputHandler();
		}
		@Override
		public void focusLost(FocusEvent e) {
			if (!isSearchOnEnterFocus()) {
				blnLookupEnabled = false;
			}
			searchInvokeCounter = 0;
		}
	}

	class LovTfFocusListener implements FocusListener {
		@Override
		public void focusGained(FocusEvent e) {
			tf.selectAll();
		}
		@Override
		public void focusLost(FocusEvent e) {
			if (cbxQuickChooser.isPopupVisible()) {
				actionPerformedQuickSearchSelected();
			} else if (clearOnEmptyInput && StringUtils.looksEmpty(tf.getText())) {
				actionPerformedQuickSearchSelected(true);
			} else if (changesPending) {
				if (lastTimerTask != null) {
					lastTimerTask.cancel();
				}
				if (lastSearchingWorker != null) {
					lastSearchingWorker.setStopped(true);
				}
				if (searchOnLostFocus) {
					actionSearch.actionPerformed(true, false);
				}
			}
			clearOnEmptyInput = false;
			changesPending = false;
		}
	}

	class LovKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			blnLookupEnabled = true;
			if ((e.isMetaDown() || e.isControlDown()) && e.getKeyCode() == KeyEvent.VK_V) {
				inputChanged.enable();
				inputChanged.startSearch(false);
			}
		}
	}

	class LovDocumentListener implements DocumentListener {
		@Override
		public void insertUpdate(DocumentEvent e) {
			if (ignoreDocumentEvents) 
				return;
			inputChanged.handleUpdate();
		}
		@Override
		public void removeUpdate(DocumentEvent e) {
			if (ignoreDocumentEvents) 
				return;
			if (ListOfValues.this.cbxQuickChooser.isPopupVisible()) {
				inputChanged.enable();
			}
		}
		@Override
		public void changedUpdate(DocumentEvent e) {
			if (ignoreDocumentEvents) 
				return;
			inputChanged.handleUpdate();
		}
	}

	class InputChanged extends Object {
		private long enabledAt = 0l;
		private boolean isEnabled = true;
		
		private void handleUpdate() {
			// verarbeite nur Events die 25 ms nach einer Tastatureingabe getätigt wurden.
			if (isEnabled && enabledAt + 25 >= System.currentTimeMillis()) {
				startSearch(false);
			}
		}
		
		private void startSearch(final boolean bFromDropDown) {
			changesPending = true;
			if (lastTimerTask != null) {
				lastTimerTask.cancel();
			}
			
			if (bFromDropDown) {
				actionSearch.actionPerformed(false, true);
				return;
			}

			// lastTimerTask = new Timer(this.getClass().getName() + " quick-search-delay");
			final Timer timer = (Timer) SpringApplicationContextHolder.getBean("timer");
			lastTimerTask = new TimerTask() {
				@Override
				public void run() {
					UIUtils.invokeOnDispatchThread(new Runnable() {
						@Override
						public void run() {
							try {
								// avoid double execution
								if (ListOfValues.this.hasFocus()) {
									if (tf.getText().length() >= 2) {
										actionSearch.actionPerformed(false, false);
									}
								}
							} catch (Exception e) {
								LOG.error("quick search delay timer task failed: " + e, e);
							}
						}
					});
				}
			};
			timer.schedule(lastTimerTask, QUICKSEARCH_DELAY_TIME);
		}

		public void enable() {
			enabledAt = System.currentTimeMillis();
		}
		
		/**
		 * set enabled 
		 * 
		 * @param enabled
		 */
		public void setEnabled(boolean enabled) {
			isEnabled = enabled;
		}
	}

	/**
	 *
	 */
	private void hideQuickSearch() {
		ListOfValues.this.cbxQuickChooser.hidePopup();
	}

	/**
	 *
	 *
	 */
	class QuickSearchActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (transferQuickSearchValue)
				actionPerformedQuickSearchSelected();
		}
	}

	class QuickSearchNavigationAction extends AbstractAction {

		private final boolean navUp;

		public QuickSearchNavigationAction(boolean navUp) {
			this.navUp = navUp;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				ListOfValues.this.transferQuickSearchValue = false;
				JComboBox cbx = ListOfValues.this.cbxQuickChooser;
				if (cbx.isPopupVisible()) {

					InputMap map = cbx.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
					ActionMap am = cbx.getActionMap();

					if	(map != null && am != null && isEnabled()) {
						KeyStroke ks = KeyStroke.getKeyStroke((navUp ? KeyEvent.VK_UP : KeyEvent.VK_DOWN), 0);
						KeyEvent ke = new KeyEvent(
								ListOfValues.this.cbxQuickChooser,
								KeyEvent.KEY_RELEASED,
								e.getWhen(),
								e.getModifiers(),
								(navUp ? KeyEvent.VK_UP : KeyEvent.VK_DOWN),
								KeyEvent.CHAR_UNDEFINED);

						Object binding = map.get(ks);
						Action action = (binding == null) ? null : am.get(binding);
						if (action != null) {
							SwingUtilities.notifyAction(action, ks, ke, cbx, ke.getModifiers());
						}
					}
				}
			} finally {
				ListOfValues.this.transferQuickSearchValue = true;
			}
		}
	}

	class QuickSearchAction extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent e) {
			actionPerformed(false, false);
		}
		
		public void actionPerformed(boolean forceSelect, boolean bDropDownOpens) {
			runQuickSearch(forceSelect, isMultiSelect(), bDropDownOpens, true, true);
		}
	}

	class SearchingWorker implements CommonClientWorker {

		private final String inputString;
		private final boolean forceSelect;
		private boolean forceResults;
		private boolean showPopup;
		private boolean dropDownOpens;

		private boolean stopped = false;
		List<CollectableValueIdField> qsResult;

		public SearchingWorker(String inputString, boolean forceSelect, boolean dropDownOpens) {
			super();
			this.inputString = inputString;
			this.forceSelect = forceSelect;
			this.forceResults = false;
			this.dropDownOpens = dropDownOpens;
		}

		public void setForceResults(boolean forceResults) {
			this.forceResults = forceResults;
		}

		public void setShowPopup(boolean showPopup) {
			this.showPopup = showPopup;
		}
		
		@Override
		public void work() throws CommonBusinessException {
			qsResult = (quickSearchResulting != null ? 
					quickSearchResulting.getQuickSearchResult(inputString, !dropDownOpens) : new ArrayList<CollectableValueIdField>());

			if (!searchRunning)
				return;
			searchRunning = false;
		}

		@Override
		public void paint() throws CommonBusinessException {
			if (isStopped()) {
				return;				
			}

			ListOfValues.this.transferQuickSearchValue = false;
			ListOfValues.this.cbxQuickChooser.removeAllItems();
			
			int iQsResultSize = qsResult != null ? qsResult.size() : 0;
			switch (iQsResultSize) {
				case 0:
					ListOfValues.this.alertQuickSearch(ListOfValues.Alert.NO_RESULT_FOUND);
					if (forceSelect) {
						ListOfValues.this.actionPerformedQuickSearchSelected();
					}
					break;
				case 1:
					if (forceResults) {
						// show popup also for single result
						runMultipleResults();
					} else {
						ListOfValues.this.cbxQuickChooser.addItem(qsResult.get(0));
						ListOfValues.this.actionPerformedQuickSearchSelected();
						if (colorProviderBackup != null || defaultColorBackup != null) {
							ListOfValues.this.alertQuickSearch(ListOfValues.Alert.ONE_RESULT_SELECTION);
						}
					}
					break;
				default:
					runMultipleResults();
			}
			
			ListOfValues.this.transferQuickSearchValue = true;
			this.forceResults = false;
		}

		private void runMultipleResults() {
			if (forceSelect) {
				ListOfValues.this.alertQuickSearch(ListOfValues.Alert.NO_RESULT_FOUND);
				ListOfValues.this.actionPerformedQuickSearchSelected(true);
				return;
			}

			for (CollectableValueIdField item : qsResult) {
				ListOfValues.this.cbxQuickChooser.addItem(item);
				if (ListOfValues.this.selectedItems.contains(item)) {
					item.setSelected(true);
				}
			}

			if (qsResult.size() == ICollectableListOfValues.QUICKSEARCH_MAX) {
				ListOfValues.this.cbxQuickChooser.addItem("...");
			}

			if (qsResult.size() < QUICKSEARCH_POPUP_ROWS) {
				for (int i = 1; i < QUICKSEARCH_POPUP_ROWS - qsResult.size(); i++) {
					ListOfValues.this.cbxQuickChooser.addItem(" ");
				}
			}

			ListOfValues.this.cbxQuickChooser.setWide(true);

			if (!ListOfValues.this.cbxQuickChooser.isPopupVisible() && showPopup)
				ListOfValues.this.cbxQuickChooser.showPopup();
		}

		@Override
		public void init() throws CommonBusinessException {
			ListOfValues.this.searchRunning = true;
			
			if (isMultiSelect()) {
				for (Iterator<Object> it = ListOfValues.this.selectedItems.iterator(); it.hasNext();) {
					Object o = it.next();
					if (ListOfValues.this.cbxQuickChooser.getModel() instanceof DefaultComboBoxModel) {
						if (((DefaultComboBoxModel)ListOfValues.this.cbxQuickChooser.getModel()).getIndexOf(o) != -1) {
							it.remove();
						}
					}
				}
				ListOfValues.this.selectedItems.addAll(getMultiSelectItems());
			}
		}

		@Override
		public void handleError(Exception e) {
			if (e instanceof IllegalComponentStateException) {
				// do nothing. popup could not be shown. maybe the frame is closed.
				LOG.error("handleError: " + e, e);
			} else {
				Errors.getInstance().showExceptionDialog(getResultsComponent(), e);
			}
		}

		@Override
		public JComponent getResultsComponent() {
			return ListOfValues.this.tf;
		}

		public boolean isStopped() {
			return stopped;
		}

		public void setStopped(boolean stopped) {
			this.stopped = stopped;
		}
	};

	class QuickSearchCancelAction extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (searchRunning) {
				searchRunning = false;
				ListOfValues.this.setEnabled(true);
			}
			if (ListOfValues.this.quickSearchCanceledListener != null) {
				ListOfValues.this.quickSearchCanceledListener.actionPerformed(new ActionEvent(ListOfValues.this, 0, ListOfValues.QUICK_SEARCH_CANCEL));
			}
			changesPending = false;
			hideQuickSearch();
		}
	}

	private void actionPerformedQuickSearchSelected() {
		actionPerformedQuickSearchSelected(false);
	}

	protected void actionPerformedQuickSearchSelected(boolean forceClear) {
		if (this.quickSearchSelectedListener != null) {
			if (forceClear) {
				this.quickSearchSelectedListener.actionPerformed(null);
			} else {
				if (ListOfValues.this.cbxQuickChooser.getSelectedIndex() >= 0 &&
//						ListOfValues.this.cbxQuickChooser.getSelectedIndex() < CollectableListOfValues.QUICKSEARCH_MAX &&
						ListOfValues.this.cbxQuickChooser.getSelectedItem() != null &&
						ListOfValues.this.cbxQuickChooser.getSelectedItem() instanceof CollectableValueIdField) {
					try {
						ignoreDocumentEvents = true;
						this.quickSearchSelectedListener.actionPerformed(((CollectableValueIdField) ListOfValues.this.cbxQuickChooser.getSelectedItem()));
					} finally {
						ignoreDocumentEvents = false;
					}
				}	
			}
		}
		if (this.cbxQuickChooser.isPopupVisible()) {
			this.cbxQuickChooser.hidePopup();
		}
	}

	public void setQuickSearchSelectedListener(final QuickSearchSelectedListener qssl) {
		this.quickSearchSelectedListener = qssl;
	}

	public QuickSearchSelectedListener getQuickSearchSelectedListener() {
		return quickSearchSelectedListener;
	}

	public void setQuickSearchCanceledListener(final ActionListener al) {
		this.quickSearchCanceledListener = al;
	}

	public boolean isSearchOnLostFocus() {
		return searchOnLostFocus;
	}

	public void setSearchOnLostFocus(boolean searchOnLostFocus) {
		this.searchOnLostFocus = searchOnLostFocus;
	}
	
	public void setSearchOnEnterFocus(boolean searchOnEnterFocus) {
		this.searchOnEnterFocus = searchOnEnterFocus;
	}
	
	public boolean isSearchOnEnterFocus() {
		return searchOnEnterFocus;
	}

	public static abstract class QuickSearchResulting {

		protected abstract List<CollectableValueIdField> getQuickSearchResult(String inputString, boolean bLimit)  throws CommonBusinessException;
	}

	public static abstract class QuickSearchSelectedListener {
		public abstract void actionPerformed(CollectableValueIdField itemSelected);
	}

	private static enum Alert {
		ONE_RESULT_SELECTION,
		NO_RESULT_FOUND;
	}

	private void alertQuickSearch(final Alert alert) {
		alertRunning = true;
		final Color color;
		switch (alert) {
		case ONE_RESULT_SELECTION:
			color = null;//new Color(180, 250, 170);
			break;
		case NO_RESULT_FOUND:
			color = new Color(250, 140, 140);
			break;
		default:
			color = Color.YELLOW;
		}

		final Color defaultColor;
		if (defaultColorBackup != null) {
			defaultColor = defaultColorBackup;
		} else {
			defaultColor = this.tf.getBackground();
		}
		final ColorProvider colorProvider;
		if (colorProviderBackup != null) {
			colorProvider = colorProviderBackup;
		} else {
			colorProvider = support.getColorProvider();
		}

		final Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (color != null) {
						Thread.sleep(200);
						support.setColorProvider(null);
						SwingUtilities.invokeAndWait(new Runnable() {
							@Override
							public void run() {
								ListOfValues.this.tf.setBackground(color);
								ListOfValues.this.tf.repaint();
							}
						});
					}

					if (alert == Alert.NO_RESULT_FOUND) {
						// backup the color provider and the default color only, for later restore...
						colorProviderBackup = colorProvider;
						defaultColorBackup = defaultColor;
					} else {
						if (color != null) {
							Thread.sleep(500);
						}
						SwingUtilities.invokeAndWait(new Runnable() {
							@Override
							public void run() {
								ListOfValues.this.tf.setBackground(defaultColor);
								ListOfValues.this.tf.repaint();
								support.setColorProvider(colorProvider);
							}
						});
						colorProviderBackup = null;
						defaultColorBackup = null;
					}

					alertRunning = false;
				} catch (Exception ex) {
					// do nothing...
				}
			}
		}, "ListOfValues.AlertQuickSearch");

		t.start();
	}

	public List<CollectableValueIdField> getQSR(String text) throws CommonBusinessException {
		if (quickSearchResulting == null) return null;
		return quickSearchResulting.getQuickSearchResult(text, true);
	}

	public QuickSearchResulting getQuickSearchResulting() {
		return quickSearchResulting;
	}

	public void setQuickSearchResulting(QuickSearchResulting quickSearchResulting) {
		this.quickSearchResulting = quickSearchResulting;
	}

	/**
	 * is quick search popup opend
	 * 
	 * @return
	 */
	public boolean isQuickSearchPopupVisible() {
		return ListOfValues.this.cbxQuickChooser.isPopupVisible();
	}

	public void runQuickSearch(boolean forceSelect, boolean forceResults, boolean bDropDownOpens, boolean bMultiThreaded, boolean bShowPopup) {
		++searchInvokeCounter;
		
		String inputString;
		if (bDropDownOpens) {
			inputString = "*";
		} else {
			inputString = ListOfValues.this.tf.getText();
		}

		
		if (StringUtils.looksEmpty(inputString) || !ListOfValues.this.tf.isEditable() || alertRunning) {
			return;			
		}

		if (lastSearchingWorker != null) {
			lastSearchingWorker.setStopped(true);
		}
		lastSearchingWorker = new SearchingWorker(inputString, forceSelect, bDropDownOpens);
		lastSearchingWorker.setForceResults(forceResults);
		lastSearchingWorker.setShowPopup(bShowPopup);
		if (bMultiThreaded) {
			CommonMultiThreader.getInstance().executeInterruptible(lastSearchingWorker);
		} else {
			try {
				lastSearchingWorker.init();
				lastSearchingWorker.work();
				lastSearchingWorker.paint();
			} catch (final Exception ex) {
				lastSearchingWorker.handleError(ex);
				return;
			}
		}

		changesPending = false;
	}
	
	public void hideQuickSearchPopup() {
		hideQuickSearch();
	}
	
	public boolean isSearchRunning() {
		return searchRunning;
	}
	
	/**
	 * get amount of search actions
	 * 
	 * @return
	 */
	public Integer getSearchInvokeCounter() {
		return searchInvokeCounter;
	}
	
	public void disableInputHandler() {
		this.inputChanged.setEnabled(false);
		if (null != lastTimerTask) {
			lastTimerTask.cancel();
		}
	}
	
	public void enableInputHandler() {
		this.inputChanged.setEnabled(true);
	}

	@Override
	public void setLayoutNavigationCollectable(LayoutNavigationCollectable lnc) {
		this.lnc = lnc;
	}
	
	public void setMultiSelect(Boolean multiSelect) {
		this.cbxQuickChooser.setMultiSelect(multiSelect);
	}
	
	public Boolean isMultiSelect() {
		return this.cbxQuickChooser.isMultiSelect();
	}
	
	public void setLovBtn(Boolean bLovButton) {
		tf.setLovBtn(bLovButton);
	}
	
	public void setLovSearch(Boolean bLovSearch) {
		tf.setLovSearch(bLovSearch);
	}
	
	public boolean hasLovSearch() {
		return tf.hasLovSearch();
	}

	public void setDropdownBtn(Boolean bDropdownBtn) {
		tf.setDropdownBtn(bDropdownBtn);
	}

	public void setCustomUsageSearch(String customUsageSearch) {
		tf.setCustomUsageSearch(customUsageSearch);
	}

	public List<Object> getMultiSelectItems() {
		
		ComboBoxEditor editor = cbxQuickChooser.getEditor();
		if (editor instanceof MultiSelectComboBoxEditor) {
			return new ArrayList(((MultiSelectComboBoxEditor)editor).getItems());
		}
		
		List<Object> items = new ArrayList<Object>();
		items.add(getJTextField().getText());
		return items;
	}
	
	public JComboBox getComboBox() {
		return cbxQuickChooser;
	}

	public void setSelection(Object oValueId) {
		this.selectedItems.clear();
		if (oValueId != null) {
			this.selectedItems.add(oValueId);			
		}
		for (int i = 0; i < this.cbxQuickChooser.getItemCount(); i++) {
			Object o = this.cbxQuickChooser.getItemAt(i);
			if (o instanceof CollectableValueIdField) {
				CollectableValueIdField clctvid = (CollectableValueIdField) o;
				if (clctvid != null) {
					if (clctvid.getValueId().equals(o)) {
						clctvid.setSelected(true);
					} else {
						clctvid.setSelected(false);
					}
				}
			}
		}
	}

	public void enableButton() {
		tf.enableDropButtonLater();
	}

	public void clearTextField() {
		tf.setText(null);
	}
}  // class ListOfValues
