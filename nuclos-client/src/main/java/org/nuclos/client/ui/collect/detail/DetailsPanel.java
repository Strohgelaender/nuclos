//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.detail;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.basic.BasicButtonUI;

import org.apache.log4j.Logger;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.common.LafParameterHelper;
import org.nuclos.client.common.LafParameterHelper.BoolLafParameterEditor;
import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.IOverlayFrameChangeListener;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.MainFrameTabListener;
import org.nuclos.client.ui.StatusBarTextField;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.EditView;
import org.nuclos.client.ui.collect.SearchOrDetailsPanel;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModel;
import org.nuclos.client.ui.collect.component.model.DetailsEditModel;
import org.nuclos.client.ui.collect.indicator.CollectPanelIndicator;
import org.nuclos.client.ui.collect.result.ResultPanel.StatusBarButton;
import org.nuclos.common.LafParameter;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.DetailsPresentation;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;

/**
 * Details panel for collecting data.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public class DetailsPanel extends SearchOrDetailsPanel {
	
	private static final Logger LOG = Logger.getLogger(DetailsPanel.class);
	
	/**
	 * If the details panel is opened as popup, this are the insets w.r.t. the parent window.
	 */
	private static final int POPUP_INSET_X = 20;
	private static final int POPUP_INSET_Y = 20;
	
	private final CollectPanelIndicator cpi = new CollectPanelIndicator(CollectPanel.TAB_DETAILS);
	
	private final List<IOverlayFrameChangeListener> overlayChangeListeners = new ArrayList<IOverlayFrameChangeListener>(1);

	/**
	 * Button: "Navigate to the first object"
	 */
	public final JButton btnFirst = new JButton();

	/**
	 * Button: "Navigate to the previous object"
	 */
	public final JButton btnPrevious = new JButton();

	/**
	 * Button: "Navigate to the last object"
	 */
	public final JButton btnLast = new JButton();

	/**
	 * Button: "Navigate to the first object"
	 */
	public final JButton btnNext = new JButton();
	
	public final StatusBarButton btnSplitOptions = new StatusBarButton("-|-");
		
	/**
	 * Non-final for close (tp)
	 */
	private JScrollPane scrlpnDetails;
	
	public static final int recordNavIconSize = 11;
	
	private JDialog dialog;
	
	public final StatusBarTextField tfMetaInformation = new StatusBarTextField("");
	
	private boolean showAllMetaInformation = false;
	
	private final DetailsPresentation detailsPresentation;
	
	/**
	 * constructs the details panel
	 */
	public DetailsPanel(UID entityId, DetailsPresentation detailsPresentation, ControllerPresentation controllerPresentation) {
		this(entityId, detailsPresentation, controllerPresentation, true);
	}

	/**
	 * constructs the details panel
	 */
	public DetailsPanel(UID entityId, DetailsPresentation detailsPresentation, ControllerPresentation controllerPresentation, boolean withScrollbar) {
		super(entityId, false);
		this.detailsPresentation = detailsPresentation;
//		btnDelete = getDeleteButton();
		super.init();
		
		//this.add(pnlToolBar, BorderLayout.NORTH);
		
		if (withScrollbar) {
			this.scrlpnDetails = new JScrollPane(getCenteringPanel(), JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			this.scrlpnDetails.setBorder(BorderFactory.createEmptyBorder());
			this.scrlpnDetails.getHorizontalScrollBar().setUnitIncrement(20);
			this.scrlpnDetails.getVerticalScrollBar().setUnitIncrement(20);
			
			//this.add(scrlpnDetails, BorderLayout.CENTER);
			//this.add(UIUtils.newStatusBar(tfStatusBar), BorderLayout.SOUTH);
			
			this.setCenterComponent(scrlpnDetails);
		} else {
			this.scrlpnDetails = null;
			this.setCenterComponent(getCenteringPanel());
		}
		if (controllerPresentation == ControllerPresentation.SPLIT_DETAIL) {
			this.setSouthComponent(UIUtils.newStatusBar(this.btnSplitOptions, 
					Box.createHorizontalGlue(), this.tfMetaInformation, Box.createHorizontalStrut(4)));
		} else if (LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Details_Navigation_Enabled, entityId)) {
			this.setSouthComponent(UIUtils.newStatusBar(this.btnFirst, this.btnPrevious, this.btnNext, this.btnLast, Box.createHorizontalStrut(10), this.tfStatusBar, 
					Box.createHorizontalGlue(), this.tfMetaInformation, Box.createHorizontalStrut(4)));
		} else {
			this.setSouthComponent(UIUtils.newStatusBar(this.tfStatusBar, 
					Box.createHorizontalGlue(), this.tfMetaInformation, Box.createHorizontalStrut(4)));
		}
		this.tfMetaInformation.setHorizontalAlignment(JTextField.RIGHT);
		
		final BoolLafParameterEditor editor = new BoolLafParameterEditor(
				LafParameter.nuclos_LAF_Details_All_Meta_Information_Enabled, entityId, tfMetaInformation);
		LafParameterHelper.installPopup(editor);
		
		this.btnFirst.setOpaque(false);
		this.btnFirst.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
		this.btnFirst.setContentAreaFilled(false);
		this.btnFirst.setName("btnFirst");
		this.btnFirst.setRolloverIcon(MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconFirstWhiteHover16(), recordNavIconSize));
		this.btnFirst.setUI(new BasicButtonUI());
		
		this.btnPrevious.setOpaque(false);
		this.btnPrevious.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
		this.btnPrevious.setContentAreaFilled(false);
		this.btnPrevious.setName("btnPrevious");
		this.btnPrevious.setRolloverIcon(MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconPreviousWhiteHover16(), recordNavIconSize));
		this.btnPrevious.setUI(new BasicButtonUI());
		
		this.btnNext.setOpaque(false);
		this.btnNext.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
		this.btnNext.setContentAreaFilled(false);
		this.btnNext.setName("btnNext");
		this.btnNext.setRolloverIcon(MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconNextWhiteHover16(), recordNavIconSize));
		this.btnNext.setUI(new BasicButtonUI());
		
		this.btnLast.setOpaque(false);
		this.btnLast.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
		this.btnLast.setContentAreaFilled(false);
		this.btnLast.setName("btnLast");
		this.btnLast.setRolloverIcon(MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconLastWhiteHover16(), recordNavIconSize));
		this.btnLast.setUI(new BasicButtonUI());
		
		this.showAllMetaInformation = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Details_All_Meta_Information_Enabled, entityId);
	}
	
	public final DetailsPresentation getDetailsPresentation() {
		return detailsPresentation;
	}

	public final CollectPanelIndicator getCollectPanelIndicator() {
		return cpi;
	}
	
	@Override
	protected void setupDefaultToolBarActions( JToolBar toolBar) {	
		toolBar.add(cpi.getJPanel());		
	}

	@Override
	public void close() {
		super.close();
		if (scrlpnDetails != null) {
			scrlpnDetails.removeAll();
			scrlpnDetails = null;
		}
	}
	
	/**
	 * @return the model of the edit view.
	 * @see #getEditView()
	 */
	@Override
	public DetailsEditModel getEditModel() {
		final EditView ev = getEditView();
		// EditView could be null if the controller is closing...
		if (ev == null) {
			return null;
		}
		return (DetailsEditModel) ev.getModel();
	}

	public static String getTextForMultiEditChange(DetailsPanel pnlDetails) {
		return pnlDetails.getMultiEditChangeMessage();
	}

	/**
	 * §precondition this.getCollectStateModel().getCollectState().isDetailsModeMultiViewOrEdit()
	 * §todo this.isMultiEditable() bzw. this.getState().isMulti[ViewOrEdit]()
	 * 
	 * @return the message to display in the status bar when changes in multi edit mode occured.
	 */
	public String getMultiEditChangeMessage() {
		final StringBuffer sb = new StringBuffer();

		for (DetailsComponentModel clctcompmodel : this.getEditModel().getCollectableComponentModels()) {
			if (clctcompmodel.isValueToBeChanged()) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(clctcompmodel.getEntityField().getLabel());
				sb.append(" = ");
				final CollectableField clctfValue = clctcompmodel.getField();
				if (clctfValue.isNull()) {
					sb.append(SpringLocaleDelegate.getInstance().getMessage("DetailsPanel.3", "<leer>"));
				}
				else {
					sb.append(clctfValue.toString());
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @param component
	 * @return false if component not found or location on screen could not be determinded
	 */
	public boolean ensureComponentIsVisible(JComponent component) {	
		List<JComponent> startPath = new ArrayList<JComponent>();
		startPath.add(getEditComponent());
		List<JComponent> resultPath = getComponentPath(startPath, component);
		if (resultPath != null) {
			for (int i = 0; i < resultPath.size(); i++) {
				JComponent c = resultPath.get(i);
				if (c instanceof JTabbedPane) {
					JTabbedPane tab = (JTabbedPane) c;
					if (resultPath.size() >= i+1) {
						int tabindex = tab.indexOfComponent(resultPath.get(i+1));
						if (tab.getSelectedIndex() != tabindex)
							tab.setSelectedIndex(tabindex);
					}
				}
			}
			
			for (int i = 0; i < resultPath.size(); i++) {
				Point location = resultPath.get(i).getLocation();
				resultPath.get(i).scrollRectToVisible(new Rectangle(location, resultPath.get(i).getPreferredSize()));
			}
			
			Point targetLocation = component.getLocation();
			component.scrollRectToVisible(new Rectangle(targetLocation, component.getPreferredSize()));
			
			try {
				component.getLocationOnScreen();
				return true;
			} catch (IllegalComponentStateException e) {
				return false;
			}
		}
		return false;
	}
	
	private List<JComponent> getComponentPath(List<JComponent> path, JComponent target) {
		for (Component c : path.get(path.size()-1).getComponents()) {
			if (c == target)
				return path;
			else
				if (c instanceof JComponent) {
					List<JComponent> extendedPath = new ArrayList<JComponent>(path);
					extendedPath.add((JComponent) c);
					List<JComponent> resultTemp = getComponentPath(extendedPath, target);
					if (resultTemp != null)
						return resultTemp;
				}
		}
		
		return null;
	}
	
	/**
	 * shows a spot on <code>comp</code>
	 * @param comp
	 */
	public void spotComponent(JComponent comp) {
		try {
			(new ComponentSpot(comp)).setVisible(true);
		} catch (Exception e) {
			LOG.error(e);
		}
	}
	
	private class ComponentSpot extends Window implements AncestorListener {
		
		private JComponent parent;

		ComponentSpot(JComponent parent) throws HeadlessException {
			super(null);
			this.parent = parent;
			if(parent != null) {
				addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) { ComponentSpot.this.dispose(); }
				});
				parent.addAncestorListener(this);
				Window windowAncestor = SwingUtilities.getWindowAncestor(parent);
				if(windowAncestor != null)
					windowAncestor.addWindowListener(new WindowAdapter() {
						@Override
						public void windowDeactivated(WindowEvent e) { ComponentSpot.this.dispose(); }
						@Override
						public void windowIconified(WindowEvent e) { ComponentSpot.this.dispose(); }
						@Override
						public void windowClosing(WindowEvent e) { ComponentSpot.this.dispose(); }
					});
				relocate(parent);
			}
			// final Timer timer = new Timer();
			final Timer timer = (Timer) SpringApplicationContextHolder.getBean("timer");
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						dispose();
						// timer.cancel();
					} catch (Exception e) {
						LOG.error("component spot dispose timer task failed: " + e, e);
					}
				}}, 5 * 1000);
			
			setAlwaysOnTop(true);
			UIUtils.setWindowOpacity(ComponentSpot.this, 0.5f);
		}
		
		@Override
		public void paint(Graphics g) {
			if (parent != null) {
				Graphics2D g2 = (Graphics2D) g;
				
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); 
				g2.setColor(Color.BLACK);
				
				Point locationParent = parent.getLocationOnScreen();
				Point locationDetails = DetailsPanel.this.getEditComponent().getLocationOnScreen();
				Dimension sizeParent = parent.getSize();
				Dimension sizeDetails = DetailsPanel.this.getEditComponent().getSize();
				
				int x1, x2, x3, x4;
				int y1, y2, y3, y4;
				
				x1 = 0;
				x2 = locationParent.x-locationDetails.x;
				x3 = x2 + sizeParent.width;
				x4 = sizeDetails.width;
				
				y1 = 0;
				y2 = locationParent.y-locationDetails.y;
				y3 = y2 + sizeParent.height;
				y4 = sizeDetails.height;
				
				g2.fillRect(x1, y1, x4-x1, y4-y1);
				g2.setComposite(AlphaComposite.getInstance(AlphaComposite.DST_OUT));
				g2.setPaint(new GradientPaint(new Point(x2+5, y2-10), new Color(255,255,255,0), new Point(x3-20, y3), Color.BLACK));
				g2.fillOval(x2-10, y2-10, x3-x2+20, y3-y2+20);
			}
		}

		private void relocate(Component parent) {
			Point location = DetailsPanel.this.getEditComponent().getLocationOnScreen();
			setBounds(location.x, location.y, DetailsPanel.this.getEditComponent().getSize().width, DetailsPanel.this.getEditComponent().getSize().height);
	    }
		
		@Override
		public void ancestorAdded(AncestorEvent event) {
		}

		@Override
		public void ancestorRemoved(AncestorEvent event) {
			ComponentSpot.this.dispose();
		}

		@Override
		public void ancestorMoved(AncestorEvent event) {
			relocate (event.getComponent());
		}
	}
	
	public void showInPopup(final MainFrameTab tab) {
		final MainFrameTabListener titleListener = new MainFrameTabAdapter() {
			@Override
			public void tabTitleChanged(MainFrameTab tab) {
				if (dialog != null) {
					dialog.setTitle(tab.getTitle());
				} else {
					tab.removeMainFrameTabListener(this);
				}
			}
		};
		tab.addMainFrameTabListener(titleListener);
		dialog = new JDialog(UIUtils.getFrameForComponent(tab), tab.getTitle(), true);
		final JPanel root = new JPanel(new BorderLayout());
		dialog.setContentPane(root);
		root.add(getToolBar(), BorderLayout.NORTH);
		showToolbar(true);
		root.add(this, BorderLayout.CENTER);
		
		/*
		Rectangle bounds = tab.getBounds();
		 */
		// https://support.novabit.de/browse/BMWFDM-723
		final GraphicsConfiguration gc = tab.getGraphicsConfiguration();
		final Rectangle bounds = gc.getBounds();
		final Point location = bounds.getLocation();
		location.translate(POPUP_INSET_X, POPUP_INSET_Y);
		bounds.grow(-2 * POPUP_INSET_X, -2 * POPUP_INSET_Y);
		
		setBounds(bounds);
		setLocation(location);
		setVisible(true);
		dialog.setBounds(bounds);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				notifyClosing(new ResultListener<Boolean>() {
					@Override
					public void done(Boolean result) {
					if (Boolean.TRUE.equals(result)) {
						tab.removeMainFrameTabListener(titleListener);
						disposePopup();
					}
					}
				});
			}
		});
		dialog.setVisible(true);
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (!visible && dialog != null) {
			disposePopup();
		}
	}

	public void disposePopup() {
		super.close();
		dialog.dispose();
		dialog = null;
		notifyClosed();
	}
	
	public void resetStatusBarMetaInformation() {
		setStatusBarMetaInformation(null, null, null, null, null, null);
	}
	
	public void setStatusBarMetaInformation(String sCreatedBy, Date dCreatedAt, String sChangedBy, Date dChangedAt, Integer sVersion, Object pk) {
		String sText = StringUtils.looksEmpty(sChangedBy)?"":(SpringLocaleDelegate.getInstance().getText("nuclos.entityfield.eo.changedby.label") + ": " + sChangedBy);
		String sToolTipText = StringUtils.looksEmpty(sCreatedBy)?"":(SpringLocaleDelegate.getInstance().getText("nuclos.entityfield.eo.createdby.label") + ": " + sCreatedBy);
		if (showAllMetaInformation && pk != null) {
			sText = SpringLocaleDelegate.getInstance().getText("primarykey") + ": " + pk + (StringUtils.looksEmpty(sText)?"":", ") + sText;
		}
		if (dChangedAt != null) {
			sText = sText + " [" + SpringLocaleDelegate.getInstance().formatDate(dChangedAt) + " " + SpringLocaleDelegate.getInstance().formatTime(dChangedAt) + "]";
		}
		if (dCreatedAt != null) {
			sToolTipText = sToolTipText + " [" + SpringLocaleDelegate.getInstance().formatDate(dCreatedAt) + " " + SpringLocaleDelegate.getInstance().formatTime(dCreatedAt) + "]";
		}
		if (showAllMetaInformation && sVersion != null) {
			sToolTipText = sToolTipText + "<br>" + SpringLocaleDelegate.getInstance().getText("nuclos.entityfield.eo.version.label") + ": " + sVersion;
		}
		tfMetaInformation.setText(sText + " ");
		tfMetaInformation.setToolTipText("<html>" + sToolTipText + "</html> ");
	}


}  // class DetailsPanel
