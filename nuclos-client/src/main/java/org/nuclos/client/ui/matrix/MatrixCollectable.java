package org.nuclos.client.ui.matrix;

import java.awt.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.swing.*;
import javax.swing.border.Border;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * TODO
 * temp class, should be removed
 * @author marc.finke
 *
 */

public class MatrixCollectable implements Collectable, Comparable<MatrixCollectable> {

	Long iId;
	Object value;
	int version;
	boolean bRemoved = false;
	boolean bModified = false;
	UID sField;
	//EntityFieldMetaDataVO metaVo;
	FieldMeta<Object> metaVo;
	EntityObjectVO vo;
	private String dataType;
	private GroupKey ygroupkey;
	private boolean marked;
	private Object yId;

	public MatrixCollectable(Object value) {
		this(null, value, 0, null);
	}
	
	public MatrixCollectable(Long iId, Object value) {
		this(iId, value, 0, null);
	}
	
	public MatrixCollectable(Long iId, Object value, int iVersion) {
		this(iId, value, iVersion, null);
	}

	public MatrixCollectable(Long iId, Object value, int iVersion, String dataType) {
		this.iId = iId;
		this.value = value;
		this.version = iVersion;
		this.dataType = dataType;
	}

	public void setVO(EntityObjectVO vo) {
		this.vo = vo;
	}
	
	public EntityObjectVO getVO() {
		return this.vo;
	}
	
	public void setEntityFieldMetaDataVO(FieldMeta<Object> vo) {
		this.metaVo = vo;
	}
	
	public FieldMeta<Object> getEntityFieldMetaDataVO() {
		return this.metaVo;
	}
	
	public void setField(UID field) {
		this.sField = field;		
	}
	
	public UID getField() {
		return this.sField;
	}
	
	@Override
	public int getVersion() {
		if(version < 0)
			return 0;
		return version;
	}

	@Override
	public Object getId() {
		return this.iId;
	}

	@Override
	public void removeId() {
		this.iId = null;
	}
	
	public String getIdentifierLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getValue(UID sFieldName) {
		return value;
	}

	@Override
	public Object getValueId(UID sFieldName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CollectableField getField(UID sFieldName)
			throws CommonFatalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setField(UID sFieldName, CollectableField clctfValue) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isComplete() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String toString() {
		if(getValue(UID.UID_NULL) == null)
			return null;
		return getValue(UID.UID_NULL).toString();
	}

	@Override
	public int hashCode() {
		if(getValue(UID.UID_NULL) == null)
			return 0;
		return getValue(UID.UID_NULL).toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof MatrixCollectable)) return false;
		
		MatrixCollectable that = (MatrixCollectable)obj;		
		if(that.getId() != null && getId() != null) {
			return that.getId().equals(getId());
		}
		return ObjectUtils.equals(that.getValue(UID.UID_NULL), this.getValue(UID.UID_NULL));
	}
	
	public void markAsModified() {
		bModified = true;
	}
	
	public boolean isModified() {
		return bModified;
	}
	
	public void markAsRemoved() {
		bRemoved = true;
	}
	
	public boolean isRemoved() {
		return bRemoved;
	}

	public String getDataType() {
		return dataType;
	}

	@Override
	public int compareTo(MatrixCollectable o) {
		return compareTo(o, true);
	}


	public int compareTo(MatrixCollectable o, boolean ascending) {
		if (o == null) {
			return 0;
		}

		if (ygroupkey != null) {
			int c = ygroupkey.compareTo(o.ygroupkey, ascending);
			if (c != 0) {
				return c;
			}
		} else if (o.ygroupkey != null) {
			return -1;
		}

		Object value = getValue(UID.UID_NULL);
		Object oValue = o.getValue(UID.UID_NULL);
		if (oValue instanceof String) {
			return StringUtils.compareIgnoreCase((String)value, (String)oValue);
		}
		else if (oValue instanceof Comparable) {
			return ObjectUtils.compare((Comparable)value, (Comparable)oValue);
		}

		return 0;
	}

	@Override
	public Object getPrimaryKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UID getEntityUID() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isDirty() {
		return bModified || bRemoved;
	}

	@Override
	public Object getLocalizedValue(UID field, UID language) {
		return null;
	};
	
	public IDataLanguageMap getDataLanguageMap(){
		// Override if needed
		return null;
	}

	public GroupKey getYgroupkey() {
		return ygroupkey;
	}

	public void setYgroupkey(final GroupKey ygroupkey) {
		this.ygroupkey = ygroupkey;
	}

	public static class GroupKey implements Comparable<GroupKey> {
		private final int group;
		private final int size;
		private final int rank;
		private final Collection<UID> identicalFields;

		public GroupKey(final int group, final int size, final int rank) {
			this.group = group;
			this.size = size;
			this.rank = rank;
			this.identicalFields = new HashSet<>();
		}

		public int getGroup() {
			return group;
		}

		public int getSize() {
			return size;
		}

		public Border getBorder(boolean left, boolean right) {
			if (rank == 0) {
				return BorderFactory.createMatteBorder(1, left ? 1 : 0, 0, right ? 1: 0, Color.GRAY);
			}
			if (rank == size - 1) {
				return BorderFactory.createMatteBorder(0, left ? 1 : 0, 1, right ? 1: 0, Color.GRAY);
			}
			return BorderFactory.createMatteBorder(0, left ? 1 : 0, 0, right ? 1: 0, Color.GRAY);
		}

		public Collection<UID> getIdenticalFields() {
			return identicalFields;
		}

		@Override
		public int compareTo(final GroupKey o) {
			return compareTo(o, true);
		}

		public int compareTo(final GroupKey o, boolean ascending) {
			if (o == null) {
				return 1;
			}
			int c1 = ObjectUtils.compare(o.group, group);
			if (c1 != 0) {
				return c1;
			}
			int c2 = ObjectUtils.compare(o.rank, rank);
			return ascending ? -c2 : c2;
		}
	}

	public void setMarked(final boolean marked) {
		this.marked = marked;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setYId(final Object yId) {
		this.yId = yId;
	}

	public Object getYId() {
		return yId != null ? yId : iId;
	}
}
