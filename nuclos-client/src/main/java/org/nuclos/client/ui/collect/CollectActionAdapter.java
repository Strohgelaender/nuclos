//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.util.Properties;

import javax.swing.*;

import org.nuclos.common.collect.collectable.Collectable;

/**
 * Interface implemented by layout buttons to trigger a custom action.
 * <p>
 * The association is done within 
 * org.nuclos.client.common.NuclosCollectController.MyLayoutMLButtonActionListener.actionPerformed(ActionEvent).
 * Properties passed into this interface are HACKed in there, i.e. it is in general <em>not</em> 
 * possible to set custom properties.
 * <p>
 * This interface is used in Nuclos core (e.g. ExecuteRuleButtonAction, ChangeStateAction,
 * SearchAction) for standard actions associated with layout buttons.
 * <p> 
 * In addition, this interface can be used in Nuclos extensions for having a custom
 * action on a layout button. The action could be set in 'Befehlstyp' in the layout
 * editor.
 * This is excessively used in the FDM extension. In this case properties are
 * <em>always</em> empty (?!, TODO: Or could you have <em>static</em> arguments with 'Argumente'?).
 * <p>
 * ATTENTION: In the layout editor, the combobox of 'Befehlstyp' <em>does not contain all 
 * implementations of this interface</em>. However, you could safely type in the fully 
 * qualified classname of your implementation there.
 * <p>
 * Old (obsolete) doc:
 * Performs an action inside a <code>CollectController</code>.
 * <p>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @author Thomas Pasch (improved javadoc)
 */
public interface CollectActionAdapter<PK,Clct extends Collectable<PK>> {
	
	void run(JButton btn, CollectController<PK,Clct> controller, Properties probs);
	
	boolean isRunnable(CollectController<PK,Clct> controller, Properties probs);
}
