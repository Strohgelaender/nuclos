//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar.editor;

import javax.swing.*;

class MenuCheckBox extends JCheckBoxMenuItem implements IToolBarComponent {
	private final ToolBarObject tbo;
	public MenuCheckBox(String name, ToolBarObject tbo) {
		super(name);
		this.tbo = tbo;
		setFocusable(false);
		setEnabled(false);
	}
	@Override
	public ToolBarObject getObject() {
		return this.tbo;
	}
	@Override
	public void reset() {
	}
	@Override
	public JComponent getJComponent() {
		return this;
	}
}