package org.nuclos.client.ui.collect.component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.masterdata.valuelistprovider.NonCacheableCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache.CachingCollectableFieldsProvider;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;

public class FieldsProviderSupport {
	
	private static final Logger LOG = Logger.getLogger(FieldsProviderSupport.class);
	
	private static final ConcurrentMap<FieldsProviderSupport, FieldsProviderWorker> mpWorkers = new ConcurrentHashMap<FieldsProviderSupport, FieldsProviderWorker>();
	
	private final CollectableFieldsProvider provider;

	private final boolean cached;
	
	public FieldsProviderSupport(CollectableFieldsProvider provider) {
		if (provider instanceof CachingCollectableFieldsProvider) {
			CachingCollectableFieldsProvider cingcfp = (CachingCollectableFieldsProvider) provider;
			this.provider = cingcfp.getDelegate();
			this.cached = true;
			
		} else {
			this.provider = provider;
			this.cached = false;

		}
	}
	
	public static class FieldsProviderWorker extends Thread {
		
		private final FieldsProviderSupport wsupport;
		private long finished = 0L;
		private List<CollectableField> fields = null;
		
		private FieldsProviderWorker(FieldsProviderSupport wsupport) {
			this.wsupport = wsupport;
		}
		
		@Override
		public void run() {
			int wait = wsupport.cached ?
					ClientParameterProvider.getInstance().getIntValue(ParameterProvider.KEY_VLP_LOADINGTHREAD_KEEP_ALIVE, 3000)
					: 0;
			getFields(wait);
			mpWorkers.remove(wsupport);
		}
		
		public void getFields(long wait) {
			try {
				fields = wsupport.provider.getCollectableFields();
			} catch (CommonBusinessException e) {
				LOG.error("FieldsProviderWorker failed: " + e, e);
			} catch (Exception e) {
				LOG.error("FieldsProviderWorker failed: " + e, e);
			} finally {
				finished = System.currentTimeMillis();
				if (fields == null) {
					fields = new ArrayList<CollectableField>();
				}
			}
			if (wait > 0) {
				try {
					Thread.sleep(wait);				
				} catch (InterruptedException ie) {
					LOG.error(ie.getMessage(), ie);
				}				
			}
		}
		
		public List<CollectableField> getCollectableFields() {
			return fields;
		}
		
		public boolean hasFinished() {
			return finished > 0L;
		}
	}
	
	public FieldsProviderWorker getFieldsProviderWorkerAndStartIfNew() {
		FieldsProviderWorker worker = mpWorkers.get(this);
		if (worker == null) {
			worker = new FieldsProviderWorker(this);
						
			if (provider instanceof NonCacheableCollectableFieldsProvider) {
				worker.getFields(0L);
			} else {
				mpWorkers.put(this, worker);
				worker.start();				
			}
		}
		return worker;			
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FieldsProviderSupport) {
			FieldsProviderSupport other = (FieldsProviderSupport)obj;
			return LangUtils.equal(provider, other.provider);
			
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(provider);
	}
	
	@Override
	public String toString() {
		return provider.toString();
	}
}
