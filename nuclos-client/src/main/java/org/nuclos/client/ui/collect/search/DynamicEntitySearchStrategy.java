//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.search;

import java.awt.*;
import java.util.List;

import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.datasource.admin.CollectableDataSource;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.E;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionToPredicateVisitor;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DynamicEntityVO;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class DynamicEntitySearchStrategy extends CollectSearchStrategy<UID,CollectableDataSource<DynamicEntityVO>> {

	private final DatasourceDelegate datasourcedelegate = DatasourceDelegate.getInstance();

	public DynamicEntitySearchStrategy() {
		//...
	}

	@Override
	public void search() {
		final CollectController cc = getCollectController();
		final MainFrameTab mft = cc.getTab();
		try {
			mft.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			
			List<MasterDataVO<UID>> lstmdvos = CollectionUtils.transform(this.datasourcedelegate.getAllDynamicEntities(), new MakeMasterDataVO());
			
			final CollectableSearchExpression clctexpr
				= new CollectableSearchExpression(getCollectableSearchCondition(), cc.getResultController().getCollectableSortingSequence());
			final List<CollectableEntityField> cefs = cc.getResultController().getFields().getSelectedFields();
			final String isp = cc.getResultPanel().fetchIncrSearchPattern();
			ClientSearchUtils.addTextSearchToSearchExpression(isp, cefs, clctexpr, cc.getEntityUid());
			
			final CollectableSearchCondition cond = clctexpr.getSearchCondition();
			if (cond != null)
				lstmdvos = CollectionUtils.<MasterDataVO<UID>>applyFilter(lstmdvos, cond.accept(new SearchConditionToPredicateVisitor()));
			
			List<CollectableDataSource> result = CollectionUtils.transform(lstmdvos, new MakeCollectable());
			if (getCollectableIdListCondition() != null) {
				result = CollectionUtils.applyFilter(result, new CollectableIdPredicate(getCollectableIdListCondition().getIds()));
			}
			
			cc.fillResultPanel(result);
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(mft, null, ex);
		} finally {
			mft.setCursor(Cursor.getDefaultCursor());
		}
	}

	private static class MakeCollectable implements Transformer<MasterDataVO<UID>, CollectableDataSource> {
		@Override
		public CollectableDataSource transform(MasterDataVO<UID> mdVO) {
			return new CollectableDataSource(getDatasourceVO(mdVO));
		}
		public static DynamicEntityVO getDatasourceVO(MasterDataVO<UID> mdVO) {
			DynamicEntityVO vo = new DynamicEntityVO(new NuclosValueObject<UID>(
					mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				mdVO.getFieldValue(E.DYNAMICENTITY.name),
				mdVO.getFieldValue(E.DYNAMICENTITY.description),
				mdVO.getFieldUid(E.DYNAMICENTITY.entity),
				mdVO.getFieldValue(E.DYNAMICENTITY.valid),
				mdVO.getFieldValue(E.DYNAMICENTITY.source),
				mdVO.getFieldUid(E.DYNAMICENTITY.nuclet));

			vo.setNuclet(mdVO.getFieldValue(E.DYNAMICENTITY.nuclet.getUID(), String.class));
			vo.setEntity((String) mdVO.getFieldValue(E.DYNAMICENTITY.entity.getUID()));
			vo.setMeta(mdVO.getFieldValue(E.DYNAMICENTITY.meta));
			vo.setQuery(mdVO.getFieldValue(E.DYNAMICENTITY.query));
			
			return vo;
		}
	}	
	private static class MakeMasterDataVO implements Transformer<DynamicEntityVO, MasterDataVO<UID>> {
		@Override
		public MasterDataVO<UID> transform(DynamicEntityVO vo) {
			return wrapDatasourceVO(vo);
		}
		public static MasterDataVO<UID> wrapDatasourceVO(DatasourceVO vo) {
			MasterDataVO<UID> result = new MasterDataVO<UID>(E.DYNAMICENTITY.getUID(), vo.getPrimaryKey(), 
					vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
			result.setFieldValue(E.DYNAMICENTITY.name, vo.getName());
			result.setFieldValue(E.DYNAMICENTITY.description, vo.getDescription());
			result.setFieldValue(E.DYNAMICENTITY.valid, vo.getValid());
			result.setFieldValue(E.DYNAMICENTITY.source, vo.getSource());
			result.setFieldUid(E.DYNAMICENTITY.nuclet, vo.getNucletUID());
			result.setFieldValue(E.DYNAMICENTITY.nuclet.getUID(), vo.getNuclet());
			
			result.setFieldUid(E.DYNAMICENTITY.entity, ((DynamicEntityVO) vo).getEntityUID());
			result.setFieldValue(E.DYNAMICENTITY.entity.getUID(), ((DynamicEntityVO) vo).getEntity());
			result.setFieldValue(E.DYNAMICENTITY.meta, vo.getMeta());
			result.setFieldValue(E.DYNAMICENTITY.query, vo.getQuery());
			
			return result;
		}
	}
}

