//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.*;
import java.util.List;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.labeled.ILabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledComponent;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common2.StringUtils;

/**
 * Default (abstract) implementation of a <code>CollectableComponent</code>,
 * consisting of a label and a second ("control") component.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public abstract class LabeledCollectableComponent extends AbstractCollectableComponent {
	
	private static final Logger LOG = Logger.getLogger(LabeledCollectableComponent.class);

	protected LabeledCollectableComponent(CollectableEntityField clctef, LabeledComponent labcomp, boolean bSearchable) {
		super(clctef, labcomp, bSearchable);
		final LabeledComponent lc = getLabeledComponent();
		final ILabeledComponentSupport support = lc.getLabeledComponentSupport();
		
		support.setToolTipTextProvider(this);
		support.setColorProvider(new BackgroundColorProvider());
	}

	public LabeledComponent getLabeledComponent() {
		return (LabeledComponent) getJComponent();
	}

	@Override
	public void setInsertable(boolean bInsertable) {
	}

	@Override
	public void setLabelText(String sLabel) {
		getLabeledComponent().setLabelText(sLabel);
	}

	@Override
	public void setMnemonic(char cMnemonic) {
		getLabeledComponent().setMnemonic(cMnemonic);
	}

	public JLabel getJLabel() {
		return getLabeledComponent().getJLabel();
	}

	@Override
	public JComponent getControlComponent() {
		return getLabeledComponent().getControlComponent();
	}

	@Override
	public void setFillControlHorizontally(boolean bFill) {
		getLabeledComponent().setFillControlHorizontally(bFill);
	}
	
	private static class LabeledTableCellRenderer implements TableCellRenderer {
		
		private TableCellRenderer parent;
		
		private final CollectableEntityField clctef;
		
		private LabeledTableCellRenderer(TableCellRenderer parent, CollectableEntityField clctef) {
			this.parent = parent;
			this.clctef = clctef;
		}
		
		@Override
		public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {
			
			Component comp = parent.getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
			
			if (comp instanceof JLabel) {
				setLabelText((JLabel) comp, oValue);
			}

			return comp;
		}
		
		private void setLabelText(JLabel lb, Object oValue) {
			String text = "";
			
			if (oValue instanceof CollectableField) {
				final CollectableField cf = (CollectableField) oValue;
				if (cf != null && cf.getValue() != null) {
					
					CollectableFieldFormat format = CollectableFieldFormat.getInstance(clctef.getJavaClass());
					StringBuilder sb = new StringBuilder();
					
					if (cf.getValue() instanceof List) {
						List<?> values = (List<?>) cf.getValue();
						for (Object o : values) {
							formatAndAppend(sb, o, format);
						}
						
					} else {
						formatAndAppend(sb, cf.getValue(), format);
					}
					
					text = sb.toString();
				} 
			}
			
			lb.setText(text);
		}
		
		private void formatAndAppend(StringBuilder sb, Object o, CollectableFieldFormat format) {
			if (o != null) {
				try {
					//@see NUCLOS-832
					String txt = format.format(clctef.getFormatOutput(), o);
					
					if (!StringUtils.isNullOrEmpty(txt) && txt.indexOf('\n') > -1) {
						txt = txt.replaceAll("\\n", " ");
					}
					
					if (sb.length() > 0) {
						sb.append(", ");
					}
					
					sb.append(txt);
					
				} catch (IllegalArgumentException e) {
					LOG.warn("Unable to format '" + o + "' with " + format, e);
				} catch (NuclosFatalException e) {
					LOG.warn("Unable to format '" + o + "' with " + format, e);								
				}
			}			
		}
	}

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		final TableCellRenderer parentRenderer = super.getTableCellRenderer(subform);
		return new LabeledTableCellRenderer(parentRenderer, getEntityField());
	}
	
}  // class LabeledCollectableComponent
