package org.nuclos.client.ui.collect.component;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.ui.CommonJTextField;
import org.nuclos.client.ui.FormatUtils;
import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledLocalizedTextField;
import org.nuclos.client.ui.message.MessageExchange;
import org.nuclos.client.ui.message.MessageExchange.MessageExchangeListener;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.Pair;

public class CollectableLocalizedTextField extends CollectableTextComponent
		implements MessageExchangeListener, CollectableLocalizedComponent<Long> {

	private static final Logger LOG = Logger.getLogger(CollectableLocalizedTextField.class);

	private CollectableEntityField clctef;

   public CollectableLocalizedTextField(CollectableEntityField clctef) {
      this(clctef, false);
      assert this.isDetailsComponent();
   }

   @Override
   protected void setEnabledState(boolean flag) {
	   super.setEnabledState(flag);
	   if (!flag) {
		   ((LabeledLocalizedTextField)getJComponent()).setButtonsDisabled();
	   }
   }
   
   public boolean isButtonDisabled() {
	   return ((LabeledLocalizedTextField)getJComponent()).isButtonsDisabled();
   }
   
   public CollectableLocalizedTextField(CollectableEntityField clctef, boolean bSearchable) {
      super(clctef, new LabeledLocalizedTextField(new LabeledComponentSupport(),clctef, bSearchable), bSearchable);
      
      ((LabeledLocalizedTextField)getJComponent()).setLocalizedComponent(this);
      this.clctef = clctef;
      FormatUtils.setupTextField(clctef, this.getJTextField(), bSearchable);
      MessageExchange.addListener(this);
      
      if(MetaProvider.getInstance().getEntityField(clctef.getUID()).getCalcFunction() != null)
    	  ((LabeledLocalizedTextField)getJComponent()).setButtonVisibility(false);
     
   }
   
   protected void updateView(CollectableField clctfValue) {
	   super.updateView(clctfValue);
	   ((LabeledLocalizedTextField)getJComponent()).setDataLanguageMapAsDirty(
			   getDetailsComponentModel().isDataLanguageMapAsDirty());
   }
   public CommonJTextField getJTextField() {
      return (CommonJTextField) this.getJTextComponent();
   }
   
   @Override
   public void setColumns(int iColumns) {
      this.getJTextField().setColumns(iColumns);
   }
   
   @Override
   public void setComparisonOperator(ComparisonOperator compop) {
      super.setComparisonOperator(compop);

      if (compop.getOperandCount() < 2) {
         this.runLocked(new Runnable() {
            @Override
            public void run() {
            	try {
            		getJTextComponent().setText(null);
				}
				catch (Exception e) {
					LOG.error("CollectableTextField.setComparisionOperator: " + e, e);
				}            		
            }
         });
      }
   }

	private static class CollectableTextFieldCellRenderer implements TableCellRenderer {
		
		private final TableCellRenderer parentRenderer;
		private final int horizontalAlignment;
		
		private CollectableTextFieldCellRenderer(TableCellRenderer parentRenderer, CommonJTextField ntf) {
			this.parentRenderer = parentRenderer;
			this.horizontalAlignment = ntf.getHorizontalAlignment();
		}

		@Override
		public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus,
				int iRow, int iColumn) {
			final Component comp = parentRenderer.getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow,
					iColumn);
			if (comp instanceof JLabel) {
				final JLabel lb = (JLabel) comp;
				lb.setHorizontalAlignment(horizontalAlignment);
			}
			return comp;
		}
	}

	
	 
	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		final TableCellRenderer parentRenderer = super.getTableCellRenderer(subform);
		final CommonJTextField ntf = getJTextField();
		return new CollectableTextFieldCellRenderer(parentRenderer, ntf);
	}

   @Override
   public void receive(Object id, ObjectType type, MessageType msg) {
      if(clctef != null && clctef.getCollectableEntity() != null) {
         Pair<UID, UID> idPair = new Pair<UID, UID>(clctef.getCollectableEntity().getUID(), clctef.getUID());
         if(idPair.equals(id))
            if (type == MessageExchangeListener.ObjectType.TEXTFIELD)
               if (msg == MessageExchangeListener.MessageType.REFRESH)
            	   if (isSearchComponent())
            		   FormatUtils.addAutoComplete(clctef, getJTextField(), FormatUtils.getAutoCompletePreferences(clctef));
      }
   }

	@Override
	public CollectableLocalizedComponentModel<Long> getDetailsComponentModel() {
		return (CollectableLocalizedComponentModel<Long>) getModel();
	}

	@Override
	public Integer getType() {
		return CollectableComponentTypes.TYPE_TEXTFIELD;
	}
	
	@Override
	protected boolean selectAllOnGainFocus() {
		return false; // NUCLOS-7216
	}
	
}
