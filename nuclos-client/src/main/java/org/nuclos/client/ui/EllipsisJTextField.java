package org.nuclos.client.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.*;

import org.jsoup.Jsoup;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common2.StringUtils;

/**
 * Created by Sebastian Debring on 7/19/2018.
 */
public class EllipsisJTextField extends CommonJTextField {

	private static final long serialVersionUID = 7846920972980061096L;

	private final JTextPane overlayTextPane;
	private final JDialog overlayDialog;
	private final boolean showEllipsisAndOverlay;
	private FontMetrics fm;
	private boolean bModify = true;
	private Point locationOnScreen = null;

	public EllipsisJTextField() {
		super();

		this.showEllipsisAndOverlay = ClientParameterProvider.getInstance()
				.isEnabled(ParameterProvider.SHOW_OVERLAY_FOR_TOO_SMALL_TEXTFIELDS, false);

		overlayDialog = new JDialog();
		overlayDialog.setUndecorated(true);
		overlayTextPane = new JTextPane();
		overlayTextPane.setContentType("text/html");
		final JScrollPane overlayScrollPane = new JScrollPane(overlayTextPane);
		overlayTextPane.setVisible(true);
		overlayTextPane.setEditable(true);
		overlayDialog.setLayout(new BorderLayout());
		overlayDialog.add(overlayScrollPane, BorderLayout.CENTER);
		overlayDialog.setFocusable(true);

		this.addPropertyChangeListener("locationOnScreen", (e -> {
			if (e.getOldValue() == null || !e.getOldValue().equals(e.getNewValue())) {
				setOverlayBounds();
			}
		}));

		Action overlayCloseAction = new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				closeOverlay(true);
			}
		};

		Action overlayCloseWithoutModificationAction = new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				closeOverlay(false);
			}
		};

		overlayTextPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "closeOverlayWithoutModification");
		overlayTextPane.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.CTRL_MASK), "closeOverlay");
		overlayTextPane.getActionMap().put("closeOverlay", overlayCloseAction);
		overlayTextPane.getActionMap().put("closeOverlayWithoutModification", overlayCloseWithoutModificationAction);

		this.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent e) {
				EllipsisJTextField.this.setCaretPosition(0);
			}
		});

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(final MouseEvent e) {
				if ((e.getModifiers() & InputEvent.BUTTON1_MASK) != 0 && e.isAltDown()) {
					showOverlay();
				}
			}
		});

		overlayTextPane.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(final FocusEvent e) {
				if (overlayDialog.isVisible()) {
					closeOverlay(isModifiable());
				}
			}
		});
	}

	protected boolean isModifiable() {
		return bModify;
	}

	private void showOverlay() {
		if (isTextTooLong() && showEllipsisAndOverlay) {
			SwingUtilities.invokeLater(() -> {
				overlayDialog.setVisible(true);
				overlayDialog.setSize(Math.max(getWidth(), 200), 200);
				setOverlayBounds();
				overlayTextPane.setEditable(isEditable() && isModifiable());
				overlayTextPane.setText(getText());
				overlayTextPane.setBorder(getBorder());
				overlayTextPane.requestFocusInWindow();
			});
		}
	}

	private void setOverlayBounds() {
		if (overlayDialog.isVisible()) {
			SwingUtilities.invokeLater(() -> {
				boolean fitsDownwards = true;
				boolean fitsRight = true;
				Point root = new Point(EllipsisJTextField.this.getLocationOnScreen());
				int widthOverlay = overlayDialog.getWidth();
				int heigthOverlay = overlayDialog.getHeight();
				int widthField = EllipsisJTextField.this.getWidth();
				int heightField = EllipsisJTextField.this.getHeight();

				Window mainWindow = UIUtils.getWindowForComponent(this);
				if (root.y + heigthOverlay > mainWindow.getLocationOnScreen().y + mainWindow.getHeight()) {
					fitsDownwards = false;
				}
				if (root.x + widthOverlay > mainWindow.getLocationOnScreen().x + mainWindow.getWidth()) {
					fitsRight = false;
				}

				if (!fitsDownwards) {
					root.y = root.y - heigthOverlay + heightField;
				}
				if (!fitsRight) {
					root.x = root.x - widthOverlay + widthField;
				}
				overlayDialog.setBounds(
						root.x,
						root.y,
						widthOverlay,
						heigthOverlay);
			});
		}
	}

	private void closeOverlay(final boolean modify) {
		SwingUtilities.invokeLater(() -> {
			bModify = modify;
			overlayDialog.setVisible(false);
			if (isModifiable() && (getText() != null && !getText().equals(Jsoup.parse(overlayTextPane.getText()).text()) || (getText() == null && overlayTextPane.getText() != null))) {
				setText(Jsoup.parse(overlayTextPane.getText()).text());
			}
			bModify = true;
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (isShowing()) {
			Point newLocationOnScreen = getLocationOnScreen();
			if (locationOnScreen == null || !locationOnScreen.equals(newLocationOnScreen)) {
				firePropertyChange("locationOnScreen", locationOnScreen, newLocationOnScreen);
				locationOnScreen = newLocationOnScreen;
			}
			drawEllipsisAtEndIfTextIsNotFullyVisible(g);
		}
	}

	private void drawEllipsisAtEndIfTextIsNotFullyVisible(Graphics g) {
		Insets insets = getInsets();
		boolean textIsToLong = isTextTooLong();
		boolean lastCharacterVisible = getStringWidthBeforeCaret() > getCachedFontMetrics().stringWidth(getText()) - getWidth();
		if (textIsToLong && !lastCharacterVisible && showEllipsisAndOverlay) {
			String ellipsis = "...";

			int x = (getWidth() - insets.right - getCachedFontMetrics().stringWidth(ellipsis));
			int y = getHeight() / 2 + getCachedFontMetrics().getAscent() / 2;

			g.setColor(getBackground());
			g.fillRect(x, insets.top, getWidth() - insets.right - x, getHeight() - insets.bottom - insets.top);
			g.setColor(getForeground());
			g.drawString(ellipsis, x, y);
		}
	}

	private boolean isTextTooLong() {
		Insets insets = getInsets();
		return getWidth() - insets.right < getCachedFontMetrics().stringWidth(getText());
	}

	private int getStringWidthBeforeCaret() {
		return getCachedFontMetrics().stringWidth(getText().substring(0, getCaretPosition()));
	}

	private FontMetrics getCachedFontMetrics() {
		if (fm == null) {
			fm = getFontMetrics(getFont());
		}
		return fm;
	}
}
