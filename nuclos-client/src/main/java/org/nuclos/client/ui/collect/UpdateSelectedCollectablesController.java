//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.CollectableGenericObjectAttributeField;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.CollectableMasterDataField;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

/**
 * Controller for updating multiple (selected) <code>Collectable</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public class UpdateSelectedCollectablesController<PK,Clct extends Collectable<PK>> extends MultiCollectablesActionController<PK,Clct, Object> {

	public static class UpdateAction<PK2,Clct2 extends Collectable<PK2>> implements Action<Clct2, Object> {
		
		private final Logger log = Logger.getLogger(this.getClass());

		private final CollectController<PK2,Clct2> ctl;

		private final Map<UID, CollectableField>	changedFields;

		protected UpdateAction(CollectController<PK2,Clct2> ctl) throws CommonBusinessException {
			this.ctl = ctl;
			this.changedFields = new HashMap<UID, CollectableField>();
			if (!ctl.getCollectState().isResultMode()) {
				for (CollectableComponent clctcomp : ctl.getEditView(false).getCollectableComponents()) {
					if (clctcomp.getDetailsModel().isValueToBeChanged()) {
						changedFields.put(clctcomp.getFieldUID(), clctcomp.getField());
					}
				}
			}
		}

		@Override
		public Object perform(Clct2 clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
			if (!changedFields.isEmpty()) {
				for (Map.Entry<UID, CollectableField> e : changedFields.entrySet()) {
					log.debug("Field to be changed: " + e.getKey());
					CollectableField clctf = e.getValue();
					if (clctf.getValue() instanceof GenericObjectDocumentFile) {
						NuclosFile fileForAllObjects = ((GenericObjectDocumentFile) clctf.getValue()).getNuclosFile();
						// set new UID (NUCLOS-7314)
						UID docUid = DocumentFileBase.newFileUID();
						GenericObjectDocumentFile docFile = new GenericObjectDocumentFile(
								fileForAllObjects.getName(), docUid, fileForAllObjects.getContent());
						if (clctf instanceof CollectableValueIdField) {
							clctf = new CollectableValueIdField(docUid, docFile);
						} else if (clctf instanceof CollectableGenericObjectAttributeField) {
							clctf = new CollectableGenericObjectAttributeField(
									new DynamicAttributeVO(e.getKey(), null, docUid, docFile),
									clctf.getFieldType());
						} else if (clct instanceof CollectableMasterDataField) {
							final CollectableMasterData<?> clctmd = ((CollectableMasterDataField) clctf).getCollectableMasterData();
							clctmd.setField(e.getKey(), new CollectableValueIdField(docUid, docFile));
							clctf = clctmd.getField(e.getKey());
						}
					}

					clct.setField(e.getKey(), clctf);
				}
			}

			if (!ctl.getCollectState().isResultMode()) {
				ctl.getResultController().replaceCollectableInTableModel(ctl.updateCollectable(clct, ctl.getAdditionalDataForMultiUpdate(clct), applyMultiEditContext, true));
			}
			return null;
		}

		@Override
		public String getText(Clct2 clct) {
			return SpringLocaleDelegate.getInstance().getMessage(
					"UpdateSelectedCollectablesController.1", "Datensatz {0} wird ge\u00e4ndert...", 
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance()));
		}

		@Override
		public String getSuccessfulMessage(Clct2 clct, Object oResult) {
			return SpringLocaleDelegate.getInstance().getMessage(
					"UpdateSelectedCollectablesController.2", "Datensatz {0} erfolgreich ge\u00e4ndert.", 
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance()));
		}

		@Override
		public String getConfirmStopMessage() {
			return SpringLocaleDelegate.getInstance().getMessage(
					"UpdateSelectedCollectablesController.3", "Wollen Sie das \u00c4ndern der Datens\u00e4tze an dieser Stelle beenden?\n(Die bisher ge\u00e4nderten Datens\u00e4tze bleiben in jedem Fall ge\u00e4ndert.)");
		}

		@Override
		public String getExceptionMessage(Clct2 clct, Exception ex) {
			return SpringLocaleDelegate.getInstance().getMessage(
					"UpdateSelectedCollectablesController.4", "Datensatz {0} konnte nicht ge\u00e4ndert werden.", 
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, ctl.getEntityUid(), MetaProvider.getInstance())) + ex.getMessage();
		}

		@Override
		public void executeFinalAction() throws CommonBusinessException {
			// jump to multi view mode:
			ctl.setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_MULTIVIEW);
		}
	}

	public UpdateSelectedCollectablesController(CollectController<PK,Clct> ctl) throws CommonBusinessException {
		super(ctl, SpringLocaleDelegate.getInstance().getMessage(
				"UpdateSelectedCollectablesController.5", "Datens\u00e4tze \u00e4ndern"), new UpdateAction<PK,Clct>(ctl), ctl.getCompleteSelectedCollectables());
	}

}  // class UpdateSelectedCollectablesController
