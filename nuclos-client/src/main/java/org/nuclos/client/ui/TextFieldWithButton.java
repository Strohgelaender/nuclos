//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.client.ui.labeled.ILabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledComboBox;
import org.nuclos.common.collection.CollectionUtils;

/**
 * 
 * TextField with inner button like a ComboBox
 *
 */
public abstract class TextFieldWithButton extends EllipsisJTextField {
	
	private final List<Icon> lstIconButtons;
	
	protected static final Cursor curIcon = new Cursor(Cursor.DEFAULT_CURSOR);
	protected static final Cursor curNotEditable = curIcon;
	protected final Cursor curDefault;
	
	private final ILabeledComponentSupport support;
	
	private final Map<Icon, ButtonState> mpBS;
	
	private final int fadeWidthLeft = 7;
	
	public TextFieldWithButton(Icon iconButton, ILabeledComponentSupport support) {
		this(CollectionUtils.newOneElementArrayList(iconButton), support);
	}

	protected TextFieldWithButton(List<Icon> paramLstIconButtons, ILabeledComponentSupport support) {
		if (paramLstIconButtons == null) {
			throw new IllegalArgumentException("lstIconButtons must not be null!");
		}
		if (support == null) {
			throw new IllegalArgumentException();
		}
		
		this.mpBS = new HashMap<Icon, TextFieldWithButton.ButtonState>();
		for (Icon icon : paramLstIconButtons) {
			this.mpBS.put(icon, ButtonState.NORMAL);
			
		}
		
		this.support = support;
		this.lstIconButtons = paramLstIconButtons;
		this.curDefault = getCursor();
		this.setPreferredSize(new Dimension(this.getPreferredSize().width, LabeledComboBox.DEFAULT_PREFERRED_SIZE.height));
		this.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent me) {
				boolean bCurIconSet = false;
				
				for (Icon icon : mpBS.keySet()) {
					int index = lstIconButtons.indexOf(icon);
					
					if (isButtonEnabled(index) && isMouseOverButton(me, index)) {
						TextFieldWithButton.this.setCursor(curIcon);
						bCurIconSet = true;
						
						if (mpBS.get(icon) != ButtonState.PRESSED) {
							mpBS.put(icon, ButtonState.HOVER);
							TextFieldWithButton.this.repaint(getIconRectangleWithFades(index));
							
						}
					} else {
						mpBS.put(icon, ButtonState.NORMAL);
						TextFieldWithButton.this.repaint(getIconRectangleWithFades(index));
						
					}
				}
				if (!bCurIconSet) {
					TextFieldWithButton.this.setCursor(getDefaultCursor(me));

				}
			}
		});
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent me) {
				for (Icon icon : mpBS.keySet()) {
					int index = lstIconButtons.indexOf(icon);
					
					mpBS.put(icon, ButtonState.NORMAL);
					TextFieldWithButton.this.repaint(getIconRectangleWithFades(index));
					
				}
			}
			@Override
			public void mouseReleased(MouseEvent me) {
				for (Icon icon : mpBS.keySet()) {
					int index = lstIconButtons.indexOf(icon);
					
					if (isButtonEnabled(index) && isMouseOverButton(me, index)) {
						mpBS.put(icon, ButtonState.NORMAL);
						TextFieldWithButton.this.repaint(getIconRectangleWithFades(index));
						
					}
				}
			}
			@Override
			public void mousePressed(MouseEvent me) {
				boolean bTextClicked = true;
				for (Icon icon : mpBS.keySet()) {
					int index = lstIconButtons.indexOf(icon);
					
					if (isButtonEnabled(index) && isMouseOverButton(me, index)) {
						bTextClicked = false;
						mpBS.put(icon, ButtonState.NORMAL);
						TextFieldWithButton.this.repaint(getIconRectangleWithFades(index));
						buttonClicked(me, index);
						
					}
				}
				
				if (bTextClicked) {
					textClicked(me);
				}
			}
		});
		this.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				TextFieldWithButton.this.setSelectionStart(0);
				TextFieldWithButton.this.setSelectionEnd(0);
			}
			@Override
			public void focusGained(FocusEvent e) {
			}
		});
	}
	
	public abstract boolean isButtonEnabled(int index);
	
	public abstract void buttonClicked(MouseEvent me, int index);
	
	public void textClicked(MouseEvent me) {
		
	}
	
	protected Cursor getDefaultCursor(MouseEvent me) {
		return TextFieldWithButton.this.isEditable() ? curDefault : curNotEditable;
	}
	
	@Override
	public Color getBackground() {
		final Color colorDefault = super.getBackground();
		if (support == null) {
			return colorDefault;
		}
		final ColorProvider colorproviderBackground = support.getColorProvider();
		if (colorproviderBackground == null) {
			return colorDefault;			
		}
		return colorproviderBackground.getColor(colorDefault);
	}
	
	private boolean isMouseOverButton(MouseEvent me, int index) {
		if (isSelectingText()) {
			return false; // hide button when selecting text...			
		}
		
		final Rectangle r = getIconRectangle(index);
		if (r.x <= me.getX() && me.getX() <= (r.x+r.width)) {
			return true;
		} else {
			return false;
		}
	}
	
	private Rectangle getIconRectangleWithFades(int index) {
		final Rectangle r = getIconRectangle(index );
		
		if (fadeLeft()) {
			r.x = r.x - fadeWidthLeft;
			r.width = r.width + fadeWidthLeft;
		}
		
		return r;
	}
	
	private Rectangle getIconRectangle(int index) {
		return getIconRectangle(TextFieldWithButton.this.getSize(), index);
	}
	
	public static Rectangle getIconRectangle(Dimension dimTextField, int index) {
		final Rectangle r = new Rectangle();
		
		final ImageIcon ico = ButtonState.NORMAL.getImageIcon();
		r.x = dimTextField.width - ico.getIconWidth() - 2;
		r.y = (dimTextField.height - ico.getIconHeight()) / 2;
		r.width = ico.getIconWidth();
		r.height = ico.getIconHeight();
		
		r.x -= index*r.width;
		
		return r;
	}
	
	protected boolean fadeLeft() {
		return true;
	}
	
	private boolean isSelectingText() {
		return getSelectionStart() != getSelectionEnd();
	}
	
	public List<Icon> getButtonIcons() {
		return lstIconButtons;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;
		
		for (Icon iconButton : lstIconButtons) {
			final int index = lstIconButtons.indexOf(iconButton);
			final Rectangle r = getIconRectangle(index);
			
			if (!mpBS.containsKey(iconButton)) {
				this.mpBS.put(iconButton, ButtonState.NORMAL);
			}
			
			
			final Color bgColor;
			if (!isOpaque() && !isEditable() && Color.WHITE.equals(getBackground())) {
				bgColor = NuclosThemeSettings.BACKGROUND_PANEL;
			} else {
				bgColor = getBackground();
			}
			if (fadeLeft()) {
				g2d.setPaint(new GradientPaint(new Point(r.x-fadeWidthLeft, r.y), new Color(bgColor.getRed(), bgColor.getGreen(), bgColor.getBlue(), 0), new Point(r.x-1, r.y), bgColor));
				g2d.fillRect(r.x-fadeWidthLeft, r.y, fadeWidthLeft, r.height);
			}
			g2d.setColor(bgColor);
			g2d.fillRect(r.x, r.y, r.width, r.height);
		
			g2d.drawImage(mpBS.get(iconButton).getImageIcon().getImage(), r.x, r.y, null);
		
			int w = iconButton.getIconWidth();
			int h = iconButton.getIconHeight();
			BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			Graphics imgg = bi.getGraphics();
			iconButton.paintIcon(this, imgg, 0, 0);
			imgg.dispose();

			float[] scales = { 1f, 1f, 1f, isButtonEnabled(index) ? 1f : 0.5f };
			float[] offsets = new float[4];
			RescaleOp rop = new RescaleOp(scales, offsets, null);
			g2d.drawImage(bi, rop, r.x, r.y);
			
		}
		
	}

	@Override
	protected boolean isModifiable() {
		return false;
	}

	private static enum ButtonState {
		
		NORMAL(Icons.getInstance().getIconTextFieldButton()),
		HOVER(Icons.getInstance().getIconTextFieldButtonHover()),
		PRESSED(Icons.getInstance().getIconTextFieldButtonPressed());
		
		ImageIcon ii;
		
		ButtonState(ImageIcon ii) {
			this.ii = ii;
		}
		
		ImageIcon getImageIcon() {
			return ii;
		}
	}	
}