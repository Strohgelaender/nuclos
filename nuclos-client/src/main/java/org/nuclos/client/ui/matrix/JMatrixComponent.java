//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.matrix;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.log4j.Logger;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MatrixController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.WorkspaceUtils;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.scripting.ScriptEvaluator;
import org.nuclos.client.scripting.context.MatrixControllerScriptContext;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.NuclosToolBar;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.VerticalTableCellRenderer;
import org.nuclos.client.ui.collect.SelectGroupsController;
import org.nuclos.client.ui.collect.subform.ClearAction;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.collect.subform.TransferLookedUpValueAction;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.client.ui.event.IPopupListener;
import org.nuclos.client.ui.event.Mouse2TableHeaderPopupMenuAdapter;
import org.nuclos.client.ui.event.TableColumnModelAdapter;
import org.nuclos.client.ui.event.TablePopupMenuEvent;
import org.nuclos.client.ui.event.TablePopupMenuMouseAdapter;
import org.nuclos.client.ui.model.ChoiceList;
import org.nuclos.common.Actions;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.SF;
import org.nuclos.common.StringComparator;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescription2.MatrixPreferences;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.security.IPermission;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import info.clearthought.layout.TableLayout;

public class JMatrixComponent extends JComponent {
	
	private static final Logger LOG = Logger.getLogger(JMatrixComponent.class);
	
	private static final int HEADER_HEIGHT = 17;
	
	private final Map<UID, Column> mpColumns = new LinkedHashMap<UID, Column>();
	
	Map<Collectable, Map<Collectable, List<Object>>> mpSelected = new HashMap<Collectable, Map<Collectable,List<Object>>>();
	Map<Collectable, Map<Collectable, List<Object>>> mpAvailable = new HashMap<Collectable, Map<Collectable,List<Object>>>();	
	SortedSet<String> setAvailable = new TreeSet<String>();
	List<String> lstSelected = new ArrayList<String>();
	
	Map<UID, Integer> mpFixedFieldWidths = new HashMap<UID, Integer>();
	
	Map<Collectable, Map<Collectable, List<Object>>> mpSelectedFixed = new HashMap<Collectable, Map<Collectable,List<Object>>>();
	Map<Collectable, Map<Collectable, List<Object>>> mpAvailableFixed = new HashMap<Collectable, Map<Collectable,List<Object>>>();	
	SortedSet<UID> setAvailableFixed = new TreeSet<UID>();
	List<UID> lstSelectedFixed = new ArrayList<UID>();
	
	Map<String, List<String>> mpSelectedGroupElements = new HashMap<String, List<String>>();
	Map<String, SortedSet<String>> mpAvailableGroupElements = new HashMap<String, SortedSet<String>>();
	
	JScrollPane scrollPane;
	JTable table;	
	JTableHeader header;	
	JTableHeader headerFixed;
	JScrollPane scrollHeader;
	NuclosToolBar toolbar;	
	String sControllerType;
	NuclosScript editEnabledScript;
	NuclosScript newEnabledScript;
	NuclosScript cloneEnabledScript;
	NuclosScript deleteEnabledScript;
	Integer iNumberState;
	String cellInputType;

	String entityMatrixReferenceField;

	String entityXVlpId;
	String entityXVlpIdFieldName;
	String entityXVlpFieldName;
	String entityXVlpReferenceParamName;
	
	String entityXHeader;
	String entityYHeader;

	String entityXSortingFields;
	String entityYSortingFields;

	boolean isEditable = true;

	JScrollPane scrFixed;
	JTable tblFixed;
	
	UID entityFieldGrouped;
	UID sMatrixEntity;
	UID sMatrixEntityParentField;
	UID sPreferencesField;
	
	UID sEntityX;
	UID sEntityY;
	
	UID sFieldCategorie;
	UID sFieldX;
	UID sFieldY;
	UID sEntityYParentField;
	UID sEntityXRefField;
	UID sEntityMatrixValueField;
	
	Map<String, Pair<Integer, Integer>> mpCategorie;	
	
	Collection<JComponent> colCategoryHeader;
	
	IPopupListener popupMenuListener;
	
	private List<ChangeListener> lstchangelistener = new LinkedList<ChangeListener>();
	
	MatrixTableModel model;
	MatrixTableModel modelFixed;
	
	boolean bLayout = false;
	boolean bCloneEnabled = true;
	boolean bNewEnabled = true;
	boolean bDeleteEnabled = true;
	
	AbstractAction actNew;
	AbstractAction actClone;
	AbstractAction actRemove;
	
	MatrixPreferences matrixPreferences;
	
	IPermission permissionYEntity;
	IPermission permissionMatrixEntity;
	
	MatrixController matrixController;
	
	boolean bFireListeners = true;
	
	boolean setLocation = false;
	
	public UID getEntityFieldGrouped() {
		return entityFieldGrouped;
	}

	public void setEntityFieldGrouped(UID entityFieldGrouped) {
		this.entityFieldGrouped = entityFieldGrouped;
	}

	public JMatrixComponent() {
		init();
	}
	
	public JMatrixComponent(boolean bLayout) {
		this.bLayout = bLayout;
		init();
	}
	
	public JMatrixComponent(boolean bLayout, UID yEntityUid) {
		this.bLayout = bLayout;
		this.sEntityY = yEntityUid;
		init();
	}
	
	private int getColumnWidth() {
		int columnWidth = cellInputType == null || LayoutMLConstants.ATTRIBUTE_CELL_INPUT_TYPE_CHECKICON.equals(cellInputType) ? 25 : 50;
		return columnWidth;
	}

	
	public void setController(MatrixController controller) {
		this.matrixController = controller;
	}
	
	public void setTableModel(MatrixTableModel model, MatrixTableModel modelFixed) {
		this.model = model;
		this.modelFixed = modelFixed;
		if(editEnabledScript != null) {
			model.setEditEnabledScript(editEnabledScript);
			modelFixed.setEditEnabledScript(editEnabledScript);
		}
		init();
	}
	
	protected void setupFixedTable() {
		tblFixed = new JTable(modelFixed);
		tblFixed.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		tblFixed.getTableHeader().setReorderingAllowed(false);
		scrFixed = new JScrollPane(tblFixed);
		
		tblFixed.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		tblFixed.setRowHeight(18);
		tblFixed.setRowMargin(2);
		
		tblFixed.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblFixed.setRowSelectionAllowed(true);
		
		tblFixed.addMouseListener(new MatrixPopupMenuMouseAdapter(tblFixed));
		
		Enumeration<TableColumn> en = tblFixed.getColumnModel().getColumns();
		
		while(en.hasMoreElements()) {
			TableColumn tc = en.nextElement();
			VerticalTableCellRenderer vren = new VerticalTableCellRenderer();		
			vren.setBorderLocation(VerticalTableCellRenderer.BORDER_NONE);
			tc.setHeaderRenderer(vren);			


			FieldMeta entityMatrixValueFieldFieldMeta = MetaProvider.getInstance().getEntityField(getEntityMatrixValueField());
			MatrixSelectCellEditor editor = new MatrixSelectCellEditor(getCellInputType(), null, getNumberState());
			editor.setEditable(isEditable);
			editor.setColumns(mpColumns);
			editor.setChangeListener(lstchangelistener);
			editor.addCellEditorListener(new MatrixCellEditorListener(model, tblFixed));
			tc.setCellEditor(editor);
			MatrixSelectCellRenderer ren = new MatrixSelectCellRenderer(getCellInputType(), entityMatrixValueFieldFieldMeta != null ? entityMatrixValueFieldFieldMeta.getFormatOutput() : null);
			tc.setCellRenderer(ren);
		}
		
		// rowindicator
		TableColumn tcFirst = tblFixed.getColumnModel().getColumn(0);		
		tcFirst.setCellRenderer(new RowIndicatorCellRenderer());
		tcFirst.setResizable(false);
		tcFirst.setWidth(20);
		tcFirst.setPreferredWidth(20);
		
	}
	
	public class RowIndicatorCellRenderer extends DefaultTableCellRenderer {

		private ImageIcon icon = (ImageIcon) Icons.getInstance().getIconRowSelection16();
		private JPanel panel = new JPanel();
		private JLabel bgLabel = new JLabel();

		public RowIndicatorCellRenderer() {
			bgLabel.setBackground(Color.LIGHT_GRAY);
			panel.setLayout(new BorderLayout());
			panel.add(bgLabel, BorderLayout.CENTER);
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			if (isSelected) {
				bgLabel.setIcon(icon);
			}
			else {
				bgLabel.setIcon(null);
			}
			return panel;
		}
	}
	
	protected void runScripting() {
		if(!bLayout) {					
			setNewEnabled(new MatrixControllerScriptContext(matrixController, getSelectedCollectable(), null, getEntityY(), getFieldY(), 0));
			setCloneEnabled(new MatrixControllerScriptContext(matrixController, getSelectedCollectable(), null, getEntityY(), getFieldY(), 0));
			setDeleteEnabled(new MatrixControllerScriptContext(matrixController, getSelectedCollectable(), null, getEntityY(), getFieldY(), 0));
		}
	}
	
	protected void init() {		
		this.removeAll();
		this.setLayout(new BorderLayout());
		if(!bLayout && model == null)
			return;
		
		if(bLayout) {
			model = wysiwygModel();
			modelFixed = wysiwygModelFixed();
		}
		
		table = new JTable(model);
		table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.setRowSelectionAllowed(true);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		table.setRowHeight(18);
		table.setRowMargin(2);
				
		model.addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent e) {
				if(bFireListeners)
					fireChangeListener();				
			}
		});
		
		table.addMouseListener(new MatrixPopupMenuMouseAdapter(table));
		
		Enumeration<TableColumn> en = table.getColumnModel().getColumns();
		while(en.hasMoreElements()) {
			TableColumn tc = en.nextElement();
			VerticalTableCellRenderer vren = new VerticalTableCellRenderer();		
			vren.setBorderLocation(VerticalTableCellRenderer.BORDER_NONE);
			tc.setHeaderRenderer(vren);			
			
			FieldMeta entityMatrixValueFieldFieldMeta = MetaProvider.getInstance().getEntityField(getEntityMatrixValueField());
			if(entityMatrixValueFieldFieldMeta != null) {
				String matrixCellValueDataType = entityMatrixValueFieldFieldMeta.getDataType();
				MatrixSelectCellEditor editor = null; 
				if(cellInputType == null || LayoutMLConstants.ATTRIBUTE_CELL_INPUT_TYPE_CHECKICON.equals(cellInputType)) {
					editor = new MatrixSelectCellEditor(LayoutMLConstants.ATTRIBUTE_CELL_INPUT_TYPE_CHECKICON, matrixCellValueDataType, getNumberState());
					editor.setEditable(isEditable);
				} else {
					editor = new MatrixSelectCellEditor(getCellInputType(), matrixCellValueDataType, getNumberState());
					editor.setEditable(isEditable);
				}
				editor.setChangeListener(lstchangelistener);			
				editor.addCellEditorListener(new MatrixCellEditorListener(model, table));
				tc.setCellEditor(editor);
			}
			
			MatrixSelectCellRenderer ren = new MatrixSelectCellRenderer(getCellInputType(), entityMatrixValueFieldFieldMeta != null ? entityMatrixValueFieldFieldMeta.getFormatOutput() : null);
			tc.setCellRenderer(ren);
		}		
		
		scrollPane = new JScrollPane(table);

		scrollPane.getHorizontalScrollBar().addAdjustmentListener((AdjustmentEvent e) -> {
			setLocation = true;
			JViewport port = scrollHeader.getViewport();
			int xValue = e.getAdjustable().getValue();
			Point point = new Point(xValue, scrollHeader.getLocation().y);
			port.setViewPosition(point);
			setLocation = false;
		});

		setupFixedTable();
		scrollPane.setRowHeaderView(tblFixed);
		scrollPane.getRowHeader().setBackground(tblFixed.getBackground());		
		scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, tblFixed.getTableHeader());
		scrollPane.getRowHeader().setPreferredSize(tblFixed.getPreferredSize());

		this.add(scrollPane, BorderLayout.CENTER);
		
		toolbar = UIUtils.createNonFloatableToolBar(JToolBar.VERTICAL);
		
		actNew = new AbstractAction(ToolbarFunction.NEW.name(), Icons.getInstance().getIconNew16()) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				MatrixTableModel model = (MatrixTableModel) table.getModel();
				model.insertNewRow();
				
				MatrixTableModel modelFixed = (MatrixTableModel) tblFixed.getModel();
				modelFixed.insertNewRow();
				
			}
			
		};
		
		JButton btNew = new JButton(actNew);
		btNew.setHideActionText(true);
		btNew.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("SubForm.7","Neuen Datensatz anlegen"));
		
		toolbar.add(btNew);
				
		actClone = new AbstractAction(ToolbarFunction.CLONE.name(), Icons.getInstance().getIconClone16()) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int rows[] = tblFixed.getSelectedRows();
				if(rows.length == 0) {
					return;
				}
				for(int i = 0; i < rows.length; i++) {
					int selectedRow = rows[i];
					if(selectedRow == -1) {
						continue;
					}
										
					MatrixTableModel model = (MatrixTableModel) table.getModel();
					model.cloneRow(selectedRow);
					
					MatrixTableModel modelFixed = (MatrixTableModel) tblFixed.getModel();
					modelFixed.cloneRow(selectedRow);
				}
			}
		};		
		actClone.setEnabled(false);
		
		JButton btClone = new JButton(actClone);
		btClone.setHideActionText(true);
		btClone.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("MasterDataSubFormController.1","Datensatz klonen"));
		
		toolbar.add(btClone);
		
		actRemove = new AbstractAction(ToolbarFunction.REMOVE.name(), Icons.getInstance().getIconDelete16()) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();				
				if(selectedRow < 0)
					selectedRow = tblFixed.getSelectedRow();
				if(selectedRow < 0) {
					return;
				}
				stopEditing();
				MatrixTableModel modelfixed = (MatrixTableModel) tblFixed.getModel();
				MatrixTableModel model = (MatrixTableModel) table.getModel();
				modelfixed.removeRow(selectedRow, model);				
				model.removeRow(selectedRow, null);					
			}
		};
		
		JButton btRemove = new JButton(actRemove);
		btRemove.setHideActionText(true);
		btRemove.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("SubForm.1","Ausgew\u00e4hlten Datensatz l\u00f6schen"));
		
		actRemove.setEnabled(false);
		toolbar.add(btRemove);
		
		double col[] = {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED};
		double row[] = {TableLayout.FILL};
		JPanel pnlNorth = new JPanel(new TableLayout(col, row));
		pnlNorth.setPreferredSize(new Dimension(500, HEADER_HEIGHT));
	
		header = createCategoryHeader();
		
		scrollHeader = new JScrollPane(header, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollHeader.setPreferredSize(new Dimension(1500, HEADER_HEIGHT));
		scrollHeader.setBorder(BorderFactory.createEmptyBorder());

		header.setPreferredSize(new Dimension(500, HEADER_HEIGHT));
		headerFixed = createCategoryHeaderFixed();
		headerFixed.setPreferredSize(new Dimension(500, HEADER_HEIGHT));
		
		JTable dummyTable = new JTable();
		dummyTable.setTableHeader(header);
		
		JTable dummyTableFixed = new JTable();
		dummyTableFixed.setTableHeader(headerFixed);
		
		pnlNorth.add(headerFixed, "1,0");		
		pnlNorth.add(scrollHeader, "2,0");
				
		JPanel pnlWest = new JPanel(new BorderLayout());
		pnlWest.setBackground(header.getBackground());
		pnlWest.setForeground(header.getForeground());
		pnlWest.setPreferredSize(new Dimension(25, HEADER_HEIGHT));
		JLabel lbIcon = new JLabel(Icons.getInstance().getIconSelectVisibleColumns16());
		lbIcon.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				final SelectGroupsController ctl = new SelectGroupsController(JMatrixComponent.this);
				cmdSelectGroups(ctl);
			}
			
		});
		
		pnlWest.add(lbIcon, BorderLayout.CENTER);
		pnlNorth.add(pnlWest, "0,0");
		
		this.add(pnlNorth, BorderLayout.NORTH);		

		this.add(toolbar, BorderLayout.WEST);
		
		prepareTable(table);		
	
		table.getTableHeader().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(final MouseEvent e) {
				
			if(SwingUtilities.isRightMouseButton(e) && e.getClickCount() == 1) {
				JPopupMenu popup = new JPopupMenu();

				JTableHeader head = (JTableHeader)e.getSource();
				final Point point = e.getPoint();
				final int colIndex = head.columnAtPoint(point);

				final SelectGroupsController ctl = new SelectGroupsController(JMatrixComponent.this);
				JMenuItem miSelectColumns = new JMenuItem(LocaleDelegate.getInstance().getResource("ResultPanel.11"));
				miSelectColumns.addActionListener((ActionEvent ev) -> cmdSelectColumns(colIndex, ctl));
				popup.add(miSelectColumns);

				popup.show(table.getTableHeader(), e.getX(), e.getY());
			}
			}						
			
		});

		addListSelectionListener(table, tblFixed);
		addListSelectionListener(tblFixed, table);

		toolbar.addMouseListener(new MatrixPopupMenuMouseAdapter(tblFixed));
		
		model.fireTableDataChanged();
		
		runScripting();
		respectRights();

		SwingUtilities.invokeLater(() -> updateFixedTableHeaderDimensions());
	}

	private void addListSelectionListener(JTable parTable, JTable otherTable) {
		parTable.getSelectionModel().addListSelectionListener((ListSelectionEvent e) -> {
			int index = parTable.getSelectionModel().getMinSelectionIndex();
			int last = parTable.getSelectionModel().getMaxSelectionIndex();
			boolean anySelection = index > -1 && last > -1;

			if (e.getValueIsAdjusting()) {
				if (!anySelection) {
					return;
				}
				int selectedMin = otherTable.getSelectionModel().getMinSelectionIndex();
				int selectedMax = otherTable.getSelectionModel().getMaxSelectionIndex();

				if (selectedMin != index || selectedMax != last) {
					List<Integer> groupRow = modelFixed.getRowsOfSameGroupForColumn(index, 1);
					if (groupRow != null && groupRow.size() >= 2) {
						int ind1 = groupRow.get(0);
						int ind2 = groupRow.get(groupRow.size() - 1);

						// select all members of group if they have not been selected yet.
						if (ind1 != selectedMin || ind2 != selectedMax) {

							tblFixed.getSelectionModel().setSelectionInterval(ind1, ind2);
							table.getSelectionModel().setSelectionInterval(ind1, ind2);
						}
					}
				}

			} else {
				actClone.setEnabled(bCloneEnabled && anySelection);
				actRemove.setEnabled(bDeleteEnabled && anySelection);

				if (!anySelection) {
					return;
				}
				if (index != last) {
					otherTable.getSelectionModel().addSelectionInterval(index, index);
				}
				else {
					otherTable.getSelectionModel().setSelectionInterval(index, index);
				}
			}
		});
	}

	public void setNewEnabled(ScriptContext sc) {
		bNewEnabled = setEnabledScript(newEnabledScript, sc);;
		actNew.setEnabled(bNewEnabled);
	}

	public void setCloneEnabled(ScriptContext sc) {
		bCloneEnabled = setEnabledScript(cloneEnabledScript, sc);
		actClone.setEnabled(bCloneEnabled);
	}

	public void setDeleteEnabled(ScriptContext sc) {
		bDeleteEnabled = setEnabledScript(deleteEnabledScript, sc);;
		actRemove.setEnabled(bDeleteEnabled);
	}

	private boolean setEnabledScript(NuclosScript script, ScriptContext sc) {
		boolean enabled = true;
		if (script != null) {
			Object o = null;
			try {
				o = ScriptEvaluator.getInstance().eval(script, sc);
			} catch (InvocationTargetException e) {
				LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
				o = enabled;
			} catch (Exception e) {
				LOG.warn("Failed to evaluate script expression: " + e, e);
				o = enabled;
			}
			if (o instanceof Boolean) {
				enabled = (Boolean) o;
			}
		}
		return enabled;
	}

	/**
	 * sets the controller type for this subform. This is used to create an appropriate controller for this subform.
	 * @param sControllerType
	 */
	public void setControllerType(String sControllerType) {
		this.sControllerType = sControllerType;
	}

	private void prepareTable(JTable tab) {
		
		mpCategorie = new HashMap<String, Pair<Integer,Integer>>();
				
		tab.getTableHeader().setReorderingAllowed(false);
		
		TableColumnModel colModel = tab.getColumnModel();
		int colCount = colModel.getColumnCount();
		
		for(int h = 0; h < colCount; h++) {			
			TableColumn tc = colModel.getColumn(h);
			tc.setPreferredWidth(getColumnWidth());
			tc.setWidth(getColumnWidth());
			tc.setResizable(false);			
		}
		
		Map<Collectable, Map<Collectable, List<Object>>> mpContent = model.getContent();
		Map<Collectable, Map<Collectable, List<Object>>> mpContentFixed = modelFixed.getContent();
		
		int headerCount = 0; 
		int colIndexCount = 0;
		int preferredSize = 0;
		for(Collectable clCategorie : mpContentFixed.keySet()) {
			int width = tblFixed.getColumnModel().getColumn(headerCount).getPreferredWidth();
			TableColumn tcHeader = headerFixed.getColumnModel().getColumn(headerCount++);
			tcHeader.setPreferredWidth(width);
			tcHeader.setWidth(width);
			preferredSize += width;
		}
		headerFixed.setPreferredSize(new Dimension(preferredSize, HEADER_HEIGHT));
		
		headerCount = 0; 
		colIndexCount = 0;
		preferredSize = 0;
				
		for(Collectable clCategorie : mpContent.keySet()) {
			String sCat = (String) clCategorie.getValue(UID.UID_NULL);			
			int count = mpContent.get(clCategorie).size() - 1;			
			mpCategorie.put(sCat, new Pair<Integer, Integer>(colIndexCount, colIndexCount +	count));
			colIndexCount = colIndexCount + count+1;
			int headersize = mpContent.get(clCategorie).size() * getColumnWidth();
			TableColumn tcHeader = header.getColumnModel().getColumn(headerCount++);
			tcHeader.setWidth(headersize);
			tcHeader.setPreferredWidth(headersize);
			preferredSize += headersize;
		}
		int w = this.getWidth();
		header.setPreferredSize(new Dimension(preferredSize + w, HEADER_HEIGHT));
		
		for(int i = 0; i < modelFixed.getColumnCount(); i++) {
			VerticalTableCellRenderer vRen = (VerticalTableCellRenderer) tblFixed.getTableHeader().getColumnModel().getColumn(i).getHeaderRenderer();
			vRen.setBorderLocation(VerticalTableCellRenderer.BORDER_LEFT);
		}		
		
		for(String key : mpCategorie.keySet()) {
			Pair<Integer, Integer> pair = mpCategorie.get(key);
			VerticalTableCellRenderer vRen = (VerticalTableCellRenderer) tab.getTableHeader().getColumnModel().getColumn(pair.x).getHeaderRenderer();
			vRen.setBorderLocation(VerticalTableCellRenderer.BORDER_LEFT);
		}
		
		tab.repaint();
		
	}
	
	public void stopEditing() {
		if(table != null && table.getCellEditor() != null)
			table.getCellEditor().stopCellEditing();
		if(tblFixed != null && tblFixed.getCellEditor() != null) {
			tblFixed.getCellEditor().stopCellEditing();
		}
	}
	
	/**
	 * this is only for test case
	 * remove method
	 * @return
	 */
	protected MatrixTableModel wysiwygModel() {
		MatrixTableModel mod = new MatrixTableModel(null);
		
		Map<Collectable, Map<Collectable, List<Object>>> mpContent = new HashMap<Collectable, Map<Collectable, List<Object>>>();
		mpContent = ListOrderedMap.decorate(mpContent);
		
		Collectable cat0 = new MatrixCollectable("Gruppe 1");
		Map<Collectable, List<Object>> mpCat0 = new HashMap<Collectable, List<Object>>();
		Collectable group1 = new MatrixCollectable("");
		List<Object> mpData1 = new ArrayList<Object>();
			
		Collectable cat1 = new MatrixCollectable("Gruppe 1");
		Map<Collectable, List<Object>> mpCat1 = new HashMap<Collectable, List<Object>>();
		mpCat1 = ListOrderedMap.decorate(mpCat1);
		
		Collectable group2 = new MatrixCollectable(" Ausprägung 1");	
		List<Object> mpData2 = new ArrayList<Object>();		
		mpCat1.put(group2, mpData2);

		Collectable group3 = new MatrixCollectable(" Ausprägung 2");	
		Collectable group314 = new MatrixCollectable(" Ausprägung 3");
		Collectable group315 = new MatrixCollectable(" Ausprägung 4");

		List<Object> mpData3 = new ArrayList<Object>();

		mpCat1.put(group3, mpData3);
		mpCat1.put(group314, mpData3);
		mpCat1.put(group315, mpData3);

		
		Collectable cat2 = new MatrixCollectable("Gruppe 2");
		Map<Collectable, List<Object>> mpCat2 = new HashMap<Collectable, List<Object>>();
		mpCat2 = ListOrderedMap.decorate(mpCat2);
		Collectable group1R = new MatrixCollectable(" Ausprägung 5");	
		Collectable group1A = new MatrixCollectable(" Ausprägung 6");
		Collectable group1F = new MatrixCollectable(" Ausprägung 7");
		List<Object> mpData1a = new ArrayList<Object>();
		Collectable row1a = new MatrixCollectable(2);
		mpData1a.add(row1a);
		Collectable row2a = new MatrixCollectable(2);
		mpData1a.add(row2a);
		Collectable row3a = new MatrixCollectable(1);
		mpData1a.add(row3a);
		mpCat2.put(group1R, mpData1a);		
		mpCat2.put(group1A, mpData1a);
		mpCat2.put(group1F, mpData1a);
		
		mpContent.put(cat0, mpCat0);
		mpContent.put(cat1, mpCat1);
		mpContent.put(cat2, mpCat2);
		
		mod.setData(mpContent);
		
		return mod;
	}
	
	protected MatrixTableModel wysiwygModelFixed() {
		MatrixTableModel mod = new MatrixTableModel(null);
		mod.setFixedModel(true);
		
		Map<Collectable, Map<Collectable, List<Object>>> mpContent = new HashMap<Collectable, Map<Collectable, List<Object>>>();
		mpContent = ListOrderedMap.decorate(mpContent);
		
		Map<UID, FieldMeta<?>> mpFieldsY = MetaProvider.getInstance().getAllEntityFieldsByEntity(getEntityY());
		
		MatrixCollectable cat0 = new MatrixCollectable(" ");
		cat0.setField(UID.UID_NULL);
		
		Map<Collectable, List<Object>> mpCat0 = new HashMap<Collectable, List<Object>>();
		Collectable group1 = new MatrixCollectable("");
		List<Object> mpData1 = new ArrayList<Object>();
		mpCat0.put(group1, mpData1);
		mpContent.put(cat0, mpCat0);
		for(UID uidField : mpFieldsY.keySet()) {
			FieldMeta meta = MetaProvider.getInstance().getEntityField(uidField);
			String sLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(meta);
			MatrixCollectable cl = new MatrixCollectable(" " + sLabel);
			cl.setField(uidField);
			Map<Collectable, List<Object>> mpCat1 = new HashMap<Collectable, List<Object>>();
			MatrixCollectable group2 = new MatrixCollectable("");			
			List<Object> mpData2 = new ArrayList<Object>();
			mpCat1 = ListOrderedMap.decorate(mpCat1);		
			mpCat1.put(group2, mpData2);
			mpContent.put(cl, mpCat1);
		}
	
		mod.setData(mpContent);
		
		return mod;
	}

	/**
	 * @param listener
	 */
	public synchronized void addChangeListener(ChangeListener listener) {
		this.lstchangelistener.add(listener);
	}

	/**
	 * @param listener
	 */
	public synchronized void removeChangeListener(ChangeListener listener) {
		this.lstchangelistener.remove(listener);
	}
	
	protected JTableHeader createCategoryHeaderFixed() {
		TableColumnModel colModel = new MatrixTableHeaderModel();		
		
		for(Collectable catHeader : modelFixed.getCategorieHeader()) {
			TableColumn columnHeader = new TableColumn();
			columnHeader.setResizable(true);
			columnHeader.setHeaderValue(" " + catHeader.getValue(UID.UID_NULL));			
			colModel.addColumn(columnHeader);
		}
		
		final JTableHeader headerCategory = new JTableHeader(colModel) {
			
			private final Icon ascendingSortIcon = UIManager.getIcon("Table.ascendingSortIcon");
			private final Icon descendingSortIcon = UIManager.getIcon("Table.descendingSortIcon");
			
			@Override
			public String getToolTipText(MouseEvent event) {
				String text = super.getToolTipText(event);
				JTableHeader head = (JTableHeader)event.getSource();
				Point point = event.getPoint();
				int colIndex = head.columnAtPoint(point);
				if(colIndex == -1) return null;
				TableColumn tc = head.getColumnModel().getColumn(colIndex);
				text = (String) tc.getHeaderValue();
				return text;
			}

			@Override
			public TableCellRenderer getDefaultRenderer() {
				final DefaultTableCellRenderer tcrDefault = (DefaultTableCellRenderer) super.getDefaultRenderer();
				
				TableCellRenderer renSort = new TableCellRenderer() {
					
					@Override
					public Component getTableCellRendererComponent(JTable table, Object value,	boolean isSelected, boolean hasFocus, int row, int column) {
						final Component comp = tcrDefault.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
						if(comp instanceof JLabel) {
							final JLabel jlabel = (JLabel) comp;
							jlabel.setHorizontalAlignment(SwingConstants.LEFT);
							List<? extends SortKey> sortKeys = modelFixed.getSortKeys();
							if(sortKeys != null && sortKeys.size() > column)  {
								SortOrder order = sortKeys.get(column).getSortOrder();
								switch (order) {
								case ASCENDING:
									jlabel.setIcon(ascendingSortIcon);
									break;
								case DESCENDING:
									jlabel.setIcon(descendingSortIcon);
									break;
								case UNSORTED:
									jlabel.setIcon(null);
									break;
								default:
									jlabel.setIcon(null);
									break;
								}
							}
							else {
								jlabel.setIcon(null);
							}
						}
						return comp;
					}
				};
				
				return renSort;
			}
			
		};
		
		headerCategory.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(final MouseEvent e) {
				
				if(SwingUtilities.isRightMouseButton(e) && e.getClickCount() == 1) {
					if (isCustomizeSubformNotAllowed()) {
						return;
					}
					JPopupMenu popup = new JPopupMenu();
					
					boolean bHide = true;
					JTableHeader head = (JTableHeader)e.getSource();
					Point point = e.getPoint();
					int colIndex = head.columnAtPoint(point);

					if(colIndex == -1)
						bHide = false;
					
					JMenuItem miHideGroup = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage("JMatrixComponent.1", "Gruppe ausblenden"));
					miHideGroup.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent ae) {
							bFireListeners = false;
							JTableHeader head = (JTableHeader)e.getSource();
							Point point = e.getPoint();
							int colIndex = head.columnAtPoint(point);
							if(colIndex == -1)
								return;
							
							List<UID> tmpSelected = new ArrayList<UID>(lstSelectedFixed);
							SortedSet<UID> tmpAvailable = new TreeSet<UID>(setAvailableFixed);
							updatePrefs();
							UID sRemoved = tmpSelected.remove(colIndex);
							tmpAvailable.add(sRemoved);
							rearrangeTableFixed(tmpAvailable, tmpSelected);
							hideNotViewableColumns();							
							bFireListeners = true;
						}
					});
					
					if(bHide)
						popup.add(miHideGroup);
					
					final SelectGroupsController ctl = new SelectGroupsController(JMatrixComponent.this);
					JMenuItem miSelectColumns = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage("JMatrixComponent.2", "Gruppen ein-ausblenden"));
					miSelectColumns.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							cmdSelectGroupsFixed(ctl);
							
						}
					});
					popup.add(miSelectColumns);					
					
					popup.show(headerCategory, e.getX(), e.getY());
				}
				else if(SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 1) {
					stopEditing();					
					JTableHeader head = (JTableHeader)e.getSource();
					Point point = e.getPoint();
					int colIndex = head.columnAtPoint(point);
					if(colIndex > 0 && modelFixed.isSortingAllowed(colIndex)) {					
						modelFixed.resetSortOrder(colIndex);
						modelFixed.sort(colIndex, model);
						updatePrefs();
					}
				}
			}						
			
		});
		
		headerCategory.getColumnModel().addColumnModelListener(new TableColumnModelAdapter() {

			@Override
			public void columnMoved(TableColumnModelEvent e) {
				if (isCustomizeSubformNotAllowed()) {
					return;
				}
			}			

		});
		
		headerCategory.setReorderingAllowed(false);
		
		headerCategory.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				updateFixedTableHeaderDimensions();
				updatePrefs();
				
			}
		});
		
								
		return headerCategory;
	}
	
	
	private void updateFixedTableHeaderDimensions() {
		int width = headerFixed.getColumnModel().getTotalColumnWidth();
		headerFixed.setPreferredSize(new Dimension(width, HEADER_HEIGHT));
		scrollPane.getRowHeader().setPreferredSize(new Dimension(width, tblFixed.getHeight()));
		scrollPane.getRowHeader().setSize(new Dimension(width, tblFixed.getHeight()));
		
		for(int i = 0; i < headerFixed.getColumnModel().getColumnCount(); i++) {
			TableColumn tc = headerFixed.getColumnModel().getColumn(i);					
			int colWidth = tc.getWidth();
			tblFixed.getColumnModel().getColumn(i).setWidth(colWidth);
			tblFixed.getColumnModel().getColumn(i).setPreferredWidth(colWidth);
			
			String headerValue = (String) tc.getHeaderValue();
			UID uidValue = getFieldUID(headerValue);
			if(uidValue.equals(UID.UID_NULL)) {
				continue;
			}
			mpFixedFieldWidths.put(uidValue, colWidth);
		}
	}

	
	protected UID getFieldUID(String label) {
		for(UID uid : MetaProvider.getInstance().getAllEntityFieldsByEntity(getEntityY()).keySet()) {
			String sLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(MetaProvider.getInstance().getEntityField(uid));
			if(sLabel.equals(label.trim()))
				return uid;
		}
		return UID.UID_NULL;
	}
	
	private boolean isCustomizeSubformNotAllowed() {
		return !SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS) &&
				WorkspaceUtils.getInstance().getWorkspace().isAssigned();			
	}
	
	protected JTableHeader createCategoryHeader() {
		
		TableColumnModel colModel = new MatrixTableHeaderModel();		
		int headerCount = 0;
		
		for(Collectable catHeader : model.getCategorieHeader()) {
			TableColumn columnHeader = new TableColumn();
			columnHeader.setResizable(false);
						
			columnHeader.setHeaderValue(" " + catHeader.getValue(UID.UID_NULL));
			
			colModel.addColumn(columnHeader);			
		}
		
		final JTableHeader headerCategory = new JTableHeader(colModel) {
						
			@Override
			public String getToolTipText(MouseEvent event) {
				String text = super.getToolTipText(event);
				JTableHeader head = (JTableHeader)event.getSource();
				Point point = event.getPoint();
				int colIndex = head.columnAtPoint(point);
				if(colIndex == -1) return null;
				TableColumn tc = head.getColumnModel().getColumn(colIndex);
				text = (String) tc.getHeaderValue();
				return text;
			}

			// Workaround, only set the location when scrollbar is in use, see also Adjustment Listener
			@Override
			public void setLocation(int x, int y) {
				// TODO Auto-generated method stub
				if(setLocation)
					super.setLocation(x, y);
			}	

		};
		
		headerCategory.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(final MouseEvent e) {
				
				if(SwingUtilities.isRightMouseButton(e) && e.getClickCount() == 1) {
					
					if (isCustomizeSubformNotAllowed()) {
						return;
					}
					
					JPopupMenu popup = new JPopupMenu();
					
					boolean bHide = true;
					JTableHeader head = (JTableHeader)e.getSource();
					Point point = e.getPoint();
					int colIndex = head.columnAtPoint(point);

					if(colIndex == -1)
						bHide = false;
					
					JMenuItem miHideGroup = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage("JMatrixComponent.1", "Gruppe ausblenden"));
					miHideGroup.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent ae) {
							JTableHeader head = (JTableHeader)e.getSource();
							Point point = e.getPoint();
							int colIndex = head.columnAtPoint(point);
							if(colIndex < 0)
								return;
							
							List<String> tmpSelected = new ArrayList<String>(lstSelected);
							SortedSet<String> tmpAvailable = new TreeSet<String>(setAvailable);
							String sRemoved = tmpSelected.remove(colIndex);
							tmpAvailable.add(sRemoved);
							rearrangeTable(tmpAvailable, tmpSelected);	
							rearrangeTableFixed(getAvailableFieldsFixed(), getSelectedFieldsFixed());
							updatePrefs();
							hideNotViewableColumns();
						}
					});
					
					if(bHide)
						popup.add(miHideGroup);
					
					final SelectGroupsController ctl = new SelectGroupsController(JMatrixComponent.this);
					JMenuItem miSelectColumns = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage("JMatrixComponent.2", "Gruppen ein-ausblenden"));
					miSelectColumns.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							cmdSelectGroups(ctl);
							
						}
					});
					popup.add(miSelectColumns);			
					
					//BMWFDM-457
					if (colIndex >= 0 && colIndex < head.getColumnModel().getColumnCount()) {
						String headerValue = (String)head.getColumnModel().getColumn(colIndex).getHeaderValue();
						final int colIndexColumn = mpCategorie.get(headerValue.trim()).x;
											
						final SelectGroupsController ctlColums = new SelectGroupsController(JMatrixComponent.this);
						JMenuItem miSelectColumn = new JMenuItem(LocaleDelegate.getInstance().getResource("ResultPanel.11"));
						miSelectColumn.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								cmdSelectColumns(colIndexColumn, ctlColums);
							}
						});
						popup.add(miSelectColumn);
					}
					
					popup.show(headerCategory, e.getX(), e.getY());
				}
			}						
			
		});
		
		headerCategory.getColumnModel().addColumnModelListener(new TableColumnModelAdapter() {

			@Override
			public void columnMoved(TableColumnModelEvent e) {
				if (isCustomizeSubformNotAllowed()) {
					return;
				}
				int from = e.getFromIndex();
				int to = e.getToIndex();
			
				if(from == to)
					return;
				ListOrderedMap mp = (ListOrderedMap) model.getContent();
				Collectable headerFrom = (Collectable) mp.get(from);
				
				Object toMove = mp.remove(from);
				mp.put(to, headerFrom, toMove);
				model.fireTableDataChanged();
				setTableModel(model, modelFixed);				
				try {
					String sMoved = lstSelected.remove(from);
					lstSelected.add(to, sMoved);
				}
				catch(IndexOutOfBoundsException ex) {
					LOG.warn(ex.getMessage());
				}
				rearrangeTableFixed(getAvailableFieldsFixed(), getSelectedFieldsFixed());
				setFixedTableColumnWidth();
				hideNotViewableColumns();
			}			

		});
						
		return headerCategory;
	}
	
	public void refresh(WorkspaceDescription2.MatrixPreferences prefs, boolean fixedTable) {
		if (modelFixed == null) {
			return;
		}
		List<SortKey> lstSorting = new ArrayList<SortKey>();
		checkPrefs(prefs);
		
		for(Pair<Integer, Integer> sorting : prefs.getSortKeys()) {
			switch (sorting.y) {
			case 1:
				lstSorting.add(new SortKey(sorting.x, SortOrder.ASCENDING));
				break;
			case 2:
				lstSorting.add(new SortKey(sorting.x, SortOrder.DESCENDING));
				break;
			default:
				lstSorting.add(new SortKey(sorting.x, SortOrder.UNSORTED));
				break;
			}			
		}

		modelFixed.setSortKeys(lstSorting);
		rearrangeTable(prefs.getAvailable(), prefs.getSelected());
				
		if(prefs.getAvailableGroupElements() != null && getPreferencesField() != null) {
			setAvailableGroupElements(prefs.getAvailableGroupElements());
		}
		if(prefs.getSelectedGroupElements() != null && getPreferencesField() != null) {
			setSelectedGroupElements(prefs.getSelectedGroupElements());
		}
		
		hideNotViewableColumns();
		
		if(fixedTable) {
			setFixedFieldWidths(prefs.getFixedFieldWidths());
			rearrangeTableFixed(prefs.getAvailablefixed(), prefs.getSelectedfixed());	
			modelFixed.sort(-1, model);
			updatePrefs();			
			setFixedTableColumnWidth();
			hideNotViewableColumns();			
		}
	}
	
	private void checkPrefs(WorkspaceDescription2.MatrixPreferences prefs) {
		if(prefs.getSortKeys() == null) {
			prefs.setSortKeys(new ArrayList<Pair<Integer,Integer>>());
		}
		if(prefs.getSelected() == null || prefs.getSelected().isEmpty()) {
			List<String> lstTmp = new ArrayList<String>();
			if (model != null) {
				for(Collectable cl : model.getContent().keySet()) {
					lstTmp.add((String) cl.getValue(UID.UID_NULL));
				}
			}
			prefs.setSelected(lstTmp);
			prefs.setAvailable(new TreeSet<String>());
		}
		if(prefs.getSelectedfixed() == null || prefs.getSelectedfixed().isEmpty()) {
			List<UID> lstTmp = new ArrayList<UID>();
			if (modelFixed != null) {
				for(Collectable cl : modelFixed.getContent().keySet()) {
					MatrixCollectable mc = (MatrixCollectable)cl;
					//lstTmp.add((UID) cl.getValue(UID.UID_NULL));
					lstTmp.add(mc.getField());
				}
			}
			prefs.setSelectedfixed(lstTmp);
			prefs.setAvailablefixed(new TreeSet<UID>());
		}
		Collection<UID> colRemove = new ArrayList<UID>();
		if(prefs.getAvailablefixed() != null) {
			for(UID uidField : prefs.getAvailablefixed()) {
				if(uidField.equals(UID.UID_NULL)) {
					continue;
				}
				try {
					MetaProvider.getInstance().getEntityField(uidField);
				}
				catch(CommonFatalException ex) {
					colRemove.add(uidField);
				}
			}
			prefs.getAvailablefixed().removeAll(colRemove);
		}
		colRemove.clear();
		if(prefs.getFixedFieldWidths() != null) {
			for(UID uidField : prefs.getFixedFieldWidths().keySet()) {
				if(uidField.equals(UID.UID_NULL)) {
					continue;
				}
				try {
					MetaProvider.getInstance().getEntityField(uidField);
				}
				catch(CommonFatalException ex) {
					colRemove.add(uidField);
				}
			}
			for(UID uidRemove : colRemove) {
				prefs.getFixedFieldWidths().remove(uidRemove);
			}
		}
		colRemove.clear();
		if(prefs.getSelectedfixed() != null) {
			for(UID uidField : prefs.getSelectedfixed()) {
				if(uidField.equals(UID.UID_NULL)) {
					continue;
				}
				try {
					MetaProvider.getInstance().getEntityField(uidField);
				}
				catch(CommonFatalException ex) {
					colRemove.add(uidField);
				}
			}
			prefs.getSelectedfixed().removeAll(colRemove);
		}
	}	
	
	public <PK> Collection<CollectableMasterData<PK>> getSelectedCollectables() {
		Collection<CollectableMasterData<PK>> colSelected = new ArrayList<>();
		
		EntityMeta<?> meta =  MetaProvider.getInstance().getEntity(getEntityY());
		CollectableEntity clcte = new CollectableMasterDataEntity(meta);
		
		if(tblFixed == null) {
			return colSelected;
		}
		int rows[] = tblFixed.getSelectedRows();
		if(rows.length == 0)
			return colSelected;
		for(int row = 0; row < rows.length; row++) {
			
			CollectableMasterData cmd = new CollectableMasterData(clcte, new MasterDataVO(meta, false));
			for(int c = 0 ; c < tblFixed.getModel().getColumnCount(); c++) {
				MatrixCollectable mc = (MatrixCollectable) tblFixed.getModel().getValueAt(rows[row], c);
				
				if(mc.getVO() != null) {
					EntityObjectVO vo = mc.getVO();					
					for(Object field : vo.getFieldValues().keySet()) {
						UID fieldUid = (UID)field;

						if (fieldUid.equals(SF.VERSION.getUID(meta))) {
							continue;
						}

						if(!clcte.getEntityField(fieldUid).isIdField()) {
							cmd.setField(fieldUid, new CollectableValueField(vo.getFieldValue(fieldUid)));
						}
					}
					
					for(Object field : vo.getFieldIds().keySet()) {
						cmd.setField((UID)field, new CollectableValueIdField(vo.getFieldId((UID)field), vo.getFieldValue((UID)field)));
					}

					for(Object field : vo.getFieldUids().keySet()) {
						cmd.setField((UID)field, new CollectableValueIdField(vo.getFieldUid((UID)field), vo.getFieldValue((UID)field)));
					}

				}
				if(mc.getId() != null) {
					cmd.getMasterDataCVO().setPrimaryKey(mc.getId());
					cmd.getMasterDataCVO().setVersion(mc.getVersion());
				}
				
			}
			
			getMatrixCollectable(cmd, row);
			
			colSelected.add(cmd);
		}

		return colSelected;
	}
	
	public <PK> CollectableMasterData<PK> getSelectedCollectable() {
		
		EntityMeta<?> meta =  MetaProvider.getInstance().getEntity(getEntityY());
		CollectableEntity clcte = new CollectableMasterDataEntity(meta);
		CollectableMasterData<PK> cmd = new CollectableMasterData<PK>(clcte, new MasterDataVO<>(meta, false));
		if(tblFixed == null) {
			return null;
		}
		int row = tblFixed.getSelectedRow();
		if(row == -1) {
			return null;			
		}
		
		for (int c = 0; c < tblFixed.getModel().getColumnCount(); c++) {
			MatrixCollectable mc = (MatrixCollectable) tblFixed.getModel().getValueAt(row, c);
			
			if (mc.getVO() != null) {
				EntityObjectVO vo = mc.getVO();
				
				for (Object field : vo.getFieldValues().keySet()) {
					UID fieldUid = (UID)field;

					if (fieldUid.equals(SF.VERSION.getUID(meta))) {
						continue;
					}
					
					if (!clcte.getEntityField(fieldUid).isIdField() && SF.SYSTEMIDENTIFIER.checkField(getEntityY(), fieldUid)) {
						cmd.setField(fieldUid, new CollectableValueField(vo.getFieldValue(fieldUid)));
					}					
					else if (!clcte.getEntityField((UID)field).isIdField()) {
						cmd.setField((UID)field, new CollectableValueField(vo.getFieldValue((UID)field)));
					};
				}
				
				for (Object field : vo.getFieldIds().keySet()) {
					cmd.setField((UID)field, new CollectableValueIdField(vo.getFieldId((UID)field), vo.getFieldValue((UID)field)));
				}

				for(Object field : vo.getFieldUids().keySet()) {
					cmd.setField((UID)field, new CollectableValueIdField(vo.getFieldUid((UID)field), vo.getFieldValue((UID)field)));
				}
				
			}
			if(mc.getId() != null) {
				cmd.getMasterDataCVO().setPrimaryKey((PK)mc.getId());
				cmd.getMasterDataCVO().setVersion(mc.getVersion());
			}
			
		}
		getMatrixCollectable(cmd, row);
		
		return cmd;
	}

	private <PK> void getMatrixCollectable(CollectableMasterData<PK> cmd, int row) {
		Collection<EntityObjectVO<Long>> collmdvoDependants = new ArrayList<>();
		MatrixTableModel model = (MatrixTableModel) table.getModel();
		Map<Collectable, Map<Collectable, List<Object>>> mp = model.getContent();
		
		for(Collectable cl : mp.keySet()) {
			Map<Collectable, List<Object>> mpList = mp.get(cl);
			for(Collectable clGroup : mpList.keySet()) {
				List<Object> lstColumn = mpList.get(clGroup);
				Object obj = lstColumn.get(row);
				MatrixCollectable mctl = (MatrixCollectable)obj;
				
				EntityObjectVO voToSave = new EntityObjectVO(getMatrixEntity());
				//value cell
				voToSave.setFieldValue(getEntityMatrixValueField(), mctl.getValue(UID.UID_NULL));
				// x axis
				voToSave.setFieldId(getEntityXRefField(), (Long) clGroup.getId());
				// y axis
				voToSave.setFieldId(getMatrixEntityParentField(), (Long) cmd.getPrimaryKey());
				voToSave.setFieldValue(getMatrixEntityParentField(), cmd.getPrimaryKey());				
				voToSave.setPrimaryKey(mctl.getId());
				voToSave.setVersion(mctl.getVersion());
				if(mctl.getId() != null && mctl.isModified())
					voToSave.flagUpdate();
				else if(mctl.getId() == null) {
					voToSave.flagNew();
				}
				
				if(mctl.isRemoved()) {
					voToSave.flagRemove();
				}
				collmdvoDependants.add(voToSave);
			}
		}
		
		IDependentDataMap mpSub = new DependentDataMap();
		IDependentKey dependentKey = DependentDataMap.createDependentKey(this.getMatrixEntityParentField());
		mpSub.addAllData(dependentKey, collmdvoDependants);
		cmd.setDependantMasterDataMap(mpSub);
	}
	
	private void setFixedTableColumnWidth() {
		int headerWidth = 0;
		if(mpFixedFieldWidths.isEmpty()) {
			Enumeration<TableColumn> enTC = tblFixed.getColumnModel().getColumns();
			while(enTC.hasMoreElements()) {
				headerWidth += enTC.nextElement().getWidth();
			}						
		}
		else {				
			for(UID key : mpFixedFieldWidths.keySet()) {
				int width = mpFixedFieldWidths.get(key);
				if(key.equals(UID.UID_NULL)) {
					headerWidth += 20;
					continue;
				}
				for(int i = 0; i < headerFixed.getColumnModel().getColumnCount(); i++) {
					TableColumn tc = headerFixed.getColumnModel().getColumn(i);
					String sHeader = ((String) tc.getHeaderValue()).trim();
					boolean setToZero = false;
					for(UID uid : setAvailableFixed) {
						if (uid == null || UID.UID_NULL.equals(uid)) {
							continue;
						}

						String sLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(MetaProvider.getInstance().getEntityField(uid));
						if(sLabel.equals(sHeader)) {
							tc.setPreferredWidth(0);
							tc.setWidth(0);		
							tc.setMinWidth(0);
							
							TableColumn colFixed = tblFixed.getColumnModel().getColumn(i);
							colFixed.setWidth(0);
							colFixed.setPreferredWidth(0);
							colFixed.setMinWidth(0);
							setToZero = true;
							break;
						}
						
					}
					if(setToZero) continue;
					
					String headerValue = ((String)tc.getHeaderValue()).trim();
					String sLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(MetaProvider.getInstance().getEntityField(key));
					if(headerValue.equals(sLabel)) {
						if(width == 0) width = 100;
						tc.setPreferredWidth(width);
						tc.setWidth(width);					
						
						TableColumn colFixed = tblFixed.getColumnModel().getColumn(i);
						colFixed.setWidth(width);
						colFixed.setPreferredWidth(width);
						
						headerWidth += width;
						break;
					}
				}
			}
		}
		
		headerFixed.setSize(new Dimension(headerWidth, tblFixed.getHeight()));
		headerFixed.setPreferredSize(new Dimension(headerWidth, tblFixed.getHeight()));
		
		scrollPane.getRowHeader().setPreferredSize(new Dimension(headerWidth, tblFixed.getHeight()));
		scrollPane.getRowHeader().setSize(new Dimension(headerWidth, tblFixed.getHeight()));
	}
	
	
	/**
	 * @param setAvailable
	 * @param lstSelected
	 */
	protected void rearrangeTable(SortedSet<String> setAvailable, List<String> lstSelected) {
		if(lstSelected == null || lstSelected.isEmpty())
			return;
		
		ListOrderedMap mp = (ListOrderedMap) model.getContent();
		ListOrderedMap mpTmp = new ListOrderedMap();
		mpTmp.putAll(mp);
		
		List<String> s = new ArrayList<String>(lstSelected);
		SortedSet<String> a = new TreeSet<String>(setAvailable);
		
		mpSelected.clear();
		mpSelected = ListOrderedMap.decorate(mpSelected);
		this.lstSelected.clear();
		
		mpAvailable.clear();
		mpAvailable = ListOrderedMap.decorate(mpAvailable);
		this.setAvailable.clear();
		
		for(String selected : s) {
			this.lstSelected.add(selected);
			for(Object ob : mp.keySet()) {
				Collectable cl = (Collectable)ob;
				String value = (String) cl.getValue(UID.UID_NULL);
				if(selected.equals(value)) {
					mpSelected.put(cl, (Map<Collectable, List<Object>>) mp.get(ob));
				}
			}
		}
		for(String available : a) {
			this.setAvailable.add(available);
			for(Object ob : mp.keySet()) {
				Collectable cl = (Collectable)ob;
				String value = (String) cl.getValue(UID.UID_NULL);
				if(available.equals(value)) {
					mpAvailable.put(cl, (Map<Collectable, List<Object>>) mp.get(ob));
				}
			}
		}
		mp.clear();		
		
		for(Collectable cl : mpSelected.keySet()) {
			mp.put(cl, mpSelected.get(cl));
		}
		
		for(Collectable cl : mpAvailable.keySet()) {
			mp.put(cl, mpAvailable.get(cl));
		}
		
		for(Object obj : mpTmp.keySet()) {
			if(!mp.containsKey(obj)) {
				mp.put(obj, mpTmp.get(obj));
				mpSelected.put((Collectable)obj, (Map<Collectable,List<Object>>)mpTmp.get(obj));
				String sValue = (String) ((MatrixCollectable)obj).getValue(UID.UID_NULL);
				this.lstSelected.add(sValue.trim());
			}
		}
		
		for(Iterator<String> it = this.lstSelected.iterator(); it.hasNext(); ) {
			String sValue = it.next();
			boolean found = false;
			for(Collectable cl : model.getContent().keySet()) {
				MatrixCollectable mc = (MatrixCollectable)cl;
				String value = (String) mc.getValue(UID.UID_NULL);
				if(sValue.equals(value.trim())) {
					found = true;
					break;
				}
			}
			if(!found) {
				it.remove();
			}
		}
		
		model.fireTableDataChanged();
		setTableModel(model, modelFixed);
		hideColumns();		
		hideNullColumns();
		setFixedTableColumnWidth();		
	}
	
	/**
	 * @param setAvailable
	 * @param lstSelected
	 */
	protected void rearrangeTableFixed(SortedSet<UID> setAvailable, List<UID> lstSelected) {
		if(lstSelected == null || lstSelected.isEmpty())
			return;
		
		ListOrderedMap mp = (ListOrderedMap) modelFixed.getContent();
		if(!setAvailable.isEmpty())
			lstSelected.removeAll(setAvailable);
				
		List<UID> s = new ArrayList<UID>(lstSelected);
		SortedSet<UID> a = new TreeSet<UID>(setAvailable);
		
		for(Iterator<UID> it = s.iterator(); it.hasNext(); ) {
			UID sValue = it.next();
			boolean found = false;
			for(Object obj : mp.keySet()) {
				MatrixCollectable mc = (MatrixCollectable)obj;				
				if(mc.getEntityFieldMetaDataVO() != null && mc.getEntityFieldMetaDataVO().getUID().equals(sValue)){
					found = true;
					break;
				}
				if(mc.getEntityFieldMetaDataVO() == null && sValue.equals(UID.UID_NULL)) {
					found = true;
					break;
				}
			}
			if(!found){
				it.remove();
			}
		}
		
		for(Object obj : mp.keySet()) {
			MatrixCollectable mc = (MatrixCollectable)obj;
			UID sValue = mc.getField();
			
			if(!s.contains(sValue) && !a.contains(sValue)) {
				s.add(sValue);			
				int defaultSize = 100;
				mpFixedFieldWidths.put(sValue, defaultSize);
			}
			else if(s.contains(sValue) && !mpFixedFieldWidths.containsKey(sValue)) {
				int defaultSize = 100;
				mpFixedFieldWidths.put(sValue, defaultSize);
			}
			else if(a.contains(sValue) && !s.contains(sValue)) {
				mpFixedFieldWidths.put(sValue, 0);
			}
		}		
		
		
		mpSelectedFixed.clear();
		mpSelectedFixed = ListOrderedMap.decorate(mpSelectedFixed);
		this.lstSelectedFixed.clear();
		
		mpAvailableFixed.clear();
		mpAvailableFixed = ListOrderedMap.decorate(mpAvailableFixed);
		this.setAvailableFixed.clear();
		
		List<UID> sAdd = new ArrayList<UID>();
		for(UID selected : s) {
			this.lstSelectedFixed.add(selected);
			for(Object ob : mp.keySet()) {
				Collectable cl = (Collectable)ob;
				//String value = (String) cl.getValue(UID.UID_NULL);
				UID value = ((MatrixCollectable)cl).getField();
				if(selected.equals(value)) {
					mpSelectedFixed.put(cl, (Map<Collectable, List<Object>>) mp.get(ob));
				}
			}
		}
		lstSelected.addAll(sAdd);
		for(UID available : a) {
			this.setAvailableFixed.add(available);
			for(Object ob : mp.keySet()) {
				MatrixCollectable cl = (MatrixCollectable)ob;
				UID value = cl.getField();
				if(available.equals(value)) {
					mpAvailableFixed.put(cl, (Map<Collectable, List<Object>>) mp.get(ob));
				}
			}
		}
		mp.clear();		
		
		for(Collectable cl : mpSelectedFixed.keySet()) {
			mp.put(cl, mpSelectedFixed.get(cl));
		}
		
		for(Collectable cl : mpAvailableFixed.keySet()) {
			mp.put(cl, mpAvailableFixed.get(cl));
		}
		
		modelFixed.fireTableDataChanged();
		setTableModel(model, modelFixed);
		hideFixedColumns();
		setFixedTableColumnWidth();
	}
	

	protected void updatePrefs() {
		if(bLayout) return;

		this.matrixPreferences.setAvailable(getAvailableFields());
		this.matrixPreferences.setSelected(getSelectedFields());
		
		this.matrixPreferences.setAvailablefixed(getAvailableFieldsFixed());
		this.matrixPreferences.setSelectedfixed(getSelectedFieldsFixed());

		List<Pair<Integer, Integer>> lstSortKeys = new ArrayList<Pair<Integer,Integer>>();
		for(SortKey key : this.getSortKeys()) {
			int sortorder = 0;
			switch (key.getSortOrder()) {
			case ASCENDING:
				sortorder = 1;
				break;
			case DESCENDING:
				sortorder = 2;
				break;
			default:
				sortorder = 0;
				break;
			}
			lstSortKeys.add(new Pair<Integer, Integer>(key.getColumn(), sortorder));
		}
		this.matrixPreferences.setSortKeys(lstSortKeys);
		this.matrixPreferences.setShowOnly(true);
		this.matrixPreferences.setFixedFieldWidths(mpFixedFieldWidths);	
		
		this.matrixPreferences.setAvailableGroupElements(getAvailableGroupElements());
		
		this.matrixPreferences.setSelectedGroupElements(getSelectedGroupElements());

	}
	
	private Map<String, SortedSet<String>> removeComparatorForPrefs(Map<String, SortedSet<String>> set) {
		Map<String, SortedSet<String>> clone = new HashMap<String, SortedSet<String>>();
		for(String key : set.keySet()) {
			clone.put(key, removeComparatorForPrefs(set.get(key)));
		}
		return clone;
	}
	
	private SortedSet<String> removeComparatorForPrefs(SortedSet<String> set) {
		SortedSet<String> clone = new TreeSet<String>();
		clone.addAll(set);
		return clone;
	}
	
	protected void hideColumns() {
		
		Enumeration<TableColumn> en = header.getColumnModel().getColumns();
		while(en.hasMoreElements()) {
			TableColumn tc = en.nextElement();
			String sHeader = (String) tc.getHeaderValue();
			if(setAvailable.contains(sHeader.trim())) {
				tc.setMinWidth(0);
				tc.setPreferredWidth(0);
				tc.setWidth(0);				
			}
		}		
		
		Enumeration<TableColumn> enColumns = getTable().getColumnModel().getColumns();
		while(enColumns.hasMoreElements()) {
			TableColumn tc = enColumns.nextElement();
			String sHeader = (String) tc.getHeaderValue();
			for(Collectable clGroup : mpAvailable.keySet()) {
				Map<Collectable, List<Object>> mpTable = mpAvailable.get(clGroup);
				for(Collectable cl : mpTable.keySet()) {
					String sValue = (String) cl.getValue(UID.UID_NULL);
					if(sValue.trim().equals(sHeader.trim())) {
						tc.setMinWidth(0);
						tc.setPreferredWidth(0);
						tc.setWidth(0);
					}
				}
			}
		}
	}	 
	
	protected void hideNullColumns() {
		if (permissionMatrixEntity != null && permissionMatrixEntity.includesWriting()) {
			return;
		}
		
		if (this.matrixController.getCollectController() instanceof MasterDataCollectController) {
			return;
		}	
		
		List<Integer> lstHideColumns = new ArrayList<Integer>();
		int colCounter = 0;
		Map<String, Integer> mp = new HashMap<String, Integer>();
		mp = ListOrderedMap.decorate(mp);
		
		for(Collectable clHead : model.getContent().keySet()) {
			Map<Collectable, List<Object>> mpForm = model.getContent().get(clHead);
			int headCount = 0;
			for(Collectable clForm : mpForm.keySet()) {
				List<Object> lstColumn = mpForm.get(clForm);
				boolean hide = true; 
				for(Object obj : lstColumn) {
					MatrixCollectable mctl = (MatrixCollectable)obj;
					// hide empty columns if readonly and check icons
					if(cellInputType == null || LayoutMLConstants.ATTRIBUTE_CELL_INPUT_TYPE_CHECKICON.equals(cellInputType)) {
						Integer iValue = (Integer) mctl.getValue(UID.UID_NULL);
						if(iValue != null && iValue > 0) {
							hide = false;                                 
						}
					} else {
						hide= false;
					}
				}
				if(hide) {
					lstHideColumns.add(colCounter);
				}
				else {
					headCount++;
				}
				colCounter++;
			}
			mp.put((String) clHead.getValue(UID.UID_NULL), headCount);
		}
		
		for(Integer iCol : lstHideColumns) {
			TableColumn tcHide = table.getColumnModel().getColumn(iCol);
			tcHide.setMinWidth(0);
			tcHide.setWidth(0);
			tcHide.setPreferredWidth(0);
		}
		
		Enumeration<TableColumn> enHeader = header.getColumnModel().getColumns();
		while(enHeader.hasMoreElements()) {
			TableColumn tc = enHeader.nextElement();
			String sHead = (String) tc.getHeaderValue();
			if(mp.containsKey(sHead.trim())) {
				Integer iCount = mp.get(sHead.trim());
				int size = iCount * getColumnWidth();
				tc.setMinWidth(size);
				tc.setWidth(size);
				tc.setPreferredWidth(size);
			}
		}
		
	}
	
	protected void hideFixedColumns() {
		
		List<Integer> lstHideCol = new ArrayList<Integer>();
		Enumeration<TableColumn> en = headerFixed.getColumnModel().getColumns();
		int h = 0;
		while(en.hasMoreElements()) {
			TableColumn tc = en.nextElement();
			String sHeader = (String) tc.getHeaderValue();
			for(UID uid : setAvailableFixed) {
				String sLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(MetaProvider.getInstance().getEntityField(uid));
				if(sLabel.equals(sHeader.trim())) {
					tc.setMinWidth(0);
					tc.setPreferredWidth(0);
					tc.setWidth(0);
					lstHideCol.add(h);
					break;
				}
			}
//			if(setAvailableFixed.contains(sHeader.trim())) {
//				tc.setMinWidth(0);
//				tc.setPreferredWidth(0);
//				tc.setWidth(0);
//				lstHideCol.add(h);
//			}
			h++;
		}		
		
		for(Integer i : lstHideCol) {
			TableColumn tc = tblFixed.getColumnModel().getColumn(i);
			tc.setMinWidth(0);
			tc.setPreferredWidth(0);
			tc.setWidth(0);
		}
		
	}
	
	public void setEditEnabledScript(NuclosScript script) {
		this.editEnabledScript = script;
	}
	
	public void setNewEnabledScript(NuclosScript script) {
		this.newEnabledScript = script;
	}
	
	public void setCloneEnabledScript(NuclosScript script) {
		this.cloneEnabledScript = script;
	}
	
	public void setDeleteEnabledScript(NuclosScript script) {
		this.deleteEnabledScript= script;
	}
	
	public NuclosScript getEditEnabledScript() {
		return editEnabledScript;
	}
	
	public UID getMatrixEntity() {
		return sMatrixEntity;
	}

	public void setMatrixEntity(UID sMatrixEntity) {
		this.sMatrixEntity = sMatrixEntity;
	}
	
	public void setMatrixEntityParentField(UID sField) {
		this.sMatrixEntityParentField = sField;
	}
	
	public UID getMatrixEntityParentField() {
		return this.sMatrixEntityParentField;
	}

	public UID getEntityX() {
		return sEntityX;
	}

	public void setEntityX(UID sEntityX) {
		this.sEntityX = sEntityX;
	}
	
	public UID getPreferencesField () {
		return this.sPreferencesField;
	}
	
	public void setPreferencesField (UID field) {
		this.sPreferencesField = field;
	}
	
	public List<String> getSelectedFields() {
		return this.lstSelected;
	}
	
	public void setSelectedFields(List<String> list) {
		this.lstSelected = list;
	}
	
	public SortedSet<String> getAvailableFields() {
		return setAvailable;
	}
	
	public void setAvailableFields(SortedSet<String> set) {
		this.setAvailable = set;
	}
	
	public List<UID> getSelectedFieldsFixed() {
		return this.lstSelectedFixed;
	}
	
	public void setSelectedFieldsFixed(List<UID> list) {
		this.lstSelectedFixed = list;
	}
	
	public SortedSet<UID> getAvailableFieldsFixed() {
		return setAvailableFixed;
	}
	
	public void setAvailableFieldsFixed(SortedSet<UID> set) {
		this.setAvailableFixed = set;
	}
	
	public Map<String, List<String>> getSelectedGroupElements() {
		return this.mpSelectedGroupElements;
	}
	
	public void setSelectedGroupElements(Map<String, List<String>> map) {
		this.mpSelectedGroupElements = map;
	}
	
	public Map<String, SortedSet<String>> getAvailableGroupElements() {
		return this.mpAvailableGroupElements;
	}
	
	public void setAvailableGroupElements(Map<String, SortedSet<String>> map) {
		this.mpAvailableGroupElements = map;
	}
	
	public Map<UID, Integer> getFixedFieldWidths() {
		return this.mpFixedFieldWidths;
	}		
	
	public void setFixedFieldWidths(Map<UID, Integer> mp) {
		if(mp != null) {
			this.mpFixedFieldWidths = mp;
		}
	}
	
	public void setSortKeys(List<SortKey> keys) {
		modelFixed.setSortKeys(keys);
	}
	
	public List<SortKey> getSortKeys() {
		return modelFixed.getSortKeys();
	}

	public UID getEntityY() {
		return sEntityY;
	}

	public void setEntityY(UID sEntityY) {
		this.sEntityY = sEntityY;
	}
	
	public UID getFieldCategorie() {
		return sFieldCategorie;
	}

	public void setFieldCategorie(UID sFieldCategorie) {
		this.sFieldCategorie = sFieldCategorie;
	}

	public UID getFieldX() {
		return sFieldX;
	}

	public void setFieldX(UID sFieldX) {
		this.sFieldX = sFieldX;
	}

	public UID getFieldY() {
		return sFieldY;
	}

	public void setFieldY(UID sFieldY) {
		this.sFieldY = sFieldY;
	}
	
	public void setEntityYParentField(UID sField) {
		this.sEntityYParentField = sField;
	}
	
	public UID getEntityYParentField() {
		return this.sEntityYParentField;
	}
	
	public void setEntityXRefField(UID sField) {
		this.sEntityXRefField = sField;
	}
	
	public UID getEntityXRefField() {
		return this.sEntityXRefField;
	}
	
	public void setEntityMatrixValueField(UID field) {
		this.sEntityMatrixValueField = field;
	}
	
	public UID getEntityMatrixValueField() {
		return this.sEntityMatrixValueField;
	}
	
	public MatrixPreferences getMatrixPreferences() {
		return matrixPreferences;
	}

	public void setMatrixPreferences(MatrixPreferences entityPreferences) {		
		this.matrixPreferences = entityPreferences;
		List<String> selected = this.matrixPreferences.getSelected();
		if(selected != null && !selected.isEmpty()) {
			final List<String> contains = new ArrayList<String>();
			Set<Collectable> setContentHeader = model.getContent().keySet();
			CollectionUtils.filter(selected, new Predicate() {
				
				@Override
				public boolean evaluate(Object object) {
					String str = (String)object;
					if(!contains.contains(str)) {
						contains.add(str);
						return true;
					}
					return false;
				}
			});
		}
			
	}
	
	public Integer getNumberState() {
		if(iNumberState == null) {
			iNumberState = 3; // default number of states
		}
		return iNumberState;
	}

	public void setNumberState(Integer iNumberState) {
		this.iNumberState = iNumberState;
	}

	public String getCellInputType() {
		return cellInputType;
	}

	public void setCellInputType(String cellInputType) {
		this.cellInputType = cellInputType;
	}

	
	public String getEntityMatrixReferenceField() {
		return entityMatrixReferenceField;
	}

	public void setEntityMatrixReferenceField(String entityMatrixReferenceField) {
		this.entityMatrixReferenceField = entityMatrixReferenceField;
	}
	
	public String getEntityXVlpId() {
		return entityXVlpId;
	}

	public void setEntityXVlpId(String entityXVlpId) {
		this.entityXVlpId = entityXVlpId;
	}

	public String getEntityXVlpIdFieldName() {
		return entityXVlpIdFieldName;
	}

	public void setEntityXVlpIdFieldName(String entityXVlpIdFieldName) {
		this.entityXVlpIdFieldName = entityXVlpIdFieldName;
	}

	public String getEntityXVlpFieldName() {
		return entityXVlpFieldName;
	}

	public void setEntityXVlpFieldName(String entityXVlpFieldName) {
		this.entityXVlpFieldName = entityXVlpFieldName;
	}

	public String getEntityXVlpReferenceParamName() {
		return entityXVlpReferenceParamName;
	}

	public void setEntityXVlpReferenceParamName(String entityXVlpReferenceParamName) {
		this.entityXVlpReferenceParamName = entityXVlpReferenceParamName;
	}

	
	public String getEntityXHeader() {
		return entityXHeader;
	}

	public void setEntityXHeader(String entityXHeader) {
		this.entityXHeader = entityXHeader;
	}

	public String getEntityYHeader() {
		return entityYHeader;
	}

	public void setEntityYHeader(String entityYHeader) {
		this.entityYHeader = entityYHeader;
	}

	public String getEntityXSortingFields() {
		return entityXSortingFields;
	}
	
	public void setEntityXSortingFields(String entityXSortingFields) {
		this.entityXSortingFields = entityXSortingFields;
	}
	
	public String getEntityYSortingFields() {
		return entityYSortingFields;
	}
	
	public void setEntityYSortingFields(String entityYSortingFields) {
		this.entityYSortingFields = entityYSortingFields;
	}
	
	
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public JTable getTable() {
		return table;
	}
	
	public JTable getTableFixed() {
		return tblFixed;
	}

	public JTableHeader getHeader() {
		return header;
	}
	
	public JTableHeader getHeaderFixed() {
		return headerFixed;
	}

	public JToolBar getToolbar() {
		return toolbar;
	}
	
	boolean isLayout() {
		return bLayout;
	}
	
	public void setLayout(boolean layout) {
		this.bLayout = layout;
	}
	
	public IPopupListener getPopupMenuListener() {
		return popupMenuListener;
	}

	public void setPopupMenuListener(IPopupListener popupMenuListener) {
		this.popupMenuListener = popupMenuListener;
	}	
	
	private void cmdSelectColumns(final int colIndex, final SelectGroupsController ctl) {
		ChoiceList<String> cl = new ChoiceList<String>();
		String categorie = null;
		for(String catKey : mpCategorie.keySet()) {
			Pair<Integer, Integer> pair = mpCategorie.get(catKey);
			if(colIndex >= pair.x && colIndex <= pair.y) {
				categorie = catKey;
				break;
			}
		}	
		
		if(categorie == null) return;
		
		List<String> lstclctefSelected = new ArrayList<String>();
		if(mpSelectedGroupElements.get(categorie) != null) {
			lstclctefSelected.addAll(mpSelectedGroupElements.get(categorie));
		}
		else {		
			for(Collectable clct : model.getContent().keySet()) {
				String sCategorie = (String) clct.getValue(UID.UID_NULL);
				if(sCategorie.equals(categorie)) {
					Map<Collectable, List<Object>> mp = model.getContent().get(clct);
					for(Collectable col : mp.keySet()) {
						lstclctefSelected.add(((String) col.getValue(UID.UID_NULL)).trim());
					}
					break;
				}
			}
		}
		if(mpAvailableGroupElements.get(categorie) != null) {
			cl.set(mpAvailableGroupElements.get(categorie), lstclctefSelected, new StringComparator());
		}
		else {																
			cl.set(new TreeSet<String>(), lstclctefSelected, new StringComparator());
		}
		ctl.setModel(cl);
		final boolean bOK = ctl.run(LocaleDelegate.getInstance().getResource("ResultPanel.11"));
		final String sGroup = categorie;
		if(bOK) {
			UIUtils.runCommand(JMatrixComponent.this, new CommonRunnable() {
				@Override
		        public void run() throws CommonBusinessException {
					SortedSet<String> setAvailable = ctl.getAvailableObjects();
					List<String> lstSelected = ctl.getSelectedObjects();
					mpSelectedGroupElements.put(sGroup, lstSelected);
					mpAvailableGroupElements.put(sGroup, setAvailable);
					updatePrefs();
					hideNotViewableColumns();
					fireChangeListener();
				}
			});
		}
	}
	
	private void hideGroupElements() {
		
		for(String sGroup : mpSelectedGroupElements.keySet()) {
			List<String> lstElements = mpSelectedGroupElements.get(sGroup);
			lstElements.clear();			
			Map<Collectable, Map<Collectable, List<Object>>> mp = model.getContent();
			for(Collectable cl : mp.keySet()) {
				String sHeader = (String) cl.getValue(UID.UID_NULL);
				if(sGroup.equals(sHeader.trim())) {
					Map<Collectable, List<Object>> mpGroup = mp.get(cl);
					for(Collectable clAttribut : mpGroup.keySet()) {
						String sValue = (String) clAttribut.getValue(UID.UID_NULL);
						lstElements.add(sValue.trim());
					}
				}
			}
			if(mpAvailableGroupElements.get(sGroup) != null) {
				SortedSet<String> lstAvailableElements = mpAvailableGroupElements.get(sGroup);	
				lstElements.removeAll(lstAvailableElements);
			}
		}	
		
		for(String sGroup : mpAvailableGroupElements.keySet()) {			
			SortedSet<String> lstElements = mpAvailableGroupElements.get(sGroup);			
			for(String sHeader : lstElements) {
				Pair<Integer, Integer> where = mpCategorie.get(sGroup);
				Enumeration<TableColumn> enTC = table.getColumnModel().getColumns();
				int colCounter = 0;
				while(enTC.hasMoreElements()) {
					TableColumn tc = enTC.nextElement();					
					String header = ((String) tc.getHeaderValue()).trim();					
					if(header.equals(sHeader) && colCounter >= where.x && colCounter <= where.y) {						
						tc.setMinWidth(0);
						tc.setPreferredWidth(0);
						tc.setWidth(0);
						//break;
					}
					colCounter++;
				}
			}			
		}		
		for(String sGroup : mpSelectedGroupElements.keySet()) {
			List<String> lstElements = mpSelectedGroupElements.get(sGroup);
			Enumeration<TableColumn> enTC = header.getColumnModel().getColumns();
			while(enTC.hasMoreElements()) {
				TableColumn tc = enTC.nextElement();
				String sHeader = ((String) tc.getHeaderValue()).trim();
				if(sHeader.equals(sGroup)) {
					tc.setPreferredWidth(lstElements.size() * getColumnWidth());
					tc.setWidth(lstElements.size() * getColumnWidth());
					break;
				}				
			}
		}
		
	}
	

	private void cmdSelectGroups(final SelectGroupsController ctl) {
		ChoiceList<String> cl = new ChoiceList<String>();
		List<String> lstclctefSelected = new ArrayList<String>();

		if(this.lstSelected.size() > 0) {
			lstclctefSelected.addAll(this.lstSelected);
		}
		else {
			for(Collectable clt : model.getCategorieHeader()) {
				lstclctefSelected.add((String) clt.getValue(UID.UID_NULL));
			}		
		}
		cl.set(this.setAvailable, lstclctefSelected,  new StringComparator());

		
		ctl.setModel(cl);
		final boolean bOK = ctl.run(LocaleDelegate.getInstance().getResource("JMatrixComponent.2"));
		
		if(bOK) {
			UIUtils.runCommand(JMatrixComponent.this, new CommonRunnable() {
				@Override
		        public void run() throws CommonBusinessException {
					SortedSet<String> setAvailable = ctl.getAvailableObjects();
					List<String> lstSelected = ctl.getSelectedObjects();
					rearrangeTable(setAvailable, lstSelected);
					rearrangeTableFixed(getAvailableFieldsFixed(), getSelectedFieldsFixed());
					updatePrefs();
					hideNotViewableColumns();
				}
			});
		}
	}
	
	private void cmdSelectGroupsFixed(final SelectGroupsController ctl) {
		ChoiceList<FieldMeta<?>> cl = new ChoiceList<FieldMeta<?>>();
		List<UID> lstclctefSelected = new ArrayList<UID>();
		
		lstSelectedFixed.removeAll(setAvailableFixed);

		if(this.lstSelectedFixed.size() > 0) {
			lstclctefSelected.addAll(this.lstSelectedFixed);
		}
		else {
			for(Collectable clt : modelFixed.getCategorieHeader()) {
				lstclctefSelected.add((UID) clt.getValue(UID.UID_NULL));
			}		
		}
		lstclctefSelected.remove(UID.UID_NULL);
		
		List<FieldMeta<?>> lstTmp = new ArrayList<FieldMeta<?>>();
		for(UID uid : lstclctefSelected) {
			FieldMeta<?> fm = MetaProvider.getInstance().getEntityField(uid);
			lstTmp.add(fm);
		}
		SortedSet<FieldMeta<?>> setTmp = new TreeSet<FieldMeta<?>>();
		for(UID uid : this.setAvailableFixed) {
			FieldMeta<?> fm = MetaProvider.getInstance().getEntityField(uid);
			setTmp.add(fm);
		}
		
		cl.set(setTmp, lstTmp, new FieldMetaComparator());
				
		ctl.setModel(cl);
		final boolean bOK = ctl.run(LocaleDelegate.getInstance().getResource("JMatrixComponent.2"));
		
		if(bOK) {
			UIUtils.runCommand(JMatrixComponent.this, new CommonRunnable() {
				@Override
		        public void run() throws CommonBusinessException {
					bFireListeners = false;
					SortedSet<FieldMeta<?>> setAvailable = ctl.getAvailableObjects();
					List<FieldMeta<?>> lstSelected = ctl.getSelectedObjects();
					List<UID> lstTmp = new ArrayList<UID>();
					lstTmp.add(0, UID.UID_NULL);
					for(FieldMeta<?> fm : lstSelected) {
						lstTmp.add(fm.getUID());
					}
					SortedSet<UID> setTmp = new TreeSet<UID>();
					for(FieldMeta<?> fm : setAvailable) {
						setTmp.add(fm.getUID());
					}
					
					rearrangeTableFixed(setTmp, lstTmp);
					updatePrefs();
					hideNotViewableColumns();
					bFireListeners = true;
				}
			});
		}
	}

	private void fireChangeListener() {

		for(ChangeListener cl : lstchangelistener) {
			final ChangeEvent ev = new ChangeEvent(this);
			cl.stateChanged(ev);
		}
		
	}
	
	public void setPermissionYEntity(IPermission permissionYEntity) {
		this.permissionYEntity = permissionYEntity;
		respectRightsYEntity();
	}

	public void setPermissionMatrixEntity(IPermission permissionMatrixEntity) {
		this.permissionMatrixEntity = permissionMatrixEntity;
		respectRightsMatrixEntity();
	}
	
	protected void respectRights() {
		respectRightsYEntity();
		respectRightsMatrixEntity();
	}
	
	private void adjustToolbar(boolean enable) {
		actNew.setEnabled(enable);
		actClone.setEnabled(enable);
		actRemove.setEnabled(enable);
	}
	
	protected void respectRightsYEntity() {

		if (permissionYEntity == null || !permissionYEntity.includesWriting()) {
			tblFixed.setEnabled(false);
			headerFixed.setEnabled(false);
			adjustToolbar(false);
		}

		if (permissionYEntity == null || !permissionYEntity.includesReading()) {
			tblFixed.setVisible(false);
			headerFixed.setVisible(false);
			toolbar.setVisible(false);
		}
	}

	protected void respectRightsMatrixEntity() {

		if (permissionMatrixEntity == null || !permissionMatrixEntity.includesWriting()) {
			table.setEnabled(false);
			header.setEnabled(false);
			adjustToolbar(false);
		}

		if (permissionMatrixEntity == null || !permissionMatrixEntity.includesReading()) {
			table.setVisible(false);
			header.setVisible(false);
		}
	}
	
	private void hideNotViewableColumns() {
		prepareTable(table);
		hideGroupElements();	
		hideColumns();
		hideNullColumns();		
	}
	
	public void setParameterValueListProvider(UID uidField, CollectableField cf) throws CommonBusinessException {
		for(UID uidColumn : mpColumns.keySet()) {
			JMatrixComponent.Column column = mpColumns.get(uidColumn);
			for(RefreshValueListAction action : column.getRefreshValueListActions()) {
				if(action.getParentComponentUID().equals(uidField)) {
					String paraName = action.getParameterNameForSourceComponent();
					UID uidTargetField = action.getTargetComponentUID();
					if(column.getValueListProvider() != null) {
						CollectableFieldsProvider cfp = column.getValueListProvider();
						cfp.setParameter(paraName, cf.getValueId());					
					}
				}
			}
			
		}
	}
	
	@Override
	public Dimension getPreferredSize() {
		Dimension dim = super.getPreferredSize();
		Dimension newDim = new Dimension(dim.width, 100);
		if(header != null) {
			int height = header.getHeight();
			newDim = new Dimension(dim.width, height);
		}
		
		return newDim;				
	}

	/**
	 * @author Thomas Pasch (javadoc, deprecation)
	 * @deprecated Superseded by the {@link TablePopupMenuEvent} infrastructure,
	 * 		also see {@link Mouse2TableHeaderPopupMenuAdapter}.
	 */
	private class MatrixPopupMenuMouseAdapter extends TablePopupMenuMouseAdapter {
		
		public MatrixPopupMenuMouseAdapter(JTable table) {
			super(table);
		}

		@Override
		public boolean doPopup(MouseEvent e, JPopupMenu menu) {
			if(getPopupMenuListener() != null) {
				return getPopupMenuListener().doPopup(e, null);
			}
			return false;
		}
	}
	
	public Column getColumn(UID sColumnName) {
		return this.mpColumns.get(sColumnName);
	}
	
	public void addColumn(Column column) {
		this.mpColumns.put(column.getName(), column);
	}
	
public static class RefreshValueListAction {
		
		private final UID targetComponentUid;
		private final UID parentComponentEntityUid;
		private final UID parentComponentUid;
		private final String sParameterNameForSourceComponent;

		/**
		 * @param targetComponentUid
		 * @param parentComponentEntityUid the entity name of the subform (shared by target component and parent component)
		 *   or the entity name of the form (in which case the parent component lies in the form, outside the subform).
		 * @param parentComponentUid
		 * @param sParameterNameForSourceComponent the name of the parameter in the valuelistprovider for the source component.
		 */
		public RefreshValueListAction(UID targetComponentUid, UID parentComponentEntityUid,
				UID parentComponentUid, String sParameterNameForSourceComponent) {
			this.targetComponentUid = targetComponentUid;
			this.parentComponentEntityUid = parentComponentEntityUid;
			this.parentComponentUid = parentComponentUid;
			this.sParameterNameForSourceComponent = sParameterNameForSourceComponent;
		}

		public UID getTargetComponentUID() {
			return this.targetComponentUid;
		}

		public UID getParentComponentEntityUID() {
			return this.parentComponentEntityUid;
		}

		public UID getParentComponentUID() {
			return this.parentComponentUid;
		}

		public String getParameterNameForSourceComponent() {
			return this.sParameterNameForSourceComponent;
		}
	}	// inner class RefreshValueListAction
	
	public static class Column {
		private final UID uid;
		private String sLabel;
		private final CollectableComponentType clctcomptype;
		private final boolean bVisible;
		private final boolean bEnabled;
		private final boolean bInsertable;
		private final boolean bCloneable;
		private final Integer iRows;
		private final Integer iColumns;
		private final Integer width;
		private final Integer initialPosition;
		private final UID nextFocusField;
		private Map<String, Object> mpProperties;

		/**
		 * Collection<TransferLookedUpValueAction>
		 */
		private Collection<TransferLookedUpValueAction> collTransferLookedUpValueActions;

		/**
		 * Collection<ClearAction>
		 */
		private Collection<ClearAction> collClearActions;

		/**
		 * Collection<RefreshValueListAction>
		 */
		private Collection<RefreshValueListAction> collRefreshValueListActions;

		private CollectableFieldsProvider valuelistprovider;

		public Column(UID uid) {
			this(uid, null, new CollectableComponentType(null, null), true, true, true, false, null, null);
		}

		public Column(UID uid, String sLabel, CollectableComponentType clctcomptype, boolean bVisible, boolean bEnabled, boolean bCloneable,
				boolean bInsertable, Integer iRows, Integer iColumns) {
			this(uid, sLabel, clctcomptype, bVisible, bEnabled, bInsertable, bCloneable, iRows, iColumns, null, null);
		}

		public Column(UID uid, String sLabel, CollectableComponentType clctcomptype, boolean bVisible, boolean bEnabled, boolean bCloneable,
			boolean bInsertable, Integer iRows, Integer iColumns, Integer width, UID nextFocusField) {
			this.uid = uid;
			this.sLabel = sLabel;
			this.clctcomptype = clctcomptype;
			this.bVisible = bVisible;
			this.bEnabled = bEnabled;
			this.bInsertable = bInsertable;
			this.bCloneable = bCloneable;
			this.iRows = iRows;
			this.iColumns = iColumns;
			this.width = width;
			this.initialPosition = null;
			this.nextFocusField = nextFocusField;
		}

		/**
		 * use {@link getUID()} instead
		 * @return
		 */
		@Deprecated
		public UID getName() {
			return this.getUID();
		}
		
		public UID getUID() {
			return this.uid;
		}

		public String getLabel() {
			return this.sLabel;
		}

		public void setLabel(String label) {
			this.sLabel = label;
		}

		public CollectableComponentType getCollectableComponentType() {
			return this.clctcomptype;
		}

		public boolean isVisible() {
			return this.bVisible;
		}

		public boolean isEnabled() {
			return this.bEnabled;
		}

		public boolean isInsertable() {
			return this.bInsertable;
		}

		public boolean isCloneable() {
			return this.bCloneable;
		}

		/**
		 * @return the number of rows used in the renderer and editor
		 */
		public Integer getRows() {
			return this.iRows;
		}

		/**
		 * @return the number of columns used in the renderer and editor
		 */
		public Integer getColumns() {
			return this.iColumns;
		}

		/**
		 * Returns the preferred width of this column.
		 */
		public Integer getWidth() {
			return width;
		}

		/**
		 * Returns the nextfocuscomponent of this column.
		 */
		public UID getNextFocusField() {
			return nextFocusField;
		}

		/**
		 * Returns the initial position of this column (relative to the other columns).
		 */
		public Integer getInitialPosition() {
			return initialPosition;
		}

		@Override
		public boolean equals(Object oValue) {
			if (this == oValue) {
				return true;
			}
			if (!(oValue instanceof Column)) {
				return false;
			}
			return LangUtils.equal(getName(), ((Column) oValue).getName());
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(getName());
		}

		public Collection<TransferLookedUpValueAction> getTransferLookedUpValueActions() {
			return (collTransferLookedUpValueActions != null) ? Collections.unmodifiableCollection(collTransferLookedUpValueActions) : Collections.<TransferLookedUpValueAction>emptyList();
		}

		public void addTransferLookedUpValueAction(TransferLookedUpValueAction act) {
			if (collTransferLookedUpValueActions == null) {
				collTransferLookedUpValueActions = new LinkedList<TransferLookedUpValueAction>();
			}
			collTransferLookedUpValueActions.add(act);
		}

		public Collection<ClearAction> getClearActions() {
			return collClearActions != null ? Collections.unmodifiableCollection(collClearActions) : Collections.<ClearAction>emptyList();
		}

		public void addClearAction(ClearAction act) {
			if (collClearActions == null) {
				collClearActions = new LinkedList<ClearAction>();
			}
			collClearActions.add(act);
		}

		public Collection<RefreshValueListAction> getRefreshValueListActions() {
			return collRefreshValueListActions != null ? Collections.unmodifiableCollection(collRefreshValueListActions) : Collections.<RefreshValueListAction>emptyList();
		}

		public void addRefreshValueListAction(RefreshValueListAction act) {
			if (collRefreshValueListActions == null) {
				collRefreshValueListActions = new LinkedList<RefreshValueListAction>();
			}
			collRefreshValueListActions.add(act);
		}

		public void setValueListProvider(CollectableFieldsProvider valuelistprovider) {
			this.valuelistprovider = valuelistprovider;
		}

		public CollectableFieldsProvider getValueListProvider() {
			return this.valuelistprovider;
		}

	    public Object getProperty(String sName) {
			return getProperties().get(sName);
		}

	    public void setProperty(String sName, Object oValue) {
			synchronized (this) {
				if (mpProperties == null) {
					mpProperties = new TreeMap<String, Object>();
				}
			}
			mpProperties.put(sName, oValue);

			assert LangUtils.equal(getProperty(sName), oValue);
		}

	    public synchronized Map<String, Object> getProperties() {
			final Map<String, Object> result = (mpProperties == null) ? Collections.<String, Object>emptyMap() : Collections.unmodifiableMap(mpProperties);
			return result;
		}

	}	// inner class Column

}
