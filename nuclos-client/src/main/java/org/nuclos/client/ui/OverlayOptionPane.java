//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.font.TextAttribute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.nuclos.client.command.ResultListener;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

public class OverlayOptionPane extends JScrollPane implements IOverlayCenterComponent {
	
	/** 
     * Type meaning Look and Feel should not supply any options -- only
     * use the options from the <code>OverlayOptionPane</code>.
     */
    public static final int         DEFAULT_OPTION = JOptionPane.DEFAULT_OPTION;
    /** Type used for <code>showConfirmDialog</code>. */
    public static final int         YES_NO_OPTION = JOptionPane.YES_NO_OPTION;
    /** Type used for <code>showConfirmDialog</code>. */
    public static final int         YES_NO_CANCEL_OPTION = JOptionPane.YES_NO_CANCEL_OPTION;
    /** Type used for <code>showConfirmDialog</code>. */
	public static final int         OK_CANCEL_OPTION = JOptionPane.OK_CANCEL_OPTION;
	
	private static final int		OK_ONLY_OPTION = 21;
	
	/** Return value from class method if YES is chosen. */
    public static final int         YES_OPTION = JOptionPane.YES_OPTION;
    /** Return value from class method if NO is chosen. */
    public static final int         NO_OPTION = JOptionPane.NO_OPTION;
    /** Return value from class method if CANCEL is chosen. */
    public static final int         CANCEL_OPTION = JOptionPane.CANCEL_OPTION;
    /** Return value form class method if OK is chosen. */
    public static final int         OK_OPTION = JOptionPane.OK_OPTION;
    /** Return value from class method if user closes window without selecting
     * anything, more than likely this should be treated as either a
     * <code>CANCEL_OPTION</code> or <code>NO_OPTION</code>. */
    public static final int         CLOSED_OPTION = JOptionPane.CLOSED_OPTION;

    public static final int			INPUT_TEXTFIELD = 50;

	public static final int			INPUT_TEXTAREA = 51;

	public static final int			INPUT_OPTIONS = 60;
 
    private static final int ESC = KeyEvent.VK_ESCAPE;
    
    private static final int ENTER = KeyEvent.VK_ENTER;
    
    private final JPanel main;
    
    private final List<IOverlayFrameChangeListener> overlayFrameChangeListeners = new ArrayList<IOverlayFrameChangeListener>(1);
    
    private OvOpListener refOvOpListener;

	private OvOpInputListener refOvOpInputListener;
    
    private final boolean autoClose;
    
    private int result = CLOSED_OPTION;
    
    public static OverlayOptionPane showMessageDialog(MainFrameTab tab, Object message, String title, 
    		boolean autoClose, OvOpListener listener) {
    	return new OverlayOptionPane(tab, message, title, OK_ONLY_OPTION, null, autoClose, listener, null);
    }
    
    public static OverlayOptionPane showMessageDialog(MainFrameTab tab, Object message, String title, 
    		Icon icon, boolean autoClose, OvOpListener listener) {
    	return new OverlayOptionPane(tab, message, title, OK_ONLY_OPTION, icon, autoClose, listener, null);
    }
    
    public static OverlayOptionPane showMessageDialog(MainFrameTab tab, Object message, String title, 
    		Icon icon, boolean autoClose, boolean focusOnFirstOption, OvOpListener listener) {
    	return new OverlayOptionPane(tab, message, title, OK_ONLY_OPTION, icon, autoClose, focusOnFirstOption, -1, null, null, listener, null);
    }
    
    public static OverlayOptionPane showConfirmDialog(MainFrameTab tab, Object message, String title, int optionType, 
    		boolean autoClose, OvOpListener listener) {
    	return new OverlayOptionPane(tab, message, title, optionType, null, autoClose, listener, null);
    }
    
    public static OverlayOptionPane showConfirmDialog(MainFrameTab tab, Object message, String title, int optionType, 
    		Icon icon, boolean autoClose, OvOpListener listener) {
    	return new OverlayOptionPane(tab, message, title, optionType, icon, autoClose, listener, null);
    }
    
    public static OverlayOptionPane showConfirmDialog(MainFrameTab tab, Object message, String title, int optionType, 
    		Icon icon, boolean autoClose, boolean focusOnFirstOption, OvOpListener listener) {
    	return new OverlayOptionPane(tab, message, title, optionType, icon, autoClose, focusOnFirstOption, -1, null, null, listener, null);
    }

	public static OverlayOptionPane showInputDialog(MainFrameTab tab, Object message, String title, int optionType, int inputType, OvOpInputListener inputListener) {
		return new OverlayOptionPane(tab, message, title, optionType, null, true, false, inputType, null, null, null, inputListener);
	}

    public static OverlayOptionPane showInputDialog(MainFrameTab tab, Object message, String title, int optionType, int inputType,
			Icon icon, OvOpInputListener inputListener) {
		return new OverlayOptionPane(tab, message, title, optionType, icon, true, false, inputType, null, null, null, inputListener);
	}

	public static OverlayOptionPane showInputDialog(MainFrameTab tab, Object message, String title, int optionType, Object[] inputOptions, Object defaultOption, OvOpInputListener inputListener) {
		return new OverlayOptionPane(tab, message, title, optionType, null, true, false, INPUT_OPTIONS, inputOptions, defaultOption, null, inputListener);
	}

	public static OverlayOptionPane showInputDialog(MainFrameTab tab, Object message, String title, int optionType, Object[] inputOptions, Object defaultOption,
													Icon icon, OvOpInputListener inputListener) {
		return new OverlayOptionPane(tab, message, title, optionType, icon, true, false, INPUT_OPTIONS, inputOptions, defaultOption, null, inputListener);
	}

    private OverlayOptionPane(final MainFrameTab tab, Object message, String title, int optionType,
			Icon icon, boolean autoClose, final OvOpListener listener, final OvOpInputListener inputListener) {
    	this(tab, message, title, optionType, icon, autoClose, true, -1, null, null, listener, inputListener);
    }
    
	private OverlayOptionPane(final MainFrameTab tab, Object message, String title, int optionType,
			Icon icon, boolean autoClose, boolean focusOnFirstOption, int inputType, Object[] inputOptions, Object defaultOption, final OvOpListener listener, final OvOpInputListener inputListener) {
		super();
		this.refOvOpListener = listener;
		this.refOvOpInputListener = inputListener;
		this.autoClose = autoClose;
		
		final MainFrameTab parentTab = tab.getTopOverlayOfTab();
		
		main = new JPanel();
		setViewportView(main);
		setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		setBorder(BorderFactory.createEmptyBorder());
				
		ITableLayoutBuilder tbllay = new TableLayoutBuilder(main).columns(TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.FILL);
		tbllay.newRow();
		JLabel jlbTitle = new JLabel(title, JLabel.CENTER);
		Map<TextAttribute, Object> fontAttributes = new HashMap<TextAttribute, Object>(jlbTitle.getFont().getAttributes());
		//@see NUCLOS-1820
		Float fontSize = ((Float)fontAttributes.get(TextAttribute.SIZE));
		if (fontSize == null)
			fontSize = jlbTitle.getFont().getSize2D() <= 0 ? new Float(11f) : new Float(jlbTitle.getFont().getSize2D());
		fontAttributes.put(TextAttribute.SIZE, new Float(fontSize.intValue() + 1));
		fontAttributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
		jlbTitle.setFont(new Font(fontAttributes));
		jlbTitle.setBorder(BorderFactory.createEmptyBorder(20, 40, 5, 40));
		jlbTitle.setOpaque(true);
		jlbTitle.setForeground(Color.WHITE);
		jlbTitle.setBackground(NuclosThemeSettings.BACKGROUND_ROOTPANE);
		tbllay.addFullSpan(jlbTitle);
		tbllay.newRow();
		
		JPanel jpnOptions = createOptions(optionType, focusOnFirstOption);
		
		if (icon != null) {
			final JPanel labHolder = new JPanel(new TableLayout(new double[] {TableLayout.PREFERRED}, new double[] {TableLayout.FILL}));
			labHolder.add(new JLabel(icon), new TableLayoutConstraints(0, 0, 0, 0, TableLayout.LEFT, TableLayout.CENTER));
			labHolder.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 0));
			tbllay.add(labHolder);
		}
		
		if (message instanceof Component) {
			tbllay.addFullSpan((Component)message);
		} else {
			final JLabel label = new LineBreakLabel(message==null?"":message.toString(), Math.max(360, jpnOptions.getPreferredSize().width + 60));
			JPopupMenu context = new JPopupMenu();
			context.add(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage("OverlayOptionPane.copy","Kopieren"), Icons.getInstance().getIconCopy16()) {
				@Override
				public void actionPerformed(ActionEvent e) {
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(label.getText()), null);
				}
			});
			label.setComponentPopupMenu(context);
			final JPanel labHolder = new JPanel();
			labHolder.add(label);
			labHolder.setBorder(BorderFactory.createEmptyBorder(20, icon==null?20:10, 10, 20));
			tbllay.addFullSpan(labHolder);
		}

		JComponent jInputComponent = null;
		double dInputHeight = TableLayout.PREFERRED;
		if (inputType == INPUT_TEXTFIELD) {
			jtfInput = new JTextField();
			jInputComponent = jtfInput;
			setFocusLater(jtfInput);
		} else if (inputType == INPUT_TEXTAREA) {
			jtaInput = new JTextArea();
			jInputComponent = new JScrollPane(jtaInput);
			dInputHeight = 100;
			setFocusLater(jtaInput);
		} else if (inputType == INPUT_OPTIONS) {
			jcbInput = new JComboBox(inputOptions);
			if (defaultOption != null) {
				jcbInput.setSelectedItem(defaultOption);
			}
			jInputComponent = jcbInput;
			setFocusLater(jcbInput);
		}
		if (jInputComponent != null) {
			final JPanel jpnHolder = new JPanel();
			TableLayoutBuilder tbllayHolder = new TableLayoutBuilder(jpnHolder).columns(TableLayout.FILL);
			tbllayHolder.newRow(dInputHeight).addFullSpan(jInputComponent);
			jpnHolder.setBorder(BorderFactory.createEmptyBorder(0, 20, 10, 20));
			tbllay.newRow().addFullSpan(jpnHolder);
		}

		tbllay.newRow().addFullSpan(jpnOptions);
		parentTab.add(this);
	}
	
	JButton jbtnOK, jbtnYES, jbtnNO, jbtnCANCEL;

    JTextField jtfInput;

    JTextArea jtaInput;

    JComboBox jcbInput;
	
	private JPanel createOptions(int optionType, boolean focusOnFirstOption) {
		
		switch (optionType) {
		case OK_ONLY_OPTION: {
			jbtnOK = createButton("OverlayOptionPane.OK", OK_OPTION);
			
			return createPanel(focusOnFirstOption?0:-1, jbtnOK); }
		case YES_NO_OPTION: {
			jbtnYES = createButton("OverlayOptionPane.Yes", YES_OPTION);
			jbtnNO = createButton("OverlayOptionPane.No", NO_OPTION);
			
			return createPanel(focusOnFirstOption?0:-1, jbtnYES, jbtnNO); }
		case YES_NO_CANCEL_OPTION: {
			jbtnYES = createButton("OverlayOptionPane.Yes", YES_OPTION);
			jbtnNO = createButton("OverlayOptionPane.No", NO_OPTION);
			jbtnCANCEL = createButton("OverlayOptionPane.Cancel", CANCEL_OPTION);
			
			return createPanel(focusOnFirstOption?0:-1, jbtnYES, jbtnNO, jbtnCANCEL); }
		default: {
			jbtnOK = createButton("OverlayOptionPane.OK", OK_OPTION);
			jbtnCANCEL = createButton("OverlayOptionPane.Cancel", CANCEL_OPTION);
			
			return createPanel(focusOnFirstOption?0:-1, jbtnOK, jbtnCANCEL);}
		}
	}
	
	private OverlayOptionPaneButton createButton(final String resource, final int result) {
		OverlayOptionPaneButton oopb = new OverlayOptionPaneButton(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage(resource,resource)) {
			@Override
			public void actionPerformed(ActionEvent e) {
				OverlayOptionPane.this.result = result;
				if (autoClose) {
					close();
				}
			}
		});
		return oopb;
	}
	
	private JPanel createPanel(final int focusOwner, final Component...components) {
		JPanel result = new JPanel(new FlowLayout());
		int maxWidth = 0;
		for (Component c : components) {
			result.add(c);
			maxWidth = Math.max(c.getPreferredSize().width, maxWidth);
		}
		
		for (Component c : components) {
			Dimension prefSize = c.getPreferredSize();
			prefSize.width = maxWidth;
			c.setPreferredSize(prefSize);
		}
		
		result.setFocusTraversalPolicy(new OverlayOptionPaneFocusTraversalPolicy(components));
		result.setFocusCycleRoot(true);
		
		if (focusOwner >= 0) {
			setFocusLater(components[focusOwner]);
		}
		return result;
	}

	private void setFocusLater(Component c) {
		if (c == null) {
			return;
		}
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// Must be invoked later, else focus is not set with compound components like LOVs
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						c.requestFocusInWindow();
					}
				});
			}
		});
	}
	
	private void close() {
		for (IOverlayFrameChangeListener changeListener : new ArrayList<IOverlayFrameChangeListener>(overlayFrameChangeListeners)) {
			changeListener.closeOverlay();
		}
	}

	@Override
	public Dimension getCenterSize() {
		final int dpi=java.awt.Toolkit.getDefaultToolkit().getScreenResolution(); 
		final Dimension size = getPreferredSize();
		size.setSize(size.getWidth()+(10*dpi/DPI),size.getHeight()+(10*dpi/DPI)); //@see NUCLOS-1408
		return size;
	}
	
	@Override
	public boolean isClosable() {
		return true;
	}

	@Override
	public void notifyClosing(ResultListener<Boolean> rl) {
		rl.done(isClosable());
		if (isClosable() && refOvOpListener != null) {
			final OvOpListener l = refOvOpListener;
			if (l != null) {
				l.done(result);
			}
		}
		if (isClosable() && refOvOpInputListener != null) {
			final OvOpInputListener l = refOvOpInputListener;
			if (l != null) {
				l.done(result, getInput());
			}
		}
	}

	@Override
	public void transferSize(Dimension size) {
	}

	@Override
	public void addOverlayFrameChangeListener(
			IOverlayFrameChangeListener changeListener) {
		overlayFrameChangeListeners.add(changeListener);
	}


	@Override
	public void removeOverlayFrameChangeListener(
			IOverlayFrameChangeListener changeListener) {
		overlayFrameChangeListeners.remove(changeListener);
	}
	
	private class OverlayOptionPaneButton extends JButton {
		
		OverlayOptionPaneButton(Action act) {
			super(act);
		}

		@Override
		public boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
			if (e.getKeyCode() == ENTER) {
				if (!pressed) {
					doClick();
					return true;
				}
			} else if (e.getKeyCode() == ESC) {
				if (!pressed) {
					if (autoClose) {
						close();
					}
					if (refOvOpListener != null) {
						final OvOpListener l = refOvOpListener;
						if (l != null) {
							l.done(CLOSED_OPTION);
						}
					}
					if (refOvOpInputListener != null) {
						final OvOpInputListener l = refOvOpInputListener;
						if (l != null) {
							l.done(CLOSED_OPTION, getInput());
						}
					}
					return true;
				}
			}
			return false;
		}	
	}
	
	private static class OverlayOptionPaneFocusTraversalPolicy extends FocusTraversalPolicy {
		Vector<Component> order;

		public OverlayOptionPaneFocusTraversalPolicy(Component[] order) {
			this.order = new Vector<Component>(order.length);
			for (Component c : order) {
				this.order.add(c);
			}
		}

		public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
			int idx = (order.indexOf(aComponent) + 1) % order.size();
			return order.get(idx);
		}

		public Component getComponentBefore(Container focusCycleRoot,
				Component aComponent) {
			int idx = order.indexOf(aComponent) - 1;
			if (idx < 0) {
				idx = order.size() - 1;
			}
			return order.get(idx);
		}

		public Component getDefaultComponent(Container focusCycleRoot) {
			return order.get(0);
		}

		public Component getLastComponent(Container focusCycleRoot) {
			return order.lastElement();
		}

		public Component getFirstComponent(Container focusCycleRoot) {
			return order.get(0);
		}
	}

	public final JButton getJButtonOK() {
		return jbtnOK;
	}

	public final JButton getJButtonYES() {
		return jbtnYES;
	}

	public final JButton getJButtonNO() {
		return jbtnNO;
	}

	public final JButton getJButtonCANCEL() {
		return jbtnCANCEL;
	}

	public final Object getInput() {
		if (jtfInput != null) {
			return jtfInput.getText();
		}
		if (jtaInput != null) {
			return jtaInput.getText();
		}
		if (jcbInput != null) {
			return jcbInput.getSelectedItem();
		}
		return null;
	}
	
}
