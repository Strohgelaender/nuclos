//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.GenericObjectClientUtils;
import org.nuclos.client.searchfilter.SearchFilter;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.result.ResultPanelFilter;
import org.nuclos.client.ui.collect.component.AbstractCollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.collect.strategy.AlwaysLoadCompleteCollectablesStrategy;
import org.nuclos.client.ui.collect.strategy.CompleteCollectablesStrategy;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

public class CollectSearchStrategy<PK,Clct extends Collectable<PK>> implements ISearchStrategy<PK,Clct> {

	private static final Logger LOG = Logger.getLogger(CollectSearchStrategy.class);

	/**
	 * Valuelist provider datasource for additional search condition
	 */
	private DatasourceVO valueListProviderDatasource;
	private Map<String, Object> valueListProviderDatasourceParameter;

	private CollectController<PK,Clct> cc;

	private CompleteCollectablesStrategy<PK,Clct> completecollectablesstrategy;

	private CollectableIdListCondition idListCondition;
	
	private UID mandator;

	public CollectSearchStrategy() {
	}

	public final void setCollectController(CollectController<PK,Clct> cc) {
		this.cc = cc;
		this.completecollectablesstrategy = new AlwaysLoadCompleteCollectablesStrategy<PK,Clct>(cc);
	}

	protected final CollectController<PK,Clct> getCollectController() {
		return cc;
	}

	/**
	 * performs a single-threaded search, according to the current search condition, if any.
	 * TODO replace with search(boolean bRefreshOnly)
	 * @see #getSearchWorker()
	 *
	 * @deprecated Use multithreaded search for new applications.
	 */
	@Deprecated
	public void search() throws CommonBusinessException {
		// leave implementation for derived class
		throw new UnsupportedOperationException("search");
	}

	@Override
	@Deprecated
	public void search(boolean bRefreshOnly) throws CommonBusinessException {
		this.search();
	}

	@Override
	public SearchWorker<PK,Clct> getSearchWorker() {
		// leave implementation for derived class
		return null;
	}

	@Override
	public SearchWorker<PK,Clct> getSearchWorker(List<Observer> lstObservers) {
		// leave implementation for derived class
		return null;
	}

	/**
	 * @return Valuelist provider datasource for additional search condition
	 */
	@Override
	public final DatasourceVO getValueListProviderDatasource() {
		return valueListProviderDatasource;
	}

	@Override
	public final Map<String, Object> getValueListProviderDatasourceParameter() {
		return valueListProviderDatasourceParameter;
	}

	/**
	 * TODO: Can we get rid of this?
	 */
	@Override
	public final void setValueListProviderDatasource(DatasourceVO valueListProviderDatasource) {
		this.valueListProviderDatasource = valueListProviderDatasource;
	}

	/**
	 * TODO: Can we get rid of this?
	 */
	@Override
	public final void setValueListProviderDatasourceParameter(Map<String, Object> valueListProviderDatasourceParameter) {
		this.valueListProviderDatasourceParameter = valueListProviderDatasourceParameter;
	}

	/**
	 * §postcondition result == null || result.isSyntacticallyCorrect()
	 * 
	 * @return the search condition to be used for a search. This method returns <code>getCollectableSearchConditionFromSearchPanel()</code>.
	 *         This condition can be refined by subclasses, by ANDing, ORing or whatever. May be <code>null</code>.
	 */
	@Override
	public CollectableSearchCondition getCollectableSearchCondition() throws CollectableFieldFormatException {
		ResultPanel<PK, Clct> pnlResult = cc.getResultPanel();
		CollectableSearchCondition clctsc = cc.getCollectableSearchConditionFromSearchPanel(false);
		CompositeCollectableSearchCondition rootCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		if (clctsc != null) {
			rootCondition.addOperand(clctsc);
		}
		if (pnlResult != null) {
			ResultPanelFilter resultFilter = pnlResult.getResultFilter();
			if (resultFilter.isFilteringActive()) {
				Map<UID, CollectableComponent> filterComponents = resultFilter.getAllFilterComponents();
				if (filterComponents != null) {
					for (CollectableComponent clctComponent : filterComponents.values()) {
						if (clctComponent instanceof AbstractCollectableComponent) {
							AbstractCollectableComponent component = (AbstractCollectableComponent) clctComponent;
							CollectableSearchCondition compSc = component.getSearchCondition();
							if (compSc != null) {
								rootCondition.addOperand(compSc);
							}
						}
					}
				}
			}
		}
		return SearchConditionUtils.simplified(addSearchFilter(rootCondition));
	}

	/**
	 * @return Set&lt;UID&gt; the names of the required fields for the search result. It is merged with the names of the fields (columns)
	 * that the user has selected to be displayed, so these don't need to be given here.
	 */
	protected final Set<UID> getRequiredFieldUidsForResult() {
		return this.getCompleteCollectablesStrategy().getRequiredFieldUidsForResult();
	}

	/**
	 * @param clct
	 * @return Is the given Collectable complete? That is: Does it contain all data necessary for display in the Details panel?
	 */
	@Override
	public final boolean isCollectableComplete(Clct clct) {
		return this.getCompleteCollectablesStrategy().isComplete(clct);
	}

	/**
	 * @return the strategy used by this CollectController to complete <code>Collectable</code>s when necessary.
	 */
	@Override
	public final CompleteCollectablesStrategy<PK,Clct> getCompleteCollectablesStrategy() {
		return this.completecollectablesstrategy;
	}

	/**
	 * @param strategy
	 */
	@Override
	public final void setCompleteCollectablesStrategy(CompleteCollectablesStrategy<PK,Clct> strategy) {
		this.completecollectablesstrategy = strategy;
	}

	/**
	 * @return Are <code>Collectable</code>s in the ResultTable always complete in this <code>CollectController</code>?
	 *
	 * @deprecated Use #getCompleteCollectablesStrategy().getCollectablesInResultAreAlwaysComplete() directly.
	 */
	@Override
	public final boolean getCollectablesInResultAreAlwaysComplete() {
		return getCompleteCollectablesStrategy().getCollectablesInResultAreAlwaysComplete();
	}

	/*
	 * The following methods have been defined only for a subset of strategies.
	 */

	@Override
	public ProxyList<PK,Clct> getCollectableProxyList() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setCollectableProxyList(ProxyList<PK,Clct> list) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ProxyList<PK,Clct> getSearchResult() throws CollectableFieldFormatException {
		throw new UnsupportedOperationException();
	}

	public CollectableIdListCondition getCollectableIdListCondition() {
		return idListCondition;
	}

	@Override
	public void setCollectableIdListCondition(CollectableIdListCondition condition) {
		this.idListCondition = condition;
	}
	
	public CollectableSearchCondition addSearchFilter(CollectableSearchCondition cond) {
		if (!cc.isSearchPanelAvailable()) {
			// No search panel? -> Add selected search filter to condition. refs NOAINT-452
			SearchFilter filter = cc.getSelectedSearchFilter();
			if (filter != null) {
				if (cond == null) {
					cond = filter.getSearchCondition();
				} else {
					cond = SearchConditionUtils.and(cond, filter.getSearchCondition());
				}
			}
		}
		if (cond == null) {
			if (cc.getMainFilter() != null) {
				return cc.getMainFilter().getSearchCondition();
			} else {
				return null;
			}
		} else {
			if (cc.getMainFilter() == null || cc.getMainFilter().getSearchCondition() == null) {
				return cond;
			} else {
				return SearchConditionUtils.and(cond, cc.getMainFilter().getSearchCondition());
			}
		}
	}
	
	@Override
	public Collection<FieldMeta<?>> getFields() {
		return null;
	}

	@Override
	public void setMandator(UID mandator) {
		this.mandator = mandator;
	}
	
	public UID getMandator() {
		return this.mandator;
	}
	
	protected List<CollectableEntityField> getCollectableFields() {
		final CollectController<PK, Clct> cc = getCollectController();
		UID entityUid = cc.getEntityUid();
		EntityMeta<?> entityMeta = MetaProvider.getInstance().getEntity(entityUid);
		final List<CollectableEntityField> cefs = new ArrayList<CollectableEntityField>(cc.getResultController().getFields().getSelectedFields());
		// add needed fields.
		if (entityMeta.getRowColorScript() != null) {
			for (CollectableEntityField cef : cc.getResultController().getFields().getAvailableFields()) {
				if (!LangUtils.equal(entityUid, cef.getEntityUID()))
					continue;	
				final FieldMeta<?> cefmeta = MetaProvider.getInstance().getEntityField(cef.getUID());
				if (entityMeta.getRowColorScript().getSource().indexOf("." + cefmeta.getFieldName()) != -1)
					cefs.add(cef);
			}
		}
		final List<CollectableEntityField> cefsToBeAdded = new ArrayList<CollectableEntityField>();

		Iterator<CollectableEntityField> fieldIterator = cefs.iterator();
		while (fieldIterator.hasNext()) {
			CollectableEntityField clctef = fieldIterator.next();
			if (!LangUtils.equal(entityUid, clctef.getEntityUID()))
					continue;

			//TODO is a RuntimeException really neccessary?
			FieldMeta<?> efmeta;
			try {
				efmeta = MetaProvider.getInstance().getEntityField(clctef.getUID());
			} catch (CommonFatalException cfe) {
				efmeta = null;
				fieldIterator.remove();
			}
			if (efmeta != null && efmeta.getBackgroundColorScript() != null) {
				for (CollectableEntityField cef : cc.getResultController().getFields().getAvailableFields()) {
					final FieldMeta<?> cefmeta = MetaProvider.getInstance().getEntityField(cef.getUID());
					if (efmeta.getBackgroundColorScript().getSource().indexOf("." + cefmeta.getFieldName()) != -1)
						cefsToBeAdded.add(cef);
				}
			}
		}
		cefs.addAll(cefsToBeAdded);
		return cefs;
	}
	
	protected final CollectableSearchCondition getInternalSearchCondition() throws CollectableFieldFormatException {
		if (getCollectableIdListCondition() != null) {
			return getCollectableIdListCondition();
		}
		final CompositeCollectableSearchCondition compositecond = new CompositeCollectableSearchCondition(LogicalOperator.AND);

		final CollectableSearchCondition clctcond = GenericObjectClientUtils.getInternalSearchCondition(getCollectController().getEntityUid(), getCollectableSearchCondition());
		if (clctcond != null)
			compositecond.addOperand(clctcond);

		return SearchConditionUtils.simplified(compositecond);
	}
	
	public CollectableSearchExpression getInternalSearchExpression() throws CollectableFieldFormatException {
		final CollectController cc = getCollectController();
		final CollectableSearchExpression clctSearchExpression 
			= new CollectableSearchExpression(getInternalSearchCondition(), 
				cc.getResultController().getCollectableSortingSequence());
		return clctSearchExpression;
	}
	
	@Override
	public CollectableSearchExpression getSearchExpressionForExport() throws CollectableFieldFormatException {
		final CollectableSearchExpression clctexprInternal = getInternalSearchExpression();
		clctexprInternal.setMandator(getMandator());
		clctexprInternal.setValueListProviderDatasource(getValueListProviderDatasource());
		clctexprInternal.setValueListProviderDatasourceParameter(getValueListProviderDatasourceParameter());	
		
		final List<CollectableEntityField> cefs = getCollectableFields();
		final CollectController cc = getCollectController();
		
		String isp = cc.getResultPanel().fetchIncrSearchPattern();
		ClientSearchUtils.addTextSearchToSearchExpression(isp, cefs, clctexprInternal, cc.getEntityUid());
		return clctexprInternal;
	}
	
}
