package org.nuclos.client.ui.labeled;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.JTextComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.command.OvOpAdapter;
import org.nuclos.client.common.NuclosCollectableLocalizedTextArea;
import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.client.ui.ColorProvider;
import org.nuclos.client.ui.CommonJScrollPane;
import org.nuclos.client.ui.DocumentFactory;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.LayoutNavigationCollectable;
import org.nuclos.client.ui.LayoutNavigationProcessor;
import org.nuclos.client.ui.OverlayOptionPane;
import org.nuclos.client.ui.ToolTipTextProvider;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.DefaultLayoutNavigationSupportContext;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.LayoutNavigationSupport.ExecutionPoint;
import org.nuclos.client.ui.collect.component.TextModuleSupport;
import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;

public class LabeledTextAreaWithButton extends LabeledTextComponent implements TextModuleSupport {

	public class InnerTextArea extends JTextArea implements LayoutNavigationProcessor {

		private final ILabeledComponentSupport support;
		
		private LayoutNavigationCollectable lnc;
		
		public InnerTextArea(ILabeledComponentSupport support) {
			if (support == null) {
				throw new NullPointerException();
			}
			this.support = support;
		
		}
		
		public boolean isButtonsDisabled() {
			return !isEnabled();
		}
		
		public void setButtonsDisabled() {
			setEnabled(false);
		}
		public ILabeledComponentSupport getLabeledComponentSupport() {
			return support;
		}

		@Override
		public String getToolTipText(MouseEvent ev) {
			final ToolTipTextProvider provider = support.getToolTipTextProvider();
			return StringUtils.concatHtml(false, provider != null ? provider.getDynamicToolTipText() : super.getToolTipText(ev),
					support.getValidationToolTip());
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			Graphics2D g2d = (Graphics2D) g;
			
			for (Icon iconButton : lstIconButtons) {
				final int index = lstIconButtons.indexOf(iconButton);
				final Rectangle r = getIconRectangle(index);
				
				if (!mpBS.containsKey(iconButton)) {
					mpBS.put(iconButton, ButtonState.NORMAL);
				}
				
				
				final Color bgColor;
				if (!isOpaque() && !isEditable() && Color.WHITE.equals(getBackground())) {
					bgColor = NuclosThemeSettings.BACKGROUND_PANEL;
				} else {
					bgColor = getBackground();
				}
				if (fadeLeft()) {
					g2d.setPaint(new GradientPaint(new Point(r.x-fadeWidthLeft, r.y), new Color(bgColor.getRed(), bgColor.getGreen(), bgColor.getBlue(), 0), new Point(r.x-1, r.y), bgColor));
					g2d.fillRect(r.x-fadeWidthLeft, r.y, fadeWidthLeft, r.height);
				}
				g2d.setColor(bgColor);
				g2d.fillRect(r.x, r.y, r.width, r.height);
			
				g2d.drawImage(mpBS.get(iconButton).getImageIcon().getImage(), r.x, r.y, null);
			
				int w = iconButton.getIconWidth();
				int h = iconButton.getIconHeight();
				BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
				Graphics imgg = bi.getGraphics();
				iconButton.paintIcon(this, imgg, 0, 0);
				imgg.dispose();

				float[] scales = { 1f, 1f, 1f, isButtonEnabled(index) ? 1f : 0.5f };
				float[] offsets = new float[4];
				RescaleOp rop = new RescaleOp(scales, offsets, null);
				g2d.drawImage(bi, rop, r.x, r.y);
				
			}
			
		}
		
		@Override
		public Color getBackground() {
			// support could be null on construction (tp)
			final Color colorDefault = super.getBackground();
			if (support == null) {
				return colorDefault;
			}
			final ColorProvider colorproviderBackground = support.getColorProvider();
			if (colorproviderBackground == null) {
				return colorDefault;
			}
			return colorproviderBackground.getColor(colorDefault);
		}

//		NUCLOS-6800
//		@Override
//		public void paste() {
//			if (this.isEditable()) {
//				Clipboard clipboard = getToolkit().getSystemClipboard();
//				try {
//					// The MacOS MRJ doesn't convert \r to \n,
//					// so do it here
//					String selection = ((String) clipboard.getContents(this).getTransferData(DataFlavor.stringFlavor)).replace('\r', '\n');
//					if (selection.endsWith("\n")) {
//						selection = selection.substring(0, selection.length()-1);
//					}
//					//NUCLEUSINT-1139
//					replaceSelection(selection.trim()); // trim selection. @see NUCLOS-1112
//				}
//				catch (Exception e) {
//					getToolkit().beep();
//					LOG.warn("Clipboard does not contain a string: " + e, e);
//				}
//			}
//		}
		
		@Override
		protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e, final int condition, final boolean pressed) {
			boolean processed = false;
			if (null != lnc) {
				final LayoutNavigationSupport lns = lnc.getLayoutNavigationSupport();
				if (lns != null) {
					final DefaultLayoutNavigationSupportContext ctx = new DefaultLayoutNavigationSupportContext(pressed, ks, e, condition, this, lnc);
					processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.BEFORE);
					if (!processed) {
						processed = super.processKeyBinding(ks, e, condition, pressed);
						//if (!processed) {
							ctx.setProcessed(processed);
							processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.AFTER);
						//}
					}
				} else {
					processed = super.processKeyBinding(ks, e, condition, pressed);					
				}
			} else {
				processed = super.processKeyBinding(ks, e, condition, pressed);
			}
			return processed;
		}
		
		@Override
		public void setLayoutNavigationCollectable(LayoutNavigationCollectable lnc) {
			this.lnc = lnc;
		}

		public void setButtonVisibility(boolean value) {
			setVisible(value);
		}
		
	}
	
	public static class TextareaScrollPane extends CommonJScrollPane {

		public TextareaScrollPane(JTextArea ta, int verticalScrollbarAsNeeded, int horizontalScrollbarNever) {
			super(ta, verticalScrollbarAsNeeded, horizontalScrollbarNever);
		}

		@Override
		public boolean hasFocus() {
			final Component view = getView();
			if (view == null) {
				return super.hasFocus();
			}
			return view.hasFocus();
		}
		
		@Override
		public boolean requestFocusInWindow() {
			final Component view = getView();
			if (view == null) {
				return super.requestFocusInWindow();
			}
			return view.requestFocusInWindow();
		}
		
		@Override
		public Font getFont() {
			final Component view = getView();
			if (view == null) {
				return super.getFont();
			}
			return view.getFont();
		}
		
		@Override
		public void setFont(Font font) {
			final Component view = getView();
			if (view == null) {
				super.setFont(font);
			} else {
				view.setFont(font);
			}
		}
		
	}
	
	private static final Logger LOG = Logger.getLogger(LabeledTextArea.class);

	private final InnerTextArea ta;

	private final JScrollPane scrlpn;
	
	private TextModuleSettings tms;
	
	private LayoutNavigationCollectable lnc;
	
	private final List<Icon> lstIconButtons;
	
	protected static final Cursor curIcon = new Cursor(Cursor.DEFAULT_CURSOR);
	protected static final Cursor curNotEditable = curIcon;
	protected final Cursor curDefault;
	private final Map<Icon, ButtonState> mpBS;
	private final int fadeWidthLeft = 7;
	private CollectableEntityField entityField;
	private CollectableEntity entity;
	private NuclosCollectableLocalizedTextArea localizedTextArea;

	public LabeledTextAreaWithButton(LabeledComponentSupport support, boolean isNullable, Class<?> javaClass, 
			String inputFormat, boolean bSearchable, CollectableEntityField entityField) {
		
		super(support, isNullable, javaClass, inputFormat, bSearchable);
		InnerTextArea inner = new InnerTextArea(support);
		scrlpn = new TextareaScrollPane(inner, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.ta = inner;
		
		this.mpBS = new HashMap<Icon, LabeledTextAreaWithButton.ButtonState>();
		this.mpBS.put(Icons.getInstance().getDataLanguageIcon(), ButtonState.NORMAL);
		lstIconButtons = new ArrayList<Icon>();
		lstIconButtons.add(Icons.getInstance().getDataLanguageIcon());
		
		this.entityField = entityField;
		this.entity = this.entityField.getCollectableEntity();
		
		this.curDefault = ta.getCursor();
		
		initValidation(isNullable, javaClass, inputFormat);
		if(this.validationLayer != null){
			this.addControl(this.validationLayer);
		} else {
			this.addControl(this.scrlpn);
		}
		this.getJLabel().setLabelFor(this.ta);

		// always enable wrapping:
		this.ta.setLineWrap(true);
		this.ta.setWrapStyleWord(true);
		
		this.ta.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent me) {
				boolean bCurIconSet = false;
				
				for (Icon icon : mpBS.keySet()) {
					int index = lstIconButtons.indexOf(icon);
					
					if (isButtonEnabled(index) && isMouseOverButton(me, index)) {
						LabeledTextAreaWithButton.this.ta.setCursor(curIcon);
						bCurIconSet = true;
						
						if (mpBS.get(icon) != ButtonState.PRESSED) {
							mpBS.put(icon, ButtonState.HOVER);
							LabeledTextAreaWithButton.this.repaint(getIconRectangleWithFades(index));							
						}
					} else {
						mpBS.put(icon, ButtonState.NORMAL);
						LabeledTextAreaWithButton.this.repaint(getIconRectangleWithFades(index));
						
					}
				}
				if (!bCurIconSet) {
					LabeledTextAreaWithButton.this.ta.setCursor(getDefaultCursor(me));

				}
			}
		});
		this.ta.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent me) {
				for (Icon icon : mpBS.keySet()) {
					int index = lstIconButtons.indexOf(icon);
					
					mpBS.put(icon, ButtonState.NORMAL);
					LabeledTextAreaWithButton.this.repaint(getIconRectangleWithFades(index));
					
				}
			}
			@Override
			public void mouseReleased(MouseEvent me) {
				for (Icon icon : mpBS.keySet()) {
					int index = lstIconButtons.indexOf(icon);
					
					if (isButtonEnabled(index) && isMouseOverButton(me, index)) {
						mpBS.put(icon, ButtonState.NORMAL);
						LabeledTextAreaWithButton.this.repaint(getIconRectangleWithFades(index));
						buttonClicked(me, index);
						
					}
				}
			}
			@Override
			public void mousePressed(MouseEvent me) {
				boolean bTextClicked = true;
				for (Icon icon : mpBS.keySet()) {
					int index = lstIconButtons.indexOf(icon);
					
					if (isButtonEnabled(index) && isMouseOverButton(me, index)) {
						bTextClicked = false;
						mpBS.put(icon, ButtonState.NORMAL);
						LabeledTextAreaWithButton.this.repaint(getIconRectangleWithFades(index));
						buttonClicked(me, index);
						
					}
				}
			}
		});
		this.ta.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {}
			@Override
			public void focusGained(FocusEvent e) {}
		});
	
	}
	
	
	public void setButtonVisibility(boolean value) {
		ta.setButtonVisibility(value);
	}
	
	public NuclosCollectableLocalizedTextArea getLocalizedTextArea() {
		return localizedTextArea;
	}


	public void setLocalizedTextArea(
			NuclosCollectableLocalizedTextArea localizedTextArea) {
		this.localizedTextArea = localizedTextArea;
	}

	public boolean isButtonsDisabled() {
		return ta.isButtonsDisabled();
	}
	
	private boolean isSelectingText() {
		return this.ta.getSelectionStart() != this.ta.getSelectionEnd();
	}
	
	private boolean isMouseOverButton(MouseEvent me, int index) {
		if (isSelectingText()) {
			return false; // hide button when selecting text...			
		}
		
		final Rectangle r = getIconRectangle(index);
		if (r.x <= me.getX() && me.getX() <= (r.x+r.width) && 
			r.y <= me.getY() && me.getY() <= (r.y+r.height)	) {
			return true;
		} else {
			return false;
		}
	}
	protected Cursor getDefaultCursor(MouseEvent me) {
		return LabeledTextAreaWithButton.this.ta.isEditable() ? curDefault : curNotEditable;
	}
	
	public void setFormat(CollectableEntityField clctef) {
		final AbstractDocument docNew = DocumentFactory.getDocument(clctef);
		if(docNew != null) {
			final AbstractDocument docOld = (AbstractDocument)ta.getDocument();
			
			ta.setDocument(docNew); // set document before adding rest of listeners.
			for(DocumentListener dl : docOld.getDocumentListeners()) {
				docNew.addDocumentListener(dl);
			}
		}
	}

	@Override
	protected JComponent getLayeredComponent(){
		return this.ta;
	}

	@Override
	protected JTextComponent getLayeredTextComponent(){
		return this.ta;
	}
	
	public JScrollPane getJScrollPane() {
		return this.scrlpn;
	}

	public JTextArea getJTextArea() {
		return this.ta;
	}

	private Rectangle getIconRectangleWithFades(int index) {
		final Rectangle r = getIconRectangle(index );
		
		if (fadeLeft()) {
			r.x = r.x - fadeWidthLeft;
			r.width = r.width + fadeWidthLeft;
		}
		
		return r;
	}
	
	private Rectangle getIconRectangle(int index) {
		return getIconRectangle(LabeledTextAreaWithButton.this.ta.getSize(), index);
	}
	
	public static Rectangle getIconRectangle(Dimension dimTextField, int index) {
		final Rectangle r = new Rectangle();
		
		final ImageIcon ico = ButtonState.NORMAL.getImageIcon();
		r.x = dimTextField.width - ico.getIconWidth();
		r.y = 0;
		r.width = ico.getIconWidth();
		r.height = ico.getIconHeight();
		
		r.x -= index*r.width;
		
		return r;
	}
	
	protected boolean fadeLeft() {
		return true;
	}
	
	/**
	 * @return the text area
	 */
	@Override
	public JTextComponent getJTextComponent() {
		return this.ta;
	}

	@Override
	public JComponent getControlComponent() {
		return this.getJScrollPane();
	}
	
	@Override
	public boolean hasFocus() {
		return this.ta.hasFocus();
	}

	/**@Override
	 * sets the static tooltip text for the label and the control component (not for the panel itself).
	 * The static tooltip is shown in the control component only if no tooltiptextprovider was set for the control.
	 * 
	 * §postcondition LangUtils.equals(this.getToolTipText(), sToolTipText)
	 * 
	 * @param sToolTipText
	 */
	@Override
	public void setToolTipText(String sToolTipText) {
		this.getJLabel().setToolTipText(sToolTipText);
		this.ta.setToolTipText(sToolTipText);

		assert LangUtils.equal(this.getToolTipText(), sToolTipText);
	}

	@Override
	protected void setToolTipTextProviderForControl(ToolTipTextProvider tooltiptextprovider) {
		super.setToolTipTextProviderForControl(tooltiptextprovider);
		if (tooltiptextprovider != null) {
			// This is necessary to enable dynamic tooltips for the text area:
			ToolTipManager.sharedInstance().registerComponent(this.getJTextArea());
		}
	}

	@Override
	protected GridBagConstraints getGridBagConstraintsForLabel() {
		final GridBagConstraints result = (GridBagConstraints) super.getGridBagConstraintsForLabel().clone();

		result.anchor = GridBagConstraints.NORTHWEST;

		return result;
	}

	@Override
	protected GridBagConstraints getGridBagConstraintsForControl(boolean bFill) {
		final GridBagConstraints result = (GridBagConstraints) super.getGridBagConstraintsForControl(bFill).clone();

		// always fill vertically:
		switch (result.fill) {
			case GridBagConstraints.NONE:
				result.fill = GridBagConstraints.VERTICAL;
				break;
			case GridBagConstraints.HORIZONTAL:
				result.fill = GridBagConstraints.BOTH;
				break;
			case GridBagConstraints.BOTH:
				result.fill = GridBagConstraints.BOTH;
				break;
			default:
				assert false;
		}

		result.weighty = 1.0;

		// no top/bottom insets:
		result.insets.top = 0;
		result.insets.bottom = 0;

		return result;
	}

	/**
	 * sets the number of columns of the textarea
	 * @param iColumns
	 */
	@Override
	public void setColumns(int iColumns) {
		this.getJTextArea().setColumns(iColumns);
	}

	@Override
	public void setRows(int iRows) {
		this.getJTextArea().setRows(iRows);
	}

	@Override
	public void setName(String sName) {
		super.setName(sName);
		UIUtils.setCombinedName(this.ta, sName, "ta");
	}
	
	public TextModuleSettings getTextModuleSettings() {
		return tms;
	}

	public boolean isButtonEnabled(int index) {
		return true;
	}
	
	public void setButtonsDisabled() {
		ta.setButtonsDisabled();
	}
	
	public List<Icon> getButtonIcons() {
		return lstIconButtons;
	}
	
	public void setDataLanguageMapAsDirty(boolean isDirty) {
		getButtonIcons().remove(0);
		if (isDirty) {
			getButtonIcons().add(Icons.getInstance().getDataLanguageDirtyIcon());
		} else {
			getButtonIcons().add(Icons.getInstance().getDataLanguageIcon());
		}
	}
	
	public void buttonClicked(MouseEvent me, int index) {
		if (index == 0 && me.getComponent() instanceof InnerTextArea) {
			InnerTextArea txtButtonTextField = (InnerTextArea) me.getComponent();
			
			CollectableLocalizedComponentModel<Long> compModel = 
					getLocalizedComponent().getDetailsComponentModel();
			
			// Editing dialog works with a copy that will be reset into the model if the user presses 'OK'
			IDataLanguageMap dataLanguageMap = 
					compModel.getDataLanguageMap().clone();
			
			List<FieldMeta> lockedFields = new ArrayList<FieldMeta>();			
			if (getLocalizedComponent().isButtonDisabled()) {
				lockedFields.add(compModel.getMetaProvider().getEntityField(getCollectableEntityField().getUID()));
			}
			
			Pair<FieldMeta, Integer> p = new Pair<FieldMeta, Integer> (
					compModel.getMetaProvider().getEntityField(getCollectableEntityField().getUID()),
					CollectableComponentTypes.TYPE_TEXTAREA);
		
			final LocalizedEntityFieldFillInPanel editingPanel = new LocalizedEntityFieldFillInPanel(
					new EntityMetaVO(compModel.getMetaProvider().getEntity(getCollectableEntity().getUID()), true),
					RigidUtils.newOneElementArrayList(p),
					compModel.getPrimaryKey(),
           		 	dataLanguageMap,
           		    lockedFields);
			
			String tabCaption = SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.78", "Mehrsprachigkeit") + ": " + 
					getCollectableEntity().getLabel() + " -> " + getCollectableEntityField().getLabel();
			
			 OverlayOptionPane.showConfirmDialog(
				 compModel.getTab(),
				 editingPanel,
				 tabCaption ,
                 OverlayOptionPane.OK_CANCEL_OPTION,
                 true,
                 new OvOpAdapter() {
                     @Override
                     public void done(int result) {
                    	 CollectableLocalizedComponentModel<Long> compModel = 
                    			 getLocalizedComponent().getDetailsComponentModel();
                    	 
                         if (OverlayOptionPane.OK_OPTION == result) {
                        	 
                        	 
                        	 UID userDL = compModel.getUserPrimaryDataLanguage();
                        	 UID systemDL = compModel.getSystemPrimaryDataLanguage();
                        	 UID field = DataLanguageUtils.extractFieldUID(getCollectableEntityField().getUID());
                        	 UID fieldFlagged = DataLanguageUtils.extractFieldUID(getCollectableEntityField().getUID(), true);
                        	 
                        	 IDataLanguageMap fieldLanguageMap = 
                        			 editingPanel.getFieldLanguageMap();
                        	 
                        	 boolean isDirty = false;
                        	 boolean structureChanged = false;
                        	 
                        	 // reset new localized data into DataLanguageMap model
                        	 Map<UID, DataLanguageLocalizedEntityEntry> languageMap = compModel.getDataLanguageMap().getLanguageMap();
                        	 for (UID lang : languageMap.keySet()) {
                        		 if (languageMap.get(lang) == null) {
                        			 if (fieldLanguageMap.getDataLanguage(lang) != null) {
                        				 languageMap.put(lang, fieldLanguageMap.getDataLanguage(lang));
                        			 }
                        		 } else {
                        			 if (fieldLanguageMap.getDataLanguage(lang) != null) {
                        				 DataLanguageLocalizedEntityEntry dataLanguage = 
                        						 fieldLanguageMap.getDataLanguage(lang);
                        				 if (!dataLanguage.isFlagUnchanged() &&
                        					 fieldLanguageMap.getDataLanguage(lang).getFieldValue(field) != null) {
												 DataLanguageLocalizedEntityEntry existBlock = languageMap.get(lang);
												 existBlock.setFieldValue(field,fieldLanguageMap.getDataLanguage(lang).getFieldValue(field));
												 existBlock.setFieldValue(fieldFlagged, fieldLanguageMap.getDataLanguage(lang).getFieldValue(fieldFlagged));
												 structureChanged = true; 
												 if (existBlock.getPrimaryKey() == null) {
													 existBlock.flagNew(); 
												 } else {
													 existBlock.flagUpdate();
												 }
                        				 	}
                        			 }
                        		 }
                        		 
                        		 if (!Boolean.FALSE.equals(languageMap.get(lang).getFieldValue(fieldFlagged))) {
                        			 isDirty = true;
                        		 }
                        	 }
                        		 
                        	 setDataLanguageMapAsDirty(isDirty);
                        	 
                        	 // use existing model values to reset CollectableTextField instance
                        	 if (userDL == null || userDL.equals(systemDL)) {                                		 
                        		 DataLanguageLocalizedEntityEntry dataLanguage = 
                        				 fieldLanguageMap.getDataLanguage(systemDL);
                        		 
                        		 if (structureChanged || dataLanguage.isFlagNew() || dataLanguage.isFlagUpdated()) {
                        			 compModel.setField(new CollectableValueField(
                        					 dataLanguage.getFieldValue(field)), true);                                			 
                        		 }
                        	 } else {
                        		 DataLanguageLocalizedEntityEntry dataLanguage = 
                        				 fieldLanguageMap.getDataLanguage(userDL);
                        		
                        		 if (dataLanguage != null) {
                        			 if (structureChanged ||dataLanguage.isFlagNew() || dataLanguage.isFlagUpdated()) {
                        				 compModel.setField(new CollectableValueField(
                        						 fieldLanguageMap.getDataLanguage(userDL).getFieldValue(field)), true);                    			                                 				 
                        			 }
                        		 } else {
                        			 compModel.setField(null, true);
                        		 }
                        	 }
                        	 compModel.stopEditing();
                         } else {
                        	 compModel.cancelEditing();
                         }
                     }

                 });
		}
	}

	private CollectableEntityField getCollectableEntityField() {
		return this.entityField;
	}

	private CollectableEntity getCollectableEntity() {
		return this.entity;
	}

	private NuclosCollectableLocalizedTextArea getLocalizedComponent() {
		return this.localizedTextArea;
	}
	
	public void setTextModuleSettings(TextModuleSettings tms) {
		this.tms = tms;
	}

	@Override
	public JTextComponent getTargetTextComponent() {
		return getJTextArea();
	}
	
	
	
	private static enum ButtonState {
		
		NORMAL(Icons.getInstance().getIconTextFieldButton()),
		HOVER(Icons.getInstance().getIconTextFieldButtonHover()),
		PRESSED(Icons.getInstance().getIconTextFieldButtonPressed());
		
		ImageIcon ii;
		
		ButtonState(ImageIcon ii) {
			this.ii = ii;
		}
		
		ImageIcon getImageIcon() {
			return ii;
		}
	}	
	
}
