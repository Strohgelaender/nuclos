package org.nuclos.client.ui.collect.subform;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.KeyBindingProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.genericobject.controller.StateChangeAction;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.model.CollectableTableModel;
import org.nuclos.client.ui.collect.result.ResultActionCollection;
import org.nuclos.client.ui.collect.search.GenericObjectViaEntityObjectSearchStrategy;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Created by oliver brausch on 17.07.17.
 */
public enum ToolbarFunction {
	NEW {
		@Override
		public AbstractButton createButton(SubForm subform) {
			final JButton res = new JButton();
			res.setActionCommand(name());
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_NEW, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					res.doClick();
				}

			}, res);
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.7","Neuen Datensatz anlegen"), Icons.getInstance().getIconNew16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.NEW;
		}
	},
	NEW_AT_POSITION(EnumSet.of(SubForm.ToolbarFunctionDisplayOption.MENU)) {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton(Icons.getInstance().getIconNew16());
			res.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.11","Neuen Datensatz an dieser Position anlegen"));
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.11","Neuen Datensatz an dieser Position anlegen"), Icons.getInstance().getIconNew16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarItems.NEW_AT_POSITION;
		}
	},
	COPY_ROW(EnumSet.of(SubForm.ToolbarFunctionDisplayOption.MENU)) {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton(Icons.getInstance().getIconCopy16());
			res.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.13","Kopiere markierte Zeile(n)"));
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.13","Kopiere markierte Zeile(n)"), Icons.getInstance().getIconCopy16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarItems.COPY_ROW;
		}
	},
	CUT_ROW(EnumSet.of(SubForm.ToolbarFunctionDisplayOption.MENU)) {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton(Icons.getInstance().getIconCut16());
			res.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.14","Schneide markeirte Zeile(n) aus"));
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.14","Schneide markeirte Zeile(n) aus"), Icons.getInstance().getIconCut16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarItems.CUT_ROW;
		}
	},
	PASTE_ROW(EnumSet.of(SubForm.ToolbarFunctionDisplayOption.MENU)) {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton(Icons.getInstance().getIconInsertRow16());
			res.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.15","Füge kopierte Zeile(n) ein"));
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.15","Füge kopierte Zeile(n) ein"), Icons.getInstance().getIconInsertRow16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarItems.PASTE_ROW;
		}
			/*
			@Override
			public boolean isActive() {
				final Clipboard sysClip = Toolkit.getDefaultToolkit().getSystemClipboard();
				final Transferable transfer = sysClip.getContents(null);
				if (transfer.isDataFlavorSupported(TableRowCopyTransferHandler.RowTransferable.getDataFlavor())) {
					return true;
				}

				return false;
			}
			*/
	},
	CLONE {
		@Override
		public AbstractButton createButton(SubForm subform) {
			final JButton res = new JButton();
			res.setActionCommand(name());
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_CLONE, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					res.doClick();
				}
			}, res);
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(

					"MasterDataSubFormController.1","Datensatz klonen"), Icons.getInstance().getIconClone16());
			res.setActionCommand(name());

			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.CLONE;
		}
	},
	REMOVE {
		@Override
		public AbstractButton createButton(SubForm subform) {
			final JButton res = new JButton();
			res.setActionCommand(name());
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_REMOVE, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					res.doClick();
				}
			}, res);
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.1","Ausgew\u00e4hlten Datensatz l\u00f6schen"), Icons.getInstance().getIconDelete16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.DELETE;
		}
	},
	MULTIEDIT {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton();
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.6","Mehrere Datens\u00e4tze hinzuf\u00fcgen/l\u00f6schen"), Icons.getInstance().getIconMultiEdit16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.QUICKSELECTION;
		}
	},
	FILTER {
		@Override
		public AbstractButton createButton(SubForm subform) {
			final JToggleButton res = new JToggleButton();
			res.setActionCommand(name());
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_FILTER, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					res.doClick();
				}
			}, res);
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JCheckBoxMenuItem res = new JCheckBoxMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.5","Datens\u00e4tze filtern"), Icons.getInstance().getIconFilter16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.FILTER;
		}
	},
	TRANSFER {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton();
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.ToolbarFunction.TRANSFER", "Datensatz für alle Entit\u00e4ten \u00fcbernehmen"), Icons.getInstance().getIconCopy16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.TRANSFER;
		}
	},
	DOCUMENTIMPORT {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton();
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.ToolbarFunction.DOCUMENTIMPORT", "Insert document(s)"), Icons.getInstance().getIconImport16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.DOCUMENT_IMPORT;
		}
	},
	PRINTREPORT {
		@Override
		public AbstractButton createButton(SubForm subform) {
			final JButton res = new JButton();
			res.setActionCommand(name());
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_PRINTREPORT, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					res.doClick();
				}
			}, res);
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.8","Daten exportieren"), Icons.getInstance().getIconPrintReport16());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.PRINT_REPORT;
		}
	},
	AUTONUMBER_PUSH_UP {
		@Override
		public AbstractButton createButton(SubForm subform) {
			final JButton res = new JButton();
			res.setActionCommand(name());
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_AUTONUMBER_PUSH_UP, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					res.doClick();
				}
			}, res);
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.9","Zeile nach oben schieben"), Icons.getInstance().getIconSortAscending());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.AUTONUMBER_PUSH_UP;
		}
	},
	LOCALIZATION {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton();
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.attributeproperties.78","Localization"), Icons.getInstance().getDataLanguageIcon());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.LOCALIZATION;
		}
	},
	AUTONUMBER_PUSH_DOWN {
		@Override
		public AbstractButton createButton(SubForm subform) {
			final JButton res = new JButton();
			res.setActionCommand(name());
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_AUTONUMBER_PUSH_DOWN, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					res.doClick();
				}
			}, res);
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.10","Zeile nach unten schieben"), Icons.getInstance().getIconSortDescending());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.AUTONUMBER_PUSH_DOWN;
		}
	},
	SUBFORM_DETAILS_VIEW {
		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton(Icons.getInstance().getSubformDetails());
			res.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.16","Detail-Ansicht"));
			res.setActionCommand(name());
			return res;
		}
		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.16","Detail-Ansicht"), Icons.getInstance().getSubformDetails());
			res.setActionCommand(name());
			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.SUBFORM_DETAILS_VIEW;
		}
	},
	SUBFORM_DETAILS_ZOOM {

		@Override
		public AbstractButton createButton(SubForm subform) {
			JButton res = new JButton(Icons.getInstance().getIconZoomIn());
			res.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.18","Detail-Zoom"));
			res.setActionCommand(name());
			return res;
		}

		@Override
		public JMenuItem createMenuItem(SubForm subform) {
			JMenuItem res = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.18","Detail-Zoom"), Icons.getInstance().getIconZoomIn());
			res.setActionCommand(name());
			return res;
		}

		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.SUBFORM_DETAILS_ZOOM;
		}
	},
	SUBFORM_CHANGE_STATE {
		@Override
		public AbstractButton createButton(SubForm subform) {
			return null;
		}
		@Override
		public JMenuItem createMenuItem(final SubForm subform) {
			final JMenu res = new JMenu(SpringLocaleDelegate.getInstance().getMessage("SubForm.17","Statuswechsel"));
			res.setActionCommand(name());

			if (!subform.bSearch && !subform.bLayout
					&& MasterDataLayoutHelper.isLayoutMLAvailable(subform.getEntityUID(), false)
					&& MetaProvider.getInstance().getEntity(subform.getEntityUID()).isStateModel()) {
				final GenericObjectCollectController gController = (GenericObjectCollectController)
						NuclosCollectControllerFactory.getInstance().newGenericObjectCollectController(
								subform.getEntityUID(), new MainFrameTab(), ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));

				gController.setSearchStrategy(new GenericObjectViaEntityObjectSearchStrategy(
						new CollectableEOEntity(MetaProvider.getInstance().getEntity(gController.getEntityUid()))) {
					@Override
					protected List<CollectableEntityField> getCollectableFields() {
						final List<CollectableEntityField> cefs = new ArrayList<CollectableEntityField>();
						final CollectableTableModel tblModel = ((CollectableTableModel)subform.getSubformTable().getModel());
						for (int i = 0; i < tblModel.getColumnCount(); i++) {
							cefs.add(tblModel.getCollectableEntityField(i));
						}
						return cefs;
					}
				});
				gController.getSearchStrategy().setCollectController(gController);

				subform.getSubformTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent ev) {
						if (!ev.getValueIsAdjusting()) {
							if (subform.isReadOnly() || !subform.isEnabled()) {
								// maybe we can remove the listener stuff if subform is enabled or readonly. we have check it here,
								//  because respectRights will run after initializing the menuitem of this toolbarfunction.
								return; // return here.
							}

							final ListSelectionModel lsm = (ListSelectionModel) ev.getSource();
							if (lsm.isSelectionEmpty()) {
								res.removeAll();
								res.setEnabled(false);
							} else {
								try {
									final CollectableTableModel tblModel = ((CollectableTableModel)subform.getSubformTable().getModel());

									final List<Integer> lstSelectedRowNumbers = CollectionUtils.asList(subform.getJTable().getSelectedRows());
									List<Long> ids = CollectionUtils.transform(
											lstSelectedRowNumbers, new Transformer<Integer, Long>() {
												@Override
												public Long transform(Integer iRow) {
													final int modelIndex = subform.getJTable().convertRowIndexToModel(iRow);
													return IdUtils.toLongId(tblModel.getCollectable(modelIndex).getPrimaryKey());
												}
											});
									CollectionUtils.retainAll(ids, new Predicate<Long>() {
										@Override
										public boolean evaluate(Long t) {
											return t != null;
										}
									});
									gController.getSearchStrategy().setCollectableIdListCondition(new CollectableIdListCondition(ids));

									gController.fillResultPanel(gController.getSearchStrategy().getSearchResult());
									gController.setCollectState(CollectState.OUTERSTATE_RESULT, CollectState.RESULTMODE_SINGLESELECTION);
									if (gController.getResultTable().getRowCount() > 0) {
										gController.getResultTable().getSelectionModel().addSelectionInterval(0, gController.getResultTable().getRowCount() - 1);
									}

									for (ResultActionCollection rac : gController.getResultActionsMultiThreaded(gController.getSelectedCollectables())) {
										if (rac.getType().equals(ResultActionCollection.ResultActionType.CHANGE_STATE)) {
											res.removeAll();
											res.setEnabled(!rac.getActions().isEmpty());
											for (final Action act : rac.getActions()) {
												final JMenuItem miStateChange = new JMenuItem(new AbstractAction((String)act.getValue(Action.NAME)) {
													@Override
													public void actionPerformed(ActionEvent e) {
														gController.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
															@Override
															public void resultModeEntered(CollectStateEvent ev) throws CommonBusinessException {
																if (ev.getNewCollectState().getInnerState() == CollectState.RESULTMODE_NOSELECTION) {
																	// refresh silent
																	tblModel.removeTableModelListener(subform.tblmdllistener);

																	for (Iterator iterator = ((CollectableTableModel)gController.getResultTable().getModel()).getCollectables().iterator(); iterator.hasNext();) {
																		final Collectable clct = (Collectable) iterator.next();
																		final int iRow = tblModel.findRowById(clct.getId());
																		for (int i = 0; i < tblModel.getColumnCount(); i++) {
																			final CollectableEntityField clctef = tblModel.getCollectableEntityField(i);
																			tblModel.setValueAt(clct.getField(clctef.getUID()), iRow, i);
																		}
																	}

																	gController.getCollectStateModel().removeCollectStateListener(this);

																	// re-select silent.
																	subform.getSubformTable().getSelectionModel().setValueIsAdjusting(true);
																	subform.getSubformTable().getSelectionModel().clearSelection();
																	for (int iRow : lstSelectedRowNumbers) {
																		subform.getSubformTable().getSelectionModel().addSelectionInterval(iRow, iRow);
																	}
																	subform.getSubformTable().getSelectionModel().setValueIsAdjusting(false);

																	// add change listener again.
																	subform.getSubformTable().getModel().addTableModelListener(subform.tblmdllistener);

																	subform.revalidate();
																	subform.repaint();
																}
																gController.getCollectStateModel().removeCollectStateListener(this);
															}
														});
														act.actionPerformed(e);
													}
												});
												((StateChangeAction)act).setMainFrameTab(MainFrameTab.getMainFrameTabForComponent(subform));

												miStateChange.setIcon(null);
												res.add(miStateChange);
											}
										}
									}
								} catch (Exception ex) {
									LOG.warn("unable to get possible status change actions.", ex);
								}
							}
						}
					}
				});
			}

			return res;
		}
		@Override
		public ToolBarItem getToolBarItem() {
			return NuclosToolBarActions.SUBFORM_CHANGE_STATE;
		}
	};

	private static Logger LOG = Logger.getLogger(ToolbarFunction.class);

	private EnumSet<SubForm.ToolbarFunctionDisplayOption> setDisplayOptions;
	private ToolbarFunction() {
		this.setDisplayOptions = EnumSet.allOf(SubForm.ToolbarFunctionDisplayOption.class);
	}
	private ToolbarFunction(EnumSet<SubForm.ToolbarFunctionDisplayOption> setOptions) {
		this.setDisplayOptions = setOptions;
	}

	public abstract AbstractButton createButton(SubForm subform);

	public abstract JMenuItem createMenuItem(SubForm subform);

	public abstract ToolBarItem getToolBarItem();

	public boolean showButton() {
		return (setDisplayOptions.contains(SubForm.ToolbarFunctionDisplayOption.BUTTON));
	}

	public boolean showMenu() {
		return (setDisplayOptions.contains(SubForm.ToolbarFunctionDisplayOption.MENU));
	}

	public static ToolbarFunction fromCommandString(String actionCommand) {
		try {
			return ToolbarFunction.valueOf(actionCommand);
		}
		catch(Exception e) {
			// ignore here.
			LOG.debug("fromCommandString failed on " + actionCommand);
			return null;
		}
	}
}

