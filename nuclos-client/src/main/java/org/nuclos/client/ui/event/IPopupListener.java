package org.nuclos.client.ui.event;

import java.awt.event.MouseEvent;

import javax.swing.*;

public interface IPopupListener {
	
	/**
	 * @param e MouseEvent that has triggered the display of the popup
	 * @param menu
	 * @return true if the event has been consumed by this IPopupListener
	 * 		(this enables chaining).
	 * 		ATTENTION: chaining is prepared but not implemented!
	 */
	boolean doPopup(MouseEvent e, JPopupMenu menu);

}
