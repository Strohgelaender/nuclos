//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.search;

import java.awt.*;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.statemodel.admin.CollectableStateModel;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.E;
import org.nuclos.common.MarshalledValue;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionToPredicateVisitor;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.IOUtils;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.statemodel.valueobject.StateModelLayout;
import org.nuclos.server.statemodel.valueobject.StateModelVO;

public class StateModelSearchStrategy extends CollectSearchStrategy<UID,CollectableStateModel> {

	public StateModelSearchStrategy() {
		//...
	}

	@Override
	public void search() {
		final CollectController cc = getCollectController();
		final MainFrameTab mft = cc.getTab();
		try {
			mft.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			
			List<MasterDataVO<UID>> lstmdvos = CollectionUtils.transform(StateDelegate.getInstance().getAllStateModels(), new MakeMasterDataVO());
			
			final CollectableSearchExpression clctexpr
				= new CollectableSearchExpression(getCollectableSearchCondition(), cc.getResultController().getCollectableSortingSequence());
			final List<CollectableEntityField> cefs = cc.getResultController().getFields().getSelectedFields();
			final String isp = cc.getResultPanel().fetchIncrSearchPattern();
			ClientSearchUtils.addTextSearchToSearchExpression(isp, cefs, clctexpr, cc.getEntityUid());
			
			final CollectableSearchCondition cond = clctexpr.getSearchCondition();
			if (cond != null)
				lstmdvos = CollectionUtils.<MasterDataVO<UID>>applyFilter(lstmdvos, cond.accept(new SearchConditionToPredicateVisitor()));
			
			List<CollectableStateModel> result = CollectionUtils.transform(lstmdvos, new MakeCollectable());
			if (getCollectableIdListCondition() != null) {
				result = CollectionUtils.applyFilter(result, new CollectableIdPredicate(getCollectableIdListCondition().getIds()));
			}
			
			cc.fillResultPanel(result);
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(mft, null, ex);
		} finally {
			mft.setCursor(Cursor.getDefaultCursor());
		}
	}

	private static class MakeCollectable implements Transformer<MasterDataVO<UID>, CollectableStateModel> {
		@Override
		public CollectableStateModel transform(MasterDataVO<UID> mdVO) {
			return new CollectableStateModel(getStateModelVO(mdVO));
		}
		private static StateModelVO getStateModelVO(MasterDataVO<UID> mdVO) {
			StateModelLayout layout = null;
			try {
				byte b[] = (byte[])mdVO.getFieldValue(E.STATEMODEL.layout);
				Object o = null;

				if (b != null) {
					String xml = new String(b);
					if(xml != null && xml.startsWith("<?xml")) {
						o = xml;
					}
					else {
						o = IOUtils.fromByteArray(b);
					}
				}

				if (o == null) {
					// after migration...
				}
				else if (o instanceof MarshalledValue) {
					// for backwards compatibility:
					layout = (StateModelLayout) ((MarshalledValue) o).get();
				}
				else if (o instanceof StateModelLayout) {
					layout = (StateModelLayout) o;
				}
				else if(o instanceof String) {
					// XML is for future, see StateModelEditor2 
				}
				else {
					throw new NuclosFatalException("Unexpected class: " + o.getClass().getName());
				}
			}
			catch(IOException e) {
				throw new NuclosFatalException("can't read StateModelLayout");
			}
			catch(ClassNotFoundException e) {
				throw new NuclosFatalException("can't read StateModelLayout");
			}

			final StateModelVO vo = new StateModelVO(new NuclosValueObject<UID>(
					mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion()),
				mdVO.getFieldValue(E.STATEMODEL.name),
				mdVO.getFieldValue(E.STATEMODEL.description),
				layout,
				mdVO.getFieldUid(E.STATEMODEL.nuclet));

			return vo;
		}
	}
	private static class MakeMasterDataVO implements Transformer<StateModelVO, MasterDataVO<UID>> {
		@Override
		public MasterDataVO<UID> transform(StateModelVO vo) {
			return wrapStateModelVO(vo);
		}
		private static MasterDataVO<UID> wrapStateModelVO(StateModelVO vo) {
			byte[] layoutData = null;
			if (vo.getLayout() == null) {
				throw new NullArgumentException("layout");
			}

			try {
				layoutData = IOUtils.toByteArray(vo.getLayout());
				assert layoutData != null;
				assert layoutData.length > 0;
			}
			catch (IOException ex) {
				throw new NuclosFatalException(ex);
			}

			MasterDataVO<UID> result = new MasterDataVO<UID>(E.STATEMODEL.getUID(), vo.getPrimaryKey(), 
					vo.getChangedAt(), vo.getCreatedBy(), vo.getChangedAt(), vo.getChangedBy(), vo.getVersion());
			result.setFieldValue(E.STATEMODEL.name, vo.getName());
			result.setFieldValue(E.STATEMODEL.description, vo.getDescription());
			result.setFieldValue(E.STATEMODEL.layout, layoutData);
			result.setFieldUid(E.STATEMODEL.nuclet, vo.getNuclet());
			
			return result;
		}
	}
}
