//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Observer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.RowSorter;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.CollectableMasterDataProxyListAdapter;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIsNullCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class MasterDataSearchStrategy<PK> extends CollectSearchStrategy<PK,CollectableMasterDataWithDependants<PK>> {

	protected final MasterDataDelegate mddelegate = MasterDataDelegate.getInstance();

	private ProxyList<PK,CollectableMasterDataWithDependants<PK>> proxylstclct;

	public MasterDataSearchStrategy() {
	}

	protected final MasterDataCollectController<PK> getMasterCollectDataController() {
		return (MasterDataCollectController<PK>) getCollectController();
	}

	@Override
	public SearchWorker<PK,CollectableMasterDataWithDependants<PK>> getSearchWorker() {
		return new MasterDataObservableSearchWorker<PK>(getMasterCollectDataController());
	}

	@Override
	public SearchWorker<PK,CollectableMasterDataWithDependants<PK>> getSearchWorker(List<Observer> lstObservers) {
		MasterDataObservableSearchWorker<PK> observableSearchWorker = new MasterDataObservableSearchWorker<PK>(getMasterCollectDataController());
		for (Observer observer : lstObservers) {
			observableSearchWorker.addObserver(observer);
		}
		return observableSearchWorker;
	}

	@Override
	public ProxyList<PK,CollectableMasterDataWithDependants<PK>> getSearchResult() throws CollectableFieldFormatException {
		final MasterDataCollectController<PK> mdc = getMasterCollectDataController();
		final CollectableSearchExpression clctexpr = new CollectableSearchExpression(getCollectableSearchCondition(),
				mdc.getResultController().getCollectableSortingSequence());
		clctexpr.setMandator(getMandator());
		clctexpr.setValueListProviderDatasource(getValueListProviderDatasource());
		clctexpr.setValueListProviderDatasourceParameter(getValueListProviderDatasourceParameter());

		final MetaProvider metaProvider = SpringApplicationContextHolder.getBean(MetaProvider.class);
		List<CollectableEntityField> cefs = (new ArrayList<>(mdc.getResultController().getFields().getSelectedFields()))
				.stream().filter((field) -> !metaProvider.getEntity(field.getEntityUID()).isProxy())
				.collect(Collectors.toList());

		// add needed fields.
		final EntityMeta<?> meta = MetaProvider.getInstance().getEntity(mdc.getEntityName());
		if (meta.getRowColorScript() != null) {
			for (CollectableEntityField cef : mdc.getResultController().getFields().getAvailableFields()) {
				if (!LangUtils.equal(meta.getUID(), cef.getEntityUID()))
					continue;	
				final FieldMeta<?> cefmeta = MetaProvider.getInstance().getEntityField(cef.getUID());
				if (meta.getRowColorScript().getSource().indexOf("." + cefmeta.getFieldName()) != -1)
					cefs.add(cef);
			}
		}
		final List<CollectableEntityField> cefsToBeAdded = new ArrayList<CollectableEntityField>();
		for (CollectableEntityField clctef : cefs) {
			if (!LangUtils.equal(meta.getUID(), clctef.getEntityUID()))
					continue;	
			final FieldMeta<?> efmeta = MetaProvider.getInstance().getEntityField(clctef.getUID());
			if (efmeta.getBackgroundColorScript() != null) {
				for (CollectableEntityField cef : mdc.getResultController().getFields().getAvailableFields()) {
					final FieldMeta<?> cefmeta = MetaProvider.getInstance().getEntityField(cef.getUID());
					if (efmeta.getBackgroundColorScript().getSource().indexOf("." + cefmeta.getFieldName()) != -1)
						cefsToBeAdded.add(cef);
				}	
			}
		}
		cefs.addAll(cefsToBeAdded);
		String isp = mdc.getResultPanel().fetchIncrSearchPattern();
		ClientSearchUtils.addTextSearchToSearchExpression(isp, cefs, clctexpr, mdc.getEntityUid());
		
		final ProxyList<PK,MasterDataVO<PK>> mdproxylst = mddelegate.getMasterDataProxyList(mdc.getEntityUid(),cefs, clctexpr);
		if (mdproxylst.isHugeList()) {
			SortableCollectableTableModel<?, ?> model = mdc.getResultTableModel();
			if (!model.getSortKeys().isEmpty()) {
				model.setSortKeys(new ArrayList<>(), false);
			}
		}
		return new CollectableMasterDataProxyListAdapter<PK>(mdproxylst, mdc.getCollectableEntity());
	}

	@Override
	public void search() throws CommonBusinessException {
		final MasterDataCollectController<PK> mdc = getMasterCollectDataController();
		if (mdc.getResultController().getSearchResultStrategy().isInitializing()) {
			return;
		}
		mdc.makeConsistent(true);
		mdc.removePreviousChangeListenersForResultTableVerticalScrollBar();
		final ProxyList<PK,CollectableMasterDataWithDependants<PK>> lstclctResult = getSearchResult();
		setCollectableProxyList(lstclctResult);
		mdc.fillResultPanel(lstclctResult, lstclctResult.totalSize(false), false);
		mdc.setupChangeListenerForResultTableVerticalScrollBar();
	}

	// todo: can all (or at least most) of the methods concerning the search
	// proxy list be factored out somehow, as they are doubled in
	// GenericObjectCollectController?

	@Override
	public void setCollectableProxyList(ProxyList<PK,CollectableMasterDataWithDependants<PK>> proxylstclct) {
		this.proxylstclct = proxylstclct;
	}

	@Override
	public ProxyList<PK,CollectableMasterDataWithDependants<PK>> getCollectableProxyList() {
		return this.proxylstclct;
	}

	@Override
	public CollectableSearchCondition getCollectableSearchCondition() throws CollectableFieldFormatException {
		final CollectableSearchCondition result;
		final CollectableSearchCondition cond = (getCollectableIdListCondition() != null) ? addSearchFilter(getCollectableIdListCondition()) : super.getCollectableSearchCondition();
		if (getMasterCollectDataController().isFilteringDesired()) {
			if (cond == null) {
				// If no other search conditions specified, the validity
				// condition is the only search condition
				result = this.getValiditySubCondition();
			} else {
				// We have to add the validity condition by AND to the other
				// condition(s)
				final CollectableSearchCondition condValidity = this.getValiditySubCondition();
				if (condValidity != null) {
					final Collection<CollectableSearchCondition> collOperands = Arrays
							.asList(new CollectableSearchCondition[] { cond, condValidity });
					result = new CompositeCollectableSearchCondition(LogicalOperator.AND, collOperands);
				} else {
					result = cond;
				}
			}
		} else {
			result = cond;
		}
		return result;
	}

	/**
	 * @return the subcondition that checks for "activeness"/validity.
	 */
	private CollectableSearchCondition getValiditySubCondition() {
		final MasterDataCollectController<PK> cc = getMasterCollectDataController();

		final CollectableSearchCondition result;
		// Create search condition to check if today is between validFrom and validUntil when they are not NULL:
		if (cc.hasValidityDate()) {
			final CollectableSearchCondition condValidFromGreaterToday = new CollectableComparison(cc
					.getCollectableEntity().getEntityField(getUIDByFieldNameUnsafe(cc.getEntityUid(), MasterDataCollectController.FIELDNAME_VALIDFROM)),
					ComparisonOperator.LESS_OR_EQUAL, new CollectableValueField(new Date()));
			final CollectableSearchCondition condValidFromIsNull = new CollectableIsNullCondition(cc
					.getCollectableEntity().getEntityField(getUIDByFieldNameUnsafe(cc.getEntityUid(), MasterDataCollectController.FIELDNAME_VALIDFROM)));
			final CollectableSearchCondition condValidFrom = new CompositeCollectableSearchCondition(
					LogicalOperator.OR, Arrays.asList(new CollectableSearchCondition[] { condValidFromIsNull,
							condValidFromGreaterToday }));

			final CollectableSearchCondition condValidUntilLessThan = new CollectableComparison(cc
					.getCollectableEntity().getEntityField(getUIDByFieldNameUnsafe(cc.getEntityUid(), MasterDataCollectController.FIELDNAME_VALIDUNTIL)),
					ComparisonOperator.GREATER_OR_EQUAL, new CollectableValueField(new Date()));
			final CollectableSearchCondition condValidUntilIsNull = new CollectableIsNullCondition(cc
					.getCollectableEntity().getEntityField(getUIDByFieldNameUnsafe(cc.getEntityUid(), MasterDataCollectController.FIELDNAME_VALIDUNTIL)));
			final CollectableSearchCondition condValidUntil = new CompositeCollectableSearchCondition(
					LogicalOperator.OR, Arrays.asList(new CollectableSearchCondition[] { condValidUntilIsNull,
							condValidUntilLessThan }));

			result = new CompositeCollectableSearchCondition(LogicalOperator.AND,
					Arrays.asList(new CollectableSearchCondition[] { condValidFrom, condValidUntil }));
		} else {
			result = null;
		}

		// Create search condition to check if active sign is true
		/*
		      if (hasActiveSign()) {
		         condActive = new CollectableComparison(getCollectableEntity().getEntityField(FIELDNAME_ACTIVE),
		               ComparisonOperator.EQUAL, new CollectableValueField(new Boolean(true)));

		         result = condActive;
		      }


		      // If both of the abovemust be checked, create a combined search condition
		      if (condValid != null && condActive != null) {
		         final Collection collOperands = Arrays.asList(new CollectableSearchCondition[]{condActive, condValid});
		         result = new CompositeCollectableSearchCondition(LogicalOperator.AND, collOperands);
		      }
		*/
		return result;
	}
	
	@Deprecated
	public static UID getUIDByFieldNameUnsafe(UID entity, String entityfield) {
		for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity).values()) {
			if (field.getFieldName().toLowerCase().equals(entityfield.toLowerCase())) {
				return field.getUID();
			}
		}
		return null;
	}
}
