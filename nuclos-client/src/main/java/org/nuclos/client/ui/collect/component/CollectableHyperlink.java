//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.JTextComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.HyperlinkTextFieldWithButton;
import org.nuclos.client.ui.InputVerifierFactory;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledHyperlink;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.StringUtils;

/**
 * <code>CollectableComponent</code> to display/enter a hyperlink.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@nuclos.de">Maik Stueker</a>
 * @version	01.00.00
 */
public class CollectableHyperlink extends CollectableTextComponent {

	private static final String PROPERTY_SELECT_DIRECTORIES_ONLY = "DIRECTORIES_ONLY";
	private static final Logger LOG = Logger.getLogger(CollectableHyperlink.class);

	/**
	 * §postcondition this.isDetailsComponent()
	 */
	public CollectableHyperlink(CollectableEntityField clctef) {
		this(clctef, false);
		this.overrideActionMap();

		assert this.isDetailsComponent();
	}

	/**
	 * @param clctef
	 * @param bSearchable
	 */
	public CollectableHyperlink(CollectableEntityField clctef, boolean bSearchable) {
		this(clctef, bSearchable,
				new LabeledHyperlink(new LabeledComponentSupport(),
						clctef.isNullable(), bSearchable));	
	}

	protected CollectableHyperlink(CollectableEntityField clctef, boolean bSearchable, LabeledHyperlink labHyperlink) {
		super(clctef, labHyperlink, bSearchable);
		getJTextComponent().setInputVerifier(InputVerifierFactory.getInputVerifier(clctef, CollectableComponentTypes.TYPE_HYPERLINK));
	}

	// Override the tab key
	protected void overrideActionMap() {
		JTextComponent component = getJTextComponent();

		// The actions
		Action nextFocusAction = new AbstractAction("insert-tab") {

			@Override
            public void actionPerformed(ActionEvent evt) {
				((Component) evt.getSource()).transferFocus();
			}
		};
		Action prevFocusAction = new AbstractAction("Move Focus Backwards") {

			@Override
            public void actionPerformed(ActionEvent evt) {
				((Component) evt.getSource()).transferFocusBackward();
			}
		};
		// Add actions
		component.getActionMap().put(nextFocusAction.getValue(Action.NAME), nextFocusAction);
		component.getKeymap().addActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, KeyEvent.SHIFT_MASK), prevFocusAction);
	}

	public HyperlinkTextFieldWithButton getHyperlink() {
		return ((LabeledHyperlink) getJComponent()).getHyperlink();
	}

	@Override
	public JComponent getFocusableComponent() {
		return getHyperlink();
	}

	@Override
	public void setColumns(int iColumns) {
		this.getHyperlink().setColumns(iColumns);
	}

	@Override
	public void setComparisonOperator(ComparisonOperator compop) {
		super.setComparisonOperator(compop);
	}

	@Override
	public CollectableField getFieldFromView() throws CollectableFieldFormatException {
		return getField(getHyperlink().getText());
	}

	private CollectableField getField(String sText) throws CollectableFieldFormatException {
		String sPattern = getHyperlink().getPattern();
		final Object oValue = CollectableFieldFormat.getInstance(this.getEntityField().getJavaClass()).parse(sPattern,
			StringUtils.nullIfEmpty(sText));
		return CollectableUtils.newCollectableFieldForValue(this.getEntityField(), oValue);
	}

	@Override
	protected void updateView(CollectableField clctfValue) {
		this.getHyperlink().setText((String) clctfValue.getValue());

		this.adjustAppearance();
	}

	@Override
	protected void adjustBackground() {
//		this.getDateChooser().getJTextField().setBackground(this.getBackgroundColor());
	}

	@Override
	protected void setEnabledState(boolean flag) {
		this.getHyperlink().setEditable(flag);
		this.getHyperlink().setButtonEnabled(flag);
	}

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		final TableCellRenderer parentRenderer = super.getTableCellRenderer(subform);
		return new HyperlinkCellRenderer(parentRenderer, getHyperlink().getFont(), getHyperlink().getButtonIcons(), subform);
	}

	public static class HyperlinkCellRenderer implements TableCellRenderer, TableCellCursor {

		private final Cursor curHand = new Cursor(Cursor.HAND_CURSOR);

		private final TableCellRenderer parentRenderer;

		private final TableCellRendererPanel cellPanel;
		
		private final boolean subform;
		
		private final Font font;
		
		private final List<Icon> lstIcons;
		
		private boolean derive = false;
		private float fontSize = 0;

		public HyperlinkCellRenderer(TableCellRenderer parentRenderer, Font font, List<Icon> lstIcons, boolean subform) {
			this.parentRenderer = parentRenderer;
			this.font = font;
			this.lstIcons = lstIcons;
			this.subform = subform;
			this.cellPanel = new TableCellRendererPanel(this.lstIcons, this.subform);
		}

		@Override
		public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {
			Component comp = parentRenderer.getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
			if (comp instanceof JLabel) {
				final JLabel lb = (JLabel) comp;
				Map<TextAttribute, Integer> fontAttributes = new HashMap<TextAttribute, Integer>();
				fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
				if (derive) {
					lb.setFont(font.deriveFont(fontSize).deriveFont(fontAttributes));
				} else {
					lb.setFont(font.deriveFont(fontAttributes));
				}
				final CollectableField cf = (CollectableField) oValue;
				if (cf == null) {
					lb.setText("");
				} else {
					lb.setText((String) cf.getValue());
				}
			}
			cellPanel.removeAll();
			cellPanel.add(comp, BorderLayout.CENTER);
			return cellPanel;
		}

		@Override
		public Cursor getCursor(Object cellValue, int cellWidth, int x) {
			return isHyperlink(cellValue, cellWidth, x) ? curHand : null;
		}
		
		public boolean isHyperlink(Object cellValue, int cellWidth, int x) {
			if (cellValue != null) {
				if (cellValue instanceof CollectableField) {
					final Object cellValueObject = ((CollectableField)cellValue).getValue();
					if (cellValueObject != null && cellValueObject instanceof String) {
						if (!StringUtils.looksEmpty((String) cellValueObject)) {
							if (subform) {
								if (lstIcons == null || lstIcons.isEmpty()) {
									return true;
									
								}
								
								for (Icon icon : lstIcons) {
									final Rectangle r = HyperlinkTextFieldWithButton.getIconRectangle(new Dimension(cellWidth, icon.getIconHeight()), lstIcons.indexOf(icon));
									if (r.contains(x, icon.getIconHeight()/2)) {
										return false;
									}
																		
								}
								return true;
								
							} else {
								return true;
								
							}
						}
					}
				}
			}
			return false;
		}

		@Override
		public void deriveFont(float size) {
			derive = true;
			fontSize = size;
		}

	}

	private static class TableCellRendererPanel extends JPanel {

		private List<Icon> lstIcons;
		
		private final boolean subform;

		public TableCellRendererPanel(List<Icon> lstIcons, boolean subform) {
			super(new BorderLayout());
			this.subform = subform;
			if (this.subform) {
				this.lstIcons = lstIcons;
			}
		}

		@Override
		public void paint(Graphics g) {
			super.paint(g);

			if (subform) {
				Graphics2D g2d = (Graphics2D) g;
	
				for (Icon icon : lstIcons) {
					final Rectangle r = HyperlinkTextFieldWithButton.getIconRectangle(this.getSize(), lstIcons.indexOf(icon));
					r.y = 0;
					
					int w = icon.getIconWidth();
					int h = icon.getIconHeight();
					BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
					Graphics imgg = bi.getGraphics();
					icon.paintIcon(this, imgg, 0, 0);
					imgg.dispose();
		
					float[] scales = { 1f, 1f, 1f, 0.5f };
					float[] offsets = new float[4];
					RescaleOp rop = new RescaleOp(scales, offsets, null);
					g2d.drawImage(bi, rop, r.x, r.y);
					
				}	
			}
		}

	}

	@Override
	protected boolean selectAllOnGainFocus() {
		return getHyperlink().selectAllOnGainFocus();
	}

	@Override
	public void bindLayoutNavigationSupportToProcessingComponent() {
		getHyperlink().setLayoutNavigationCollectable(this);
		
	}

	
	@Override
    public void setProperty(String sName, Object oValue) {
		super.setProperty(sName,oValue);
		if(PROPERTY_SELECT_DIRECTORIES_ONLY.equals(sName))
		{
			getHyperlink().setSelectFolderAllowed(true);
		}
	}
}  // class CollectableHyperlink
