//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.eo.component;

import java.text.ParseException;

import javax.swing.*;

import org.nuclos.client.ui.eo.mvc.IModelAwareGuiComponent;
import org.nuclos.client.ui.eo.mvc.ITypeConverter;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonValidationException;

public class EoValueJLabel<PK> extends JLabel implements IModelAwareGuiComponent<PK> {
	
	private final UID fieldUid;
	
	private final ITypeConverter converter;
	
	private EntityObjectVO<PK> associatedEo;
	
	public EoValueJLabel(TypeConverterFactory tcFactory, UID fieldUid) {
		super();
		this.fieldUid = fieldUid;
		this.converter = tcFactory.getTypeConverterForField(fieldUid);
	}

	@Override
	public void fromComponentToModel() throws CommonValidationException, ParseException {
		String guiValue = getText();
		if (guiValue != null) {
			guiValue = guiValue.trim();
			if (guiValue.equals("")) {
				guiValue = null;
			}
		}
		final Object old = associatedEo.getFieldValue(fieldUid);
		final Object converted = converter.fromComponentToModel(guiValue);
		if (!LangUtils.equal(old, converted)) {
			associatedEo.setFieldValue(fieldUid, converted);
			if (!associatedEo.isFlagNew() && !associatedEo.isFlagRemoved()) {
				associatedEo.flagUpdate();
			}
		}
	}

	@Override
	public void fromModelToComponent() {
		String modelFieldValue = null;
		if (associatedEo != null) {
			modelFieldValue = (String) converter.fromModelToComponent(associatedEo.getFieldValue(fieldUid));
		}
		if (modelFieldValue == null) {
			modelFieldValue = "";
		}
		setText(modelFieldValue);
	}

	@Override
	public void setAssociatedEntityObject(EntityObjectVO<PK> eo) {
		this.associatedEo = eo;
	}

	@Override
	public EntityObjectVO<PK> getAssociatedEntityObject() {
		return associatedEo;
	}

}
