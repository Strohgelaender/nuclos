package org.nuclos.client.ui.collect.subform;

import java.awt.*;

import javax.swing.*;

import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;
import org.jdesktop.swingx.painter.TextPainter;

/**
 * Created by Oliver Brausch on 17.07.17.
 */ // class TranslucentLockableUI --->
class TranslucentLockableUI extends LockableUI {

	private final TextPainter textMessage;

	public TranslucentLockableUI() {
		Font fMessage = new Font("Monospaced", Font.BOLD, 16);
		textMessage = new TextPainter("", Color.BLACK);
		textMessage.setFont(fMessage);
		textMessage.setText("");
	}

	@Override
	protected void paintLayer(Graphics2D g2, JXLayer<? extends JComponent> l) {
		super.paintLayer(g2, l);
		if (isLocked()) {
			g2.setColor(SubForm.LAYER_BUSY_COLOR);
			g2.fillRect(0, 0, l.getWidth(), l.getHeight());
			textMessage.paint(g2, l, l.getWidth(), l.getHeight() + 50);
		}
	}

	public void setText(String text) {
		textMessage.setText(text);
	}

} // class TranslucentLockableUI
