package org.nuclos.client.ui;

import org.nuclos.client.theme.NuclosThemeSettings;

import javax.swing.*;
import java.awt.*;

/*
 * Used in conjunction with the Bubble-class. This Object implements the Content of the Bubble, as the 'paint'
 * -method of classes extending JWindow should not be overwritten.
 * The Object is meant to be appended to the contentPane of the Bubble.
 * @arg JLabel textLabel: The Label containing the information to be displayed.
 * @arg int width, height: Width and height of the surrounding Bubble-Object.
 * @arg BubbleUtils.Position pos: where the Bubble should be positioned relative to the parent. This Object also
 * 		implements the Shape of the Bubble.
 * @arg JComponent parent: The parent of the Bubble-Object.
 */
public class BubbleContent extends JPanel{

    private Shape bubbleShape;
    private JLabel textLabel;
    private BubbleUtils.Position pos;

    /*
     * Initializes the BubbleContent-Object.
     * Importantly the bubbleShape-Object is calculated, which is later used to paint the BubbleContent.
     */
    BubbleContent(JLabel textLabel, int width, int height, BubbleUtils.Position pos, JComponent parent) {
        this.pos = pos;
        this.textLabel = textLabel;
        setSize(width,height);
        bubbleShape = pos.getShape(getWidth(),getHeight(),3);
    }

    /*
     * This method creates the actual content.
     * First, the background is painted.
     * Second, the Shape is outlined.
     * Lastly, the text is written.
     */
    @Override
    public void paint(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(NuclosThemeSettings.BUBBLE_FILL_COLOR);
        g2.fill(bubbleShape);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        BasicStroke bs = new BasicStroke(2f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND);
        g2.setStroke(bs);

        g2.setColor(NuclosThemeSettings.BUBBLE_BORDER_COLOR);
        g2.draw(bubbleShape);

        Graphics gtl = g2.create();
        pos.translateForLabel(gtl, getWidth(), getHeight(), BubbleUtils.MAX_ARC_SIZE);

        gtl.translate(
                Math.min(Math.min(getWidth(), getHeight()), BubbleUtils.MAX_ARC_SIZE)/2,
                Math.min(Math.min(getWidth(), getHeight()), BubbleUtils.MAX_ARC_SIZE)/4);

        textLabel.setSize(textLabel.getPreferredSize());
        textLabel.paint(gtl);
    }

}
