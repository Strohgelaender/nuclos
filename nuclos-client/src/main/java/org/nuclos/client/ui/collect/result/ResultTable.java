package org.nuclos.client.ui.collect.result;

import java.awt.event.KeyEvent;

import javax.swing.*;

class ResultTable extends FilterableTable {

	private final CommonResultState state;

	ResultTable(CommonResultState state) {
		this.state = state;
	}
	
	@Override
	public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
		super.changeSelection(rowIndex, columnIndex, 
				state.isToggleSelection() && state.isAlternateSelectionToggle()? !toggle: toggle, extend);
	}
	
	@Override
	protected boolean processKeyBinding(KeyStroke ks, KeyEvent e, int condition, boolean pressed) {
		boolean processed = false;
		for (ResultKeyListener rkl : state.getKeyListener()) {
			if (rkl.processKeyBinding(ks, e, condition, pressed)) {
				processed = true;
			}
		}
		if (processed) {
			return true;
		} else {
			return super.processKeyBinding(ks, e, condition, pressed);
		}
	}
}
