package org.nuclos.client.ui.error;

/**
 * Defines the severity.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.4.1
 */
public enum ESeverity {
	
	INFO,
	WARN,
	ERROR,
	FATAL;

}
