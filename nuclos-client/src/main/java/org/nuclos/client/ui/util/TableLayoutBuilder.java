//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.ui.util;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.nuclos.client.wizard.util.MoreOptionPanel;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstants;
import info.clearthought.layout.TableLayoutConstraints;

public class TableLayoutBuilder implements TableLayoutConstants, ITableLayoutBuilder, Cloneable {

	private final ITableLayoutBuilder parent;
	
	private final JPanel panel;
	
	private final MyTableLayout tableLayout;
	
	// private Component lastComponent;
	
	// configuration
	
	/*
	private double[] columnSpec;
	
	private int hgap;
	
	private int vgap;
	 */
	
	// state
	
	private int row = -1;
	
	private int column = 0;
	
	public TableLayoutBuilder() {
		this(new JPanel());
	}

	public TableLayoutBuilder(JPanel panel) {
		this(null, panel);
	}
	
	public TableLayoutBuilder(ITableLayoutBuilder parent, JPanel panel) {
		this.parent = parent;
		this.tableLayout = new MyTableLayout();
		this.panel = panel;
		panel.setLayout(tableLayout);
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public TableLayoutBuilder columns(double... columnSpec) {
		// this.columnSpec = columnSpec;
		
		tableLayout.setColumn(columnSpec);
		return this;
	}
	
	public TableLayoutBuilder gaps(int hgap, int vgap) {
		// this.hgap = hgap;
		// this.vgap = vgap;
		
		tableLayout.setHGap(hgap);
		tableLayout.setVGap(vgap);
		return this;
	}
	
	public void reset() {
		final Set<Component> components = tableLayout.reset();
		for (Component c: components) {
			panel.remove(c);
		}
		row = -1;
		column = 0;
		/*
		tableLayout = new MyTableLayout();
		columns(columnSpec);
		gaps(hgap, vgap);
		 */
	}

	public void replace(Component newComponent, int upperRow, int leftColumn) {
		final Component old = tableLayout.find(upperRow, leftColumn);
		if (old == null) {
			throw new IllegalStateException("No old component found at (" + upperRow + ", " + leftColumn + ")");
		}
		replaceWith(old, newComponent);
	}
	
	public void replaceWith(Component oldComponent, Component newComponent) {
		final TableLayoutConstraints constraints = tableLayout.getConstraints(oldComponent);
		tableLayout.removeLayoutComponent(oldComponent);
		panel.remove(oldComponent);
		panel.add(newComponent, constraints);
		
		// tableLayout.replaceWith(oldComponent, newComponent);
	}
	
	public int getMinRow(Component comp) {
		final TableLayoutConstraints contraints = tableLayout.getConstraints(comp);
		return contraints.row1;
	}
	
	public int getMaxRow(Component comp) {
		final TableLayoutConstraints contraints = tableLayout.getConstraints(comp);
		return contraints.row2;
	}
	
	/**
	 * ATTENTION:
	 * This method will create a new row at the very end of the 'main' builder,
	 * i.e. <em>not</em> at the insertion point if you used a 
	 * {@link #insertRowAt(int)} to obtain this builder.
	 */
	@Override
	public ITableLayoutBuilder newRow() {
		return newRow(TableLayout.PREFERRED);
	}

	/**
	 * ATTENTION:
	 * This method will create a new row at the very end of the 'main' builder,
	 * i.e. <em>not</em> at the insertion point if you used a 
	 * {@link #insertRowAt(int)} to obtain this builder.
	 */
	@Override
	public ITableLayoutBuilder newRow(double height) {
		row = tableLayout.getNumRow();
		newRow(row, height);
		return this;
	}
	
	/**
	 * ATTENTION: Only for intern use with {@link #clone()} and {@link #insertRowAt(int)}.
	 */
	private void newRow(int rowIndex, double height) {
		tableLayout.insertRow(rowIndex, height);
		column = 0;
	}
	
	public ITableLayoutBuilder insertRowAt(int rowIndex) {
		return insertRowAt(rowIndex, TableLayout.PREFERRED);
	}
	
	/**
	 * Insert a new row a rowIndex.
	 * 
	 * @param rowIndex where to insert the row (index starts at 0)
	 * @param height of the row to insert
	 * @return a <em>CLONED</em> {@link ITableLayoutBuilder} to work on the inserted row.
	 */
	public ITableLayoutBuilder insertRowAt(int rowIndex, double height) {
		if (rowIndex < 0) {
			return newRow(height);
		}
		final TableLayoutBuilder result;
		try {
			result = (TableLayoutBuilder) clone();
		} catch (CloneNotSupportedException e) {
			throw new IllegalStateException(e);
		}
		result.row = rowIndex;
		result.newRow(rowIndex, height);
		return result;
	}
	
	public int getRowSize() {
		return tableLayout.getNumRow();
	}
	
	public void deleteRow(int index) {
		final Collection<Component> comps = tableLayout.getComponentsOnRow(index, false);
		for (Component c: comps) {
			tableLayout.removeLayoutComponent(c);
			panel.remove(c);
		}
		tableLayout.deleteRow(index);
		row = tableLayout.getNumRow();
	}
	
	@Override
	public ITableLayoutBuilder skip() {
		column++;
		return this;
	}
	
	@Override
	public ITableLayoutBuilder skip(int n) {
		column += n;
		return this;
	}
	
	@Override
	public ITableLayoutBuilder add(Component c) {
		return add(c, 1);
	}
	
	@Override
	public ITableLayoutBuilder add(Component c, int colspan) {
		return add(c, colspan, TableLayout.FULL, TableLayout.FULL);
	}

	@Override
	public ITableLayoutBuilder add(Component c, int colspan, int halign, int valign) {
		final int nextColumn = column + colspan - 1;
		ensureColumns(nextColumn);
		if (c != null) {
			panel.add(c, new TableLayoutConstraints(column, row, nextColumn, row, halign, valign));
		}
		column = nextColumn + 1;
		return this;
	}
	
	@Override
	public ITableLayoutBuilder addFullSpan(Component c) {
		return add(c, Math.max(1, tableLayout.getNumColumn() - column));
	}

	@Override
	public ITableLayoutBuilder addLabel(String text) {
		return addLabel(text, CENTER);
	}

	@Override
	public ITableLayoutBuilder addLabel(String text, int valign) {
		return addLabel(text, null, valign);
	}

	@Override
	public ITableLayoutBuilder addLabel(String text, String toolTip, int valign) {
		JLabel label = createLabel(text, toolTip);
		add(label, 1, FULL, valign);
		return this;
	}

	@Override
	public ITableLayoutBuilder addLocalizedLabel(String resourceId) {
		return addLocalizedLabel(resourceId, CENTER);
	}
	
	@Override
	public ITableLayoutBuilder addLocalizedLabel(String resourceId, int valign) {
		return addLocalizedLabel(resourceId, null, valign);
	}	

	@Override
	public ITableLayoutBuilder addLocalizedLabel(String resourceId, String toolTipResourceId) {
		return addLocalizedLabel(resourceId, toolTipResourceId, CENTER);
	}	

	@Override
	public ITableLayoutBuilder addLocalizedLabel(String resourceId, String toolTipResourceId, int valign) {
		String text = SpringLocaleDelegate.getInstance().getMessage(resourceId, null);
		String toolTip = (toolTipResourceId != null) 
				? SpringLocaleDelegate.getInstance().getMessage(toolTipResourceId, null) : null;
		return addLabel(text, toolTip, valign);
	}	
	
	private JLabel createLabel(String text, String toolTipText) {
		JLabel label = new JLabel(text);
		if (toolTipText == null) {
			toolTipText = text;
			if (toolTipText.endsWith(":")) {
				toolTipText = toolTipText.substring(0, toolTipText.length() - 1).trim();
			}
		}
		label.setToolTipText(toolTipText);
		return label;
	}

	
	public ITableLayoutBuilder newMoreOptionPanel() {
		JPanel optionPanel;
		newRow();
		addFullSpan(new MoreOptionPanel(optionPanel = new JPanel()));
		return new TableLayoutBuilder(optionPanel);
	}
	
	private void ensureColumns(int columns) {
		if (row == -1)
			newRow();
		for (int col = tableLayout.getNumColumn(); col < columns; col++) {
			tableLayout.insertColumn(col, TableLayout.PREFERRED);
		}
	}

	public ITableLayoutBuilder close() {
		getPanel();
		if (parent == null) {
			throw new IllegalStateException();
		}
		return parent;
	}
	
	public JPanel getPanel() {
		return panel;
	}
	
	private static final class MyTableLayout extends TableLayout {

		MyTableLayout() {
			super();
		}

		Collection<Component> getComponentsOnRow(int row, boolean includeMultiRows) {
			final List<Component> result = new ArrayList<Component>();

			// Count contraints
			int numEntry = list.size();

			// If there are no components, they can't be overlapping
			if (numEntry == 0) {
				return result;
			}

			// Put entries in an array
			final Entry entry[] = (Entry[]) list.toArray(new Entry[numEntry]);
			for (Entry en : entry) {
				final int r1 = en.cr1[1];
				if (en.cr1[1] == row) {
					result.add(en.component);
					continue;
				}
				if (includeMultiRows) {
					final int r2 = en.cr2[1];
					if (r1 < row && r2 > row) {
						result.add(en.component);
					}
				}
			}
			return result;
		}
		
		Component find(int upperRow, int leftColumn) {
			Component result = null;
			for (final Iterator<Entry> it = list.iterator(); it.hasNext(); ) {
				final Entry entry = it.next();
				if (entry.cr1[C] == leftColumn && entry.cr1[R] == upperRow) {
					result = entry.component;
					break;
				}
			}
			return result;
		}
		
		Set<Component> reset() {
			final Set<Component> result = new HashSet<Component>();
			for (final Object o: list) {
				final Entry entry = (Entry) o;
				result.add(entry.component);
			}
			list.clear();
			setRow(defaultSize[R]);
			return result;
		}
		
	}

	@Override
	public TableLayout getTableLayout() {
		return tableLayout;
	}

}
