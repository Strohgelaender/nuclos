//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
//
package org.nuclos.client.ui.gc;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

// @Component
@ManagedResource(
		objectName="nuclos.client.gc:name=QueueSingleton",
		description="Nuclos Client QueueSingleton",
		currencyTimeLimit=15,
		log=false)
public class QueueSingleton implements InitializingBean {
	
	private static final Logger LOG = Logger.getLogger(QueueSingleton.class);

	// 10 sec
	private static final long INTERVAL = 10 * 1000L;
	
	private static QueueSingleton INSTANCE;
	
	//
	
	private Timer timer;
	
	private final ReferenceQueue<EventListener> queue = new ReferenceQueue<EventListener>();
	
	private final Map<Reference<EventListener>, IRegister> eventListener2Register 
		= new ConcurrentHashMap<Reference<EventListener>, IRegister>();
	
	QueueSingleton() {
		INSTANCE = this;
	}
	
	// @Autowired
	public final void setTimer(Timer timer) {
		this.timer = timer;
	}
	
	// @PostConstruct
	public final void afterPropertiesSet() {
		timer.schedule(new QueueTask(), INTERVAL, INTERVAL);
	}
	
	public static QueueSingleton getInstance() {
		return INSTANCE;
	}
	
	public void register(IRegister register) {
		eventListener2Register.put(register.getReference(), register);
	}
	
	public ReferenceQueue<EventListener> getQueue() {
		return queue;
	}
	
	@ManagedAttribute(description="get the size (number of users/UserRights) of mpUserRights")
	public int getNumberOfRegisteredListeners() {
		return eventListener2Register.size();
	}
	
	@ManagedOperation(description="get overview of elements in queue")
	public Map<String,Integer> getSizeStatistics() {
		final Map<String,Integer> result = new TreeMap<String, Integer>();
		int gced = 0;
		for (Reference<EventListener> ref: eventListener2Register.keySet()) {
			final IRegister reg = eventListener2Register.get(ref);
			final boolean collected;
			if (ref.get() == null) {
				collected = true;
				++gced;
			} else {
				collected = false;
			}
			final String name = reg.getClass().getName() + ":" + collected;
			Integer value = result.get(name);
			if (value == null) {
				value = Integer.valueOf(1);
			} else {
				value = Integer.valueOf(value.intValue() + 1);
			}
			result.put(name, value);
		}
		result.put("totalGced", gced);
		return result;
	}
	
	private class QueueTask extends TimerTask {
		
		private QueueTask() {
		}
		
		@Override
		public void run() {
			Reference<? extends Object> ref;
			do {
				ref = queue.poll();
				if (ref != null) {
					// final Object o = ref.get();
					final IRegister c = eventListener2Register.remove(ref);
					if (c != null) {
						c.unregister();
						LOG.debug("unregistered " + c + ", mapSize=" + eventListener2Register.size());
					}
					else {
						LOG.debug("removed gc'ed ref " + ref + ", mapSize=" + eventListener2Register.size());
					}
				}
			} while (ref != null);
			
			// This should be unnecessary (as we use a gc queue) at the moment I test if the result is cleaner (tp)
			// remove gc'ed references
			final List<Reference<EventListener>> remove = new LinkedList<Reference<EventListener>>();
			for (Reference<EventListener> r: eventListener2Register.keySet()) {
				if (r.get() == null) {
					remove.add(r);
				}
			}
			if (!remove.isEmpty()) {
				LOG.warn("Unexpected EventListeners already gc'ed: " + remove.size());
				for (Reference<EventListener> r: remove) {
					final IRegister reg = eventListener2Register.remove(r);
					if (reg != null) {
						reg.unregister();
					}
				}
			}
		}
		
	}

}
