package org.nuclos.client.ui.collect;

public interface ITabSelectListener {
	
	void selectionChanged(boolean b);

}
