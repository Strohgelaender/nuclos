//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.search;

import java.awt.event.ActionEvent;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import org.apache.log4j.Logger;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.AskAndSaveResult;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CommonController;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.SearchComponentModel;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.client.ui.collect.searcheditor.SearchEditorController;
import org.nuclos.client.ui.collect.searcheditor.SearchEditorPanel;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.common.SF;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.InvalidCollectableSearchConditionException;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.collectable.searchcondition.ToHumanReadablePresentationVisitor;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.CommonRunnable;

/**
 * Controller for the Search panel.
 */
public class SearchController<PK,Clct extends Collectable<PK>> extends CommonController<PK,Clct> {
	
	private static final Logger LOG = Logger.getLogger(SearchController.class);
	
	//
	
	boolean searchEditorActive = false;
	
	/**
	 * Action for showing/hiding the search editor.
	 */
	private class ToggleSearchEditorAction extends CommonAbstractAction {

		/**
		 * avoids recursion
		 */
		private boolean bLocked;

		ToggleSearchEditorAction() {
			super();
		}

		@Override
        public void actionPerformed(ActionEvent ev) {
			if (!bLocked) {
				try {
					final CollectController<PK,Clct> cc = getCollectController();
					bLocked = true;
					final SearchPanel pnlSearch = cc.getSearchPanel();
//					boolean bSelected = Boolean.TRUE.equals(actSearchEditor.getValue(Action.SELECTED_KEY));
//					assert bSelected != cc.getSearchPanel().isSearchEditorVisible();
					searchEditorActive = !searchEditorActive;
					try {
						if (searchEditorActive) {
							// search fields -> SearchEditor:
							cc.stopEditingInSearch();
							/** TODO It would be better to call getCollectableSearchConditionFromSearchFields(true) here
							 * so bad or inconsistent view is detected. In that case, the exception must be caught here. */
							cc.getSearchPanel().getSearchEditorPanel().setSortedSearchCondition(cc.getCollectableSearchConditionFromSearchFields(false));

							actNewWithSearchValues.setEnabled(false);
						}
						else {
							final CollectableSearchCondition cond = cc.getSearchPanel().getSearchEditorPanel().getSearchCondition();
							if (cond != null && !cond.isSyntacticallyCorrect()) {
								searchEditorActive = true;
								throw new InvalidCollectableSearchConditionException(getSpringLocaleDelegate().getMessage(
										"CollectController.17","Die Suchbedingung ist unvollst\u00e4ndig."));
							}
							else if (!cc.getSearchPanel().canDisplayConditionInFields(cond)) {
								searchEditorActive = true;
								throw new InvalidCollectableSearchConditionException(getSpringLocaleDelegate().getMessage(
										"CollectController.18","Eine zusammengesetzte Suchbedingung kann nur im Sucheditor, nicht in der Suchmaske dargestellt werden."));
							}
							else {
								// SearchEditor -> search fields:
								cc.setSearchFieldsAccordingToSearchCondition(cond, true);
							}
						}
					}
					catch (InvalidCollectableSearchConditionException ex) {
						// undo button press:
						searchEditorActive = !searchEditorActive;
						Errors.getInstance().showExceptionDialog(cc.getTab(), ex);
					}
					catch (Exception ex) {
						// TODO don't catch and wrap RuntimeExceptions here!
						final String sMessage = getSpringLocaleDelegate().getMessage(
								"CollectController.16","Diese Suchbedingung kann nur im Sucheditor, nicht in der Suchmaske dargestellt werden.") + "\n" + ex.getMessage();
						// undo button press:
						searchEditorActive = !searchEditorActive;
						Errors.getInstance().showExceptionDialog(cc.getTab(), new InvalidCollectableSearchConditionException(sMessage, ex));
					}
//					cc.getSearchPanel().setSearchEditorVisible(bSelected);
					setSearchEditorVisible(searchEditorActive);
				}
				finally {
					bLocked = false;
				}
			}
			assert !bLocked;
		}
	}
	
	/**
	 * action: New with search values
	 */
	private final Action actNewWithSearchValues = NuclosToolBarActions.NEW_FROM_SEARCH.init(new CommonAbstractAction() {

		@Override
        public void actionPerformed(ActionEvent ev) {
			final CollectController<PK,Clct> cc = getCollectController();
			cc.askAndSaveIfNecessary(new ResultListener<AskAndSaveResult>() {
				@Override
				public void done(AskAndSaveResult result) {
					if (AskAndSaveResult.isClosable(result)) {
						cc.cmdEnterNewModeWithSearchValues();
					}
				}
			});
		}
	});
	
	private final Action actSearchEditor = NuclosToolBarActions.SEARCH_EDITOR.init(new ToggleSearchEditorAction());

	/**
	 * action: Search
	 * 
	 * TODO: Move to ResultController???
	 */
	private final Action actSearch = NuclosToolBarActions.SEARCH.init(new CommonAbstractAction() {

		@Override
        public void actionPerformed(ActionEvent ev) {
			final CollectController<PK,Clct> cc = getCollectController();
			cc.stopEditingInSearch();
			if(cc.getCollectPanel().getTabbedPaneSelectedIndex() == CollectPanel.TAB_SEARCH)
				cc.getResultController().getSearchResultStrategy().cmdSearch();
		}
	});

	/**
	 * action: Clear Search Condition
	 */
	private final Action actClearSearchCondition = NuclosToolBarActions.CLEAR_SEARCH_FORM.init(new CommonAbstractAction() {

		@Override
        public void actionPerformed(ActionEvent ev) {
			getCollectController().cmdClearSearchCondition();
		}
	});

	/**
	 * Listener for searchcondition changes.
	 * 
	 * Cannot be final because set to null in close(). (tp)
	 */
	private CollectableComponentModelListener ccmlistener = new CollectableComponentModelAdapter() {
		@Override
		public void searchConditionChangedInModel(SearchComponentModelEvent ev) {
			final CollectController<PK,Clct> cc = getCollectController();
			
			// not true/valid for BMW_FDM, hence commented out (tp)
			// assert cc.getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_SEARCH;

			// Note that we want to call "searchChanged()" on every change, not only valid changes:
			if (cc != null) {
				cc.searchChanged(ev.getSearchComponentModel());				
			}
		}
	};

	public SearchController(CollectController<PK,Clct> cc) {
		super(cc);
	}
	
	public final Action getNewWithSearchValuesAction() {
		return actNewWithSearchValues;
	}
	
	public final Action getSearchAction() {
		return actSearch;
	}

	/**
	 * TODO: Make this private again.
	 */
	public void setupSearchPanel() {
		final CollectController<PK,Clct> cc = getCollectController();
		final SearchPanel pnlSearch = cc.getSearchPanel();
		// action: Search
//		pnlSearch.btnSearch.setAction(this.actSearch);
		pnlSearch.registerToolBarAction(NuclosToolBarItems.SEARCH, actSearch);

		// action: Clear Search Condition
//		pnlSearch.btnClearSearchCondition.setAction(this.actClearSearchCondition);
		pnlSearch.registerToolBarAction(NuclosToolBarItems.CLEAR_SEARCH_FORM, actClearSearchCondition);

		// action: New
//		pnlSearch.btnNew.setAction(cc.getNewAction());
		pnlSearch.registerToolBarAction(NuclosToolBarItems.NEW, cc.getNewAction());

		// action: New with search values
//		pnlSearch.btnNewWithSearchValues.setAction(cc.getNewWithSearchValuesAction());
//		pnlSearch.btnNewWithSearchValues.getAction().setEnabled(false);
		pnlSearch.registerToolBarAction(NuclosToolBarItems.NEW_FROM_SEARCH, cc.getNewWithSearchValuesAction());
		getNewWithSearchValuesAction().setEnabled(false);

//		final Action actToggleSearchEditor = new ToggleSearchEditorAction();

//		pnlSearch.btnSearchEditor.setAction(actToggleSearchEditor);
		pnlSearch.registerToolBarAction(NuclosToolBarItems.SEARCH_EDITOR, actSearchEditor);
		// In order to enable the search editor, a CollectableFieldsProviderFactory needs to be defined.
//		actToggleSearchEditor.setEnabled(cc.getCollectableFieldsProviderFactoryForSearchEditor() != null);
		actSearchEditor.setEnabled(cc.getCollectableFieldsProviderFactoryForSearchEditor() != null);

		UIUtils.readSplitPaneStateFromPrefs(getCurrentLayoutUid(), cc.getPreferences(), pnlSearch);
	}

	@Override
	protected boolean isSearchPanel() {
		return true;
	}

	/**
	 * displays the current search condition in the Search panel's status bar.
	 */
	public final void cmdDisplayCurrentSearchConditionInSearchPanelStatusBar(MainFrameTab tab) {
		UIUtils.runShortCommand(tab, new CommonRunnable() {
			@Override
            public void run() {
				displayCurrentSearchConditionInSearchPanelStatusBar();
			}
		});
	}

	/**
	 * TODO: Make protected again.
	 */
	@Override
	public Collection<SearchComponentModel> getCollectableComponentModels() {
		final CollectController<PK,Clct> cc = getCollectController();
		return cc.getSearchPanel().getEditModel().getCollectableComponentModels();
	}

	@Override
	protected CollectableComponentModelListener getCollectableComponentModelListener() {
		return this.ccmlistener;
	}

	@Override
	protected void addAdditionalChangeListeners() {
		getCollectController().addAdditionalChangeListenersForSearch();
	}

	@Override
	protected void removeAdditionalChangeListeners() {
		getCollectController().removeAdditionalChangeListenersForSearch();
	}

	/**
	 * TODO: Make this private again.
	 */
	public void setupSearchEditor() {
		final CollectController<PK,Clct> cc = getCollectController();
		final SearchEditorPanel pnlSearchEditor = cc.getSearchPanel().getSearchEditorPanel();
		new SearchEditorController(cc.getTab(), pnlSearchEditor, cc.getCollectableEntity(),
				cc.getCollectableFieldsProviderFactoryForSearchEditor(),
				cc.getAdditionalSearchFields()
		);

		pnlSearchEditor.getTreeModel().addTreeModelListener(new TreeModelListener() {
			@Override
            public void treeNodesChanged(TreeModelEvent ev) {
				changed(ev);
			}

			@Override
            public void treeNodesInserted(TreeModelEvent ev) {
				changed(ev);
			}

			@Override
            public void treeNodesRemoved(TreeModelEvent ev) {
				changed(ev);
			}

			@Override
            public void treeStructureChanged(TreeModelEvent ev) {
				changed(ev);
			}

			private void changed(TreeModelEvent ev) {
				this.adjustSearchEditorButton();
				getCollectController().searchChanged(ev.getPath());
			}

			private void adjustSearchEditorButton() {
				final CollectController<PK,Clct> cc = getCollectController();
//				cc.getSearchPanel().btnSearchEditor.setEnabled(cc.getSearchPanel().canDisplayConditionInFields(
//						cc.getSearchPanel().getSearchEditorPanel().getSearchCondition()));
				actSearchEditor.setEnabled(cc.getSearchPanel().canDisplayConditionInFields(
						cc.getSearchPanel().getSearchEditorPanel().getSearchCondition()));
			}
		});
	}

	/**
	 * releases the resources (esp. listeners) for this controller.
	 */
	@Override
	public void close() {
		if (!isClosed()) {
			final CollectController<PK,Clct> cc = getCollectController();
			final SearchPanel pnlSearch = getSearchPanel();
//			pnlSearch.btnSearch.setAction(null);
//			pnlSearch.btnSearchEditor.setAction(null);
//			pnlSearch.btnClearSearchCondition.setAction(null);
//			pnlSearch.btnNew.setAction(null);
	
			UIUtils.writeSplitPaneStateToPrefs(getCurrentLayoutUid(), cc.getPreferences(), getSearchPanel());
			ccmlistener = null;
			
			super.close();
		}
	}

	/**
	 * displays the current search condition in the Search panel's status bar.
	 * 
	 * TODO: Make this private again.
	 */
	public void displayCurrentSearchConditionInSearchPanelStatusBar() {
		String sSearchCondition;
		String addStatusMsg = "";
		final CollectController<PK,Clct> cc = getCollectController();
		try {
			CollectableSearchCondition searchcond = cc.getCollectableSearchConditionToDisplay();
			/*if (getCollectController() instanceof GenericObjectCollectController && searchcond != null) { // auskommentiert wegen NUCLOS-6556 (NUCLOS-5205 ist fehlerhaft)
				GenericObjectCollectController gocc = (GenericObjectCollectController) getCollectController();
				if (gocc.getProcess() != null && gocc.isCalledByProcessMenu()) {
					searchcond = cleanSearchConditionFromProcess(searchcond, gocc);
				}
			}*/
			if (searchcond == null) {
				sSearchCondition = getSpringLocaleDelegate().getMessage(
						"CollectController.2","<Alle> (Keine Einschr\u00e4nkung)");
			} else {
				sSearchCondition = searchcond.accept(new ToHumanReadablePresentationVisitor());
			}
			if(displayMixedSearchCondition()){
				addStatusMsg = getSpringLocaleDelegate().getMessage("CollectController.22","Kombinierte ");
			}
		}
		catch (CollectableFieldFormatException ex) {
			sSearchCondition = getSpringLocaleDelegate().getMessage("CollectController.3","<Ung\u00fcltig>");
		}
		getSearchPanel().setStatusBarText(addStatusMsg + getSpringLocaleDelegate().getMessage(
				"CollectController.26","Suchbedingung: ") + sSearchCondition);
	}
	
	public static final CollectableSearchCondition cleanSearchConditionFromProcess(CollectableSearchCondition searchcond, GenericObjectCollectController gocc) {
		CompositeCollectableSearchCondition result = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		if (searchcond instanceof CompositeCollectableSearchCondition) {
			for (CollectableSearchCondition operand : ((CompositeCollectableSearchCondition)searchcond).getOperands()) {
				CollectableSearchCondition cleansc = cleanSearchConditionFromProcess(operand, gocc);
				if (cleansc != null) {
					result.addOperand(cleansc);
				}
			}
		} else if (searchcond instanceof AtomicCollectableSearchCondition){
			AtomicCollectableSearchCondition atomsc = (AtomicCollectableSearchCondition) searchcond;
			if (!atomsc.getEntityField().equals(gocc.getCollectableEntity().getEntityField(SF.PROCESS_UID.getUID(gocc.getEntityUid())))
					|| !(atomsc instanceof CollectableComparison) || !((CollectableComparison)atomsc).getComparand().equals(gocc.getProcess())) {
				result.addOperand(searchcond);
			}
		} else {
			result.addOperand(searchcond);
		}
		return SearchConditionUtils.simplified(result);
	}
	
	private boolean displayMixedSearchCondition = false;

	public boolean displayMixedSearchCondition() {
		return displayMixedSearchCondition;
	}

	public void setDisplayMixedSearchCondition(boolean displayMixedSearchCondition) {
		this.displayMixedSearchCondition = displayMixedSearchCondition;
	}
	
	private final SearchPanel getSearchPanel() {
		return getCollectController().getSearchPanel();
	}
	
	public void setSearchEditorVisible(boolean bVisible) {
		getSearchPanel().setSearchEditorVisible(bVisible);
		actSearchEditor.putValue(Action.SELECTED_KEY, bVisible);
		searchEditorActive = bVisible;
	}

}	// class SearchController

