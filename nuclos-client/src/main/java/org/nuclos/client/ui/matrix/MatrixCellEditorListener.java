//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.matrix;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;

public class MatrixCellEditorListener implements CellEditorListener {
	
	MatrixTableModel model;	
	JTable table;
	

	public MatrixCellEditorListener(MatrixTableModel model, JTable table) {
		super();
		this.model = model;
		this.table = table;
	}

	@Override
	public void editingStopped(ChangeEvent e) {
		Object objSource = e.getSource();
		if(objSource instanceof MatrixSelectCellEditor) {
			MatrixSelectCellEditor edit = (MatrixSelectCellEditor)objSource;
			MatrixCollectable collect = (MatrixCollectable) edit.getCellEditorValue();
			int col = table.getSelectedColumn();
			int row = table.getSelectedRow();
			if (row > -1 && col > -1) {
				collect.markAsModified();
				model.setValueAt(collect, row, col);
			}
		}
		
	}

	@Override
	public void editingCanceled(ChangeEvent e) {
		
	}

}
