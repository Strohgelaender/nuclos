//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.event;

import java.awt.event.ComponentEvent;

import javax.swing.*;

/**
 * Event that informs listeners of a request to pop up a (context) menu on a 
 * {@link JTable}.
 * <p>
 * Part of the support abstraction for popup menus on {@link JTable}s needed
 * for http://project.nuclos.de/browse/LIN-165.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.3.0
 */
public class TablePopupMenuEvent extends ComponentEvent {

	public static final int HEADER_EVENT = 9821;
	
	public static final int CELL_EVENT = 9822;
	
	public static final int ROW_EVENT = 9823;
	
	public static final int COLUMN_EVENT = 9824;
	
	private int rowIndex = -1;
	
	private int columnIndex = -1;
	
	public TablePopupMenuEvent(JTable source, int id, int columnIndex, int rowIndex) {
		super(source, id);
		this.columnIndex = columnIndex;
		this.rowIndex = rowIndex;
	}
	
	public int getColumnIndex() {
		return columnIndex;
	}
	
	public int getRowIndex() {
		return rowIndex;
	}

}
