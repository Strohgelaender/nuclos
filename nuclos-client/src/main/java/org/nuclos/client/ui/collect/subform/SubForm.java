//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.subform;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Closeable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.JTextComponent;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.common.CollectableDocumentFileChooserBase;
import org.nuclos.client.common.EnabledListener;
import org.nuclos.client.common.FocusActionListener;
import org.nuclos.client.common.ISubformHistoricalViewLookUp;
import org.nuclos.client.common.KeyBindingProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableListOfValues;
import org.nuclos.client.common.NuclosCollectableLocalizedTextArea;
import org.nuclos.client.common.NuclosCollectableTextArea;
import org.nuclos.client.common.SearchConditionSubFormController.SearchConditionTableModel;
import org.nuclos.client.common.Utils;
import org.nuclos.client.genericobject.valuelistprovider.MandatorCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.ProcessCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusNumeralCollectableFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.DatasourceBasedCollectableFieldsProvider;
import org.nuclos.client.scripting.ScriptEvaluator;
import org.nuclos.client.scripting.context.SubformControllerScriptContext;
import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.NuclosToolBar;
import org.nuclos.client.ui.SizeKnownListener;
import org.nuclos.client.ui.StatusBarPanel;
import org.nuclos.client.ui.StatusBarTextField;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.URIMouseAdapter;
import org.nuclos.client.ui.collect.CollectableResultComponent;
import org.nuclos.client.ui.collect.ColletableEntityObjectLookup;
import org.nuclos.client.ui.collect.DynamicRowHeightChangeListener;
import org.nuclos.client.ui.collect.DynamicRowHeightChangeProvider;
import org.nuclos.client.ui.collect.ISubFormCtrlLateInit;
import org.nuclos.client.ui.collect.ITabSelectListener;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.component.CollectableCheckBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.CollectableLocalizedComponent;
import org.nuclos.client.ui.collect.component.CollectableLocalizedTextField;
import org.nuclos.client.ui.collect.component.CollectableOptionGroup;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.LookupListener;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.collect.toolbar.INuclosToolBarItem;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.client.ui.event.IPopupListener;
import org.nuclos.client.ui.event.Mouse2TableHeaderPopupMenuAdapter;
import org.nuclos.client.ui.event.TableColumnModelAdapter;
import org.nuclos.client.ui.event.TablePopupMenuEvent;
import org.nuclos.client.ui.event.TablePopupMenuMouseAdapter;
import org.nuclos.client.ui.labeled.LabeledComponent;
import org.nuclos.client.ui.popupmenu.JPopupMenuFactory;
import org.nuclos.client.ui.table.SortableTableModelEvent;
import org.nuclos.client.ui.table.TableCellEditorProvider;
import org.nuclos.client.ui.table.TableCellRendererProvider;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.valuelistprovider.VLPClientUtils;
import org.nuclos.common.E;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.ToolBarItemGroup;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common.textmodule.TextModuleSettings;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Subform for displaying/editing dependant <code>Collectable</code>s.
 *
 * Changes as of 2010-08-12: The subform's toolbar is no longer publicly
 * accessible! If your client-code needs toolbar-events, add a SubFormToolListener
 * on the subForm itself, which will be notifies about ALL toolbar events at once.
 *
 * If you need to add another toolbar button, pass the button along with its
 * action command to addToolbarFunction. The given action command will be propagated
 * the same way as the internal default commands.
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class SubForm extends CollectableResultComponent
	implements TableCellRendererProvider, ActionListener, Closeable, DynamicRowHeightChangeListener {

	static final Logger LOG = Logger.getLogger(SubForm.class);
	public static Color colorCommonValues = Utils.translateColorFromParameter(ParameterProvider.KEY_HISTORICAL_STATE_CHANGED_COLOR);//new Color(246,229,255);
	private static final int STRETCH_SHRINK_COLUMN_DIFF = 10;

	private SpringLocaleDelegate localeDelegate = null;
	private Integer rowCount = 0;
	private Integer selectedRowCount = 0;

	private SpringLocaleDelegate getSpringLocaleDelegate() {
		return localeDelegate != null ? localeDelegate : SpringLocaleDelegate.getInstance();
	}

	public void setStatusbarRowCount(final Integer count) {
		rowCount = count;
		refreshStatusCount();
	}

	public void setStatusbarSelectedRowCount(final Integer count) {
		selectedRowCount = count;
		refreshStatusCount();
	}

	private void refreshStatusCount() {
		if (lbStatusCount != null) {
			lbStatusCount.setText(
					getSpringLocaleDelegate().getMessage("ResultController.1", "{0} Datens\u00e4tze gefunden.", rowCount) + " / "
							+ getSpringLocaleDelegate().getMessage("ResultController.14", " {0} selected rows", selectedRowCount));
		}
	}

	public interface SubFormToolListener extends EventListener {
		void toolbarAction(String actionCommand);
	}

	public static enum ToolbarFunctionDisplayOption{
		BUTTON,
		MENU
	}
	
	private class ToolBarAction extends AbstractAction {
		public ToolBarAction() {
			super();
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			String actionCommand = StringUtils.nullIfEmpty(e.getActionCommand());
			SubForm.this.actionPerformed(actionCommand);
		}
	}

	/* the minimum row height for the table(s). */
	public static final int MIN_ROWHEIGHT = 20;
	public static final int MAX_DYNAMIC_ROWHEIGHT = 200;
	public static final int DYNAMIC_ROW_HEIGHTS = -1;

	private boolean dynamicRowHeights = false;
	private boolean dynamicRowHeightsDefault = false;
	private boolean ignoreSubLayout = false;
	private Integer maxEntries = null;

	private ColletableEntityObjectLookup lookupService;
	
	static final Color LAYER_BUSY_COLOR = new Color(128, 128, 128, 128);

	//
	private final CollectableComponentModelAdapter editorChangeListener = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			if (ev.collectableFieldHasChanged()) {
				fireStateChanged(ev);
			}
		}

		@Override
		public void searchConditionChangedInModel(SearchComponentModelEvent ev) {
			fireStateChanged(ev);
		}
	};

	private JXLayer<JComponent>	 layer;
	private AtomicInteger	     lockCount	= new AtomicInteger(0);

	private HashMap<String, AbstractButton>   toolbarButtons;
	private List<String>					  toolbarOrder;
	private HashMap<String, MenuElement>	  toolbarMenuItems;

	/**
	 * @see #isDetailsChangedIgnored()
	 */
	private boolean bDetailsChangedIgnored;

	/**
	 * Can't be final because it must be set to null in close() to avoid memeory leaks. (tp)
	 */
	private NuclosToolBar toolbar;

	private StatusBarPanel statusbar;

	private boolean showStatusbar;

	private Boolean openDetailsWithTabRecycling;

	/**
	 * Can't be final because it must be set to null in close() to avoid memeory leaks. (tp)
	 */
	private JPanel contentPane = new JPanel(new BorderLayout());

	private JPanel innerContentPane = new JPanel(new BorderLayout());

	private StatusBarTextField lbStatusCount = new StatusBarTextField("");
	private StatusBarTextField lbStatusPK = new StatusBarTextField("");

	private JScrollPane scrollPane = new JScrollPane();

	private SubFormTable subformtbl;

	private SubFormFilter subFormFilter;

	private final UID foreignKeyFieldToParent;

	private final List<LookupListener> lookupListener = new ArrayList<LookupListener>();

	protected final List<FocusActionListener> lstFocusActionListener = new ArrayList<FocusActionListener>();

	/**
	 * NUCLOSINT-63: To display the size of subform list in the corresponding tab.
	 */
	private SizeKnownListener sizeKnownListener;

	/**
	 * maps column names to columns
	 */
	private final Map<UID, Column> mpColumns = new LinkedHashMap<UID, Column>();
	private final Map<CollectableEntityField, TableCellRenderer> mpColumnRenderer = new HashMap<>();
	private final Map<CollectableEntityField, CollectableComponentTableCellEditor> mpStaticColumnEditors = new HashMap<>();

	private List<ChangeListener> lstchangelistener = new LinkedList<>();
	private List<EnabledListener> lstEnabledListeners = new ArrayList<>();

	private UID uniqueMasterColumnUid;

	private UID sSubFormParent;

	private String sControllerType;

	private UID sInitialSortingColumn;
	private String sInitialSortingOrder;
	
	private boolean bMultiEdit;
	
	private Map<String, Object> mpParams = new HashMap<String, Object>();
	
	public interface ParameterChangeListener extends ChangeListener {
		@Override
		void stateChanged(ChangeEvent e);
	}

	private final List<ParameterChangeListener>	parameterListener = new ArrayList<>();

	/**
	 * Use custom column widths? This will always be true as soon as the user changed one or more column width
	 * the first time.
	 */
	private boolean useCustomColumnWidths;

	TableModelListener tblmdllistener;
	private TableColumnModelListener columnmodellistener;

	private SubformRowHeader rowHeader;

	private CollectableComponentFactory collectableComponentFactory;

	private List<SubFormToolListener> listeners;

	private IPopupListener popupMenuListener;

	private final List<Pair<JComponent,MouseListener>> mouseListener = new ArrayList<Pair<JComponent,MouseListener>>();

	private boolean closed = false;

	private boolean enabled = true;
	private boolean enabledByLayout = true;

	private boolean readonly = false;
	private boolean detailViewReadOnlyAvailable = false;
	
	private boolean bCloneable = true;
	private boolean bMultieditable = true;
    private boolean isAutonumberSorting = true;
    private boolean bWithinTabbedPane = false;
    
	public boolean isWithinTabbedPane() {
		return bWithinTabbedPane;
	}

	public void setWithinTabbedPane(boolean bWithinTabbedPane) {
		this.bWithinTabbedPane = bWithinTabbedPane;
	}

	private NuclosScript newEnabledScript;
	private NuclosScript editEnabledScript;
	private NuclosScript deleteEnabledScript;
	private NuclosScript cloneEnabledScript;
	private NuclosScript dynamicRowColorScript;

	final boolean bSearch;
	final boolean bLayout;

	private final RowHeightController rowHeightCtrl;
	
	private final ToolbarManager toolbarManager;
	
	private final Map<UID, TextModuleSettings> mpTextModuleSettings;
	private LayoutNavigationSupport lns;

	private final UID rootLayoutUID;

	/**
	 * §precondition entityName != null
	 * §postcondition this.getForeignKeyFieldToParent() == null
	 *  @param entityUID
	 * @param iToolBarOrientation {@link JToolBar#setOrientation(int)}
	 * @param rootLayoutUID
	 */
	public SubForm(UID entityUID, boolean bSearch, int iToolBarOrientation, final UID rootLayoutUID) {
		this(entityUID, bSearch, iToolBarOrientation, rootLayoutUID, null);

		assert this.getForeignKeyFieldToParent() == null;
	}

	/**
	 * §precondition entityName != null
	 * §postcondition this.getForeignKeyFieldToParent() == foreignKeyFieldToParent
	 *  @param toolBarOrientation {@link JToolBar#setOrientation(int)}
	 * @param rootLayoutUID
	 * @param foreignKeyFieldToParent Needs only be specified if not unique. {@link #getForeignKeyFieldToParent()}
	 */
	public SubForm(UID entityUID, boolean bSearch, int toolBarOrientation, final UID rootLayoutUID, UID foreignKeyFieldToParent) {
		this(entityUID, bSearch, toolBarOrientation, rootLayoutUID, foreignKeyFieldToParent, false);
	}

	/**
	 * §precondition entityName != null
	 * §postcondition this.getForeignKeyFieldToParent() == foreignKeyFieldToParent
	 *  @param toolBarOrientation {@link JToolBar#setOrientation(int)}
	 * @param rootLayoutUID
	 * @param foreignKeyFieldToParent Needs only be specified if not unique. {@link #getForeignKeyFieldToParent()}
	 */
	public SubForm(UID entityUID, boolean bSearch, int toolBarOrientation, final UID rootLayoutUID, UID foreignKeyFieldToParent, boolean bLayout) {
		super(new GridLayout(1, 1));
		this.mpTextModuleSettings = new HashMap<UID, TextModuleSettings>();
		this.toolbarManager = new ToolbarManager();
		this.rowHeightCtrl = new RowHeightController(this);

		this.bSearch = bSearch;
		this.rootLayoutUID = rootLayoutUID;
		this.bLayout = bLayout;
		
		this.toolbar = UIUtils.createNonFloatableToolBar(toolBarOrientation);

		this.listeners = new ArrayList<SubFormToolListener>();
		subformtbl = new SubFormTable(this) {
		    protected void configureEnclosingScrollPane() {
		    	super.configureEnclosingScrollPane();
		    	if (subFormFilter != null) {
			    	subFormFilter.setupTableHeaderForScrollPane(scrollPane);
		    	}
		    }
		};

		ToolTipManager.sharedInstance().setDismissDelay(60000);
		ToolTipManager.sharedInstance().registerComponent(subformtbl);

		final DragGestureListener dragGestureListener = new DragGestureListener() {
            @Override 
            public void dragGestureRecognized(DragGestureEvent e) {
            	if (subformtbl.getCellEditor() != null) {
            		final CollectableComponent clctComponent = ((CollectableComponentTableCellEditor)subformtbl.getCellEditor()).getCollectableComponent();
            		if (clctComponent instanceof CollectableDocumentFileChooserBase) {
            			final DragGestureListener dragGestureListener = ((CollectableDocumentFileChooserBase)clctComponent).getDragGestureListener();
            			if (dragGestureListener != null)
            				dragGestureListener.dragGestureRecognized(
            					new DragGestureEvent(e.getSourceAsDragGestureRecognizer(), e.getDragAction(), e.getDragOrigin(), Collections.singletonList(e.getTriggerEvent())));
            		}
        		}
            }
        };
        new DragSource().createDefaultDragGestureRecognizer(subformtbl, DnDConstants.ACTION_COPY_OR_MOVE, dragGestureListener);

		subformtbl.addMouseListener(new SubFormPopupMenuMouseAdapter(subformtbl));
		subformtbl.addMouseListener(new DoubleClickMouseAdapter());
		subformtbl.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent e) {
				SubForm.this.setStatusbarSelectedRowCount(subformtbl.getSelectedRowCount());

				//TODO how to display long pk sequences?
//				String sPKs = "";
//				if (subformtbl.getModel() instanceof CollectableTableModel) {
//					CollectableTableModel model = ((CollectableTableModel) subformtbl.getModel());
//					sPKs = (String) model.getRows(subformtbl.getSelectedRows()).stream().map(clct -> ((Collectable)clct).getPrimaryKey().toString()).collect(Collectors.joining(", "));
//				}
//
//				lbStatusPK.setText(getSpringLocaleDelegate().getText("primarykey") + ": " + sPKs);
			}
		});
		
		final URIMouseAdapter uriMouseAdapter = new URIMouseAdapter();
		subformtbl.addMouseListener(uriMouseAdapter);
		subformtbl.addMouseMotionListener(uriMouseAdapter);

		innerContentPane.add(scrollPane, BorderLayout.CENTER);
		if (!bSearch) {
			statusbar = UIUtils.newStatusBar(Box.createHorizontalStrut(10), lbStatusCount, Box.createHorizontalGlue(), lbStatusPK);
			innerContentPane.add(statusbar, BorderLayout.SOUTH);
		}
		contentPane.add(innerContentPane, BorderLayout.CENTER);

		if (entityUID == null) {
			throw new NullArgumentException("entityUID");
		}
		this.entityUid = entityUID;
		
		if (toolBarOrientation == -1) {
			this.toolbar.setVisible(false);
		} else {
			this.toolbar.setOrientation(toolBarOrientation);
		}
		this.foreignKeyFieldToParent = foreignKeyFieldToParent;
		this.collectableComponentFactory = CollectableComponentFactory.getInstance();
		layer = new JXLayer<JComponent>(contentPane, new TranslucentLockableUI());
		layer.setName("JXLayerGlasspane");
		add(layer);

		toolbarButtons = new HashMap<String, AbstractButton>();
		toolbarMenuItems = new HashMap<String, MenuElement>();
		toolbarOrder = new ArrayList<String>();
		for(ToolbarFunction func : ToolbarFunction.values()) {
			//Revert Nuclos-1944, avoid subform change menuitem. Re-open ticket
			if (func == ToolbarFunction.SUBFORM_CHANGE_STATE) {
				continue;
			}
 			final AbstractButton button = func.createButton(this);
			if (button != null) {
				button.putClientProperty(ToolBarItem.COMPONENT_PROPERTY_KEY, func.getToolBarItem());
				if (func.getToolBarItem() instanceof ToolBarItemAction) {
					String sCommand = button.getActionCommand();
					button.setAction(((INuclosToolBarItem) func.getToolBarItem()).init(new ToolBarAction()));
					button.setActionCommand(sCommand);
				} else {
					button.addActionListener(this);
				}
				toolbarButtons.put(func.name(), button);
				toolbar.add(button);
			}
			final MenuElement mi = func.createMenuItem(this);
			if (mi != null) {
				((AbstractButton)mi).addActionListener(this);
				toolbarMenuItems.put(func.name(), mi);
			}
			toolbarOrder.add(func.name());
		}
	
		setToolbarFunctionState(ToolbarFunction.REMOVE, ToolbarFunctionState.DISABLED);
		setToolbarFunctionState(ToolbarFunction.MULTIEDIT, ToolbarFunctionState.HIDDEN);
		setToolbarFunctionState(ToolbarFunction.DOCUMENTIMPORT, ToolbarFunctionState.HIDDEN);
		setToolbarFunctionState(ToolbarFunction.FILTER, ToolbarFunctionState.HIDDEN);
		setToolbarFunctionState(ToolbarFunction.AUTONUMBER_PUSH_DOWN, ToolbarFunctionState.HIDDEN);
		setToolbarFunctionState(ToolbarFunction.AUTONUMBER_PUSH_UP, ToolbarFunctionState.HIDDEN);
		setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.HIDDEN);
		setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.HIDDEN);
		setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_ZOOM, ToolbarFunctionState.DISABLED);
		
		this.init();

		subFormFilter = new SubFormFilter(this);
		
		assert this.getForeignKeyFieldToParent() == foreignKeyFieldToParent;		


		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_STRETCH_ACTIVE_COLUMN, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					stretchActiveColumn(true);
				}

			}, this.getJTable());

			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SUBFORM_SHRINK_ACTIVE_COLUMN, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					stretchActiveColumn(false);
				}

			}, this.getJTable());

	}
    /**
	 * Stretches or Shrinks the current selected column via CTRL+ALT+"+" or
	 * CTRL+ALT+"-"
	 * @param stretch
	 */
	private void stretchActiveColumn(boolean stretch) {
		int columnIdx = this.getSubformTable().getSelectedColumn();

		if (columnIdx < 0) {
			return;
		}
		TableColumn column = getSubformTable().getColumnModel().getColumn(columnIdx);

		if (stretch) {
			column.setPreferredWidth(column.getPreferredWidth() + STRETCH_SHRINK_COLUMN_DIFF);

		} else {
			column.setPreferredWidth(column.getPreferredWidth() - STRETCH_SHRINK_COLUMN_DIFF);
		}
		column.setWidth(column.getPreferredWidth());
	}

	public SubFormFilter getSubFormFilter() {
		return subFormFilter;
	}
	
	public void initializeParametersForSubformFilter(SubFormParameterProvider parameterProvider, UID parentEntityUID, CollectableFieldsProviderFactory collectableFieldsProviderFactory, Long intidForVLP, UID mandator) {
		subFormFilter.setupFilter(parameterProvider, parentEntityUID, collectableFieldsProviderFactory, intidForVLP, mandator);
		
		// always test if the is a stored filter
		if (subFormFilter.isFilterStored()) {
			// ... if there is one, create subform filter immediately
			subFormFilter.createFilter(true);
		}
	}
	
	public void setLayoutNavigationSupport(LayoutNavigationSupport lns) {
		this.lns = lns;
	}
	
	public LayoutNavigationSupport getLayoutNavigationSupport() {
		return this.lns;
	}

	public ColletableEntityObjectLookup getLookupService() {
		return lookupService;
	}

	public void setLookupService(ColletableEntityObjectLookup lookupService) {
		this.lookupService = lookupService;
	}

	@Override
	public final void close() {
		// Close is needed for avoiding memory leaks
		// If you want to change something here, please consult me (tp).
		if (!closed) {
			LOG.debug("close(): " + this);
			if (rowHeader != null) {
				rowHeader.close();
			}
			rowHeader = null;
			if (subformtbl != null) {
				subformtbl.close();
			}
			subformtbl = null;
			if (subFormFilter != null) {
				subFormFilter.close();
			}
			subFormFilter = null;
			for (Pair<JComponent,MouseListener> p: mouseListener) {
				p.getX().removeMouseListener(p.getY());
			}
			sizeKnownListener = null;

			// Partial fix for http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=7079260
			popupMenuListener = null;
			scrollPane = null;

			contentPane = null;
			toolbar.close();
			toolbar = null;

			statusbar = null;

			mpColumnRenderer.clear();
			mpColumns.clear();
			mpStaticColumnEditors.clear();

			parameterListener.clear();
			lstchangelistener.clear();
			lstFocusActionListener.clear();
			lookupListener.clear();
			listeners.clear();
			mouseListener.clear();
			
			mpParams.clear();
			layer = null;

			closed = true;
		}
	}

	public void addColumnModelListener(TableColumnModelListener tblcolumnlistener) {
		subformtbl.getColumnModel().addColumnModelListener(tblcolumnlistener);
	}

	public void addSubFormToolListener(SubFormToolListener l) {
		listeners.add(l);
	}

	public void removeSubFormToolListener(SubFormToolListener l) {
		listeners.remove(l);
	}

	public JScrollPane getSubformScrollPane() {
		return this.scrollPane;
	}

	public JScrollPane getScrollPane() {
		return getSubformScrollPane();
	}

	@Override
	public UID getLayoutUID() {
		return rootLayoutUID;
	}

	public void actionPerformed(String actionCommand) {
		if (actionCommand != null) {			
			for (SubFormToolListener l : new ArrayList<SubFormToolListener>(listeners)) {
				l.toolbarAction(actionCommand);				
			}
		}
	}

	@Override
    public void actionPerformed(ActionEvent e) {
		actionPerformed(StringUtils.nullIfEmpty(e.getActionCommand()));
	}

	public void setToolbarFunctionState(ToolbarFunction func, ToolbarFunctionState state) {
		// set state at toolbar manager.
		getToolbarManager().setState(func, state);
		
		final AbstractButton button = toolbarButtons.get(func.name());
		if(button != null) {
			if (func.showButton()) {
				state.set(button);
			} else {
				button.setVisible(false);
			}
		}
		final AbstractButton mi = (AbstractButton)toolbarMenuItems.get(func.name());
		if (mi != null) {
			if (func.showMenu()) {
				state.set(mi);
			} else {
				mi.setVisible(false);
			}
		}
	}
	
	//--- needed by the wysiwyg editor only -----------------------------------
	public void addToolbarButtonMouseListener(MouseListener l) {
		for(AbstractButton b : toolbarButtons.values())
			b.addMouseListener(l);
	}

	public void removeToolbarButtonMouseListener(MouseListener l) {
		for(AbstractButton b : toolbarButtons.values())
			b.removeMouseListener(l);
	}

	public JToolBar getToolbar() {
		return toolbar;
	}

	public int getToolbarOrientation() {
		return toolbar.getOrientation();
	}

	public Rectangle getToolbarBounds() {
		return toolbar.getBounds();
	}

	public Collection<Column> getColumns() {
		return this.mpColumns.values();
	}

	public Collection<UID> getColumnUIDs() {
		return this.mpColumns.keySet();
	}

	public AbstractButton getToolbarButton(String function) {
		return toolbarButtons.get(function);

	}
	public MenuElement getMenuItem(String function) {
		return toolbarMenuItems.get(function);
	}

	//--- end needed by the wysiwyg editor only --------------------------------

	public void setLockedLayer() {
		setLockedLayer(null);
	}

	public void setLockedLayer(String text) {
		if(lockCount.incrementAndGet() == 1) {
			if(layer != null && !((LockableUI) layer.getUI()).isLocked()) {
				((LockableUI) layer.getUI()).setLocked(true);
				if (text != null) {
					((TranslucentLockableUI) layer.getUI()).setText(text);
				}
			}
		}
	}

	public void forceUnlockFrame() {
		lockCount.set(0);
		((TranslucentLockableUI) layer.getUI()).setText(null);
		((LockableUI) layer.getUI()).setLocked(false);
	}


	private void init() {
		contentPane.add(toolbar,
			toolbar.getOrientation() == JToolBar.HORIZONTAL
			? BorderLayout.NORTH
			: BorderLayout.WEST);

		// Configure table
		scrollPane.getViewport().setBackground(subformtbl.getBackground());
		subformtbl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		subformtbl.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		scrollPane.getViewport().setView(subformtbl);
		JLabel labCorner = new JLabel();
		labCorner.setEnabled(false);
		labCorner.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.GRAY));
		labCorner.setBackground(Color.LIGHT_GRAY);
		scrollPane.setCorner(ScrollPaneConstants.UPPER_LEFT_CORNER, labCorner);

		rowHeader = createTableRowHeader(subformtbl, scrollPane);
		subformtbl.setRowHeaderTable(rowHeader);

		if (!bLayout) {
			addToolbarMouseListener(subformtbl, subformtbl, subformtbl);
			addToolbarMouseListener(scrollPane.getViewport(), scrollPane.getViewport(), subformtbl);
		}
	}

	private void addToolbarMouseListener(JComponent src, JComponent parent, JTable table) {
		final MouseListener ml = newToolbarContextMenuListener(parent, table);
		src.addMouseListener(ml);
		mouseListener.add(new Pair<JComponent,MouseListener>(src, ml));
	}
	
	private class StandardPopupMenuMouseListener extends MouseAdapter {
		
		private final JComponent parent;
		
		private final JTable table;
		
		private StandardPopupMenuMouseListener(final JComponent parent, final JTable table) {
			this.parent = parent;
			this.table = table;
		}
		
		@Override
		public void mouseClicked(MouseEvent mev) {
			if (SwingUtilities.isRightMouseButton(mev)) {
				int row = table.rowAtPoint(mev.getPoint());
				int column = table.columnAtPoint(mev.getPoint());
				
				if(row !=-1 && column!=-1){
					return;
				}
			
				List<JComponent> items = new ArrayList<JComponent>();
				addToolbarMenuItems(items);
				if (items.isEmpty())
					return;

				JPopupMenu popup = new JPopupMenu();
				for (JComponent c : items)
					popup.add(c);
				popup.show(parent, mev.getX(), mev.getY());
			}
			if (SwingUtilities.isLeftMouseButton(mev) && mev.getClickCount() == 2) {
				int row = table.rowAtPoint(mev.getPoint());
				int column = table.columnAtPoint(mev.getPoint());
				LOG.info(StringUtils.concat("Doubleclick on subform: column=",column,",row=",row));
				if (row == -1 || column == -1) {
					if (((AbstractButton) toolbarMenuItems.get(ToolbarFunction.NEW.name())).isEnabled()) {
						actionPerformed(ToolbarFunction.NEW.name());
					}
				}
			}
		}
	}

	/**
	 * @Deprecated Never use this directly, instead use {@link #addToolbarMenuItems(List)}.
	 */
	private MouseListener newToolbarContextMenuListener(final JComponent parent, final JTable table) {
		final MouseListener res = new StandardPopupMenuMouseListener(parent, table);
		return res;
	}

	/**
	 * do not store items permanent!
	 * @param result
	 */
	public void addToolbarMenuItems(List<JComponent> result) {
		for (String actionCommand : toolbarOrder) {
			result.add((JComponent)toolbarMenuItems.get(actionCommand));
		}
	}
	
	public void removeStandardMenuPopup() {
		for (MouseListener l: subformtbl.getMouseListeners()) {
			if (l instanceof StandardPopupMenuMouseListener) {
				subformtbl.removeMouseListener(l);
			}
		}
		for (MouseListener l: scrollPane.getMouseListeners()) {
			if (l instanceof StandardPopupMenuMouseListener) {
				subformtbl.removeMouseListener(l);
			}
		}
	}

	public boolean isLayout() {
		return bLayout;
	}

	/**
	 * @param tbl the table to add the header to
	 * @param scrlpnTable §todo what is this?
	 */
	protected SubformRowHeader createTableRowHeader(final SubFormTable tbl, JScrollPane scrlpnTable) {
		return new SubformRowHeader(tbl, scrlpnTable);
	}

	public void setTableRowHeader(SubformRowHeader newTableRowHeader) {
		this.rowHeader = newTableRowHeader;
		this.subformtbl.setRowHeaderTable(rowHeader);
		newTableRowHeader.setExternalTable(subformtbl, scrollPane);

		addToolbarMouseListener(rowHeader.getHeaderTable(), rowHeader.getHeaderTable(), rowHeader.getHeaderTable());
		addToolbarMouseListener(scrollPane.getRowHeader(), scrollPane.getRowHeader(), rowHeader.getHeaderTable());
	}

	public SubformRowHeader getSubformRowHeader() {
		return this.rowHeader;
	}

	public JTable getJTable() {
		return this.subformtbl;
	}

	public void setRowHeight(int iHeight) {
		int tableHeaderHeight = iHeight;
		if (DYNAMIC_ROW_HEIGHTS == iHeight) {
			this.subformtbl.setRowHeight(getMinRowHeight());
			tableHeaderHeight = getMinRowHeight();
			this.dynamicRowHeights = true;
			this.rowHeightCtrl.clear();
			this.subformtbl.updateRowHeights();
		} else {
			this.dynamicRowHeights = false;
			this.subformtbl.setRowHeight(iHeight);
		}
		
		// Increase the TableHeader height for MetalLookAndFeel
		if (UIManager.getLookAndFeel() instanceof MetalLookAndFeel) {
			Dimension d = this.subformtbl.getTableHeader().getPreferredSize();
			d.setSize(d.getWidth(), tableHeaderHeight);
			this.subformtbl.getTableHeader().setPreferredSize(d);
		}
	}

	@Override
	public Dimension getMinimumSize() {
		Dimension result = super.getMinimumSize();
		result.height = Math.max(result.height, 100);

		return result;
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the name of this subform's entity.
	 */
	public UID getEntityUID() {
		return this.entityUid;
	}

	/**
	 * @return the foreign key field that references the parent object. May be null for convenience. In that case,
	 * the SubFormController will try to find the field from the field meta information. This is only possible if
	 * there is only one field in the subform's entity that references the parent entity.
	 */
	public final UID getForeignKeyFieldToParent() {
		return this.foreignKeyFieldToParent;
	}
	
	public void setMultiEdit(boolean multiEdit) {
		this.bMultiEdit = multiEdit;	
		
		for(CollectableEntityField cf : mpStaticColumnEditors.keySet()) {
			CollectableComponentTableCellEditor edit = mpStaticColumnEditors.get(cf);
			CollectableComponent comp = edit.getCollectableComponent();
			if(comp instanceof LabeledCollectableComponentWithVLP) {
				LabeledCollectableComponentWithVLP box =(LabeledCollectableComponentWithVLP)comp;
				box.setMultiEditInSubform(bMultiEdit);
			}
		}
	}

	public boolean isMultiEdit()  {
		return this.bMultiEdit;
	}

	public boolean isMultieditable() {
		return bMultieditable;
	}

	public void setMultieditable(boolean bMultieditable) {
		this.bMultieditable = bMultieditable;
	}
	
	public boolean isShowStatusbar() {
		return showStatusbar;
	}

	public void setShowStatusbar(final boolean showStatusbar) {
		this.showStatusbar = showStatusbar;
		if (statusbar != null) {
			statusbar.setVisible(showStatusbar);
		}
	}

	public void setOpenDetailsWithTabRecycling(final boolean openDetailsWithTabRecycling) {
		this.openDetailsWithTabRecycling = openDetailsWithTabRecycling;
	}

	public Boolean getOpenDetailsWithTabRecycling() {
		return this.openDetailsWithTabRecycling;
	}

	/**
	 * Method needed for WYSIWYG Editor
	 * NUCLEUSINT-265
	 * @return
	 */
	public SubFormTable getSubformTable() {
		return this.subformtbl;
	}

	public TableCellEditorProvider getTableCellEditorProvider() {
		return this.subformtbl.getTableCellEditorProvider();
	}

	public void setTableCellEditorProvider(TableCellEditorProvider celleditorprovider) {
		this.subformtbl.setTableCellEditorProvider(celleditorprovider);
	}

	public TableCellRendererProvider getTableCellRendererProvider() {
		return this.subformtbl.getTableCellRendererProvider();
	}

	public void setTableCellRendererProvider(TableCellRendererProvider cellrendererprovider) {
		this.subformtbl.setTableCellRendererProvider(cellrendererprovider);
	}

	public void endEditing() {
		if (subformtbl.getCellEditor() != null) {
			subformtbl.getCellEditor().stopCellEditing();
		}
		this.rowHeader.endEditing();
		this.rowHeightCtrl.clearEditorHeight();
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		setEnabledState(enabled && !readonly && enabledByLayout);
	}

	public void setEnabledByLayout(boolean enabled) {
		this.enabledByLayout = enabled;
	}
	
	private void setEnabledState(boolean enabled) {
		super.setEnabled(enabled);
		fireEnabledChanged(enabled);
	}

	public void setNewEnabled(ScriptContext sc) {
		boolean enabled = this.enabled && !readonly && enabledByLayout && canCreate();
		if (enabled && getNewEnabledScript() != null) {
			Object o;
			try {
				if (sc instanceof SubformControllerScriptContext) {
					SubformControllerScriptContext sfcsc = (SubformControllerScriptContext) sc;
					if (sfcsc.getCollectable() == null) {
						// set collectable from collect controllers edit panel...
						LOG.debug("readCollectableWithoutDepedents for new SubformControllerScriptContext...");
						final Collectable clctDelegate = 
								sfcsc.getParentCollectController() != null ? sfcsc.getParentCollectController().getReadDelegateCollectable() : null;
						if (clctDelegate != null) {
							sc = new SubformControllerScriptContext(
									sfcsc.getParentCollectController(),
									sfcsc.getParentSubFormController(),
									sfcsc.getSubFormController(),
									clctDelegate
							);
						}
					}
				}
				o = ScriptEvaluator.getInstance().eval(getNewEnabledScript(), sc);
			} catch (InvocationTargetException e) {
				LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
				o = false;
			} catch (Exception e) {
				LOG.warn("Failed to evaluate script expression: " + e, e);
				o = false;
			}
			if (o instanceof Boolean) {
				enabled = (Boolean) o;
			}
		}
		final ToolbarFunctionState state = enabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED;
		setToolbarFunctionState(ToolbarFunction.NEW, state);
		setToolbarFunctionState(ToolbarFunction.NEW_AT_POSITION, state);
		// FIXME it's of kind "NEW" it doesn't belong here
		setToolbarFunctionState(ToolbarFunction.PASTE_ROW, state);
		setToolbarFunctionState(ToolbarFunction.CUT_ROW, state);
		
		if (getToolbarManager().getStateIfAvailable(ToolbarFunction.DOCUMENTIMPORT) == null 
				|| getToolbarManager().getStateIfAvailable(ToolbarFunction.DOCUMENTIMPORT) != ToolbarFunctionState.HIDDEN)
			setToolbarFunctionState(ToolbarFunction.DOCUMENTIMPORT, enabled ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
	}

	public void setReadOnly(boolean readonly) {
		this.readonly = readonly;
		setEnabledState(enabled && !readonly);
	}
	
	public void setReadOnlyWithDetailView(boolean readonly, boolean detailView) {
		this.readonly = readonly;
		this.detailViewReadOnlyAvailable = detailView;
		setEnabledState(enabled && !readonly);
	}

	public boolean isReadOnly() {
		return readonly;
	}

	public boolean isDetailViewReadOnlyAvailable() {
		return detailViewReadOnlyAvailable;
	}

	private void fireEnabledChanged(boolean bEnabled) {
		for (EnabledListener el : lstEnabledListeners) {
			el.enabledChanged(bEnabled);
		}
	}

	private boolean create = true;

	/**
	 *
	 * @param b
	 * @return true if boolean has been changed from false to true
	 */
	public boolean setCreate(boolean b) {
		boolean enabledCreateFromDisabled = !this.create && b;
		this.create = b;
		return enabledCreateFromDisabled;
	}

	public boolean canCreate() {
		return this.create;
	}

	private boolean delete = true;

	public void setDelete(boolean b) {
		this.delete = b;
	}

	public boolean canDelete() {
		return delete;
	}

	private SubformPermission subformPermission;

	public boolean setSubformPermission(SubformPermission sp) {
		if (sp != this.subformPermission) {
			this.subformPermission = sp;
			return true;
		}
		return false;
	}

	public SubformPermission getSubformPermission() {
		return this.subformPermission;
	}

	/**
	 * cloneable, that is makes it accessible (or not).
	 * @param bCloneable
	 */
	public void setCloneable(boolean bCloneable) {
		this.bCloneable = bCloneable;
	}

	/**
	 * Is this component cloneable?
	 * @return
	 */
	public boolean isCloneable() {
		return bCloneable;
	}

	public void setCollectableComponentFactory(CollectableComponentFactory collectableComponentFactory) {
		this.collectableComponentFactory = collectableComponentFactory;
	}

	/**
	 * @param listener
	 */
	public synchronized void addChangeListener(ChangeListener listener) {
		this.lstchangelistener.add(listener);
	}

	/**
	 * @param listener
	 */
	public synchronized void removeChangeListener(ChangeListener listener) {
		this.lstchangelistener.remove(listener);
	}

	/**
	 * sets the detailsChangedIgnored property.
	 * TODO: move to DetailsController
	 * 
	 * §postcondition isDetailsChangedIgnored() == bDetailsChangedIgnored
	 * 
	 * @param bDetailsChangedIgnored
	 * @see #isDetailsChangedIgnored()
	 */
	public final void setDetailsChangedIgnored(boolean bDetailsChangedIgnored) {
		this.bDetailsChangedIgnored = bDetailsChangedIgnored;
	}

	/**
	 * @return Is detailsChanged() ignored? If so, detailsChanged() won't be called when the values of
	 *         <code>CollectableComponent</code>s change.
	 * TODO move to DetailsController
	 */
	public final boolean isDetailsChangedIgnored() {
		return this.bDetailsChangedIgnored;
	}

	/**
	 * fires a <code>ChangeEvent</code> whenever the model of this <code>SubForm</code> changes.
	 */
	public synchronized void fireStateChanged(ChangeEvent ev) {
		if(layer == null || (layer != null && !((LockableUI) layer.getUI()).isLocked())){
			if (!isDetailsChangedIgnored()) {
				for (ChangeListener changelistener : lstchangelistener) {
					changelistener.stateChanged(ev);
				}
			}
		}
	}

	public void fireFocusGained() {
		AWTEvent event = EventQueue.getCurrentEvent();
		if(event instanceof KeyEvent) {
			if(getJTable().getModel().getRowCount() > 0) {
				getJTable().editCellAt(0, 0);
				getSubformTable().changeSelection(0, 0, false, false);
			}
			else if(getJTable().getModel().getRowCount() == 0) {
				for(FocusActionListener fal : getFocusActionLister()) {
					fal.focusAction(new EventObject(this));					
				}
			}
			
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					Component editor = getJTable().getEditorComponent();
					
					if (editor != null){
						editor.requestFocusInWindow();
					}
					else if(getJTable().getModel().getRowCount() > 0) {					
						getJTable().requestFocusInWindow();
					}
				}
			});
		}
	}

	/**
	 * @param column
	 */
	public void addColumn(Column column) {
		this.mpColumns.put(column.getName(), column);
	}

	public Column getColumn(UID sColumnName) {
		return this.mpColumns.get(sColumnName);
	}

	/**
	 * Handling of intial sorting order (from layoutml).
	 *
	 * @deprecated Sorting order is persisted into user preferences, so why is this? (tp)
	 */
	public UID getInitialSortingColumn() {
		return this.sInitialSortingColumn;
	}

	public String getInitialSortingOrder() {
		return this.sInitialSortingOrder;
	}

	public void setInitialSortingOrder(UID sColumnName, String sInitialSortingOrder) {
		this.sInitialSortingColumn = sColumnName;
		this.sInitialSortingOrder = sInitialSortingOrder;
	}
	
	private Font font;
	public void setFont(Font font) {
		this.font = font;
	}
	
	public Font getFont() {
		return font != null ? font : super.getFont();
	}
	
	public int getMinRowHeight() {
		if (font == null)
			return MIN_ROWHEIGHT;
		int result = 4;
		result += getFontMetrics(font).getHeight();
		if (subformtbl != null) {
			result += subformtbl.getRowMargin();
		}
		else {
			LOG.info("getMinRowHeight: subformtbl is null"); 
		}
		return Math.max(result, MIN_ROWHEIGHT);
	}

	/**
	 * @param sColumnName
	 * @return the <code>CollectableComponentType</code> of the column with the given name (default: null).
	 */
	public CollectableComponentType getCollectableComponentType(UID sColumnName, boolean bSearchable) {
		final Column column = this.getColumn(sColumnName);
		return (column != null) ? column.getCollectableComponentType() : null;
	}

	/**
	 * @param sColumnName
	 * @return Is the column with the given name visible? (default: true)
	 */
	public boolean isColumnVisible(UID sColumnName) {
		final Column column = this.getColumn(sColumnName);
		return (column == null) || column.isVisible();
	}
	
	public boolean isLovSearch(UID sColumnName) {
		final Column column = this.getColumn(sColumnName);
		return (column == null) || column.isLovSearch();
	}

	/**
	 * @return Is the column with the given name enabled? (default: true)
	 */
	public boolean isColumnEnabled(UID columnUid) {
		// NUCLOS-6134
		Permission permission = getColumnPermission(columnUid);

		boolean isModifiable = true;
		try {
			isModifiable = MetaProvider.getInstance().getEntityField(columnUid).isModifiable();
		} catch (CommonFatalException e) {
			//do nothing
		}

		if (permission == null || !permission.includesWriting() || !isModifiable) {
			return false;
		}

		final Column column = this.getColumn(columnUid);
		if (column == null) {
			return !SF.isEOField(getEntityUID(), columnUid);
		}

		return column.isEnabled();
	}

	/**
	 * @param sColumnName
	 * @return Is the column with the given name insertable? (default: false)
	 */
	public boolean isColumnInsertable(UID sColumnName) {
		final Column column = this.getColumn(sColumnName);
		return (column != null) && column.isInsertable();
	}

	public String getColumnLabel(UID sColumnName) {
		final Column column = this.getColumn(sColumnName);
		return (column != null) ? column.getLabel() : null;
	}

	/**
	 * Returns the column width from the layout, or null if unspecified.
	 */
	public Integer getColumnWidth(UID cColumnName) {
		final Column column = this.getColumn(cColumnName);
		return (column != null) ? column.getWidth() : null;
	}

	/**
	 * Returns the column nextfocus component from the layout, or null if unspecified.
	 */
	public UID getColumnNextFocusField(UID cColumnName) {
		final Column column = this.getColumn(cColumnName);
		return (column != null) ? column.getNextFocusField() : null;
	}

	/**
	 * @param sColumnName
	 * @return Collection&lt;TransferLookedUpValueAction&gt; the TransferLookedUpValueActions 
	 * 		defined for the column with the given name (default: empty collection).
	 */
	public Collection<TransferLookedUpValueAction> getTransferLookedUpValueActions(UID sColumnName) {
		// look-up actions belong to the column that perform the look-up (i.e. the LOV/combobox)
		final Column column = this.getColumn(sColumnName);
		return (column != null) ? column.getTransferLookedUpValueActions() : Collections.<TransferLookedUpValueAction>emptySet();
	}

	/**
	 * @param sColumnName
	 * @return Collection&lt;ClearAction&gt; the ClearActions defined for the 
	 * 		column with the given name (default: empty collection).
	 */
	public Collection<ClearAction> getClearActions(UID sColumnName) {
		// clear actions belong to the column that perform the look-up (i.e. the LOV/combobox)
		final Column column = this.getColumn(sColumnName);
		return (column != null) ? column.getClearActions() : Collections.<ClearAction>emptySet();
	}

	/**
	 * @param sColumnName
	 * @return Collection&lt;RefreshValueListAction&gt; the RefreshValueListActions 
	 * 		defined for the column with the given name (default: empty 
	 * 		collection).
	 */
	public Collection<RefreshValueListAction> getRefreshValueListActions(UID sColumnName) {
		// refresh actions belong to the column that needs the refresh (_not_ to the column that triggers the refresh)
		final Column column = this.getColumn(sColumnName);
		return (column != null) ? column.getRefreshValueListActions() : Collections.<RefreshValueListAction>emptySet();
	}

	public CollectableFieldsProvider getValueListProvider(UID sColumnName) {
		final Column column = this.getColumn(sColumnName);
		return (column != null) ? column.getValueListProvider() : null;
	}

	public SizeKnownListener getSizeKnownListener() {
		return sizeKnownListener;
	}

	public void setSizeKnownListener(SizeKnownListener skl) {
		sizeKnownListener = skl;
	}

	/**
	 * @see #getUniqueMasterColumnUid()
	 */
	public void setUniqueMasterColumnUid(UID uniqueMasterColumnUid) {
		this.uniqueMasterColumnUid = uniqueMasterColumnUid;

		if(uniqueMasterColumnUid != null)
			setToolbarFunctionState(ToolbarFunction.MULTIEDIT, isEnabled() ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
		else
			setToolbarFunctionState(ToolbarFunction.MULTIEDIT,  ToolbarFunctionState.HIDDEN);
	}

	/**
	 * @return the name of the unique master column, if any. This is used for the multi edit facility.
	 */
	public UID getUniqueMasterColumnUid() {
		return this.uniqueMasterColumnUid;
	}

	/**
	 * set the parent subform for this subform, if the data of this subform depends
	 * on the selected row of the parent subform instead on the curent collectable
	 * @param sSubFormParent
	 */
	public void setParentSubForm(UID sSubFormParent) {
		this.sSubFormParent = sSubFormParent;
	}

	/**
	 * @return parent subform for this subform
	 */
	public UID getParentSubForm() {
		return this.sSubFormParent;
	}

	/**
	 * sets the controller type for this subform. This is used to create an appropriate controller for this subform.
	 * @param sControllerType
	 */
	public void setControllerType(String sControllerType) {
		this.sControllerType = sControllerType;
	}

	/**
	 * @return the controller type for this subform. This is used to create an appropriate controller for this subform.
	 * null == "default": Use default controller. Otherwise use a special controller.
	 */
	public String getControllerType() {
		return this.sControllerType;
	}

	public IPopupListener getPopupMenuListener() {
		return popupMenuListener;
	}

	public void setPopupMenuListener(IPopupListener popupMenuListener) {
		this.popupMenuListener = popupMenuListener;
	}

	@Override
    public final TableCellRenderer getTableCellRenderer(CollectableEntityField clctef) {
		return this.mpColumnRenderer.get(clctef);
	}
    
	private ISubformHistoricalViewLookUp historyLookUp;
	   
	public void setupHistoryLookUp(ISubformHistoricalViewLookUp historyLookUp) {
		this.historyLookUp = historyLookUp;
	}
	
	public ISubformHistoricalViewLookUp getSubformHistoricalViewLookUp() {
		return this.historyLookUp;
	}

	private Permission getColumnPermission(UID columnUid) {
		try {
			UID sGroup = MetaProvider.getInstance().getEntityField(columnUid).getFieldGroup();
			return subformPermission != null ? subformPermission.getGroupPermission(sGroup) : Permission.READWRITE;
		} catch (CommonFatalException cfe) {
			// NUCLOS-6180 This "columnUid" does not exist in the meta-cache, so do not restrict the permissions for it.
		}
		return Permission.READWRITE;
	}
	
	public final void setupTableCellRenderers(CollectableEntity clcte, CollectableEntityFieldBasedTableModel subformtblmdl, CollectableFieldsProviderFactory clctfproviderfactory, final SubFormParameterProvider parameterProvider, boolean bSearchable) {
		//Check if it is a systemEntity as the combo-boxes in subforms still need the combo-box-renderer, because ValueIDFields don't have the string-representation
		//TOOD: Change nuclos-core for system-combo-boxes having the String-representation, too.
		boolean bSystemEntity = MetaProvider.getInstance().isNuclosEntity(clcte.getUID());

		// setup a table cell renderer for each column:
		for (int iColumnNr = 0; iColumnNr < subformtblmdl.getColumnCount(); iColumnNr++) {
			final CollectableEntityField clctef = subformtblmdl.getCollectableEntityField(iColumnNr);

			final UID sColumnName = clctef.getUID();
			final CollectableComponentType clctcomptype;

			Permission permission = getColumnPermission(sColumnName);
			// NUCLOS-6183 Do not exclude system fields
			if ((permission == null || !permission.includesReading()) && !SF.isEOField(clctef.getEntityUID(), sColumnName)) {
				clctcomptype = new CollectableComponentType(CollectableComponentTypes.TYPE_HIDDEN, null);
			}
			else if (sColumnName.equals(this.getForeignKeyFieldToParent())) {
				// in some cases a column for this foreign key field is created. But if it is a ComboBox, refreshValueList() later will load all values from server... bad thing...
				clctcomptype = new CollectableComponentType(CollectableComponentTypes.TYPE_LISTOFVALUES, null);
			} else {
				clctcomptype = this.getCollectableComponentType(sColumnName, bSearchable);
			}
			
			final CollectableComponent clctcomp = CollectableComponentFactory.getInstance().newCollectableComponent(clctef, clctcomptype, bSearchable, !bSystemEntity, true);
			if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
				final LabeledCollectableComponentWithVLP clctWithVLP = (LabeledCollectableComponentWithVLP) clctcomp;
				CollectableFieldsProvider valuelistprovider = getValueListProvider(sColumnName);
				if (valuelistprovider == null) {
					valuelistprovider = clctfproviderfactory.newDefaultCollectableFieldsProvider(clctWithVLP.getFieldUID());
				}
				clctWithVLP.setValueListProvider(valuelistprovider);
				if (!bLayout)
					clctWithVLP.refreshValueList(true);
				SwingUtilities.invokeLater(new Runnable() {
					
					@Override
					public void run() {
	    				final Collection<RefreshValueListAction> collRefreshValueListActions = getRefreshValueListActions(clctWithVLP.getFieldUID());
	    				if (!collRefreshValueListActions.isEmpty()) {
		    				// set the value list provider (dynamically):
		    				CollectableFieldsProvider valuelistprovider = getValueListProvider(clctWithVLP.getFieldUID());
		    				if (valuelistprovider != null) {
			    				clctWithVLP.setValueListProvider(valuelistprovider);
			
			    				// set parameters:
			    				for (RefreshValueListAction rvlact : collRefreshValueListActions) {
			    					SubForm.setParameterForRefreshValueListAction(rvlact, -1, clctWithVLP, (SubFormTableModel)getSubformTable().getModel(), parameterProvider);					
			    				}

			    	            JTextComponent compText = null;
			    	            JComponent comp = clctcomp.getControlComponent();
			    	            if (comp instanceof ListOfValues) {
			    	            	compText = ((ListOfValues)comp).getJTextField();
			    	            } else if (comp instanceof JComboBox) {
			    	            	compText = (JTextComponent)((JComboBox)comp).getEditor().getEditorComponent();
			    	            }   

			    	            // remember old value here. 
			    				String clctfValue = compText.getText();
			    				// refresh value list:
								clctWithVLP.refreshValueList(false);
			    				compText.setText(clctfValue);
		    				}
	    				}
					}
				});
			}
			
			if(clctcomp instanceof LabeledCollectableComponentWithVLP) {
				LabeledCollectableComponentWithVLP box = (LabeledCollectableComponentWithVLP)clctcomp;
				box.setMultiEditInSubform(bMultiEdit);
			}
			
			final Column subformcolumn = this.getColumn(sColumnName);
			if (subformcolumn != null) {
				final Integer iRows = subformcolumn.getRows();
				if (iRows != null) {
					clctcomp.setRows(iRows);
				}
				final Integer iColumns = subformcolumn.getColumns();
				if (iColumns != null) {
					clctcomp.setColumns(iColumns);
				}
				final Map<String, Object> properties = subformcolumn.getProperties();
				for (String property : properties.keySet()) {
					clctcomp.setProperty(property, properties.get(property));
				}
			}
			clctcomp.setToolTipText(clctef.getDescription());

			final JComponent c = clctcomp.getJComponent();
			if (c instanceof LabeledComponent) {
				((LabeledComponent) c).getLabeledComponentSupport().setColorProvider(null);
			}
			mpColumnRenderer.put(clctef, clctcomp.getTableCellRenderer(true));
		}
	}

	/**
	 * sets all column widths to user preferences; set optimal width if no preferences yet saved
	 * @param tableColumnWidthsFromPreferences
	 */
	public final void setColumnWidths(List<Integer> tableColumnWidthsFromPreferences) {
		useCustomColumnWidths = !tableColumnWidthsFromPreferences.isEmpty() && tableColumnWidthsFromPreferences.size() <= subformtbl.getColumnCount();
		if (useCustomColumnWidths) {
			assert(tableColumnWidthsFromPreferences.size() <= subformtbl.getColumnCount());
			final Enumeration<TableColumn> enumeration = subformtbl.getColumnModel().getColumns();
			int iColumn = 0;
			while (enumeration.hasMoreElements()) {
				final TableColumn column = enumeration.nextElement();
				final int iPreferredCellWidth;
				if (iColumn < tableColumnWidthsFromPreferences.size()) {
					// known column
					iPreferredCellWidth = tableColumnWidthsFromPreferences.get(iColumn++);
				} else {
					// new column
					iPreferredCellWidth = getDefaultColumnWidth(column, iColumn++);
				}
				column.setPreferredWidth(iPreferredCellWidth);
				column.setWidth(iPreferredCellWidth);
			}
		}
		else {
			resetDefaultColumnWidths();
		}
	}

	public final void resetDefaultColumnWidths() {
		if (subformtbl != null) {
			// Because of LIN-199, it is now possible that all rows have been filtered (tp)
			if (subformtbl.getRowCount() > 0) {
				for (int iColumn = 0; iColumn < subformtbl.getColumnCount(); iColumn++) {
					final TableColumn tableColumn = subformtbl.getColumnModel().getColumn(iColumn);
					final int width = getDefaultColumnWidth(tableColumn, iColumn);
					if (width != tableColumn.getWidth()) {
						tableColumn.setPreferredWidth(width);
						tableColumn.setWidth(width);
					}
				}
			}
			useCustomColumnWidths = false;
			subformtbl.revalidate();
		}
	}


	private int getDefaultColumnWidth(TableColumn tc, int iColumn) {
		final Integer preferredCellWidth = tc.getIdentifier()==null?null:getColumnWidth((UID)tc.getIdentifier());
		
		final int width = (preferredCellWidth != null)
			? preferredCellWidth
			: Math.max(TableUtils.getPreferredColumnWidth(subformtbl, iColumn, 50, TableUtils.TABLE_INSETS), subformtbl.getSubFormModel().getMinimumColumnWidth(iColumn));
		return width;
	}

	public void setupTableModelListener() {
		this.tblmdllistener = new TableModelListener() {
			@Override
            public void tableChanged(TableModelEvent ev) {
				// recalculate the optimal column widths when custom column widths are not used yet and
				// a row is inserted (the first time):
				// §todo: this here is entered not only the first time a row is entered but also on startup (sort fires a tableDataChanged)
				if (!useCustomColumnWidths) {
					// Now this is an example for an API that sucks :(
					final boolean bRowsInserted = (ev.getType() == TableModelEvent.INSERT) ||
							// §todo: The first condition (INSERT) is clear, but what for the second (complete UPDATE)?
							(ev.getType() == TableModelEvent.UPDATE && ev.getColumn() == TableModelEvent.ALL_COLUMNS && ev.getLastRow() == Integer.MAX_VALUE);
					if (bRowsInserted) {
						resetDefaultColumnWidths();
						LOG.debug("Custom column widths should be used here.");	// Setting them manually.");
					}
				}
				// TableModelEvents caused by sorting don't change the subform state:
				if (!(ev instanceof SortableTableModelEvent)) {
					fireStateChanged(new ChangeEvent(SubForm.this));
				}
				
				if(getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != null 
						&& getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != ToolbarFunctionState.HIDDEN) {
					setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, getSubformTable().getSelectedRowCount() > 0
							? getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) : ToolbarFunctionState.DISABLED);
				}
			}
		};
		subformtbl.getModel().addTableModelListener(this.tblmdllistener);
	}

	public void removeTableModelListener() {
		subformtbl.getModel().removeTableModelListener(this.tblmdllistener);
		this.tblmdllistener = null;
	}

	public final void setupColumnModelListener() {
		this.columnmodellistener = new TableColumnModelAdapter() {
			@Override
            public void columnMoved(TableColumnModelEvent ev) {
				// workaround for JTable flaw:
				if (subformtbl.isEditing())
					subformtbl.getCellEditor().stopCellEditing();
			}
			@Override
			public void columnMarginChanged(ChangeEvent e) {
				useCustomColumnWidths = true;
			}
		};
		subformtbl.getColumnModel().addColumnModelListener(this.columnmodellistener);
	}

	public void removeColumnModelListener() {
		subformtbl.getColumnModel().removeColumnModelListener(this.columnmodellistener);
		this.columnmodellistener = null;
	}

	
	/**
	 * Implementation of <code>TableCellEditorProvider</code>.
	 * 
	 * §todo move back to SubFormController
	 * 
	 * @param tbl
	 * @param iRow row of the table (not the table model)
	 * @param clcte
	 * @param clctefTarget
	 * @param subformtblmdl
	 * @param bSearchable
	 * @param prefs
	 * @param getCollectableFieldsProviderFactory
	 * @param parameterProvider
	 * @return a <code>TableCellEditor</code> for columns that need a dynamic <code>TableCellEditor</code>.
	 * <code>null</code> for all other columns.
	 */
	public TableCellEditor getTableCellEditor(JTable tbl, int iRow, CollectableEntity clcte, CollectableEntityField clctefTarget,
			final SubFormTableModel subformtblmdl, boolean bSearchable, Preferences prefs,
			CollectableFieldsProviderFactory getCollectableFieldsProviderFactory, SubFormParameterProvider parameterProvider, UID mandator, Long intidForVLP) {
		CollectableComponentTableCellEditor result;

		final UID sColumnNameTarget = clctefTarget.getUID();

		if (!this.isDynamicTableCellEditorNeeded(sColumnNameTarget)) {
			result = this.mpStaticColumnEditors.get(clctefTarget);
			final CollectableComponent ccmp = result==null ? null : result.getCollectableComponent();
			// CollectableComponent may be LOV when validity tolerance mode is activated
			if(ccmp instanceof LabeledCollectableComponentWithVLP) {
				CollectableFieldsProvider vlp = ((LabeledCollectableComponentWithVLP) ccmp).getValueListProvider();
				
				if (VLPClientUtils.setVLPBaseRestrictions(ccmp.getFieldUID(), vlp, intidForVLP, mandator)) {
					((LabeledCollectableComponentWithVLP)ccmp).refreshValueList(true, getSubformTable().getSelectedRow() == iRow);
				}
			}
			else if (ccmp instanceof CollectableLocalizedTextField) {
				if (getLookupService() != null) {
					getLookupService().setCollectableLocalizedComponent((CollectableLocalizedTextField)ccmp, iRow);
				}
			}
			else if (ccmp instanceof NuclosCollectableLocalizedTextArea) {
				if (getLookupService() != null) {
					getLookupService().setCollectableLocalizedComponent((NuclosCollectableLocalizedTextArea)ccmp, iRow);
				}
			}
		}
		else {
			// We need a dynamic CellEditor:
			result = this.newTableCellEditor(clcte, sColumnNameTarget, bSearchable, prefs, subformtblmdl);

			final CollectableComponent ccmp = result.getCollectableComponent();
			// CollectableComponent may be LOV when validity tolerance mode is activated
			if(ccmp instanceof LabeledCollectableComponentWithVLP) {
				final LabeledCollectableComponentWithVLP clctWithVLP = (LabeledCollectableComponentWithVLP) ccmp;

				final Collection<RefreshValueListAction> collRefreshValueListActions = getRefreshValueListActions(sColumnNameTarget);
				assert !collRefreshValueListActions.isEmpty();

				// check that the value list provider was not set:
				assert clctWithVLP.getValueListProvider() == null;

				// set the value list provider (dynamically):
				CollectableFieldsProvider valuelistprovider = this.getValueListProvider(sColumnNameTarget);
				if (valuelistprovider == null) {
					// If no provider was set, use the dependant provider for dynamic cell editors by default:
					valuelistprovider = getCollectableFieldsProviderFactory.newDependantCollectableFieldsProvider(clctWithVLP.getEntityField().getUID());
				}
				clctWithVLP.setValueListProvider(valuelistprovider);
				
				// set parameters:
				for (RefreshValueListAction rvlact : collRefreshValueListActions) {
					setParameterForRefreshValueListAction(rvlact, iRow, clctWithVLP, subformtblmdl, parameterProvider);
				}

				// refresh value list:
				if (VLPClientUtils.setVLPBaseRestrictions(ccmp.getFieldUID(), valuelistprovider, intidForVLP, mandator) ||
						(!bLayout && !(valuelistprovider instanceof DatasourceBasedCollectableFieldsProvider)) ) {
					clctWithVLP.refreshValueList(true, getSubformTable().getSelectedRow() == iRow);
				}
			}
		}
		if (result != null) {
			CollectableComponent ccmp = result.getCollectableComponent();
			if(ccmp instanceof LabeledCollectableComponentWithVLP) {
				LabeledCollectableComponentWithVLP bx = (LabeledCollectableComponentWithVLP)ccmp;
				bx.setMultiEditInSubform(bMultiEdit);
			}
		}

		return result;
	}

	public static void setParameterForRefreshValueListAction(RefreshValueListAction rvlact, int iRow,
															 LabeledCollectableComponentWithVLP clctWithVLP, SubFormTableModel subformtblmdl, SubFormParameterProvider parameterProvider) {
		if (iRow < 0) {
			return;
		}
		try {
			final UID sParentComponentName = rvlact.getParentComponentUID();
			final UID sParentComponentEntityName = rvlact.getParentComponentEntityUID();
			final CollectableField clctfParent = parameterProvider.getParameterForRefreshValueList(subformtblmdl, iRow, sParentComponentName, sParentComponentEntityName);
			Object oValue = (clctfParent.getFieldType() == CollectableField.TYPE_VALUEIDFIELD) ? clctfParent.getValueId() : clctfParent.getValue();
			clctWithVLP.getValueListProvider().setParameter(rvlact.getParameterNameForSourceComponent(), oValue);	
		} catch (Exception e) {
			LOG.warn("error setParameterForRefreshValueListAction: " + e.getMessage());
		}
	}

	/**
	 * create a Tablecelleditor for the given CollectableEntityField
	 * @param clcte
	 * @param sColumnName
	 * @param bSearchable
	 * @param prefs
	 * @param subformtblmdl
	 * @return the cell editor
	 */
	protected final <PK2> CollectableComponentTableCellEditor newTableCellEditor(CollectableEntity clcte,
			UID sColumnName, final boolean bSearchable, Preferences prefs, final SubFormTableModel subformtblmdl) {
		final CollectableComponent clctcomp = newCollectableComponent(clcte, sColumnName, bSearchable, prefs);
			
		final CollectableComponentTableCellEditor result = createTableCellEditor(clctcomp);
		
		// implement TransferLookedUpValueActions for LOVs:
		if (clctcomp instanceof CollectableListOfValues) {
			final CollectableListOfValues<PK2> clctlov = (CollectableListOfValues<PK2>) clctcomp;
			for (LookupListener<PK2> lookup : lookupListener) {
				clctlov.addLookupListener(lookup);
			}

			final Collection<ClearAction> collClearActions = getClearActions(sColumnName);
			if (!collClearActions.isEmpty()) {
				clctlov.addLookupListener(new LookupClearListener(
						subformtblmdl, subformtbl, collClearActions));
			}

			final Collection<TransferLookedUpValueAction> collTransferValueActions = getTransferLookedUpValueActions(sColumnName);
			if (!collTransferValueActions.isEmpty()) {
				clctlov.addLookupListener(new LookupValuesListener(
						subformtblmdl, bSearchable, subformtbl, collTransferValueActions));
			}

			result.addCollectableComponentModelListener(subformtbl, new SubformModelListener(
					subformtblmdl, bSearchable, subformtbl, collTransferValueActions, clctcomp,
					collClearActions, result, getEntityUID()));
		}
		else if (clctcomp instanceof CollectableLocalizedComponent) {
			CollectableLocalizedComponent lclFieldComponent = (CollectableLocalizedComponent) clctcomp;
			if (getLookupService() != null)
				getLookupService().setCollectableLocalizedComponent(lclFieldComponent, getSubformTable().getSelectedRow());
			
		} else {
			final Collection<ClearAction> collClearActions = getClearActions(sColumnName);
			final Collection<TransferLookedUpValueAction> collTransferValueActions = getTransferLookedUpValueActions(sColumnName);
			if (!collClearActions.isEmpty() || !collTransferValueActions.isEmpty() ) {
				// Better alternative: result.addCellEditorListener(new CellEditorListener()) with overridden editingStopped(ChangeEvent e)
				// However, that solution had some issues with the save action and checkbox values which are not resolved...
				result.addCollectableComponentModelListener(subformtbl, new SubformModelListener(
						subformtblmdl, bSearchable, subformtbl, collTransferValueActions, clctcomp,
						collClearActions, result, getEntityUID()));
			}
		}

		return result;
	}

	/**
	 * create a CollectableComponent for the given EntityField
	 * @param clcte
	 * @param fieldUID
	 * @param bSearchable
	 * @param prefs
	 * @return the newly created collectable component
	 */
	private CollectableComponent newCollectableComponent(CollectableEntity clcte, UID fieldUID,
			boolean bSearchable, Preferences prefs) {
		final CollectableComponentType clctcomptype = getCollectableComponentType(fieldUID, bSearchable);

		final CollectableComponent result = collectableComponentFactory.newCollectableComponent(clcte.getEntityField(fieldUID), clctcomptype, bSearchable, false, true);
	
		result.setInsertable(bSearchable || isColumnInsertable(fieldUID));
		result.setPreferences(prefs);

		return result;
	}
	
	/**
	 * create an Tablecelleditor for the given component
	 * @param clctcomp
	 * @return the newly created table cell editor
	 */
	private CollectableComponentTableCellEditor createTableCellEditor(final CollectableComponent clctcomp) {
		if (getColumn(clctcomp.getFieldUID()) != null) {
			Column column = getColumn(clctcomp.getFieldUID());
			final Map<String, Object> properties = column.getProperties();
			for (String property : properties.keySet()) {
				clctcomp.setProperty(property, properties.get(property));
			}
			clctcomp.setCustomUsageSearch(column.getCustomUsageSearch());
		}
		final CollectableComponentTableCellEditor result = new CollectableComponentTableCellEditor(clctcomp, clctcomp.isSearchComponent());

		result.addCollectableComponentModelListener(null, getCollectableTableCellEditorChangeListener());		
		// @see NUCLOS-603. checkboxes and options should setvalue directly.
		if (clctcomp instanceof CollectableCheckBox) {
			((CollectableCheckBox) clctcomp).getJCheckBox().addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					SwingUtilities.invokeLater(new Runnable() { //@see NUCLOSINT-1635
						public void run() {
							try {
								if (getSubformTable().getModel() instanceof SubFormTableModel) {
									int row  = getSubformTable().getSelectedRow();
									int column = getSubformTable().getSelectedColumn();
									if (row != -1 && column != -1) {
										if (getSubformTable().getModel() instanceof SearchConditionTableModel) {
											getSubformTable().setValueAt(clctcomp.getSearchCondition(), row, column);
										} else {
											// check if column is the proper one
											try {
												TableColumn tCol = getSubformTable().getColumnModel().getColumn(column);											
												if(StringUtils.equals(clctcomp.getEntityField().getLabel(), (String)tCol.getHeaderValue())) {
													getSubformTable().setValueAt(clctcomp.getField(), row, column);
												}
											}
											catch(NullPointerException exNull) {
												LOG.warn("Groovy Script may not running for " + MetaProvider.getInstance().getEntity(getEntityUID()));
											}
										}
									}
								}
							} catch (CollectableFieldFormatException e1) {
								LOG.warn("could not set value for " + clctcomp.getFieldUID(), e1);
							}
						}
					});
				}
			});
		}
		if (clctcomp instanceof CollectableOptionGroup) {		
			((CollectableOptionGroup) clctcomp).getOptionGroup().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						if (getSubformTable().getModel() instanceof SubFormTableModel) {
							int row  = getSubformTable().getSelectedRow();
							int column = getSubformTable().getSelectedColumn();
							if (row != -1 && column != -1)
								getSubformTable().setValueAt(clctcomp.getField(), row, column);
						}
						
					} catch (CollectableFieldFormatException e1) {
						LOG.warn("could not set value for " + clctcomp.getFieldUID(), e1);
					}	
				}
			});
		}
		
		// textarea have to handle Tab in an subform differently.
		if (clctcomp instanceof NuclosCollectableTextArea) {
			((NuclosCollectableTextArea) clctcomp).overrideActionMap(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent evt) {
					Component c = (Component)((NuclosCollectableTextArea) clctcomp).getJTextArea().getParent();
					c.dispatchEvent(new KeyEvent(c, KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, KeyEvent.VK_TAB));
				}
			}, null);
			
			((NuclosCollectableTextArea) clctcomp).getJTextArea().getKeymap().addActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.SHIFT_DOWN_MASK), new AbstractAction() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					//do nothing!
				}
			});
			
			((NuclosCollectableTextArea) clctcomp).getJTextArea().getKeymap().addActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.CTRL_DOWN_MASK), new AbstractAction() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					//do nothing!
				}
			});
			
			// add text module settings if available
			if (mpTextModuleSettings.containsKey(clctcomp.getFieldUID())) {
				((NuclosCollectableTextArea) clctcomp).getLabeledTextArea().setTextModuleSettings(mpTextModuleSettings.get(clctcomp.getFieldUID()));
			}
		}
		if (clctcomp instanceof DynamicRowHeightChangeProvider) {
			((DynamicRowHeightChangeProvider) clctcomp).addDynamicRowHeightChangeListener(this);
		}

		return result;
	}

	public CollectableComponentModelAdapter getCollectableTableCellEditorChangeListener() {
		return editorChangeListener;
	}

	public final void setupStaticTableCellEditors(final JTable tbl, boolean bSearchable, Preferences prefs,
			final SubFormTableModel subformtblmdl, CollectableFieldsProviderFactory clctfproviderfactory,
			UID sParentEntityName, CollectableEntity clcte) {
		// setup a table cell editor for each column:
		// if commented out for NUCLOSINT-1425 (ts, tp):
		// if (this.isEnabled())
		{
			final UID sForeignKeyFieldName = this.getForeignKeyFieldName(sParentEntityName, clcte);
			for (int iColumnNr = 0; iColumnNr < subformtblmdl.getColumnCount(); iColumnNr++) {

				final CollectableEntityField clctef = subformtblmdl.getCollectableEntityField(iColumnNr);
				final UID columnUid = clctef.getUID();

				if (!this.isDynamicTableCellEditorNeeded(columnUid)) {
					// It may be wrong not to set a cell editor if the subform is invisible or disabled.
					// If the subform is shown or enabled later, the cell editor may be missing.
					// Note that we do not set a cell editor jfor the foreign key field name, as this is never shown.
					assert this.isColumnVisible(columnUid);
					assert !columnUid.equals(sForeignKeyFieldName);
					
					// A static CellEditor is sufficient and can be set right here:
					final CollectableComponentTableCellEditor clctcompcelleditor =
							this.newTableCellEditor(clcte, columnUid, bSearchable, prefs, subformtblmdl);

					
					// set a (static) value list provider for ComboBoxes:
					final CollectableComponent clctcomp = clctcompcelleditor.getCollectableComponent();
					clctcomp.setEnabled(bSearchable || isColumnEnabled(columnUid));
					clctcomp.setToolTipText(clctef.getDescription());
					if (clctcomp instanceof NuclosCollectableListOfValues) {
						((NuclosCollectableListOfValues) clctcomp).setLovSearch(this.isLovSearch(columnUid));
					}
					if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
						final LabeledCollectableComponentWithVLP clctWithVLP = (LabeledCollectableComponentWithVLP) clctcomp;
						assert clctWithVLP.getValueListProvider() == null;

						CollectableFieldsProvider valuelistprovider = getValueListProvider(columnUid);
						if (valuelistprovider == null) {
							// If no provider was set, use the default provider for static cell editors by default:
							if (LangUtils.equal(SF.STATE.getUID(getEntityUID()), columnUid)) {
								valuelistprovider = new StatusCollectableFieldsProvider(getEntityUID(), null);
							} else 	if (LangUtils.equal(SF.STATENUMBER.getUID(getEntityUID()), columnUid)) {
								valuelistprovider = new StatusNumeralCollectableFieldsProvider(getEntityUID(), null);
							} else 	if (LangUtils.equal(SF.PROCESS.getUID(getEntityUID()), columnUid)) {
								valuelistprovider = new ProcessCollectableFieldsProvider(getEntityUID());
							} else 	if (LangUtils.equal(SF.MANDATOR.getUID(getEntityUID()), columnUid)) {
								valuelistprovider = new MandatorCollectableFieldsProvider(getEntityUID(), null);
							} else {
								valuelistprovider = clctfproviderfactory.newDefaultCollectableFieldsProvider(clctWithVLP.getFieldUID());
							}
						}
						clctWithVLP.setValueListProvider(valuelistprovider);
						if (!bLayout) {
							if (bAddedToTabbedPane) {
								//Performance: We earn a lot of time and ressource, if this would be called only when needed (e.g. Subformtab is explicitely clicked)
								toBeRefreshed.add(clctWithVLP);
								
							} else {
								clctWithVLP.refreshValueList(true, true, true);
								
							}
						
						}
					}
					mpStaticColumnEditors.put(clctef, clctcompcelleditor);
				}
			}
		}
	}
	
	private boolean bSelected = false;
	
	public static final int NOTIFY_ADDED_TO_TABBEDPANE = 0;
	public static final int NOTIFY_TABBEDPANE_SELECTION = 1;
	public static final int NOTIFY_TABBEDPANE_DESELECTION = 2;
	
	private ISubFormCtrlLateInit subFormCtlrLateInit;
	private ITabSelectListener tabSelectListener;
	private boolean bAddedToTabbedPane = false;
	private Set<LabeledCollectableComponentWithVLP> toBeRefreshed = new HashSet<LabeledCollectableComponentWithVLP>();
	
	public boolean isSelected() {
		return bSelected;
	}
	
	private void setSelectedWithNotification(boolean b) {
		if (b != bSelected) {
			bSelected = b;
			if (tabSelectListener != null) {
				tabSelectListener.selectionChanged(b);
			}
		}
	}
	
	public void notifySelection(int notifyMessage) {
		if (notifyMessage == NOTIFY_ADDED_TO_TABBEDPANE) {
			bAddedToTabbedPane = true;
			bSelected = false;
			
		} else if (bAddedToTabbedPane) {
				
			if (notifyMessage == NOTIFY_TABBEDPANE_SELECTION) {
				
				setSelectedWithNotification(true);
				
				if (subFormCtlrLateInit != null) {
					
					subFormCtlrLateInit.lateInit();
					subFormCtlrLateInit = null; //Late init just once
					
					if (getParentSubForm() == null) { //Re-select in order to refresh possible sub-subforms
						int i = getJTable().getSelectionModel().getMinSelectionIndex();
						
						if (i >= 0) {
							getJTable().getSelectionModel().clearSelection();
							getJTable().getSelectionModel().setSelectionInterval(i, i);						
						}
					}
				}
				
				if (!toBeRefreshed.isEmpty()) {
					Set<LabeledCollectableComponentWithVLP> toBeRefreshed2 = new HashSet<LabeledCollectableComponentWithVLP>(toBeRefreshed);
					toBeRefreshed.clear();
					for (LabeledCollectableComponentWithVLP clctWithVLP : toBeRefreshed2) {
						clctWithVLP.refreshValueList(true, true, true);
					}				
				}
				
			} else if (notifyMessage == NOTIFY_TABBEDPANE_DESELECTION) {
				setSelectedWithNotification(false);
			}
		}
	}
	
	public void setSubFormCtrlLateInit(ISubFormCtrlLateInit subFormCtrlLateInit) {
		this.subFormCtlrLateInit = subFormCtrlLateInit;
	}
	
	public void setTabSelectListener(ITabSelectListener tabSelectListener) {
		this.tabSelectListener = tabSelectListener;
	}

	/**
	 * Transfers the looked-up values.
	 */
	public static void transferLookedUpValues(Collectable<?> clctSelected, final SubFormTable subformtbl, final boolean isSearchable, int iRow, Collection<TransferLookedUpValueAction> collTransferValueActions, final boolean bSetInModel) {
		if (isSearchable)
			return; // do not transfer lookedUp values in search fields.
		
		if (iRow >= 0) {
			iRow = subformtbl.convertRowIndexToModel(iRow);
		}
		// transfer the looked up values:
		for (TransferLookedUpValueAction act : collTransferValueActions) {
			final UID sourceFieldUid = act.getSourceFieldName();
			final SubFormTableModel subformtblmdl = subformtbl.getSubFormModel();
			final int iTargetColumn = subformtblmdl.findColumnByFieldUid(act.getTargetComponentName());
			if (iTargetColumn == -1) {
				throw new CommonFatalException(SpringLocaleDelegate.getInstance().getMessage(
						"SubForm.2","Das Unterformular enth\u00e4lt keine Spalte namens {0}", act.getTargetComponentName()));
			}

			final CollectableEntityField clctefTarget = subformtblmdl.getCollectableEntityField(iTargetColumn);
			final Object oValue;
			if (clctSelected == null) {
				oValue = subformtblmdl.getNullValue(clctefTarget);
			}
			else {
				final CollectableField clctfValue = clctSelected.getField(sourceFieldUid);
				assert clctfValue != null;
				oValue = isSearchable ? getSearchConditionForValue(clctefTarget, clctfValue) : clctfValue;
			}
			
			if (iRow < 0) {
				iRow = subformtbl.getSelectedRow();
			}

			if (iRow >= 0) {
				if (bSetInModel && !isSearchable) {
					int iCol = subformtbl.convertColumnIndexToView(iTargetColumn);
					CollectableComponentTableCellEditor editor = null;
					if (iCol != -1)
						editor = (CollectableComponentTableCellEditor)subformtbl.getCellEditorContinueEditing(iRow, iCol);
					else
						editor = (CollectableComponentTableCellEditor)subformtbl.getCellEditorContinueEditing(iRow, clctefTarget);
					if (editor != null) {
						try {
							CollectableField oldValue = editor.getCollectableComponent().getField();
							editor.getCollectableComponent().setField((CollectableField)oValue);
							
							boolean bChanged = true;
							if (oldValue == null) {
								bChanged = oValue != null;
							} else {
								bChanged = !oldValue.equals((CollectableField)oValue, false);
							}
							if (!bChanged) {
								// always invoke collectableFieldChangedInModel even if oldvalue equals newvalue.
								CollectableComponent comp = editor.getCollectableComponent();
								LinkedHashSet<CollectableComponentModel> source = new LinkedHashSet<>();
								source.add(comp.getModel());
								editor.collectableFieldChangedInModel(new CollectableComponentModelEvent(
										source, (CollectableField)subformtblmdl.getValueAt(iRow, iTargetColumn), (CollectableField)oValue));
							}
						} catch (CollectableFieldFormatException e) {
							// ignore this here.
						}
					}
				}
				subformtblmdl.setValueAt(oValue, iRow, iTargetColumn);
			}
		}
	}

	/**
	 * Clears the given values.
	 */
	static void clearValues(final SubFormTable subformtbl, SubFormTableModel subformtblmdl, int iRow, Collection<ClearAction> collClearActions) {
		if (iRow >= 0) {
			iRow = subformtbl.convertRowIndexToModel(iRow);
		}
		if (iRow < 0) {
			iRow = subformtbl.getSelectedRow();
		}

		// transfer the looked up values:
		SubFormTools.clearValues(subformtblmdl, iRow, collClearActions);
	}

	/**
	 * §precondition clctfValue != null
	 * §postcondition clctfValue.isNull() --&gt; result == null
	 */
	private static CollectableComparison getSearchConditionForValue(CollectableEntityField clctef, CollectableField clctfValue) {
		if (clctfValue == null) {
			throw new NullArgumentException("clctfValue");
		}
		return clctfValue.isNull() ? null : new CollectableComparison(clctef, ComparisonOperator.EQUAL, clctfValue);
	}

	/**
	 * @param sColumnName
	 * @return Is a dynamic TableCellEditor needed for the column with the given name?
	 * A dynamic TableCellEditor is needed if there is a refresh-possible-values action for this column.
	 */
	public final boolean isDynamicTableCellEditorNeeded(UID sColumnName) {
		return !getRefreshValueListActions(sColumnName).isEmpty();
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the name of the foreign key field referencing the parent entity
	 */
	public UID getForeignKeyFieldName(UID parentEntity, CollectableEntity clcte) {

		// Use the field referencing the parent entity from the subform, if any:
		UID result = this.getForeignKeyFieldToParent();

		if (result == null) {
			// Default: Find the field referencing the parent entity from the meta data.
			// If more than one field applies, throw an exception:
			for (UID sFieldName : clcte.getFieldUIDs()) {
				if (parentEntity.equals(clcte.getEntityField(sFieldName).getReferencedEntityUID())) {
					if (result == null) {
						// this is the foreign key field:
						result = sFieldName;
					}
					else {
						final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
								"SubForm.4","Das Unterformular f\u00fcr die Entit\u00e4t \"{0}\" enth\u00e4lt mehr als ein Fremdschl\u00fcsselfeld, das die \u00fcbergeordnete Entit\u00e4t \"{1}\" referenziert:\n\t{2}\n\t{3}\nBitte geben Sie das Feld im Layout explizit an.", clcte.getUID(), parentEntity, result, sFieldName);
						throw new CommonFatalException(sMessage);
					}
				}
			}
		}

		if (result == null) {
			throw new CommonFatalException(SpringLocaleDelegate.getInstance().getMessage(
					"SubForm.3","Das Unterformular f\u00fcr die Entit\u00e4t \"{0}\" enth\u00e4lt kein Fremdschl\u00fcsselfeld, das die \u00fcbergeordnete Entit\u00e4t \"{1}\" referenziert.\nBitte geben Sie das Feld im Layout explizit an.", clcte.getUID(), parentEntity));
		}
		assert result != null;
		return result;
	}

	public boolean isUseCustomColumnWidths() {
		return this.useCustomColumnWidths;
	}
	/**
     * @param lookupListener the LookupListener to add
     */
    public void addLookupListener(LookupListener lookupListener) {
	    this.lookupListener.add(lookupListener);
    }

    public void addFocusActionListener(FocusActionListener fal) {
    	lstFocusActionListener.add(fal);
    }

    public List<FocusActionListener> getFocusActionLister() {
    	return lstFocusActionListener;
    }

    public void addParameterListener(ParameterChangeListener pl) {
    	parameterListener.add(pl);
    }

	/**
	 * fires a <code>ChangeEvent</code> whenever the model of this <code>SubForm</code> changes.
	 */
	private synchronized void fireParameterChanged() {
		if(layer == null || (layer != null && !((LockableUI) layer.getUI()).isLocked())){
			final ChangeEvent ev = new ChangeEvent(this);
			for (ChangeListener changelistener : parameterListener) {
				changelistener.stateChanged(ev);
			}
		}
	}

    public NuclosScript getNewEnabledScript() {
		return newEnabledScript;
	}

	public void setNewEnabledScript(NuclosScript newEnabledScript) {
		this.newEnabledScript = newEnabledScript;
	}

	public NuclosScript getEditEnabledScript() {
		return editEnabledScript;
	}

	public void setEditEnabledScript(NuclosScript editEnabledScript) {
		this.editEnabledScript = editEnabledScript;
	}

	public NuclosScript getDeleteEnabledScript() {
		return deleteEnabledScript;
	}

	public void setDeleteEnabledScript(NuclosScript deleteEnabledScript) {
		this.deleteEnabledScript = deleteEnabledScript;
	}

	public NuclosScript getCloneEnabledScript() {
		return cloneEnabledScript;
	}

	public void setCloneEnabledScript(NuclosScript cloneEnabledScript) {
		this.cloneEnabledScript = cloneEnabledScript;
	}

	public NuclosScript getDynamicRowColorScript() {
		return dynamicRowColorScript;
	}

	public void setDynamicRowColorScript(NuclosScript dynamicRowColorScript) {
		this.dynamicRowColorScript = dynamicRowColorScript;
	}


	/**
	 * @author Thomas Pasch (javadoc, deprecation)
	 * @deprecated Superseded by the {@link TablePopupMenuEvent} infrastructure,
	 * 		also see {@link Mouse2TableHeaderPopupMenuAdapter}.
	 */
	public class SubFormPopupMenuMouseAdapter extends TablePopupMenuMouseAdapter {

		public SubFormPopupMenuMouseAdapter(JTable table) {
			super(table);
		}
		
		@Override
		public boolean doPopup(MouseEvent e, JPopupMenu menu) {		
			final JTable table = getTable();
			final int col = table.columnAtPoint(e.getPoint());
			final int row = table.rowAtPoint(e.getPoint());

			if (table instanceof FixedColumnRowHeader.HeaderTable) {
				if (((FixedColumnRowHeader.HeaderTable) table).getColumnModel().getColumnIndexAtX(e.getX()) == 0) {
					return false;
				}
			}

			if(col != -1 && row != -1) {
				TableCellEditor tce = table.getCellEditor(row, col);
				menu = null;
				CollectableComponent clctcmp = null;
				if(tce instanceof CollectableComponentTableCellEditor) {
					clctcmp = ((CollectableComponentTableCellEditor) tce).getCollectableComponent();
					if(clctcmp instanceof JPopupMenuFactory) {
						// convertColumnIndexToModel not necessary, already done by getValueAt
						// Object value = table.getValueAt(row, table.convertColumnIndexToModel(col));
						Object value = table.getValueAt(row, col);
						tce.getTableCellEditorComponent(table, value, false, row, col);	// contained field is set without triggering listeners
						clctcmp.getJComponent().setEnabled(table.isCellEditable(row, col));
						menu = ((JPopupMenuFactory) clctcmp).newJPopupMenu();
					}
				}
				if (menu != null && E.isNuclosEntity(clctcmp.getEntityField().getEntityUID())) {
					table.editCellAt(row, col, e);
					menu.show(table, e.getX(), e.getY());
				}
				else if (getPopupMenuListener() != null) {
					getPopupMenuListener().doPopup(e, menu);
				}
			} else {
				if (isLayout() && getPopupMenuListener() != null) {
					getPopupMenuListener().doPopup(e, null);
				}
			}
			return true;
		}
	}

	private class DoubleClickMouseAdapter extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON1) {
		    	if (e.getClickCount() == 2) {
		    		JTable table = getJTable();
		    		int col = table.columnAtPoint(e.getPoint());
					int row = table.rowAtPoint(e.getPoint());
					if (col != -1 && row != -1) {
						TableCellEditor tce = table.getCellEditor(row, col);
						if(tce instanceof CollectableComponentTableCellEditor) {
							CollectableComponent clctcmp = ((CollectableComponentTableCellEditor) tce).getCollectableComponent();
							if (clctcmp instanceof MouseListener) {
								Object value = table.getModel().getValueAt(row, table.convertColumnIndexToModel(col));
								tce.getTableCellEditorComponent(table, value, false, row, col);	// contained field is set without triggering listeners
								((MouseListener) clctcmp).mouseClicked(e);
							}
						}
					}
		    	}
		    }
		}
	}

	@Override
	public void heightChanged(int height) {
		final int iCol = subformtbl.getSelectedColumn();
		final int iRow = subformtbl.getSelectedRow();
		rowHeightCtrl.setEditorHeight(iCol, iRow, height);
	}

	public int getValidRowHeight(int iHeight) {
		return Math.max(getMinRowHeight()-(subformtbl == null ? 0 : subformtbl.getRowMargin()), Math.min(MAX_DYNAMIC_ROWHEIGHT, iHeight));
	}

	public boolean isDynamicRowHeights() {
		return dynamicRowHeights;
	}

	public void setDynamicRowHeightsDefault() {
		dynamicRowHeightsDefault = true;
	}
	
	public boolean isDynamicRowHeightsDefault() {
		return dynamicRowHeightsDefault;
	}
	
	public void setIgnoreSubLayout() {
		ignoreSubLayout = true;
	}

	public boolean isIgnoreSubLayout() {
		return ignoreSubLayout;
	}
	
	public void setMaxEntries(Integer maxEntries) {
		this.maxEntries = maxEntries;
	}

	public Integer getMaxEntries() {
		return this.maxEntries;
	}
	
	/**
	 * @return param map to the subform's parent entity. May be <code>null</code>.
	 */
	public Map<String, Object> getMapParams() {
		return Collections.unmodifiableMap(this.mpParams);
	}
	
	public void setMapParams(Map<String, Object> mpParams) {
		this.mpParams.clear();	
		this.mpParams.putAll(mpParams);
		fireParameterChanged();
	}

	public ToolbarManager getToolbarManager() {
		return this.toolbarManager;
	}

	public void setToolBarItemGroup(ToolBarItemGroup toolBarGroup) {
		toolbar.setToolBarItemGroup(toolBarGroup);
	}
	
	public void putTextModuleSettings(UID column, TextModuleSettings tms) {
		mpTextModuleSettings.put(column, tms);
	}

	public Boolean isAutonumberSorting() {
		return this.isAutonumberSorting;
	}
	public void releaseFocus() {
		if(this.getParent().getParent()==null)			
			this.getParent().requestFocus();
		else
			this.getParent().getParent().requestFocus();
	}
	
	public void setAutonumberSorting(boolean isAutonumberSorting) {
		this.isAutonumberSorting = isAutonumberSorting;		
	}


	RowHeightController getRowHeightCtrl() {
		return rowHeightCtrl;
	}

}	// class SubForm