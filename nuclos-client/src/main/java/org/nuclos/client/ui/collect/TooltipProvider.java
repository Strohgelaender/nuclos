package org.nuclos.client.ui.collect;

import javax.swing.*;

public interface TooltipProvider {
	
	public JToolTip createToolTip();
	
}
