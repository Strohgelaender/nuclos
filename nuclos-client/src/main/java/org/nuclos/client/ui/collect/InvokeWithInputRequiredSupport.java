//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.io.Serializable;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.log4j.Logger;
import org.nuclos.api.context.InputDelegate;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.context.InputSpecification;
import org.nuclos.client.command.MTRListener;
import org.nuclos.client.command.MultiThreadedRunnable;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.remote.NuclosHttpInvokerAttributeContext;
import org.nuclos.client.ui.LineBreakLabel;
import org.nuclos.client.ui.OverlayOptionPane;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

// @Component
public class InvokeWithInputRequiredSupport {
	
	private static final Logger LOG = Logger.getLogger(InvokeWithInputRequiredSupport.class);

	private NuclosHttpInvokerAttributeContext ctx;

	public InvokeWithInputRequiredSupport() {
	}

	// @Autowired
	public final void setNuclosHttpInvokerAttributeContext(NuclosHttpInvokerAttributeContext ctx) {
		this.ctx = ctx;
	}
	
	public void invokeForMultiThreaded(final MultiThreadedRunnable runnable, final Map<String, Serializable> context, 
			final Map<String, Serializable> applyMultiEditContext, final MainFrameTab parent, final ResultListener<Exception> exResult) {
		final LayerLock lock = parent.lockLayer();
		try {
			runnable.setListener(new MTRListener() {
				@Override
				public void error(Exception mcrlex) {
					InputRequiredException ex = getInputRequiredException(mcrlex);
					if (ex != null) {
						handleInputRequiredExceptionForMultiThreaded(ex, runnable, context, applyMultiEditContext, parent, exResult);
					} else {
						try {
							exResult.done(mcrlex);
						} finally {
							cleanup();
						}
					}
				}
				
				@Override
				public void done() {
					try {
						exResult.done(null);
					} finally {
						cleanup();
					}
				}
				
				private void cleanup() {
					context.clear();
					runnable.clearAttrInput();
					runnable.setAttrInputIsSupported(false);
					runnable.setAttrMessageReceiver(null);
				}
			});
			
			runnable.setAttributeContext(ctx);
			runnable.setAttrInputIsSupported(true);
			runnable.putAllAttrInput(context);
			runnable.putAllAttrInput(applyMultiEditContext);
			if (parent != null) {
				runnable.setAttrMessageReceiver(parent.getId());
			}
			
			runnable.run();
		}
		catch (Exception ex) {
			exResult.done(ex);
		}
		finally {
			parent.unlockLayer(lock);
		}
	}

	public void invoke(CommonRunnable runnable, Map<String, Serializable> context, Map<String, Serializable> applyMultiEditContext, MainFrameTab parent) throws CommonBusinessException {
		final LayerLock lock = parent.lockLayer();
		try {
			ctx.setSupported(true);
			ctx.putAll(context);
			ctx.putAll(applyMultiEditContext);
			if (parent != null) {
				ctx.setMessageReceiver(parent.getId());
			}
			try {
				runnable.run();
				context.clear();
			}
			finally {
				ctx.clear();
				ctx.setSupported(false);
				ctx.setMessageReceiver(null);
			}
		}
		catch (InputRequiredException irex) {			
			handleInputRequiredException(irex, runnable, context, applyMultiEditContext, parent, null);
		}
		catch (CommonBusinessException cbex) {
			InputRequiredException ex = getInputRequiredException(cbex);
			if (ex != null) {
				handleInputRequiredException(ex, runnable, context, applyMultiEditContext, parent, null);
			}
			else {
				context.clear();
				throw cbex;
			}
		}
		catch (CommonFatalException cfex) {
			InputRequiredException ex = getInputRequiredException(cfex);
			if (ex != null) {
				handleInputRequiredException(ex, runnable, context, applyMultiEditContext, parent, null);
			}
			else {
				context.clear();
				throw cfex;
			}
		}
		finally {
			parent.unlockLayer(lock);
		}
	}
	
	private void handleInputRequiredExceptionForMultiThreaded(final InputRequiredException ex, final MultiThreadedRunnable r, final Map<String, Serializable> context, 
			final Map<String, Serializable> applyMultiEditContext, final MainFrameTab parent, final ResultListener<Exception> exResult) {
		UIUtils.invokeOnDispatchThread(new Runnable() {
			@Override
			public void run() {
				try {
					handleInputRequiredException(ex, r, context, applyMultiEditContext, parent, exResult);
				} catch (Exception ex) {
					r.finishedWithError(ex);
				}
			}
		});
	}

	private void handleInputRequiredException(final InputRequiredException ex, final CommonRunnable r, final Map<String, Serializable> context, 
			final Map<String, Serializable> applyMultiEditContext, final MainFrameTab parent, final ResultListener<Exception> exResult) throws CommonBusinessException {
		String title = Main.getInstance().getMainFrame().getTitle();
		
		final Map<String, Serializable> contextToPut;
		
		if (ex.getInputSpecification() != null) {
			contextToPut = applyMultiEditContext != null && ex.getInputSpecification().applyForAllMultiEditObjects() ? 
					applyMultiEditContext:
						context;
			title = ex.getInputSpecification().getTitle();
			String message = ex.getInputSpecification().getMessage();
			
			switch (ex.getInputSpecification().getType()) {
			case InputSpecification.CONFIRM_YES_NO:
				if (parent != null && exResult != null) {
					OverlayOptionPane.showConfirmDialog(parent, message, title, OverlayOptionPane.YES_NO_OPTION, true, (i) -> {
						if (i != JOptionPane.CLOSED_OPTION && i != JOptionPane.CANCEL_OPTION) {
							contextToPut.put(ex.getInputSpecification().getKey(),(i == JOptionPane.YES_OPTION) ? InputSpecification.YES : InputSpecification.NO);
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							((MultiThreadedRunnable)r).finishedWithError(new UserCancelledException("handleInputRequiredException2"));
						}
					});
				} else {
					int i = JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION);
					if (i != JOptionPane.CLOSED_OPTION && i != JOptionPane.CANCEL_OPTION) {
						contextToPut.put(ex.getInputSpecification().getKey(), (i == JOptionPane.YES_OPTION) ? InputSpecification.YES : InputSpecification.NO);
						if (exResult != null) {
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							invoke(r, context, applyMultiEditContext, parent);
						}
					} else {
						throw new UserCancelledException("handleInputRequiredException1");
					}
				}
				break;
			case InputSpecification.CONFIRM_OK_CANCEL:
				if (parent != null && exResult != null) {
					OverlayOptionPane.showConfirmDialog(parent, message, title, OverlayOptionPane.OK_CANCEL_OPTION, true, (i) -> {
						if (i != JOptionPane.CLOSED_OPTION && i != JOptionPane.CANCEL_OPTION) {
							contextToPut.put(ex.getInputSpecification().getKey(), InputSpecification.OK);
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							((MultiThreadedRunnable)r).finishedWithError(new UserCancelledException("handleInputRequiredException2"));
						}
					});
				} else {
					int i = JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.OK_CANCEL_OPTION);
					if (i != JOptionPane.CLOSED_OPTION && i != JOptionPane.CANCEL_OPTION) {
						contextToPut.put(ex.getInputSpecification().getKey(), InputSpecification.OK);
						if (exResult != null) {
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							invoke(r, context, applyMultiEditContext, parent);
						}
					} else {
						throw new UserCancelledException("handleInputRequiredException2");
					}
				}
				break;
			case InputSpecification.INPUT_VALUE:
				if (parent != null && exResult != null) {
					OverlayOptionPane.showInputDialog(parent, message, title, OverlayOptionPane.OK_CANCEL_OPTION, OverlayOptionPane.INPUT_TEXTFIELD, (i, input) -> {
						if (i != JOptionPane.CLOSED_OPTION && i != JOptionPane.CANCEL_OPTION) {
							contextToPut.put(ex.getInputSpecification().getKey(), (Serializable) input);
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							((MultiThreadedRunnable)r).finishedWithError(new UserCancelledException("handleInputRequiredException3"));
						}
					});
				} else {
					String s = JOptionPane.showInputDialog(parent, message, title, JOptionPane.PLAIN_MESSAGE);
					if (s != null) {
						contextToPut.put(ex.getInputSpecification().getKey(), s);
						if (exResult != null) {
							invoke(r, context, applyMultiEditContext, parent);
						} else {
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						}
					} else {
						throw new UserCancelledException("handleInputRequiredException3");
					}
				}
				break;
			case InputSpecification.INPUT_MEMO:
				if (parent != null && exResult != null) {
					OverlayOptionPane.showInputDialog(parent, message, title, OverlayOptionPane.OK_CANCEL_OPTION, OverlayOptionPane.INPUT_TEXTAREA, (i, input) -> {
						if (i != JOptionPane.CLOSED_OPTION && i != JOptionPane.CANCEL_OPTION && input != null) {
							contextToPut.put(ex.getInputSpecification().getKey(), (Serializable) input);
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							((MultiThreadedRunnable)r).finishedWithError(new UserCancelledException("handleInputRequiredException3"));
						}
					});
				} else {
					JLabel label = new LineBreakLabel(message==null?"":message.toString(), 240);
					JPanel jpnInput = new JPanel();
					TableLayoutBuilder tbllay = new TableLayoutBuilder(jpnInput).columns(10, 240, 10);
					tbllay.newRow().skip().add(label);
					JTextArea jtaInput = new JTextArea();
					tbllay.newRow(4);
					tbllay.newRow(100).skip().add(new JScrollPane(jtaInput));
					final int confirmResult = JOptionPane.showConfirmDialog(parent, jpnInput, title, JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
					final String sMemo = jtaInput.getText();
					if (confirmResult == JOptionPane.OK_OPTION && sMemo != null) {
						contextToPut.put(ex.getInputSpecification().getKey(), sMemo);
						if (exResult != null) {
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							invoke(r, context, applyMultiEditContext, parent);
						}
					} else {
						throw new UserCancelledException("handleInputRequiredException3");
					}
				}
				break;
			case InputSpecification.INPUT_OPTION:
				if (parent != null && exResult != null) {
					OverlayOptionPane.showInputDialog(parent, message, title, OverlayOptionPane.OK_CANCEL_OPTION,
							ex.getInputSpecification().getOptions(), ex.getInputSpecification().getDefaultOption(), (i, input) -> {
								if (i != JOptionPane.CLOSED_OPTION && i != JOptionPane.CANCEL_OPTION && input != null) {
									contextToPut.put(ex.getInputSpecification().getKey(), (Serializable) input);
									invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
								} else {
									((MultiThreadedRunnable)r).finishedWithError(new UserCancelledException("handleInputRequiredException4"));
								}
							});
				} else {
					Object o = JOptionPane.showInputDialog(parent, message, title, JOptionPane.PLAIN_MESSAGE, null, ex.getInputSpecification().getOptions(), ex.getInputSpecification().getDefaultOption());
					if (o != null) {
						contextToPut.put(ex.getInputSpecification().getKey(), (Serializable) o);
						if (exResult != null) {
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							invoke(r, context, applyMultiEditContext, parent);
						}
					} else {
						throw new UserCancelledException("handleInputRequiredException4");
					}
				}
				break;
			default:
				break;
			}
		}
		else if (ex.getInputDelegateSpecification() != null) {
			contextToPut = applyMultiEditContext != null && ex.getInputDelegateSpecification().applyForAllMultiEditObjects() ? 
					applyMultiEditContext:
						context; 
			String classname = ex.getInputDelegateSpecification().getDelegateClass();

			try {
				Class<?> clazz = LangUtils.getClassLoaderThatWorksForWebStart().loadClass(classname);
				if (InputDelegate.class.isAssignableFrom(clazz)) {
					InputDelegate ics = (InputDelegate) clazz.newInstance();
					InputDelegatePane pane = new InputDelegatePane(ics);
					JDialog dialog = new JDialog(Main.getInstance().getMainFrame(), title, true);
					Map<String, Serializable> result = pane.show(dialog, ex.getInputDelegateSpecification().getData(), parent);
					if (result != null) {
						contextToPut.putAll(result);
						if (exResult != null) {
							invokeForMultiThreaded((MultiThreadedRunnable) r, context, applyMultiEditContext, parent, exResult);
						} else {
							invoke(r, context, applyMultiEditContext, parent);
						}
					}
					else {
						throw new UserCancelledException("handleInputRequiredException5");
					}
				}
			}
			catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
			catch (InstantiationException e) {
				throw new RuntimeException(e);
			}
			catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
		else {
			throw new UnsupportedOperationException("Unable to process exception " +  ex.toString());
		}
	}

	private InputRequiredException getInputRequiredException(Throwable ex) {
		if (ex instanceof InputRequiredException) {
			return (InputRequiredException) ex;
		}
		else if (ex.getCause() != null) {
			return getInputRequiredException(ex.getCause());
		}
		else {
			return null;
		}
	}
}
