package org.nuclos.client.ui.collect.result;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.io.Closeable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;
import org.jdesktop.swingx.HorizontalLayout;
import org.jdesktop.swingx.JXCollapsiblePane;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.WideJComboBox;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.component.CollectableComboBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.collect.subform.SubformRowHeader;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.schema.layout.layoutml.Tablelayout;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * Created by guenthse on 3/2/2017.
 */
public class ResultFilterPanel extends JXCollapsiblePane implements Closeable {

    private static final Logger LOG = Logger.getLogger(ResultFilterPanel.class);

    private JPanel filterPanel = new JPanel(new TableLayout());
    private JPanel resetFilterButtonPanel = new JPanel(new BorderLayout());
    private final JButton resetFilterButton = new JButton();

    private TableColumnModel columnModel;
    private Boolean fixedTable = false;
    private JTable table;

    private Map<UID, CollectableComponent> column2component;

    private boolean closed = false;

	public ResultFilterPanel(JTable table, TableColumnModel columnModel, Map<UID, CollectableComponent> column2component, Boolean fixedTable) {
        setLayout(new BorderLayout());
        this.columnModel = columnModel;
        this.column2component = column2component;
        this.fixedTable = fixedTable;
        this.table = table;
        

        add(filterPanel, BorderLayout.CENTER);
        init();
    }

    private void init() {
        setCollapsed(true);

        if (fixedTable) {
            resetFilterButtonPanel.setPreferredSize(new Dimension(SubformRowHeader.COLUMN_SIZE, 20));
            resetFilterButton.setIcon(Icons.getInstance().getIconClearSearch16());
            resetFilterButton.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
                    "subform.filter.tooltip", "Filterkriterien l\u00f6schen"));
            resetFilterButtonPanel.add(resetFilterButton, BorderLayout.CENTER);
        }

        arrangeFilterComponents();

        addListener();
        // subformFilterPanel is invisible by default. @see NUCLOSINT-1630
        setVisible(false);
    }
    @Override
    public void setCollapsed(boolean val) {
        super.setCollapsed(val);
    }

    /**
     * adds a listener on the columnmodel to get informed, when e.g. a column margin was changed
     * and to arrange the filter components again
     */
    public void addListener() {
        TableColumnModelListener listener = new TableColumnModelListener() {

            @Override
            public void columnAdded(TableColumnModelEvent e) {
                // do nothing
            }

            @Override
            public void columnMarginChanged(ChangeEvent e) {
                arrangeFilterComponents();
            }

            @Override
            public void columnMoved(TableColumnModelEvent e) {
            	if (e.getFromIndex() != e.getToIndex()) {
					arrangeFilterComponents();
				}
            }

            @Override
            public void columnRemoved(TableColumnModelEvent e) {
                // do nothing
            }

            @Override
            public void columnSelectionChanged(ListSelectionEvent e) {
                // arrangeFilterComponents();
            }
        };

        columnModel.addColumnModelListener(listener);
    }

    /**
     * arranges all filter components, because e.g a column was moved,
     * so that the filter components stay directly above the corresponding columns
     */
    public final void arrangeFilterComponents() {
        filterPanel.removeAll();
        TableLayout layout = new TableLayout();
		filterPanel.setLayout(layout);
		layout.insertRow(0, 20);

		if (this.fixedTable) {
			layout.insertColumn(0, SubformRowHeader.COLUMN_SIZE);
            filterPanel.add(this.resetFilterButtonPanel, new TableLayoutConstraints( 0, 0, 0, 0));
        }

        int column = this.fixedTable ? 1 : 0;
        for (TableColumn tc : CollectionUtils.iterableEnum(columnModel.getColumns())) {
            if (tc.getIdentifier() == null) {
				continue;
			}

            UID identifier = getColumnIdentifier(tc);
            CollectableComponent clctcomp = identifier != null ? getCollectableComponentByName(identifier) : null;
            JComponent comp = clctcomp == null ? null : clctcomp.getControlComponent();

            
            if (clctcomp instanceof CollectableComboBox && table instanceof  SubFormTable) {
				Column col=((SubFormTable)table).getSubForm().getColumn(identifier);
				if (col != null) {
					clctcomp.setMultiSelect(col.isMultiselectsearchfilter());
				}
            }
            
            if (comp != null) {
                // place checkboxes in the center
                if (comp instanceof JCheckBox) {
                    JPanel p = new JPanel(new GridBagLayout());
                    p.add(comp);
                    comp = p;
                }

                comp.setPreferredSize(new Dimension(tc.getWidth(), 20));
                layout.insertColumn(column, tc.getWidth());
				TableLayoutConstraints constraints = new TableLayoutConstraints(column, 0, column, 0);
                filterPanel.add(comp, constraints);

                column += 1;
            }
        }

        filterPanel.revalidate();
    }

    private CollectableComponent getCollectableComponentByName(UID name) {
        if (org.nuclos.common.UID.UID_NULL.equals(name) || null == name) {
            return null;
        }

        for (CollectableComponent comp : column2component.values()) {
            if (comp.getEntityField().getUID().equals(name)) {
                return comp;
            }
        }

        return null;
    }

    public JButton getResetFilterButton() {
        return this.resetFilterButton;
    }

    private UID getColumnIdentifier(TableColumn tc) {
    	return tc.getIdentifier() instanceof UID ? (UID) tc.getIdentifier() :
				(table.getModel() instanceof CollectableEntityFieldBasedTableModel ?
						((CollectableEntityFieldBasedTableModel) table.getModel()).getColumnFieldUid(tc.getModelIndex()) : null);
	}

    /**
     * @return the active filter components
     */
    public Map<UID, CollectableComponent> getActiveFilterComponents() {
        Map<UID, CollectableComponent> activeActiveFilterComponents = new HashMap<>();

        for (int index = 0; index < columnModel.getColumnCount(); index++) {
            TableColumn column = columnModel.getColumn(index);
            if (column.getIdentifier() == null) {
				continue;
			}
            UID identifier = getColumnIdentifier(column);
            if (identifier != null) {
                CollectableComponent comp = column2component.get(identifier);

                if (!this.fixedTable) {
                    activeActiveFilterComponents.put(identifier, comp);
                }
                else if (!"".equals(identifier)) {
                    activeActiveFilterComponents.put(identifier, comp);
                }
            }
        }

        return activeActiveFilterComponents;
    }

    public Collection<CollectableComponent> updateColumn2Components(Map<UID, CollectableComponent> c2c) {
		Collection<CollectableComponent> added = new HashSet<>();
    	for (UID uid : c2c.keySet()) {
    		if (!column2component.containsKey(uid)) {
    			column2component.put(uid, c2c.get(uid));
				added.add(c2c.get(uid));
			}
		}
		return added;
	}

    @Override
    public void close() {
        // Close is needed for avoiding memory leaks
        // If you want to change something here, please consult me (tp).
        if (!closed) {
            LOG.debug("close(): " + this);
            filterPanel = null;
            columnModel = null;
            column2component.clear();
            column2component = null;

            // JXCollapsiblePane
            removeAll();
            // setContentPane(null);

            closed = true;
        }
    }
}
