//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.ui.event;

import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.*;

public abstract class TablePopupMenuMouseAdapter extends PopupMenuMouseAdapter {

	private final JTable table;

	public TablePopupMenuMouseAdapter(JTable table) {
		if (table == null) {
			throw new NullPointerException();
		}
		this.table = table;
	}
	
	protected JTable getTable() {
		return table;
	}

	@Override
	boolean checkAndDo(MouseEvent e) {
		if (e.isPopupTrigger()) {
			if (table != null) {
				int clickRow = table.rowAtPoint(e.getPoint());
				int[] selectedRows = table.getSelectedRows();

				if (Arrays.binarySearch(selectedRows, clickRow) < 0) {
					if ((e.getModifiers() & MouseEvent.CTRL_MASK) != 0) {
						table.getSelectionModel().addSelectionInterval(clickRow, clickRow);
					}
					else {
						table.getSelectionModel().setSelectionInterval(clickRow, clickRow);
					}
				}
			}
			return doPopup(e, null);
		}
		return false;
	}

}
