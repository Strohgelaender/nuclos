//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.client.ui.layoutml.IField2Tag;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableField;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.exception.CollectableFieldValidationException;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.exception.CommonFatalException;
import org.springframework.util.ObjectUtils;

/**
 * Model for a <code>CollectableComponent</code>. A model can be shared by more than one
 * <code>CollectableComponent</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public abstract class CollectableComponentModel {

	private final CollectableComponentModelListenerSupport listenerSupport = new CollectableComponentModelListenerSupport();
	private final CollectableEntityField clctef;
	protected CollectableField clctfValue;
	
	private final AtomicBoolean isInitializing = new AtomicBoolean(false);
	private final AtomicBoolean isExistingBo = new AtomicBoolean(false);

	/**
	 * package private ctor. Use <code>newCollectableComponentModel()</code> to create an instance of this class.
	 * @param clctef the <code>CollectableEntityField</code> describing the field this model represents.
	 */
	CollectableComponentModel(CollectableEntityField clctef) {
		this.clctef = clctef;
		this.clctfValue = clctef.getNullField();
	}
	
	public final CollectableComponentModelListenerSupport getListenerSupport() {
		return listenerSupport;
	}

	/**
	 * §postcondition result.isSearchModel() &lt;--&gt; bSearchable
	 * 
	 * @param clctef the <code>CollectableEntityField</code> describing the field this model represents.
	 * @param bSearchable Is this component to be used to enter a search condition?
	 * @return a new <code>CollectableComponentModel</code>.
	 */
	public static CollectableComponentModel newCollectableComponentModel(CollectableEntityField clctef, boolean bSearchable) {
		final CollectableComponentModel result;
		if (bSearchable) {
			result = new SearchComponentModel(clctef);				
		}
		else {
			if (clctef.isLocalized() && MetaProvider.getInstance().getEntityField(clctef.getUID()).getCalcFunction() == null) {
				if (!bSearchable) {
					result = new DetailsLocalizedComponentModel(clctef);					
				} else {
					result = new DetailsComponentModel(clctef);
				}
			} else {
				result = new DetailsComponentModel(clctef);				
			}
		}
		assert result.isSearchModel() == bSearchable;

		return result;
	}

	/**
	 * @return the <code>CollectableEntityField</code> describing the field this model represents.
	 */
	public CollectableEntityField getEntityField() {
		return this.clctef;
	}

	/**
	 * @return the name of this field. Shortcut for <code>this.getEntityField().getName()</code>.
	 */
	public UID getFieldUID() {
		return this.clctef.getUID();
	}

	/**
	 * @return Is this component to be used to enter a search condition?
	 */
	public abstract boolean isSearchModel();

	/**
	 * @return Is this component to be used to enter/display a value?
	 */
	public final boolean isDetailsModel() {
		return !this.isSearchModel();
	}

	/**
	 * @return Is this component to be used to enter/display a value in a multiedit scenario?
	 */
	public abstract boolean isMultiEditable();

	/**
	 * §postcondition result != null
	 * 
	 * @return the value of this component.
	 */
	public CollectableField getField() {
		return this.clctfValue;
	}

	
	/**
	 * §postcondition result != null
	 * 
	 * @return the value of this component.
	 */
	public CollectableField getField(boolean readingCollectController) {
		return this.clctfValue;
	}

	
	/**
	 * sets the "collectableField" (the composite value) of this component.
	 * Note that a <code>CollectableComponentEvent</code> is fired even if the model did not change.
	 * That's because there may be an inconsistent <code>CollectableComponent</code> listening to
	 * this. In that case, the component (view) must be updated anyway.
	 * 
	 * §postcondition this.getField().equals(clctfValue)
	 * 
	 * §todo This could be changed (maybe this would mean an optimization) in the future. getField() should
	 * throw an InconsistentStateException rather than return the old value if there is an inconsistent view. Only
	 * then we would have to notify the listeners even if the value did not change.
	 * 
	 * @param clctfValue The field type must match the field type of <code>getEntityField</code>.
	 */
	public abstract void setField(CollectableField clctfValue);
	
	public void setFieldInitial(final CollectableField clctValue) {
		isInitializing.set(true);
		UIUtils.invokeOnDispatchThread(new Runnable() {
			
			@Override
			public void run() {
				setField(clctValue);
				isInitializing.set(false);
			}
		});
		//isInitializing.set(false);
	}

	/**
	 * sets the "collectableField" (the composite value) of this component and optionally notifies the listeners.
	 * Note that a <code>CollectableComponentEvent</code> is fired even if the model did not change.
	 * That's because there may be an inconsistent <code>CollectableComponent</code> listening to
	 * this. In that case, the component (view) must be updated anyway.
	 * 
	 * §precondition clctfValue != null
	 * §postcondition this.getField().equals(clctfValue)
	 * §todo @see #setField(CollectableField clctfValue)
	 * 
	 * @param clctfValue The field type must match the field type of <code>getEntityField</code>.
	 * @param bNotifyListeners Are listeners to be notified?
	 */
	void setField(CollectableField clctfValue, boolean bNotifyListeners) {
		this.setField(clctfValue, bNotifyListeners, false, null);
	}

	public void setField(CollectableField clctfValue, boolean bNotifyListeners, CollectableComponentModelEvent ev) {
		this.setField(clctfValue, bNotifyListeners, false, ev);
	}

	public void setField(CollectableField clctfValue, Boolean bExistingBo) {
		if (bExistingBo != null) {
			isExistingBo.set(bExistingBo);
		}
		setField(clctfValue);
	}
	
	public void setFieldInitial(CollectableField clctValue, boolean bNotifyListeners, Boolean bExistingBo) {
		if (bExistingBo != null) {
			isExistingBo.set(bExistingBo);			
		}
		isInitializing.set(true);
		setField(clctValue, bNotifyListeners, false);
		isInitializing.set(false);
	}

	/**
	 * sets the "collectableField" (the composite value) of this component and optionally notifies the listeners.
	 * Note that a <code>CollectableComponentEvent</code> is fired even if the model did not change.
	 * That's because there may be an inconsistent <code>CollectableComponent</code> listening to
	 * this. In that case, the component (view) must be updated anyway.
	 * 
	 * §precondition clctfValue != null
	 * §postcondition this.getField().equals(clctfValue)
	 * §todo @see #setField(CollectableField clctfValue)
	 * 
	 * @param clctfValue The field type must match the field type of <code>getEntityField</code>.
	 * @param bNotifyListeners Are listeners to be notified?
	 */
	void setField(CollectableField clctfValue, boolean bNotifyListeners, boolean bDirty, CollectableComponentModelEvent ev) {
		if (clctfValue == null) {
			throw new NullArgumentException("clctfValue");
		}
		if (!bDirty) {
			try {
				CollectableUtils.validateFieldType(clctfValue, this.getEntityField());
				if (!(clctfValue instanceof CollectableValueIdField)) {
					if (this.getEntityField().getJavaClass().equals(Date.class) && clctfValue.getValue() != null && 
						clctfValue.getValue().equals(RelativeDate.today().toString())) {
						//ok
					}
					else if(this.getEntityField().getJavaClass().isAssignableFrom(NuclosImage.class)) {
						// special behavior
					}
					else {
						CollectableUtils.validateValueClass(clctfValue, this.getEntityField());
					}
				}
			}
			catch (CollectableFieldValidationException ex) {
				throw new CommonFatalException(ex);
			}
		}
		// NUCLOS-6806 fire field changes for new collectables!
		final boolean bForceNotifyListeners = !isExistingBo.get();
		final boolean bIsEqual = ObjectUtils.nullSafeEquals(this.clctfValue, clctfValue);

		// only if value has changed
		if (bForceNotifyListeners || !bIsEqual) {
			final CollectableField clctfOldValue = this.clctfValue;
			this.clctfValue = clctfValue;

			boolean bSetDirty = true;

			// NUCLOS-6480 Don't setDirty and don't notify when Id of an Id-Field hasn't changed
			if (AbstractCollectableField.areIdFieldsWithEqualIds(clctfOldValue, clctfValue)) {
				bSetDirty = false;
				bNotifyListeners = false;
			}

			if (bSetDirty) {
				setDirty();
			}
			if (bForceNotifyListeners || bNotifyListeners) {
				fireFieldChanged(clctfOldValue, clctfValue, ev);
			}
		}
	}

	void setField(CollectableField clctfValue, boolean bNotifyListeners, boolean bDirty) {
		setField(clctfValue, bNotifyListeners, bDirty, null);
	}
	
	/**
	 * called when the value (the CollectableField) of this model is changed.
	 */
	protected void setDirty() {
		// default: do nothing
	}

	/**
	 * clears the model and notifies the listeners.
	 * 
	 * §postcondition this.getField().isNull()
	 */
	public abstract void clear();

	public void clear(CollectableComponentModelEvent ev) {
		this.clear();
	}

	/**
	 * adds a listener that gets notified every time the value of this model changes.
	 * @param listener
	 */
	public void addCollectableComponentModelListener(IReferenceHolder outer, CollectableComponentModelListener listener) {
		listenerSupport.addCollectableComponentModelListener(outer, listener);
	}

	/**
	 * @see #addCollectableComponentModelListener
	 * @param listener
	 */
	public void removeCollectableComponentModelListener(CollectableComponentModelListener listener) {
		this.listenerSupport.removeCollectableComponentModelListener(listener);
	}

	public Map<UID, String> getMappingOfRefreshValueListParamaters() {
		Map<UID, String> map = new HashMap<UID, String>();
		for (CollectableComponentModelListener lst : listenerSupport.getCollectableComponentModelListeners()) {
			if (lst instanceof IField2Tag) {
				IField2Tag if2t = (IField2Tag) lst;
				map.putAll(if2t.mpField2Tag());
			}
		}
		return map;
	}

	void fireFieldChanged(CollectableField clctfOldValue, CollectableField clctfNewValue, CollectableComponentModelEvent ev) {
		listenerSupport.fireCollectableFieldChanged(this, clctfOldValue, clctfNewValue, ev);
	}

	void fireFieldChanged(CollectableField clctfOldValue, CollectableField clctfNewValue) {
		fireFieldChanged(clctfOldValue, clctfNewValue, null);
	}

	/**
	 * Predicate HasFieldName
	 */
	public static class HasFieldName implements Predicate<CollectableComponentModel> {
		private final String sFieldName;

		public HasFieldName(String sFieldName) {
			this.sFieldName = sFieldName;
		}

		@Override
		public boolean evaluate(CollectableComponentModel clctcompmodel) {
			return clctcompmodel.getFieldUID().equals(sFieldName);
		}
	}
	
	public boolean isInitializing() {
		return isInitializing.get();
	}
	
	public boolean isExistingBo() {
		return isExistingBo.get();
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + clctef + "," + clctfValue + "]";
	}

}  // class CollectableComponentModel
