//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.ui.gc.WeakCollectableComponentModelAdapter;
import org.nuclos.common.collect.collectable.CollectableField;

/**
 * Helper class for handling <code>CollectableComponentModelEvent</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 */
public class CollectableComponentModelListenerSupport {

	private final transient List<CollectableComponentModelListener> lstListeners = new LinkedList<CollectableComponentModelListener>();
	
	public CollectableComponentModelListenerSupport() {
	}

	public synchronized void removeCollectableComponentModelListener(CollectableComponentModelListener l) {
		for (Iterator<CollectableComponentModelListener> iterator = lstListeners.iterator(); iterator.hasNext();) {
			CollectableComponentModelListener listener = iterator.next();
			if (listener instanceof WeakCollectableComponentModelAdapter) {
				EventListener evlistener = ((WeakCollectableComponentModelAdapter)listener).getReference().get();
				if (evlistener instanceof CollectableComponentModelListener) {
					listener = (CollectableComponentModelListener) evlistener;
				}
			}
			if (listener.equals(l)) {
				iterator.remove();
				break;
			}	
		}
	}
	
	List<? extends CollectableComponentModelListener> getCollectableComponentModelListeners() {
		return Collections.unmodifiableList(lstListeners);
	}

	public void addCollectableComponentModelListener(IReferenceHolder outer, CollectableComponentModelListener l) {
		if (outer == null) {
			lstListeners.add(l);
			
		} else {
			ListenerUtil.registerCollectableComponentModelListener(this, outer, l);
			
		}
	}
	
	public <PK> Collection<PK> getListenersOfType(Class<PK> clazz) {
		Collection<PK> retVal = new HashSet<PK>();
		for (CollectableComponentModelListener l : lstListeners) {
			Object obj = l;
			
			if (obj instanceof WeakCollectableComponentModelAdapter) {
				obj = ((WeakCollectableComponentModelAdapter)obj).getReference().get();
			}
			
			if (clazz.isInstance(obj)) {
				retVal.add((PK)obj);
			}
		}
		
		return retVal;
	}
	
	/**
	 * Attention: This is the <em>intern</em> method for registering a {@link CollectableComponentModelListener}.
	 * 
	 * @deprecated Normal user should use 
	 * 		{@link #addCollectableComponentModelListener(IReferenceHolder, CollectableComponentModelListener)}.
	 */
	public synchronized void addCollectableComponentModelListener(WeakCollectableComponentModelAdapter l) {
		lstListeners.add(l);
	}

	public void fireCollectableFieldChanged(CollectableComponentModel clctcompmodel, CollectableField clctfOldValue,
			CollectableField clctfNewValue, CollectableComponentModelEvent ev) {
		LinkedHashSet<CollectableComponentModel> source;
		if (ev != null) {
			source = ev.getSources();
		} else {
			source = new LinkedHashSet<>();
		}
		source.add(clctcompmodel);
		fireCollectableFieldChangedEvent(new CollectableComponentModelEvent(source, clctfOldValue, clctfNewValue));
	}

	public void fireCollectableFieldChanged(CollectableComponentModel clctcompmodel, CollectableField clctfOldValue,
											CollectableField clctfNewValue) {
		fireCollectableFieldChanged(clctcompmodel, clctfOldValue, clctfNewValue, null);
	}

	public void fireSearchConditionChanged(SearchComponentModel clctcompmodel) {
		fireSearchConditionChangedEvent(new SearchComponentModelEvent(clctcompmodel));
	}

	public void fireValueToBeChanged(DetailsComponentModel clctcompmodel, boolean bValueToBeChanged) {
		LinkedHashSet<CollectableComponentModel> source = new LinkedHashSet<>();
		source.add(clctcompmodel);
		fireValueToBeChangedEvent(new DetailsComponentModelEvent(source, bValueToBeChanged));
	}

	private void fireCollectableFieldChangedEvent(CollectableComponentModelEvent ev) {
		for (CollectableComponentModelListener listener : new ArrayList<CollectableComponentModelListener>(lstListeners)) {
			listener.collectableFieldChangedInModel(ev);
		}
	}

	private void fireSearchConditionChangedEvent(SearchComponentModelEvent ev) {
		for (CollectableComponentModelListener listener : new ArrayList<CollectableComponentModelListener>(lstListeners)) {
			listener.searchConditionChangedInModel(ev);
		}
	}

	private void fireValueToBeChangedEvent(DetailsComponentModelEvent ev) {
		for (CollectableComponentModelListener listener : new ArrayList<CollectableComponentModelListener>(lstListeners)) {
			listener.valueToBeChanged(ev);
		}
	}

}	// class CollectableComponentModelHelper
