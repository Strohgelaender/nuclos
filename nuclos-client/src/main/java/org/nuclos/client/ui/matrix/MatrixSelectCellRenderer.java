//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.matrix;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.component.custom.FileChooserComponent;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

public class MatrixSelectCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
	
	Logger LOG = Logger.getLogger(getClass());
	public static Color markedColor = new Color(255, 255, 128);
	public static Color markedSelectColor = new Color(128, 128, 64);

	protected List<Icon> lstIcons;

	protected String cellInputType;
	protected String outputFormat;
	
	public MatrixSelectCellRenderer(String cellInputType, String outputFormat) {
		this.cellInputType = cellInputType;
		this.outputFormat = outputFormat;
		init();
	}
	
	protected void init() {
		Icon iconPlus = Icons.getInstance().getIconPlus16();
		Icon iconMinus = Icons.getInstance().getIconMinus16();
		
		lstIcons = new ArrayList<Icon>();
		lstIcons.add(Icons.getInstance().getIconEmpty16());
		
		lstIcons.add(iconPlus);
		lstIcons.add(iconMinus);
	}

	private static JLabel EMPTYLABEL = new JLabel("");

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,	boolean isSelected, boolean hasFocus, int row, int column) {

		if(value instanceof Collectable) {			
			MatrixCollectable cl = (MatrixCollectable)value;

			JComponent comp;
			if((cl.getEntityFieldMetaDataVO() != null && cl.getEntityFieldMetaDataVO().getForeignEntity() != null)) {
				Object oValue = cl.getValue(UID.UID_NULL);
				if (oValue == null) {
					oValue = "";
				}
				JLabel lb = new JLabel(oValue.toString());
				lb.setOpaque(true);
				lb.setHorizontalAlignment(SwingConstants.LEFT);
				lb.setVerticalAlignment(SwingConstants.CENTER);
				comp = lb;
			}
			else if(cl.getEntityFieldMetaDataVO() != null && cl.getEntityFieldMetaDataVO().getDataType().equals("org.nuclos.common.NuclosImage")) {
				JLabel lbImage = new JLabel();
				lbImage.setHorizontalAlignment(SwingConstants.CENTER);
				lbImage.setVerticalAlignment(SwingConstants.CENTER);
				lbImage.setOpaque(true);
				NuclosImage image = (NuclosImage)cl.getValue(UID.UID_NULL);
				if (image != null) {
					BufferedImage buffImage = image.getImage();
					if(buffImage != null) {
						if (image.getThumbnail() == null) {
							image.produceThumbnail();							
						}
						
						if (image.getThumbnail() != null) {
							ImageIcon ii = new ImageIcon(image.getThumbnail());
							lbImage.setIcon(ii);
							lbImage.setSize(ii.getIconWidth(), ii.getIconHeight());
						}
						else {
							ImageIcon icon = new ImageIcon(buffImage);
							lbImage.setIcon(icon);
							lbImage.setSize(icon.getIconWidth(), icon.getIconHeight());
						}
					
					}
				}
				comp = lbImage;
			}
			else if((cl.getEntityFieldMetaDataVO() != null && cl.getEntityFieldMetaDataVO().getDataType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile"))) {
				FileChooserComponent fileChooser = new FileChooserComponent();
				GenericObjectDocumentFile file = (GenericObjectDocumentFile)cl.getValue(UID.UID_NULL);
				if(file != null) {
					String sFileName = file.getFilename();
					fileChooser.setFileName(sFileName);
				}
				fileChooser.setEnabled(false);
				fileChooser.setOpaque(true);
				comp = fileChooser;
			}
			else if((cl.getEntityFieldMetaDataVO() != null && cl.getEntityFieldMetaDataVO().getDataType().equals("java.lang.Integer"))) {
				JLabel lb = null;
				Integer i = null;
				if(cl.getValue(UID.UID_NULL) != null) {
					i = (Integer) cl.getValue(UID.UID_NULL);
					lb = new JLabel(i.toString());
				}
				else {
					lb = new JLabel();
				}
				lb.setOpaque(true);
				lb.setHorizontalAlignment(SwingConstants.RIGHT);
				lb.setVerticalAlignment(SwingConstants.CENTER);
				comp = lb;
			}
			else if((cl.getEntityFieldMetaDataVO() != null && (cl.getEntityFieldMetaDataVO().getDataType().equals("java.util.Date")
					|| cl.getEntityFieldMetaDataVO().getDataType().equals("org.nuclos.common2.InternalTimestamp")))) {
				Date date = (Date)cl.getValue(UID.UID_NULL);
				SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
				String dateValue = "";
				if(date != null) {
					dateValue = sdf.format(date);
				}
				JLabel lb = new JLabel(dateValue);
				lb.setHorizontalAlignment(SwingConstants.LEFT);
				lb.setVerticalAlignment(SwingConstants.CENTER);
				lb.setOpaque(true);
				comp = lb;
			}
			else if(cl.getValue(UID.UID_NULL) instanceof Double) {
				String formattedValue = "";
				if(value != null) {
					formattedValue = value.toString();
					if(outputFormat != null) {
						formattedValue = new DecimalFormat(outputFormat).format(new Double(value.toString()));
					}
				}
				JLabel lb = new JLabel(formattedValue);
				lb.setHorizontalAlignment(SwingConstants.CENTER);
				lb.setVerticalAlignment(SwingConstants.CENTER);
				lb.setOpaque(true);
				comp = lb;
			}
			else if(cl.getValue(UID.UID_NULL) instanceof Integer) {

				if(LayoutMLConstants.ATTRIBUTE_CELL_INPUT_TYPE_TEXTFIELD.equals(cellInputType)) { // textfield

					String sValue = cl.getValue(UID.UID_NULL).toString();
					JLabel lb = new JLabel(sValue);
					lb.setHorizontalAlignment(SwingConstants.LEFT);
					lb.setVerticalAlignment(SwingConstants.CENTER);
					lb.setOpaque(true);
					comp = lb;
				} else { // check icon
					Integer iIconCount = (Integer) cl.getValue(UID.UID_NULL);
					Icon icon = lstIcons.get(iIconCount);
					JLabel lb = new JLabel(icon);
					lb.setHorizontalAlignment(SwingConstants.CENTER);
					lb.setVerticalAlignment(SwingConstants.CENTER);
					lb.setOpaque(true);
					comp = lb;
				}
			}
			else if(cl.getValue(UID.UID_NULL) instanceof String) {
				String sValue = (String) cl.getValue(UID.UID_NULL);
				JLabel lb = new JLabel(sValue);
				lb.setHorizontalAlignment(SwingConstants.LEFT);
				lb.setVerticalAlignment(SwingConstants.CENTER);
				lb.setOpaque(true);
				comp = lb;
			}
			else if(cl.getValue(UID.UID_NULL) instanceof Boolean || (cl.getEntityFieldMetaDataVO() != null && cl.getEntityFieldMetaDataVO().getDataType().equals("java.lang.Boolean"))) {
				Boolean bValue = Boolean.FALSE;
				if(cl.getValue(UID.UID_NULL) != null) {
					bValue = (Boolean) cl.getValue(UID.UID_NULL);										
				}
				JCheckBox box = new JCheckBox();
				box.setHorizontalAlignment(SwingConstants.CENTER);
				box.setHideActionText(true);
				box.setSelected(bValue);
				box.setOpaque(true);
				comp = box;
			}
			else {
				JLabel lb = new JLabel(value.toString());
				lb.setHorizontalAlignment(SwingConstants.CENTER);
				lb.setVerticalAlignment(SwingConstants.CENTER);
				lb.setOpaque(true);
				comp = lb;
			}

			MatrixCollectable.GroupKey ygroupkey = cl.getYgroupkey();
			if (ygroupkey != null) {
				if (ygroupkey.getIdenticalFields().contains(cl.getField())) {
					comp = new JLabel("");
					comp.setOpaque(true);
				}
				comp.setBorder(ygroupkey.getBorder(column == 1, false));
				if (comp instanceof JCheckBox) {
					((JCheckBox)comp).setBorderPainted(true);
				}
			}

			if (cl.isMarked()) {
				comp.setBackground(isSelected ? markedSelectColor : markedColor);
			}
			else if (isSelected) {
				comp.setBackground(table.getSelectionBackground());
			}

			return comp;
		}		
		else {
			if(value == null) {
				return EMPTYLABEL;
			}
			else {
				return EMPTYLABEL;
			}
		}
	}	

}
