//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.subform;

import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.RowFilter;
import javax.swing.RowSorter;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.JTextComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.genericobject.valuelistprovider.MandatorCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.ProcessCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusCollectableFieldsProvider;
import org.nuclos.client.genericobject.valuelistprovider.StatusNumeralCollectableFieldsProvider;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.collect.CollectableResultComponent;
import org.nuclos.client.ui.collect.subform.FixedColumnRowHeader.FixedRowIndicatorTableModel;
import org.nuclos.client.ui.collect.result.ResultFilter;
import org.nuclos.client.ui.collect.component.AbstractCollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.collect.result.FilterableTable;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.valuelistprovider.VLPClientUtils;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.preferences.ColumnPreferences;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;

import sun.awt.CausedFocusEvent;

/**
 * SubFormFilter that handles the collapsible filter panels for the
 * fixed and external tables of a SubForm and filters the corresponding table data.
 * <p>
 * containing the {@link SubFormFilterPanel}s.
 * 
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 01.00.00
 */
public class SubFormFilter extends ResultFilter {

	private static final Logger LOG = Logger.getLogger(SubFormFilter.class);

	private JCheckBoxMenuItem miFilter;

	private SubFormParameterProvider parameterProvider;
	private UID parentEntityUid;
	private Long intidForVLP;
	private UID mandator;
	public SubForm subform;
	private List<SubFormFilterChangeListener> listeners = new ArrayList<>();

	SubFormFilter(SubForm subform) {
		super(subform);
		this.subform=subform;
	}

	void setupFilter(SubFormParameterProvider parameterProvider, UID parentEntityUid,
					 CollectableFieldsProviderFactory collectableFieldsProviderFactory, Long intidForVLP, UID mandator) {
		super.setupFilter(parentEntityUid, collectableFieldsProviderFactory);

		this.parentEntityUid = parentEntityUid;
		this.parameterProvider = parameterProvider;
		this.intidForVLP = intidForVLP;
		this.mandator = mandator;

		getMenuItem().addActionListener(this);
	}

	@Override
	public void createFilter(final boolean nonEmptyFilterLoaded) {
		if (created) {
			return;
		}

		created = true;

		_createFilter();
		this.miFilter = getMenuItem();
        setSubFormParameterProvider(parameterProvider);

		if (nonEmptyFilterLoaded) {
			loadTableFilter();
		} else {
			doFiltering();
		}
	}

	@Override
	protected JTable getFixedTable() {
		if (!(resultComponent instanceof SubForm)) {
			return null;
		}

		SubForm subForm = (SubForm) resultComponent;
		return subForm.getSubformRowHeader().getHeaderTable();
	}

	@Override
	protected FilterableTable getExternalTable() {
		if (!(resultComponent instanceof SubForm)) {
			return null;
		}

		SubForm subForm = (SubForm) resultComponent;
		return subForm.getSubformRowHeader().getExternalTable();
	}

	/**
	 * actionlistener to collapse or expand the searchfilter panels
	 */
	protected void addActionListener() {
		if (!(resultComponent instanceof SubForm)) {
			return;
		}

		SubForm subForm = (SubForm) resultComponent;

		ListenerUtil.registerSubFormToolListener(subForm, this, new SubForm.SubFormToolListener() {
			@Override
			public void toolbarAction(String actionCommand) {
				if (ToolbarFunction.fromCommandString(actionCommand) == ToolbarFunction.FILTER) {
					// collapse removes the filter, expanding filters with the (maybe set) entered filters
					if (!getFixedResultFilter().isCollapsed() || !getExternalResultFilter().isCollapsed()) {
						//NUCLEUSINT-789 f
						clearFilter();

						miFilter.setSelected(false);
						filterButton.setSelected(false);
					} else {
						filter();

						miFilter.setSelected(true);
						filterButton.setSelected(true);
					}

					getFixedResultFilter().setCollapsed(!getFixedResultFilter().isCollapsed());
					getExternalResultFilter().setCollapsed(!getExternalResultFilter().isCollapsed());
				}
			}
		});
	}

	@Override
	protected void handleVLP(final UID sfEntityUid, final CollectableEntityField cef, final UID columnName, final CollectableComponent clctcomp, final CollectableResultComponent clctResultComponent) {

		if (!(clctResultComponent instanceof SubForm)) {
			return;
		}


		//BMWFDM-322 et.al: In the search (and only there) the ValueListProviders will not be considered for List of Values (LOV).
		//This has been already the case for standard search mask and from now for Subform-Search-Filters as well.

		final SubForm subForm = (SubForm) clctResultComponent;
		// handle valuelistprovider
		final LabeledCollectableComponentWithVLP clctWithVLP = (LabeledCollectableComponentWithVLP) clctcomp;
		final UID fieldUid = clctWithVLP.getFieldUID();
		CollectableFieldsProvider valuelistprovider = subForm.getValueListProvider(columnName);
		if (valuelistprovider == null && cef.isReferencing()) {
			valuelistprovider = collectableFieldsProviderFactory
					.newDefaultCollectableFieldsProvider(fieldUid);
		}
		clctWithVLP.setValueListProvider(valuelistprovider);
		if (!subForm.isLayout()) {
			clctWithVLP.refreshValueList(true);
		}
		final FocusAdapter refreshVLPAdapter = new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {

				if (e instanceof CausedFocusEvent) {
					CausedFocusEvent ce = (CausedFocusEvent) e;
					if (!CausedFocusEvent.Cause.ACTIVATION.equals(ce.getCause())) {
						// set the value list provider (dynamically):
						CollectableFieldsProvider valuelistprovider = subForm.getValueListProvider(fieldUid);
						if (valuelistprovider == null) {
							// If no provider was set, use the default provider for static cell editors by default:
							if (LangUtils.equal(SF.STATE.getUID(sfEntityUid), fieldUid)) {
								valuelistprovider = new StatusCollectableFieldsProvider(sfEntityUid, null);
							} else if (LangUtils.equal(SF.STATENUMBER.getUID(sfEntityUid),
									fieldUid)) {
								valuelistprovider = new StatusNumeralCollectableFieldsProvider(sfEntityUid, null);
							} else if (LangUtils.equal(SF.PROCESS.getUID(sfEntityUid),
									fieldUid)) {
								valuelistprovider = new ProcessCollectableFieldsProvider(sfEntityUid);
							} else if (LangUtils.equal(SF.MANDATOR.getUID(sfEntityUid),
									fieldUid)) {
								valuelistprovider = new MandatorCollectableFieldsProvider(sfEntityUid, null);
							} else {
								valuelistprovider = collectableFieldsProviderFactory
										.newDefaultCollectableFieldsProvider(fieldUid);
							}
						}
						clctWithVLP.setValueListProvider(valuelistprovider);

						final Collection<RefreshValueListAction> collRefreshValueListActions = subForm
								.getRefreshValueListActions(fieldUid);
						if (!collRefreshValueListActions.isEmpty()) {
							// set parameters:
							for (RefreshValueListAction rvlact : collRefreshValueListActions) {
								SubForm.setParameterForRefreshValueListAction(rvlact, -1, clctWithVLP,
										(SubFormTableModel) subForm.getSubformTable().getModel(), parameterProvider);
							}
						}

						JTextComponent compText = null;
						JComponent comp = clctcomp.getControlComponent();
						if (comp instanceof ListOfValues) {
							compText = ((ListOfValues) comp).getJTextField();
						} else if (comp instanceof JComboBox) {
							compText = (JTextComponent) ((JComboBox) comp).getEditor().getEditorComponent();
						}

						// remember old value here.
						String clctfValue = compText.getText();
						// refresh value list:
						if (VLPClientUtils.setVLPBaseRestrictions(fieldUid, valuelistprovider, intidForVLP, mandator)) {
							clctWithVLP.refreshValueList(false);
						}
						compText.setText(clctfValue);
					}
				}
			}
		};
		JComponent comp = clctcomp.getControlComponent();
		if (comp instanceof ListOfValues) {
			((ListOfValues) comp).getJTextField().addFocusListener(refreshVLPAdapter);
		} else if (comp instanceof JComboBox) {
			for (Component c : ((JComboBox) comp).getComponents()) {
				if (c instanceof JButton)
					((JButton) c).addMouseListener(new MouseAdapter() {
						@Override
						public void mouseEntered(MouseEvent e) {
							refreshVLPAdapter.focusGained(null);
						}
					});
			}
			((JComboBox) comp).getEditor().getEditorComponent().addFocusListener(refreshVLPAdapter);
		}
	}

	public void setSubFormParameterProvider(SubFormParameterProvider parameterProvider) {
		this.parameterProvider = parameterProvider;
	}

	/*
	static Preferences getFilterPreferences(String entityName, SubForm subform) {
		return ClientPreferences.getInstance().getUserPreferences().node("collect").node("entity").node(entityName)
				.node("subformfilter").node(subform.getEntityUID().getString());
	}
	 */

	private void setFixedSubFormFilter(JTable table, TableColumnModel columnModel,
			Map<UID, CollectableComponent> searchFilterComponents) {
		setFixedResultFilter(table, columnModel, searchFilterComponents);
	}

	public SubFormFilterPanel getFixedSubFormFilter() {
		return (SubFormFilterPanel) getFixedResultFilter();
	}

	private void setExternalSubFormFilter(JTable table, TableColumnModel columnModel,
			Map<UID, CollectableComponent> searchFilterComponents) {
		setExternalResultFilter(table, columnModel, searchFilterComponents);
	}

	public SubFormFilterPanel getExternalSubFormFilter() {
		return (SubFormFilterPanel) getExternalResultFilter();
	}

	/**
	 * filters the tablemodel using the entered data in the filter components located above the columns
	 */
	@Override
	protected void filter() {
		// subformFilterPanel is invisible by default. @see NUCLOSINT-1630
		if (getFixedResultFilter() == null || getExternalResultFilter() == null)
			return;

		getFixedResultFilter().setVisible(true);
		getExternalResultFilter().setVisible(true);

		Map<UID, CollectableComponent> columnFilters = getAllFilterComponents();
		if (this.subform.getMaxEntries() == null || this.subform.getMaxEntries() == 0) {
			ArrayList<RowFilter<TableModel, Integer>> filters = new ArrayList<RowFilter<TableModel, Integer>>();
			CollectableEntityFieldBasedTableModel tablemodel = externalTable.getCollectableEntityFieldBasedTableModel();

			for (int index = 0; index < tablemodel.getColumnCount(); index++) {
				CollectableEntityField cef = tablemodel.getCollectableEntityField(index);

				CollectableComponent clctcomp = columnFilters.get(cef.getUID());

				try {
					if (clctcomp == null || clctcomp.getSearchCondition() == null) {//Field().isNull()) {
						continue;
					}
				} catch (CollectableFieldFormatException e) {
					throw new CommonFatalException(e);
				}

				filters.add(new SubFormRowFilter(cef, clctcomp,subform));
			}

			RowFilter<TableModel, Integer> filter = null;
			filters.addAll(fixedRowFilters);
			if (filters.size() == 1) {
				filter = filters.get(0);
			} else if (filters.size() > 1) {
				filter = RowFilter.andFilter(filters);
			}

			final RowSorter<?> rowSorter = externalTable.getRowSorter();
			if (rowSorter instanceof TableRowSorter) {
				((TableRowSorter<?>) rowSorter).setRowFilter(filter);
			}
			filteringActive = (filter != null);

			fireFilterChanged(filter);
		} else {
			if (columnFilters != null) {
				CompositeCollectableSearchCondition filterCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND);
				for (CollectableComponent clctComponent : columnFilters.values()) {
					filteringActive = true;
					if (clctComponent instanceof AbstractCollectableComponent) {
						AbstractCollectableComponent component = (AbstractCollectableComponent) clctComponent;
						CollectableSearchCondition compSc = null;
						try {
							compSc = component.getSearchCondition();
						} catch (CollectableFieldFormatException e) {
							throw new CommonFatalException(e);
						}
						if (compSc != null) {
							filterCondition.addOperand(compSc);
						}
					}
				}
				fireFilterChanged(filterCondition);
			}
		}
		if (parameterProvider != null) {
			storeTableFilter();
		}
	}

	private void fireFilterChanged(Object filter) {
		SubFormFilterChangeEvent e = new SubFormFilterChangeEvent(this, filter);

		for (SubFormFilterChangeListener l : listeners) {
			l.filterChanged(e);
		}
	}

	/**
	 * Removes filtering
	 * NUCLEUSINT-789 f
	 */
	public void clearFilter() {
		Icon icon = Icons.getInstance().getIconFilter16();
		this.filterButton.setIcon(icon);
		this.miFilter.setIcon(icon);

		RowSorter<?> rowSorter = externalTable.getRowSorter();
		if (rowSorter instanceof TableRowSorter) {
			((TableRowSorter<?>) rowSorter).setRowFilter(null);
		}
		filteringActive = false;
	}

	public void removeFiltering() {
		super.removeFiltering();
		if (miFilter != null) {
			miFilter.setSelected(false);
		}
	}

	public SubFormRowFilter newSubformRowFilter(CollectableEntityField cef, CollectableComponent clctcomp) {
		if (created) {
			return new SubFormRowFilter(cef, clctcomp,subform);
		}
		return null;
	}

	public void addFilterChangeListener(SubFormFilterChangeListener listener) {
		this.listeners.add(listener);
	}

	public class SubFormRowFilter extends RowFilter<TableModel, Integer> {
		private CollectableEntityField cef;
		private CollectableComponent clctcomp;
		private SubForm subform;
		
		public SubFormRowFilter(CollectableEntityField cef, CollectableComponent clctcomp,SubForm sub) {
			this.clctcomp = clctcomp;
			this.cef = cef;
			this.subform=sub;
		}

		@Override
		public boolean include(RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {			
			TableModel model = entry.getModel();			
			if (model instanceof FixedRowIndicatorTableModel) {
				model = ((FixedRowIndicatorTableModel) model).getExternalModel();
			}
			
			int colIndex = -1;
			if (model instanceof SubFormTableModel) {
				colIndex = ((SubFormTableModel) model).findColumnByFieldUid(cef.getUID());
				
			}

			if (colIndex == -1) {
				return true;
			}

			try {
				/**
				 * NUCLOS-1897
				 * Valuelist Provider is executed by rendering, the model only contains id's. This results in an invalid string comparison.
				 * Solution: References get compared by id (maybe later by BusinessObject comparables?)
				 */
				boolean comparisonByReference = false;
				if (!SubFormFilter.this.filterByString && clctcomp.getField() instanceof CollectableValueIdField) {
					if (clctcomp.getField().getValueId() != null)
						comparisonByReference = true;
				}

				Object v = ((SubFormTableModel) model).getValueAt(entry.getIdentifier(), colIndex);

				
				
				if (!(v instanceof CollectableField))
					return true;

				CollectableField f = (CollectableField) v;

				Object o1 = null;

				if (comparisonByReference) {
					if (f.getValueId() instanceof UID) {
						o1 = f.getValueId();
					} else {
						o1 = IdUtils.toLongId(f.getValueId());
					}
				} else {
					o1 = f.getValue();
				}
		
				// NUCLOSINT-823: If the field references an optional foreign key, it could be null.
				// In this case, we want to exclude it from the search, as a value for the foreign 
				// key was given in the search query. (Thomas Pasch)
				if (o1 == null && comparisonByReference) { // with the comparisonByReference i think NUCLOSINT-823 is respected enough here.
					return false;
				}

				final Comparable cellValue = o1 == null ? null : ((o1 instanceof Comparable) ? (Comparable) o1
						: toFilterableString(o1).toLowerCase());
				String cellValueAsStr = toFilterableString(cellValue) == null ? null : toFilterableString(cellValue)
						.toLowerCase();

				final ComparisonOperator comparisonOperator;
				final Comparable filterValue;

				CollectableSearchCondition cond = clctcomp.getSearchCondition();
				Object o = null;
				if (comparisonByReference) {
					if (clctcomp.getField().getValueId() instanceof UID) {
						o = clctcomp.getField().getValueId();
					} else {
						o = IdUtils.toLongId(clctcomp.getField().getValueId());
					}
				} else {
					o = getFilterValue(clctcomp);
				}
				// no search condition means no filtering, so return true
				if (cond == null) {
					return true;
				}				
				
				// Handle Timestamps -> Remove the times that only dates are compared
				if(o1 instanceof InternalTimestamp && o instanceof InternalTimestamp){
					
					((InternalTimestamp)o1).setHours(0);
					((InternalTimestamp)o1).setMinutes(0);
					((InternalTimestamp)o1).setSeconds(0);
					((InternalTimestamp)o1).setNanos(0);
					
					((InternalTimestamp)o).setHours(0);
					((InternalTimestamp)o).setMinutes(0);
					((InternalTimestamp)o).setSeconds(0);
					((InternalTimestamp)o).setNanos(0);					
				}
				
				String filterValueAsString = null;
				if (cond instanceof AtomicCollectableSearchCondition) {
					comparisonOperator = ((AtomicCollectableSearchCondition) cond).getComparisonOperator();
					if (cond instanceof CollectableLikeCondition) {
						filterValue = o.toString().toLowerCase();
						filterValueAsString = filterValue.toString();
					} else {
						filterValue = (o instanceof Comparable) ? (Comparable) o
								: (toFilterableString(o) == null ? null : toFilterableString(o).toLowerCase());
						filterValueAsString = toFilterableString(o) == null ? null : toFilterableString(o)
								.toLowerCase();
					}
				} else {
					return true;
				}

			
				
				switch (comparisonOperator) {
				case NONE:
					return true;
				case EQUAL:
					return RigidUtils.equal(cellValue, filterValue);
				case LESS:
					return RigidUtils.compareComparables(cellValue, filterValue) < 0;
				case GREATER:
					return RigidUtils.compareComparables(cellValue, filterValue) > 0;
				case LESS_OR_EQUAL:
					return RigidUtils.compareComparables(cellValue, filterValue) <= 0;
				case GREATER_OR_EQUAL:
					return RigidUtils.compareComparables(cellValue, filterValue) >= 0;
				case NOT_EQUAL:
					return !RigidUtils.equal(cellValue, filterValue);
				case LIKE:
					return cellValueAsStr != null && cellValueAsStr.matches(filterValueAsString.replaceAll("\\*", "\\.\\*"));
				case NOT_LIKE:
					return cellValueAsStr == null || !cellValueAsStr.matches(filterValueAsString.replaceAll("\\*", "\\.\\*"));
				case IS_NULL:
					return cellValue == null;
				case IS_NOT_NULL:
					return cellValue != null;
				default:
					
					//MultiselectSearchfilter
					Column columnNeu=(Column)subform.getColumn(cef.getUID());
					
					if(columnNeu.isMultiselectsearchfilter() && filterValueAsString!=null)
					{						
						return Arrays.asList(filterValueAsString.split(",")).contains(cellValueAsStr);						
					}				
					else
						return (cellValueAsStr.indexOf(filterValueAsString) == -1 ? false : true); // do a like search per default this.isEqual(cellValue, filterValue);
					
				}
			} catch (CollectableFieldFormatException e) {
				LOG.warn("include failed: " + e, e);
				JOptionPane
						.showConfirmDialog(
								null,
								SpringLocaleDelegate.getInstance().getMessage(
										"subform.filter.exception",
										"Das Format der Suchkomponente '{0}' ist nicht korrekt.", cef.getLabel()),
								SpringLocaleDelegate.getInstance().getMessage("subform.filter.exception.title",
										"Formatfehler"),
								JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
				return true;
			}
		}

		/**
		 * converts the given String into a comparable String which can be
		 * interpreted by this RowFilter
		 * @param value
		 * @return a comparable string
		 */
		private String toFilterableString(Object value) {
			if (value == null) {
				return null;
			}

			String result = "";

			if (cef.isReferencing() || cef.getJavaClass().equals(String.class)) {
				result = value.toString();
			}
			else /*if (cef.getJavaClass().equals(Date.class)) {
					result = String.format("%TF", value);
					}
					else if (cef.getJavaClass().equals(Integer.class)) {
					result = String.format("%0100d", value);
					}
					else if (cef.getJavaClass().equals(Double.class)) {
					result = String.format("%0111.010f", value);
					}
					else if (cef.getJavaClass().equals(Boolean.class)) {
					result = (value.equals(Boolean.TRUE)) ? "1" : "0";
					}
					else {
					result = value.toString();
					}*/
			{
				result = CollectableFieldFormat.getInstance(cef.getJavaClass()).format(cef.getFormatOutput(), value);
			}

			return result;
		}

	}

	/**
	 * stores the content of the table filter in the preferences
	 */
	private void storeTableFilter() {
		// 1. remove all filter entries
		final TablePreferences tp = getTablePreferences();
		if(tp == null) {
			return;
		}
		for (ColumnPreferences cp: tp.getSelectedColumnPreferences()) {
			cp.setColumnFilter(null);
			cp.setColumnFilterOp(null);
		}
		// 2. Do nothing in case the filter is not active
		if (!isFilteringActive()) {
			parameterProvider.updateSelectedProfile();
			return;
		}
		// 3. Otherwise, store all filter entries
		final Map<UID, CollectableComponent> comps = getAllFilterComponents();
		for (ColumnPreferences cp: tp.getSelectedColumnPreferences()) {
			final AbstractCollectableComponent comp = (AbstractCollectableComponent) comps.get(cp.getColumn());
			if (comp == null) {
				continue;
			}
			final Class<?> type = comp.getEntityField().getJavaClass();
			final Object filter = getFilterValue(comp);
			// Special handling for checkboxes - needed for http://project.nuclos.de/browse/LIN-414 (tp)
			if (Boolean.class.equals(type)) {
				cp.setColumnFilter(filter);
				cp.setColumnFilterOp(filter != null ? ComparisonOperator.EQUAL : ComparisonOperator.NONE);
			} else {
				if (filter != null) {
					cp.setColumnFilter(filter);
					cp.setColumnFilterOp(comp.getComparisonOperator());
				}
			}
		}
		parameterProvider.updateSelectedProfile();
	}

	@Override
	public boolean isFilterStored() {
		if (!(resultComponent instanceof SubForm)) {
			return false;
		}
		final TablePreferences tp = parameterProvider.getSelectedProfile().getPreferences();
		boolean nonEmptyFilterLoaded = false;
		if(tp == null) {
			return false;
		}
		for (ColumnPreferences cp: tp.getSelectedColumnPreferences()) {
			try {
				final Object filter = cp.getColumnFilter();
				if (filter != null) {
					nonEmptyFilterLoaded = true;
					break;
				}
			} catch (NoSuchMethodError e) {
				// ignore
			}
		}
		return nonEmptyFilterLoaded;
	}
	
	private boolean ifFilterIsStoredRestore() {
		final TablePreferences tp = getTablePreferences();
		boolean nonEmptyFilterLoaded = false;
		final Map<UID, CollectableComponent> uid2comp = getAllFilterComponents();
		for (ColumnPreferences cp: tp.getSelectedColumnPreferences()) {
			final UID fieldUid = cp.getColumn();
			final Object filter = cp.getColumnFilter();
			if (filter != null) {
				// ???
				final CollectableField field = new CollectableValueField(filter);
				final AbstractCollectableComponent comp = (AbstractCollectableComponent) uid2comp.get(fieldUid);
				if (comp != null) {
					comp.setField(field);
					comp.setComparisonOperator(cp.getColumnFilterOp());
					nonEmptyFilterLoaded = true;
				}
			}
		}
		return nonEmptyFilterLoaded;
	}
	
	TablePreferences getTablePreferences() {
		return parameterProvider.getSelectedProfile().getPreferences();
	}

	/**
	 * load the content of the table filter of the preferences
	 */
	@Override
	protected void loadTableFilter() {
		final boolean nonEmptyFilterLoaded = ifFilterIsStoredRestore();
		if (nonEmptyFilterLoaded) {
			filteringActive = true;
			getExternalResultFilter().setCollapsed(false);
			getFixedResultFilter().setCollapsed(false);
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					// does not work: filter is always collapsed first time
					// filterButton.doClick();
					// much better
					filter();
				}
			});
		}
	}

	@Override
	public JToggleButton getToggleButton() {
		if (!(resultComponent instanceof SubForm)) {
			return null;
		}
		return (JToggleButton) ((SubForm)resultComponent).getToolbarButton(ToolbarFunction.FILTER.name());
	}

	@Override
	protected JCheckBoxMenuItem getMenuItem() {
		if (!(resultComponent instanceof SubForm)) {
			return null;
		}
		return (JCheckBoxMenuItem) ((SubForm)resultComponent).getMenuItem(ToolbarFunction.FILTER.name());
	}

}
