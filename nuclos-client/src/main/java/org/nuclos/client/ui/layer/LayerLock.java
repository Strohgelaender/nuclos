package org.nuclos.client.ui.layer;

import java.lang.ref.WeakReference;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.ext.LockableUI;

/**
 * Layer locking object that hides the details of the jxlayer implementation
 * from the user code.
 * <p>
 * This is the central class of the refactoring triggered by NUCLOS-2523.
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.15.3
 */
public class LayerLock {
	
	private static final Logger LOG = Logger.getLogger(LayerLock.class);

	/**
	 * Avoid memory leaks. If the JXLayer is only referenced from
	 * here, it is not in the GUI tree any more. (tp)
	 */
	private final WeakReference<JXLayer<JComponent>> layer;
	
	private boolean locked;
	
	public LayerLock(JXLayer<JComponent> layer) {
		if (layer == null) {
			throw new NullPointerException();
		}
		if (!(layer.getUI() instanceof LockableUI)) {
			throw new IllegalArgumentException();
		}
		this.layer = new WeakReference<JXLayer<JComponent>>(layer);
		this.locked = ((LockableUI)layer.getUI()).isLocked();
	}
	
	public JXLayer<JComponent> getLayer() {
		return layer.get();
	}
	
	public void setLocked(boolean locked) {
		try {
			getLockableUI().setLocked(locked);
			this.locked = locked;
		} catch (NullPointerException npe) {
			LOG.debug("Lockable UI does not exist any more", npe);
		}
	}
	
	public boolean isLocked() {
		return this.locked;
	}
	
	private LockableUI getLockableUI() {
		final JXLayer<JComponent> l = layer.get();
		if (l == null) {
			return null;
		} else {
			return (LockableUI) l.getUI();
		}
	}
}
