//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.labeled.LabeledComponent;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;

public abstract class LabeledCollectableComponentWithVLP extends LabeledCollectableComponent implements Parameterisable, CollectableComponentWithValueListProvider {
	private static final Logger LOG = Logger.getLogger(LabeledCollectableComponentWithVLP.class);

	protected CollectableFieldsProvider valueListProvider;
	
	protected LabeledCollectableComponentWithVLP(CollectableEntityField clctef,
        LabeledComponent labcomp, boolean bSearchable) {
	    super(clctef, labcomp, bSearchable);
    }

	/**
	 * sets the value list provider that provides the dropdown values for this combobox.
	 * @param clctfsprovider
	 */
	@Override
    public void setValueListProvider(CollectableFieldsProvider clctfsprovider) {
		valueListProvider = clctfsprovider;
	}

	/**
	 * @return the value list provider that provides the dropdown values for this combobox.
	 */
	@Override
    public CollectableFieldsProvider getValueListProvider() {
		return valueListProvider;
	}

	/**
	 * refreshes the list of values by asking the value list provider.
	 * If no value list provider was set, the model will be empty.
	 */
	@Override
	public void refreshValueList(boolean async) {
		refreshValueList(async, true);
	}
	
	/**
	 * refreshes the list of values by asking the value list provider.
	 * If no value list provider was set, the model will be empty.
	 */
	@Override
	public void refreshValueList(boolean async, boolean bSetDefault) {
		refreshValueList(async, bSetDefault, false);
	}
	
	/**
	 * refreshes the list of values by asking the value list provider.
	 * If no value list provider was set, the model will be empty.
	 */
	@Override
	public abstract void refreshValueList(boolean async, boolean bSetDefault, boolean isInitial);

	@Override
	public void refreshValueList() {
		refreshValueList(false);
	}

	@Override
    public void setParameter(String sName, Object oValue) {
		getValueListProvider().setParameter(sName, oValue);
	}

	protected abstract void refreshValueListFromApplyParameters(boolean async, boolean isInitial);

	@Override
    public void applyParameters() throws CommonBusinessException {
		refreshValueListFromApplyParameters(false, true);
	}

	@Override
    public void applyParameters(boolean async) throws CommonBusinessException {
		refreshValueListFromApplyParameters(async, true);
	}

	@Override
    public void applyParameters(boolean async, boolean isInitial) throws CommonBusinessException {
		refreshValueListFromApplyParameters(async, isInitial);
	}
	
	protected abstract List<Object> getCurrentItems();
	
	protected abstract boolean isMultiSelect();
	
	public abstract boolean isInsertable();
	
	public abstract JComboBox getJComboBox();
	
	protected DefaultComboBoxModel getDefaultComboBoxModel() {
		return (DefaultComboBoxModel) getJComboBox().getModel();
	}
	
	protected void requestValueList() {
	}
	
	/**
	 * @return the list of (possible) values (dropdown list)
	 */
	public List<CollectableField> getValueList() {
		//NUCLOS-5037 ValueList is requested, so make sure that combo-box is filled.
		requestValueList();
		
		final List<CollectableField> result = new ArrayList<CollectableField>();
		final DefaultComboBoxModel<?> model = getDefaultComboBoxModel();
		for (int i = 0; i < model.getSize(); i++) {
			result.add((CollectableField) model.getElementAt(i));
		}
		return result;
	}
	

	protected CollectableSearchCondition getSearchConditionFromComboItems() throws CollectableFieldFormatException {
		if (!isSearchComponent()) {
			throw new IllegalStateException("!isSearchComponent()");
		}
		List<Object> items = getCurrentItems();
		CollectableSearchCondition cond;
		if (items.size() == 0) {
			cond = null;
		} else if (items.size() == 1 && !isMultiSelect()) {
			String search = LangUtils.toString(items.get(0));
			cond = getSearchConditionFromViewImpl(search);					
		} else {
			Map<Object, String> keyMap = new HashMap<Object, String>();
			for (Object o : items) {
				if (o instanceof CollectableValueIdField) {
					CollectableValueIdField cvif = (CollectableValueIdField) o;
					keyMap.put(cvif.getValueId(), LangUtils.toString(cvif.getValue()));
				}
			}
			cond = new CollectableInCondition<String>(getEntityField(), keyMap);
		}
		return cond;
	}

	protected boolean bMultiEditInSubform = false;	
	protected boolean bSubformComponent = false;

	public void setMultiEditInSubform(boolean bMultiEdit) {
		this.bMultiEditInSubform = bMultiEdit;
		this.bSubformComponent = true;
	}

	public void setBackgroundColor(final Color color) {
		getJComboBox().setBackground(color);
		((JTextField)getJComboBox().getEditor().getEditorComponent()).setBackground(color);
		
		getJComboBox().setRenderer(new CollectableFieldRenderer() {
			@Override
			protected void paintComponent(Graphics g) {
				setBackground(color);
				super.paintComponent(g);
			}
		});
	}
	
	protected abstract Integer getColumns();
	
	protected abstract CollectableField getClctfExtra();

	public class CollectableFieldRenderer extends DefaultListCellRenderer {

		@Override
		public Component getListCellRendererComponent(JList list, Object oValue, int iIndex, boolean bSelected, boolean bCellHasFocus) {
			super.getListCellRendererComponent(list, oValue, iIndex, bSelected, bCellHasFocus);

			// reset preferred size:
			setPreferredSize(null);

			final CollectableField clctf = (CollectableField) oValue;
			Color colorForeground = bSelected ? list.getSelectionForeground() : list.getForeground();
			if (clctf == null) {
				LOG.warn("CollectableFieldRenderer.getListCellRendererComponent: oValue == null");
			}
			else {
				String sText = null;
				if (clctf.equals(getClctfExtra())) {
					// NUCLOS-82	colorForeground = Color.RED;
					if (StringUtils.looksEmpty(LangUtils.toString(clctf.getValue()))) {
						sText = "<ung\u00fcltiger Eintrag>";
					}
				}
				// The space character for the null entry is important because otherwise the cell has minimal height.
				// Note that this is only for display. It doesn't have any impact on the internal value.
				if (sText == null) {
					sText = StringUtils.looksEmpty(LangUtils.toString(clctf.getValue())) ? " " : clctf.toString();
				}
				setText(sText);

				// add 10 pixel to the component width, otherwise the largest item
				// could not be shown completely in the combobox
				int height = getPreferredSize().height;
				int width = getPreferredSize().width;
				setPreferredSize(new Dimension(width+10, height));

				// Set a tooltip if the text cannot be displayed completely:
				final String sToolTipText = isToolTipNecessary() ? (StringUtils.looksEmpty(sText) ? null : sText) : null;
				setToolTipText(sToolTipText);
			}
			setForeground(colorForeground);

			// Does not work - see header!
			if (iIndex < 0) {
				Color background = getJComboBox().getBackground();
				setBackground(bSelected ? list.getSelectionBackground() : background);
			}

			// respect setColumns() - note that this has no effect by default as getColumns() always returns null:
			final Integer iColumns = getColumns();
			if (iColumns != null) {
				final Dimension dimPreferredSize = getPreferredSize();
				final int iPreferredWidth = Math.min(dimPreferredSize.width, getFontMetrics(getFont()).charWidth('m') * iColumns);
				setPreferredSize(new Dimension(iPreferredWidth, dimPreferredSize.height));
			}
			return this;
		}

		@Override
		public boolean isOpaque() {
			return true;
		}

		private boolean isToolTipNecessary() {
			// this is not exactly right, but it's probably the best guess we can make here:
			return getPreferredSize().width > getJComboBox().getSize().width;
		}
	}
	
	private final Set<LabeledCollectableComponentWithVLP> vlrTargets = new HashSet<LabeledCollectableComponentWithVLP>();
	private final Set<LabeledCollectableComponentWithVLP> vlrSources = new HashSet<LabeledCollectableComponentWithVLP>();

	Set<LabeledCollectableComponentWithVLP> getVlrTargets() {
		return vlrTargets;
	}

	Set<LabeledCollectableComponentWithVLP> getVlrSources() {
		return vlrSources;
	}

	protected abstract void refreshValueListFromRestrictingSources(Set<LabeledCollectableComponentWithVLP> restricting);
	
	protected void refreshValueListsOfAllTargets() {
		for (LabeledCollectableComponentWithVLP p : vlrTargets) {
			p.refreshValueListFromRestrictingSources(p.vlrSources);
		}
	}
	
	@Override
	public void introduceValueListRefreshingTarget(Parameterisable parameterisable) {
		if (parameterisable instanceof LabeledCollectableComponentWithVLP) {
			vlrTargets.add((LabeledCollectableComponentWithVLP)parameterisable);			
		}
	}
	
	@Override
	public void introduceValueListRefreshingSource(Parameterisable parameterisable) {
		if (parameterisable instanceof LabeledCollectableComponentWithVLP) {
			vlrSources.add((LabeledCollectableComponentWithVLP)parameterisable);			
		}
	}
	
}
