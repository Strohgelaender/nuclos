//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.LayoutNavigationProcessor;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledPasswordField;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;

/**
 * NUCLOS-6388
 * @author oliver.brausch
 * @version 01.00.00
 */
public class CollectableHiddenField extends LabeledCollectableComponent {

	/**
	 *
	 */
	public CollectableHiddenField(CollectableEntityField clctef) {
		super(clctef, new LabeledPasswordField(new LabeledComponentSupport(),
				clctef.isNullable(), clctef.getJavaClass(), clctef.getFormatInput(), false), false);
	}

	@Override
	protected void updateView(CollectableField clctfValue) {
	}

	@Override
	public CollectableField getFieldFromView() throws CollectableFieldFormatException {
		return null;
	}

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		return new HiddenCellRenderer();
	}
	
	/**
	 * 
	 * 
	 *
	 */
	protected class HiddenCellRenderer extends DefaultTableCellRenderer {
		
		public HiddenCellRenderer() {
			setVerticalAlignment(SwingConstants.TOP);
		}
		
		@Override
		public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {

			setText(HIDDENSTRING);
			setBackgroundColor(this, tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
			
			return this;
		}
	}

}	// class CollectableHiddenField
