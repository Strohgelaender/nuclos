package org.nuclos.client.ui.labeled;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.text.JTextComponent;

import org.nuclos.client.command.OvOpAdapter;
import org.nuclos.client.ui.ColorProvider;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.LayoutNavigationCollectable;
import org.nuclos.client.ui.LayoutNavigationProcessor;
import org.nuclos.client.ui.OverlayOptionPane;
import org.nuclos.client.ui.TextFieldWithButton;
import org.nuclos.client.ui.ToolTipTextProvider;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.component.CollectableLocalizedComponent;
import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;

public class LabeledLocalizedTextField extends LabeledTextComponent {

	private InnerJTextFieldWithButton tf;
	private CollectableEntity entity;
	private CollectableEntityField clctef;
	private CollectableLocalizedComponent<Long> listener;
	private ImageIcon dataLanguageDirtyIcon = null;
	private ImageIcon dataLanguageIcon = null; 
	public JTextField getTextField() {
		return this.tf;
	}
	
	public CollectableEntity getCollectableEntity() {
		return this.entity;
	}
	
	public LabeledLocalizedTextField(ILabeledComponentSupport support, CollectableEntity entity){
		this(support,entity, true, String.class, null, false);
	}
	
	public void setLocalizedComponent (CollectableLocalizedComponent<Long> listener) {
		this.listener = listener;
	}
	

	public LabeledLocalizedTextField(ILabeledComponentSupport support, CollectableEntity entity, boolean isNullable, 
			Class<?> javaClass, String inputFormat, boolean bSearchable) {		
		
		super(support, isNullable, javaClass, inputFormat, bSearchable);
	
		initValidation(isNullable, javaClass, inputFormat);
		
		this.entity = entity;

		dataLanguageDirtyIcon = Icons.getInstance().getDataLanguageDirtyIcon();
		dataLanguageIcon = Icons.getInstance().getDataLanguageIcon();
		
		
		tf = new InnerJTextFieldWithButton(
			Icons.getInstance().getInstance().getDataLanguageIcon(), getLabeledComponentSupport());			
		
		if(this.validationLayer != null){
			this.addControl(this.validationLayer);
		} else {
			this.addControl(this.tf);				
		}
		this.getJLabel().setLabelFor(this.tf);
	}
	
	public LabeledLocalizedTextField(
			LabeledComponentSupport labeledComponentSupport,
			CollectableEntityField clctef, boolean bSearchable) {
		
		this(labeledComponentSupport, clctef.getCollectableEntity(), clctef.isNullable(), clctef.getJavaClass(), clctef.getFormatInput(), bSearchable);
		this.clctef = clctef;
	}

	public void setButtonsDisabled() {
		tf.setButtonsDisabled();
	}
	
	public void setButtonVisibility(boolean value) {
		tf.setButtonVisibility(value);
	}
	
	public boolean isButtonsDisabled() {
		return tf.isButtonsDisabled();
	}
	
	public CollectableLocalizedComponent<Long> getLocalizedComponent() {
		return this.listener;
	}
	
	@Override
	public JTextComponent getJTextComponent() {
		return getTextField();
	}

	@Override
	protected JComponent getLayeredComponent() {
		return getTextField();
	}

	@Override
	protected JTextComponent getLayeredTextComponent() {
		return getTextField();
	}

	public CollectableEntityField getCollectableEntityField() {
		return this.clctef;
	}
	
	public void setDataLanguageMapAsDirty(boolean isDirty) {
		this.tf.setDataLanguageMapAsDirty(isDirty);
	}
	
	public class InnerJTextFieldWithButton extends TextFieldWithButton implements LayoutNavigationProcessor {
		
		boolean buttonIsVisible = false;
		
		public InnerJTextFieldWithButton(Icon iconButton, ILabeledComponentSupport support) {
			super(iconButton, support);
		}

		public void setButtonVisibility(boolean value) {
			buttonIsVisible = value;
		}

		protected LayoutNavigationSupport lns;
		private LayoutNavigationCollectable lnc;
		
		@Override
		public String getToolTipText(MouseEvent ev) {
			final ILabeledComponentSupport support = getLabeledComponentSupport();
			final ToolTipTextProvider provider = support.getToolTipTextProvider();
			return StringUtils.concatHtml(false, provider != null ? provider.getDynamicToolTipText() : super.getToolTipText(ev),
					support.getValidationToolTip());
		}
		
		@Override
		public Color getBackground() {
			final ColorProvider colorproviderBackground = getLabeledComponentSupport().getColorProvider();
			final Color colorDefault = super.getBackground();
			return (colorproviderBackground != null) ? colorproviderBackground.getColor(colorDefault) : colorDefault;
		}

		@Override
		public void setLayoutNavigationCollectable(
				LayoutNavigationCollectable lnc) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isButtonEnabled(int index) {
			return true;
		}
		
		public boolean isButtonVisible() {
			return this.buttonIsVisible;
		}
		
		public void setButtonsDisabled() {
			setEnabled(false);
		}

		public boolean isButtonsDisabled() {
			return !isEnabled();
		}
		
		public void setDataLanguageMapAsDirty(boolean isDirty) {
			getButtonIcons().clear();
			if (isDirty) {
				getButtonIcons().add(dataLanguageDirtyIcon);
			} else {
				getButtonIcons().add(dataLanguageIcon);
			}
		}
		
		@Override
		public void buttonClicked(MouseEvent me, int index) {
			if (index == 0 && me.getComponent() instanceof InnerJTextFieldWithButton) {
				final InnerJTextFieldWithButton txtButtonTextField = (InnerJTextFieldWithButton) me.getComponent();
				
				CollectableLocalizedComponentModel<Long> compModel = 
						getLocalizedComponent().getDetailsComponentModel();
				
				// Editing dialog works with a copy that will be reset into the model if the user presses 'OK'
				IDataLanguageMap dataLanguageMap = 
						compModel.getDataLanguageMap().clone();
				
				List<FieldMeta> lockedFields = new ArrayList<FieldMeta>();			
				if (getLocalizedComponent().isButtonDisabled()) {
					lockedFields.add(compModel.getMetaProvider().getEntityField(getCollectableEntityField().getUID()));
				}
				Pair<FieldMeta, Integer> p = new Pair<FieldMeta, Integer> (
						compModel.getMetaProvider().getEntityField(getCollectableEntityField().getUID()),
						CollectableComponentTypes.TYPE_TEXTFIELD);
				
						
				final LocalizedEntityFieldFillInPanel editingPanel = new LocalizedEntityFieldFillInPanel(
						new EntityMetaVO(compModel.getMetaProvider().getEntity(getCollectableEntity().getUID()), true),
						RigidUtils.newOneElementArrayList(p),
						compModel.getPrimaryKey(),
               		 	dataLanguageMap,
               		 lockedFields);
				
				String tabCaption = SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.78", "Mehrsprachigkeit") + ": " + 
						getCollectableEntity().getLabel() + " -> " + getCollectableEntityField().getLabel();
				
				 OverlayOptionPane.showConfirmDialog(
					 compModel.getTab(),
					 editingPanel,
					 tabCaption ,
                     OverlayOptionPane.OK_CANCEL_OPTION,
                     true,
                     new OvOpAdapter() {
                         @Override
                         public void done(int result) {
                        	 
                        	 CollectableLocalizedComponentModel<Long> compModel = 
                        			 getLocalizedComponent().getDetailsComponentModel();
                        	 
                             if (OverlayOptionPane.OK_OPTION == result) {
                            	 
                            	 
                            	 UID userDL = compModel.getUserPrimaryDataLanguage();
                            	 UID systemDL = compModel.getSystemPrimaryDataLanguage();
                            	 UID field = DataLanguageUtils.extractFieldUID(getCollectableEntityField().getUID());
                            	 UID fieldFlagged = DataLanguageUtils.extractFieldUID(getCollectableEntityField().getUID(), true);
                            	 
                            	 IDataLanguageMap fieldLanguageMap = 
                            			 editingPanel.getFieldLanguageMap();
                            	 
                            	 boolean isDirty = false;
                            	 boolean structureChanged = false;
                            	 
                            	 // reset new localized data into DataLanguageMap model
                            	 Map<UID, DataLanguageLocalizedEntityEntry> languageMap = compModel.getDataLanguageMap().getLanguageMap();
                            	 for (UID lang : languageMap.keySet()) {
                            		 if (languageMap.get(lang) == null) {
                            			 if (fieldLanguageMap.getDataLanguage(lang) != null) {
                            				 languageMap.put(lang, fieldLanguageMap.getDataLanguage(lang));
                            			 }
                            		 } else {
                            			 if (fieldLanguageMap.getDataLanguage(lang) != null) {
                            				 DataLanguageLocalizedEntityEntry dataLanguage = 
                            						 fieldLanguageMap.getDataLanguage(lang);
                            				 if (!dataLanguage.isFlagUnchanged() &&
                            					 fieldLanguageMap.getDataLanguage(lang).getFieldValue(field) != null) {
													 DataLanguageLocalizedEntityEntry existBlock = languageMap.get(lang);
													 existBlock.setFieldValue(field,fieldLanguageMap.getDataLanguage(lang).getFieldValue(field));
													 existBlock.setFieldValue(fieldFlagged, fieldLanguageMap.getDataLanguage(lang).getFieldValue(fieldFlagged));
													 structureChanged = true; 
													 if (existBlock.getPrimaryKey() == null) {
														 existBlock.flagNew(); 
													 } else {
														 existBlock.flagUpdate();
													 }
                            				 	}
                            			 }
                            		 }
                            		 
                            		 if (!Boolean.FALSE.equals(languageMap.get(lang).getFieldValue(fieldFlagged))) {
                            			 isDirty = true;
                            		 }
                            	 }
                            		 
                            	 setDataLanguageMapAsDirty(isDirty);
                            	 
                            	 // use existing model values to reset CollectableTextField instance
                            	 if (userDL == null || userDL.equals(systemDL)) {                                		 
                            		 DataLanguageLocalizedEntityEntry dataLanguage = 
                            				 fieldLanguageMap.getDataLanguage(systemDL);
                            		 
                            		 if (structureChanged || dataLanguage.isFlagNew() || dataLanguage.isFlagUpdated()) {
                            			 compModel.setField(new CollectableValueField(
                            					 dataLanguage.getFieldValue(field)), true);                                			 
                            		 }
                            	 } else {
                            		 DataLanguageLocalizedEntityEntry dataLanguage = 
                            				 fieldLanguageMap.getDataLanguage(userDL);
                            		
                            		 if (dataLanguage != null) {
                            			 if (structureChanged ||dataLanguage.isFlagNew() || dataLanguage.isFlagUpdated()) {
                            				 compModel.setField(new CollectableValueField(
                            						 fieldLanguageMap.getDataLanguage(userDL).getFieldValue(field)), false);                    			                                 				 
                            			 }
                            		 } else {
                            			 compModel.setField(null, true);
                            		 }
                            	 }
                            	 
                            	 compModel.stopEditing();
                             } else {
                            	 compModel.cancelEditing();
                             }
                         }
                     });
			}
		}
	}
	
	
}

