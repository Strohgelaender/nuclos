//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.ComboBoxEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.autocomplete.AutoCompleteComboBoxEditor;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.masterdata.valuelistprovider.DefaultValueProvider;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.WideJComboBox;
import org.nuclos.client.ui.collect.CollectController.CollectableEventListener;
import org.nuclos.client.ui.collect.CollectController.MessageType;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.component.FieldsProviderSupport.FieldsProviderWorker;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.client.ui.labeled.LabeledComboBox;
import org.nuclos.client.ui.labeled.LabeledComboBox.MyComboBoxEditor;
import org.nuclos.client.ui.labeled.LabeledComboBox.MyComboBoxEditorTextField;
import org.nuclos.client.ui.popupmenu.JPopupMenuListener;
import org.nuclos.client.valuelistprovider.VLPClientUtils;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache.CachingCollectableFieldsProvider;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldComparatorFactory;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.LocalizedCollectableValueField;
import org.nuclos.common.collect.collectable.LocalizedCollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.AbstractCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonDateValues;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithOtherField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithParameter;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIsNullCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.ToHumanReadablePresentationVisitor;
import org.nuclos.common.collect.collectable.searchcondition.visit.AtomicVisitor;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * A <code>CollectableComponent</code> that presents a value in a <code>JComboBox</code>.
 * The value is always displayed, even if it is not contained in the dropdown list. To make this possible,
 * the dropdown list is dynamically extended by the selected entry, iff the selected entry is not in the list.
 * The additional entry is removed when it is deselected.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableComboBox extends LabeledCollectableComponentWithVLP implements CollectableEventListener, PopupMenuListener {

	private static final Logger LOG = Logger.getLogger(CollectableComboBox.class);

	/**
	 * an additional entry that is dynamically added when a value is to be set that is not contained
	 * in the dropdown list.
	 */
	private CollectableField clctfExtra;

	private boolean insertable;

	private boolean blnIsLookupEntity = false;
	
	/**
	 * <code>ActionListener</code> that gets notified whenever the selection changes (even on deselection).
	 * The ActionListener even gets notified when the new selected item is the same as the previously selected item.
	 */
	private final ActionListener alCollectableComponentChanged = new ActionListener() {
		@Override
        public void actionPerformed(ActionEvent ev) {
			try {
				if (isSearchComponent()) {
					adjustComparisonOperator();
				}
				viewToModel();
			}
			catch (CollectableFieldFormatException ex) {
				// do nothing. The model can't be updated.
				assert !isConsistent();
			}
		}

		/**
		 * adjusts the comparison operator for searchable (and insertable) comboboxes, after user changes
		 * 
		 * §precondition CollectableComboBox.isSearchComponent()
		 */
		private void adjustComparisonOperator() throws CollectableFieldFormatException {
			if (!isSearchComponent()) {
				throw new IllegalStateException("isSearchComponent");
			}

			if (isInsertable()) {
				// For insertable comboboxes, the document listener sets the comparison operator,
				// so we needn't do anything here.
			}
			else {
				// For non-insertable comboboxes, we must set the comparison operator here:
				if (getFieldFromView().isNull()) {
					setComparisonOperator(ComparisonOperator.NONE);
				}
				else {
					if (getComparisonOperator().getOperandCount() < 2) {
						setComparisonOperator(ComparisonOperator.EQUAL);
					}
				}
			}
		}
	};

	private DocumentListener doclistener;

	/**
	 * §postcondition isDetailsComponent()
	 */
	public CollectableComboBox(CollectableEntityField clctef) {
		this(clctef, false);

		assert isDetailsComponent();
	}

	/**
	 * @param clctef
	 */
	public CollectableComboBox(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, new LabeledComboBox(), bSearchable);

		assert !isInsertable();
		/** @todo setInsertable should not be used for this - it means something different! */
		setInsertable(isSearchComponent());

		setRenderer();
		getJComboBox().addActionListener(alCollectableComponentChanged);

		setComboBoxModel(Collections.<CollectableField>emptyList());
		this.getJComboBox().addPopupMenuListener(this);

		try {
			FieldMeta<?> efMeta = MetaProvider.getInstance().getEntityField(clctef.getUID());

			blnIsLookupEntity = efMeta.getLookupEntity() != null || !clctef.isIdField();
		} catch (Exception e) {
			blnIsLookupEntity = !clctef.isIdField();
		}
		assert isInsertable() == isSearchComponent();
	}

	@Override
	protected void setupJPopupMenuListener(JPopupMenuListener popupmenulistener) {
		getLabeledComboBox().setupJPopupMenuListener(popupmenulistener);
	}

	private void removeJPopupMenuListeners() {
		removeAllJPopupMenuListeners(getJComboBox().getEditor().getEditorComponent());
		removeAllJPopupMenuListeners(getControlComponent());
	}

	private static void removeAllJPopupMenuListeners(Component comp) {
		for (MouseListener ml : comp.getMouseListeners()) {
			if (ml instanceof JPopupMenuListener) {
				comp.removeMouseListener(ml);
			}
		}
	}

	/**
	 * §todo clarify semantics (insertable vs. searchable?)
	 */
	public boolean isInsertable() {
		return insertable;
	}

	@Override
	public void setInsertable(boolean bInsertable) {
		if (bInsertable != isInsertable()) {
			insertable = bInsertable;

			removeDocumentListenerForEditor();
			removeJPopupMenuListeners();

			getJComboBox().setEditable(bInsertable);

			if (bInsertable) {
				addDocumentListenerForEditor();
			}
			setupJPopupMenuListener(newJPopupMenuListener());
		}
	}
	
	@Override
	public void setMultiSelect(boolean bMultiSelect) {
		getLabeledComboBox().setMultiSelect(bMultiSelect);
	}

	private DocumentListener getDocumentListener() {
		if (doclistener == null) {
			doclistener = newDocumentListenerForTextComponentWithComparisonOperator();
		}
		return doclistener;
	}
	
	public void setBackgroundColor(final Color color) {
		getLabeledComboBox().getLabeledComponentSupport().setColorProvider(null);
		super.setBackgroundColor(color);
	}

	/**
	 * @return the text component of the editor, if any.
	 */
	private Document getEditorDocument() {
		Document result = null;
		final ComboBoxEditor editor = getJComboBox().getEditor();
		if (editor != null) {
			final Component comp = editor.getEditorComponent();
			if (comp instanceof JTextComponent) {
				result = ((JTextComponent) comp).getDocument();
			}
		}
		return result;
	}

	public void addDocumentListenerForEditor() {
		final Document document = getEditorDocument();
		if (document != null) {
			document.addDocumentListener(getDocumentListener());
		}
	}

	public void removeDocumentListenerForEditor() {
		if (doclistener != null) {
			final Document document = getEditorDocument();
			if (document != null) {
				document.removeDocumentListener(getDocumentListener());
			}
		}
	}
	
	public boolean hasDocumentListenerForEditor() {
		return doclistener != null;
	}

	@Override
	public void setMnemonic(char cMnemonic) {
		super.setMnemonic(cMnemonic);
		final ComboBoxEditor editor = getJComboBox().getEditor();
		if (editor != null) {
			final Component comp = editor.getEditorComponent();
			if (comp instanceof JTextComponent) {
				((JTextComponent) comp).setFocusAccelerator(cMnemonic);
			}
		}
	}
	
	private void popupClosing(boolean cancel) {
		if (!cancel) {
			refreshValueListsOfAllTargets();
		}
	}
	
	@Override
    public void applyParameters(boolean async, boolean isInitial) throws CommonBusinessException {
		if (!isMultiSelect() || async || isInitial) {
			super.applyParameters(async, isInitial);
			
		}	//else {
			//NUCLOS-4636 Do nothing, because in this case the closing of the Combo-box will trigger 
			//refreshValueListsOfAllTargets(), via popupClosing(boolean cancel)
		//}
	}
	
	//NUCLOS-4936 Lazy Loading for Combo-boxes. Saves a lot of SQLs and CPU for masks with lots of drop-downs.
	//TODO: if established, "releasePreviousRefreshs()" and "async" are obsolete and can be removed.
	private Queue<RefreshValueListWorker> queueWorkers = new LinkedBlockingQueue<>(256);
	
	@Override
	protected void requestValueList() {
		while (queueWorkers.peek() != null) {
			queueWorkers.poll().run();
		}
	}
		
	@Override
	protected void refreshValueListFromRestrictingSources(Set<LabeledCollectableComponentWithVLP> restricting) {
		refreshValueList(false, true, false, false, restricting);
	}
		
	private void refreshValueList(boolean async, boolean bSetDefault, boolean isInitial, boolean bFromApplyParameter, Set<LabeledCollectableComponentWithVLP> restricting) {

		if (!isSearchComponent() && VLPClientUtils.isVLPBaseRestrictionMissingValues(getFieldUID(), getValueListProvider())) {
			// Fehlen noch die wichtigsten Parameter Werte darf nicht refreshed werden! (unsafeFillDetails setzt diese)
			// Die ersten Ergebnisse wären falsch, gerade beim Mandantenwesen, und da die tollen Caches (FieldsProviderSupport) 
			// ein Refresh verhindern, scheint dies die beste Methode.
			return;
		}

		final boolean bDefaultVLP = getValueListProvider() instanceof DefaultValueProvider;
		CollectableField cfDefaultValue = null;
		// NUCLOS-7154 Get the defaultvalue if needed and decide if there is reason to pre-load the combobox
		if (bSetDefault && bDefaultVLP && !isSearchComponent()) {
			cfDefaultValue = ((DefaultValueProvider) getValueListProvider()).getDefaultValue();
		}

		// Asynchronous loading of the comboboxes is only possible, when it's not setting the default-value of a vlp.

		final boolean setDefaultAndFieldIsNull = cfDefaultValue != null && !cfDefaultValue.isNull() && getModel().getField().isNull();
		final boolean doLazyFilling = !setDefaultAndFieldIsNull && bDefaultVLP && getVlrSources().isEmpty();

		LOG.debug("CollectableComboBox.refreshValueList called for component " + this + " doLazyFilling:" + doLazyFilling);

		RefreshValueListWorker rvlr = new RefreshValueListWorker(cfDefaultValue, isInitial, bFromApplyParameter, restricting);
		
		if (doLazyFilling) {
			queueWorkers.offer(rvlr);
			return;
		}

		UIUtils.setWaitCursor(getJComboBox());

		releasePreviousRefreshs();
		runningRefreshs.add(rvlr);

		if (async) {
			rvlr.execute();
		} else {
			rvlr.run();
		}
	}

	/**
	 * refreshes the list of values by asking the value list provider.
	 * If no value list provider was set, the model will be empty.
	 */
	@Override
    public void refreshValueList(boolean async, boolean bSetDefault, boolean isInitial) {
		refreshValueList(async, bSetDefault, isInitial, false, null);
	}
	
	@Override
    protected void refreshValueListFromApplyParameters(boolean async, boolean isInitial) {
		refreshValueList(async, true, isInitial, true, null);
	}

	private Set<RefreshValueListWorker> runningRefreshs = Collections.synchronizedSet(new HashSet<RefreshValueListWorker>());

	private void releasePreviousRefreshs() {
		synchronized(runningRefreshs) {
			for (RefreshValueListWorker refresh : new HashSet<RefreshValueListWorker>(runningRefreshs)) {
				refresh.ignoreResult();
				//No need to actually cancel the refresh anymore
			}
		}
	}

	private class RefreshValueListWorker extends SwingWorker<List<CollectableField>, Object> {

		private boolean ignoreResult = false;
		private final CollectableField cfDefaultValue;
		private final boolean bSetDefaultInitial;
		private final boolean bFromApplyParameters;
		private final Set<LabeledCollectableComponentWithVLP> restricting;

		private RefreshValueListWorker(CollectableField cfDefaultValue, boolean bSetDefaultInitial, boolean bFromApplyParameters, Set<LabeledCollectableComponentWithVLP> restricting) {
			this.cfDefaultValue = cfDefaultValue;
			this.bSetDefaultInitial = bSetDefaultInitial;
			this.bFromApplyParameters = bFromApplyParameters;
			this.restricting = restricting;
		}

		public void ignoreResult() {
			this.ignoreResult = true;
		}

		@Override
		protected List<CollectableField> doInBackground() throws Exception {
			try {
				CollectableFieldsProvider provider = getValueListProvider();
				if (provider == null) {
					return Collections.<CollectableField>emptyList();
				}
				if (restricting != null) {
					for (LabeledCollectableComponentWithVLP ccc : restricting) {
						if (!ccc.isSearchComponent()) {
							continue;
						}
						
						Map<UID, String> mapTags = ccc.getSearchModel().getMappingOfRefreshValueListParamaters();
						CollectableSearchCondition cond = ccc.getSearchConditionFromView();
						if (cond instanceof CollectableInCondition) {
							CollectableInCondition<?> cic = (CollectableInCondition<?>) cond;
							Set<Object> ids = cic.getKeyMap() != null ? cic.getKeyMap().keySet() : null;
							if (ids != null) {
								String tag = mapTags.get(ccc.getFieldUID());
								if (tag != null) {
									provider.setParameter(tag, ids);
								}
							}
						}						
					}
				}
				
				FieldsProviderSupport support = new FieldsProviderSupport(provider);
				LOG.debug("Provider:" + support);
				
				FieldsProviderWorker worker = support.getFieldsProviderWorkerAndStartIfNew();
				while (!worker.hasFinished()) {
					Thread.sleep(200L);
				}
				return worker.getCollectableFields();					
			}
			catch (Exception ex) {
				LOG.error("RefreshValueListWorker failed: " + ex, ex);
			}
			return null;
		}

		@Override
		protected void done() {
			try {
				if (!isCancelled() && !ignoreResult) {
					if (getValueListProvider() != null) {
						LOG.debug("Refresh valuelist: " + getEntityField().getEntityUID() + "." + getEntityField() +
								" " + getValueListProvider().toString());
						
						final boolean bMultiEditable = getModel().isMultiEditable();
						boolean bSubformMultiEditable = bMultiEditInSubform && bSubformComponent;						
	
						setComboBoxModel(get(), false);
						getJComboBox().setCursor(null);

						// use getModel().getField() here. getField() trys to makeConsistant.
						if (cfDefaultValue != null && !isSearchComponent()) {

							if (getModel().getField() != null && !(bMultiEditable || bSubformMultiEditable)) {
																
								if (!bSetDefaultInitial) {
									if (!cfDefaultValue.isNull()) {
										getModel().setField(cfDefaultValue);										
									}
								} else if (getModel().getField().isNull()) {
									//NUCLOS-4322. There are two cases where the Default Value of a VLP is set.
									//1) From a LayoutML Rule "refresh valuelist" this happens after the initializing of the model
									//   and this is the second case here
									//2) If the field is not a target of such LayoutML rule, the value is set when a new objeczt
									//	 is created, means while the model is initializing (first case here)
									if (bFromApplyParameters) {
										if (getModel().isInitializing()) {
											getModel().setFieldInitial(cfDefaultValue);
										}
										
									} else if (!getModel().isInitializing()) {
										getModel().setFieldInitial(cfDefaultValue);												
									}
								}
										
							}
						}
						((WideJComboBox)getJComboBox()).setWide(true);
						
					}
					else {
						LOG.debug("Ignoring refresh for " + getEntityField().getEntityUID() + "." + getEntityField());
					}
				}
			} catch(Exception e) {
				Errors.getInstance().showExceptionDialog(CollectableComboBox.this.getJComponent(), e);
			} finally {
				runningRefreshs.remove(this);
			}
		}
		
		@Override
		public String toString() {
			return "Combobox-VL-Worker:" + getEntityField();
		}
	}

	/**
	 * uses a copy of the given Collection as model for this combobox.
	 * 
	 * §todo make private
	 * 
	 * @param collEnumeratedFields Collection&lt;CollectableField&gt;
	 */
	public void setComboBoxModel(Collection<? extends CollectableField> collEnumeratedFields) {
		setComboBoxModel(collEnumeratedFields, true);
	}

	/**
	 * uses a copy of the given Collection as model for this combobox. The selected value is not changed,
	 * thus no CollectableFieldEvents are fired.
	 * 
	 * §todo make private
	 * 
	 * @param collEnumeratedFields Collection&lt;CollectableField&gt;
	 * @param bSort Sort the fields before adding them to the model?
	 */
	public synchronized void setComboBoxModel(Collection<? extends CollectableField> collEnumeratedFields, boolean bSort) {
		final List<CollectableField> lst = new ArrayList<CollectableField>();
		if (collEnumeratedFields != null) {
			lst.addAll(collEnumeratedFields);
		}

		if (bSort) {
			Collections.sort(lst, CollectableFieldComparatorFactory.getInstance().newCollectableFieldComparator(getEntityField()));
		}

		runLocked(new Runnable() {
			@Override
            public void run() {
				// re-fill the model:
				final DefaultComboBoxModel model = getDefaultComboBoxModel();

				final Map<Object, Boolean> mapSelection = new HashMap<Object, Boolean>();
				getOrSetSelectionsOfMultiSelect(model, mapSelection, true);
				model.removeAllElements();

				// forget the previous additional entry, if any. If an additional entry is needed after setting the model,
				// it will be set later:
				if (hasAdditionalEntry()) {
					removeAdditionalEntry();
				}

				// always add a null value as the first entry:
				model.addElement(getEntityField().getNullField());

				for (CollectableField clctf : lst) {
					Object value = clctf.getValue();
					if (value instanceof String) {
						value = value.toString().replaceAll("\\p{Cntrl}", "");
						if (clctf instanceof LocalizedCollectableValueField)
							clctf = new LocalizedCollectableValueField(value, ((LocalizedCollectableValueField)clctf).toLocalizedString());
						else if (clctf instanceof CollectableValueField)
							clctf = new CollectableValueField(value);
						if (clctf instanceof LocalizedCollectableValueIdField)
							clctf = new LocalizedCollectableValueIdField(((CollectableValueIdField)clctf).getValueId(), value, ((LocalizedCollectableValueIdField)clctf).toLocalizedString());
						else if (clctf instanceof CollectableValueIdField) {
							clctf = new CollectableValueIdField(((CollectableValueIdField)clctf).getValueId(), value);
						}
					}
					model.addElement(clctf);
				}

				modelToView();
				Runnable r = new Runnable() {
					@Override
					public void run() {
						getOrSetSelectionsOfMultiSelect(model, mapSelection, false);
						getJComboBox().getEditor().setItem("");
						getOrSetSelectionsOfMultiSelect(model, mapSelection, false);
					}
				};
				if (isMultiSelect()) {
					getOrSetSelectionsOfMultiSelect(model, mapSelection, false);
					SwingUtilities.invokeLater(r);
				}
			}
		});

	}
	
	private static void getOrSetSelectionsOfMultiSelect(DefaultComboBoxModel model, Map<Object, Boolean> mapSelection, boolean get) {
		for (int i = 0; i < model.getSize(); i++) {
			Object o = model.getElementAt(i);
			if (o instanceof CollectableValueIdField) {
				CollectableValueIdField cvif = (CollectableValueIdField) o;
				Object id = cvif.getValueId();
				if (get) {
					mapSelection.put(id, cvif.isSelected());
				} else {
					if (mapSelection.containsKey(id)) {
						cvif.setSelected(mapSelection.get(id));									
					}					
				}
			}
		}
	}

	@Override
	public JComboBox getJComboBox() {
		return getLabeledComboBox().getJComboBox();
	}

	private LabeledComboBox getLabeledComboBox() {
		return ((LabeledComboBox) getJComponent());
	}
	
	@Override
	public boolean isMultiSelect() {
		return getLabeledComboBox().isMultiSelect();
	}

	@Override
	public CollectableField getFieldFromView() throws CollectableFieldFormatException {
		final Object oCurrentItem = getCurrentItems().get(0);

		final CollectableField result;
		if (isInsertable() && (oCurrentItem instanceof String)) {
			final String sText = ((String) oCurrentItem).replaceAll("\\p{Cntrl}", "");
			if (!blnIsLookupEntity)
				result = CollectableTextComponentHelper.write(sText, getEntityField());
			else {
				if (!getEntityField().isIdField())
					result = new CollectableValueField(CollectableTextComponentHelper.write(sText, getEntityField()).getValue());
				else
					result = new CollectableValueIdField(null, CollectableTextComponentHelper.write(sText, getEntityField()).getValue());
			}
		} else {
			if (!blnIsLookupEntity)
				result = (oCurrentItem == null) ? getEntityField().getNullField() : (CollectableField) oCurrentItem;
			else {
				if (!getEntityField().isIdField())
					result = (oCurrentItem == null) ? getEntityField().getNullField() : new CollectableValueField(((CollectableField) oCurrentItem).getValue());
				else
					result = (oCurrentItem == null) ? getEntityField().getNullField() : new CollectableValueIdField(null, ((CollectableField) oCurrentItem).getValue());
			}
		}
		return result;
	}
	
	private List<Object> getMultiSelectItems() {
		ComboBoxEditor editor = getJComboBox().getEditor();
		if (editor instanceof AutoCompleteComboBoxEditor) try {
			Field f = AutoCompleteComboBoxEditor.class.getDeclaredField("wrapped");
			f.setAccessible(true);
			editor = (ComboBoxEditor) f.get(editor);
		} catch (Exception e) {
			//ignore
		}
		if (editor instanceof MyComboBoxEditor) {
			return new ArrayList<>(((MyComboBoxEditor) editor).getItems());
		}
		return new ArrayList<Object>();
	}
	
	@Override
	public void clear() {
		if (isMultiSelect()) {
			DefaultComboBoxModel model = getDefaultComboBoxModel();
			for (int i = 0; i < model.getSize(); i++) {
				Object o = model.getElementAt(i);
				if (o instanceof CollectableValueIdField) {
					CollectableValueIdField cvif = (CollectableValueIdField) o;
					cvif.setSelected(false);
				}
			}
			getJComboBox().getEditor().setItem("");
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					popupClosing(false);
				}
			});
		} else {
			super.clear();
		}
	}

	/**
	 * @return the current item. That is the edited text, if this is insertable, or the selected item otherwise.
	 */
	@Override
	public List<Object> getCurrentItems() {
		List<Object> result = new ArrayList<Object>();
		if (isMultiSelect()) {
			return getMultiSelectItems();
		} else if (isInsertable() && !getEntityField().isIdField()) {
			// Insertable: If the edited text differs from the selected item, the edited text is the master.
			Object item = getJComboBox().getEditor().getItem();
			if (item == null) {
				Component editor = getJComboBox().getEditor().getEditorComponent();
				if (editor instanceof JTextComponent)
					item = ((JTextComponent)editor).getText();
			}
			if (item != null) result.add(item);
		}
		else {
			// Default: use the selected item.
			result.add(getJComboBox().getSelectedItem());
		}
		return result;
	}

	@Override
	protected void viewToModel() throws CollectableFieldFormatException {
		runLocked(new ExceptionalRunnable() {
			@Override
            public void run() throws CollectableFieldFormatException {
				if (isSearchComponent()) {
					getSearchModel().setSearchCondition(getSearchConditionFromView());
					// no need to care about additional entries for searchable combobox
				}
				else {
					try {
						final CollectableField clctf = getFieldFromView();
						viewToModel(clctf);

						/** @todo Does this belong here? This has nothing to do with updating the model. */
						final boolean bAdditionalEntrySelected = clctf.equals(clctfExtra);
// NUCLOS-82			getJComboBox().setForeground(bAdditionalEntrySelected ? Color.RED : null);
						if (!bAdditionalEntrySelected && hasAdditionalEntry()) {
							removeAdditionalEntry();
						}
					}
					catch (CollectableFieldFormatException ex) {
						/** @todo Is it right to clear the model here? See also AbstractCollectableComponent.viewToModel. */
						// the value in the view is not valid. At least, the model is cleared:
						getModel().clear();
						if (hasAdditionalEntry()) {
							removeAdditionalEntry();
						}
						throw ex;
					}
				}
			}
		});
	}

	@Override
	protected void updateView(CollectableField clctfValue) {
		if (hasAdditionalEntry()) {
			removeAdditionalEntry();
		}

		final JComboBox cb = getJComboBox();
		final DefaultComboBoxModel cbm = getDefaultComboBoxModel();

		int iIndex = cbm.getIndexOf(clctfValue);
		// If the value could not be found, it might be the mnemonic, so look for the value id
		if(iIndex < 0 && clctfValue.isIdField() && clctfValue.getValueId() != null) {
			for(int i=0; i < cbm.getSize(); i++) {
				final CollectableField cf = (CollectableField) cbm.getElementAt(i);
				if (!cf.isIdField())
					continue;
				CollectableValueIdField cvif = (CollectableValueIdField) cf;
				Object valueId = cvif.getValueId();
				Object vid = clctfValue.getValueId();
				if (vid instanceof Number) {
					vid = IdUtils.toLongId(vid);
				} else if (vid instanceof Collection) {
					Collection coll = (Collection) vid;
					if (coll.contains(valueId)) cvif.setSelected(true);
				}
				if(LangUtils.equal(vid, valueId)) {
					cvif.setSelected(true);
					iIndex = i;
					break;
				}
			}
		}
		if (isMultiSelect()) return;
		
		if (iIndex >= 0) {
			cb.setSelectedIndex(iIndex);
			// Note that setSelectedItem doesn't work here, as clctf might have no label.
		}
		else {
			assert iIndex == -1;

			final Object vid = !clctfValue.isIdField() ? null : clctfValue.getValueId();
			Object oValueId = null;
			if (clctfValue.isIdField()) {
				oValueId = vid;
				// sbWarning.append(" (Id: ").append(oValueId).append(")");
			}

			if (isInsertable() && (oValueId == null) && (vid != null)) {
				final String sText = clctfValue.toString();
				cb.setSelectedItem(sText);
			}
			else {
				addAdditionalEntry(clctfValue);
				cb.setSelectedIndex(cbm.getIndexOf(clctfValue));
			}
		}
	}

	private boolean hasAdditionalEntry() {
		return clctfExtra != null;
	}

	/**
	 * §precondition !hasAdditionalEntry()
	 * §postcondition hasAdditionalEntry()
	 */
	private void addAdditionalEntry(CollectableField clctf) {
		assert !hasAdditionalEntry();

		getDefaultComboBoxModel().addElement(clctf);
		clctfExtra = clctf;

		assert hasAdditionalEntry();
	}

	/**
	 * §precondition hasAdditionalEntry()
	 * §postcondition !hasAdditionalEntry()
	 */
	private void removeAdditionalEntry() {
		assert hasAdditionalEntry();

		//NUCLEUSINT-470
		try {
			getDefaultComboBoxModel().removeElement(clctfExtra);
		} catch (IllegalStateException e) {
			LOG.info(e);
		}

		clctfExtra = null;

		assert !hasAdditionalEntry();
	}

	@Override
	public boolean hasComparisonOperator() {
		return true;
	}

	@Override
	public void setComparisonOperator(ComparisonOperator compop) {
		super.setComparisonOperator(compop);

		if (compop.getOperandCount() < 2) {
			runLocked(new Runnable() {
				@Override
                public void run() {
					getJComboBox().setSelectedItem(null);
					getJComboBox().getEditor().setItem(null);
				}
			});
		}
	}

	@Override
	protected CollectableSearchCondition getSearchConditionFromView() throws CollectableFieldFormatException {
		return super.getSearchConditionFromComboItems();
	}
	
	/**
	 * Implementation of <code>CollectableComponentModelListener</code>.
	 * This event is (and must be) ignored for a searchable combobox.
	 * 
	 * §todo refactor!!! Maybe no event should be fired for searchable components in general!
	 * 
	 * @param ev
	 */
	@Override
	public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
		if (isSearchComponent()) {
			// simply ignore this event
		}
		else {
			super.collectableFieldChangedInModel(ev);
		}
	}

	/**
	 * Implementation of <code>CollectableComponentModelListener</code>.
	 * @param ev
	 */
	@Override
	public void searchConditionChangedInModel(final SearchComponentModelEvent ev) {
		// update the view:
		runLocked(new Runnable() {
			@Override
            public void run() {
				final CollectableSearchCondition cond = ev.getSearchComponentModel().getSearchCondition();
				if (cond == null) {
					resetWithComparison();
					updateView(getEntityField().getNullField());
					setComparisonOperator(ComparisonOperator.NONE);
				}
				else {
					final AtomicCollectableSearchCondition atomiccond = (AtomicCollectableSearchCondition) cond;

					updateView(atomiccond.accept(new AtomicVisitor<CollectableField, RuntimeException>() {
						@Override
						public CollectableField visitIsNullCondition(CollectableIsNullCondition isnullcond) {
							resetWithComparison();
							return getEntityField().getNullField();
						}

						@Override
						public CollectableField visitComparison(CollectableComparison comparison) {
							resetWithComparison();
							return comparison.getComparand();
						}

						@Override
						public CollectableField visitComparisonWithParameter(CollectableComparisonWithParameter comparisonwp) {
							setWithComparison(comparisonwp.getParameter());
							return getEntityField().getNullField();
						}

						@Override
						public CollectableField visitComparisonWithOtherField(CollectableComparisonWithOtherField comparisonwf) {
							setWithComparison(comparisonwf.getOtherField());
							return getEntityField().getNullField();
						}

						@Override
						public CollectableField visitComparisonDateValues(CollectableComparisonDateValues comparisondv) throws RuntimeException {
							setWithComparison(comparisondv.getDateValues());
							return getEntityField().getNullField();
						}

						@Override
						public CollectableField visitLikeCondition(CollectableLikeCondition likecond) {
							resetWithComparison();
							return CollectableUtils.newCollectableFieldForValue(getEntityField(), likecond.getLikeComparand());
						}
						
						@Override
						public <T> CollectableField visitInCondition(CollectableInCondition<T> incond) {
							resetWithComparison();
							return CollectableUtils.newCollectableFieldForValue(getEntityField(), incond.getInComparands());
						}
					}));
					setComparisonOperator(atomiccond.getComparisonOperator());
				}
			}
		});
	}

	/**
	 * @return the number of columns for this combobox. This default implementation returns always null.
	 * For compatibility reasons, setColumns() has no effect on a <code>CollectableComboBox</code>.
	 */
	protected Integer getColumns() {
		return null;
	}

	private class My2CollectableComponentDetailTableCellRenderer extends CollectableComponentDetailTableCellRenderer {

		private My2CollectableComponentDetailTableCellRenderer() {
		}
		
		@Override
        public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {
			
			final Component result = super.getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
			
			if (result instanceof JLabel) {
				((JLabel) result).setBorder(new JTextField().getBorder());
				((JLabel) result).setVerticalAlignment(SwingConstants.CENTER);
			}

			return result;
		}

		@Override
		protected void setValue(Object value) {
			if (value instanceof AbstractCollectableSearchCondition)
				setToolTipText(((CollectableSearchCondition)value).accept(new ToHumanReadablePresentationVisitor()));
			if (value instanceof CollectableComparison)
				value = ((CollectableComparison) value).getComparand();
			if (value instanceof AtomicCollectableSearchCondition)
				value = ((AtomicCollectableSearchCondition)value).getComparandAsString();

			super.setValue(value);
		}
	}

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		if (!isSearchComponent() && getValueListProvider() != null) {
			return new CollectableComponentDetailTableCellRenderer() {

				@Override
				public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {
					CollectableField cf = (CollectableField) oValue;
					boolean valid = true;
					// NUCLEUSINT-885
					Object valueId = (cf != null && cf.isIdField()) ? cf.getValueId() : null;
					if (valueId != null) {
						valid = false;
						DefaultComboBoxModel cbModel = getDefaultComboBoxModel();
						for (int i = 0, n = cbModel.getSize(); i < n; i++) {
							CollectableField cf2 = (CollectableField) cbModel.getElementAt(i);
							if (cf2 != null && cf2.isIdField() && IdUtils.areEqual(valueId, cf2.getValueId())) {
								cf = cf2;
								valid = true;
								break;
							}
						}
					} else if (!cf.isIdField()) {
						valid = insertable;
						Object value = cf.getValue();
						DefaultComboBoxModel cbModel = getDefaultComboBoxModel();
						for (int i = 0, n = cbModel.getSize(); i < n; i++) {
							CollectableField cf2 = (CollectableField) cbModel.getElementAt(i);
							if (cf2 != null && ObjectUtils.equals(value, cf2.getValue())) {
								cf = cf2;
								valid = true;
								break;
							}
						}
					}
					
					if(tbl instanceof SubFormTable) {
						SubFormTable table = (SubFormTable)tbl;
						bMultiEditInSubform = table.getSubForm().isMultiEdit();
					}
					
					Component renderer = super.getTableCellRendererComponent(tbl, cf, bSelected, bHasFocus, iRow, iColumn);
					// NUCLEUSINT-525
					if (!valid) {
// NUCLOS-82			setForeground(Color.RED);
						if (StringUtils.looksEmpty((getText()))) {
							setText("<ung\u00fcltiger Eintrag>");
						}
					}
					return renderer;
				}
			};
		} else if (isSearchComponent()){
			//NOAINT-215
			return new My2CollectableComponentDetailTableCellRenderer();
		}
		else {
			return new CollectableComponentDetailTableCellRenderer();
		}
	}

	/**
	 * sets the renderer for the combo box. The value according to <code>CollectableField.getValue()</code>
	 * is rendered.
	 * The background color will not be displayed. This is a known Bug (http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6363287),
	 * and will hopefully be fixed by Sun, as there is no known workaround.
	 */
	protected void setRenderer() {
		getJComboBox().setRenderer(new CollectableFieldRenderer());
	}

	/**
	 * Overrides due to error in Swing: renderer/editor AND background color cannot be set at the same time!
	 * Temporary workaround is putting the information normally transported via color into tooltip.
	 */
	@Override
	protected String getToolTipTextForMultiEdit() {
		if (!isMultiEditable()) {
			throw new IllegalStateException("multiEditable");
		}
		String result;
		final String sLabel = getEntityField().getLabel();
		try {
			if (getDetailsModel().isValueToBeChanged()) {
				final CollectableField clctf = getFieldFromView();
				final String sValue = clctf.isNull() ? "<leer>" : (String) clctf.getValue();
				result = sLabel + " = " + sValue;
			}
			else {
				result = sLabel + " (<keine \u00c4nderung>)";
			}
		}
		catch (CollectableFieldFormatException ex) {
			result = "<Ung\u00fcltiger Wert>";
		}
		if (!getDetailsModel().hasCommonValue()) {
			// Due to an error in the Swing libraries which prevents the changing of background colors
			result += " - ACHTUNG! Dieses Feld hat unterschiedliche Werte in den bearbeiteten Objekten.";
		}

		return result;
	}

	public class CollectableFieldRenderer extends DefaultListCellRenderer {

		@Override
		public Component getListCellRendererComponent(JList list, Object oValue, int iIndex, boolean bSelected, boolean bCellHasFocus) {
			super.getListCellRendererComponent(list, oValue, iIndex, bSelected, bCellHasFocus);

			// reset preferred size:
			setPreferredSize(null);

			final CollectableField clctf = (CollectableField) oValue;
			Color colorForeground = bSelected ? list.getSelectionForeground() : list.getForeground();
			if (clctf == null) {
				LOG.warn("CollectableFieldRenderer.getListCellRendererComponent: oValue == null");
			}
			else {
				String sText = null;
				if (clctf.equals(clctfExtra)) {
					// NUCLOS-82	colorForeground = Color.RED;
					if (StringUtils.looksEmpty(LangUtils.toString(clctf.getValue()))) {
						sText = "<ung\u00fcltiger Eintrag>";
					}
				}
				// The space character for the null entry is important because otherwise the cell has minimal height.
				// Note that this is only for display. It doesn't have any impact on the internal value.
				if (sText == null) {
					sText = StringUtils.looksEmpty(LangUtils.toString(clctf.getValue())) ? " " : clctf.toString();
				}
				setText(sText);

				// add 10 pixel to the component width, otherwise the largest item
				// could not be shown completely in the combobox
				int height = getPreferredSize().height;
				int width = getPreferredSize().width;
				setPreferredSize(new Dimension(width+10, height));

				// Set a tooltip if the text cannot be displayed completely:
				final String sToolTipText = isToolTipNecessary() ? (StringUtils.looksEmpty(sText) ? null : sText) : null;
				setToolTipText(sToolTipText);
			}
			setForeground(colorForeground);

			// Does not work - see header!
			if (iIndex < 0) {
				Color background = getJComboBox().getBackground();
				setBackground(bSelected ? list.getSelectionBackground() : background);
			}

			// respect setColumns() - note that this has no effect by default as getColumns() always returns null:
			final Integer iColumns = getColumns();
			if (iColumns != null) {
				final Dimension dimPreferredSize = getPreferredSize();
				final int iPreferredWidth = Math.min(dimPreferredSize.width, getFontMetrics(getFont()).charWidth('m') * iColumns);
				setPreferredSize(new Dimension(iPreferredWidth, dimPreferredSize.height));
			}
			return this;
		}

		@Override
		public boolean isOpaque() {
			return true;
		}

		private boolean isToolTipNecessary() {
			// this is not exactly right, but it's probably the best guess we can make here:
			return getPreferredSize().width > getJComboBox().getSize().width;
		}
	}

	/**
	 * adds a "Refresh" entry to the popup menu, for a non-searchable component.
	 */
	@Override
	public JPopupMenu newJPopupMenu() {
		JPopupMenu result = super.newJPopupMenu();
		if (!this.isSearchComponent() /*&& getEntityField().isReferencing()*/) {
			if (getEntityField().isReferencing() || getValueListProvider() != null) {
				if (result == null)
					result = new JPopupMenu();
				result.add(newRefreshEntry());
			}
		}
		return result;
	}

	/**
	 * §precondition getEntityField().isReferencing()
	 * @return a new "refresh" entry for the context menu in edit mode
	 */
	protected final JMenuItem newRefreshEntry() {
		/*if (!getEntityField().isReferencing()) {
			throw new IllegalStateException();
		}*/
		final JMenuItem result = new JMenuItem(TEXT_REFRESH);
		result.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent ev) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						refreshCacheAndValueList(false);
					}
				});
			}
		});
		result.setEnabled(getJComponent().isEnabled());
		return result;
	}

	@Override
	public void handleCollectableEvent(final Collectable collectable, final MessageType messageType) {
		switch (messageType) {
			case EDIT_DONE:
			case DELETE_DONE:
			case STATECHANGE_DONE:
				// data has changed -> simply refresh.
				refreshCacheAndValueList(false);
				break;
			case NEW_DONE:
				// data has changed -> refresh and try to select new collectable.
				refreshCacheAndValueList(false);
				for (int i = 0; i < getJComboBox().getItemCount(); i++) {
					Object o = getJComboBox().getItemAt(i);
					if (o instanceof CollectableField) {
						CollectableField field = (CollectableField) o;
						if (LangUtils.equal(field.getValueId(), collectable.getId())) {
							getModel().setField(field);
							runLocked(new Runnable() {
								@Override
								public void run() {
									modelToView();
								}
							});
							break;
						}
					}
				}
				break;
		}
	}

	private void refreshCacheAndValueList(boolean async) {
		if (getValueListProvider() instanceof CachingCollectableFieldsProvider) {
			((CachingCollectableFieldsProvider)getValueListProvider()).clear();
		}
		refreshValueList(async);
	}

	@Override
	public void bindLayoutNavigationSupportToProcessingComponent() {
		ComboBoxEditor cbxEditor = getLabeledComboBox().getJComboBox().getEditor();
		
		if (cbxEditor instanceof MyComboBoxEditor) {
			MyComboBoxEditor myCbxEditor = (MyComboBoxEditor)cbxEditor;
			JTextField textField = myCbxEditor.getMyComboBoxEditorTextField();
			if (textField instanceof MyComboBoxEditorTextField) {
				((MyComboBoxEditorTextField)textField).setLayoutNavigationCollectable(this);
			}
		}
	}

	@Override
	public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
		requestValueList();
	}

	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
		popupClosing(false);
	}

	@Override
	public void popupMenuCanceled(PopupMenuEvent e) {
		popupClosing(true);
		
	}

	@Override
	protected CollectableField getClctfExtra() {
		return clctfExtra;
	}
}	// class CollectableComboBox
