package org.nuclos.client.ui;

import org.apache.log4j.Logger;
import org.nuclos.common.collection.Pair;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.ArrayList;

/*
 * Helper class for the Bubble-Object.
 * The Position-Objects define the position of the Bubble relative to it's parent.
 * The BubbleShapeXX-Classes define the shape of the Bubble-Object.
 * The shape of a Bubble differs relative to its position.
 */
public class BubbleUtils {

    private static final Logger LOG = Logger.getLogger(Bubble.class);

    static final int MAX_ARC_SIZE = 20;

    public enum Position {
        NW(0.2, 0.1, 0.15) {
            @Override
            public Shape getShape(int width, int height, int margin) {
                return new BubbleShapeNx(this, width, height, margin);
            }

            @Override
            public void relocate(Bubble bubble, Component parent, Point parentLocation, Dimension parentSize, Dimension size) {
                bubble.setBounds(
                        parentLocation.x - 10,
                        parentLocation.y - size.height,
                        size.width,
                        size.height);
            }

            @Override
            public void translateForLabel(Graphics g, int width, int height, int arcSize) {
                g.translate(
                        arcSize / 2,
                        arcSize / 2);
            }
        },
        NE(0.2, 0.1, 0.15) {
            @Override
            public Shape getShape(int width, int height, int margin) {
                return new BubbleShapeNx(this, width, height, margin);
            }

            @Override
            public void relocate(Bubble bubble, Component parent, Point parentLocation, Dimension parentSize, Dimension size) {
                bubble.setBounds(
                        parentLocation.x + parentSize.width - 10,
                        parentLocation.y + 10 - size.height,
                        size.width,
                        size.height);
            }

            @Override
            public void translateForLabel(Graphics g, int width, int height, int arcSize) {
                g.translate(
                        arcSize / 2,
                        arcSize / 2);
            }
        },
        CENTER(0.0, 0.0, 0.0) {
            @Override
            public Shape getShape(int width, int height, int margin) {
                return new BubbleShape(this, margin, margin, width, height, margin);
            }

            @Override
            public void relocate(Bubble bubble, Component parent, Point parentLocation, Dimension parentSize, Dimension size) {
                bubble.setBounds(
                        parentLocation.x + parentSize.width / 2 - size.width / 2,
                        parentLocation.y + parentSize.height / 2 - size.height / 2,
                        size.width,
                        size.height);
            }

            @Override
            public void translateForLabel(Graphics g, int width, int height, int arcSize) {
                g.translate(
                        arcSize / 2,
                        arcSize / 2);
            }
        },
        SW(0.2, 0.75, 0.15) {
            @Override
            public Shape getShape(int width, int height, int margin) {
                return new BubbleShapeSW(this, width, height, margin);
            }

            @Override
            public void relocate(Bubble bubble, Component parent, Point parentLocation, Dimension parentSize, Dimension size) {
                bubble.setBounds(
                        parentLocation.x - size.width + 10,
                        parentLocation.y + parentSize.height - 10,
                        size.width,
                        size.height);
            }

            @Override
            public void translateForLabel(Graphics g, int width, int height, int arcSize) {
                g.translate(
                        arcSize / 2,
                        (int) (height * arrowRelLength + MAX_ARC_SIZE / 4));
            }
        },
        SE(0.2, 0.5, 0.25) {
            @Override
            public Shape getShape(int width, int height, int margin) {
                return new BubbleShapeSE(this, width,height,margin);
            }

            @Override
            public void relocate(Bubble bubble, Component parent, Point parentLocation, Dimension parentSize, Dimension size) {
                bubble.setBounds(
                        parentLocation.x - 10,
                        parentLocation.y + parentSize.height - 10,
                        size.width,
                        size.height);
            }

            @Override
            public void translateForLabel(Graphics g, int width, int height, int arcSize) {
                g.translate(
                        arcSize / 2,
                        (int) (height * arrowRelLength + MAX_ARC_SIZE / 4));
            }
        };
        public final double arrowRelLength;
        public final double arrowRelPos;
        public final double arrowRelWidth;

        Position(double arrowRelLength, double arrowRelPos, double arrowRelWidth) {
            this.arrowRelLength = arrowRelLength;
            this.arrowRelPos = arrowRelPos;
            this.arrowRelWidth = arrowRelWidth;
        }

        public abstract Shape getShape(int width, int height, int margin);
        public abstract void relocate(Bubble bubble, Component parent, Point parentLocation, Dimension parentSize, Dimension size);
        public abstract void translateForLabel(Graphics g, int width, int height, int arcSize);
    }

    /*
     * The most basic Bubble-shape is simply a rectangle with rounded corners.
     * This class is used as superclass to the other Bubble-shapes and therefore implements all the functions that
     * differ from its RoundRectangle2D.Float-superclass (mainly to incorporate the arrow that is included in the other
     * Bubble-shapes).
     * @arg BubbleUtils.Position pos: Used in the BubblePathIterator to determine whether the arrow is supposed to be
     *      above or below the rectangle.
     * @arg int x, y: Coordinates of the top left corner of the rectangle.
     * @arg int width, height: Width and height of the rectangle.
     * @arg int margin: Amount of pixels by which the shape should be shrunk. This is relevant as the shape of both the
     *      Bubble-Object and the BubbleContent-Object are determined by this class: The Bubble-Object needs to be
     *      larger lest the outline of the BubbleContent be cut short.
     */
    private static class BubbleShape extends java.awt.geom.RoundRectangle2D.Float {
        private static final long serialVersionUID = 8895815821355924870L;
        private Polygon arrow;
        private BubbleUtils.Position pos;

        BubbleShape(BubbleUtils.Position pos,int x, int y, int width, int height, int margin) {
            super(x, y, width - (2 * margin), height - (2 * margin), BubbleUtils.MAX_ARC_SIZE, BubbleUtils.MAX_ARC_SIZE);
            this.pos = pos;
        }

        /*
         * The following functions all modify the superclass to incorporate the arrow if it exists.
         */
        @Override
        public boolean contains(double x, double y) {
            return super.contains(x, y) || (arrow != null && arrow.contains(x, y));
        }

        @Override
        public boolean contains(double x, double y, double w, double h) {
            return super.contains(x, y, w, h) || (arrow != null && arrow.contains(x, y, w, h));
        }

        @Override
        public boolean intersects(double x, double y, double w, double h) {
            return super.intersects(x, y, w, h) || (arrow != null && arrow.intersects(x, y, w, h));
        }

        @Override
        public PathIterator getPathIterator(AffineTransform at) {
            if (arrow != null){
                return new BubblePathIterator(super.getPathIterator(at), arrow, pos);
            }else {
                return super.getPathIterator(at);
            }
        }

        @Override
        public PathIterator getPathIterator(AffineTransform at, double flatness) {
            if (arrow != null){
                return new BubblePathIterator(super.getPathIterator(at,flatness), arrow, pos);
            }else {
                return super.getPathIterator(at, flatness);
            }
        }

        /*
         * If the BubbleShape contains an arrow, the PathIterator has to be modified. For this, a custom class was
         * created. The new lines are added to the top or bottom of the rectangle depending on the position of the
         * Bubble.
         * @arg PathIterator parent: The PathIterator to be modified.
         * @arg Polygon arrow: The points that make up the arrow.
         * @arg Position pos: The position of the Bubble.
         */
        private static class BubblePathIterator implements PathIterator {
            private java.util.List<Pair<Integer, double[]>> l = new ArrayList<>();
            private int index;

            BubblePathIterator(PathIterator parent, Polygon arrow, Position pos) {
                while(!parent.isDone()) {
                    double[] values = new double[6];
                    int t = parent.currentSegment(values);
                    l.add(new Pair<>(t, values));
                    parent.next();
                }
                switch (pos){
                    case NE:
                    case NW:
                        l.add(3, new Pair<>(PathIterator.SEG_LINETO, new double[] { arrow.xpoints[0], arrow.ypoints[0], 0.0, 0.0, 0.0, 0.0 }));
                        l.add(4, new Pair<>(PathIterator.SEG_LINETO, new double[] { arrow.xpoints[1], arrow.ypoints[1], 0.0, 0.0, 0.0, 0.0 }));
                        l.add(5, new Pair<>(PathIterator.SEG_LINETO, new double[] { arrow.xpoints[2], arrow.ypoints[2], 0.0, 0.0, 0.0, 0.0 }));
                        break;
                    case SE:
                    case SW:
                        l.add(7, new Pair<>(PathIterator.SEG_LINETO, new double[] { arrow.xpoints[0], arrow.ypoints[0], 0.0, 0.0, 0.0, 0.0 }));
                        l.add(8, new Pair<>(PathIterator.SEG_LINETO, new double[] { arrow.xpoints[1], arrow.ypoints[1], 0.0, 0.0, 0.0, 0.0 }));
                        l.add(9, new Pair<>(PathIterator.SEG_LINETO, new double[] { arrow.xpoints[2], arrow.ypoints[2], 0.0, 0.0, 0.0, 0.0 }));
                        break;
                }
                index = 0;
            }

            @Override
            public int getWindingRule() {
                return PathIterator.WIND_EVEN_ODD;
            }

            @Override
            public boolean isDone() {
                return index >= l.size();
            }

            @Override
            public void next() {
                index++;
            }

            @Override
            public int currentSegment(float[] coordinates) {
                Pair<Integer, double[]> pair = l.get(index);
                for(int i=0; i<6; i++)
                    coordinates[i] = (float) pair.y[i];
                return pair.x;
            }

            @Override
            public int currentSegment(double[] coordinates) {
                Pair<Integer, double[]> pair = l.get(index);
                System.arraycopy(pair.y, 0, coordinates, 0, 6);
                return pair.x;
            }
        }
    }

    /*
     * Describes the shape of a Bubble that appears anywhere north of its parent.
     * @arg Position pos: Used to determine how to draw the arrow.
     * @arg int width, height: Width and height of the Bubble.
     * @arg int margin: Amount of pixels by which the shape should be shrunk (->see BubbleShape).
     */
    private static class BubbleShapeNx extends BubbleShape {
        BubbleShapeNx(Position pos, int width, int height, int margin){
            super(pos, margin, margin, width, (int) (height * (1 - pos.arrowRelLength)), margin);

            int extraPadding = 0;
            if (margin == 1) extraPadding = 5;
            int[] xPoints = new int[] {
                    (int)(width * pos.arrowRelPos) + margin - extraPadding,
                    margin + 20,
                    (int)((width * pos.arrowRelPos) + (width * pos.arrowRelWidth)) + margin };
            int[] yPoints = new int[] {
                    (int) (height * (1 - pos.arrowRelLength) - margin),
                    height - margin,
                    (int) (height * (1 - pos.arrowRelLength) - margin) };
            super.arrow = new Polygon(xPoints, yPoints, xPoints.length);
        }
    }

    /*
     * Describes the shape of a Bubble that appears southwest of its parent.
     * @arg Position pos: Used to determine how to draw the arrow.
     * @arg int width, height: Width and height of the Bubble.
     * @arg int margin: Amount of pixels by which the shape should be shrunk (->see BubbleShape).
     */
    private static class BubbleShapeSW extends BubbleShape {
        BubbleShapeSW(Position pos, int width, int height, int margin){
            super(pos, margin, (int)(height * pos.arrowRelLength) + margin, width, (int) (height * (1 - pos.arrowRelLength)), margin);
            int extraPadding = 0;
            if (margin == 1) extraPadding = 20;
            int[] xPoints = new int[] {
                    (int) (width * pos.arrowRelPos) - margin + extraPadding,
                    width,
                    (int) ((width * pos.arrowRelPos) - (width * pos.arrowRelWidth)) + margin};
            int[] yPoints = new int[] {
                    (int) (height * pos.arrowRelLength) + margin,
                    margin + (extraPadding/10) ,
                    (int) (height * pos.arrowRelLength) + margin};
            super.arrow = new Polygon(xPoints, yPoints, xPoints.length);
        }
    }

    /*
     * Describes the shape of a Bubble that appears southeast of and relative to its parent.
     * @arg Position pos: Used to determine how to draw the arrow.
     * @arg int width, height: Width and height of the Bubble.
     * @arg int margin: Amount of pixels by which the shape should be shrunk (->see BubbleShape).
     */
    private static class BubbleShapeSE extends BubbleShape {
        BubbleShapeSE(Position pos, int width, int height, int margin){
            super(pos, margin, (int)(height * pos.arrowRelLength) + margin, width, (int) (height * (1 - pos.arrowRelLength)), margin);
            int extraPadding = 0;
            if (margin == 1) extraPadding = 20;
            int[] xPoints = new int[] {
                    (int) (width * pos.arrowRelPos) - margin,
                    margin + 18,
                    (int) ((width * pos.arrowRelPos) - (width * pos.arrowRelWidth)) + margin - extraPadding};
            int[] yPoints = new int[] {
                    (int) (height * pos.arrowRelLength) + margin,
                    margin + (extraPadding/10),
                    (int) (height * pos.arrowRelLength) + margin};
            super.arrow = new Polygon(xPoints, yPoints, xPoints.length);
        }
    }
}
