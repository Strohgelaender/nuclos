//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.labeled;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.text.JTextComponent;

import org.nuclos.client.ui.ColorProvider;
import org.nuclos.client.ui.CommonJTextField;
import org.nuclos.client.ui.EllipsisJTextField;
import org.nuclos.client.ui.LayoutNavigationCollectable;
import org.nuclos.client.ui.LayoutNavigationProcessor;
import org.nuclos.client.ui.ToolTipTextProvider;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.URIMouseAdapter;
import org.nuclos.client.ui.collect.DefaultLayoutNavigationSupportContext;
import org.nuclos.client.ui.collect.LayoutNavigationSupport;
import org.nuclos.client.ui.collect.LayoutNavigationSupport.ExecutionPoint;
import org.nuclos.common2.StringUtils;

/**
 * <code>CollectableComponent</code> that presents a value in a <code>JTextField</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public class LabeledTextField extends LabeledTextComponent {
	
	public class InnerJTextField extends EllipsisJTextField implements LayoutNavigationProcessor {
		protected LayoutNavigationSupport lns;
		private LayoutNavigationCollectable lnc;

		@Override
		public String getToolTipText(MouseEvent ev) {
			final ILabeledComponentSupport support = getLabeledComponentSupport();
			final ToolTipTextProvider provider = support.getToolTipTextProvider();
			return StringUtils.concatHtml(false, provider != null ? provider.getDynamicToolTipText() : super.getToolTipText(ev),
					support.getValidationToolTip());
		}
		
		@Override
		public Color getBackground() {
			final ColorProvider colorproviderBackground = getLabeledComponentSupport().getColorProvider();
			final Color colorDefault = super.getBackground();
			return (colorproviderBackground != null) ? colorproviderBackground.getColor(colorDefault) : colorDefault;
		}
		
		@Override
		protected boolean processKeyBinding(final KeyStroke ks, final KeyEvent e,
				final int condition, final boolean pressed) {
			
			boolean processed = false;
			if (null != lnc) {
				final LayoutNavigationSupport lns = lnc.getLayoutNavigationSupport();
				if (lns != null) {
					final DefaultLayoutNavigationSupportContext ctx = new DefaultLayoutNavigationSupportContext(pressed, ks, e, condition, this, lnc);
					processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.BEFORE);
					if (!processed) {
						processed = super.processKeyBinding(ks, e, condition, pressed);
						//if (!processed) {
							ctx.setProcessed(processed);
							processed = lns.processLayoutNavigationEvent(ctx, ExecutionPoint.AFTER);
						//}
					}
				} else {
					processed = super.processKeyBinding(ks, e, condition, pressed);
				}
			} else {
				processed = super.processKeyBinding(ks, e, condition, pressed);
			}
			return processed;
		}
		
		@Override
		public void setLayoutNavigationCollectable(
				LayoutNavigationCollectable lnc) {
				this.lnc = lnc;
			
		}
	}
	
	private final JTextField tf = new InnerJTextField();
	
	private LayoutNavigationSupport lns;
	
	public LabeledTextField(ILabeledComponentSupport support){
		this(support, true, String.class, null, false);
	}
	
	public LabeledTextField(ILabeledComponentSupport support, boolean isNullable, Class<?> javaClass, String inputFormat, boolean bSearchable) {
		super(support, isNullable, javaClass, inputFormat, bSearchable);
		initValidation(isNullable, javaClass, inputFormat);
		if(this.validationLayer != null){
			this.addControl(this.validationLayer);
		} else {
			this.addControl(this.tf);
		}
		this.getJLabel().setLabelFor(this.tf);
		setMouseListenerOnComponent();
	}
	
	protected void setMouseListenerOnComponent() {
		if(getTextField() != null) {
			getTextField().addMouseListener(new URIMouseAdapter());
		}
	}
	
	@Override
	protected JComponent getLayeredComponent(){
		return this.tf;
	}

	@Override
	protected JTextComponent getLayeredTextComponent(){
		return this.tf;
	}
	
	public JTextField getTextField() {
		return this.tf;
	}
	
	public JLabel getLabel(){
		return this.getJLabel();
	}

	/**
	 * @return the text field
	 */
	@Override
	public JTextComponent getJTextComponent() {
		return this.getTextField();
	}

	/**
	 * sets the number of columns of the textfield
	 * @param iColumns
	 */
	@Override
	public void setColumns(int iColumns) {
		this.getTextField().setColumns(iColumns);
	}

	@Override
	public void setName(String sName) {
		super.setName(sName);
		UIUtils.setCombinedName(this.tf, sName, "tf");
	}
	
	@Override
	public void setForeground(Color foregroundColor) {
		super.setForeground(foregroundColor);
		if (tf != null) {
			tf.setForeground(foregroundColor);
		}
	}


}  // class LabeledTextField
