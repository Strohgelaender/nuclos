//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.eo.component;

import java.text.ParseException;

import javax.swing.*;

import org.nuclos.client.ui.eo.mvc.IModelAwareGuiComponent;
import org.nuclos.client.ui.eo.mvc.ITypeConverter;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonValidationException;

public class SimpleValueJComboBox<PK> extends JComboBox implements IModelAwareGuiComponent<PK> {
	
	private final UID modelFieldUid;
	
	private final Object defaultValue;
	
	private final ITypeConverter converter;
	
	private EntityObjectVO<PK> associatedEo;
	
	public SimpleValueJComboBox(TypeConverterFactory tcFactory, ComboBoxModel model, UID modelFieldUid, Object defaultValue) {
		super(model);
		this.modelFieldUid = modelFieldUid;
		this.defaultValue = defaultValue;
		this.converter = tcFactory.getTypeConverterForField(modelFieldUid);
	}
	
	@Override
	public void fromComponentToModel() throws CommonValidationException, ParseException {
		final Object guiValue = getSelectedItem();
		if (guiValue != null) {
			EntityObjectUtils.setOrUpdateValue(associatedEo, modelFieldUid, converter.fromComponentToModel(guiValue));
			// to.setFieldValue(modelFieldUid, converter.fromComponentToModel(guiValue));
		} else {
			EntityObjectUtils.setOrUpdateValue(associatedEo, modelFieldUid, defaultValue);
			// to.setFieldValue(modelFieldUid, defaultValue);
		}
	}

	@Override
	public void fromModelToComponent() {
		if (associatedEo != null) {
			final Object modelValue = associatedEo.getFieldValue(modelFieldUid);
			if (modelValue != null) {
				setSelectedItem(converter.fromModelToComponent(modelValue));
			} else {
				setSelectedItem(defaultValue);
			}
		} else {
			setSelectedItem(defaultValue);			
		}
	}

	@Override
	public void setAssociatedEntityObject(EntityObjectVO<PK> eo) {
		this.associatedEo = eo;
	}

	@Override
	public EntityObjectVO<PK> getAssociatedEntityObject() {
		return associatedEo;
	}

}
