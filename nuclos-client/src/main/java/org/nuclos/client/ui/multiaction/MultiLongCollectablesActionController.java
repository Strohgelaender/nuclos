//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.multiaction;

import java.util.Collection;

import javax.swing.*;

import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.collect.collectable.Collectable;

/**
 * Controller for actions (delete, change etc.) on multiple <code>Collectable</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public class MultiLongCollectablesActionController<R> extends MultiCollectablesActionController<Long,Collectable<Long>,R>{

	public MultiLongCollectablesActionController(
			CollectController<Long, ? extends Collectable<Long>> ctl,
			String sTitle,
			org.nuclos.client.ui.multiaction.MultiCollectablesActionController.Action<Collectable<Long>, R> action,
			Collection<Collectable<Long>> collclct) {
		super(ctl, sTitle, action, collclct);
		// TODO Auto-generated constructor stub
	}

	public MultiLongCollectablesActionController(
			MainFrameTab tab,
			Collection<Collectable<Long>> coll,
			String sTitle,
			Icon iconFrame,
			org.nuclos.client.ui.multiaction.MultiCollectablesActionController.Action<Collectable<Long>, R> action) {
		super(tab, coll, sTitle, iconFrame, action);
		// TODO Auto-generated constructor stub
	}



}  // class MultiCollectablesActionController

