package org.nuclos.client.ui.collect.subform;

/**
 * Created by oliver brausch on 17.07.17.
 */

import java.awt.*;
import java.awt.event.InvocationEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.io.Closeable;
import java.util.EventListener;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.event.TableColumnModelExtListener;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.FocusActionListener;
import org.nuclos.client.common.SubFormController;
import org.nuclos.client.ui.collect.DynamicRowHeightSupport;
import org.nuclos.client.ui.collect.TooltipProvider;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.client.ui.collect.component.TableCellCursor;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.client.ui.collect.result.FilterableTable;
import org.nuclos.client.ui.event.TableColumnModelAdapter;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.client.ui.table.DummyTableModel;
import org.nuclos.client.ui.table.TableCellEditorProvider;
import org.nuclos.client.ui.table.TableCellRendererProvider;
import org.nuclos.common.NuclosFieldNotInModelException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * not anymoure "inner class SubFormTable".
 */
public class SubFormTable extends FilterableTable implements IReferenceHolder {

	private static final Logger LOG = Logger.getLogger(SubFormTable.class);

	public static class SubFormTableRowSorter extends TableRowSorter<TableModel> {
		SubFormTableRowSorter(TableModel model) {
			super(model);

		}

		@Override
		public boolean isSortable(int column) {
			// At the moment, sorting still is performed via the SortableTableModel
			return false;
		}
	}



	/**
	 * TODO: It would be very nice to have a static inner class here. However,
	 * 		 when I tried this (with a reference to the TableModel in the constructor)
	 * 		 I trashed the WYSIWYG subform editor (e.g. properties is complex Subform
	 * 		 cases (Accelingua)). (tp)
	 */
	private class SubformTableColumnModel extends DefaultTableColumnModel
			implements Closeable {

		private boolean closed = false;

		private SubformTableColumnModel(TableModel model) {


		}


		@Override
		public void close() {
			// Close is needed for avoiding memory leaks
			// If you want to change something here, please consult me (tp).
			if (!closed) {
				LOG.debug("close() SubformTableColumnModel: " + this);
				tableColumns.clear();
				closed = true;
			}
		}

		@Override
		public void addColumn(TableColumn column) {
			if (getModel() instanceof SubFormTableModel) {
				// NUCLEUSINT-742: the identifier of the column is now the entity field name
				// (instead of the localized label)
				UID fieldUid = ((SubFormTableModel) getModel()).getColumnFieldUid(column.getModelIndex());
				column.setIdentifier(fieldUid);
			}
			if (column != null)
				super.addColumn(column);
		}

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			super.propertyChange(evt);
			fireColumnPropertyChange(evt);
		}

		protected void fireColumnPropertyChange(PropertyChangeEvent evt) {
			Object[] listeners = listenerList.getListenerList();
			for (int i = listeners.length - 2; i >= 0; i -= 2) {
				if (listeners[i] == TableColumnModelListener.class
						&& listeners[i + 1] instanceof TableColumnModelExtListener) {
					((TableColumnModelExtListener) listeners[i + 1]).columnPropertyChange(evt);
				}
			}
		}
	}

	private final List<EventListener> refs = new LinkedList<EventListener>();

	private TooltipProvider tooltipProvider;

	private TableCellEditorProvider celleditorprovider;
	private TableCellRendererProvider cellrendererprovider;
	private SubForm subform;

	private SubformRowHeader rowheader;

	private boolean newRowOnNext = false;

	// private SubformTableColumnModel myTableColumnModel;

	private boolean closed = false;

	boolean invalidateRowHeights = false;

	private SubFormController.FocusListSelectionListener focusListSelectionListener;

	private UID prevComponent = null;

	public SubFormTable() {
		setCellSelectionEnabled(true);
	}

	public void setTooltipProvider(TooltipProvider tooltipProvider) {
		this.tooltipProvider = tooltipProvider;
	}

	@Override
	public JToolTip createToolTip() {
		if(tooltipProvider!=null) {
			JToolTip tip = tooltipProvider.createToolTip();
			setToolTipText(tip.getToolTipText());
			tip.setComponent(this);
			return tip;
		}
		return super.createToolTip();
	}

	@Override
	public void editingCanceled(ChangeEvent e) {
		if (null != getSubForm().getLayoutNavigationSupport()) {
			getSubForm().getLayoutNavigationSupport().editingCanceled(e);
		} else {
			super.editingCanceled(e);
		}
	}

	@Override
	public boolean isRequestFocusEnabled() {
		return true;
	}

	SubFormTable(SubForm sub){
		this();
		this.subform = sub;
		getColumnModel().addColumnModelListener(new TableColumnModelAdapter() {

			@Override
			public void columnMarginChanged(ChangeEvent e) {
				super.columnMarginChanged(e);
//					updateRowHeights();
			}

		});
	}

	public SubForm getSubForm() {
		return subform;
	}

	void updateRowHeights() {
		for (int iRow = 0; iRow < getRowCount(); iRow++) {
			final int iHeight = subform.getRowHeightCtrl().getMaxRowHeight(iRow);
			if (iHeight > 0) {
				setRowHeightStrict(iRow, subform.getValidRowHeight(iHeight));
			} else {
				setRowHeight(iRow, subform.getMinRowHeight());
			}
		}
	}

	@Override
	public void close() {
		// Close is needed for avoiding memory leaks
		// If you want to change something here, please consult me (tp).
		if (!closed) {
			LOG.debug("close() SubFormTable: " + this);
			if (getModel() instanceof SubformTableColumnModel) {
				((SubformTableColumnModel) getModel()).close();
			}
			if (getColumnModel() instanceof SubformTableColumnModel) {
				((SubformTableColumnModel) getColumnModel()).close();
			}
			// not allowed by swing
			// setModel(null);
			setModel(DummyTableModel.INSTANCE);
			if (rowheader != null) {
				rowheader.close();
			}
			rowheader = null;
			subform = null;
			tooltipProvider = null;
			celleditorprovider = null;
			cellrendererprovider = null;
			focusListSelectionListener = null;

			closed = true;
		}
	}

	public void setFocusListSelectionListener(SubFormController.FocusListSelectionListener focusListSelectionListener) {
		this.focusListSelectionListener = focusListSelectionListener;
	}

	private UID getComponentBefore(UID identifier) {
		UID result = null;
		for (Column column : this.getSubForm().getColumns()) {
			if (column.getNextFocusField() != null && column.getNextFocusField().equals(identifier)) {
				result = column.getUID();
				if (prevComponent == null || prevComponent.equals(result))
					return result;				}
		}
		return result;
	}

	private void setPrevComponent(UID prevComponent) {
		this.prevComponent = prevComponent;
		SubformRowHeader rowHeader = getSubForm().getSubformRowHeader();
		if ((rowHeader.getHeaderTable() instanceof FixedColumnRowHeader.HeaderTable)) {
			((FixedColumnRowHeader.HeaderTable)rowHeader.getHeaderTable()).setPreviousComponent(prevComponent);
		}
	}

	void setPreviousComponent(UID prevComponent) {
		this.prevComponent = prevComponent;
	}

	@Override
	public void changeSelection(int rowIndex, final int columnIndex, boolean toggle, boolean extend) {
		final AWTEvent event = EventQueue.getCurrentEvent();
		if (event instanceof KeyEvent) {
			final KeyEvent ke = (KeyEvent)event;
			if(ke.isShiftDown() || ke.isControlDown() || ke.getKeyCode() == KeyEvent.VK_UP) {
				newRowOnNext = false;
				SubformRowHeader rowHeader = getSubForm().getSubformRowHeader();
				boolean blnHasFixedRows = (rowHeader != null && rowHeader.getHeaderTable().getColumnCount() > 1);
				if (blnHasFixedRows && columnIndex == getColumnCount() - 1)
					rowIndex = rowIndex + 1 == getRowCount() ? 0 : rowIndex + 1;
			}
		}
		if (event instanceof MouseEvent || (extend && !toggle)) { // NUCLOS-7409 Let Swing handle "ExtendSelection"
			super.changeSelection(rowIndex, columnIndex, toggle, extend);
		}
		else
			changeSelection(rowIndex, columnIndex, toggle, extend, false);

		int colCount = getColumnCount();
		if(columnIndex == colCount-1) {
			if (event instanceof KeyEvent) {
				final KeyEvent ke = (KeyEvent)event;
				if(!ke.isShiftDown() && !ke.isControlDown())
					newRowOnNext = true;
				else
					newRowOnNext = false;
			} else
				newRowOnNext = true;
		} else if (event instanceof MouseEvent)
			newRowOnNext = false;

		if (getSubForm() == null) {
			super.changeSelection(rowIndex, columnIndex, toggle, extend);
			return;
		}

		final UID sNextColumn = getSelectedColumn() == -1 ? null :
				getSubForm().getColumnNextFocusField((UID)getColumnModel().getColumn(getSelectedColumn()).getIdentifier());

		if (sNextColumn != null) {
			int colIndex = -1;
			try {
				colIndex = getColumnModel().getColumnIndex(sNextColumn);
			} catch (Exception e) {
				// ignore.
			}
			if (colIndex != -1) {
				if (colIndex < getSelectedColumn() &&
						rowIndex == getRowCount() - 1)
					newRowOnNext = true;
				else
					newRowOnNext = false;
			}
			else
				newRowOnNext = false;
		}
	}

	public void changeSelection(final int rwIndex, final int clIndex, boolean toggle, boolean extend, final boolean fixed) {
		boolean bChange = true;

		if (getColumnCount() <= getSelectedColumn())
			return;

		UID sNextColumn = getSelectedColumn() == -1 || fixed ? null :
				getSubForm().getColumnNextFocusField((UID)getColumnModel().getColumn(getSelectedColumn()).getIdentifier());

		final AWTEvent event = EventQueue.getCurrentEvent();

		int rowIndex = rwIndex;
		int colIndex;
		try {
			if (event instanceof KeyEvent && !fixed) {
				final KeyEvent ke = (KeyEvent)event;
				if(ke.isShiftDown() || ke.isControlDown()) {
					sNextColumn = getSelectedColumn() == -1 ? null
							: getComponentBefore((UID)getColumnModel().getColumn(getSelectedColumn()).getIdentifier());
				}
			}

			setPrevComponent(null);
			colIndex = sNextColumn == null || fixed ? clIndex : getColumnModel().getColumnIndex(sNextColumn);
			if (sNextColumn != null) {
				final UID colIdentifier = (UID)getColumnModel().getColumn(getSelectedColumn()).getIdentifier();
				if (event instanceof KeyEvent) {
					final KeyEvent ke = (KeyEvent)event;
					if(ke.isShiftDown() || ke.isControlDown()) {
						;//sNextColumn = null;
					} else
						setPrevComponent(colIdentifier);
				} else
					setPrevComponent(colIdentifier);
			}
		} catch (IllegalArgumentException e) {
			final SubformRowHeader rowHeader = getSubForm().getSubformRowHeader();
			boolean blnHasFixedRows = (rowHeader != null && rowHeader.getHeaderTable().getColumnCount() > 1);
			if (blnHasFixedRows) {
				colIndex = rowHeader.getHeaderTable().getColumnModel().getColumnIndex(sNextColumn);
				if (newRowOnNext && getSelectedRow() == getRowCount() - 1) {
					final int col = colIndex;
					final int row = getRowCount();
					for (FocusActionListener fal : subform.getFocusActionLister()) {
						fal.focusAction(new EventObject(this));
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								if (!(rowHeader.getHeaderTable() instanceof FixedColumnRowHeader.HeaderTable)) {
									rowHeader.getHeaderTable().changeSelection(row, col, false, false);
								} else {
									((FixedColumnRowHeader.HeaderTable)rowHeader.getHeaderTable()).changeSelection(row, col, false, false, true);
								}
							}
						});
					}

				} else {
					if (!(rowHeader.getHeaderTable() instanceof FixedColumnRowHeader.HeaderTable)) {
						rowHeader.getHeaderTable().changeSelection(rowIndex, colIndex, false, false);
					} else {
						((FixedColumnRowHeader.HeaderTable)rowHeader.getHeaderTable()).changeSelection(rowIndex, colIndex, false, false, true);
					}
				}
				return;
			} else {
				colIndex = clIndex;
			}
		}

		final int columnIndex = colIndex;
		if (fixed)
			newRowOnNext = false;

		if (newRowOnNext) {
			if (focusListSelectionListener != null) {
				int cIndex = getSelectedColumn() == getColumnCount() -1 ? 0 : getSelectedColumn();
				if (sNextColumn != null || (((rowIndex == 1 && cIndex == -1) || (rowIndex == 0 && cIndex == 0)) && getSelectedRow() > 0)) {
					if (sNextColumn != null || (rowIndex == 0 && cIndex == 0 && getSelectedRow() > 0)) {
						focusListSelectionListener.valueChanged(new ListSelectionEvent(this, rowIndex, getSelectedRow(), false));
						bChange = !(event instanceof KeyEvent || event instanceof InvocationEvent) ? true : false;
					}
				}
			}
		}

		if (bChange) {
			if (sNextColumn != null) {
				int cIndex = sNextColumn == null ? -1 : getColumnModel().getColumnIndex(sNextColumn);
				if (cIndex != -1) {
					if (cIndex <= getSelectedColumn())
						rowIndex = rowIndex + 1;
				}
			}
			super.changeSelection(rowIndex, columnIndex, toggle, extend);
		}

		if(event instanceof KeyEvent || event instanceof InvocationEvent) {
			if(newRowOnNext) {
				if(getRowCount() == 1 && !(event instanceof InvocationEvent)) {
					final KeyEvent ke = (KeyEvent)event;
					if(!ke.isShiftDown() && !ke.isControlDown()) {
						for(FocusActionListener fal : subform.getFocusActionLister()) {
							fal.focusAction(new EventObject(this));
							final int row = rowIndex;
							final int col[] = getNextEditableCell(this, rowIndex, columnIndex, false);
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									if (editCellAt(row + 1, col[1])) {
										Component editor = getEditorComponent();
										if(editor != null)
											editor.requestFocusInWindow();
										changeSelection(row + 1, col[1], false, false);
									}
								}
							});
						}
					}
				}
				newRowOnNext = false;

				if (rowIndex == 0)
					return;
			}
			int colCount = getColumnCount();
			if(columnIndex == colCount-1) {
				if (event instanceof KeyEvent) {
					final KeyEvent ke = (KeyEvent)event;
					if(!ke.isShiftDown() && !ke.isControlDown())
						newRowOnNext = true;
					else {
						newRowOnNext = rowIndex == getRowCount() -1;
					}
				} else
					newRowOnNext = true;
			}

			boolean bShift = false;
			if (event instanceof KeyEvent) {
				final KeyEvent ke = (KeyEvent)event;
				if(ke.isShiftDown() || ke.isControlDown()) {
					bShift = true;
				}
			}

			SubformRowHeader rowHeader = getSubForm().getSubformRowHeader();
			boolean blnHasFixedRows = (rowHeader != null && rowHeader.getHeaderTable().getColumnCount() > 1);

			if (!fixed && ((columnIndex == 0 && !bShift) || (columnIndex == 0 && blnHasFixedRows) || (columnIndex == getColumnCount() - 1 && bShift))) {
				if (blnHasFixedRows) {
					if (!(rowHeader.getHeaderTable() instanceof FixedColumnRowHeader.HeaderTable))
						if (event instanceof KeyEvent) {
							final KeyEvent ke = (KeyEvent)event;
							if(!ke.isShiftDown() && !ke.isControlDown()) {
								rowHeader.getHeaderTable().changeSelection(rowIndex, 0, false, false);
							} else {
								rowHeader.getHeaderTable().changeSelection(rowIndex, rowHeader.getHeaderTable().getColumnCount() - 1, false, false);
							}
						}
						else
							rowHeader.getHeaderTable().changeSelection(rowIndex, 0, false, false);
					else
					if (event instanceof KeyEvent) {
						final KeyEvent ke = (KeyEvent)event;
						if(!ke.isShiftDown() && !ke.isControlDown()) {
							((FixedColumnRowHeader.HeaderTable)rowHeader.getHeaderTable()).changeSelection(rowIndex, 0, false, false, true);
						} else {
							((FixedColumnRowHeader.HeaderTable)rowHeader.getHeaderTable()).changeSelection(
									rowIndex, rowHeader.getHeaderTable().getColumnCount() - 1, false, false, true);
						}
					}
					else
						((FixedColumnRowHeader.HeaderTable)rowHeader.getHeaderTable()).changeSelection(rowIndex, 0, false, false, true);
					return;
				}
			}

			final int row = rowIndex;
			final boolean bHasNextComponent = sNextColumn != null;
			if (isCellEditable(rowIndex, columnIndex)) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						int rIndex = row;
						if (event instanceof KeyEvent) {
							final KeyEvent ke = (KeyEvent)event;
							if(!ke.isShiftDown() && !ke.isControlDown()) {
									/*if (row == 0 && columnIndex == 0
											&& !fixed)
										return;*/
							} else {
								if (getColumnCount() -1 == columnIndex && bHasNextComponent) {
									rIndex = row - 1 == -1 ? 0 : row - 1;
								}
							}
						} else {
								/*if (row == 0 && columnIndex == 0
										&& !fixed)
									return;*/
						}
						SubFormTable.super.changeSelection(rIndex, columnIndex, false, false);
						editCellAt(rIndex, columnIndex, null);
					}
				});
			}
			else {
				final int rowCol[] = getNextEditableCell(this, rowIndex, columnIndex, bShift);
				if (isCellEditable(rowCol[0], rowCol[1])) {
					if(!fixed && event instanceof KeyEvent && ((KeyEvent)event).isConsumed())
						return;
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							if (editCellAt(rowCol[0], rowCol[1])) {
								Component editor = getEditorComponent();
								if(editor != null) {
									editor.requestFocusInWindow();
									if(rowCol[0] < getRowCount())
										changeSelection(rowCol[0], rowCol[1], false, false);
								}
							}
						}
					});
				}
				else {
					if(newRowOnNext) {
						for(FocusActionListener fal : subform.getFocusActionLister()) {
							fal.focusAction(new EventObject(this));
							final int col[] = getNextEditableCell(this, rowIndex + 1, 0, bShift);
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									if (editCellAt(col[0], col[1])) {
										Component editor = getEditorComponent();
										if(editor != null)
											editor.requestFocusInWindow();
										changeSelection(col[0], col[1], false, false);
									}
									else if (col[0] >= getRowCount()){
										changeSelection(0, col[1], false, false);
									}
								}
							});
						}
						newRowOnNext = false;
					} else if (event instanceof KeyEvent && ((KeyEvent) event).getKeyCode() == KeyEvent.VK_TAB){
						changeSelection(rowIndex, columnIndex + 1, toggle, extend);
					}
				}
			}
		}
	}

	private int[] getNextEditableCell(JTable table, int row, int col, boolean bReverse) {
		int rowCol[] = {row,col};
		int colCount = getColumnCount();
		boolean colFound = false;
		if (!bReverse) {
			for(int i = col; i < colCount; i++) {
				if(table.isCellEditable(row, i)) {
					colFound = true;
					rowCol[1] = i;
					break;
				}
			}
			if(!colFound) {
				row++;
				if(row >= getRowCount())
					return rowCol;
				for(int i = 0; i < col; i++) {
					if(table.isCellEditable(row, i)) {
						rowCol[0] = row;
						rowCol[1] = i;
						break;
					}
				}
			}
		} else {
			for(int i = col; i >= 0; i--) {
				if(table.isCellEditable(row, i)) {
					colFound = true;
					rowCol[1] = i;
					break;
				}
			}
			if(!colFound) {
				row--;
				if(row <= 0)
					return rowCol;
				for(int i = colCount - 1; i >= 0; i--) {
					if(table.isCellEditable(row, i)) {
						rowCol[0] = row;
						rowCol[1] = i;
						break;
					}
				}
			}
		}
		return rowCol;
	}

	@Override
	protected SubformTableColumnModel createDefaultColumnModel() {
		return new SubformTableColumnModel(getModel());
	}

	void setRowHeaderTable(SubformRowHeader rowheader) {
		this.rowheader = rowheader;
		if (!(rowheader instanceof FixedColumnRowHeader)) { // NUCLOSINT-491
			// NUCLEUSINT-299: focus mysteriously remained in the
			// toolbar or wherever. Force to the table, when the
			// row header gets a mouse-press.
			rowheader.getHeaderTable().addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					requestFocusInWindow();
				}
			});
		}
	}

	public final SubFormTableModel getSubFormModel() {
		return (SubFormTableModel) super.getModel();
	}

	@Override
	public void setBackground(Color color) {
		super.setBackground(color);
		if (rowheader != null && rowheader.getHeaderTable() != null) {
			this.rowheader.getHeaderTable().setBackground(color);
			final Container parent = rowheader.getHeaderTable().getParent();
			if (parent instanceof JViewport) {
				parent.setBackground(color);
			}
		}
	}

	@Override
	public void setTableHeader(JTableHeader tblheader) {
		super.setTableHeader(tblheader);
		configureEnclosingScrollPane();
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		if ((row > -1 && row < getRowCount()) &&
				(column > -1 && column < getColumnCount())) {
			return super.isCellEditable(row, column);
		}
		return false;
	}

	@Override
	public TableCellRenderer getCellRenderer(int iRow, int iColumn) {
		TableCellRenderer result = null;

		if (cellrendererprovider != null && getModel() instanceof SubFormTableModel) {
			final int iModelColumn = getColumnModel().getColumn(iColumn).getModelIndex();
			final CollectableEntityField clctefTarget = ((SubFormTableModel) getModel()).getCollectableEntityField(iModelColumn);
			result = cellrendererprovider.getTableCellRenderer(clctefTarget);
		}
		if (result == null) {
			result = super.getCellRenderer(iRow, iColumn);
		}
		if (subform != null && result instanceof DynamicRowHeightSupport) {
			result = new DynamicRowHeightCellRenderer(
					result,
					(DynamicRowHeightSupport) result,
					iColumn,
					subform.getRowHeightCtrl());
		}
		if (result instanceof TableCellCursor) {
			if (subform != null) {
				((TableCellCursor) result).deriveFont(subform.getFont().getSize2D());
			}
			return result;
		} else {
			final TableCellRenderer cellRender = result;
			return new TableCellRenderer() {
				@Override
				public Component getTableCellRendererComponent(JTable table, Object value,
															   boolean isSelected, boolean hasFocus, int row, int column) {
					Component c = cellRender.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
					if (c != null && c.getFont() != null && subform != null) {
						c.setFont(c.getFont().deriveFont(subform.getFont().getSize2D()));
					}

					if (getSubForm() != null && getSubForm().getSubformHistoricalViewLookUp() != null) {
						if (getSubForm().getSubformHistoricalViewLookUp().inHistoricalView(getSubForm().getEntityUID(), getSubForm().getForeignKeyFieldToParent(),
								getSubFormModel().getColumnFieldUid(column), row)) {
							c.setBackground(SubForm.colorCommonValues);
						}
					}

					if (!"false".equals(ClientParameterProvider.getInstance().getValue("DISPLAY_REFERENCE_FIELDS_ITALIC")) && c instanceof JLabel && getSubFormModel().getCollectableEntityField(convertColumnIndexToModel(column)).isReferencing()) {
						JLabel label = (JLabel)c;
						label.setFont(getFont().deriveFont(Font.ITALIC));
//							if (!StringUtils.isNullOrEmpty(label.getText())) {
//								TableCellEditor tce = SubFormTable.this.getCellEditor(row, column);
//								CollectableComponent clctcmp = null;
//								if(tce instanceof CollectableComponentTableCellEditor) {
//									clctcmp = ((CollectableComponentTableCellEditor) tce).getCollectableComponent();
//								}
//								Icon icon = null;
//								if (clctcmp instanceof NuclosCollectableComboBox) {
//									icon = Icons.getInstance().getIconTextFieldButtonCombobox();
//								} else if (clctcmp instanceof NuclosCollectableListOfValues) {
//									icon = Icons.getInstance().getIconTextFieldButtonLOV();
//								}
//								int textWidth = getFontMetrics(getFont()).stringWidth(label.getText());
//								Insets insets = getInsets();
//								int iconTextGap = 0 - SubFormTable.this.getColumnModel().getColumn(column).getWidth() + insets.right;
//								label.setIcon(icon);
//								label.setIconTextGap(iconTextGap);
//								label.setFont(getFont().deriveFont(Font.ITALIC));
//							} else {
//								label.setIcon(null);
//								label.setIconTextGap(0);
//							}
					}

					return c;
				}
			};
		}
	}

	private TableCellRenderer getCellRendererFromClassField(CollectableEntityField entityField, UID classFieldname, int iRow) {
		int typeId = CollectableComponentTypes.TYPE_TEXTFIELD;
		Object oValue = getSubFormModel().getValueAt(iRow, getSubFormModel().findColumnByFieldUid(classFieldname));
		try {
			typeId = CollectableUtils.getCollectableComponentTypeForClass(Class.forName(oValue.toString()));
			LangUtils.getClassLoaderThatWorksForWebStart().loadClass(oValue.toString());
		}
		catch(ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
		CollectableComponent comp = CollectableComponentFactory.getInstance().newCollectableComponent(
				entityField, new CollectableComponentType(typeId,null), false);
		return comp.getTableCellRenderer(true);
	}

	TableCellEditorProvider getTableCellEditorProvider() {
		return this.celleditorprovider;
	}

	TableCellRendererProvider getTableCellRendererProvider() {
		return this.cellrendererprovider;
	}

	void setTableCellEditorProvider(TableCellEditorProvider celleditorprovider) {
		this.celleditorprovider = celleditorprovider;
	}

	public void setTableCellRendererProvider(TableCellRendererProvider cellrendererprovider) {
		this.cellrendererprovider = cellrendererprovider;
	}

	TableCellEditor getCellEditorContinueEditing(int iRow, CollectableEntityField clctefTarget) {
		return getCellEditor(iRow, clctefTarget, true);
	}

	TableCellEditor getCellEditor(int iRow, CollectableEntityField clctefTarget) {
		return getCellEditor(iRow, clctefTarget, false);
	}

	TableCellEditor getCellEditor(int iRow, CollectableEntityField clctefTarget, boolean continueEditing) {
		TableCellEditor result = null;
		if (celleditorprovider != null && getModel() instanceof SubFormTableModel) {
			try {
				if (continueEditing) {
					result = celleditorprovider.getTableCellEditorContinueEditing(this, iRow, clctefTarget);
				} else {
					result = celleditorprovider.getTableCellEditorContinueEditing(this, iRow, clctefTarget);
				}
			}
			catch(NuclosFieldNotInModelException e) {
				// expected exception
				LOG.info("getCellEditor: " + e);
				result = null;
			}
		}
		if (result == null) {
			result = getCellEditor(iRow, ((SubFormTableModel) getModel()).getColumn(clctefTarget));
		}
		if (result instanceof CollectableComponentTableCellEditor) {
			final CollectableComponentTableCellEditor cellEditor = (CollectableComponentTableCellEditor) result;
			return new CollectableComponentTableCellEditor(cellEditor.getCollectableComponent(), cellEditor.isSearchable()) {

				@Override
				public boolean stopCellEditing() {
					return cellEditor.stopCellEditing();
				}

				@Override
				public boolean shouldSelectCell(EventObject anEvent) {
					return cellEditor.shouldSelectCell(anEvent);
				}

				@Override
				public void removeCellEditorListener(CellEditorListener l) {
					cellEditor.removeCellEditorListener(l);
				}

				@Override
				public boolean isCellEditable(EventObject anEvent) {
					return cellEditor.isCellEditable(anEvent);
				}

				@Override
				public Object getCellEditorValue() {
					return cellEditor.getCellEditorValue();
				}

				@Override
				public void cancelCellEditing() {
					cellEditor.cancelCellEditing();
				}

				@Override
				public void addCellEditorListener(CellEditorListener l) {
					cellEditor.addCellEditorListener(l);
				}

				@Override
				public CollectableComponent getCollectableComponent() {
					return cellEditor.getCollectableComponent();
				}

				@Override
				public CellEditorListener[] getCellEditorListeners() {
					return cellEditor.getCellEditorListeners();
				}

				@Override
				public void collectableFieldChangedInModel(
						CollectableComponentModelEvent ev) {
					cellEditor.collectableFieldChangedInModel(ev);
				}

				@Override
				public void addCollectableComponentModelListener(IReferenceHolder outer, CollectableComponentModelListener listener) {
					cellEditor.addCollectableComponentModelListener(outer, listener);
				}

				@Override
				public int getLastEditingRow() {
					return cellEditor.getLastEditingRow();
				}

				@Override
				public boolean isSearchable() {
					return cellEditor.isSearchable();
				}

				@Override
				public void removeCollectableComponentModelListener(CollectableComponentModelListener listener) {
					cellEditor.removeCollectableComponentModelListener(listener);
				}

				@Override
				public void searchConditionChangedInModel(
						SearchComponentModelEvent ev) {
					cellEditor.searchConditionChangedInModel(ev);
				}

				@Override
				public void valueToBeChanged(DetailsComponentModelEvent ev) {
					cellEditor.valueToBeChanged(ev);
				}

				@Override
				public Component getTableCellEditorComponent(JTable table, Object value,
															 boolean isSelected, int row, int column) {
					boolean bWasDetailsChangeIgnored = subform.isDetailsChangedIgnored();
					subform.setDetailsChangedIgnored(true);
					Component c = cellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
					if (c != null && c.getFont() != null && subform != null)
						c.setFont(c.getFont().deriveFont(subform.getFont().getSize2D()));
					subform.setDetailsChangedIgnored(bWasDetailsChangeIgnored);
					return c;
				}
			};
		}
		final TableCellEditor cellEditor = result;
		return new TableCellEditor() {

			@Override
			public boolean stopCellEditing() {
				return cellEditor.stopCellEditing();
			}

			@Override
			public boolean shouldSelectCell(EventObject anEvent) {
				return cellEditor.shouldSelectCell(anEvent);
			}

			@Override
			public void removeCellEditorListener(CellEditorListener l) {
				cellEditor.removeCellEditorListener(l);
			}

			@Override
			public boolean isCellEditable(EventObject anEvent) {
				return cellEditor.isCellEditable(anEvent);
			}

			@Override
			public Object getCellEditorValue() {
				return cellEditor.getCellEditorValue();
			}

			@Override
			public void cancelCellEditing() {
				cellEditor.cancelCellEditing();
			}

			@Override
			public void addCellEditorListener(CellEditorListener l) {
				cellEditor.addCellEditorListener(l);
			}

			@Override
			public Component getTableCellEditorComponent(JTable table, Object value,
														 boolean isSelected, int row, int column) {
				boolean bWasDetailsChangeIgnored = subform.isDetailsChangedIgnored();
				subform.setDetailsChangedIgnored(true);
				Component c = cellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
				if (c != null && c.getFont() != null && subform != null)
					c.setFont(c.getFont().deriveFont(subform.getFont().getSize2D()));
				subform.setDetailsChangedIgnored(bWasDetailsChangeIgnored);
				return c;
			}
		};
	}

	TableCellEditor getCellEditorContinueEditing(int iRow, int iColumn) {
		return this.getCellEditor(iRow, iColumn, true);
	}

	@Override
	public TableCellEditor getCellEditor(int iRow, int iColumn) {
		return this.getCellEditor(iRow, iColumn, false);
	}

	public TableCellEditor getCellEditor(int iRow, int iColumn, boolean continueEditing) {
		TableCellEditor result = null;
		if (celleditorprovider != null && getModel() instanceof SubFormTableModel) {
			final int iModelColumn = getColumnModel().getColumn(iColumn).getModelIndex();
			final CollectableEntityField clctefTarget = ((SubFormTableModel) getModel()).getCollectableEntityField(iModelColumn);
			try {
				if (!continueEditing) {
					result = celleditorprovider.getTableCellEditor(this, iRow, clctefTarget);
				} else {
					result = celleditorprovider.getTableCellEditorContinueEditing(this, iRow, clctefTarget);
				}
			}
			catch(NuclosFieldNotInModelException e) {
				// expected exception
				LOG.info("getCellEditor: " + e);
				result = null;
			}
		}
		if (result == null) {
			result = super.getCellEditor(iRow, iColumn);
		}
		if (result instanceof CollectableComponentTableCellEditor) {
			final CollectableComponentTableCellEditor cellEditor = (CollectableComponentTableCellEditor) result;
			return new CollectableComponentTableCellEditor(cellEditor.getCollectableComponent(), cellEditor.isSearchable()) {

				@Override
				public boolean stopCellEditing() {
				 	return cellEditor.stopCellEditing();
				}

				@Override
				public boolean shouldSelectCell(EventObject anEvent) {
					return cellEditor.shouldSelectCell(anEvent);
				}

				@Override
				public void removeCellEditorListener(CellEditorListener l) {
					cellEditor.removeCellEditorListener(l);
				}

				@Override
				public boolean isCellEditable(EventObject anEvent) {
					return cellEditor.isCellEditable(anEvent);
				}

				@Override
				public Object getCellEditorValue() {
					return cellEditor.getCellEditorValue();
				}

				@Override
				public void cancelCellEditing() {
					cellEditor.cancelCellEditing();
				}

				@Override
				public void addCellEditorListener(CellEditorListener l) {
					cellEditor.addCellEditorListener(l);
				}

				@Override
				public CollectableComponent getCollectableComponent() {
					return cellEditor.getCollectableComponent();
				}

				@Override
				public CellEditorListener[] getCellEditorListeners() {
					return cellEditor.getCellEditorListeners();
				}

				@Override
				public void collectableFieldChangedInModel(
						CollectableComponentModelEvent ev) {
					cellEditor.collectableFieldChangedInModel(ev);
				}

				@Override
				public void addCollectableComponentModelListener(IReferenceHolder outer, CollectableComponentModelListener listener) {
					cellEditor.addCollectableComponentModelListener(outer, listener);
				}

				@Override
				public int getLastEditingRow() {
					return cellEditor.getLastEditingRow();
				}

				@Override
				public boolean isSearchable() {
					return cellEditor.isSearchable();
				}

				@Override
				public void removeCollectableComponentModelListener(
						CollectableComponentModelListener listener) {
					cellEditor.removeCollectableComponentModelListener(listener);
				}

				@Override
				public void searchConditionChangedInModel(
						SearchComponentModelEvent ev) {
					cellEditor.searchConditionChangedInModel(ev);
				}

				@Override
				public void valueToBeChanged(DetailsComponentModelEvent ev) {
					cellEditor.valueToBeChanged(ev);
				}

				@Override
				public Component getTableCellEditorComponent(JTable table, Object value,
															 boolean isSelected, int row, int column) {
					boolean bWasDetailsChangeIgnored = subform.isDetailsChangedIgnored();
					subform.setDetailsChangedIgnored(true);
					Component c = cellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);

					if (c != null && c.getFont() != null && subform != null)
						c.setFont(c.getFont().deriveFont(subform.getFont().getSize2D()));
					subform.setDetailsChangedIgnored(bWasDetailsChangeIgnored);
					return c;
				}
			};
		}
		final TableCellEditor cellEditor = result;
		return new TableCellEditor() {

			@Override
			public boolean stopCellEditing() {
				return cellEditor.stopCellEditing();
			}

			@Override
			public boolean shouldSelectCell(EventObject anEvent) {
				return cellEditor.shouldSelectCell(anEvent);
			}

			@Override
			public void removeCellEditorListener(CellEditorListener l) {
				cellEditor.removeCellEditorListener(l);
			}

			@Override
			public boolean isCellEditable(EventObject anEvent) {
				return cellEditor.isCellEditable(anEvent);
			}

			@Override
			public Object getCellEditorValue() {
				return cellEditor.getCellEditorValue();
			}

			@Override
			public void cancelCellEditing() {
				cellEditor.cancelCellEditing();
			}

			@Override
			public void addCellEditorListener(CellEditorListener l) {
				cellEditor.addCellEditorListener(l);
			}

			@Override
			public Component getTableCellEditorComponent(JTable table, Object value,
														 boolean isSelected, int row, int column) {
				Component c = cellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
				if (c != null && c.getFont() != null && subform != null)
					c.setFont(c.getFont().deriveFont(subform.getFont().getSize2D()));
				return c;
			}
		};
	}

	/**
	 * ensures that this table always fills its enclosing viewport (if any) vertically.
	 * This is useful default behavior (and thus should be defined in JTable already) because of the following reasons:
	 * <ul>
	 * 	<li>allows dropping into the empty space of a table.</li>
	 * 	<li>allows opening a popup menu in the empty space of a table.</li>
	 * 	<li>paints the background color of the table in the empty space, rather than that of the viewport.</li>
	 * </ul>
	 * The alternative each time is to define a transfer handler, a popup menu listener and the background of the enclosing
	 * viewport separately, but that is somewhat awkward.
	 * Code copied from JList.
	 *
	 * §todo if this behavior is needed in other places (which it is likely), move to CommonJTable (as optional behavior)
	 */
	@Override
	public boolean getScrollableTracksViewportHeight() {
		if (getParent() instanceof JViewport) try {
			return (getParent().getHeight() > getPreferredSize().height);
		} catch (NoSuchElementException nsee) {
			//ignore
		}
		return false;
	}

	@Override
	public void setModel(TableModel dataModel) {
		super.setModel(dataModel);
		setRowSorter(new SubFormTableRowSorter(dataModel));
	}

	@Override
	public void setColumnModel(TableColumnModel columnModel) {
		final TableColumnModel old = getColumnModel();
		if (old instanceof SubformTableColumnModel) {
			((SubformTableColumnModel) old).close();
		}

		super.setColumnModel(columnModel);
	}

	void setRowHeightStrict(int iRow, int iHeight) {
		setRowHeight(iRow, iHeight+getRowMargin());
	}

	@Override
	public void setRowHeight(int row, int rowHeight) {
		super.setRowHeight(row, rowHeight);
		if (rowheader != null)
			rowheader.setRowHeightInRow(row, rowHeight);
	}

	@Override
	public void setRowHeight(int rowHeight) {
		super.setRowHeight(rowHeight);
		if (rowheader != null)
			rowheader.setRowHeight(rowHeight);
	}

	@Override
	public void columnAdded(TableColumnModelEvent e) {
		super.columnAdded(e);
		if (subform != null) {
			subform.getRowHeightCtrl().clear();
			invalidateRowHeights();
		}
	}

	@Override
	public void columnRemoved(TableColumnModelEvent e) {
		super.columnRemoved(e);
		if (subform != null) {
			subform.getRowHeightCtrl().clear();
			invalidateRowHeights();
		}
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		super.tableChanged(e);

		if (subform != null && e.getFirstRow() != TableModelEvent.HEADER_ROW) {
			if (e.getFirstRow() == 0 && e.getLastRow() == Integer.MAX_VALUE) {
				subform.getRowHeightCtrl().clear();
			} else {
				for (int iRow = e.getFirstRow(); iRow <= e.getLastRow(); iRow++) {
					if (e.getColumn() == TableModelEvent.ALL_COLUMNS) {
						subform.getRowHeightCtrl().clear(iRow);
					} else {
						subform.getRowHeightCtrl().clear(e.getColumn(), iRow);
					}
				}
			}
		}
		invalidateRowHeights();
	}

	private void invalidateRowHeights() {
		if (subform != null && !invalidateRowHeights) {
			invalidateRowHeights = true;
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					updateRowHeights();
					invalidateRowHeights = false;
				}
			});
		}
	}

	@Override
	public boolean editCellAt(int row, int column, EventObject e) {
		final boolean result = super.editCellAt(row, column, e);
		if (result) {
			Component editor = getEditorComponent();
			if(editor != null)
				editor.requestFocusInWindow();
		}

		return result;
	}

	int getRowHeightWithMargin(int row) {
		return getRowHeight(row)+getRowMargin();
	}

	@Override
	public void addRef(EventListener o) {
		refs.add(o);
	}

}
