//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.eo.component;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.text.Document;

import org.nuclos.client.ui.eo.mvc.IModelAwareGuiComponent;
import org.nuclos.client.ui.eo.mvc.ITypeConverter;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonValidationException;

public class EoValueJTextField<PK> extends JTextField implements IModelAwareGuiComponent<PK> {
	
	private final FieldMeta<?> fieldMeta;
	
	private final Color defaultBackground;
	
	private ITypeConverter converter;
	
	private EntityObjectVO<PK> associatedEo;
	
	public EoValueJTextField(TypeConverterFactory tcFactory, int columns, FieldMeta<?> fieldMeta) {
		super(new SwitchableNotificationDocument(), null, columns);
		
		this.fieldMeta = fieldMeta;
		this.converter = tcFactory.getTypeConverterForField(fieldMeta.getUID());
		this.defaultBackground = getBackground();
		
		addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent ev) {
				try {
					setNotifyDocumentListeners(false);
					setText((String) converter.fromModelToComponent(
							converter.fromComponentToModel(getText())));
					setBackground(defaultBackground);
				} catch (CommonValidationException e) {
					// leave given text alone
					setBackground(Color.RED);
				} catch (ParseException e) {
					// leave given text alone
					setBackground(Color.RED);
				} finally {
					setNotifyDocumentListeners(true);
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// do nothing
			}
		});
	}

	@Override
	public void fromComponentToModel() throws CommonValidationException, ParseException {
		String guiValue = getText();
		if (guiValue != null) {
			guiValue = guiValue.trim();
			if (guiValue.equals("")) {
				guiValue = null;
			}
		}
		final Object old = associatedEo.getFieldValue(fieldMeta.getUID());
		final Object converted = converter.fromComponentToModel(guiValue);
		if (!LangUtils.equal(old, converted)) {
			associatedEo.setFieldValue(fieldMeta.getUID(), converted);
			if (!associatedEo.isFlagNew() && !associatedEo.isFlagRemoved()) {
				associatedEo.flagUpdate();
			}
		}
	}

	@Override
	public void fromModelToComponent() {
		String modelFieldValue = null;
		if (associatedEo != null) {
			final Object value = converter.fromModelToComponent(
					associatedEo.getFieldValue(fieldMeta.getUID()));
			if (value == null) {
				modelFieldValue = null;
			} else {
				final Class<?> vclazz = value.getClass();
				if (String.class.isAssignableFrom(vclazz)) {
					modelFieldValue = (String) value;
				} else {
					// general toString support
					modelFieldValue = value.toString();
				}
			}
		}
		if (modelFieldValue == null) {
			modelFieldValue = "";
		}
		setText(modelFieldValue);
	}

	@Override
	public void setAssociatedEntityObject(EntityObjectVO<PK> eo) {
		this.associatedEo = eo;
	}

	@Override
	public EntityObjectVO<PK> getAssociatedEntityObject() {
		return associatedEo;
	}
	
	public void setConverter(ITypeConverter converter) {
		this.converter = converter;
	}
	
	public ITypeConverter getConverter() {
		return converter;
	}
	
	public void setNotifyDocumentListeners(boolean notifyListeners) {
		final Document doc = getDocument();
		if (doc instanceof SwitchableNotificationDocument) {
			final SwitchableNotificationDocument snd = (SwitchableNotificationDocument) doc;
			snd.setNotifyListeners(notifyListeners);
		}
	}

}
