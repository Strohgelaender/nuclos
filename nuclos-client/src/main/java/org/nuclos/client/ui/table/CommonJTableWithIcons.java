package org.nuclos.client.ui.table;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

@SuppressWarnings("serial")
public class CommonJTableWithIcons extends CommonJTable {
	
	private static final TableCellRenderer ICONRENDERER = new DefaultTableCellRenderer() {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {
			
			if (value instanceof byte[]) {
				JLabel l = new JLabel(new ImageIcon((byte[])value));
				if (isSelected) {
					l.setOpaque(true);
					l.setBackground(table.getSelectionBackground());

				}
				return l;				
			}
			
			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		}
	};

	@Override
	public TableCellRenderer getDefaultRenderer(Class<?> clsColumn) {
		if (clsColumn == byte[].class) {
			return ICONRENDERER;
		}
		return super.getDefaultRenderer(clsColumn);
	}

}
