//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.result;

import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Closeable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.EventListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Future;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.ui.ResultToolbarItem;
import org.nuclos.api.ui.ResultToolbarItemContext;
import org.nuclos.api.ui.ResultToolbarItemFactory;
import org.nuclos.client.common.EnabledListener;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.TimedPreferencesUpdater;
import org.nuclos.client.common.WorkspaceUtils;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.client.genericobject.RestoreSelectedCollectablesController;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.nuclet.NucletComponentRepository;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateModel;
import org.nuclos.client.ui.collect.CollectableTableHelper;
import org.nuclos.client.ui.collect.DeleteSelectedCollectablesController;
import org.nuclos.client.ui.collect.SelectColumnsController;
import org.nuclos.client.ui.collect.ToolTipsTableHeader;
import org.nuclos.client.ui.collect.component.model.ChoiceEntityFieldList;
import org.nuclos.client.ui.collect.model.CollectableTableModel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.toolbar.DeleteOrRestoreToggleAction;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.client.ui.gc.IReferenceHolder;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.table.SortableTableModel;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldUtils;
import org.nuclos.common.ProfileUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.PredicateUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.genericobject.GenericObjectUtils;
import org.nuclos.common.preferences.PreferencesProvider;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.AbstractProxyList;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller for the Result panel.
 * <p>
 * This implementation is to use with {@link CollectController}s for which
 * no subtype is defined.
 * </p><p>
 * The ResultController is used for displaying a search result on
 * ResultPanel. The result shown are (list) representations of
 * (DB) persisted entities.
 * </p><p>
 * Since Nuclos 3.1.01 ResultController is a Controller hierarchy of its own.
 * The main intention of this is to remove as many methods away from
 * CollectController as possible. In principle, the ResultController hierarchy
 * should mimic the hierarchy of CollectControllers. In practice, subtypes of
 * ResultController are only implemented in case they change the behaviour
 * defined here.
 * </p>
 * @see ResultPanel
 * @since Nuclos 3.1.01 this is a top-level class.
 * @author Thomas Pasch
 */
public class ResultController<PK,Clct extends Collectable<PK>> implements IReferenceHolder, Closeable {

	private static final Logger LOG = LoggerFactory.getLogger(ResultController.class);
	
	private static final String RESULT_ACTIONS_VISIBLE = "resultactionsvisible";
	
	static final int ESC = KeyEvent.VK_ESCAPE;
	
	static final int UP = KeyEvent.VK_UP;
	
	static final int DOWN = KeyEvent.VK_DOWN;

	/**
	 * The entity for which the results are displayed.
	 *
	 * @since Nuclos 3.1.01
	 * @author Thomas Pasch
	 */
	private final CollectableEntity clcte;

	/**
	 * The SearchResultStrategy for the result displayed.
	 *
	 * @since Nuclos 3.1.01
	 * @author Thomas Pasch
	 */
	private final ISearchResultStrategy<PK,Clct> srs;
	private final CollectableFieldsProviderFactory clctfproviderfactory;

	/**
	 * TODO: Try to avoid cyclic dependency: The ResultController shouldn't depend on the CollectController.
	 * 		While this would be desirable, it is - in real - completely unrealistic at present. Even
	 * 		more, a Result is always the result (pun intended!) of some other (controller) operation!
	 * 		(Thomas Pasch)
	 */
	private CollectController<PK,Clct> clctctl;

	/**
	 * the lists of available and selected fields, resp.
	 * 
	 * Not final because must be reset in close() to avoid memory leak. (tp)
	 */
	private ChoiceEntityFieldList fields = new ChoiceEntityFieldList(null);

	/**
	 * action: Edit selected Collectable (in Result panel)
	 */
	private Action actEditSelectedCollectables;

	/**
	 * action: Delete selected Collectable (in Result panel)
	 */
	private final DeleteOrRestoreToggleAction actDeleteSelectedCollectables;
	
	private final Action actRefresh;
	
	private Collection<JButton> customButtons = new ArrayList<>();
	
	private MouseListener mouselistenerTableDblClick;

	protected boolean isIgnorePreferencesUpdate = true;
	
	private final EnabledListener actionsVisibleListener = new EnabledListener() {
		@Override
		public void enabledChanged(boolean visible) {
			getCollectController().getPreferences().putBoolean(RESULT_ACTIONS_VISIBLE, visible);
		}
	};
	
	private final List<EventListener> refs = new LinkedList<EventListener>();
	
	private final Map<UID, FieldMeta<?>> mpEntityFields;

	private final TablePreferencesManager tblprefManager;

	/**
	 * Don't make this public!
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	@Deprecated
	ResultController(CollectController<PK,Clct> clctctl, CollectableEntity clcte, ISearchResultStrategy<PK,Clct> srs) {
		this(clcte, srs);
		setCollectController(clctctl);
	}

	/**
	 * @deprecated You should really provide a CollectableEntity here.
	 */
	@Deprecated
	public ResultController(UID entityUid, ISearchResultStrategy<PK,Clct> srs) {
		this(NuclosCollectableEntityProvider.getInstance().getCollectableEntity(entityUid), srs);
	}

	public ResultController(CollectableEntity clcte, ISearchResultStrategy<PK,Clct> parsrs) {
		mpEntityFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(clcte.getUID());
		tblprefManager = PreferencesProvider.getInstance().getTablePreferencesManagerForEntity(clcte.getUID(),
				Main.getInstance().getMainController().getUserName(),
				ClientMandatorContext.getMandatorUID());

		this.clctfproviderfactory = MasterDataCollectableFieldsProviderFactory.newFactory(clcte.getUID());

		this.srs = parsrs;
		srs.setResultController(this);
		actDeleteSelectedCollectables = NuclosToolBarActions.MULTI_DELETE.init(new DeleteOrRestoreToggleAction() {

			@Override
	        public void actionPerformed(ActionEvent ev) {
				if (DeleteOrRestoreToggleAction.Mode.RESTORE == getMode()) {
					cmdRestoreSelectedCollectables();
				} else {
					cmdDeleteSelectedCollectables();
				}
			}
		});
		this.clcte = clcte;
		actRefresh = NuclosToolBarActions.REFRESH_LIST.init(new CommonAbstractAction() {

			@Override
            public void actionPerformed(ActionEvent ev) {
				srs.cmdRefreshResult();
			}
		});
	}

	public final TablePreferencesManager getTablePreferencesManager() {
		return tblprefManager;
	}

	@Override
	public void addRef(EventListener o) {
		refs.add(o);
	}
	
	protected SpringLocaleDelegate getSpringLocaleDelegate() {
		return SpringLocaleDelegate.getInstance();
	}
	
	protected MainFrame getMainFrame() {
		return Main.getInstance().getMainFrame();
	}
	
	protected WorkspaceUtils getWorkspaceUtils() {
		return WorkspaceUtils.getInstance();
	}

	/**
	 * TODO: Could this be protected?
	 */
	public final ISearchResultStrategy<PK,Clct> getSearchResultStrategy() {
		return srs;
	}

	public final Action getEditSelectedCollectablesAction() {
		return actEditSelectedCollectables;
	}

	public final Action getDeleteSelectedCollectablesAction() {
		return actDeleteSelectedCollectables;
	}
	
	public final Action getRefreshAction() {
		return actRefresh;
	}

	public final MouseListener getTableDblClickML() {
		return mouselistenerTableDblClick;
	}

	public final void setCollectController(CollectController<PK,Clct> controller) {
		this.clctctl = controller;
	}

	protected final CollectController<PK,Clct> getCollectController() {
		return clctctl;
	}

	/**
	 * TODO: Make protected again.
	 */
	public final CollectableEntity getEntity() {
		return clcte;
	}

	protected ResultPanel<PK,Clct> getResultPanel() {
		if (clctctl == null) {
			return null;
		}
		return this.clctctl.getResultPanel();
	}

	private void statusBarTextButtonPushed() {
		Runnable runCount = new Runnable() {
			@Override
			public void run() {
				runCount();
			}
		};
		
		(new Thread(runCount)).start();
	}

	private NuclosBusinessRuleException countException;

	private void runCount() {
		Runnable disableStatusBarTextButton = new Runnable() {
			@Override
			public void run() {
				getResultPanel().getStatusBarTextButton().setEnabled(false);
			}
		};

		Runnable enableStatusBarTextButton = new Runnable() {
			@Override
			public void run() {
				getResultPanel().getStatusBarTextButton().setEnabled(true);
			}
		};

		SwingUtilities.invokeLater(disableStatusBarTextButton);
		try {
			final int count = clctctl.getSearchStrategy().getCollectableProxyList().totalSize(true);
			Runnable setStatusBarText = new Runnable() {
				@Override
				public void run() {
					getResultPanel().setStatusBar(count);
				}
			};
			SwingUtilities.invokeLater(setStatusBarText);
			if (getCollectController() instanceof EntityCollectController) {
				((EntityCollectController) getCollectController()).setPointerInformation(null, null);
			}
		} catch (final Exception e) {
			if (getCollectController() instanceof EntityCollectController) {
				countException = new NuclosBusinessRuleException(new BusinessException(getSpringLocaleDelegate().getMessage("ResultController.17","Es ist zu einer datenbankseitigen Zeitüberschreitung gekommen. Das Zählen der Datensätze wurde abgebrochen!"),e));
				((EntityCollectController) getCollectController()).handlePointerException(countException);
			}
		} finally {
			SwingUtilities.invokeLater(enableStatusBarTextButton);
		}

	}

	/**
	 * TODO: Make this package visible again.
	 */
	public void setupResultPanel() {
		if (E.isNuclosEntity(getEntity().getUID())) {
			this.getResultPanel().setActionsEnabled(false);
		}
		this.getResultPanel().addActionsVisibleListener(actionsVisibleListener);
		
		setupActions();

		// add selection listener for Result table:
		final ResultPanel<PK, Clct> panel = getResultPanel();
		final JTable tblResult = panel.getResultTable();

		tblResult.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblResult.getSelectionModel().addListSelectionListener(newListSelectionListener(tblResult));
		
		panel.getToggleSelectionModeWidget().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final CommonResultState state = getResultPanel().getState();
				state.setToggleSelection(!state.isToggleSelection());
			}
		});
		panel.getStatusBarTextButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				statusBarTextButtonPushed();
			}
		});
		panel.getSelectAllRowsWidget().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int rowCount = tblResult.getRowCount();
				if (rowCount > 0) {
					int maxCount = AbstractProxyList.HUGELISTSIZE;
					if (rowCount > maxCount) {
						String msg = getSpringLocaleDelegate().getMessage("EntityCollectController.Limit",
								"The list is too large for this action. Please limit to a max number of {0}!", maxCount);
						JOptionPane.showMessageDialog(MainController.getMainFrame(), msg);
					} else {
						UIUtils.setWaitCursor();
						tblResult.getSelectionModel().setSelectionInterval(0, rowCount - 1);
						UIUtils.setDefaultCursor();
					}
				}
			}
		});
		panel.getDeselectAllRowsWidget().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tblResult.getSelectionModel().clearSelection();
			}
		});
		panel.addResultKeyListener(new ResultKeyListenerImpl(tblResult, panel.getState()));

		// add mouse listener for double click in table:
		mouselistenerTableDblClick = new MouseAdapter() {
			
			private long lastClick = 0L;
			private int lastSelectedRow = -1;
			
			@Override
			public void mouseClicked(MouseEvent ev) {
				if (SwingUtilities.isLeftMouseButton(ev)) {
					if (lastClick + MainFrameTabbedPane.DOUBLE_CLICK_SPEED > System.currentTimeMillis()) {
						int iRow = tblResult.rowAtPoint(ev.getPoint());
						if (iRow >= 0 && iRow < tblResult.getRowCount() && iRow == lastSelectedRow) {
							tblResult.getSelectionModel().setSelectionInterval(iRow, iRow);
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									if (getSelectedCollectableFromTableModel() != null) {
										clctctl.cmdViewSelectedCollectables();
									}
								}
							});
						}
					} else {
						getResultPanel().appendStatusBarSelectedRowCount(tblResult.getSelectedRowCount());
					}

					lastSelectedRow = tblResult.rowAtPoint(ev.getPoint());
					lastClick = System.currentTimeMillis();
				}
			}
		};
		if (clctctl.getViewMode() == ControllerPresentation.DEFAULT) {
			panel.addDoubleClickMouseListener(this.mouselistenerTableDblClick);
		}
		if (!SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS) &&
				getMainFrame().getWorkspace().isAssigned()) {
			panel.getResultTable().getTableHeader().setReorderingAllowed(false);
		}
		
		// change column ordering in table model when table columns are reordered by dragging a column with the mouse:
		panel.addColumnModelListener(newColumnModelListener());
		final PreferencesUpdateListener pul = newResultTablePreferencesUpdateListener();
		panel.addColumnModelListener(pul);
		
		panel.addPopupMenuListener();
		panel.getSearchFilterBar().addEnabledListener(new ResetMainFilterEnabledListener());
		
		panel.setActionsVisible(getCollectController().getPreferences().getBoolean(RESULT_ACTIONS_VISIBLE, true));
		
		// FIXME is there any need to initialize splitpanes within the result controller?
		//UIUtils.readSplitPaneStateFromPrefs(getCollectController().getPreferences(), getResultPanel());

		getResultPanel().getResultTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setDeleteButtonToggleInResult();
			}
		});
	}

	private void setupActions() {
		final ResultPanel<PK,Clct> pnlResult = this.getResultPanel();

		// action: Refresh (search again)
		
//		pnlResult.btnRefresh.setAction(actRefresh);
		pnlResult.registerToolBarAction(NuclosToolBarItems.REFRESH_LIST, actRefresh);

		// action: New
//		pnlResult.btnNew.setAction(this.clctctl.getNewAction());
		pnlResult.registerToolBarAction(NuclosToolBarItems.NEW, this.clctctl.getNewAction());

		// action: Bookmark
//		pnlResult.btnBookmark.setAction(this.clctctl.getBookmarkAction());
		pnlResult.registerToolBarAction(NuclosToolBarItems.BOOKMARK, this.clctctl.getBookmarkAction());

		// action: Clone
//		pnlResult.btnClone.setAction(this.clctctl.getCloneAction());
		pnlResult.registerToolBarAction(NuclosToolBarItems.CLONE, this.clctctl.getCloneAction());

		// action: Delete
//		pnlResult.btnDelete.setAction(this.actDeleteSelectedCollectables);
		pnlResult.registerToolBarAction(NuclosToolBarItems.MULTI_DELETE, actDeleteSelectedCollectables);
		this.actDeleteSelectedCollectables.setEnabled(false);
		
//		pnlResult.btnResetMainFilter.setAction(this.clctctl.getResetMainFilterAction());
		pnlResult.registerToolBarAction(NuclosToolBarItems.RESET_FILTER, this.clctctl.getResetMainFilterAction());
		this.clctctl.getResetMainFilterAction().setEnabled(false);
//		pnlResult.btnResetMainFilter.setVisible(false);
		this.clctctl.getResetMainFilterAction().putValue(ToolBarItemAction.VISIBLE, Boolean.FALSE);

		// action: View
		this.actEditSelectedCollectables = NuclosToolBarActions.MULTI_EDIT.init(new CommonAbstractAction() {

			@Override
            public void actionPerformed(ActionEvent ev) {
				clctctl.cmdViewSelectedCollectables();
			}
		});

//		pnlResult.btnEdit.setAction(this.actEditSelectedCollectables);
		if (getCollectController().getViewMode() != ControllerPresentation.SPLIT_RESULT) {
			pnlResult.registerToolBarAction(NuclosToolBarItems.MULTI_EDIT, this.actEditSelectedCollectables);
			actEditSelectedCollectables.setEnabled(false);
		}

		pnlResult.registerToolBarAction(NuclosToolBarItems.FILTER, NuclosToolBarActions.FILTER.init(new CommonAbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ResultPanelFilter filter = pnlResult.getResultFilter();
				if (!filter.isFilterSetup()) {
					filter.setupFilter(null, getCollectableFieldsProviderFactory());
				}
				filter.createFilter(filter.isFilterStored());

				if (!filter.getFixedResultFilter().isCollapsed() || !filter.getExternalResultFilter().isCollapsed()) {
					filter.setFilteringActive(false);
					filter.doFiltering();
					filter.getToggleButton().setSelected(false);
				} else {
					filter.setFilteringActive(true);
					filter.doFiltering();

					filter.getToggleButton().setSelected(true);
				}

				filter.getFixedResultFilter().setCollapsed(!filter.getFixedResultFilter().isCollapsed());
				filter.getExternalResultFilter().setCollapsed(!filter.getExternalResultFilter().isCollapsed());
			}
		}));
		
		final SearchField sf = pnlResult.getSearchField();
		final ActionListener refreshListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (pnlResult.hasIncrSearchFieldChanged()) actRefresh.actionPerformed(new ActionEvent(this, 1, "searchFieldChanged"));
			}
		};
		final KeyListener keyListener = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
					final JTable table = getResultPanel().getResultTable();
					table.requestFocusInWindow();
					int i = table.getSelectedRow();
					if (e.getKeyCode() == KeyEvent.VK_DOWN && i < table.getRowCount() - 1) {
						table.setRowSelectionInterval(i + 1, i + 1);
					} else if (e.getKeyCode() == KeyEvent.VK_UP && i > 0) {
						table.setRowSelectionInterval(i - 1, i - 1);
					}
				}
			}
		};
		ListenerUtil.registerActionListener(sf, this, refreshListener);
		ListenerUtil.registerKeyListener(sf, this, keyListener);

		// action: Select Columns
		Action actSelectColumns = NuclosToolBarActions.CONFIG_COLUMNS.init(new AbstractAction() {
			@Override
            public void actionPerformed(ActionEvent ev) {
				cmdSelectColumns(clctctl.getFields());
			}
		});
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS) ||
				(WorkspaceUtils.getInstance() != null && !WorkspaceUtils.getInstance().getWorkspace().isAssigned())) {
//			addPopupExtraMenuItem(btnSelectColumns);
			pnlResult.registerToolBarAction(NuclosToolBarItems.CONFIG_COLUMNS, actSelectColumns);
		}

		if (!clctctl.isTransferable()) {
			pnlResult.getExportWidget().setVisible(false);
			pnlResult.getImportWidget().setVisible(false);
		}
		else {
			// action: Export
			pnlResult.getExportWidget().setAction(new CommonAbstractAction(pnlResult.getExportWidget()) {

				@Override
                public void actionPerformed(ActionEvent ev) {
					pnlResult.cmdExport(clctctl);
				}
			});

			// action: Import
			pnlResult.getImportWidget().setAction(new CommonAbstractAction(pnlResult.getImportWidget()) {

				@Override
                public void actionPerformed(ActionEvent ev) {
					pnlResult.cmdOldXmlImport(clctctl);
				}
			});
		}

		pnlResult.getEditWidget().setAction(actEditSelectedCollectables);
		pnlResult.getCloneWidget().setAction(clctctl.getCloneAction());
		pnlResult.getDeleteWidget().setAction(this.actDeleteSelectedCollectables);
		pnlResult.getOpenInNewTabWidget().setAction(clctctl.getOpenInNewTabAction());
		pnlResult.getBookmarkWidget().setAction(clctctl.getBookmarkAction());
		pnlResult.getCopyCellsWidget().setAction(clctctl.getCopyCellsAction());
		pnlResult.getCopyRowsWidget().setAction(clctctl.getCopyRowsAction());
		
		((NuclosResultPanel)pnlResult).getFixedEditWidget().setAction(actEditSelectedCollectables);
		((NuclosResultPanel)pnlResult).getFixedCloneWidget().setAction(clctctl.getCloneAction());
		((NuclosResultPanel)pnlResult).getFixedDeleteWidget().setAction(this.actDeleteSelectedCollectables);
		((NuclosResultPanel)pnlResult).getFixedOpenInNewTabWidget().setAction(clctctl.getOpenInNewTabAction());
		((NuclosResultPanel)pnlResult).getFixedBookmarkWidget().setAction(clctctl.getBookmarkAction());
		((NuclosResultPanel)pnlResult).getFixedCopyCellsWidget().setAction(clctctl.getCopyCellsAction());
		((NuclosResultPanel)pnlResult).getFixedCopyRowsWidget().setAction(clctctl.getCopyRowsAction());
		
		// Action Show OnlineHelp
		if (!E.isNuclosEntity(clctctl.getEntityUid()))
			pnlResult.registerToolBarAction(NuclosToolBarItems.HELP, clctctl.getOnlineHelpAction());
		
		// add actions defined in extension via ResultToolbarItem
		if (!E.isNuclosEntity(clcte.getUID())) {
			NucletComponentRepository ncr = SpringApplicationContextHolder.getBean(NucletComponentRepository.class);
			List<ResultToolbarItemFactory> resultToolbarItemFactories = ncr.getResultToolbarItemFactory(getEntity().getUID());
			if (resultToolbarItemFactories.size() != 0) {
				pnlResult.getToolBar().addSeparator();
			}
		
			for (final ResultToolbarItemFactory rtif : CollectionUtils.sorted(resultToolbarItemFactories, new Comparator<ResultToolbarItemFactory>() {
	
				@Override
				public int compare(ResultToolbarItemFactory o1,
						ResultToolbarItemFactory o2) {
					return o1.getOrder() < o2.getOrder() ? -1 : o1.getOrder() == o2.getOrder() ? 0 : 1;
				}
			})) {
				CustomButtonAction a = new CustomButtonAction(rtif); 
				JButton btn = pnlResult.getToolBar().add(a);
				customButtons.add(btn);
				btn.setText(rtif.getLabel());
				btn.setIcon(rtif.getIcon());
				
				FontMetrics metrics = btn.getFontMetrics(btn.getFont()); 
			    int width = metrics.stringWidth( btn.getText() ) + (btn.getIcon() != null ? btn.getIcon().getIconWidth() : 0);
			    int height = metrics.getHeight();
			    Dimension newDimension =  new Dimension(width + 20,height+10);
			    btn.setPreferredSize(newDimension);
			    btn.setBounds(new Rectangle(
			    		btn.getLocation(), btn.getPreferredSize()));
			    btn.setHorizontalTextPosition(SwingConstants.RIGHT);
				btn.setToolTipText(rtif.getTooltip());
			}
			updateCustomButtons();
		}
	}
	
	private void updateCustomButtons() {
		for (JButton btn : customButtons) {
			if (btn.getAction() instanceof ResultController.CustomButtonAction) {
				ResultController.CustomButtonAction action = (ResultController.CustomButtonAction)btn.getAction();
				action.setEnabled(action.getItem().isEnabled(action.context.getSelectedResultItems()));
			}
		}
	}
	
	private class ResultToolbarItemContextImpl implements ResultToolbarItemContext {
		
		Future<LayerLock> frameLock;

		@Override
		public Collection<Object> getSelectedResultItems() {
			List<Object> result = new ArrayList<>();
			try {
				for (Clct clct : ResultController.this.getSelectedCollectablesFromTableModel()) {
					if (clct instanceof CollectableMasterData) {
						result.add(((CollectableMasterData)clct).getMasterDataCVO().getEntityObject());
					} else if (clct instanceof CollectableGenericObject) {
						result.add(DalSupportForGO.wrapGenericObjectVO(((CollectableGenericObject)clct).getGenericObjectCVO()));
					}
				}
			} catch (ClassCastException e) {
				//do nothing
			}
			return result;
		}

		@Override
		public Object getSearchCondition() {
			try {
				return ResultController.this.getCollectController().getSearchStrategy().getCollectableSearchCondition();
			} catch (CollectableFieldFormatException e) {
				Errors.getInstance().showExceptionDialog(ResultController.this.getResultPanel(), e);
			}
			return null;
		}

		@Override
		public void setUiLocked(boolean locked) {
			if (locked) {
				frameLock = ResultController.this.getCollectController().lockFrame();
			} else if (frameLock != null){
				ResultController.this.getCollectController().unLockFrame(frameLock);
			}
		}

		@Override
		public void reloadResultItem(Object resultItem) {
			if (resultItem != null) {
				EntityObjectVO eoVo = (EntityObjectVO) resultItem;
				Clct clct = null;
				try {
					clct = ResultController.this.clctctl.findCollectableById(ResultController.this.getEntity().getUID(), (PK)eoVo.getId());
					ResultController.this.replaceCollectableInTableModel(clct);
				} catch (CommonBusinessException e) {
					Errors.getInstance().showExceptionDialog(ResultController.this.getResultPanel(), e);
				}
			}
		}

		@Override
		public void reloadResultList() {
			ResultController.this.srs.cmdRefreshResult();
		}	
	}
	
	private class CustomButtonAction extends AbstractAction {
		
		ResultToolbarItemContext context = new ResultToolbarItemContextImpl();
		ResultToolbarItem item;
		ResultToolbarItemFactory rtif;

		public CustomButtonAction(ResultToolbarItemFactory rtif) {
			item = rtif.newInstance();
			this.rtif = rtif;
		}
		
		public ResultToolbarItem getItem() {
			return item;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			item.action(context);
			for (Object resultItem : context.getSelectedResultItems()) {
				context.reloadResultItem(resultItem);
			}
			updateCustomButtons();
		}

		@Override
		public boolean isEnabled() {
			return enabled && rtif.isAllowed(SecurityCache.getInstance().getCustomActionsAllowed());
		}
	}

	/**
	 * Initializes the <code>fields</code> field as follows:
	 * <ol>
	 *   <li>(re-)set the list of available fields to {@link #getFieldsAvailableForResult}</li>
	 *   <li>(re-)set the list of selected fields to {@link #getSelectedFieldsFromPreferences}</li>
	 *   <li>(re)-set the comparator to {@link #getCollectableEntityFieldComparator()}</li>
	 * </ol>
	 *
	 * TODO: Make protected again.
	 */
	public void initializeFields() {
		final Comparator<CollectableEntityField> comp = new Comparator<CollectableEntityField>() {
			@Override
			public int compare(CollectableEntityField o1, CollectableEntityField o2) {
				int result = StringUtils.compareIgnoreCase(
						SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(o1.getEntityUID())),
								SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(o2.getEntityUID())));
				if (result == 0)
					result = StringUtils.compareIgnoreCase(o1.getLabel(), o2.getLabel());
				return result;
			}
		};
		fields.set(
				getFieldsAvailableForResult(comp),
				new ArrayList<CollectableEntityField>(),
				comp);

		// select the previously selected fields according to user preferences:
		fields.moveToSelectedFields(getSelectedFieldsFromPreferences());
	}

	/**
	 * initializes the <code>fields</code> field, while setting selected columns.
	 */
	public final void initializeFields(final ChoiceEntityFieldList fields, final List<CollectableEntityField> lstSelectedNew)
	{
		fields.setSelectedFields(lstSelectedNew);
	}

	/**
	 * tries to read the selected fields from the preferences, making sure they contain at least one field.
	 * 
	 * §postcondition !result.isEmpty()
	 * 
	 * @return List<CollectableEntityField> the selected fields from the user preferences.
	 */
	private List<CollectableEntityField> getSelectedFieldsFromPreferences() {
		final List<CollectableEntityField> lstSelectedFields = (List<CollectableEntityField>) readSelectedFieldsFromPreferences();
		
		final List<CollectableEntityField> result = new ArrayList<CollectableEntityField>();
		// remove duplicates...
		for (Iterator iterator = lstSelectedFields.iterator(); iterator.hasNext();) {
			final CollectableEntityField clctef = (CollectableEntityField) iterator.next();
			if (hideThisField(clctef.getUID())) {
				continue;
			}
			
			if (!result.contains(clctef)) {
				result.add(clctef);				
			}
		}

		clctctl.makeSureSelectedFieldsAreNonEmpty(clcte, result);
		
		// Here we have at least one field as selected column:
		assert !result.isEmpty();
		return result;
	}

	private ListSelectionListener newListSelectionListener(final JTable tblResult) {
		return new ListSelectionListener() {
			@Override
            public void valueChanged(ListSelectionEvent ev) {
				try {
					final ListSelectionModel lsm = (ListSelectionModel) ev.getSource();

					final CollectStateModel<?,?> clctstatemodel = clctctl.getCollectStateModel();
					if (clctstatemodel.getOuterState() == CollectState.OUTERSTATE_RESULT) {
						final int iResultMode = CollectStateModel.getResultModeFromSelectionModel(lsm);
						if (iResultMode != clctctl.getCollectStateModel().getResultMode()) {
							clctctl.setCollectState(CollectState.OUTERSTATE_RESULT, iResultMode);
						}
					}
					updateCustomButtons();

					if (!ev.getValueIsAdjusting()) {
						// Autoscroll selection. It's okay to do that here rather than in the CollectStateListener.
						if (!lsm.isSelectionEmpty() && tblResult.getAutoscrolls()) {
							// ensure that the last selected row is visible:
							final int iRowIndex = lsm.getLeadSelectionIndex();
							final int iColIndex = tblResult.getSelectedColumn();
							final Rectangle rectCell = tblResult.getCellRect(iRowIndex, iColIndex != -1 ? iColIndex : 0, true);
							if (rectCell != null) {
								tblResult.scrollRectToVisible(rectCell);
							}
						}
					}
				}
				catch (CommonBusinessException ex) {
					Errors.getInstance().showExceptionDialog(clctctl.getTab(), ex);
				}
			}	// valueChanged
		};
	}

	/**
	 * change column ordering in table model when table columns
	 * are reordered by dragging a column with the mouse
	 */
	private TableColumnModelListener newColumnModelListener() {
		return new TableColumnModelListener() {
			@Override
            public void columnMoved(TableColumnModelEvent ev) {
				final int iFromColumn = ev.getFromIndex();
				final int iToColumn = ev.getToIndex();
				if (iFromColumn != iToColumn) {
					//log.debug("column moved from " + iFromColumn + " to " + iToColumn);
					// Sync the "selected fields" with the table column model:
					getResultPanel().columnMovedInHeader(clctctl.getFields());

					// Note that the columns of the result table model are not adjusted here.
					// That means, the table column model and the table model are not 1:1 -
					// use JTable.convertColumnIndexToModel to convert between the two.
				}
			}

			@Override
            public void columnAdded(TableColumnModelEvent ev) {
			}

			@Override
            public void columnRemoved(TableColumnModelEvent ev) {
			}

			@Override
            public void columnMarginChanged(ChangeEvent ev) {
			}

			@Override
            public void columnSelectionChanged(ListSelectionEvent ev) {
			}
		};
	}
	
	protected final PreferencesUpdateListener newResultTablePreferencesUpdateListener() {
		return new PreferencesUpdateListener();
	}

	protected class PreferencesUpdateListener extends TimedPreferencesUpdater implements ChangeListener, TableColumnModelListener  {
		@Override
		public void stateChanged(ChangeEvent ev) {
//			System.out.println("stateChanged " + ev);
			if (!isIgnorePreferencesUpdate) {
				clctctl.writeColumnOrderToPreferences();
			}
		}
		
		@Override
		public void columnSelectionChanged(ListSelectionEvent ev) {
//			System.out.println("columnSelectionChanged " + ev);
			if (!isIgnorePreferencesUpdate) {
//				writeSelectedFieldsAndWidthsToPreferences();
			}
		}
		
		@Override
		public void columnMoved(TableColumnModelEvent ev) {
//			System.out.println("columnMoved " + ev);
			if (!isIgnorePreferencesUpdate) {
				addFieldsAndWidthsWorker();
			}
		}
		
		@Override
		public void columnMarginChanged(ChangeEvent ev) {
//			System.out.println("columnMarginChanged " + ev);
			if (!isIgnorePreferencesUpdate) {
				addFieldsAndWidthsWorker();
			}
		}
		
		@Override
		public void columnAdded(TableColumnModelEvent ev) {
		}
		
		@Override
		public void columnRemoved(TableColumnModelEvent ev) {
		}

		private void addFieldsAndWidthsWorker() {
			super.registerWorker(new PreferencesStoreWorker(getTablePreferencesManager().getSelected().getUID()) {
				@Override
				public void storePreferences() {
					writeSelectedFieldsAndWidthsToPreferences();
				}
			});
		}
	}

	/**
	 * releases the resources (esp. listeners) for this controller.
	 */
	@Override
	public void close() {
		fields = null;
		final ResultPanel<PK,Clct> pnlResult = getResultPanel();
		if (pnlResult != null) {
//			pnlResult.btnRefresh.setAction(null);
//			pnlResult.btnNew.setAction(null);
//			pnlResult.btnBookmark.setAction(null);
//			pnlResult.btnClone.setAction(null);
//			pnlResult.btnDelete.setAction(null);
//			pnlResult.btnEdit.setAction(null);
//			pnlResult.btnSelectColumns.setAction(null);
			pnlResult.getExportWidget().setAction(null);
			pnlResult.getImportWidget().setAction(null);
			pnlResult.getEditWidget().setAction(null);
			pnlResult.getCloneWidget().setAction(null);
			pnlResult.getDeleteWidget().setAction(null);
			pnlResult.getOpenInNewTabWidget().setAction(null);
			pnlResult.getBookmarkWidget().setAction(null);
			pnlResult.removeActionsVisibleListener(actionsVisibleListener);
			UIUtils.removeAllMouseListeners(pnlResult.getResultTable().getTableHeader());
			pnlResult.close();
			mouselistenerTableDblClick = null;
			refs.clear();
			// FIXME is there any need to persist splitpanes within the result controller?
			//UIUtils.writeSplitPaneStateToPrefs(getCollectController().getPreferences(), getResultPanel());
		}
	}

	/**
	 * Command: Delete selected <code>Collectable</code>s in the Result panel.
	 */
	private void cmdDeleteSelectedCollectables() {
		assert clctctl.getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_RESULT;
		assert CollectState.isResultModeSelected(clctctl.getCollectStateModel().getResultMode());

		if (clctctl.multipleCollectablesSelected()) {
			final int iCount = clctctl.getResultTable().getSelectedRowCount();
			final String sMessagePattern = getSpringLocaleDelegate().getMessage(
					"ResultController.13","Sollen die ausgew\u00e4hlten {0} Datens\u00e4tze wirklich gel\u00f6scht werden?");
			final String sMessage = MessageFormat.format(sMessagePattern, iCount);
			final int btn = JOptionPane.showConfirmDialog(clctctl.getTab(), sMessage, getSpringLocaleDelegate().getMessage(
					"ResultController.7","Datens\u00e4tze l\u00f6schen"), JOptionPane.YES_NO_OPTION);
			if (btn == JOptionPane.YES_OPTION) {
				new DeleteSelectedCollectablesController<PK,Clct>(clctctl).run(clctctl.getMultiActionProgressPanel(iCount));
			}
		}
		else {
			final String sMessagePattern = getSpringLocaleDelegate().getMessage(
					"ResultController.12","Soll der ausgew\u00e4hlte Datensatz ({0}) wirklich gel\u00f6scht werden?");
			final String sMessage = MessageFormat.format(sMessagePattern,
					SpringLocaleDelegate.getInstance().getIdentifierLabel(getSelectedCollectableFromTableModel(), getSelectedCollectableFromTableModel().getEntityUID(), MetaProvider.getInstance()));
			final int btn = JOptionPane.showConfirmDialog(clctctl.getTab(), sMessage, getSpringLocaleDelegate().getMessage(
					"ResultController.8","Datensatz l\u00f6schen"), JOptionPane.YES_NO_OPTION);

			if (btn == JOptionPane.YES_OPTION) {
				UIUtils.runCommand(clctctl.getTab(), new Runnable() {
					@Override
                    public void run() {
						try {
							clctctl.checkedDeleteSelectedCollectable();
						}
						catch (CommonPermissionException ex) {
							final String sErrorMsg = getSpringLocaleDelegate().getMessage(
									"ResultController.11","Sie verf\u00fcgen nicht \u00fcber die ausreichenden Rechte, um diesen Datensatz zu l\u00f6schen.");
							Errors.getInstance().showExceptionDialog(clctctl.getTab(), sErrorMsg, ex);
						}
						catch (CommonBusinessException ex) {
							Errors.getInstance().showExceptionDialog(clctctl.getTab(), "Der Datensatz konnte nicht gel\u00f6scht werden.", ex);
						}
					}
				});
			}
		}
	}
	
	private void cmdRestoreSelectedCollectables() {
		assert clctctl.getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_RESULT;
		assert CollectState.isResultModeSelected(clctctl.getCollectStateModel().getResultMode());

		if (clctctl.multipleCollectablesSelected()) {
			final int iCount = clctctl.getResultTable().getSelectedRowCount();
			LOG.debug("restore multiple collectables: {}", StringUtils.join(",", clctctl.getSelectedCollectables()));
			final String sMessagePattern = getSpringLocaleDelegate().getMessage(
					"ResultController.restore.multipleselection.confirm","Sollen die ausgew\u00e4hlten {0} Datens\u00e4tze wirklich wiederhergestellt werden?");
			final String sMessage = MessageFormat.format(sMessagePattern, iCount);
			final int btn = JOptionPane.showConfirmDialog(clctctl.getTab(), sMessage, getSpringLocaleDelegate().getMessage(
					"ResultController.restore.multipleselection.restore_collectable","Datensätze wiederherstellen"), JOptionPane.YES_NO_OPTION);
			if (btn == JOptionPane.YES_OPTION) {
				new RestoreSelectedCollectablesController<PK>((CollectController<PK, Collectable<PK>>) clctctl).run(clctctl.getMultiActionProgressPanel(iCount));
			}
		}
		else {
			final Clct selectedCollectableFromTableModel = getSelectedCollectableFromTableModel();
			LOG.debug("restore single collectable: {}", selectedCollectableFromTableModel.getPrimaryKey() );
			final String sMessagePattern = getSpringLocaleDelegate().getMessage(
					"ResultController.restore.singleselection.confirm","Soll der ausgewählte Datensatz ({0}) wirklich wiederhergestellt werden?");
			final String sMessage = MessageFormat.format(sMessagePattern,
					SpringLocaleDelegate.getInstance().getIdentifierLabel(selectedCollectableFromTableModel, selectedCollectableFromTableModel.getEntityUID(), MetaProvider.getInstance()));
			final int btn = JOptionPane.showConfirmDialog(clctctl.getTab(), sMessage, getSpringLocaleDelegate().getMessage(
					"ResultController.restore.singleselection.restore_collectable","Datensatz wiederherstellen"), JOptionPane.YES_NO_OPTION);

			if (btn == JOptionPane.YES_OPTION) {
				UIUtils.runCommand(clctctl.getTab(), new Runnable() {
					@Override
                    public void run() {
						try {
							clctctl.checkedRestoreCollectable(selectedCollectableFromTableModel, null);
							getSearchResultStrategy().refreshResult();
							
						}
						catch (CommonPermissionException ex) {
							final String msg = getSpringLocaleDelegate().getMessage(
									"ResultController.restore.singleselection.insufficient_rights","Sie verf\u00fcgen nicht \u00fcber die ausreichenden Rechte, um diesen Datensatz zu wiederherzustellen.");
							Errors.getInstance().showExceptionDialog(clctctl.getTab(), msg, ex);
						}
						catch (CommonBusinessException ex) {
							final String msg = getSpringLocaleDelegate().getMessage(
									"ResultController.restore.singleselection.failed","Der Datensatz {0} konnte nicht wiederhergestellt werden.");
							Errors.getInstance().showExceptionDialog(clctctl.getTab(), msg, ex);
						}
					}
				});
			}
		}
	}

	/**
	 * @return the fields displayed in the result panel
	 */
	public ChoiceEntityFieldList getFields() {
		return fields;
	}

	/**
	 * reads the previously selected fields from the user preferences, ignoring unknown fields that might occur when
	 * the database schema has changed from one software release to another. This method tries to avoid throwing exceptions.
	 * 
	 * TODO: make private?
	 * 
	 * @return List&lt;CollectableEntityField&gt; the previously selected fields from the user preferences.
	 * @see #writeSelectedFieldsAndWidthsToPreferences()
	 */
	protected List<? extends CollectableEntityField> readSelectedFieldsFromPreferences() {
		List<UID> lstSelectedFields = getTablePreferencesManager().getSelectedColumns();
		
		final List<CollectableEntityField> result = createCollectableEntityFieldListFromFieldNames(lstSelectedFields);
		return result;
	}
	
	private List<CollectableEntityField> createCollectableEntityFieldListFromFieldNames(List<UID> lstSelectedFields) {
		assert lstSelectedFields != null;
		
		final List<CollectableEntityField> result = new ArrayList<CollectableEntityField>();
		for (UID field : lstSelectedFields) {
			try {
				final UID entity = MetaProvider.getInstance().getEntityField(field).getEntity();
				final CollectableEntity ce;
				if (!entity.equals(clcte.getUID())) {
					ce = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(entity);
				} else {
					ce = clcte;
				}
				
				result.add(getCollectableEntityFieldForResult(ce, field));
			}
			catch (Exception ex) {
				// ignore unknown fields
				LOG.warn("Ein Feld mit dem Namen \"" + field + "\" ist nicht in dem Businessobjekt " + clcte.getLabel() + " enthalten.", ex);
			}
		}
		return result;
	}

	/**
	 * TODO Make this private.
	 * 
	 * @param comp
	 * @return SortedSet&lt;CollectableEntityField&gt; the fields that are available for the result. This default implementation
	 * returns all fields of the given entity that are to be display in the table.
	 * Successors may want to do weird things like appending fields from subentities here...
	 */
	public SortedSet<CollectableEntityField> getFieldsAvailableForResult(Comparator<CollectableEntityField> comp) {
		final SortedSet<CollectableEntityField> result = new TreeSet<CollectableEntityField>(comp);
		
		for (UID fieldUid : clcte.getFieldUIDs()) {
			if (hideThisField(fieldUid)) {
				continue;
			}
			
			if (this.isFieldToBeDisplayedInTable(fieldUid)) {
				result.add(getCollectableEntityFieldForResult(clcte, fieldUid));
			}
		}
		
		final CollectController<?, ?> controller = getCollectController();
		
		// add subentities' fields, if any:
		final Set<UID> stSubEntityNames = GenericObjectMetaDataCache.getInstance().getSubFormEntityNamesByModuleId(controller.getEntityUid());
		final Set<String> stSubEntityLabels = new HashSet<String>();
		
		for (UID sSubEntityName : stSubEntityNames) {
			if (sSubEntityName == null)
				continue;
			
			if (sSubEntityName.getString().startsWith(E.CHART.getUID().getString()))
				continue;
			
			getFieldsAvaibleInSubform(result, stSubEntityLabels, sSubEntityName);
		}
		
		return result;
	}
	
	private boolean hideThisField(UID fieldUid) {
		return FieldUtils.hideField(fieldUid, mpEntityFields, SecurityCache.getInstance().isSuperUser());
	}
	
	protected final static Set<UID> filterHiddenFields(Set<UID> fields, UID entity) {
		Set<UID> retVal = new HashSet<UID>();
		Map<UID, FieldMeta<?>> parMpEntityFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(entity);
		boolean bSuperUser = SecurityCache.getInstance().isSuperUser();
		
		for (UID field : fields) {
			if (FieldUtils.isBinary(field, parMpEntityFields)) {
				continue;
			}
			
			if (!FieldUtils.hideField(field, parMpEntityFields, bSuperUser)) {
				retVal.add(field);
			}
		}
		
		return retVal;
	}

	/**
	 * can be used to hide columns in the table.
	 * @param fieldUid
	 * @return Is the field with the given name to be displayed in a table?
	 * TODO move to ResultPanel.isFieldToBeShown
	 */
	protected boolean isFieldToBeDisplayedInTable(UID fieldUid) {
		return true;
	}

	/**
	 * TODO: Make this private.
	 * 
	 * @param sClcte
	 * @param fieldUid
	 * @return a <code>CollectableEntityField</code> of the given entity with the given field name, to be used in the Result metadata.
	 * Some successors may want to do weird things here...
	 */
	public CollectableEntityField getCollectableEntityFieldForResult(CollectableEntity sClcte, UID fieldUid) {
		if (!clcte.getUID().equals(sClcte.getUID())) {
			return GenericObjectUtils.getCollectableEntityFieldForResult(sClcte, fieldUid, clcte);
		}
		return clcte.getEntityField(fieldUid);
	}

	/**
	 * §postcondition result != null
	 * TODO: move to ResultController or ResultPanel
	 * 
	 * @return the <code>Comparator</code> used for <code>CollectableEntityField</code>s (columns in the Result).
	 * The default is to compare the column labels.
	 */
	public Comparator<CollectableEntityField> getCollectableEntityFieldComparator() {
		return new Comparator<CollectableEntityField>() {
			@Override
			public int compare(CollectableEntityField o1, CollectableEntityField o2) {
				return o1.compareTo(o2);
				/*int result = StringUtils.compareIgnoreCase(
						SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(o1.getEntityUID())),
								SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(o2.getEntityUID())));
				if (result == 0)
					result = StringUtils.compareIgnoreCase(o1.getLabel(), o2.getLabel());
				return result;
				*/
			}
			
		};
	}

	CollectableFieldsProviderFactory getCollectableFieldsProviderFactory() {
		return this.clctfproviderfactory;
	}

	/**
	 * sets all column widths to user preferences; set optimal width if no preferences yet saved
	 * Copied from the SubFormController
	 * @param tbl
	 */
	public void setColumnWidths(final JTable tbl) {
		isIgnorePreferencesUpdate = true;
		this.getResultPanel().setColumnWidths(tbl, clctctl.getTablePreferencesManager());
		isIgnorePreferencesUpdate = false;
	}

	/**
	 * @return the list of sorting columns.
	 * TODO make this private
	 */
	public List<CollectableSorting> getCollectableSortingSequence() {
		final UID baseEntityUid = getEntity().getUID();
		final Set<CollectableSorting> set = new HashSet<CollectableSorting>();
		final List<CollectableSorting> result = new ArrayList<CollectableSorting>();
		SortableCollectableTableModel<?, ?> model = clctctl.getResultTableModel();
		
		for (SortKey sortKey : model.getSortKeys()) {
			final CollectableEntityField sortField = model.getCollectableEntityField(sortKey.getColumn());
			final CollectableSorting sort;
			if (sortField.getEntityUID().equals(baseEntityUid)) {
				if (sortKey.getSortOrder() != SortOrder.UNSORTED) {
					sort = new CollectableSorting(SystemFields.BASE_ALIAS, baseEntityUid, baseEntityUid.equals(sortField.getEntityUID()),
							sortField.getUID(), sortKey.getSortOrder() == SortOrder.ASCENDING);
				} else {
					continue;
				}
			}
			else {
				// not a field of the base entity => we cannot sort that
				continue;
			}
			// only use each sort column once
			if (set.add(sort)) {
				result.add(sort);
			}
		}
		// ??? is this needed (see above)
		CollectionUtils.removeDublicates(result);
		return result;
	}

	/**
	 * @return the selected <code>Collectable</code>, if any, from the table model.
	 * Note that there is no selected <code>Collectable</code> in New mode.
	 * Note that the result might be incomplete, that means, some fields might be missing.
	 * TODO make this private
	 */
	public Clct getSelectedCollectableFromTableModel() {
		final JTable t = clctctl.getResultTable();
		if (t == null) {
			return null;
		}
		final int iSelectedRow = clctctl.getResultTable().getSelectedRow();
		return (iSelectedRow == -1) ? null : clctctl.getResultTableModel().getCollectable(iSelectedRow);
	}
	
	/**
	 * @return the selected <code>Collectable</code>'s, if any, from the table model.
	 * Note that there is no selected <code>Collectable</code> in New mode.
	 * Note that the result might be incomplete, that means, some fields might be missing.
	 */
	public Collection<Clct> getSelectedCollectablesFromTableModel() {
		int[] iSelectedRows = clctctl.getResultTable().getSelectedRows();
		return clctctl.getResultTableModel().getRows(iSelectedRows);
	}
	
	public boolean isDeleteAllowed(Clct clct) {
		return SecurityCache.getInstance().isDeleteAllowedForMasterData(clct.getEntityUID()) 
		&& MasterDataDelegate.getInstance().getMetaData(clct.getEntityUID()).isEditable();
	}

	/**
	 * replaces the selected <code>Collectable</code> in the table model with <code>clct</code>.
	 * 
	 * §precondition clct != null
	 *
	 * @deprecated This method is flawed, because if there are more than one row selected, it's faulty (NUCLOS-6441)
	 * Use replaceCollectableInTableModel(Clct clct) instead
	 */
	@Deprecated
	public final void replaceSelectedCollectableInTableModel(Clct clct) {
		if (clct == null) {
			throw new NullArgumentException("clct");
		}
		this.replaceCollectableInTableModel(clctctl.getResultTable().getSelectedRow(), clct);
	}

	/**
	 * replaces the <code>Collectable</code> in the table model that has the same id as <code>clct</code>
	 * with <code>clct</code>.
	 * 
	 * §precondition clct != null
	 * TODO: make this private
	 */
	public final void replaceCollectableInTableModel(Clct clct) {
		if (clct == null) {
			throw new NullArgumentException("clct");
		}
		final int iRow = clctctl.getResultTableModel().findRowById(clct.getId());
		if (iRow == -1) {
			throw new CommonFatalException("Der Datensatz mit der Id " + clct.getId() + " ist nicht im Suchergebnis vorhanden.");
		}
		this.replaceCollectableInTableModel(iRow, clct);
	}

	/**
	 * replaces the <code>Collectable</code> in the given row of the table model with <code>clct</code>.
	 * 
	 * §precondition clct != null
	 * TODO move to ResultController
	 */
	private void replaceCollectableInTableModel(int iRow, Clct clct) {
		if (clct == null) {
			throw new NullArgumentException("clct");
		}
		clctctl.getResultTableModel().setCollectable(iRow, clct);
	}

	/**
	 * replaces the <code>Collectable</code>s the table model that have the same ids
	 * as the given <code>Collectable</code>s, with those <code>Collectable</code>s.
	 * 
	 * §precondition collclct != null
	 *
	 * TODO: Make this protected again.
	 */
	public final void replaceCollectablesInTableModel(Collection<Clct> collclct) {
		if (collclct == null) {
			throw new NullArgumentException("collclct");
		}
		for (Clct clct : collclct) {
			this.replaceCollectableInTableModel(clct);
		}
	}

	/**
	 * command: select columns
	 * Lets the user select the columns to show in the result list.
	 */
	public void cmdSelectColumns(final ChoiceEntityFieldList fields) {
		final SelectColumnsController ctl = new SelectColumnsController(clctctl.getTab());
		// final List<CollectableEntityField> lstAvailable = (List<CollectableEntityField>) fields.getAvailableFields();
		// final List<CollectableEntityField> lstSelected = (List<CollectableEntityField>) fields.getSelectedFields();
		final ResultPanel<PK,Clct> panel = getResultPanel();
		final JTable tbl = panel.getResultTable();

		final Map<UID, Integer> mpWidths = panel.getVisibleColumnWidth(fields.getSelectedFields());
		ctl.setModel(fields);
		final boolean bOK = ctl.run(getSpringLocaleDelegate().getMessage(
				"SelectColumnsController.1","Anzuzeigende Spalten ausw\u00e4hlen"));

		if (bOK) {
			UIUtils.runCommand(clctctl.getTab(), new CommonRunnable() {
				@Override
                public void run() throws CommonBusinessException {
					final int iSelectedRow = tbl.getSelectedRow();
					fields.set(ctl.getAvailableObjects(), ctl.getSelectedObjects(), clctctl.getResultController().getCollectableEntityFieldComparator());
					final List<? extends CollectableEntityField> lstSelectedNew = fields.getSelectedFields();
					((CollectableTableModel<?,?>) tbl.getModel()).setColumns(lstSelectedNew);
					panel.setupTableCellRenderers(tbl);
					Collection<CollectableEntityField> collNewlySelected = new ArrayList<CollectableEntityField>(lstSelectedNew);
					collNewlySelected.removeAll(fields.getSelectedFields());
					if (!collNewlySelected.isEmpty()) {
						if (!clctctl.getSearchStrategy().getCollectablesInResultAreAlwaysComplete()) {
							// refresh the result:
							clctctl.getResultController().getSearchResultStrategy().refreshResult();
						}
						getResultPanel().getResultFilter().updateFilterColumns();
					}

					// reselect the previously selected row (which gets lost be refreshing the model)
					if (iSelectedRow != -1) {
						tbl.setRowSelectionInterval(iSelectedRow, iSelectedRow);
					}

					isIgnorePreferencesUpdate = true;
					panel.restoreColumnWidths(ctl.getSelectedObjects(), mpWidths);
					isIgnorePreferencesUpdate = false;

					// Set the newly added columns to optimal width
					for (CollectableEntityField clctef : collNewlySelected) {
						TableUtils.setOptimalColumnWidth(tbl, tbl.getColumnModel().getColumnIndex(clctef.getLabel()));
					}
				}
			});
		}
	}

	/**
	 * adds the column with the given field name before column columnBefore to the table.
	 * @param fields available and selected fields of the result table
	 * @param columnBefore the column of the column model (as opposed to the column of the table model)
	 * @param fieldUidToAdd name of the column to add
	 */
	protected void cmdAddColumn(final ChoiceEntityFieldList fields, TableColumn columnBefore, final UID fieldUidToAdd) throws CommonBusinessException {
		// find the field with the given name in available fields:
		final CollectableEntityField clctef = CollectionUtils.findFirst(fields.getAvailableFields(),
				PredicateUtils.transformedInputEquals(new CollectableEntityField.GetUID(), fieldUidToAdd));

		assert clctef != null;

		final CollectableTableModel<?,?> tblmodel = (CollectableTableModel<?,?>) getResultPanel().getResultTable().getModel();
		final CollectableEntityField clctefBefore = tblmodel.getCollectableEntityField(columnBefore.getModelIndex());
		int insertPos = fields.getSelectedFields().indexOf(clctefBefore);
		insertPos = (insertPos >= 0) ? insertPos : 0;
		fields.moveToSelectedFields(insertPos, clctef);

		// Note that it is not enough to add the column to the result table model.
		// We must rebuild the table tblmodel's columns in order to sync it with the table column model:
		tblmodel.setColumns(fields.getSelectedFields());
	}

	/**
	 * removes the given column from the table
	 * @param entityField the column of the column model (as opposed to the column of the table model)
	 */
	protected void cmdRemoveColumn(final ChoiceEntityFieldList fields, CollectableEntityField entityField) {
		fields.moveToAvailableFields(entityField);

		// Note that it is not enough to remove the column from the result table model.
		// We must rebuild the table model's columns in order to sync it with the table column model:
		// this.getResultTableModel().removeColumn(iColumn);
		((SortableCollectableTableModel<?,?>) getResultPanel().getResultTable().getModel()).setColumns(fields.getSelectedFields());
	}

	/**
	 * TODO: Make this protected again.
	 */
	public void setModel(CollectableTableModel<PK,Clct> tblmodel) {
		final ResultPanel<PK,Clct> panel = getResultPanel();
		final JTable resultTable = panel.getResultTable();
		resultTable.setModel(tblmodel);
		((ToolTipsTableHeader) resultTable.getTableHeader()).setExternalModel(tblmodel);
		if (tblmodel instanceof SortableTableModel) {
			((SortableTableModel) tblmodel).addSortingListener(newResultTablePreferencesUpdateListener());
		}
	}
	
	public void setIgnorePreferencesUpdate(boolean ignore) {
		this.isIgnorePreferencesUpdate = ignore;
	}

	protected void toggleColumnVisibility(TableColumn columnBefore, final UID fieldUid)  {
		final ResultPanel<PK,Clct> panel = getResultPanel();
		try {
			final ChoiceEntityFieldList fields = clctctl.getFields();
			final Map<UID, Integer> mpWidths = panel.getVisibleColumnWidth(fields.getSelectedFields());
			final CollectableEntityField clctef = clcte.getEntityField(fieldUid);
			final int iIndex = fields.getSelectedFields().indexOf(clctef);
			if (iIndex == -1) {
				cmdAddColumn(fields, columnBefore, fieldUid);
				if (!clctctl.getSearchStrategy().getCollectablesInResultAreAlwaysComplete()) {
					clctctl.getResultController().getSearchResultStrategy().refreshResult();
				}
			}
			else {
				cmdRemoveColumn(fields, clctef);
			}
			isIgnorePreferencesUpdate = true;
			panel.restoreColumnWidths(fields.getSelectedFields(), mpWidths);
			isIgnorePreferencesUpdate = false;
		}
		catch (CommonBusinessException e) {
			// TODO Auto-generated catch block
			LOG.warn("toggleColumnVisibility failed: " + e, e);
		}
	}
	
	/**
	 * writes the selected columns (fields) and their widths to the user preferences.
	 * TODO make private again or refactor!
	 */
	public void writeSelectedFieldsAndWidthsToPreferences() {
		writeSelectedFieldsAndWidthsToPreferences(null);
	}

	/**
	 * writes the selected columns (fields) and their widths to the user preferences.
	 * TODO make private again or refactor!
	 * @param mpWidths 
	 */
	public void writeSelectedFieldsAndWidthsToPreferences(Map<UID, Integer> mpWidths) {
		writeSelectedFieldsAndWidthsToPreferences(clctctl.getTablePreferencesManager(), clctctl.getFields().getSelectedFields(), mpWidths);
	}
	
	protected void writeSelectedFieldsAndWidthsToPreferences(
			final TablePreferencesManager tblprefManager, List<? extends CollectableEntityField> lstclctefSelected, Map<UID, Integer> mpWidths) {
		writeSelectedFieldsAndWidthsToPreferences(tblprefManager, lstclctefSelected, mpWidths, null);
	}

	protected void writeSelectedFieldsAndWidthsToPreferences(
		final TablePreferencesManager tblprefManager, List<? extends CollectableEntityField> lstclctefSelected, Map<UID, Integer> mpWidths, List<UID> fixedFields) {
		final List<UID> lstFields = getFieldsForPreferences(lstclctefSelected);
		final List<Integer> lstFieldWidths = new ArrayList<Integer>();
		if (mpWidths==null) {
			mpWidths = getResultPanel().getVisibleColumnWidth(lstclctefSelected);
		}
		for (CollectableEntityField clctef : lstclctefSelected) {
			final UID field = clctef.getUID();
			if (mpWidths.containsKey(field)) {
				lstFieldWidths.add(mpWidths.get(field));
			} else {
				lstFieldWidths.add(ProfileUtils.getMinimumColumnWidth(clctef.getJavaClass()));
			}
		}

		tblprefManager.setColumnPreferences(lstFields, lstFieldWidths, fixedFields);
	}
	
	protected List<UID> getFieldsForPreferences(List<? extends CollectableEntityField> lstclctefSelected) {
		return CollectableUtils.getFieldUidsFromCollectableEntityFields(lstclctefSelected);
	}
	
	protected List<Integer> getFieldWidthsForPreferences() {
		return CollectableTableHelper.getColumnWidths(getResultPanel().getResultTable());
	}
	
	private class ResetMainFilterEnabledListener implements EnabledListener {
		@Override
		public void enabledChanged(boolean enabled) {
//			getResultPanel().btnResetMainFilter.setVisible(enabled);
			clctctl.getResetMainFilterAction().putValue(ToolBarItemAction.VISIBLE, enabled);
		}
	}

	public void postInitialisation() {
		// TODO Auto-generated method stub
		
	}
	
	private void setDeleteButtonToggleInResult() {
		
		int countLogicalDeleted = 0;
		int countNotLogicalDeleted = 0;
		int countNotDeletable = 0;
		for (Clct clct : getSelectedCollectablesFromTableModel()) {
			boolean isLogicalDeleted = false;
			// this part is used both for MasterData and GenericObject as well
			if (clct instanceof CollectableGenericObject) {
				isLogicalDeleted = (Boolean) clct.getValue(SF.LOGICALDELETED.getUID(clct.getEntityUID()));
			} else {
				isLogicalDeleted = false;
			}
			if (isLogicalDeleted) {
				++countLogicalDeleted;
			} else {
				++countNotLogicalDeleted;
			}
			if (!isDeleteAllowed(clct)) {
				countNotDeletable++;
			}
		}
		
		final DeleteOrRestoreToggleAction action = actDeleteSelectedCollectables;
		boolean enabled = true;
		if (countLogicalDeleted > 0 && countNotLogicalDeleted > 0) {
			enabled = false;
		} else if (countLogicalDeleted > 0) {
			NuclosToolBarActions.MULTI_RESTORE
			.init(action);
			action.setMode(DeleteOrRestoreToggleAction.Mode.RESTORE);
			action.putValue(Action.SELECTED_KEY,
					Boolean.TRUE);
		} else if (countNotLogicalDeleted > 0) {
			NuclosToolBarActions.MULTI_DELETE
			.init(action);
			action.setMode(DeleteOrRestoreToggleAction.Mode.DELETE);
			action.putValue(Action.SELECTED_KEY,
					Boolean.FALSE);
		} else {
			enabled = false;
		}
		enabled = enabled && countNotDeletable == 0;
		action.setEnabled(enabled);
		
	}
	
	private void getFieldsAvaibleInSubform(SortedSet<CollectableEntityField> result, Set<String> stSubEntityLabels, UID sSubEntityName) {
		final CollectableEntity clcteSub = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(sSubEntityName);
		// WORKAROUND for general search: We don't want duplicate entities (assetcomment, ordercomment etc.), so we
		// ignore entities with duplicate labels:
		// TODO: eliminate this workaround 
		final String sSubEntityLabel = clcteSub.getLabel();
		if (!stSubEntityLabels.contains(sSubEntityLabel)) {
			stSubEntityLabels.add(sSubEntityLabel);
			Set<UID> visibleFields = filterHiddenFields(clcteSub.getFieldUIDs(), clcteSub.getUID());
			if (!clcteSub.getMeta().isProxy()) {
				result.addAll(CollectionUtils.transform(visibleFields, new GetCollectableEntityFieldForResult(clcteSub)));
			}
		}
	}
	
	private final class GetCollectableEntityFieldForResult implements Transformer<UID, CollectableEntityField> {
		private final CollectableEntity clcte;

		public GetCollectableEntityFieldForResult(CollectableEntity clcte) {
			this.clcte = clcte;
		}

		@Override
		public CollectableEntityField transform(UID sFieldName) {
			return getCollectableEntityFieldForResult(clcte, sFieldName);
		}
	}

}	// class ResultController
