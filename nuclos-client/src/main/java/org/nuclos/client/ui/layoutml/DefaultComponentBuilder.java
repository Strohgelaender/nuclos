package org.nuclos.client.ui.layoutml;

import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.NuclosDropTargetListener;
import org.nuclos.client.common.NuclosDropTargetVisitor;
import org.nuclos.client.ui.JInfoTabbedPane;
import org.nuclos.client.ui.SizeKnownListener;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableOptionGroup;
import org.nuclos.common.LafParameter;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.xml.sax.SAXException;

/**
 * class <code>ComponentBuilder</code>. Builds a regular <code>JComponent</code>.
 */
class DefaultComponentBuilder implements ComponentBuilder {

	private JComponent comp;
	private Object oConstraints;
	private String translation;

	/**
	 * §postcondition this.getComponent() == comp
	 * 
	 * @param comp the component to build
	 */
	DefaultComponentBuilder(JComponent comp) {
		this.comp = comp;
	}

	/**
	 * §postcondition this.getConstraints() == oConstraints
	 * 
	 * @param oConstraints the constraints to use when adding the internal component to its parent.
	 */
	@Override
	public void setConstraints(Object oConstraints) {
		this.oConstraints = oConstraints;
	}

	/**
	 * adds <code>comp</code> to the internal component (as in <code>getComponent()</code>),
	 * using the given constraints
	 * @param comp
	 * @param oConstraints
	 */
	@Override
	public void add(JComponent comp, Object oConstraints) {
		this.comp.add(comp, oConstraints);
	}

	/**
	 * @param comp
	 * @return the name (if any) and class name of the given component
	 */
	private static String getComponentNameAndClassName(JComponent comp) {
		final StringBuffer sb = new StringBuffer();
		final String sName = comp.getName();
		if (!StringUtils.isNullOrEmpty(sName)) {
			sb.append('"');
			sb.append(sName);
			sb.append('"');
			sb.append(' ');
		}
		sb.append("(").append(SpringLocaleDelegate.getInstance().getMessage(
				"LayoutMLParser.12", "Klasse")).append(": ");
		sb.append(comp.getClass().getName());
		sb.append(')');
		return sb.toString();
	}

	/**
	 * @return the internal component (the component to build)
	 */
	@Override
	public JComponent getComponent() {
		return this.comp;
	}

	/**
	 * @return the constraints to use when adding the internal component to its parent.
	 */
	@Override
	public Object getConstraints() {
		return this.oConstraints;
	}

	/** Gets the translation to use (instead of the original text). */
	@Override
	public String getTranslation() {
		return translation;
	}

	/** Sets the translation to use. */
	@Override
    public void setTranslation(String translation) {
		this.translation = translation;
	}

	/**
	 * finishes the internal component
	 * @param cbParent
	 */
	@Override
	public void finish(ComponentBuilder cbParent) throws SAXException {
		try {
			// add component to parent:
			cbParent.add(getComponent(), getConstraints());
		}
		catch (Exception ex) {
			final String sMessage = StringUtils.getParameterizedExceptionMessage("LayoutMLParser.13",
				getComponentNameAndClassName(comp), getComponentNameAndClassName(cbParent.getComponent()), oConstraints);
			throw new SAXException(sMessage, ex);
		}
	}
	
	static class TabbedPaneConstraints implements LocalizationHandler {
		String sTitle;
		boolean bEnabled = true;
		String sInternalname;
		String sMnemonic;

		TabbedPaneConstraints() {
		}

		@Override
		public void setTranslation(String translation) {
			this.sTitle = translation;
		}
	}

	static class CollectableComponentBuilder extends DefaultComponentBuilder {
		final CollectableComponent clctcomp;

		/**
		 * contains options for option group
		 */
		private List<String[]> lstOptions;

		/**
		 * contains default value for option group
		 */
		private String sDefaultOption;

		/**
		 * §postcondition this.getComponent() == clctcomp
		 * 
		 * @param clctcomp the collectable component to build
		 */
		CollectableComponentBuilder(CollectableComponent clctcomp) {
			super(clctcomp.getJComponent());
			this.clctcomp = clctcomp;
		}

		/**
		 * @return the internal component (the component to build)
		 */
		public CollectableComponent getCollectableComponent() {
			return this.clctcomp;
		}

		/**
		 * Adds an option to a collectable component (option group)
		 * @param sValue
		 * @param sLabel
		 * @param sMnemonic
		 */
		public String[] addOption(String sValue, String sLabel, String sMnemonic) {
			if (lstOptions == null) {
				lstOptions = new ArrayList<String[]>();
			}
			final String[] asOptions = new String[3];
			asOptions[0] = sValue;
			asOptions[1] = sLabel;
			asOptions[2] = StringUtils.emptyIfNull(sMnemonic);
			lstOptions.add(asOptions);
			return asOptions;
		}

		public void setDefaultOption(String sValue) {
			this.sDefaultOption = sValue;
		}

		@Override
		public void finish(ComponentBuilder cbParent) throws SAXException {
			super.finish(cbParent);

			/** @todo Finally we got rid of values - now we have options here... */
			if (this.getCollectableComponent() instanceof CollectableOptionGroup) {
				final CollectableOptionGroup group = (CollectableOptionGroup) this.getCollectableComponent();
				if (lstOptions != null) {
					group.setOptions(lstOptions);
				}
				group.setDefaultOption(sDefaultOption);
			}
		}

	}	// inner class CollectableComponentBuilder
	
	/**
	 * inner class <code>TabbedPaneBuilder</code>. Builds a <code>JInfoTabbedPane</code>.
	 */
	static class TabbedPaneBuilder extends DefaultComponentBuilder implements NuclosDropTargetVisitor {

		private SubForm previousSubForm;

		private int previousTab = -1;

		TabbedPaneBuilder(JInfoTabbedPane tbdpane) {
			super(tbdpane);
		}

		/**
		 * adds <code>comp</code> as a tab, using the given constraints
		 * 
		 * §precondition oConstraints != null
		 * 
		 * @param comp
		 * @param oConstraints the String to appear on the tab
		 */
		@Override
		public void add(JComponent comp, Object oConstraints) {
			final TabbedPaneConstraints tbdpaneconstraints = (TabbedPaneConstraints) oConstraints;
			final JInfoTabbedPane tbdpn = (JInfoTabbedPane) getComponent();
			tbdpn.addTab(tbdpaneconstraints.sTitle, comp);
			final int index = tbdpn.indexOfComponent(comp);
			tbdpn.setEnabledAt(index, tbdpaneconstraints.bEnabled);
			if(tbdpaneconstraints.sMnemonic != null) {
				tbdpn.setMnemonicAt(index, Integer.parseInt(tbdpaneconstraints.sMnemonic));
			}
			tbdpn.updateTab(index);
			final JComponent jcomp = (JComponent) tbdpn.getTabComponentAt(index);
			setupDragDrop(jcomp);
			if(tbdpaneconstraints.sInternalname != null)
				comp.setName(tbdpaneconstraints.sInternalname);
		}

		protected void setupDragDrop(final JComponent comp) {
			NuclosDropTargetListener listener = new NuclosDropTargetListener(this);
			DropTarget drop = new DropTarget(comp, listener);
			drop.setActive(true);
		}

		@Override
		public void visitDragEnter(DropTargetDragEvent dtde) {}

		@Override
		public void visitDragExit(DropTargetEvent dte) {}

		@Override
		public void visitDragOver(DropTargetDragEvent dtde) {
			final DropTarget target = (DropTarget)dtde.getSource();
			final JLabel label = (JLabel) target.getComponent();
			final JInfoTabbedPane tbdpn = (JInfoTabbedPane) getComponent();
			final int index = tbdpn.indexOfTabComponent(label);
			tbdpn.setSelectedIndex(index);
			dtde.rejectDrag();
		}

		@Override
		public void visitDrop(DropTargetDropEvent dtde) {}

		@Override
		public void visitDropActionChanged(DropTargetDragEvent dtde) {}

		/**
		 * NUCLOSINT-63: connect the subform with the corresponding tab via the SizeKnownListener.
		 */
		public void addSubForm(SubForm subform) {
			final JInfoTabbedPane tbdpn = (JInfoTabbedPane) getComponent();
			final int tabs = tbdpn.getTabCount();
			// Only add the listener to the subform if there is no previous
			// subform in this tab.
			if (previousTab != tabs) {
				previousSubForm = subform;
				previousTab = tabs;
				final Boolean showRowCount = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Layout_Tab_Show_Row_Count, subform.getEntityUID());
				if (!Boolean.FALSE.equals(showRowCount)) {
					subform.setSizeKnownListener(new SizeKnownListener(tbdpn));
				} else {
					subform.setSizeKnownListener(new SizeKnownListener(null));
				}
			}
			// If there is a second subform in this tab remove the listener
			// from the first subform.
			else if (previousSubForm != null) {
				//previousSubForm.setSizeKnownListener(null); // @todo is this needed here? @see  	RSWORGA-19
				previousSubForm = null;
				subform.setSizeKnownListener(new SizeKnownListener(null));
			}
		}

	}	// inner class TabbedPaneBuilder
	
	/**
	 * inner class <code>ScrollPaneBuilder</code>. Builds a <code>JScrollPane</code>.
	 */
	static class ScrollPaneBuilder extends DefaultComponentBuilder {
		private JScrollPane scrlpn;

		ScrollPaneBuilder(JScrollPane tbdpane) {
			super(tbdpane);
			this.scrlpn = tbdpane;
		}

		/**
		 * adds <code>comp</code> to the scrollpane's viewport.
		 * @param comp
		 * @param constraints are ignored
		 */
		@Override
		public void add(JComponent comp, Object constraints) {
			this.scrlpn.getViewport().add(comp);
		}
	}	// inner class ScrollPaneBuilder
}	//class ComponentBuilder

