// Copyright (C) 2011 Novabit Informationssysteme GmbH
//
// This file is part of Nuclos.
//
// Nuclos is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nuclos is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Nuclos. If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import org.nuclos.client.ui.collect.subform.SubForm;

/**
 * A Listener to a certain tab of a JInfoTabbedPane.
 * 
 * @see JInfoTabbedPane
 * @author Thomas Pasch
 * @since Nuclos 3.1.00
 */
public class SizeKnownListener implements ActionListener {

	private final JInfoTabbedPane pane;
	
	public SizeKnownListener(JInfoTabbedPane pane) {
		this.pane = pane;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e instanceof SizeKnownEvent) {
			final SizeKnownEvent ske = (SizeKnownEvent) e;

			if (ske.getSource() instanceof SubForm) {
				((SubForm) ske.getSource()).setStatusbarRowCount(((SizeKnownEvent) e).getSize());
			}

			if (pane == null && ske.getSource() instanceof SubForm) {
				JTable table = ((SubForm) ske.getSource()).getSubformTable();
				if (table.getModel() instanceof AbstractTableModel) {
					AbstractTableModel m2 = (AbstractTableModel) table.getModel();
					if (table.getColumnCount() == 0) {
						m2.fireTableStructureChanged();						
					} else {
						m2.fireTableDataChanged();
					}
				}
			} else if (pane != null) {
				sizeKnowEventForTabbedPane(pane, ske);				
			}
		}
	}
	
	private static void sizeKnowEventForTabbedPane(JInfoTabbedPane tPane, SizeKnownEvent ske) {
		int tab = -1;
		for (int i = 0; i < tPane.getTabCount(); i++) {
			Collection<?> coll = UIUtils.findAllInstancesOf(tPane.getComponentAt(i), (Class)ske.getSource().getClass());
			for (Object object : coll) {
				if (object == ske.getSource()) {
					tab = i;
					break;
				}	
			}
		}
		if (tab != -1) {
			tPane.setTabInfoAt(tab, ske.getSize());
			tPane.setDisplayTabInfoAt(tab, true);
		}		
	}
}
