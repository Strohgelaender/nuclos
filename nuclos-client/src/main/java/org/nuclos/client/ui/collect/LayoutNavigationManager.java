//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.lang.ref.WeakReference;
import java.util.Date;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.text.JTextComponent;

import org.nuclos.client.autonumber.AutonumberUiUtils;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.Utils;
import org.nuclos.client.genericobject.CollectableGenericObjectFileChooser;
import org.nuclos.client.ui.DateChooser;
import org.nuclos.client.ui.DateChooser.DateSelectedListener;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.HyperlinkTextFieldWithButton;
import org.nuclos.client.ui.ListOfValues;
import org.nuclos.client.ui.ListOfValues.QuickSearchSelectedListener;
import org.nuclos.client.ui.collect.component.AbstractCollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableCheckBox;
import org.nuclos.client.ui.collect.component.CollectableComboBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.CollectableDateChooser;
import org.nuclos.client.ui.collect.component.CollectableHyperlink;
import org.nuclos.client.ui.collect.component.CollectableListOfValues;
import org.nuclos.client.ui.collect.component.CollectableMediaComponent;
import org.nuclos.client.ui.collect.component.CollectablePasswordField;
import org.nuclos.client.ui.collect.component.CollectableTextArea;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.custom.FileChooserComponent;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.labeled.LabeledImage;
import org.nuclos.client.ui.labeled.LabeledTextArea.InnerTextArea;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.collect.collectable.AbstractCollectableField;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LayoutNavigationManager
 * <p>
 * FIXME:
 * extract actions (right/left, up/down) to external class
 * make ready for standard layout (not only subformular)
 * remove EventConsumedFocusListener, only used for LOV handling
 *
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class LayoutNavigationManager implements LayoutNavigationSupport {

	private class EventConsumedFocusListener extends FocusAdapter {

		@Override
		public void focusLost(FocusEvent e) {
			super.focusLost(e);
			LayoutNavigationManager.this.consumed = false;
		}

		@Override
		public void focusGained(FocusEvent e) {
			super.focusGained(e);
			LayoutNavigationManager.this.consumed = false;
		}

	}

	private class ChangeListener implements TableModelListener {

		private boolean enabled = false;

		@Override
		public void tableChanged(TableModelEvent e) {
			if (isEnabled()) {
				resetPendingRow();
				setEnabled(false);
			}
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

	}

	/**
	 * LOV selection listener
	 * <p>
	 * delegating listener for {@link ListOfValues}
	 */
	public class DelegatingQuickSearchSelectedListener extends QuickSearchSelectedListener {

		private final QuickSearchSelectedListener parentListener;
		private CollectableListOfValues cltListOfValues;

		DelegatingQuickSearchSelectedListener(CollectableListOfValues cltListOfValues) {
			this.parentListener = cltListOfValues.getListOfValues().getQuickSearchSelectedListener();
			this.cltListOfValues = cltListOfValues;
		}

		@Override
		public void actionPerformed(CollectableValueIdField itemSelected) {
			// first apply parent listener
			if (null != parentListener) {
				parentListener.actionPerformed(itemSelected);
			}
			if (!LayoutNavigationManager.this.consumed && cltListOfValues.getListOfValues().hasFocus()) {
				actionCommit(cltListOfValues);
				LayoutNavigationManager.this.consumed = true;
			}
		}

	}

	private final static Logger LOG = LoggerFactory.getLogger(LayoutNavigationManager.class);


	private volatile boolean consumed = false;

	/**
	 * Wrapped reference maybe null - as it gets gc'ed! (tp)
	 */
	private final WeakReference<DetailsSubFormController<?, ?>> ctl;

	private final ChangeListener pendingRowListener;

	private Collectable<?> pendingRow;

	/**
	 * @param ctl subform controller
	 */
	public LayoutNavigationManager(final DetailsSubFormController<?, ?> ctl) {
		if (ctl == null) {
			throw new NullPointerException();
		}
		this.ctl = new WeakReference<>(ctl);
		this.pendingRow = null;
		this.pendingRowListener = new ChangeListener();

		// add TableModelListener for pending row cleanup
		ctl.getSubForm().getSubformTable().getSubFormModel().addTableModelListener(pendingRowListener);
	}

	private DetailsSubFormController<?, ?> getController() {
		return ctl.get();
	}

	private boolean isPendingNewRow() {
		return (pendingRow != null);
	}

	private boolean validateField(final CollectableComponent source) {
		try {
			source.getFieldFromView();
		} catch (final CollectableFieldFormatException ex) {
			final String sMessage = StringUtils.getParameterizedExceptionMessage("field.invalid.value", source.getEntityField().getLabel());
			//"Das Feld \"" + clctef.getLabel() + "\" hat keinen g\u00fcltigen Wert.";
			Errors.getInstance().showExceptionDialog(null, sMessage, ex);
			return false;
		}
		return true;
	}

	private void actionCommit(final CollectableComponent source) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;

		/*
		 * BMWFDM-308
		 * don't create new lines for a default commit (by enter key)
		 */

		int row = ctl.getJTable().getSelectedRow();
		int column = ctl.getJTable().getSelectedColumn();
		Pair<Integer, Integer> newCoords = findNextCell(row, column);
		if (newCoords.equals(new Pair<>(row, column))) {
			// reached end of the row, stop cell editing process
			ctl.stopEditing();
		} else {
			// in the middle of a row, continue by calling actionRight();
			actionRight(source);
		}
	}

	private void actionRight(final CollectableComponent source) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;
		LOG.debug("action right");

		int row = ctl.getJTable().getSelectedRow();
		int column = ctl.getJTable().getSelectedColumn();
		Pair<Integer, Integer> newCoords = findNextCell(row, column);

		//ctl.stopEditing();
		if (newCoords.equals(new Pair<>(row, column))) {
			if (!createNewRow()) {
				return;
			} else {
				newCoords = findNextCell(row, column);
			}
		}
		// validate field content
		if (validateField(source)) {
			ctl.getJTable().editCellAt(newCoords.x, newCoords.y);
			ctl.getJTable().changeSelection(newCoords.x, newCoords.y, false, false);
		}
	}


	private void actionLeft(final CollectableComponent source) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;
		LOG.debug("action left");
		int row = ctl.getJTable().getSelectedRow();
		final Pair<Integer, Integer> newCoords = findPreviousCell(ctl.getJTable().getSelectedRow(), ctl.getJTable().getSelectedColumn());
		if (newCoords.x < row) {
			cleanupNewRow();
		}
		// validate field content
		if (validateField(source)) {
			ctl.getJTable().editCellAt(newCoords.x, newCoords.y);
			ctl.getJTable().changeSelection(newCoords.x, newCoords.y, false, false);
		}
	}

	private void actionDown(final CollectableComponent source) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;
		LOG.debug("action down");
		int row = ctl.getJTable().getSelectedRow();
		int column = ctl.getJTable().getSelectedColumn();
		int rowCount = ctl.getJTable().getRowCount();
		int newRow = row;
		boolean isEditable = false;
		// run to next editable cell (cells between might be disabled by layout based rules(groovy) 
		while (!isEditable && newRow < rowCount - 1) {
			++newRow;
			if (newRow >= rowCount) {
				if (!createNewRow()) {
					--newRow;
				}
			}
			if (ctl.getJTable().isCellEditable(newRow, column)) {
				isEditable = true;
			}
		}

		// validate field content
		if (validateField(source)) {
			ctl.getJTable().editCellAt(newRow, column);
			ctl.getJTable().changeSelection(newRow, column, false, false);
		}
	}

	private void actionUp(final CollectableComponent source) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;
		LOG.debug("action up");
		int row = ctl.getJTable().getSelectedRow();
		int column = ctl.getJTable().getSelectedColumn();
		if (row >= 0) {
			int newRow = row;
			boolean isEditable = false;
			// run (invers) to next editable cell (cells between might be disabled by layout based rules(groovy)
			while (!isEditable && newRow > 0) {
				--newRow;
				if (ctl.getJTable().isCellEditable(newRow, column)) {

					cleanupNewRow();
					isEditable = true;
				}
			}
			// validate field content
			if (validateField(source)) {
				ctl.getJTable().editCellAt(newRow, column);
				ctl.getJTable().changeSelection(newRow, column, false, false);
			}
		}
	}

	private void actionF8() {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;
		int row = ctl.getSubForm().getSubformTable().getSelectedRow();
		int column = ctl.getSubForm().getSubformTable().getSelectedColumn();
		if (ctl.getSubForm().getSubformTable().isCellEditable(row, column)) {
			if (row > 0) {

				AbstractCollectableField clFieldFrom = (AbstractCollectableField) ctl.getSubForm().getSubformTable().getValueAt(row - 1, column);
				ctl.getSubForm().getSubformTable().setValueAt(clFieldFrom, row, column);
				final CollectableComponentTableCellEditor editor = (CollectableComponentTableCellEditor) ctl.getSubForm().getSubformTable().getCellEditor();
				editor.getCollectableComponent().setField(clFieldFrom);

			}

		}
	}

	/**
	 * reset temporary row
	 */
	private void resetPendingRow() {
		pendingRow = null;
	}

	/**
	 * cleanup temporary row
	 */
	private void cleanupNewRow() {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;
		if (ctl.isRowMovingAllowed() && isPendingNewRow()) {
			ctl.getCollectableTableModel().remove((Collectable) pendingRow);
		}
	}

	/**
	 * create temporary row
	 *
	 * @return new row was created(true) or not (false)
	 */
	private boolean createNewRow() {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return false;

		boolean result = false;
		if (newRowAllowed()) {
			try {
				ctl.stopEditing();
				pendingRow = ctl.insertNewRow();
				// update autonumbers
				manageAutoNumbers();
				pendingRowListener.setEnabled(true);
				result = true;
			} catch (final NuclosBusinessException ex) {
				LOG.error(ex.getMessage(), ex);
				throw new NuclosFatalException(ex);
			}
		}
		return result;
	}

	private boolean newRowAllowed() {
		final DetailsSubFormController<?, ?> ctl = getController();

		if (ctl == null) {
			return false;
		}

		return !isPendingNewRow() && ctl.isRowMovingAllowed();
	}

	/**
	 * find next cell
	 *
	 * @param currentRow    current row
	 * @param currentColumn current column
	 * @return new row/column {@link Pair}, returns currentRow/currentColumn if there is no next cell
	 */
	private Pair<Integer, Integer> findNextCell(int currentRow, int currentColumn) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return null;

		// FIXME check lineend
		int[] result = getNextEditableCell(ctl.getSubForm().getSubformTable(), currentRow, currentColumn, false);
		return new Pair<>(result[0], result[1]);
	}

	/**
	 * find previous cell
	 *
	 * @param currentRow    current row
	 * @param currentColumn current column
	 * @return new row/column {@link Pair}, returns currentRow/currentColumn if there is no previous cell
	 */
	private Pair<Integer, Integer> findPreviousCell(int currentRow, int currentColumn) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return null;

		int[] result = getNextEditableCell(ctl.getSubForm().getSubformTable(), currentRow, currentColumn, true);
		return new Pair<>(result[0], result[1]);
	}

	/**
	 * taken from {@link SubForm}, BUT modified
	 *
	 * @param table    table
	 * @param row      row idx
	 * @param col      col idx
	 * @param bReverse backwards/forward
	 * @return [0] row [1] column
	 */
	private int[] getNextEditableCell(JTable table, int row, int col, boolean bReverse) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) throw new IllegalStateException();

		int rowCol[] = {row, col};
		int colCount = ctl.getSubForm().getSubformTable().getColumnCount();
		int rowCount = ctl.getSubForm().getSubformTable().getRowCount();

		boolean colFound = false;
		if (!bReverse) {
			for (int i = col + 1; i < colCount; i++) {
				if (table.isCellEditable(row, i)) {
					colFound = true;
					rowCol[1] = i;
					break;
				}
			}
			if (!colFound) {
				row++;
				if (row >= rowCount)
					return rowCol;
				while (row < rowCount) {
					for (int i = 0; i < col; i++) {
						if (table.isCellEditable(row, i)) {
							colFound = true;
							rowCol[0] = row;
							rowCol[1] = i;
							break;
						}
					}
					if (colFound) {
						break;
					}
					row++;
				}
			}
		} else {
			for (int i = col - 1; i >= 0; i--) {
				if (table.isCellEditable(row, i)) {
					colFound = true;
					rowCol[1] = i;
					break;
				}
			}
			if (!colFound) {
				row--;
				while (row >= 0) {
					for (int i = colCount - 1; i >= 0; i--) {
						if (table.isCellEditable(row, i)) {
							rowCol[0] = row;
							rowCol[1] = i;
							colFound = true;
							break;
						}
					}
					if (colFound) {
						break;
					}
					row--;
				}
			}
		}

		return rowCol;
	}

	/**
	 * @param comp editor component
	 */
	@Deprecated
	private boolean registerEventConsumedFocusListener(final JComponent comp) {
		boolean found = false;
		boolean result = false;
		for (final FocusListener fl : comp.getFocusListeners()) {
			if (fl.getClass().equals(EventConsumedFocusListener.class)) {
				found = true;
				break;
			}
		}
		if (!found) {
			comp.addFocusListener(new EventConsumedFocusListener());
			result = true;
		}

		return result;
	}

	@Override
	public void prepareCollectable(final CollectableComponent clctcomp) {
		if (clctcomp instanceof AbstractCollectableComponent) {
			AbstractCollectableComponent acc = (AbstractCollectableComponent) clctcomp;
			acc.setLayoutNavigationSupport(this);
			acc.bindLayoutNavigationSupportToProcessingComponent();

			if (clctcomp instanceof CollectableDateChooser) {
				// handling DateChooser
				final DateChooser dc = ((CollectableDateChooser) clctcomp).getDateChooser();
				dc.addCalendarAction("ActionF2", KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), new AbstractAction() {

					@Override
					public void actionPerformed(ActionEvent e) {
						dc.hideCalendar();
					}
				});
				// perform action right after date selection
				dc.setDateSelectedListener(new DateSelectedListener() {

					@Override
					public void actionPerformed(Date dateSelected) {
						actionRight(clctcomp);
					}
				});
			} else if (clctcomp instanceof CollectableListOfValues) {
				// handling LOV
				final ListOfValues lov = ((CollectableListOfValues) clctcomp).getListOfValues();

				if (!(lov.getQuickSearchSelectedListener() instanceof DelegatingQuickSearchSelectedListener)) {
					lov.setQuickSearchSelectedListener(new DelegatingQuickSearchSelectedListener(((CollectableListOfValues) clctcomp)));
				}
				registerEventConsumedFocusListener(lov);

			}
		}
	}

	private boolean moveToNextCell(final Component c) {
		boolean result = false;

		if (null == c) {
			result = true;
		} else if (c instanceof JTextComponent) {
			final JTextComponent jtc = (JTextComponent) c;

			if (jtc.getHighlighter().getHighlights().length == 1) {

				// check for complete highlights
				if (jtc.getHighlighter().getHighlights()[0].getStartOffset() == jtc.getDocument().getStartPosition().getOffset() &&
						jtc.getHighlighter().getHighlights()[0].getEndOffset() == (jtc.getDocument().getEndPosition().getOffset() - 1)) {
					result = true;
				}
			}
			if (jtc.getDocument().getLength() == 0) {
				result = true;
			}
		} else {
			result = true;
		}

		return result;
	}

	@Override
	public void editingCanceled(final ChangeEvent changeEvent) {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;

		int row = ctl.getSubForm().getSubformTable().getSelectedRow();
		int column = ctl.getSubForm().getSubformTable().getSelectedColumn();
		if (row > -1 && column > -1) {
			// restore value from model (model -> view)
			final Object oldValue = ctl.getSubForm().getSubformTable().getValueAt(row, column);
			final CollectableComponentTableCellEditor editor = (CollectableComponentTableCellEditor) ctl.getSubForm().getSubformTable().getCellEditor();
			editor.getCollectableComponent().setField((CollectableField) oldValue);

			// select editor text
			final Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
			if (focusOwner instanceof JTextField) {
				((JTextField) focusOwner).selectAll();
			}
		}
	}

	@Override
	public boolean processLayoutNavigationEvent(
			final LayoutNavigationSupportContext ctx, ExecutionPoint ep) {
		boolean result = false;


		// distinguish field type
		if (ctx.getLayoutNavigationCollectable() instanceof CollectableListOfValues) {
			result = processListOfValues(ctx, ep);
		} else if (ctx.getLayoutNavigationCollectable() instanceof CollectableComboBox) {
			result = processComboBox(ctx, ep);
		} else if (ctx.getLayoutNavigationCollectable() instanceof CollectableTextArea) {
			result = processTextArea(ctx, ep);
		} else if (ctx.getLayoutNavigationCollectable() instanceof CollectableDateChooser) {
			result = processDateChooser(ctx, ep);
		} else if (ctx.getLayoutNavigationCollectable() instanceof CollectableCheckBox) {
			result = processCheckBox(ctx, ep);
		} else if (ctx.getLayoutNavigationCollectable() instanceof LabeledImage) {
			result = processImage(ctx, ep);
		} else if (ctx.getLayoutNavigationCollectable() instanceof CollectableHyperlink) {
			result = processHyperlink(ctx, ep);
		} else if (ctx.getLayoutNavigationCollectable() instanceof CollectableTextField) {
			result = processDefault(ctx, ep);
		} else if (ctx.getLayoutNavigationCollectable() instanceof CollectableMediaComponent ||
				ctx.getLayoutNavigationCollectable() instanceof CollectableGenericObjectFileChooser ||
				ctx.getLayoutNavigationCollectable() instanceof CollectablePasswordField) {
			result = processDefault(ctx, ep);
		}
		// cancel cell editing, restore old value
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl != null && ctx.getKeyEvent().getKeyCode() == KeyEvent.VK_ESCAPE) {
			ctl.getSubForm().getSubformTable().getCellEditor().cancelCellEditing();
			ctl.getSubForm().releaseFocus();
		}

		return result;
	}

	/**
	 * lov processing
	 *
	 * @param ctx context
	 * @param ep  execution point
	 */
	private boolean processListOfValues(
			final LayoutNavigationSupportContext ctx,
			ExecutionPoint ep
	) {

		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return true;

		boolean result = ctx.hasBeenProcessed();
		final CollectableComponent clctcmp = (CollectableComponent) ctx.getLayoutNavigationCollectable();
		final KeyEvent e = ctx.getKeyEvent();
		final ListOfValues lov = (ListOfValues) ctx.getComponent();
		final JTextComponent ec = lov.getJTextField();

		if (!ctx.hasBeenProcessed() && ctx.getPressed()) {
			if (ExecutionPoint.BEFORE.equals(ep)) {
				if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					if (moveToNextCell(ec)) {
						actionRight(clctcmp);
						result = true;
					}
				} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					if (moveToNextCell(ec)) {
						actionLeft(clctcmp);
						result = true;
					}
				} else if (e.getKeyCode() == KeyEvent.VK_F2) {
					if (lov.isQuickSearchPopupVisible()) {
						lov.hideQuickSearchPopup();
					} else {
						lov.runQuickSearch(false, true, false, true, true);
					}
					result = true;
				}
			} else if (ExecutionPoint.AFTER.equals(ep)) {
				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					if (e.getModifiers() == KeyEvent.SHIFT_MASK) {
						if (!consumed) {
							actionLeft(clctcmp);
							this.consumed = true;
						}
					} else {
						if (!consumed) {
							actionRight(clctcmp);
							this.consumed = true;
						}
					}
					result = true;
				} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					if (moveToNextCell(ec)) {
						actionRight(clctcmp);
						result = true;
					}
				} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					if (moveToNextCell(ec)) {
						actionLeft(clctcmp);
						result = true;
					}
				}
			}
		} else {
			// already handled by the lov
			if (ExecutionPoint.AFTER.equals(ep)) {
				if (ctx.getPressed()) {


					if (e.getKeyCode() == KeyEvent.VK_UP && !lov.isQuickSearchPopupVisible()) {
						actionUp(clctcmp);
					} else if (e.getKeyCode() == KeyEvent.VK_DOWN && !lov.isQuickSearchPopupVisible()) {
						actionDown(clctcmp);
					}

				} else {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						if (!lov.isSearchRunning() && lov.getSearchInvokeCounter() > 0) {
							if (!consumed) {
								actionCommit(clctcmp);
								this.consumed = true;
							}
						}
					}

				}

			}
			// use lov result
			result = ctx.hasBeenProcessed();
		}
		if (ExecutionPoint.BEFORE.equals(ep) && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			lov.disableInputHandler();
			result = false;
		} else if (ExecutionPoint.AFTER.equals(ep) && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			lov.enableInputHandler();
			result = true;

		}
		// get value from cell above
		if (ctx.getPressed() && ExecutionPoint.AFTER.equals(ep) && ctx.getKeyEvent().getKeyCode() == KeyEvent.VK_F8) {
			// FIXME this is hacked
			int row = ctl.getSubForm().getSubformTable().getSelectedRow();
			int column = ctl.getSubForm().getSubformTable().getSelectedColumn();
			if (row > -1 && column > -1) {
				actionF8();

				final AbstractCollectableField clcteof = (AbstractCollectableField) ctl.getSubForm().getSubformTable().getValueAt(row, column);

				final CollectableEntityField clctef = ctl.getSubForm().getSubformTable().getSubFormModel().getCollectableEntityField(ctl.getSubForm().getSubformTable().convertColumnIndexToModel(column));
				try {
					final Collectable c = Utils.getReferencedCollectable(clctef.getEntityUID(), clctef.getUID(), clcteof.getValueId());
					final CollectableComponentTableCellEditor editor = (CollectableComponentTableCellEditor) ctl.getSubForm().getSubformTable().getCellEditor();
					if (null != c) {
						((CollectableListOfValues) editor.getCollectableComponent()).acceptLookedUpCollectable(c);
					}
				} catch (UnsupportedOperationException | CommonBusinessException e1) {
					LOG.error(e1.getMessage(), e1);
					throw new NuclosFatalException(e1);
				}
				consumed = true;
				result = true;
			}
		}
		return result;
	}

	/**
	 * combobox processing
	 *
	 * @param ctx context
	 * @param ep  execution point
	 */
	private boolean processComboBox(LayoutNavigationSupportContext ctx, ExecutionPoint ep) {
		boolean result = ctx.hasBeenProcessed();
		final LabeledCollectableComponentWithVLP clctcmp = (LabeledCollectableComponentWithVLP) ctx.getLayoutNavigationCollectable();
		final KeyEvent e = ctx.getKeyEvent();
		final boolean isPopupVisible = clctcmp.getJComboBox().isPopupVisible();
		if (ExecutionPoint.BEFORE.equals(ep)) {
			if (!ctx.hasBeenProcessed() && ctx.getPressed()) {
				if (!isPopupVisible) {
					if (moveToNextCell(ctx.getComponent())) {
						if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
							actionRight(clctcmp);
							result = true;
						} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
							actionLeft(clctcmp);
							result = true;
						} else if (e.getKeyCode() == KeyEvent.VK_UP) {
							actionUp(clctcmp);
							result = true;
						} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
							result = false;
						}
					}
				}
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					//actionRight(clctcmp);
					actionCommit(clctcmp);
					result = true;
				} else if (e.getKeyCode() == KeyEvent.VK_TAB) {
					if (e.getModifiers() == KeyEvent.SHIFT_MASK) {
						actionLeft(clctcmp);
					} else {
						if (moveToNextCell(ctx.getComponent())) {
							actionRight(clctcmp);
						} else {
							actionCommit(clctcmp);
						}
					}
					result = true;
				} else if (e.getKeyCode() == KeyEvent.VK_A && e.isControlDown()) {
					final Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
					if (focusOwner instanceof JTextField) {
						((JTextField) focusOwner).selectAll();
						result = true;
					}
				} else if (e.getKeyCode() == KeyEvent.VK_F2) {
					if (isPopupVisible) {
						// FIXME the event is triggered two times, hidePopup would close the popup immediately after showup
						//clctcmp.getJComboBox().hidePopup();
					} else {
						clctcmp.getJComboBox().showPopup();
					}
					result = true;
				} else if (ctx.getKeyEvent().getKeyCode() == KeyEvent.VK_F8) {
					actionF8();
					result = true;
				}

			}

			if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * default processing
	 *
	 * @param ctx context
	 * @param ep  execution point
	 */
	private boolean processDefault(LayoutNavigationSupportContext ctx, ExecutionPoint ep) {
		boolean result = ctx.hasBeenProcessed();
		final CollectableComponent clctcmp = (CollectableComponent) ctx.getLayoutNavigationCollectable();
		final KeyEvent e = ctx.getKeyEvent();
		if (!ctx.hasBeenProcessed() && ctx.getPressed()) {
			if (ExecutionPoint.BEFORE.equals(ep)) {
				if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					if (moveToNextCell(ctx.getComponent())) {
						actionRight(clctcmp);
						result = true;
					}
				} else if (e.getKeyCode() == KeyEvent.VK_ENTER && e.getModifiers() == 0) {
					//actionRight(clctcmp);
					actionCommit(clctcmp);
					result = true;
				} else if (e.getKeyCode() == KeyEvent.VK_TAB) {
					if (e.getModifiers() == KeyEvent.SHIFT_MASK) {
						actionLeft(clctcmp);
					} else {
						if (moveToNextCell(ctx.getComponent())) {
							actionRight(clctcmp);
						} else {
							actionCommit(clctcmp);
						}
					}
					result = true;
				} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					if (moveToNextCell(ctx.getComponent())) {
						actionLeft(clctcmp);
						result = true;
					}
				} else if (e.getKeyCode() == KeyEvent.VK_UP) {
					actionUp(clctcmp);
					result = true;
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					actionDown(clctcmp);
					result = true;
				}
			}
		}

		if (ExecutionPoint.BEFORE.equals(ep) && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			result = true;
		}
		// get value from cell above
		if (ctx.getPressed() && ExecutionPoint.AFTER.equals(ep) && ctx.getKeyEvent().getKeyCode() == KeyEvent.VK_F8) {
			if (!(ctx.getComponent() instanceof FileChooserComponent) &&
					(!(ctx.getComponent() instanceof LabeledImage))
					) {
				actionF8();
				result = true;
			}
		}

		return result;
	}

	/**
	 * datechooser processing
	 *
	 * @param ctx context
	 * @param ep  execution point
	 */
	private boolean processDateChooser(LayoutNavigationSupportContext ctx, ExecutionPoint ep) {
		KeyEvent e = ctx.getKeyEvent();
		CollectableDateChooser cdc = (CollectableDateChooser) ctx.getLayoutNavigationCollectable();
		final DateChooser dc = cdc.getDateChooser();

		boolean result = processDefault(ctx, ep);
		if (!ctx.hasBeenProcessed()) {
			if (ExecutionPoint.BEFORE.equals(ep) && ctx.getPressed()) {
				if (e.getKeyCode() == KeyEvent.VK_F2) {
					if (!dc.isCalendarShowing()) {
						dc.showCalendar();
					}
					result = true;
				}
			}
		} else {
			result = ctx.hasBeenProcessed();
		}

		if (ExecutionPoint.BEFORE.equals(ep) && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			result = true;
		}
		return result;
	}


	private boolean processHyperlink(LayoutNavigationSupportContext ctx, ExecutionPoint ep) {
		KeyEvent e = ctx.getKeyEvent();
		final HyperlinkTextFieldWithButton hl = (HyperlinkTextFieldWithButton) ctx.getComponent();

		boolean result = processDefault(ctx, ep);
		if (!ctx.hasBeenProcessed()) {
			if (ExecutionPoint.BEFORE.equals(ep) && ctx.getPressed()) {
				if (e.getKeyCode() == KeyEvent.VK_F2) {
					hl.moveCursorToEndOfDocument();
					result = true;
				}
			}
		} else {
			result = ctx.hasBeenProcessed();
		}

		if (ExecutionPoint.BEFORE.equals(ep) && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			result = true;
		}
		return result;
	}

	private boolean processCheckBox(LayoutNavigationSupportContext ctx, ExecutionPoint ep) {
		final KeyEvent e = ctx.getKeyEvent();

		boolean result = processDefault(ctx, ep);
		if (ctx.getPressed()) {
			if (e.getKeyCode() == KeyEvent.VK_A && e.isControlDown()) {
				return true;
			}

		}
		if (ExecutionPoint.BEFORE.equals(ep) && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			result = true;
		}
		return result;
	}

	/**
	 * image processing
	 *
	 * @param ctx context
	 * @param ep  execution point
	 */
	private boolean processImage(LayoutNavigationSupportContext ctx, ExecutionPoint ep) {
		final KeyEvent e = ctx.getKeyEvent();

		boolean result = processDefault(ctx, ep);
		if (ctx.getPressed()) {
			if (e.getKeyCode() == KeyEvent.VK_A && e.isControlDown()) {
				return true;
			}
		}

		if (ExecutionPoint.BEFORE.equals(ep) && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			result = true;
		}
		return result;
	}

	/**
	 * textarea processing
	 *
	 * @param ctx context
	 * @param ep  execution point
	 */
	private boolean processTextArea(LayoutNavigationSupportContext ctx, ExecutionPoint ep) {
		final CollectableComponent clctcmp = (CollectableComponent) ctx.getLayoutNavigationCollectable();
		final KeyEvent e = ctx.getKeyEvent();
		final InnerTextArea ec = (InnerTextArea) ctx.getComponent();
		boolean result = ctx.hasBeenProcessed();
		if (ctx.getPressed()) {
			if (ExecutionPoint.BEFORE.equals(ep)) {
				if (moveToNextCell(ec)) {

					if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
						if (moveToNextCell(ctx.getComponent())) {
							actionRight(clctcmp);
							result = true;
						}
					} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						//actionRight(clctcmp);
						actionCommit(clctcmp);
						result = true;
					} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
						if (moveToNextCell(ctx.getComponent())) {
							actionLeft(clctcmp);
							result = true;
						}
					} else if (e.getKeyCode() == KeyEvent.VK_UP) {
						if (moveToNextCell(ctx.getComponent())) {
							actionUp(clctcmp);
							result = true;
						}

					} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
						if (moveToNextCell(ctx.getComponent())) {
							actionDown(clctcmp);
							result = true;
						}
					}
				}

				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					if (e.getModifiers() == KeyEvent.SHIFT_MASK) {
						actionLeft(clctcmp);
					} else {
						actionRight(clctcmp);
					}
					result = true;
				}
			}
		}

		if (ExecutionPoint.BEFORE.equals(ep) && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			result = true;
		}

		// get value from cell above
		if (ctx.getPressed() && ExecutionPoint.AFTER.equals(ep) && ctx.getKeyEvent().getKeyCode() == KeyEvent.VK_F8) {
			actionF8();
			result = true;
		}

		return result;
	}


	private void manageAutoNumbers() {
		final DetailsSubFormController<?, ?> ctl = getController();
		if (ctl == null) return;

		// FIXME not here, should be managed by listener in DetailsSubFormController
		AutonumberUiUtils.fixSubFormOrdering(ctl.getSubForm().getSubformTable());
	}

}
