//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar;

import javax.swing.*;

import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common2.SpringLocaleDelegate;

public class ToolBarItemAction extends ToolBarItem implements INuclosToolBarItem {
	
	/**
	 * Action property for visibility of ToolBarItemActions, if supported by item type.<br>
	 * Value: <code>java.lang.Boolean</code>
	 */
	public static final String VISIBLE = "nuclos_Visible";
	
	/**
	 * Optional colored label in menu, if supported by item type.<br>
	 * Value: <code>java.lang.String</code>
	 */
	public static final String MENU_LABEL = "nuclos_MenuLabel";
	
	/**
	 * Set the background color of menu label, if supported by item type.<br>
	 * Value: <code>java.awt.Color</code>
	 */
	public static final String MENU_LABEL_COLOR = "nuclos_MenuLabelColor";
	
	/**
	 * Set the selectable items of an object, if supported by item type.<br>
	 * Value: <code>java.lang.Object[]</code>
	 */
	public static final String ITEM_ARRAY = "nuclos_ItemArray";
	
	/**
	 * Set the selected index of an object array, if supported by item type.<br>
	 * Value: <code>java.lang.Integer</code>
	 */
	public static final String ITEM_SELECTED_INDEX = "nuclos_ItemSelectedIndex";
	
	/**
	 * Disable the action, if supported by item type<br>
	 * Value: <code>java.lang.Boolean</code>
	 */
	public static final String ACTION_DISABLED = "nuclos_ActionDisabled";
	
	/**
	 * Dummy item for Toolbar only. Hidden in Menu
	 */
	public interface ToolBarItemDummy {
	}
	
	private static SpringLocaleDelegate locale = SpringLocaleDelegate.getInstance();
	
	private final String resourceIdLabel;
	private final String resourceIdToolTip;
	private final Icon icon;
	
	public ToolBarItemAction(ToolBarItem item, String resourceIdLabel, Icon icon) {
		this(item, resourceIdLabel, null, icon);
	}

	public ToolBarItemAction(ToolBarItem item, String resourceIdLabel, String resourceIdToolTip, Icon icon) {
		super(item.getType(), item.getKey());
		this.resourceIdLabel = resourceIdLabel;
		this.resourceIdToolTip = resourceIdToolTip;
		this.icon = icon;
	}
	
	@Override
	public String getLabel() {
		if (resourceIdLabel != null) {
			return locale.getResource(resourceIdLabel, resourceIdLabel);
		}
		return null;
	}
	
	@Override
	public String getTooltip() {
		if (resourceIdToolTip != null) {
			return locale.getResource(resourceIdToolTip, resourceIdToolTip);
		}
		return getLabel();
	}
	
	@Override
	public Icon getIcon() {
		return icon;
	}
	
	@Override
	public <T extends Action> T init(Action action) {
		if (resourceIdLabel != null) {
			action.putValue(Action.NAME, locale.getResource(resourceIdLabel, resourceIdLabel));
		}
		if (resourceIdToolTip != null) {
			action.putValue(Action.SHORT_DESCRIPTION, locale.getResource(resourceIdToolTip, resourceIdToolTip));
		}
		if (icon != null) {
			action.putValue(Action.SMALL_ICON, icon);
		}
		return (T) action;
	}

}
