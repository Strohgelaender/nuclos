package org.nuclos.client.ui;

import java.io.Closeable;

import javax.swing.*;

import org.nuclos.client.command.ResultListener;
import org.nuclos.client.main.mainframe.IconResolver;
import org.nuclos.common.collection.Pair;

public interface ITopController extends Closeable {

	ImageIcon getIconUnsafe();
	
	Pair<IconResolver, String> getIconAndResolver();
	
	/**
	 * asks the user to save the current record if necessary, so that it can be abandoned afterwards.
	 * @param rl
	 */
	void askAndSaveIfNecessary(ResultListener<AskAndSaveResult> rl);
}
