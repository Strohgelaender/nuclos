//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.matrix;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.RowSorter.SortKey;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MatrixController;
import org.nuclos.client.scripting.ScriptEvaluator;
import org.nuclos.client.scripting.context.MatrixControllerScriptContext;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.LangUtils;

public class MatrixTableModel extends DefaultTableModel {
	
	Map<Collectable, Map<Collectable, List<Object>>> mpContent;
	
	Map<Collectable, List<Object>> mpRemovedObjects = new HashMap<Collectable, List<Object>>();

	private Collection<Object> marked = Collections.emptySet();
	
	Logger LOG = Logger.getLogger(getClass());
	
	List<SortKey> sortKeys;
	
	String groupField;
	
	boolean fixed;
	NuclosScript editEnabledScript;
	MatrixController matrixController;
	
	public MatrixTableModel(MatrixController mc) {
		this.matrixController = mc;
		mpContent = new HashMap<Collectable, Map<Collectable,List<Object>>>();
		sortKeys = new ArrayList<SortKey>();
		initSortKeys();		
	}
	
	public void setEditEnabledScript(NuclosScript script) {
		this.editEnabledScript = script;
	}
	
	public void setFixedModel(boolean fixed) {
		this.fixed = fixed;
	}
	
	public void setData(Map<Collectable, Map<Collectable, List<Object>>> mpContent) {		
		this.mpContent = mpContent;
		fireTableDataChanged();
	}
	
	public void setSortKeys(List<SortKey> sortKeys) {
		this.sortKeys = sortKeys;
	}
	
	public List<SortKey> getSortKeys() {
		return this.sortKeys;
	}
	
	public void sort(int column, MatrixTableModel modelExtern) {
		if(sortKeys.size() == 0)
			initSortKeys();
		for(SortKey key : sortKeys) {
			if(key.getColumn() == column) {
				SortOrder order = key.getSortOrder();
				switch (order) {
				case UNSORTED:
					setSortOrder(column, SortOrder.ASCENDING, false);
					break;
				case ASCENDING:
					setSortOrder(column, SortOrder.DESCENDING, false);
					break;
				case DESCENDING:
					setSortOrder(column, SortOrder.UNSORTED, false);
					break;					
				default:
					setSortOrder(column, SortOrder.UNSORTED, false);
					break;
				}
			}
		}
		reSort(modelExtern);		
	}
	
	public boolean isSortingAllowed(int colIndex) {
		boolean sorting = true;
		
		int counter = 0;
		for(Collectable cl : mpContent.keySet()) {
			if(colIndex == counter++) {
				MatrixCollectable mc = (MatrixCollectable)cl;
				if(mc.getEntityFieldMetaDataVO() != null) {
					FieldMeta<Object> voField = mc.getEntityFieldMetaDataVO();
					if(voField.getDataType().equals("org.nuclos.common.NuclosImage") || voField.getDataType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile")) {
						return false;
					}
				}
			}
		}
		
		return sorting;
	}
	
	public void resetSortOrder(int colIndex) {
		for(SortKey key : sortKeys) {
			if(key.getColumn() == colIndex) continue;
			sortKeys.set(key.getColumn(), new SortKey(key.getColumn(), SortOrder.UNSORTED));
		}
	}
	
	protected void reSort(MatrixTableModel modelExtern) {
		int sortGroup = -1;
		SortOrder order = SortOrder.UNSORTED;
		for(SortKey key : sortKeys) {
			if(key.getSortOrder() != SortOrder.UNSORTED) {
				sortGroup = key.getColumn();
				order = key.getSortOrder();
				break;
			}
		}
		boolean ascending = order == SortOrder.ASCENDING;

		if(sortGroup == -1) return;
		
		SortedMap<Integer, Integer> mpReorder = new TreeMap<Integer, Integer>();
				 
		int groupIndex = 0;
		for(Collectable clGroup : mpContent.keySet()) {
			if(groupIndex++ == sortGroup) {
				Map<Collectable, List<Object>> mp = mpContent.get(clGroup);
				Collectable clColumn = mp.keySet().iterator().next();
				List<Object> lstColumn = mp.get(clColumn);
				
				List<Object> lstSorted = CollectionUtils.sorted(lstColumn, (Object o1, Object o2) -> {
					MatrixCollectable m1 = (MatrixCollectable)o1;
					MatrixCollectable m2 = (MatrixCollectable)o2;

					int compare = m1.compareTo(m2, ascending);
					if (!ascending) {
						compare *= -1;
					}
					return compare;
				});
				
				mp.put(clColumn, lstSorted);
				
				int x = 0;
				for(Object obj : lstColumn) {
					MatrixCollectable source = (MatrixCollectable)obj;
					int y = lstSorted.indexOf(source);
					if(y >= 0) {
						mpReorder.put(x++, y);						
					}
				
				}
				
				break;
			}
		}
		
		groupIndex = 0;
		for(Collectable clGroup : mpContent.keySet()) {
			if(groupIndex++ != sortGroup) {
				Map<Collectable, List<Object>> mp = mpContent.get(clGroup);
				for(Collectable cl : mp.keySet()) {
					List<Object> lstOld = mp.get(cl);
					List<Object> lstNew = new ArrayList<Object>(lstOld);
					for(Integer iSource : mpReorder.keySet()) {
						Object objSource = lstOld.get(iSource);
						int newIndex = mpReorder.get(iSource);
						lstNew.set(newIndex, objSource);
					}					
					
					mp.put(cl, lstNew);
				}
			}
		}
		
		if(modelExtern != null) {
			for(Collectable clGroup : modelExtern.getContent().keySet()) {
				if(groupIndex++ != sortGroup) {
					Map<Collectable, List<Object>> mp = modelExtern.getContent().get(clGroup);
					for(Collectable cl : mp.keySet()) {
						List<Object> lstOld = mp.get(cl);
						List<Object> lstNew = new ArrayList<Object>(lstOld);
						for(Integer iSource : mpReorder.keySet()) {
							Object objSource = lstOld.get(iSource);
							int newIndex = mpReorder.get(iSource);
							lstNew.set(newIndex, objSource);
						}					
						
						mp.put(cl, lstNew);
					}
				}
			}
		}
		
		
		fireTableDataChanged();
	}
	
	public void setSortOrder(int column, SortOrder sortOrder, boolean sortImmediately) {
		for(SortKey key : sortKeys) {
			if(key.getColumn() == column) {
				sortKeys.set(column, new SortKey(column, sortOrder));
			}
		}		
		if(sortImmediately)
			reSort(null);

	}
	
	protected void initSortKeys() {
		for(int i = 0; i < mpContent.size(); i++) {
			sortKeys.add(new SortKey(i, SortOrder.UNSORTED));
		}
	}
	
	
	@Override
	public boolean isCellEditable(int row, int column) {
		if(fixed && column == 0) return false;
		MatrixCollectable mc = (MatrixCollectable) getValueAt(row, column);
		if (mc == null || (mc.getEntityFieldMetaDataVO() != null && "org.nuclos.common.NuclosImage".equals(mc.getEntityFieldMetaDataVO().getDataType()))) {
			return false;
		}

		if (mc.getYgroupkey() != null && mc.getYgroupkey().getIdenticalFields().contains(mc.getField())) {
			return false;
		}

		if(editEnabledScript != null) {
			final UID sEntity;
			final UID sField;
			if(fixed) {				
				sEntity = matrixController.getMatrixComponent().getEntityY();
				sField = mc.getField();
			}
			else {
				sEntity = matrixController.getMatrixComponent().getMatrixEntity();
				sField = matrixController.getMatrixComponent().getEntityMatrixValueField();
			}
			
			MatrixControllerScriptContext context = new MatrixControllerScriptContext(matrixController, matrixController.getSelectedCollectable(), (Collectable) getValueAt(row, column), sEntity, sField, row);
			Object o = null;
			try {
				o = ScriptEvaluator.getInstance().eval(editEnabledScript, context);
			} catch (InvocationTargetException e) {
				LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
				o = true;
			} catch (Exception e) {
				LOG.warn("Failed to evaluate script expression: " + e, e);
				o = true;
			}
			if (o instanceof Boolean) {
				return (Boolean)o;
			}
			
		}
		return true;
	}
	
	public Map<Collectable, Map<Collectable, List<Object>>> getContent() {
		return this.mpContent;
	}
	
	public Map<Collectable, List<Object>> getRemovedObjects() {
		return mpRemovedObjects;
	}

	public List<Collectable> getCategorieHeader() {
		List<Collectable> lHeader = new ArrayList<Collectable>();
		
		for(Collectable key : mpContent.keySet()) {
			lHeader.add(key);
		}
		
		return lHeader;
	}

	@Override
	public int getRowCount() {
		int rowCount = 0;
		if(mpContent == null) {
			return rowCount;
		}
		
		for(Collectable keyCategorie : mpContent.keySet()) {
			Map<Collectable, List<Object>> mpCategorie = mpContent.get(keyCategorie);
			if(!mpCategorie.isEmpty()) {
				
				Collectable key = mpCategorie.keySet().iterator().next();
				List<Object> lst = mpCategorie.get(key);				
				return lst.size();
			}
		}
		
		return rowCount;		
	}

	@Override
	public int getColumnCount() {
		int colCount = 0;
		if(mpContent.keySet().isEmpty())
			return colCount;
		
		for(Collectable clCategorie : mpContent.keySet()) {
			Map<Collectable, List<Object>> mpGroups = mpContent.get(clCategorie);
			if(mpGroups != null)
				colCount += mpGroups.size();			
		}
		return colCount;
	}

	@Override
	public String getColumnName(int column) {
		String colName = "default";
		int index = 0;
		for(Collectable keyCategorie : mpContent.keySet()) {
			Map<Collectable, List<Object>> mpGroup = mpContent.get(keyCategorie);
			for(Collectable keyGroup : mpGroup.keySet()) {
				if(column == index)
					return keyGroup.getValue(UID.UID_NULL).toString();
				index++;
			}
		}
		return colName;
	}
	
	public void insertNewRow() {		
		for(Collectable keyCategorie : mpContent.keySet()) {
			Map<Collectable, List<Object>> mpGroup = mpContent.get(keyCategorie);
			for(Collectable keyGroup : mpGroup.keySet()) {
				if(fixed) {
					MatrixCollectable cl = (MatrixCollectable)keyCategorie;
					if(cl.getEntityFieldMetaDataVO() == null) { // row indicator
						MatrixCollectable mc = new MatrixCollectable(-100L, "");
						mpGroup.get(keyGroup).add(mc);
					}
					else if(cl.getEntityFieldMetaDataVO().getDataType().equals("java.lang.Boolean")) {
						String sDefault = cl.getEntityFieldMetaDataVO().getDefaultValue();
						MatrixCollectable mc = new MatrixCollectable(Boolean.FALSE);
						if(sDefault != null && "ja".equals(sDefault)) {
							mc = new MatrixCollectable(Boolean.TRUE);
						}
						
						mc.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());					
						
						mc.setField(cl.getField());
						mpGroup.get(keyGroup).add(mc);
					}
					else if(cl.getEntityFieldMetaDataVO().getDataType().equals("java.util.Date") || cl.getEntityFieldMetaDataVO().getDataType().equals("org.nuclos.common2.InternalTimestamp")) {
						String sDefault = cl.getEntityFieldMetaDataVO().getDefaultValue();
						MatrixCollectable mc = new MatrixCollectable(null);
						if(sDefault != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
							try {
								mc = new MatrixCollectable(sdf.parseObject(sDefault));
							} catch (ParseException e) {
								LOG.warn(e.getMessage());
							}
						}
						
						mc.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());					
						
						mc.setField(cl.getField());
						mpGroup.get(keyGroup).add(mc);
					}
					else if(cl.getEntityFieldMetaDataVO().getDataType().equals("java.lang.Integer")) {
						String sDefault = cl.getEntityFieldMetaDataVO().getDefaultValue();
						MatrixCollectable mc = new MatrixCollectable(null);
						if(sDefault != null ) {							
							try {
								mc = new MatrixCollectable(Integer.parseInt(sDefault));
							} catch (Exception e) {
								LOG.warn(e.getMessage());
							}
						}
						
						mc.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());					
						
						mc.setField(cl.getField());
						mpGroup.get(keyGroup).add(mc);
					}
					else {
						MatrixCollectable mc = new MatrixCollectable(null);
						mc.setEntityFieldMetaDataVO(cl.getEntityFieldMetaDataVO());
						mc.setField(cl.getField());
						mpGroup.get(keyGroup).add(mc);
					}
					
				}
				else {
					mpGroup.get(keyGroup).add(new MatrixCollectable(null));
				}
			}
		}
		this.fireTableDataChanged();
	}
	
	public void cloneRow(int row) {		
		
		for(Collectable keyCategorie : mpContent.keySet()) {
			Map<Collectable, List<Object>> mpGroup = mpContent.get(keyCategorie);
			for(Collectable keyGroup : mpGroup.keySet()) {				
				List<Object> lstGroup = mpGroup.get(keyGroup);
				MatrixCollectable value = (MatrixCollectable) lstGroup.get(row);
				MatrixCollectable mc = new MatrixCollectable(value.getValue(UID.UID_NULL));
				mc.setField(value.getField());
				mc.setEntityFieldMetaDataVO(value.getEntityFieldMetaDataVO());
				lstGroup.add(mc);
			}
		}
		this.fireTableDataChanged();
	}
	
	public void removeRow(int row, MatrixTableModel modelSync) {
		boolean first = true;
		MatrixCollectable removedKey = null;
		int rowindicator = 0;
		for(Collectable keyCategorie : mpContent.keySet()) {			
			if(rowindicator == 0 && fixed) {
				rowindicator++;
				Map<Collectable, List<Object>> mpGroup = mpContent.get(keyCategorie);
				Collectable ct = mpGroup.keySet().iterator().next();
				mpGroup.get(ct).remove(row);
				continue;
			}
			Map<Collectable, List<Object>> mpGroup = mpContent.get(keyCategorie);
			for(Collectable keyGroup : mpGroup.keySet()) {
				List<Object> lstGroup = mpGroup.get(keyGroup);
				MatrixCollectable value = (MatrixCollectable) lstGroup.get(row);	
				value.markAsRemoved();				
				if(first) {
					removedKey = value;					
					mpRemovedObjects.put(removedKey, new ArrayList<Object>());
					first = false;
				}
				else {
					mpRemovedObjects.get(removedKey).add(value);
				}
				lstGroup.remove(row);
			}
		}
		
		if(modelSync != null) {
			for(Collectable keyCategorie : modelSync.getContent().keySet()) {
				Map<Collectable, List<Object>> mpGroup = modelSync.getContent().get(keyCategorie);
				for(Collectable keyGroup : mpGroup.keySet()) {
					List<Object> lstGroup = mpGroup.get(keyGroup);
					MatrixCollectable value = (MatrixCollectable) lstGroup.get(row);
					
					value.markAsRemoved();				
					mpRemovedObjects.get(removedKey).add(value);
				}
			}
		}

		for(Collectable cl : new ArrayList<>(mpRemovedObjects.keySet())) {
			MatrixCollectable mc = (MatrixCollectable)cl;
			if(mc.getId() == null) {
				mpRemovedObjects.remove(mc);
			}
		}

		this.fireTableDataChanged();
	}

	@Override
	public Object getValueAt(int row, int column) {
		String sValue = "NA";
		int colIndex = 0;
		for(Collectable keyCategorie : mpContent.keySet()) {
			Map<Collectable, List<Object>> mpGroup = mpContent.get(keyCategorie);
			for(Collectable keyGroup : mpGroup.keySet()) {
				List<Object> mpData = mpGroup.get(keyGroup);
				if(column == colIndex) {
					MatrixCollectable collect = (MatrixCollectable) mpData.get(row);
					collect.setMarked(marked.contains(collect.getYId()));
					if(collect.isRemoved())
						return null;
					return collect;
				}
				colIndex++;
			}
		}
		return sValue;
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		MatrixCollectable mcNew = aValue instanceof MatrixCollectable ?
				(MatrixCollectable)aValue : new MatrixCollectable(aValue);

		List<Object> mpData = getCellsPerRowForColumn(column);
		List<Integer> rowsToBeChanged = getRowsOfSameGroup(mpData, row);

		if (rowsToBeChanged != null) {
			// NUCLOS-7261 Transfer value to all cells with the same value within the group
			for (Integer r : rowsToBeChanged) {
				MatrixCollectable mc = (MatrixCollectable)mpData.get(r);
				mc.value = mcNew.value;
				mc.markAsModified();
			}
		} else {
			mpData.set(row, mcNew);
		}

		fireTableDataChanged();		
	}

	private List<Integer> getRowsOfSameGroup(List<Object> mpData, int row) {
		if (mpData == null || mpData.size() <= row) {
			return null;
		}
		Object oldValue = mpData.get(row);
		if (!(oldValue instanceof MatrixCollectable)) {
			return null;
		}

		MatrixCollectable mcOld = (MatrixCollectable)oldValue;
		if (mcOld.getYgroupkey() == null) {
			return Collections.singletonList(row);
		}

		List<Integer> result = new ArrayList<>();
		for (Object data : mpData) {
			if (data instanceof MatrixCollectable) {
				MatrixCollectable mcOld2 = (MatrixCollectable) data;
				if (!LangUtils.equal(mcOld.value, mcOld2.value) || mcOld2.getYgroupkey() == null) {
					continue;
				}
				if (mcOld2.getYgroupkey().getGroup() == mcOld.getYgroupkey().getGroup()) {
					result.add(mpData.indexOf(data));
				}
			}
		}
		return result;
	}

	private List<Object> getCellsPerRowForColumn(int column) {
		int colIndex = 0;
		for(Collectable keyCategorie : mpContent.keySet()) {
			Map<Collectable, List<Object>> mpGroup = mpContent.get(keyCategorie);
			for(Collectable keyGroup : mpGroup.keySet()) {
				if (column == colIndex++) {
					return mpGroup.get(keyGroup);
				}
			}
		}
		return Collections.emptyList();
	}

	public List<Integer> getRowsOfSameGroupForColumn(int row, int column) {
		return getRowsOfSameGroup(getCellsPerRowForColumn(column), row);
	}

	@Override
	public void fireTableDataChanged() {
		super.fireTableDataChanged();
		
	}

	public void setMarked(final Collection<Object> marked) {
		this.marked = marked;
	}
}
