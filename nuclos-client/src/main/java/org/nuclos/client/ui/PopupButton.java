package org.nuclos.client.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.apache.log4j.Logger;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction.ToolBarItemDummy;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemGroupComponent;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.ToolBarItem.Type;
import org.nuclos.common2.StringUtils;

public class PopupButton extends JToggleButton implements PopupMenuListener, ActionListener {
	
	private static final Logger LOG = Logger.getLogger(PopupButton.class);

	public final JPopupMenu popupMenu;

	private long hideTime = 0l;
	
	private final ToolBarItemGroupComponent groupComponent;

	private final MouseListener itemMouseListener = new MouseAdapter() {
		@Override
		public void mouseReleased(MouseEvent e) {
			popupMenu.setVisible(false);
		}
	};

	public PopupButton() {
		this("");
	}
	
	public PopupButton(String text) {
		super(text);
		setSelected(false);
		setFocusable(false);
		setHorizontalAlignment(SwingConstants.LEFT);

		popupMenu = new JPopupMenu() {

			@Override
			protected JMenuItem createActionComponent(Action a) {
				JMenuItem result = super.createActionComponent(a);
				result.addMouseListener(itemMouseListener);
				return result;
			}
		};

		popupMenu.addPopupMenuListener(this);
		addActionListener(this);
		
		groupComponent = new ToolBarItemGroupComponent(this) {
			
			@Override
			protected JComponent[] createStatic(ToolBarItem item) {
				switch (item.getType()) {
				case SEPARATOR:
					boolean isFirstVisibleSeparator = false;
					for (Component c : popupMenu.getComponents()) {
						if (c.isVisible()) {
							if (c instanceof JSeparator) {
								isFirstVisibleSeparator = true;
							}
							break;
						}
					}
					if (isFirstVisibleSeparator) {
						return null;
					} else {
						return new JComponent[] {new JPopupMenu.Separator()};
					}
				default:
					return null;
				}
			}
			
			@Override
			protected JComponent[] create(final ToolBarItem item, final Action action) {
				final JComponent[] result;
				switch (item.getType()) {
				case ACTION:
				case TOGGLE_ACTION:
					result = new JComponent[] {new JMenuItem(action)};
					break;
				case CHECKBOX:
					result = new JComponent[] {new JCheckBoxMenuItem(action)};
					break;
				case RADIOBUTTON:
					result = new JComponent[] {new JRadioButtonMenuItem(action)};
					break;
				case PR_ACTION:
				case PSR_ACTION:
				case PSD_ACTION:
					Object[] items = (Object[]) action.getValue(ToolBarItemAction.ITEM_ARRAY);
					Integer iSelected = (Integer) action.getValue(ToolBarItemAction.ITEM_SELECTED_INDEX);
					final List<AbstractButton> buttons = new ArrayList<AbstractButton>();
					final ButtonGroup group = new ButtonGroup();
					final JMenu submenu = new JMenu((String) action.getValue(Action.NAME));
					submenu.setIcon((Icon) action.getValue(Action.SMALL_ICON));
					generateMenuButtons(buttons, items, group, iSelected, action, item.getType());
					action.addPropertyChangeListener(new PropertyChangeListener() {
						@Override
						public void propertyChange(PropertyChangeEvent evt) {
							if (ToolBarItemAction.ITEM_SELECTED_INDEX.equals(evt.getPropertyName())) {
								Integer newIndex = (Integer) evt.getNewValue();
								if (newIndex == null) {
									group.clearSelection();
								} else {
									try {
										buttons.get(newIndex).setSelected(true);
									} catch (Exception ex) {
										// ignore...
									}
								}
							} else if (ToolBarItemAction.ITEM_ARRAY.equals(evt.getPropertyName())) {
								if (item.getType() == Type.PSR_ACTION ||
									item.getType() == Type.PSD_ACTION) {
									submenu.removeAll();
									Object[] items = (Object[]) evt.getNewValue();
									Integer iSelected = (Integer) action.getValue(ToolBarItemAction.ITEM_SELECTED_INDEX);
									generateMenuButtons(buttons, items, group, iSelected, action, item.getType());
									for (int i = 0; i < buttons.size(); i++) {
										AbstractButton button = buttons.get(i);
										submenu.add(button);
									}
								}
							}
						}
					});
					
					if (item.getType() == Type.PSR_ACTION ||
						item.getType() == Type.PSD_ACTION) {
						for (int i = 0; i < buttons.size(); i++) {
							AbstractButton button = buttons.get(i);
							submenu.add(button);
						}
						result = new JComponent[] {submenu};
					} else {
						result = new JComponent[buttons.size()];
						for (int i = 0; i < buttons.size(); i++) {
							AbstractButton button = buttons.get(i);
							result[i] = button;
						}
					}
					break;
				default:
					result = null;
					break;
				}
				if (result != null) {
					for (JComponent c : result) {
						if (Boolean.FALSE.equals(action.getValue(ToolBarItemAction.VISIBLE))) {
							c.setVisible(false);
						} else {
							c.setVisible(true);
						}
					}
					action.addPropertyChangeListener(new PropertyChangeListener() {
						@Override
						public void propertyChange(PropertyChangeEvent evt) {
							if (ToolBarItemAction.VISIBLE.equals(evt.getPropertyName())) {
								for (final JComponent c : result) {
									if (Boolean.FALSE.equals(evt.getNewValue())) {
										c.setVisible(false);
									} else {
										c.setVisible(true);
									}
								}
							}
						}
					});
				}
				return result;
			}
			
			@Override
			protected void invalidate() {
				if (popupMenu.getComponentCount() > 0) {
					try {
						if (popupMenu.getComponent(0) instanceof JSeparator) {
							popupMenu.remove(0);
						}
					} catch (Exception ex) {
						LOG.error("Removing of unnecessary beginning separator failed", ex);
					}
				}
			}

			@Override
			protected boolean isHideLabelPossible() {
				return false;
			};
		};
		this.putClientProperty(ToolBarItemGroupComponent.PROPERTY_KEY, groupComponent);
	}

	public void addSeparator() {
		popupMenu.addSeparator();
	}

	public JMenuItem add(JMenuItem menuItem) {
		menuItem.addMouseListener(itemMouseListener);
		return popupMenu.add(menuItem);
	}

	@Override
	public Component add(Component comp) {
		comp.addMouseListener(itemMouseListener);
		return popupMenu.add(comp);
	}

	@Override
	public Component add(Component comp, int index) {
		comp.addMouseListener(itemMouseListener);
		popupMenu.insert(comp, index);
		return comp;
	}

	@Override
	public void remove(Component comp) {
		super.remove(comp);
		popupMenu.remove(comp);
	}

	@Override
	public Dimension getPreferredSize() {
		Dimension defaultPrefSize = super.getPreferredSize();
		return new Dimension(
				defaultPrefSize.width
				+ Icons.getInstance().getArrowButtonX().getIconWidth()
				+ (StringUtils.looksEmpty(getText())?-4:1), 
				defaultPrefSize.height
				+ (StringUtils.looksEmpty(getText())?Icons.getInstance().getArrowButtonX().getIconHeight()-2:0));
	}

	@Override
	public Dimension getMaximumSize() {
		Dimension defaultMaxSize = super.getMaximumSize();
		return new Dimension(
				defaultMaxSize.width
				+ Icons.getInstance().getArrowButtonX().getIconWidth()
				+ (StringUtils.looksEmpty(getText())?-4:1), 
				defaultMaxSize.height
				+ (StringUtils.looksEmpty(getText())?Icons.getInstance().getArrowButtonX().getIconHeight()-2:0));
	}

	@Override
	public Dimension getMinimumSize() {
		Dimension defaultMinSize = super.getMinimumSize();
		return new Dimension(
				defaultMinSize.width
				+ Icons.getInstance().getArrowButtonX().getIconWidth()
				+ (StringUtils.looksEmpty(getText())?-4:1), 
				defaultMinSize.height
				+ (StringUtils.looksEmpty(getText())?Icons.getInstance().getArrowButtonX().getIconHeight()-2:0));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!popupMenu.isShowing()) {
			if (hideTime + 200l < System.currentTimeMillis()) {
				popupMenu.show(PopupButton.this, 0, PopupButton.this.getHeight());
			} else {
				PopupButton.this.setSelected(false);
			}
		}
		else {
			popupMenu.setVisible(false);
		}
	}

	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
		setSelected(false);
		hideTime = System.currentTimeMillis();
	}

	@Override
	public void popupMenuCanceled(PopupMenuEvent e) {
	}

	@Override
	public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
		for (int i = 0; i < popupMenu.getComponentCount(); i++) {
			Component c = popupMenu.getComponent(i);
			if (c instanceof AbstractButton) {
				AbstractButton btn = (AbstractButton) c;
				if (btn.getAction() != null) {
					btn.setEnabled(btn.getAction().isEnabled());
				}
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Graphics2D g2 = (Graphics2D) g;

		ImageIcon bg = Icons.getInstance().getArrowButtonX();
		ImageIcon ar = Icons.getInstance().getArrowButtonDown();

		int xBg = getWidth() - 2 - bg.getIconWidth();
		int yBg = 3;
		int xAr = xBg + (bg.getIconWidth() - ar.getIconWidth()) / 2;
		int yAr = yBg + (bg.getIconHeight() - ar.getIconHeight()) / 2;

		g2.drawImage(bg.getImage(), xBg, yBg, null);
		g2.drawImage(ar.getImage(), xAr, yAr, null);
	}


	public int getItemCount() {
		return popupMenu.getComponentCount();
	}
	
	private static void generateMenuButtons(final List<AbstractButton> buttons, Object[] items, ButtonGroup group, Integer iSelected, final Action action, Type type) {
		for (AbstractButton button : buttons) {
			group.remove(button);
		}
		buttons.clear();
		if (items != null) {
			for (int i = 0; i < items.length; i++) {
				final int index = i;
				Object o = items[i];
				if (o instanceof ToolBarItemDummy) {
					continue;
				}
				AbstractButton button;
				switch (type) {
				case RADIOBUTTON:
				case PSR_ACTION:
				case PR_ACTION:
					button = new JRadioButtonMenuItem();
					if (iSelected != null && iSelected.equals(i)) {
						button.setSelected(true);
					}
					group.add(button);
					break;
				case ACTION:
				case TOGGLE_ACTION:
				case PSD_ACTION:
					button = new JMenuItem();
					break;
				default:
					return;
				}
				button.setAction(new AbstractAction(o.toString()) {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (!Boolean.TRUE.equals(action.getValue(ToolBarItemAction.ACTION_DISABLED))) {
							action.actionPerformed(new ActionEvent(this, index, "selected"));
						}
					}
				});
				buttons.add(button);
			}
		}
	}
}
