package org.nuclos.client;

import java.awt.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.common2.LangUtils;

public class FullScreenTest {

	public static void main(String[] args) {
        if (isMacOSX()) {
            System.setProperty(
                    "com.apple.mrj.application.apple.menu.about.name",
                    "Full Screen Test");
        }
        JFrame frame = new JFrame("Full Screen Test");
        JMenuBar menu = new JMenuBar();
        menu.add(new JLabel("Full Screen Test Menu Bar"));
        frame.setJMenuBar(menu);
        frame.getContentPane().setLayout(new FlowLayout(FlowLayout.LEFT));
        frame.getContentPane().add(new JLabel("Full Screen Test Content Pane"));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(600, 400);
        if (isMacOSX()) {
            enableFullScreenMode(frame);
        }
        frame.setVisible(true);
    }
 
    public static void enableFullScreenMode(final Window window) {
        String className = "com.apple.eawt.FullScreenUtilities";
        String methodName = "setWindowCanFullScreen";
 
        try {
            Class<?> clazz = LangUtils.getClassLoaderThatWorksForWebStart().loadClass(className);
            Method method = clazz.getMethod(methodName, new Class<?>[] {
                    Window.class, boolean.class });
            method.invoke(null, window, true);
            
            InvocationHandler handler = new InvocationHandler() {
				@Override
				public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
					// do something
					return null;
				}
            };
            
            Class<?> clazz2 = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.FullScreenListener");
            Object proxyObject = Proxy.newProxyInstance(LangUtils.getClassLoaderThatWorksForWebStart(), new Class[] {clazz2}, handler);
            
            method = clazz.getMethod("addFullScreenListenerTo", new Class<?>[] {
                    Window.class, clazz2 });
            method.invoke(null, window, proxyObject);
             
        } catch (Exception e) {
        	Logger.getLogger(FullScreenTest.class).error("Full screen mode is not supported", e);
        }
    }
 
    private static boolean isMacOSX() {
        return System.getProperty("os.name").indexOf("Mac OS X") >= 0;
    }
	
}
