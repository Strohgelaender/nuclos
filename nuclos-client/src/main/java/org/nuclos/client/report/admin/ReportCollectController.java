//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.report.admin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.report.ReportDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.model.CollectableTableModel;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.labeled.LabeledTextField;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collect.exception.CollectableFieldValidationException;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common.report.valueobject.ReportVO.OutputType;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.SystemUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonIOException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * <code>MasterDataCollectController</code> for reports.
 * todo: entity 'template' should get an own collect controller, as it is more or less a dummy entity (no fields, just one record)
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class ReportCollectController extends MasterDataCollectController<UID> {
	
	// former Spring injection
	
	private ReportFacadeRemote reportFacadeRemote;
	
	private ReportDelegate reportDelegate;
	
	// end of former Spring injection

	//private final JButton btnShowResultInExplorer = new JButton();
	private final JButton btnPreview = new JButton();
	private final JButton btnRun = new JButton();
	private final JButton btnMakeTreeRoot = new JButton();
	private final JButton btnExportFile = new JButton();

	private final EntityMeta<UID> outputEntity;
	private final FieldMeta<?> outputReferencingField;
	private final EntityMeta<UID> subreportEntity;
	private final FieldMeta<?> subreportReferencingField;
	private ReportVO.OutputType outputtype = ReportVO.OutputType.SINGLE;
	// Controls which are shown or hidden, depending on the output type of the report
	private LabeledCollectableComponentWithVLP clctcmbbxDataSource = null;
	private LabeledTextField clctlabelDataSource = null;
	private List<JComponent> compsWithRuleClass = new ArrayList<>();
	private JComponent pnlAttach = null;
	private JComponent pnlFileChoosers = null;

	// Backups from table columns, which are shown or hidden, depending on the output type of the report
	private TableColumn tablecolumnDataSource = null;
	private TableColumn tablecolumnSourceFile = null;
	private TableColumn tablecolumnParameter = null;
	private TableColumn tablecolumnFilename = null;
	private TableColumn tablecolumnFormat = null;
	private TableColumn tablecolumnAttach = null;
	
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public ReportCollectController(UID entityUid, MainFrameTab tabIfAny) {
		super(entityUid, tabIfAny, null);

		if (entityUid.equals(E.REPORT.getUID())) {
			outputEntity = E.REPORTOUTPUT;
			outputReferencingField = E.REPORTOUTPUT.parent;
			subreportEntity = E.SUBREPORT;
			subreportReferencingField = E.SUBREPORT.reportoutput;
		}
		else if (entityUid.equals(E.FORM.getUID())) {
			outputEntity = E.FORMOUTPUT;
			outputReferencingField = E.FORMOUTPUT.parent;
			subreportEntity = E.SUBFORM;
			subreportReferencingField = E.SUBFORM.formoutput;
		}
		else
			throw new IllegalArgumentException();
		
//		setupDetailsToolBar();
		
		clctcmbbxDataSource = (LabeledCollectableComponentWithVLP) getFirstComponent(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.datasource.getUID() : E.FORM.datasource.getUID());
		clctlabelDataSource = (LabeledTextField) UIUtils.findJComponent(getDetailsPanel(), getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.datasource.getUID().getStringifiedDefinition() : E.FORM.datasource.getUID().getStringifiedDefinition());
		pnlFileChoosers = (JComponent) UIUtils.findJComponent(getDetailsPanel(), "filechoosers");
		pnlAttach = (JComponent) UIUtils.findJComponent(getDetailsPanel(), "attach");
		
		setReportType(getEntityUid().equals(E.REPORT.getUID()) ? ReportType.REPORT : ReportType.FORM);

		if (!SecurityCache.getInstance().isSuperUser()) {
			// only visible for Superusers...
			UIUtils.findAllJComponents(compsWithRuleClass, getDetailsPanel(), null, getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.withRuleClass.getUID().getStringifiedDefinition() : E.FORM.withRuleClass.getUID().getStringifiedDefinition());
			UIUtils.findAllJComponents(compsWithRuleClass, getSearchPanel(), null, getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.withRuleClass.getUID().getStringifiedDefinition() : E.FORM.withRuleClass.getUID().getStringifiedDefinition());
			for (JComponent jc : compsWithRuleClass) {
				jc.setVisible(false);
			}
		}
	
		final LabeledCollectableComponentWithVLP cmbbxOutputType = (LabeledCollectableComponentWithVLP) getFirstComponent(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.outputtype.getUID() : E.FORM.outputtype.getUID());
		if (cmbbxOutputType != null) {
			cmbbxOutputType.getModel().addCollectableComponentModelListener(
					ReportCollectController.this,
					new CollectableComponentModelAdapter() {
				@Override
				public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
					final CollectableField clctf = ev.getNewValue();
					for (OutputType e : OutputType.class.getEnumConstants()) {
						if (e.getValue().equals(clctf.getValue())) {
							setOutputType(e);
							return; 
						}
					}
					//setOutputType(null);
					return;
				}
			});
		}

		getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void detailsModeEntered(CollectStateEvent ev) {
				final boolean bViewingExistingRecord = (ev.getNewCollectState().getInnerState() == CollectState.DETAILSMODE_VIEW);
				btnMakeTreeRoot.setEnabled(bViewingExistingRecord);
				btnPreview.setEnabled(bViewingExistingRecord);
			}
		});
		
		setReportFacadeRemote(SpringApplicationContextHolder.getBean(ReportFacadeRemote.class));
		setReportDelegate(SpringApplicationContextHolder.getBean(ReportDelegate.class));
	}
	
	final void setReportFacadeRemote(ReportFacadeRemote reportFacadeRemote) {
		this.reportFacadeRemote = reportFacadeRemote;
	}
	
	final ReportFacadeRemote getReportFacadeRemote() {
		return reportFacadeRemote;
	}
	
	final void setReportDelegate(ReportDelegate reportDelegate) {
		this.reportDelegate = reportDelegate;
	}

	final ReportDelegate getReportDelegate() {
		return reportDelegate;
	}

	private ReportVO.OutputType getOutputType() {
		return outputtype;
	}

	/**
	 * Adapts the (details) layout to one of the select report type.
	 */
	private void setReportType(ReportType reportType) {
		boolean formUsageEnabled = reportType == ReportType.FORM;
		DetailsSubFormController<UID,CollectableEntityObject<UID>> formUsageController = getSubFormController(E.FORMUSAGE.getUID());
		if (formUsageController != null && formUsageController.getSubForm() != null) {
			SubForm subForm = formUsageController.getSubForm();
			if (subForm.getParent().getParent().getParent() instanceof JTabbedPane) {				
				JTabbedPane tabbedPane = (JTabbedPane) subForm.getParent().getParent().getParent();
				for (int tabIndex = 0; tabIndex < tabbedPane.getTabCount(); tabIndex++) {					
					JComponent comp = UIUtils.findFirstJComponent((JComponent) tabbedPane.getComponentAt(tabIndex), subForm.getClass() );
					if (comp == subForm) {
						tabbedPane.setEnabledAt(tabIndex, formUsageEnabled);
						if (tabbedPane.getSelectedIndex() == tabIndex && !formUsageEnabled)
							tabbedPane.setSelectedIndex(-1);
						break;
					}
				}
			}
		}
	}
	
	
	/**
	 * sets the (details) layout to one of the possible report output states.
	 * @param iOutputType
	 */
	private void setOutputType(OutputType outputType) {
		outputtype = outputType;
		
		clctcmbbxDataSource.setVisible(false);
		for (CollectableComponent comp : getDetailsEditView().getCollectableComponentsFor(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.datasource.getUID() : E.FORM.datasource.getUID()))
			comp.setVisible(false);

		if (outputType == null)
			return;

		// todo: Is there a better way to connect the columns to the fieldnames?
		final JTable tbl = getSubFormController(outputEntity.getUID()).getSubForm().getJTable();
		final TableColumnModel columnmodel = tbl.getColumnModel();
		switch (outputtype) {
		case SINGLE:
			if (tablecolumnDataSource == null) {
				try {
					tablecolumnDataSource = tbl.getColumn(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.datasource.getUID() : E.FORMOUTPUT.datasource.getUID());
					columnmodel.removeColumn(tablecolumnDataSource);	
				} catch (IllegalArgumentException e) {
					// column is hidden right now.
				}
			}
			if (tablecolumnParameter != null) {
				columnmodel.addColumn(tablecolumnParameter);
				tablecolumnParameter = null;
			}
			if (tablecolumnFilename != null) {
				columnmodel.addColumn(tablecolumnFilename);
				tablecolumnFilename = null;
			}
			if (tablecolumnSourceFile != null) {
				columnmodel.addColumn(tablecolumnSourceFile);
				tablecolumnSourceFile = null;
			}
			if (tablecolumnFormat != null) {
				columnmodel.addColumn(tablecolumnFormat);
				tablecolumnFormat = null;
			}
			if (tablecolumnAttach != null) {
				columnmodel.addColumn(tablecolumnAttach);
				tablecolumnAttach = null;
			}
			
			clctcmbbxDataSource.getLabeledComponent().setVisible(true);
			clctlabelDataSource.setVisible(true);
			pnlFileChoosers.setVisible(false);
			if (pnlAttach != null) pnlAttach.setVisible(false);
			break;

		case COLLECTIVE:
			if (tablecolumnAttach == null) {
				try {
					tablecolumnAttach = tbl.getColumn(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.attach.getUID() : E.FORMOUTPUT.attach.getUID());
					columnmodel.removeColumn(tablecolumnAttach);	
				} catch (IllegalArgumentException e) {
					// column is hidden right now.
				}
			}
			if (tablecolumnAttach == null) {
				try {
					tablecolumnAttach = tbl.getColumn(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.attach.getUID() : E.FORMOUTPUT.attach.getUID());
					columnmodel.removeColumn(tablecolumnAttach);	
				} catch (IllegalArgumentException e) {
					// column is hidden right now.
				}
			}
			if (tablecolumnDataSource != null) {
				columnmodel.addColumn(tablecolumnDataSource);
				tablecolumnDataSource = null;
			}
			if (tablecolumnParameter != null) {
				columnmodel.addColumn(tablecolumnParameter);
				tablecolumnParameter = null;
			}
			if (tablecolumnFilename != null) {
				columnmodel.addColumn(tablecolumnFilename);
				tablecolumnFilename = null;
			}
			if (tablecolumnSourceFile != null) {
				columnmodel.addColumn(tablecolumnSourceFile);
				tablecolumnSourceFile = null;
			}
			if (tablecolumnFormat != null) {
				columnmodel.addColumn(tablecolumnFormat);
				tablecolumnFormat = null;
			}

			clctcmbbxDataSource.setVisible(false);
			clctlabelDataSource.setVisible(false);
			pnlFileChoosers.setVisible(false);
			if (pnlAttach != null) pnlAttach.setVisible(true);
			break;

		case EXCEL:
			if (tablecolumnDataSource != null) {
				columnmodel.addColumn(tablecolumnDataSource);
				tablecolumnDataSource = null;
			}
			if (tablecolumnParameter == null) {
				try {
					tablecolumnParameter = tbl.getColumn(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.parameter.getUID() : E.FORMOUTPUT.parameter.getUID());
					columnmodel.removeColumn(tablecolumnParameter);	
				} catch (IllegalArgumentException e) {
					// column is hidden right now.
				}
			}
			if (tablecolumnFilename == null) {
				try {
					tablecolumnFilename = tbl.getColumn(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.filename.getUID() : E.FORMOUTPUT.filename.getUID());
					columnmodel.removeColumn(tablecolumnFilename);
				} catch (IllegalArgumentException e) {
					// column is hidden right now.
				}
			}
			if (tablecolumnSourceFile == null) {
				try {
					tablecolumnSourceFile = tbl.getColumn(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID());
					columnmodel.removeColumn(tablecolumnSourceFile);
				} catch (IllegalArgumentException e) {
					// column is hidden right now.
				}
			}
			if (tablecolumnFormat == null) {
				try {
					tablecolumnFormat = tbl.getColumn(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID());
					columnmodel.removeColumn(tablecolumnFormat);
				} catch (IllegalArgumentException e) {
					// column is hidden right now.
				}
			}
			if (tablecolumnAttach == null) {
				try {
					tablecolumnAttach = tbl.getColumn(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.attach.getUID() : E.FORMOUTPUT.attach.getUID());
					columnmodel.removeColumn(tablecolumnAttach);	
				} catch (IllegalArgumentException e) {
					// column is hidden right now.
				}
			}

			clctcmbbxDataSource.setVisible(false);
			clctlabelDataSource.setVisible(false);
			pnlFileChoosers.setVisible(true);
			if (pnlAttach != null) pnlAttach.setVisible(true);			
			break;

		default:
			throw new NuclosFatalException(getSpringLocaleDelegate().getMessage(
					"ReportExecutionCollectController.5","Unbekannter Reporttyp: {0}", outputType));
		}
	}

	/**
	 * Show all removed columns again, so they can be saved with their width, and are restored when opened again.
	 */
	@Override
	public void close() {
		final DetailsSubFormController<UID,CollectableEntityObject<UID>> sfc = getSubFormController(outputEntity.getUID());
		if (sfc != null) {
			final SubForm sf = sfc.getSubForm();
			if (sf != null) {
				final JTable tbl = sf.getJTable();
				if (tbl != null) {
					final TableColumnModel columnmodel = tbl.getColumnModel();
					if (columnmodel != null) {
						if (tablecolumnDataSource != null) {
							columnmodel.addColumn(tablecolumnDataSource);
							tablecolumnDataSource = null;
						}
						if (tablecolumnParameter != null) {
							columnmodel.addColumn(tablecolumnParameter);
							tablecolumnParameter = null;
						}
						if (tablecolumnFilename != null) {
							columnmodel.addColumn(tablecolumnFilename);
							tablecolumnFilename = null;
						}
						if (tablecolumnSourceFile != null) {
							columnmodel.addColumn(tablecolumnSourceFile);
							tablecolumnSourceFile = null;
						}
						if (tablecolumnFormat != null) {
							columnmodel.addColumn(tablecolumnFormat);
							tablecolumnFormat = null;
						}
						if (tablecolumnAttach != null) {
							columnmodel.addColumn(tablecolumnAttach);
							tablecolumnAttach = null;
						}
					}
				}
			}
		}
		super.close();
	}

	private CollectableComponent getFirstComponent(UID sFieldName) {
		final Collection<CollectableComponent> collclctcomp = getDetailsPanel().getEditView().getCollectableComponentsFor(sFieldName);
		return (collclctcomp.size() == 0) ? null : collclctcomp.iterator().next();
	}

	/**
	 *
	 */
	protected void setupDetailsToolBar() {
		super.setupDetailsToolBar();
		// additional functionality in Result panel:
		//final JToolBar toolbar = UIUtils.createNonFloatableToolBar();

		btnExportFile.setIcon(Icons.getInstance().getIconExport16());
		btnExportFile.setToolTipText(getSpringLocaleDelegate().getMessage(
				"ReportCollectController.1", "Reportvorlage exportieren"));
		btnExportFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cmdExportFile();
			}
		});

		btnPreview.setIcon(Icons.getInstance().getIconTest());
		btnPreview.setToolTipText(getSpringLocaleDelegate().getMessage(
				"ReportCollectController.2", "Layoutansicht"));
		btnPreview.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cmdPreview();
			}
		});
		
		btnRun.setIcon(Icons.getInstance().getIconPlay16());
		btnRun.setToolTipText(getSpringLocaleDelegate().getMessage(
				"nuclos.entity.reportExecution.label", "Report ausführen"));
		btnRun.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cmdRun();
			}
		});
		
		
		this.getDetailsPanel().addToolBarComponent(btnExportFile);
		this.getDetailsPanel().addToolBarComponent(btnPreview);
		this.getDetailsPanel().addToolBarComponent(btnRun);
	}

	/**
	 *
	 * @param clct
	 */
	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		getReportDelegate().removeReport(clct.getMasterDataCVO());
		MasterDataCache.getInstance().invalidate(E.REPORT.getUID());
	}

	/**
	 *
	 * @param clctEdited
	 * @return the current collectable.
	 */
	@Override
	protected CollectableMasterDataWithDependants<UID> updateCurrentCollectable(
			CollectableMasterDataWithDependants<UID> clctEdited) throws CommonBusinessException {
		getSubFormController(outputEntity.getUID()).stopEditing();

		adjustValuesToTypeAndOutputFormat(clctEdited);

		validateReport(clctEdited);

		CollectableMasterDataWithDependants retVal = updateCollectable(clctEdited, getAllSubFormData(clctEdited.getId()), null);
		MasterDataCache.getInstance().invalidate(E.REPORT.getUID());

		return retVal;
	}

	/**
	 * Updates the currently edited Collectable in the database. PDF templates will be compiled by the report facade.
	 * @param clct
	 * @param oDependantData
	 * @return
	 * @throws CommonBusinessException
	 */
	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oDependantData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap) oDependantData;
		
		final UID oId = getReportDelegate().modify(clct.getMasterDataCVO(), mpclctDependants.toDependentDataMap());

		final MasterDataVO<UID> mdvoUpdated = mddelegate.get(getEntityUid(), oId);
		mdvoUpdated.setDependents(readDependants(mdvoUpdated.getPrimaryKey()));

		return new CollectableMasterDataWithDependants<UID>(clct.getCollectableEntity(), new MasterDataVO<UID>(mdvoUpdated.getEntityObject()));
	}
	
	@Override
	public CollectableMasterDataWithDependants<UID> newCollectable() {
		CollectableMasterDataWithDependants<UID> result = super.newCollectable();
		result.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.type.getUID() : E.FORM.type.getUID(),
					getEntityUid().equals(E.REPORT.getUID()) ? new CollectableValueField(ReportType.REPORT.getValue()) : new CollectableValueField(ReportType.FORM.getValue()));
		return result;
	}
	@Override
	protected CollectableMasterDataWithDependants<UID> newCollectableWithDefaultValues(boolean forInsert) {
		CollectableMasterDataWithDependants<UID> result = super.newCollectableWithDefaultValues(forInsert);
		result.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.type.getUID() : E.FORM.type.getUID(),
				getEntityUid().equals(E.REPORT.getUID()) ? new CollectableValueField(ReportType.REPORT.getValue()) : new CollectableValueField(ReportType.FORM.getValue()));
		return result;
	}

	/**
	 * Inserts the currently edited Collectable in the database. PDF template files are compiled by the report facade.
	 * @param clctNew
	 * @return
	 * @throws CommonBusinessException
	 */
	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(
			CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		if (clctNew.getPrimaryKey() != null)
			throw new IllegalArgumentException("clctNew");

		adjustValuesToTypeAndOutputFormat(clctNew);

		validateReport(clctNew);

		//		// We have to clear the ids for cloned objects:
		//		@todo eliminate this workaround
		final IDependentDataMap mpmdvoDependants = org.nuclos.common.Utils.clearIds(getAllSubFormData(null).toDependentDataMap());	
		final MasterDataVO<UID> mdvoInserted = getReportDelegate().create(clctNew.getMasterDataCVO(), mpmdvoDependants);
		mdvoInserted.setDependents(readDependants(mdvoInserted.getPrimaryKey()));

		CollectableMasterDataWithDependants retVal = new CollectableMasterDataWithDependants(getCollectableEntity(), new MasterDataVO(mdvoInserted, readDependants(mdvoInserted.getId())));
		MasterDataCache.getInstance().invalidate(E.REPORT.getUID());

		return retVal;
	}

	private void validateReport(CollectableMasterDataWithDependants<UID> clctmdwd) throws CommonValidationException {
		// removed. @see NUCLOS-913
		//for(Character ch : clctmdwd.getField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.name.getUID() : E.FORM.name.getUID()).toString().toCharArray())
			//if (!Character.isLetterOrDigit(ch) && !ch.equals('_') && !Character.isWhitespace(ch))
				//throw new CollectableFieldValidationException(StringUtils.getParameterizedExceptionMessage("ReportCollectController.3", ch));
		if ((getOutputType() != OutputType.COLLECTIVE && getOutputType() != OutputType.EXCEL)
				&& clctmdwd.getField(getEntityUid().equals(org.nuclos.common.E.REPORT.getUID()) ? org.nuclos.common.E.REPORT.datasource.getUID() : org.nuclos.common.E.FORM.datasource.getUID()).getValueId() == null)
			throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
					"ReportCollectController.32", "Bitte geben Sie eine Datenquelle an."));
		boolean existsAnyOutputFormat = false;
		for (EntityObjectVO<?> md : getAllSubFormData(clctmdwd.getPrimaryKey()).toDependentDataMap().getData(outputReferencingField))
			if (!md.isFlagRemoved()) {
				existsAnyOutputFormat = true;
				boolean subreportsallowed = false;				
				if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.destination.getUID() : E.FORMOUTPUT.destination.getUID()) == null)
					throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
							"ReportCollectController.4", "Bitte geben Sie f\u00fcr jedes Ausgabeformat ein Ausgabemedium an."));
				if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()) == null)
					throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
							"ReportCollectController.5", "Bitte geben Sie f\u00fcr jedes Ausgabeformat ein Ausgabeformat an."));
				final File fileSource = (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID()) != null ?
						new File((String)md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID())) : new File(""));
				final File fileDestination  = (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.parameter.getUID() : E.FORMOUTPUT.parameter.getUID()) != null ?
						new File((String)md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.parameter.getUID() : E.FORMOUTPUT.parameter.getUID())) : new File(""));
				final File fileSourceCollect = (clctmdwd.getField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.sourceFile.getUID() : E.FORM.sourceFile.getUID()).getValue() != null ?
						new File((String)clctmdwd.getField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.sourceFile.getUID() : E.FORM.sourceFile.getUID()).getValue()) : new File(""));
				final File fileDestinationCollect  = (clctmdwd.getField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.parameter.getUID() : E.FORM.parameter.getUID()).getValue() != null ?
						new File((String)clctmdwd.getField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.parameter.getUID() : E.FORM.parameter.getUID()).getValue()) : new File(""));
				switch (getOutputType()) {
					case EXCEL: {
						if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("XLS")) {
							if ((fileSourceCollect.getName().equals("")) || (!fileSourceCollect.getName().toLowerCase().endsWith(".xls")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.8", "Bitte geben Sie f\u00fcr das Ausgabeformat XLS eine MS Excel Vorlage (.xls) an."));
							if ((!fileDestinationCollect.getName().equals("")) && (!fileDestinationCollect.isDirectory()) && (!fileDestinationCollect.getName().toLowerCase().endsWith(".xls")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.9", "Bitte geben Sie f\u00fcr das Ausgabeformat XLS eine MS Excel Zieldatei (.xls) an."));
						}
						else if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("XLSX")) {
							if ((fileSourceCollect.getName().equals("")) || (!fileSourceCollect.getName().toLowerCase().endsWith(".xlsx")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.8a", "Bitte geben Sie f\u00fcr das Ausgabeformat XLSX eine MS Excel Vorlage (.xlsx) an."));
							if ((!fileDestinationCollect.getName().equals("")) && (!fileDestinationCollect.isDirectory()) && (!fileDestinationCollect.getName().toLowerCase().endsWith(".xlsx")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.9a", "Bitte geben Sie f\u00fcr das Ausgabeformat XLSX eine MS Excel Zieldatei (.xlsx) an."));
						}
						break;
					}
					default: {
						if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("DOC")) {
							if ((fileSource.getName().equals("")) || (!fileSource.getName().toLowerCase().endsWith(".doc")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.6", "Bitte geben Sie f\u00fcr das Ausgabeformat DOC eine MS Word Vorlage (.doc) an."));
							if ((!fileDestination.getName().equals("")) && (!fileDestination.isDirectory()) && (!fileDestination.getName().toLowerCase().endsWith(".doc")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.7", "Bitte geben Sie f\u00fcr das Ausgabeformat DOC eine MS Word Zieldatei (.doc) an."));
							if ((md.getFieldValue(E.REPORTOUTPUT.destination.getUID()).toString().endsWith("Server")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.31", "Ausgabemedium \u00fcber Server ist momentan nur f\u00fcr das Ausgabeformat PDF und DOCX erlaubt."));
						} 
						else if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("DOCX")) {
							if ((fileSource.getName().equals("")) || (!fileSource.getName().toLowerCase().endsWith(".docx")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.6a", "Bitte geben Sie f\u00fcr das Ausgabeformat DOCX eine MS Word Vorlage (.docx) an."));
							if ((!fileDestination.getName().equals("")) && (!fileDestination.isDirectory()) && (!fileDestination.getName().toLowerCase().endsWith(".docx")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.7a", "Bitte geben Sie f\u00fcr das Ausgabeformat DOCX eine MS Word Zieldatei (.docx) an."));
						}
						else if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("XLS")) {
							if ((fileSource.getName().equals("")) || (!fileSource.getName().toLowerCase().endsWith(".xls")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.8", "Bitte geben Sie f\u00fcr das Ausgabeformat XLS eine MS Excel Vorlage (.xls) an."));
							if ((!fileDestination.getName().equals("")) && (!fileDestination.isDirectory()) && (!fileDestination.getName().toLowerCase().endsWith(".xls")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.9", "Bitte geben Sie f\u00fcr das Ausgabeformat XLS eine MS Excel Zieldatei (.xls) an."));
							if ((md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.destination.getUID() : E.FORMOUTPUT.destination.getUID()).toString().endsWith("Server")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.31", "Ausgabemedium \u00fcber Server ist momentan nur f\u00fcr das Ausgabeformat PDF und DOCX erlaubt."));
						} else if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("XLSX")) {
								if ((fileSource.getName().equals("")) || (!fileSource.getName().toLowerCase().endsWith(".xlsx")))
									throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
											"ReportCollectController.8a", "Bitte geben Sie f\u00fcr das Ausgabeformat XLSX eine MS Excel Vorlage (.xlsx) an."));
								if ((!fileDestination.getName().equals("")) && (!fileDestination.isDirectory()) && (!fileDestination.getName().toLowerCase().endsWith(".xlsX")))
									throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
											"ReportCollectController.9a", "Bitte geben Sie f\u00fcr das Ausgabeformat XLSX eine MS Excel Zieldatei (.xlsx) an."));
								if ((md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.destination.getUID() : E.FORMOUTPUT.destination.getUID()).toString().endsWith("Server")))
									throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
											"ReportCollectController.31", "Ausgabemedium \u00fcber Server ist momentan nur f\u00fcr das Ausgabeformat PDF und DOCX erlaubt."));
						}
						else if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("CSV")) {
							//if ((fileSource.getName().equals("")) || (!fileSource.getName().toLowerCase().endsWith(".csv")))
								//throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
									//	"ReportCollectController.33", "Bitte geben Sie f\u00fcr das Ausgabeformat CSV eine Vorlage (.csv) an."));
							if ((md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.destination.getUID() : E.FORMOUTPUT.destination.getUID()).toString().endsWith("Server")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.31", "Ausgabemedium \u00fcber Server ist momentan nur f\u00fcr das Ausgabeformat PDF und DOCX erlaubt."));
						}
						else if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("TSV")) {
							//if ((fileSource.getName().equals("")) || (!fileSource.getName().toLowerCase().endsWith(".tsv")))
								//throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
									//	"ReportCollectController.34", "Bitte geben Sie f\u00fcr das Ausgabeformat TSV eine Vorlage (.tsv) an."));
							if ((md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.destination.getUID() : E.FORMOUTPUT.destination.getUID()).toString().endsWith("Server")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.31", "Ausgabemedium \u00fcber Server ist momentan nur f\u00fcr das Ausgabeformat PDF und DOCX erlaubt."));
						}
						else if (md.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID()).equals("PDF")) {
							String fileName = fileSource.getName().toLowerCase();
							if (fileName.isEmpty() || !(fileName.endsWith(".xml") || fileName.endsWith(".jrxml")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.10", "Bitte geben Sie f\u00fcr das Ausgabeformat PDF eine Jasper Reports Vorlage (.xml, .jrxml) an."));
							if ((!fileDestination.getName().equals("")) && (!fileDestination.isDirectory()) && (!fileDestination.getName().toLowerCase().endsWith(".pdf")))
								throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
										"ReportCollectController.11", "Bitte geben Sie f\u00fcr das Ausgabeformat PDF eine PDF Zieldatei (.pdf) an."));
							subreportsallowed = true;
						}
					}
				}

				for (EntityObjectVO<?> subreport : md.getDependents().getData(subreportReferencingField)) {
					if (subreport.isFlagRemoved())
						continue;

					if (!subreportsallowed)
						throw new CollectableFieldValidationException("ReportCollectController.validation.nosubreport");

					final String subreportsourcefilename = (String)subreport.getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.SUBREPORT.sourcefilename.getUID() : E.SUBFORM.sourcefilename.getUID());
					if (StringUtils.isNullOrEmpty(subreportsourcefilename) || !(subreportsourcefilename.endsWith(".xml") || subreportsourcefilename.endsWith(".jrxml")))
						throw new CollectableFieldValidationException("ReportCollectController.validation.subreportmissingsource");
				}
			}
		if (!existsAnyOutputFormat)
			throw new CollectableFieldValidationException(getSpringLocaleDelegate().getMessage(
					"ReportCollectController.12", "Bitte geben Sie mindestens ein Ausgabeformat an."));
	}

	/**
	 * Adjust the entries from the output subform according to the chosen output format:
	 * - if no collective report, copy main datasource into all output dependants
	 * - in case of excel report set all output format values to xls
	 * @param clctmd
	 */
	private void adjustValuesToTypeAndOutputFormat(CollectableMasterData<UID> clctmd) throws CommonBusinessException {
		switch (getOutputType()) {
		case SINGLE: {
			clctmd.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.attach.getUID() : E.FORM.attach.getUID(), new CollectableValueField(false));
			if (clctcmbbxDataSource != null && clctcmbbxDataSource.getJComboBox().getSelectedItem() != null) {
				final CollectableField clctf = (CollectableField) clctcmbbxDataSource.getJComboBox().getSelectedItem();
				final CollectableTableModel<UID,CollectableEntityObject<UID>> tblmodel = getSubFormController(outputEntity.getUID()).getCollectableTableModel();
				for (int i = 0; i < tblmodel.getRowCount(); i++)
					tblmodel.getCollectable(i).setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.datasource.getUID() : E.FORMOUTPUT.datasource.getUID(), clctf);
			}
			break;
		}
		case EXCEL: {
			final String sDestination = (String) clctmd.getMasterDataCVO().getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.parameter.getUID() : E.FORM.parameter.getUID());
			final String sSourceFile = (String) clctmd.getMasterDataCVO().getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORT.sourceFile.getUID() : E.FORM.sourceFile.getUID());
			final CollectableTableModel<UID,CollectableEntityObject<UID>> tblmodel = getSubFormController(outputEntity.getUID()).getCollectableTableModel();
			final boolean xlsxSource = sSourceFile != null && sSourceFile.toLowerCase().endsWith("xlsx");
			for (int i = 0; i < tblmodel.getRowCount(); i++) {
				final Collectable<UID> clct = tblmodel.getCollectable(i);
				clct.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.format.getUID() : E.FORMOUTPUT.format.getUID(), new CollectableValueField(xlsxSource ? ReportOutputVO.Format.XLSX.getValue() : ReportOutputVO.Format.XLS.getValue()));
				clct.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.parameter.getUID() : E.FORMOUTPUT.parameter.getUID(), new CollectableValueField(sDestination));
				clct.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID(), new CollectableValueField(sSourceFile));
			}
			break;
		}
		}

		importFiles(clctmd);
	}

	/**
	 * Read the specified files and set the content into the respective fields for outputs with template files
	 * We use the existence of a path in the filename as the sign, that the file template has changed, as the path will not be stored.
	 * @throws CommonBusinessException
	 */
	private void importFiles(CollectableMasterData<UID> clctmd) throws CommonIOException {
		final CollectableTableModel<UID,CollectableEntityObject<UID>> tblmodel = getSubFormController(outputEntity.getUID()).getCollectableTableModel();
		for (int i = 0; i < tblmodel.getRowCount(); i++) {
			final CollectableEntityObject<UID> clct = tblmodel.getCollectable(i);
			final CollectableField clctfSource = clct.getField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID());
			if (clctfSource != null) {
				final String sSource = (String) clctfSource.getValue();
				if (sSource != null) {
					final File fileSource = new File(sSource);
					if (!fileSource.getPath().equals(fileSource.getName()))
						try {
							final byte[] abSourceFile = IOUtils.readFromBinaryFile(fileSource);
							clct.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFileContent.getUID() : E.FORMOUTPUT.sourceFileContent.getUID(),
									new CollectableValueField(new org.nuclos.common.report.ByteArrayCarrier(abSourceFile)));
							clct.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID(),
									new CollectableValueField(fileSource.getName()));
						}
					catch (IOException ex) {
						throw new CommonIOException(StringUtils.getParameterizedExceptionMessage("ReportCollectController.13", sSource), ex);
					}
				}
			}

			for (CollectableEntityObject<?> subreportclct : clct.getDependantCollectableMasterDataMap().getValues(subreportReferencingField)) {
				final CollectableField subreportclctfSource = subreportclct.getField(getEntityUid().equals(E.REPORT.getUID()) ? E.SUBREPORT.sourcefilename.getUID() : E.SUBFORM.sourcefilename.getUID());
				if (subreportclctfSource != null) {
					final String sourcefilename = (String) subreportclctfSource.getValue();
					if (sourcefilename != null) {
						final File subreportSource = new File(sourcefilename);
						if (!subreportSource.getPath().equals(subreportSource.getName()))
							try {
								final byte[] subreportByte = IOUtils.readFromBinaryFile(subreportSource);
								subreportclct.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.SUBREPORT.sourcefileContent.getUID() : E.SUBFORM.sourcefileContent.getUID(),
										new CollectableValueField(new org.nuclos.common.report.ByteArrayCarrier(subreportByte)));
								subreportclct.setField(getEntityUid().equals(E.REPORT.getUID()) ? E.SUBREPORT.sourcefilename.getUID() : E.SUBFORM.sourcefilename.getUID(),
										new CollectableValueField(subreportSource.getName()));
							}
						catch (IOException ex) {
							throw new CommonIOException(StringUtils.getParameterizedExceptionMessage("ReportCollectController.13", subreportSource), ex);
						}
					}
				}
			}
		}

		if (getOutputType() == ReportVO.OutputType.EXCEL && tblmodel.getRowCount() > 0) {
			final String sSourceFile = (String) clctmd.getMasterDataCVO().getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID());
			if (sSourceFile != null) {
				final File fileSource = new File(sSourceFile);
				if (!fileSource.getPath().equals(fileSource.getName()))
					clctmd.getMasterDataCVO().setFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID(), fileSource.getName());
			}
		}
	}

	private void cmdExportFile() {
		String sFileName = null;
		CollectableEntityObject<UID> clct = null;
		if (getOutputType() == ReportVO.OutputType.EXCEL) {
			sFileName = (String) getSelectedCollectable().getMasterDataCVO().getFieldValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID());

			final CollectableTableModel<UID,CollectableEntityObject<UID>> tblmodel = getSubFormController(outputEntity.getUID()).getCollectableTableModel();
			for (int i = 0; i < tblmodel.getRowCount(); i++) {
				clct = tblmodel.getCollectable(i);
				final CollectableField clctfSource = clct.getField(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFileContent.getUID() : E.FORMOUTPUT.sourceFileContent.getUID());
				if (clctfSource != null && clctfSource.getValue() != null)
					break;
			}

			if (sFileName == null || clct == null) {
				JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
						"ReportCollectController.14", "Dieses Objekt enth\u00e4lt keine Vorlagedatei."));
				return;
			}
		}
		else {
			clct = getSubFormController(outputEntity.getUID()).getSelectedCollectable();
			if (clct == null) {
				JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
						"ReportCollectController.15", "Bitte w\u00e4hlen Sie das Ausgabeformat aus, dessen Vorlagedatei exportiert werden soll."));
				return;
			}

			sFileName = (String) clct.getValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFile.getUID() : E.FORMOUTPUT.sourceFile.getUID());

			if (sFileName == null) {
				JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
						"ReportCollectController.16", "Das ausgew\u00e4hlte Ausgabeformat enth\u00e4lt keine Vorlagedatei."));
				return;
			}
		}

		final org.nuclos.common.report.ByteArrayCarrier bacFileContent = (org.nuclos.common.report.ByteArrayCarrier) 
				clct.getValue(getEntityUid().equals(E.REPORT.getUID()) ? E.REPORTOUTPUT.sourceFileContent.getUID() : E.FORMOUTPUT.sourceFileContent.getUID());
		if (bacFileContent == null) {
			JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
					"ReportCollectController.17", "Die Vorlagedatei kann nicht exportiert werden, da sie noch nicht importiert wurde."));
			return;
		}

		boolean includeSubreports = false;
		if (clct.getDependantCollectableMasterDataMap().getValues(subreportReferencingField).size() > 0)
			includeSubreports = JOptionPane.showOptionDialog(getTab(), getSpringLocaleDelegate().getMessage(
					"ReportCollectController.question.exportsubreports", "Sollen Subreports exportiert werden?"), "Subreports", 
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null) == JOptionPane.YES_OPTION;

		final JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		if (fileChooser.showSaveDialog(getTab()) == JFileChooser.APPROVE_OPTION) {
			final File targetDirectory = fileChooser.getSelectedFile();
			final File targetFile = new File(targetDirectory, sFileName);
			if (targetFile.exists())
				if (JOptionPane.showConfirmDialog(getTab(), getSpringLocaleDelegate().getMessage(
						"ReportCollectController.18", "Die Datei \"{0}\" existiert bereits. Wollen Sie sie \u00fcberschreiben?", 
						targetFile.getPath()), getSpringLocaleDelegate().getMessage(
								"ReportCollectController.19", "Vorlage exportieren"), 
						JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
					return;

			try {
				final String targetFileName = targetFile.getName();
				final boolean isXml = targetFileName.endsWith(".xml") || targetFileName.endsWith(".jrxml");
				if (isXml) {
					final BufferedInputStream xml = new BufferedInputStream(new ByteArrayInputStream(bacFileContent.getData()));
					IOUtils.writeToXmlFile(targetFile, xml, "UTF-8");
				} else {
					IOUtils.writeToBinaryFile(targetFile, bacFileContent.getData());
				}
			}
			catch (Exception ex) {
				throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("ReportCollectController.19", targetFile.getPath()), ex);
			}

			if (includeSubreports) {
				for (CollectableMasterData<?> subreport : clct.getDependantCollectableMasterDataMap().getValues(subreportReferencingField)) {
					String subreportfilename = sFileName = (String) subreport.getValue(getEntityUid().equals(E.REPORT.getUID()) ? E.SUBREPORT.sourcefilename.getUID() : E.SUBFORM.sourcefilename.getUID());
					if (subreportfilename != null) {
						final org.nuclos.common.report.ByteArrayCarrier bacFileSubreportContent = (org.nuclos.common.report.ByteArrayCarrier)
								subreport.getValue(getEntityUid().equals(E.REPORT.getUID()) ? E.SUBREPORT.sourcefileContent.getUID() : E.SUBFORM.sourcefileContent.getUID());
						if (bacFileSubreportContent == null) {
							JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
									"ReportCollectController.17", "Die Vorlagedatei kann nicht exportiert werden, da sie noch nicht importiert wurde."));
							continue;
						}

						File subreportFile = new File(targetDirectory, subreportfilename);
						if (subreportFile.exists())
							if (JOptionPane.showConfirmDialog(getTab(), getSpringLocaleDelegate().getMessage(
									"ReportCollectController.18", "Die Datei \"{0}\" existiert bereits. Wollen Sie sie \u00fcberschreiben?", subreportFile.getPath()), 
									getSpringLocaleDelegate().getMessage("ReportCollectController.19", "Vorlage exportieren"), JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION)
								continue;

						try {
							final String subreportFileName = subreportFile.getName();
							final boolean isXml = subreportFileName.endsWith(".xml") || subreportFileName.endsWith(".jrxml");
							if (isXml) {
								final BufferedInputStream xml = new BufferedInputStream(new ByteArrayInputStream(bacFileSubreportContent.getData()));
								IOUtils.writeToXmlFile(subreportFile, xml, "UTF-8");
							} else {
								IOUtils.writeToBinaryFile(subreportFile, bacFileSubreportContent.getData());
							}
						}
						catch (Exception ex) {
							throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("ReportCollectController.19", subreportFile.getPath()), ex);
						}
					}
				}
			}
		}
	}

	private void cmdPreview() {
		UIUtils.runCommand(getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				DefaultReportOutputVO outputvoFormat = null;
				final CollectableMasterData clctSelected = ReportCollectController.this.getSelectedCollectable();
				if (clctSelected != null)
					try {
						final Collection<DefaultReportOutputVO> collReportOutputVO = getReportFacadeRemote().getReportOutputs(
								(UID) clctSelected.getPrimaryKey());
						DefaultReportOutputVO outputvo = null;

						if (collReportOutputVO.size() == 0) {
							JOptionPane.showMessageDialog(
									ReportCollectController.this.getTab(),
									getSpringLocaleDelegate().getMessage("ReportCollectController.20",
													"Diesem Report/Formular wurde keine Vorlage zugewiesen, dessen Layout als Vorschau angezeigt werden k\u00f6nnte."));
							return;
						}
						else if (collReportOutputVO.size() == 1)
							outputvo = collReportOutputVO.iterator().next();
						else if (collReportOutputVO.size() > 1) {
							Collectable<?> clctReportOutput = ReportCollectController.this.getSubFormController(
									outputEntity.getUID()).getSelectedCollectable();
							if (clctReportOutput == null) {
								JOptionPane.showMessageDialog(
										ReportCollectController.this.getTab(),
										getSpringLocaleDelegate().getMessage("ReportCollectController.21",
														"Bitte w\u00e4hlen Sie die Vorlage aus, dessen Layout als Vorschau angezeigt werden soll."));
								return;
							}
							outputvo = getReportFacadeRemote().getReportOutput((UID) clctReportOutput.getPrimaryKey());
						}

						if (outputvo == null
								|| (outputvo.getSourceFile() == null && !"CSV".equals(outputvo.getFormat().getValue()))) {
							JOptionPane.showMessageDialog(
									ReportCollectController.this.getTab(),
									getSpringLocaleDelegate().getMessage("ReportCollectController.22",
													"Ein Fehler bei der Anzeige des Layouts der Vorlage ist aufgetreten. M\u00f6glicherweise wurde keine Vorlage zugewiesen."));
							return;
						}

						if ("PDF".equals(outputvo.getFormat().getValue())) {
							outputvoFormat = outputvo;
						} else if ("XLS".equals(outputvo.getFormat().getValue())) {
							outputvoFormat = outputvo;
						} else if ("XLSX".equals(outputvo.getFormat().getValue())) {
							outputvoFormat = outputvo;
						} else if ("DOC".equals(outputvo.getFormat().getValue())) {
							outputvoFormat = outputvo;
						} else if ("CSV".equals(outputvo.getFormat().getValue())) {
							outputvoFormat = outputvo;
						}
						if (outputvoFormat == null) {
							// todo: some error handling here?
							return;
						}
						previewReportOutputVO(outputvoFormat);
					} catch (IOException ex) {
						throw new CommonIOException(ex.getMessage(), ex);
					} 
					/* catch (RuntimeException ex) {
						throw new CommonBusinessException(ex.getMessage(), ex);
					} */
			}
		});
	}

	/**
	 * Externalised Preview Body for
	 * ELISA-6574
	 * @param outputvo
	 * @throws CommonBusinessException
	 * @throws IOException 
	 */
	private void previewReportOutputVO(DefaultReportOutputVO outputvo) throws IOException{
		String filename = outputvo.getSourceFile();
		if (filename == null) {
			filename = getSpringLocaleDelegate().getMessage("ReportCollectController.23", "Vorschau");
		}
		else {
			filename = filename.substring(0, filename.lastIndexOf('.'));
		}
		final DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
		final String fileName = filename + "_" 
				+ dateformat.format(Calendar.getInstance(Locale.getDefault()).getTime()).replaceAll("[/+*%?#!.:]", "-");		
		final File targetFile = File.createTempFile(filename, outputvo.getFormat().getExtension());
		try {
			targetFile.deleteOnExit();
			final NuclosFile preview = getReportDelegate().testReport(outputvo.getId());

			IOUtils.writeToBinaryFile(targetFile, preview.getContent());
			SystemUtils.open(targetFile);
		} catch (NuclosReportException e) {
			Errors.getInstance().showExceptionDialog(getParent(), e);
		} 
		/* catch (IOException ex) {
			throw new CommonIOException(StringUtils.getParameterizedExceptionMessage("ReportCollectController.19", targetFile.getPath()), ex);
		} */
	}
	
	private void cmdRun() {
		try {
			MasterDataVO<UID> mdReport = MasterDataDelegate.getInstance().<UID>get(E.REPORTEXECUTION.getUID(), getSelectedCollectableId());
			ReportExecutionCollectController.execReport(getTab(), E.REPORTEXECUTION.getUID(), mdReport);
		} catch (Exception e) {
			Errors.getInstance().showExceptionDialog(getParent(), e);
		}
	}

	@Override
	protected boolean isSaveAllowed() {
		if (!SecurityCache.getInstance().isWriteAllowedForMasterData(getEntityUid()))
			return false;

		if (getSelectedCollectableId() != null)
			return getReportDelegate().isSaveAllowed((UID) getSelectedCollectableId());
		else
			// new reports/forms may always be saved:
			return true;
	}

	@Override
	protected boolean isNewAllowed() {
		return SecurityCache.getInstance().isWriteAllowedForMasterData(getEntityUid());
	}

	/**
	 * @return Is the "Delete" action for the given Collectable allowed? Default: true. May be overridden by subclasses.
	 */
	@Override
	protected boolean isDeleteAllowed(CollectableMasterDataWithDependants<UID> clct) {
		if (clct == null) {
			return false;
		}
		if (!SecurityCache.getInstance().isDeleteAllowedForMasterData(getEntityUid())) {
			return false;
		}
		return getReportDelegate().isSaveAllowed((UID) clct.getPrimaryKey());
	}

	/**
	 * @deprecated Move to SearchController and make protected again. 
	 */
	@Override
	public CollectableSearchCondition getCollectableSearchConditionToDisplay() throws CollectableFieldFormatException {
		return getSearchStrategy().getCollectableSearchCondition();
	}

	public UID getSubReportEntity() {
		return subreportEntity.getUID();
	}
	
}	// class ReportCollectController