//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.report.reportrunner;

import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.Future;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.ServiceUI;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.datasource.admin.ParameterPanel;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.main.Main;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.report.ReportDelegate;
import org.nuclos.client.report.reportrunner.source.DefaultReportSource;
import org.nuclos.client.report.reportrunner.source.JasperResultVoReportSource;
import org.nuclos.client.report.reportrunner.source.SearchExpressionReportSource;
import org.nuclos.client.ui.CommonInterruptibleProcess;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.NuclosReportPrintJob;
import org.nuclos.common.report.NuclosReportRemotePrintService;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.print.CSVPrintJob;
import org.nuclos.common.report.print.DOCPrintJob;
import org.nuclos.common.report.print.FilePrintJob;
import org.nuclos.common.report.print.PDFPrintJob;
import org.nuclos.common.report.print.XLSPrintJob;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.Destination;
import org.nuclos.common.report.valueobject.ReportOutputVO.Format;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.SystemUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import net.sf.jasperreports.engine.JRReport;

/**
 * Create a new thread in which a <code>ReportExporter</code> is executed.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @author <a href="mailto:rostislav.maksymovskyi@novabit.de">rostislav.maksymovskyi</a>
 * @version 02.00.00
 */
public class ReportRunner implements Runnable, BackgroundProcessInfo, CommonInterruptibleProcess {

	private static final Logger LOG = Logger.getLogger(ReportRunner.class);
	
	public static final String TEMPDIR = System.getProperty("java.io.tmpdir");
	
	//
	//

	private final List<ProcessListener> listeners = new LinkedList<ProcessListener>();

	private final ReportSource source;

	private final String reportname;
	private final String directory;
	private final String filename;
	
	private final boolean temporaryReport;

	private final ReportOutputVO.Format format;
	private final ReportOutputVO.Destination destination;
	private final Integer copies;

	private volatile Status status = Status.NOTRUNNING;
	private volatile Date dateStartTime;
	private volatile String message;
	private volatile Throwable throwable;

	private Future<?> future = null;
	
	@Deprecated
	private Observable observable = null;

	private boolean bColumnScaled;
	private ReportOutputVO.PageOrientation pageOrientation;

	private final ReportAttachmentInfo attachmentInfo;
	
	// former Spring injection

	private SpringLocaleDelegate localeDelegate;
	
	private DatasourceFacadeRemote datasourceFacadeRemote;
	
	// end of former Spring injection

	/**
	 * Called for forms and reports (with real ReportVO and ReportOutputVO)
	 * 
	 * @param parent
	 * @param mpParameter
	 * @param outputVO
	 * @param bExecuteDatasource
	 * @param iMaxRowCount
	 * @param attachmentInfo
	 * @return the newly created report job thread
	 */
	public static ReportThread createJob(Component parent, Map<String, Object> mpParameter, ReportVO reportVO, DefaultReportOutputVO outputVO, boolean bExecuteDatasource, Integer iMaxRowCount, String sReportFilename,
			ReportAttachmentInfo attachmentInfo, UID language) {
		final ReportSource source = new DefaultReportSource(outputVO, mpParameter, iMaxRowCount, language);
		final ReportRunner runner = new ReportRunner(parent, reportVO, outputVO, source, sReportFilename, attachmentInfo);
		registerAsBackgroundProcess(parent, runner);
		return new ReportThread(runner);
	}

	public static ReportThread createExportJob(Component parent, ReportOutputVO.Format format, CollectableSearchExpression expr, List<? extends CollectableEntityField> lstclctefweSelected, List<Integer> selectedFieldWidth, 
			UID entityUID, String customUsage, ReportOutputVO.PageOrientation orientation, boolean columnScaled, Runnable finishCallback) {
		final ReportSource source = new SearchExpressionReportSource(expr, lstclctefweSelected, selectedFieldWidth, entityUID, format, customUsage, orientation, columnScaled);
		final ReportRunner runner = new ReportRunner(parent, source, format, SpringLocaleDelegate.getInstance().getMessage("ReportRunner.2", "Suchergebnis"), finishCallback);
		registerAsBackgroundProcess(parent, runner);
		return new ReportThread(runner);
	}
	
	/**
	 * used for CSV and XLS export
	 * 
	 * @param parent
	 * @param resultVO the reports data
	 * @param format
	 * @param iMaxRowCount
	 */
	public static ReportThread createExportJob(Component parent, String searchCondition, ResultVO resultVO, ReportOutputVO.Format format, byte pageOrientation, boolean bColumnScaled, Integer iMaxRowCount, String sDatasourceName) {
		ReportOutputVO.PageOrientation orientation = ReportOutputVO.PageOrientation.PORTRAIT;
		if (pageOrientation == JRReport.ORIENTATION_LANDSCAPE) {
			orientation = ReportOutputVO.PageOrientation.LANDSCAPE;
		}		
		final ReportSource source = new JasperResultVoReportSource(searchCondition, resultVO, format, orientation, bColumnScaled);
		final ReportRunner runner = new ReportRunner(parent, source, format, pageOrientation, bColumnScaled, 
				(sDatasourceName != null) ? sDatasourceName : SpringLocaleDelegate.getInstance().getMessage("ReportRunner.2", "Suchergebnis"));
		registerAsBackgroundProcess(parent, runner);
		return new ReportThread(runner);
	}
	
	private static void registerAsBackgroundProcess(final Component parent, final ReportRunner runner) {
		final BackgroundProcessStatusTableModel model = BackgroundProcessStatusController.getStatusDialog(
				UIUtils.getFrameForComponent(parent)).getStatusPanel().getModel();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				model.addEntry(runner);
			}
		});
	}

	private ReportRunner(Component parent, ReportVO reportvo, ReportOutputVO reportoutputvo, ReportSource source, String reportFilename, ReportAttachmentInfo attachmentInfo) {
		this.source = source;
		this.reportname = reportvo.getName();
		this.format = reportoutputvo.getFormat();
		this.destination = reportoutputvo.getDestination();
		this.copies = reportoutputvo.getPrintProperties().getCopies();
		this.attachmentInfo = attachmentInfo;
		
		if (!StringUtils.isNullOrEmpty(reportoutputvo.getParameter())) {
			directory = reportoutputvo.getParameter();
			this.temporaryReport = false;
		} else {
			directory = TEMPDIR;
			if (this.destination != null && !this.destination.equals(ReportOutputVO.Destination.FILE)) {
				this.temporaryReport = true;
			} else {
				this.temporaryReport = false;
			}
		}
		if (!StringUtils.isNullOrEmpty(reportFilename)) {
			filename = reportFilename;
		} else {
			filename = null;
		}
		
		setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
		setDatasourceFacadeRemote(SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class));
	}

	private Runnable finishCallback;
	
	private ReportRunner(Component parent, ReportSource source, ReportOutputVO.Format format, String filename, Runnable finishCallback) {
		this.directory = TEMPDIR;
		this.temporaryReport = true;
		this.filename = filename;
		this.source = source;
		this.reportname = filename;
		this.format = format;
		this.destination = Destination.SCREEN;
		this.copies = null;
		this.attachmentInfo = null;

		this.finishCallback = finishCallback;
		
		setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
		setDatasourceFacadeRemote(SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class));
	}

	private ReportRunner(Component parent, ReportSource source, ReportOutputVO.Format format, byte pageOrientation, boolean bColumnScaled, String filename) {
		this.directory = TEMPDIR;
		this.temporaryReport = true;
		this.filename = filename;
		this.source = source;
		this.reportname = filename;
		this.format = format;
		this.destination = Destination.SCREEN;
		this.copies = null;
		this.attachmentInfo = null;
		this.bColumnScaled = bColumnScaled;
		
		if (pageOrientation == JRReport.ORIENTATION_PORTRAIT) {
			this.pageOrientation = ReportOutputVO.PageOrientation.PORTRAIT;
		}
		else {
			this.pageOrientation = ReportOutputVO.PageOrientation.LANDSCAPE;
		}
		
		setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
		setDatasourceFacadeRemote(SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class));
	}

	final void setSpringLocaleDelegate(SpringLocaleDelegate cld) {
		this.localeDelegate = cld;
	}
	
	final SpringLocaleDelegate getSpringLocaleDelegate() {
		return localeDelegate;
	}
	
	final void setDatasourceFacadeRemote(DatasourceFacadeRemote datasourceFacadeRemote) {
		this.datasourceFacadeRemote = datasourceFacadeRemote;
	}

	final DatasourceFacadeRemote getDatasourceFacadeRemote() {
		return datasourceFacadeRemote;
	}

	@Override
	public void setBackgroundProcessInterruptionIntervalForCurrentThread() throws InterruptedException {
		Thread.sleep(GENERAL_INTERRUPTION_INTERVAL);
	}

	@Override
	@Deprecated
	public void addObservable(Observable observable) {
		this.observable = observable;
	}

	@Override
	public void cancelProzess() {
		if (this.future != null) {
			boolean cancelled = this.future.cancel(true);
			LOG.debug("cancelProzess>>>>>>>>>> cancelled future: " + cancelled);
		}
	}

	@Override
	public Future<?> getProcessFuture() {
		return future;
	}

	public void setProcessFuture(Future<?> future) {
		this.future = future;
	}

	@Override
	public void run() {
		try {
			this.setStatus(Status.RUNNING);
			this.dateStartTime = new Date(Calendar.getInstance().getTimeInMillis());

			this.execute();

			if (attachmentInfo != null) {
				String filename = attachDocument();
				this.setStatus(Status.DONE);
				this.setMessage(getSpringLocaleDelegate().getMessage(
						"ReportRunner.fileattached", "Document {0} has been attached to object {1}.", filename, attachmentInfo.getGenericObjectIdentifier()));
			}
			else {
				this.setStatus(Status.DONE);
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			this.setStatus(Status.CANCELLED);
			this.setMessage(getSpringLocaleDelegate().getMessage(
					"ReportRunner.1", "Der Prozess wurde abgebrochen."));
		}
		catch (final Exception ex) {
			this.setStatus(Status.ERROR);
			this.setException(ex);
			this.setMessage(ex.getMessage());
			// on error show status dialog.
			BackgroundProcessStatusController.getStatusDialog(Main.getInstance().getMainFrame().getFrame()).show();
			// with a double click in the background dialog, the exception dialog will be shown. @see NUCLOS-1064
			/*SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					Errors.getInstance().showExceptionDialog(UIUtils.getFrameForComponent(parent), ex);
				}
			});*/
		}
		finally {
			if (finishCallback != null) {
				finishCallback.run();
				finishCallback = null;
			}
			// release the job - avoid memory leak:
			// this.job = null;
		}
	}

	private void execute() throws NuclosReportException, InterruptedException {
		try {
			setBackgroundProcessInterruptionIntervalForCurrentThread();
			final NuclosFile result = source.getReport();
			final String f = getFileName(!StringUtils.isNullOrEmpty(filename) ? filename : result.getName());
			open(result, f);
		} catch (CommonBusinessException e) {
			throw new NuclosReportException(e);
		} catch (IOException e) {
			throw new NuclosReportException(e);
		}
	}

	/**
	 * get parameter values from user
	 * 
	 * @param collFormat
	 * @param mpParams
	 */
	public static boolean prepareParameters(String sReportName, Collection<DefaultReportOutputVO> collFormat, Map<String, Object> mpParams) throws NuclosReportException {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		boolean result = true;

		final Collection<UID> collDatasourceId = new HashSet<UID>();
		for (ReportOutputVO rovo : collFormat) {
			collDatasourceId.add(rovo.getDatasourceUID());
		}

		try {
			// NUCLEUSINT-182 Arraylists instead of Sets
			final List<String> liParamNamesInThisDatasource = new ArrayList<String>();
			final List<String> liParamNamesInAllDatasources = new ArrayList<String>();
			final List<DatasourceParameterVO> liParamsEmpty = new ArrayList<DatasourceParameterVO>();

			for (UID iDatasourceId : collDatasourceId) {
				if (iDatasourceId == null)
					throw new NuclosReportException(localeDelegate.getMessage("ReportRunner.4", "Keine Datenquelle angegeben"));

				final DatasourceFacadeRemote datasourceFacadeRemote = SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class);
				final DatasourceVO datasourcevo = datasourceFacadeRemote.get(iDatasourceId);
				for (DatasourceParameterVO paramvo : datasourceFacadeRemote.getParametersFromXML(datasourcevo.getSource())) {
					paramvo.setDatasourceId(iDatasourceId);
					final String sParamName = paramvo.getParameter();
					liParamNamesInThisDatasource.add(sParamName);
					if (!liParamNamesInAllDatasources.contains(sParamName)) {
						liParamsEmpty.add(paramvo);
						liParamNamesInAllDatasources.add(sParamName);
					}
				}
				if (mpParams.containsKey("intid")) {
					if (!liParamNamesInThisDatasource.contains("intid")) {
						throw new NuclosReportException(localeDelegate.getMessage("ReportRunner.5", "Die dem Formular zugrundeliegende Datenquelle \"{0}\" muss den Parameter intid definieren.", datasourcevo.getName()));
					}
				}
				liParamNamesInThisDatasource.clear();
			}

			if (!liParamsEmpty.isEmpty()) {
				final ParameterPanel panel = new ParameterPanel(liParamsEmpty);
				result = (panel.showOptionDialog(Main.getInstance().getMainFrame(), panel, sReportName != null ? sReportName :
						localeDelegate.getMessage("ReportRunner.8", "Parameter"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null) == JOptionPane.OK_OPTION);
				if (result) {
					panel.fillParameterMap(liParamsEmpty, mpParams);
				}
			}
		}
		catch (RuntimeException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (CommonBusinessException ex) {
			throw new NuclosReportException(ex);
		}

		return result;
	}

	private boolean isForPrinting() {
		switch (destination) {
			case DEFAULT_PRINTER_CLIENT:
			case DEFAULT_PRINTER_SERVER:
			case PRINTER_CLIENT:
			case PRINTER_SERVER:
				return true;
			default:
				return false;
		}
	}

	private String attachDocument() throws NuclosBusinessException {
		if (attachmentInfo == null) {
			throw new NuclosFatalException("ReportAttachmentInfo not set.");
		}

		try {
			final File file = new File(getDocumentName());
			final byte[] abFileContent = IOUtils.readFromBinaryFile(file);
			final GenericObjectDocumentFile loFile = new GenericObjectDocumentFile(file.getName(), abFileContent);

			final MasterDataVO mdvo = new MasterDataVO(MasterDataDelegate.getInstance().getMetaData(attachmentInfo.getDocumentEntityUID()), false);
			UID parentAttribute = null;
			if (E.GENERALSEARCHDOCUMENT.getUID().equals(attachmentInfo.getDocumentEntityUID())) {
				parentAttribute = E.GENERALSEARCHDOCUMENT.genericObject.getUID();
			} else {
				for (FieldMeta<?> fMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(attachmentInfo.getDocumentEntityUID()).values()) {
					if (fMeta.getFieldName().equals("genericObject")) {
						parentAttribute = fMeta.getUID();
					}
				}
			}
			if (parentAttribute == null) {
				throw new NuclosFatalException("Parent attribute \"genericObject\" for document attachment not found");
			}
			mdvo.setFieldId(parentAttribute, attachmentInfo.getGenericObjectId());
			mdvo.setFieldValue(attachmentInfo.getDocumentEntityUID(), attachmentInfo.getDocumentEntityUID());
			mdvo.setFieldValue(attachmentInfo.getDocumentFieldUIDs()[0], getSpringLocaleDelegate().getMessage(
					"ReportController.2", "Automatisch angef\u00fcgtes Dokument"));
			mdvo.setFieldValue(attachmentInfo.getDocumentFieldUIDs()[1], new InternalTimestamp(System.currentTimeMillis()));
			mdvo.setFieldValue(attachmentInfo.getDocumentFieldUIDs()[2], Main.getInstance().getMainController().getUserName());
			if (E.GENERALSEARCHDOCUMENT.getUID().equals(attachmentInfo.getDocumentEntityUID())) {
				mdvo.setFieldValue(E.GENERALSEARCHDOCUMENT.documentfile.getUID(), loFile);
			}
			else {
				mdvo.setFieldValue(attachmentInfo.getDocumentFieldUIDs()[3], loFile);
			}

			GenericObjectDelegate.getInstance().attachDocumentToObject(mdvo, parentAttribute);
			return file.getName();
		}
		catch (Exception ex) {
			throw new NuclosReportException(getSpringLocaleDelegate().getMessage(
					"ReportController.1", "Anh\u00e4ngen der Datei \"{0}\" an GenericObject \"{1}\" fehlgeschlagen.", getDocumentName(), attachmentInfo.getGenericObjectIdentifier()), ex);
		}
	}

	private String getDocumentName() {
		if (Thread.currentThread().getClass().equals(ReportThread.class)) {
			/** @todo refactor! */
			return ((ReportThread) Thread.currentThread()).getDocumentName();
		}
		throw new NuclosFatalException("Invalid thread type.");
	}

	/**
	 * @return the current status
	 */
	@Override
	public Status getStatus() {
		return this.status;
	}

	@Override
	public void setStatus(Status status) {
		this.status = status;
		if (this.observable != null) {
			this.observable.notifyObservers();
		}
		
		// bail out if not finished
		if (!status.isFinished()) {
			return;
		}
		final List<ProcessListener> copy;
		synchronized (listeners) {
			// bail out if empty
			if (listeners.isEmpty()) {
				return;
			}
			// defensive copy
			copy = new ArrayList<ProcessListener>(listeners);
		}
		final ProcessEvent event = new ProcessEvent(this);
		for (ProcessListener l: copy) {
			switch (status) {
			case DONE:
				l.onDone(event);
				break;
			case CANCELLED:
				l.onCancel(event);
				break;
			case ERROR:
				l.onError(event);
				break;
			default:
				throw new IllegalStateException(status.name());
			}
		}
	}

	/**
	 * @return the current exception if any
	 */
	@Override
	public Throwable getException() {
		return this.throwable;
	}

	@Override
	public void setException(Throwable throwable) {
		this.throwable = throwable;
	}

	/**
	 * @return the name of the report job
	 */
	@Override
	public String getJobName() {
		return getSpringLocaleDelegate().getMessage("ReportRunner.6", "Report: \"{0}\"", reportname);
	}

	/**
	 * @return the start time of the job
	 */
	@Override
	public Date getStartedAt() {
		return this.dateStartTime;
	}

	/** 
	 * §todo use error message from exception 
	 */
	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;
	}

	protected final String getFileName(String filename) throws IOException {
		return getExportFile(directory, filename, format);
	}
		
	public static String getExportFile(String outputPath, String filename, ReportOutputVO.Format format) throws IOException {
		final String sExportPath = createExportDir(outputPath);
		final boolean isTemp = TEMPDIR.equals(sExportPath);
		
		String result;
		final String extension = format.getExtension();
		final File exportDir = new File(sExportPath);

		if (filename.lastIndexOf(".") != -1) {
			filename = filename.substring(0, filename.lastIndexOf("."));
		}
		filename = filename.replaceAll(extension, "");

		final Calendar cal = Calendar.getInstance();
		final DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		if (exportDir.isDirectory()) {
			result = filename + "_" + dateformat.format(cal.getTime());
			result = result.replaceAll("[/+*%?#!.:]", "-");
			
			if (isTemp) {
				String tDir = System.getProperty("java.io.tmpdir");
				final File f = new File(tDir, result + extension);
				result = f.getPath();
				
			} else {
				result = sExportPath + ((sExportPath.endsWith(File.separator)) ? "" : File.separator) + result;
				
			}
		}
		else {
			result = sExportPath.substring(0, sExportPath.lastIndexOf("."));
			result = result + "_" + dateformat.format(cal.getTime());
			
		}
		
		if (!isTemp) {
			result += extension;
		}
		
		// set the name of the generated report in the thread for further use
		if (Thread.currentThread().getClass().equals(ReportThread.class)) {
			/** @todo refactor! */
			((ReportThread) Thread.currentThread()).setDocumentName(result);
		}

		return result;
	}

	/**
	 * @param sExportPath
	 *            destination
	 * @return directory for export or filename (if filename was given)
	 */
	public static String createExportDir(final String sExportPath) throws IOException {
		final String result;

		if (sExportPath == null) {
			// result = System.getProperty("java.io.tmpdir");
			throw new NullPointerException();
		}
		result = sExportPath;
		final int lastDotPos = sExportPath.lastIndexOf(".");
		final int lastSlashPos = sExportPath.lastIndexOf(File.separator);
		if (lastSlashPos >= lastDotPos) {
			final File fileExportDir = new File(sExportPath);
			if (!fileExportDir.exists()) {
				if (!fileExportDir.mkdir()) {
					throw new IOException(SpringLocaleDelegate.getInstance()
							.getMessage("AbstractReportExporter.1", "Das Verzeichnis {0} konnte nicht angelegt werden.", sExportPath));
				}
			}
		}
		return result;
	}

	protected void open(NuclosFile file, String filename) throws NuclosReportException {
		switch (destination) {
		case FILE:
			openFile(file, filename, false);
			break;
		case PRINTER_CLIENT:
			openPrintDialog(file, filename, true, false);
			break;
		case PRINTER_SERVER:
			openPrintDialog(file, filename, false, false);
			break;
		case DEFAULT_PRINTER_CLIENT:
			openPrintDialog(file, filename, true, true);
			break;
		case DEFAULT_PRINTER_SERVER:
			openPrintDialog(file, filename, false, true);
			break;
		default:
			// TYPE SCREEN
			openFile(file, filename, true);
			break;
		}
	}

	private void openFile(NuclosFile file, String sFileName, boolean bOpenFile) throws NuclosReportException {
		try {
			saveFile(file, sFileName);
			if (bOpenFile) {
				SystemUtils.open(sFileName);
			}
			else {
				LOG.debug("NOT opening " + sFileName);
			}
		}
		catch (IOException ex) {
			throw new NuclosReportException(SpringLocaleDelegate.getInstance().getMessage("AbstractReportExporter.4", "Die Datei {0} konnte nicht ge\u00f6ffnet werden.", sFileName), ex);
		}
	}
	
	public void saveFile(NuclosFile file, String filename) throws IOException {
		saveFile(file, filename, temporaryReport);
	}
	
	public static void saveFile(NuclosFile file, String filename, boolean temporaryReport) throws IOException {
		final File f = new File(filename);
		IOUtils.writeToBinaryFile(f, file.getContent());
		if (temporaryReport) {
			f.deleteOnExit();
		}
	}

	private void openPrintDialog(NuclosFile file, String sFileName, boolean bIsClient, boolean bIsDefault) throws NuclosReportException {
		try {
			PrintService prservDflt;
			if (bIsClient) {
				prservDflt = PrintServiceLookup.lookupDefaultPrintService();
			}
			else {
				prservDflt = ReportDelegate.getInstance().lookupDefaultPrintService();
			}

			PrintService[] prservices;
			if (bIsClient) {
				prservices = PrintServiceLookup.lookupPrintServices(null, null);
			}
			else {
				prservices = ReportDelegate.getInstance().lookupPrintServices(null, null);
			}

			if (null == prservices || 0 >= prservices.length) {
				if (null != prservDflt) {
					prservices = new PrintService[] { prservDflt };
				}
				else {
					throw new NuclosReportException(SpringLocaleDelegate.getInstance().getMessage("AbstractReportExporter.5", "Es ist kein passender Print-Service installiert."));
				}
			}

			PrintService prserv = null;
			PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
			if (prservDflt == null || !bIsDefault) {
				aset.add(new Copies(copies == null || copies <= 0 ? 1 : copies));
				Rectangle gcBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds();
				prserv = ServiceUI.printDialog(null, (gcBounds.width / 2) - 200, (gcBounds.height / 2) - 200, prservices, prservDflt, null, aset);
				aset.remove(MediaPrintableArea.class); // Seitenraender nicht beachten! @see  	CDWEINHANDEL-150
			}
			else {
				aset.add(new Copies(copies == null || copies <= 0 ? 1 : copies));
				prserv = prservDflt;
				aset.remove(MediaPrintableArea.class); // Seitenraender nicht beachten! @see  	CDWEINHANDEL-150
			}

			if (null != prserv) {
				if (bIsClient) {
					saveFile(file, sFileName);
					getNuclosReportPrintJob().print(prserv, sFileName, aset);
				}
				else {
					ReportDelegate.getInstance().printViaPrintService((NuclosReportRemotePrintService) prserv, getNuclosReportPrintJob(), aset, file.getContent());
				}
			}
		}
		catch (Exception e) {
			throw new NuclosReportException(e);
		}
	}
	
	private NuclosReportPrintJob getNuclosReportPrintJob() {
		return getNuclosReportPrintJob(format);
	}
	
	public static NuclosReportPrintJob getNuclosReportPrintJob(Format format) {
		switch (format) {
		case PDF:
			return new PDFPrintJob();
		case CSV:
			return new CSVPrintJob();
		case XLS:
		case XLSX:
			return new XLSPrintJob();
		case DOC:
		case DOCX:
			return new DOCPrintJob();
		default:
			return new FilePrintJob();
		}
	}

	@Override
	public void addProcessListener(ProcessListener listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		synchronized (listeners) {
			if (!listeners.contains(listener)) {
				listeners.add(listener);
			}
		}
	}

	@Override
	public void removeProcessListener(ProcessListener listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}
	
} // class ReportRunner
