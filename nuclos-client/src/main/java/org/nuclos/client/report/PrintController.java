//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.report;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EventListener;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;
import org.fusesource.hawtbuf.ByteArrayInputStream;
import org.jdesktop.swingx.JXTreeTable;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.printout.Printout;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.client.command.OvOpAdapter;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.datasource.admin.DatasourceCollectController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.printservice.PrintDialog;
import org.nuclos.client.printservice.ui.ExtendedAbstractTreeTable;
import org.nuclos.client.printservice.ui.IPrintoutCellEditorHelper;
import org.nuclos.client.printservice.ui.NuclosPrintoutModel;
import org.nuclos.client.printservice.ui.OutputFormatTreeNode;
import org.nuclos.client.printservice.ui.PrintoutNodeUpdateVisitor;
import org.nuclos.client.printservice.ui.PrintoutTreeNode;
import org.nuclos.client.report.reportrunner.BackgroundProcessInfo;
import org.nuclos.client.report.reportrunner.BackgroundProcessStatusController;
import org.nuclos.client.report.reportrunner.BackgroundProcessStatusDialog;
import org.nuclos.client.report.reportrunner.BackgroundProcessStatusTableModel;
import org.nuclos.client.report.reportrunner.BackgroundProcessTableEntry;
import org.nuclos.client.report.reportrunner.ReportRunner;
import org.nuclos.client.ui.Controller;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.OverlayOptionPane;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.multiaction.IMultiActionProgressResultHandler;
import org.nuclos.client.ui.multiaction.MultiActionProgressLine;
import org.nuclos.client.ui.multiaction.MultiActionProgressPanel;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.E;
import org.nuclos.common.Mutable;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.printservice.PrintExecutionContext;
import org.nuclos.common.printservice.PrintServiceFacadeRemote;
import org.nuclos.common.printservice.PrintServiceLocator;
import org.nuclos.common.report.print.PDFPrintJob;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintPropertiesTO;
import org.nuclos.common.report.valueobject.PrintResultTO;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common.report.valueobject.PrintoutTO;
import org.nuclos.common.report.valueobject.ReportOutputVO.Destination;
import org.nuclos.common.report.valueobject.ReportOutputVO.Format;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common.report.valueobject.ReportVO.OutputType;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.SystemUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPrintException;
import org.nuclos.common2.exception.PrintoutException;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

import com.google.common.io.Files;

/**
 * {@link PrintController} formerly known as ReportController
 * controls the printing process/printing Dialog {@link PrintDialog}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintController<PK, Clct extends AbstractCollectable<PK>> extends Controller<JComponent> {
	
	private static final Logger LOG = Logger.getLogger(PrintController.class);
	
	public static enum PrintControllerEventType {
		CANCEL, 		 // dialog was canceled
		SUBMIT_SUCCESS,  // dialog was submitted and all actions were executed successfully
		SUBMIT_ERROR,  	 // dialog was submitted and at least one action failed
		PRINT_ERROR		 // dialog not closed, continue.
	}
	
	public static interface PrintControllerEventListener extends EventListener {
		/**
		 * execute event action
		 * 
		 * @param event	{@link PrintControllerEventType}
		 * @param value payload value
		 */
		public void execute(final PrintControllerEventType event, final Object value);
	}
	
	private static class MultiPrintCollectablesActionController<PK, Clct extends AbstractCollectable<PK>> 
		extends MultiCollectablesActionController<Long, Clct, Object> {
		
		public MultiPrintCollectablesActionController(MainFrameTab tab, List<Clct> collectables, 
				PrintControllerEventListener finalListener, UsageCriteria usagecriteria, 
				UID attachmentSubEntityUID, UID[] attachmentSubEntityAttributeUIDs, List<PrintoutTO> printouts,
				boolean attach) {
			super(tab, collectables, SpringLocaleDelegate.getInstance().getMsg("Print"), tab.getTabIcon(), 
					new MultiPrintAction<PK, Clct>(tab, finalListener, usagecriteria, attachmentSubEntityUID, attachmentSubEntityAttributeUIDs, printouts, attach));
		}
		
		@Override
		public void run(MultiActionProgressPanel pnl) {
			pnl.setResultHandler(new IMultiActionProgressResultHandler() {
				@Override
				public String getSuccessLabel() {
					return SpringLocaleDelegate.getInstance().getMsg("MultiActionProgressResultHandler.3","erfolgreich");
				}
				@Override
				public String getExceptionLabel() {
					return SpringLocaleDelegate.getInstance().getMsg("MultiActionProgressResultHandler.4","nicht erfolgreich");
				}
				@Override public void handleMultiSelection(Collection<MultiActionProgressLine> selection) {
					UID entityUID = null;
					final CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.OR);
					for (MultiActionProgressLine selItem : selection) {
						Clct clct = (Clct) selItem.getSourceObject();
						entityUID = clct.getEntityUID();
						cond.addOperand(new CollectableIdCondition(clct.getId()));
					}
					try {
						NuclosCollectController<?, ?> clctcontroller = NuclosCollectControllerFactory.getInstance().newCollectController(
								entityUID, 
								null, 
								ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
						clctcontroller.runViewResults(cond);
					} catch (CommonFatalException | CommonBusinessException e) {
						Errors.getInstance().showExceptionDialog(getTab(), e);
					} 
				}
				@Override public String getSingleSelectionMenuLabel() {
					return SpringLocaleDelegate.getInstance().getMsg("PrintDialog.new.search.result.from.selection");
				}
				@Override public String getMultiSelectionMenuLabel() {
					return SpringLocaleDelegate.getInstance().getMsg("PrintDialog.new.search.result.from.selection");
				}
				@Override public String getStateHeaderLabel() {return null;}
			});
			super.run(pnl);
		}
		
	}
	
	private static class MultiPrintAction<PK, Clct extends AbstractCollectable<PK>>
		implements org.nuclos.client.ui.multiaction.MultiCollectablesActionController.Action<Clct, Object>,
		PrintControllerEventListener {
		
		private final MainFrameTab tab;
		
		private final PrintControllerEventListener finalListener;
		private final UsageCriteria usagecriteria;
		private final UID attachmentSubEntityUID;
		private final UID[] attachmentSubEntityAttributeUIDs;
		private final List<PrintoutTO> printouts;
		private final boolean attach;
		
		// last execution result
		private PrintControllerEventType event;
		private Object value;
		
		public MultiPrintAction(MainFrameTab tab, PrintControllerEventListener finalListener, UsageCriteria usagecriteria, 
				UID attachmentSubEntityUID, 
				UID[] attachmentSubEntityAttributeUIDs, 
				List<PrintoutTO> printouts,
				boolean attach) {
			super();
			this.tab = tab;
			this.finalListener = finalListener;
			this.usagecriteria = usagecriteria;
			this.attachmentSubEntityUID = attachmentSubEntityUID;
			this.attachmentSubEntityAttributeUIDs = attachmentSubEntityAttributeUIDs;
			this.printouts = printouts;
			this.attach = attach;
		}

		@Override
		public Object perform(Clct clct, Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
			final PrintExecutionContext context = new PrintExecutionContext(usagecriteria, (Long) clct.getId(), attachmentSubEntityUID, attachmentSubEntityAttributeUIDs);
			List<PrintoutTO> printoutsWithPk = new ArrayList<PrintoutTO>();
			for (PrintoutTO printout : printouts) {

				// clone printout and set id
				PrintoutTO printoutWithPk = printout.clone();
				printoutWithPk.setBusinessObjectId((Long) clct.getId());
				
				if (!fillDatasourceParameterMaps(tab, printoutWithPk, getIdentifierLabel(clct))) {
					LOG.debug("user canceled datasource param input");
					continue;
				}
				printoutsWithPk.add(printoutWithPk);
			}
			
			if (!printoutsWithPk.isEmpty()) {
				String identifierLabel = getIdentifierLabel(clct);
				PrintSwingWorker printWorker = createPrintWorker(tab, context, identifierLabel, printoutsWithPk, attach, this, null);
				printWorker.executeInForeground();
				if (event == PrintControllerEventType.PRINT_ERROR) {
					finalListener.execute(event, value);
				}
				if (event == PrintControllerEventType.PRINT_ERROR || event == PrintControllerEventType.SUBMIT_ERROR) {
					throw new NuclosBusinessRuleException(SpringLocaleDelegate.getInstance().getMsg("PrintDialog.error.in.background"));
				}
			}
			return null;
		}
		
		// PrintControllerEventListener
		@Override
		public void execute(PrintControllerEventType event, Object value) {
			this.event = event;
			this.value = value;
		}
		
		@Override
		public String getText(Clct clct) {
			return SpringLocaleDelegate.getInstance().getMsg("Print") + "... " + getIdentifierLabel(clct);
		}
		
		@Override
		public String getSuccessfulMessage(Clct clct, Object rResult) {
			return SpringLocaleDelegate.getInstance().getMsg("Print") + "... " + getIdentifierLabel(clct);
		}
		
		@Override
		public String getExceptionMessage(Clct clct, Exception ex) {
			return SpringLocaleDelegate.getInstance().getMsg("Print") + "... " + getIdentifierLabel(clct) + ": " + ex.getLocalizedMessage();
		}
		
		@Override
		public String getConfirmStopMessage() {
			return SpringLocaleDelegate.getInstance().getMsg("PrintDialog.confirm.cancel.multi");
		}
		
		@Override
		public void executeFinalAction() throws CommonBusinessException {
			finalListener.execute(PrintControllerEventType.SUBMIT_SUCCESS, null);
		}	
	}

	

	
	private PrintDialog dialog;
	private NuclosPrintoutModel modelPrintouts;
	private final List<PrintServiceTO> modelPrintServices;
	private final List<PrintServiceTO> modelPrintServicesClient;
	// @FIXME use appropriate delegate
	private final PrintServiceFacadeRemote psFacadeRemote;
	private final PropertyChangeSupport propertyChangeSupport;
	private final IPrintoutCellEditorHelper cellEditorHelper;
	
	private MainFrameTab tab;
	private OverlayOptionPane optionPane;
	
	private final int[] preferredColumnWidths = new int[]{
		300, 40, 200, 100, 50, 40
	};
	
	private static String getDestinationForRenderer(Destination destination) {
		String resId = null;
		switch (destination) {
		case DEFAULT_PRINTER_CLIENT:
		case PRINTER_CLIENT:
			resId = Destination.DEFAULT_PRINTER_CLIENT.getResourceId();
			break;
		case DEFAULT_PRINTER_SERVER:
		case PRINTER_SERVER:
			resId = Destination.DEFAULT_PRINTER_SERVER.getResourceId();
			break;
		default:
			resId = destination.getResourceId();
		}
		assert resId != null;
		return SpringLocaleDelegate.getInstance().getText(resId);
	}

	public PrintController(final MainFrameTab tab) {
		super(tab);
		this.tab = tab;
		this.modelPrintouts = new NuclosPrintoutModel(new ArrayList<PrintoutTreeNode>());
		this.psFacadeRemote = SpringApplicationContextHolder.getBean(PrintServiceFacadeRemote.class);
		this.modelPrintServices = new ArrayList<PrintServiceTO>();
		this.modelPrintServicesClient = new ArrayList<PrintServiceTO>();
		for (PrintService psrv : PrintServiceLocator.lookupPrintServices()) {
			PrintServiceTO psrvTO = new PrintServiceTO(psrv.getName());
			for (PrintServiceTO.Tray tray : PrintServiceLocator.createTrayList(psrv)) {
				psrvTO.addTray(tray);
			}
			modelPrintServicesClient.add(psrvTO);
		}
		Collections.sort(modelPrintServicesClient);
		this.modelPrintouts.setPrintServicesClient(this.modelPrintServicesClient);
		
		this.cellEditorHelper = new IPrintoutCellEditorHelper() {
			@Override
			public PrintServiceTO printServiceAtRow(int iRow) {
				final ExtendedAbstractTreeTable controlComponent = dialog.getOutputSelectionComponent().getControlComponent();
				final TreePath pathForRow = controlComponent.getPathForRow(iRow);
				final int iCol = modelPrintouts.getIndexOfColumn(E.REPORTOUTPUT.printservice.getUID());
				final Object valueAt = modelPrintouts.getValueAt(pathForRow.getLastPathComponent(), iCol);
				return (PrintServiceTO) valueAt;
			}
		};
		this.propertyChangeSupport = new PropertyChangeSupport(this);
		setupNodeUpdateVisitors(modelPrintouts);
		
		this.dialog = createDialog();
		this.dialog.getOutputSelectionComponent().getControlComponent().setVisible(false);
		this.dialog.getOutputSelectionComponent().getControlComponent().setTreeTableModel(modelPrintouts);
		this.dialog.getOutputSelectionComponent().getControlComponent().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.dialog.getOutputSelectionComponent().getControlComponent().getTableHeader().setReorderingAllowed(false);
		this.dialog.getOutputSelectionComponent().getControlComponent().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				SwingUtilities.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						final ExtendedAbstractTreeTable controlComponent = dialog.getOutputSelectionComponent().getControlComponent();
						for (int i = 0; i < preferredColumnWidths.length; i++) {
							controlComponent.getColumn(i).setPreferredWidth(preferredColumnWidths[i]);
						}
						controlComponent.setVisible(true);
						
						final int iColChoose = modelPrintouts.getIndexOfColumn(NuclosPrintoutModel.COLUMN_CHOOSE);
						final int iColPrintservice = modelPrintouts.getIndexOfColumn(E.REPORTOUTPUT.printservice.getUID());
						final int iColTray = modelPrintouts.getIndexOfColumn(E.REPORTOUTPUT.tray.getUID());
						final int iColCopies = modelPrintouts.getIndexOfColumn(E.REPORTOUTPUT.copies.getUID());
						final int iColDuplex = modelPrintouts.getIndexOfColumn(E.REPORTOUTPUT.isDuplex.getUID());
						final TableCellRenderer cellRendererChoose = controlComponent.getCellRenderer(0, iColChoose);
						final TableCellRenderer cellRendererPrintService = controlComponent.getCellRenderer(0, iColPrintservice);
						final TableCellRenderer cellRendererTray = controlComponent.getCellRenderer(0, iColTray);
						final TableCellRenderer cellRendererCopies = controlComponent.getCellRenderer(0, iColCopies);
						final TableCellRenderer cellRendererDuplex = controlComponent.getCellRenderer(0, iColDuplex);
						
						controlComponent.getColumn(iColChoose).setCellRenderer(new TableCellRenderer() {
							private final JLabel nullLabel = new JLabel("");
							@Override
							public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
								JCheckBox result = (JCheckBox) cellRendererChoose.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
								boolean enabled = true;
								final TreePath pathForRow = controlComponent.getPathForRow(row);
								enabled = modelPrintouts.isCellEditable(pathForRow.getLastPathComponent(), column);
								if (pathForRow.getLastPathComponent() instanceof PrintoutTreeNode) {
									final PrintoutTreeNode poNode = (PrintoutTreeNode) pathForRow.getLastPathComponent();
									if (poNode.getOutputType() == OutputType.SINGLE) {
										nullLabel.setBackground(result.getBackground());
										return nullLabel;
									}
								}
//								if (pathForRow.getLastPathComponent() instanceof OutputFormatTreeNode) {
//									final OutputFormatTreeNode formatNode = (OutputFormatTreeNode) pathForRow.getLastPathComponent();
//									final PrintoutTreeNode poNode = (PrintoutTreeNode) formatNode.getParent();
//									if (formatNode.isMandatory() || poNode.getOutputType() != OutputType.SINGLE) {
//										enabled = false;
//									}
//								}
								result.setEnabled(enabled);
								return result;
							}
						});
						
						controlComponent.getColumn(iColPrintservice).setCellRenderer(new TableCellRenderer() {
							private final JLabel printerLabel = new JLabel("");
							@Override
							public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
								Component result = cellRendererPrintService.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
								final TreePath pathForRow = controlComponent.getPathForRow(row);
								if (pathForRow.getLastPathComponent() instanceof PrintoutTreeNode) {
									printerLabel.setBackground(result.getBackground());
									printerLabel.setText("");
									result = printerLabel;
								} else if (pathForRow.getLastPathComponent() instanceof OutputFormatTreeNode) {
									OutputFormatTreeNode formatNode = (OutputFormatTreeNode) pathForRow.getLastPathComponent();
									if (formatNode.getOutputFormat().getDestination() == Destination.FILE ||
										formatNode.getOutputFormat().getDestination() == Destination.SCREEN) {
										printerLabel.setBackground(result.getBackground());
										printerLabel.setText(getDestinationForRenderer(formatNode.getOutputFormat().getDestination()));
										result = printerLabel;
									} else {
										if (formatNode.getProperties().getPrinterNameOnClient() == null &&
											formatNode.getProperties().getPrintServiceId() == null &&
											formatNode.getOutputFormat().getProperties().getPrintServiceId() == null) {
											JLabel lblResult = (JLabel) result;
											lblResult.setText(getDestinationForRenderer(formatNode.getOutputFormat().getDestination()));
										}
									}
								}
								return result;
							}
						});
						
						controlComponent.getColumn(iColTray).setCellRenderer(new TableCellRenderer() {
							private final JLabel nullLabel = new JLabel("");
							@Override
							public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
								Component result = cellRendererTray.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
								final TreePath pathForRow = controlComponent.getPathForRow(row);
								if (pathForRow.getLastPathComponent() instanceof PrintoutTreeNode) {
									nullLabel.setBackground(result.getBackground());
									result = nullLabel;
								} else if (pathForRow.getLastPathComponent() instanceof OutputFormatTreeNode) {
									OutputFormatTreeNode formatNode = (OutputFormatTreeNode) pathForRow.getLastPathComponent();
									if (formatNode.getOutputFormat().getDestination() == Destination.FILE ||
										formatNode.getOutputFormat().getDestination() == Destination.SCREEN) {
										nullLabel.setBackground(result.getBackground());
										result = nullLabel;
									}
								}
								return result;
							}
						});
						
						controlComponent.getColumn(iColCopies).setCellRenderer(new TableCellRenderer() {
							private final JLabel nullLabel = new JLabel("");
							@Override
							public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
								Component result = cellRendererCopies.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
								final TreePath pathForRow = controlComponent.getPathForRow(row);
								if (pathForRow.getLastPathComponent() instanceof PrintoutTreeNode) {
									nullLabel.setBackground(result.getBackground());
									result = nullLabel;
								} else if (pathForRow.getLastPathComponent() instanceof OutputFormatTreeNode) {
									OutputFormatTreeNode formatNode = (OutputFormatTreeNode) pathForRow.getLastPathComponent();
									if (formatNode.getOutputFormat().getDestination() == Destination.FILE ||
										formatNode.getOutputFormat().getDestination() == Destination.SCREEN) {
										nullLabel.setBackground(result.getBackground());
										result = nullLabel;
									}
								}
								return result;
							}
						});
						
						controlComponent.getColumn(iColDuplex).setCellRenderer(new TableCellRenderer() {
							private final JLabel nullLabel = new JLabel("");
							@Override
							public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
								Component result = cellRendererDuplex.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
								final TreePath pathForRow = controlComponent.getPathForRow(row);
								if (pathForRow.getLastPathComponent() instanceof PrintoutTreeNode) {
									nullLabel.setBackground(result.getBackground());
									result = nullLabel;
								} else if (pathForRow.getLastPathComponent() instanceof OutputFormatTreeNode) {
									OutputFormatTreeNode formatNode = (OutputFormatTreeNode) pathForRow.getLastPathComponent();
									if (formatNode.getOutputFormat().getDestination() == Destination.FILE ||
										formatNode.getOutputFormat().getDestination() == Destination.SCREEN) {
										nullLabel.setBackground(result.getBackground());
										result = nullLabel;
									}
								}
								return result;
							}
						});
					}
				});
			}
		});
	}
	
	protected void updateOkButtonState() {
		boolean enabled = !modelPrintouts.getSelectedPrintouts().isEmpty();
		updateOkButtonState(enabled);
	}
	
	protected void updateOkButtonState(boolean enabled) {
		if (optionPane == null) {
			return;
		}
		optionPane.getJButtonOK().setEnabled(enabled);
		if (enabled && !dialog.getAttachDocumentCheckBox().isSelected()) {
			boolean autoEnableAttach = false;
			for (PrintoutTO printout : modelPrintouts.getSelectedPrintouts()) {
				if (autoEnableAttach) {
					break;
				}
				for (OutputFormatTO outputFormat : printout.getOutputFormats()) {
					if (outputFormat.isAttachDocument()) {
						autoEnableAttach = true;
						break;
					}
				}
			}
			if (autoEnableAttach) {
				dialog.getAttachDocumentCheckBox().setSelected(true);
			}
		}
	}
	
	protected void setupNodeUpdateVisitors(final NuclosPrintoutModel model) {
		model.setNodeVisitor(NuclosPrintoutModel.COLUMN_CHOOSE, new PrintoutNodeUpdateVisitor() {

			@Override
			public void visit(UID idColumn, TreeNode node, Object oldValue,
					Object newValue) {
				if (node instanceof PrintoutTreeNode) {
					final PrintoutTreeNode poNode = (PrintoutTreeNode)node;
					for (final OutputFormatTreeNode outputFormatNode : poNode.getOutputFormats()) {
						if (poNode.getOutputType() == OutputType.COLLECTIVE) {
							outputFormatNode.setChoose((Boolean)newValue);
						}
						else 
							if (modelPrintouts.isCellEditable(outputFormatNode, 
								modelPrintouts.getIndexOfColumn(NuclosPrintoutModel.COLUMN_CHOOSE))
						) {
							outputFormatNode.setChoose((Boolean)newValue);
						}
					}
					updateOkButtonState();
				} else if (node instanceof OutputFormatTreeNode) {
					final OutputFormatTreeNode ofNode = (OutputFormatTreeNode) node;
					final PrintoutTreeNode poNode = (PrintoutTreeNode) ofNode.getParent();
					// evaluate output type
					if (ReportVO.OutputType.COLLECTIVE.equals(poNode.getOutputType())) {
						poNode.setChoose((Boolean)newValue);
						for (final OutputFormatTreeNode outputFormatNode : poNode.getOutputFormats()) {
							outputFormatNode.setChoose((Boolean)newValue);
						}
					}
					updateOkButtonState();
				}
				
			}
			
			
		});
	}
	
	protected PrintDialog createDialog() {
		final PrintDialog d = new PrintDialog(tab, preferredColumnWidths, modelPrintServicesClient, cellEditorHelper);
		//d.addListener(this);
		propertyChangeSupport.addPropertyChangeListener(d);
		final ExtendedAbstractTreeTable table = d.getOutputSelectionComponent().getControlComponent();
		
		/**
		 * attach mouse listener for preview action
		 */
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
//				if (2 == e.getClickCount()) {
					assert (e.getSource() instanceof JXTreeTable);
					final JXTreeTable table = (JXTreeTable)e.getSource();
					final TreePath path = table.getPathForRow(table.rowAtPoint(e.getPoint()));
					int idxRow = table.rowAtPoint(e.getPoint());
					int idxCol = table.columnAtPoint(e.getPoint());
					if (idxRow == -1) {
						LOG.warn("illegal row selection");
						return;
					}
					if (idxCol == -1) {
						LOG.warn("illegal column selection");
						return;
					}
					final int iColChoose = modelPrintouts.getIndexOfColumn(NuclosPrintoutModel.COLUMN_CHOOSE);
					
					final Object lastNode = path.getLastPathComponent();
					final Object lastParentNode = path.getParentPath().getLastPathComponent();
					if (
						lastNode != null && (lastNode instanceof OutputFormatTreeNode)
						&& lastParentNode != null && (lastParentNode instanceof PrintoutTreeNode)
					) {
						final OutputFormatTreeNode outputFormatNode = (OutputFormatTreeNode) lastNode;
						final PrintoutTreeNode printoutNode = (PrintoutTreeNode) lastParentNode;
						if (2 == e.getClickCount() && idxCol == 0) {
							previewOnScreen(
								printoutNode.getPrintout(),
								outputFormatNode.getOutputFormat()
							);
						} else if (1 == e.getClickCount() && idxCol != iColChoose) {
							Mutable<Boolean> isSelectionEmpty = new Mutable<Boolean>(true); 
							TreeNode selectedNode = modelPrintouts.getSelectedSingleNonMandatoryTreeNode(isSelectionEmpty);
							if (isSelectionEmpty.getValue()) {
								if (modelPrintouts.isCellEditable(outputFormatNode, iColChoose)) {
									outputFormatNode.setChoose(true);
									updateOkButtonState(true);
								}
							} else if (selectedNode != null && selectedNode instanceof OutputFormatTreeNode) {
								OutputFormatTreeNode selectedFormatNode = (OutputFormatTreeNode) selectedNode;
								if (!selectedFormatNode.isEdited() && 
										!selectedFormatNode.getId().equals(outputFormatNode.getId())) {
									if (modelPrintouts.isCellEditable(selectedFormatNode, iColChoose) &&
										modelPrintouts.isCellEditable(outputFormatNode, iColChoose)) {
										outputFormatNode.setChoose(true);
										selectedFormatNode.setChoose(false);
										updateOkButtonState(true);
									}
								}
							} else if (selectedNode != null && selectedNode instanceof PrintoutTreeNode) {
								PrintoutTreeNode selectedPrintoutNode = (PrintoutTreeNode) selectedNode;
								if (selectedPrintoutNode.getOutputType() != OutputType.SINGLE) {
									if (modelPrintouts.isCellEditable(outputFormatNode, iColChoose)) {
										outputFormatNode.setChoose(true);
										selectedPrintoutNode.setChoose(false);
										updateOkButtonState(true);
									}
								}
							}
						} 
					} else if (lastNode != null && (lastNode instanceof PrintoutTreeNode)) {
						final PrintoutTreeNode printoutNode = (PrintoutTreeNode) lastNode;
						if (2 == e.getClickCount() && printoutNode.getOutputType() == OutputType.EXCEL) {
							previewOnScreen(printoutNode.getPrintout());
						} else if (1 == e.getClickCount() && idxCol != iColChoose && printoutNode.getOutputType() != OutputType.SINGLE) {
							Mutable<Boolean> isSelectionEmpty = new Mutable<Boolean>(true); 
							TreeNode selectedNode = modelPrintouts.getSelectedSingleNonMandatoryTreeNode(isSelectionEmpty);
							if (isSelectionEmpty.getValue()) {
								printoutNode.setChoose(true);
								updateOkButtonState(true);
							} else if (selectedNode != null && selectedNode instanceof PrintoutTreeNode) {
								PrintoutTreeNode selectedPrintoutNode = (PrintoutTreeNode) selectedNode;
								if (selectedPrintoutNode.getOutputType() != OutputType.SINGLE && 
										!selectedPrintoutNode.getId().equals(printoutNode.getId())) {
									printoutNode.setChoose(true);
									selectedPrintoutNode.setChoose(false);
									updateOkButtonState(true);
								}
							} else if (selectedNode != null && selectedNode instanceof OutputFormatTreeNode) {
								OutputFormatTreeNode selectedFormatNode = (OutputFormatTreeNode) selectedNode;
								if (!selectedFormatNode.isEdited()) {
									if (modelPrintouts.isCellEditable(selectedFormatNode, iColChoose) && 
										printoutNode.getOutputType() != OutputType.SINGLE) {
										printoutNode.setChoose(true);
										selectedFormatNode.setChoose(false);
										updateOkButtonState(true);
									}
								}
							}
						} 
					}
//				}
			}
		});
		d.getCollapseTreeButton().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				d.getOutputSelectionComponent().getControlComponent().collapseAll();
			}
			
		});
		return d;
	}
	
	/**
	 * run preview for {@link OutputFormat}
	 * 
	 * @param outputFormat {@link OutputFormatTO}
	 * @param idBo	{@link BusinessObject} id
	 */
	protected void previewOnScreen(final PrintoutTO printout, final OutputFormatTO outputFormat) {
		if (printout.getBusinessObjectId() == null) {
			// multiprint
			return;
		}
		if (!fillDatasourceParameterMap(getParent(), printout, outputFormat)) {
			LOG.debug("user canceled datasource param input");
			return;
		}
		new SwingWorker<PrintResultTO,Integer>() {

			@Override
			protected PrintResultTO doInBackground() throws Exception {
				LOG.info("run preview for "+outputFormat+" businessobject "+printout.getBusinessObjectId());
				return psFacadeRemote.executeOutputFormat(outputFormat, printout.getBusinessObjectId());
			}
			
			protected void done() {
				try {
					final PrintResultTO result = get();
					final NuclosFile previewFile = result.getOutput();
					final File tmpFiles = File.createTempFile("tmp",previewFile.getName());
					Files.write(previewFile.getContent(), tmpFiles);
					SystemUtils.open(tmpFiles);

				} catch (final InterruptedException ex) {
					LOG.error(ex.getMessage(), ex);
				} catch (final ExecutionException ex) {
					LOG.error(ex.getMessage(), ex);
				} catch (final IOException ex) {
					LOG.error(ex.getMessage(), ex);
				}
			}
		
		}.execute();
	}
	

	/**
	 * run preview for {@link Printout}
	 * 
	 * @param printout {@link PrintoutTO}
	 */
	protected void previewOnScreen(final PrintoutTO printout) {
		if (printout.getBusinessObjectId() == null) {
			// multiprint
			return;
		}
		if (!fillDatasourceParameterMaps(printout)) {
			LOG.debug("user canceled datasource param input");
			return;
		}
		new SwingWorker<List<PrintResultTO>,Integer>() {

			@Override
			protected List<PrintResultTO> doInBackground() throws Exception {
				LOG.info("run preview for "+printout);
				return psFacadeRemote.executePreview(Collections.singletonList(printout));
			}
			
			protected void done() {
				try {
					final List<PrintResultTO> results = get();
					for (PrintResultTO result : results) {
					final NuclosFile previewFile = result.getOutput();
					final File tmpFiles = File.createTempFile("tmp",previewFile.getName());
					Files.write(previewFile.getContent(), tmpFiles);
					SystemUtils.open(tmpFiles);
					}
				} catch (final InterruptedException ex) {
					LOG.error(ex.getMessage(), ex);
				} catch (final ExecutionException ex) {
					LOG.error(ex.getMessage(), ex);
				} catch (final IOException ex) {
					LOG.error(ex.getMessage(), ex);
				}
			}
		
		}.execute();
	}
	
	public void show(
			final UsageCriteria usagecriteria, 
			final List<Clct> collectables, 
			final UID attachmentSubEntityUID,
			final UID[] attachmentSubEntityAttributeUIDs,
			final PrintControllerEventListener listener) {
		
		dialog.getAttachDocumentCheckBox().setVisible(attachmentSubEntityUID != null);
		
		final List<PrintoutTO> resultPrintouts = new ArrayList<PrintoutTO>();
		final List<PrintServiceTO> resultPrintServices = new ArrayList<PrintServiceTO>();
		
		
		new SwingWorker<List<Void>, Void>() {

			@Override
			protected List<Void> doInBackground() throws Exception {
				
				// fetch output formats
				PK pk = null;
				if (collectables.size() == 1) {
					pk = collectables.get(0).getPrimaryKey();
				}
				resultPrintouts.addAll(fetchPrintoutList(usagecriteria, pk));
				
				// fetch printservice
				resultPrintServices.addAll(fetchPrintServiceList());
				
				return null;
			}
			
			@Override
			protected void done() {
				try {
					get();
					// test for direct printing without dialog...
					if (collectables.size() == 1 && resultPrintouts.size() == 1) {
						PrintoutTO printout = resultPrintouts.get(0);
						if (printout.getBusinessObjectId() != null
								&& printout.getOutputFormats().size() == 1) {
							if (!fillDatasourceParameterMaps(printout)) {
								LOG.debug("user canceled datasource param input");
								return;
							}
							OutputFormatTO outputFormat = printout.getOutputFormats().get(0);
							if (outputFormat.getDestination() == Destination.DEFAULT_PRINTER_CLIENT || 
								outputFormat.getDestination() == Destination.DEFAULT_PRINTER_SERVER || 
								outputFormat.getDestination() == Destination.FILE || 
								outputFormat.getDestination() == Destination.SCREEN) {
								// direct printing...
								final String identifierLabel = getIdentifierLabel(collectables.get(0));
								final PrintExecutionContext context = new PrintExecutionContext(usagecriteria, printout.getBusinessObjectId(), attachmentSubEntityUID, attachmentSubEntityAttributeUIDs);
								SwingWorker<List<PrintResultTO>, Void> printWorker = createPrintWorker(context, identifierLabel, resultPrintouts, outputFormat.isAttachDocument(), listener, null);
								printWorker.execute();
								
								return;
							}
						}
					}
					
					//open dialog
					UIUtils.runCommand(getParent(), new Runnable() {

						@Override
						public void run() {
							if (LOG.isDebugEnabled()) {
								LOG.debug("publish model change printservices : "+resultPrintServices.size());
							}
							
							modelPrintouts.setPrintServices(resultPrintServices);

							propertyChangeSupport.firePropertyChange(
									new PropertyChangeEvent(
											this, 
											PrintEvents.PROPERTY_MODEL_PRINTSERVICES, 
											modelPrintServices, resultPrintServices)
									);
							modelPrintServices.clear();
							modelPrintServices.addAll(resultPrintServices);

							
							if (LOG.isDebugEnabled()) {
								LOG.debug("publish model change printouts : "+resultPrintouts.size());
							}
							
							modelPrintouts.clear();
							for (final PrintoutTO printout : resultPrintouts) {
								modelPrintouts.addPrintout(printout);
							}
							modelPrintouts.fireTreeStructureChanged(new TreePath(modelPrintouts.getRoot()));
							dialog.getOutputSelectionComponent().getControlComponent().expandAll();
							
							final String resourceTitle = collectables.size() > 1 ? "PrintDialog.title.multi" : "PrintDialog.title";

							optionPane = OverlayOptionPane.showConfirmDialog(
									tab, 
									dialog,
									SpringLocaleDelegate.getInstance().getMessage(resourceTitle, "Formularauswahl") , 
									OverlayOptionPane.OK_CANCEL_OPTION, 
									true, 
									new OvOpAdapter() {
										@Override
										public void done(int result) {
											if (OverlayOptionPane.OK_OPTION == result) {
												// do nothing here, action is replaced
											} else {
												PrintController.this.performCancelAction(listener);
											}
										}
									});
							
							final Action defaultOkAction = optionPane.getJButtonOK().getAction();
							final Date start = new Date();
							optionPane.getJButtonOK().setAction(new AbstractAction((String)defaultOkAction.getValue(Action.NAME)) {
								@Override
								public void actionPerformed(final ActionEvent e) {
									try {
										PrintControllerEventListener doNotCloseOnPrintErrorListener = new PrintControllerEventListener() {
											@Override
											public void execute(PrintControllerEventType event, Object value) {
												if (PrintControllerEventType.PRINT_ERROR == event) {
													handleError(value, start);
												} else {
													// submit and close...
													listener.execute(event, value);
													defaultOkAction.actionPerformed(e);  
												}
											}
										}; 
										PrintController.this.performPrintAction(usagecriteria, collectables, attachmentSubEntityUID, attachmentSubEntityAttributeUIDs, 
												doNotCloseOnPrintErrorListener);
									} catch (Exception e1) {
										Errors.getInstance().showExceptionDialog(dialog, e1);
									}
								}
							});
							
							updateOkButtonState();
							
						}
					});
					
					
				} catch (Exception ex) {
					if (ex instanceof ExecutionException && ex.getCause() instanceof Exception) {
						ex = (Exception) ((ExecutionException) ex).getCause();
					}
					LOG.error(ex.getMessage(), ex);
					listener.execute(PrintControllerEventType.SUBMIT_ERROR, Collections.singletonList(new PrintResultTO(null, ex, null, null)));
				} 
			}
			
		}.run();
	}
	
	private void handleError(final Object value, final Date start) {
		List<PrintResultTO> errorResult = (List<PrintResultTO>) value;
		final BackgroundProcessStatusDialog bgProcessDialog = BackgroundProcessStatusController.getStatusDialog(
				UIUtils.getFrameForComponent(dialog));
		final BackgroundProcessStatusTableModel bgProcessModel = bgProcessDialog.getStatusPanel().getModel();
		bgProcessDialog.show();
		for (final PrintResultTO pr : errorResult) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					String title;
					String message;
					title = SpringLocaleDelegate.getInstance().getMsg("Print") + "... " + pr.getSourceObjectIdentifierLabel();
					if (pr.getOutputFormat() != null && !StringUtils.looksEmpty(pr.getOutputFormat().getDescription())) {
						title = title + " / " + pr.getOutputFormat().getDescription();
					}
					if (pr.getException() == null) {
						message = pr.getMessage();
					} else {
						if (!StringUtils.looksEmpty(pr.getMessage())) {
							title = title + " (" + pr.getMessage() + ")";
						}
						message = extractDetailMessageFromThrowable(pr.getException());
					}
					
					BackgroundProcessTableEntry backgroundProcessTableEntry = new BackgroundProcessTableEntry(title, BackgroundProcessInfo.Status.ERROR, start);
					backgroundProcessTableEntry.setMessage(message);
					backgroundProcessTableEntry.setException(pr.getException());
					bgProcessModel.addEntry(backgroundProcessTableEntry);
				}
			});
		}
	}
	
	/**
	 * get the most detailed exception message for the user
	 * 
	 * @param throwable {@link Throwable}
	 * 
	 * @return exception message
	 */
	private String extractDetailMessageFromThrowable(final Throwable throwable) {
		Throwable detailThrowable = throwable;
		String message = "";
		   while (detailThrowable != null) {
		        message = detailThrowable.getMessage();
		        detailThrowable = detailThrowable.getCause();
		    }
		   
		return message;
	}
	
	protected void performCancelAction(PrintControllerEventListener listener) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("submitted performCancelAction");
		}
		listener.execute(PrintControllerEventType.CANCEL, null);
	}

	protected void performPrintAction(
			final UsageCriteria usagecriteria, 
			final List<Clct> collectables, 
			final UID attachmentSubEntityUID,
			final UID[] attachmentSubEntityAttributeUIDs,
			final PrintControllerEventListener listener) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("submitted performPrintAction");
		}
		
		final boolean attach = dialog.getAttachDocumentCheckBox().isSelected();
		final List<PrintoutTO> selectedPrintouts = modelPrintouts.getSelectedPrintouts();
		if (LOG.isDebugEnabled()) {
			LOG.debug("selected "+selectedPrintouts.size()+" printouts");
		}
		for (final PrintoutTO printout : selectedPrintouts) {
			if (LOG.isTraceEnabled()) {
				LOG.trace("selected printout "+printout);
				for (final OutputFormatTO outputformat : printout.getOutputFormats()) {
					LOG.trace("selected outputformat "+outputformat);	
				}
			}
		}
		
		final Container optionPaneParent = optionPane.getParent();
		optionPaneParent.setVisible(false);
		
		if (collectables.size() == 1) {
			for (PrintoutTO printout : selectedPrintouts) {
				if (!fillDatasourceParameterMaps(printout)) {
					LOG.debug("user canceled datasource param input");
					listener.execute(PrintControllerEventType.CANCEL, null);
					return;
				}
			}
			Clct clct = collectables.get(0);
			String identifierLabel = getIdentifierLabel(clct);
			final PrintExecutionContext context = new PrintExecutionContext(usagecriteria, (Long) clct.getId(), attachmentSubEntityUID, attachmentSubEntityAttributeUIDs);
			SwingWorker<List<PrintResultTO>, Void> printWorker = createPrintWorker(context, identifierLabel, selectedPrintouts, attach, listener, optionPaneParent);
			printWorker.execute();
		} else {
			// multiprint
			MultiPrintCollectablesActionController<PK, Clct> multiActionController = new MultiPrintCollectablesActionController<PK, Clct>(tab, 
					collectables, listener, usagecriteria, attachmentSubEntityUID, attachmentSubEntityAttributeUIDs, selectedPrintouts, attach);
			multiActionController.run(new MultiActionProgressPanel(collectables.size()));
		}
	}
	
	protected SwingWorker<List<PrintResultTO>, Void> createPrintWorker(final PrintExecutionContext context, final String identifierLabel, 
			final List<PrintoutTO> selectedPrintouts, final boolean attachDocuments, final PrintControllerEventListener listener, final Component showOnException) {
		return createPrintWorker(tab, context, identifierLabel, selectedPrintouts, attachDocuments, listener, showOnException);
	}
	
	private static PrintSwingWorker createPrintWorker(final MainFrameTab tab, final PrintExecutionContext context, final String identifierLabel,
			final List<PrintoutTO> selectedPrintouts, final boolean attachDocuments, final PrintControllerEventListener listener, final Component showOnException) {
		PrintSwingWorker printWorker = new PrintSwingWorker(tab, context, identifierLabel, selectedPrintouts, attachDocuments, listener, showOnException);	
		return printWorker;
	}
	
	private static class PrintSwingWorker extends SwingWorker<List<PrintResultTO>, Void> {

		final MainFrameTab tab;
		final PrintExecutionContext context;
		final String identifierLabel;
		final List<PrintoutTO> selectedPrintouts;
		final boolean attachDocuments;
		final PrintControllerEventListener listener;
		final Component showOnException;
		
		public PrintSwingWorker(MainFrameTab tab, PrintExecutionContext context, String identifierLabel, List<PrintoutTO> selectedPrintouts, boolean attachDocuments, PrintControllerEventListener listener,
				Component showOnException) {
			super();
			this.tab = tab;
			this.context = context;
			this.identifierLabel = identifierLabel;
			this.selectedPrintouts = selectedPrintouts;
			this.attachDocuments = attachDocuments;
			this.listener = listener;
			this.showOnException = showOnException;
		}

		private final Date start = new Date();
		private PrintoutException ex1;
		private NuclosBusinessRuleException ex2;
		
		boolean executeInForeground = false;
		
		@Override
		protected List<PrintResultTO> doInBackground() throws Exception {
			try {
				for (PrintoutTO printout : selectedPrintouts) {
					for (OutputFormatTO outputFormat : printout.getOutputFormats()) {
						outputFormat.setAttachDocument(attachDocuments);
					}
				}
				return SpringApplicationContextHolder.getBean(PrintServiceFacadeRemote.class).executePrintout(selectedPrintouts, context);
			} catch (final PrintoutException ex) {
				ex1 = ex;
			} catch (NuclosBusinessRuleException ex) {
				ex2 = ex;
			}
			return null;
		}
		
		public void executeInForeground() {
			executeInForeground = true;
			done();
		}
		
		private List<PrintResultTO> executePrintout() throws PrintoutException, NuclosBusinessRuleException {
			List<PrintResultTO> result;
			try {
				if (executeInForeground) {
					result = doInBackground();
				} else {
					result = get();
				}
			} catch (Exception ex) {
				if (showOnException != null) {
					showOnException.setVisible(true);
				}
				Errors.getInstance().showDetailedExceptionDialog(tab, ex);
				throw new NuclosFatalException(ex);
			}
			if (ex1 != null) {
				throw ex1;
			} else if (ex2 != null) {
				throw ex2;
			} else {
				return result;
			}
		}
		
		@Override
		protected void done() {
			try {
				boolean success = true;
				List<PrintResultTO> errorResult = new ArrayList<PrintResultTO>(); 
				List<PrintResultTO> clientPrintouts = executePrintout();
				for (PrintResultTO cpo : clientPrintouts) {
					final PrintPropertiesTO ppto = cpo.getOutputFormat().getProperties();
					String outputPath = cpo.getOutputFormat().getOutputPath();
					Format format = cpo.getOutputFormat().getFormat();

					switch (cpo.getOutputFormat().getDestination()) {
					case DEFAULT_PRINTER_CLIENT:
					case PRINTER_CLIENT:
						final PrintService printService = PrintServiceLocator.lookupPrintServiceByPrinterName(ppto.getPrinterNameOnClient());
						PrintRequestAttributeSet printAttributeSet;
						try {
							printAttributeSet = PrintServiceLocator.createAttributeSet(printService, ppto.getTrayNumberOnClient(), ppto.getCopies(), ppto.isDuplex());
						} catch (CommonPrintException e1) {
							errorResult.add(new PrintResultTO(cpo.getOutputFormat(), e1, null, identifierLabel));
							continue;
						}
						if (format == Format.DOCX) {
							// server generates PDF for DOCX to print
							format = Format.PDF;
						}
						InputStream is = null;
						try {
							is = new ByteArrayInputStream(cpo.getOutput().getContent());
							if (format == Format.PDF) {
								final JobName jobname = new JobName(cpo.getOutput().getName(), Locale.getDefault()); 
								printAttributeSet.add(jobname);
								new PDFPrintJob().printViaPdfbox(printService, is, printAttributeSet);
							} else {
								ReportRunner.getNuclosReportPrintJob(format).print(printService, is, printAttributeSet);
							}
							
						} catch (PrintException e) {
							success = false;
							LOG.error(e.getMessage(), e);
							errorResult.add(new PrintResultTO(cpo.getOutputFormat(), e, null, identifierLabel));
						} finally {
							if (null != is) {
								try {
									is.close();
								} catch (final IOException ex) {
									LOG.error(ex.getMessage(), ex);
									throw new NuclosFatalException(ex);
								}
							}
						}
						break;
					case FILE:
						if (outputPath == null) {
							outputPath = System.getProperty("user.home");
						}
					case SCREEN:
						String exportFile = null;
						try {
							boolean isTemporaryReport = outputPath==null;
							outputPath = RigidUtils.defaultIfNull(outputPath, ReportRunner.TEMPDIR);
							exportFile = ReportRunner.getExportFile(outputPath, cpo.getOutput().getName(), format);
							ReportRunner.saveFile((org.nuclos.common.NuclosFile) cpo.getOutput(), exportFile, isTemporaryReport);
							if (cpo.getOutputFormat().getDestination() == Destination.SCREEN) {
								SystemUtils.open(exportFile);
							}
						} catch (IOException ex) {
							success = false;
							String err = SpringLocaleDelegate.getInstance().getMessage("AbstractReportExporter.4", "Die Datei {0} konnte nicht ge\u00f6ffnet werden.", exportFile);
							errorResult.add(new PrintResultTO(cpo.getOutputFormat(), ex, err, identifierLabel));
						}
					}
				}
				
				int countOutputs = 0;
				for (PrintoutTO po : selectedPrintouts) {
					countOutputs += po.getOutputFormats().size();
				}
				final String title = SpringLocaleDelegate.getInstance().getMsg("Print") + "... " + identifierLabel;
				final String message = SpringLocaleDelegate.getInstance().getMsg("PrintResult.server.success."+(countOutputs==1?"single":"multiple"), countOutputs);
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						BackgroundProcessStatusTableModel bgProcessModel = BackgroundProcessStatusController.getStatusDialog(UIUtils.getFrameForComponent(tab)).getStatusPanel().getModel();
						BackgroundProcessTableEntry backgroundProcessTableEntry = new BackgroundProcessTableEntry(title, BackgroundProcessInfo.Status.DONE, start);
						backgroundProcessTableEntry.setMessage(message);
						bgProcessModel.addEntry(backgroundProcessTableEntry);
					}
				});
				
				if (success) {
					listener.execute(PrintControllerEventType.SUBMIT_SUCCESS, null);
				} else {
					if (showOnException != null) {
						showOnException.setVisible(true);
					}
					listener.execute(PrintControllerEventType.PRINT_ERROR, errorResult);
				}
			} catch (final PrintoutException ex) {
				if (showOnException != null) {
					showOnException.setVisible(true);
				}
				if (ex.getPrintout() instanceof PrintoutTO) {
					PrintoutTO printout = (PrintoutTO) ex.getPrintout();
					String msg = printout.getName();
					listener.execute(PrintControllerEventType.PRINT_ERROR, Collections.singletonList(new PrintResultTO(null, ex, msg, identifierLabel)));
				} else if (ex.getOutputFormat() instanceof OutputFormatTO) {
					listener.execute(PrintControllerEventType.PRINT_ERROR, Collections.singletonList(new PrintResultTO((OutputFormatTO) ex.getOutputFormat(), ex, null, identifierLabel)));
				} else {
					listener.execute(PrintControllerEventType.PRINT_ERROR, Collections.singletonList(new PrintResultTO(null, ex, null, identifierLabel)));
				}
			} catch (NuclosBusinessRuleException ex) {
				if (showOnException != null) {
					showOnException.setVisible(true);
				}
				listener.execute(PrintControllerEventType.PRINT_ERROR, Collections.singletonList(new PrintResultTO(null, ex, null, identifierLabel)));
			}
		}
		
	}
	
	
	
	protected List<PrintoutTO> fetchPrintoutList(UsageCriteria usagecriteria, Object pk) throws CommonPrintException {
		try {
			final List<PrintoutTO> result = psFacadeRemote.printoutsForBO(usagecriteria, (Long) pk);

			if (null != result) {
				return result;
			} else {
				return Collections.emptyList();
			}
		} catch(final Exception ex) {
			throw new CommonPrintException(ex.getMessage());
		}
	}
	
	protected List<PrintServiceTO> fetchPrintServiceList() {
		final List<PrintServiceTO> result = psFacadeRemote.printServices();
		
		if (null != result) {
			return result;
		} else {
			return Collections.emptyList();
		}
	}
	
	private boolean fillDatasourceParameterMaps(final PrintoutTO printout) {
		return fillDatasourceParameterMaps(getParent(), printout, null);
	}
	
	private static boolean fillDatasourceParameterMaps(final JComponent parent, final PrintoutTO printout, final String identifierLabel) {
		for (OutputFormatTO outputFormat : printout.getOutputFormats()) {
			if (!fillDatasourceParameterMap(parent, printout, outputFormat, identifierLabel)) {
				return false;
			}
		}
		return true;
	}
	
	private static boolean fillDatasourceParameterMap(final JComponent parent, final PrintoutTO printout, final OutputFormatTO outputFormat) {
		return fillDatasourceParameterMap(parent, printout, outputFormat, null);
	}
	
	private static boolean fillDatasourceParameterMap(final JComponent parent, final PrintoutTO printout, final OutputFormatTO outputFormat, final String identifierLabel) {
		if (printout == null || printout.getDatasourceId() == null) {
			// Sammelausgabe
			return true;
		}
		try {
			final Mutable<Boolean> result = new Mutable<Boolean>(true);
			final Map<String, Object> params = outputFormat.getDatasourceParams();
			List<DatasourceParameterVO> lstParameters = DatasourceDelegate.getInstance().getParametersFromXML(DatasourceDelegate.getInstance().getDatasource((UID)printout.getDatasourceId()).getSource());
			final List<DatasourceParameterVO> lstNewParameters = new ArrayList<DatasourceParameterVO>();
			for (DatasourceParameterVO dspvo: lstParameters) {
				if (!dspvo.getParameter().equals("intid")) {
					lstNewParameters.add(dspvo);
				}
			}
			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					try {
						String label = identifierLabel != null ? (identifierLabel + ": ") : "";
						label += printout.getName();
						result.setValue(DatasourceCollectController.createParamMap(label, lstNewParameters, params, parent, printout.getBusinessObjectId()));
					} catch (CommonBusinessException e) {
						Errors.getInstance().showExceptionDialog(null, e);
					}
				}
			});
			if (!result.getValue()) {
				return false;
			}
			return true;
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}
	
	
	public static <Clct extends AbstractCollectable<?>> String getIdentifierLabel(Clct clct) {
		String identifierLabel = null;
		try {
			identifierLabel = SpringLocaleDelegate.getInstance().getIdentifierLabel(clct, clct.getEntityUID(), MetaProvider.getInstance());
		} catch (Exception ex) {
			LOG.error("Object label for datasource parameter form not accessible: " + ex.getMessage(), ex);
			identifierLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(clct.getEntityUID())) + ": ID" + clct.getPrimaryKey();
		}
		return identifierLabel;
	}
}
