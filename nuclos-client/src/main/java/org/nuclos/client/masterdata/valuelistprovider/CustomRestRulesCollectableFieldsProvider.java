//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;

public class CustomRestRulesCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(CustomRestRulesCollectableFieldsProvider.class);

	//

	public CustomRestRulesCollectableFieldsProvider() {
	}
	
	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		return _getCollectableFields();
	}
	
	private <PK> List<CollectableField> _getCollectableFields() {
		List<CollectableField> result = new ArrayList<>();

		try {
			for (EventSupportSourceVO essVO : EventSupportRepository.getInstance().getAllEventSupports()) {
				if (!essVO.isActive()) {
					continue;
				}
				if (essVO.getInterface().contains("org.nuclos.api.rule.CustomRestRule")) {
					result.add(new CollectableValueIdField(essVO.getPrimaryKey(), essVO.getClassname()));
				}
			}
		} catch (CommonBusinessException cbe) {
			throw new CommonFatalException(cbe);
		}

		Collections.sort(result);
		return result;
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		LOG.info("Unknown parameter " + sName + " with value " + oValue);
	}
	
}	// class AssignableWorkspaceCollectableFieldsProvider
