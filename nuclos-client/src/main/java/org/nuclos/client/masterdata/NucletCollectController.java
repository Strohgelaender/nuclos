//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.Map;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.dbtransfer.DBTransferExport;
import org.nuclos.client.dbtransfer.DBTransferImport;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.navigation.treenode.nuclet.NuclosInstanceTreeNode;

/**
 * <code>CollectController</code> for entity "Nuclet".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Maik.Stueker@novabit.de">Maik Stueker</a>
 * @version 01.00.00
 */
public class NucletCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(NucletCollectController.class);

	private JButton btnExport;
	private JButton btnImport;
	private JButton btnShowDependences;

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public NucletCollectController(MainFrameTab tabIfAny) {
		super(E.NUCLET.getUID(), tabIfAny, null);
	}

	@Override
	public void init() {
		super.init();
//		this.setupDetailsToolBar();
		this.getResultTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				updateExportButtonState();
			}
		});

		btnExport.setFocusable(false);
		btnImport.setFocusable(false);
		btnShowDependences.setFocusable(false);
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext)
		throws CommonBusinessException {
		super.deleteCollectable(clct, applyMultiEditContext);
		updateExportButtonState();
		EventSupportDelegate.getInstance().invalidateCaches(E.NUCLET);
		EventSupportRepository.getInstance().updateEventSupports();
	}
		
		
	protected CollectableMasterDataWithDependants<UID> updateCurrentCollectable() throws CommonBusinessException {
		final boolean bWasDetailsChangedIgnored = this.isDetailsChangedIgnored();
		this.setDetailsChangedIgnored(true);
		
		try {
			final CollectableMasterDataWithDependants<UID> clctCurrent = this.getCollectStateModel().getEditedCollectable();
			assert clctCurrent != null;
			
			String curPackage = (String) clctCurrent.getField(E.NUCLET.packagefield.getUID()).getValue();
			
			this.readValuesFromEditPanel(clctCurrent, false);
			String updatedPackage = (String) clctCurrent.getField(E.NUCLET.packagefield.getUID()).getValue();
			
			this.prepareCollectableForSaving(clctCurrent, this.getCollectableEntityForDetails());
			final CollectableMasterDataWithDependants<UID> result = this.updateCurrentCollectable(clctCurrent);
			assert result != null;
			assert this.getSearchStrategy().isCollectableComplete(result);
			return result;
		}
		finally {
			this.setDetailsChangedIgnored(bWasDetailsChangedIgnored);
			EventSupportDelegate.getInstance().invalidateCaches(E.NUCLET);
			EventSupportRepository.getInstance().updateEventSupports();
		}
	}
	
		
	@Override
	protected CollectableMasterDataWithDependants insertCollectable(CollectableMasterDataWithDependants clctNew) throws CommonBusinessException {
		CollectableMasterDataWithDependants result = super.insertCollectable(clctNew);
		updateExportButtonState();
		EventSupportDelegate.getInstance().invalidateCaches(E.NUCLET);
		EventSupportRepository.getInstance().updateEventSupports();
		
		return result;
	}
	
//	public static class DetailsToolbarPropertyChangeListener implements PropertyChangeListener {
//		
//		private final JButton newMakeTreeRoot;
//		
//		private DetailsToolbarPropertyChangeListener(JButton newMakeTreeRoot) {
//			this.newMakeTreeRoot = newMakeTreeRoot;
//		}
//		
//		@Override
//		public void propertyChange(PropertyChangeEvent evt) {
//			newMakeTreeRoot.setEnabled((Boolean) evt.getNewValue());
//		}
//
//	}

	protected void setupDetailsToolBar() {
		super.setupDetailsToolBar();
		//final JToolBar toolbarCustomDetails = UIUtils.createNonFloatableToolBar();

		final JButton newMakeTreeRoot = new JButton(actMakeTreeRoot);
		newMakeTreeRoot.setText(getSpringLocaleDelegate().getText("NucletCollectController.5", "Nuclet Bestandteile"));

//		for (ActionListener al : btnMakeTreeRoot.getActionListeners())
//			newMakeTreeRoot.addActionListener(al);
//		btnMakeTreeRoot.addPropertyChangeListener("enabled", 
//				new DetailsToolbarPropertyChangeListener(newMakeTreeRoot));

		this.getDetailsPanel().addToolBarComponent(newMakeTreeRoot);

		btnShowDependences = new JButton(Icons.getInstance().getIconTree16());
		btnShowDependences.setText(getSpringLocaleDelegate().getText("NucletCollectController.6", "Baum der Nuclets"));
		btnShowDependences.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cmdShowNucletDependences();
			}
		});
		this.getResultPanel().addToolBarComponent(btnShowDependences);

		btnImport = new JButton(Icons.getInstance().getIconImport16());
		btnImport.setToolTipText(getSpringLocaleDelegate().getText("NucletCollectController.1", "Nuclet importieren..."));
		btnImport.setText(getSpringLocaleDelegate().getText("NucletCollectController.3", "Importieren"));
		btnImport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new DBTransferImport(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							getResultController().getSearchResultStrategy().refreshResult();
							NucletCollectController.this.getNewAction().setEnabled(NucletCollectController.this.isNewAllowed());
						}
						catch(Exception e1) {
							LOG.warn("actionPerformed: " + e1);
						}
					}
				}).showWizard(Main.getInstance().getMainFrame().getHomePane());
			}
		});
		btnImport.setEnabled(SecurityCache.getInstance().isSuperUser() || SecurityCache.getInstance().isNucletImportUser());
		//toolbarCustomDetails.add(btnImport);
		this.getResultPanel().addToolBarComponent(btnImport);

		btnExport = new JButton(Icons.getInstance().getIconExport16());
		btnExport.setToolTipText(getSpringLocaleDelegate().getText("NucletCollectController.2", "Nuclet exportieren..."));
		btnExport.setText(getSpringLocaleDelegate().getText("NucletCollectController.4", "Exportieren"));
		btnExport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				UID nucletUID = NucletCollectController.this.getSelectedCollectableId();

				new DBTransferExport(nucletUID).showWizard(
						Main.getInstance().getMainFrame().getHomePane());
				try {
					getResultController().getSearchResultStrategy().refreshResult();
				}
				catch(CommonBusinessException e1) {
					LOG.warn("actionPerformed: " + e1);
				}
			}
		});
		updateExportButtonState();
		//toolbarCustomDetails.add(btnExport);
		
		final NuclosResultPanel<UID,CollectableMasterDataWithDependants<UID>> rp = getResultPanel();
		rp.addToolBarComponent(btnExport);
		rp.getExportWidget().setVisible(false);
		rp.getImportWidget().setVisible(false);
		
//		btnMakeTreeRoot.setVisible(false);
		actMakeTreeRoot.putValue(ToolBarItemAction.VISIBLE, Boolean.FALSE);
		btnShowResultInExplorer.setVisible(false);
	}

	private void updateExportButtonState() {
		btnExport.setEnabled(SecurityCache.getInstance().isSuperUser() || SecurityCache.getInstance().isNucletExportUser());
	}

	private void cmdShowNucletDependences() {
		UIUtils.runCommand(this.getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonFinderException, CommonPermissionException {
				Main.getInstance().getMainController().getExplorerController().cmdShowInOwnTab(new NuclosInstanceTreeNode());
			}
		});
	}

}
