//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * A collectable master data (record) along with its dependants.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableMasterDataWithDependants<PK> extends CollectableMasterData<PK> implements CollectableWithDependants<PK> {

	/**
	 * §precondition govo != null
	 */
	public static <PK2> CollectableMasterDataWithDependants<PK2> newInstance(CollectableEntity clcte, MasterDataVO<PK2> mdvo) {
		MasterDataVO<PK2> copy = mdvo.copy();
		copy.setPrimaryKey(mdvo.getPrimaryKey());
		copy.setVersion(mdvo.getVersion());
		copy.setDataLanguageMap(mdvo.getDataLanguageMap());
		return new CollectableMasterDataWithDependants<PK2>(clcte, copy);
	}

	public CollectableMasterDataWithDependants(CollectableEntity clcte, MasterDataVO<PK> mdwdcvo) {
		super(clcte, mdwdcvo, mdwdcvo.getDependents());
	}

	public MasterDataVO<PK> getMasterDataWithDependantsCVO() {
		return (MasterDataVO<PK>) this.getMasterDataCVO();
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param referencingField
	 * @return a copy of the contained dependants for the subentity with the given dependentKey.
	 */
	@Override
	public Collection<CollectableEntityObject<PK>> getDependants(FieldMeta<?> referencingField) {
		IDependentKey dependentKey = DependentDataMap.createDependentKey(referencingField);
		return getDependants(dependentKey);
	}
	
	/**
	 * §postcondition result != null
	 * 
	 * @param dependentKey
	 * @return a copy of the contained dependants for the subentity with the given dependentKey.
	 */
	@Override
	public Collection<CollectableEntityObject<PK>> getDependants(IDependentKey dependentKey) {
		FieldMeta<?> refFieldMeta = MetaProvider.getInstance().getEntityField(dependentKey.getDependentRefFieldUID());
		CollectableEntity ce = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(refFieldMeta.getEntity());
		if (ce instanceof CollectableMasterDataEntity){
			EntityMeta<PK> metaVO = (EntityMeta<PK>) MetaProvider.getInstance().getEntity(ce.getUID());
			// Map<UID, FieldMeta<?>> mpFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(subEntityUid);
			ce = new CollectableEOEntity(metaVO);
		}
		final CollectableEOEntity clctmde = (CollectableEOEntity)ce;
		final Collection<EntityObjectVO<PK>> collmdvoDependants = getMasterDataWithDependantsCVO().getDependents().getDataPk(dependentKey);
		final List<CollectableEntityObject<PK>> result = CollectionUtils.transform(collmdvoDependants, 
				new CollectableEntityObject.MakeCollectable<PK>(clctmde));
		assert result != null;
		return result;
	}

	@Override
	public Map<UID, CollectableField> getFieldMap() {
		return super.getFieldMap();
	}
	
	/**
	 * inner class MakeCollectable
	 */
	public static class MakeCollectable<PK2> implements Transformer<MasterDataVO<PK2>, CollectableMasterDataWithDependants<PK2>> {
		
		private final CollectableMasterDataEntity clctmde;

		public MakeCollectable(CollectableMasterDataEntity clctmde) {
			this.clctmde = clctmde;
		}

		@Override
		public CollectableMasterDataWithDependants<PK2> transform(MasterDataVO<PK2> mdwdcvo) {
			return new CollectableMasterDataWithDependants<PK2>(clctmde, mdwdcvo);
		}
	}

	public IDataLanguageMap getDataLanguageMap() {
		return this.getMasterDataCVO().getDataLanguageMap();
	}
	
	@Override
	public IDependentDataMap getDepenentDataMap() {
		return getMasterDataWithDependantsCVO().getDependents();
	}

	@Override
	public void setDependents(final IDependentDataMap mpDependents) {
		getMasterDataWithDependantsCVO().setDependents(mpDependents);
	}
}	// class CollectableMasterDataWithDependants
