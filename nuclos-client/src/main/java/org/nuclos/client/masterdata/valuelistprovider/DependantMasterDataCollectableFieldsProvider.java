//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.Utils;
import org.nuclos.client.entityobject.EntityFacadeDelegate;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.valuelistprovider.cache.ManagedCollectableFieldsProvider;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableEntityProvider;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.ejb3.CollectableFieldsByNameParams;

/**
 * <code>CollectableFieldsProvider</code> for dependant masterdata in Nucleus.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class DependantMasterDataCollectableFieldsProvider extends ManagedCollectableFieldsProvider implements CacheableCollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(AllReportsCollectableFieldsProvider.class);
	
	public static final String RELATED_ID = "relatedId";
	
	public static final String FIELDS_TO_DISPLAY = "fieldToDisplay";
	
	//
	
	private final UID entityUid;
	private final boolean checkValidity;
	private final String stringifiedFieldDefinition;
	private String sFieldToDisplay;
	private final UID foreignEntityUid;
	private Object oRelatedId;
	private boolean bSearchMode = false;
	
	/**
	 * @deprecated Use {@link #DependantMasterDataCollectableFieldsProvider(UID, UID)} 
	 */
	public DependantMasterDataCollectableFieldsProvider(UID entityUid, UID fieldUid) {
		this.entityUid = entityUid;
		this.stringifiedFieldDefinition = fieldUid.getStringifiedDefinition();
		final CollectableEntityProvider clcteprovider = DefaultCollectableEntityProvider.getInstance();
		final CollectableEntity ce = clcteprovider.getCollectableEntity(entityUid);
		final CollectableEntityField cef = ce.getEntityField(fieldUid);
		final String field = cef.getReferencedEntityFieldName();
		this.foreignEntityUid = cef.getReferencedEntityUID();
		
		/* 
		 * There is no such thing like an 'field to display' as the 'stringified' ref could
		 * be something like '${bezeichnung} - ${name}' - and this would be _two_ referenced
		 * fields.
		 * 
		 * getIdentifierFieldName() just looks for (very old) '${name}' that is not present
		 * today.
		 * 
		 * (tp)
		 */
		// this.sFieldToDisplay = ce.getIdentifierFieldName();
		this.sFieldToDisplay = "<field to display was not set with setParameter()>";
		
		this.checkValidity = Utils.hasValidOrActiveField(entityUid);
	}

	/**
	 * valid parameters:
	 * <ul>
	 *   <li>RELATED_ID = related id</li>
	 *   <li>FIELDS_TO_DISPLAY = name of the field to display</li>
	 *   <li>SEARCHMODE = collectable in search mask?</li>
	 * </ul>
	 * @param sName parameter name
	 * @param oValue parameter value
	 * 
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (sName.equals(RELATED_ID)) {
			this.oRelatedId = oValue;
		}
		else if (sName.equals(FIELDS_TO_DISPLAY)) {
			this.sFieldToDisplay = (String) oValue;
		}
		else if (sName.equals(NuclosConstants.VLP_SEARCHMODE_PARAMETER)) {
			bSearchMode = (Boolean) oValue;
		}
		else if (sName.equals(NuclosConstants.VLP_MANDATOR_PARAMETER)) {
			setMandator((UID) oValue);
		}
		else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}
	
	private boolean shouldCheckValidity() {
		// validity check only in detail mode (and only if the entity supports these fields)
		return !this.getIgnoreValidity() && !bSearchMode && checkValidity;
	}

	@Override
	public Object getCacheKey() {
		return Arrays.asList(
			oRelatedId,
			entityUid,
			stringifiedFieldDefinition,
			foreignEntityUid,
			sFieldToDisplay,
			shouldCheckValidity());
	}
	
	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		if (this.sFieldToDisplay == null) {
			throw new IllegalArgumentException(SpringLocaleDelegate.getInstance().getMessage(
					"DependantMasterDataCollectableFieldsProvider.1",
					"Die Entit\u00e4t {0} hat kein identifizierendes Feld.", foreignEntityUid));
		}

		if (this.oRelatedId == null) {
			return Collections.emptyList();
		} else {
			
			UID datalanguage = DataLanguageContext.getDataUserLanguage() != null ?
					DataLanguageContext.getDataUserLanguage() : DataLanguageContext.getDataSystemLanguage();

			final List<CollectableField> result = EntityFacadeDelegate.getInstance().getCollectableFieldsByName(
					CollectableFieldsByNameParams.builder()
							.entityUid(entityUid)
							.stringifiedFieldDefinition(stringifiedFieldDefinition)
							.checkValidity(checkValidity)
							.mandator(getMandator())
							.dataLanguage(datalanguage)
							.build()
			);
			Collections.sort(result);
			return result;
		}
	}

}
