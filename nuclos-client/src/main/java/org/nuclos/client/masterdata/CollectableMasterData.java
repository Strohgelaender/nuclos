//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.util.Map;

import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityProvider;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.collectable.IUncompletable;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Removable;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.IdUtils;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Makes a <code>MasterDataVO</code> <code>Collectable</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableMasterData<PK> extends AbstractCollectable<PK> implements IUncompletable, Removable {

	private final MasterDataVO<PK> mdvo;
	private final CollectableEntity clcte;
	private final Map<UID, CollectableField> mpFields = CollectionUtils.newHashMap();
	private final MetaProvider metaProvider = MetaProvider.getInstance();
	
	private UID userDataLangUID = DataLanguageContext.getDataUserLanguage();
	private UID datalangUID = DataLanguageContext.getDataSystemLanguage();

	// map for dependant child subform data
	private IDependentDataMap depmd;
	private final DependantCollectableMasterDataMap depclctmd = new DependantCollectableMasterDataMap();
	
	public CollectableMasterData(CollectableEntity clcte, MasterDataVO<PK> mdvo) {
		this(clcte, mdvo, mdvo.getDependents());
	}

	public CollectableMasterData(CollectableEntity clcte, MasterDataVO<PK> mdvo, IDependentDataMap dependents) {
		this.mdvo = mdvo;
		this.clcte = clcte;

		if (dependents == null) {
			this.setDependantMasterDataMap(new DependentDataMap());
		} else {
			this.setDependantMasterDataMap(dependents);
		}
	}

	@Override
	public void markRemoved() {
		getMasterDataCVO().remove();
	}

	@Override
	public boolean isMarkedRemoved() {
		return getMasterDataCVO().isRemoved();
	}

	/**
	 * @return this object's primary key
	 * 
	 * @deprecated Use {@link #getPrimaryKey()}.
	 */
	@Override
	public PK getId() {
		return getMasterDataCVO().getId();
	}

	@Override
	public void removeId() {
		getMasterDataCVO().setPrimaryKey(null);
	}
	
	@Override
	public PK getPrimaryKey() {
		return getMasterDataCVO().getPrimaryKey();
	}
	
	public CollectableEntity getCollectableEntity() {
		return clcte;
	}

	public MasterDataVO<PK> getMasterDataCVO() {
		return mdvo;
	}

	@Override
	public Object getValue(UID fieldUid) {
		Object retVal = this.mdvo.getFieldValue(fieldUid);
		
		if (metaProvider.getEntityField(fieldUid).isLocalized() && metaProvider.getEntityField(fieldUid).getCalcFunction() == null) {
			if (this.depmd != null && this.mdvo.getDataLanguageMap() != null) {
				IDataLanguageMap dataLanguageMap = mdvo.getDataLanguageMap();
				UID fieldDatLang = DataLanguageUtils.extractFieldUID(fieldUid);
				if (mdvo.getDataLanguageMap().getDataLanguage(userDataLangUID) != null &&
						mdvo.getDataLanguageMap().getDataLanguage(userDataLangUID).getFieldValue(fieldDatLang) != null) {
					retVal = mdvo.getDataLanguageMap().getDataLanguage(userDataLangUID).getFieldValue(fieldDatLang);
				} else {
					retVal = mdvo.getDataLanguageMap().getDataLanguage(datalangUID).getFieldValue(fieldDatLang);
				}
			}
		} 
		
		return retVal;
	}
	
	@Override
	public int getVersion() {
		return this.mdvo.getVersion();
	}

	@Override
	public CollectableField getField(UID fieldUid) {
		// mpFields is used as a cache:
		CollectableField result = this.mpFields.get(fieldUid);
		if (result == null) {
			result = new CollectableMasterDataField(this, fieldUid);				
			this.mpFields.put(fieldUid, result);
		}
		assert result != null;

		return result;
	}

	public Map<UID, CollectableField> getFieldMap() {
		return this.mpFields;		
	}
	
	@Override
	public void setField(UID fieldUid, CollectableField clctfValue) {
		if (fieldUid.equals(E.ENTITYFIELD.valuedefault.getUID())) {
			try {
				String s = CollectableFieldFormat.getInstance(Class.forName(
						mdvo.getFieldValue(E.ENTITYFIELD.datatype).toString())).format(null, clctfValue.getValue());
				this.mdvo.setFieldValue(fieldUid, s);
			}
			catch(ClassNotFoundException e) {
				throw new NuclosFatalException(e);
			}
		}
		else {			
			this.mdvo.setFieldValue(fieldUid, clctfValue.getValue());
		}
		if (clcte.getEntityField(fieldUid).isIdField()) {
			if (clctfValue.getValueId() == null) {
				this.mdvo.setFieldUid(fieldUid, null);
				this.mdvo.setFieldId(fieldUid, null);
			} else {
				if (clctfValue.getValueId() instanceof UID) {
					this.mdvo.setFieldUid(fieldUid, (UID) clctfValue.getValueId());
				} else {
					this.mdvo.setFieldId(fieldUid, IdUtils.toLongId(clctfValue.getValueId()));
				}
			}
		}


		// remove the entry from the cache, if there is one already:
		this.mpFields.remove(fieldUid);

		//assert this.getField(fieldUid).equals(clctfValue); // not needed here. otherwise we have to compare the value AND the id.
	}

	public DependantCollectableMasterDataMap getDependantCollectableMasterDataMap() {
		return this.depclctmd;
	}

	public IDependentDataMap getDependantMasterDataMap() {
		return this.depmd;
	}

	public void setDependantMasterDataMap(IDependentDataMap depmd) {
		this.depmd = depmd;
		this.depclctmd.setAllValues(depmd);
		if (mdvo != null) {
			mdvo.setDependents(depmd);
		}
	}

	@Override
	public boolean isComplete() {
		return mdvo.getEntityObject().isComplete();
	}

	@Override
	public void markAsUncomplete() {
		mdvo.getEntityObject().setComplete(false);
	}	

	@Override
	public UID getEntityUID() {
		return clcte.getUID();
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getCollectableEntity());
		result.append(",vo=").append(getMasterDataCVO());
		result.append(",dep=").append(getDependantMasterDataMap());
		result.append(",cdep=").append(getDependantCollectableMasterDataMap());
		result.append(",id=").append(getId());
//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//		result.append(",label=").append(getIdentifierLabel());
		result.append(",complete=").append(isComplete());
		result.append("]");
		return result.toString();
	}

	/**
	 * inner class MakeCollectable: makes a <code>MasterDataVO</code> <code>Collectable</code>.
	 */
	public static class MakeCollectable<PK> implements Transformer<MasterDataVO<PK>, CollectableMasterData<PK>> {
		final CollectableMasterDataEntity clctmde;

		public MakeCollectable(CollectableEntityProvider clcteprovider, UID entityUid) {
			this((CollectableMasterDataEntity) clcteprovider.getCollectableEntity(entityUid));
		}

		public MakeCollectable(CollectableMasterDataEntity clctmde) {
			this.clctmde = clctmde;
		}

		@Override
		public CollectableMasterData<PK> transform(MasterDataVO<PK> mdvo) {
			return new CollectableMasterData<PK>(this.clctmde, mdvo);
		}

	}

	/**
	 * inner class ExtractMasterDataVO: the inverse operation of <code>MakeCollectable</code>.
	 */
	public static class ExtractMasterDataVO<PK> implements Transformer<CollectableMasterData<PK>, MasterDataVO<PK>> {
		@Override
		public MasterDataVO<PK> transform(CollectableMasterData<PK> clctmd) {
			IDependentDataMap depmdmp = clctmd.getDependantMasterDataMap();
			MasterDataVO<PK> mdVO = clctmd.getMasterDataCVO();
			mdVO.setDependents(depmdmp);
			return mdVO;
		}
	}

	@Override
	public boolean isDirty() {
		if (mdvo == null) return false;
		return !mdvo.getEntityObject().isFlagUnchanged();
	}
	
	@Override
	public String getLocalizedValue(UID field, UID language) {
		if (mdvo == null || mdvo.getDataLanguageMap() == null 
			|| mdvo.getDataLanguageMap().getLanguageMap() == null) return null;
		
		Map<UID, DataLanguageLocalizedEntityEntry> languageMap = 
				mdvo.getDataLanguageMap().getLanguageMap();
		
		if (languageMap.get(language) != null) {
			Object fieldValue = languageMap.get(language).getFieldValue(
					DataLanguageUtils.extractFieldUID(field));
			return fieldValue != null ? (String) fieldValue : null;
		}
		return null;
		
	}
	
	@Override
	public IDataLanguageMap getDataLanguageMap() {
		return this.mdvo.getDataLanguageMap();
	}
	
}	// class CollectableMasterData
