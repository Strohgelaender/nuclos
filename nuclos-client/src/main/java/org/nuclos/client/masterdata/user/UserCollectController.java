//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.user;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.SelectUserController;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.ldap.LDAPDataDelegate;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.component.CollectableCheckBox;
import org.nuclos.client.ui.collect.component.CollectablePasswordField;
import org.nuclos.client.ui.collect.component.InnerCheckBox;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.result.NuclosSearchResultStrategy;
import org.nuclos.client.ui.collect.result.UserResultController;
import org.nuclos.client.ui.model.ChoiceList;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVOWrapper;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * <code>CollectController</code> for entity "user".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version 01.00.00
 */
public class UserCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(UserCollectController.class);

	public final static String FIELD_PREFERENCES = "preferences";
	public final static String FIELD_PASSWORD = "password";

	private static final String PLACEHOLDER_SETPASSWORD = "PlaceholderSetPassword";
	private static final String PLACEHOLDER_SENDEMAIL = "PlaceholderSendEmail";
	private static final String PLACEHOLDER_PASSWORD = "PlaceholderPassword";
	private static final String PLACEHOLDER_PASSWORDREPEAT = "PlaceholderPasswordRepeat";

	private CollectableEntityField clctefSetPassword;
	private CollectableEntityField clctefSendPassword;
	private CollectableEntityField clctefNewPassword;
	private CollectableEntityField clctefNewPasswordRepeat;
	
	private CollectableCheckBox chkSetPassword;
	private CollectableCheckBox chkSendPassword;
	private CollectablePasswordField pwdPassword;
	private CollectablePasswordField pwdPasswordRepeat;

	protected final boolean ldapAuthentication = isLdapAuthenticationEnabled();
	protected final boolean ldapSynchronization = isLdapSynchronizationEnabled();

	protected LDAPDataDelegate ldapdelegate = null;
	private List<MasterDataVO<UID>> ldapRegisteredUsers = null;
	
	
	private final CollectableComponentModelListener ccml_superuser = new CollectableComponentModelAdapter();

	private final CollectableComponentModelListener ccml_locked = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			Boolean locked = LangUtils.defaultIfNull((Boolean)ev.getOldValue().getValue(), Boolean.FALSE);
			Integer attempts = LangUtils.defaultIfNull((Integer) 
					getDetailsComponentModel(E.USER.loginattempts.getUID()).getField().getValue(), new Integer(0));
			if (locked && attempts > 0) {
				getDetailsComponentModel(E.USER.loginattempts.getUID()).setField(new CollectableValueField(new Integer(0)));
			}
		}
	};

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
 	 * @deprecated You should normally do sth. like this:<pre><code>
 	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
     */
	public UserCollectController(MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		super(E.USER, tabIfAny,
				new UserResultController<UID,CollectableMasterDataWithDependants<UID>>(
						E.USER.getUID(), new NuclosSearchResultStrategy<UID,CollectableMasterDataWithDependants<UID>>(), state), null);
		if(this.ldapSynchronization){
			this.ldapdelegate = LDAPDataDelegate.getInstance();
		}
	}

	@Override
	protected void initialize(CollectPanel<UID,CollectableMasterDataWithDependants<UID>> pnlCollect) {
		super.initialize(pnlCollect);
		final UID entity = E.USER.getUID();
		clctefSetPassword = new DefaultCollectableEntityField(new UID("setPassword"), Boolean.class, 
				"lblSetPassword", "tltSetPassword", null, null, true, CollectableField.TYPE_VALUEFIELD, null, null, entity, null);
		clctefSendPassword = new DefaultCollectableEntityField(new UID("notifyUser"), Boolean.class, 
				"lblSendPassword", "tltSendPassword", null, null, true, CollectableField.TYPE_VALUEFIELD, null, null, entity, null);
		clctefNewPassword = new DefaultCollectableEntityField(new UID("newPassword"), String.class, 
				"lblNewPassword", "tltNewPassword", 255, null, true, CollectableField.TYPE_VALUEFIELD, null, null, entity, null);
		clctefNewPasswordRepeat = new DefaultCollectableEntityField(new UID("newPassword"), String.class, 
				"lblNewPasswordRepeat", "tltNewPasswordRepeat", 255, null, true, CollectableField.TYPE_VALUEFIELD, null, null, entity, null);

		chkSetPassword = new CollectableCheckBox(clctefSetPassword);
		chkSendPassword = new CollectableCheckBox(clctefSendPassword);
		pwdPassword = new CollectablePasswordField(clctefNewPassword);
		pwdPasswordRepeat = new CollectablePasswordField(clctefNewPasswordRepeat);

		((JCheckBox)chkSetPassword.getControlComponent()).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setDependantControlStates();
				boolean isSelected = false;
				try {
					isSelected = LangUtils.defaultIfNull((Boolean)chkSetPassword.getField().getValue(), Boolean.FALSE);
				} catch (CollectableFieldFormatException ex) {
					throw new NuclosFatalException(ex);
				}
				CollectableComponentModel model = getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.requirepasswordchange.getUID());
				if (!isNew() && model.getField() != null && !LangUtils.defaultIfNull(model.getField().getValue(), Boolean.FALSE).equals(Boolean.valueOf(isSelected))) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.requirepasswordchange.getUID())
							.setField(new CollectableValueField(isSelected));
				}
			}
		});

		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {

			@Override
			public void resultModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				int resultMode = ev.getNewCollectState().getInnerState();
				copyPrefsAction.setEnabled((resultMode == CollectState.RESULTMODE_SINGLESELECTION)
					|| (resultMode == CollectState.RESULTMODE_MULTISELECTION));
			}

			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				// replace components
				replace(PLACEHOLDER_SETPASSWORD, chkSetPassword.getJCheckBox());
				replace(PLACEHOLDER_SENDEMAIL, chkSendPassword.getJCheckBox());
				replace(PLACEHOLDER_PASSWORD, pwdPassword.getControlComponent());
				replace(PLACEHOLDER_PASSWORDREPEAT, pwdPasswordRepeat.getControlComponent());

				// setup component values
				if (!CollectState.isDetailsModeChangesPending(ev.getNewCollectState().getInnerState())) {
					// enable components (depending on collect state)
					boolean enableSetPassword = !ev.getNewCollectState().isDetailsModeMultiViewOrEdit();
					chkSetPassword.setEnabled(enableSetPassword);
					chkSetPassword.setField(new CollectableValueField(ev.getNewCollectState().isDetailsModeNew()));
					setDependantControlStates();
				}
				if (!CollectState.isDetailsModeChangesPending(ev.getNewCollectState().getInnerState())) {
					// enable components (depending on collect state)
					boolean enableSendPassword = !ev.getNewCollectState().isDetailsModeMultiViewOrEdit();
					chkSendPassword.setEnabled(enableSendPassword);
					chkSendPassword.setField(new CollectableValueField(!chkSendPassword.getControlComponent().isEnabled() && ev.getNewCollectState().isDetailsModeNew()));
				}

				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.superuser.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.superuser.getUID())
							.addCollectableComponentModelListener(null, ccml_superuser);
				}

				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.locked.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.locked.getUID())
							.addCollectableComponentModelListener(null, ccml_locked);
				}
			}

			@Override
			public void detailsModeLeft(CollectStateEvent ev) throws CommonBusinessException {
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.superuser.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.superuser.getUID()).removeCollectableComponentModelListener(ccml_superuser);
				}

				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.locked.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(E.USER.locked.getUID()).removeCollectableComponentModelListener(ccml_locked);
				}
			}
		});
		
		JComponent chkSuperUser = UIUtils.findJComponent(this.getDetailsPanel(), InnerCheckBox.class, E.USER.superuser.getUID().getStringifiedDefinition());
		if (chkSuperUser!=null) {
			((InnerCheckBox)chkSuperUser).setForceDisabled(!SecurityCache.getInstance().isSuperUser());
		}
	}

	@Override
	public void close() {
		chkSendPassword = null;
		chkSetPassword = null;
		
		super.close();
	}
	
	@Override
	protected boolean isDeleteSelectedCollectableAllowed() {
		if (getSelectedCollectable() != null) {
			final UserVO user = new UserVO(getSelectedCollectable().getMasterDataCVO());
			if (SecurityCache.getInstance().getUsername().equals(user.getName()))
				return false;
		}	
		return super.isDeleteSelectedCollectableAllowed();
	}


	@Override
	public CollectableMasterDataWithDependants<UID> newCollectable() {
		CollectableMasterDataWithDependants<UID> clctNew = super.newCollectable();
		clctNew.setField(E.USER.requirepasswordchange.getUID(), new CollectableValueField(true));
		return clctNew;
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		if (clctNew.getPrimaryKey() != null) {
			throw new IllegalArgumentException("clctNew");
		}

		final IDependentDataMap mpmdvoDependants = org.nuclos.common.Utils.clearIds(getAllSubFormData(null).toDependentDataMap());

		UserVO user = new UserVO(clctNew.getMasterDataCVO());
		getAdditionalDataFromView(user);

		final MasterDataVO<UID> mdvoInserted = UserDelegate.getInstance().create(user, mpmdvoDependants).toMasterDataVO();

		return new CollectableMasterDataWithDependants<UID>(clctNew.getCollectableEntity(), 
				new MasterDataVO<UID>(mdvoInserted, readDependants(mdvoInserted.getPrimaryKey())));
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap) oAdditionalData;

		UserVO user = new UserVO(clct.getMasterDataCVO());
		getAdditionalDataFromView(user);

		final MasterDataVO<UID> mdvoUpdated = UserDelegate.getInstance().modify(user, mpclctDependants.toDependentDataMap()).toMasterDataVO();

		return new CollectableMasterDataWithDependants<UID>(clct.getCollectableEntity(), 
				new MasterDataVO<UID>(mdvoUpdated, this.readDependants(mdvoUpdated.getPrimaryKey())));
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		UserDelegate.getInstance().remove(new UserVO(clct.getMasterDataCVO()));
	}

	private void getAdditionalDataFromView(UserVO user) throws CommonBusinessException {
		user.setSetPassword(LangUtils.defaultIfNull((Boolean)chkSetPassword.getFieldFromView().getValue(), Boolean.FALSE));
		user.setNotifyUser(LangUtils.defaultIfNull((Boolean)chkSendPassword.getFieldFromView().getValue(), Boolean.FALSE));
		if (user.getSetPassword()) {
			String passwd1 = (String)pwdPassword.getFieldFromView().getValue();
			String passwd2 = (String)pwdPasswordRepeat.getFieldFromView().getValue();

			if (!LangUtils.equal(passwd1, passwd2)) {
				throw new CommonValidationException("exception.password.match");
			}
			user.setNewPassword(passwd1);
		}
	}

	protected void setupDetailsToolBar() {
		if(this.ldapSynchronization){
			final JButton btnSynchronizeWithLDAP = new JButton();
			btnSynchronizeWithLDAP.setName("btnSynchronizeWithLDAP");
			btnSynchronizeWithLDAP.setIcon(Icons.getInstance().getIconLDAP());
			btnSynchronizeWithLDAP.setToolTipText(getSpringLocaleDelegate().getMessage(
					"UserCollectController.1", "Mit LDAP synchronisieren"));

			// action: Select Columns
			btnSynchronizeWithLDAP.setAction(new CommonAbstractAction(btnSynchronizeWithLDAP) {
				@Override
				public void actionPerformed(ActionEvent ev) {
					UserCollectController.this.cmdSynchronizeUser();
				}
			});

			this.getDetailsPanel().addToolBarComponent(btnSynchronizeWithLDAP);
		}

		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_MANAGEMENT_CONSOLE)) {
			this.getDetailsPanel().addToolBarComponent(new JButton(copyPrefsAction));
		}
	}

	/**
	 * command: select columns
	 * Lets the user select the columns to show in the result list.
	 * @throws CommonBusinessException
	 *
	 * @deprecated Move to ResultController hierarchy.
	 */
	private void cmdSynchronizeUser() {
		if(!synchronizeWithLDAP()){
			return;
		}
		final SelectUserController<MasterDataVO<UID>> ctl = new SelectUserController<MasterDataVO<UID>>(getTab(), 
				getSpringLocaleDelegate().getMessage("UserCollectController.2", "LDAP Benutzer"),
				getSpringLocaleDelegate().getMessage(
						"UserCollectController.3", "Ausgew\u00e4hlte Benutzer synchronisieren"), null, null);

		final List<MasterDataVO<UID>> lstAvailable = ldapRegisteredUsers;

		final JTable tbl = getResultTable();

		final ChoiceList<MasterDataVO<UID>> ro = new ChoiceList<MasterDataVO<UID>>();
		ro.set(lstAvailable, new Comparator<MasterDataVO<UID>>() {

			@Override
			public int compare(MasterDataVO<UID> o1, MasterDataVO<UID> o2) {
				return o1.getPrimaryKey().compareTo(o2.getPrimaryKey());
			}			
			
		});
		ctl.setModel(ro);
		final boolean bOK = ctl.run(getSpringLocaleDelegate().getMessage(
				"SelectUserController.7", "Mit LDAP Synchronisieren"));

		if (bOK) {
			UIUtils.runCommand(getTab(), new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					final int iSelectedRow = tbl.getSelectedRow();
					final List<MasterDataVOWrapper<UID>> selected = ctl.getSelectedColumns();
					for(MasterDataVOWrapper<UID> selectedWrapper : selected){
						if(selectedWrapper.isWrapped()){
							selectedWrapper.setPrimaryKey(null);
							if(selectedWrapper.getDependents() != null){
								for(IDependentKey dependentKey : selectedWrapper.getDependents().getKeySet()) {
									for(EntityObjectVO<?> dependant : selectedWrapper.getDependents().getData(dependentKey)) {
										dependant.setPrimaryKey(null);
									}
								}
							}

							if (selectedWrapper.getFieldValue(E.USER.superuser.getUID()) == null) {
								selectedWrapper.setFieldValue(E.USER.superuser.getUID(), false);
							}
							mddelegate.create(E.USER.getUID(), selectedWrapper, selectedWrapper.getDependents(), null);
						} else {
							if(selectedWrapper.isMapped()){
								selectedWrapper.replaceNativeFields();
								//TODO collective processing possible?
								mddelegate.update(E.USER.getUID(), selectedWrapper, selectedWrapper.getDependents(), null, false);
							} else {
								if(selectedWrapper.isNative()){
									mddelegate.remove(E.USER.getUID(), selectedWrapper, null);
								}
							}
						}

					}
					// refresh the result:
					getResultController().getSearchResultStrategy().refreshResult();

					// reselect the previously selected row (which gets lost be refreshing the model)
					if (iSelectedRow != -1) {
						tbl.setRowSelectionInterval(iSelectedRow, iSelectedRow);
					}

					//restoreColumnWidths(ctl.getSelectedColumns(), mpWidths);
				}
			});
		}
	}

	private boolean synchronizeWithLDAP() {
		final boolean[] synchronizedWithLDAP = new boolean[] {false};
		UIUtils.runCommand(this.getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				try {
					Collection<MasterDataVOWrapper<UID>> mdwrapperlst = ldapdelegate.getUsers(null, null);
					filterOutLDAPUsers(mdwrapperlst);
					synchronizedWithLDAP[0] = true;
				} catch (CollectableFieldFormatException e) {
					LOG.warn("synchronizeWithLDAP failed: " + e, e);
					final String sMessage = getSpringLocaleDelegate().getMessage("UserCollectController.4",
						"LDAP Synchronisierung ist gescheitert.\nEine Liste der in LDAP registrierten Benutzer kann nicht dargestellt werden.");
					Errors.getInstance().showExceptionDialog(getTab(), sMessage, e);
					synchronizedWithLDAP[0] = false;
				}
			}
		});
		return synchronizedWithLDAP[0];
	}

	private void filterOutLDAPUsers(Collection<MasterDataVOWrapper<UID>> ldapusers) throws CommonBusinessException {
		final CollectableSearchCondition currentcondition = getSearchStrategy().getCollectableSearchCondition();

		if (currentcondition != null) {
			final CollectableSearchCondition condition = SearchConditionUtils.not(currentcondition);
			final Collection<MasterDataVO<UID>> usersToRemove = mddelegate.getMasterData(getEntityUid(), condition);

			final List<UID> namestoremove = CollectionUtils.transform(usersToRemove, new Transformer<MasterDataVO<UID>, UID>() {
				@Override
				public UID transform(MasterDataVO<UID> i) {
					return i.getPrimaryKey();
				}
			});

			CollectionUtils.removeAll(ldapusers, new Predicate<MasterDataVOWrapper<UID>>() {
				@Override
				public boolean evaluate(MasterDataVOWrapper<UID> t) {
					return namestoremove.contains(t.getPrimaryKey());
				}
			});
		}

		this.ldapRegisteredUsers = new ArrayList<MasterDataVO<UID>>(ldapusers);
	}

	private static boolean isLdapAuthenticationEnabled() {
		return SecurityDelegate.getInstance().isLdapAuthenticationActive();
	}

	private static boolean isLdapSynchronizationEnabled() {
	 	return SecurityDelegate.getInstance().isLdapSynchronizationActive();
	}

	@Override
	protected void validate(CollectableMasterDataWithDependants<UID> clct) throws CommonValidationException {
		super.validate(clct);

		// check if a user with the given name exists already (ignoring case):
		final CollectableSearchCondition cond = SearchConditionUtils.newComparison(
				E.USER.name.getUID(), ComparisonOperator.EQUAL, clct.getValue(E.USER.name.getUID()));
		final TruncatableCollection<MasterDataVO<UID>> collmdvo = mddelegate.getMasterData(getEntityUid(), cond, true);
		switch (collmdvo.size()) {
			case 0:
				// OK
				break;
			case 1:
				final Object oIdExistingUser = collmdvo.iterator().next().getPrimaryKey();
				if (!oIdExistingUser.equals(clct.getPrimaryKey())) {
					throw new CommonValidationException(getSpringLocaleDelegate().getMessage("UserCollectController.5",
						"Ein Benutzer namens \"{0}\" ist bereits im System vorhanden.", clct.getValue(E.USER.name.getUID())));
				}
				break;
			default:
				throw new CommonValidationException(getSpringLocaleDelegate().getMessage("UserCollectController.6",
					"Es sind bereits mehrere Benutzer(!) unter dem Namen \"{0}\" im System vorhanden.", clct.getValue(E.USER.name.getUID())));
		}
	}

	final Action copyPrefsAction = new AbstractAction() {
		{
			putValue(Action.SHORT_DESCRIPTION, getSpringLocaleDelegate().getMessage("nuclos.preferences.transfer", null));
			putValue(Action.SMALL_ICON, Icons.getInstance().getIconPrefsCopy());
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			List<CollectableMasterDataWithDependants<UID>> selectedUsers = getSelectedCollectables();
			CopyPreferencesPanel panel = new CopyPreferencesPanel(Main.getInstance().getMainController().getUserName());
			int opt = JOptionPane.showConfirmDialog(getTab(), panel,
					getSpringLocaleDelegate().getMessage("nuclos.preferences.transfer", null),
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (opt == JOptionPane.OK_OPTION) {
				Map<String, Map<String, String>> selectedPreferences = panel.getSelectedPreferences();
				for (Collectable<UID> c : selectedUsers) {
					String userName = (String) c.getField(E.USER.name.getUID()).getValue();
					try {
						PreferencesFacadeRemote preferencesFacadeRemote
							= SpringApplicationContextHolder.getBean(PreferencesFacadeRemote.class);
						preferencesFacadeRemote.mergePreferencesForUser(userName, selectedPreferences);
					} catch(CommonFinderException ex) {
						Errors.getInstance().showExceptionDialog(getTab(),
								getSpringLocaleDelegate().getMessage("nuclos.preferences.transfer.error", userName), ex);
					}
				}
			}
		}
	};

	private void replace(String placeholdername, JComponent component) {
		JComponent placeholder = UIUtils.findJComponent(getDetailsPanel(), placeholdername);
		if (placeholder != null) {
			Container container = placeholder.getParent();
			TableLayout layoutManager = (TableLayout) container.getLayout();
			TableLayoutConstraints constraints = layoutManager.getConstraints(placeholder);

			container.remove(placeholder);
			container.add(component, constraints);
		}
	}

	private void setDependantControlStates() {
		boolean isSelected = false;
		try {
			isSelected = LangUtils.defaultIfNull((Boolean)chkSetPassword.getField().getValue(), Boolean.FALSE);
		} catch (CollectableFieldFormatException e) {
			throw new NuclosFatalException(e);
		}
		chkSendPassword.setEnabled(isSelected);
		chkSendPassword.setField(new CollectableValueField(isSelected));
		pwdPassword.setEnabled(isSelected);
		pwdPasswordRepeat.setEnabled(isSelected);
		pwdPassword.setField(new CollectableValueField(null));
		pwdPasswordRepeat.setField(new CollectableValueField(null));
	}

	private boolean isMultiEdit() {
		return getCollectState().isDetailsModeMultiViewOrEdit();
	}

	private boolean isNew() {
		return getCollectState().isDetailsModeNew();
	}
}	// class UserCollectController
