//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.util.Collection;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.maintenance.MaintenanceUtils;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * <code>CollectController</code> for entity "NucletIntegrationPoint".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Maik.Stueker@novabit.de">Maik Stueker</a>
 * @version 01.00.00
 */
public class NucletIntegrationPointCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(NucletIntegrationPointCollectController.class);

	private Boolean nucletIntegationEnabled;

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public NucletIntegrationPointCollectController(MainFrameTab tabIfAny) {
		super(E.NUCLET_INTEGRATION_POINT.getUID(), tabIfAny, null);
	}

	public boolean isNucletIntegationEnabled() {
		if (nucletIntegationEnabled == null) {
			nucletIntegationEnabled = MaintenanceUtils.isNucletIntegationPossible();
		}
		return nucletIntegationEnabled;
	}

	@Override
	protected boolean isMultiEditAllowed() {
		return false;
	}

	@Override
	protected boolean isNewAllowed() {
		return isNucletIntegationEnabled() && super.isNewAllowed();
	}

	@Override
	protected boolean isCloneAllowed() {
		try {
			return isNucletIntegationEnabled() && super.isCloneAllowed() && isNucletSource(getCompleteSelectedCollectable(true, false));
		} catch (CommonBusinessException e) {
			return false;
		}
	}

	@Override
	protected boolean isSaveAllowed() {
		return isNucletIntegationEnabled() && super.isSaveAllowed();
	}

	@Override
	protected boolean isDeleteAllowed(final CollectableMasterDataWithDependants<UID> clct) {
		return isNucletIntegationEnabled() && super.isDeleteAllowed(clct) && isNucletSource(clct);
	}

	@Override
	protected void unsafeFillDetailsPanel(final CollectableMasterDataWithDependants<UID> clct) throws NuclosBusinessException {
		super.unsafeFillDetailsPanel(clct);

		if (!isNucletIntegationEnabled()) {
			final Collection<CollectableComponent> detailCollectableComponents = getDetailsPanel().getEditView().getCollectableComponents();
			for (CollectableComponent clctcomp : detailCollectableComponents) {
				clctcomp.setEnabled(false);
			}
		} else {
			final boolean bNucletSourceMode = isNucletSource(clct);
			setComponentsEnabled(E.NUCLET_INTEGRATION_POINT.nuclet.getUID(), bNucletSourceMode &&
					/*NUCLOS-6319:*/clct.getPrimaryKey()==null);
			setComponentsEnabled(E.NUCLET_INTEGRATION_POINT.name.getUID(), bNucletSourceMode);
			setComponentsEnabled(E.NUCLET_INTEGRATION_POINT.view.getUID(), bNucletSourceMode);
			setComponentsEnabled(E.NUCLET_INTEGRATION_POINT.optional.getUID(), bNucletSourceMode);
			setComponentsEnabled(E.NUCLET_INTEGRATION_POINT.readonly.getUID(), bNucletSourceMode);
			setComponentsEnabled(E.NUCLET_INTEGRATION_POINT.stateful.getUID(), bNucletSourceMode);
			setComponentsEnabled(E.NUCLET_INTEGRATION_POINT.note.getUID(), bNucletSourceMode);
			setComponentsEnabled(E.NUCLET_INTEGRATION_POINT.readme.getUID(), bNucletSourceMode);
			setNucletSourceModeInSubFormControllers(bNucletSourceMode);
		}
	}

	private boolean isNucletSource(final CollectableMasterData<UID> clct) {
		if (clct == null) {
			return false;
		}
		final CollectableField clctfNuclet = clct.getField(E.NUCLET_INTEGRATION_POINT.nuclet.getUID());
		final UID nucletUID = (UID) clctfNuclet.getValueId();
		boolean bNucletSourceMode = true;
		if (nucletUID != null) {
			for (EntityObjectVO<UID> eoNuclet : MetaProvider.getInstance().getNuclets()) {
				if (eoNuclet.getPrimaryKey().equals(nucletUID)) {
					bNucletSourceMode = Boolean.TRUE.equals(eoNuclet.getFieldValue(E.NUCLET.source));
				}
			}
		}
		return bNucletSourceMode;
	}

	private void setComponentsEnabled(UID fieldUID, boolean enabled) {
		final List<CollectableComponent> detailCollectableComponentsForField = getDetailCollectableComponentsFor(fieldUID);
		for (CollectableComponent clctcomp : detailCollectableComponentsForField) {
			clctcomp.setEnabled(enabled);
		}
	}

	private void setNucletSourceModeInSubFormControllers(boolean bNucletSourceMode) {
		final IntFieldDetailsSubFormController intFieldSubFormController = (IntFieldDetailsSubFormController) getMapOfSubFormControllersInDetails().get(E.NUCLET_INTEGRATION_FIELD.getUID());
		intFieldSubFormController.setNucletSourceMode(bNucletSourceMode);
	}

	@Override
	public MasterDataSubFormController<UID> newDetailsSubFormController(SubForm subform,
																	   UID parentEntityUid, CollectableComponentModelProvider clctcompmodelprovider,
																	   MainFrameTab tab, JComponent compDetails, Preferences prefs, WorkspaceDescription2.EntityPreferences entityPrefs,
																		EntityCollectController<?, ?> eoController) {
		if (subform.getEntityUID().equals(E.NUCLET_INTEGRATION_FIELD.getUID())) {
			IntFieldDetailsSubFormController controller = new IntFieldDetailsSubFormController(tab, clctcompmodelprovider, parentEntityUid, subform,
					prefs, entityPrefs, valueListProviderCache, state, eoController);
			controller.setParentController((EntityCollectController<UID, CollectableEntityObject<UID>>)(Object) this);
			controller.getSubForm().setEnabled(isNucletIntegationEnabled());
			return controller;
		}
		return super.newDetailsSubFormController(subform, parentEntityUid, clctcompmodelprovider, tab, compDetails, prefs, entityPrefs, eoController);
	}

	private static class IntFieldDetailsSubFormController extends MasterDataSubFormController<UID> {

		private boolean bNucletSourceMode = true;

		public IntFieldDetailsSubFormController(final MainFrameTab tab, final CollectableComponentModelProvider clctcompmodelproviderParent, final UID parentEntityUid, final SubForm subform,
												final Preferences prefsUserParent, final WorkspaceDescription2.EntityPreferences entityPrefs, final CollectableFieldsProviderCache valueListProviderCache,
												final NuclosCollectControllerCommonState state, final EntityCollectController<?, ?> eoController) {
			super(tab, clctcompmodelproviderParent, parentEntityUid, subform, prefsUserParent, entityPrefs, valueListProviderCache, state, eoController);
		}

		public void setNucletSourceMode(final boolean bNucletSourceMode) {
			this.bNucletSourceMode = bNucletSourceMode;
			getSubForm().setCreate(bNucletSourceMode);
			getSubForm().setDelete(bNucletSourceMode);
		}

		@Override
		public boolean isColumnEnabled(final UID sColumnUid) {
			if (bNucletSourceMode) {
				return super.isColumnEnabled(sColumnUid);
			} else {
				if (sColumnUid.equals(E.NUCLET_INTEGRATION_FIELD.name.getUID()) ||
						sColumnUid.equals(E.NUCLET_INTEGRATION_FIELD.datatype.getUID()) ||
						sColumnUid.equals(E.NUCLET_INTEGRATION_FIELD.datascale.getUID()) ||
						sColumnUid.equals(E.NUCLET_INTEGRATION_FIELD.dataprecision.getUID()) ||
						sColumnUid.equals(E.NUCLET_INTEGRATION_FIELD.entityReferenceField.getUID()) ||
						sColumnUid.equals(E.NUCLET_INTEGRATION_FIELD.integrationPointReferenceField.getUID()) ||
						sColumnUid.equals(E.NUCLET_INTEGRATION_FIELD.optional.getUID()) ||
						sColumnUid.equals(E.NUCLET_INTEGRATION_FIELD.readonly.getUID())) {
					return false;
				}
				return super.isColumnEnabled(sColumnUid);
			}
		}
	}
}
