//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

/**
 * Collectable fields provider for masterdata fields belonging to a certain (masterdata or module) entity. 
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 */

public class EntityFieldsCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(EntityFieldsCollectableFieldsProvider.class);

	//Do not change this string-identifier!
	public static final String ENTITY_UID = "entityId";
	
	public static final String TARGET_MANDATOR_UID = "targetMandatorUid";

	/**
	 * @deprecated Use {@link #ENTITY_UID} instead.
	 */
	public static final String RELATED_ID = "relatedId";

	public static final String RESTRICTION = "restriction";

	/**
	 * This is the default.
	 */
	public static final String WITH_SYSTEMFIELDS = "with systemfields";

	public static final String WITHOUT_SYSTEMFIELDS = "without systemfields";

	public static final String WITH_REFERENCEFIELDS = "with referencefields";

	public static final String WITHOUT_REFERENCEFIELDS = "without referencefields";

	public static final String RESTRICT_SAME_DATA_TYPE_AS_FIELD = "restrictSameDataTypeAsField";

	public static final String RESTRICT_DATA_TYPE = "restrictDataType";

	public static final String RESTRICT_DATA_PRECISION = "restrictDataPrecision";

	public static final String RESTRICT_DATA_SCALE = "restrictDataScale";

	public static final String WITH_CREATE_NEW_ENTRY = "withCreateNewEntry";

	public static final String RESTRICT_REFERENCE_ENTITY = "restrictReferenceEntity";

	public static final String RESTRICT_REFERENCE_INTEGRATION_POINT = "restrictReferenceIntegrationPoint";

	public static final String RESTRICT_MANDATORY_FIELDS_ONLY = "restrictMandatoryFieldsOnly";

	/**
	 * Readonly Flag from IntegrationPoint.
	 * true = readonly + writable
	 * false = writable only
	 */
	public static final String READONLY_INTGRPOINT_MODE = "readonlyIntgrPointMode";

	//

	private UID entityUid;

	private boolean blnWithoutSystemFields;

	private boolean blnWithoutReferenceFields;
	
	private UID targetMandatorUid;

	private FieldMeta<?> restrictingSameDataType;

	private String restrictingDataType;

	private Integer restrictingDataScale;

	private Integer restrictingDataPrecision;

	private boolean restrictSameDataTypeValueIsNull = false;

	private boolean restrictDataTypeValueIsNull = false;

	private boolean withCreateNewEntry = false;

	private UID restrictReferenceEntityUid;

	private UID restrictReferenceIntegrationPointUid;

	private Boolean bRestrictMandatoryFieldsOnly;

	private Boolean bReadonlyIntgrPointMode;

	EntityFieldsCollectableFieldsProvider() {
	}

	public EntityFieldsCollectableFieldsProvider(UID entityUid, boolean withoutSystemFields) {
		setParameter(ENTITY_UID, entityUid);
		if (withoutSystemFields) {
			setParameter(RESTRICTION, WITHOUT_SYSTEMFIELDS);
		} else {
			setParameter(RESTRICTION, WITH_SYSTEMFIELDS);
		}
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		LOG.debug("setParameter - sName = " + sName + " - oValue = " + oValue);
		if (sName.equals(ENTITY_UID) || sName.equals(RELATED_ID)) {
			entityUid = (UID) oValue;
		} else if (sName.equals(NuclosConstants.VLP_ENTITY_UID_PARAMETER)) {
			// entityUid = MetaDataCache.getInstance().getMetaData((String) oValue).getPrimaryKey();
			throw new UnsupportedOperationException(sName + ": " + oValue);
		} else if (sName.equals(RESTRICTION)) {
			if (WITH_SYSTEMFIELDS.equals(oValue)) {
				blnWithoutSystemFields = false;
			} else if (WITHOUT_SYSTEMFIELDS.equals(oValue)) {
				blnWithoutSystemFields = true;
			} else if (WITH_REFERENCEFIELDS.equals(oValue)) {
				blnWithoutReferenceFields = false;
			} else if (WITHOUT_REFERENCEFIELDS.equals(oValue)) {
				blnWithoutReferenceFields = true;
			}
		} else if (sName.equals(TARGET_MANDATOR_UID)) {
			targetMandatorUid = (UID) oValue;
		} else if (sName.equalsIgnoreCase(RESTRICT_SAME_DATA_TYPE_AS_FIELD)) {
			if (oValue == null || (oValue instanceof String && ((String) oValue).trim().isEmpty())) {
				restrictSameDataTypeValueIsNull = true;
			} else {
				restrictSameDataTypeValueIsNull = false;
				UID otherFieldUID = (UID) oValue;
				final FieldMeta<?> otherFieldMeta = MetaProvider.getInstance().getEntityField(otherFieldUID);
				restrictingSameDataType = otherFieldMeta;
			}
		} else if (sName.equals(RESTRICT_DATA_TYPE)) {
			if (oValue == null || (oValue instanceof String && ((String) oValue).trim().isEmpty())) {
				restrictDataTypeValueIsNull = true;
			} else {
				// no referencing fields if data type is set (integration point configuration)
				blnWithoutReferenceFields = true;
				restrictDataTypeValueIsNull = false;
				String sValue = (String) oValue;
				restrictingDataType = sValue;
			}
		} else if (sName.equals(RESTRICT_DATA_SCALE)) {
			restrictingDataScale = getInteger(oValue);
		} else if (sName.equals(RESTRICT_DATA_PRECISION)) {
			restrictingDataPrecision = getInteger(oValue);
		} else if (sName.equals(WITH_CREATE_NEW_ENTRY)) {
			withCreateNewEntry = true;
		} else if (sName.equals(RESTRICT_REFERENCE_ENTITY)) {
			restrictReferenceEntityUid = (UID) oValue;
			if (oValue != null) {
				blnWithoutReferenceFields = false;
				restrictingDataType = null;
				restrictingSameDataType = null;
			}
		} else if (sName.equals(RESTRICT_REFERENCE_INTEGRATION_POINT)) {
			restrictReferenceIntegrationPointUid = (UID) oValue;
			if (oValue != null) {
				blnWithoutReferenceFields = false;
				restrictingDataType = null;
				restrictingSameDataType = null;
			}
		} else if (sName.equalsIgnoreCase(READONLY_INTGRPOINT_MODE)) {
			bReadonlyIntgrPointMode = RigidUtils.parseBoolean(oValue);
		} else if (sName.equalsIgnoreCase(RESTRICT_MANDATORY_FIELDS_ONLY)) {
			bRestrictMandatoryFieldsOnly = RigidUtils.parseBoolean(oValue);
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	private static Integer getInteger(Object oValue) {
		if (oValue instanceof Number) {
			return ((Number) oValue).intValue();
		} else if (oValue instanceof  String) {
			return Integer.valueOf((String) oValue);
		} else if (oValue == null) {
			return null;
		} else {
			throw new IllegalArgumentException("Class " + oValue.getClass().getCanonicalName() + " not supported.");
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		LOG.debug("getCollectableFields");
		List<CollectableField> result;
		if (this.entityUid == null) {
			result = Collections.emptyList();
		} else if ((this.restrictDataTypeValueIsNull || this.restrictSameDataTypeValueIsNull)
				&& (this.restrictReferenceEntityUid == null && this.restrictReferenceIntegrationPointUid == null)) {
			result = Collections.emptyList();
		} else {
			final MetaProvider mProv = MetaProvider.getInstance();
			final EntityMeta<?> entityMeta = mProv.getEntity(entityUid);
			final Collection<FieldMeta<?>> fields = new ArrayList<FieldMeta<?>>(entityMeta.getFields());

			final Iterator<FieldMeta<?>> iterator = fields.iterator();
			while (iterator.hasNext()) {
				final FieldMeta<?> fieldMeta = iterator.next();
				if (restrictingSameDataType != null) {
					if (!restrictingSameDataType.getDataType().equals(fieldMeta.getDataType())) {
						iterator.remove();
					} else if (restrictingSameDataType.getForeignEntity() != null && fieldMeta.getForeignEntity() == null) {
						iterator.remove();
					} else if (restrictingSameDataType.getForeignEntity() == null && fieldMeta.getForeignEntity() != null) {
						iterator.remove();
					} else if (restrictingSameDataType.getForeignEntity() != null && fieldMeta.getForeignEntity() != null) {
						final EntityMeta<?> restrictEntityMeta = mProv.getEntity(restrictingSameDataType.getForeignEntity());
						if (restrictEntityMeta.isGeneric()) {
							// links to generic entity, only entities implementing this generic entity are allowed
							final Set<UID> implementingEntities = mProv.getImplementingEntities(restrictEntityMeta.getUID());
							if (!implementingEntities.contains(fieldMeta.getForeignEntity())) {
								iterator.remove();
								continue;
							}
						}
					}
				}
				if (restrictingDataType != null) {
					if (!restrictingDataType.equals(fieldMeta.getDataType())) {
						iterator.remove();
						continue;
					}
					if (restrictingDataScale != null) {
						if (fieldMeta.getScale() == null || restrictingDataScale > fieldMeta.getScale()) {
							iterator.remove();
							continue;
						}
					}
					if (restrictingDataPrecision != null) {
						if (fieldMeta.getPrecision() == null || restrictingDataPrecision > fieldMeta.getPrecision()) {
							iterator.remove();
							continue;
						}
					}
				}
				if (blnWithoutReferenceFields) {
					if (fieldMeta.getForeignEntity() != null && !fieldMeta.isFileDataType()) {
						iterator.remove();
						continue;
					}
				}
				if (restrictReferenceEntityUid != null) {
					if (!restrictReferenceEntityUid.equals(fieldMeta.getForeignEntity())) {
						iterator.remove();
						continue;
					}
				}
				if (restrictReferenceIntegrationPointUid != null) {
					final EntityObjectVO<UID> refIntgrPoint = MasterDataCache.getInstance().get(E.NUCLET_INTEGRATION_POINT.getUID(), restrictReferenceIntegrationPointUid).getEntityObject();
					final UID targetEntityUID = refIntgrPoint.getFieldUid(E.NUCLET_INTEGRATION_POINT.targetEntity);
					if (targetEntityUID == null || !targetEntityUID.equals(fieldMeta.getForeignEntity())) {
						iterator.remove();
						continue;
					}
				}
				if (bReadonlyIntgrPointMode != null) {
					if (Boolean.FALSE.equals(bReadonlyIntgrPointMode) // readonly==false ? -> must be writable
							&& fieldMeta.isReadonly()) {
						iterator.remove();
						continue;
					}
				}
				if (Boolean.TRUE.equals(bRestrictMandatoryFieldsOnly)) {
					if(fieldMeta.isNullable()) {
						iterator.remove();
						continue;
					}
				}
			}

			result = CollectionUtils.transform(fields,
					new Transformer<FieldMeta<?>, CollectableField>() {
						@Override
						public CollectableField transform(FieldMeta<?> mdcvo) {
							return new CollectableValueIdField(mdcvo.getUID(),
									mdcvo.getFieldName());
									//SpringLocaleDelegate.getInstance().getResource(mdcvo.getLocaleResourceIdForLabel(),mdcvo.getFallbackLabel()));
						}
					});

			if (result.isEmpty()) {
				if (entityMeta.isStateModel()) {
					final Collection<AttributeCVO> collattrcvo = 
							GenericObjectMetaDataCache.getInstance().getAttributeCVOsByModule(entityUid, Boolean.FALSE);

					result = CollectionUtils.transform(collattrcvo, new Transformer<AttributeCVO, CollectableField>() {
						@Override
						public CollectableField transform(AttributeCVO attrcvo) {
							return new CollectableValueIdField(attrcvo.getId(),
									attrcvo.getName());
									//SpringLocaleDelegate.getInstance().getLabelFromAttributeCVO(attrcvo));
						}
					});
					Collections.sort(result);
				}
			}
			if (blnWithoutSystemFields) {
				Collection<CollectableField> col = new ArrayList<CollectableField>(result);
				for (CollectableField f : col) {
					if (SF.isEOField(entityUid, (UID)f.getValueId())) {
						result.remove(f);
					}
				}
			}
		}
		result = validateMandatorRestrictions(result, this.targetMandatorUid);
		Collections.sort(result);

		if (withCreateNewEntry) {
			final String label = SpringLocaleDelegate.getInstance().getMsg("Create new attribute");
			CollectableField createNewEntry = new CollectableValueIdField(new UID("-1"), "<" + label + ">");
			result = new ArrayList<>(result);
			result.add(0, createNewEntry);
		}

		return result;
	}
	
	public static List<CollectableField> validateMandatorRestrictions(List<CollectableField> lst, UID targetMandatorUid) {
		if (targetMandatorUid == null) {
			return lst;
		}
		List<CollectableField> result = new ArrayList<CollectableField>();
		for (CollectableField field : lst) {
			CollectableValueIdField idfield = (CollectableValueIdField) field;
			UID fieldUID = (UID) idfield.getValueId();
			FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(fieldUID);
			if (fMeta.getForeignEntity() != null || fMeta.getLookupEntity() != null) {
				UID foreignEntityUID = RigidUtils.defaultIfNull(fMeta.getForeignEntity(), fMeta.getLookupEntity());
				EntityMeta<?> foreignEntityMeta = MetaProvider.getInstance().getEntity(foreignEntityUID);
				if (foreignEntityMeta.isMandator()) {
					// remove mandator depending foreign fields from result
					continue;
				}
			}
			result.add(field);
		}
		return result;
	}
}
