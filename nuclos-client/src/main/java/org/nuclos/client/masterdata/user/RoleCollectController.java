//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.user;

import java.io.Serializable;
import java.util.Map;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.statemodel.RoleRepository;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * <code>CollectController</code> for entity "role".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version 01.00.00
 */
public class RoleCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(RoleCollectController.class);

	private CollectableEventListener<UID> clctEventListener = null;
	
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
 	 * @deprecated You should normally do sth. like this:<pre><code>
 	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
     */
	public RoleCollectController(MainFrameTab tabIfAny) {
		super(E.ROLE, tabIfAny, null);
		
		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void searchModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				removeCollectableEventListener(clctEventListener);
				clctEventListener = null;
			}
			@Override
			public void resultModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				removeCollectableEventListener(clctEventListener);
				clctEventListener = null;
			}
		});
	}
	
	@Override
	protected void cloneSelectedCollectable() throws CommonBusinessException {
		final UID clonedUid = getSelectedCollectableId();
		
		super.cloneSelectedCollectable();
		
		clctEventListener = new CollectableEventListener<UID>() {
			@Override
			public void handleCollectableEvent(Collectable<UID> collectable, org.nuclos.client.ui.collect.CollectController.MessageType messageType) {
				switch(messageType) {
					case NEW_DONE :
						try {
							StateDelegate.getInstance().copyRolePermissions(clonedUid, collectable.getPrimaryKey());
							StateDelegate.getInstance().invalidate();
						} catch (Exception e) {
							final String msg = SpringLocaleDelegate.getInstance().getText("RoleCollectController.error.copypermissions.details",
									"Die Statusmodell-Berechtigungen für die geklonte Rolle konnten nicht übertragen werden. Bitte tragen Sie dies manuel nach.");
							JOptionPane.showMessageDialog(getDetailsPanel(), msg, 
									SpringLocaleDelegate.getInstance().getText("RoleCollectController.error.copypermissions.title",	"Fehler beim Kopieren der Statusmodell-Berechtigungen"), JOptionPane.WARNING_MESSAGE);

							LOG.error("Error copying statemodel permissions.", e);
						}

						try {
							RoleRepository.getInstance().invalidate();
						} catch(Exception e) {
							throw new NuclosFatalException(e);
						}
						
						removeCollectableEventListener(this);
						clctEventListener = null;
						break;
					default:
						removeCollectableEventListener(this);
						clctEventListener = null;
						break;
				}
			}
		};
		addCollectableEventListener(clctEventListener);
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(final CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		CollectableMasterDataWithDependants<UID> insertCollectable = super.insertCollectable(clctNew);
		return insertCollectable;
	}
	
	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(final CollectableMasterDataWithDependants<UID> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		CollectableMasterDataWithDependants<UID> updateCollectable = super.updateCollectable(clct, oAdditionalData, applyMultiEditContext);
		return updateCollectable;
	}
	
	@Override
	protected void deleteCollectable(final CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		super.deleteCollectable(clct, applyMultiEditContext);
	}
	
}	// class RoleCollectController
