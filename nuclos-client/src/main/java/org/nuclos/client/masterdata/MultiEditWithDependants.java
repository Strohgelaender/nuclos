//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellRenderer;

import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.IMultiUpdateOfDependants;
import org.nuclos.client.common.ISubFormCollectableMap;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.SubFormController;
import org.nuclos.client.common.SubFormTableCellRenderer;
import org.nuclos.client.common.Utils;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.SymmetricBinaryPredicate;
import org.nuclos.common.dal.vo.EntityObjectVO;

/**
 * Provides support for multi-update of dependant masterdata. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author Oliver Brausch
 * @version 01.00.00
 */
public class MultiEditWithDependants<PK> implements IMultiUpdateOfDependants<PK> {

	private final Map<UID, ISubFormCollectableMap<PK>> subFormCollectableMaps = new HashMap<>();

	/**
	 * the color to be used as background for multi editable subforms that don't
	 * share a common value.
	 */
	public final static Color COLOR_NO_COMMON_VALUES = Utils.translateColorFromParameter(ParameterProvider.KEY_HISTORICAL_STATE_CHANGED_COLOR);// new
																																				// Color(246,229,255);

	/**
	 * initiates the multi-editing of dependants. Calculates the objects that
	 * are common in the dependants of all the given collectables and puts these
	 * in the given subform controllers, for each subentity.
	 */
	public MultiEditWithDependants(Map<CollectableEntityObject<PK>, List<CollectableEntityObject<PK>>> data, MasterDataSubFormController<PK> subformctl) {
		prepareSubFormsForMultiEdit(data, subformctl);
	}

	/**
	 * prepares the subforms for multi edit. Creates the id mapping for multi
	 * edit.
	 */
	private void prepareSubFormsForMultiEdit(Map<CollectableEntityObject<PK>, List<CollectableEntityObject<PK>>> data, MasterDataSubFormController<PK> subformctl) {
		subformctl.clear();
		final UID subEntityUid = subformctl.getSubForm().getEntityUID();
		final UID parentFieldUid = subformctl.getForeignKeyFieldUID();

		ISubFormCollectableMap<PK> subFormCollectableMap = getCommonSubCollectables(data, subEntityUid, parentFieldUid, subformctl);
		
		subFormCollectableMaps.put(subEntityUid, subFormCollectableMap);
		
		//NUCLOS-5686 Register itself when subFormCollectableMaps has been initialized, not before
		subformctl.setMultiUpdateOfDependants(this);
					
		final Collection<CollectableEntityObject<PK>> collclctCommon = subFormCollectableMap.keySet();

		if (areDependantsEmpty(data) || !collclctCommon.isEmpty()) {
			/**
			 * @todo try to use MasterDataSubFormController.fillSubForm
			 *       instead
			 */
			final boolean bWasDetailsChangedIgnored = subformctl.getParentController().isDetailsChangedIgnored();
			subformctl.getParentController().setDetailsChangedIgnored(true);
			
			subformctl.updateTableModel(new ArrayList<>(collclctCommon));
			subformctl.getSubForm().getJTable().setBackground(null);
			
			subformctl.getParentController().setDetailsChangedIgnored(bWasDetailsChangedIgnored);
		}
		else {
			subformctl.getSubForm().getJTable().setBackground(COLOR_NO_COMMON_VALUES);
		}
	}

	/**
	 * @param subEntityUid
	 * @return dependant data records of the given sub entity that are identical
	 *         for all given Collectables
	 */
	private static <PK2> ISubFormCollectableMap<PK2> getCommonSubCollectables(
			final Map<CollectableEntityObject<PK2>, List<CollectableEntityObject<PK2>>> data, 
			final UID subEntityUid, final UID parentFieldUid,
			MasterDataSubFormController<PK2> subformctl) {

		// compare all fields except the parent field:
		List<CollectableEntityObject<PK2>> collclct = new ArrayList<>();
		Collection<CollectableEntityObject<PK2>> dependants = new ArrayList<>();
		for (CollectableEntityObject<PK2> ceo : data.keySet()) {
			collclct.add(ceo);
			List<CollectableEntityObject<PK2>> lstCEO = data.get(ceo);
			dependants.addAll(lstCEO);
		}

		Collection<UID> combination = new ArrayList<>();

		// determine field combination
		EntityMeta<?> subEntityMeta = MetaProvider.getInstance().getEntity(subEntityUid);
		Map<UID, FieldMeta<?>> entityfields = MetaProvider.getInstance().getAllEntityFieldsByEntity(subEntityUid); 
		
		final UID[] aEqual = subEntityMeta.getFieldsForEquality();
		if (aEqual != null) {
			for (UID field : aEqual) {
				if (entityfields.containsKey(field)) {
					combination.add(field);
				}
			}
		}
		
		if (combination.size() == 0) {
			for (FieldMeta<?> field : entityfields.values()) {
				if (field.isUnique()) {
					combination.add(field.getUID());
				}
			}
		}
		
		if (combination.size() == 0) {
			combination.addAll(entityfields.keySet());
		}

		CollectionUtils.retainAll(combination, (UID t) -> {
			if (SF.isEOField(subEntityUid, t)) {
				return false;
			} else if (parentFieldUid.equals(t)) {
				return false;
			}
			return true;
		});
		
		List<Set<CollectableEntityObject<PK2>>> equivalenceClasses =
				IMultiUpdateOfDependants.getEquivalenceClasses(dependants, new AreFieldsEqual<>(combination));
		return new SubSubFormCollectableMap<>(equivalenceClasses, subEntityUid, parentFieldUid,
				collclct, subformctl, combination);
	}

	private static <PK2> boolean areDependantsEmpty(Map<CollectableEntityObject<PK2>, List<CollectableEntityObject<PK2>>> data) {

		List<CollectableEntityObject<PK2>> dependants = new ArrayList<>();
		for (CollectableEntityObject<PK2> ceo : data.keySet()) {
			if (ceo.getId() != null)
				dependants.addAll(data.get(ceo));
		}
		return dependants.isEmpty();
	}

	private static void setParentIds(Collection<EntityObjectVO<Object>> collmdvo, UID foreignKeyFieldUid, Object iParentId) {
		for (EntityObjectVO<?> mdvo : collmdvo) {
			//mdvo.setFieldValue(foreignKeyFieldUid + "Id", iParentId); //@TODO MultiNuclet.
			if (iParentId instanceof Integer) {
				Long id = ((Integer) iParentId).longValue();
				mdvo.setFieldId(foreignKeyFieldUid, id);
			}
			else {
				mdvo.setFieldId(foreignKeyFieldUid, (Long) iParentId);
			}
		}
	}

	private static class AreFieldsEqual<PK2> implements SymmetricBinaryPredicate<Collectable<PK2>> {
		
		private Collection<UID> collFieldUIDs;

		AreFieldsEqual(Collection<UID> collFieldUIDs) {
			this.collFieldUIDs = collFieldUIDs;
		}

		@Override
		public boolean evaluate(Collectable<PK2> clct1, Collectable<PK2> clct2) {
			for (UID fieldUid : collFieldUIDs) {
				if (!clct1.getField(fieldUid).equals(clct2.getField(fieldUid))) {
					return false;
				}
			}
			return true;
		}

	} // inner class AreFieldsEqual

	@Override
	public TableCellRenderer getTableCellRenderer(CollectableEntityField clctefTarget) {
		UID entityUid = clctefTarget.getEntityUID();
		ISubFormCollectableMap<PK> map = subFormCollectableMaps.get(entityUid);
		return new SubFormTableCellRenderer<PK>(map);
	}

	public void transfer(SubForm sf, DetailsSubFormController<PK,?> dsfCtl) {
		MasterDataSubFormController<PK> msdfCtl = null;
		if (dsfCtl instanceof MasterDataSubFormController) {
			msdfCtl = (MasterDataSubFormController<PK>) dsfCtl;
		}

		int[] selectedRows = sf.getJTable().getSelectedRows();
		ISubFormCollectableMap<PK> map = subFormCollectableMaps.get(sf.getEntityUID());
		for (int row : selectedRows) {
			if (!map.allEntitiesHaveDataInRow(row)) {
				CollectableEntityObject<PK> prototype = (CollectableEntityObject<PK>) map.getPrototype(row);
				Map<PK, CollectableEntityObject<PK>> newCeos = map.transferDataToAllEntities(prototype);
				if (msdfCtl != null) msdfCtl.insertTransferedRows(prototype, newCeos);
			}
		}
		sf.getJTable().repaint();
		sf.fireStateChanged(new ChangeEvent(sf));
	}

	public boolean isTransferPossible(SubForm sf) {
		if (sf.isEnabled()) {
			ISubFormCollectableMap<PK> map = subFormCollectableMaps.get(sf.getEntityUID());
			int[] selectedRows = sf.getJTable().getSelectedRows();
			for (int row : selectedRows) {
				if (!map.allEntitiesHaveDataInRow(row)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	public void close(SubFormController sfCtl) {
		for (ISubFormCollectableMap<PK> map : subFormCollectableMaps.values()) {
			if (map.getSubformController() == sfCtl) {
				map.close();
			}
		}
	}
	
} // class MultiUpdateOfDependants
