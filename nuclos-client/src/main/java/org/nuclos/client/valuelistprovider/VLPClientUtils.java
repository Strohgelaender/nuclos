package org.nuclos.client.valuelistprovider;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.valuelistprovider.ISelfIdCollectableFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.DatasourceBasedCollectableFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.MandatorRestrictionCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache.CachingCollectableFieldsProvider;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.exception.CommonFatalException;

public class VLPClientUtils {
	
	public static final String PARAM_INTID = "intid";
	
	public static DatasourceBasedCollectableFieldsProvider getDatasourceBasedVLP(CollectableFieldsProvider fieldProv) {
		DatasourceBasedCollectableFieldsProvider dsbVlp = null;
		if (fieldProv instanceof DatasourceBasedCollectableFieldsProvider) {	
			dsbVlp = (DatasourceBasedCollectableFieldsProvider) fieldProv;
		} else if (fieldProv instanceof CachingCollectableFieldsProvider) {
			CachingCollectableFieldsProvider cacheVlp = (CachingCollectableFieldsProvider) fieldProv;
			if (cacheVlp.getDelegate() instanceof DatasourceBasedCollectableFieldsProvider) {
				dsbVlp = (DatasourceBasedCollectableFieldsProvider) cacheVlp.getDelegate();
			}
		}
		return dsbVlp;
	}
	
	public static boolean vlpRequiresParameter(CollectableFieldsProvider fieldProv, String sParameterName) {
		DatasourceBasedCollectableFieldsProvider dsbVlp = getDatasourceBasedVLP(fieldProv);
		if (dsbVlp != null) {
			return dsbVlp.requiresParameter(sParameterName);
		}
		return false;
	}
	
	public static Object getParameterValue(CollectableFieldsProvider fieldProv, String sParameterName) {
		if (NuclosConstants.VLP_MANDATOR_PARAMETER.equals(sParameterName)) {
			MandatorRestrictionCollectableFieldsProvider mresVlp = getMandatorRestrictedVLP(fieldProv);
			return mresVlp.getMandator();
		}
		DatasourceBasedCollectableFieldsProvider dsbVlp = getDatasourceBasedVLP(fieldProv);
		if (dsbVlp != null) {
			return dsbVlp.getValueListParameter().get(sParameterName);
		}
		return null;
	}
	
	public static MandatorRestrictionCollectableFieldsProvider getMandatorRestrictedVLP(CollectableFieldsProvider fieldProv) {
		if (fieldProv instanceof MandatorRestrictionCollectableFieldsProvider) {
			return (MandatorRestrictionCollectableFieldsProvider)fieldProv;
		}
		return null;
	}
	
	public static boolean isMandatorRestrictionNecessary(UID fieldUID, CollectableFieldsProvider fieldProv) {
		MandatorRestrictionCollectableFieldsProvider mres = getMandatorRestrictedVLP(fieldProv);
		if (mres != null) {
			try {
				FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(fieldUID);
				if (fMeta.getForeignEntity() != null && MetaProvider.getInstance().getEntity(fMeta.getForeignEntity()).isMandator()) {
					return true;
				}
			} catch (CommonFatalException e) {
				// ignore.
			}
		}
		return false;
	}
	
	public static boolean isVLPBaseRestrictionMissingValues(UID fieldUID, CollectableFieldsProvider fieldProv) {
		if (fieldProv != null) {
			// search for values with mandator from login (NUCLOS-5389)
			/*if (isMandatorRestrictionNecessary(fieldUID, fieldProv)
					 || vlpRequiresParameter(fieldProv, NuclosConstants.VLP_MANDATOR_PARAMETER)) {
				if (getParameterValue(fieldProv, NuclosConstants.VLP_MANDATOR_PARAMETER) == null) {
					return true;
				}
			}*/
			if (vlpRequiresParameter(fieldProv, PARAM_INTID)) {
				if (getParameterValue(fieldProv, PARAM_INTID) == null) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean setVLPBaseRestrictions(UID fieldUID, CollectableFieldsProvider fieldProv, Object id, UID dataMandator) {
		boolean updated = false;
		if (fieldProv != null) {
			if (isMandatorRestrictionNecessary(fieldUID, fieldProv) || vlpRequiresParameter(fieldProv, NuclosConstants.VLP_MANDATOR_PARAMETER)) {
				Object oldValue = getParameterValue(fieldProv, NuclosConstants.VLP_MANDATOR_PARAMETER);
				if (!RigidUtils.equal(oldValue, dataMandator)) {
					updated = true;
					fieldProv.setParameter(NuclosConstants.VLP_MANDATOR_PARAMETER, dataMandator);
				}
			}
			
			if (vlpRequiresParameter(fieldProv, PARAM_INTID)) {
				Object oldValue = getParameterValue(fieldProv, PARAM_INTID);
				if (!RigidUtils.equal(oldValue, id)) {
					updated = true;
					fieldProv.setParameter(PARAM_INTID, id);
				}
			}
			
			if (fieldProv instanceof ISelfIdCollectableFieldsProvider) {
				updated = true;
				fieldProv.setParameter(ISelfIdCollectableFieldsProvider.SELF_ID, id);
			}
		}
		return updated;
	}
	
}
