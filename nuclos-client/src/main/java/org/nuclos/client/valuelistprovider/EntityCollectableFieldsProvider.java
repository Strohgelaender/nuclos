//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider to get all entities containing a menupath.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@novabit.de">Maik Stueker</a>
 * @version 00.01.000
 */
public class EntityCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(EntityCollectableFieldsProvider.class);
	
	public static final String PARAM_INCLUDE_SYSTEM_ENTITIES = "nuclosentities";
	
	public static final String PARAM_RESTRICTION = "restriction";
	public static final String VALUE_ENTITIES_WITH_STATEMODEL_ONLY = "entities with statemodel";
	public static final String VALUE_ENTITIES_WITHOUT_STATEMODEL_ONLY = "entities without statemodel";

	public static final String PARAM_MENUPATH_TYPE = "menupath";
	public static final String VALUE_MENUPATH_TYPE_OPTIONAL = "optional";
	
	public static final String PARAM_ONLY_GENERIC = "onlyGeneric";

	public static final String PARAM_NUCLET_ID = "nucletId";

	public static final String PARAM_EXCLUDE_NUCLET_ID = "excludeNucletId";

	/**
	 * no Proxy
	 */
	public static final String PARAM_ONLY_QUERYABLE = "onlyQueryable";

	/**
	 * Readonly Flag from IntegrationPoint.
	 * true = readonly + writable
	 * false = writable only
	 */
	public static final String PARAM_READONLY_INTGRPOINT_MODE = "readonlyIntgrPointMode";

	/**
	 * true = only entities with statemodel
	 * false = all entities
	 */
	public static final String PARAM_WITH_STATEMODEL_ONLY = "withStatemodelOnly";
	
	//

	private boolean bWithStatemodel = true;
	private boolean bWithoutStatemodel = true;
	private boolean includeEntitiesWithoutMenu = false;
	protected boolean includeSystemEntities = true;
	private boolean bOnlyGeneric = false;
	private boolean bNoGeneric = true;
	private UID nucletUID = null;
	private UID excludeNucletUID = null;
	private Boolean bOnlyQueryable;
	private Boolean bOnlyWithStatemodel;
	private Boolean bReadonlyIntgrPointMode;

	public EntityCollectableFieldsProvider() {
	}
	
	public EntityCollectableFieldsProvider(boolean withStatemodelOnly, boolean withoutStatemodelOnly, 
			boolean includeEntitiesWithoutMenu, boolean includeSystemEntities) {
		this.bWithStatemodel = withStatemodelOnly;
		this.bWithoutStatemodel = withoutStatemodelOnly;
		this.includeEntitiesWithoutMenu = includeEntitiesWithoutMenu;
		this.includeSystemEntities = includeSystemEntities;
	}

	@Override
	public void setParameter(String parameter, Object oValue) {
		if (parameter.equalsIgnoreCase(PARAM_INCLUDE_SYSTEM_ENTITIES)) {
			includeSystemEntities = RigidUtils.parseBoolean(oValue);
		} else if (parameter.equalsIgnoreCase(PARAM_ONLY_GENERIC)) {
			bOnlyGeneric = RigidUtils.parseBoolean(oValue);
			bNoGeneric = !bOnlyGeneric;
		} else if (parameter.equalsIgnoreCase(PARAM_ONLY_QUERYABLE)) {
			bOnlyQueryable = RigidUtils.parseBoolean(oValue);
		} else if (parameter.equalsIgnoreCase(PARAM_READONLY_INTGRPOINT_MODE)) {
			bReadonlyIntgrPointMode = RigidUtils.parseBoolean(oValue);
		} else if (parameter.equalsIgnoreCase(PARAM_WITH_STATEMODEL_ONLY)) {
			bOnlyWithStatemodel = RigidUtils.parseBoolean(oValue);
		} else if (parameter.equalsIgnoreCase(PARAM_RESTRICTION)) {
			if (VALUE_ENTITIES_WITH_STATEMODEL_ONLY.equalsIgnoreCase((String) oValue)) {
				bWithoutStatemodel = false;
			} else if (VALUE_ENTITIES_WITHOUT_STATEMODEL_ONLY.equalsIgnoreCase((String) oValue)) {
				bWithStatemodel = false;
			}
		} else if (parameter.equalsIgnoreCase(PARAM_MENUPATH_TYPE)) {
			if (VALUE_MENUPATH_TYPE_OPTIONAL.equals(oValue)) {
				includeEntitiesWithoutMenu = true;
			}
		} else if (parameter.equalsIgnoreCase(PARAM_NUCLET_ID)) {
			if (oValue instanceof UID) {
				nucletUID = (UID) oValue;
			} else if (oValue instanceof String) {
				nucletUID = UID.parseUID((String) oValue);
			} else if (oValue == null) {
				nucletUID = null;
			} else {
				throw new IllegalArgumentException("Class " + oValue.getClass().getCanonicalName() + " not supported");
			}
		} else if (parameter.equalsIgnoreCase(PARAM_EXCLUDE_NUCLET_ID)) {
			if (oValue instanceof UID) {
				excludeNucletUID = (UID) oValue;
			} else if (oValue instanceof String) {
				excludeNucletUID = UID.parseUID((String) oValue);
			} else if (oValue == null) {
				excludeNucletUID = null;
			} else {
				throw new IllegalArgumentException("Class " + oValue.getClass().getCanonicalName() + " not supported");
			}
		} else {
			LOG.info("Unknown parameter " + parameter + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		final Collection<EntityMeta<?>> entites = new ArrayList<EntityMeta<?>>();
		for (EntityMeta<?> eMeta : MetaProvider.getInstance().getAllEntities()) {
			if (bOnlyGeneric && !eMeta.isGeneric()) {
				continue;
			}
			if (bNoGeneric && eMeta.isGeneric()) {
				continue;
			}
			if (Boolean.TRUE.equals(bOnlyQueryable) && eMeta.isProxy()) {
				continue;
			}
			if (Boolean.TRUE.equals(bOnlyWithStatemodel) && !eMeta.isStateModel()) {
				continue;
			}
			if (Boolean.FALSE.equals(bReadonlyIntgrPointMode) // readonly==false ? -> must be writable
					&& eMeta.isReadonly()) {
				continue;
			}
			if (!includeSystemEntities && E.isNuclosEntity(eMeta.getUID())) {
				continue;
			}
			if (nucletUID != null && ObjectUtils.notEqual(eMeta.getNuclet(), nucletUID)) {
				continue;
			}
			if (excludeNucletUID != null && ObjectUtils.equals(eMeta.getNuclet(), excludeNucletUID)) {
				continue;
			}
			boolean add = false;
			if (bOnlyGeneric || includeEntitiesWithoutMenu || (eMeta.getLocaleResourceIdForMenuPath() != null)) {
				if (!eMeta.isDatasourceBased()) {
					if (bWithStatemodel && eMeta.isStateModel()) {
						entites.add(eMeta);
					} else { 
						if (bWithoutStatemodel && !eMeta.isStateModel()){
							entites.add(eMeta);
						}
					}
				}
			}
		}
		final List<CollectableField> result = CollectionUtils.transform(entites, new Transformer<EntityMeta<?>, CollectableField>() {
			@Override
			public CollectableField transform(EntityMeta<?> eMeta) {
				return makeCollectableField(eMeta);
			}
		});
		Collections.sort(result);
		return result;
	}
	
	protected CollectableField makeCollectableField(EntityMeta<?> eMeta) {
		return new CollectableValueIdField(eMeta.getUID(), eMeta.getEntityName());
	}
}
