//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.startup;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;

import org.apache.log4j.Logger;
import org.nuclos.client.LocalUserProperties;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.customcomp.CustomComponentCache;
import org.nuclos.client.genericobject.GenericObjectLayoutCache;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Local user properties that cannot be stored on the server, because they are needed before
 * the client is connected to the server, such as the user name and the look &amp; feel.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class StoreLocalUserCaches {

	private static final Logger LOG = Logger.getLogger(StoreLocalUserCaches.class);

	// Spring injection

	@Autowired
	private ApplicationProperties applicationProperties;
	
	// end of Spring injection

	private static StoreLocalUserCaches INSTANCE;

	StoreLocalUserCaches() {
		INSTANCE = this;
	}

	public void store() {
		try {
			final File cacheFile = getCachesFile(applicationProperties);
			if (cacheFile.exists()) {
				cacheFile.deleteOnExit();
				cacheFile.delete();
				LOG.info("client local user cache is REMOVED, thus deleting " + cacheFile);
			}
		} catch (Exception ex) {
			final String sMessage = "Lokale Caches konnten nicht gelöscht werden: " + ex;
			LOG.error(sMessage, ex);
		}
	} // store

	static File getCachesFile(ApplicationProperties applicationProperties) {
		File fileHomeDir = new File(System.getProperty("user.home"));
		String fileName = applicationProperties.getAppId() + ".caches";
		fileName = System.getProperty("local.caches.filename", fileName);
		return new File(fileHomeDir, fileName);
	}

} // class LocalUserCaches
