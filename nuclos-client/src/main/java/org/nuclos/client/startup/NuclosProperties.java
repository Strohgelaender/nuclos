package org.nuclos.client.startup;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;

import org.apache.log4j.Logger;
import org.nuclos.common.startup.NuclosEnviromentConstants;
import org.nuclos.common2.StringUtils;

public class NuclosProperties extends Properties {
	
	private static final Logger LOG = Logger.getLogger(NuclosProperties.class);
	
	private final BasicService basicService;
	
	private final URL codeBase;
	
	private URL server;

	private URL rest;

	private URL webclient;

	private static NuclosProperties instance = null;

	public static NuclosProperties getInstance() {
		return instance;
	}
	
	NuclosProperties() {
		BasicService bs = null;
		try {
			bs = (BasicService) ServiceManager.lookup("javax.jnlp.BasicService");
		} catch (UnavailableServiceException e) {
			// ignore
		}
		this.basicService = bs;
		if (this.basicService != null) {
			codeBase = this.basicService.getCodeBase();
			LOG.info("Java Web Start Environment: codebase is " + codeBase);
		} else {
			codeBase = null;
			LOG.info("Java Web Start NOT detected: no codebase available");
		}

		instance = this;
	}
	
	@PostConstruct
	public void init() throws MalformedURLException {
		if (isEmpty()) {
			if (basicService != null) {
				server = baseFromCodeBase(codeBase.toExternalForm());
			} else {
				server = null;
			}
		}
		
		String jms, remote;
		
		// Backward compatibility
		if (server == null) {
			jms = System.getProperty(NuclosEnviromentConstants.JMS_VARIABLE);
			remote = System.getProperty(NuclosEnviromentConstants.REMOTE_VARIABLE);
			if (!StringUtils.isNullOrEmpty(jms)) {
				LOG.warn("Properties '" + NuclosEnviromentConstants.JMS_VARIABLE + "' and '"
						+ NuclosEnviromentConstants.REMOTE_VARIABLE + "' are deprecated, use '"
						+ NuclosEnviromentConstants.SERVER_VARIABLE + "' as replacement.");
				final URL s = baseFromJms(jms);
				if (!s.equals(baseFromRemote(remote))) {
					throw new IllegalStateException("Conflicting properties: '"
							+ NuclosEnviromentConstants.JMS_VARIABLE + "' " + jms + " vs '"
							+ NuclosEnviromentConstants.REMOTE_VARIABLE + "' " + remote);
				}
				server = s;
			} else {
				server = new URL(System.getProperty(NuclosEnviromentConstants.SERVER_VARIABLE));
				jms = server.toExternalForm() + NuclosEnviromentConstants.JMS_ENDING;
				remote = server.toExternalForm() + NuclosEnviromentConstants.REMOTE_ENDING;
			}
		} else {
			jms = server.toExternalForm() + NuclosEnviromentConstants.JMS_ENDING;
			remote = server.toExternalForm() + NuclosEnviromentConstants.REMOTE_ENDING;
		}
		rest = new URL(server.toExternalForm() + NuclosEnviromentConstants.REST_ENDING);
		
		LOG.info("nuclos server base is " + server);
		put(NuclosEnviromentConstants.SERVER_VARIABLE, server);
		put(NuclosEnviromentConstants.REST_VARIABLE, rest);
		put(NuclosEnviromentConstants.JMS_VARIABLE, jms);
		put(NuclosEnviromentConstants.REMOTE_VARIABLE, remote);

		final String sWebclientProperty = System.getProperty(NuclosEnviromentConstants.WEBCLIENT_VARIABLE);
		if (sWebclientProperty != null) {
			webclient = new URL(sWebclientProperty);
		} else {
			webclient = new URL(server.getProtocol(), server.getHost(), server.getPort(), "/webclient/#");
		}

		LOG.info("nuclos webclient url is " + webclient);
		put(NuclosEnviromentConstants.WEBCLIENT_VARIABLE, webclient);
	}
	
	private static URL baseFromCodeBase(String codeBase) {
		return cutUrl(codeBase, NuclosEnviromentConstants.CODEBASE_ENDING);
	}

	private static URL baseFromJms(String codeBase) {
		return cutUrl(codeBase, NuclosEnviromentConstants.JMS_ENDING);
	}

	private static URL baseFromRemote(String codeBase) {
		return cutUrl(codeBase, NuclosEnviromentConstants.REMOTE_ENDING);
	}

	private static URL cutUrl(String url, String cut) {
		final int idx = url.lastIndexOf(cut);
		if (idx < 0) {
			throw new IllegalStateException("Can't detemine URL base from code base " + url + " and cut string " + cut);
		}
		try {
			return new URL(url.substring(0, idx));
		} catch (MalformedURLException e) {
			throw new IllegalStateException("Can't detemine URL base from code base " + url + " and cut string " + cut);
		}
	}

}
