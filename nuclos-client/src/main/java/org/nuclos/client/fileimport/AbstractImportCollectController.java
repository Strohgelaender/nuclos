//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.fileimport;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateConstants;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableTextArea;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ProgressNotification;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common.fileimport.ImportResult;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.fileimport.ejb3.ImportFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * Special masterdata collect controller for generic object file import.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public abstract class AbstractImportCollectController extends MasterDataCollectController<UID> implements
		MessageListener {

	private static final Logger LOG = Logger.getLogger(AbstractImportCollectController.class);

	private final Action actImport = new CommonAbstractAction(Icons.getInstance().getIconPlay16(),
			getSpringLocaleDelegate().getMessage(
					"GenericObjectImportCollectController.import", "Importieren")) {

		@Override
		public void actionPerformed(ActionEvent ev) {
			cmdImport();
		}
	};

	private final Action actStop = new CommonAbstractAction(Icons.getInstance().getIconStop16(),
			getSpringLocaleDelegate().getMessage(
					"GenericObjectImportCollectController.stopimport", "Import abbrechen")) {

		@Override
		public void actionPerformed(ActionEvent ev) {
			cmdCancelImport();
		}
	};

	final ResourceBundle bundle;

	JButton btnStart;
	JButton btnStop;

	JProgressBar progressBar;

	ProgressNotification lastnotification;

	TopicNotificationReceiver tnr;

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<code><pre>
	 * ResultController<~> rc = new ResultController<~>();
	 * *CollectController<~> cc = new *CollectController<~>(.., rc);
	 * </code></pre>
	 */
	AbstractImportCollectController(UID entityUid, MainFrameTab tabIfAny) {
		super(entityUid, tabIfAny, null);

		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setSize(200, 100);

		Component placeHolder = getPlaceHolder(getDetailsPanel(), "lblPlaceholder1");
		// You only can replace the place holder once. (tp)
		if (placeHolder != null) {
			Container container = placeHolder.getParent();
			TableLayout layoutManager = (TableLayout) container.getLayout();
			TableLayoutConstraints constraints = layoutManager.getConstraints(placeHolder);

			container.remove(placeHolder);
			container.add(progressBar, constraints);
		}

		this.bundle = LocaleDelegate.getInstance().getResourceBundle();
	}

	@Override
	public void init() {
		super.init();
		getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				if (!SecurityCache.getInstance().isSuperUser()) {
					for (CollectableComponent c : getDetailCollectableComponentsFor(getModeField())) {
						if (c instanceof LabeledCollectableComponentWithVLP) {
							c.setEnabled(false);
						}
					}
				}
				resetProgressBars();

				if (ev.getNewCollectState().getInnerState() == CollectState.DETAILSMODE_VIEW) {
					setupDetailsToolBar(false, false);
				}
				else {
					setupDetailsToolBar(false, true);
				}
				if (ev.getNewCollectState().getInnerState() != CollectStateConstants.DETAILSMODE_NEW) {
					setResultLastRunInDetails();
				}
			}

			@Override
			public void detailsModeLeft(CollectStateEvent ev) throws CommonBusinessException {
				getTopicNotificationReceiver().unsubscribe(AbstractImportCollectController.this);
			}
		});
		setCellRendererInResultTable();
		getResultTable().getModel().addTableModelListener((TableModelEvent e) -> setCellRendererInResultTable());

	}

	final TopicNotificationReceiver getTopicNotificationReceiver() {
		if (tnr == null) {
			tnr = SpringApplicationContextHolder.getBean(TopicNotificationReceiver.class);
		}
		return tnr;
	}

	void resetProgressBars() {
		try {
			if (lastnotification != null) {
				this.progressBar.setString(getSpringLocaleDelegate()
						.getMessageFromResource(
								lastnotification.getMessage()));
				this.progressBar.setMinimum(lastnotification.getProgressMinimum());
				this.progressBar.setMaximum(lastnotification.getProgressMaximum());
				this.progressBar.setValue(lastnotification.getValue());
			}
			else {
				this.progressBar.setString(getSpringLocaleDelegate()
						.getMessageFromResource(
								"GenericObjectImportCollectController.18"));
				this.progressBar.setMinimum(0);
				this.progressBar.setMaximum(0);
				this.progressBar.setValue(0);
			}
		}
		finally {
			lastnotification = null;
		}
	}

	static Component getPlaceHolder(Component component, String name) {
		if (name.equals(component.getName())) {
			return component;
		}

		if (component instanceof Container) {
			Container container = (Container) component;
			for (Component c : container.getComponents()) {
				Component result = getPlaceHolder(c, name);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

	void setupDetailsToolBar(boolean running, boolean editmode) {

		if (btnStart != null) {
			this.getDetailsPanel().removeToolBarComponent(btnStart);
		}
		btnStart = new JButton(this.actImport);
		btnStart.setName("btnImport");
		btnStart.setText(getSpringLocaleDelegate().getMessage("GenericObjectImportCollectController.import",
				"Importieren"));
		btnStart.setEnabled(!running && !editmode);
		this.getDetailsPanel().addToolBarComponent(btnStart);

		if (btnStop != null) {
			this.getDetailsPanel().removeToolBarComponent(btnStop);
		}
		btnStop = new JButton(this.actStop);
		btnStop.setName("btnStop");
		btnStop.setText(getSpringLocaleDelegate().getMessage("GenericObjectImportCollectController.stopimport",
				"Import abbrechen"));
		btnStop.setEnabled(running && !editmode);
		this.getDetailsPanel().addToolBarComponent(btnStop);

	}

	Icon getIconForImportResult(String resultLastRun) {
		ImportResult ir = KeyEnum.Utils.findEnum(ImportResult.class, resultLastRun);
		if (ImportResult.OK.equals(ir)) {
			return Icons.getInstance().getIconJobSuccessful();
		}
		else if (ImportResult.ERROR.equals(ir)) {
			return Icons.getInstance().getIconJobError();
		}
		else if (ImportResult.INCOMPLETE.equals(ir)) {
			return Icons.getInstance().getIconJobWarning();
		}
		else {
			return Icons.getInstance().getIconJobUnknown();
		}
	}

	@Override
	public void onMessage(Message message) {
		try {
			if (message instanceof ObjectMessage) {
				ObjectMessage objectMessage = (ObjectMessage) message;
				if (objectMessage.getObject() instanceof ProgressNotification) {
					final ProgressNotification notification = (ProgressNotification) objectMessage.getObject();
					if (notification.getState() == ProgressNotification.RUNNING) {
						LOG.info("onMessage " + this + " progressing...");
						this.btnStart.setEnabled(false);
						this.btnStop.setEnabled(true);

						this.progressBar.setString(
								getSpringLocaleDelegate().getMessageFromResource(notification.getMessage()));
						this.progressBar.setMinimum(notification.getProgressMinimum());
						this.progressBar.setMaximum(notification.getProgressMaximum());
						this.progressBar.setValue(notification.getValue());
					}
					else {
						try {
							LOG.info("onMessage " + this + " refreshCurrentCollectable...");
							this.lastnotification = notification;
							refreshCurrentCollectable(false);
						}
						catch (CommonBusinessException e) {
							LOG.error("onMessage failed: " + e, e);
						}
					}
				}
			}
		}
		catch (JMSException ex) {
			LOG.error(ex);
		}
	}

	class ResultMessageCellRenderer extends DefaultTableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object oValue, boolean bSelected,
				boolean bHasFocus, int iRow, int iColumn) {
			final JLabel jLabel = (JLabel) super.getTableCellRendererComponent(table, oValue, bSelected, bHasFocus,
					iRow, iColumn);

			final String resultLastRun = oValue == null ? "" : oValue.toString();
			final StringBuffer resultMessage = new StringBuffer();
			final String[] results = resultLastRun.split("\n");
			for (int i = 0; i < results.length; i++) {
				resultMessage.append(localize(results[i]));
				if (i < results.length - 1)
					resultMessage.append(" ");
			}

			jLabel.setText(resultMessage.toString());

			return jLabel;
		}
	}

	class TrafficLightCellRenderer extends DefaultTableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object oValue, boolean bSelected,
				boolean bHasFocus, int iRow, int iColumn) {
			final JLabel jLabel = (JLabel) super.getTableCellRendererComponent(table, oValue, bSelected, bHasFocus,
					iRow, iColumn);

			jLabel.setText("");
			jLabel.setHorizontalAlignment(SwingConstants.CENTER);

			final String sValue = (String) ((CollectableField) oValue).getValue();
			jLabel.setIcon(getIconForImportResult(sValue));
			return jLabel;
		}
	}

	String localize(String message) {
		return ImportFacadeRemote.localize(message, bundle);
	}

	protected abstract ImportFacadeRemote getImportFacadeRemote();

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct,
				 Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap) oAdditionalData;
		clct.getMasterDataCVO().getEntityObject().setDependents(mpclctDependants.toDependentDataMap());
		final Object oId = getImportFacadeRemote().modifyFileImport(
				new MasterDataVO<>(clct.getMasterDataCVO().getEntityObject()));

		final MasterDataVO<UID> mdvoUpdated = this.mddelegate.get(this.getEntityUid(), (UID) oId);
		return new CollectableMasterDataWithDependants(clct.getCollectableEntity(),
				new MasterDataVO<>(mdvoUpdated.getEntityObject()));
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext)
			throws CommonBusinessException {
		getImportFacadeRemote().removeFileImport(clct.getMasterDataCVO());
	}

	@Override
	public CollectableMasterDataWithDependants<UID> insertCollectable(CollectableMasterDataWithDependants<UID> clctNew)
			throws CommonBusinessException {
		if (clctNew.getPrimaryKey() != null) {
			throw new IllegalArgumentException("clctNew");
		}

		// We have to clear the ids for cloned objects:
		/**
		 * @todo eliminate this workaround - this is the wrong place. The right
		 *       place is the Clone action!
		 */
		final IDependentDataMap mpmdvoDependants = org.nuclos.common.Utils.clearIds(this.getAllSubFormData(null).toDependentDataMap());
		clctNew.getMasterDataCVO().getEntityObject().setDependents(mpmdvoDependants);
		final MasterDataVO<UID> mdvoInserted = getImportFacadeRemote().createFileImport(new MasterDataVO<>(clctNew
				.getMasterDataCVO().getEntityObject()));
		mdvoInserted.getEntityObject().setDependents(this.readDependants(mdvoInserted.getPrimaryKey()));

		return new CollectableMasterDataWithDependants(clctNew.getCollectableEntity(),
				new MasterDataVO<>(mdvoInserted.getEntityObject()));
	}

	protected abstract UID getResultField();

	protected abstract UID getLastStateField();

	protected abstract UID getModeField();

	public void setResultLastRunInDetails() {
		UID resultField = getResultField();
		UID laststateField = getLastStateField();

		for (CollectableComponent clct : this.getDetailCollectableComponentsFor(resultField)) {
			if (clct instanceof CollectableTextArea) {
				final String resultLastRun = this.getSelectedCollectable() != null
						? (this.getSelectedCollectable().getValue(resultField) != null
						? (String) this.getSelectedCollectable().getValue(resultField) : null)
						: null;
				final StringBuffer resultMessage = new StringBuffer();
				final String[] results = resultLastRun == null ? new String[0] : resultLastRun.split("\n");
				for (int i = 0; i < results.length; i++) {
					resultMessage.append(localize(results[i]));
					if (i < results.length - 1)
						resultMessage.append("\n");
				}
				clct.getModel().setFieldInitial(
						new CollectableValueField(resultMessage.toString()), true, !isDetailmodeAnyNew());
			}
		}
		for (CollectableComponent clct : this.getDetailCollectableComponentsFor(laststateField)) {
			if (clct instanceof CollectableTextField) {
				((CollectableTextField) clct).getJTextField().setVisible(false);
				((CollectableTextField) clct).getJLabel().setVisible(true);
				((CollectableTextField) clct).getJLabel().setText("");
				final String resultLastRun = this.getSelectedCollectable() != null
						? (this.getSelectedCollectable().getValue(laststateField) != null
						? (String) this.getSelectedCollectable().getValue(laststateField)
						: null) : null;
				((CollectableTextField) clct).getJLabel().setIcon(getIconForImportResult(resultLastRun));
				clct.setToolTipText(resultLastRun);
			}
		}
	}

	private void setCellRendererInResultTable() {
		SwingUtilities.invokeLater(() -> {
			final int idx_result = getResultTableModel().findColumnByFieldUid(getResultField());
			if (idx_result >= 0) {
				final TableColumn column = getResultTable().getColumnModel().getColumn(idx_result);
				column.setCellRenderer(new ResultMessageCellRenderer());
			}
			final int idx_laststate = getResultTableModel().findColumnByFieldUid(getLastStateField());
			if (idx_laststate >= 0) {
				final TableColumn column = getResultTable().getColumnModel().getColumn(idx_laststate);
				column.setCellRenderer(new TrafficLightCellRenderer());
			}
		});
	}

	private void cmdImport() {
		UID importfileId = getSelectedCollectableId();
		try {
			final String correlationId;
			try {
				correlationId = getImportFacadeRemote().doImport(importfileId);
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
			if (correlationId != null) {
				this.progressBar.setString("Warte auf Status");
				setupDetailsToolBar(true, false);
				getTopicNotificationReceiver().subscribe(JMSConstants.TOPICNAME_PROGRESSNOTIFICATION,
						correlationId, this);
			}
		}
		catch (CommonBusinessException e) {
			JOptionPane.showMessageDialog(this.getDetailsPanel(),
					getSpringLocaleDelegate().getMessageFromResource(e.getMessage()), "", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void cmdCancelImport() {
		UID importfileId = getSelectedCollectableId();
		try {
			getImportFacadeRemote().stopImport(importfileId);
		}
		catch (NuclosFileImportException e) {
			LOG.error("cmdCancelImport failed: " + e, e);
			JOptionPane.showMessageDialog(this.getDetailsPanel(),
					getSpringLocaleDelegate().getMessageFromResource(e.getMessage()), "", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> newCollectableWithDefaultValues(boolean forInsert) {
		final CollectableMasterDataWithDependants<UID> result = super.newCollectableWithDefaultValues(forInsert);
		result.setField(getModeField(), new CollectableValueField(ImportMode.NUCLOSIMPORT.getValue()));
		return result;
	}

} // class AbstractImportCollectController
