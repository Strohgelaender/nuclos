//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.fileimport;

import java.io.Serializable;
import java.util.Map;

import javax.swing.*;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateConstants;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableTextArea;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.fileimport.ejb3.CsvImportFacadeRemote;
import org.nuclos.server.fileimport.ejb3.ImportFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Special masterdata collect controller for generic object file import.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Configurable
public class CsvImportCollectController extends AbstractImportCollectController {

	// Spring injection

	@Autowired
	CsvImportFacadeRemote importFacadeRemote;

	// end of Spring injection

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<code><pre>
	 * ResultController<~> rc = new ResultController<~>();
	 * *CollectController<~> cc = new *CollectController<~>(.., rc);
	 * </code></pre>
	 */
	public CsvImportCollectController(MainFrameTab tabIfAny) {
		super(E.IMPORTFILE.getUID(), tabIfAny);
	}

	@Override
	protected final UID getResultField() {
		return E.IMPORTFILE.result.getUID();
	}

	@Override
	protected final UID getLastStateField() {
		return E.IMPORTFILE.laststate.getUID();
	}

	@Override
	protected final UID getModeField() {
		return E.IMPORTFILE.mode.getUID();
	}

	@Override
	protected final ImportFacadeRemote getImportFacadeRemote() {
		return importFacadeRemote;
	}

} // class GenericObjectImportCollectController
