package org.nuclos.client.customcode;

import java.awt.Component;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.text.Document;

import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.PointerException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.common.NuclosUpdateException;
import org.nuclos.server.eventsupport.valueobject.CommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.statemodel.valueobject.StateModelVO;

public class ServerCodeCollectController extends CodeCollectController {

	private final CodeDelegate codeDelegate = CodeDelegate.getInstance();
	
	public ServerCodeCollectController(UID entity, MainFrameTab tabIfAny) {
		super(entity, tabIfAny);
	}

	/**
	 *
	 * @throws CommonBusinessException
	 */
	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		try {
			// Special treatment for EventSupport
			checkEventSupportAttachment(clct);
			
			codeDelegate.remove(clct.getMasterDataCVO());
			EventSupportDelegate.getInstance().invalidateCaches(E.SERVERCODE);
			EventSupportRepository.getInstance().updateEventSupports();
		}
		catch (NuclosCompileException ex) {
			pnlEdit.setMessages(ex.getErrorMessages());
			throw new PointerException("RuleCollectController.6");
		}
	}
	
	/**
	 * @param clctCurrent
	 * @return
	 * @throws CommonBusinessException
	 */
	@Override
	protected CollectableMasterDataWithDependants<UID> updateCurrentCollectable(CollectableMasterDataWithDependants<UID> clctCurrent) throws CommonBusinessException {
		return this.updateCollectable(clctCurrent, null, null);
	}
	
	@Override
	protected CollectableMasterDataWithDependants insertCollectable(CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		try {
			final MasterDataVO<UID> mdcvoUpdated = this.codeDelegate.create(clctNew.getMasterDataCVO());
			
			clctNew = CollectableMasterDataWithDependants.newInstance(clctNew.getCollectableEntity(), mdcvoUpdated);

			// If the update contains a new (class)name we need to update all relations; 
			String prevName = clctNew.getMasterDataCVO().getFieldValue(E.SERVERCODE.name);
			
			// If the collectable's name has been changed we must update all existing relations too
			updateCollectablesRelations(clctNew, prevName);
			
			pnlEdit.clearMessages();
			
			EventSupportDelegate.getInstance().invalidateCaches(E.SERVERCODE);
			EventSupportRepository.getInstance().updateEventSupports();
			
			return clctNew;
		}
		catch (NuclosCompileException ex) {
			pnlEdit.setMessages(ex.getErrorMessages());
			throw new PointerException("RuleCollectController.4");
			//throw new CommonBusinessException(SpringLocaleDelegate.getMessage("RuleCollectController.4", "Fehler beim \u00dcbersetzen des Quellcodes.\nBitte \u00fcberpr\u00fcfen Sie die Meldungen."));
		}
		catch (CommonBusinessException ex) {
			throw new PointerException(ex.getMessage(), ex);
		}
	}
	
	private void checkEventSupportAttachment(CollectableMasterDataWithDependants clct) 
			throws CommonRemoveException, CommonFinderException, CommonPermissionException, NuclosBusinessException {
		
		String classname = (String) clct.getMasterDataCVO().getEntityObject().getFieldValue(E.SERVERCODE.name);
		UID nucletUid = clct.getMasterDataCVO().getEntityObject().getFieldUid(E.SERVERCODE.nuclet);
		if (clct.getPrimaryKey() != null) {
			String completeName = classname;
			if (!classname.contains(".")) {
				String packageName = "org.nuclet.businessentity";
				if (nucletUid != null) {
					MasterDataVO<?> nuclet = MasterDataCache.getInstance().get(E.NUCLET.getUID(), nucletUid);
					if (nuclet != null) {
						String fieldPackage = nuclet.getFieldValue(E.NUCLET.packagefield);
						if (fieldPackage != null) 
							packageName = fieldPackage;
					}
				}
				completeName = packageName + "." + classname;
			}
			EventSupportSourceVO suppClass = EventSupportRepository.getInstance().getEventSupportByClassname(completeName);
			if (suppClass != null) {
				// Generation
				if (EventSupportRepository.getInstance().getGenerationsByEventSupportClassname(classname).size() > 0) {
					throw new CommonRemoveException(SpringLocaleDelegate.getInstance().getMessage(
							"RuleCollectController.Exception.1", "Regel kann nicht geloescht werden"));
				}
				// Job
				if (EventSupportRepository.getInstance().getJobsByEventSupportClassname(classname).size() > 0) {
					throw new CommonRemoveException(SpringLocaleDelegate.getInstance().getMessage(
							"RuleCollectController.Exception.2", "Regel kann nicht geloescht werden"));
				}
				// Statemodel
				if (EventSupportRepository.getInstance().getStateModelByEventSupportClassname(classname).size() > 0) {
					throw new CommonRemoveException(SpringLocaleDelegate.getInstance().getMessage(
							"RuleCollectController.Exception.3", "Regel kann nicht geloescht werden"));
				}
				// Entities
				if (EventSupportRepository.getInstance().getEventSupportEntitiesByClassname(classname, null).size() > 0) {
					throw new CommonRemoveException(SpringLocaleDelegate.getInstance().getMessage(
							"RuleCollectController.Exception.4", "Regel kann nicht geloescht werden"));
				}
				// Communication port
				if (EventSupportRepository.getInstance().getCommunicationPortsByEventSupportClassname(classname).size() > 0) {
					throw new CommonRemoveException(SpringLocaleDelegate.getInstance().getMessage(
							"RuleCollectController.Exception.4", "Regel kann nicht geloescht werden"));
				}
			}
		}			
	}

	private void updateCollectablesRelations(
			CollectableMasterDataWithDependants clct, final String classname) throws CommonBusinessException {

		// check collectable first
		if (clct == null || clct.getMasterDataCVO() == null) {
			throw new NuclosUpdateException("Collectable not found");
		}
		MasterDataVO<?> serverCode = MasterDataDelegate.getInstance().get(E.SERVERCODE.getUID(), clct.getId());
		
		if (serverCode == null) {
			throw new NuclosUpdateException("ServerCode entry not found");
		}
		// The name of the server code might have been changed
		if (!serverCode.getFieldValue(E.SERVERCODE.name).equals(classname)) {
			final String newClassName = serverCode.getFieldValue(E.SERVERCODE.name);
			
			// Update relations
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							// Entities
							List<EventSupportEventVO> lstESEvents = EventSupportRepository.getInstance().getEventSupportEntitiesByClassname(classname, null);
							for (EventSupportEventVO eseVO : lstESEvents) {
								eseVO.setEventSupportClass(newClassName);
								EventSupportDelegate.getInstance().modifyEventSupportEvent(eseVO, eseVO.getStateUID(), eseVO.getProcessUID());					
							}
							
							// Statemodels
							List<StateModelVO> lstESStatemodels = EventSupportRepository.getInstance().getStateModelByEventSupportClassname(classname);
							for (StateModelVO eseVO : lstESStatemodels) {
								List<EventSupportTransitionVO> lstESTransitions = EventSupportRepository.getInstance().getEventSupportsByStateModelUid(eseVO.getId());
								for (EventSupportTransitionVO t : lstESTransitions) {
									if (!RigidUtils.equal(classname, t.getEventSupportClass())) {
										continue;
									}
									t.setEventSupportClass(newClassName);
									EventSupportDelegate.getInstance().modifyEventSupportTransition(t, t.getTransition());								
								}
							}
							
							// Jobs
							List<JobVO> lstJobs = EventSupportRepository.getInstance().getJobsByEventSupportClassname(classname);
							for (JobVO job : lstJobs) {
								Collection<EventSupportJobVO> lstESJobs = EventSupportRepository.getInstance().getEventSupportsByJobUid(job.getId());
								for (EventSupportJobVO eseJob : lstESJobs) {
									if (!RigidUtils.equal(classname, eseJob.getEventSupportClass())) {
										continue;
									}
									eseJob.setEventSupportClass(newClassName);
									EventSupportDelegate.getInstance().modifyEventSupportJob(eseJob);		 
								}
							}
							
							// Generation
							List<GeneratorActionVO> lstGens = EventSupportRepository.getInstance().getGenerationsByEventSupportClassname(classname);
							for (GeneratorActionVO gen : lstGens) {
								Collection<EventSupportGenerationVO> lstESGens = EventSupportRepository.getInstance().getEventSupportsByGenerationUid(gen.getId());
								for (EventSupportGenerationVO eseGen : lstESGens) {
									if (!RigidUtils.equal(classname, eseGen.getEventSupportClass())) {
										continue;
									}
									eseGen.setEventSupportClass(newClassName);
									EventSupportDelegate.getInstance().modifyEventSupportGeneration(eseGen);
								}
							}
							
							// Communication port
							List<CommunicationPortVO> lstPorts = EventSupportRepository.getInstance().getCommunicationPortsByEventSupportClassname(classname);
							for (CommunicationPortVO port : lstPorts) {
								Collection<EventSupportCommunicationPortVO> lstESPorts = EventSupportRepository.getInstance().getEventSupportsByCommunicationPortUid(port.getId());
								for (EventSupportCommunicationPortVO esePort : lstESPorts) {
									if (!RigidUtils.equal(classname, esePort.getEventSupportClass())) {
										continue;
									}
									esePort.setEventSupportClass(newClassName);
									EventSupportDelegate.getInstance().modifyEventSupportCommunicationPort(esePort);
								}
							}
						} catch (CommonFinderException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						} catch (CommonPermissionException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						} catch (NuclosBusinessException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						} catch (CommonValidationException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						} catch (CommonCreateException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						} catch (CommonRemoveException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						} catch (CommonStaleVersionException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						} catch (NuclosCompileException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						}
					}
				}).start();
		}
	}
	
	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) 
			throws CommonBusinessException {
		
		try {
			// If the update contains a new (class)name we need to update all relations; 
			String prevName = clct.getMasterDataCVO().getFieldValue(E.SERVERCODE.name);
			
			final MasterDataVO<UID> mdcvoUpdated = this.codeDelegate.modify(clct.getMasterDataCVO());
			
			// If the collectable's name has been changed we must update all existing relations too
			updateCollectablesRelations(clct, prevName);
			
			pnlEdit.clearMessages();
			
			EventSupportDelegate.getInstance().invalidateCaches(E.SERVERCODE);
			EventSupportRepository.getInstance().updateEventSupports();
			
			return CollectableMasterDataWithDependants.newInstance(clct.getCollectableEntity(), mdcvoUpdated);
		}
		catch (NuclosCompileException ex) {
			pnlEdit.setMessages(ex.getErrorMessages());
			throw new PointerException("RuleCollectController.4");
			//throw new CommonBusinessException(SpringLocaleDelegate.getMessage("RuleCollectController.4", "Fehler beim \u00dcbersetzen des Quellcodes.\nBitte \u00fcberpr\u00fcfen Sie die Meldungen."));
		}
		catch (CommonBusinessException ex) {
			throw new PointerException(ex.getMessage(), ex);
		}
	}
	
	protected void cmdJumpToTree() {
		UIUtils.runCommand(this.getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonFinderException, CommonPermissionException {
				final UID pk = getSelectedCollectableId();
				final UID entityUid = getSelectedCollectable().getCollectableEntity().getUID();
				if (pk != null) {
					TreeNode treenode = EventSupportDelegate.getInstance().createExplorerNode(pk);
					getMainController().getExplorerController().showInOwnTab(treenode);
				}				
			}
		});
	}

	@Override
	protected List<Component> getAdditionalToolBarComponents() {
		List<Component> retVal = new ArrayList<Component>();
		
		JButton btnCheckRuleSource = new JButton(this.actCheckRuleSource);
		btnCheckRuleSource.setName("btnCheckRuleSource");
		
		retVal.add(btnCheckRuleSource);
		
		return retVal;
	}
	
	
	private CodeCheckAction actCheckRuleSource = new CodeCheckAction() {
		
		@Override
		void cmdCheckRuleSource() {
			
			UIUtils.runCommand(ServerCodeCollectController.this.getTab(), new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					// get the actual (edited) code
					String sJavaCode = pnlEdit.getJavaEditorPanel().getText();

					CollectableMasterDataWithDependants<UID> clct = ServerCodeCollectController.this.getCompleteSelectedCollectable();
					if (clct == null) {
						clct = ServerCodeCollectController.this.newCollectable();
					}
					ServerCodeCollectController.this.readValuesFromEditPanel(clct, false);

					MasterDataVO<UID> mdvo = clct.getMasterDataCVO();
					mdvo.setFieldValue(E.SERVERCODE.source, sJavaCode);

					pnlEdit.clearMessages();

					try {
						codeDelegate.compile(mdvo);
						JOptionPane.showMessageDialog(ServerCodeCollectController.this.getTab(), getSpringLocaleDelegate().getMessage(
								"CodeCollectController.compiledsuccessfully", "Quellcode erfolgreich kompiliert."));
					}
					catch (NuclosCompileException ex) {
						pnlEdit.setMessages(ex.getErrorMessages());
					}
					catch (CommonBusinessException ex) {
						handlePointerException(new PointerException(ex.getMessage()));
					}
				}
			});
		}
	};


	@Override
	protected CodeCheckAction getCodeCheckAction() {
		return this.actCheckRuleSource;
	}

	@Override
	protected void readAdditionalValuesFromEditPanel(
			CollectableMasterDataWithDependants<UID> clct, boolean bSearchTab)
			throws CollectableValidationException {
		clct.setField(E.SERVERCODE.source.getUID(), new CollectableValueField(pnlEdit.getJavaEditorPanel().getText()));
		if (pnlEdit.getNucletUid() != null) {
			clct.setField(E.SERVERCODE.nuclet.getUID(), new CollectableValueIdField(pnlEdit.getNucletUid(), pnlEdit.getNucletUid()));			
		}
	}
	
	/**
	 *
	 * @param clctmd
	 * @throws NuclosBusinessException
	 */
	@Override
	protected void unsafeFillDetailsPanel(CollectableMasterDataWithDependants<UID> clctmd) throws NuclosBusinessException {
		// fill the textfields:
		super.unsafeFillDetailsPanel(clctmd);

		Document doc1 = pnlEdit.getJavaEditorPanel().getDocument();
		doc1.removeUndoableEditListener(undoableEditListener);

		pnlEdit.setEntityUid(this.getEntityUid());
		
		if (clctmd.getPrimaryKey() != null) {
			pnlEdit.getJavaEditorPanel().setText((String)clctmd.getValue(E.SERVERCODE.source.getUID()));
			pnlEdit.getJavaEditorPanel().setCaretPosition(0);
			pnlEdit.setUid((UID)clctmd.getPrimaryKey());
		} else {
			if (clctmd.getValue(E.SERVERCODE.source.getUID()) != null) {
				pnlEdit.getJavaEditorPanel().setText((String)clctmd.getValue(E.SERVERCODE.source.getUID()));
			} else {
				pnlEdit.getJavaEditorPanel().setText(null);
			}
			pnlEdit.getJavaEditorPanel().setCaretPosition(0);
		}
		if (clctmd.getValueId(E.SERVERCODE.nuclet.getUID()) != null) {
			pnlEdit.setNucletUid((UID)clctmd.getValueId(E.SERVERCODE.nuclet.getUID())); 
		}

		doc1.addUndoableEditListener(undoableEditListener);
	}
	
	@Override
	protected CollectableMasterDataWithDependants<UID> newCollectableWithDefaultValues(boolean forInsert) {
		CollectableMasterDataWithDependants<UID> result = super.newCollectable();
		result.setField(E.SERVERCODE.name.getUID(), new CollectableValueField(
				getSpringLocaleDelegate().getText("CodeCollectController.fieldvalue.name.temp", " ")));
		return result;
	}
}
