//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.command;

import java.awt.*;

import org.nuclos.client.ui.UIUtils;

/**
 * defines what happens before and after the execution of a command.
 * @see UIUtils#runCommand(Component, Runnable)
 */
public interface CommandHandler {
	/**
	 * is executed when a command is started.
	 * @param parent
	 */
	void commandStarted(Component parent);

	/**
	 * is executed when a command is finished.
	 * @param parent
	 */
	void commandFinished(Component parent);
}

