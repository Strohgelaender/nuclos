package org.nuclos.client.login;

import java.awt.*;

import javax.swing.*;

import org.nuclos.client.LocalUserProperties;

import jiconfont.DefaultIconCode;
import jiconfont.IconCode;
import jiconfont.icons.FontAwesome;
import jiconfont.swing.IconFontSwing;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SSLCheckLabel extends JLabel {

	static {
		IconFontSwing.register(FontAwesome.getIconFont());
	}

	private static final int SIZE = 16;
	enum STATE {
		NONE(null, LocalUserProperties.KEY_LAB_SSLCHECK_NONE, Color.GRAY),
		INPROGRESS(IconFontSwing.buildIcon(FontAwesome.HOURGLASS, SIZE), LocalUserProperties.KEY_LAB_SSLCHECK_INPROGRESS, Color.BLACK),
		TRUSTED(IconFontSwing.buildIcon(FontAwesome.CHECK, SIZE, new Color(0, 160, 0)), LocalUserProperties.KEY_LAB_SSLCHECK_TRUSTED, new Color(0, 160, 0)),
		UNTRUSTED(IconFontSwing.buildIcon(FontAwesome.EXCLAMATION_TRIANGLE, SIZE, new Color(230, 150, 0)), LocalUserProperties.KEY_LAB_SSLCHECK_UNTRUSTED, new Color(230, 150, 0)),
		INVALID(IconFontSwing.buildIcon(FontAwesome.EXCLAMATION_TRIANGLE, SIZE, new Color(190, 0, 0)), LocalUserProperties.KEY_LAB_SSLCHECK_INVALID, new Color(190, 0, 0)),
		INSECURE(IconFontSwing.buildIcon(FontAwesome.EXCLAMATION_TRIANGLE, SIZE, new Color(230, 150, 0)), LocalUserProperties.KEY_LAB_SSLCHECK_INSECURE, new Color(230, 150, 0));

		final Icon icon;
		final String resourceKey;
		final Color color;

		STATE(final Icon icon, final String resourceKey, final Color color) {
			this.icon = icon;
			this.resourceKey = resourceKey;
			this.color = color;
		}

		public Icon getIcon() {
			return icon;
		}

		public String getLabel() {
			return LocalUserProperties.getInstance().getLoginResource(resourceKey);
		}

		public Color getColor() {
			return color;
		}
	}

	public SSLCheckLabel() {
		setState(STATE.NONE);
		setHorizontalAlignment(CENTER);
	}

	public void setState(STATE state) {
		setIcon(state.getIcon());
		setText(state.getLabel());
		setForeground(state.getColor());
	}
}
