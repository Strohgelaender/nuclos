//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.login;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentSkipListSet;

import javax.naming.NamingException;
import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.JTextComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.LocalUserProperties;
import org.nuclos.client.StartIcons;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.common.ShutdownActions;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.i18n.language.data.DataLanguageDelegate;
import org.nuclos.client.main.ChangePasswordPanel;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.SimpleSplash;
import org.nuclos.client.main.SwingLocaleSwitcher;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.client.security.NuclosRemoteServerSession;
import org.nuclos.client.security.NuclosRemoteServerSession.MaintenanceModeException;
import org.nuclos.client.startup.NuclosProperties;
import org.nuclos.client.ui.Controller;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.CryptUtil;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.http.RequestConfigHolder;
import org.nuclos.common.security.RemoteAuthenticationManager;
import org.nuclos.common.startup.NuclosEnviromentConstants;
import org.nuclos.common.tls.SSLChecker;
import org.nuclos.common2.ContextConditionVariable;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.SecurityFacadeRemote;
import org.nuclos.server.i18n.language.data.DataLanguageVO;
import org.nuclos.server.servermeta.ejb3.ServerMetaFacadeRemote;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;

/**
 * Performs the login process.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class LoginController extends Controller<Component> {

	private static final Logger LOG = Logger.getLogger(LoginController.class);
	private static Boolean finishDone=false;
	private static final byte[] CRYPT = new byte[] {
		(byte) 0x1e, (byte) 0x63, (byte) 0xc5, (byte) 0xe6, (byte) 0x41,
		(byte) 0x82, (byte) 0x9e, (byte) 0x16, (byte) 0xff, (byte) 0xce,
		(byte) 0x59, (byte) 0xc7, (byte) 0x9a, (byte) 0x12, (byte) 0x3c,
		(byte) 0xb3
	};

	private static final String ARGUMENT_USERID = "userid";
	private static final String ARGUMENT_USER_ID = "user_id";

	/**
	 * the default username that is taken if none has ever been entered before.
	 */
	private final String	          USERNAME_DEFAULT	= "";
	private boolean	                  passwordSaveAllowed;
	private boolean					  mandatorIsPresent;
	private boolean					  mandatorLoaded;
	private boolean					  mandatorAutoLoginCanceled;
	private LoginPanel	              loginPanel;

	/**
	 * LoginListeners in ascending priority.
	 */
	private Set<LoginListener> loginListeners = new ConcurrentSkipListSet<>(
			Comparator.comparing(LoginListener::getPriority)
	);

	private double                    shakeStepSize	    = 0;
	private final String[] args;
	
	private ContextConditionVariable clientContextCondition;
	
	// former Spring injection
	
	private ServerMetaFacadeRemote serverMetaFacadeRemote;
	
	private NuclosRemoteServerSession nuclosRemoteServerSession;
	
	private SecurityFacadeRemote securityFacadeRemote;
	
	// end of former Spring injection
	private List<DataLanguageVO> allDataLanguages;

	private static LoginController instance;

	public static LoginController getInstance() {
		return instance;
	}

	public LoginController(Component parent, String[] args, ClassPathXmlApplicationContext startupContext, ContextConditionVariable clientContextCondition) {
		super(parent);

		this.args = args == null ? null : Arrays.copyOf(args, args.length);
		this.clientContextCondition = clientContextCondition;

		try {
			setServerMetaFacadeRemote(startupContext.getBean(ServerMetaFacadeRemote.class));
			setNuclosRemoteServerSession(startupContext.getBean(NuclosRemoteServerSession.class));
			setSecurityFacadeRemote(startupContext.getBean(SecurityFacadeRemote.class));
			
	        passwordSaveAllowed = Boolean.valueOf(
	        	StringUtils.defaultIfNull(
	        		StringUtils.nullIfEmpty(
	        			serverMetaFacadeRemote.getServerProperty("application.settings.client.autologin.allowed")),
	        	"false"));
	        mandatorIsPresent = serverMetaFacadeRemote.isMandatorPresent();
        }
        catch(CommonFatalException e) {
        	LOG.fatal("LoginController failed: " + e, e);
        	// Ok! (tp)
        	e.printStackTrace();
	        JOptionPane.showMessageDialog(
	        	null,
	        	"The server at " + System.getProperty("url.remoting") + " cannot be reached.\n\n"
	        	+ "Please contact your system administrator.",
	        	"Fatal Error",
	        	JOptionPane.ERROR_MESSAGE);
			Main.getInstance().exit(Main.ExitResult.ABNORMAL);
        }
		loginPanel = LoginPanel.getInstance(securityFacadeRemote);
		loginPanel.enableRememberCheckbox(passwordSaveAllowed);
		if (mandatorIsPresent) {
			loginPanel.mandatorIsPresent();
		}

		// fill language combo with language information
		List<LocaleInfo> localeInfo = LocalUserProperties.getInstance().getLoginLocaleSelection();

		if(localeInfo == null) {
			this.loginPanel.hideLanguageSelection();
		}
		else {
			UID preselectId = LocalUserProperties.getInstance().getLoginLocaleUid();
			loginPanel.getLanguageComboBox().setModel(new DefaultComboBoxModel(localeInfo.toArray(new LocaleInfo[localeInfo.size()])));
			for(int i = 0; i < localeInfo.size(); i++)
				if(localeInfo.get(i).getLocale().equals(preselectId))
					loginPanel.getLanguageComboBox().setSelectedIndex(i);
		}
		
		// Data languages
		Locale selectedDataLangUID = LocalUserProperties.getInstance().getLoginDataLocale();
		allDataLanguages = (List<DataLanguageVO>) serverMetaFacadeRemote.getDataLanguages();
		if (allDataLanguages.size() > 0) {
			loginPanel.getDataLanguageComboBox().setModel(
					new DefaultComboBoxModel(allDataLanguages.toArray(new DataLanguageVO[allDataLanguages.size()])));
			final ListCellRenderer renderer = loginPanel.getDataLanguageComboBox().getRenderer();
			loginPanel.getDataLanguageComboBox().setRenderer(new DefaultListCellRenderer() {
				public Component getListCellRendererComponent(
				        JList list,
				        Object value,
				        int index,
				        boolean isSelected,
				        boolean cellHasFocus)
				    {	
						Object useValue = value != null ? value instanceof DataLanguageVO ? ((DataLanguageVO) value).toStringShort() : value : null;
						return renderer.getListCellRendererComponent(list, useValue, index, isSelected, cellHasFocus);
				    }
			});
			if (selectedDataLangUID != null) {
				for(int i = 0; i < allDataLanguages.size(); i++)
					if(allDataLanguages.get(i).equalsLocale(selectedDataLangUID))
						loginPanel.getDataLanguageComboBox().setSelectedIndex(i);				
			} 
		}
		else {
			loginPanel.hideDataLanguageSelection();
		}

		instance = this;

		checkSSL();
	}

	public LoginController(Component parent) {
		super(parent);
		this.args = new String[]{};
	}
	
	final void setServerMetaFacadeRemote(ServerMetaFacadeRemote serverMetaFacadeRemote) {
		this.serverMetaFacadeRemote = serverMetaFacadeRemote;
	}
	
	final ServerMetaFacadeRemote getServerMetaFacadeRemote() {
		return serverMetaFacadeRemote;
	}
	
	final void setNuclosRemoteServerSession(NuclosRemoteServerSession nuclosRemoteServerSession) {
		this.nuclosRemoteServerSession = nuclosRemoteServerSession;
	}

	final NuclosRemoteServerSession getNuclosRemoteServerSession() {
		return nuclosRemoteServerSession;
	}
	
	private WeakReference<JOptionPane> optpnWeak = new WeakReference<JOptionPane>(null);
	private WeakReference<JFrame> frameWeak = new WeakReference<JFrame>(null);
	
	final String sOkay = StringUtils.defaultIfNull(UIManager.getString("OptionPane.okButtonText"), "OK");
	final JButton okay = new JButton(sOkay);
	final JButton cancel = new JButton(StringUtils.defaultIfNull(UIManager.getString("OptionPane.cancelButtonText"), "Cancel"));

	final void setSecurityFacadeRemote(SecurityFacadeRemote securityFacadeRemote) {
		this.securityFacadeRemote = securityFacadeRemote;
	}
	
	final SecurityFacadeRemote getSecurityFacadeRemote() {
		return securityFacadeRemote;
	}

	public void run(SimpleSplash splash) {
		final LocalUserProperties props = LocalUserProperties.getInstance();
		
		SwingLocaleSwitcher.setLocale(props.getLoginLocale());
		
		okay.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	JOptionPane optpn = optpnWeak.get();
		    	if (optpn != null) {
		    		optpn.setValue(JOptionPane.OK_OPTION);
		    	}
		    }
		});
		cancel.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		    	JOptionPane optpn = optpnWeak.get();
		    	if (optpn != null) {
		    		optpn.setValue(JOptionPane.CANCEL_OPTION);
		    	}
		    }
		});

		final JOptionPane optpn = new JOptionPane(loginPanel, JOptionPane.PLAIN_MESSAGE, 
				JOptionPane.OK_CANCEL_OPTION, null, new Component[] {okay, cancel}, okay);
		optpnWeak = new WeakReference<JOptionPane>(optpn);
		optpn.setInitialValue(null);
		optpn.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		optpn.setOpaque(true);
		
		String sTitle;
		if (ApplicationProperties.arePropertiesAvailaible()) {
			sTitle = ApplicationProperties.getInstance().getCurrentVersion().getShortName();
		} else {
			sTitle = "nuclos";
		}
		sTitle = MessageFormat.format(
			props.getLoginResource(LocalUserProperties.KEY_LOGIN_TITLE),
			sTitle);
		splash.removePanel();
		final JFrame frame = splash;		
		frameWeak = new WeakReference<JFrame>(frame);
		frame.setTitle(sTitle);
		frame.setName("frmLogin");
		
		Image img = StartIcons.getInstance().getDefaultFrameIcon().getImage();
		frame.setIconImage(img);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.add(optpn);

		frame.pack(); //Resizes
		frame.setLocationRelativeTo(null); //Sets in the middle

		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		boolean attemptAutoLogin = false;

		// fill in last entered username:
		String userName = props.getUserName();
		if(userName == null) {
			userName = USERNAME_DEFAULT;
		}

		String userid = System.getProperty("loginUsername");

		if (StringUtils.isNullOrEmpty(userid) && this.args != null) {
			for (String arg : this.args) {
				if (arg != null && (arg.toLowerCase().startsWith(ARGUMENT_USERID.toLowerCase()) || arg.toLowerCase().startsWith(ARGUMENT_USER_ID.toLowerCase())) && arg.indexOf('=') > -1 && arg.indexOf('=') + 1 < arg.length()) {
					userid = arg.substring(arg.indexOf('=') + 1);
				}
			}
		}

		loginPanel.getUsernameField().setText(userid != null ? userid : userName);
		loginPanel.getUsernameField().addFocusListener(new SelectAllFocusAdapter());
		loginPanel.getPasswordField().addFocusListener(new SelectAllFocusAdapter());

		// set focus to password field:
		if(!userName.equals("")) {
			String pass = StringUtils.nullIfEmpty(props.getUserPasswd());

			if (userid != null && !userid.equals(userName)) {
				pass = null;
			}

			if(pass != null && passwordSaveAllowed) {
				try {
					String dec = CryptUtil.decryptAESHex(pass, CRYPT);
					loginPanel.getPasswordField().setText(dec);
					loginPanel.getPasswordField().setSelectionStart(0);
					loginPanel.getPasswordField().setSelectionEnd(dec.length());
					loginPanel.getRememberPwCheckBox().setSelected(true);
					attemptAutoLogin = true;
				}
				catch(Exception e) {
					LOG.error("Error decoding autologin-pass", e);
				}
			}
			loginPanel.getPasswordField().requestFocusInWindow();
		}
		final WindowAdapter windowlistener = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent ev) {
				super.windowClosing(ev);
				JOptionPane optpn = optpnWeak.get();
		    	if (optpn != null) {
		    		optpn.setValue(JOptionPane.CANCEL_OPTION);
		    	}
			}
		};
		frame.addWindowListener(windowlistener);
		
		final Runnable finishLogin = new Runnable() {		
			@Override
			public void run() {
				try {
					
					if (mandatorIsPresent) {
						ClientMandatorContext.setMandator((MandatorVO) loginPanel.getMandatorComboBox().getSelectedItem());
						SecurityFacadeRemote security = SpringApplicationContextHolder.getBean(SecurityFacadeRemote.class);
						security.checkMandatorPermission();
					}
					
					if(finishDone==true)
						return;
					finishDone=true;
					
					UIUtils.invokeOnDispatchThread(new Runnable() {
						public void run() {
							okay.setText(sOkay);
							okay.setEnabled(false);
							cancel.setEnabled(false);
							loginPanel.getMandatorComboBox().setEnabled(false);
							loginPanel.getDataLanguageComboBox().setEnabled(false);
						}});
					UIUtils.invokeOnDispatchThread(new Runnable() {
						public void run() {
							frame.removeWindowListener(windowlistener);
							postProcessLogin();
							LoginController.this.fireLoginSuccessful();
							frame.dispose();
						}});
				} catch (final CommonPermissionException e) {
					UIUtils.invokeOnDispatchThread(new Runnable() {
						public void run() {
							okay.setText(sOkay);
							optpn.setValue(optpn.getInitialValue());
							JOptionPane.showMessageDialog(optpn, e.getMessage(), "Nuclos", JOptionPane.ERROR_MESSAGE);
						}});
				}
			}
		};

		PropertyChangeListener optPaneListener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent ev) {
				final Integer iValue = (Integer) ev.getNewValue();
				if (iValue != null) {
					switch (iValue) {
					case JOptionPane.OK_OPTION:
						if (mandatorIsPresent && mandatorLoaded) {
							mandatorAutoLoginCanceled = true;
							finishLogin.run();
						} else {
							JOptionPane optpn = optpnWeak.get();
							if (optpn != null) {
								if(cmdPerformLogin(frame, optpn, iValue, props)) {
									showMandator(optpn, finishLogin);
								}
								else {
									optpn.setValue(optpn.getInitialValue());
								}
							}
						}
						break;
					case JOptionPane.CANCEL_OPTION:
					case JOptionPane.CLOSED_OPTION:
						frame.removeWindowListener(windowlistener);
						frame.dispose();
						LoginController.this.fireLoginCanceled();
						break;
					default:
						throw new NuclosFatalException();
					}
				}
			}
		};
		optpn.addPropertyChangeListener("value", optPaneListener);

		frame.setVisible(true);

		if(attemptAutoLogin) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						JOptionPane optpn = optpnWeak.get();
				    	if (optpn != null) {
							if(cmdPerformLogin(frame, optpn, JOptionPane.OK_OPTION,  props)) {
								showMandator(optpn, finishLogin);
							}
				    	}
					}
					catch (Exception e) {
						LOG.error("attemptAutoLogin failed: " + e, e);
					}
				}
			});
		}
	}

	/**
	 * Performs the SSL check in background and updates the corresponding label.
	 */
	private void checkSSL() {
		try {
			final URL serverUrl = (URL) NuclosProperties.getInstance().get(NuclosEnviromentConstants.SERVER_VARIABLE);

			if ("https".equalsIgnoreCase(serverUrl.getProtocol())) {
				SwingWorker<Void, SSLCheckLabel.STATE> swingWorker = new SwingWorker<Void, SSLCheckLabel.STATE>() {
					@Override
					protected Void doInBackground() throws Exception {
						new SSLChecker(serverUrl) {
							@Override
							protected void notifyInProgress() {
								publish(SSLCheckLabel.STATE.INPROGRESS);
							}

							@Override
							protected void notifyTrusted() {
								publish(SSLCheckLabel.STATE.TRUSTED);
							}

							@Override
							protected void notifyUntrusted() {
								publish(SSLCheckLabel.STATE.UNTRUSTED);
							}

							@Override
							protected void notifyInvalid() {
								publish(SSLCheckLabel.STATE.INVALID);
							}
						}.run();
						return null;
					}

					@Override
					protected void process(final List<SSLCheckLabel.STATE> chunks) {
						final SSLCheckLabel.STATE state = chunks.get(chunks.size() - 1);
						loginPanel.setSSLCheckState(state);
					}
				};

				swingWorker.execute();
			}
			else {
				loginPanel.setSSLCheckState(SSLCheckLabel.STATE.INSECURE);
			}
		} catch (Exception e) {
			LOG.error("SSL-Check failed", e);
		}
	}

	private void showMandator(final JOptionPane optpn, final Runnable finishLogin) {
		if (mandatorIsPresent && !mandatorLoaded) {
			loginPanel.enableMandator();
			int mandatorAutoLogin = loadMandators();
			optpn.setValue(optpn.getInitialValue());
			if (mandatorAutoLogin == 2) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						finishLogin.run();
					}
				});
			} else if (mandatorAutoLogin == 1) {
				Thread t = new Thread(new Runnable() {
					int waits = 5;
					@Override
					public void run() {
						while (waits > 0 && !mandatorAutoLoginCanceled) {
							final int w = waits;
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									okay.setText(sOkay + " (" + w + ")");
								}
							});
							waits--;
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								finishLogin.run();
							}
						}
						if (!mandatorAutoLoginCanceled) {
							finishLogin.run();
						} else {
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									okay.setText(sOkay);
								}
							});
						}
					}
				});
				t.start();
			}
		} else {
			finishLogin.run();
		}
	}

	public boolean run(JFrame frame) {
		final LocalUserProperties props = LocalUserProperties.getInstance();

		loginPanel = LoginPanel.getInstance(securityFacadeRemote);
		loginPanel.hideLanguageSelection();

		final JOptionPane optpn = new JOptionPane(loginPanel, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
		optpn.setInitialValue(null);
		optpn.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));

		String sTitle = MessageFormat.format(
			props.getLoginResource(LocalUserProperties.KEY_LOGIN_TITLE),
			ApplicationProperties.getInstance().getCurrentVersion().getShortName());
		final JDialog dialog = optpn.createDialog(frame, sTitle);
		optpnWeak = new WeakReference<JOptionPane>(optpn);
		dialog.setName("dlgLogin");
		dialog.setIconImage(StartIcons.getInstance().getDefaultFrameIcon().getImage());
		dialog.pack();
		dialog.setResizable(false);
		dialog.setLocationRelativeTo(frame);
		
		loginPanel.getUsernameField().setText(props.getUserName());
		loginPanel.getUsernameField().setEnabled(false);
		loginPanel.getUsernameField().addFocusListener(new SelectAllFocusAdapter());
		loginPanel.getPasswordField().addFocusListener(new SelectAllFocusAdapter());

		while (true) {
			dialog.setVisible(true);
			Object result = optpn.getValue();
			if (result instanceof Integer) {
				Integer iValue = (Integer) result;
				switch (iValue) {
					case JOptionPane.OK_OPTION:
						if(cmdPerformLogin(frame, optpn, iValue, props)) {
							dialog.dispose();
							return true;
						}
						else {
							optpn.setValue(optpn.getInitialValue());
						}
						break;
					case JOptionPane.CANCEL_OPTION:
					case JOptionPane.CLOSED_OPTION:
						dialog.dispose();
						return false;
					default:
						throw new NuclosFatalException();
				}
			}
			return false;
		}
	}

	private void postProcessLogin() {
		List<LocaleInfo> clientCachedLocaleInfo
			= LocalUserProperties.getInstance().getLoginLocaleSelection();
	
		final LocaleDelegate localeDelegate = SpringApplicationContextHolder.getBean(LocaleDelegate.class);
		if (localeDelegate == null) {
			throw new IllegalStateException("Spring injection failed: Most probably cause: You need load-time weaving but started client without -javaagent.");
		}
		Collection<LocaleInfo> localeInfos = localeDelegate.getAllLocales(false);

		LocalUserProperties props = LocalUserProperties.getInstance();
		LocaleInfo selLocale;

		if (clientCachedLocaleInfo == null || !props.hasLoginLocaleUserSetting()) {
			Vector<LocaleInfo> vecLocales;
			
			String sLocales = ClientParameterProvider.getInstance().getValue(ParameterProvider.LOCALE_OPTIONS);
			//NUCLOS-5377 - If there a info about locale options in system parameters, use it.
			if (!StringUtils.isNullOrEmpty(sLocales)) {
				vecLocales = new Vector<LocaleInfo>();
				for (String sLocale : sLocales.split(",")) {
					for (LocaleInfo localeInfo : localeInfos) {
						if (localeInfo.getLanguage().equals(sLocale)) {
							vecLocales.add(localeInfo);
							break;
						}
					}
				}
			} else {
				vecLocales = new Vector<LocaleInfo>(localeInfos);
			}
			
			if (vecLocales.isEmpty()) {
				vecLocales = new Vector<LocaleInfo>(localeInfos);
			} 
			
			if (vecLocales.size() == 1) {
				selLocale = vecLocales.get(0);
				
			} else {
				// Must select new locale
				JPanel pnlSelLang = new JPanel(new BorderLayout());
				pnlSelLang.setBackground(Color.WHITE);
				pnlSelLang.setBorder(BorderFactory.createEtchedBorder());
				JTextArea ta = new JTextArea(
					MessageFormat.format(props.getLoginResource(LocalUserProperties.KEY_LANG_SELECT),
						ApplicationProperties.getInstance().getCurrentVersion().getShortName()));
				ta.setFont(loginPanel.getFont());
				ta.setEditable(false);
				pnlSelLang.add(ta, BorderLayout.CENTER);

				JComboBox<LocaleInfo> cmb = new JComboBox<LocaleInfo>(vecLocales);
				JPanel pnlCmb = new JPanel(new FlowLayout(FlowLayout.CENTER));
				pnlCmb.setBackground(Color.WHITE);
				pnlCmb.add(cmb);
				pnlSelLang.add(pnlCmb, BorderLayout.SOUTH);

				JOptionPane.showMessageDialog(this.loginPanel, pnlSelLang);

				selLocale = (LocaleInfo) cmb.getSelectedItem();				
			}
			
		}
		else {
			selLocale = (LocaleInfo) loginPanel.getLanguageComboBox().getSelectedItem();
		}
		if (clientContextCondition != null) {
			clientContextCondition.waitFor();
		}
		localeDelegate.selectLocale(localeInfos, selLocale);
	
		// NuclosCollectableEntityProvider and MainFrame
		// need access to (locale) resources. This is the first place
		// where we know the locale.
		NuclosCollectableEntityProvider.getInstance().init();
		Modules.initialize();
		
		try {
			String sReadTimeout = ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_CLIENT_READ_TIMEOUT);
			if (!StringUtils.looksEmpty(sReadTimeout)) {
				LOG.info("Set client read timeout to " + sReadTimeout + "...");

				try {
					int readTimeout = Integer.parseInt(sReadTimeout);
					RequestConfigHolder.getInstance().setDefaultSocketTimeout(readTimeout * 1000);
				} catch (NumberFormatException e) {
					LOG.error("Failed to change the client read timeout to " + sReadTimeout, e);
				}
			}
		} catch (Exception ex) {
			LOG.error("Client read timeout could not be set, using default.", ex);
		}
	}

	private boolean cmdPerformLogin(final JFrame frame, final JOptionPane optpn, final int selectedOption, final LocalUserProperties props) {
		shakeStepSize += 6;
		loginPanel.setProgressVisible(true);

		// frame.setEnabled(false);
		// disable all buttons:
		final Class<?>[] acls = {JButton.class, JTextField.class, JPasswordField.class, JComboBox.class, JCheckBox.class};
		setSubComponentsEnabled(optpn, acls, false);
		UIUtils.paintImmediately(optpn);

		boolean result = false;
		try {
			frame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			if (selectedOption == JOptionPane.OK_OPTION) {
				final String sUserName = loginPanel.getUsernameField().getText().trim();
				final char[] acPassword = loginPanel.getPasswordField().getPassword();
			
				try {
					try {
						performLogin(sUserName, acPassword);
						result = true;
					}
					catch (CredentialsExpiredException ex) {
						ChangePasswordPanel cpp = new ChangePasswordPanel(false, new String(acPassword), true);
						boolean ok = cpp.showInDialog(frame, new ChangePasswordPanel.ChangePasswordDelegate() {
							@Override
							public void changePassword(String oldPw, String newPw) throws CommonBusinessException {
								RemoteAuthenticationManager ram = SpringApplicationContextHolder.getBean(RemoteAuthenticationManager.class);
								ram.changePassword(sUserName, new String(acPassword), newPw, null);
								loginPanel.getPasswordField().setText(newPw);
							}
						});
						if (!ok) {
							return result;
						}
						else {
							performLogin(sUserName, loginPanel.getPasswordField().getPassword());
							result = true;
						}
					}

					// check for credential expiration date
					Date expirationdate = SecurityDelegate.getInstance().getPasswordExpiration();
					if (expirationdate != null) {
						long difference = expirationdate.getTime() - Calendar.getInstance().getTimeInMillis();
						difference = (difference / (1000 * 60 * 60 * 24)) + 1L;
						if (difference <= 3) {
							String message = MessageFormat.format(LocalUserProperties.getInstance().getLoginResource("login.question.password.change"), difference);
							int i = JOptionPane.showConfirmDialog(frame, message, ApplicationProperties.getInstance().getName(), JOptionPane.YES_NO_OPTION);
							if (i == JOptionPane.YES_OPTION) {
								ChangePasswordPanel cpp = new ChangePasswordPanel(false, new String(acPassword), true);
								result = cpp.showInDialog(frame, new ChangePasswordPanel.ChangePasswordDelegate() {
									@Override
									public void changePassword(String oldPw, String newPw) throws CommonBusinessException {
										RemoteAuthenticationManager ram = SpringApplicationContextHolder.getBean(RemoteAuthenticationManager.class);
										ram.changePassword(sUserName, new String(acPassword), newPw, null);
										getNuclosRemoteServerSession().relogin(sUserName, newPw);
									}
								});
							}
						}
					}

					if (result) {
						props.setUserName(sUserName);

						props.setUserPasswd(
							loginPanel.getRememberPwCheckBox().isSelected()
							? CryptUtil.encryptAESHex(new String(loginPanel.getPasswordField().getPassword()), CRYPT)
						    : "");

						// Data language
						if (loginPanel.getDataLanguageComboBox().isVisible()) {
							DataLanguageVO dataLang = (DataLanguageVO) loginPanel.getDataLanguageComboBox().getSelectedItem();
							DataLanguageContext.setDataUserLanguage(dataLang.getPrimaryKey());
							DataLanguageContext.setDataSystemLanguage(
									DataLanguageDelegate.getInstance().getPrimaryDataLanguage());
							
							props.setLoginDataLocale(dataLang.getLocale());							
						}
						
						props.store();
					}
				}
				catch (LockedException ex) {
					loginPanel.shake(shakeStepSize);
					loginPanel.setPasswordError(LocalUserProperties.getInstance().getLoginResource(LocalUserProperties.KEY_ERR_LOCKED));
				}
				catch (AccountExpiredException ex) {
					loginPanel.shake(shakeStepSize);
					loginPanel.setPasswordError(LocalUserProperties.getInstance().getLoginResource(LocalUserProperties.KEY_ERR_ACCOUNT_EXPIRED));
				}
				catch (MaintenanceModeException ex) {
					loginPanel.shake(shakeStepSize);
					loginPanel.setPasswordError(LocalUserProperties.getInstance().getLoginResource(LocalUserProperties.KEY_ERR_MAINTENANCEMODE));
				}
				catch (AuthenticationException ex) {
					loginPanel.shake(shakeStepSize);
					loginPanel.setPasswordError(LocalUserProperties.getInstance().getLoginResource(LocalUserProperties.KEY_ERR_UPASS));
				}
				catch (AccessDeniedException ex) {
					loginPanel.shake(shakeStepSize);
					loginPanel.setPasswordError(LocalUserProperties.getInstance().getLoginResource(LocalUserProperties.KEY_ERR_UPERM));
				}
				catch (RemoteAccessException ex) {
					loginPanel.shake(shakeStepSize);
					loginPanel.setPasswordError(LocalUserProperties.getInstance().getLoginResource(LocalUserProperties.KEY_ERR_SERVER));
				}
				catch (Exception ex) {
					Errors.getInstance().showExceptionDialog(frame, ex);
				}
				finally {
					StringUtils.clear(acPassword);
				}
			}
		}
		catch (CommonFatalException ex) {
			try {
				final Throwable tCause = ex.getCause();
				if (tCause instanceof Exception) {
					throw (Exception) tCause;
				}
				else if (tCause instanceof Error) {
					throw (Error) tCause;
				}
				else {
					throw new CommonFatalException("Unknown Throwable", ex);
				}
			}
			catch (NamingException exCause) {
				final String sMessage = StringUtils.looksEmpty(ex.getMessage()) ? LocalUserProperties.getInstance().getLoginResource(LocalUserProperties.KEY_ERR_SERVER) : "";
				Errors.getInstance().showExceptionDialog(frame, sMessage, ex);
			}
			catch (Exception exCause) {	// everything else
				final String sMessage = StringUtils.looksEmpty(ex.getMessage()) ? LocalUserProperties.getInstance().getLoginResource(LocalUserProperties.KEY_ERR_SERVER) : "";
				Errors.getInstance().showExceptionDialog(frame, sMessage, ex);
			}
		}
		catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(frame, ex);
		}
		finally {
			frame.setEnabled(true);
			frame.requestFocusInWindow();
			setSubComponentsEnabled(optpn, acls, true);
			loginPanel.setProgressVisible(result);
			loginPanel.getPasswordField().setText("");
			loginPanel.getPasswordField().requestFocusInWindow();
			frame.setCursor(null);
		}
		return result;
	}

	/**
	 * @todo factor out (this is generic)
	 * <br>
	 * @param comp
	 * @param acls
	 * @param bEnabled
	 */
	private static void setSubComponentsEnabled(Component comp, final Class<?>[] acls, final boolean bEnabled) {
		if (Arrays.asList(acls).contains(comp.getClass())) {
			comp.setEnabled(bEnabled);
		}

		if (comp instanceof Container) {
			Container container = (Container) comp;

			for (Component compChild : container.getComponents()) {
				if (compChild instanceof Container) {
					setSubComponentsEnabled(compChild, acls, bEnabled);
				}
			}
		}
	}

	private String performLogin(String sUserName, char[] acPassword) {
		// wait for context set. @see NUCLOS-1036 
		if (clientContextCondition != null) {
			clientContextCondition.waitFor();
		}
		final String result = getNuclosRemoteServerSession().login(sUserName, new String(acPassword));
		if (!ShutdownActions.getInstance().isRegistered(ShutdownActions.SHUTDOWNORDER_LOGOUT)) {
			ShutdownActions.getInstance().registerShutdownAction(ShutdownActions.SHUTDOWNORDER_LOGOUT, new Logout());
		}
		return result;
	}
	
	private int loadMandators() {
		 int autologin = 0;
		 UID selected = LocalUserProperties.getInstance().getMandator();
		 SecurityFacadeRemote security = SpringApplicationContextHolder.getBean(SecurityFacadeRemote.class);
		 List<MandatorVO> lstMandators = new ArrayList<MandatorVO>(security.getMandators());
		 if (lstMandators.size() == 1 && !UID.UID_NULL.equals(selected)) {
			 selected = lstMandators.get(0).getUID();
			 LocalUserProperties.getInstance().setMandator(selected);
		 }
		 lstMandators.add(0, null);
		 loginPanel.getMandatorComboBox().setModel(new DefaultComboBoxModel(lstMandators.toArray(new MandatorVO[]{})));
		 if (selected != null) {
			 for (MandatorVO mandator : lstMandators) {
				 if (UID.UID_NULL.equals(selected) && mandator == null) {
					 loginPanel.getMandatorComboBox().setSelectedIndex(0);
					 autologin = 1;
				 }
				 if (mandator != null && selected.equals(mandator.getUID())) {
					 loginPanel.getMandatorComboBox().setSelectedItem(mandator);
					 autologin = 1;
				 }
			 }
		 }
		 JOptionPane optpn = optpnWeak.get();
		 JFrame frame = frameWeak.get();
		 if (frame != null && optpn != null) {
			 loginPanel.invalidate();
			 loginPanel.revalidate();
			 optpn.invalidate();
			 optpn.revalidate();
			 optpn.repaint();
			 frame.pack();
		 }
		 loginPanel.getMandatorComboBox().setEnabled(true);
		 loginPanel.getMandatorComboBox().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					mandatorAutoLoginCanceled = true;
					MandatorVO mandator = (MandatorVO) loginPanel.getMandatorComboBox().getSelectedItem();
					LocalUserProperties.getInstance().setMandator(mandator==null?UID.UID_NULL:mandator.getUID());
				}
			});
		 loginPanel.getMandatorComboBox().addPopupMenuListener(new PopupMenuListener() {
			
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				mandatorAutoLoginCanceled = true;
			}
			
			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				//do nothing
			}
			
			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				//do nothing
			}
		});
		 
		 loginPanel.getDataLanguageComboBox().addPopupMenuListener(new PopupMenuListener() {
			
			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				mandatorAutoLoginCanceled = true;
			}
			
			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				//do nothing
			}
			
			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				//do nothing
			}
		});
		 mandatorLoaded = true;
		 if (lstMandators.size() == 2) {
			 // only 1 mandator
			 autologin = 2; 
			 // no waiting time
		 }
		 return autologin;
	}

	public void addLoginListener(LoginListener loginlistener) {
		loginListeners.add(loginlistener);
	}

	public synchronized void removeLoginListener(LoginListener loginlistener) {
		loginListeners.remove(loginlistener);
	}		
	public synchronized void fireLoginSuccessful() {
		LoginEvent ev = new LoginEvent(this, loginPanel.getUsernameField().getText().trim(), "default");
		
		for (LoginListener loginlistener : loginListeners) {
			loginlistener.loginSuccessful(ev);
		}
	}

	public void fireLoginCanceled() {
		LoginEvent ev = new LoginEvent(this);
		for (LoginListener loginlistener : loginListeners) {
			loginlistener.loginCanceled(ev);
		}
	}

	private static class SelectAllFocusAdapter extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent ev) {
			JTextComponent tc = (JTextComponent) ev.getComponent();
			tc.selectAll();
		}
	}

	private class Logout implements Runnable {
		@Override
		public void run() {
			getNuclosRemoteServerSession().logout();
		}
	}

	public void increaseLoginProgressBar(int step) {
		this.loginPanel.increaseProgress(step);
	}
}	// class LoginController
