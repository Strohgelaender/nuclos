//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.DataTyp;
import org.nuclos.client.wizard.steps.NuclosEntityAttributeAutoNumberStep;
import org.nuclos.client.wizard.steps.NuclosEntityAttributeCommonPropertiesStep;
import org.nuclos.client.wizard.steps.NuclosEntityAttributeLookupShipStep;
import org.nuclos.client.wizard.steps.NuclosEntityAttributeRelationShipStep;
import org.nuclos.client.wizard.steps.NuclosEntityAttributeTranslationStep;
import org.nuclos.common.TranslationVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.pietschy.wizard.WizardStep;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
* 
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/

public class NuclosEntityAttributeWizardStaticModel extends NuclosWizardStaticModel {

	final NuclosEntityWizardStaticModel wizardModel;

	String name;
	String desc;
	Attribute attribute;
	int iAttributeCount;
	boolean blnValueListType;
	boolean blnRefenceType;
	boolean blnLookupType;
	boolean blnAutoNumberType;
	boolean blnDocuemntFileType;
	
	boolean blnEditMode;
	
	private List<TranslationVO> lstTranslation;
	
	boolean blnProxy;
	boolean blnWriteProxy;
	boolean blnGeneric;

	public NuclosEntityAttributeWizardStaticModel(final NuclosEntityWizardStaticModel wizardModel, int attributeCount) throws CommonFinderException, CommonPermissionException {
		this.wizardModel = wizardModel;
		attribute = new Attribute(false);
		attribute.setDatatyp(DataTyp.getDefaultDataTyp());
		this.iAttributeCount = attributeCount;
		lstTranslation = new ArrayList<TranslationVO>();
	}
	
	public void setEditMode(boolean editable) {
		this.blnEditMode = editable;
	}
	
	public boolean isEditMode() {
		return blnEditMode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		this.attribute.setLabel(name);
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
		attribute.setDescription(desc);
	}
	
	public void setAttribute(Attribute attr) {
		this.attribute = attr;
	}

	public Attribute getAttribute() {
		return attribute;
	}
	
	public int getAttributeCount () {
		return iAttributeCount;
	}
			
	public boolean isValueListType() {
		return blnValueListType;
	}

	public void setValueListTyp(boolean blnValueListTyp) {
		this.blnValueListType = blnValueListTyp;
	}	
	
	public boolean isReferenceType() {
		return blnRefenceType;
	}

	public void setReferenzTyp(boolean blnReferenceTyp) {
		this.blnRefenceType = blnReferenceTyp;
	}
	
	public boolean isDocumentFileType() {
		return blnDocuemntFileType;
	}

	public void setDocumentFileType(boolean blnDocuemntFileType) {
		this.blnDocuemntFileType = blnDocuemntFileType;
	}
	
	public boolean isLookupType() {
		return blnLookupType;
	}

	public void setLookupTyp(boolean blnLookupTyp) {
		this.blnLookupType = blnLookupTyp;
	}	
	
	public boolean isAutoNumberType() {
		return blnAutoNumberType;
	}

	public void setAutoNumberType(boolean blnAutoNumberTyp) {
		this.blnAutoNumberType = blnAutoNumberTyp;
	}	
	
	public boolean isProxy() {
		return blnProxy;
	}
	
	public void setProxy(boolean proxy) {
		this.blnProxy = proxy;
	}
	public boolean isWriteProxy() {
		return blnWriteProxy;
	}
	
	public void setWriteProxy(boolean writeproxy) {
		this.blnWriteProxy = writeproxy;
	}
	public boolean isGeneric() {
		return blnGeneric;
	}

	public void setGeneric(boolean generic) {
		this.blnGeneric = generic;
	}
	
	@Override
    public JComponent getOverviewComponent() {
      return new NuclosEntityWizardStaticModelOverview(this);
    }

	@Override
	public void nextStep() {
		super.nextStep();
	}

	@Override
	public void previousStep() {
		WizardStep step = this.getActiveStep();
		if(step instanceof NuclosEntityAttributeTranslationStep) {
			if (this.isValueListType()) {
				super.previousStep();
				super.previousStep();
				super.previousStep();
				super.previousStep();
				
			} else if (isReferenceType()) {
				super.previousStep();
				super.previousStep();
				super.previousStep();
			} else if (isLookupType()) {
				super.previousStep();
				super.previousStep();
				
			} else if (isAutoNumberType()) {
				super.previousStep();
				
			} else {
				super.previousStep();
				super.previousStep();
				super.previousStep();
				super.previousStep();
				super.previousStep();
			}
		}
		else if(step instanceof NuclosEntityAttributeRelationShipStep){
			super.previousStep();
			super.previousStep();
		}
		else if(step instanceof NuclosEntityAttributeLookupShipStep){
			super.previousStep();
			super.previousStep();
			super.previousStep();
		}else if(step instanceof NuclosEntityAttributeAutoNumberStep){
			super.previousStep();
			super.previousStep();
			super.previousStep();
			super.previousStep();
		}
//		else if(step instanceof NuclosEntityAttributeAutoNumberStep){
//			super.previousStep();
//			super.previousStep();
//			super.previousStep();
//			super.previousStep();
//		}
		else if(step instanceof NuclosEntityAttributeCommonPropertiesStep){
			super.previousStep();
		}
		else {
			super.previousStep();
		}
   }
	
	public void setTranslation(List<TranslationVO> translation) {
		this.lstTranslation = translation;
	}
	
	public List<TranslationVO> getTranslation() {
		return lstTranslation;
	}

	public NuclosEntityWizardStaticModel getWizardModel() {
		return wizardModel;
	}
}
