//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.AWTKeyStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.BadLocationException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.ObjectUtils;
import org.apache.http.util.LangUtils;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.customcomp.CustomComponentCache;
import org.nuclos.client.customcomp.resplan.ClientPlanElement;
import org.nuclos.client.customcomp.resplan.ClientResPlanConfigVO;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.layout.wysiwyg.WYSIWYGLayoutControllingPanel;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGChart;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGMatrix;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubForm;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.DataTyp;
import org.nuclos.client.wizard.model.EntityAttributeTableModel;
import org.nuclos.client.wizard.model.EntityTranslationTableModel;
import org.nuclos.client.wizard.model.ValueList;
import org.nuclos.client.wizard.util.DefaultValue;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.MandatorLevelVO;
import org.nuclos.common.NucletConstants;
import org.nuclos.common.NucletLanguageEntityMeta;
import org.nuclos.common.NuclosBusinessObjectImport;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeRemote;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.pietschy.wizard.InvalidStateException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.xml.sax.SAXException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityNameStep extends NuclosEntityAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityNameStep.class);
	
	public static final String PREFS_NODE = "EntityWizard_NameStep";
	
	public static final String PREFS_KEY_FILTER_ENTITY_NAME = "filterEntityName";
	public static final String PREFS_KEY_FILTER_NUCLET = "filterNuclet";
	public static final String PREFS_KEY_FILTER_MENUPATH = "filterMenuPath";
	public static final String PREFS_KEY_FILTER_STATEMODEL = "filterStatemodel";
	public static final String PREFS_KEY_FILTER_VIRTUAL = "filterVirtual";
	public static final String PREFS_KEY_FILTER_PROXY = "filterProxy";
	public static final String PREFS_KEY_FILTER_GENERIC = "filterGeneric";
	public static final String PREFS_KEY_FILTER_DBO = "filterDbo";
	public static final String PREFS_KEY_FILTER_MANDATOR_LEVEL = "filterMandatorLevel";
	
	public static final String PREFS_KEY_COL_ENTITY_NAME = "colEntityName";
	public static final String PREFS_KEY_COL_NUCLET = "colNuclet";
	public static final String PREFS_KEY_COL_MENUPATH = "colMenuPath";
	public static final String PREFS_KEY_COL_STATEMODEL = "colStatemodel";
	public static final String PREFS_KEY_COL_VIRTUAL = "colVirtual";
	public static final String PREFS_KEY_COL_PROXY = "colProxy";
	public static final String PREFS_KEY_COL_GENERIC = "colGeneric";
	public static final String PREFS_KEY_COL_DBO = "colDbo";
	public static final String PREFS_KEY_COL_MANDATOR_LEVEL = "colMandatorLevel";
	
	private JLabel lbName;
	private JTextField tfName;
	
	private JLabel lbNuclet;
	private JComboBox<String> cmbNuclet;

	private JLabel lbChoice;
	private JXTable tblEntity;
	private EntityTableModel tblmdlEntity;
	private TableColumn[] hiddenColumns;
	
	private boolean filterEnabled;
	private JTextField tfFilterEntityName;
	private JTextField tfFilterDbo;
	private JComboBox<String> cmbFilterNuclet;
	private JComboBox<String> cmbFilterMenupath;
	private JComboBox<String> cmbFilterStatemodel;
	private JComboBox<String> cmbFilterVirtual;
	private JComboBox<String> cmbFilterProxy;
	private JComboBox<String> cmbFilterGeneric;
	private JComboBox<String> cmbFilterMandatorLevel;
	
	private JCheckBox chkShowNuclet;
	private JCheckBox chkShowMenupath;
	private JCheckBox chkShowStatemodel;
	private JCheckBox chkShowVirtual;
	private JCheckBox chkShowProxy;
	private JCheckBox chkShowGeneric;
	private JCheckBox chkShowDbo;
	private JCheckBox chkShowMandatorLevel;
	
	private JButton btnClearFilter;
	
	private JLabel lbInfo;

	private JButton btnRemove;

	private Collection<EntityMeta<?>> colMasterdata;
	private EntityMeta<?> toEdit;
	private List<MandatorLevelVO> mandatorLevels;
	private List<EntityObjectVO<UID>> nuclets;


	public NuclosEntityNameStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	public void setEntityToEdit(EntityMeta<?> vo) {
		this.toEdit = vo;
	}

	@SuppressWarnings("serial")
	@Override
	protected void initComponents() {

		colMasterdata =  MetaProvider.getInstance().getAllEntities();
		mandatorLevels = NuclosEntityCommonPropertiesStep.getMandatorLevels();
		nuclets = MetaProvider.getInstance().getNuclets();

		double size [][] = {{TableLayout.PREFERRED, 200, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.FILL}, {20,20,20,TableLayout.FILL,20}};
		
		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);
		lbName = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityname.1", "Bitte geben Sie den Namen der neuen Entit\u00e4t ein"));
		tfName = new JTextField();
		tfName.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityname.tooltip.1", "Namen der neuen Entit\u00e4t"));
		tfName.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfName.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				nextIfComplete();
			}
		});

		// NUCLOS-6405
		tfName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(final KeyEvent e) {
				if (e.getKeyChar() == '_') {
					e.consume();
				}
			}
		});
		
		lbNuclet = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.entityname.18", "in Nuclet"));
		cmbNuclet = new JComboBox<String>((String[])ArrayUtils.add(getNuclets(), 0, ""));
		cmbNuclet.setEnabled(false);
		cmbNuclet.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "nextIfComplete");
		cmbNuclet.getActionMap().put("nextIfComplete", new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				nextIfComplete();
			}
		});

		lbChoice = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityname.2", "oder w\u00e4hlen Sie eine Entit\u00e4t die Sie ver\u00e4ndern m\u00f6chten"));
		tblEntity = new JXTable();
		tblEntity.setEditable(false);
		tblEntity.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblEntity.setSortable(true);
		this.fillEntityCombobox();
		tblEntity.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
		tblEntity.getTableHeader().setReorderingAllowed(false);
		
		tblEntity.getColumn(EntityTableModel.COL_ENTITY_NAME).setMinWidth(100);
		tblEntity.getColumn(EntityTableModel.COL_NUCLET).setMinWidth(100);
		tblEntity.getColumn(EntityTableModel.COL_MENUPATH).setMinWidth(100);
		tblEntity.getColumn(EntityTableModel.COL_STATEMODEL).setMaxWidth(80);
		tblEntity.getColumn(EntityTableModel.COL_STATEMODEL).setMinWidth(15);
		tblEntity.getColumn(EntityTableModel.COL_VIRTUAL).setMaxWidth(50);
		tblEntity.getColumn(EntityTableModel.COL_VIRTUAL).setMinWidth(15);
		tblEntity.getColumn(EntityTableModel.COL_PROXY).setMaxWidth(50);
		tblEntity.getColumn(EntityTableModel.COL_PROXY).setMinWidth(15);
		tblEntity.getColumn(EntityTableModel.COL_GENERIC).setMaxWidth(60);
		tblEntity.getColumn(EntityTableModel.COL_GENERIC).setMinWidth(15);
		tblEntity.getColumn(EntityTableModel.COL_DBO).setMinWidth(100);
		if (!mandatorLevels.isEmpty()) {
			tblEntity.getColumn(EntityTableModel.COL_MANDATOR_LEVEL).setMinWidth(80);
		}
		
		JScrollPane scrlEntity = new JScrollPane(tblEntity);
		scrlEntity.setPreferredSize(new Dimension(200, 200));
		
		Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node(PREFS_NODE);
		
		chkShowNuclet = createShowColumnCheckBox(prefs, PREFS_KEY_COL_NUCLET, true);
		chkShowMenupath = createShowColumnCheckBox(prefs, PREFS_KEY_COL_MENUPATH, false);
		chkShowStatemodel = createShowColumnCheckBox(prefs, PREFS_KEY_COL_STATEMODEL, true);
		chkShowVirtual = createShowColumnCheckBox(prefs, PREFS_KEY_COL_VIRTUAL, false);
		chkShowProxy = createShowColumnCheckBox(prefs, PREFS_KEY_COL_PROXY, false);
		chkShowGeneric = createShowColumnCheckBox(prefs, PREFS_KEY_COL_GENERIC, false);
		chkShowDbo = createShowColumnCheckBox(prefs, PREFS_KEY_COL_DBO, true);
		chkShowMandatorLevel = createShowColumnCheckBox(prefs, PREFS_KEY_COL_MANDATOR_LEVEL, true);
		
		initTableColumns(prefs);
		updateTableColumnVisibility();
		
		String[] sBoolValues = new String[] {
				"", SpringLocaleDelegate.getInstance().getMsg("Yes"), SpringLocaleDelegate.getInstance().getMsg("No")
		};
		
		tfFilterEntityName = addFilterListener(new JTextField());
		cmbFilterNuclet = addFilterListener(new JComboBox<String>((String[])ArrayUtils.add(tblmdlEntity.getNuclets(), 0, "")));
		cmbFilterNuclet.setEditable(true);
		AutoCompleteDecorator.decorate(cmbFilterNuclet);
		cmbFilterMenupath = addFilterListener(new JComboBox<String>((String[])ArrayUtils.add(getMenupaths(), 0, "")));
		cmbFilterMenupath.setEditable(true);
		AutoCompleteDecorator.decorate(cmbFilterMenupath);
		cmbFilterStatemodel = addFilterListener(new JComboBox<>(sBoolValues));
		cmbFilterVirtual = addFilterListener(new JComboBox<>(sBoolValues));
		cmbFilterProxy = addFilterListener(new JComboBox<>(sBoolValues));
		cmbFilterGeneric = addFilterListener(new JComboBox<>(sBoolValues));
		tfFilterDbo = addFilterListener(new JTextField());
		cmbFilterMandatorLevel = addFilterListener(new JComboBox<>((String[])ArrayUtils.add(getMandatorLevelNames(), 0, "")));
		
		btnClearFilter = new JButton(SpringLocaleDelegate.getInstance().getMsg("general.clear.filter"), Icons.getInstance().getIconClearSearch16());
		
		try {
			EntityFilterValues filterValues = new EntityFilterValues(
					prefs.get(PREFS_KEY_FILTER_ENTITY_NAME, null), 
					prefs.get(PREFS_KEY_FILTER_NUCLET, null),
					prefs.get(PREFS_KEY_FILTER_MENUPATH, null),
					prefs.get(PREFS_KEY_FILTER_STATEMODEL, null), 
					prefs.get(PREFS_KEY_FILTER_VIRTUAL, null), 
					prefs.get(PREFS_KEY_FILTER_PROXY, null),
					prefs.get(PREFS_KEY_FILTER_GENERIC, null),
					prefs.get(PREFS_KEY_FILTER_DBO, null),
					prefs.get(PREFS_KEY_FILTER_MANDATOR_LEVEL, null));
			if (!filterValues.isEmpty()) {
				filterEnabled = false;
				tfFilterEntityName.setText(filterValues.sEntity);
				cmbFilterNuclet.setSelectedItem(filterValues.sNuclet);
				cmbFilterMenupath.setSelectedItem(filterValues.sMenupath);
				setBooleanInCombo(cmbFilterStatemodel, filterValues.bStatemodel);
				setBooleanInCombo(cmbFilterVirtual, filterValues.bVirtual);
				setBooleanInCombo(cmbFilterProxy, filterValues.bProxy);
				setBooleanInCombo(cmbFilterGeneric, filterValues.bGeneric);
				tfFilterDbo.setText(filterValues.sDbo);
				cmbFilterMandatorLevel.setSelectedItem(filterValues.sMandatorLevel);
				filterEnabled = true;
				filterEntityTable(filterValues);
			}
		} catch (Exception ex) {
			LOG.error(ex);
		} finally {
			filterEnabled = true;
		}
		
		JPanel jpnFilter = new JPanel();
		jpnFilter.setBorder(BorderFactory.createTitledBorder(SpringLocaleDelegate.getInstance().getMsg("general.columns")));
		TableLayoutBuilder tbllayFilter = new TableLayoutBuilder(jpnFilter).columns(TableLayout.PREFERRED, 5, TableLayout.PREFERRED, 15, TableLayout.PREFERRED, TableLayout.FILL);
		tbllayFilter.newRow().skip(4).addLocalizedLabel(SpringLocaleDelegate.getInstance().getMsg("general.filter"));
		tbllayFilter.newRow(20).skip(2).addLocalizedLabel("wizard.step.entityname.name").skip().addFullSpan(tfFilterEntityName).newRow(3);
		tbllayFilter.newRow(20).add(chkShowNuclet).skip().addLocalizedLabel("wizard.step.entityname.nuclet").skip().addFullSpan(cmbFilterNuclet).newRow(3);
		tbllayFilter.newRow(20).add(chkShowMenupath).skip().addLocalizedLabel("wizard.step.entityname.menupath").skip().addFullSpan(cmbFilterMenupath).newRow(3);
		tbllayFilter.newRow(20).add(chkShowStatemodel).skip().addLocalizedLabel("wizard.step.entityname.statemodel").skip().addFullSpan(cmbFilterStatemodel).newRow(3);
		tbllayFilter.newRow(20).add(chkShowVirtual).skip().addLocalizedLabel("wizard.step.entityname.virtual").skip().addFullSpan(cmbFilterVirtual).newRow(3);
		tbllayFilter.newRow(20).add(chkShowProxy).skip().addLocalizedLabel("wizard.step.entityname.proxy").skip().addFullSpan(cmbFilterProxy).newRow(3);
		tbllayFilter.newRow(20).add(chkShowGeneric).skip().addLocalizedLabel("wizard.step.entityname.generic").skip().addFullSpan(cmbFilterGeneric).newRow(3);
		tbllayFilter.newRow(20).add(chkShowDbo).skip().addLocalizedLabel("wizard.step.entityname.dbo").skip().addFullSpan(tfFilterDbo);
		if (!mandatorLevels.isEmpty()) {
			tbllayFilter.newRow(3);
			tbllayFilter.newRow(20).add(chkShowMandatorLevel).skip().addLocalizedLabel("nuclos.entity.mandatorLevel.label").skip().addFullSpan(cmbFilterMandatorLevel);
		}
		tbllayFilter.newRow(10);
		tbllayFilter.newRow(20).skip(4).add(btnClearFilter);
		
		JPanel jpnFilterBorder = new JPanel(new BorderLayout());
		jpnFilterBorder.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		jpnFilterBorder.add(jpnFilter, BorderLayout.NORTH);

		lbInfo = new JLabel();
		lbInfo.setVisible(false);
		lbInfo.setForeground(Color.RED);
		
		btnRemove = new JButton(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entityname.8", "Entit\u00e4t entfernen"));
		btnRemove.setVisible(false);

		this.add(lbName, "0,0");
		this.add(tfName, "1,0");
		this.add(lbNuclet, "2,0");
		this.add(cmbNuclet, "3,0");
		this.add(lbInfo, "1,1,4,1");
		this.add(lbChoice, "0,2");
		this.add(jpnFilterBorder, "0,3");
		this.add(scrlEntity, "1,2,4,3");
		this.add(btnRemove,"1,4");
		
		class EntityNewListener implements DocumentListener, ItemListener {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					doSomeWork(tfName.getText());
				}
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					final String entityname = e.getDocument().getText(0, e.getDocument().getLength()).trim();
					doSomeWork(entityname);
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityNameStep.this, ex);
				}
			}
			protected void doSomeWork(final String entityname) {
				lbInfo.setVisible(false);
				btnRemove.setVisible(false);
				if(StringUtils.isNullOrEmpty(entityname)) {
					NuclosEntityNameStep.this.model.resetModel();
					NuclosEntityNameStep.this.setComplete(false);
					setTableEnabled(true);
					cmbNuclet.setEnabled(false);
					return;
				}
				
				String sNucletPackage = (String)cmbNuclet.getSelectedItem();
				UID nucletUID = null;
				String localIdentifier = NucletConstants.DEFAULT_LOCALIDENTIFIER;
				if (!StringUtils.looksEmpty(sNucletPackage)) {
					for (EntityObjectVO<UID> nucletVO : nuclets) {
						if (nucletVO.getFieldValue(E.NUCLET.packagefield).equals(sNucletPackage)) {
							nucletUID = nucletVO.getPrimaryKey();
							localIdentifier = nucletVO.getFieldValue(E.NUCLET.localidentifier);
							break;
						}
					}
				}

				// NUCLOS-6296
				/*if (nucletUID == null) {
					NuclosEntityNameStep.this.setComplete(false);
					cmbNuclet.setEnabled(true);
					return;
				}*/

				NuclosEntityNameStep.this.model.setUID(new UID());

				if (!validateNameLength(entityname)) return;
				if (!validateNameUnique(entityname, NuclosEntityNameStep.this.model.getUID(), nucletUID)) return;
				if (!validateNameLegal(entityname, NuclosEntityNameStep.this.model.getUID(), nucletUID)) return;

				if(entityname.length() > 25) {
					lbInfo.setText(SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.entityname.13", "Der Tabellenname wird für interne Zwecke gekürzt!"));
					lbInfo.setVisible(true);
				} else {
					lbInfo.setVisible(false);
				}
				
				if(entityname.length() > 0) {
					if(tblEntity.getSelectedRow() >= 0) {
						NuclosEntityNameStep.this.model.resetModel();
					}
					NuclosEntityNameStep.this.setComplete(true);
					setTableEnabled(false);
					cmbNuclet.setEnabled(true);
					tblEntity.clearSelection();
				} else {
					NuclosEntityNameStep.this.model.resetModel();
					NuclosEntityNameStep.this.setComplete(false);
					setTableEnabled(true);
					cmbNuclet.setEnabled(false);
				}

				NuclosEntityNameStep.this.model.setEntityName(entityname);
				NuclosEntityNameStep.this.model.setNucletUID(nucletUID);
				NuclosEntityNameStep.this.model.setNucletLocalIdentifier(localIdentifier);
				final String proxyInterface = NuclosEntityValidator.getEntityProxyInterfaceQualifiedName(sNucletPackage, entityname, false);
			    NuclosEntityNameStep.this.model.setProxyInterface(proxyInterface);
				if(!NuclosEntityNameStep.this.model.isEditMode()) {
					if (!LangUtils.equals(NuclosEntityNameStep.this.model.getLabelSingular(), entityname)) {
						for (TranslationVO translationVO : NuclosEntityNameStep.this.model.getTranslation()) {
							translationVO.getLabels().put("titel", entityname);
						}
					}
					NuclosEntityNameStep.this.model.setLabelSingular(entityname);
				}
			}
		};
		
		cmbNuclet.addItemListener(new EntityNewListener());
		tfName.getDocument().addDocumentListener(new EntityNewListener());

		tblEntity.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getFirstIndex() >= 0 && !e.getValueIsAdjusting()) {
					final Object obj = getSelectedEntity();
					if(obj == null || obj instanceof String || (obj instanceof EntityMeta<?> && EntityMeta.NULL.equals(obj))) {
						NuclosEntityNameStep.this.model.setEditMode(false);
						NuclosEntityNameStep.this.model.resetModel();
						btnRemove.setVisible(false);
						NuclosEntityNameStep.this.setComplete(false);
					} else if(obj instanceof EntityMeta<?>) {
						NuclosEntityNameStep.this.model.setEditMode(true);
						NuclosEntityNameStep.this.model.resetModel();
						NuclosEntityNameStep.this.setComplete(true);
						btnRemove.setVisible(true);
					}
				}
			}
		});

		tblEntity.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				if (!NuclosEntityNameStep.this.isComplete()) {
					return;
				}
				if (e.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(e)) {
					try {
						applyState();
						model.nextStep();
					} catch (InvalidStateException e1) {
						// ignore
					}
				}
			}
		});
		
		ListenerUtil.registerActionListener(btnRemove, this, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					StringBuffer sbMessage = new StringBuffer();
					if(!dropEntityAllowed(getSelectedEntity(), sbMessage)) {
						JOptionPane.showMessageDialog(NuclosEntityNameStep.this, sbMessage,
							SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.12", "Entfernen nicht möglich!"), 
							JOptionPane.OK_OPTION);
						return;
					}

					boolean blnImportStructure = MetaDataDelegate.getInstance().hasEntityImportStructure((getSelectedEntity()).getUID());
					if(blnImportStructure) {
						String sMessage = SpringLocaleDelegate.getInstance().getMessage(
								"wizard.step.entityname.14",
								"Diese Entität besitzt Strukturdefinitionen für Objektimporte. Diese wird gelöscht! Sie können den Vorgang abbrechen!");
						int abort = JOptionPane.showConfirmDialog(NuclosEntityNameStep.this, sMessage,
							SpringLocaleDelegate.getInstance().getMessage("wizard.step.entityname.16", "Achtung!"), 
							JOptionPane.OK_CANCEL_OPTION);
						if(abort != JOptionPane.OK_OPTION)
							return;
					}

					boolean blnWorkflow = MetaDataDelegate.getInstance().hasEntityWorkflow((getSelectedEntity()).getUID());
					if(blnWorkflow) {
						String sMessage = SpringLocaleDelegate.getInstance().getMessage(
								"wizard.step.entityname.15",
								"Dieses Businessobjekt wird von einem Objektgenerator verwendet! Dieser Objektgenerator wird gelöscht!");
						int abort = JOptionPane.showConfirmDialog(NuclosEntityNameStep.this, sMessage,
							SpringLocaleDelegate.getInstance().getMessage("wizard.step.entityname.16", "Achtung!"), JOptionPane.OK_CANCEL_OPTION);
						if(abort != JOptionPane.OK_OPTION)
							return;
					}

					final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.entityname.9", 
							"Sind Sie sicher, dass Sie die Entit\u00e4t l\u00f6schen m\u00f6chten?");
					final String sTitle = SpringLocaleDelegate.getInstance().getMessage("wizard.step.entityname.10", "L\u00f6schen");

					int dropEntity = JOptionPane.showConfirmDialog(NuclosEntityNameStep.this, sMessage, sTitle, JOptionPane.YES_NO_OPTION);
					switch(dropEntity) {
					case JOptionPane.YES_OPTION:
						boolean bDropLayout = false;
						if(MetaDataDelegate.getInstance().hasEntityLayout((getSelectedEntity()).getUID())) {
							int dropLayout = JOptionPane.showConfirmDialog(NuclosEntityNameStep.this,
									SpringLocaleDelegate.getInstance().getMessage("wizard.step.entityname.11", "Soll das Layout der Entität gelöscht werden?"), 
									sTitle, JOptionPane.YES_NO_OPTION);
							bDropLayout = (dropLayout == JOptionPane.YES_OPTION);
						}
						
						final boolean bDropLayoutfinal = bDropLayout;
						final MainFrameTab parentFrame = model.getParentFrame();
						
						// lock screen
						final LayerLock lock = parentFrame.lockLayer();
						UIUtils.runCommandLater(getParent(), new Runnable() {
							@Override
							public void run() {
								try {
									final UID entityUid = getSelectedEntity().getUID();
									
									modifyLayoutsUsesEntity(entityUid);
									MetaDataDelegate.getInstance().removeEntity(entityUid, bDropLayoutfinal);
									
									EntityUtils.clearAllEntityCaches();

									NuclosEntityNameStep.this.model.cancelWizard();
								} catch (Exception ex) {
									Errors.getInstance().showExceptionDialog(NuclosEntityNameStep.this, ex);
									return;
								} finally {
									MasterDataDelegate.getInstance().invalidateLayoutCache();
									// unlock screen
									parentFrame.unlockLayer(lock);
								}
							}
						});
						break;
					case JOptionPane.NO_OPTION:
					default:
						return;
					}
				} catch (Exception ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityNameStep.this, ex);
					return;
				}
			}
		});

		FocusListener focusListenerSelectAllText = new FocusAdapter() {
			@Override
			public void focusGained(final FocusEvent e) {
				if (!(e.getComponent() instanceof JTextField)) {
					return;
				}
				final JTextField tf = (JTextField) e.getComponent();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						tf.selectAll();
					}
				});
			}
		};

		Set<AWTKeyStroke> keyStrokeFocusOnDown = new HashSet<AWTKeyStroke>();
		keyStrokeFocusOnDown.add(KeyStroke.getKeyStroke("DOWN"));
		tfFilterEntityName.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, keyStrokeFocusOnDown);
		tfFilterEntityName.addFocusListener(focusListenerSelectAllText);
		tfFilterDbo.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, keyStrokeFocusOnDown);
		tfFilterDbo.addFocusListener(focusListenerSelectAllText);
		Action actJumpToTable = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						if (tblEntity.getRowCount() > 0) {
							tblEntity.requestFocusInWindow();
							EntityMeta<?> selected = getSelectedEntity();
							if (selected == null) {
								tblEntity.getSelectionModel().setSelectionInterval(0, 0);
							}
						}
					}
				});				
			}
		};
		tfFilterEntityName.getInputMap().put(KeyStroke.getKeyStroke("TAB"), "jumpToTable");
		tfFilterDbo.getInputMap().put(KeyStroke.getKeyStroke("TAB"), "jumpToTable");
		tfFilterEntityName.getActionMap().put("jumpToTable", actJumpToTable);
		tfFilterDbo.getActionMap().put("jumpToTable", actJumpToTable);
		
		Action actJumpToNew = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						tfName.requestFocusInWindow();
					}
				});
			}
		};
		tfFilterEntityName.getInputMap().put(KeyStroke.getKeyStroke("UP"), "jumpToNew");
		tfFilterEntityName.getActionMap().put("jumpToNew", actJumpToNew);
		
		Action actJumpToFilter = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						tfFilterEntityName.requestFocusInWindow();
					}
				});
			}
		};
		tfName.getInputMap().put(KeyStroke.getKeyStroke("DOWN"), "jumpToFilter");
		tfName.getActionMap().put("jumpToFilter", actJumpToFilter);
		tfName.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "jumpToNew");
		tfName.getActionMap().put("jumpToNew", actJumpToNew);

		tblEntity.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "nextStep");
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tblEntity.getActionMap().put("nextStep", model.getActionNextStep());
			}
		});

		tblEntity.setFocusCycleRoot(false);
		btnRemove.setFocusCycleRoot(false);
		
		btnClearFilter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearFilter();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						tfFilterEntityName.requestFocusInWindow();
					}
				});
			}
		});
	}
	
	private void setTableEnabled(boolean enabled) {
		tblEntity.setEnabled(enabled);
		tfFilterEntityName.setEnabled(enabled);
		cmbFilterNuclet.setEnabled(enabled);
		cmbFilterMenupath.setEnabled(enabled);
		cmbFilterStatemodel.setEnabled(enabled);
		cmbFilterVirtual.setEnabled(enabled);
		cmbFilterProxy.setEnabled(enabled);
		cmbFilterGeneric.setEnabled(enabled);
		tfFilterDbo.setEnabled(enabled);
		cmbFilterMandatorLevel.setEnabled(enabled);
		btnClearFilter.setEnabled(enabled);
		chkShowNuclet.setEnabled(enabled);
		chkShowMenupath.setEnabled(enabled);
		chkShowStatemodel.setEnabled(enabled);
		chkShowVirtual.setEnabled(enabled);
		chkShowProxy.setEnabled(enabled);
		chkShowGeneric.setEnabled(enabled);
		chkShowDbo.setEnabled(enabled);
		chkShowMandatorLevel.setEnabled(enabled);
	}
	
	private void clearFilter() {
		filterEnabled = false;
		try {
			tfFilterEntityName.setText(null);
			cmbFilterNuclet.setSelectedIndex(0);
			cmbFilterMenupath.setSelectedIndex(0);
			cmbFilterStatemodel.setSelectedIndex(0);
			cmbFilterVirtual.setSelectedIndex(0);
			cmbFilterProxy.setSelectedIndex(0);
			cmbFilterGeneric.setSelectedIndex(0);
			tfFilterDbo.setText(null);
			cmbFilterMandatorLevel.setSelectedIndex(0);
		} finally {
			filterEnabled = true;
		}
		filterEntityTable(null);
	}
	
	private EntityMeta<?> getSelectedEntity() {
		EntityMeta<?> result = null;
		try {
			int selectedRow = tblEntity.convertRowIndexToModel(tblEntity.getSelectedRow());
			if (selectedRow >= 0) {
				result = tblmdlEntity.getEntityAt(selectedRow);
			}
		} catch (IndexOutOfBoundsException ex) {
			// ignore, filtering hides value.
		}
		return result;
	}
	
	private void setSelectedEntity(EntityMeta<?> eMeta) {
		if (eMeta != null) {
			clearFilter();
			int i = 0;
			for (EntityMeta<?> eMetaTable : tblmdlEntity.lstEntity) {
				if (eMetaTable.equals(eMeta)) {
					int iRow = tblEntity.convertRowIndexToView(i);
					tblEntity.setRowSelectionInterval(iRow, iRow);
					break;
				}
				i++;
			}
		}
	}
	
	private <C extends JComponent> C addFilterListener(C c) {
		if (c instanceof JTextField) {
			JTextField tf = (JTextField) c;
			tf.getDocument().addDocumentListener(new DocumentListener() {
				@Override
				public void removeUpdate(DocumentEvent e) {
					filterEntityTable();
				}
				@Override
				public void insertUpdate(DocumentEvent e) {
					filterEntityTable();
				}
				@Override
				public void changedUpdate(DocumentEvent e) {
					filterEntityTable();
				}
			});
			
			return c;
		} else if (c instanceof JComboBox) {
			@SuppressWarnings("unchecked")
			JComboBox<String> cmb = (JComboBox<String>) c;
			cmb.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						filterEntityTable();
					}
				}
			});
			return c;
		}
		
		throw new NotImplementedException();
	}
	
	private JCheckBox createShowColumnCheckBox(final Preferences prefs, final String prefsKey, boolean bDefaultSelected) {
		final JCheckBox result = new JCheckBox();
		boolean bSelected = bDefaultSelected;
		try {
			bSelected = prefs.getBoolean(prefsKey+"Visible", bDefaultSelected);
		} catch (Exception ex) {
			LOG.error(ex);
		}
		result.setSelected(bSelected);
		result.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						updateTableColumnVisibility();
					}
				});
			}
		});
		
		return result;
	}
	
	private void initTableColumns(Preferences prefs) {
		initTableColumnWidths(prefs, PREFS_KEY_COL_ENTITY_NAME, EntityTableModel.COL_ENTITY_NAME);
		initTableColumnWidths(prefs, PREFS_KEY_COL_NUCLET, EntityTableModel.COL_NUCLET);
		initTableColumnWidths(prefs, PREFS_KEY_COL_MENUPATH, EntityTableModel.COL_MENUPATH);
		initTableColumnWidths(prefs, PREFS_KEY_COL_STATEMODEL, EntityTableModel.COL_STATEMODEL);
		initTableColumnWidths(prefs, PREFS_KEY_COL_VIRTUAL, EntityTableModel.COL_VIRTUAL);
		initTableColumnWidths(prefs, PREFS_KEY_COL_PROXY, EntityTableModel.COL_PROXY);
		initTableColumnWidths(prefs, PREFS_KEY_COL_GENERIC, EntityTableModel.COL_GENERIC);
		initTableColumnWidths(prefs, PREFS_KEY_COL_DBO, EntityTableModel.COL_DBO);
		if (!mandatorLevels.isEmpty()) {
			initTableColumnWidths(prefs, PREFS_KEY_COL_MANDATOR_LEVEL, EntityTableModel.COL_MANDATOR_LEVEL);
		}
	}
	
	private void initTableColumnWidths(Preferences prefs, String prefsKey, int iModelIndex) {
		if (iModelIndex < tblEntity.getColumnCount()) {
			TableColumn col = tblEntity.getColumn(iModelIndex);
			int iMinWidth = Math.max(prefs.getInt(prefsKey+"MinWidth", col.getMinWidth()), col.getMinWidth());
			int iMaxWidth = Math.max(prefs.getInt(prefsKey+"MaxWidth", col.getMaxWidth()), col.getMaxWidth());
			int iWidth = Math.max(prefs.getInt(prefsKey+"Width", col.getWidth()), col.getWidth());
			col.setMinWidth(iMinWidth);
			col.setMaxWidth(iMaxWidth);
			col.setWidth(iWidth);
			col.setPreferredWidth(iWidth);
		}
	}
	
	private void updateTableColumnVisibility() {
		if (hiddenColumns == null) {
			hiddenColumns = new TableColumn[tblmdlEntity.getColumnCount()];
		}
		setTableColumnVisibility(EntityTableModel.COL_NUCLET, chkShowNuclet.isSelected());
		setTableColumnVisibility(EntityTableModel.COL_MENUPATH, chkShowMenupath.isSelected());
		setTableColumnVisibility(EntityTableModel.COL_STATEMODEL, chkShowStatemodel.isSelected());
		setTableColumnVisibility(EntityTableModel.COL_VIRTUAL, chkShowVirtual.isSelected());
		setTableColumnVisibility(EntityTableModel.COL_PROXY, chkShowProxy.isSelected());
		setTableColumnVisibility(EntityTableModel.COL_GENERIC, chkShowGeneric.isSelected());
		setTableColumnVisibility(EntityTableModel.COL_DBO, chkShowDbo.isSelected());
		if (!mandatorLevels.isEmpty()) {
			setTableColumnVisibility(EntityTableModel.COL_MANDATOR_LEVEL, chkShowMandatorLevel.isSelected());
		}
	}
	
	private void setTableColumnVisibility(int colModelIndex, boolean bVisible) {
		if (bVisible) {
			if (hiddenColumns[colModelIndex] != null) {
				final TableColumn col = tblEntity.getColumn(colModelIndex);
				col.setMinWidth(hiddenColumns[colModelIndex].getMinWidth());
				col.setMaxWidth(hiddenColumns[colModelIndex].getMaxWidth());
				col.setWidth(hiddenColumns[colModelIndex].getWidth());
				col.setPreferredWidth(hiddenColumns[colModelIndex].getWidth());
				hiddenColumns[colModelIndex] = null;
			}
		} else {
			if (hiddenColumns[colModelIndex] == null) {
				final TableColumn col = tblEntity.getColumn(colModelIndex);
				hiddenColumns[colModelIndex] = new TableColumn(colModelIndex); 
				hiddenColumns[colModelIndex].setMinWidth(col.getMinWidth());
				hiddenColumns[colModelIndex].setMaxWidth(col.getMaxWidth());
				hiddenColumns[colModelIndex].setWidth(col.getWidth());
				col.setMinWidth(0);
				col.setMaxWidth(0);
				col.setWidth(0);
			}
		}
	}
	
	private static class EntityFilterValues {
		public final String sEntity;
		public final String sNuclet;
		public final String sMenupath;
		public final Boolean bStatemodel;
		public final Boolean bVirtual;
		public final Boolean bProxy;
		public final Boolean bGeneric;
		public final String sDbo;
		public final String sMandatorLevel;
		public EntityFilterValues(String sEntity, String sNuclet, String sMenupath, String sStatemodel, String sVirtual, String sProxy, String sGeneric, String sDbo, String sMandatorLevel) {
			this(sEntity, sNuclet, sMenupath,
					sStatemodel==null?null:Boolean.parseBoolean(sStatemodel),
					sVirtual==null?null:Boolean.parseBoolean(sVirtual),
					sProxy==null?null:Boolean.parseBoolean(sProxy),
					sGeneric==null?null:Boolean.parseBoolean(sGeneric),
					sDbo,
					sMandatorLevel);
		}
		public EntityFilterValues(String sEntity, String sNuclet, String sMenupath, Boolean bStatemodel, Boolean bVirtual, Boolean bProxy, Boolean bGeneric, String sDbo, String sMandatorLevel) {
			super();
			this.sEntity = sEntity==null?null:sEntity.toLowerCase();
			this.sNuclet = sNuclet==null?null:sNuclet.toLowerCase();
			this.sMenupath = sMenupath==null?null:sMenupath.toLowerCase();
			this.bStatemodel = bStatemodel;
			this.bVirtual = bVirtual;
			this.bProxy = bProxy;
			this.bGeneric = bGeneric;
			this.sDbo = sDbo==null?null:sDbo.toLowerCase();
			this.sMandatorLevel = sMandatorLevel==null?null:sMandatorLevel.toLowerCase();
		}
		public boolean isEmpty() {
			return StringUtils.looksEmpty(sEntity) &&
					StringUtils.looksEmpty(sNuclet) &&
					StringUtils.looksEmpty(sMenupath) &&
					bStatemodel == null &&
					bVirtual == null &&
					bProxy == null &&
					bGeneric == null &&
					StringUtils.looksEmpty(sDbo) && 
					StringUtils.looksEmpty(sMandatorLevel);
		}
	}
	
	/**
	 * selected index
	 * 0 == null
	 * 1 == true
	 * 2 == false
	 */
	private Boolean getBooleanFromCombo(@SuppressWarnings("rawtypes") JComboBox cmb) {
		switch (cmb.getSelectedIndex()) {
		case 1: return true;
		case 2: return false;
		default: return null;
		}
	}
	
	/**
	 * selected index
	 * 0 == null
	 * 1 == true
	 * 2 == false
	 */
	private void setBooleanInCombo(@SuppressWarnings("rawtypes") JComboBox cmb, Boolean b) {
		if (b == null) {
			cmb.setSelectedIndex(0);
		} else {
			cmb.setSelectedIndex(b?1:2);
		}
	}
	
	private static class EntityFilter extends RowFilter<EntityTableModel, Integer> {

		private final EntityFilterValues values;
		
		public EntityFilter(EntityFilterValues values) {
			super();
			this.values = values;
		}

		@Override
		public boolean include(javax.swing.RowFilter.Entry<? extends EntityTableModel, ? extends Integer> entry) {
			EntityTableModel model = entry.getModel();
			Integer iRow = entry.getIdentifier();
			if (!include(model.getValueAt(iRow, EntityTableModel.COL_ENTITY_NAME), values.sEntity, true)) {
				return false;
			}
			if (!include(model.getValueAt(iRow, EntityTableModel.COL_NUCLET), values.sNuclet, false)) {
				return false;
			}
			if (!include(model.getValueAt(iRow, EntityTableModel.COL_MENUPATH), values.sMenupath, false)) {
				return false;
			}
			if (values.bStatemodel != null && !values.bStatemodel.equals(Boolean.TRUE.equals(model.getValueAt(iRow, EntityTableModel.COL_STATEMODEL)))) {
				return false;
			}
			if (values.bVirtual != null && !values.bVirtual.equals(Boolean.TRUE.equals(model.getValueAt(iRow, EntityTableModel.COL_VIRTUAL)))) {
				return false;
			}
			if (values.bProxy != null && !values.bProxy.equals(Boolean.TRUE.equals(model.getValueAt(iRow, EntityTableModel.COL_PROXY)))) {
				return false;
			}
			if (values.bGeneric != null && !values.bGeneric.equals(Boolean.TRUE.equals(model.getValueAt(iRow, EntityTableModel.COL_GENERIC)))) {
				return false;
			}
			if (!include(model.getValueAt(iRow, EntityTableModel.COL_DBO), values.sDbo, true)) {
				return false;
			}
			if (!include(model.getValueAt(iRow, EntityTableModel.COL_MANDATOR_LEVEL), values.sMandatorLevel, false)) {
				return false;
			}
			
			return true;
		}
		
		private boolean include(Object oEntry, String sFilterValue, boolean bLike) {
			String sEntry = (String) oEntry;
			if (!StringUtils.looksEmpty(sFilterValue)) {
				if (sFilterValue.endsWith("*") || sFilterValue.startsWith("*")) {
					bLike = true;
				}
				if (StringUtils.looksEmpty(sEntry)) {
					return false;
				}
				String sLowerEntry = sEntry.toLowerCase().trim();
				if (bLike) {
					if (sFilterValue.startsWith("\"")) {
						String s = sFilterValue.replaceAll("\"", "").replaceAll("\\*", "");
						if (!sLowerEntry.startsWith(s)) {
							return false;
						}
					} else {
						String s = sFilterValue.replaceAll("\\*", "");
						if (!sLowerEntry.contains(s)) {
							return false;
						}
					}
				} else {
					if (!sLowerEntry.equals(sFilterValue)) {
						return false;
					}
				}
			}
			return true;
		}
		
	}
	
	private void filterEntityTable() {
		if (filterEnabled) {
			final EntityFilterValues filterValues = new EntityFilterValues(
				tfFilterEntityName.getText(), 
				(String)cmbFilterNuclet.getSelectedItem(), 
				(String)cmbFilterMenupath.getSelectedItem(),
				getBooleanFromCombo(cmbFilterStatemodel), 
				getBooleanFromCombo(cmbFilterVirtual), 
				getBooleanFromCombo(cmbFilterProxy),
				getBooleanFromCombo(cmbFilterGeneric),
				tfFilterDbo.getText(),
				(String)cmbFilterMandatorLevel.getSelectedItem());
			filterEntityTable(filterValues);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void filterEntityTable(EntityFilterValues filterValues) {
		if (filterEnabled) {
			if (filterValues == null || filterValues.isEmpty()) {
				tblEntity.setRowFilter(null);
			} else {
				tblEntity.setRowFilter((RowFilter)new EntityFilter(filterValues));
			}
		}
	}

	/**
	 * Shows an error text and marks this step as incomplete.
	 *
	 * @param resourceKey
	 * @param defaultText
	 */
	private void error(final String resourceKey, final String defaultText) {
		NuclosEntityNameStep.this.setComplete(false);
		lbInfo.setText(SpringLocaleDelegate.getInstance().getMessage(resourceKey, defaultText));
		lbInfo.setVisible(true);
	}

	private Collection<EntityMeta<?>> getEntityMetasForValidation() {
		Collection<EntityMeta<?>> colMetas = new ArrayList<>(MetaProvider.getInstance().getAllEntities());
		for (MasterDataVO<?> ipMd : MasterDataCache.getInstance().get(E.NUCLET_INTEGRATION_POINT.getUID())) {
			EntityMetaVO ipMeta = new EntityMetaVO(UID.class);
			ipMeta.setUID((UID) ipMd.getPrimaryKey());
			ipMeta.setNuclet(ipMd.getFieldUid(E.NUCLET_INTEGRATION_POINT.nuclet));
			ipMeta.setEntityName(ipMd.getFieldValue(E.NUCLET_INTEGRATION_POINT.name));
			colMetas.add(ipMeta);
		}
		return colMetas;
	}

	private boolean validateNameLegal(final String entityname, final UID entityUID, final UID nucletUID) {
		if (!NuclosEntityValidator.isValidEntityName(
				entityname,
				nucletUID,
				entityUID,
				getEntityMetasForValidation(),
				Arrays.asList(NuclosBusinessObjectImport.values())
		)) {
			error(
					"wizard.step.entityname.17",
					"Der angegebene Name ist für systeminterne Zwecke reserviert!"
			);
			return false;
		}
		return true;
	}

	private boolean validateNameUnique(final String entityname, final UID entityUID, final UID nucletUID) {

		if (NuclosEntityValidator.isDuplicate(
				entityname,
				nucletUID,
				entityUID,
				getEntityMetasForValidation()
		)) {
			error(
					"wizard.step.entityname.4",
					"Entit\u00e4t ist schon vorhanden!"
			);
			return false;
		}
		return true;
	}

	private boolean validateNameLength(final String entityname) {
		if(entityname.length() > 250) {
			error(
					"wizard.step.entityname.12",
					"Der Name ist zu lang. Bitte k\u00fcrzen!"
			);
			return false;
		}
		return true;
	}

	private void modifyLayoutsUsesEntity(UID entityUid) {
		final Set<UID> setLayoutsUsagesEntity = NuclosWizardUtils.searchParentEntity(entityUid);
		for (UID layoutUsagesUid : setLayoutsUsagesEntity) {
			final CollectableComparison compare = org.nuclos.common.SearchConditionUtils.newComparison(E.LAYOUTUSAGE.entity, ComparisonOperator.EQUAL, layoutUsagesUid);
			for(MasterDataVO<UID> voUsage : MasterDataDelegate.getInstance().<UID>getMasterData(E.LAYOUTUSAGE.getUID(), compare)) {
				try {
					final UID layoutUid = voUsage.getFieldUid(E.LAYOUTUSAGE.layout);
					final UID layoutEntityUID = voUsage.getFieldUid(E.LAYOUTUSAGE.entity);
					final MasterDataVO<?> voLayout = MasterDataDelegate.getInstance().get(E.LAYOUT.getUID(), layoutUid);
	
					String sLayout = voLayout.getFieldValue(E.LAYOUT.layoutML);
					if(sLayout == null)
						continue;
					
					WYSIWYGLayoutControllingPanel ctrlPanel = new WYSIWYGLayoutControllingPanel(new WYSIWYGMetaInformation(layoutUid));
					try {
						CollectableEntity entity = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(layoutEntityUID);
						ctrlPanel.getMetaInformation().setCollectableEntity(entity);
						ctrlPanel.setLayoutML(sLayout);
						for(WYSIWYGComponent comp : ctrlPanel.getAllComponents()) {
							if (comp instanceof WYSIWYGChart) {
								if(LangUtils.equals(((WYSIWYGChart)comp).getEntityUID(), entityUid)) {
									ctrlPanel.getEditorPanel().getTableLayoutUtil().removeComponentFromLayout(comp);
								}
							} else if (comp instanceof WYSIWYGSubForm) {
								if(LangUtils.equals(((WYSIWYGSubForm)comp).getEntityUID(), entityUid)) {
									ctrlPanel.getEditorPanel().getTableLayoutUtil().removeComponentFromLayout(comp);
								}
							} else if (comp instanceof WYSIWYGMatrix) {
								if(LangUtils.equals(((WYSIWYGMatrix)comp).getEntityX(), entityUid)
										|| LangUtils.equals(((WYSIWYGMatrix)comp).getEntityY(), entityUid)) {
									ctrlPanel.getEditorPanel().getTableLayoutUtil().removeComponentFromLayout(comp);
								}
							}
						}
						sLayout = ctrlPanel.getLayoutML();
					}
					catch(CommonBusinessException e) {
						// don't modify layout
						LOG.info("modifyLayout: " + e);
					}
					catch(SAXException e) {
					   // don't modify layout
						LOG.info("modifyLayout: " + e);
					}
	
					if(sLayout != null) {
						voLayout.setFieldValue(E.LAYOUT.layoutML, sLayout);
						try {
							MasterDataDelegate.getInstance().update(E.LAYOUT.getUID(), voLayout, new DependentDataMap(), null, false);
						} catch(CommonBusinessException e) {
							throw new NuclosFatalException(e);
						}
					}
				} catch(CommonBusinessException e) {
					throw new NuclosFatalException(e);
				}
			}
		}
	}

	static void loadValueList(Attribute attr) {
		if(attr.getMetaVO() == null)
			return;

		UID entity = attr.getMetaVO().getUID();

		final Collection<MasterDataVO<?>> colMasterdata = MasterDataCache.getInstance().get(entity);

		attr.setValueListEntity(attr.getMetaVO().getUID());
		attr.setValueListEntityName(attr.getMetaVO().getEntityName());
		attr.setValueListNew(false);

		if(colMasterdata.size() == 0) {
			attr.setValueList(new ArrayList<ValueList>());
			return;
		}
		
		UID value = null;
		UID description = null;
		UID mnemonic = null;
		UID validFrom = null;
		UID validUntil = null;
		
		for (FieldMeta<?> fieldMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity).values()) {
			if ("value".equals(fieldMeta.getFieldName())) {
				value = fieldMeta.getUID();
			} else if ("description".equals(fieldMeta.getFieldName())) {
				description = fieldMeta.getUID();
			} else if ("mnemonic".equals(fieldMeta.getFieldName())) {
				mnemonic = fieldMeta.getUID();
			} else if ("validFrom".equals(fieldMeta.getFieldName())) {
				validFrom = fieldMeta.getUID();
			} else if ("validUntil".equals(fieldMeta.getFieldName())) {
				validUntil = fieldMeta.getUID();
			} 
		}

		for(MasterDataVO<?> vo : colMasterdata) {
			final ValueList valueList = new ValueList();
			
			valueList.setId(IdUtils.toLongId(vo.getPrimaryKey()));
			
			if (value != null) valueList.setLabel(vo.getFieldValue(value)+"");
			if (description != null) valueList.setDescription((String)vo.getFieldValue(description));
			if (mnemonic != null) valueList.setMnemonic(vo.getFieldValue(mnemonic)+"");
			if (validFrom != null) valueList.setValidFrom((Date)vo.getFieldValue(validFrom));
			if (validUntil != null) valueList.setValidUntil((Date)vo.getFieldValue(validUntil));
			
			valueList.setVersionId(vo.getVersion());
			
			attr.getValueList().add(valueList);
		}
	}

	private boolean dropEntityAllowed(EntityMeta<?> voEntity, StringBuffer sb) {
		final MetaProvider mp = MetaProvider.getInstance();
		final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
		final String entityToDrop = voEntity.getEntityName();
		final List<String> params = new ArrayList<String>();
		boolean blnAllowed = true;

		final Jaxb2Marshaller jaxb2Marshaller = SpringApplicationContextHolder.getBean(Jaxb2Marshaller.class);

		for (EntityMeta<?> vo : mp.getAllEntities()) {
			if (vo.getUID().equals(voEntity.getUID()) || "NucletReleaseNote".equals(vo.getEntityName()) || vo instanceof NucletLanguageEntityMeta) {
				continue;
			}
			for (FieldMeta<?> voField : mp.getAllEntityFieldsByEntity(vo.getUID()).values()) {
				final UID foreignEntity = voField.getForeignEntity();
				if (foreignEntity != null 
						&& foreignEntity.equals(voEntity.getUID())) {
					params.add(sld.getTextFallback(
							vo.getLocaleResourceIdForLabel(), vo.getEntityName()));
					params.add(sld.getTextFallback(
							voField.getLocaleResourceIdForLabel(), voField.getFieldName()));
					blnAllowed = false;
				}
			}
			//check for usage in plan-element
			if (vo.getUID().equals(E.CUSTOMCOMPONENT.getUID())) {
				for (CustomComponentVO ccvo : CustomComponentCache.getInstance().getAll()) {
					if ("org.nuclos.resplan".equals(ccvo.getComponentType())) {
						ClientResPlanConfigVO ptConfigVO = ClientResPlanConfigVO.fromBytes(ccvo.getData(),jaxb2Marshaller);
						List<ClientPlanElement> lstPLEs = ptConfigVO.getPlanElements();
						for (ClientPlanElement ple : lstPLEs) {
							if (voEntity.getUID().equals(ple.getEntity())) {
								blnAllowed = false;
								if (sb.length() > 0) {
									sb.append("\n");
								}
								sb.append(sld.getMessage("wizard.step.entityname.19",
										"Die Entität wird in einem Planelement ...", entityToDrop, ccvo.getInternalName()));
							}
						}
					}
				}
			}
		}
		if (!blnAllowed) {
			for (Iterator<String> it = params.iterator(); it.hasNext();) {
				if (sb.length() > 0) {
					sb.append("\n");
				}
				sb.append(sld.getMessage("wizard.step.entityname.7",
						"Die Entität wird referenziert von ...", entityToDrop, it.next(), it.next()));
			}
		}

		return blnAllowed;
	}

	protected static void setAttributeGroup(Attribute attr, FieldMeta<?> fieldVO) {
		if(fieldVO.getFieldGroup()  != null) {
			for(MasterDataVO<?> vo : MasterDataCache.getInstance().get(E.ENTITYFIELDGROUP.getUID())) {
				if(LangUtils.equals(vo.getPrimaryKey(), fieldVO.getFieldGroup())) {
					attr.setAttributeGroup((UID) vo.getPrimaryKey());
					break;
				}
			}
		}
	}

	@Override
	public void close() {
		lbName = null;
		tfName = null;
		
		lbNuclet = null;
		cmbNuclet = null;

		lbChoice = null;
		
		Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node(PREFS_NODE);
		try {
			writeToPreferences(prefs, PREFS_KEY_FILTER_ENTITY_NAME, tfFilterEntityName.getText());
			writeToPreferences(prefs, PREFS_KEY_FILTER_NUCLET, (String)cmbFilterNuclet.getSelectedItem());
			writeToPreferences(prefs, PREFS_KEY_FILTER_MENUPATH, (String)cmbFilterMenupath.getSelectedItem());
			writeToPreferences(prefs, PREFS_KEY_FILTER_STATEMODEL, getBooleanFromCombo(cmbFilterStatemodel));
			writeToPreferences(prefs, PREFS_KEY_FILTER_VIRTUAL, getBooleanFromCombo(cmbFilterVirtual));
			writeToPreferences(prefs, PREFS_KEY_FILTER_PROXY, getBooleanFromCombo(cmbFilterProxy));
			writeToPreferences(prefs, PREFS_KEY_FILTER_GENERIC, getBooleanFromCombo(cmbFilterGeneric));
			writeToPreferences(prefs, PREFS_KEY_FILTER_DBO, tfFilterDbo.getText());
			writeToPreferences(prefs, PREFS_KEY_FILTER_MANDATOR_LEVEL, (String)cmbFilterMandatorLevel.getSelectedItem());
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_ENTITY_NAME, tblEntity, EntityTableModel.COL_ENTITY_NAME, null);
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_NUCLET, tblEntity, EntityTableModel.COL_NUCLET, chkShowNuclet);
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_MENUPATH, tblEntity, EntityTableModel.COL_MENUPATH, chkShowMenupath);
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_STATEMODEL, tblEntity, EntityTableModel.COL_STATEMODEL, chkShowStatemodel);
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_VIRTUAL, tblEntity, EntityTableModel.COL_VIRTUAL, chkShowVirtual);
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_PROXY, tblEntity, EntityTableModel.COL_PROXY, chkShowProxy);
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_GENERIC, tblEntity, EntityTableModel.COL_GENERIC, chkShowGeneric);
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_DBO, tblEntity, EntityTableModel.COL_DBO, chkShowDbo);
			writeTableColumnToPreferences(prefs, PREFS_KEY_COL_MANDATOR_LEVEL, tblEntity, EntityTableModel.COL_MANDATOR_LEVEL, chkShowMandatorLevel);
		} catch (Exception ex) {
			LOG.error(ex);
		}

		tblEntity = null;
		tblmdlEntity = null;
		hiddenColumns = null;
		
		tfFilterEntityName = null;
		cmbFilterNuclet = null;
		cmbFilterMenupath = null;
		cmbFilterStatemodel = null;
		cmbFilterVirtual = null;
		cmbFilterProxy = null;
		cmbFilterGeneric = null;
		tfFilterDbo = null;
		cmbFilterMandatorLevel = null;
		
		btnClearFilter = null;
		
		chkShowNuclet = null;
		chkShowMenupath = null;
		chkShowStatemodel = null;
		chkShowVirtual = null;
		chkShowProxy = null;
		chkShowGeneric = null;
		chkShowDbo = null;
		chkShowMandatorLevel = null;
		
		lbInfo = null;

		btnRemove = null;

		colMasterdata = null;
		
		toEdit = null;
		
		mandatorLevels = null;
		
		nuclets = null;

		super.close();
	}
	
	private static void writeToPreferences(Preferences prefs, String key, Boolean value) {
		if (value == null) {
			prefs.remove(key);
		} else {
			writeToPreferences(prefs, key, value.toString());
		}
	}
	
	private static void writeToPreferences(Preferences prefs, String key, String value) {
		if (StringUtils.looksEmpty(value)) {
			prefs.remove(key);
		} else {
			prefs.put(key, value);
		}
	}
	
	private static void writeTableColumnToPreferences(Preferences prefs, String key, JXTable table, int iModelIndex, JCheckBox chkVisible) {
		if (chkVisible != null) {
			prefs.putBoolean(key+"Visible", chkVisible.isSelected());
		}
		if (iModelIndex < table.getColumnCount()) {
			TableColumn col = table.getColumn(iModelIndex);
			prefs.putInt(key+"MinWidth", col.getMinWidth());
			prefs.putInt(key+"MaxWidth", col.getMaxWidth());
			prefs.putInt(key+"Width", col.getWidth());
		}
	}

	@Override
	public void applyState() throws InvalidStateException {
		String localIdentifier = NucletConstants.DEFAULT_LOCALIDENTIFIER;
		
		Object obj = getSelectedEntity();
		if(obj instanceof EntityMeta<?> && !EntityMeta.NULL.equals(obj)) {
			try {
				final EntityMeta<?> vo = (EntityMeta<?>)obj;
				
				NuclosEntityNameStep.this.model.resetModel();
				if (vo.getVirtualEntity() != null || vo.isProxy() || vo.isGeneric()) {
					NuclosEntityNameStep.this.model.setHasRows(true);
				} else {
					if (vo.getUID() == null)
						NuclosEntityNameStep.this.model.setHasRows(false);
					else
						NuclosEntityNameStep.this.model.setHasRows(
								MetaDataDelegate.getInstance().hasEntityRows(vo.getUID()));
				}

				NuclosEntityNameStep.this.model.setNucletUID(vo.getNuclet());
				NuclosEntityNameStep.this.model.setUID(vo.getUID());
				NuclosEntityNameStep.this.model.setEditMode(true);
				NuclosEntityNameStep.this.model.setResultDetailsSplit(vo.isResultdetailssplitview());
				NuclosEntityNameStep.this.model.setEntityName(vo.getEntityName());
				NuclosEntityNameStep.this.model.setLabelSingular(SpringLocaleDelegate.getInstance().getResource(
						vo.getLocaleResourceIdForLabel(), ""));
				NuclosEntityNameStep.this.model.setEditable(vo.isEditable());
				NuclosEntityNameStep.this.model.setSearchable(vo.isSearchable());
				NuclosEntityNameStep.this.model.setShowSearch(vo.isShowSearch());
				NuclosEntityNameStep.this.model.setThin(vo.isThin());
				NuclosEntityNameStep.this.model.setLogbook(vo.isLogBookTracking());
				NuclosEntityNameStep.this.model.setMenuPath(SpringLocaleDelegate.getInstance().getResource(
						vo.getLocaleResourceIdForMenuPath(), ""));
				NuclosEntityNameStep.this.model.setCachable(vo.isCacheable());
				NuclosEntityNameStep.this.model.setImExport(vo.isImportExport());
				NuclosEntityNameStep.this.model.setShowRelation(vo.isTreeRelation());
				NuclosEntityNameStep.this.model.setShowGroups(vo.isTreeGroup());
				NuclosEntityNameStep.this.model.setDataLangRefPath(vo.getDataLangRefPath());
				NuclosEntityNameStep.this.model.setComment(vo.getComment());
				NuclosEntityNameStep.this.model.setStateModel(vo.isStateModel());
				NuclosEntityNameStep.this.model.setTableOrViewName(vo.getDbSelect());
				final String sNodeLabel = vo.getLocaleResourceIdForTreeView() == null ? ""
						: SpringLocaleDelegate.getInstance().getTextForStaticLabel(vo.getLocaleResourceIdForTreeView());
				NuclosEntityNameStep.this.model.setNodeLabel(sNodeLabel.startsWith("[Missing resource id=null, locale") ? null : sNodeLabel);
				final String sNodeTooltip = vo.getLocaleResourceIdForTreeViewDescription() == null ? ""
						: SpringLocaleDelegate.getInstance().getTextForStaticLabel(vo.getLocaleResourceIdForTreeViewDescription());
				NuclosEntityNameStep.this.model.setNodeTooltip(sNodeTooltip.startsWith("[Missing resource id=null, locale") ? null : sNodeTooltip);
				NuclosEntityNameStep.this.model.setMultiEditEquation(vo.getFieldsForEquality());
				NuclosEntityNameStep.this.model.setLabelSingularResource(vo.getLocaleResourceIdForLabel());
				NuclosEntityNameStep.this.model.setMenuPathResource(vo.getLocaleResourceIdForMenuPath());
				NuclosEntityNameStep.this.model.setNodeLabelResource(vo.getLocaleResourceIdForTreeView());
				NuclosEntityNameStep.this.model.setNodeTooltipResource(vo.getLocaleResourceIdForTreeViewDescription());
				NuclosEntityNameStep.this.model.setVirtualentity(vo.getVirtualEntity());
				NuclosEntityNameStep.this.model.setIdFactory(vo.getIdFactory());
				NuclosEntityNameStep.this.model.setRowColorScript(vo.getRowColorScript());
				NuclosEntityNameStep.this.model.setCloneGenerator(vo.getCloneGenerator());
				NuclosEntityNameStep.this.model.setMandatorLevel(vo.getMandatorLevel());
				NuclosEntityNameStep.this.model.setMandatorLevelUnchanged(vo.getMandatorLevel());
				NuclosEntityNameStep.this.model.setMandatorInitialFill(null);
				NuclosEntityNameStep.this.model.setMandatorUnique(vo.isMandatorUnique());
				NuclosEntityNameStep.this.model.setLockMode(vo.getLockMode());
				NuclosEntityNameStep.this.model.setOwnerForeignEntityField(vo.getOwnerForeignEntityField());
				NuclosEntityNameStep.this.model.setUnlockMode(vo.getUnlockMode());
				
				if(vo.getResource() != null) {
					NuclosEntityNameStep.this.model.setResourceName(
							ResourceCache.getInstance().getResourceById(vo.getResource()).getName());
				}
		
				NuclosEntityNameStep.this.model.setSystemIdPrefix(vo.getSystemIdPrefix());
		
				NuclosEntityNameStep.this.model.setAccelerator(vo.getAccelerator());
				NuclosEntityNameStep.this.model.setModifier(vo.getAcceleratorModifier());
		
			    NuclosEntityNameStep.this.model.setResourceUID(vo.getResource());
			    NuclosEntityNameStep.this.model.setNuclosResourceName(vo.getNuclosResource());
			    
			    NuclosEntityNameStep.this.model.setProxy(vo.isProxy());
			    NuclosEntityNameStep.this.model.setWriteProxy(vo.isWriteProxy());

			    
			    final String proxyInterface;
			    if (vo.isProxy()||vo.isWriteProxy()) {
			    	proxyInterface = SpringApplicationContextHolder.getBean(EventSupportFacadeRemote.class).
			    		getEntityProxyInterfaceQualifiedName(vo.getUID());
			    } else {
			    	proxyInterface = null;
			    }
			    NuclosEntityNameStep.this.model.setProxyInterface(proxyInterface);
				NuclosEntityNameStep.this.model.setGeneric(vo.isGeneric());
		
			    if (model.isStateModel()) {
			    	loadProcesses(vo.getUID());
			    }
			    loadEntityMenus(vo.getUID());
		
				final EntityAttributeTableModel attributeModel = new EntityAttributeTableModel(model);
				final Collection<FieldMeta<?>> lstFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(vo.getUID()).values();
				for(FieldMeta<?> fieldVO : CollectionUtils.sorted(lstFields, new Comparator<FieldMeta<?>>() {
						@Override
						public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
							Integer order1 = (o1.getOrder()==null)?0:o1.getOrder();
							Integer order2 = (o2.getOrder()==null)?0:o2.getOrder();
							return order1.compareTo(order2);
						}
					})) {
					if (SF.isEOField(vo.getUID(), fieldVO.getUID())) {
						attributeModel.addSystemAttribute(wrapEntityMetaFieldVO(fieldVO, true));
						continue;
					}
					
					attributeModel.addAttribute(wrapEntityMetaFieldVO(fieldVO, false));
				}
				
				NuclosEntityNameStep.this.model.setAttributeModel(attributeModel);

				final UID uidNuclet = vo.getNuclet();
				if (uidNuclet != null) {
					final MasterDataVO<UID> voNuclet = MasterDataCache.getInstance().get(E.NUCLET.getUID(), uidNuclet);
					localIdentifier = voNuclet.getFieldValue(E.NUCLET.localidentifier);
				}
				NuclosEntityNameStep.this.model.setNucletLocalIdentifier(localIdentifier);
			}
			catch(CommonFinderException e1) {
				Errors.getInstance().showExceptionDialog(NuclosEntityNameStep.this, e1);
			}
			catch(CommonPermissionException e1) {
				Errors.getInstance().showExceptionDialog(NuclosEntityNameStep.this, e1);
			}
			
			// setup tree configuration
			loadTreeView();
		}
		
		if (StringUtils.looksEmpty(NuclosEntityNameStep.this.model.getNucletLocalIdentifier())) {
			throw new NuclosFatalException("Local identifier must not be null");
		}

		model.getParentFrame().setTitle(model.getEntityName());
		NuclosEntityNameStep.this.model.setTranslation(fillUpTranslations());
		
		super.applyState();
	}
	
	private List<TranslationVO> fillUpTranslations() {
		final EntityTranslationTableModel tablemodel = new EntityTranslationTableModel();
		for(LocaleInfo voLocale : LocaleDelegate.getInstance().getAllLocales(false)) {
			final Map<String, String> map = new HashMap<String, String>();			
			final TranslationVO translation = new TranslationVO(voLocale, map);
			for(String sLabel : TranslationVO.LABELS_ENTITY) {									
				translation.getLabels().put(sLabel, "");
			}
			tablemodel.getRows().add(translation);
		}
		for(LocaleInfo voLocale : LocaleDelegate.getInstance().getAllLocales(false)) {
			final String sLocaleLabel = voLocale.getLanguage(); 
			
			if(model.isEditMode()) {
				EntityMeta<?> voMeta = MetaProvider.getInstance().getEntity(model.getUID());
					
				String sResourceIdForLabel = voMeta.getLocaleResourceIdForLabel();
				String sResourceIdForMenuPath = voMeta.getLocaleResourceIdForMenuPath();
				String sResourceIdForTreeView = voMeta.getLocaleResourceIdForTreeView();
				String sResourceIdForViewDescription = voMeta.getLocaleResourceIdForTreeViewDescription();				

				String sLabel = LocaleDelegate.getInstance().getResourceByStringId(voLocale, sResourceIdForLabel);				
				String sMenuPath = LocaleDelegate.getInstance().getResourceByStringId(voLocale, sResourceIdForMenuPath);
				String sTreeview = LocaleDelegate.getInstance().getResourceByStringId(voLocale, sResourceIdForTreeView);
				String sTooltip = LocaleDelegate.getInstance().getResourceByStringId(voLocale, sResourceIdForViewDescription);
				if(tablemodel.getTranslationByName(sLocaleLabel) != null) {
					tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[0], sLabel);
					tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[1], sMenuPath);
					tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[2], sTreeview);
					tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[3], sTooltip);
				}
			}
			else {			
				if(tablemodel.getTranslationByName(sLocaleLabel) != null) {
					tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[0], model.getLabelSingular());
					tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[1], model.getMenuPath());
					tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[2], model.getNodeLabel());
					tablemodel.getTranslationByName(sLocaleLabel).getLabels().put(TranslationVO.LABELS_ENTITY[3], model.getNodeTooltip());
				}
			}
		}
		
		return tablemodel.getRows(); 
	}

	@Override
	public void prepare() {
		super.prepare();

		if (model.getParentFrame() != null) {
			model.getParentFrame().setTitle(SpringLocaleDelegate.getInstance().getMsg("wizard.show.17"));
		}

		DocumentListener li[] = tfName.getListeners(DocumentListener.class);
		for(DocumentListener l : li)
			tfName.getDocument().removeDocumentListener(l);

		if(toEdit != null) {
			//cmbEntity.setSelectedItem(EntityUtils.wrapMetaData(toEdit));
			setSelectedEntity(toEdit);
			return;
		}
		if(model.getEntityName() != null && !model.isEditMode()) {
			tfName.setText(model.getEntityName());
		}

		for(DocumentListener l : li)
			tfName.getDocument().addDocumentListener(l);
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (tfFilterEntityName.isEnabled()) {
					tfFilterEntityName.requestFocusInWindow();
				} else {
					if (tfName.isEnabled()) {
						tfName.requestFocusInWindow();
					}
				}
			}
		});
	}

	private void loadTreeView() {
		MasterDataDelegate.getInstance().loadTreeView(model.getUID(), model.getTreeView());
	}

	private void loadProcesses(UID entityUID) {
		model.setProcesses(MasterDataDelegate.getInstance().<UID, UID>getDependentDataCollection(E.PROCESS.getUID(), E.PROCESS.module.getUID(), null, entityUID));
	}

	private void loadEntityMenus(UID entityUID) {
		final Collection<EntityObjectVO<UID>> entityMenus = 
				MasterDataDelegate.getInstance().<UID, UID>getDependentDataCollection(E.ENTITYMENU.getUID(), E.ENTITYMENU.entity.getUID(), null, entityUID);
		for (EntityObjectVO<UID> menu : entityMenus) {
			// saving: org.nuclos.client.wizard.steps.NuclosEntitySQLLayoutStep.createOrModifyEntity()
			for (LocaleInfo li : LocaleDelegate.getInstance().getAllLocales(false)) {
				menu.setFieldValue(new UID("menupath_" + li.getTag()), SpringLocaleDelegate.getInstance().getResourceById(
						li, menu.getFieldValue(E.ENTITYMENU.menupath)));
			}
		}
		model.setEntityMenus(entityMenus);
	}
	
	private static class EntityTableModel extends AbstractTableModel {
		
		public static final int COL_ENTITY_NAME = 0;
		public static final int COL_NUCLET = 1;
		public static final int COL_MENUPATH = 2;
		public static final int COL_STATEMODEL = 3;
		public static final int COL_VIRTUAL = 4;
		public static final int COL_PROXY = 5;
		public static final int COL_GENERIC = 6;
		public static final int COL_DBO = 7;
		public static final int COL_MANDATOR_LEVEL = 8;

		private final List<EntityMeta<?>> lstEntity;
		
		private final Map<UID, EntityObjectVO<UID>> mapNuclets;
		
		private final Map<UID, MandatorLevelVO> mapMandatorLevels;
		
		private final Map<UID, String> mapMenupaths;
		
		public EntityTableModel(List<EntityMeta<?>> lstEntity, List<EntityObjectVO<UID>> nuclets, List<MandatorLevelVO> lstMandatorLevels) {
			super();
			this.lstEntity = lstEntity;
			
			Map<UID, String> mapMenupaths = new HashMap<>();
			Set<UID> setNucletsWithEntities = new HashSet<>();
			for (EntityMeta<?> eMeta : lstEntity) {
				if (eMeta.getNuclet() != null) {
					setNucletsWithEntities.add(eMeta.getNuclet());
				}
				String localeResourceId = eMeta.getLocaleResourceIdForMenuPath();
				if (!StringUtils.isNullOrEmpty(localeResourceId)) {
					String sMenupath = SpringLocaleDelegate.getInstance().getMsg(localeResourceId);
					mapMenupaths.put(eMeta.getUID(), sMenupath);
				}
			}
			this.mapMenupaths = Collections.unmodifiableMap(mapMenupaths);
			
			Map<UID, EntityObjectVO<UID>> mapNuclets = new HashMap<>();
			for (EntityObjectVO<UID> nucletVO : nuclets) {
				if (setNucletsWithEntities.contains(nucletVO.getPrimaryKey())) {
					mapNuclets.put(nucletVO.getPrimaryKey(), nucletVO);
				}
			}
			this.mapNuclets = Collections.unmodifiableMap(mapNuclets);
			
			Map<UID, MandatorLevelVO> mapMandatorLevels = new HashMap<>();
			for (MandatorLevelVO level : lstMandatorLevels) {
				mapMandatorLevels.put(level.getUID(), level);
			}
			this.mapMandatorLevels = Collections.unmodifiableMap(mapMandatorLevels);
		}

		public String getNuclet(EntityMeta<?> eMeta) {
			if (eMeta.getNuclet() != null) {
				EntityObjectVO<UID> nucletVO = mapNuclets.get(eMeta.getNuclet());
				return nucletVO.getFieldValue(E.NUCLET.packagefield);
			}
			return null;
		}

		public String[] getNuclets() {
			Set<String> setResult = new HashSet<>();
			for (EntityObjectVO<UID> nucletVO : mapNuclets.values()) {
				String sPackage = nucletVO.getFieldValue(E.NUCLET.packagefield);
				setResult.add(sPackage);
			}
			List<String> result = CollectionUtils.sorted(setResult);
			return result.toArray(new String[]{});
		}

		public EntityMeta<?> getEntityAt(int iRow) {
			return lstEntity.get(iRow);
		}

		@Override
		public int getRowCount() {
			return lstEntity.size();
		}

		@Override
		public int getColumnCount() {
			if (mapMandatorLevels.isEmpty()) {
				return 8;
			} else {
				return 9;
			}
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			final EntityMeta<?> eMeta = lstEntity.get(rowIndex);
			final boolean isVirtual = !StringUtils.looksEmpty(eMeta.getVirtualEntity());
			switch (columnIndex) {
			case COL_ENTITY_NAME:
				return eMeta.getEntityName();
			case COL_NUCLET:
				return getNuclet(eMeta);
			case COL_MENUPATH:
				return mapMenupaths.get(eMeta.getUID());
			case COL_STATEMODEL:
				return eMeta.isStateModel();
			case COL_VIRTUAL:
				return isVirtual;
			case COL_PROXY:
				return eMeta.isProxy();
			case COL_GENERIC:
				return eMeta.isGeneric();
			case COL_DBO:
				if (isVirtual) {
					return eMeta.getVirtualEntity();
				} else {
					if (eMeta.isProxy()) {
						return null;
					} else {
						return eMeta.getDbTable();
					}
				}
			case COL_MANDATOR_LEVEL:
				if (eMeta.getMandatorLevel() != null) {					
					MandatorLevelVO level = mapMandatorLevels.get(eMeta.getMandatorLevel());
					if (level == null) {
						return "null";
					} else {
						return level.toString();
					}
				} else {
					return null;
				}
			default:
				return null;
			}
		}
		
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			switch (columnIndex) {
			case COL_ENTITY_NAME:
				return String.class;
			case COL_NUCLET:
				return String.class;
			case COL_MENUPATH:
				return String.class;
			case COL_STATEMODEL:
				return Boolean.class;
			case COL_VIRTUAL:
				return Boolean.class;
			case COL_PROXY:
				return Boolean.class;
			case COL_GENERIC:
				return Boolean.class;
			case COL_DBO:
				return String.class;
			case COL_MANDATOR_LEVEL:
				return String.class;
			default:
				return null;
			}
		}
		
		@Override
		public String getColumnName(int columnIndex) {
			switch (columnIndex) {
			case COL_ENTITY_NAME:
				return SpringLocaleDelegate.getInstance().getMsg("wizard.step.entityname.name");
			case COL_NUCLET:
				return SpringLocaleDelegate.getInstance().getMsg("wizard.step.entityname.nuclet");
			case COL_MENUPATH:
				return SpringLocaleDelegate.getInstance().getMsg("wizard.step.entityname.menupath");
			case COL_STATEMODEL:
				return SpringLocaleDelegate.getInstance().getMsg("wizard.step.entityname.statemodel") + "?";
			case COL_VIRTUAL:
				return SpringLocaleDelegate.getInstance().getMsg("wizard.step.entityname.virtual") + "?";
			case COL_PROXY:
				return SpringLocaleDelegate.getInstance().getMsg("wizard.step.entityname.proxy") + "?";
			case COL_GENERIC:
				return SpringLocaleDelegate.getInstance().getMsg("wizard.step.entityname.generic") + "?";
			case COL_DBO:
				return SpringLocaleDelegate.getInstance().getMsg("wizard.step.entityname.dbo");
			case COL_MANDATOR_LEVEL:
				return SpringLocaleDelegate.getInstance().getMsg("nuclos.entity.mandatorLevel.label");
			default:
				return null;
			}
		}
		
	}

	private void fillEntityCombobox() {
		final List<EntityMeta<?>> lstMasterdata = new ArrayList<EntityMeta<?>>(colMasterdata);
		
		Collections.sort(lstMasterdata, EntityUtils.getMetaComparator(EntityMeta.class));
		
		List<EntityMeta<?>> lstSelectableEntities = new ArrayList<>();
		for(EntityMeta<?> vo : lstMasterdata) {
			if(E.isNuclosEntity(vo.getUID()) || vo.isDatasourceBased() || vo.isFieldValueEntity())
				continue;
			lstSelectableEntities.add(vo);
		}
		tblmdlEntity = new EntityTableModel(lstSelectableEntities, nuclets, mandatorLevels);
		tblEntity.setModel(tblmdlEntity);
	}
	
	public static Attribute wrapEntityMetaFieldVO(FieldMeta<?> fieldVO, boolean bSystem) throws CommonFinderException,
        CommonPermissionException {
		
		final Attribute attr = new Attribute(bSystem);
		attr.setUID(fieldVO.getUID());
		if(attr.getUID() == null)
			attr.setUID(new UID());
        attr.setDistinct(fieldVO.isUnique());
        attr.setLogBook(fieldVO.isLogBookTracking());
        attr.setMandatory(!fieldVO.isNullable());
        attr.setHidden(fieldVO.isHidden());
        attr.setIndexed(Boolean.TRUE.equals(fieldVO.isIndexed()));
        attr.setLabel(SpringLocaleDelegate.getInstance().getResource(
        		fieldVO.getLocaleResourceIdForLabel(), fieldVO.getFallbackLabel()));
        attr.setDescription(SpringLocaleDelegate.getInstance().getResource(fieldVO.getLocaleResourceIdForDescription(), ""));
        attr.setLabelResource(fieldVO.getLocaleResourceIdForLabel());
        attr.setDescriptionResource(fieldVO.getLocaleResourceIdForDescription());

        attr.setInternalName(StringUtils.getModuleFieldName(fieldVO.getFieldName()));
        attr.setOldInternalName(StringUtils.getModuleFieldName(fieldVO.getFieldName()));
        attr.setDbName(fieldVO.getDbColumn());
        attr.setValueListProvider(fieldVO.isSearchable());
        attr.setDefaultValue(fieldVO.getDefaultValue());
        if(fieldVO.getDefaultForeignId() != null) {
        	attr.setIdDefaultValue(new DefaultValue(fieldVO.getDefaultForeignId(), fieldVO.getDefaultValue()));
        }
        attr.setModifiable(fieldVO.isModifiable());
        setAttributeGroup(attr, fieldVO);
        attr.setCalcFunction(fieldVO.getCalcFunction());
        attr.setCalcAttributeDS(fieldVO.getCalcAttributeDS());
        attr.setCalcAttributeParamValues(fieldVO.getCalcAttributeParamValues());
        attr.setCalcAttributeAllowCustomization(fieldVO.isCalcAttributeAllowCustomization());
        attr.setCalcOndemand(fieldVO.isCalcOndemand());
        attr.setOutputFormat(fieldVO.getFormatOutput());
        attr.setInputValidation(fieldVO.getFormatInput());
        attr.setCalculationScript(fieldVO.getCalculationScript());
        attr.setBackgroundColorScript(fieldVO.getBackgroundColorScript());
        attr.setIsLocalized(fieldVO.isLocalized());
        attr.setComment(fieldVO.getComment());
        
        final UID uidForeignEntity = fieldVO.getForeignEntity();
        final UID uidAutoNubmerEntity = fieldVO.getAutonumberEntity();
        if (null != uidAutoNubmerEntity) {
        	attr.setAutoNumberMetaVO(MetaProvider.getInstance().getEntity(uidAutoNubmerEntity));
        }
        if (fieldVO.getJavaClass() == GenericObjectDocumentFile.class) {
        	if (uidForeignEntity == null || fieldVO.getForeignEntityField() == null || !fieldVO.getDbColumn().toUpperCase().startsWith("STRVALUE_")) {
        		attr.fixedProblem();
        		attr.setMetaVO(E.DOCUMENTFILE);
        		attr.setField(E.stringify(E.DOCUMENTFILE.filename));
        		if (!fieldVO.getDbColumn().toUpperCase().startsWith("STRVALUE_")) {
        			attr.setDbName("STRVALUE_" + fieldVO.getDbColumn().replaceFirst("^STRUID_", ""));
        		}
        	} else {
        		EntityMeta<?> voForeignEntity = MetaProvider.getInstance().getEntity(uidForeignEntity);
        		attr.setMetaVO(voForeignEntity);
        		attr.setField(fieldVO.getForeignEntityField());
        	}
        	attr.setOnDeleteCascade(fieldVO.isOnDeleteCascade());
    		attr.setSearchField(fieldVO.getSearchField());
    		attr.setDatatyp(NuclosWizardUtils.getDataTyp(fieldVO.getDataType(), fieldVO.getDefaultComponentType(), 
	        		fieldVO.getScale(), fieldVO.getPrecision(), fieldVO.getFormatInput(),
	        		fieldVO.getFormatOutput()));
        }
        else if(uidForeignEntity != null) {
        	EntityMeta<?> voForeignEntity = MetaProvider.getInstance().getEntity(uidForeignEntity);
        	if(voForeignEntity.isFieldValueEntity()) {
        		attr.setMetaVO(voForeignEntity);
        		attr.setField(fieldVO.getForeignEntityField());
        		attr.setSearchField(fieldVO.getSearchField());
        		attr.setDatatyp(DataTyp.getDefaultStringTyp());
        		loadValueList(attr);
        	}
        	else {
        		attr.setForeignIntegrationPoint(fieldVO.getForeignIntegrationPoint());
        		attr.setMetaVO(voForeignEntity);

        		attr.setOnDeleteCascade(fieldVO.isOnDeleteCascade());
        		attr.setField(fieldVO.getForeignEntityField());
        		attr.setSearchField(fieldVO.getSearchField());
        		attr.setDatatyp(DataTyp.getReferenzTyp());
        		if(!Modules.getInstance().isModule(uidForeignEntity) && fieldVO.getForeignEntityField() != null) {
        			String sForeignField = fieldVO.getForeignEntityField();
        			if(sForeignField.indexOf("uid{") >= 0) {
        				attr.setDatatyp(DataTyp.getReferenzTyp());
        			}
        			else {
        				FieldMeta<?> voField = null;
						try {
							voField = MetaProvider.getInstance().getEntityField(UID.parseUID(sForeignField));
						} catch (Exception ex) {
							// no field
						}
						if (voField != null) {
							attr.getDatatyp().setJavaType(voField.getDataType());
							if (voField.getPrecision() != null)
								attr.getDatatyp().setPrecision(voField.getPrecision());
							if (voField.getScale() != null)
								attr.getDatatyp().setScale(voField.getScale());
							if (voField.getFormatInput() != null)
								attr.getDatatyp().setInputFormat(voField.getFormatInput());
							if (voField.getFormatOutput() != null)
								attr.getDatatyp().setOutputFormat(voField.getFormatOutput());
						} else {
							attr.getDatatyp().setJavaType(String.class.getCanonicalName());
							attr.getDatatyp().setScale(255);
						}
        			}
        		}
        	}
        }
        else if (fieldVO.getForeignIntegrationPoint() != null) {
        	// point is not integrated
			attr.setForeignIntegrationPoint(fieldVO.getForeignIntegrationPoint());

			attr.setOnDeleteCascade(fieldVO.isOnDeleteCascade());
			attr.setField(fieldVO.getForeignEntityField());
			attr.setSearchField(fieldVO.getSearchField());
			attr.setDatatyp(DataTyp.getReferenzTyp());
		}
        else {
            final UID uidLookupEntity = fieldVO.getLookupEntity();
            if(uidLookupEntity != null) {
            	final EntityMeta<?> voLookupEntity = MetaProvider.getInstance().getEntity(uidLookupEntity);
            	if(voLookupEntity.isFieldValueEntity()) {
            		attr.setMetaVO(voLookupEntity);
            		attr.setField(fieldVO.getLookupEntityField());
            		attr.setSearchField(fieldVO.getSearchField());
            		attr.setDatatyp(DataTyp.getDefaultStringTyp());
            		loadValueList(attr);
            	}
            	else {
            		attr.setLookupMetaVO(voLookupEntity);

            		attr.setOnDeleteCascade(fieldVO.isOnDeleteCascade());
            		attr.setField(fieldVO.getLookupEntityField());
            		attr.setSearchField(fieldVO.getSearchField());
            		attr.setDatatyp(DataTyp.getLookupTyp());
            		if(!Modules.getInstance().isModule(uidLookupEntity) && fieldVO.getLookupEntityField() != null) {
            			String sLookupField = fieldVO.getLookupEntityField();
            			if(sLookupField.indexOf("uid{") >= 0) {
            				attr.setDatatyp(DataTyp.getLookupTyp());
            			}
            			else {
            				final FieldMeta<?> voField =  MetaProvider.getInstance().getEntityField(UID.parseUID(sLookupField));

            				attr.getDatatyp().setJavaType(voField.getDataType());
            				if(voField.getPrecision() != null)
            					attr.getDatatyp().setPrecision(voField.getPrecision());
            				if(voField.getScale() != null)
            					attr.getDatatyp().setScale(voField.getScale());
            				if(voField.getFormatInput() != null)
            					attr.getDatatyp().setInputFormat(voField.getFormatInput());
            				if(voField.getFormatOutput() != null)
            					attr.getDatatyp().setOutputFormat(voField.getFormatOutput());
            			}

            		}
            	}
            } 
            else {
	        	attr.setDatatyp(NuclosWizardUtils.getDataTyp(fieldVO.getDataType(), fieldVO.getDefaultComponentType(), 
	        		fieldVO.getScale(), fieldVO.getPrecision(), fieldVO.getFormatInput(),
	        		fieldVO.getFormatOutput()));
            }
        }

        try {
        	setDefaultMandatoryValue(fieldVO, attr);
        }
        catch (Exception e) {
			// value can't be set, don't worry
		}

        attr.setDescription(SpringLocaleDelegate.getInstance().getResource(fieldVO.getLocaleResourceIdForDescription(), ""));
        attr.setOrder(fieldVO.getOrder() != null ? fieldVO.getOrder() : 0);
        attr.setSnapshot();
        return attr;
    }

	private static void setDefaultMandatoryValue(FieldMeta<?> fieldVO,	Attribute attr) throws Exception {
		String sJavaType = attr.getDatatyp().getJavaType();
		if(sJavaType.equals("java.lang.Integer")) {
			attr.setMandatoryValue(new Integer(fieldVO.getDefaultMandatory()));
		}
		else if(sJavaType.equals("java.lang.Double")) {
			attr.setMandatoryValue(new Double(fieldVO.getDefaultMandatory().replace(',', '.')));
		}
		else if(sJavaType.equals("java.util.Date")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
			Date date = sdf.parse(fieldVO.getDefaultMandatory());
			attr.setMandatoryValue(date);
		}
		else if(sJavaType.equals("java.lang.Boolean")){
			if(ObjectUtils.equals(fieldVO.getDefaultMandatory(),"true")){
				attr.setMandatoryValue(Boolean.TRUE);
			}
			else {
				attr.setMandatoryValue(Boolean.FALSE);
			}
		}
		else {
			attr.setMandatoryValue(fieldVO.getDefaultMandatory());
		}
	}
	
	/**
	 * Name is unique
	 * @return
	 */
	private String[] getMandatorLevelNames() {
		List<String> lst = new ArrayList<>();
		for (MandatorLevelVO level : mandatorLevels) {
			lst.add(level.getName());
		}
		return lst.toArray(new String[]{});
	}
	
	private String[] getMenupaths() {
		List<String> lst = new ArrayList<>();
		for(String sMenu : NuclosWizardUtils.getExistingMenuPaths()) {
			lst.add(sMenu);
		}
		return lst.toArray(new String[]{});
	}
	
	private String[] getNuclets() {
		List<String> lst = new ArrayList<>();
		for(EntityObjectVO<UID> nucletVO : nuclets) {
			lst.add(nucletVO.getFieldValue(E.NUCLET.packagefield));
		}
		Collections.sort(lst);
		return lst.toArray(new String[]{});
	}
	
}
