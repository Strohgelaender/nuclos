//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.log4j.Logger;
import org.nuclos.client.autonumber.AutonumberUiUtils;
import org.nuclos.client.i18n.language.data.DataLanguageDelegate;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.ui.DateChooser;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.wizard.DataTypeCollectController;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.DataTyp;
import org.nuclos.client.wizard.model.DataTyp.JavaTypeEnum;
import org.nuclos.client.wizard.util.DoubleFormatDocument;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.client.wizard.util.NumericFormatDocument;
import org.nuclos.common.E;
import org.nuclos.common.NuclosDateTime;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityAttributePropertiesStep extends NuclosEntityAttributeAbstractStep implements ChangeListener, ActionListener {

	private static final Logger LOG = Logger.getLogger(NuclosEntityAttributeCommonPropertiesStep.class);

	private final static JavaTypeEnum[] JAVA_TYPES = {
		JavaTypeEnum.String,
		JavaTypeEnum.Integer,
		JavaTypeEnum.Double,
		JavaTypeEnum.Date,
		JavaTypeEnum.NuclosDateTime,
		JavaTypeEnum.Boolean,
		JavaTypeEnum.GenericObjectDocumentFile,
		JavaTypeEnum.ByteArray,
		JavaTypeEnum.NuclosPassword,
		JavaTypeEnum.NuclosImage
	};

	private static final String ACTIONCOMMAND_NAME = "actionName";
	private static final String ACTIONCOMMAND_DESCRIPTION = "actionDescription";
	private static final String ACTIONCOMMAND_DATATYPE = "actionDatatype";
	private static final String ACTIONCOMMAND_JAVATYPE = "actionJavatype";
	private static final String ACTIONCOMMAND_FIELDWIDTH = "actionFieldwidth";
	private static final String ACTIONCOMMAND_FIELDPRECISION = "actionFieldprecision";
	private static final String ACTIONCOMMAND_OUTPUTFORMAT = "actionOutputformat";
	private static final String ACTIONCOMMAND_REFERENCE = "actionReference";
	private static final String ACTIONCOMMAND_VALUELIST = "actionValuelist";
	private static final String ACTIONCOMMAND_INTERNATIONAL = "actionInternational";
	private static final String ACTIONCOMMAND_MINVALUE = "actionMinValue";
	private static final String ACTIONCOMMAND_MAXVALUE = "actionMaxValue";
	private static final String ACTIONCOMMAND_MINVALUEDATE = "actionMinValueDate";
	private static final String ACTIONCOMMAND_MAXVALUEDATE = "actionMaxValueDate";
	
	//

	private JLabel lbName;
	private JTextField tfName;
	private JLabel lbDesc;
	private JTextField tfDesc;
	private JLabel lbDatatyp;
	private JComboBox cbxDatatyp;
	private JLabel lbJavatype;
	private JComboBox cbxJavatype;
	private JLabel lbFieldWidth;
	private JTextField tfFieldWidth;
	private JLabel lbFieldPrecision;
	private JTextField tfFieldPrecision;
	private JLabel lbOutputFormat;
	private JTextField tfOutputFormat;
	private JLabel lbReference;
	private JCheckBox cbxReference;
	private JLabel lbValueList;
	private JCheckBox cbxValueList;
	private JLabel lbInfo;
	private JCheckBox cbxIsLocalized;
	private JLabel lbIsLocalized;	
	private JLabel lbMinValue;
	private JLabel lbMaxValue;
	private JTextField tfMinValue;
	private JTextField tfMaxValue;
	private JLabel lbComment;
	private JTextArea txtComment;
	private JScrollPane scpComment;
	
	private DateChooser datMinValue;
	private DateChooser datMaxValue;

	private boolean columnTypeChangeAllowed;

	private JButton btnDataTyp;

	private boolean blnDescModified;

	private NuclosEntityWizardStaticModel parentWizardModel;

	private JComponent parent;

	private String customtypename;

	private DataTyp customtype;

	public NuclosEntityAttributePropertiesStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	protected void initComponents() {
		customtypename = SpringLocaleDelegate.getInstance().getText("wizard.datatype.individual");
		customtype = new DataTyp(customtypename, null, null, null, null, null, null, null);

		lbName = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.1", "Anzeigename"));
		tfName = new JTextField();
		tfName.setActionCommand(ACTIONCOMMAND_NAME);
		tfName.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.1", "Anzeigename"));
		tfName.setEditable(false);
		
		lbDesc = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.2", "Beschreibung"));
		tfDesc = new JTextField();
		tfDesc.setActionCommand(ACTIONCOMMAND_DESCRIPTION);
		tfDesc.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.2", "Beschreibung"));
		tfDesc.setEditable(false);

		lbDatatyp = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.3", "Datentyp"));
		cbxDatatyp = new JComboBox();
		cbxDatatyp.setActionCommand(ACTIONCOMMAND_DATATYPE);
		cbxDatatyp.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.3", "Datentyp"));
		cbxDatatyp.addActionListener(this);

		lbJavatype = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.javatype", "Javatyp"));
		List<JavaTypeEnum> javaTypes = CollectionUtils.asList(JAVA_TYPES);
		Collections.sort(javaTypes, new Comparator<JavaTypeEnum>() {
			@Override
			public int compare(JavaTypeEnum o1, JavaTypeEnum o2) {
				return o1.toString().compareToIgnoreCase(o2.toString());
			}
		});
		cbxJavatype = new JComboBox(javaTypes.toArray());
		cbxJavatype.setActionCommand(ACTIONCOMMAND_JAVATYPE);
		cbxJavatype.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.javatype", "Javatyp"));
		cbxJavatype.addActionListener(this);

		lbFieldWidth = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.datatype.3", "Feldbreite"));
		tfFieldWidth = new JTextField();
		tfFieldWidth.setActionCommand(ACTIONCOMMAND_FIELDWIDTH);
		tfFieldWidth.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.datatype.tooltip.3", "Maximale Anzahl von Zeichen im Feld"));
		tfFieldWidth.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfFieldWidth.addActionListener(this);

		lbFieldPrecision = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.datatype.4", "Nachkommastellen"));
		tfFieldPrecision = new JTextField();
		tfFieldPrecision.setActionCommand(ACTIONCOMMAND_FIELDPRECISION);
		tfFieldPrecision.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.datatype.tooltip.4", "Anzahl an Nachkommastellen"));
		tfFieldPrecision.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfFieldPrecision.addActionListener(this);

		lbOutputFormat = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.datatype.6", "Ausgabeformat"));
		tfOutputFormat = new JTextField();
		tfOutputFormat.setActionCommand(ACTIONCOMMAND_OUTPUTFORMAT);
		tfOutputFormat.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.datatype.tooltip.6", "Formatierung der Ausgabe"));
		tfOutputFormat.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfOutputFormat.addActionListener(this);

		lbMinValue = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.21", "Mindestwert"));
		tfMinValue = new JTextField();
		tfMinValue.setActionCommand(ACTIONCOMMAND_MINVALUE);
		tfMinValue.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfMinValue.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.23", "Bestimmen Sie einen Mindeswert, der in der Eingabemaske validiert wird."));
		tfMinValue.setEnabled(false);
		tfMinValue.addActionListener(this);

		lbMaxValue = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.22", "Maximalwert"));
		tfMaxValue = new JTextField();
		tfMaxValue.setActionCommand(ACTIONCOMMAND_MAXVALUE);
		tfMaxValue.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfMaxValue.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.24", "Bestimmen Sie einen Maximalwert, der in der Eingabemaske validiert wird."));
		tfMaxValue.setEnabled(false);
		tfMaxValue.addActionListener(this);

		final LabeledComponentSupport support = new LabeledComponentSupport();

		datMinValue = new DateChooser(support);
		datMinValue.setActionCommand(ACTIONCOMMAND_MINVALUEDATE);
		datMinValue.setVisible(false);
		datMinValue.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.23", "Bestimmen Sie einen Mindeswert, der in der Eingabemaske validiert wird."));
		datMinValue.addActionListener(this);

		datMaxValue = new DateChooser(support);
		datMaxValue.setActionCommand(ACTIONCOMMAND_MAXVALUEDATE);
		datMaxValue.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.24", "Bestimmen Sie einen Maximalwert, der in der Eingabemaske validiert wird."));
		datMaxValue.setVisible(false);
		datMaxValue.addActionListener(this);

		btnDataTyp = new JButton("...");
		btnDataTyp.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.4", "Datentypen konfigurieren"));

		lbReference = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.reference", "Referenzfeld") + "?");
		cbxReference = new JCheckBox();
		cbxReference.setActionCommand(ACTIONCOMMAND_REFERENCE);
		cbxReference.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.reference", "Ein Referenzfeld erm\u00f6glicht die Auswahl von Werten aus einer anderen Entit\u00e4t."));
		cbxReference.addActionListener(this);

		lbValueList = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.77", "Werteliste"));
		cbxValueList = new JCheckBox();
		cbxValueList.setActionCommand(ACTIONCOMMAND_VALUELIST);
		cbxValueList.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.22", "In einer Werteliste definieren Sie fest definierte Werte"));
		cbxValueList.addActionListener(this);

		lbIsLocalized= new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.78", "Mehrsprachigkeit?"));
		cbxIsLocalized = new JCheckBox();
		cbxIsLocalized.setActionCommand(ACTIONCOMMAND_INTERNATIONAL);
		cbxIsLocalized.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.78", "Definieren Sie hier, ob der Inhalt dieses Feldes mehrsprachig angelegt werden soll"));
		cbxIsLocalized.addActionListener(this);
		
		lbComment= new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.80", "Kommentar"));
		txtComment = new JTextArea();
		txtComment.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributeproperties.tooltip.81", "Mit der Angabe eines Kommentares können zusätzliche Informationen über ein Attribut eines BusinessObjekt hinterlegt werden."));
		scpComment = new JScrollPane(txtComment,  
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		lbInfo = new JLabel();

		TableLayoutBuilder tbllay = new TableLayoutBuilder(this)
				.columns(TableLayout.PREFERRED, 300, 30, TableLayout.FILL)
				.gaps(5, 3);

		tbllay.newRow(20).add(lbName).add(tfName).addFullSpan(lbInfo);
		tbllay.newRow(20).add(lbDesc).add(tfDesc);
		tbllay.newRow(20).add(lbDatatyp).add(cbxDatatyp).add(btnDataTyp);
		tbllay.newRow(20).add(lbJavatype).add(cbxJavatype);
		tbllay.newRow(20).add(lbFieldWidth).add(tfFieldWidth);
		tbllay.newRow(20).add(lbFieldPrecision).add(tfFieldPrecision);
		tbllay.newRow(20).add(lbOutputFormat).add(tfOutputFormat);
		tbllay.newRow(20).add(lbReference).add(cbxReference);
		// valuelist is not used any more... Right?
		//tbllay.newRow(20).add(lbValueList).add(cbxValueList);
		tbllay.newRow(20).add(lbMinValue).add(tfMinValue);
		tbllay.newRow(20).add(lbMaxValue).add(tfMaxValue);
		tbllay.newRow(20).add(lbIsLocalized).add(cbxIsLocalized);
		tbllay.newRow(80).add(lbComment, 1, TableLayout.LEFT, TableLayout.TOP).add(scpComment);
		lbComment.setBorder(BorderFactory.createEmptyBorder(3,1,0,0));
		this.add(datMinValue, tbllay.getTableLayout().getConstraints(tfMinValue));
		this.add(datMaxValue, tbllay.getTableLayout().getConstraints(tfMaxValue));
		
		tfName.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			protected void doSomeWork(DocumentEvent e) {
				if(!blnDescModified) {
					tfDesc.setText(tfName.getText());
				}
				int size = e.getDocument().getLength();
				if(size > 0) {
					NuclosEntityAttributePropertiesStep.this.setComplete(true);
				}
				else  {
					NuclosEntityAttributePropertiesStep.this.setComplete(false);
				}
			}
		});

		tfDesc.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			protected void doSomeWork(DocumentEvent e) {
				if (tfDesc.hasFocus()) {
					blnDescModified = true;
				}
			}
		});

		btnDataTyp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
				final MainFrameTab tabDataType = new MainFrameTab(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.23", "Datentypen verwalten"));
				final DataTypeCollectController dtcc = factory.newDataTypeCollectController(tabDataType);
				dtcc.addChangeListener(NuclosEntityAttributePropertiesStep.this);
				parent.add(tabDataType);

				try {
					dtcc.runNew();
				} catch(CommonBusinessException e1) {
					Errors.getInstance().showExceptionDialog(parent, e1);
				}
			}
		});
	}

	@Override
	public void prepare() {
		super.prepare();
		
		cbxDatatyp.removeActionListener(this);
		cbxJavatype.removeActionListener(this);
		
		if (model.isEditMode() || parentWizardModel.isVirtual()) {
			blnDescModified = model.isEditMode();
			if (StringUtils.looksEmpty(model.getAttribute().getLabel())) {
				// NUCLOS-6502
				tfName.setText(model.getAttribute().getInternalName());
			} else {
				tfName.setText(model.getAttribute().getLabel());
			}
			tfDesc.setText(model.getAttribute().getDescription());
			tfFieldWidth.setText(model.getAttribute().getDatatyp().getScale() == null ? null : String.valueOf(model.getAttribute().getDatatyp().getScale()));
			tfFieldPrecision.setText(model.getAttribute().getDatatyp().getPrecision() == null ? null : String.valueOf(model.getAttribute().getDatatyp().getPrecision()));
			tfOutputFormat.setText(model.getAttribute().getDatatyp().getOutputFormat());
			cbxReference.setSelected(model.getAttribute().getMetaVO() != null || model.getAttribute().getForeignIntegrationPoint() != null);
			cbxValueList.setSelected(model.getAttribute().isValueList());
			cbxIsLocalized.setSelected(model.getAttribute().isLocalized());
			txtComment.setText(model.getAttribute().getComment());
			
			// setup change options for current attribute
			cbxDatatyp.removeAllItems();

			List<DataTyp> lstTypes = new ArrayList<DataTyp>();
			if (!columnTypeChangeAllowed || parentWizardModel.isVirtual()) {
				for(DataTyp type : DataTyp.getConvertibleTypes(model.getAttribute().getDatatyp(), parentWizardModel.isGeneric())) {
					lstTypes.add(type);
				}
			} else {
				for(DataTyp type : DataTyp.getAllDataTyps(parentWizardModel.isGeneric())) {
					lstTypes.add(type);
				}
			}

			DataTyp current = model.getAttribute().getDatatyp();
			customtype.setJavaType(current.getJavaType());
			customtype.setScale(current.getScale());
			customtype.setPrecision(current.getPrecision());
			customtype.setOutputFormat(current.getOutputFormat());
			customtype.setInputFormat(current.getOutputFormat());

			setInputValidation(current.getJavaType(), model.getAttribute().getInputValidation());

			lstTypes.add(customtype);
			//cbxDatatyp.addItem(customtype);
			Collections.sort(lstTypes, new Comparator<DataTyp>() {
				@Override
				public int compare(DataTyp o1, DataTyp o2) {
					return o1.getName().compareToIgnoreCase(o2.getName());
				}
			});
			for (DataTyp dataTyp : lstTypes) {
				cbxDatatyp.addItem(dataTyp);
			}
			
			cbxDatatyp.setSelectedItem(NuclosEntityAttributePropertiesStep.this.model.getAttribute().getDatatyp());
			cbxJavatype.setSelectedItem(JavaTypeEnum.fromClazz(customtype.getJavaType()));
			
			if (!columnTypeChangeAllowed || parentWizardModel.isVirtual()) {
				cbxJavatype.removeAllItems();
				cbxJavatype.addItem(JavaTypeEnum.fromClazz(model.getAttribute().getDatatyp().getJavaType()));
				if (!String.class.getName().equals(model.getAttribute().getDatatyp().getJavaType())) {
					cbxJavatype.addItem(JavaTypeEnum.fromClazz(String.class.getName()));
				}
			}
		}
		else {
			List<DataTyp> lstTypes = DataTyp.getAllDataTyps(parentWizardModel.isGeneric());
			lstTypes.add(customtype);
			Collections.sort(lstTypes, new Comparator<DataTyp>() {
				@Override
				public int compare(DataTyp o1, DataTyp o2) {
					return o1.getName().compareToIgnoreCase(o2.getName());
				}
			});
			for (DataTyp dataTyp : lstTypes) {
				cbxDatatyp.addItem(dataTyp);
			}
			if(model.getAttribute().getDatatyp() == null) {
				cbxDatatyp.setSelectedItem(DataTyp.getDefaultStringTyp());
			}
			else {
				cbxDatatyp.setSelectedItem(model.getAttribute().getDatatyp());
			}
		}
		// NUCLOS-1477
		handleAutoNumber();
		
		cbxDatatyp.addActionListener(this);
		cbxJavatype.addActionListener(this);

		updateState();
		
		boolean fieldWillBeAdded = this.model.getAttribute().getUID() == null;
		
		if (fieldWillBeAdded) {
			tfName.setEditable(true);
			tfName.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
			tfName.addActionListener(this);
			SwingUtilities.invokeLater(new Runnable() {			
				@Override
				public void run() {
					tfName.requestFocus();
				}
			});

			tfDesc.setEditable(true);
			tfDesc.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
			tfDesc.addActionListener(this);
			
			tfName.requestFocusInWindow();
			
		} else {
			if (this.model.getAttribute().isSystem()) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						btnDataTyp.setEnabled(false);
						cbxDatatyp.setEnabled(false);
						cbxJavatype.setEnabled(false);
						cbxReference.setEnabled(false);
						cbxValueList.setEnabled(false);
					}					
				});
				
			} else {
				SwingUtilities.invokeLater(new Runnable() {			
					@Override
					public void run() {
						cbxDatatyp.requestFocus();
					}
				});
				cbxDatatyp.requestFocusInWindow();				
			}
			
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tfName.requestFocusInWindow();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		LOG.debug(MessageFormat.format("actionPerformed({0})", String.valueOf(e)));
		updateState();
	}

	private void updateState() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				DataTyp selectedType = (DataTyp) cbxDatatyp.getSelectedItem();
				Boolean isCustomType = selectedType.getName().equals(customtypename);

				if(parentWizardModel.getAttributeModel() != null && !model.isEditMode()) {
					for(Attribute attr : parentWizardModel.getAttributeModel().getNucletAttributes()) {
						if(attr.getLabel().equals(tfName.getText())) {
							lbInfo.setForeground(Color.RED);
							lbInfo.setText(SpringLocaleDelegate.getInstance().getMessage(
									"wizard.step.attributeproperties.5", "Der Name wurde schon einmal vergeben"));
							lbInfo.setVisible(true);
							setComplete(false);
							break;
						}
						else {
							lbInfo.setVisible(false);
							if (tfName.getText() != null && tfName.getText().trim().length() > 0) {
								setComplete(true);
							}
						}
					}
				}

				cbxJavatype.setEnabled(isCustomType);
				tfOutputFormat.setEnabled(isCustomType);
				tfFieldWidth.setEnabled(isCustomType);
				tfFieldPrecision.setEnabled(isCustomType);
				if (model.isEditMode()) {
					cbxReference.setEnabled(isCustomType && columnTypeChangeAllowed && !selectedType.isValueListTyp());
					cbxValueList.setEnabled(DataTyp.getDefaultStringTyp().equals(selectedType) && columnTypeChangeAllowed && !selectedType.isRefenceTyp());
				}
				else {
					cbxReference.setEnabled(isCustomType && !selectedType.isValueListTyp());
					cbxValueList.setEnabled(DataTyp.getDefaultStringTyp().equals(selectedType) && !selectedType.isRefenceTyp());
				}


				if (!isCustomType) {
					cbxJavatype.getModel().setSelectedItem(JavaTypeEnum.fromClazz(selectedType.getJavaType()));
					
					if (cbxDatatyp.getModel().getSelectedItem() != null && cbxDatatyp.getModel().getSelectedItem() instanceof DataTyp) {
						DataTyp selTyp = (DataTyp) cbxDatatyp.getModel().getSelectedItem();
						if (selTyp.equals(DataTyp.getDefaultStringTyp()) || 
							selTyp.isMemoTyp() || selTyp.isLargTextObjectTyp()) {
							if (DataLanguageDelegate.getInstance().getSelectedLocales().size() > 0) {
								cbxIsLocalized.setEnabled(true);							
							} else {
								cbxIsLocalized.setEnabled(false);
								cbxIsLocalized.setSelected(false);
							}
						} else {
							cbxIsLocalized.setSelected(false);
							cbxIsLocalized.setEnabled(false);
						}						
					} else {
						cbxIsLocalized.setSelected(false);
						cbxIsLocalized.setEnabled(false);
					}
					
					tfOutputFormat.setText(selectedType.getOutputFormat());
					tfFieldWidth.setText(selectedType.getScale() != null ? selectedType.getScale().toString(): "");
					tfFieldPrecision.setText(selectedType.getPrecision() != null ? selectedType.getPrecision().toString(): "");
					cbxReference.setSelected(selectedType.isRefenceTyp());
					if (!DataTyp.getDefaultStringTyp().equals(selectedType) || selectedType.isRefenceTyp()) {
						cbxValueList.setSelected(false);
					}
					setInputValidation(selectedType.getJavaType(), model.getAttribute().getInputValidation());
				}
				else {
					JavaTypeEnum javaType = (JavaTypeEnum) cbxJavatype.getSelectedItem();
					String clazz = javaType != null ? javaType.getClazz() : null;
					if (clazz == null) {
						tfFieldWidth.setEnabled(false);
						tfFieldWidth.setText(null);
						tfFieldPrecision.setEnabled(false);
						tfFieldPrecision.setText(null);
						tfOutputFormat.setEnabled(false);
						
					} else if (Integer.class.getName().equals(clazz)) {
						tfFieldWidth.setEnabled(true);
						tfFieldPrecision.setEnabled(false);
						tfFieldPrecision.setText(null);
						tfOutputFormat.setEnabled(true);
					}
					else if (Double.class.getName().equals(clazz)) {
						tfFieldWidth.setEnabled(true);
						tfFieldPrecision.setEnabled(true);
						tfOutputFormat.setEnabled(true);
					}
					else if (Date.class.getName().equals(clazz)) {
						tfFieldWidth.setEnabled(false);
						tfFieldWidth.setText(null);
						tfFieldPrecision.setEnabled(false);
						tfFieldPrecision.setText(null);
						tfOutputFormat.setEnabled(true);
					}
					else if (NuclosDateTime.class.getName().equals(clazz)) {
						tfFieldWidth.setEnabled(false);
						tfFieldWidth.setText(null);
						tfFieldPrecision.setEnabled(false);
						tfFieldPrecision.setText(null);
						tfOutputFormat.setEnabled(true);
					}
					else if (Boolean.class.getName().equals(clazz)) {
						tfFieldWidth.setEnabled(false);
						tfFieldWidth.setText("1");
						tfFieldPrecision.setEnabled(false);
						tfFieldPrecision.setText(null);
						tfOutputFormat.setEnabled(false);
					}
					else {
						tfFieldWidth.setEnabled(true);
						tfFieldPrecision.setEnabled(false);
						tfFieldPrecision.setText(null);
						tfOutputFormat.setEnabled(false);
					}
				}
				
				enableMinMaxValue(cbxJavatype.getSelectedItem() == null ? null : ((JavaTypeEnum) cbxJavatype.getSelectedItem()).getClazz());
				if (!isCustomType) {
					setInputValidation(selectedType.getJavaType(), model.getAttribute().getInputValidation());
				} else {
					setInputValidation(model.getAttribute().getDatatyp().getJavaType(), model.getAttribute().getInputValidation());					
				}

				if (parentWizardModel.isVirtual()) {
					cbxJavatype.setEnabled(false);
					tfFieldWidth.setEnabled(false);
					tfFieldPrecision.setEnabled(false);
					cbxReference.setEnabled(false);
					cbxValueList.setEnabled(false);
					tfMinValue.setEnabled(false);
					tfMaxValue.setEnabled(false);
					datMinValue.setEnabled(false);
					datMaxValue.setEnabled(false);
				}

				if (parentWizardModel.isGeneric()) {
					cbxValueList.setEnabled(false);
					cbxIsLocalized.setEnabled(false);
				}
			}
		});
	}

	private void setInputValidation(String clazz, String input) {
		if(input != null && input.length() > 0) {
			String s[] = input.split(" ");
			if (s.length == 2) {
				if(clazz.equals("java.lang.Integer") || clazz.equals("java.lang.Double")) {
					tfMinValue.setText(s[0]);
					tfMaxValue.setText(s[1]);
				}
				else if(clazz.equals("java.util.Date")) {
					Date dateMin = new Date(Long.parseLong(s[0]));
					Date dateMax = new Date(Long.parseLong(s[1]));
					datMinValue.setDate(dateMin);
					datMaxValue.setDate(dateMax);
				}
			}
		}
    }

	@Override
	public void close() {
		lbName = null;
		tfName = null;
		lbDesc = null;
		tfDesc = null;
		lbDatatyp = null;
		cbxDatatyp = null;
		lbJavatype = null;
		cbxJavatype = null;
		lbFieldWidth = null;
		tfFieldWidth = null;
		lbFieldPrecision = null;
		tfFieldPrecision = null;
		lbOutputFormat = null;
		tfOutputFormat = null;
		lbReference = null;
		cbxReference = null;
		lbValueList = null;
		cbxValueList = null;
		lbInfo = null;
		cbxIsLocalized = null;
		lbIsLocalized = null;
		lbMinValue = null;
		lbMaxValue = null;
		tfMinValue = null;
		tfMaxValue = null;
		txtComment = null;
		datMinValue = null;
		datMaxValue = null;

		btnDataTyp = null;

		parentWizardModel = null;

		parent = null;

		customtypename = null;

		customtype = null;
		
		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		if (this.model.getAttribute().isSystem()) {
			applyStateFinal();
			return;
		}
		
		try {
			
	        final DataTyp selectedType = (DataTyp)cbxDatatyp.getSelectedItem();
	        
	        if (null == selectedType) {
	        	throw new CommonValidationException(SpringLocaleDelegate.getInstance().getMessage(
	        			"wizard.step.attributeproperties.31", "Der Datentyp muss gesetzt sein."));
	        }
	        // consider type "Individuell", it doesn't have any java datatype
			final String javaType = (null != selectedType.getJavaType()) ? selectedType.getJavaType() : cbxJavatype.getSelectedItem().toString();

	        if (null == javaType) {
	        	throw new CommonValidationException(SpringLocaleDelegate.getInstance().getMessage(
	        			"wizard.step.attributeproperties.30", "Der Java Datentyp muss gesetzt sein."));
	        }
			validateMinMaxValues(javaType);
        }
        catch(CommonValidationException e) {
	        Errors.getInstance().showExceptionDialog(this, e);
	        throw new InvalidStateException(e.toString());
        }
        catch (Exception e) {
        	Errors.getInstance().showExceptionDialog(this, e);
        	throw new InvalidStateException(e.toString());
		}
		model.setName(tfName.getText());
		model.setDesc(tfDesc.getText());
		
		DataTyp selectedType = (DataTyp) cbxDatatyp.getSelectedItem();
		boolean customType = selectedType.getName().equals(customtypename);
		if (customType) {
			DataTyp newType;
			try {
				newType = (DataTyp)selectedType.clone();
			} catch (CloneNotSupportedException e) {
				throw new InvalidStateException(e.getMessage());
			}
			newType.setJavaType(((JavaTypeEnum)cbxJavatype.getSelectedItem()).getClazz());
			try {
				newType.setScale((Integer)CollectableFieldFormat.getInstance(Integer.class).parse(null, tfFieldWidth.getText()));
			}
			catch (CollectableFieldFormatException ex) {
				throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.validation.scale", "", tfFieldWidth.getText()));
			}
			try {
				newType.setPrecision((Integer)CollectableFieldFormat.getInstance(Integer.class).parse(null, tfFieldPrecision.getText()));
			}
			catch (CollectableFieldFormatException ex) {
				throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.validation.precision", "", tfFieldPrecision.getText()));
			}

			try {
				if (!StringUtils.isNullOrEmpty(tfOutputFormat.getText())) {
					if (Integer.class.getName().equals(newType.getJavaType()) || Double.class.getName().equals(newType.getJavaType())) {
						new DecimalFormat(tfOutputFormat.getText());
					}
					else if (Date.class.getName().equals(newType.getJavaType())) {
						new SimpleDateFormat(tfOutputFormat.getText());
					}
					else {
						tfOutputFormat.setText(null);
					}
					newType.setOutputFormat(tfOutputFormat.getText());
				}
			}
			catch (IllegalArgumentException ex) {
				throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.validation.outputformat", "", tfOutputFormat.getText(), selectedType.getJavaType()));
			}
			if (model.isEditMode() && !DataTyp.isConversionSupported(selectedType, newType)) {
				throw new InvalidStateException(SpringLocaleDelegate.getInstance().getText("wizard.step.attributeproperties.validation.conversion"));
			}
			else {
				selectedType = newType;
			}
			model.setReferenzTyp(cbxReference.isSelected());
			model.setValueListTyp(cbxValueList.isSelected());
		}
		else {
		model.setReferenzTyp(selectedType.isRefenceTyp());
			model.setValueListTyp(cbxValueList.isSelected());
			model.setLookupTyp(selectedType.isLookupTyp());
			model.setAutoNumberType(selectedType.isAutoNumberTyp());
			model.setDocumentFileType(selectedType.isDocumentFileType());
		}
		model.getAttribute().setDatatyp(selectedType);
		model.getAttribute().setOutputFormat(selectedType.getOutputFormat());

		model.getAttribute().setIsLocalized(cbxIsLocalized.isSelected());
		model.getAttribute().setComment(txtComment.getText());
		
		applyStateFinal();
	}
	
	private void applyStateFinal() throws InvalidStateException {
		
		if(!this.model.isValueListType() && !this.model.isReferenceType() && !this.model.isLookupType() && !this.model.isAutoNumberType()) {
			model.getAttribute().setMetaVO(null);
			model.getAttribute().setForeignIntegrationPoint(null);
			model.getAttribute().setLookupMetaVO(null);
			model.getAttribute().setField(null);
			model.getAttribute().setSearchField(null);
			model.nextStep();
			model.nextStep();
			model.nextStep();
			model.nextStep();
			model.refreshModelState();
		}
		else if(this.model.isReferenceType()) {
			if (this.model.isDocumentFileType()) {
				model.getAttribute().setMetaVO(E.DOCUMENTFILE);
				model.getAttribute().setField(E.stringify(E.DOCUMENTFILE.filename));
				// like normal attribute, no editing of foreign entity
				model.nextStep();
				model.nextStep();
				model.nextStep();
			}
			model.nextStep();
			model.refreshModelState();
		}
		else if(this.model.isLookupType()) {
			model.nextStep();
			model.nextStep();
			model.refreshModelState();
		}
		else if(this.model.isAutoNumberType()) {
			// NUCLOS-1477
			model.nextStep();
			model.nextStep();
			model.nextStep();
			model.refreshModelState();
		}
		
		super.applyState();
	}

	private void validateMinMaxValues(String javaType) throws CommonValidationException {
		if (null == javaType) {
			throw new IllegalArgumentException("java datatype must not be null");
		}
		
		if(javaType.equals("java.util.Date")) {
			Date dMin = datMinValue.getDate();
			Date dMax = datMaxValue.getDate();
			if(dMin == null && dMax == null) {
				model.getAttribute().setInputValidation(null);
				return;
			}
			if(!(dMin != null && dMax != null)) {
				throw new CommonValidationException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.24", "Mindest- und Maximalwert müssen gesetzt sein."));
			}
			long l1 = dMin.getTime();
			long l2 = dMax.getTime();
			if(l1 > l2) {
				throw new CommonValidationException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.25", "Der Maximalwert ist kleiner als der Mindestwert."));
			}
			model.getAttribute().setInputValidation(l1 + " " + l2);
		}
		else if(javaType.equals("java.lang.Integer") || javaType.equals("java.lang.Double")) {
			String s1 = tfMinValue.getText();
			String s2 = tfMaxValue.getText();
			if(s1.length() == 0 && s2.length() == 0) {
				model.getAttribute().setInputValidation(null);
				return;
			}

			if(!(s1.length() > 0 && s2.length() > 0)) {
				throw new CommonValidationException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributeproperties.24", "Mindest- und Maximalwert müssen gesetzt sein."));
			}
			if(javaType.equals("java.lang.Integer")) {
				Integer i1 = Integer.parseInt(s1);
				Integer i2 = Integer.parseInt(s2);
				if(i1 > i2) {
					throw new CommonValidationException(SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.attributeproperties.25", "Der Maximalwert ist kleiner als der Mindestwert."));
				}
				model.getAttribute().setInputValidation(s1 + " " + s2);
			}
			else if(javaType.equals("java.lang.Double")) {
				s1 = s1.replace(',', '.');
				s2 = s2.replace(',', '.');
				Double d1 = Double.parseDouble(s1);
				Double d2 = Double.parseDouble(s2);
				if(d1 > d2) {
					throw new CommonValidationException(SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.attributeproperties.25", "Der Maximalwert ist kleiner als der Mindestwert."));
				}
				model.getAttribute().setInputValidation(s1 + " " + s2);
			}
			else
				model.getAttribute().setInputValidation(null);
		}
		else
			model.getAttribute().setInputValidation(null);

	}

	public void setParent(JComponent comp) {
		this.parent = comp;
	}

	public void setParentWizardModel(NuclosEntityWizardStaticModel model) {
		this.parentWizardModel = model;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		CollectableMasterDataWithDependants clct = (CollectableMasterDataWithDependants)e.getSource();
		final DataTyp typ = new DataTyp(clct.getMasterDataCVO());

		cbxDatatyp.addItem(typ);

		List<DataTyp> lst = new ArrayList<DataTyp>();
		for(int i = 0; i < cbxDatatyp.getModel().getSize(); i++) {
			lst.add((DataTyp)cbxDatatyp.getModel().getElementAt(i));
		}
		Collections.sort(lst, new Comparator<DataTyp>() {

			@Override
			public int compare(DataTyp o1, DataTyp o2) {
				return o1.getName().toUpperCase().compareTo(o2.getName().toUpperCase());
			}

		});

		cbxDatatyp.removeAllItems();
		for(DataTyp type : lst) {
			cbxDatatyp.addItem(type);
		}

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				cbxDatatyp.setSelectedItem(typ);
			}
		});
	}

	private void enableMinMaxValue(String dataTyp) {
		if(Integer.class.getName().equals(dataTyp) || Double.class.getName().equals(dataTyp)) {
			tfMinValue.setEnabled(true);
			tfMaxValue.setEnabled(true);
			tfMinValue.setVisible(true);
			tfMaxValue.setVisible(true);
			datMinValue.setDate(null);
			datMaxValue.setDate(null);
			datMinValue.setVisible(false);
			datMaxValue.setVisible(false);

			if(dataTyp.equals("java.lang.Integer")) {
				tfMinValue.setDocument(new NumericFormatDocument());
				tfMaxValue.setDocument(new NumericFormatDocument());
			}
			else {
				tfMinValue.setDocument(new DoubleFormatDocument());
				tfMaxValue.setDocument(new DoubleFormatDocument());
			}
		}
		else if(Date.class.getName().equals(dataTyp)) {
			tfMinValue.setText("");
			tfMaxValue.setText("");
			tfMinValue.setVisible(false);
			tfMaxValue.setVisible(false);
			datMinValue.setVisible(true);
			datMaxValue.setVisible(true);
		}
		else {
			tfMinValue.setEnabled(false);
			tfMaxValue.setEnabled(false);
			tfMinValue.setText("");
			tfMaxValue.setText("");
			tfMinValue.setVisible(true);
			tfMaxValue.setVisible(true);
			datMinValue.setDate(null);
			datMaxValue.setDate(null);
			datMinValue.setVisible(false);
			datMaxValue.setVisible(false);
		}
	}

	public void setColumnTypeChangeAllowed(boolean blnAllowed)  {
		this.columnTypeChangeAllowed = blnAllowed;
	}
	
	private final void handleAutoNumber() {
		
		// lookup for autonumber field in parten model
		Attribute attrAutoNumber = null;
		for (final Attribute attribute: parentWizardModel.getAttributeModel().getNucletAttributes()) {
			final DataTyp dt = attribute.getDatatyp();
			if (AutonumberUiUtils.isAutonumber(dt.getDefaultComponentType())) {
				attrAutoNumber = attribute;
				break;
			}
			
		}
		
		// remove autonumber item if it was found in the parent model
		if (null != attrAutoNumber 
			&& !attrAutoNumber.getDatatyp().equals(cbxDatatyp.getSelectedItem())) {
			cbxDatatyp.removeItem(attrAutoNumber.getDatatyp());
		}
	}
}
