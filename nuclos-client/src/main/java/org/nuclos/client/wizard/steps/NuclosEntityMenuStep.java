//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.FixedColumnRowHeader;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.common.E;
import org.nuclos.common.SF;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableEntity;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.pietschy.wizard.InvalidStateException;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*/
public class NuclosEntityMenuStep extends NuclosEntityConfWithSubformAbstractStep {

	private static final long serialVersionUID = 2900241917334839766L;

	public static final String[] labels = TranslationVO.LABELS_ENTITY;
	
	public NuclosEntityMenuStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	public void prepare() {
		super.prepare();
		this.removeAll();

		subform = new SubForm(E.ENTITYMENU.getUID(), false, JToolBar.VERTICAL, UID.UID_NULL, E.ENTITYMENU.entity.getUID());

		CollectableEntity clcte = new EntityMenuCollectableEntity(CollectableEOEntityClientProvider.getInstance().getCollectableEntity(E.ENTITYMENU.getUID()));

		// do not show column for resource id
		Column column = new Column(E.ENTITYMENU.menupath.getUID(), "Menupath", new CollectableComponentType(CollectableComponentTypes.TYPE_TEXTFIELD, null), false, false,false, false, false, false, 0, 0, null);
		subform.addColumn(column);

		Column column2 = new Column(E.ENTITYMENU.process.getUID(), clcte.getEntityField(E.ENTITYMENU.process.getUID()).getLabel(),
				new CollectableComponentType(CollectableComponentTypes.TYPE_COMBOBOX, null), true, model.isStateModel(), false, false,false, false, 0, 0, null);
		column2.setValueListProvider(new ProcessCollectableFieldsProvider());
		subform.addColumn(column2);

		this.add(subform, "0,0");

		MainFrameTab tab = getModel().getParentFrame();

		try {
			CollectableComponentModelProvider provider = getCollectableComponentModelProvider();
			Preferences prefs = java.util.prefs.Preferences.userRoot().node("org/nuclos/client/entitywizard/steps/menu");

			subFormController = new MasterDataSubFormController(clcte, tab, provider, E.ENTITYMENU.getUID(), subform, prefs,
					getEntityPreferences(), null, null, null);

			final Map<Object, Integer> mpWidths = new HashMap<>(20);

			List<CollectableEntityField> lstAllFields = new ArrayList<>();
			for (UID fieldUID : clcte.getFieldUIDs()) {
				if (!SF.CREATEDBY.checkField(clcte.getUID(), fieldUID)
						&& !SF.CREATEDAT.checkField(clcte.getUID(), fieldUID)
						&& !SF.CHANGEDBY.checkField(clcte.getUID(), fieldUID)
						&& !SF.CHANGEDAT.checkField(clcte.getUID(), fieldUID)) {
					lstAllFields.add(clcte.getEntityField(fieldUID));
				}
				if (fieldUID.getString().startsWith("menupath_")) {
					mpWidths.put(fieldUID, 140);
				}
			}

			// remember the widths of the currently visible columns
			TableColumnModel externalColumnModel = subform.getSubformRowHeader().getExternalTable().getColumnModel();
			for (Enumeration<TableColumn> columnEnum = externalColumnModel.getColumns(); columnEnum.hasMoreElements();) {
				TableColumn varColumn = columnEnum.nextElement();
				mpWidths.put(varColumn.getIdentifier(), new Integer(varColumn.getWidth()));
			}

			((FixedColumnRowHeader)subform.getSubformRowHeader()).changeSelectedColumns(
					lstAllFields, Collections.EMPTY_SET, null, mpWidths, null, null);

			fillSubformAndRequestFocus(model.getEntityMenus());
		} catch (Exception e) {
			Errors.getInstance().showExceptionDialog(tab, e);
		}

	}

	@Override
	public void applyState() throws InvalidStateException {
		List<CollectableEntityObject> subformdata;
		try {
			subformdata = subFormController.getCollectables(true, true, true);
		} catch (CommonValidationException e1) {
			JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMessageFromResource(e1.getMessage()),
	    			SpringLocaleDelegate.getInstance().getMessage("wizard.step.menu.error.title", "Achtung!"), 
	    			JOptionPane.OK_OPTION);
 	        throw new InvalidStateException();
		}
		Collection<EntityObjectVO<UID>> entityMenus = CollectionUtils.transform(subformdata, new Transformer<CollectableEntityObject, EntityObjectVO<UID>>() {
			@Override
			public EntityObjectVO<UID> transform(CollectableEntityObject i) {
				return i.getEntityObjectVO();
			}
		});
		model.setEntityMenus(entityMenus);
		
		super.applyState();
	}

	public static class EntityMenuCollectableEntity extends AbstractCollectableEntity {

		public EntityMenuCollectableEntity(CollectableEntity clcte) {
			super(E.ENTITYMENU.getUID(), clcte.getLabel());

			for (UID field : clcte.getFieldUIDs()) {
				this.addCollectableEntityField(clcte.getEntityField(field));
			}

			for (LocaleInfo li : LocaleDelegate.getInstance().getAllLocales(false)) {
				String fieldname = "menupath_" + li.getTag();
				String label = getSpringLocaleDelegate().getMessage(
						"EntityMenuCollectableEntity.translationfield.label", "Menu path ({0})", li.getTitle());
				String description = getSpringLocaleDelegate().getMessage(
						"EntityMenuCollectableEntity.translationfield.description", "Menu path ({0}). Use \\ to create submenus.", li.getTitle());
				DefaultCollectableEntityField field = new DefaultCollectableEntityField(new UID(fieldname), String.class, label,
						description, 255, null, false, CollectableField.TYPE_VALUEFIELD, null, null, E.ENTITYMENU.getUID(), null);
				field.setCollectableEntity(this);
				this.addCollectableEntityField(field);
			}
		}
	}

	private class ProcessCollectableFieldsProvider implements CollectableFieldsProvider {
		@Override
		public void setParameter(String sName, Object oValue) { 
		}
		@Override
		public List<CollectableField> getCollectableFields() throws CommonBusinessException {
			if (model.getProcesses() != null) {
				return CollectionUtils.transform(model.getProcesses(), new Transformer<EntityObjectVO<?>, CollectableField>() {
					@Override
					public CollectableField transform(EntityObjectVO<?> i) {
						return new CollectableValueIdField(i.getId(), i.getFieldValue(E.PROCESS.name));
					}
				});
			}
			else {
				return new ArrayList<CollectableField>();
			}
		}
	}
}
