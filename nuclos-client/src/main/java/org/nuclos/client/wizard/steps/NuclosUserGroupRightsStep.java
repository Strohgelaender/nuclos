//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.wizard.model.EntityRightsSelectTableModel;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.LockMode;
import org.nuclos.common.UnlockMode;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.LocalizedCollectableValueField;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.Localizable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.MasterDataPermission;
import org.nuclos.server.common.ModulePermission;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
* 
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosUserGroupRightsStep extends NuclosEntityAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosUserGroupRightsStep.class);

	private JTable table;
	private JScrollPane scroll;
	private EntityRightsSelectTableModel userRightsModel;
	private TableColumn col;
	private JComboBox cmbUserRights;
	
	private boolean preparing = false;
	private JComboBox cmbLockMode;
	private JComboBox cmbOwnerForeignEntityField;
	private JComboBox cmbUnlockMode;

	public NuclosUserGroupRightsStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	protected final void initComponents() {
		
		TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.PREFERRED, TableLayout.FILL, 10);
		
		userRightsModel = new EntityRightsSelectTableModel();
		
		table = new JTable(userRightsModel);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		table.getTableHeader().setReorderingAllowed(false);
		
		col = table.getColumnModel().getColumn(1);
		cmbUserRights = new JComboBox();
		
		col.setCellEditor(new DefaultCellEditor(cmbUserRights));
		
		scroll = new JScrollPane();
		scroll.getViewport().add(table);
		
		cmbLockMode = new JComboBox();
		cmbOwnerForeignEntityField = new JComboBox();
		cmbUnlockMode = new JComboBox();
		
		cmbLockMode.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (preparing) {
					return;
				}
				LockModeObject lockModeObject = (LockModeObject) e.getItem();
				if (lockModeObject.lockMode == null) {
					if (cmbOwnerForeignEntityField.getItemCount()>0) {
						cmbOwnerForeignEntityField.setSelectedIndex(0);
					}
					if (cmbUnlockMode.getItemCount()>0) {
						cmbUnlockMode.setSelectedIndex(0);
					}
				} else {
					if (!cmbOwnerForeignEntityField.isEnabled() && cmbOwnerForeignEntityField.getItemCount()>1) {
						cmbOwnerForeignEntityField.setSelectedIndex(1);
					}
					if (!cmbUnlockMode.isEnabled() && cmbUnlockMode.getItemCount()>1) {
						cmbUnlockMode.setSelectedIndex(1);
					}
				}
				cmbOwnerForeignEntityField.setEnabled(lockModeObject.lockMode != null);
				cmbUnlockMode.setEnabled(lockModeObject.lockMode != null);
			}
		});
		
		tbllay.newRow(TableLayout.FILL).addFullSpan(scroll);
		tbllay.newRow(10);
		
		JPanel lockFunctionPanel = new JPanel();
		lockFunctionPanel.setBorder(BorderFactory.createTitledBorder(SpringLocaleDelegate.getInstance().getMsg("wizard.step.permission.locking.label")));
		TableLayoutBuilder tbllayLock = new TableLayoutBuilder(lockFunctionPanel).columns(TableLayout.PREFERRED, 10, TableLayout.PREFERRED);
		
		tbllayLock.newRow().add(new JLabel(SpringLocaleDelegate.getInstance().getMsg("wizard.step.permission.lockmode.label"))).skip().add(cmbLockMode);
		tbllayLock.newRow(2);
		tbllayLock.newRow().add(new JLabel(SpringLocaleDelegate.getInstance().getMsg("wizard.step.permission.owner.label"))).skip().add(cmbOwnerForeignEntityField);
		tbllayLock.newRow(2);
		tbllayLock.newRow().add(new JLabel(SpringLocaleDelegate.getInstance().getMsg("wizard.step.permission.unlockmode.label"))).skip().add(cmbUnlockMode);
		tbllayLock.newRow(4);
		
		tbllay.newRow(TableLayout.PREFERRED).add(lockFunctionPanel);
	}

	@Override
	public void close() {
		table = null;
		scroll = null;
		userRightsModel = null;
		col = null;
		cmbUserRights = null;
		
		cmbLockMode = null;
		cmbOwnerForeignEntityField = null;
		cmbUnlockMode = null;
		
		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		this.model.setUserRights(this.userRightsModel.getUserRights());
		LockModeObject lockModeObject = (LockModeObject) this.cmbLockMode.getSelectedItem();
		this.model.setLockMode(lockModeObject.lockMode);
		OwnerForeignEntityFieldObject ownerForeignEntityFieldObject = (OwnerForeignEntityFieldObject) this.cmbOwnerForeignEntityField.getSelectedItem();
		this.model.setOwnerForeignEntityField(ownerForeignEntityFieldObject.sOwnerForeignEntityField);
		UnlockModeObject unlockModeObject = (UnlockModeObject) this.cmbUnlockMode.getSelectedItem();
		this.model.setUnlockMode(unlockModeObject.unlockMode);
		
		super.applyState();		
	}

	@Override
	public void prepare() {
		super.prepare();
		preparing = true;
		cmbUserRights.removeAllItems();
		cmbUserRights.addItem("");
		try {
			Class<? extends Enum<?>> clazz = (Class<? extends Enum<?>>) 
					LangUtils.getClassLoaderThatWorksForWebStart().loadClass(
							"org.nuclos.server.common.MasterDataPermission").asSubclass(Enum.class);
			for (Enum<?> e : clazz.getEnumConstants()) {
				// Don't add NO permission
				if (MasterDataPermission.NO.equals(e)) {
					continue;
				}
				Object value = (e instanceof KeyEnum) ? ((KeyEnum<?>) e).getValue() : e.name();
				String text = (e instanceof Localizable) ? 
						SpringLocaleDelegate.getInstance().getText((Localizable) e) : e.toString();
				CollectableField cf = new LocalizedCollectableValueField(value, text);
				cmbUserRights.addItem(cf);
			}
		}
		catch(ClassCastException e) {
			throw new CommonFatalException(e);
		}
        catch(ClassNotFoundException e) {
        	throw new CommonFatalException(e);
        }
        
		if(!this.model.isStateModel()) {
			userRightsModel.setType(EntityRightsSelectTableModel.TYPE_MASTERDATA);
		} else {
			userRightsModel.setType(EntityRightsSelectTableModel.TYPE_STATEMODEL);
			cmbUserRights.addItem(new LocalizedCollectableValueField(
					ModulePermission.DELETE_PHYSICALLY.getValue(), "Lesen/Schreiben/Physikalisch L\u00f6schen"));			
		}

		loadUserRights();

		if(this.model.getUserRights().size() > 0) {
			final Collection<MasterDataVO<?>> colVO = MasterDataCache.getInstance().get(E.ROLE.getUID());
			userRightsModel.clear();
			for(MasterDataVO<?> ur : this.model.getUserRights()) {				
				this.userRightsModel.addRole(ur);
			}
			
			for(MasterDataVO<?> voRole : colVO) {
				boolean add = true;
				for(MasterDataVO<?> voEntityRole : userRightsModel.getUserRights()) {
					if(voRole.getPrimaryKey().equals(voEntityRole.getFieldUid(
							this.model.isStateModel() ? E.ROLEMODULE.role.getUID() : E.ROLEMASTERDATA.role.getUID()))) {
						add = false;
						break;
					}
				}
				if(add) {
					this.userRightsModel.addRole(NuclosWizardUtils.setFieldsForUserRight(voRole, this.model));
				}
			}		
		}
		cmbLockMode.removeAllItems();
		cmbOwnerForeignEntityField.removeAllItems();
		cmbUnlockMode.removeAllItems();
		
		cmbLockMode.addItem(new LockModeObject(null));
		cmbLockMode.addItem(new LockModeObject(LockMode.OWNER_API_ONLY));
		cmbOwnerForeignEntityField.addItem(new OwnerForeignEntityFieldObject(null, ""));
		cmbOwnerForeignEntityField.addItem(new OwnerForeignEntityFieldObject(E.stringify(E.USER.name), 
											SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(E.USER.name)));
		cmbOwnerForeignEntityField.addItem(new OwnerForeignEntityFieldObject(E.stringify(E.USER.lastname) + ", " + 
											E.stringify(E.USER.firstname), 
											SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(E.USER.lastname) + ", " +
											SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(E.USER.firstname)));
		cmbOwnerForeignEntityField.addItem(new OwnerForeignEntityFieldObject(E.stringify(E.USER.firstname) + " " + 
											E.stringify(E.USER.lastname), 
											SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(E.USER.firstname) + " " +
											SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(E.USER.lastname)));
		cmbOwnerForeignEntityField.addItem(new OwnerForeignEntityFieldObject(E.stringify(E.USER.email), 
											SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(E.USER.email)));
		cmbUnlockMode.addItem(new UnlockModeObject(null));
		cmbUnlockMode.addItem(new UnlockModeObject(UnlockMode.API_ONLY));
		cmbUnlockMode.addItem(new UnlockModeObject(UnlockMode.ALL_USERS_MANUALLY));
		
		cmbLockMode.setSelectedItem(new LockModeObject(this.model.getLockMode()));
		cmbUnlockMode.setSelectedItem(new UnlockModeObject(this.model.getUnlockMode()));
		OwnerForeignEntityFieldObject ownerForeignEntityFieldObject = new OwnerForeignEntityFieldObject(this.model.getOwnerForeignEntityField(), this.model.getOwnerForeignEntityField());
		cmbOwnerForeignEntityField.setSelectedItem(ownerForeignEntityFieldObject);
		if (ownerForeignEntityFieldObject.sOwnerForeignEntityField != null && cmbOwnerForeignEntityField.getSelectedIndex() == 0) {
			// userdefined presentation...
			cmbOwnerForeignEntityField.addItem(ownerForeignEntityFieldObject);
			cmbOwnerForeignEntityField.setSelectedItem(ownerForeignEntityFieldObject);
		}
		
		cmbOwnerForeignEntityField.setEnabled(this.model.getLockMode() != null);
		cmbUnlockMode.setEnabled(this.model.getLockMode() != null);
		
		if (this.model.isProxy() || this.model.isGeneric()) {
			cmbLockMode.setSelectedIndex(0);
			cmbOwnerForeignEntityField.setSelectedIndex(0);
			cmbUnlockMode.setSelectedIndex(0);
			cmbLockMode.setEnabled(false);
			cmbOwnerForeignEntityField.setEnabled(false);
			cmbUnlockMode.setEnabled(false);
		}
		
		preparing = false;

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.requestFocusInWindow();
			}
		});
	}

	private void loadUserRights() {
		this.model.getUserRights().clear();
		if(!this.model.isEditMode()) {
			for(MasterDataVO<?> vo : MasterDataCache.getInstance().get(E.ROLE.getUID())) {
				final MasterDataVO<?> voAdd = NuclosWizardUtils.setFieldsForUserRight(vo, this.model);
				this.model.getUserRights().add(voAdd);
			}
		} else {
			final Collection<MasterDataVO<?>> colRolePermisson;
			if(this.model.isStateModel()) {
				colRolePermisson = MasterDataCache.getInstance().get(E.ROLEMODULE.getUID());
			} else {
				colRolePermisson = MasterDataCache.getInstance().get(E.ROLEMASTERDATA.getUID());
			}
			for(MasterDataVO<?> voPerm : colRolePermisson) {
				try {
					if(this.model.isStateModel()) {
						if(voPerm.getFieldUid(E.ROLEMODULE.module).equals(this.model.getUID())) {
							//if (!this.model.getUserRights().contains(voPerm)) {
							this.model.getUserRights().add(voPerm);
							//}
						}
					} else {
						if(voPerm.getFieldUid(E.ROLEMASTERDATA.entity).equals(this.model.getUID())) {
							//if (!this.model.getUserRights().contains(voPerm)) {
							this.model.getUserRights().add(voPerm);
							//}
							//break;
						}
					}
				} catch (CommonFatalException ex) {
					// entity in colRoleMasterdata does not exist anymore
					LOG.warn(ex.getMessage());
					continue;
				}
			}
		}
	}
	
	private static class LockModeObject {
		private final LockMode lockMode;
		public LockModeObject(LockMode lockMode) {
			this.lockMode = lockMode;
		}
		@Override
		public String toString() {
			return lockMode==null?"":lockMode.toString().replace('_', ' ');
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((lockMode == null) ? 0 : lockMode.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			LockModeObject other = (LockModeObject) obj;
			if (lockMode == null) {
				if (other.lockMode != null)
					return false;
			} else if (!lockMode.equals(other.lockMode))
				return false;
			return true;
		}
	}
	
	private static class UnlockModeObject {
		private final UnlockMode unlockMode;
		public UnlockModeObject(UnlockMode unlockMode) {
			this.unlockMode = unlockMode;
		}
		@Override
		public String toString() {
			return unlockMode==null?"":unlockMode.toString().replace('_', ' ');
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((unlockMode == null) ? 0 : unlockMode.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UnlockModeObject other = (UnlockModeObject) obj;
			if (unlockMode != other.unlockMode)
				return false;
			return true;
		}
	}
	
	private static class OwnerForeignEntityFieldObject {
		private final String sOwnerForeignEntityField;
		private final String sLabel;
		public OwnerForeignEntityFieldObject(String sOwnerForeignEntityField, String sLabel) {
			super();
			this.sOwnerForeignEntityField = sOwnerForeignEntityField;
			this.sLabel = sLabel;
		}
		@Override
		public String toString() {
			return sLabel;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((sLabel == null) ? 0 : sLabel.hashCode());
			result = prime * result + ((sOwnerForeignEntityField == null) ? 0 : sOwnerForeignEntityField.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			OwnerForeignEntityFieldObject other = (OwnerForeignEntityFieldObject) obj;
			if (sOwnerForeignEntityField == null) {
				if (other.sOwnerForeignEntityField != null)
					return false;
			} else if (!sOwnerForeignEntityField.equals(other.sOwnerForeignEntityField))
				return false;
			return true;
		}
		
	}
}
