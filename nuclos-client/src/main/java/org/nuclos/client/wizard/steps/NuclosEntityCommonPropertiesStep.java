//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.GeneratorActions;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.resource.NuclosResourceCategory;
import org.nuclos.client.resource.admin.CollectableResouceSaveListener;
import org.nuclos.client.resource.admin.ResourceCollectController;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIDPresentation;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.client.ui.resource.ResourceIconChooser;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.DataTyp;
import org.nuclos.client.wizard.model.EntityAttributeTableModel;
import org.nuclos.client.wizard.util.ModifierMap;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.MandatorLevelVO;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityCommonPropertiesStep extends NuclosEntityAbstractStep implements CollectableResouceSaveListener {

	private static final Logger LOG = Logger.getLogger(NuclosEntityCommonPropertiesStep.class);

	private static final String[] VIRTUAL_STATICFIELDS = new String[] {"INTID", "DATCREATED", "STRCREATED", "DATCHANGED", "STRCHANGED", "INTVERSION"};

	private JLabel lbLabelSingular;
	private JTextField tfLabelSingular;
	private JLabel lbMenupath;
	private JComboBox cbMenupath;
	private JLabel lbSystemIdPrefix;
	private JTextField tfSystemIdPrefix;
	private JLabel lbIconCustom;
	private JComboBox cbxIcon;
	private JButton btNewIcon;

	private JLabel lbAccelerator;
	private JComboBox cbxModifier;
	private JTextField tfMnemonic;

	private JLabel lbLogbook;
	private JCheckBox cbLogbook;
	private JLabel lbSearchable;
	private JCheckBox cbSearchable;
	private JLabel lbShowSearch;
	private JCheckBox cbShowSearch;
	private JLabel lbEditable;
	private JCheckBox cbEditable;
	private JLabel lbResultDetailsSplit;
	private JCheckBox cbResultDetailsSplit;
	private JLabel lbThin;
	private JCheckBox cbThin;

	private JLabel lbShowRelation;
	private JCheckBox cbShowRelation;
	private JLabel lbShowGroups;
	private JCheckBox cbShowGroups;

	private JLabel lbStateModel;
	private JCheckBox cbStateModel;

	private JLabel lbVirtual;
	private JComboBox cbxVirtual;
	private JLabel lbIdFactory;
	private JComboBox cbxIdFactory;
	
	private JLabel lbProxy;
	private JCheckBox cbProxy;
	
	private JLabel lblWriteProxy;
	private JCheckBox cbWriteProxy;
	
	private JLabel lbProxyInterface;
	private JTextField tfProxyInterface;
	private JLabel lbGeneric;
	private JCheckBox cbGeneric;

	private JLabel lbCache;
	private JCheckBox cbCache;
	private JLabel lbTableName;
	private JLabel lbTableNamePrefix;
	private JTextField tfTableName;
	private JLabel lbInternalEntityName;
	private JTextField tfInternalEntityName;
	
	private JLabel lbCloneGenerator;
	private JComboBox cbxCloneGenerator;

	private JLabel lbIcon;
	private ResourceIconChooser nuclosIconChooser;
	
	private JLabel lbMandatorLevel;
	private JComboBox cbxMandatorLevel;
	private JLabel lbMandatorInitialFill;
	private JComboBox cbxMandatorInitialFill;
	private JLabel lbMandatorUnique;
	private JCheckBox cbMandatorUnique;

	private boolean blnSingular;
	private boolean blnSingularModified;
	private boolean blnMenuPathModified;
	private boolean blnSystemId;

	private JLabel lbComment;
	private JTextArea txtComment;
	private JScrollPane scpComment;

	public NuclosEntityCommonPropertiesStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	public void prepare() {
		super.prepare();
		initMenupathElement();
		if(!model.isEditMode()) {
			if(!blnSingularModified) {
				tfLabelSingular.setText(model.getLabelSingular());
			}
			if(!blnMenuPathModified) {
				JTextField field = (JTextField)cbMenupath.getEditor().getEditorComponent();
				field.setText(model.getMenuPath());
			}
			cbMenupath.setSelectedItem(model.getMenuPath());
			tfInternalEntityName.setText(model.getEntityName());
			setComplete(true);
			if (model.isProxy() || model.isGeneric()) {
				tfTableName.setText(model.getTableOrViewNameWithoutPrefix());
			} else {
				tfTableName.setText(NuclosWizardUtils.replace(model.getTableOrViewNameWithoutPrefix()));
			}
			tfSystemIdPrefix.setText(model.getSystemIdPrefix());
			tfMnemonic.setText(model.getAccelerator());
			cbxModifier.setSelectedItem(model.getModifierAsString());
			cbStateModel.setSelected(model.isStateModel());
			cbxCloneGenerator.setEnabled(false);
			txtComment.setText(model.getComment());
			boolean enabledTypeChange = !model.hasAttributes();
			cbGeneric.setEnabled(enabledTypeChange);
			lbGeneric.setEnabled(enabledTypeChange);
					
			cbProxy.setEnabled(enabledTypeChange);
			lbProxy.setEnabled(enabledTypeChange);
		}
		else {
			tfLabelSingular.setText(model.getLabelSingular());
			JTextField field = (JTextField)cbMenupath.getEditor().getEditorComponent();
			field.setText(model.getMenuPath());
			cbMenupath.setSelectedItem(model.getMenuPath());
			tfTableName.setText(model.getTableOrViewNameWithoutPrefix());
			// NUCLOS-6143 enable table name change
			// tfTableName.setEnabled(false);
			cbCache.setSelected(model.isCachable());
			cbSearchable.setSelected(model.isSearchable());
			handleShowSearch(cbSearchable.isSelected());
			cbShowSearch.setSelected(model.showsSearch());
			cbEditable.setSelected(model.isEditable());
			cbResultDetailsSplit.setSelected(model.isResultDetailsSplit());
			cbThin.setSelected(model.isThin());
			cbShowRelation.setSelected(model.isShowRelation());
			cbShowGroups.setSelected(model.isShowGroups());
			cbLogbook.setSelected(model.isLogbook());
			tfInternalEntityName.setText(model.getEntityName());
			// NUCLOS-6143 enable internal entity name change
			// tfInternalEntityName.setEnabled(false);
			tfSystemIdPrefix.setText(model.getSystemIdPrefix());
			cbShowGroups.setSelected(model.isShowGroups());
			cbShowRelation.setSelected(model.isShowRelation());
			cbStateModel.setSelected(model.isStateModel());
			if(!model.isStateModel()){
				lbSystemIdPrefix.setText(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.entitycommonproperties.12", "K\u00fcrzel f\u00fcr Identifizierer:"));
			}
			else {
				lbSystemIdPrefix.setText(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.entitycommonproperties.13", "K\u00fcrzel f\u00fcr Identifizierer *:"));
			}
			txtComment.setText(model.getComment());
			tfMnemonic.setText(model.getAccelerator());
			
			class LabeledGenerator {
				private final UID uid; 
				private final String label;
				public LabeledGenerator(UID uid, String label) {
					this.uid = uid;
					this.label = label;
				}
				@Override
				public String toString() {
					return label;
				}
				@Override
				public boolean equals(Object obj) {
					if (!(obj instanceof LabeledGenerator)) {
						return false;
					}
					return LangUtils.equal(uid, ((LabeledGenerator)obj).uid);
				}
				@Override
				public int hashCode() {
					return uid != null ? uid.hashCode() : 2347;
				}
			}
			cbxCloneGenerator.setEnabled(true);
			final List<GeneratorActionVO> lstGenerators = GeneratorActions.getGeneratorActions(model.getUID());
			final List<LabeledGenerator> lstGeneratorModel = new ArrayList<LabeledGenerator>();
			// add empty entry
			lstGeneratorModel.add(new LabeledGenerator(null, ""));
			for (GeneratorActionVO genvo : lstGenerators) {
				if (LangUtils.equal(genvo.getSourceModule(), genvo.getTargetModule())) {
					lstGeneratorModel.add(new LabeledGenerator(genvo.getId(), genvo.getName()));
				}
			}
			cbxCloneGenerator.setModel(new DefaultComboBoxModel(lstGeneratorModel.toArray(new LabeledGenerator[0])));
			if (lstGeneratorModel.size() <= 1 || model.isVirtual()) {
				cbxCloneGenerator.setEnabled(false);
				model.setCloneGenerator(null);
			}
			cbxCloneGenerator.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						model.setCloneGenerator(((LabeledGenerator)e.getItem()).uid);
					}
				}
			});
			cbxCloneGenerator.setSelectedItem(new LabeledGenerator(model.getCloneGenerator(), null));

			cbxModifier.setSelectedItem(model.getModifierAsString());
			
			final UID resourceUID = model.getResourceUID();
			if(resourceUID != null) {
				final MasterDataVO<?> vo = MasterDataCache.getInstance().get(E.RESOURCE.getUID(), resourceUID);
				cbxIcon.setSelectedItem(new UIDPresentation((UID)vo.getPrimaryKey(), vo.getFieldValue(E.RESOURCE.name).toString()));
			}

			nuclosIconChooser.setSelected(model.getNuclosResourceName());

			final boolean virtual = model.isVirtual();
			if (virtual) {
				cbxVirtual.setSelectedItem(model.getVirtualentity());
				cbxIdFactory.setSelectedItem(model.getIdFactory());
				cbStateModel.setEnabled(false);
				cbLogbook.setEnabled(false);
				cbEditable.setEnabled(false);
				final boolean emptyIdFactory = StringUtils.isNullOrEmpty(model.getIdFactory());
				cbEditable.setSelected(!emptyIdFactory);
			}
			cbxVirtual.setEnabled(false);
			
			final boolean proxy = model.isProxy();
			cbProxy.setEnabled(false);
			cbProxy.setSelected(proxy);
			lbProxy.setEnabled(false);
			tfProxyInterface.setText(model.getProxyInterface());
			final boolean generic = model.isGeneric();
			cbGeneric.setEnabled(false);
			cbGeneric.setSelected(generic);
			cbGeneric.setEnabled(false);
			if (proxy || generic) {
				handleEnable(proxy, generic);
			}
		}

		
		lbTableNamePrefix.setText(model.getNucletLocalIdentifier() + "_");
		
		model.setEditable(cbEditable.isSelected());
		model.setResultDetailsSplit(cbResultDetailsSplit.isSelected());
		model.setSearchable(cbSearchable.isSelected());
		model.setStateModel(cbStateModel.isSelected());
		model.setShowGroups(cbShowGroups.isSelected());
		model.setShowRelation(cbShowRelation.isSelected());
		model.setLogbook(cbLogbook.isSelected());
		model.setProxy(cbProxy.isSelected());
		model.setGeneric(cbGeneric.isSelected());

		lbSystemIdPrefix.setEnabled(model.isStateModel());
		tfSystemIdPrefix.setEnabled(model.isStateModel());
		lbShowGroups.setEnabled(model.isStateModel());
		cbShowGroups.setEnabled(model.isStateModel());
		lbShowRelation.setEnabled(model.isStateModel());
		cbShowRelation.setEnabled(model.isStateModel());

		cbWriteProxy.setSelected(model.isWriteProxy());
		if(model.isWriteProxy())
			cbEditable.setSelected(true);
		
		MandatorLevelVO selectedLevel = null;
		List<MandatorLevelVO> levels = getMandatorLevels();
		for(MandatorLevelVO level : levels) {
			if (level.getUID().equals(model.getMandatorLevel())) {
				selectedLevel = level;
			}
		}
		levels.add(0, null);
		cbxMandatorLevel.setModel(new DefaultComboBoxModel(levels.toArray(new MandatorLevelVO[]{})));
		if (selectedLevel != null) {
			cbxMandatorLevel.setSelectedItem(selectedLevel);
		} else {
			cbxMandatorLevel.setSelectedIndex(0);
			lbMandatorInitialFill.setEnabled(false);
			cbxMandatorInitialFill.setEnabled(false);
			cbxMandatorInitialFill.setModel(new DefaultComboBoxModel());
			enableMandatorInitialFill(null, null);
			lbMandatorUnique.setEnabled(false);
			cbMandatorUnique.setEnabled(false);
			cbMandatorUnique.setSelected(false);
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				setComplete(true);
				tfLabelSingular.requestFocusInWindow();
			}
		});

	}	
	
	public static List<MandatorLevelVO> getMandatorLevels() {
		List<MandatorLevelVO> levels = new ArrayList<MandatorLevelVO>();
		for(MasterDataVO<?> mdvo : MasterDataDelegate.getInstance().getMasterData(E.MANDATOR_LEVEL.getUID())) {
			MandatorLevelVO level = new MandatorLevelVO((EntityObjectVO<UID>) mdvo.getEntityObject());
			levels.add(level);
		}
		MandatorLevelVO.sort(levels);
		return levels;
	}

	private void initMenupathElement() {
		cbMenupath.removeAllItems();
		cbMenupath.addItem("");
		for(String sMenu : NuclosWizardUtils.getExistingMenuPaths()) {
			cbMenupath.addItem(sMenu);
		}
	}

	@Override
	protected void initComponents() {

		lbLabelSingular = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.1", "Beschriftung Fenstertitel"));
		tfLabelSingular = new JTextField();
		tfLabelSingular.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.1", "Beschriftung Fenstertitel"));
		tfLabelSingular.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		lbMenupath = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.2", "Pfad im Men\u00fc"));

		lbComment = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.19", 
				"Kommentar"));
		txtComment = new JTextArea();
		txtComment.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.18", 
				"Mit der Angabe eines Kommentares können zusätzliche Informationen über ein BusinessObjekt hinterlegt werden."));
		scpComment = new JScrollPane(txtComment,  
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	
		cbMenupath = new JComboBox();
		cbMenupath.setEditable(true);
		cbMenupath.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.2", "Pfad im Men\u00fc"));
		cbMenupath.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		AutoCompleteDecorator.decorate(cbMenupath);

		lbSystemIdPrefix = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.12", "K\u00fcrzel f\u00fcr Identifizierer"));
		tfSystemIdPrefix = new JTextField();
		tfSystemIdPrefix.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.12", "K\u00fcrzel f\u00fcr Identifizierer"));
		tfSystemIdPrefix.setDocument(new LimitSpecialCharacterDocument(3));
		lbIconCustom = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.20", "Benutzerdefiniertes Icon"));
		cbxIcon = new JComboBox();
		cbxIcon.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.3", "Icon"));
		cbxIcon.setRenderer(new DefaultListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				final JLabel lbl = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				if (value instanceof MasterDataVO<?>) {
					lbl.setText(((MasterDataVO<?>)value).getFieldValue(E.RESOURCE.name));
				}
				return lbl;
			}
		});
 		btNewIcon = new JButton("...");
		btNewIcon.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.4", "Neues Icon erstellen"));

		lbAccelerator = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.5", "Tastenk\u00fcrzel"));
		cbxModifier = new JComboBox();
		cbxModifier.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.5", "Tastenk\u00fcrzel"));
		cbxModifier.addItem("");
		for(String str : ModifierMap.getModifierMap().keySet()) {
			cbxModifier.addItem(str);
		}
		tfMnemonic = new JTextField();
		tfMnemonic.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.5", "Tastenk\u00fcrzel"));
		tfMnemonic.setDocument(new LimitDocument(2));

		lbLogbook = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.14", "Historie?"));
		cbLogbook = new JCheckBox();
		cbLogbook.setSelected(false);
		cbLogbook.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.14", "Historie?"));

		lbSearchable = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.6", "Wird eine Suchmaske ben\u00f6tigt"));
		cbSearchable = new JCheckBox();
		cbSearchable.setSelected(true);
		cbSearchable.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.6", "Wird eine Suchmaske ben\u00f6tigt"));
		
		lbShowSearch = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.6b", "Suchmaske direkt \u00f6ffnen?"));
		cbShowSearch = new JCheckBox();
		cbShowSearch.setSelected(false);
		cbShowSearch.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.6b", "Suchmaske direkt \u00f6ffnen"));

		lbEditable = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.7", "Ist die Entit\u00e4t modifizierbar?"));
		cbEditable = new JCheckBox();
		cbEditable.setSelected(true);
		cbEditable.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.7", "Ist die Entit\u00e4t modifizierbar"));

		lbResultDetailsSplit = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.entitycommonproperties.24", "Gesplittete Anzeige?"));
		cbResultDetailsSplit = new JCheckBox();
		cbResultDetailsSplit.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.entitycommonproperties.tooltip.24", "Gesplittete Result- und Detailsanzeige"));
		
		lbThin = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.6c", "Thin Businessobjekt?"));
		cbThin = new JCheckBox();
		cbThin.setSelected(true);
		cbThin.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.6c", "Thin Businessobjekt"));
		
		lbShowRelation = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.15", "Zeige Relationen?"));
		cbShowRelation = new JCheckBox();
		cbShowRelation.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.15", "Zeige Relationen"));

		lbShowGroups = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.16", "Zeige Gruppen?"));
		cbShowGroups = new JCheckBox();
		cbShowGroups.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.16", "Zeige Gruppen:"));

		lbStateModel = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.17", "Statusmodell?"));
		cbStateModel = new JCheckBox();
		cbStateModel.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.17", "Statusmodell?"));

		lbVirtual = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.virtual.label", "Virtual entity"));
		final List<String> ves = MetaDataDelegate.getInstance().getVirtualEntities();
		ves.add("");
		Collections.sort(ves);
		final ListComboBoxModel<String> virtualentitymodel = new ListComboBoxModel<String>(ves);
		cbxVirtual = new JComboBox(virtualentitymodel);
		cbxVirtual.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.virtual.description", "Virtual entity (use an existing view)"));
		// enable virtual entity name for new entities
		cbxVirtual.setEnabled(true);
		
		//proxy
		lbProxy = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.proxy.label", "Proxy entity?"));
		cbProxy = new JCheckBox();
		cbProxy.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.proxy.description", "Generates a BO interface. Implement this in your extension or a rule."));
		// enable proxy entity for new entities
		cbProxy.setEnabled(true);
		lbProxy.setEnabled(true);
		
		
		//schreib proxy wizard.step.entitycommonproperties.proxy.label
		lblWriteProxy = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.schreibproxy.label", "Schreib-Proxy Businessobjekt?"));

		cbWriteProxy = new JCheckBox();
		cbWriteProxy.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.proxy.description", "Generates a BO interface. Implement this in your extension or a rule."));
		// disable proxy entity for new entities
		cbWriteProxy.setEnabled(false);
		lblWriteProxy.setEnabled(true);
		
		
		//proxy IF
		lbProxyInterface = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.proxyInterface.label", "Proxy Interface"));
		tfProxyInterface = new JTextField();
		tfProxyInterface.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.proxyInterface.description", "Use a server rule to implement this interface"));
		tfProxyInterface.setEditable(false);
		lbProxyInterface.setEnabled(false);

		lbGeneric = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.generic.label", "Generic business object?"));
		cbGeneric = new JCheckBox();
		cbGeneric.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.generic.description", "Defines similarities between multiple BOs in a Generic object."));
		// enable proxy entity for new entities
		cbGeneric.setEnabled(true);
		lbGeneric.setEnabled(true);

		lbIdFactory = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.idfactory.label",
				"Primary key factory for VE"));
		final List<String> pif = MetaDataDelegate.getInstance().getPossibleIdFactories();
		pif.add("");
		Collections.sort(pif);
		final ListComboBoxModel<String> pifModel = new ListComboBoxModel<String>(pif);
		cbxIdFactory = new JComboBox(pifModel);
		cbxIdFactory.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.idfactory.description",
				"DB object to use for Id generation (if you want to write to VE)"));
		// disable id factories for new entities (will be enabled if a virtual entity is chosen)
		cbxIdFactory.setEnabled(false);

		lbCache = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.9", "Entit\u00e4t cachen"));
		cbCache = new JCheckBox();
		cbCache.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.9", "Entit\u00e4t cachen"));

		lbTableName = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.10", "Tabellenname"));
		lbTableNamePrefix = new JLabel();
		
		tfTableName = new JTextField();
		tfTableName.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.10", "Tabellenname"));
		tfTableName.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
		tfTableName.setDocument(new LimitDocument(26));

		lbInternalEntityName = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.11", "Interner Entity-Name"));
		tfInternalEntityName = new JTextField();
		tfInternalEntityName.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.11", "Interner Entity-Name"));

		lbCloneGenerator = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.21", "Objektgenerator für Klonen"));
		cbxCloneGenerator = new JComboBox();
		cbxCloneGenerator.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.tooltip.21", "Objektgenerator für Klonen"));

		lbIcon = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.3", "Icon"));
		nuclosIconChooser = new ResourceIconChooser(NuclosResourceCategory.ENTITY_ICON);
		
		lbMandatorLevel = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.22", "Mandantenabhängigket"));
		cbxMandatorLevel = new JComboBox();
		lbMandatorInitialFill = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.23", "Mandanten Erstbefüllung mit"));
		cbxMandatorInitialFill = new JComboBox();
		lbMandatorInitialFill.setEnabled(false);
		cbxMandatorInitialFill.setEnabled(false);
		lbMandatorUnique = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitycommonproperties.25", "Eindeutig pro Mandant?"));
		cbMandatorUnique = new JCheckBox();
		lbMandatorUnique.setEnabled(false);
		cbMandatorUnique.setEnabled(false);

		double size [][] = {{500,10, TableLayout.FILL, 20}, {20, TableLayout.FILL}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);

		final JPanel pnlTableName = new JPanel();
		pnlTableName.setLayout(new BorderLayout(0,0));
		pnlTableName.add(lbTableNamePrefix, BorderLayout.WEST);
		pnlTableName.add(tfTableName, BorderLayout.CENTER);

		final JPanel jpnProperties = new JPanel();
		jpnProperties.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		TableLayoutBuilder tbllay = new TableLayoutBuilder(jpnProperties)
				.columns(170, TableLayout.FILL, 20, 5)
				.gaps(5, 3);

		tbllay.newRow(20).add(lbLabelSingular).addFullSpan(tfLabelSingular);
		tbllay.newRow(20).add(lbTableName).addFullSpan(pnlTableName);
		tbllay.newRow(20).add(lbInternalEntityName).addFullSpan(tfInternalEntityName);
		tbllay.newRow(15);
		tbllay.newRow(20).add(lbEditable).addFullSpan(cbEditable);
		tbllay.newRow(20).add(lbVirtual).addFullSpan(cbxVirtual);
		tbllay.newRow(20).add(lblWriteProxy).addFullSpan(cbWriteProxy);

		tbllay.newRow(20).add(lbIdFactory).addFullSpan(cbxIdFactory);
		tbllay.newRow(20).add(lbGeneric).addFullSpan(cbGeneric);
		tbllay.newRow(20).add(lbProxy).addFullSpan(cbProxy);
		
		tbllay.newRow(20).add(lbProxyInterface).addFullSpan(tfProxyInterface);
		tbllay.newRow(20).add(lbThin).addFullSpan(cbThin);
		tbllay.newRow(15);
		tbllay.newRow(20).add(lbStateModel).addFullSpan(cbStateModel);
		tbllay.newRow(20).add(lbSystemIdPrefix).addFullSpan(tfSystemIdPrefix);
		tbllay.newRow(20).add(lbShowRelation).addFullSpan(cbShowRelation);
		tbllay.newRow(20).add(lbShowGroups).addFullSpan(cbShowGroups);
		tbllay.newRow(15);
		tbllay.newRow(20).add(lbMenupath).addFullSpan(cbMenupath);
		tbllay.newRow(20).add(lbAccelerator).add(cbxModifier).add(tfMnemonic);
		tbllay.newRow(20).add(lbSearchable).addFullSpan(cbSearchable);
		tbllay.newRow(20).add(lbShowSearch).addFullSpan(cbShowSearch);
		tbllay.newRow(20).add(lbResultDetailsSplit).add(cbResultDetailsSplit);
		tbllay.newRow(20).add(lbIconCustom).add(cbxIcon).add(btNewIcon);
		tbllay.newRow(15);
		tbllay.newRow(20).add(lbLogbook).addFullSpan(cbLogbook);
		tbllay.newRow(20).add(lbMandatorLevel).addFullSpan(cbxMandatorLevel);
		tbllay.newRow(20).add(lbMandatorInitialFill).addFullSpan(cbxMandatorInitialFill);
		tbllay.newRow(20).add(lbMandatorUnique).addFullSpan(cbMandatorUnique);
		tbllay.newRow(20).add(lbCloneGenerator).addFullSpan(cbxCloneGenerator);
		tbllay.newRow(80).add(lbComment, 1, TableLayout.LEFT, TableLayout.TOP).addFullSpan(scpComment);
		lbComment.setBorder(BorderFactory.createEmptyBorder(3,1,0,0));

		this.add(lbIcon, "2,0");
		this.add(nuclosIconChooser, "2,1");

		JScrollPane scrollProperties = new JScrollPane(jpnProperties,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollProperties.getVerticalScrollBar().setUnitIncrement(20);
		scrollProperties.setBorder(BorderFactory.createEmptyBorder());

		this.add(scrollProperties, "0,0, 0,1");

		fillIconCombobox();
		
		cbLogbook.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setLogbook(cb.isSelected());
			}
		});

		cbSearchable.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setSearchable(cb.isSelected());
				handleShowSearch(cb.isSelected());
			}
		});
		
		cbShowSearch.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setShowSearch(cb.isSelected());
			}
		});


		cbEditable.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setEditable(cb.isSelected());
			}
		});
		
		cbResultDetailsSplit.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setResultDetailsSplit(cb.isSelected());				
			}
		});

		cbThin.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setThin(cb.isSelected());
			}
		});
		
		cbStateModel.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setStateModel(cb.isSelected());
				final boolean selected = cb.isSelected();
				if(selected) {
					lbSystemIdPrefix.setText(SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.entitycommonproperties.13", "K\u00fcrzel f\u00fcr Identifizierer *:"));
					if(tfSystemIdPrefix.getText().isEmpty()){
						NuclosEntityCommonPropertiesStep.this.setComplete(false);
					}
					else {
						NuclosEntityCommonPropertiesStep.this.setComplete(true);
					}
				}
				else {
					lbSystemIdPrefix.setText(SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.entitycommonproperties.12", "K\u00fcrzel f\u00fcr Identifizierer:"));
					NuclosEntityCommonPropertiesStep.this.setComplete(true);
				}
				lbSystemIdPrefix.setEnabled(selected);
				tfSystemIdPrefix.setEnabled(selected);
				lbShowGroups.setEnabled(selected);
				cbShowGroups.setEnabled(selected);
				lbShowRelation.setEnabled(selected);
				cbShowRelation.setEnabled(selected);

				// in case of a state entity, no virtual entity is possible
				lbVirtual.setEnabled(!selected);
				cbxVirtual.setEnabled(!selected);
				lbIdFactory.setEnabled(!selected);
				cbxIdFactory.setEnabled(!selected);
			}
		});

		cbCache.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setCachable(cb.isSelected());
			}
		});

		cbShowRelation.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setShowRelation(cb.isSelected());
			}
		});

		cbShowGroups.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setShowGroups(cb.isSelected());
			}
		});

		cbStateModel.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityCommonPropertiesStep.this.model.setStateModel(cb.isSelected());
			}
		});

		tfLabelSingular.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				doSomeWork();
			}

			@Override
			public void keyReleased(KeyEvent e) {
				doSomeWork();
			}

			@Override
			public void keyPressed(KeyEvent e) {
				doSomeWork();
			}

			protected void doSomeWork() {
				blnSingularModified = true;
			}

		});

		txtComment.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomething(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomething(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomething(e);
			}
			
			private void doSomething(DocumentEvent e) {
				try {
					final String comment = e.getDocument().getText(0, e.getDocument().getLength());
					NuclosEntityCommonPropertiesStep.this.model.setComment(comment);					
				}  catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
			}
		});
		
		tfLabelSingular.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			protected void doSomeWork(DocumentEvent e) {
				int size = e.getDocument().getLength();
				if(size > 0) {
					blnSingular = true;
				}
				else  {
					blnSingular = false;

				}
				if(blnSingular && (blnSystemId || !model.isStateModel())) {
					NuclosEntityCommonPropertiesStep.this.setComplete(true);
				}
				else {
					NuclosEntityCommonPropertiesStep.this.setComplete(false);
				}
				try {
					final String label = e.getDocument().getText(0, e.getDocument().getLength());
					if (!NuclosEntityCommonPropertiesStep.this.model.isEditMode() 
							|| !LangUtils.equal(NuclosEntityCommonPropertiesStep.this.model.getLabelSingular(), label)) {
						for (TranslationVO translationVO : NuclosEntityCommonPropertiesStep.this.model.getTranslation()) {
							translationVO.getLabels().put("titel", label);
						}
					}
					NuclosEntityCommonPropertiesStep.this.model.setLabelSingular(label);
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
			}
		});

		tfTableName.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			protected void doSomeWork(DocumentEvent e) {
				try {
					NuclosEntityCommonPropertiesStep.this.model.setTableOrViewName(e.getDocument().getText(0, e.getDocument().getLength()));
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
			}
		});

		tfInternalEntityName.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			protected void doSomeWork(DocumentEvent e) {
				try {
					NuclosEntityCommonPropertiesStep.this.model.setModifiedEntityName(e.getDocument().getText(0, e.getDocument().getLength()));
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
			}
		});

		tfMnemonic.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			protected void doSomeWork(DocumentEvent e) {
				try {
					NuclosEntityCommonPropertiesStep.this.model.setAccelerator(e.getDocument().getText(0, 1));
				} catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
			}
		});

		cbxIcon.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(final ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (isNotifyIconSelectionChange()) {
						setNotifyIconSelectionChange(false);
						nuclosIconChooser.clearSelection();
						setNotifyIconSelectionChange(true);
					}
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							final Object obj = e.getItem();
							if(obj == null) {
								NuclosEntityCommonPropertiesStep.this.model.setResourceUID(null);
								NuclosEntityCommonPropertiesStep.this.model.setResourceName(null);
							} else {
								UIDPresentation vo = (UIDPresentation)obj;
								NuclosEntityCommonPropertiesStep.this.model.setResourceUID(vo.getUid());
								NuclosEntityCommonPropertiesStep.this.model.setResourceName(vo.getLabel());
							}
						}
					});
				}

			}
		});

		nuclosIconChooser.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(final ListSelectionEvent e) {
				if (isNotifyIconSelectionChange()) {
					setNotifyIconSelectionChange(false);
					cbxIcon.setSelectedIndex(0);
					setNotifyIconSelectionChange(true);
				}
				NuclosEntityCommonPropertiesStep.this.model.setNuclosResourceName(nuclosIconChooser.getSelectedResourceIconName());
			}
		});

		cbxModifier.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					String sModifier = (String)e.getItem();
					Integer iModifier = ModifierMap.getModifierMap().get(sModifier);
					NuclosEntityCommonPropertiesStep.this.model.setModifier(iModifier);
				}
			}
		});

		((JTextField)cbMenupath.getEditor().getEditorComponent()).addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				doSomeWork();
			}

			@Override
			public void keyReleased(KeyEvent e) {
				doSomeWork();
			}

			@Override
			public void keyPressed(KeyEvent e) {
				doSomeWork();
			}

			protected void doSomeWork() {
				blnMenuPathModified = true;
				String str = ((JTextField)cbMenupath.getEditor().getEditorComponent()).getText();
				lbSearchable.setEnabled(str.length() > 0 && !model.isProxy() && !model.isGeneric());
				cbSearchable.setEnabled(str.length() > 0 && !model.isProxy() && !model.isGeneric());
				handleShowSearch(cbSearchable.isEnabled());
			}

		});

		cbMenupath.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					String s = (String)e.getItem();
					lbSearchable.setEnabled(s != null && s.length() != 0 && !model.isProxy() && !model.isGeneric());
					cbSearchable.setEnabled(s != null && s.length() != 0 && !model.isProxy() && !model.isGeneric());
					handleShowSearch(cbSearchable.isSelected());
				}
			}
		});

		btNewIcon.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					 final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
					 ResourceCollectController rcc = factory.newResourceCollectController(null);
					 rcc.addResouceSaveListener(NuclosEntityCommonPropertiesStep.this);
					 rcc.runNew();
				}
				catch(NuclosBusinessException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
				catch(CommonPermissionException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
				catch(CommonFatalException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
				catch(CommonBusinessException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, ex);
				}
			}
		});

		tfSystemIdPrefix.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			protected void doSomeWork(DocumentEvent e) {
				if(e.getDocument().getLength() < 1) {
					if(model.isStateModel()) {
						blnSystemId = false;
					} else {
						blnSystemId = true;
					}
				} else {
					blnSystemId = true;
				}
				if(blnSingular && blnSystemId) {
					NuclosEntityCommonPropertiesStep.this.setComplete(true);
				}
				else {
					NuclosEntityCommonPropertiesStep.this.setComplete(false);
				}

				try {
					NuclosEntityCommonPropertiesStep.this.getModel().setSystemIdPrefix(e.getDocument().getText(0, e.getDocument().getLength()));
				}
				catch(BadLocationException e1) {
					LOG.warn("doSomeWork failed: " + e1, e1);
				}
			}

		});

		cbxVirtual.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				final String item = (String) e.getItem();
				final boolean empty = StringUtils.isNullOrEmpty(item);
				final boolean idEmpty = StringUtils.isNullOrEmpty((String) cbxIdFactory.getSelectedItem());

				cbEditable.setSelected(empty || !idEmpty);
				cbEditable.setEnabled(empty);

				model.setEditable(cbEditable.isSelected());
				cbLogbook.setSelected(empty);
				cbLogbook.setEnabled(empty);
				
				//NUCLOS-5291
				if(!empty)
				{
				cbProxy.setSelected(false);
				cbProxy.setEnabled(false);
				cbGeneric.setSelected(false);
				cbGeneric.setEnabled(false);
				cbEditable.setSelected(false);
				cbEditable.setEnabled(false);
				cbStateModel.setSelected(false);
				cbStateModel.setEnabled(false);
				cbWriteProxy.setSelected(false);
				cbWriteProxy.setEnabled(true);
				}else
				{
					cbProxy.setEnabled(true);
					cbGeneric.setEnabled(true);
					cbEditable.setEnabled(true);
					cbStateModel.setEnabled(true);
					cbWriteProxy.setEnabled(false);
				}
				
				model.setEditable(cbLogbook.isSelected());
				cbCache.setSelected(empty);
				cbCache.setEnabled(empty);
				model.setCachable(cbCache.isSelected());
				cbStateModel.setEnabled(empty);

				// Modify table name (tp)
				if (empty) {
					model.setVirtualentity(null);
					tfTableName.setText(model.getTableOrViewName());
				}
				else {
					model.setVirtualentity(item);
					tfTableName.setText(null);
				}

				// id factory is only possible for virtual entities
				cbxIdFactory.setEnabled(!empty);
				if (empty) {
					cbxIdFactory.setSelectedIndex(0);
				}
			}
		});
		cbxIdFactory.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				final String item = (String) e.getItem();
				final boolean empty = StringUtils.isNullOrEmpty(item);
				final boolean veEmpty = StringUtils.isNullOrEmpty((String) cbxVirtual.getSelectedItem());
				cbEditable.setSelected(!empty || veEmpty);
			}
		});
		ActionListener alProxyOrGeneric = new ActionListener() {
			// only editable for new entities.
			
			// store old values
			private String tablename = "";
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean isProxy = cbProxy.isSelected();				
				boolean isGeneric = cbGeneric.isSelected();
				
				model.setProxy(isProxy);
				model.setGeneric(isGeneric);
				if (isProxy) {
					cbGeneric.setEnabled(false);
				}
				if (isGeneric) {
					cbProxy.setEnabled(false);
				}
				if (!isProxy && !isGeneric) {
					cbProxy.setEnabled(true);
					cbGeneric.setEnabled(true);
				}
				if (isProxy || isGeneric) {
					tablename = tfTableName.getText();
					tfTableName.setText(isProxy ? "[proxy]" : "[generic]");
					tfSystemIdPrefix.setText(null);
					if (cbxVirtual.getSelectedIndex() > 0) {
						cbxVirtual.setSelectedIndex(0);
					}
					if (cbxIdFactory.getSelectedIndex() > 0) {
						cbxIdFactory.setSelectedIndex(0);
					}
					if (cbxCloneGenerator.getSelectedIndex() > 0) {
						cbxCloneGenerator.setSelectedIndex(0);
					}
					cbLogbook.setSelected(false);
					cbCache.setSelected(false);
					cbStateModel.setSelected(false);
					cbSearchable.setSelected(false);
					cbShowGroups.setSelected(false);
					cbShowRelation.setSelected(false);
					cbMenupath.setSelectedItem(null);
					if (isProxy ) {
						String proxyInterface = SpringApplicationContextHolder.getBean(EventSupportFacadeRemote.class).
								getEntityProxyInterfaceQualifiedName(
										NuclosEntityCommonPropertiesStep.this.model.getEntityName(), NuclosEntityCommonPropertiesStep.this.model.getNucletUID());
						tfProxyInterface.setText(proxyInterface);
					} else {
						tfProxyInterface.setText(null);
					}
				} else {
					tfTableName.setText(tablename);
					tfProxyInterface.setText(null);
					NuclosEntityCommonPropertiesStep.this.model.setProxyInterface(null);
				}
				handleEnable(isProxy, isGeneric);
			}
		};
		cbProxy.addActionListener(alProxyOrGeneric);
		cbGeneric.addActionListener(alProxyOrGeneric);
		
		cbWriteProxy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean isWriteProxy = cbWriteProxy.isSelected();
				model.setWriteProxy(isWriteProxy);
				
				if (isWriteProxy) {					
					String proxyInterface = SpringApplicationContextHolder.getBean(EventSupportFacadeRemote.class).
							getEntityProxyInterfaceQualifiedName(
									NuclosEntityCommonPropertiesStep.this.model.getEntityName(), NuclosEntityCommonPropertiesStep.this.model.getNucletUID());
					cbxIdFactory.setEnabled(false);
					cbxIdFactory.setSelectedItem(null);			
					tfProxyInterface.setText(proxyInterface);
					cbEditable.setEnabled(false);
					cbEditable.setSelected(true);
					
				}else
				{
					cbxIdFactory.setEnabled(true);								
					tfProxyInterface.setText(null);
					cbEditable.setEnabled(false);
					cbEditable.setSelected(false);
				}
			}
		});
		cbxMandatorLevel.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				MandatorLevelVO level = (MandatorLevelVO) cbxMandatorLevel.getSelectedItem();
				NuclosEntityCommonPropertiesStep.this.model.setMandatorLevel(level==null?null:level.getUID());
				boolean bMandatorUnique = level != null; // enabled for new entities
				if (model.isEditMode()) {
					bMandatorUnique = bMandatorUnique && NuclosEntityCommonPropertiesStep.this.model.isMandatorUnique(); // edit mode = get value from model
				}
				lbMandatorUnique.setEnabled(level != null);
				cbMandatorUnique.setEnabled(level != null);
				cbMandatorUnique.setSelected(bMandatorUnique);
				enableMandatorInitialFill(NuclosEntityCommonPropertiesStep.this.model.getMandatorLevel(), NuclosEntityCommonPropertiesStep.this.model.getMandatorInitialFill());
				if (model.isEditMode() && !model.isVirtual() && level != null) {
					enableMandatorInitialFill(level.getUID(), NuclosEntityCommonPropertiesStep.this.model.getMandatorInitialFill());
				} else {
					enableMandatorInitialFill(null, null);
				}

			}
		});
		cbxMandatorInitialFill.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				MandatorVO mandator = (MandatorVO) cbxMandatorInitialFill.getSelectedItem();
				NuclosEntityCommonPropertiesStep.this.model.setMandatorInitialFill(mandator==null?null:mandator.getUID());
			}
		});
	}
	
	private void enableMandatorInitialFill(UID levelUID, UID mandatorInitialFill) {
		if (levelUID != null) {
			MandatorVO selected = null;
			List<MandatorVO> mandatorsByLevel = new ArrayList<MandatorVO>(SecurityDelegate.getInstance().getMandatorsByLevel(levelUID));
			if (mandatorInitialFill != null) {
				for (MandatorVO mandator : mandatorsByLevel) {
					if (mandator.getUID().equals(mandatorInitialFill)) {
						selected = mandator;
					}
				}
			}
			mandatorsByLevel.add(0, null);
			lbMandatorInitialFill.setEnabled(true);
			cbxMandatorInitialFill.setEnabled(true);
			cbxMandatorInitialFill.setModel(new DefaultComboBoxModel(mandatorsByLevel.toArray(new MandatorVO[]{})));
			if (selected != null) {
				cbxMandatorInitialFill.setSelectedItem(selected);
			}
		} else {
			lbMandatorInitialFill.setEnabled(false);
			cbxMandatorInitialFill.setEnabled(false);
			cbxMandatorInitialFill.setModel(new DefaultComboBoxModel());
		}
	}
	
	private void handleEnable(boolean isProxy, boolean isGeneric) {
		boolean isProxyOrGeneric = isProxy || isGeneric;
		lbTableNamePrefix.setVisible(!isProxyOrGeneric);
		tfTableName.setEnabled(!isProxyOrGeneric);
		lbTableName.setEnabled(!isProxyOrGeneric);
		cbxVirtual.setEnabled(!isProxyOrGeneric);
		lbVirtual.setEnabled(!isProxyOrGeneric);
		cbxIdFactory.setEnabled(!isProxyOrGeneric);
		lbIdFactory.setEnabled(!isProxyOrGeneric);
		cbxCloneGenerator.setEnabled(!isProxyOrGeneric);
		lbCloneGenerator.setEnabled(!isProxyOrGeneric);
		cbLogbook.setEnabled(!isProxyOrGeneric);
		lbLogbook.setEnabled(!isProxyOrGeneric);
		cbCache.setEnabled(!isProxyOrGeneric);
		lbCache.setEnabled(!isProxyOrGeneric);
		cbStateModel.setEnabled(!isProxyOrGeneric);
		lbStateModel.setEnabled(!isProxyOrGeneric);
		cbSearchable.setEnabled(!isProxyOrGeneric);
		lbSearchable.setEnabled(!isProxyOrGeneric);
		cbMenupath.setEnabled(!isProxyOrGeneric);
	}
	
	private void handleShowSearch(boolean searchOn) {
		if (!searchOn) {
			cbShowSearch.setSelected(false);
		}
		cbShowSearch.setEnabled(searchOn);
		lbShowSearch.setEnabled(searchOn);
	}

	protected void fillIconCombobox() {
		final List<MasterDataVO<?>> lstIcon = new ArrayList<MasterDataVO<?>>(MasterDataCache.getInstance().get(E.RESOURCE.getUID()));
		Collections.sort(lstIcon, new Comparator<MasterDataVO<?>>() {
			@Override
            public int compare(MasterDataVO<?> o1, MasterDataVO<?> o2) {
	            String sField1 = o1.getFieldValue(E.RESOURCE.name);
	            String sField2 = o2.getFieldValue(E.RESOURCE.name);
	            return sField1.toUpperCase().compareTo(sField2.toUpperCase());
            }
		});


		cbxIcon.addItem(null);
		for(MasterDataVO vo : lstIcon) {
			cbxIcon.addItem(new UIDPresentation((UID)vo.getPrimaryKey(), vo.getFieldValue(E.RESOURCE.name).toString()));
		}

	}

	@Override
	public void close() {
		lbLabelSingular = null;
		tfLabelSingular = null;
		lbMenupath = null;
		cbMenupath = null;
		lbSystemIdPrefix = null;
		tfSystemIdPrefix = null;
		lbIconCustom = null;
		cbxIcon = null;
		btNewIcon = null;

		lbAccelerator = null;
		cbxModifier = null;
		tfMnemonic = null;

		lbLogbook = null;
		cbLogbook = null;
		lbSearchable = null;
		cbSearchable = null;
		lbShowSearch = null;
		cbShowSearch = null;
		lbEditable = null;
		cbEditable = null;
		cbResultDetailsSplit = null;
		lbThin = null;
		cbThin = null;

		lbShowRelation = null;
		cbShowRelation = null;
		lbShowGroups = null;
		cbShowGroups = null;

		lbStateModel = null;
		cbStateModel = null;

		lbVirtual = null;
		cbxVirtual = null;
		lbIdFactory = null;
		cbxIdFactory = null;
		
		lbProxy = null;
		cbProxy = null;
		lblWriteProxy=null;
		cbWriteProxy=null;
		lbProxyInterface = null;
		tfProxyInterface = null;
		lbGeneric = null;
		cbGeneric = null;
		
		lbMandatorLevel = null;
		cbxMandatorLevel = null;
		lbMandatorInitialFill = null;
		cbxMandatorInitialFill = null;
		lbMandatorUnique = null;
		cbMandatorUnique = null;

		lbCache = null;
		cbCache = null;
		lbTableName = null;
		tfTableName = null;
		lbInternalEntityName = null;
		tfInternalEntityName = null;

		lbIcon = null;
		nuclosIconChooser = null;

		txtComment = null;
		
		super.close();
	}

	@Override
    public void applyState() throws InvalidStateException {
		if (!LangUtils.equal(model.getMenuPath(), cbMenupath.getSelectedItem())) {
			for (TranslationVO translationVO : model.getTranslation()) {
				translationVO.getLabels().put("menupath", (String)cbMenupath.getSelectedItem());
			}
		}
	    model.setMenuPath((String)cbMenupath.getSelectedItem());
	    model.setVirtualentity((String)cbxVirtual.getSelectedItem());
	    model.setIdFactory((String) cbxIdFactory.getSelectedItem());
	    model.setProxy(cbProxy.isSelected());
	    model.setWriteProxy(cbWriteProxy.isSelected());
		model.setGeneric(cbGeneric.isSelected());
	    
	    MandatorLevelVO level = (MandatorLevelVO) cbxMandatorLevel.getSelectedItem();
		model.setMandatorLevel(level==null?null:level.getUID());
		MandatorVO mandatorInitialFill = (MandatorVO) cbxMandatorInitialFill.getSelectedItem();
		model.setMandatorInitialFill(mandatorInitialFill==null?null:mandatorInitialFill.getUID());
		model.setMandatorUnique(cbMandatorUnique.isSelected());

	    String sTable = tfTableName.getText();
	    if (!model.isProxy() && !model.isGeneric()) {
		    for(EntityMeta<?> vo : MetaProvider.getInstance().getAllEntities()) {
		    	if(sTable.equalsIgnoreCase(vo.getDbTable())) {
		    		if(this.model.getUID().equals(vo.getUID())) {
		    			continue;
					}
		    		JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMessage(
		    				"wizard.step.entitycommonproperties.18", 
		    				"Der vergebene Tabellenname ist schon vorhanden. Bitte ändern Sie ihn in den erweiterten Einstellungen!"),
		    				SpringLocaleDelegate.getInstance().getMessage("wizard.step.entitycommonproperties.19", "Achtung!"), 
		    				JOptionPane.OK_OPTION);
		 	        throw new InvalidStateException();
		    	}
		    }
	    }
	    
	    if(model.isVirtual()) {
	    	// initialize/update attributes from table/view metadata
	    	final List<String> staticfields = CollectionUtils.asList(VIRTUAL_STATICFIELDS);
	    	final List<FieldMeta<?>> attributes;
	    	Map<String, FieldMeta<?>> fields = new HashMap<String, FieldMeta<?>>();
	    	if (model.isEditMode()) {
	    		attributes = new ArrayList<FieldMeta<?>>();
		    	fields = new HashMap<String, FieldMeta<?>>();
		    	for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(model.getUID()).values()) {
		    		if (staticfields.contains(field.getDbColumn().toUpperCase())) {
		    			continue;
		    		}
		    		fields.put(field.getDbColumn().toUpperCase(), field);
		    	}
		    	for (FieldMeta<?> dbattribute : MetaDataDelegate.getInstance().getVirtualEntityFields(model.getVirtualentity())) {
		    		String name = dbattribute.getDbColumn().toUpperCase();
		    		if (fields.containsKey(name)) {
		    			FieldMetaVO<?> field = new FieldMetaVO(fields.get(name));
		    			if (!"org.nuclos.common.NuclosImage".equals(field.getDataType())) {
		    				field.setDataType(dbattribute.getDataType());
		    				field.setScale(dbattribute.getScale());
		    				field.setPrecision(dbattribute.getPrecision());
		    			}
		    			attributes.add(field);
		    			fields.remove(name);
		    		}
		    		else if (name.startsWith("INTID_") && fields.containsKey(name.replaceFirst("^INTID_", "STRVALUE_"))) {
		    			name = name.replaceFirst("^INTID_", "STRVALUE_");
		    			// was defined as reference field
		    			FieldMeta<?> field = fields.get(name);
		    			attributes.add(field);
		    			fields.remove(name);
		    		} else {
		    			attributes.add(dbattribute);
		    		}
		    	}
	    	}
	    	else {
	    		attributes = MetaDataDelegate.getInstance().getVirtualEntityFields(model.getVirtualentity());
	    	}
	    	EntityAttributeTableModel attributeModel = new EntityAttributeTableModel(model);
			for(FieldMeta<?> attribute : attributes) {
				String dbcolumn = attribute.getDbColumn().toUpperCase();
				if (staticfields.contains(dbcolumn)) {
					staticfields.remove(dbcolumn);
	    			continue;
	    		}
				try {
					Attribute attr = NuclosEntityNameStep.wrapEntityMetaFieldVO(attribute, false);
					attr.setReadonly(!model.isEditable());
					if (StringUtils.isNullOrEmpty(attr.getLabel())) {
						attr.setLabel(attribute.getFieldName());
					}
					if (StringUtils.isNullOrEmpty(attr.getDescription())) {
						attr.setDescription(attribute.getFieldName());
					}
					if (dbcolumn.startsWith("INTID_") || dbcolumn.startsWith("STRUID_")) {
						attr.setDatatyp(DataTyp.getReferenzTyp());
					}
					attributeModel.addAttribute(attr);
				}
				catch(CommonFinderException | CommonPermissionException e1) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, e1);
				}
			}
			for (FieldMeta<?> attribute : fields.values()) {
				try {
					final Attribute attr = NuclosEntityNameStep.wrapEntityMetaFieldVO(attribute, false);
					attr.setReadonly(!model.isEditable());
					attributeModel.addAttribute(attr);
					attributeModel.removeRow(attributeModel.getRowCount() - 1, true);
				}
				catch(Exception e1) {
					Errors.getInstance().showExceptionDialog(NuclosEntityCommonPropertiesStep.this, e1);
				}
			}
			if (staticfields.size() > 0) {
				String message = SpringLocaleDelegate.getInstance().getMessage("wizard.step.entitycommonproperties.error.virtual.fields", 
						"Virtual entity is missing required attributes ({0}).", 
						StringUtils.join(", ", staticfields));
				JOptionPane.showMessageDialog(this, message, SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.entitycommonproperties.19", "Achtung!"), 
						JOptionPane.OK_OPTION);
		 	    throw new InvalidStateException();
			}
			else {
				// check if select is possible
				try {
					EntityMetaVO<Long> virtualentity = new EntityMetaVO(Long.class);
					virtualentity.setUID(model.getUID());
					virtualentity.setVirtualEntity(model.getVirtualentity());
					virtualentity.setIdFactory(model.getIdFactory());
					virtualentity.setEntityName(model.getEntityName());
					virtualentity.setDbTable(model.getVirtualentity());
					virtualentity.setFields(attributes);
					model.setTableOrViewName(model.getVirtualentity());
					MetaDataDelegate.getInstance().tryVirtualEntitySelect(virtualentity);
				}
				catch (NuclosBusinessException ex) {
					String message = SpringLocaleDelegate.getInstance().getMessageFromResource(ex.getMessage());
					JOptionPane.showMessageDialog(this, message, SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.entitycommonproperties.19", "Achtung!"), 
							JOptionPane.OK_OPTION);
			 	    throw new InvalidStateException(ex.getMessage(), ex);
				}
				model.setAttributeModel(attributeModel);
			}
		} else {
			// not virtual
			if (model.isEditMode() && model.getMandatorLevel() != null && model.getMandatorLevelUnchanged() == null && model.getMandatorInitialFill() == null) {
				try {
					long count = EntityObjectDelegate.getInstance().countEntityObjectRows(model.getUID(), new CollectableSearchExpression());
					if (count > 0) {
						String message = SpringLocaleDelegate.getInstance().getMessage("wizard.step.entitycommonproperties.error.fill.mandator",
								"Die Mandantenabhängigkeit benötigt einen Mandanten für die Erstbefüllung.");
						JOptionPane.showMessageDialog(this, message, SpringLocaleDelegate.getInstance().getMessage(
								"wizard.step.entitycommonproperties.19", "Achtung!"),
								JOptionPane.OK_OPTION);
						throw new InvalidStateException();
					}
				} catch (CommonPermissionException cpe) {
					// Should never happen for superuser
					throw new NuclosFatalException(cpe);
				}
			}
		}

	    super.applyState();
    }


	@Override
	public void fireSaveEvent(Collectable clt, ResourceCollectController clct) {
		if(clt != null) {
			CollectableMasterDataWithDependants mdwd = (CollectableMasterDataWithDependants)clt;
			MasterDataVO vo = mdwd.getMasterDataCVO();
			UIDPresentation uidp = new UIDPresentation((UID)vo.getPrimaryKey(), vo.getFieldValue(E.RESOURCE.name).toString());
			cbxIcon.addItem(uidp);
			cbxIcon.setSelectedItem(uidp);
		}
		clct.getTab().close();
		this.requestFocusInWindow();
	}

	private class LimitDocument extends PlainDocument {

		private int max;

		public LimitDocument(int max) {
	        this.max = max;
        }

		@Override
		public void insertString(int offs, String str, AttributeSet a)	throws BadLocationException {
			if(str == null) {
				return;
			}
			if(getLength() + str.length() < max) {
				super.insertString(offs, str, a);
			}
		}
	}

	private class LimitSpecialCharacterDocument extends PlainDocument {

		private int max;

		public LimitSpecialCharacterDocument(int max) {
			this.max = max;
		}

		@Override
		public void insertString(int offs, String str, AttributeSet a)	throws BadLocationException {
			if(str == null)
				return;
			if(getLength() + str.length() + 1 < max) {
				str = org.apache.commons.lang.StringUtils.replace(str, "\u00c4", "AE");
				str = org.apache.commons.lang.StringUtils.replace(str, "\u00e4", "ae");
				str = org.apache.commons.lang.StringUtils.replace(str, "\u00d6", "OE");
				str = org.apache.commons.lang.StringUtils.replace(str, "\u00f6", "oe");
				str = org.apache.commons.lang.StringUtils.replace(str, "\u00dc", "UE");
				str = org.apache.commons.lang.StringUtils.replace(str, "\u00fc", "ue");
				str = org.apache.commons.lang.StringUtils.replace(str, "\u00df", "ss");
				str = str.replaceAll("[^\\w]", "");
				super.insertString(offs, str, a);
			}
		}
	}

	private boolean notifyIconSelectionChanged = true;
	private boolean isNotifyIconSelectionChange() {
		return notifyIconSelectionChanged;
	}
	private void setNotifyIconSelectionChange(boolean notifyIconSelectionChanged) {
		this.notifyIconSelectionChanged = notifyIconSelectionChanged;
	}

}
