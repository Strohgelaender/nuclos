package org.nuclos.client.wizard.steps;

import java.util.Collection;
import java.util.Collections;

import javax.swing.*;

import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModel;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.dal.vo.EntityObjectVO;

import info.clearthought.layout.TableLayout;

/**
 * Created by Oliver Brausch on 15.10.17.
 */
public abstract class NuclosEntityConfWithSubformAbstractStep extends NuclosEntityAbstractStep {

	protected SubForm subform;
	protected MasterDataSubFormController subFormController;

	private CollectableComponentModelProvider provider;

	public NuclosEntityConfWithSubformAbstractStep(String name, String summary) {
		super(name, summary);
	}

	@Override
	protected void initComponents() {
		double size [][] = {{TableLayout.FILL}, {TableLayout.FILL}};
		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		setLayout(layout);
	}

	protected final void fillSubformAndRequestFocus(Collection<EntityObjectVO<UID>> data) {
		if (data != null) {
			try {
				subFormController.clear();
				subFormController.fillSubForm(null, data);
			} catch (NuclosBusinessException e) {
				throw new NuclosFatalException(e);
			}
		}
		setComplete(true);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				subform.requestFocusInWindow();
			}
		});
	}

	protected final CollectableComponentModelProvider getCollectableComponentModelProvider() {
		if (provider == null) {
			provider = new CollectableComponentModelProvider() {
				private CollectableComponentModel entityModel = new DetailsComponentModel(DefaultCollectableEntityProvider.getInstance().getCollectableEntity(E.ENTITY.getUID()).getEntityField(E.ENTITY.entity.getUID()));

				@Override
				public Collection<UID> getFieldUids() {
					return Collections.singleton(E.ENTITY.entity.getUID());
				}

				@Override
				public Collection<? extends CollectableComponentModel> getCollectableComponentModels() {
					return Collections.singleton(entityModel);
				}

				@Override
				public CollectableComponentModel getCollectableComponentModelFor(UID fieldUID) {
					if (E.ENTITY.entity.getUID().equals(fieldUID)) {
						return entityModel;
					}
					return null;
				}
			};
		}
		return provider;
	}

	@Override
	public void close() {
		if (subFormController != null) {
			subFormController.close();
		}
		subFormController = null;
		if (subform != null) {
			subform.close();
		}
		subform = null;

		super.close();
	}

}
