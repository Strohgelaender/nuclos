//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.JOptionPane;
import javax.swing.JToolBar;

import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.pietschy.wizard.InvalidStateException;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*/
public class NuclosEntityProcessStep extends NuclosEntityConfWithSubformAbstractStep {

	private static final long serialVersionUID = 2900241917334839766L;

	public static final String[] labels = TranslationVO.LABELS_ENTITY;
	
	public NuclosEntityProcessStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	public void prepare() {
		super.prepare();
		this.removeAll();

		subform = new SubForm(E.PROCESS.getUID(), false, JToolBar.VERTICAL, UID.UID_NULL, E.PROCESS.module.getUID());

		Column column = new Column(E.PROCESS.nuclet.getUID(), "Nuclet", new CollectableComponentType(CollectableUtils.getCollectableComponentTypeForClass(String.class), null), false, false,false, false, false, false, 0, 0, null);
		subform.addColumn(column);
		this.add(subform, "0,0");

		MainFrameTab tab = getModel().getParentFrame();

		try {
			CollectableComponentModelProvider provider = getCollectableComponentModelProvider();
			Preferences prefs = java.util.prefs.Preferences.userRoot().node("org/nuclos/client/entitywizard/steps/process");

			subFormController = new ProcessSubformController(tab, provider,E.PROCESS.getUID(), subform, prefs,
					getEntityPreferences(), null);

			fillSubformAndRequestFocus(model.getProcesses());
		} catch (Exception e) {
			Errors.getInstance().showExceptionDialog(tab, e);
		}

	}

	@Override
	public void applyState() throws InvalidStateException {
		List<CollectableEntityObject> subformdata;
		try {
			subformdata = subFormController.getCollectables(true, true, true);
		} catch (CommonValidationException e1) {
			JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMessageFromResource(e1.getMessage()),
	    			SpringLocaleDelegate.getInstance().getMessage("wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
 	        throw new InvalidStateException();
		}
		Collection<EntityObjectVO<UID>> processes = CollectionUtils.transform(subformdata, new Transformer<CollectableEntityObject, EntityObjectVO<UID>>() {
			@Override
			public EntityObjectVO<UID> transform(CollectableEntityObject i) {
				final EntityObjectVO<UID> eo = i.getEntityObjectVO();
				if (eo.getPrimaryKey() == null)
					eo.setPrimaryKey(new UID());
				return eo;
			}
		});
		// validate processes
		Set<String> names = new HashSet<String>();
		for (EntityObjectVO<UID> process : processes) {
			String name = process.getFieldValue(E.PROCESS.name);
			if (StringUtils.isNullOrEmpty(name)) {
				JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.processes.error.name.mandatory", "Bitte definieren Sie für jede Aktion einen Namen."),
		    			SpringLocaleDelegate.getInstance().getMessage("wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
	 	        throw new InvalidStateException();
			}
			if (!names.add(name)) {
				JOptionPane.showMessageDialog(this, SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.processes.error.name.unique", "Der Name einer Aktion muss eindeutig sein ({0}).", name),
		    			SpringLocaleDelegate.getInstance().getMessage("wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
	 	        throw new InvalidStateException();
			}
		}
		model.setProcesses(processes);
		subFormController.close();
		super.applyState();
		
		// close Subform support
		subform.close();
		subform = null;
		
		super.applyState();
	}

	private class ProcessSubformController extends MasterDataSubFormController<UID> {

		public ProcessSubformController(MainFrameTab tab, CollectableComponentModelProvider clctcompmodelproviderParent, UID parentEntity, SubForm subform, Preferences prefsUserParent, 
				EntityPreferences entityPrefs, CollectableFieldsProviderCache valueListProviderCache) {
			super(tab, clctcompmodelproviderParent, parentEntity, subform, prefsUserParent, entityPrefs, valueListProviderCache, null, null);
		}

		@Override
		protected void removeSelectedRows() {
			for (CollectableEntityObject<UID> process : this.getSelectedCollectables()) {
				if (process.getId() != null) {
					try {
						MetaDataDelegate.getInstance().tryRemoveProcess(process.getEntityObjectVO());
					}
					catch (NuclosBusinessException e) {
						JOptionPane.showMessageDialog(NuclosEntityProcessStep.this, 
								getSpringLocaleDelegate().getMessage(
										"wizard.step.processes.error.removeprocess", "Aktion {0} ist bereits in Verwendung und kann nicht entfernt werden.", process.getField(E.PROCESS.name.getUID())),
								getSpringLocaleDelegate().getMessage(
										"wizard.step.processes.error.title", "Achtung!"), JOptionPane.OK_OPTION);
						return;
					}
				}
			}
			super.removeSelectedRows();
		}
	}
}
