//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.util;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.text.JTextComponent;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.util.LangUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.main.Main;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.client.wizard.model.DataTyp;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.xml.sax.InputSource;


/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/

public class NuclosWizardUtils {

	private static final Logger LOG = Logger.getLogger(NuclosWizardUtils.class);

	public static final String COLUMN_PREFFIX = "c_";
	public static final String COLUMN_STRING_PREFFIX = "STR";
	public static final String COLUMN_INTEGER_PREFFIX = "INT";
	public static final String COLUMN_DATE_PREFFIX = "DAT";
	public static final String COLUMN_BOOLEAN_PREFFIX = "BLN";
	public static final String COLUMN_DOUBLE_PREFFIX = "DBL";
	public static final String COLUMN_OBJECT_PREFFIX = "OBJ";
	public static final String COLUMN_FILE_PREFFIX = "STRVALUE_";

	public static DataTyp getDataTyp(String javaType, String defaultComponentType, Integer scale, Integer precision, String inputFormat, String outputFormat) throws CommonFinderException, CommonPermissionException{
		DataTyp typ = null;
		Collection<MasterDataVO<?>> colMasterData = MasterDataCache.getInstance().get(E.DATATYPE.getUID());

		List<MasterDataVO<?>> lstVO = new ArrayList<MasterDataVO<?>>(colMasterData);
		Collections.sort(lstVO, new Comparator<MasterDataVO<?>>() {

			@Override
			public int compare(MasterDataVO<?> o1, MasterDataVO<?> o2) {
				return (o1.getFieldValue(E.DATATYPE.name)).compareTo(o2.getFieldValue(E.DATATYPE.name));
			}

		});

		for(MasterDataVO<?> vo : lstVO) {
			String strJavaTyp = vo.getFieldValue(E.DATATYPE.javatyp);
			String strDefaultComponentType = vo.getFieldValue(E.DATATYPE.defaultcomponenttype);
			String strOutputFormat = vo.getFieldValue(E.DATATYPE.outputformat);
			String strInputFormat = vo.getFieldValue(E.DATATYPE.inputformat);
			Integer iScale = vo.getFieldValue(E.DATATYPE.scale);
			if(iScale != null && iScale.intValue() == 0) {
				iScale = null;
			}
			Integer iPrecision = vo.getFieldValue(E.DATATYPE.precision);
			if(iPrecision != null && iPrecision.intValue() == 0) {
				iPrecision = null;
			}

			String strDatabaseTyp = vo.getFieldValue(E.DATATYPE.databasetyp);
			String strName = vo.getFieldValue(E.DATATYPE.name);
			if(strName.equals("Referenzfeld")) {
				continue;
			}
			if(strName.equals("Nachschlagefeld")) {
				continue;
			}

			if(StringUtils.equals(javaType, strJavaTyp) && StringUtils.equals(outputFormat, strOutputFormat) &&
					ObjectUtils.equals(scale, iScale) && ObjectUtils.equals(precision, iPrecision) && StringUtils.equals(defaultComponentType, strDefaultComponentType)) {
				typ = new DataTyp(strName, strInputFormat, strOutputFormat, strDatabaseTyp, iScale, iPrecision, strJavaTyp, strDefaultComponentType);
				break;
			}
		}
		if(typ == null) {
			Integer iScale = scale;
			// scale must be smaller than 10^9
			if (iScale != null && iScale.toString().length() > 9) {  
				iScale = null;
			}
			typ = new DataTyp(SpringLocaleDelegate.getInstance().getText("wizard.datatype.individual"),
					inputFormat, outputFormat, null, iScale, precision, javaType, defaultComponentType);
		}
		return typ;
	}

	public static Set<UID> searchParentEntity(UID entity) {
		Set<UID> setParents = new HashSet<UID>();

		for(MasterDataVO<?> vo : MasterDataCache.getInstance().get(E.LAYOUT.getUID())) {
			final LayoutMLParser parser = new LayoutMLParser();
			try {
	            final String sLayout = vo.getFieldValue(E.LAYOUT.layoutML);
	            if(sLayout == null) {
	            	continue;
	            }
				final Set<UID> setSubforms = parser.getSubFormEntityUids(new InputSource(new StringReader(sLayout)));
	            if(setSubforms.contains(entity)) {
					final CollectableComparison compare = SearchConditionUtils.newComparison(E.LAYOUTUSAGE.layout, ComparisonOperator.EQUAL, vo.getPrimaryKey());
					for(MasterDataVO<UID> voUsage : MasterDataDelegate.getInstance().<UID>getMasterData(E.LAYOUTUSAGE.getUID(), compare)) {
						setParents.add(voUsage.getFieldUid(E.LAYOUTUSAGE.entity));
					}
	            }
	            final Set<UID> setCharts = parser.getChartEntityUids(new InputSource(new StringReader(sLayout)));
	            if(setCharts.contains(entity)) {
					final CollectableComparison compare = SearchConditionUtils.newComparison(E.LAYOUTUSAGE.layout, ComparisonOperator.EQUAL, vo.getPrimaryKey());
					for(MasterDataVO<UID> voUsage : MasterDataDelegate.getInstance().<UID>getMasterData(E.LAYOUTUSAGE.getUID(), compare)) {
						setParents.add(voUsage.getFieldUid(E.LAYOUTUSAGE.entity));
					}
	            }
            }
            catch(LayoutMLException e) {
	            // do nothing here
            	LOG.info("searchParentEntity: " + e);
            }
		}
		return setParents;
	}

	public static FocusAdapter createWizardFocusAdapter() {
		return new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				if(e.getSource() instanceof JTextComponent) {
					JTextComponent tf = (JTextComponent)e.getSource();
					tf.setSelectionStart(0);
					tf.setSelectionEnd(tf.getText().length());
				}
			}

		};
	}

	public static UID getResourceUID(String sResource) {
		final Collection<MasterDataVO<?>> colVO = MasterDataCache.getInstance().get(E.RESOURCE.getUID());
		for(MasterDataVO<?> vo : colVO) {
			if(LangUtils.equals(vo.getFieldValue(E.RESOURCE.name), sResource)) {
				return (UID)vo.getPrimaryKey();
			}
		}
		return null;
	}

	public static String replace(String str) {
		if(str == null) {
			return str;
		}
		
		String s = str;
		s = StringUtils.replace(s, "\u00e4", "ae");
		s = StringUtils.replace(s, "\u00f6", "oe");
		s = StringUtils.replace(s, "\u00fc", "ue");
		s = StringUtils.replace(s, "\u00c4", "Ae");
		s = StringUtils.replace(s, "\u00d6", "Oe");
		s = StringUtils.replace(s, "\u00dc", "Ue");
		s = StringUtils.replace(s, "\u00df", "ss");
		s = s.replaceAll("[^\\w]", "");
		return s;
	}
	
	public static boolean isSystemField(String dbColumn) {
		for(SF<?> sfField : SF.getAllFields()) {
			if (sfField.getDbColumn().equalsIgnoreCase(dbColumn)) {
				return true;
			}
		}
		
		return false;
	}

	public static boolean isSystemField(UID entity, UID field) {
		if (entity == null || field == null) {
			return false;
		}
		return SF.isEOField(entity, field);
	}
	
	public static boolean isSystemField(EntityMeta<?> eMeta, FieldMeta<?> fMeta) {
		if (eMeta == null || fMeta == null) {
			return false;
		}
		return isSystemField(eMeta.getUID(), fMeta.getUID());
	}

	public static boolean isSystemField(FieldMeta<?> meta) {
		return isSystemField(meta.getEntity(), meta.getUID());
	}

	public static String getStapledString(String str){
		return "${" + org.nuclos.common2.StringUtils.emptyIfNull(str) + "}";
	}

	public static Collection<String> getExistingMenuPaths() {
		Collection<String> colMenuPaths = new ArrayList<String>();

		final Collection<EntityMeta<?>> colMetaData = MetaProvider.getInstance().getAllEntities();
		final Collection<EntityMeta<?>> colNonMetaData = new ArrayList<EntityMeta<?>>();
		for (EntityMeta<?> emdvo : colMetaData) {
			if (E.isNuclosEntity(emdvo.getUID())) {
				continue;
			}
			colNonMetaData.add(emdvo);
		}
		colMenuPaths = CollectionUtils.transform(colNonMetaData, new Transformer<EntityMeta<?>, String>() {
			@Override
			public String transform(EntityMeta<?> i) {
				final String menu = SpringLocaleDelegate.getInstance().getResource(i.getLocaleResourceIdForMenuPath(), "");
				return org.nuclos.common2.StringUtils.emptyIfNull(menu);
			}
		}, new Predicate<String>() {
			@Override
			public boolean evaluate(String t) {
				return !(t == null || t.length() == 0);
			}
		});

		final Set<String> setMenupaths = new HashSet<String>(colMenuPaths);
		setMenupaths.add(Main.getInstance().getMainController().getMainMenuAdministration());
		return CollectionUtils.sorted(setMenupaths);
	}

	public static MasterDataVO<?> setFieldsForUserRight(MasterDataVO<?> vo, NuclosEntityWizardStaticModel model) {
		MasterDataVO<?> voAdd = null;
	    if(model.isStateModel()) {
	    	final CollectableMasterDataEntity masterDataEntity = new CollectableMasterDataEntity(E.ROLEMODULE);
	    	voAdd = new CollectableMasterData(masterDataEntity,
	    			new MasterDataVO(masterDataEntity.getMeta(), false)).getMasterDataCVO();
	    	voAdd.setFieldUid(E.ROLEMODULE.module, null);
	    	voAdd.setFieldUid(E.ROLEMODULE.role, (UID)vo.getPrimaryKey());
	    	voAdd.setFieldValue(E.ROLEMODULE.modulepermission, null);
	    }
	    else if(voAdd == null && !model.isStateModel()) {
	    	final CollectableMasterDataEntity masterDataEntity = new CollectableMasterDataEntity(E.ROLEMASTERDATA);
	    	voAdd = new CollectableMasterData(masterDataEntity,
	    			new MasterDataVO(masterDataEntity.getMeta(), false)).getMasterDataCVO();
	    	voAdd.setFieldUid(E.ROLEMASTERDATA.entity, null);
	    	voAdd.setFieldUid(E.ROLEMASTERDATA.role, (UID)vo.getPrimaryKey());
	    	voAdd.setFieldValue(E.ROLEMASTERDATA.masterdatapermission, null);
	    }
	    return voAdd;
    }
}
