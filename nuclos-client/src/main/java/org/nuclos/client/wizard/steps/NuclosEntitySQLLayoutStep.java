//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.scripting.ScriptEditor;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.BubbleUtils;
import org.nuclos.client.ui.BubbleUtils.Position;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.wizard.NuclosEntityUpdater;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonValidationException;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntitySQLLayoutStep extends NuclosEntityAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityAbstractStep.class);

	private JLabel lbRowColorScript;
	private JButton btRowColorScript;

	private JLabel lbLayout;
	private JCheckBox cbLayout;
	private JLabel lbAttributeGroup;
	private JCheckBox cbAttributeGroup;
	private JLabel lbSubforms;
	private JCheckBox cbSubforms;
	private JLabel lbEditFields;
	private JCheckBox cbEditFields;

	private JLabel lbAttributeText;

	private boolean hasEntityLayout;

	private JPanel panelAttributes;
	private JScrollPane sPane;
	private JList listAttributeOrder;
	private JTree treeAttributeOrder;
	private JButton btUp;
	private JButton btDown;

	private Set<Attribute> stFieldNameChanged;

	private MyTreeModel treeModel;

	public NuclosEntitySQLLayoutStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	protected void initComponents() {

		double size [][] = {{TableLayout.FILL, 20, TableLayout.FILL}, {TableLayout.PREFERRED, 20,20,20,20,20,5,TableLayout.PREFERRED,20 }};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);

		lbRowColorScript = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.rowcolor", "Hintergrundfarbe der Zeilendarstellung konfigurieren"));
		btRowColorScript = new JButton("...");
		btRowColorScript.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.tooltip.1", "Geben Sie eine groovy-Regel für die Darstellung der Hintergrundfarbe an."));
		btRowColorScript.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ScriptEditor editor = new ScriptEditor();
				if (getModel().getRowColorScript() != null) {
					editor.setScript(getModel().getRowColorScript());
				}
				editor.run(btRowColorScript);
				NuclosScript script = editor.getScript();
				if (org.nuclos.common2.StringUtils.isNullOrEmpty(script.getSource())) {
					script = null;
				}
				getModel().setRowColorScript(script);
			}
		});


		lbLayout = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.2", "M\u00f6chten Sie eine Standard-Maske generieren lassen"));
		cbLayout = new JCheckBox();
		cbLayout.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.tooltip.2", "M\u00f6chten Sie eine Standard-Maske generieren lassen"));
		lbLayout.setLabelFor(cbLayout);

		lbAttributeGroup = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.12", "Attributegruppen werden zusammengefasst"));
		cbAttributeGroup = new JCheckBox();
		cbAttributeGroup.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.tooltip.3", "Attributegruppen werden zusammengefasst"));
		lbAttributeGroup.setLabelFor(cbAttributeGroup);

		lbAttributeText = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.13", "Geben Sie die Reihenfolge an, in der die Felder in der Maske erstellt werden sollen"));

		lbSubforms = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.14", "Vorhandene Unterformular mit ins Layout aufnehmen"));
		cbSubforms = new JCheckBox();
		cbSubforms.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.tooltip.4", "Vorhandene Unterformulare mit ins Layout aufnehmen"));
		lbSubforms.setLabelFor(cbSubforms);

		lbEditFields = new JLabel(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.15","Editierungsfelder erstellen:"));
		cbEditFields = new JCheckBox();
		cbEditFields.setSelected(false);
		cbEditFields.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.tooltip.7","Editierungsfelder mit in das Layout aufnehmen"));
		lbEditFields.setLabelFor(cbEditFields);

		listAttributeOrder = new JList();

		treeModel = new MyTreeModel();

		treeAttributeOrder = new JTree(treeModel);
		treeAttributeOrder.setCellRenderer(new AttributeTreeCellRenderer());
		treeAttributeOrder.setRootVisible(false);
		treeAttributeOrder.setExpandsSelectedPaths(true);

		sPane = new JScrollPane(listAttributeOrder);

		panelAttributes = new JPanel();
		double sizePanel [][] = {{TableLayout.FILL, 3, 20}, {20,3,20,3,TableLayout.PREFERRED }};
		panelAttributes.setLayout(new TableLayout(sizePanel));

		btUp = new JButton(Icons.getInstance().getIconSortAscending());
		btUp.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.tooltip.5", "Attribut nach oben schieben"));
		btDown = new JButton(Icons.getInstance().getIconSortDescending());
		btDown.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.entitysqllayout.tooltip.6", "Attribut nach unten schieben"));

		panelAttributes.add(sPane, "0,0, 0,4");
		panelAttributes.add(btUp, "2,0");
		panelAttributes.add(btDown, "2,2");

		this.add(lbRowColorScript, "0,0");
		this.add(btRowColorScript, "1,0");
		this.add(lbLayout, "0,1");
		this.add(cbLayout, "1,1");
		this.add(lbAttributeGroup, "0,2");
		this.add(cbAttributeGroup, "1,2");
		this.add(lbSubforms, "0,3");
		this.add(cbSubforms, "1,3");
		this.add(lbEditFields, "0,4");
		this.add(cbEditFields, "1,4");
		this.add(lbAttributeText, "0,5, 1,5");
		this.add(panelAttributes, "0,6, 1,7");

		enableLayoutOptions(false);
		
		cbLayout.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntitySQLLayoutStep.this.model.setCreateLayout(cb.isSelected());
				if(cb.isSelected() && hasEntityLayout) {
					(new Bubble(cbLayout, SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.entitysqllayout.9", "Achtung! Ihr bestehendes Layout wird überschrieben!"), 
							5, BubbleUtils.Position.NW)).setVisible(true);
				}
				enableLayoutOptions(cb.isSelected());
			}
		});

		btUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonUpAttributeAction();
			}
		});

		btDown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buttonDownAttributeAction();
			}
		});
	}

	private void buttonDownAttributeAction() {
		int iSelected = listAttributeOrder.getSelectedIndex();
		if(iSelected < 0 || iSelected >= listAttributeOrder.getModel().getSize()-1)
			return;

		DefaultListModel model = (DefaultListModel)listAttributeOrder.getModel();
		Object obj = model.remove(iSelected);
		model.add(iSelected+1, obj);
		listAttributeOrder.getSelectionModel().setSelectionInterval(iSelected+1, iSelected+1);
	}

	private void buttonUpAttributeAction() {
		int iSelected = listAttributeOrder.getSelectedIndex();
		if(iSelected < 1 || iSelected > listAttributeOrder.getModel().getSize())
			return;

		DefaultListModel model = (DefaultListModel)listAttributeOrder.getModel();
		Object obj = model.remove(iSelected);
		model.add(iSelected-1, obj);

		listAttributeOrder.getSelectionModel().setSelectionInterval(iSelected-1, iSelected-1);
	}


	private void enableLayoutOptions(boolean enable) {
		lbAttributeGroup.setEnabled(enable && getModel().isStateModel());
		lbSubforms.setEnabled(enable);
		lbEditFields.setEnabled(enable);
		cbAttributeGroup.setEnabled(enable && getModel().isStateModel());
		cbSubforms.setEnabled(enable);
		cbEditFields.setEnabled(enable);
		lbAttributeText.setEnabled(enable);
		listAttributeOrder.setEnabled(enable);
		btDown.setEnabled(enable);
		btUp.setEnabled(enable);
	}

	@Override
	public void close() {
		lbRowColorScript = null;
		btRowColorScript = null;

		lbLayout = null;
		cbLayout = null;
		lbAttributeGroup = null;
		cbAttributeGroup = null;
		lbSubforms = null;
		cbSubforms = null;
		lbEditFields = null;
		cbEditFields = null;

		lbAttributeText = null;

		panelAttributes = null;
		sPane = null;
		listAttributeOrder = null;
		treeAttributeOrder = null;
		btUp = null;
		btDown = null;

		if (stFieldNameChanged != null) {
			stFieldNameChanged.clear();
		}
		stFieldNameChanged = null;

		treeModel = null;

		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		try {
			// TODO: Don't call this here. The wizard itself should trigger the update once all steps are complete.
			if(!new NuclosEntityUpdater(model, listAttributeOrder, cbAttributeGroup, cbSubforms, cbEditFields, treeModel, stFieldNameChanged).createOrModifyEntity()) {
				return;
			}
		} catch (Exception e) {
			Errors.getInstance().showExceptionDialog(super.getParent(), e);
			throw new NuclosFatalException(e);
		} 

		super.applyState();
	}

	@Override
	public void prepare() {
		this.setComplete(true);
		stFieldNameChanged = new HashSet<>();

		final boolean rowColorEnabled = !this.model.isGeneric();
		lbRowColorScript.setEnabled(rowColorEnabled);
		btRowColorScript.setEnabled(rowColorEnabled);

		final boolean layoutDisabled = this.model.getAttributeModel().getNucletAttributes().size() == 0 ||
				this.model.isGeneric();
		lbLayout.setEnabled(!layoutDisabled);
		cbLayout.setEnabled(!layoutDisabled);
		
		hasEntityLayout = MasterDataLayoutHelper.isLayoutMLAvailable(model.getUID(), false);
		if(hasEntityLayout){
			lbLayout.setText(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.entitysqllayout.16", "Wollen Sie die bestehende Maske aktualisieren:"));
		}
		DefaultListModel listmodel = new DefaultListModel();
		List<Attribute> lstAttr =
			new ArrayList<>(this.model.getAttributeModel().getNucletAttributes());

		for(Attribute attr : lstAttr) {
			listmodel.addElement(attr);
		}
		treeModel = new MyTreeModel(model.getAttributeModel().getAttributeMapByGroup());

		try {
			treeAttributeOrder.setModel(treeModel);
			treeModel.expandWholeTree();
		}
		catch (Exception e) {
			LOG.info("prepare failed: " + e);
		}
		listAttributeOrder.setModel(listmodel);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				try {
			        validateEntity();
		        }
		        catch(CommonValidationException ex) {
		        	NuclosEntitySQLLayoutStep.this.setComplete(false);
		        	Errors.getInstance().showExceptionDialog(NuclosEntitySQLLayoutStep.this, ex);
		        }
			}
		});
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				cbLayout.requestFocusInWindow();
			}
		});
	}

	private void validateEntity() throws CommonValidationException {
		// validate attributes that changed the name
		for(Attribute attr : model.getAttributeModel().getNucletAttributes()) {
			if(!attr.hasInternalNameChanged())
				continue;
			for(TranslationVO voTranslation : this.model.getTranslation()) {
				for(String sKey : voTranslation.getLabels().keySet()) {
					final String sValue = voTranslation.getLabels().get(sKey);
					StringBuffer sb = new StringBuffer();
					if(hasStringChangedAttributes(sValue, sb)) {
						final String sMessage = sb.toString();
						throw new CommonValidationException(sMessage);
					}
				}
			}
		}

		// validate MultiEditEquation
		if(model.getMultiEditEquation() != null && model.getMultiEditEquation().length > 0) {
			for(UID field : model.getMultiEditEquation()) {
				for(Attribute attr : this.model.getAttributeModel().getRemoveAttributes()) {
					if(attr.getUID().equals(field)) {
						String sMessage = SpringLocaleDelegate.getInstance().getMessage(
								"wizard.step.entitysqllayout.10", 
								"Das gelöschte Attribut " + attr.getInternalName() + " befindet sich in Ihren Angaben!\nBitte überprüfen Sie Ihre Eintragungen!", 
								attr.getInternalName());
						throw new CommonValidationException(sMessage);
					}
				}
			}

			for(UID field : model.getMultiEditEquation()) {
				for(Attribute attr : this.model.getAttributeModel().getNucletAttributes()) {
					if(!attr.hasInternalNameChanged())
						continue;
					if(attr.getUID().equals(field)) {
						String sMessage = SpringLocaleDelegate.getInstance().getMessage(
								"wizard.step.entitysqllayout.11", 
								"Das geänderte Attribut " + attr.getOldInternalName() + " befindet sich in Ihren Angaben!\nBitte überprüfen Sie Ihre Eintragungen!", 
								attr.getOldInternalName());
						throw new CommonValidationException(sMessage);
					}
				}
			}
		}
	}

	private boolean hasStringChangedAttributes(String sNodeLabel, StringBuffer sb) {
		if(sNodeLabel == null) {
			return false;
		}
		Pattern referencedEntityPattern = Pattern.compile ("[$][{][\\w\\[\\]]+[}]");
	    Matcher referencedEntityMatcher = referencedEntityPattern.matcher (sNodeLabel);

		while (referencedEntityMatcher.find()) {
			Object value = referencedEntityMatcher.group().substring(2,referencedEntityMatcher.group().length()-1);

		  	String sName = value.toString();
		  	for(Attribute attr : this.model.getAttributeModel().getNucletAttributes()) {
			  	if(!attr.hasInternalNameChanged())
				  	continue;
			  	if(attr.getOldInternalName().equals(sName)){
				  	String str = SpringLocaleDelegate.getInstance().getMessage(
						  "wizard.step.entitysqllayout.11", 
						  "Das geänderte Attribut " + attr.getOldInternalName() + " befindet sich in Ihren Angaben!\nBitte überprüfen Sie Ihre Eintragungen!", 
						  attr.getOldInternalName());
				  	sb.append(str);
				  	return true;
			  	}
		  	}
		}
		return false;
	}

	private boolean hasStringRemovedAttributes(String sNodeLabel, StringBuffer sb) {
		if(sNodeLabel == null) {
			return false;
		}
		Pattern referencedEntityPattern = Pattern.compile ("[$][{][\\w\\[\\]]+[}]");
	    Matcher referencedEntityMatcher = referencedEntityPattern.matcher (sNodeLabel);

		while (referencedEntityMatcher.find()) {
			Object value = referencedEntityMatcher.group().substring(2,referencedEntityMatcher.group().length()-1);

			String sName = value.toString();
			for(Attribute attr : this.model.getAttributeModel().getRemoveAttributes()) {
				if(attr.getInternalName().equals(sName)){
					String str = SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.entitysqllayout.10", 
							"Das gelöschte Attribut " + attr.getInternalName() + " befindet sich in Ihren Angaben!\nBitte überprüfen Sie Ihre Eintragungen!", 
							attr.getInternalName());
					 sb.append(str);
					 return true;
				}
			 }
		}
		return false;
	}


	class AttributeTreeCellRenderer extends DefaultTreeCellRenderer {
		@Override
		public Icon getLeafIcon() {
			return Icons.getInstance().getIconStateNewNote();
		}
		@Override
		public Icon getClosedIcon() {
			return Icons.getInstance().getIconModule();
		}
		@Override
		public Icon getDefaultClosedIcon() {
			return Icons.getInstance().getIconModule();
		}
		@Override
		public Icon getDefaultOpenIcon() {
			return Icons.getInstance().getIconModule();
		}
		@Override
		public Icon getOpenIcon() {
			return Icons.getInstance().getIconModule();
		}
	}

	public class MyTreeModel implements TreeModel {

		UID root = new UID("<<<<root>>>>");
		Map<UID, List<Attribute>> mpGroup;
		Map<Integer, UID> mpIndex;
		Collection<TreeModelListener> colListener;

		MyTreeModel() {
			mpGroup = new HashMap<>();
			mpIndex = new HashMap<>();
		}

		MyTreeModel(Map<UID, List<Attribute>> mp) {
			mpGroup = mp;
			initIndexMap();
			expandWholeTree();
		}

		public Map<Integer, UID> getIndexMap() {
			return mpIndex;
		}

		public void groupUpIfPossible(UID group) {
			int index = getIndexOfGroup(group);
			if(index == -1 || index == 0)
				return;

			UID obj1 = mpIndex.remove(index);
			UID obj2 = mpIndex.remove(index-1);
			mpIndex.put(index , obj2);
			mpIndex.put(index-1, obj1);

		}

		public void groupDownIfPossible(UID group) {
			int index = getIndexOfGroup(group);
			if(index == -1 || index == mpIndex.size()-1)
				return;

			UID obj1 = mpIndex.remove(index);
			UID obj2 = mpIndex.remove(index+1);
			mpIndex.put(index , obj2);
			mpIndex.put(index+1, obj1);
		}

		private Integer getIndexOfGroup(UID group) {
			for(Integer index : mpIndex.keySet()) {
				if(mpIndex.get(index).equals(group))
					return index;
			}
			return -1;
		}

		public List<Attribute> getAttributeGroupList(UID group) {
			return mpGroup.get(group);
		}

		private void initIndexMap() {
			mpIndex = new HashMap<Integer, UID>();
			int counter = 0;
			for(UID key : mpGroup.keySet()) {
				mpIndex.put(counter++, key);
			}
		}

		@Override
		public void addTreeModelListener(TreeModelListener l) {
			if(colListener == null)
				colListener = Collections.synchronizedList(new ArrayList<TreeModelListener>());
			colListener.add(l);
		}

		@Override
		public Object getChild(Object parent, int index) {
			if(mpGroup.get(parent) != null) {
				try {
					return mpGroup.get(parent).get(index);
				}
				catch(Exception e) {
					LOG.info("getChild: " + e);
					return null;
				}
			}
			if(parent instanceof UID) {
				UID uid = (UID)parent;
				if(root.equals(uid)) {
					try {
						UID group = mpIndex.get(index);
						return group;
					}
					catch(Exception e) {
						LOG.info("getChild: " + e);
						return null;
					}
				}
			}
			return null;
		}

		@Override
		public int getChildCount(Object parent) {
			if(parent instanceof UID) {
				UID uidParent =(UID)parent;
				if(root.equals(uidParent))
					return mpGroup.size();
				if(mpGroup.get(uidParent) != null)
					return mpGroup.get(uidParent).size();
			}

			return 0;
		}

		@Override
		public int getIndexOfChild(Object parent, Object child) {
			if(child instanceof Attribute) {
				String sParent = (String)parent;
				return mpGroup.get(sParent).indexOf(child);
			}
			if(child instanceof UID) {
				UID group = (UID)child;
				if(root.equals(group))
					return 0;

				for(Integer index : mpIndex.keySet()) {
					if(mpIndex.get(index).equals(group))
						return index;
				}
			}
			return 0;
		}

		@Override
		public Object getRoot() {
			return root;
		}

		@Override
		public boolean isLeaf(Object node) {
			if(node instanceof Attribute)
				return true;
			else
				return false;
		}

		@Override
		public void removeTreeModelListener(TreeModelListener l) {
			if(colListener == null)
				return;
			colListener.remove(l);

		}

		@Override
		public void valueForPathChanged(TreePath path, Object newValue) {
			LOG.info(newValue);
		}

		public void setModelData(Map<UID, List<Attribute>> mp) {
			this.mpGroup = mp;
			initIndexMap();
			fireTreeModelChanged();
		}

		public void fireTreeModelChanged() {
			TreePath pathSelected = treeAttributeOrder.getSelectionPath();
			TreePath path = pathSelected.getParentPath();

			for(TreeModelListener l : colListener) {
				l.treeStructureChanged(new TreeModelEvent(btUp, path));
			}
			treeAttributeOrder.setSelectionPath(pathSelected);
			expandWholeTree();
		}

		private void expandWholeTree() {
			int row = 0;
		    while (row < treeAttributeOrder.getRowCount()) {
		    	treeAttributeOrder.expandRow(row);
		    	row++;
		      }
		}

	}

}
