package org.nuclos.client.wizard;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.layout.DefaultLayoutMLFactory;
import org.nuclos.client.layout.wysiwyg.WYSIWYGLayoutControllingPanel;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubForm;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubFormColumn;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRule;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleAction;
import org.nuclos.client.main.Main;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.statemodel.RoleRepository;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.EntityAttributeTableModel;
import org.nuclos.client.wizard.model.ValueList;
import org.nuclos.client.wizard.steps.NuclosEntitySQLLayoutStep;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.DefaultComponentTypes;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.MasterDataToEntityObjectTransformer;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.transport.vo.EntityMetaTransport;
import org.nuclos.common.transport.vo.FieldMetaTransport;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.xml.sax.InputSource;

/**
 * Performs the necessary modification steps after the entity wizard is finished.
 *
 * Moved here from NuclosEntitySQLLayoutStep.
 * TODO: Refactor this more, it's still quite a mess.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NuclosEntityUpdater {
	private static final Logger LOG = Logger.getLogger(NuclosEntityUpdater.class);

	private final NuclosEntityWizardStaticModel model;

	// TODO: Get rid of dependencies on GUI components here
	private final JList listAttributeOrder;
	private final JCheckBox cbAttributeGroup;
	private final JCheckBox cbSubforms;
	private final JCheckBox cbEditFields;

	private final NuclosEntitySQLLayoutStep.MyTreeModel treeModel;
	private final Set<Attribute> stFieldNameChanged;

	public NuclosEntityUpdater(final NuclosEntityWizardStaticModel model, final JList listAttributeOrder, final JCheckBox cbAttributeGroup, final JCheckBox cbSubforms, final JCheckBox cbEditFields, final NuclosEntitySQLLayoutStep.MyTreeModel treeModel, final Set<Attribute> stFieldNameChanged) {
		this.model = model;
		this.listAttributeOrder = listAttributeOrder;
		this.cbAttributeGroup = cbAttributeGroup;
		this.cbSubforms = cbSubforms;
		this.cbEditFields = cbEditFields;
		this.treeModel = treeModel;
		this.stFieldNameChanged = stFieldNameChanged;
	}

	public boolean createOrModifyEntity() throws CommonFinderException, CommonPermissionException {
		buildValueListIfNeeded();

		NuclosEntityWizardStaticModel wizardModel = this.getModel();
		EntityMeta<?> metaVOOld = null;
		if(wizardModel.isEditMode()) {
			metaVOOld = MetaProvider.getInstance().getEntity(wizardModel.getUID());
		}

		EntityAttributeTableModel attributeModel = wizardModel.getAttributeModel();

		final EntityMetaVO<?> metaVO;

		if (metaVOOld != null){
			metaVO = new EntityMetaVO<>(metaVOOld, false);
		} else {
			metaVO = new EntityMetaVO<>(Long.class);
		}

		metaVO.setWriteProxy(wizardModel.isWriteProxy());
		metaVO.setProxy(wizardModel.isProxy());
		metaVO.setGeneric(wizardModel.isGeneric());
		metaVO.setSearchable(wizardModel.isSearchable());
		metaVO.setShowSearch(wizardModel.showsSearch());
		metaVO.setThin(wizardModel.isThin());
		metaVO.setCacheable(wizardModel.isCachable());
		metaVO.setResource(wizardModel.getResourceUID());
		metaVO.setNuclosResource(wizardModel.getNuclosResourceName());
		metaVO.setLocaleResourceIdForLabel(wizardModel.getLabelSingularResource());
		metaVO.setLocaleResourceIdForMenuPath(wizardModel.getMenuPathResource());
		metaVO.setLocaleResourceIdForTreeView(wizardModel.getNodeLabelResource());
		metaVO.setLocaleResourceIdForTreeViewDescription(wizardModel.getNodeTooltipResource());
		metaVO.setEditable(wizardModel.isEditable());
		metaVO.setResultdetailssplitview(wizardModel.isResultDetailsSplit());
		metaVO.setSystemIdPrefix(wizardModel.getSystemIdPrefix());
		metaVO.setLogBookTracking(wizardModel.isLogbook());
		metaVO.setTreeRelation(wizardModel.isShowRelation());
		metaVO.setTreeGroup(wizardModel.isShowGroups());
		metaVO.setStateModel(wizardModel.isStateModel());
		metaVO.setImportExport(true);
		metaVO.setDynamic(false);
		metaVO.setAccelerator(wizardModel.getAccelerator());
		metaVO.setFieldValueEntity(Boolean.FALSE);

		metaVO.setComment(wizardModel.getComment());

		if (!wizardModel.getAttributeModel().hasLocalizedFields()) {
			metaVO.setDataLangRefPath(null);
			metaVO.setDataLanguageDBTable(null);
		} else {
			metaVO.setDataLangRefPath(wizardModel.getDataLangRefPath());
		}

		if(wizardModel.getAccelerator() != null) {
			metaVO.setAcceleratorModifier(wizardModel.getModifier());
		} else {
			metaVO.setAcceleratorModifier(null);
		}

		if(metaVOOld != null) {
			metaVO.setUID(metaVOOld.getUID());
		} else {
			metaVO.setUID(wizardModel.getUID());
			metaVO.setNuclet(wizardModel.getNucletUID());
		}

		metaVO.setFieldsForEquality(wizardModel.getMultiEditEquation());
		metaVO.setVirtualEntity(wizardModel.getVirtualentity());
		metaVO.setIdFactory(wizardModel.getIdFactory());
		metaVO.setRowColorScript(wizardModel.getRowColorScript());
		metaVO.setCloneGenerator(wizardModel.getCloneGenerator());
		metaVO.setMandatorLevel(wizardModel.getMandatorLevel());
		metaVO.setMandatorUnique(wizardModel.isMandatorUnique());

		metaVO.setEntityName(StringUtils.defaultIfNull(wizardModel.getModifiedEntityName(), wizardModel.getEntityName()));
		if (wizardModel.isProxy() || wizardModel.isGeneric()) {
			metaVO.setDbTable(wizardModel.getTableOrViewName());
		} else {
			metaVO.setDbTable(NuclosWizardUtils.replace(wizardModel.getTableOrViewName()));
		}

		metaVO.setLockMode(wizardModel.getLockMode());
		metaVO.setOwnerForeignEntityField(wizardModel.getOwnerForeignEntityField());
		metaVO.setUnlockMode(wizardModel.getUnlockMode());

		List<FieldMetaTransport> lstEntityFields = new ArrayList<>();

		List<Attribute> lstAttributes = attributeModel.getNucletAttributesWithOrder();
		lstAttributes.addAll(attributeModel.getAlteredSystemAttributes());

		for (Attribute attr : lstAttributes) {
			NucletFieldMeta<?> mdFieldVO = buildMasterDataField(attr, metaVO, false);

			FieldMetaTransport to = new FieldMetaTransport();
			to.setEntityFieldMeta(mdFieldVO);
			to.setTranslation(attributeModel.getTranslation().get(attr));
			lstEntityFields.add(to);
		}

		if(wizardModel.isEditMode()) {
			for(Attribute attr : attributeModel.getRemoveAttributes()) {
				NucletFieldMeta<?> mdFieldVO = buildMasterDataField(attr, metaVO, true);

				FieldMetaTransport to = new FieldMetaTransport();
				to.setEntityFieldMeta(mdFieldVO);
				to.setTranslation(attributeModel.getTranslation().get(attr));
				lstEntityFields.add(to);
			}
		}

		try {
			EntityMetaTransport entityMetaTO = new EntityMetaTransport();
			entityMetaTO.setEntityMetaVO(new NucletEntityMeta(metaVO, false));
			entityMetaTO.setTranslation(wizardModel.getTranslation());
			entityMetaTO.setTreeView(wizardModel.getTreeView());
			if (metaVO.isStateModel()) {
				entityMetaTO.setProcesses(wizardModel.getProcesses());
			}

			Map<EntityObjectVO<UID>, Map<String, String>> menus = new HashMap<>();
			for (EntityObjectVO<UID> menu : wizardModel.getEntityMenus()) {
				Map<String, String> localeResources = new HashMap<>();
				for (UID valueKey : menu.getFieldValues().keySet()) {
					if (valueKey.getString().startsWith("menupath_")) {
						String locale = valueKey.getString().substring(9);
						String value = menu.getFieldValue(valueKey, String.class);
						localeResources.put(locale, value);
					}
				}
				menus.put(menu, localeResources);
			}
			entityMetaTO.setMenus(menus);
			entityMetaTO.setMandatorInitialFill(wizardModel.getMandatorInitialFill());
			String sResult = MetaDataDelegate.getInstance().createOrModifyEntity(entityMetaTO, lstEntityFields);
			if(sResult != null) {
				Errors.getInstance().showExceptionDialog(null, new CommonFatalException(sResult));
				throw new CommonFatalException(sResult);
			}

			this.model.setResultText(sResult);

			removeValueListIfNotNeeded();
		} catch(Exception e) {
			Errors.getInstance().showExceptionDialog(null, e);
			return false;
		} finally {
			MetaProvider.getInstance().revalidate();
			MetaDataDelegate.getInstance().invalidateServerMetadata();
		}

		try {
			RoleRepository.getInstance().invalidate();
		} catch(Exception e) {
			throw new NuclosFatalException(e);
		}

		// user rights + permission
		try {
			for(MasterDataVO<UID> roleModuleVO : wizardModel.getUserRights()) {
				if(wizardModel.isStateModel()) {
					UID roleUID = roleModuleVO.getFieldUid(E.ROLEMODULE.role);
					final MasterDataVO<?> voRole;
					try {
						voRole = RoleRepository.getInstance().getRole(roleUID);
					} catch(Exception e) {
						throw new NuclosFatalException(e);
					}

					Integer permission = roleModuleVO.getFieldValue(E.ROLEMODULE.modulepermission);
					if(permission == null) {
						roleModuleVO.remove();
					}

					roleModuleVO.setFieldUid(E.ROLEMODULE.module, metaVO.getUID());
					roleModuleVO.setFieldValue(E.ROLEMODULE.modulepermission, permission);

					EntityObjectVO<UID> roleModuleEO = roleModuleVO.getEntityObject();
					boolean update = true;
					try {
						if (!roleModuleEO.isFlagNew() /*&& !roleModuleEO.isFlagRemoved()*/) {
							if (roleModuleEO.isFlagRemoved() && roleModuleEO.getPrimaryKey() == null) {
								update = false;
							} else {
								EntityObjectVO<UID> originEo = EntityObjectDelegate.getInstance().get(E.ROLEMODULE.getUID(), roleModuleEO.getPrimaryKey());
								if (RigidUtils.equal(originEo.getPrimaryKey(), roleModuleEO.getPrimaryKey()) &&
										RigidUtils.equal(originEo.getFieldUid(E.ROLEMODULE.module), roleModuleEO.getFieldUid(E.ROLEMODULE.module)) &&
										RigidUtils.equal(originEo.getFieldUid(E.ROLEMODULE.role), roleModuleEO.getFieldUid(E.ROLEMODULE.role)) &&
										RigidUtils.equal(originEo.getFieldValue(E.ROLEMODULE.modulepermission), roleModuleEO.getFieldValue(E.ROLEMODULE.modulepermission))) {
									//for nuclet export / import
									update = false;
								}
							}
						}
					} catch (Exception ex) {
						LOG.warn("Error during field equality check for module permission with UID " + roleModuleVO
								.getPrimaryKey() + " Error: " + ex.getMessage());
					}

					if (update) {
						IDependentDataMap mp = new DependentDataMap();
						mp.addData(E.ROLEMODULE.role, roleModuleEO);
						MasterDataDelegate.getInstance().update(E.ROLE.getUID(), voRole, mp, null, false);
					}
				} else {
					// no statemodel
					final MasterDataVO<?> voRole;
					UID roleUID = roleModuleVO.getFieldUid(E.ROLEMASTERDATA.role);
					try {
						voRole = RoleRepository.getInstance().getRole(roleUID);
					}
					catch(Exception e) {
						throw new NuclosFatalException(e);
					}

					Integer permission = roleModuleVO.getFieldValue(E.ROLEMASTERDATA.masterdatapermission);
					if(permission == null) {
						roleModuleVO.remove();
					}

					roleModuleVO.setFieldUid(E.ROLEMASTERDATA.entity, metaVO.getUID());
					roleModuleVO.setFieldValue(E.ROLEMASTERDATA.masterdatapermission, permission);

					EntityObjectVO<UID> eo = roleModuleVO.getEntityObject();
					boolean update = true;
					try {
						if (!eo.isFlagNew() /*&& !roleModuleEO.isFlagRemoved()*/) {
							if (eo.isFlagRemoved() && eo.getPrimaryKey() == null) {
								update = false;
							} else {
								EntityObjectVO<UID> originEo = EntityObjectDelegate.getInstance().get(E.ROLEMASTERDATA.getUID(), eo.getPrimaryKey());
								if (RigidUtils.equal(originEo.getPrimaryKey(), eo.getPrimaryKey()) &&
										RigidUtils.equal(originEo.getFieldUid(E.ROLEMASTERDATA.entity), eo.getFieldUid(E.ROLEMASTERDATA.entity)) &&
										RigidUtils.equal(originEo.getFieldUid(E.ROLEMASTERDATA.role), eo.getFieldUid(E.ROLEMASTERDATA.role)) &&
										RigidUtils.equal(originEo.getFieldValue(E.ROLEMASTERDATA.masterdatapermission), eo.getFieldValue(E.ROLEMASTERDATA.masterdatapermission))) {
									//for nuclet export / import
									update = false;
								}
							}
						}
					} catch (Exception ex) {
						LOG.warn("Error during field equality check for masterdata permission with UID " + roleModuleVO
								.getPrimaryKey() + " Error: " + ex.getMessage());
					}

					if (update) {
						IDependentDataMap mp = new DependentDataMap();
						mp.addData(E.ROLEMASTERDATA.role, eo);
						MasterDataDelegate.getInstance().update(E.ROLE.getUID(), voRole, mp, null, false);
					}
				}

				try {
					RoleRepository.getInstance().invalidate();
				} catch(Exception e) {
					throw new NuclosFatalException(e);
				}
			}
		} catch(CommonBusinessException bex) {
			// do nothing here
			LOG.warn("createOrModifyEntity: " + bex);
		}

		// optional: remove state model usage
		try {
			if (wizardModel.isEditMode()) {
				Collection<EntityObjectVO<UID>> colVo = MasterDataDelegate.getInstance()
						.getDependentDataCollection(E.STATEMODELUSAGE.getUID(),
								E.STATEMODELUSAGE.nuclos_module.getUID(),
								null, metaVO.getUID());
				if (!wizardModel.isStateModel()) {
					for(EntityObjectVO vo : colVo) {
						MasterDataDelegate.getInstance().remove(E.STATEMODELUSAGE.getUID(), DalSupportForMD.wrapEntityObjectVO(vo), null);
					}
				}
			}
		} catch(Exception ex) {
			// do nothing here
			LOG.warn("createOrModifyEntity: " + ex);
		}

		if(wizardModel.isImportTable()) {
			MetaDataDelegate.getInstance().transferTable(wizardModel.getJdbcUrl(), wizardModel.getExternalUser(),
					wizardModel.getExternalPassword(), null, wizardModel.getExternalTable(),
					wizardModel.getUID());
		}

		if(wizardModel.isCreateLayout()) {
			final EntityMeta<?> masterVO = E.ENTITY;
			CollectableMasterDataEntity masterDataEntity = new CollectableMasterDataEntity(masterVO);
			buildLayoutML(wizardModel, masterDataEntity, metaVO.getUID());
		}

		//load all layouts for entity
		final Set<UID> lstLayouts = new HashSet<>();
		final CollectableComparison compare = SearchConditionUtils.newComparison(E.LAYOUTUSAGE.entity, ComparisonOperator.EQUAL, wizardModel.getUID());
		for(MasterDataVO<?> layout : MasterDataDelegate.getInstance().getMasterData(E.LAYOUTUSAGE.getUID(), compare)) {
			lstLayouts.add(layout.getFieldUid(E.LAYOUTUSAGE.layout));
		}

		//deletes all all removed fields from layout 
		if (wizardModel.getAttributeModel().getRemoveAttributes().size() > 0 && !wizardModel.isCreateLayout()) {
			// update all layouts that contain the removed attributes...
			for (UID layoutUID : lstLayouts) {
				final MasterDataVO<?> voLayout = MasterDataCache.getInstance().get(E.LAYOUT.getUID(), layoutUID);
				String sLayout = voLayout.getFieldValue(E.LAYOUT.layoutML);
				if (sLayout == null) {
					continue;
				}

				// Remove deleted fields from layout
				sLayout = removeAttributesFromWYSIWYGLayout(layoutUID, sLayout, wizardModel.getAttributeModel().getRemoveAttributes(), wizardModel.getUID());
				
				if (sLayout != null) {
					voLayout.setFieldValue(E.LAYOUT.layoutML, sLayout);
					try {
						MasterDataDelegate.getInstance().update(E.LAYOUT.getUID(), voLayout, null, null, false);
					} catch (CommonBusinessException e) {
						throw new NuclosFatalException(e);
					}
				}
			}// for each layout
		} // if

		modifyParentLayouts();

		EntityUtils.clearAllEntityCaches();

		// We have to start the BO-building after the common EntityWizzard process, not to
		// run into a cache-invalidation dilemma with our thread. (NUCLOS-1491)

		if(wizardModel.isEditMode()) {
			// Validate integration points (NUCLOS-6246)
			boolean bNewProblems = false;
			final Set<UID> ipWithProblemSet = new HashSet<>();
			final List<EntityObjectVO<UID>> integrationPointList = CollectionUtils.transform(MasterDataCache.getInstance().get(E.NUCLET_INTEGRATION_POINT.getUID()), new MasterDataToEntityObjectTransformer());
			for (EntityObjectVO<UID> eoIntegrationPoint : integrationPointList) {
				boolean bRevalidate = false;
				final UID targetEntityUID = eoIntegrationPoint.getFieldUid(E.NUCLET_INTEGRATION_POINT.targetEntity);
				if (RigidUtils.equal(targetEntityUID, metaVO.getUID())) {
					bRevalidate = true;
				}
				if (!bRevalidate) {
					for (FieldMetaTransport efMetaTransport : lstEntityFields) {
						final UID foreignIntegrationPointUID = efMetaTransport.getEntityFieldMeta().getForeignIntegrationPoint();
						if (RigidUtils.equal(foreignIntegrationPointUID, eoIntegrationPoint.getPrimaryKey())) {
							bRevalidate = true;
							break;
						}
					}
				}
				final boolean bProblemsBefore = Boolean.TRUE.equals(eoIntegrationPoint.getFieldValue(E.NUCLET_INTEGRATION_POINT.problem));
				if (bProblemsBefore) {
					ipWithProblemSet.add(eoIntegrationPoint.getPrimaryKey());
				}

				if (!bRevalidate) {
					continue;
				}
				try {
					eoIntegrationPoint = EntityObjectDelegate.getInstance().executeBusinessRules(
							Collections.singletonList(EventSupportSourceVO.getSystemRuleForExecuteBusinessRules("org.nuclos.businessentity.rule.NucletIntegrationSaveRule")),
							eoIntegrationPoint, null, false);
				} catch (Exception e) {
					Errors.getInstance().showExceptionDialog(null, e);
					continue;
				}
				final boolean bProblemsAfter = Boolean.TRUE.equals(eoIntegrationPoint.getFieldValue(E.NUCLET_INTEGRATION_POINT.problem));
				if (!bProblemsBefore && bProblemsAfter) {
					bNewProblems = true;
				}
				if (bProblemsAfter) {
					ipWithProblemSet.add(eoIntegrationPoint.getPrimaryKey());
				}
			}
			if (bNewProblems) {
				final int iUserResult = JOptionPane.showConfirmDialog(null,
						SpringLocaleDelegate.getInstance().getMsg("wizard.step.entitysqllayout.17"),
						SpringLocaleDelegate.getInstance().getMsg("wizard.step.entitysqllayout.18"),
						JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);
				if (iUserResult == JOptionPane.YES_OPTION) {
					try {
						Main.getInstance().getMainController().showList(E.NUCLET_INTEGRATION_POINT.getUID(), new ArrayList<Object>(ipWithProblemSet));
					} catch (CommonBusinessException e) {
						LOG.error(e.getMessage(), e);
					}
				}
			}
		}

		return true;
	}



	private void modifyParentLayouts() {
		for(Attribute attr : model.getAttributeModel().getRemoveAttributes()) {
			for(UID parentEntity : searchParentEntity(model.getUID())) {
				Set<UID> lstLayouts = new HashSet<UID>();
				CollectableComparison compare = SearchConditionUtils.newComparison(E.LAYOUTUSAGE.entity, ComparisonOperator.EQUAL, parentEntity);
				for(MasterDataVO<UID> layout : MasterDataDelegate.getInstance().<UID>getMasterData(E.LAYOUTUSAGE.getUID(), compare)) {
					lstLayouts.add(layout.getPrimaryKey());
				}
				for(UID layoutUID : lstLayouts) {
					try {
						final MasterDataVO<?> voLayout = MasterDataCache.getInstance().get(E.LAYOUT.getUID(), layoutUID);

						String sLayout = voLayout.getFieldValue(E.LAYOUT.layoutML);

						WYSIWYGLayoutControllingPanel ctrlPanel = new WYSIWYGLayoutControllingPanel(new WYSIWYGMetaInformation(layoutUID));
						CollectableEntity entity = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(parentEntity);
						ctrlPanel.getMetaInformation().setCollectableEntity(entity);
						ctrlPanel.setLayoutML(sLayout);

						if(attr.getField() != null && NuclosWizardUtils.searchParentEntity(getModel().getUID()).size() < 1) {
							List<WYSIWYGComponent> allCollectables = new ArrayList<>();
							ctrlPanel.getEditorPanel().getWYSIWYGComponents(WYSIWYGComponent.class, ctrlPanel.getEditorPanel().getMainEditorPanel(), allCollectables);
							for(WYSIWYGComponent collectable : allCollectables) {
								if(collectable.getLayoutMLRulesIfCapable() == null)
									continue;
								Collection<LayoutMLRule> copyOfRules = new ArrayList<>();
								copyOfRules.addAll(collectable.getLayoutMLRulesIfCapable().getRules());
								for(LayoutMLRule rule : collectable.getLayoutMLRulesIfCapable().getRules()) {
									for(LayoutMLRuleAction action : rule.getLayoutMLRuleActions().getSingleActions()) {
										if(action.getEntity() != null && action.getEntity().equals(model.getEntityName())) {
											if(action.getTargetComponent().equals(attr.getInternalName())) {
												copyOfRules.remove(rule);
											}
										}
									}
								}
								collectable.getLayoutMLRulesIfCapable().clearRulesForComponent();
								for(LayoutMLRule rule : copyOfRules) {
									collectable.getLayoutMLRulesIfCapable().addRule(rule);
								}
							}

							List<WYSIWYGSubForm> allSubForms = new ArrayList<>();
							ctrlPanel.getEditorPanel().getWYSIWYGComponents(WYSIWYGSubForm.class, ctrlPanel.getEditorPanel().getMainEditorPanel(), allSubForms);
							for(WYSIWYGSubForm collectable : allSubForms) {
								Collection<WYSIWYGSubFormColumn> copyOfColumns = new ArrayList<WYSIWYGSubFormColumn>(collectable.getColumns());
								for(WYSIWYGSubFormColumn col : copyOfColumns) {
									CollectableEntityField field = col.getEntityField();
									if(field.getUID().equals(attr.getUID())) {
										collectable.removeColumn(col.getName());
									}

									if(col.getLayoutMLRulesIfCapable() == null)
										continue;
									Collection<LayoutMLRule> copyOfRules = new ArrayList<LayoutMLRule>();
									copyOfRules.addAll(col.getLayoutMLRulesIfCapable().getRules());
									for(LayoutMLRule rule : col.getLayoutMLRulesIfCapable().getRules()) {
										for(LayoutMLRuleAction action : rule.getLayoutMLRuleActions().getSingleActions()) {
											if(action.getEntity() != null && action.getEntity().equals(model.getEntityName())) {
												if(action.getTargetComponent().equals(attr.getInternalName())) {
													copyOfRules.remove(rule);
												}
											}
										}
									}
									col.getLayoutMLRulesIfCapable().clearRulesForComponent();
									for(LayoutMLRule rule : copyOfRules) {
										col.getLayoutMLRulesIfCapable().addRule(rule);
									}
								}
							}
						}
						else if(NuclosWizardUtils.searchParentEntity(getModel().getUID()).size() > 0) {
							List<WYSIWYGSubForm> allSubForms = new ArrayList<WYSIWYGSubForm>();
							ctrlPanel.getEditorPanel().getWYSIWYGComponents(WYSIWYGSubForm.class, ctrlPanel.getEditorPanel().getMainEditorPanel(), allSubForms);
							for(WYSIWYGSubForm collectable : allSubForms) {
								UID sSubFormEntity = collectable.getEntityUID();
								if(getModel().getUID().equals(sSubFormEntity) &&
										((attr.getMetaVO() != null && attr.getMetaVO().getUID().equals(parentEntity))
												|| (attr.getLookupMetaVO() != null && attr.getLookupMetaVO().getUID().equals(parentEntity)))) {
									ctrlPanel.getEditorPanel().getTableLayoutUtil().removeComponentFromLayout(collectable);
								}

								Collection<WYSIWYGSubFormColumn> copyOfColumns = new ArrayList<WYSIWYGSubFormColumn>(collectable.getColumns());
								for(WYSIWYGSubFormColumn col : copyOfColumns) {
									CollectableEntityField field = col.getEntityField();
									if(field.getUID().equals(attr.getUID())) {
										collectable.removeColumn(col.getName());
									}

									if(col.getLayoutMLRulesIfCapable() == null)
										continue;
									Collection<LayoutMLRule> copyOfRules = new ArrayList<>();
									copyOfRules.addAll(col.getLayoutMLRulesIfCapable().getRules());
									for(LayoutMLRule rule : col.getLayoutMLRulesIfCapable().getRules()) {
										for(LayoutMLRuleAction action : rule.getLayoutMLRuleActions().getSingleActions()) {
											if(action.getEntity() != null && action.getEntity().equals(model.getEntityName())) {
												if(action.getTargetComponent().equals(attr.getInternalName())) {
													copyOfRules.remove(rule);
												}
											}
										}
									}
									col.getLayoutMLRulesIfCapable().clearRulesForComponent();
									for(LayoutMLRule rule : copyOfRules) {
										col.getLayoutMLRulesIfCapable().addRule(rule);
									}
								}
							}
						}

						sLayout = ctrlPanel.getLayoutML();

						voLayout.setFieldValue(E.LAYOUT.layoutML, sLayout);

						try {
							MasterDataDelegate.getInstance().update(E.LAYOUT.getUID(), voLayout, null, null, false);
						}
						catch(CommonBusinessException e) {
							throw new NuclosFatalException(e);
						}
					}
					catch(Exception e) {
						// don't modify layout
						LOG.info("searchParentLayouts failed: " + e);
					}
				}
			}
		}
	}

	private Set<UID> searchParentEntity(UID entity) {
		Set<UID> setParents = new HashSet<>();

		for(MasterDataVO<?> vo : MasterDataCache.getInstance().get(E.LAYOUT.getUID())) {
			final LayoutMLParser parser = new LayoutMLParser();
			try {
				final String sLayout = vo.getFieldValue(E.LAYOUT.layoutML);
				if(sLayout == null)
					continue;
				final Set<UID> setSubforms = parser.getSubFormEntityUids(new InputSource(new StringReader(sLayout)));
				if(setSubforms.contains(entity)) {
					CollectableComparison compare = SearchConditionUtils.newComparison(E.LAYOUTUSAGE.layout, ComparisonOperator.EQUAL, vo.getPrimaryKey());
					for(MasterDataVO<?> voUsage : MasterDataDelegate.getInstance().getMasterData(E.LAYOUTUSAGE.getUID(), compare)) {
						setParents.add(voUsage.getFieldUid(E.LAYOUTUSAGE.entity));
					}
				}
				Set<UID> setCharts = parser.getChartEntityUids(new InputSource(new StringReader(sLayout)));
				if(setCharts.contains(entity)) {
					CollectableComparison compare = SearchConditionUtils.newComparison(E.LAYOUTUSAGE.layout, ComparisonOperator.EQUAL, vo.getPrimaryKey());
					for(MasterDataVO<?> voUsage : MasterDataDelegate.getInstance().getMasterData(E.LAYOUTUSAGE.getUID(), compare)) {
						setParents.add(voUsage.getFieldUid(E.LAYOUTUSAGE.entity));
					}
				}
			}
			catch(LayoutMLException e) {
				// do nothing here
				LOG.info("searchParentEntity failed: " + e);
			}
		}
		return setParents;
	}

	private void buildLayoutML(NuclosEntityWizardStaticModel wizardModel, CollectableMasterDataEntity masterDataEntity, UID entityUID) {
		try {
			final List<FieldMeta<?>> fields = new ArrayList<>();

			final List<FieldMeta<?>> metaFields = new ArrayList<>(
					MetaDataDelegate.getInstance().getAllEntityFieldsByEntity(wizardModel.getUID())
							.values());
			for(int i = 0; i < listAttributeOrder.getModel().getSize(); i++) {
				Attribute attr = (Attribute)listAttributeOrder.getModel().getElementAt(i);
				for(FieldMeta<?> voField : metaFields) {
					if(voField.getUID().equals(attr.getUID())) {
						fields.add(voField);
						break;
					}
				}
			}
			// add system fields.
			for(FieldMeta voField : metaFields) {
				if (!fields.contains(voField))
					fields.add(voField);
			}


			final Map<UID, String> attributeGroups = new HashMap<>();
			for(UID groupUID : getAttributeGroups()) {
				attributeGroups.put(groupUID, getAttributeGroupName(groupUID));
			}

			final DefaultLayoutMLFactory mlFactory = new DefaultLayoutMLFactory(UID.UID_NULL, attributeGroups);
			mlFactory.createLayout(entityUID, fields, cbAttributeGroup.isSelected(), cbSubforms.isSelected(), cbEditFields.isSelected());
		}
		catch(CommonValidationException e) {
			Errors.getInstance().showExceptionDialog(null, e);
		}catch(CommonBusinessException e1) {
			Errors.getInstance().showExceptionDialog(null, e1);
		}
	}

	private String getAttributeGroupName(UID groupUID) {
		final MasterDataVO<UID> result = MasterDataCache.getInstance()
				.get(E.ENTITYFIELDGROUP.getUID(), groupUID);
		if (result != null) {
			return result.getFieldValue(E.ENTITYFIELDGROUP.name);
		}
		return null;
	}

	private Set<UID> getAttributeGroups() {
		final Set<UID> set = new HashSet<>();
		for(Integer key : treeModel.getIndexMap().keySet()) {
			UID uid = treeModel.getIndexMap().get(key);
			if (uid != null) {
				set.add(treeModel.getIndexMap().get(key));
			}
		}

		return set;
	}

	private NucletFieldMeta<?> buildMasterDataField(
			Attribute attr,
			EntityMeta<?> parentVO,
			boolean bRemove)
			throws CommonFinderException, CommonPermissionException {

		final NucletFieldMeta<?> metaFieldVO = new NucletFieldMeta();
		metaFieldVO.setOrder(bRemove ? 0 : attr.getOrder());
		metaFieldVO.setPrimaryKey(attr.getUID());

		if (bRemove || attr.compareWithSnapshot() != 0 || attr.isFixedProblem()) {
			try {
				final MasterDataVO<UID> v = MasterDataDelegate.getInstance().get(E.ENTITYFIELD.getUID(), attr.getUID());

				metaFieldVO.setChangedAt(new InternalTimestamp(v.getChangedAt().getTime()));
				metaFieldVO.setChangedBy(v.getChangedBy());
				metaFieldVO.setCreatedAt(new InternalTimestamp(v.getCreatedAt().getTime()));
				metaFieldVO.setCreatedBy(v.getCreatedBy());
				metaFieldVO.setVersion(v.getVersion());
				metaFieldVO.flagUpdate();

				if(!v.getFieldValue(E.ENTITYFIELD.field).equals(attr.getInternalName())) {
					stFieldNameChanged.add(attr);
				}

				if(attr.isDistinct()) {
					if(attr.isDistinct() != v.getFieldValue(E.ENTITYFIELD.unique)) {
						final boolean blnAllowed = MetaDataDelegate.getInstance().isChangeDatabaseColumnToUniqueAllowed(attr.getUID());
						if(!blnAllowed) {
							final String msg = SpringLocaleDelegate.getInstance().getMessage(
									"wizard.step.entitysqllayout.6",
									"Das Feld {0} kann nicht auf ein eindeutiges Feld umgestellt werden.",
									attr.getLabel());
							model.setResultText(msg + "\n");
							throw new CommonFatalException(msg);
						}
					}
				}

			} catch (CommonFinderException finderex) {
				// Attribute is new
				metaFieldVO.flagNew();
			}
		}

		metaFieldVO.setEntity(parentVO.getUID());
		metaFieldVO.setForeignEntityField(null);
		metaFieldVO.setForeignEntity(null);
		metaFieldVO.setUnique(attr.isDistinct());
		metaFieldVO.setIndexed(attr.isIndexed());
		metaFieldVO.setLogBookTracking(attr.isLogBook());
		metaFieldVO.setHidden(attr.isHidden());

		metaFieldVO.setFormatOutput(attr.getOutputFormat());
		metaFieldVO.setDataType(attr.getDatatyp().getJavaType());
		metaFieldVO.setDefaultComponentType(attr.getDatatyp().getDefaultComponentType());
		metaFieldVO.setPrecision(attr.getDatatyp().getPrecision());
		metaFieldVO.setScale(attr.getDatatyp().getScale());
		metaFieldVO.setNullable(!attr.isMandatory());
		metaFieldVO.setModifiable(attr.isModifiable());
		metaFieldVO.setFormatInput(attr.getInputValidation());
		if(!StringUtils.isNullOrEmpty(attr.getCalcFunction()) || attr.getCalcAttributeDS() != null) {
			if (!StringUtils.isNullOrEmpty(attr.getCalcFunction())) {
				metaFieldVO.setCalcFunction(attr.getCalcFunction());
				metaFieldVO.setCalcAttributeParamValues(null);
			} else if (attr.getCalcAttributeDS() != null) {
				metaFieldVO.setCalcAttributeDS(attr.getCalcAttributeDS());
				if (!StringUtils.isNullOrEmpty(attr.getCalcAttributeParamValues())) {
					metaFieldVO.setCalcAttributeParamValues(attr.getCalcAttributeParamValues());
					metaFieldVO.setCalcAttributeAllowCustomization(attr.isCalcAttributeAllowCustomization());
				}
			}
			metaFieldVO.setCalcOndemand(attr.isCalcOndemand());
			metaFieldVO.setReadonly(Boolean.TRUE);
		}
		else {
			metaFieldVO.setCalcFunction(null);
			metaFieldVO.setCalcAttributeDS(null);
			metaFieldVO.setCalcAttributeParamValues(null);
			metaFieldVO.setCalcAttributeAllowCustomization(false);
			metaFieldVO.setCalcOndemand(false);
			metaFieldVO.setReadonly(parentVO.getVirtualEntity() != null ? attr.isReadonly() : Boolean.FALSE);
		}
		metaFieldVO.setInsertable(false);

		metaFieldVO.setSearchable(attr.isValueListProvider());

		metaFieldVO.setShowMnemonic(false);

		metaFieldVO.setLocaleResourceIdForLabel(attr.getLabelResource());
		metaFieldVO.setLocaleResourceIdForDescription(attr.getDescriptionResource());
		metaFieldVO.setFieldGroup(attr.getAttributeGroup());

		String sDbFieldName = attr.getDbName();
		if (!model.isProxy() && !model.isGeneric()) {
			attr.setDbName(sDbFieldName.replaceAll("[^A-Za-z0-9_]", "_"));
		}

		metaFieldVO.setDbColumn(attr.getDbName());
		metaFieldVO.setFieldName(attr.getInternalName());
		if(attr.getIdDefaultValue() != null && attr.getIdDefaultValue().getId() != null){
			if (attr.getIdDefaultValue().getId() instanceof Long) {
				metaFieldVO.setDefaultForeignId((Long) attr.getIdDefaultValue().getId());
				metaFieldVO.setDefaultValue(attr.getIdDefaultValue().getValue());
			} else if (attr.getIdDefaultValue().getId() instanceof Integer) {
				metaFieldVO.setDefaultForeignId((long) (Integer) attr.getIdDefaultValue().getId());
				metaFieldVO.setDefaultValue(attr.getIdDefaultValue().getValue());
			} else {
				LOG.warn("Unsupported Default ID type " + attr.getIdDefaultValue().getId());
			}
		}
		else {
			if(attr.getMetaVO() != null && attr.getField() != null) {
				metaFieldVO.setDefaultForeignId(null);
				metaFieldVO.setDefaultValue(null);
			} else {
				metaFieldVO.setDefaultValue(attr.getDefaultValue());
			}

		}
		metaFieldVO.setSearchable(attr.isValueListProvider());
		if(attr.getField() != null && attr.getField().length() < 1)
			attr.setField(null);

		metaFieldVO.setForeignIntegrationPoint(attr.getForeignIntegrationPoint());
		if(attr.getForeignIntegrationPoint() != null || (attr.getMetaVO() != null && attr.getField() != null)) {
			metaFieldVO.setModifiable(false);
			if (attr.getMetaVO() != null) {
				metaFieldVO.setForeignEntity(attr.getMetaVO().getUID());
			}
			metaFieldVO.setOnDeleteCascade(attr.isOnDeleteCascade());
			metaFieldVO.setForeignEntityField(attr.getField());
			metaFieldVO.setSearchField(attr.getSearchField());
			if(!attr.getDbName().startsWith("STRVALUE_") && !model.isProxy() && !model.isGeneric()) {
				metaFieldVO.setDbColumn("STRVALUE_"+ attr.getDbName().replaceFirst("^INTID_", "").replaceFirst("^STRUID_", ""));
			}
			metaFieldVO.setSearchField(attr.getSearchField());
		}
		else if (attr.getMetaVO() != null && attr.getField() == null) {
			metaFieldVO.setForeignEntity(attr.getMetaVO().getUID());
			metaFieldVO.setOnDeleteCascade(attr.isOnDeleteCascade());
			if(!attr.getDbName().startsWith("INTID_") && !model.isProxy() && !model.isGeneric()) {
				metaFieldVO.setDbColumn("INTID_"+ attr.getDbName().replaceFirst("^STRVALUE_", ""));
			}
			metaFieldVO.setSearchField(null);
			metaFieldVO.setForeignEntityField(null);
			metaFieldVO.setModifiable(false);
		}
		else {
			metaFieldVO.setModifiable(attr.isModifiable());
		}
		if(attr.getLookupMetaVO() != null && attr.getField() != null) {
			metaFieldVO.setModifiable(false);
			metaFieldVO.setLookupEntity(attr.getLookupMetaVO().getUID());
			metaFieldVO.setOnDeleteCascade(attr.isOnDeleteCascade());
			metaFieldVO.setLookupEntityField(attr.getField());
			metaFieldVO.setSearchField(attr.getSearchField());
		}
		else if (attr.getLookupMetaVO() != null && attr.getField() == null) {
			metaFieldVO.setLookupEntity(attr.getLookupMetaVO().getUID());
			metaFieldVO.setOnDeleteCascade(attr.isOnDeleteCascade());
			metaFieldVO.setLookupEntityField(null);
			metaFieldVO.setSearchField(null);
			metaFieldVO.setModifiable(false);
		}
		else {
			metaFieldVO.setModifiable(attr.isModifiable());
		}
		if (StringUtils.equals(attr.getDatatyp().getDefaultComponentType(), DefaultComponentTypes.AUTONUMBER)) {
			metaFieldVO.setNullable(true);
		}
		// handle autonumber
		if (attr.getAutoNumberMetaVO() != null) {
			UID idAutoNumberEntity = attr.getAutoNumberMetaVO().getUID();
			metaFieldVO.setAutonumberEntity(idAutoNumberEntity);
		}

		setDefaultMandatoryValue(attr, metaFieldVO);
		metaFieldVO.setCalculationScript(attr.getCalculationScript());
		metaFieldVO.setBackgroundColorScript(attr.getBackgroundColorScript());

		metaFieldVO.setLocalized(attr.isLocalized());
		metaFieldVO.setComment(attr.getComment());


		if (bRemove) {
			metaFieldVO.flagRemove();
		}

		return metaFieldVO;
	}

	private void setDefaultMandatoryValue(Attribute attr, NucletFieldMeta<?> metaFieldVO) {
		if(attr.getMandatoryValue() == null || !attr.isMandatory())
			return;
		String sJavaType = attr.getDatatyp().getJavaType();
		if(sJavaType.equals("java.lang.Integer")) {
			metaFieldVO.setDefaultMandatory(attr.getMandatoryValue().toString());
		}
		else if(sJavaType.equals("java.lang.Double")) {
			metaFieldVO.setDefaultMandatory(attr.getMandatoryValue().toString());
		}
		else if(sJavaType.equals("java.util.Date")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
			String sDate = sdf.format((Date)(attr.getMandatoryValue()));
			metaFieldVO.setDefaultMandatory(sDate);
		}
		else if(sJavaType.equals("java.lang.Boolean")){
			metaFieldVO.setDefaultMandatory(attr.getMandatoryValue().toString());
		}
		else {
			metaFieldVO.setDefaultMandatory(attr.getMandatoryValue().toString());
		}


	}

	protected String replace(String str) {
		str = StringUtils.replace(str, "\u00e4", "ae");
		str = StringUtils.replace(str, "\u00f6", "oe");
		str = StringUtils.replace(str, "\u00fc", "ue");
		str = StringUtils.replace(str, "\u00c4", "Ae");
		str = StringUtils.replace(str, "\u00d6", "Oe");
		str = StringUtils.replace(str, "\u00dc", "Ue");
		str = StringUtils.replace(str, "\u00df", "ss");
		str = str.replaceAll("[^\\w]", "");
		return str;
	}

	private String removeAttributesFromWYSIWYGLayout(UID layoutUID, String layoutML, List<Attribute> listAttributes, UID entityUID) {
		String sLayout = null;

		WYSIWYGLayoutControllingPanel ctrlPanel = new WYSIWYGLayoutControllingPanel(new WYSIWYGMetaInformation(layoutUID));

		CollectableEntity entity = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(entityUID);
		ctrlPanel.getMetaInformation().setCollectableEntity(entity);
	
		try {
			ctrlPanel.setLayoutML(layoutML);
			for (WYSIWYGComponent comp : ctrlPanel.getAllComponents()) {
				final PropertyValue<String> uidField = (PropertyValue<String>) comp.getProperties().getProperty(WYSIWYGStringsAndLabels.PROPERTY_LABELS.UID);

				for (Attribute attr : listAttributes) {
					if (LangUtils.equal(uidField == null ? null : UID.parseUID(uidField.getValue()), attr.getUID())) {
						ctrlPanel.getEditorPanel().getTableLayoutUtil().removeComponentFromLayout(comp);
					}
				}
			}
			sLayout = ctrlPanel.getLayoutML();
		} catch (Exception e) {
			LOG.info("removeAttributesFromWYSIWYGLayout failed: " + e);
		}

		return sLayout;
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	private void removeValueListIfNotNeeded() {
		// check to remove first.
		for(Attribute attr : getModel().getAttributeModel().getRemoveAttributes()) {
			if(!attr.isValueList() || attr.isValueListNew())
				continue;

			try {
				MetaDataDelegate.getInstance().removeEntity(attr.getMetaVO().getUID(), false);
			} catch (Exception e) {
				// ignore. @todo catch a better exception than pure exception.
				// (in case the entity is currently used a DBException is thrown.)
				// or we have to iterate through the entityfields if valuelist is used elsewhere.
				LOG.warn("can not remove entity " + attr.getMetaVO().getUID() + ". maybe it is used elsewhere. " + e.getMessage());
			}
		}
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	private void buildValueListIfNeeded() {
		final List<FieldMetaTransport> lstFields = new ArrayList<>();
		for(Attribute attr : getModel().getAttributeModel().getNucletAttributes()) {

			if(!attr.isValueList())
				continue;

			if(attr.isValueListNew()) {
				createValueListEntity(lstFields, attr);
			}

			for(ValueList valueList: attr.getValueList()) {
				Map<UID, Object> mpFields = new HashMap<UID, Object>();

				UID value = null;
				UID mnemonic = null;
				UID description = null;
				UID validFrom = null;
				UID validUntil = null;

				for (FieldMeta<?> fieldMeta : MetaDataDelegate.getInstance().getAllEntityFieldsByEntity(attr.getValueListEntity()).values()) {
					if ("value".equals(fieldMeta.getFieldName())) {
						value = fieldMeta.getUID();
					} else if ("mnemonic".equals(fieldMeta.getFieldName())) {
						mnemonic = fieldMeta.getUID();
					} else if ("description".equals(fieldMeta.getFieldName())) {
						description = fieldMeta.getUID();
					} else if ("validFrom".equals(fieldMeta.getFieldName())) {
						validFrom = fieldMeta.getUID();
					} else if ("validUntil".equals(fieldMeta.getFieldName())) {
						validUntil = fieldMeta.getUID();
					}
				}

				if (value!=null) mpFields.put(value, valueList.getLabel());
				if (mnemonic!=null) mpFields.put(mnemonic, valueList.getMnemonic());
				if (description!=null) mpFields.put(description, valueList.getDescription());
				if (validFrom!=null) mpFields.put(validFrom, valueList.getValidFrom());
				if (validUntil!=null) mpFields.put(validUntil, valueList.getValidUntil());

				try {
					final UID entity = getModel().getUID();
					if(valueList.getId() != null) {
						MasterDataVO<Long> vo = new MasterDataVO<Long>(entity, valueList.getId(),
								new Date(), "nuclos", new Date(), "nuclos", valueList.getVersionId(), mpFields, null, null, false);
						MasterDataDelegate.getInstance().update(attr.getValueListEntity(), vo, null, null, false);
					}
					else {
						MasterDataVO<?> vo = new MasterDataVO<Long>(entity, null,
								new Date(), "nuclos", new Date(), "nuclos", 1, mpFields, null, null, false);
						MasterDataDelegate.getInstance().create(attr.getValueListEntity(), vo, null, null);
					}
				}
				catch(CommonBusinessException e) {
					LOG.error("buildValueListIfNeeded failed: " + e, e);
				}
			}
		}
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	private void createValueListEntity(List<FieldMetaTransport> lstFields, Attribute attr) {

		EntityMetaTransport toEntity = new EntityMetaTransport();
		NucletEntityMeta voEntity = new NucletEntityMeta();
		voEntity.flagNew();
		voEntity.setSearchable(true);
		voEntity.setCacheable(false);
		voEntity.setEditable(true);
		voEntity.setImportExport(true);
		voEntity.setTreeRelation(false);
		voEntity.setTreeGroup(false);
		voEntity.setStateModel(false);
		voEntity.setDynamic(false);
		voEntity.setLogBookTracking(false);
		voEntity.setFieldValueEntity(Boolean.TRUE);
		toEntity.setEntityMetaVO(voEntity);
		toEntity.setTranslation(new ArrayList<TranslationVO>());
		toEntity.setTreeView(new ArrayList<EntityTreeViewVO>());
		voEntity.setEntityName(attr.getValueListEntityName());
		voEntity.setDbTable("V_EO_"+ attr.getValueListEntityName());

		attr.setMetaVO(voEntity);
		attr.setField("${value}");

		NucletFieldMeta<?> voField = new NucletFieldMeta();
		buildValueListField("value","STRVALUE","java.lang.String", 255, attr, voField);
		FieldMetaTransport toField = new FieldMetaTransport();
		toField.setEntityFieldMeta(voField);
		toField.setTranslation(new ArrayList<TranslationVO>());
		lstFields.add(toField);

		voField = new NucletFieldMeta();
		buildValueListField("mnemonic","STRMNEMONIC","java.lang.String", 255, attr, voField);
		toField = new FieldMetaTransport();
		toField.setEntityFieldMeta(voField);
		toField.setTranslation(new ArrayList<TranslationVO>());
		lstFields.add(toField);

		voField = new NucletFieldMeta();
		buildValueListField("description","STRDESCRIPTION","java.lang.String", 255, attr, voField);
		toField = new FieldMetaTransport();
		toField.setEntityFieldMeta(voField);
		toField.setTranslation(new ArrayList<TranslationVO>());
		lstFields.add(toField);

		voField = new NucletFieldMeta();
		buildValueListField("validFrom","DATVALIDFROM","java.util.Date", null, attr, voField);
		toField = new FieldMetaTransport();
		toField.setEntityFieldMeta(voField);
		toField.setTranslation(new ArrayList<TranslationVO>());
		lstFields.add(toField);

		voField = new NucletFieldMeta();
		buildValueListField("validUntil","DATVALIDUNTIL","java.util.Date", null, attr, voField);
		toField = new FieldMetaTransport();
		toField.setEntityFieldMeta(voField);
		toField.setTranslation(new ArrayList<TranslationVO>());
		lstFields.add(toField);

		try {
			MetaDataDelegate.getInstance().createOrModifyEntity(toEntity, lstFields);
		} catch(NuclosBusinessException e) {
			LOG.error("createValueList failed: " + e, e);
		}
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	private void buildValueListField(String fieldname, String dbField, String javaType, Integer iScale, Attribute attr, NucletFieldMeta<?> voField) {
		voField.flagNew();
		voField.setUnique(false);
		voField.setLogBookTracking(false);
		voField.setModifiable(false);
		voField.setSearchable(false);
		voField.setInsertable(false);
		voField.setUnique(false);
		voField.setShowMnemonic(false);
		voField.setNullable(true);
		voField.setReadonly(false);

		voField.setDataType(javaType);
		voField.setPrecision(null);
		voField.setScale(iScale);
		voField.setFieldName(fieldname);
		voField.setDbColumn(dbField);
	}

	private NuclosEntityWizardStaticModel getModel() {
		return model;
	}
}
