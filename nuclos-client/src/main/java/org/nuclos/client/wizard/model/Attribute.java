//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.model;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.wizard.util.DefaultValue;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import com.thoughtworks.xstream.XStream;

public class Attribute implements Serializable {
	
	private final boolean bSystem;

	UID uid;
	Long internalId;
	String label;
	String description;
	DataTyp dataTyp;
	String internalName;
	String oldInternalName;
	String dbName;
	boolean mandatory;
	boolean logBook;
	boolean distinct;
	boolean readonly;
	boolean hidden;
	boolean modifiable;
	EntityMeta<?> metaVO;
	UID foreignIntegrationPoint;
	EntityMeta<?> lookupMetaVO;
	EntityMeta<?> autoNumberMetaVO;
	boolean blnValueListProvider;
	String strField;
	String strSearchField;
	String defaultValue;
	DefaultValue idDefaultValue;
	UID attributeGroup;
	String calcFunction;
	UID calcAttributeDS;
	String calcAttributeParamValues;
	boolean calcAttributeAllowCustomization;
	boolean calcOndemand;
	UID valueListEntity;
	String valueListEntityName;
	String outputFormat;
	String sInputValidation;
	List<ValueList> lstValueList;

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	boolean blnValueListNew;
	boolean indexed;
	Object mandatoryValue;
	boolean onDeleteCascade;
	NuclosScript calculationScript;
	NuclosScript backgroundColorScript;
	Boolean isLocalized;
	String comment;
	
	private String labelRes;
	private String descriptionRes;

	boolean blnResume;

	boolean blnRemove;
	
	boolean blnFixedProblem = false;
	
	private int order;
	private transient String xmlSnapshot;

	public Attribute(boolean bSystem) {
		this.bSystem = bSystem;
	}
	
	public boolean isSystem() {
		return bSystem;
	}
	
	public void setSnapshot() {
		xmlSnapshot = new XStream().toXML(this);
	}
	
	public int compareWithSnapshot() {
		String xmlNow = new XStream().toXML(this);
		if (xmlSnapshot == null) {
			return 1;
		}
		return xmlNow.compareTo(xmlSnapshot);
	}
	
	public void setOrder(int o) {
		this.order = o;
	}
	
	public int getOrder() {
		return order;
	}

	public boolean hasInternalNameChanged() {
		if(oldInternalName != null && oldInternalName.length() > 0) {
			return !oldInternalName.equals(internalName);
		}
		return false;
	}

	public UID getUID() {
		return uid;
	}

	public void setUID(UID uid) {
		this.uid = uid;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public DataTyp getDatatyp() {
		return dataTyp;
	}

	public void setDatatyp(DataTyp datatyp) {
		this.dataTyp = datatyp;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public boolean isLogBook() {
		return logBook;
	}

	public void setLogBook(boolean logBook) {
		this.logBook = logBook;
	}

	public boolean isModifiable() {
		return modifiable;
	}

	public void setModifiable(boolean bModifiable) {
		this.modifiable = bModifiable;
	}

	public boolean isRemove() {
		return blnRemove;
	}

	public void setRemove(boolean remove) {
		this.blnRemove = remove;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public String getInternalName() {
		return internalName;
	}

	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	public void setOldInternalName(String name) {
		this.oldInternalName = name;
	}

	public String getOldInternalName() {
		return this.oldInternalName;
	}

	public void setDbName(String sName) {
		this.dbName = sName;
	}

	public static String getDBPrefix(Attribute attr) {
		if(attr.getDatatyp().getJavaType().equals("java.lang.String")) {
			return NuclosWizardUtils.COLUMN_STRING_PREFFIX;
		}
		else if(attr.getDatatyp().getJavaType().equals("java.lang.Boolean")) {
			return NuclosWizardUtils.COLUMN_BOOLEAN_PREFFIX;
		}
		else if(attr.getDatatyp().getJavaType().equals("java.lang.Integer")) {
			return NuclosWizardUtils.COLUMN_INTEGER_PREFFIX;
		}
		else if(attr.getDatatyp().getJavaType().equals("java.lang.Double")) {
			return NuclosWizardUtils.COLUMN_DOUBLE_PREFFIX;
		}
		else if(attr.getDatatyp().getJavaType().equals("java.util.Date")) {
			return NuclosWizardUtils.COLUMN_DATE_PREFFIX;
		}
		else if(attr.getDatatyp().getJavaType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile")) {
			return NuclosWizardUtils.COLUMN_FILE_PREFFIX;
		}
		else {
			return NuclosWizardUtils.COLUMN_PREFFIX;
		}
	}

	public String getDbName() {
		if(dbName != null)
			return dbName;
		else {
			if(this.getDatatyp().getJavaType().equals("java.lang.String")) {
				return NuclosWizardUtils.COLUMN_STRING_PREFFIX + internalName.replaceAll(" ", "");
			}
			else if(this.getDatatyp().getJavaType().equals("java.lang.Boolean")) {
				return NuclosWizardUtils.COLUMN_BOOLEAN_PREFFIX + internalName.replaceAll(" ", "");
			}
			else if(this.getDatatyp().getJavaType().equals("java.lang.Integer")) {
				return NuclosWizardUtils.COLUMN_INTEGER_PREFFIX + internalName.replaceAll(" ", "");
			}
			else if(this.getDatatyp().getJavaType().equals("java.lang.Double")) {
				return NuclosWizardUtils.COLUMN_DOUBLE_PREFFIX + internalName.replaceAll(" ", "");
			}
			else if(this.getDatatyp().getJavaType().equals("java.util.Date")) {
				return NuclosWizardUtils.COLUMN_DATE_PREFFIX + internalName.replaceAll(" ", "");
			}
			else if(this.getDatatyp().getJavaType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile")) {
				return NuclosWizardUtils.COLUMN_FILE_PREFFIX + internalName.replaceAll(" ", "");
			}
			else {
				return NuclosWizardUtils.COLUMN_PREFFIX + internalName.replaceAll(" ", "");
			}
		}
	}
	
	public Boolean isLocalized() {
		return isLocalized;
	}

	public void setIsLocalized(Boolean isLocalized) {
		this.isLocalized = isLocalized;
	}

	public EntityMeta<?> getMetaVO() {
		return metaVO;
	}

	public void setMetaVO(EntityMeta<?> metaVO) {
		this.metaVO = metaVO;
	}

	public UID getForeignIntegrationPoint() {
		return foreignIntegrationPoint;
	}

	public void setForeignIntegrationPoint(final UID foreignIntegrationPoint) {
		this.foreignIntegrationPoint = foreignIntegrationPoint;
	}

	public EntityMeta<?> getLookupMetaVO() {
		return lookupMetaVO;
	}

	public void setLookupMetaVO(EntityMeta<?> metaVO) {
		this.lookupMetaVO = metaVO;
	}
	
	public EntityMeta<?> getAutoNumberMetaVO() {
		return autoNumberMetaVO;
	}
	
	public void setAutoNumberMetaVO(final EntityMeta<?> metaVO) {
		this.autoNumberMetaVO = metaVO;
	}

	public String getField() {
		return strField;
	}

	public void setField(String strField) {
		this.strField = strField;
	}

	public String getSearchField() {
		return strSearchField;
	}

	public void setSearchField(String strSearchField) {
		this.strSearchField = strSearchField;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		if ("".equals(defaultValue)) {
			this.defaultValue = null;
		} else {
			this.defaultValue = defaultValue;
		}
	}

	@Override
	public String toString() {
		return getInternalName();
	}
	
	@Override
	public Object clone() {
		Attribute attr = new Attribute(bSystem);

		attr.setDatatyp(this.getDatatyp());
		attr.setDbName(this.getDbName());
		attr.setDefaultValue(this.getDefaultValue());
		attr.setIdDefaultValue(this.idDefaultValue);
		attr.setDescription(this.getDescription());
		attr.setDistinct(this.isDistinct());
		attr.setHidden(this.isHidden());
		attr.setModifiable(this.isModifiable());
		attr.setField(this.getField());
		attr.setSearchField(this.getSearchField());
		attr.setUID(this.getUID());
		attr.setInternalName(this.getInternalName());
		attr.setLabel(this.getLabel());
		attr.setLogBook(this.isLogBook());
		attr.setMandatory(this.isMandatory());
		attr.setMetaVO(this.getMetaVO());
		attr.setForeignIntegrationPoint(this.getForeignIntegrationPoint());
		attr.setLookupMetaVO(this.getLookupMetaVO());
		attr.setOldInternalName(this.oldInternalName);
		attr.setRemove(this.isRemove());
		attr.setResume(this.isForResume());
		attr.setValueListProvider(this.isValueListProvider());
		attr.setValueList(this.getValueList());
		attr.setValueListNew(this.isValueListNew());
		attr.setValueListEntity(this.getValueListEntity());
		attr.setLabelResource(this.getLabelResource());
		attr.setDescriptionResource(this.getDescriptionResource());
		attr.setAttributeGroup(this.getAttributeGroup());
		attr.setCalcFunction(this.getCalcFunction());
		attr.setCalcAttributeDS(this.getCalcAttributeDS());
		attr.setCalcAttributeParamValues(this.getCalcAttributeParamValues());
		attr.setCalcAttributeAllowCustomization(this.isCalcAttributeAllowCustomization());
		attr.setCalcOndemand(this.isCalcOndemand());
		attr.setInputValidation(this.sInputValidation);
		attr.setIndexed(this.isIndexed());
		attr.setMandatoryValue(this.getMandatoryValue());
		attr.setOnDeleteCascade(this.isOnDeleteCascade());
		attr.setCalculationScript(this.getCalculationScript());
		attr.setBackgroundColorScript(this.getBackgroundColorScript());
		attr.setAutoNumberMetaVO(this.getAutoNumberMetaVO());
		attr.setIsLocalized(this.isLocalized());
		attr.setComment(this.getComment());
		
		return attr;
	}

	public boolean isForResume() {
		return blnResume;
	}

	public void setResume(boolean resume) {
		this.blnResume = resume;
	}

	public boolean isValueListProvider() {
		return blnValueListProvider;
	}

	public void setValueListProvider(boolean valuelistprovider) {
		this.blnValueListProvider = valuelistprovider;
	}

	public UID getAttributeGroup() {
		return attributeGroup;
	}

	public void setAttributeGroup(UID attributeGroup) {
		this.attributeGroup = attributeGroup;
	}

	public String getCalcFunction() {
		return calcFunction;
	}
	
	public void setCalcFunction(String calcFunction) {
		this.calcFunction = calcFunction;
	}
	
	public UID getCalcAttributeDS() {
		return calcAttributeDS;
	}

	public String getCalcAttributeParamValues() {
		return calcAttributeParamValues;
	}
	
	public boolean isCalcAttributeAllowCustomization() {
		return calcAttributeAllowCustomization;
	}

	public boolean isCalcOndemand() {
		return calcOndemand;
	}	

	public void setCalcAttributeDS(UID calcAttributeDS) {
		this.calcAttributeDS = calcAttributeDS;
	}

	public void setCalcAttributeParamValues(String calcAttributeParamValues) {
		this.calcAttributeParamValues = calcAttributeParamValues;
	}
	
	public void setCalcAttributeAllowCustomization(boolean calcAttributeAllowCustomization) {
		this.calcAttributeAllowCustomization = calcAttributeAllowCustomization;
	}	

	public void setCalcOndemand(boolean calcOndemand) {
		this.calcOndemand = calcOndemand;
	}	

	public void setValueList(List<ValueList> lstValueList) {
		this.lstValueList = lstValueList;
	}

	public List<ValueList> getValueList() {
		if(lstValueList == null)
			lstValueList = new ArrayList<ValueList>();
		return lstValueList;
	}

	public String getLabelResource() {
		return labelRes;
	}

	public void setLabelResource(String labelRes) {
		this.labelRes = labelRes;
	}
	
	public void checkSystemLabelResource(List<TranslationVO> lstTrans) {
		setLabelResource(getCheckedResource(getLabelResource(), "label", lstTrans));
	}

	public String getDescriptionResource() {
		return descriptionRes;
	}

	public void setDescriptionResource(String descriptionRes) {
		this.descriptionRes = descriptionRes;
	}
	
	public void checkSystemDescriptionResource(List<TranslationVO> lstTrans) {
		setDescriptionResource(getCheckedResource(getDescriptionResource(), "description", lstTrans));		
	}
	
	private static String getCheckedResource(String resource, String tag, List<TranslationVO> lstTrans) {
		if (resource != null && resource.startsWith("nuclos.entityfield.eo")) {
			for (TranslationVO tvo : lstTrans) {
				String labelNow = tvo.getLabels().get(tag);
				if (labelNow != null) {
				    for (LocaleInfo info : LocaleDelegate.getInstance().getAllLocales(false)) {
				    	if (info.getLocale().equals(tvo.getLocale())) {
				    		String labelOld = SpringLocaleDelegate.getInstance().getResourceById(info, resource);
				    		if (!LangUtils.equal(labelNow, labelOld)) {
				    			return null;
				    		}
				    		continue;
				    	}
				    }
				}
			}
		}					
		return resource;
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	public void setValueListEntity(UID valueListEntity) {
		this.valueListEntity = valueListEntity;
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	public UID getValueListEntity() {
		return this.valueListEntity;
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	public void setValueListEntityName(String valueListEntityName) {
		this.valueListEntityName = valueListEntityName;
	}

	public String getValueListEntityName() {
		return this.valueListEntityName;
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	public boolean isValueList() {
		return this.valueListEntityName != null;
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	public void setValueListNew(boolean blnNew) {
		this.blnValueListNew = blnNew;
	}

	/** @deprecated ValueList (Werteliste) is removed. */
	@Deprecated
	public boolean isValueListNew()  {
		return this.blnValueListNew;
	}

	public String getInputValidation() {
    	return sInputValidation;
    }

	public void setInputValidation(String inputValidation) {
    	this.sInputValidation = inputValidation;
    }

	public void setIdDefaultValue(DefaultValue dv){
		this.idDefaultValue = dv;
	}

	public DefaultValue getIdDefaultValue() {
		return this.idDefaultValue;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public boolean isImage() {
		return this.getDatatyp().getJavaType().equals("org.nuclos.common.NuclosImage");
	}

	public boolean isPasswordField() {
		return this.getDatatyp().getJavaType().equals("org.nuclos.common.NuclosPassword");
	}

	public boolean isFileType() {
		return this.getDatatyp().getJavaType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile");
	}

	public Object getMandatoryValue() {
		return this.mandatoryValue;
	}

	public void setMandatoryValue(Object value) {
		this.mandatoryValue = value;
	}

	public String getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public boolean isReadonly() {
		return readonly;
	}

	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public boolean isOnDeleteCascade() {
		return onDeleteCascade;
	}

	public void setOnDeleteCascade(boolean onDeleteCascade) {
		this.onDeleteCascade = onDeleteCascade;
	}

	public NuclosScript getCalculationScript() {
		return calculationScript;
	}

	public void setCalculationScript(NuclosScript calculationScript) {
		this.calculationScript = calculationScript;
	}

	public NuclosScript getBackgroundColorScript() {
		return backgroundColorScript;
	}

	public void setBackgroundColorScript(NuclosScript backgroundColorScript) {
		this.backgroundColorScript = backgroundColorScript;
	}

	@Override
	public int hashCode() {
		if(this.internalName != null){
			return this.internalName.hashCode();
		}
		else
			return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Attribute) {
			Attribute that = (Attribute)obj;
			return ObjectUtils.equals(that.getUID(), this.getUID()) || StringUtils.equals(that.getInternalName(), this.getInternalName());
		}
		return false;
	}
	
	public static void fillAttributeGroupBox(JComboBox cbxAttributeGroup, final Attribute attribute, boolean bEditMode) {
		if (cbxAttributeGroup.getItemCount() == 0) {
			cbxAttributeGroup.setRenderer(new DefaultListCellRenderer() {
				@Override
				public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
					final JLabel lbl = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
					if (value instanceof MasterDataVO<?>) {
						lbl.setText(((MasterDataVO<?>)value).getFieldValue(E.ENTITYFIELDGROUP.name));
					}
					return lbl;
				}
			});
			
			cbxAttributeGroup.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if(e.getStateChange() == ItemEvent.SELECTED) {
						final Object obj = e.getItem();
						if (obj instanceof MasterDataVO<?>) {
							attribute.setAttributeGroup(((MasterDataVO<UID>)obj).getPrimaryKey());
						} else {
							attribute.setAttributeGroup(null);
						}
					}
				}
			});

		}

		ItemListener ilArray[] = cbxAttributeGroup.getItemListeners();
		for(ItemListener il : ilArray) {
			cbxAttributeGroup.removeItemListener(il);
		}

		cbxAttributeGroup.removeAllItems();		
		cbxAttributeGroup.addItem(" ");

		final List<MasterDataVO<?>> lstAttributeGroup = new ArrayList<MasterDataVO<?>>(MasterDataCache.getInstance().get(E.ENTITYFIELDGROUP.getUID()));
		Collections.sort(lstAttributeGroup, new Comparator<MasterDataVO<?>>() {
			@Override
            public int compare(MasterDataVO<?> o1, MasterDataVO<?> o2) {
	            String sField1 = o1.getFieldValue(E.ENTITYFIELDGROUP.name);
	            String sField2 = o2.getFieldValue(E.ENTITYFIELDGROUP.name);
	            return sField1.compareToIgnoreCase(sField2);
            }
		});
		for(MasterDataVO<?> voAttributeGroup : lstAttributeGroup) {
			cbxAttributeGroup.addItem((MasterDataVO<UID>)voAttributeGroup);
		}
		
		cbxAttributeGroup.setSelectedIndex(0);
		for(ItemListener il : ilArray) {
			cbxAttributeGroup.addItemListener(il);
		}
		
		if (bEditMode) {
			cbxAttributeGroup.setSelectedItem(
					attribute.getAttributeGroup() == null ? null
							: MasterDataCache.getInstance().get(E.ENTITYFIELDGROUP.getUID(), attribute.getAttributeGroup()));

		} else {
			if (cbxAttributeGroup.getModel().getSize() > 1) {
				cbxAttributeGroup.setSelectedIndex(1);					
			}
		}
		
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void fixedProblem() {
		this.blnFixedProblem = true;
	}
	
	public boolean isFixedProblem() {
		return this.blnFixedProblem;
	}
}
