//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import org.apache.log4j.Logger;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.entityobject.EntityObjectPresenter;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.wizard.NuclosEntityWizardStaticModel;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityAttributeRelationShipStep extends NuclosEntityAttributeAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityAttributeRelationShipStep.class);

	private JLabel lbIntegrationPoint;
	private JComboBox cbxIntegrationPoint;
	private boolean cbxIntegrationPointListenerEnabled;
	private final Map<UID, EntityObjectPresenter> mapIntegrationPointsById = new HashMap<>();

	private JLabel lbEntity;
	private JComboBox cbxEntity;

	private JScrollPane scrollPane;
	private JList listFields;
	private JButton btSelect;
	private JButton btSelectSearch;
	private JLabel lblSelectSearch;

	private JLabel lbInfo;

	private JTextField tfAlternativeTextField;
	private JTextField tfAlternativeSearchTextField;

	private JLabel lbValueListProvider;
	private JCheckBox cbValueListProvider;

	private JLabel lbOnDeleteCascade;
	private JCheckBox cbOnDeleteCascade;

	private NuclosEntityWizardStaticModel parentWizardModel;

	public NuclosEntityAttributeRelationShipStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	@Override
	protected void initComponents() {
		double size [][] = {{TableLayout.PREFERRED,40, TableLayout.FILL}, {20,20,20,20,20,50,20,TableLayout.PREFERRED, TableLayout.FILL}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);

		lbIntegrationPoint = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.10", "Verweis auf Integrationspunkt"));
		cbxIntegrationPoint = new JComboBox();
		cbxIntegrationPoint.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.10", "Der Verweis auf das Businessobjekt wird vom Integrationspunkt gepflegt."));

		lbEntity = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.1", "Verweis auf Entit\u00e4t"));
		cbxEntity = new JComboBox();
		cbxEntity.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.1", "Verweis auf Entit\u00e4t"));

		tfAlternativeTextField = new JTextField();
		tfAlternativeTextField.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.3", "Fremdschl\u00fcsselaufbau"));
		tfAlternativeTextField.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());

		tfAlternativeSearchTextField = new JTextField();
		tfAlternativeSearchTextField.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.3a", "Suchschl\u00fcsselaufbau"));
		tfAlternativeSearchTextField.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());

		lbOnDeleteCascade = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.deletecascade.label", "Cascade on delete"));
		cbOnDeleteCascade = new JCheckBox();
		cbOnDeleteCascade.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.attributerelationship.deletecascade.description", "Delete all referencing objects in this entity if the referenced object is deleted."));

		lbValueListProvider = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.4", "Suchfeld"));
		cbValueListProvider = new JCheckBox();
		cbValueListProvider.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.4", "Suchfeld"));

		lbInfo = new JLabel();

		listFields = new JList();
		listFields.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane = new JScrollPane(listFields);
		scrollPane.setPreferredSize(new Dimension(150, 25));
		btSelect = new JButton(">");
		btSelectSearch = new JButton(">");
		
		lblSelectSearch = new JLabel(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.9", "Optionale Suchkriterien") + ": ");
		lblSelectSearch.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("wizard.step.attributerelationship.tooltip.9", "Optionale Suchkriterien, die bei einer Suche herangezogen werden. Wenn leer, werden die normalen Kriterien genutzt."));
		
		// set search field visible
		lblSelectSearch.setVisible(false);
		btSelectSearch.setVisible(false);
		tfAlternativeSearchTextField.setVisible(false);
		
		this.add(lbIntegrationPoint, "0,0, 1,0");
		this.add(cbxIntegrationPoint, "2,0");

		this.add(lbEntity, "0,1, 1,1");
		this.add(cbxEntity, "2,1");

		this.add(lbOnDeleteCascade, "0,2, 1,2");
		this.add(cbOnDeleteCascade, "2,2");

		this.add(lbValueListProvider, "0,3, 1,3");
		this.add(cbValueListProvider, "2,3");


		this.add(scrollPane, "0,4, 0,8");

		this.add(btSelect, "1,4");
		
		this.add(tfAlternativeTextField, "2,4");
		this.add(lbInfo, "0,7, 2,7");

	    this.add(lblSelectSearch, "1,5,2,1");
		this.add(btSelectSearch, "1,6");
		this.add(tfAlternativeSearchTextField, "2,6");

		cbxIntegrationPoint.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if (!cbxIntegrationPointListenerEnabled) {
					return;
				}
				if(e.getStateChange() == ItemEvent.SELECTED) {
					final EntityObjectPresenter eop = (EntityObjectPresenter) e.getItem();
					if (eop.getEo() != null) {
						model.getAttribute().setForeignIntegrationPoint((UID) eop.getEo().getPrimaryKey());
						// get target entity from integration if set already (NUCLOS-6241)
						final EntityObjectVO<UID> eoIntegrationPoint = MasterDataCache.getInstance().<UID>get(E.NUCLET_INTEGRATION_POINT.getUID(), (UID)eop.getEo().getPrimaryKey()).getEntityObject();
						final UID targetEntityUID = eoIntegrationPoint.getFieldUid(E.NUCLET_INTEGRATION_POINT.targetEntity);
						if (targetEntityUID != null) {
							final EntityMeta<?> targetEntityMeta = MetaProvider.getInstance().getEntity(targetEntityUID);
							final String sLabel = targetEntityMeta.getLocaleResourceIdForTreeView() == null ? ""
									: SpringLocaleDelegate.getInstance().getTextForStaticLabel(targetEntityMeta.getLocaleResourceIdForTreeView());
							cbxEntity.setSelectedItem(targetEntityMeta);
							model.getAttribute().setMetaVO(targetEntityMeta);
							model.getAttribute().setField(sLabel);
							fillAlternativeTextField();
						} else {
							// clear foreign entity selection. Settings are made from integration system.
							cbxEntity.setSelectedIndex(0);
							listFields.setListData(new FieldMeta<?>[]{});
							tfAlternativeTextField.setText(null);
						}
						tfAlternativeSearchTextField.setText(null);
						cbxEntity.setEnabled(false);
						NuclosEntityAttributeRelationShipStep.this.setComplete(true);
					} else {
						model.getAttribute().setForeignIntegrationPoint(null);
						cbxEntity.setEnabled(true);
						boolean complete = model.getAttribute().getMetaVO() != null && model.getAttribute().getField() != null;
						NuclosEntityAttributeRelationShipStep.this.setComplete(complete);
					}
				}
			}
		});

		cbxEntity.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(final ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					final Object obj = e.getItem();
					if (obj instanceof EntityMeta<?> && ((EntityMeta<?>)obj).getUID() != null) {
						final EntityMeta<?> entity = (EntityMeta<?>)obj;
						
						final List<FieldMeta<?>> lstFields
							= new ArrayList<FieldMeta<?>>(MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values());
						Collections.sort(lstFields, EntityUtils.getMetaComparator(FieldMeta.class));
						
						List<FieldMeta<?>> lstFieldItems = new ArrayList<FieldMeta<?>>();
						for(FieldMeta<?> field : lstFields) {
							//@see NUCLOSINT-1232
							if(
									//field.getForeignEntity() == null &&
									//voField.getLookupEntity() == null &&
									!field.isCalculated()) {
								lstFieldItems.add(EntityUtils.wrapMetaData(field));
							}
						}
						
						listFields.setListData(lstFieldItems.toArray());

						NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setMetaVO(entity);
						NuclosEntityAttributeRelationShipStep.this.setComplete(true);

						if (StringUtils.isNullOrEmpty(model.getAttribute().getField())) {
							//@see NUCLOS-1037
							final String sLabel = entity.getLocaleResourceIdForTreeView() == null ? ""
									: SpringLocaleDelegate.getInstance().getTextForStaticLabel(entity.getLocaleResourceIdForTreeView());
							if (!StringUtils.isNullOrEmpty(sLabel)) {
								tfAlternativeTextField.setText(sLabel.indexOf("$") == -1 ? "" : sLabel);
								final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, sLabel, new Transformer<String, String>() {
									@Override
									public String transform(String i) {
										for(FieldMeta<?> field : entity.getFields()) {
											if (field.getFieldName().equals(i))
												return field.getUID().getStringifiedDefinition();
										}
										return "";
									}					
								});
								NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setField(sFieldUids);
							} else {
								tfAlternativeTextField.setText("");
								NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setField(null);
							}
						}
						
						NuclosEntityAttributeRelationShipStep.this.setComplete(
								!StringUtils.isNullOrEmpty(NuclosEntityAttributeRelationShipStep.this.model.getAttribute().getField()));

						try {
							checkReferenceField();
						}
						catch(Exception e1) {
							LOG.debug("itemStateChanged: checkReference failed: " + e1);
							tfAlternativeTextField.setText("");
							NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setField(null);
							NuclosEntityAttributeRelationShipStep.this.setComplete(false);
						}
						try {
							checkReferenceSearchField();
						}
						catch(Exception e1) {
							LOG.debug("itemStateChanged: checkReference failed: " + e1);
							tfAlternativeSearchTextField.setText("");
							NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setSearchField(null);
							NuclosEntityAttributeRelationShipStep.this.setComplete(false);
						}

						if (parentWizardModel.isGeneric()) {
							NuclosEntityAttributeRelationShipStep.this.setComplete(true);
						}
					}
					else {
						NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setMetaVO(null);
						NuclosEntityAttributeRelationShipStep.this.setComplete(false);
					}
				}
			}
		});

		btSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
 				 setKeyDescription();
			}
		});
		btSelectSearch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
 				 setSearchKeyDescription();
			}
		});

		listFields.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(SwingUtilities.isLeftMouseButton(e)) {
					if(e.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(e)) {
						 setKeyDescription();
					}
				}
			}
		});
		listFields.getInputMap(WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "setKeyDescription");
		listFields.getActionMap().put("setKeyDescription", new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				setKeyDescription();
			}
		});

		cbOnDeleteCascade.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				final JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setOnDeleteCascade(cb.isSelected());
			}
		});

		cbValueListProvider.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				final JCheckBox cb = (JCheckBox)e.getItem();
				NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setValueListProvider(cb.isSelected());
				
				// set search field visible
				lblSelectSearch.setVisible(cb.isSelected());
				btSelectSearch.setVisible(cb.isSelected());
				tfAlternativeSearchTextField.setVisible(cb.isSelected());
			}
		});

		tfAlternativeTextField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					final String sText = e.getDocument().getText(0, e.getDocument().getLength());
					final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, sText, new Transformer<String, String>() {
						@Override
						public String transform(String i) {
							for(FieldMeta<?> field : model.getAttribute().getMetaVO().getFields()) {
								if (field.getFieldName().equals(i))
									return field.getUID().getStringifiedDefinition();
							}
							return "";
						}					
					});
					model.getAttribute().setField(sFieldUids);
					NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setField(sFieldUids);
				}
				catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityAttributeRelationShipStep.this, ex);
				}				

				NuclosEntityAttributeRelationShipStep.this.setComplete(e.getDocument().getLength() > 0);
				
				try {
					checkReferenceField();
				} catch (Exception ex) {
					NuclosEntityAttributeRelationShipStep.this.setComplete(false);
				}
			}
		});
		tfAlternativeSearchTextField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				doSomeWork(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				doSomeWork(e);
			}
			protected void doSomeWork(DocumentEvent e) {
				try {
					final String sText = e.getDocument().getText(0, e.getDocument().getLength());
					final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, sText, new Transformer<String, String>() {
						@Override
						public String transform(String i) {
							for(FieldMeta<?> field : model.getAttribute().getMetaVO().getFields()) {
								if (field.getFieldName().equals(i))
									return field.getUID().getStringifiedDefinition();
							}
							return "";
						}					
					});
					model.getAttribute().setSearchField(sFieldUids);
					NuclosEntityAttributeRelationShipStep.this.model.getAttribute().setSearchField(sFieldUids);
				}
				catch (BadLocationException ex) {
					Errors.getInstance().showExceptionDialog(NuclosEntityAttributeRelationShipStep.this, ex);
				}				

				
				try {
					checkReferenceSearchField();
				} catch (Exception ex) {
					// do nothing here
				}
			}
		});
	}

	private void fillAlternativeTextField() {
		final String sField = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, model.getAttribute().getField(), new Transformer<String, String>() {
			@Override
			public String transform(String i) {
				for(FieldMeta<?> field : model.getAttribute().getMetaVO().getFields()) {
					if (field.getUID().getStringifiedDefinition().equals(i))
						return "${" + field.getFieldName() + "}";
				}
				return "";
			}
		});
		tfAlternativeTextField.setText(sField);
	}

	@Override
	public void prepare() {
		super.prepare();

		fillIntegrationPointCombobox();
		fillEntityCombobox();

		final EntityMeta<?> metaOld =this.model.getAttribute().getMetaVO();
		final String searchFieldOld = model.getAttribute().getSearchField();
		
		if(this.model.getAttribute().getMetaVO() != null) {
			if(model.getAttribute().getField() != null) {
				fillAlternativeTextField();
				
				cbxEntity.setSelectedItem(model.getAttribute().getMetaVO());
				
				cbOnDeleteCascade.setSelected(this.model.getAttribute().isOnDeleteCascade());
				cbValueListProvider.setSelected(this.model.getAttribute().isValueListProvider());
			}
			else {
				cbxEntity.setSelectedItem(this.model.getAttribute().getMetaVO());
			}
			if(searchFieldOld != null && metaOld.getUID().equals(this.model.getAttribute().getMetaVO().getUID())) {
				
				final String sField = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, model.getAttribute().getSearchField(), new Transformer<String, String>() {
					@Override
					public String transform(String i) {
						for(FieldMeta<?> field : model.getAttribute().getMetaVO().getFields()) {
							if (field.getUID().getStringifiedDefinition().equals(i))
								return "${" + field.getFieldName() + "}";
						}
						return "";
					}					
				});
				
				tfAlternativeSearchTextField.setText(sField);
			}
			
			// set search field visible
			lblSelectSearch.setVisible(cbValueListProvider.isSelected());
			btSelectSearch.setVisible(cbValueListProvider.isSelected());
			tfAlternativeSearchTextField.setVisible(cbValueListProvider.isSelected());
		}

		if(model.getAttribute().getForeignIntegrationPoint() != null) {
			cbxEntity.setEnabled(false);
			NuclosEntityAttributeRelationShipStep.this.setComplete(true);
		}
		else if(parentWizardModel.hasRows() && model.isEditMode() && !parentWizardModel.isVirtual()) {
			cbxEntity.setEnabled(false);
			cbxEntity.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.attributerelationship.tooltip.5", "Verweis kann nicht geändert werden. Da bereits Datensätze vorhanden sind."));
		}
		else {
			cbxEntity.setEnabled(true);
			cbxEntity.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.attributerelationship.tooltip.1", "Verweis auf Entit\u00e4t"));
		}

		if (model.getAttribute().getForeignIntegrationPoint() != null) {
			final EntityObjectPresenter eopIP = mapIntegrationPointsById.get(model.getAttribute().getForeignIntegrationPoint());
			cbxIntegrationPointListenerEnabled = false;
			cbxIntegrationPoint.setSelectedItem(eopIP);
			cbxIntegrationPointListenerEnabled = true;
		}

		cbOnDeleteCascade.setEnabled(!model.isProxy() && !model.isGeneric());
		cbValueListProvider.setEnabled(!model.isGeneric());

		if (model.isGeneric()) {
			listFields.setEnabled(false);
			btSelect.setEnabled(false);
			tfAlternativeTextField.setEnabled(false);
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (cbxEntity.isEnabled()) {
					cbxEntity.requestFocusInWindow();
				} else {
					cbxIntegrationPoint.requestFocusInWindow();
				}
			}
		});
	}

	private void fillIntegrationPointCombobox() {
		cbxIntegrationPointListenerEnabled = false;
		try {
			cbxIntegrationPoint.removeAllItems();
			mapIntegrationPointsById.clear();
			UID nucletUID = model.getWizardModel().getNucletUID();
			if (nucletUID != null && !model.getWizardModel().isGeneric()) {
				final Collection<EntityObjectVO<UID>> integrationPointList = EntityObjectDelegate.getInstance().getDependentEntityObjects(
						E.NUCLET_INTEGRATION_POINT.getUID(),
						E.NUCLET_INTEGRATION_POINT.nuclet.getUID(),
						nucletUID);
				for (EntityObjectPresenter eop : EntityObjectPresenter.transform(integrationPointList, E.NUCLET_INTEGRATION_POINT.name.getUID(), true)) {
					final EntityObjectVO<?> eo = eop.getEo();
					if (eo != null) {
						// NUCLOS-6248:
						/*final Boolean bOptional = eo.getFieldValue(E.NUCLET_INTEGRATION_POINT.optional);
						if (Boolean.TRUE.equals(bOptional)) {
							// References on optional integration point is not allowed
							continue;
						}*/
						mapIntegrationPointsById.put((UID) eo.getPrimaryKey(), eop);
					}
					cbxIntegrationPoint.addItem(eop);
				}
			}
		} catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(this, ex);
		} finally {
			cbxIntegrationPointListenerEnabled = true;
		}
	}

	private void fillEntityCombobox() {
		final ItemListener ilArray[] = cbxEntity.getItemListeners();
		for(ItemListener il : ilArray) {
			cbxEntity.removeItemListener(il);
		}

		cbxEntity.removeAllItems();

		final UID nucletUID = this.model.getWizardModel().getNucletUID();
		final EntityMeta<?> foreignEntityMeta = this.model.getAttribute().getMetaVO();

		final List<EntityMeta<?>> lstEntities
			= new ArrayList<EntityMeta<?>>(MetaProvider.getInstance().getAllEntities());
		final Iterator<EntityMeta<?>> iterator = lstEntities.iterator();
		while (iterator.hasNext()) {
			final EntityMeta<?> eMeta = iterator.next();
			if (eMeta.isGeneric() != NuclosEntityAttributeRelationShipStep.this.model.isGeneric()) {
				iterator.remove();
				continue;
			}
			if (eMeta.isDatasourceBased()) {
				iterator.remove();
				continue;
			}
			if (NucletEntityMeta.isEntityLanguageUID(eMeta.getUID())) {
				iterator.remove();
				continue;
			}
			// NUCLOS-6296
			if (!RigidUtils.equal(nucletUID, eMeta.getNuclet()) && !E.isNuclosEntity(eMeta.getUID())) {
				boolean bRemove = true;
				if (foreignEntityMeta != null && foreignEntityMeta.getUID().equals(eMeta.getUID())) {
					// Because of the backward compatibility, allow this entity of another nuclet.
					bRemove = false;
				}
				if (bRemove) {
					iterator.remove();
					continue;
				}
			}
		}
		Collections.sort(lstEntities, EntityUtils.getMetaComparator(EntityMeta.class));

		cbxEntity.addItem(EntityUtils.wrapMetaData(EntityMeta.NULL));
		
		for(EntityMeta<?> entity : lstEntities) {
			if (E.isNuclosEntity(entity.getUID())) {
				if(E.PROCESS.getUID().equals(entity.getUID()) ||
				   E.USER.getUID().equals(entity.getUID()) ||
				   E.DATA_LANGUAGE.getUID().equals(entity.getUID()) ||
				   E.ROLE.getUID().equals(entity.getUID()) ||
				   E.MANDATOR.getUID().equals(entity.getUID()) ||
				   E.PRINTSERVICE.getUID().equals(entity.getUID()) ||
				   E.PRINTSERVICE_TRAY.getUID().equals(entity.getUID()))
				cbxEntity.addItem(EntityUtils.wrapMetaData(entity));
			} else {
				cbxEntity.addItem(EntityUtils.wrapMetaData(entity));
			}
		}

		for(ItemListener il : ilArray) {
			cbxEntity.addItemListener(il);
		}
	}

	public void setParentWizardModel(NuclosEntityWizardStaticModel model) {
		this.parentWizardModel = model;
	}

	@Override
	public void close() {
		lbIntegrationPoint = null;
		cbxIntegrationPoint = null;

		lbEntity = null;
		cbxEntity = null;

		scrollPane = null;
		listFields = null;
		btSelect = null;
		btSelectSearch = null;

		lbInfo = null;
		lblSelectSearch = null;

		tfAlternativeTextField = null;
		tfAlternativeSearchTextField = null;

		lbValueListProvider = null;
		cbValueListProvider = null;

		lbOnDeleteCascade = null;
		cbOnDeleteCascade = null;
		
		parentWizardModel = null;
		
		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		checkReferenceField();
		checkReferenceSearchField();
		this.model.nextStep();
		this.model.nextStep();
		this.model.refreshModelState();
		
		super.applyState();
	}

	private void checkReferenceField() throws InvalidStateException {
		final String sField = this.model.getAttribute().getField();
		if (StringUtils.isNullOrEmpty(sField))
			return;
		
		final Pattern referencedEntityPattern = Pattern.compile ("uid[{][\\w|\\-\\[\\]]+[}]");
		final Matcher referencedEntityMatcher = referencedEntityPattern.matcher(sField);
		
		boolean invalid = true;
		while (referencedEntityMatcher.find()) {
			String sUID = referencedEntityMatcher.group().substring(4, referencedEntityMatcher.group().length() - 1);
			try {
				final FieldMeta<?> voField = MetaDataDelegate.getInstance().getEntityField(new UID(sUID));
				//@see NUCLOSINT-1232
				if(
						//voField.getForeignEntity() == null &&
						//voField.getLookupEntity() == null &&
						!voField.isCalculated()) {
					invalid = false;
				} else {
					invalid = true;
					break;
				}
			}
			catch (Exception e) {
				throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributerelationship.7", "Es wurde ein ungültiger Eintrag gefunden: " + sUID, sUID));
			}
		}

		if (invalid) {
			final String msg = "Es wurde kein gültiger Referenzeintrag gefunden: Feld '" + sField + "'";
			throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.attributerelationship.8", msg));
			// LOG.warn(msg);
		}
	}
	
	private void checkReferenceSearchField() throws InvalidStateException {
		String sField = this.model.getAttribute().getSearchField();
		if (sField == null || sField.length() < 1)
			return;
		final Pattern referencedEntityPattern = Pattern.compile ("uid[{][\\w|\\-\\[\\]]+[}]");
		Matcher referencedEntityMatcher = referencedEntityPattern.matcher(sField);
		boolean invalid = true;
		while (referencedEntityMatcher.find()) {
			String sName = referencedEntityMatcher.group().substring(4, referencedEntityMatcher.group().length() - 1);
			try {
				FieldMeta<?> voField = MetaDataDelegate.getInstance().getEntityField(new UID(sName));
				//@see NUCLOSINT-1232
				if(voField.getForeignEntity() == null
						//&& voField.getLookupEntity() == null
						&& !voField.isCalculated())
				invalid = false;
				else {
					invalid = true;
					break;
				}
			}
			catch (Exception e) {
				throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
						"wizard.step.attributerelationship.7a", "Es wurde ein ungültiger Eintrag für die Suche gefunden: " + sName,
						sName));
			}
		}

		if (invalid) {
			final String msg = "Es wurde kein gültiger Referenzeintrag für die Suche gefunden: Feld '" 
					+ sField + "'";
			throw new InvalidStateException(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.attributerelationship.8a", msg));
			// LOG.warn(msg);
		}
	}

	private void setKeyDescription() {
		 final FieldMeta<?> field = (FieldMeta<?>)listFields.getSelectedValue();
		 if(field == null) {
			 return;
		 }
		 String strText = tfAlternativeTextField.getText();
		 strText += "${" + field.getFieldName() + "}";
		 
		 tfAlternativeTextField.setText(strText);
	}

	private void setSearchKeyDescription() {
		final FieldMeta<?> field = (FieldMeta<?>)listFields.getSelectedValue();
		 if(field == null) {
			 return;
		 }
		 String strText = tfAlternativeSearchTextField.getText();
		 strText += "${" + field.getFieldName() + "}";

		 tfAlternativeSearchTextField.setText(strText);
	}

}

