//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;

public class EntityAttributeTableModel extends AbstractTableModel {
	
	private final List<Attribute> lstNucletAttribute;
	private final List<Attribute> lstRemovedAttribute;
	private final List<Attribute> lstSystemAttribute;
	private final Map<Attribute, List<TranslationVO>> mpTranslation;
	private final IEntityAttributeScope entityAttributeScope;
	private boolean blnStateModel;
	private boolean showSystemAttributes;
	
	public EntityAttributeTableModel(IEntityAttributeScope entityAttributeScope) {
		this.entityAttributeScope = entityAttributeScope;
		lstNucletAttribute = new ArrayList<Attribute>();
		lstRemovedAttribute = new ArrayList<Attribute>();
		lstSystemAttribute = new ArrayList<Attribute>();
		mpTranslation = new HashMap<Attribute, List<TranslationVO>>();
	}
	
	public void setAttributeAt(Attribute attribute, int row) {
		if(row > lstNucletAttribute.size()){
			row = lstNucletAttribute.size();
		}
		
		lstNucletAttribute.set(row, attribute);
		this.fireTableDataChanged();
	}
	
	public void addAttribute(Attribute attribute) {
		lstNucletAttribute.add(attribute);
		this.fireTableDataChanged();
	}
	
	public void addSystemAttribute(Attribute attribute) {
		lstSystemAttribute.add(attribute);
	}

	public boolean hasAttributeChangeName() {
		for(Attribute attr : getNucletAttributes()) {
			if(attr.hasInternalNameChanged())
				return true;
		}
		return false;
	}
	
	public String getAttributeNameByUID(UID fieldUID) {
		String retVal = null;
		for (Attribute a : lstNucletAttribute) {
			if (a.getUID().equals(fieldUID)) {
				retVal = a.getLabel();
			}
		}
		return retVal;
	}

	@Override
	public void fireTableDataChanged() {
		super.fireTableDataChanged();
	}

	public List<Attribute> getNucletAttributes() {
		return lstNucletAttribute;
	}
	
	public List<Attribute> getNucletAttributesWithOrder() {
		List<Attribute> lstAttrWithOrder = new ArrayList<Attribute>();
		
		for (int i = 0; i < lstNucletAttribute.size(); i++) {
			Attribute attribute = lstNucletAttribute.get(i);
			attribute.setOrder(i + 1);
			lstAttrWithOrder.add(attribute);
		}
		
		return lstAttrWithOrder;
	}
	
	public boolean hasSystemAttributes() {
		return !lstSystemAttribute.isEmpty();
	}
	
	public boolean hasLocalizedFields() {
		boolean retVal = false;
		
		for (Attribute a: this.lstNucletAttribute) {
			if (a.isLocalized()) {
				retVal = true;
				break;
			}
		}
		
		return retVal;
	}
	
	public List<Attribute> getAlteredSystemAttributes() {
		List<Attribute> lstAlteredSystemAttr = new ArrayList<Attribute>();
		
		for (Attribute attribute : lstSystemAttribute) {
			if (attribute.compareWithSnapshot() != 0) {
				List<TranslationVO> lstTrans = getTranslation().get(attribute);
				if (lstTrans != null && !lstTrans.isEmpty()) {
					attribute.checkSystemLabelResource(lstTrans);
					attribute.checkSystemDescriptionResource(lstTrans);
				}
				lstAlteredSystemAttr.add(attribute);				
			}
		}
		
		return lstAlteredSystemAttr;
	}
	
	public void addTranslation(Attribute attr, List<TranslationVO> lstTranslation) {
		mpTranslation.put(attr, lstTranslation);
	}
	
	public Map<Attribute, List<TranslationVO>> getTranslation () {
		return mpTranslation;
	}
	
	public Map<UID, List<Attribute>> getAttributeMapByGroup() {
		Map<UID, List<Attribute>> mp = new HashMap<UID, List<Attribute>>();
		
		for(Attribute attr : getNucletAttributes()) {
			if(mp.get(attr.getAttributeGroup()) == null) {
				mp.put(attr.getAttributeGroup(), Collections.synchronizedList(new ArrayList<Attribute>()));
			}
			mp.get(attr.getAttributeGroup()).add(attr);
		}
		
		return mp;
	}

	@Override
	public int getColumnCount() {		
		return 16;
	}

	@Override
	public int getRowCount() {
		int iAttr = lstNucletAttribute.size();
		if (!showSystemAttributes) {
			return iAttr;
		}
		int iSystemAttr = lstSystemAttribute.size();
		return iAttr + iSystemAttr;
	}
	
	public void switchGrauMipse() {
		showSystemAttributes = !showSystemAttributes;
		fireTableDataChanged();
	}
	
	public void removeRow(int row, boolean addToList) {
		if (row >= lstNucletAttribute.size()) {
			return;
		}
		
		Attribute attr = lstNucletAttribute.remove(row);
		if(addToList)
			lstRemovedAttribute.add(attr);
		this.fireTableDataChanged();
	}
	
	public List<Attribute> getRemoveAttributes() {
		return lstRemovedAttribute;
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 5:
			case 6:
			case 7:
			case 10:
			case 13:
			case 14:
			case 15:
				return Boolean.class;
			default:
				break;
		}
		return super.getColumnClass(columnIndex);
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex < lstNucletAttribute.size()) {
			switch (columnIndex) {
				case 6:
				case 8:
				case 12:
				case 13:
				case 14:
					return true;
				case 5:
					return entityAttributeScope != null ? entityAttributeScope.canBeSetToUnique(getAttribute(rowIndex)) : false;
				default:
					return false;
			}			
		} else {
			switch (columnIndex) {
				case 12:
				case 13:
				case 14:
					return true;
				default:
					return false;
			}			
		}
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		final Attribute attribute = getAttribute(rowIndex);

		switch (columnIndex) {
			case 5:
				attribute.setDistinct((Boolean)aValue);
				break;
			case 6:
				attribute.setLogBook((Boolean)aValue);
				break;
			case 8:
				if (aValue instanceof String) {
					String newString = ((String)aValue).trim();
					String regex_alpha_numeric_underscore_notempty = "^[0-9a-zA-Z]+$";
					if (newString.matches(regex_alpha_numeric_underscore_notempty)) {
						attribute.setInternalName(newString);						
					}
				}
				break;
			case 11:
				attribute.setDefaultValue((String)aValue);						
				break;
			case 13:
				attribute.setHidden((Boolean)aValue);
				break;
			case 14:
				attribute.setModifiable((Boolean)aValue);
				break;
			case 15:
				attribute.setIsLocalized((Boolean) aValue);
				break;
			default:
				break;
		}
	}
	
	public Attribute getAttribute(int rowIndex) {
		boolean bSystemAttribute = rowIndex >= lstNucletAttribute.size();
		return bSystemAttribute ? lstSystemAttribute.get(rowIndex - lstNucletAttribute.size()) : lstNucletAttribute.get(rowIndex);		
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		final Attribute attribute = getAttribute(rowIndex);
		
		switch (columnIndex) {
		case 0:
			return attribute.getLabel();
		case 1:
			return attribute.getDescription();
		case 2:
			return attribute.getDatatyp();
		case 3:
			return attribute.getDatatyp().getScale();
		case 4:			
			return attribute.getDatatyp().getPrecision();
		case 5:
			return new Boolean(attribute.isDistinct());
		case 6:
			return new Boolean(attribute.isLogBook());
		case 7:
			return new Boolean(attribute.isMandatory());
		case 8:
			return attribute.getInternalName();
		case 9:
			return attribute.getDatatyp().getShortJavaType();
		case 10:
			return new Boolean(attribute.getCalcFunction() != null || attribute.getCalcAttributeDS() != null || attribute.getCalculationScript() != null);
		case 11:			
			return attribute.getDefaultValue();
		case 12:				
			return attribute.getAttributeGroup() == null ? null : MasterDataCache.getInstance().get(
					E.ENTITYFIELDGROUP.getUID(), attribute.getAttributeGroup()).getFieldValue(E.ENTITYFIELDGROUP.name);
		case 13:
			return new Boolean(attribute.isHidden());
		case 14:
			return new Boolean(attribute.isModifiable());
		case 15:
			return new Boolean(attribute.isLocalized() != null ? attribute.isLocalized() : Boolean.FALSE);
		default:			
			return null;
		}		
	}	
	
	@Override
	public String getColumnName(int column) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		switch (column) {
		case 0:
			return localeDelegate.getMessage("wizard.step.attributeproperties.1", "Anzeigename");
		case 1:
			return localeDelegate.getMessage("wizard.step.attributeproperties.2", "Beschreibung");
		case 2:
			return localeDelegate.getMessage("wizard.step.attributeproperties.3", "Datentyp");		
		case 3:
			return localeDelegate.getMessage("wizard.datatype.3", "Feldbreite");
		case 4:
			return localeDelegate.getMessage("wizard.datatype.4", "Nachkommastellen");
		case 5:
			return localeDelegate.getMessage("wizard.step.attributeproperties.7", "Eindeutig");
		case 6:
			return localeDelegate.getMessage("wizard.step.attributeproperties.8", "Logbuch");
		case 7:
			return localeDelegate.getMessage("wizard.step.attributeproperties.9", "Pflichtfeld");
		case 8:
			return localeDelegate.getMessage("wizard.step.attributeproperties.10", "Feldname");
		case 9:
			return localeDelegate.getMessage("wizard.datatype.45", "Javatyp");
		case 10:
			return localeDelegate.getMessage("wizard.step.attributeproperties.29", "Berechnet");
		case 11:
			return localeDelegate.getMessage("wizard.step.attributeproperties.28", "Defaultwert");
		case 12:
			return localeDelegate.getMessage("wizard.step.attributeproperties.13", "Attributegruppe");
		case 13:
			return localeDelegate.getMessage("wizard.step.attributeproperties.14a", "Versteckt");
		case 14:
			return localeDelegate.getMessage("wizard.step.attributeproperties.14b", "Veränderbar");
		case 15:
			return localeDelegate.getMessage("wizard.step.attributeproperties.36", "Mehrsprachigkeit");
		default:			
			return "";
		}		
	}
	
	public void setStatemodel(boolean bStatemodel) {
		this.blnStateModel = bStatemodel;
	}
	
	public boolean isStatemodel() {
		return blnStateModel;
	}
	
	public void reorder(int fromIndex, int toIndex) {
		Attribute attrDrag = lstNucletAttribute.get(fromIndex);
		lstNucletAttribute.remove(fromIndex);
		lstNucletAttribute.add(fromIndex<(toIndex-1)?(toIndex-1):toIndex, attrDrag);
	}

}
