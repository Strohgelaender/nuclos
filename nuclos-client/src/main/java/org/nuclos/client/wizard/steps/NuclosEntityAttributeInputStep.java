	//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.wizard.steps;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragSource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MetaDataDelegate;
import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.wizard.NuclosEntityAttributeWizard;
import org.nuclos.client.wizard.NuclosEntityAttributeWizardStaticModel;
import org.nuclos.client.wizard.model.Attribute;
import org.nuclos.client.wizard.model.EntityAttributeTableModel;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.WizardEvent;
import org.pietschy.wizard.WizardListener;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
* <br>
* Created by Novabit Informationssysteme GmbH <br>
* Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*
* @author <a href="mailto:marc.finke@novabit.de">Marc Finke</a>
* @version 01.00.00
*/
public class NuclosEntityAttributeInputStep extends NuclosEntityAbstractStep {

	private static final Logger LOG = Logger.getLogger(NuclosEntityAttributeInputStep.class);

	private JScrollPane scrolPane;
	private JTable tblAttributes;
	private JButton btUp;
	private JButton btDown;
	private JButton btSystemAttributes;
	private JPanel panelAttributes;

	private JButton btnNewAttribute;
	private JButton btnDropAttribute;
	private JButton btnEditAttribute;
	private TableColumn colGroup;

	private EntityAttributeTableModel entityModel;

	public NuclosEntityAttributeInputStep(String name, String summary) {
		super(name, summary);
		initComponents();
	}

	public static class VisiblePropertyChangeListener implements PropertyChangeListener {
		
		private final JOptionPane pane;
		private final JDialog dia;
		
		private VisiblePropertyChangeListener(JOptionPane pane, JDialog dia) {
			this.pane = pane;
			this.dia = dia;
		}
		
        @Override
		public void propertyChange(PropertyChangeEvent e) {
            String prop = e.getPropertyName();
            if (dia.isVisible() && (e.getSource() == pane) && (prop.equals(JOptionPane.VALUE_PROPERTY))) {
                dia.setVisible(false);
            }
        }		
	}

	private static final Color COLORBACKGROUNDNOTEDITABLE = Color.decode("#eeeeeee");
			
	@Override
	protected void initComponents() {

		double size [][] = {{160,160,160,160,TableLayout.FILL}, {TableLayout.FILL, 25,10}};

		TableLayout layout = new TableLayout(size);
		layout.setVGap(3);
		layout.setHGap(5);
		this.setLayout(layout);

		entityModel = new EntityAttributeTableModel(model);

		tblAttributes = new JTable(entityModel) {
			@Override
			public TableCellEditor getCellEditor(int row, int column) {
				if (column == 12) {
					JComboBox cbx = new JComboBox();
					Attribute attribute = entityModel.getAttribute(row);
					Attribute.fillAttributeGroupBox(cbx, attribute, true);
					return new DefaultCellEditor(cbx);
				}
				return super.getCellEditor(row, column);
			}
		};
		
		tblAttributes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblAttributes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblAttributes.getTableHeader().setReorderingAllowed(false);
		tblAttributes.setDragEnabled(true);
		tblAttributes.setDropMode(DropMode.INSERT_ROWS);
		tblAttributes.setTransferHandler(new TableRowTransferHandler(tblAttributes));

		tblAttributes.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				int index = tblAttributes.getSelectedRow();
				int lastNucletAttributeIndex = entityModel.getNucletAttributes().size() - 1;
				
				boolean bDroppableAttribute = index >= 0 && index <= lastNucletAttributeIndex && !isVirtual();
				btnDropAttribute.setEnabled(bDroppableAttribute);
				
				boolean bEditableAttribute = index >= 0;
				btnEditAttribute.setEnabled(bEditableAttribute);
				
				boolean bMoveUp = index >= 1 && index <= lastNucletAttributeIndex;
				btUp.setEnabled(bMoveUp);
				
				boolean bMoveDown = index >= 0 && index < lastNucletAttributeIndex;
				btDown.setEnabled(bMoveDown);
			}
		});

		tblAttributes.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value,
					boolean isSelected, boolean hasFocus, int row, int column) {
				final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				c.setForeground(null);
				final Attribute attribute = entityModel.getAttribute(row);
				if (attribute != null && attribute.getForeignIntegrationPoint() != null) {
					c.setForeground(NuclosThemeSettings.HOME_TABBED_PANE_BACKGROUND.darker());
				}

				switch (column) {
					case 5:
					case 6:
					case 7:
					case 10:
					case 13:
						break;
					default:
						if (isSelected) {
							c.setBackground(table.getSelectionBackground());
						} else {
							boolean editable = entityModel.isCellEditable(row, column);
							c.setBackground(editable ? Color.WHITE : COLORBACKGROUNDNOTEDITABLE);							
						}
						break;
				}
				return c;
			}
		});

		tblAttributes.setDefaultRenderer(Boolean.class, new DefaultTableCellRenderer() {

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value,
					boolean isSelected, boolean hasFocus, int row, int column) {
				final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				
				final JCheckBox cb = new JCheckBox();
				switch (column) {
					case 5:
					case 6:
					case 7:
					case 10:
					case 13:
					case 14:
					case 15:
						cb.setSelected(value == null ? false : Boolean.parseBoolean(value.toString()));
						cb.setOpaque(true);
						cb.setHorizontalAlignment(SwingConstants.CENTER);
						if (isSelected) {
							cb.setBackground(table.getSelectionBackground());
						} else {
							boolean editable = entityModel.isCellEditable(row, column);
							cb.setBackground(editable ? Color.WHITE : COLORBACKGROUNDNOTEDITABLE);							
						}
						break;
					default:
						return c;
				}

				return cb;
			}
		});
		
		tblAttributes.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(e)) {
					editSelectedAttribute();
				}
			}
		});
		tblAttributes.getInputMap(WHEN_FOCUSED).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "editSelectedAttribute");
		tblAttributes.getActionMap().put("editSelectedAttribute", new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				editSelectedAttribute();
			}
		});

		scrolPane = new JScrollPane();
		scrolPane.getViewport().add(tblAttributes);

		btnNewAttribute = new JButton(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.inputattribute.1", "Attribut hinzuf\u00fcgen"));
		btnNewAttribute.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				showNuclosEntityAttributeWizard(null,false,-1);
			}
		});
		btnNewAttribute.setToolTipText(Main.getKeyboardShortcutDescription(true, false, false, "N", SpringLocaleDelegate.getInstance().getLocale()));
		btnNewAttribute.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "newAttributeWizard");
		btnNewAttribute.getActionMap().put("newAttributeWizard", new AbstractAction() {
			@Override
			public void actionPerformed(final ActionEvent e) {
				showNuclosEntityAttributeWizard(null,false,-1);
			}
		});

		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		btnDropAttribute = new JButton(localeDelegate.getMessage(
				"wizard.step.inputattribute.2", "Attribut entfernen"));
		btnDropAttribute.setEnabled(false);
		btnDropAttribute.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int selected = tblAttributes.getSelectedRow();

				Attribute attr = entityModel.getAttribute(selected);

				StringBuffer sMessage = new StringBuffer();
				if(hasAttributeReference(attr, sMessage)) {
					JOptionPane.showMessageDialog(NuclosEntityAttributeInputStep.this, sMessage,
						localeDelegate.getMessage("wizard.step.inputattribute.12", "Entfernen nicht möglich!"), 
						JOptionPane.OK_OPTION);
					return;
				}

				//This only make sense, if the Entity already exists. If not there was an exception thrown anyway NUCLOS-2774
				if (model.isEditMode()) {
					try {
						Collection<MasterDataVO<UID>> colImportStructure = MetaDataDelegate.getInstance().hasEntityFieldInImportStructure(attr.getUID());
						if(colImportStructure.size() > 0) {
							Collection<String> colImportStructureNames = CollectionUtils.transform(colImportStructure, new Transformer<MasterDataVO<UID>, String>() {
								@Override
								public String transform(MasterDataVO<UID> vo) {
									return vo.getFieldValue(E.IMPORT.name);
								}
							});
	
							String sMessageText = localeDelegate.getMessage(
									"wizard.step.inputattribute.15", "Das Attribut wird in der Import Strukturdefinition ");
	
							for(String sImport : colImportStructureNames){
								sMessageText += sImport + " ";
							}
	
							sMessageText += localeDelegate.getMessage(
									"wizard.step.inputattribute.16", "\nDie Referenz wird entfernt, wenn das Attribut gelöscht wird!\nSoll das Attribut trotzdem gelöscht werden?");
							int option = JOptionPane.showConfirmDialog(NuclosEntityAttributeInputStep.this, sMessageText,
									localeDelegate.getMessage("wizard.step.inputattribute.17", "Achtung"), 
									JOptionPane.YES_NO_OPTION);
							if(option != JOptionPane.YES_OPTION) {
								return;
							}
						}	
					} catch (Exception ex) {
						// Field was new
					}
				}

				if(attr.getMetaVO() != null) {
					if(isSubformOrChartEntity(attr)){
						if(NuclosEntityAttributeInputStep.this.getModel().hasRows()) {
							String sText = localeDelegate.getMessage("wizard.step.inputattribute.14",
								"Die Entität enthält bereits Daten. Wenn Sie dieses Attribut löschen,\n" +
									"verliert die Hauptentität, die Datensätze dieser Entität.\n" +
									"Das Unterformular oder der Chart wird ebenfalls aus der Maske herausgenommen.");
							final JOptionPane pane = new JOptionPane(sText, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
							final JDialog dia = new JDialog(Main.getInstance().getMainFrame(), true);
							pane.addPropertyChangeListener(
									new VisiblePropertyChangeListener(pane, dia));

							dia.setContentPane(pane);
							dia.setTitle(localeDelegate.getMessage(
									"wizard.step.inputattribute.13", "Attribut entfernen?"));
							dia.setLocationRelativeTo(NuclosEntityAttributeInputStep.this.getModel().getParentFrame());
							dia.pack();
							dia.setVisible(true);

							if(!(pane.getValue() instanceof Integer))
								return;

							int value = ((Integer)pane.getValue()).intValue();

							if(value != JOptionPane.YES_OPTION) {
								return;
							}
						}
					}
				}

				if(selected < 0)
					return;
				entityModel.removeRow(selected, true);
				NuclosEntityAttributeInputStep.this.model.setAttributeModel(entityModel);
			}
		});

		btnEditAttribute = new JButton(localeDelegate.getMessage(
				"wizard.step.inputattribute.3", "Attribut bearbeiten"));
		btnEditAttribute.setEnabled(false);
		btnEditAttribute.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				editSelectedAttribute();
			}
		});

		panelAttributes = new JPanel();
		double sizePanel [][] = {{TableLayout.FILL, 3, 20}, {20, 20, 3, 20, 23, 20, TableLayout.FILL}};
		panelAttributes.setLayout(new TableLayout(sizePanel));

		btUp = new JButton(Icons.getInstance().getIconSortAscending());
		btUp.setToolTipText(localeDelegate.getMessage(
				"wizard.step.entitysqllayout.tooltip.5", "Attribut nach oben schieben"));
		btDown = new JButton(Icons.getInstance().getIconSortDescending());
		btDown.setToolTipText(localeDelegate.getMessage(
				"wizard.step.entitysqllayout.tooltip.6", "Attribut nach unten schieben"));

		btSystemAttributes = new JButton(Icons.getInstance().getIconPartOf());
		btSystemAttributes.setToolTipText(localeDelegate.getMessage(
				"wizard.step.entitysqllayout.tooltip.8", "Systemattribute einblenden/ausblenden"));

		panelAttributes.add(scrolPane, "0,0, 0,6");
		panelAttributes.add(btUp, "2,1");
		panelAttributes.add(btDown, "2,3");

		// NUCLOS-3244 Feature disabled because of resulting problems
		// panelAttributes.add(btSystemAttributes, "2,5");

		btUp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonUpAttributeAction();
			}
		});

		btDown.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonDownAttributeAction();
			}
		});

		btSystemAttributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				buttonSystemAttributesAction();
			}
		});

		this.add(panelAttributes, new TableLayoutConstraints(0, 0, 4, 0));
		this.add(btnNewAttribute, "0,1");
		this.add(btnDropAttribute, "1,1");
		this.add(btnEditAttribute, "2,1");

	}

	private boolean isVirtual() {
		return model != null ? model.isVirtual() : false;
	}

	private boolean isSubformOrChartEntity(Attribute attribute) {
		final Set<UID> set = NuclosWizardUtils.searchParentEntity(model.getUID());
		if(set.size() > 0) {
			if(set.contains(attribute.getMetaVO().getUID()))
				return true;
		}
		return false;
	}

	@Override
	public void prepare() {
		super.prepare();
		
		this.entityModel = this.model.getAttributeModel();
		this.tblAttributes.setModel(this.entityModel);
		
		EntityAttributeTableModel tableModel = (EntityAttributeTableModel)tblAttributes.getModel();
		if (this.model.isEditMode() || (entityModel != null && entityModel.getNucletAttributes().size() > 0)) {
			
			List<Attribute> lstAttribute = entityModel.getNucletAttributes();
			for(Attribute attr : lstAttribute) {
				boolean hasAlready = false;
				for(Attribute attrModel : tableModel.getNucletAttributes()) {
					if(attr.getInternalName().equals(attrModel.getInternalName())) {
						hasAlready = true;
						break;
					}
				}
				if(!hasAlready) {
					tableModel.addAttribute(attr);
					tableModel.addTranslation(attr, entityModel.getTranslation().get(attr));
				}
			}
			
			this.setComplete(true);
			this.tblAttributes.repaint();
		}
		
		if (tableModel != null && !tableModel.hasSystemAttributes()) {
			btSystemAttributes.setEnabled(false);
		}
					
		btnNewAttribute.setEnabled(!this.model.isImportTable());

		TableUtils.setOptimalColumnWidths(tblAttributes);

		if (model.isVirtual()) {
			btnNewAttribute.setEnabled(false);
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tblAttributes.requestFocusInWindow();
			}
		});
	}
	
	@Override
	public void close() {
		scrolPane = null;
		tblAttributes = null;
		btUp = null;
		btDown = null;
		btSystemAttributes = null;
		panelAttributes = null;

		btnNewAttribute = null;
		btnDropAttribute = null;
		btnEditAttribute = null;
		colGroup = null;

		entityModel = null;
		
		super.close();
	}

	@Override
	public void applyState() throws InvalidStateException {
		if (tblAttributes.getCellEditor() != null) {
			tblAttributes.getCellEditor().stopCellEditing();			
		}
		EntityAttributeTableModel tableModel = (EntityAttributeTableModel)tblAttributes.getModel();
		List<String> missingreferences = new ArrayList<String>();
		for (Attribute a : tableModel.getNucletAttributes()) {
			if (a.getDatatyp().isRefenceTyp() && a.getMetaVO() == null && a.getForeignIntegrationPoint() == null) {
				missingreferences.add(a.getInternalName());
			} else if (a.getDatatyp().isLookupTyp() && a.getLookupMetaVO() == null) {
				missingreferences.add(a.getInternalName());
			}
		}
		if (missingreferences.size() > 0) {
			String message = SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputattribute.validation.reference", 
					"Please select a foreign entity for each reference field ({0}).", 
					StringUtils.join(", ", missingreferences));
			JOptionPane.showMessageDialog(this, message, 
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.entitycommonproperties.19", "Achtung!"), 
					JOptionPane.OK_OPTION);
	        throw new InvalidStateException();
		}
		NuclosEntityAttributeInputStep.this.model.setAttributeModel(entityModel);
		
		super.applyState();
	}

	private void buttonDownAttributeAction() {
		int iSelected = tblAttributes.getSelectedRow();
		if (iSelected < 0 || iSelected >= entityModel.getNucletAttributes().size() - 1) {
			return;			
		}

		entityModel.reorder(iSelected, iSelected + 1);
		tblAttributes.getSelectionModel().setSelectionInterval(iSelected + 1, iSelected + 1);
		tblAttributes.invalidate();
		tblAttributes.repaint();
	}

	private void buttonUpAttributeAction() {
		int iSelected = tblAttributes.getSelectedRow();
		if (iSelected < 1 || iSelected >= entityModel.getNucletAttributes().size()) {
			return;			
		}

		entityModel.reorder(iSelected, iSelected - 1);
		tblAttributes.getSelectionModel().setSelectionInterval(iSelected - 1, iSelected - 1);
		tblAttributes.invalidate();
		tblAttributes.repaint();
	}
	
	private void buttonSystemAttributesAction() {
		entityModel.switchGrauMipse();
	}

	protected void showNuclosEntityAttributeWizard(final Attribute attr, final boolean editMode, final int row) {

		final MainFrameTab tabAttribute = new MainFrameTab(SpringLocaleDelegate.getInstance().getMessage(
				"wizard.step.inputattribute.8", "Attribut Wizard f\u00fcr Entit\u00e4t"+" " 
				+ NuclosEntityAttributeInputStep.this.model.getEntityName()));

		tabAttribute.addMainFrameTabListener(new MainFrameTabAdapter() {
			@Override
			public void tabClosed(final MainFrameTab tab) {
				getModel().setNavigationEnabled(true);
				tblAttributes.requestFocusInWindow();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						try {
							tblAttributes.getSelectionModel().setSelectionInterval(row, row);
						} catch (Exception ex) {
							LOG.error(ex.getMessage(), ex);
						}
					}
				});
			}
		});
		getModel().setNavigationEnabled(false);

		try {
			final NuclosEntityAttributeWizardStaticModel model = new NuclosEntityAttributeWizardStaticModel(getModel(), tblAttributes.getModel().getRowCount());
			model.setEditMode(editMode);
			model.setLastVisible(false);
			if(attr != null) {
				if (attr.isSystem()) {
					model.setAttribute(attr);
				} else {
					model.setAttribute((Attribute)attr.clone());					
				}
				
				if(attr.isValueList()) {
					model.setValueListTyp(true);					
				} else {
					model.setValueListTyp(false);					
				}
			}
			model.setTranslation(entityModel.getTranslation().get(attr));
			model.setProxy(NuclosEntityAttributeInputStep.this.model.isProxy());
			model.setWriteProxy(NuclosEntityAttributeInputStep.this.model.isWriteProxy());
			model.setGeneric(NuclosEntityAttributeInputStep.this.model.isGeneric());
			
			NuclosEntityAttributePropertiesStep step1 = new NuclosEntityAttributePropertiesStep(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputattribute.4", "Eigenschaften"), 
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.4", "Eigenschaften"));
			step1.setParentWizardModel(this.model);
			step1.setParent(tabAttribute);
			step1.setColumnTypeChangeAllowed(!this.hasEntityValues());
			// TODO: Remove valueList (werteliste).
			NuclosEntityAttributeValueListShipStep step2a = new NuclosEntityAttributeValueListShipStep(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputattribute.5", "Werteliste"), 
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.5", "Werteliste"));
			NuclosEntityAttributeRelationShipStep step2b = new NuclosEntityAttributeRelationShipStep(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputattribute.10", "Verkn\u00fcpfung zu anderen Entit\u00e4ten"), 
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.10", "Verkn\u00fcpfung zu anderen Entit\u00e4ten"));
			step2b.setParentWizardModel(this.model);
            NuclosEntityAttributeLookupShipStep step2c = new NuclosEntityAttributeLookupShipStep(
            		SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.18", "Nachschlage-Entit\u00e4t definieren"), 
            		SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.18", "Nachschlage-Entit\u00e4t definieren"));
            step2c.setParentWizardModel(this.model);
            NuclosEntityAttributeAutoNumberStep step2d = new NuclosEntityAttributeAutoNumberStep(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputattribute.19", "Verwende Hauptentit\u00e4t f\u00dchr Nummerierung"), 
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.19", "Verwende Hauptentit\u00e4t f\u00dchr Nummerierung"));
			step2d.setAttributeList(this.getModel().getAttributeModel().getNucletAttributes());
			step2d.setParentWizardModel(this.model);
			
			NuclosEntityAttributeTranslationStep step3 = new NuclosEntityAttributeTranslationStep(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputattribute.6", "\u00dcbersetzungen"), 
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.6", "\u00dcbersetzungen"));
			step3.setParentWizardModel(this.model);
			step3.setComplete(true);
			NuclosEntityAttributeCommonPropertiesStep step4 = new NuclosEntityAttributeCommonPropertiesStep(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputattribute.7", "Allgemeine Eigenschaften"), 
					SpringLocaleDelegate.getInstance().getMessage("wizard.step.inputattribute.7", "Allgemeine Eigenschaften"));
			step4.setParentWizardModel(this.model);
			model.add(step1);
			model.add(step2a);
			model.add(step2b);
			model.add(step2c);
			model.add(step2d);
			model.add(step3);
			model.add(step4);


			NuclosEntityAttributeWizard wizard = new NuclosEntityAttributeWizard(model);
			model.setWizard(wizard);

			wizard.addWizardListener(new WizardListener() {
				@Override
				public void wizardClosed(WizardEvent e) {
					Attribute attribute = model.getAttribute();
					if (attribute != null) {
						
						if (!attribute.isSystem()) {
							if(attribute.getIdDefaultValue()!=null) {
								attribute.setDefaultValue(attribute.getIdDefaultValue().getValue());
							}
							
							if(!editMode || attribute.getUID() == null) {
								attribute.setUID(new UID());
							}

							if(row >= 0 && editMode) {
								entityModel.removeRow(row, false);
							}
		
							entityModel.addAttribute(attribute);
							if(row >= 0 && row < entityModel.getNucletAttributes().size() && editMode) {
								entityModel.reorder(entityModel.getNucletAttributes().size() - 1, row);
							}							
						}
						
						entityModel.addTranslation(attribute, model.getTranslation());
					}
					
					NuclosEntityAttributeInputStep.this.setComplete(true);
					NuclosEntityAttributeInputStep.this.model.setAttributeModel(entityModel);

					tabAttribute.close();
					TableUtils.setOptimalColumnWidths(tblAttributes);
					
					parent.remove(tabAttribute);
				}
				
				@Override
				public void wizardCancelled(WizardEvent e) {
					model.setAttribute(attr);
					tabAttribute.close();
					parent.remove(tabAttribute);
				}
			});

			tabAttribute.setLayeredComponent(wizard);
			parent.add(tabAttribute);
		}
		catch(CommonFinderException ex) {
			Errors.getInstance().showExceptionDialog(NuclosEntityAttributeInputStep.this, ex);
		}
		catch(CommonPermissionException e) {
			Errors.getInstance().showExceptionDialog(NuclosEntityAttributeInputStep.this, e);
		}
	}
	

	private boolean hasEntityValues() {
		boolean yes = false;

		if(this.model.isEditMode()) {
			return this.model.hasRows();
		}

		return yes;
	}


	public void resetStep() {
		entityModel = new EntityAttributeTableModel(model);
		tblAttributes.setModel(entityModel);
	}

	private boolean hasAttributeReference(Attribute attr, StringBuffer message) {
		MetaProvider metaProvider = SpringApplicationContextHolder.getBean(MetaProvider.class);
		boolean blnRef = false;

		for(EntityMeta<?> vo : metaProvider.getAllEntities()) {
			if(vo.getUID().equals(this.model.getUID()))
				continue;
			if (vo.getUID().equals(E.NUCLET_INTEGRATION_FIELD.getUID())) {
				CollectableSearchCondition cond = new CollectableComparison(
						new CollectableEOEntityField(metaProvider.getEntityField(E.NUCLET_INTEGRATION_FIELD.targetField.getUID())),
						ComparisonOperator.EQUAL,
						new CollectableValueIdField(attr.getUID(), null));
				Collection<MasterDataVO<Object>> mdvos = MasterDataDelegate.getInstance().getMasterData(E.NUCLET_INTEGRATION_FIELD.getUID(), cond);
				blnRef = mdvos.size() > 0;
				for (MasterDataVO mdvo : mdvos) {
					String sMessage = SpringLocaleDelegate.getInstance().getMessage(
							"wizard.step.inputattribute.20", "Das Attribut " + mdvo.getFieldValue(E.NUCLET_INTEGRATION_FIELD.name) + " im Integrationspunkt " + mdvo.getFieldValue(E.NUCLET_INTEGRATION_FIELD.integrationPoint.getUID())
									+ " verweist auf das Feld" + attr.getLabel() + "\n"
									+ "Bitte entfernen Sie vorher das Feld dort!",
							mdvo.getFieldValue(E.NUCLET_INTEGRATION_FIELD.name), mdvo.getFieldValue(E.NUCLET_INTEGRATION_FIELD.integrationPoint.getUID()), attr.getLabel());
					message.append("\n" + sMessage);
				}
			} else {
				for (FieldMeta<?> voField : MetaProvider.getInstance().getAllEntityFieldsByEntity(vo.getUID()).values()) {
					if (voField.getForeignEntity() == null)
						continue;
					if (voField.getForeignEntityField() == null)
						continue;
					final UID foreign = voField.getForeignEntity();
					final String sForeignField = voField.getForeignEntityField();
					if (foreign.equals(this.model.getUID())) {
						if (sForeignField.indexOf(attr.getUID().toString()) >= 0) {
							String sMessage = SpringLocaleDelegate.getInstance().getMessage(
									"wizard.step.inputattribute.11", "Die Entität " + vo.getEntityName()
											+ " verweist auf das Feld " + attr.getLabel() + "\n"
											+ "Bitte entfernen Sie vorher das Feld dort!",
									vo.getEntityName(), attr.getLabel());
							message.append("\n" + sMessage);
							blnRef = true;
						}
					}
				}
			}
		}

		return blnRef;
	}

	public static class TableRowTransferHandler extends TransferHandler {
		private JTable table = null;

		public TableRowTransferHandler(JTable table) {
			this.table = table;
		}

		@Override
		protected Transferable createTransferable(JComponent c) {
			assert (c == table);
			return new IndexTransferable(table.getSelectedRow());
		}

		@Override
		public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
			for (DataFlavor df : transferFlavors) {
				if (df == indexFlavor)
					return true;
			}
			return super.canImport(comp, transferFlavors);
		}

		@Override
		public boolean canImport(TransferHandler.TransferSupport info) {
			boolean b = info.getComponent() == table && info.isDrop()
					&& info.isDataFlavorSupported(indexFlavor);
			table.setCursor(b ? DragSource.DefaultMoveDrop
					: DragSource.DefaultMoveNoDrop);
			return b;
		}

		@Override
		public int getSourceActions(JComponent c) {
			return TransferHandler.COPY_OR_MOVE;
		}

		@Override
		public boolean importData(TransferHandler.TransferSupport info) {
			JTable target = (JTable) info.getComponent();
			JTable.DropLocation dl = (JTable.DropLocation) info
					.getDropLocation();
			int index = dl.getRow();
			int max = table.getModel().getRowCount();
			if (index < 0 || index > max)
				index = max;
			target.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			try {
				Integer rowFrom = (Integer) info.getTransferable()
						.getTransferData(indexFlavor);
				if (rowFrom != -1 && rowFrom != index) {
					((EntityAttributeTableModel) table.getModel()).reorder(
							rowFrom, index);
					if (index > rowFrom)
						index--;
					target.getSelectionModel().addSelectionInterval(index,
							index);
					return true;
				}
			} catch (Exception e) {
				LOG.error("Error during attribute drop", e);
			}
			return false;
		}

		@Override
		protected void exportDone(JComponent c, Transferable t, int act) {
			if (act == TransferHandler.MOVE) {
				table.setCursor(Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		}
	}

	private static class IndexTransferable implements Transferable {

		final Integer index;

		public IndexTransferable(Integer index) {
			this.index = index;
		}

		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return flavors;
		}

		@Override
		public boolean isDataFlavorSupported(DataFlavor fl) {
			if (indexFlavor.equals(fl))
				return true;
			return false;
		}

		@Override
		public Object getTransferData(DataFlavor fl) {
		    if (indexFlavor.equals(fl)) {
		      return index;
		    }
		    return null;
		}
	}

	public static final DataFlavor indexFlavor = new DataFlavor(Integer.class, "index");
	private static final DataFlavor[] flavors = new DataFlavor[] {indexFlavor};

	private void editSelectedAttribute() {
		int selected = tblAttributes.getSelectedRow();
		if(selected < 0) {
			return;
		}
		EntityAttributeTableModel model = (EntityAttributeTableModel)tblAttributes.getModel();
		if (selected >= model.getRowCount()) {
			return;
		}
		Attribute attr = model.getAttribute(selected);
		showNuclosEntityAttributeWizard(attr, true, selected);
	}

}
