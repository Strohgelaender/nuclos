//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.main.mainframe.workspace;

import org.apache.log4j.Logger;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.common.Actions;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.maintenance.MaintenanceFacadeRemote;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * Business Delegate for <code>MasterDataFacadeBean</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author      <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
// @Component
@ManagedResource(
		objectName="nuclos.client.cache:name=MaintenanceDelegate",
		description="Nuclos Client MaintenanceDelegate",
		currencyTimeLimit=15,
		log=false)
public class MaintenanceDelegate {

	private static final Logger LOG = Logger.getLogger(MaintenanceDelegate.class);
	
	private static final SecurityCache SECURITYCACHE = SecurityCache.getInstance();
	
	private static MaintenanceDelegate INSTANCE;

	private MaintenanceFacadeRemote facade;

	// @Autowired
	public final void setMaintenanceFacadeRemote(MaintenanceFacadeRemote maintenanceFacadeRemote) {
		this.facade = maintenanceFacadeRemote;
	}
	
	/**
	 * Use getInstance() to create an (the) instance of this class
	 * 
	 */
	protected MaintenanceDelegate() throws RuntimeException {
		INSTANCE = this;
	}

	public static MaintenanceDelegate getInstance() {
		if (INSTANCE == null) {
			// lazy support
			INSTANCE = SpringApplicationContextHolder.getBean(MaintenanceDelegate.class);
		}
		return INSTANCE;
	}
	
	public boolean isSuperUserAndMaintenanceModeOn() {
		return (SECURITYCACHE.isSuperUser() || SECURITYCACHE.isMaintenanceUser()) && MaintenanceFacadeRemote.MAINTENANCE_MODE_ON.equals(getMaintenanceMode());
	}

	public boolean isSuperUserAndMaintenanceModeInitialized() {
		return (SECURITYCACHE.isSuperUser() || SECURITYCACHE.isMaintenanceUser()) && MaintenanceFacadeRemote.MAINTENANCE_MODE_INITIALIZED.equals(getMaintenanceMode());
	}
	
	public boolean isMaintenanceModeOff() {
		return MaintenanceFacadeRemote.MAINTENANCE_MODE_OFF.equals(getMaintenanceMode());		
	}

	private String getMaintenanceMode() {
		return facade.getMaintenanceMode();
	}

	public void exitMaintenanceMode() throws CommonValidationException {
		facade.exitMaintenanceMode();
	}

	public Integer getCompleteWaittimeInSeconds() {
		return facade.getCompleteWaittimeInSeconds();
	}

}
// class MasterDataDelegate
