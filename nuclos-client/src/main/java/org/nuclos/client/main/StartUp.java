//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;
import javax.naming.NamingException;
import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.nuclos.api.ui.annotation.NucletComponent;
import org.nuclos.client.StartIcons;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.common.NuclosCollectableComponentFactory;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.login.LoginController;
import org.nuclos.client.login.LoginEvent;
import org.nuclos.client.login.LoginListener;
import org.nuclos.client.login.LoginPanel;
import org.nuclos.client.searchfilter.SearchFilterCache;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.SpringApplicationSubContextsHolder;
import org.nuclos.common.Version;
import org.nuclos.common2.ContextConditionVariable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.SecurityFacadeRemote;
import org.nuclos.server.servermeta.ejb3.ServerMetaFacadeRemote;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.SystemPropertyUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Controller responsible for starting up the Nucleus client.
 * This is a temporary object which will be discarded after the startup is done.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
public class StartUp  {

	private static final Logger LOG = Logger.getLogger(StartUp.class);

    /** If this system property is present (no values, only presence) then the version comparison is skipped. */
    private static final String PARAM_SKIP_VERSION_COMPARION = "skipVersionComparison";

    private static Class<?> nuclosSyntheticaUtilsClass;

	// These progress constants sum up to to 100:
	static final int PROGRESS_COMPARE_VERSIONS = 5;
	static final int PROGRESS_INIT_SECURITYCACHE = 5;
	static final int PROGRESS_INIT_NOTIFICATION = 5;
	static final int PROGRESS_READ_ATTRIBUTES = 8;
	static final int PROGRESS_READ_LOMETA = 5;
	static final int PROGRESS_READ_SEARCHFILTER = 5;
	static final int PROGRESS_READ_RULES = 5;
	static final int PROGRESS_CREATE_MAINFRAME = 23;
	static final int PROGRESS_INIT_ONLINEHELP = 4;
	static final int PROGRESS_CREATE_MAINMENU = 6;
	static final int PROGRESS_RESTORE_WORKSPACE = 29;

	/**
	 * log4j category
	 * @todo this shouldn't be a member probably
	 */
	private static Logger log;
	
	private static class StartUpApplicationListener implements ApplicationListener<ContextRefreshedEvent> {
		
		private final ContextConditionVariable contextCondition;
		
		private StartUpApplicationListener(ContextConditionVariable contextCondition) {
			this.contextCondition = contextCondition;
		}
		
		@Override
		public void onApplicationEvent(ContextRefreshedEvent event) {
			synchronized (contextCondition) {
				contextCondition.refreshed();
				contextCondition.notify();
			}
		}
	}
	
	private static final String[] CLIENT_SPRING_BEANS = new String[] {
		"META-INF/nuclos/client-beans.xml", "META-INF/nuclos/client-jmx-beans.xml", "META-INF/nuclos/client-cache-beans.xml"
	};
	
	private static final String EXTENSION_THEME_PATH = "META-INF/nuclos/nuclos-theme.properties";
	
	/**
	 * To avoid classpath scanning we limit ourself to ONE extension theme (tp)
	 */
	private static final String EXTENSION_THEMES = "classpath:" + EXTENSION_THEME_PATH;

	/**
	 * To avoid classpath scanning we limit ourself to ONE extension (tp)
	 */
	private static final String EXTENSION_SPRING_BEANS = "classpath*:META-INF/nuclos/nuclos-extension-client-beans.xml";
	
	//

	private final String[] args;
	
	private ClassPathXmlApplicationContext startupContext;
	
	private ClassPathXmlApplicationContext clientContext;
	
	/**
	 * Condition variable for processing themes.
	 */
	private final ContextConditionVariable themesContextCondition = new ContextConditionVariable("themes");
	
	/**
	 * Condition variable for Spring client context wait (e.g. client-beans.xml)
	 */
	private final ContextConditionVariable clientContextCondition = new ContextConditionVariable("client");
	
	/**
	 * Condition variable for Spring complete initialization (e.g. all nuclos extension
	 * and their associated Spring contexts).
	 */
	private final ContextConditionVariable lastContextCondition = new ContextConditionVariable("extensions");
	
	//
	
	public StartUp(String[] args) {
		this.args = args == null ? null : Arrays.copyOf(args, args.length);
	}
	
	private static synchronized void loadNuclosSyntheticaUtils() throws ClassNotFoundException {
		if (nuclosSyntheticaUtilsClass == null) {
			nuclosSyntheticaUtilsClass = LangUtils.getClassLoaderThatWorksForWebStart()
					.loadClass("org.nuclos.client.synthetica.NuclosSyntheticaUtils");
		}
	}
	
	private static void registerNuclosTheme(String themeName, URL pathToXml) {
		try {
			loadNuclosSyntheticaUtils();
			final Method registerNuclosTheme = nuclosSyntheticaUtilsClass.getDeclaredMethod("registerNuclosTheme", 
					new Class[] { String.class, URL.class });
			registerNuclosTheme.invoke(null, themeName, pathToXml);
		} catch (Exception e) {
			throw new IllegalStateException("registerNuclosTheme failed: ", e);
		}
	}

	private static void setLookAndFeel(String nuclosTheme, String nuclosFontFamily) {
		try {
			loadNuclosSyntheticaUtils();
			
			final Method setLookAndFeel = nuclosSyntheticaUtilsClass.getDeclaredMethod("setLookAndFeel",
					new Class[] { String.class, String.class });
			setLookAndFeel.invoke(null, nuclosTheme, nuclosFontFamily);
			
		} catch (Exception e) {
			throw new IllegalStateException("setLookAndFeel failed", e);
		}
	}
    
    public final void init(final SimpleSplash splash) {
		// setup client side logging:
		// System.out.println("Default TEMP: " + org.nuclos.common2.IOUtils.getDefaultTempDir());
		setupClientLogging();
		final NuclosCriticalErrorHandler criticalErrorHandler = new NuclosCriticalErrorHandler(false);
		
		/*
		 * NEVER CHANGE THIS!!! (tp)
		 * NUCLOS-2164: 
		 * If you set this to some obscure, broken, and wrong <code>getClass().getClassLoader()</code>
		 * YOU WILL break java web start.
		 */
		final ClassLoader cl = LangUtils.getClassLoaderThatWorksForWebStart();

		startupContext = new ClassPathXmlApplicationContext(
				new String[] { "META-INF/nuclos/client-beans-startup.xml" }, false);
		// see http://fitw.wordpress.com/2009/03/14/web-start-and-spring/ why this is needed (tp)
		startupContext.setClassLoader(cl);
		//PROFILING_NEXTCMD: 2,2s / 11s
		startupContext.refresh();
		LOG.info("Spring startupContext refreshed");
		startupContext.registerShutdownHook();
		
		final Runnable run1 = new Runnable() {

			@Override
			public void run() {
				try {
					// final Resource[] themes = clientContext.getResources(EXTENSION_THEMES);
					final Resource[] themes = new Resource[] { startupContext.getResource(EXTENSION_THEMES) };
					log.info("loading themes properties from the following files: " + Arrays.asList(themes));
					// Thread.yield();
					
					for (Resource r : themes) {
						if (!r.exists()) {
							log.warn("theme properties not found: " + r);
							continue;
						}
						log.info("Processing theme properties: " + r);
						Properties p = new Properties();
						p.load(r.getInputStream());

						for (Object key : p.keySet()) {
							if (key instanceof String && p.get(key) != null && p.get(key) instanceof String) {

								String sKey = (String) key;
								if (sKey.startsWith("name")) {
									String xmlKey = "xml";
									if (sKey.length() > 4) {
										String sNumber = sKey.substring(4);
										xmlKey = xmlKey + sNumber;
									}
									Object xml = p.get(xmlKey);
									if (xml != null && xml instanceof String) {
										final String name = (String) p.get(key);
										LOG.info("Register theme '" + name + "' xml=" + xml + " from " + r.getURI());
										final String uri = r.getURI().toString();
										final int idx = uri.indexOf(EXTENSION_THEME_PATH);
										if (idx < 0) {
											throw new IllegalStateException();
										}
										String jarUrl = uri.substring(0, idx);
										String path = (String) xml;
										while (jarUrl.endsWith("/")) {
											jarUrl = jarUrl.substring(0, jarUrl.length() - 1);
										}
										while (path.startsWith("/")) {
											path = path.substring(1);
										}
										registerNuclosTheme(name, new URL(jarUrl + "/" + path));
									}
								}
							}
						}
					}
					synchronized (themesContextCondition) {
						themesContextCondition.refreshed();
						themesContextCondition.notify();
					}
					
					// Time zone stuff
			        final ServerMetaFacadeRemote sm = startupContext.getBean(ServerMetaFacadeRemote.class);
			        final TimeZone serverDefaultTimeZone = sm.getServerDefaultTimeZone();
			        final StringBuilder msg = new StringBuilder(); 
			        msg.append("Default local  time zone is: ").append(TimeZone.getDefault().getID()).append("\n");
			        msg.append("Default server time zone is: ").append(serverDefaultTimeZone.getID()).append("\n");
					if (!LangUtils.equal(TimeZone.getDefault(), serverDefaultTimeZone)) {
						TimeZone.setDefault(serverDefaultTimeZone);
						msg.append("Local default time zone is set to server default!\n");
					}
					msg.append("Initial local  time zone is: " + Main.getInitialTimeZone().getID());
					log.info(msg);
					
					// Scanning context
					clientContext = new ClassPathXmlApplicationContext(CLIENT_SPRING_BEANS, false, startupContext);
					final StartUpApplicationListener clientListener = new StartUpApplicationListener(clientContextCondition);
					clientContext.addApplicationListener(clientListener);
					
					// see http://fitw.wordpress.com/2009/03/14/web-start-and-spring/ why this is needed (tp)
					clientContext.setClassLoader(cl);
					final SpringApplicationSubContextsHolder holder = SpringApplicationSubContextsHolder.getInstance();
					holder.setClientContext(clientContext);
					holder.setLastContextCondition(lastContextCondition);
					// Thread.yield();
					//PROFILING_NEXTCMD: 4,6s / 11s
					clientContext.refresh();
					log.info("Spring clientContext refreshed");
					clientContext.registerShutdownHook();
					// Ready for subscribe
					final TopicNotificationReceiver tnr = startupContext.getBean(TopicNotificationReceiver.class);
					tnr.setReadyToSubscribe(true);
					log.info("@NucletComponents within spring context: " + clientContext.getBeansWithAnnotation(NucletComponent.class));
					
					final Resource[] extensions = clientContext.getResources(EXTENSION_SPRING_BEANS);
					// final Resource[] extensions = new Resource[] { clientContext.getResource(EXTENSION_SPRING_BEANS) };
					log.info("loading extensions spring sub contexts from the following xml files: " + Arrays.asList(extensions));

					final int size = extensions.length;
					if (size == 0) {
						// propagate clientContextCondition to lastContextCondition
						clientContextCondition.waitFor();
						synchronized (lastContextCondition) {
							lastContextCondition.refreshed();
							lastContextCondition.notify();
						}
						log.info("no extension contexts found, all spring contexts refreshed");
					}
					else {
						// Thread.yield();
						for (int i = 0; i < size; ++i) {
							final Resource r = extensions[i];
							final boolean last = (i + 1 == size);
							if (!r.exists()) {
								log.error("spring sub context xml not found: " + r);
								if (last) {
									synchronized (lastContextCondition) {
										lastContextCondition.refreshed();
										lastContextCondition.notify();
									}
								}
								continue;
							}
							log.info("Processing sub context xml: " + r);
							final AbstractXmlApplicationContext ctx;
							if (r instanceof ClassPathResource) {
								ctx = new ClassPathXmlApplicationContext(new String[] { ((ClassPathResource)r).getPath() }, false, clientContext);
							} 
							else if (r instanceof UrlResource) {
								final UrlResource ur = (UrlResource) r;
								final File springXml = File.createTempFile("spring-nuclos-extension", ".xml");
								log.info("spring-nuclos-extension.xml stored in: " + springXml.getCanonicalPath());
								springXml.deleteOnExit();
								IOUtils.copy(ur.getInputStream(), new FileOutputStream(springXml));
								// ctx = new ResourceXmlApplicationContext(r, Collections.EMPTY_LIST, clientContext, Collections.EMPTY_LIST, false);
								ctx = new FileSystemXmlApplicationContext(new String[] { "file:" + springXml.getPath() }, false, clientContext);
							}
							else {
								ctx = new FileSystemXmlApplicationContext(new String[] { r.getFile().getPath() }, false, clientContext);
							}
							if (last) {
								final StartUpApplicationListener lastListener = new StartUpApplicationListener(lastContextCondition);
								ctx.addApplicationListener(lastListener);
								log.info("last extension context " + r + " used as condition variable");
							}
							// see http://fitw.wordpress.com/2009/03/14/web-start-and-spring/ why this is needed (tp)
							ctx.setClassLoader(cl);
							// clientContext.addApplicationListener(refreshListener);
							log.info("before refreshing spring context " + r);
							ctx.refresh();
							holder.registerSubContext(ctx);
							
							log.info("Spring subcontext refreshed: '" + r + "' last=" + last);
						}
					}
					// spring is ready (client side)
					SpringApplicationContextHolder.setSpringReady();
				}
				catch (IOException e1) {
					log.error(e1.getMessage(), e1);
				}
			}
		};
		final Thread thread1 = new Thread(run1, "Startup.init.run1");
		thread1.setPriority(Thread.NORM_PRIORITY - 1);
		thread1.start();
		
		// set the default locale:
		// this makes sure the client is independent of the host's locale.
		/** @todo i18n */
		Locale.setDefault(Locale.GERMANY);

		log.info("Java-Version " + System.getProperty("java.version") + " (" + System.getProperty("java.vm.version") +
				") from " + System.getProperty("java.vendor") + " (" + System.getProperty("java.vm.name") + ")");

		// initialize the Errors instance:
		Errors.getInstance().setAppName(ApplicationProperties.getInstance().getName());
		// first, set the non-strict version of the critical error handler to avoid locking-out on initialization errors.
		// on successful initialization, the strict version of the critical error handler will be set.
		Errors.getInstance().setCriticalErrorHandler(criticalErrorHandler);

		// from here, we can issue error messages using the Errors class.

		// Install the CollectableComponentFactory for the application:
		CollectableComponentFactory.setInstance(newCollectableComponentFactory());

		// we create the gui completely in the event dispatch thread, as recommended by Sun:

		EventQueue.invokeLater(new Runnable() {
			@Override
            public void run() {
				try {
					// this waits for some condition! (tp)
					createGUI(splash);
					if (Main.getInstance().isMacOSX()) {
						Class<?> macAppClass = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.Application");
						Object macAppObject = macAppClass.getConstructor().newInstance();
						// set Nuclos dock icon
						macAppClass.getMethod("setDockIconImage", java.awt.Image.class).invoke(
								macAppObject, StartIcons.getInstance().getBigTransparentApplicationIcon512().getImage());
					}
					// really subscribe to the topics collected at startup time
					TopicNotificationReceiver.getInstance().realSubscribe();
				}
				catch (Exception e) {
					LOG.fatal("Startup failed: " + e, e);
				}
			}
		});
	}	// ctor

	private static CollectableComponentFactory newCollectableComponentFactory() {
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getCollectableComponentFactoryClassName(),
					NuclosCollectableComponentFactory.class.getName());

			return (CollectableComponentFactory) 
					LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName).newInstance();
		}
		catch (Exception ex) {
			throw new CommonFatalException("CollectableComponentFactory cannot be created.", ex);
		}
	}

	private void setupClientLogging() {
		String sLog4jUrl = System.getProperty("log4j2.url");
		if (!StringUtils.isNullOrEmpty(sLog4jUrl)) {
			try {
				System.out.println("Try to configure loggging from " + sLog4jUrl + ".");
				if (new File(sLog4jUrl).exists()) {
					initLogging(sLog4jUrl);
					log = Logger.getLogger(StartUp.class);
					log.info("Logging configured from " + sLog4jUrl);
					return;
				} else {
					System.out.println("Failed to configure logging from " + sLog4jUrl + ": file not existing.");
				}
			}
			catch(Exception e) {
		        // Ok! (tp)
				System.err.println("Failed to configure logging from " + sLog4jUrl + ": " + e.getMessage());
			}
		}
	
		final URL configurationfile = getLog4jConfigurationFile();
		try {
			System.out.println("Try to configure loggging from configuration file: " + configurationfile);
			initLogging(configurationfile.toExternalForm());
			log = Logger.getLogger(StartUp.class);
			log.info("Logging configured from log4j configuration: " + configurationfile);
		}
		catch (Throwable t) {
			throw new NuclosFatalException("The client-side logging could not be initialized, because the configuration file " 
					+ configurationfile  + " was not found in the Classpath.", t);
		}
	}
	
	private void initLogging(String location) {
		try {
			//final Class<?> log4jConfigurer = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("org.springframework.util.Log4jConfigurer");
			//final Method initLogging = log4jConfigurer.getDeclaredMethod("initLogging", String.class);
			//initLogging.invoke(null, location);
			Configurator.initialize(null, location);
		} catch (Throwable e) {
			System.err.println("Could not init Log4j: " + e);
			e.printStackTrace();
		}
	}
	
	private static URL getLog4jConfigurationFile() {
		String conf = System.getProperty("log4j.configuration");
		if (conf == null) {
			conf = Boolean.getBoolean("functionblock.dev")
					? "log4j2-dev.xml" : "log4j2-prod.xml";
		}
		URL url;
		try {
			url = new URL(conf);
		}
		catch (MalformedURLException e) {
			url = null;
		}
		if (url == null) {
			url = LangUtils.getClassLoaderThatWorksForWebStart().getResource(conf);
		}
		return url;
	}
	
	/**
	 * 
	 * @return 
	 * 		logfile with path
	 */
	public static String getLogFile() {
		String result = "NOT AVAILABLE";
		try {
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    Document doc = dBuilder.parse(StartUp.getLog4jConfigurationFile().openStream());
		  
		    doc.getDocumentElement().normalize();
		    
		    NodeList elementsRolling = doc.getElementsByTagName("RollingFile");
		    Node attrFileName = elementsRolling.item(0).getAttributes().getNamedItem("fileName");
		    result = attrFileName.getNodeValue();
		    result = result.replace("sys:java.io.tmpdir", "java.io.tmpdir");
			result = SystemPropertyUtils.resolvePlaceholders(result);
		    
		} catch (Exception ex) {
			// do nothing
		}
		return result;
	}

	private void createGUI(final SimpleSplash splash) {
		themesContextCondition.waitFor();
		setupLookAndFeel();
		
		LoginPanel.getInstance(startupContext.getBean(SecurityFacadeRemote.class));
		final Main main = Main.getInstance();
		try {
			// perform login:
			final LoginController ctlLogin = new LoginController(null, this.args, startupContext, clientContextCondition);

			ctlLogin.addLoginListener(new LoginListener() {
				@Override
                public void loginSuccessful(final LoginEvent ev) {
					splash.remove();
					log.info("login of " + ev.getAuthenticatedUserName() + " to server " 
							+ ev.getConnectedServerName() + " triggered");
					try {
						registerOSXHandler();
					}
					catch (Exception ex) {
						Errors.getInstance().showExceptionDialog(null, ex);
						main.exit(Main.ExitResult.ABNORMAL);
					}
					
					UIUtils.runCommandForTabbedPane(null, new Runnable() {
						@Override
                        public void run() {
							try {
								main.getMainFrame().postConstruct();
								// ???
								main.getMainFrame().init(ev.getAuthenticatedUserName(), ev.getConnectedServerName());
								
								compareClientAndServerVersions();
								ctlLogin.increaseLoginProgressBar(PROGRESS_COMPARE_VERSIONS);
								
								//PROFILING_NEXTCMD: 4,2s / 11s
								createMainController(ev.getAuthenticatedUserName(), ev.getConnectedServerName(), ctlLogin);

								try {
									// notify LaunchListeners
									ServiceManager.lookup("javax.jnlp.SingleInstanceService");
									main.notifyListeners(StartUp.this.args);
							    }
								catch (UnavailableServiceException ex) {
							    	// no webstart context
									main.notifyListeners(StartUp.this.args);
							    }

								// After successful initialization, set the strict version of the critical error handler:
								SwingUtilities.invokeLater(new Runnable() {
									@Override
                                    public void run() {
										Errors.getInstance().setCriticalErrorHandler(new NuclosCriticalErrorHandler(true));
										log.info("login done");
									}
								});
							}
							catch (Exception ex) {
								Errors.getInstance().showExceptionDialog(null, ex);
								main.exit(Main.ExitResult.ABNORMAL);
							}
						}

						private void compareClientAndServerVersions() {
                            if (System.getProperty(PARAM_SKIP_VERSION_COMPARION) != null) {
                                return;
                            }
                            final Version versionClient = ApplicationProperties.getInstance().getNuclosVersion();
							final Version versionServer = SecurityDelegate.getInstance().getCurrentApplicationVersionOnServer();
							if (!versionClient.equals(versionServer)) {
								final String sMessage = "The version of this client is not compatible with the version of the connected server." +
										"\nClient-version: " + versionClient.toString() + "\nServer-version: " + versionServer.toString() +
										"\n\nPlease contact the system administrator.";
									//"Die Version dieses Clients ist nicht kompatibel mit der Version des verbundenen Servers.\n" + "Client-Version: " + versionClient + "\n" + "Server-Version: " + versionServer + "\n" + "\nBitte wenden Sie sich an den Systemadministrator.";
								throw new NuclosFatalException(sMessage);
							}
						}

					});
				}

				@Override
                public void loginCanceled(LoginEvent ev) {
					splash.remove();
					/** @todo adjust semantics (failed vs. canceled) */
					main.exit(Main.ExitResult.LOGIN_FAILED);
				}

				@Override
				public int getPriority() {
					return 0;
				}
			});

			// show LoginPanel as soon as possible
			ctlLogin.run(splash);
			/** @todo move exception handling to LoginController.run() */
		}
		catch (CommonFatalException ex) {
			try {
				final Throwable tCause = ex.getCause();
				if (tCause instanceof Exception) {
					throw (Exception) tCause;
				}
				else if (tCause instanceof Error) {
					throw (Error) tCause;
				}
				else {
					throw new CommonFatalException("Unknown Throwable", ex);
				}
			}
			catch (NamingException exCause) {
				final String sMessage = "No connection could be establish to the server.\nPlease contact the system administrator.";
				Errors.getInstance().showExceptionDialog(null, sMessage, ex);
			}
			catch (Exception exCause) {	
				// everything else
				final String sMessage = "A fatal error occurred.";
				Errors.getInstance().showExceptionDialog(null, sMessage, ex);
			}
			finally {
				main.exit(Main.ExitResult.ABNORMAL);
			}
		}
		catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(null, null, ex);
			main.exit(Main.ExitResult.ABNORMAL);
		}
	}

	private static void registerOSXHandler() 
			throws ClassNotFoundException, IllegalArgumentException, SecurityException, InstantiationException, IllegalAccessException, 
			InvocationTargetException, NoSuchMethodException 
	{
		final Main main = Main.getInstance();
		if (main.isMacOSX()) {
			Class<?> macAppClass = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.Application");
			Object macAppObject = macAppClass.getConstructor().newInstance();

			// register about handler
			Class<?> macAboutHandlerClass = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.AboutHandler");
			Method macAppSetAboutHandlerMethod = macAppClass.getDeclaredMethod("setAboutHandler", new Class[] { macAboutHandlerClass });

			Object macAboutHandler = Proxy.newProxyInstance(LangUtils.getClassLoaderThatWorksForWebStart(), 
					new Class[] { macAboutHandlerClass }, new InvocationHandler() {
				
				@Override
				public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
					if (method != null && "handleAbout".equals(method.getName()) && args.length == 1) {
						main.getMainController().cmdShowAboutDialog();
					}

					return null;
				}
			});
			macAppSetAboutHandlerMethod.invoke(macAppObject, new Object[] { macAboutHandler });

			// register preferences handler
			Class<?> macPreferencesHandlerClass = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.PreferencesHandler");
			Method macAppSetPreferencesHandlerMethod = macAppClass.getDeclaredMethod("setPreferencesHandler", new Class[] { macPreferencesHandlerClass });

			Object macPreferencesHandler = Proxy.newProxyInstance(LangUtils.getClassLoaderThatWorksForWebStart(), 
					new Class[] { macPreferencesHandlerClass }, new InvocationHandler() {
				
				@Override
				public Object invoke(Object proxy, Method method, Object[] args)	throws Throwable {
					if (method != null && "handlePreferences".equals(method.getName()) && args.length == 1) {
						main.getMainController().cmdOpenSettings();
					}

					return null;
				}
			});
			macAppSetPreferencesHandlerMethod.invoke(macAppObject, new Object[] { macPreferencesHandler });

			// register quit handler
            Class<?> macQuitHandlerClass = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.QuitHandler");
            Method macAppSetQuitHandlerMethod = macAppClass.getDeclaredMethod("setQuitHandler", new Class[] { macQuitHandlerClass });

            Object macQuitHandler = Proxy.newProxyInstance(LangUtils.getClassLoaderThatWorksForWebStart(), 
            		new Class[] { macQuitHandlerClass }, new InvocationHandler() {
            	
				@Override
				public Object invoke(Object proxy, Method method, final Object[] args)	throws Throwable {
					if (method != null && "handleQuitRequestWith".equals(method.getName()) && args.length == 2) {
						Class<?> macQuitResponseClass = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.QuitResponse");
						final Method macQuitResponsePerformQuitMethod = macQuitResponseClass.getDeclaredMethod("performQuit");
						final Method macQuitResponseCancelQuitMethod = macQuitResponseClass.getDeclaredMethod("cancelQuit");

						Main.getInstance().getMainController().cmdWindowClosing(true, new ResultListener<Boolean>() {
							@Override
							public void done(Boolean result) {
								try {
									if (Boolean.TRUE.equals(result)) {
										macQuitResponsePerformQuitMethod.invoke(args[1]);
									} else {
										macQuitResponseCancelQuitMethod.invoke(args[1]);
									}
								} catch (Exception ex) {
									LOG.error(ex.getMessage(), ex);
								}
							}
						});
					}
					return null;
				}
			});
            macAppSetQuitHandlerMethod.invoke(macAppObject, new Object[] { macQuitHandler });

            //register dock menu
            PopupMenu macDockMenu = new PopupMenu();
            MenuItem miDockLogoutExit = new MenuItem(SpringLocaleDelegate.getInstance().getResource("miLogoutExit", "Abmelden und Beenden"));
            miDockLogoutExit.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					main.getMainController().cmdLogoutExit();
				}
			});
            macDockMenu.add(miDockLogoutExit);

            Method macAppSetDockMenuMethod = macAppClass.getDeclaredMethod("setDockMenu", PopupMenu.class);
            macAppSetDockMenuMethod.invoke(macAppObject, macDockMenu);
		}
		
		if (main.isMacOSXLionOrBetter()) {
			//register Mac OS X Lion Fullscreen Support
            try {
                Class<?> util = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.FullScreenUtilities");
                Class<?> params[] = new Class[2];
                params[0] = Window.class;
                params[1] = Boolean.TYPE;
                Method method = util.getMethod("setWindowCanFullScreen", params);
                method.invoke(util, Main.getInstance().getMainFrame(), true);
                
                // listener for fullscreen toggle 
                // currently not in use... 
                /*
                InvocationHandler listenerHandler = new InvocationHandler() {
    				@Override
    				public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    					// fullscreen enabled/disabled
    					return null;
    				}
                };
                Class<?> listener = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("com.apple.eawt.FullScreenListener");
                Object proxyObject = Proxy.newProxyInstance(LangUtils.getClassLoaderThatWorksForWebStart(), new Class[] {listener}, listenerHandler);
                method = util.getMethod("addFullScreenListenerTo", new Class<?>[] {Window.class, listener});
                method.invoke(null, Main.getInstance().getMainFrame(), proxyObject);*/
                
            } catch (ClassNotFoundException e) {
            	LOG.warn("Mac OS X Lion Fullscreen Support not activ. Java Update necesary...", e);
            } catch (NoSuchMethodException e) {
                LOG.error(e.getMessage(), e);
            } catch (InvocationTargetException e) {
            	LOG.error(e.getMessage(), e);
            } catch (IllegalAccessException e) {
            	LOG.error(e.getMessage(), e);
            }
		}
	}

	private void createMainController(String sUserName, String sNucleusServerName, LoginController lc)
			throws CommonPermissionException {
		
		final SearchFilterCache searchFilterCache = SearchFilterCache.getInstance();
		searchFilterCache.setUserName(sUserName);
		
		log.info("beginning of createMainController processing");
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getMainControllerClassName(),
					MainController.class.getName());

			final Class<? extends MainController> clsMainController = (Class<? extends MainController>) 
					LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName);
			final Constructor<? extends MainController> ctor = clsMainController.getConstructor(
					String.class, String.class, LoginController.class, ContextConditionVariable.class);
			ctor.newInstance(sUserName, sNucleusServerName, lc, lastContextCondition);
		}
		catch (InvocationTargetException ex) {
			final Throwable tTarget = ex.getTargetException();
			if (tTarget instanceof CommonPermissionException) {
				throw ((CommonPermissionException) tTarget);
			}
			else {
				throw new CommonFatalException(tTarget);
			}
		}
		catch (Exception ex) {
			throw new CommonFatalException("MainController cannot be created.", ex);
		}
	}

	
	private void setupLookAndFeel() {
		setupLookAndFeel(startupContext);
	}

	private static String lastNuclosTheme = "<NoTheme>";
	static void setupLookAndFeel(ClassPathXmlApplicationContext startupContext) {
		try {
			String defaultNuclosTheme = null;
			String nuclosFontFamily = null;
			
			if (startupContext != null) {
				ServerMetaFacadeRemote sm = (ServerMetaFacadeRemote) startupContext.getBean("serverMetaService");
				defaultNuclosTheme = sm.getDefaultNuclosTheme();
				
				// With windows we have to check for unicode fonts in nuclos parameters
				if(Main.isWindows()) {
					nuclosFontFamily = sm.getNuclosFontFamily();					
				}
			}
			if (defaultNuclosTheme == null && lastNuclosTheme == null) {
			} else if (defaultNuclosTheme == null || lastNuclosTheme == null) {
				setLookAndFeel(defaultNuclosTheme, nuclosFontFamily);
			} else if (defaultNuclosTheme.equals(lastNuclosTheme)) {
				setLookAndFeel(defaultNuclosTheme,nuclosFontFamily);
			}
			lastNuclosTheme = defaultNuclosTheme;

			UIManager.put("TabbedPane.contentOpaque", Boolean.FALSE);
			UIManager.put("DesktopIconUI", "org.nuclos.client.ui.NuclosDesktopIconUI");
			UIManager.put("TextArea.font", UIManager.get("TextField.font"));
			
			Toolkit.getDefaultToolkit().setDynamicLayout(false);
			System.setProperty("awt.dynamicLayoutSupported", "false");

			// enable for test only:
//			System.setProperty("apple.laf.useScreenMenuBar", "false");
		}
		catch (Exception ex) {
			// If the look&feel can't be set, don't worry. Just print stack trace and continue:
			if (log != null) {
				log.warn("Look&Feel cannot be set", ex);
			} else {
				// Ok! (tp)
				ex.printStackTrace();
			}
		}
	}
	
	public static void setUIFont(FontUIResource f) {
		   java.util.Enumeration keys = UIManager.getDefaults().keys();
		    while (keys.hasMoreElements()) {
		      Object key = keys.nextElement();
		      Object value = UIManager.get (key);
		      if (value != null && value instanceof java.awt.Font)
		        UIManager.put (key, f);
		      }
    }
	

}	// class StartUp
