package org.nuclos.client.main;

import java.awt.*;
import java.net.URL;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.common2.LangUtils;

public class SimpleSplash extends JFrame implements Runnable {
	
	private static final Logger LOG = Logger.getLogger(SimpleSplash.class);
	
	private static final String fpicture = "org/nuclos/client/images/Preloader5.500XX.png";
	private static final int nsequences = 17;
	private static final long maxTime = 120*1000L;
	private static final long sleepTime = 20L;
	
	private static final Color transGrey = new Color(238, 238, 238, 0);
	
	//

	private boolean stopped = false;
	private JPanel panel;
	private JLabel label;
	private Color oldBackground;
	
	SimpleSplash() {
		setUndecorated(true);
		Color grey = Color.LIGHT_GRAY;
		if (!isUnix()) {
			grey = transGrey;
			oldBackground = getBackground();
			setBackground(grey);
		}
		Container contentPane = getContentPane();
		contentPane.setBackground(grey);
		if (contentPane instanceof JPanel) {
			((JPanel)contentPane).setOpaque(false);
		}
		
		JRootPane rootPane = getRootPane();
		rootPane.setBackground(grey);
		rootPane.setOpaque(false);
		
		ImageIcon icon = getNextImageIcon();
		setIconImage(icon.getImage());
		int width = 272;// image.getIconWidth();
		int height = 275; // image.getIconHeight();
		panel = new JPanel();
		panel.setBackground(grey);
		panel.setLayout(new GridBagLayout());
		panel.setOpaque(false);
		label = new JLabel(icon);
		label.setBackground(grey);
		label.setOpaque(false);
		label.setSize(width, height);
		panel.setSize(width, height);
		panel.add(label);
		contentPane.add(panel);
		
		Thread t = new Thread(this);
		t.start();
		
		setSize(width, height);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	@Override
	public void run() {
		 long startTime = System.currentTimeMillis();
		 for (;;) try {
			 Thread.sleep(sleepTime);
			 ImageIcon image = getNextImageIcon();
			 if (image != null) {
				 label.setIcon(image);				 
			 }
			 if (stopped) {
				 break;
			 }
			 if (System.currentTimeMillis() - startTime >= maxTime) {
				 break;
			 }
		 } catch (InterruptedException ie) {
			 // Ok! (tp)
			 ie.printStackTrace();
		 }
	}

	private static int iSequ = 0;
	static ImageIcon getNextImageIcon() {
		iSequ++;
		if (iSequ > nsequences) iSequ = 1;
		String sSequ = String.valueOf(iSequ);
		if (iSequ < 10) sSequ = "0" + sSequ;
		String resName = fpicture.replaceFirst("XX", sSequ);
		URL url = LangUtils.getClassLoaderThatWorksForWebStart().getResource(resName);
		if (url == null)
			return null;
		ImageIcon image = new ImageIcon(url);
		return image;
	}
	
	public void removePanel() {
		super.remove(panel);
		if (oldBackground != null) {
			SwingUtilities.invokeLater(new Runnable() {				
				@Override
				public void run() {
					setBackground(oldBackground);
				}
			});
		}
	}
	
	void remove(){
		final JFrame frame = this;
		SwingUtilities.invokeLater(new Runnable() {		
			@SuppressWarnings("restriction")
			@Override
			public void run() {
				for (int i = 100; i >= 0; i-=5) try {
					com.sun.awt.AWTUtilities.setWindowOpacity(frame, i*0.01f);
					Thread.sleep(30L);
				} catch (Exception e) {
				}
				frame.dispose();
				stopped = true;
			}
		});
	}
	
	private static String OS = System.getProperty("os.name").toLowerCase();
	public static boolean isWindows() { 
		return (OS.indexOf("win") >= 0);
	}
 
	public static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}
 
	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}
 
	public static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}
}


