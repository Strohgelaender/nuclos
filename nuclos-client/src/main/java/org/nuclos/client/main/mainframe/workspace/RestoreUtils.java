//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.main.mainframe.workspace;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import org.apache.log4j.Logger;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.command.ResultListenerX;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.GenericAction;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.ExternalFrame;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameSplitPane;
import org.nuclos.client.main.mainframe.MainFrameSpringComponent;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.client.ui.CommonJFrame;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.error.ESeverity;
import org.nuclos.client.ui.error.SimpleExceptionForDisplay;
import org.nuclos.common.Actions;
import org.nuclos.common.MutableBoolean;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.SpringApplicationSubContextsHolder;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescription2.Frame;
import org.nuclos.common.WorkspaceDescription2.MutableContent;
import org.nuclos.common.WorkspaceDescription2.Split;
import org.nuclos.common.WorkspaceDescription2.Tabbed;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common.preferences.WorkspaceManager;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;

public class RestoreUtils {

	private static final Logger LOG = Logger.getLogger(RestoreUtils.class);

	private static final String PREFS_NODE_LAST_SETTINGS = "lastSettings";
	private static final String NORMAL_BOUNDS = "normalBounds";
	private static final String EXTENDED_STATE = "extendedState";

	private static final String THREAD_NAME = "RestoreUtils.workspaceRestore.";
	private static final List<Thread> threadList = new ArrayList<Thread>();
	private static final List<MainFrameTabbedPane> finishInitialisationTabbedPanes = new ArrayList<MainFrameTabbedPane>();
	
	private static RestoreUtils INSTANCE;
	
	//

	private static List<GenericAction> cachedActions;
	
	// Spring injection
	
	private PreferencesFacadeRemote preferencesFacadeRemote;
	
	private MainFrameSpringComponent mainFrameSpringComponent;
	
	private static WorkspaceManager _workspaceManager = null;
	private static WorkspaceManager getWorkspaceManager() {
		if (_workspaceManager == null) {
			_workspaceManager = WorkspaceManager.INSTANCE(SpringLocaleDelegate.getInstance());
		}
		return _workspaceManager;
	}
	
	
	
	// end of Spring injection

	RestoreUtils() {
		INSTANCE = this;
	}
	
	public final void setPreferencesFacadeRemote(PreferencesFacadeRemote preferencesFacadeRemote) {
		this.preferencesFacadeRemote = preferencesFacadeRemote;
	}
	
	public final void setMainFrameSpringComponent(MainFrameSpringComponent mainFrameSpringComponent) {
		this.mainFrameSpringComponent = mainFrameSpringComponent;
	}
	
	public MainFrame getMainFrame() {
		return mainFrameSpringComponent.getMainFrame();
	}
	
	public WorkspaceVO getWorkspace() {
		final WorkspaceVO result = mainFrameSpringComponent.getMainFrame().getWorkspace();
		if (result == null) {
			throw new NullPointerException();
		}
		return result;
	}

	private synchronized boolean restoreTab(WorkspaceDescription2.Tab wdTab, MainFrameTab tab, boolean onDemand) {
		try {
			final ClassLoader cl = LangUtils.getClassLoaderThatWorksForWebStart();
			Class<?> c = cl.loadClass(wdTab.getRestoreController());
			
			final ITabRestoreController restoreController;
			if (c.isAnnotationPresent(org.springframework.stereotype.Component.class)) {
				if (SpringApplicationContextHolder.containsBean(c)) {
					restoreController = (ITabRestoreController) SpringApplicationContextHolder.getBean(c);
				} else {
					restoreController = (ITabRestoreController) SpringApplicationSubContextsHolder.getInstance().getBean(c);
				}
			} else {
				restoreController = (ITabRestoreController) c.getConstructor().newInstance();
			}
			
			if (restoreController.validate(wdTab.getPreferencesXML())) {
				if (onDemand) {
					tab.setTabRestoreController(restoreController);
					tab.setTabRestorePreferencesXML(wdTab.getPreferencesXML());
				}
				else {
					restoreController.restoreFromPreferences(wdTab.getPreferencesXML(), tab);
				}
			} else {
				tab.setNeverClose(false);
				tab.setIgnoreClosableCheck(true);
				tab.close();
				return false;
			}
		}
		catch (RuntimeException e) {
			LOG.warn("TabRestoreController failed, disposing " + tab + ": " + e, e);
			tab.setNeverClose(false);
			tab.setIgnoreClosableCheck(true);
			tab.close();
		}
		catch (Exception e) {
			LOG.warn("TabRestoreController could not be created or restored", e);
			return false;
		}
		return true;
	}

	/**
	 *
	 * @param content
	 * @param frame
	 * @return
	 */
	private synchronized ContentRestorer createContentRestorer(WorkspaceDescription2.MutableContent content, JFrame frame) {
		if (content.getContent() instanceof WorkspaceDescription2.Split) {
			return new SplitRestorer((Split) content.getContent(), frame);
		} else if (content.getContent() instanceof WorkspaceDescription2.Tabbed) {
			return new TabbedRestorer((Tabbed) content.getContent(), frame);
		} else {
			throw new IllegalArgumentException(content.getClass().getName());
		}
	}

	/**
	 *
	 * @param wdFrame
	 */
	private synchronized void restoreFrame(final WorkspaceDescription2 wd, final WorkspaceDescription2.Frame wdFrame) {
		final MainFrame mainFrame = Main.getInstance().getMainFrame();
		final WorkspaceFrame wsFrame;
		
		if (wdFrame.isMainFrame()) {
			wsFrame = mainFrame;
		} else {
			wsFrame = MainFrame.createWorkspaceFrame();
		}

		final CommonJFrame frame = wsFrame.getFrame();


		final Rectangle recNormalBounds;
		final int iExtendedState;
		if (wdFrame.isMainFrame() && wd.isUseLastFrameSettings()) {
			Preferences prefs = Main.getInstance().getMainController().getMainFramePreferences();
			recNormalBounds = PreferencesUtils.getRectangle(prefs.node(PREFS_NODE_LAST_SETTINGS), NORMAL_BOUNDS, wdFrame.getNormalBounds().width, wdFrame.getNormalBounds().height);
			iExtendedState = prefs.node(PREFS_NODE_LAST_SETTINGS).getInt(EXTENDED_STATE, wdFrame.getExtendedState());
		} else {
			recNormalBounds = wdFrame.getNormalBounds();
			iExtendedState = wdFrame.getExtendedState();
		}

		boolean restoreAtStoredPosition = false;
		LOG.info("stored frame bounds: " + recNormalBounds + ", extended state: " + iExtendedState);

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] devices = ge.getScreenDevices();
		for (GraphicsDevice gd : devices) {
			if (restoreAtStoredPosition) {
				break;
			}

			GraphicsConfiguration[] gc = gd.getConfigurations();
			for (int i=0; i < gc.length && !restoreAtStoredPosition; i++) {
				Rectangle devBounds = gc[i].getBounds();
				Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(gc[i]);
				LOG.debug("device bounds: " + devBounds + ", insets: " + screenInsets);
				Rectangle testBounds = new Rectangle(devBounds);
				testBounds.x = devBounds.x + screenInsets.left;
				testBounds.y = devBounds.y + screenInsets.top;
				testBounds.width = testBounds.width - screenInsets.left - screenInsets.right;
				testBounds.height = testBounds.height - screenInsets.top - screenInsets.bottom;
				LOG.debug("device test bounds: " + testBounds);
				if (testBounds.contains(recNormalBounds)) {
					restoreAtStoredPosition = true;
					break;
				} else {
					// NUCLOS-5370 Esp. in Mac the upper "menuleiste" can make problems. So try with shifting down.
					if (testBounds.y > recNormalBounds.y) {
						int recNormalBoundSave = recNormalBounds.y;
						recNormalBounds.y = testBounds.y;

						if (testBounds.contains(recNormalBounds)) {
							// Shifting down worked well
							restoreAtStoredPosition = true;
							break;
						} else {
							// It didn't work, so restore old values
							recNormalBounds.y = recNormalBoundSave;
						}
					}
				}
			}
		}
		
		if (restoreAtStoredPosition) {
			LOG.info("restore at stored position");
			frame.setNormalBounds(recNormalBounds);
			frame.setBounds(recNormalBounds);
			frame.setExtendedState(iExtendedState);
		} else {
			GraphicsConfiguration defaultgc = ge.getDefaultScreenDevice().getDefaultConfiguration();
			Rectangle defaultBounds = defaultgc.getBounds();
			Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(defaultgc);
			defaultBounds.x = defaultBounds.x + screenInsets.left;
			defaultBounds.y = defaultBounds.y + screenInsets.top;
			defaultBounds.width = defaultBounds.width - screenInsets.left - screenInsets.right;
			defaultBounds.height = defaultBounds.height - screenInsets.top - screenInsets.bottom
					/*for Retina (Mac only?) displays:*/ -1;
			frame.setNormalBounds(defaultBounds);
			frame.setBounds(defaultBounds);
			
			LOG.info("restore on default screen");
			if (Toolkit.getDefaultToolkit().isFrameStateSupported(java.awt.Frame.MAXIMIZED_BOTH)) {
				frame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
			}
		} 

		if (wdFrame.isMainFrame()) {
			mainFrame.repositionSwitchingWorkspace();
			JPanel contentpane = (JPanel) mainFrame.getContentPane();
			contentpane.revalidate();
			contentpane.paintImmediately(0,0,contentpane.getSize().width,contentpane.getSize().height);
		}

		ContentRestorer cr = createContentRestorer(wdFrame.getContent(), frame);
		wsFrame.setFrameContent(cr.getEmptyContent());

		cr.restoreContent();
		
		frame.invalidate();
		frame.validate();
		frame.repaint();
		
		for (MainFrameTabbedPane mfTabbed : finishInitialisationTabbedPanes) {
			mfTabbed.finishInitiating();
		}

		MainFrame.updateTabbedPaneActions(frame);

		if (wdFrame.isMainFrame()) {
			mainFrame.showSwitchingWorkspace(false);
		} else {
			frame.setVisible(true);
		}
	}

	/**
	 * @throws CommonBusinessException
	 */
	public synchronized void restoreWorkspaceThreaded() throws CommonBusinessException {
		MutableBoolean newOneCreated = new MutableBoolean();
		
		WorkspaceVO wovoToRestore = getWorkspaceManager().getWorkspaceToRestore(preferencesFacadeRemote, newOneCreated);

		// if maintenancemode is on then switch to maintenance workspace
		if(MaintenanceDelegate.getInstance().isSuperUserAndMaintenanceModeOn()) {
			for(WorkspaceVO workspaceVO : getWorkspaceManager().getWorkspaces()) {
				if(workspaceVO!=null && workspaceVO.isMaintenance() != null && workspaceVO.isMaintenance()) {
					wovoToRestore = workspaceVO;
					break;
				}
			}
		}
		
		// getWorkspace legt einen Standard Workspace an, falls die Liste der Moeglichen leer ist (Neues System z.b.)
		// In dem Fall muessen wir die Leiste hier manuell aktualisieren
		if (newOneCreated.getValue()) {
			WorkspaceChooserController.getInstance().setupWorkspaces();
		}
		restoreWorkspaceThreaded(wovoToRestore);
	}

	/**
	 *
	 * @param name
	 * @param restoreToDefault
	 * @throws CommonBusinessException
	 */
	private synchronized void restoreWorkspaceThreaded(WorkspaceVO wovo) throws CommonBusinessException {
		checkRestoreRunning();
		
		final MainFrame mainFrame = mainFrameSpringComponent.getMainFrame();
		mainFrame.setWorkspaceManagementEnabled(false);
		cachedActions = Main.getInstance().getMainController().getGenericActions();

		//load from db. wovo contains header only
		WorkspaceDescription2 wd; 
		
		if (LOCALUSERWORKSPACECACHE.containsKey(wovo.getPrimaryKey())) {
			wovo = LOCALUSERWORKSPACECACHE.get(wovo.getPrimaryKey());
			wd = wovo.getWoDesc();
		} else {
			wd = getWorkspaceManager().loadWorkspaceFromDB(wovo, preferencesFacadeRemote);			
		}
		
		mainFrame.setWorkspace(wovo);
		

		threadList.clear();
		finishInitialisationTabbedPanes.clear();

		MainFrame.resetExternalFrameNumber();
		for (WorkspaceDescription2.Frame wdFrame : CollectionUtils.sorted(wd.getFrames(), new Comparator<WorkspaceDescription2.Frame>() {
			@Override
			public int compare(Frame o1, Frame o2) {
				return LangUtils.compare(o1.getNumber(), o2.getNumber());
			}
		})) {
			restoreFrame(wd, wdFrame);
		}

		// refresh menus for window list
		Main.getInstance().getMainController().setupMenuBar();

		final Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Main.getInstance().getMainFrame().hideProgress();
					mainFrame.setWorkspaceManagementEnabled(true);
					MainFrame.setActiveTabNavigation(MainFrame.getHomePane());
					threadList.clear();
				}
				catch (Exception e) {
					LOG.error("restoreWorkspaceThreaded failed: " + e, e);
				}
			}
		}, THREAD_NAME + "restoreWorkspaceThreaded");
		t.setDaemon(true);
		threadList.add(t);

		Main.getInstance().getMainFrame().showProgress(threadList.size());
		if (threadList.size() > 0) {
			threadList.get(0).start();
		}
	}

	private synchronized boolean storeTab(WorkspaceDescription2.Tabbed wdTabbed, MainFrameTab tab) {
		if (!tab.hadErrorsWhileOpening() && (tab.hasTabStoreController() || tab.hasTabRestoreController())) {
			final WorkspaceDescription2.Tab wdTab = new WorkspaceDescription2.Tab();
			wdTab.setLabel(tab.getTitle());
			wdTab.setIconResolver(tab.getTabIconResolver());
			wdTab.setIcon(tab.getTabIconName());
			wdTab.setNeverClose(tab.isNeverClose());
			wdTab.setFromAssigned(tab.isFromAssigned());
			wdTab.setPreferencesXML(tab.getTabRestorePreferencesXML());
			wdTab.setRestoreController(tab.getTabRestoreControllerClassName());
			wdTabbed.addTab(ClientMandatorContext.getMandatorUID(), wdTab);
			return true;
		} else {
			return false;
		}
	}

	private synchronized void storeTabbedPane(MutableContent content, MainFrameTabbedPane tabbedPane) {
		final WorkspaceDescription2.Tabbed wdTabbed = new WorkspaceDescription2.Tabbed();
		content.setContent(wdTabbed);

		wdTabbed.setHome(tabbedPane.isHome());
		wdTabbed.setHomeTree(tabbedPane.isHomeTree());
		wdTabbed.setNeverHideStartmenu(tabbedPane.isNeverHideStartmenu());
		wdTabbed.setNeverHideHistory(tabbedPane.isNeverHideHistory());
		wdTabbed.setNeverHideBookmark(tabbedPane.isNeverHideBookmark());
		wdTabbed.setAlwaysHideStartmenu(tabbedPane.isAlwaysHideStartmenu());
		wdTabbed.setAlwaysHideHistory(tabbedPane.isAlwaysHideHistory());
		wdTabbed.setAlwaysHideBookmark(tabbedPane.isAlwaysHideBookmark());
		wdTabbed.setShowAdministration(tabbedPane.isShowAdministration());
		wdTabbed.setShowConfiguration(tabbedPane.isShowConfiguration());
		wdTabbed.setShowEntity(tabbedPane.isShowEntity());
		wdTabbed.addAllPredefinedEntityOpenLocations(MainFrame.getPredefinedEntityOpenLocations(tabbedPane));
		wdTabbed.addAllReducedStartmenus(tabbedPane.getReducedStartmenus());
		wdTabbed.addAllReducedHistoryEntities(tabbedPane.getReducedHistoryEntities());
		wdTabbed.addAllReducedBookmarkEntities(tabbedPane.getReducedBookmarkEntities());
		wdTabbed.setDesktop(tabbedPane.getDesktop());
		wdTabbed.setDesktopActive(tabbedPane.isDesktopActive());
		wdTabbed.setHideStartTab(!tabbedPane.isStartTabVisible());
		
		Map<UID, Integer> allSelectedByMandator = tabbedPane.getAllSelectedByMandator();
		wdTabbed.setAllSelectedByMandator(allSelectedByMandator);
		wdTabbed.setSelected(ClientMandatorContext.getMandatorUID(), -1);
		
		Map<UID, List<WorkspaceDescription2.Tab>> allTabsByMandator = tabbedPane.getAllTabsByMandator();
		wdTabbed.setAllTabsByMandator(allTabsByMandator);
		wdTabbed.clearTabs(ClientMandatorContext.getMandatorUID());
		
		int tabOrder = 0;
		for (MainFrameTab tab : tabbedPane.getHiddenTabs()) {
			if (storeTab(wdTabbed, tab)) {
				tabOrder++;
			}
		}

		final int selected = tabbedPane.getSelectedIndex();
		for (int i = 0; i < tabbedPane.getTabCount(); i++) {
			if (tabbedPane.getComponentAt(i) instanceof MainFrameTab) {
				if (storeTab(wdTabbed, (MainFrameTab) tabbedPane.getComponentAt(i))) {
					if (selected == i) {
						wdTabbed.setSelected(ClientMandatorContext.getMandatorUID(), tabOrder);
					}
					tabOrder++;
				}
			}
		}
		
	}

	private synchronized void storeSplitPane(MutableContent content, MainFrameSplitPane splitPane) {
		final WorkspaceDescription2.Split wdSplit = new WorkspaceDescription2.Split();
		content.setContent(wdSplit);
		wdSplit.setHorizontal(splitPane.getOrientation()==JSplitPane.HORIZONTAL_SPLIT);
		wdSplit.setPosition(splitPane.getDividerLocation());
                wdSplit.setLastDividerLocation(splitPane.getLastDividerLocation());

            int dividerSize = splitPane.getDividerSize();
            if (wdSplit.isHorizontal()) {
                    int splitPaneWidth = splitPane.getSize().width;
                    int leftCompWidth = splitPane.getLeftComponent().getSize().width;
                    int rightCompWidth = splitPane.getRightComponent().getSize().width;
                    if (leftCompWidth + dividerSize == splitPaneWidth) {
                        wdSplit.setExpandedState(Split.EXPANDED_STATE_LEFT);
                    } else if (rightCompWidth + dividerSize == splitPaneWidth) {
                        wdSplit.setExpandedState(Split.EXPANDED_STATE_RIGHT);
                    } else {
                        wdSplit.setExpandedState(Split.EXPANDED_STATE_NONE);
                    }
		} else {
                    int splitPaneHeight = splitPane.getSize().height;
                    int topCompHeight = splitPane.getTopComponent().getSize().height;
                    int bottomCompHeight = splitPane.getBottomComponent().getSize().height;
                    if (topCompHeight + dividerSize == splitPaneHeight) {
                        wdSplit.setExpandedState(Split.EXPANDED_STATE_LEFT);
                    } else if (bottomCompHeight + dividerSize == splitPaneHeight) {
                        wdSplit.setExpandedState(Split.EXPANDED_STATE_RIGHT);
                    } else {
                        wdSplit.setExpandedState(Split.EXPANDED_STATE_NONE);
                    }
		}

		wdSplit.setFixedState(splitPane.getFixedState());
		storeContent(wdSplit.getContentA(), splitPane.getLeftComponent());
		storeContent(wdSplit.getContentB(), splitPane.getRightComponent());
	}

	private synchronized void storeContent(MutableContent content, Component comp) {
		if (comp instanceof MainFrameSplitPane) {
			storeSplitPane(content, (MainFrameSplitPane) comp);
		} else if (comp instanceof MainFrameTabbedPane.ComponentPanel) {
			storeTabbedPane(content, ((MainFrameTabbedPane.ComponentPanel) comp).getMainFrameTabbedPane());
		} else {
			throw new IllegalArgumentException(comp.toString());
		}
	}

	private synchronized void storeFrame(WorkspaceDescription2 wd, CommonJFrame frame) {
		if (frame instanceof WorkspaceFrame) {

			final WorkspaceDescription2.Frame wdFrame = new WorkspaceDescription2.Frame();
			wdFrame.setExtendedState(frame.getExtendedState());
			wdFrame.setNormalBounds(frame.getNormalBounds());
			wdFrame.setMainFrame(Main.getInstance().getMainFrame()==frame);
			wdFrame.setNumber(((WorkspaceFrame) frame).getNumber());
			storeContent(wdFrame.getContent(), ((WorkspaceFrame) frame).getFrameContent());

			wd.addFrame(wdFrame);

			if (Main.getInstance().getMainFrame()==frame) {
				Main.getInstance().getMainController().getMainFramePreferences().node(PREFS_NODE_LAST_SETTINGS).putInt(EXTENDED_STATE, frame.getExtendedState());
				PreferencesUtils.putRectangle(Main.getInstance().getMainController().getMainFramePreferences().node(PREFS_NODE_LAST_SETTINGS), NORMAL_BOUNDS, frame.getNormalBounds());
			}
		}
	}
	
	private static final Map<UID, WorkspaceVO> LOCALUSERWORKSPACECACHE = new HashMap<UID, WorkspaceVO>();

	public synchronized WorkspaceVO storeWorkspace(WorkspaceVO wovo, boolean bForcePersistence) throws PreferencesException {
		if (wovo == null) {
			return null;
		}

		if (isRestoreRunning()) {
			// do not store workspace if restore is running...
			return null;
		}

		WorkspaceDescription2 wd = wovo.getWoDesc();
		wd.getFrames().clear();

		Main.getInstance().getMainFrame().restoreAllTabbedPaneContainingArea();
		for (CommonJFrame frame : MainFrame.getOrderedFrames()) {
			storeFrame(wd, frame);
		}
		
		if (!bForcePersistence && !SecurityCache.getInstance().isSuperUser()) {
			LOG.info("STORING WORKSPACE INTO LOCAL USER WORKSPACE CACHE:" + wovo.getPrimaryKey());
			return LOCALUSERWORKSPACECACHE.put(wovo.getPrimaryKey(), wovo);
			
		} else if (!LOCALUSERWORKSPACECACHE.isEmpty()){
			LOG.info("EMPTYING WORKSPACES IN LOCAL USER WORKSPACE CACHE:" + LOCALUSERWORKSPACECACHE.size());
			for (UID wovoUid : LOCALUSERWORKSPACECACHE.keySet()) {
				if (!wovoUid.equals(wovo.getPrimaryKey())) {
					preferencesFacadeRemote.storeWorkspace(LOCALUSERWORKSPACECACHE.get(wovoUid));
				}
			}
			LOCALUSERWORKSPACECACHE.clear();
			
		}

		return preferencesFacadeRemote.storeWorkspace(wovo);
	}

	public synchronized void clearAndRestoreWorkspace(final WorkspaceVO wovo, final ResultListenerX<Boolean, CommonBusinessException> rl) {
		clearWorkspace(new ResultListener<Boolean>() {
			@Override
			public void done(Boolean result) {
				if (Boolean.TRUE.equals(result)) {
					try {
						restoreWorkspaceThreaded(wovo);
						rl.done(true, null);
					} catch (CommonBusinessException x) {
						rl.done(null, x);
					}
				} else {
					rl.done(false, null);
				}
			}
		});
	}

	public synchronized void clearAndRestoreToDefaultWorkspace(final ResultListenerX<WorkspaceVO, CommonBusinessException> rl) {
		clearAndRestoreToDefaultWorkspace(getWorkspaceManager().createDefaultWorkspace(), rl);
	}

	public synchronized void clearAndRestoreToDefaultWorkspace(WorkspaceDescription2 wd, final ResultListenerX<WorkspaceVO, CommonBusinessException> rl) {
		final WorkspaceVO wovo;
		try {
			wovo = getWorkspaceManager().createAndStoreDefaultWorkspace(wd, preferencesFacadeRemote);
		} catch (CommonBusinessException e) {
			rl.done(null, e);
			return;
		}
		
		clearWorkspace(new ResultListener<Boolean>() {
			@Override
			public void done(Boolean result) {
				if (Boolean.TRUE.equals(result)) {
					try {
						restoreWorkspaceThreaded(wovo);
					} catch (CommonBusinessException e) {
						rl.done(null, e);
						return;
					}

					if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_TIMELIMIT_LIST))
						Main.getInstance().getMainController().getTaskController().getTimelimitTaskController().cmdShowTimelimitTasks();

					Main.getInstance().getMainController().getExplorerController().cmdShowPersonalSearchFilters();

					//arrangeTabsOnDefaultWorkspace(removeTabsFromWorkspace());

					for (MainFrameTabbedPane tabbedPane : MainFrame.getAllTabbedPanes()) {
						if (tabbedPane.getTabCount() > 1) {
							tabbedPane.setSelectedIndex(1);
						}
					}
					
					rl.done(wovo, null);
				} else {
					preferencesFacadeRemote.removeWorkspace(wovo.getPrimaryKey());
					rl.done(null, null);
				}
			}
		});
	}

	public synchronized void closeTabs(final boolean notifyOnly, final ResultListener<Boolean> rl) {
		final List<MainFrameTab> allTabs = MainFrame.getAllTabs();
		if (allTabs.isEmpty()) {
			rl.done(true);
			return;
		}
		
		final Iterator<MainFrameTab> iter = allTabs.iterator();
		
		class Helper {
			public void next() {
				if (iter.hasNext()) {
					final MainFrameTab tab = iter.next();
					tab.notifyClosing(new ResultListener<Boolean>() {
						@Override
						public void done(Boolean result) {
							if (Boolean.TRUE.equals(result)) {
								if (!notifyOnly) {
									tab.notifyClosed();
								}
								new Helper().next();
							} else {
								rl.done(false);
							}
						}
					});
				} else {
					rl.done(true);
				}		
			}
		}
		
		new Helper().next();
	}

	public synchronized void clearWorkspace(final ResultListener<Boolean> rl) {
		checkRestoreRunning();

		closeTabs(false, new ResultListener<Boolean>() {
			@Override
			public void done(Boolean result) {
				if (Boolean.FALSE.equals(result)) {
					rl.done(false);
				} else {
					for (JFrame frame : new ArrayList<JFrame>(MainFrame.getAllFrames())) {
						if (frame instanceof ExternalFrame) {
							frame.dispose();
						} else if (frame instanceof MainFrame) {
							Main.getInstance().getMainFrame().showSwitchingWorkspace(true);
							((MainFrame) frame).clearFrame();
						}
					}

					for (MainFrameTabbedPane tabbedPane : new ArrayList<MainFrameTabbedPane>(MainFrame.getAllTabbedPanes())) {
						Main.getInstance().getMainFrame().removeTabbedPane(tabbedPane, true, false);
					}
					
					rl.done(true);
				}
			}
		});
	}

	private class TabbedRestorer implements ContentRestorer {

		WorkspaceDescription2.Tabbed wdTabbed;

		MainFrameTabbedPane result;

		public TabbedRestorer(WorkspaceDescription2.Tabbed wdTabbed, JFrame frame) {
			this.wdTabbed = wdTabbed;
			result = Main.getInstance().getMainFrame().createTabbedPane(frame);
		}

		@Override
		public Component getEmptyContent() {
			return result.getComponentPanel();
		}

		@Override
		public void restoreContent() {
			result.startInitiating();
			
			MainFrameTab toSelect = null;

			if (wdTabbed.isHome()) result.setHome();
			if (wdTabbed.isHomeTree()) result.setHomeTree();
			result.setNeverHideStartmenu(wdTabbed.isNeverHideStartmenu());
			result.setNeverHideHistory(wdTabbed.isNeverHideHistory());
			result.setNeverHideBookmark(wdTabbed.isNeverHideBookmark());
			result.setAlwaysHideStartmenu(wdTabbed.isAlwaysHideStartmenu());
			result.setAlwaysHideHistory(wdTabbed.isAlwaysHideHistory());
			result.setAlwaysHideBookmark(wdTabbed.isAlwaysHideBookmark());
			result.setShowAdministration(wdTabbed.isShowAdministration());
			result.setShowConfiguration(wdTabbed.isShowConfiguration());
			result.setShowEntity(wdTabbed.isShowEntity());
			for (UID entity : wdTabbed.getPredefinedEntityOpenLocations()) {
				MainFrame.setPredefinedEntityOpenLocation(entity, result);
			}
			if (wdTabbed.getReducedStartmenus() != null) {
				result.setReducedStartmenus(wdTabbed.getReducedStartmenus());
			}
			if (wdTabbed.getReducedHistoryEntities() != null) {
				result.setReducedHistoryEntities(wdTabbed.getReducedHistoryEntities());
			}
			if (wdTabbed.getReducedBookmarkEntities() != null) {
				result.setReducedBookmarkEntities(wdTabbed.getReducedBookmarkEntities());
			}
			result.setDesktop(wdTabbed.getDesktop(), cachedActions);
			result.setDesktopActive(wdTabbed.isDesktopActive());
			result.setStartTabVisible(!wdTabbed.isHideStartTab());
			
			result.setAllSelectedByMandator(wdTabbed.getAllSelectedByMandator());
			result.setAllTabsByMandator(wdTabbed.getAllTabsByMandator());
			
			final int selected = wdTabbed.getSelected(ClientMandatorContext.getMandatorUID());
			List<WorkspaceDescription2.Tab> tabs =  wdTabbed.getTabs(ClientMandatorContext.getMandatorUID());

			for (int i = 0; i < tabs.size(); i++) {
				final WorkspaceDescription2.Tab wdTab = tabs.get(i);
				final MainFrameTab tab = new MainFrameTab(wdTab.getLabel());
				tab.setTabIcon(wdTab.getIconResolver(), wdTab.getIcon());
				tab.setNeverClose(wdTab.isNeverClose());
				tab.setFromAssigned(wdTab.isFromAssigned());
				result.addTab(tab, false);

				if (selected == i) {
					toSelect = tab;
					tab.setNotifyRestore(true);
					
					final Thread t = new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								threadList.remove(0);
	
								UIUtils.runCommand(tab, new Runnable() {
									@Override
									public void run() {
										UIUtils.invokeOnDispatchThread(new Runnable() {
											@Override
											public void run() {
												try {
													if (restoreTab(wdTab, tab, false)) {
														tab.postAdd();
													} else {
														tab.setTabIconFromSystem("getIconTabNotRestored");
														tab.setTitle(wdTab.getLabel());
														// TODO TABS Show nice message in content "Tab konnte nicht wiederhergestellt werden. Möglicherweise existiert der Datensatz oder die Funktion nicht länger, oder Ihnen wurde die Berechtigung entzogen."
													}
												}
												catch (Exception e) {
													LOG.error("restoreContent failed: " + e, e);
												}
											}
										});
									}
								});
	
								Main.getInstance().getMainFrame().continueProgress();
	
								if (threadList.size() > 0) {
									threadList.get(0).start();
								}
							}
							catch (Exception e) {
								LOG.error("restoreContent failed: " + e, e);
							}
						}
					}, THREAD_NAME + "restoreContent");
					t.setDaemon(true);
					tab.addMainFrameTabListener(new MainFrameTabAdapter() {
						@Override
						public void tabClosing(MainFrameTab tab, ResultListener<Boolean> rl) {
							if (!t.isAlive()) {
								threadList.remove(t);
								Main.getInstance().getMainFrame().continueProgress();
							}
							tab.removeMainFrameTabListener(this);
							rl.done(true);
						}
					});
	
//					if (selected == i) {
//						toSelect = tab;
						threadList.add(0, t);
//					} else {
//						threadList.add(t);
//					}

				} else {
					// restores on demand...
					restoreTab(wdTab, tab, true);
					Main.getInstance().getMainFrame().continueProgress();
				}
				
			}
			
			final MainFrameTab selectLater = toSelect;
//			SwingUtilities.invokeLater(new Runnable() {
//				@Override
//				public void run() {
					try {
						finishInitialisationTabbedPanes.add(result);
//						result.finishInitiating();
						if (selectLater != null && wdTabbed.getSelected(ClientMandatorContext.getMandatorUID()) >= 0) {
							result.setSelectedComponent(selectLater, false);
						}
					} catch(IllegalArgumentException e) {
						// may be not all tabs are restoredaubspl
						LOG.info("restoreContent: " + e);
					}
//				}
//			});
		}
	}

	private class SplitRestorer implements ContentRestorer {

		WorkspaceDescription2.Split wdSplit;
		ContentRestorer crA;
		ContentRestorer crB;

		MainFrameSplitPane result;

		public SplitRestorer(WorkspaceDescription2.Split wdSplit, JFrame frame) {
			this.wdSplit = wdSplit;
			crA = createContentRestorer(wdSplit.getContentA(), frame);
			crB = createContentRestorer(wdSplit.getContentB(), frame);
			result = new MainFrameSplitPane(wdSplit.isHorizontal()? JSplitPane.HORIZONTAL_SPLIT : JSplitPane.VERTICAL_SPLIT,
				MainFrame.SPLIT_CONTINUOS_LAYOUT,
				crA.getEmptyContent(), crB.getEmptyContent());
		}

		@Override
		public Component getEmptyContent() {
			return result;
		}

		@Override
		public void restoreContent() {
			result.setOneTouchExpandable(MainFrame.SPLIT_ONE_TOUCH_EXPANDABLE);
			crA.restoreContent();
			crB.restoreContent();

			final int expandedState = wdSplit.getExpandedState();


			if (expandedState == Split.EXPANDED_STATE_NONE) {
				// not expanded: set position hard:
				result.setDividerLocation(wdSplit.getPosition());
				// resize expands right/bottom content:
				result.setResizeWeight(0.0);

			} else {
				// apply expand/collapsed only if the state is not fixed.
				if (wdSplit.getFixedState() == Split.FIXED_STATE_NONE) {

					// one side is collapsed:
					result.addAncestorListener(new AncestorListener() {
						@Override
						public void ancestorAdded(final AncestorEvent event) {
							if (expandedState == Split.EXPANDED_STATE_LEFT) {
								result.setDividerLocation(1.0);
							} else if (expandedState == Split.EXPANDED_STATE_RIGHT) {
								result.setDividerLocation(0.0);
							}
							try {
								Field m = BasicSplitPaneUI.class.getDeclaredField("keepHidden");
								m.setAccessible(true);
								m.set(result.getUI(), true);
								if (wdSplit.getLastDividerLocation() > 0) {
									result.setLastDividerLocation(wdSplit.getLastDividerLocation());
								}
							} catch (NoSuchFieldException | IllegalAccessException e) {
								Errors.getInstance().showExceptionDialog(new SimpleExceptionForDisplay(e, false, false, ESeverity.FATAL, e.getMessage()));
							}

							result.removeAncestorListener(this);
						}

						@Override
						public void ancestorRemoved(final AncestorEvent event) {
							//do nothing
						}

						@Override
						public void ancestorMoved(final AncestorEvent event) {
							//do nothing
						}
					});
					// also sets divider size, and resize weight:
					result.setFixedState(wdSplit.getFixedState());
				}
			}
		}
	}
	
	private interface ContentRestorer {
		
		Component getEmptyContent();
		
		void restoreContent();
	}

	private synchronized void checkRestoreRunning() {
		if (isRestoreRunning())
			throw new IllegalArgumentException("Workspace Restore is running");
	}

	public synchronized boolean isRestoreRunning() {
		return !threadList.isEmpty();
	}

}
