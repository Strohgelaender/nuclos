//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.main;

import java.awt.*;
import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;

import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.jnlp.SingleInstanceService;
import javax.jnlp.UnavailableServiceException;
import javax.swing.*;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.nuclos.client.LaunchListener;
import org.nuclos.client.launch.LauncherConnection;
import org.nuclos.client.launch.NuclosLaunchListener;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.startup.JWSClassLoaderFix;
import org.nuclos.client.startup.JarSignersHardLinker;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.SpringApplicationSubContextsHolder;
import org.nuclos.common.preferences.NuclosPreferencesFactory;
import org.nuclos.common.startup.NuclosEnviromentConstants;

/**
 * The main class of the Nucleus client. Contains some global constants and objects.
 * Provides access to the main frame and its controller.
 * Keep this class as small as possible.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class);
	
	private static final TimeZone initialTimeZone = TimeZone.getDefault();
	
	private static final boolean MAC_OSX = System.getProperty("os.name").toLowerCase().startsWith("mac os x");

	private static final boolean WINDOWS = System.getProperty("os.name").toLowerCase().startsWith("win");
	
	private static final boolean LINUX = System.getProperty("os.name").toLowerCase().startsWith("linux");
	
	public static final String appName = "Nuclos";
	
	private static final Main INSTANCE = new Main();
	
	/**
	 * Disable following redirections cause of bug with security dialogs, otherwise Client freezes when dialog appears from EventDispatcherThread.
	 * Only the security manager from webstart will bring up this dialogs (see Sun Bug #7177349) 
	 */
	public static final boolean HTTP_CONNECTION_FOLLOW_REDIRECTIONS_ENABLED = false;

	/**
	 * @deprecated Workaround
	 * @return Initial vm timezone.
	 */
	@Deprecated
	public static TimeZone getInitialTimeZone() {
		return initialTimeZone;
	}

	/**
	 * (the only!) exit point of the application. Does some cleanup and finally
	 * calls <code>System.exit(iResult)</code>.
	 * todo this is public only to be used in JFCUnit!
	 */
	public void exit(ExitResult exitresult) {
		try {
			final JWSSingleInstanceListener sil = singleInstance.getAndSet(null);
			if (sil != null) {
				try {
					final SingleInstanceService siService = (SingleInstanceService) ServiceManager
							.lookup("javax.jnlp.SingleInstanceService");
					siService.removeSingleInstanceListener(sil);
				} catch (UnavailableServiceException e) {
					LOG.warn("Unregister JWS single instance failed: " + e);
				}
			}
			System.runFinalization();
		}
		finally {
			System.exit(exitresult.ordinal());
		}
	}

	//

	public static enum ExitResult {
		NORMAL,
		LOGIN_FAILED,
		ABNORMAL
	}

	/**
	 * the controller for the main frame.
	 */
	private MainController maincontroller;
	
	private AtomicReference<JWSSingleInstanceListener> singleInstance = new AtomicReference<JWSSingleInstanceListener>();

	/**
	 * creates an empty main object.
	 */
	private Main() {
	}
	
	public static Main getInstance() {
		return INSTANCE;
	}

	void setMainController(MainController maincontroller) {
		this.maincontroller = maincontroller;
	}

	public MainController getMainController() {
		if (maincontroller == null) throw new NullPointerException("too early");
		return maincontroller;
	}

	/**
	 * §todo this shouldn't be a singleton. Use child window as parameter!
	 * @return the <code>MainFrame</code> of this application, if any.
	 * 
	 * @deprecated Use MainFrame Spring injection via MainFrameSpringComponent. (tp)
	 */
	public MainFrame getMainFrame() {
		for (Frame frm : JFrame.getFrames()) {
			if (frm instanceof MainFrame) {
				return (MainFrame) frm;
			}
		}
		return null;
	}

	/**
	 * the starting point of the Nucleus client
	 * @param asArgs
	 */
	public static void main(String[] asArgs) throws Exception {
		final Main main = Main.getInstance();
		/*
		 * Misc java web start links:
		 * http://docs.oracle.com/javase/tutorial/deployment/deploymentInDepth/deployingWithoutCodebase.html
		 * http://lopica.sourceforge.net/faq.html
		 * https://netbeans.org/kb/docs/java/javase-jws.html
		 * http://docs.oracle.com/javase/7/docs/technotes/guides/javaws/developersguide/syntax.html
		 * http://docs.oracle.com/javase/7/docs/technotes/guides/jweb/riaJnlpProps.html
		 */
		
		// Before rendering any graphical output, need to set this system property on linux
		if (isLinux()) {
		    System.setProperty("sun.java2d.xrender", "false");
		}

		JWSClassLoaderFix.quickAndDirtyFixForProblemWithWebStartInJava7u25();
		// http://support.nuclos.de/browse/NUCLOS-1746
		JarSignersHardLinker.go();
		
		// for Mac OS X ...
		//System.setProperty("-Xdock:name", appName);
		System.setProperty("com.apple.mrj.application.apple.menu.about.name", appName);
		System.setProperty("java.util.prefs.PreferencesFactory", NuclosPreferencesFactory.class.getName());
		StartUp.setupLookAndFeel(null);
		LOG.info("Starting splash ...");
		SimpleSplash splash = new SimpleSplash();
		// set the PreferencesFactory:

		if (isMacOSX()) {
			try {
				// check if java 1.6.0_20+ (Java for Mac OS X 10.6, Update 3+ | Java for Mac OS X 10.5, Update 8+)
				String runtimeVersion = System.getProperty("java.version");
				String[] fragments1 = runtimeVersion.split("_"); //e.g. "1.6.0", "20"
				String[] fragments2 = fragments1[0].split("\\."); //e.g. "1", "6", "0"

		        int minorVers = Integer.parseInt(fragments2[1]);
		        if (minorVers < 6) {
		        		JOptionPane.showMessageDialog(null,
		        			Main.isMacOSXSnowLeopardOrBetter() ?
		        				"Nuclos Client requires Java for Mac OS X 10.6, Update 3+" :
		        					"Nuclos Client requires Java for Mac OS X 10.5, Update 8+");
		        		main.exit(Main.ExitResult.ABNORMAL);
		        } else if (minorVers == 6) {
		        		int bugfixVers = Integer.parseInt(fragments1[1]);
		        		if (bugfixVers < 20) {
		        			JOptionPane.showMessageDialog(null,
		        				Main.isMacOSXSnowLeopardOrBetter() ?
		        				"Nuclos Client requires Java for Mac OS X 10.6, Update 3+" :
		        					"Nuclos Client requires Java for Mac OS X 10.5, Update 8+");
			        		main.exit(Main.ExitResult.ABNORMAL);
		        		}
		        }
			} catch (Exception e) {
				LOG.fatal("main failed: " + e, e);
				ErrorInfo ei = new ErrorInfo("Fatal Error", e.getMessage(), null, null, e, null, null);
				JXErrorPane.showDialog(null, ei);
				main.exit(Main.ExitResult.ABNORMAL);
			}
		} 
		
		final StringBuilder msg = new StringBuilder();
		try {
			msg.append("java version: ").append(System.getProperty("java.version")).append("\n");
			msg.append("java vendor: ").append(System.getProperty("java.vendor")).append("\n");
			final BasicService bService = (BasicService) ServiceManager.lookup("javax.jnlp.BasicService");
			final URL codebase = bService.getCodeBase();
			final URL clientProperties = new URL(codebase, "client.properties");
			processClientProperties(clientProperties, msg);

			if ("true".equals(System.getProperty("nuclos.client.singleinstance"))) {
				// register instance as single instance
				final SingleInstanceService siService = (SingleInstanceService) ServiceManager
						.lookup("javax.jnlp.SingleInstanceService");
				siService.addSingleInstanceListener(JWSSingleInstanceListener.getInstance());
				main.singleInstance.set(JWSSingleInstanceListener.getInstance());
				Runtime.getRuntime().addShutdownHook(new Thread() {
					
					@Override
					public void run() {
						final JWSSingleInstanceListener sil = main.singleInstance.getAndSet(null);
						if (sil != null) {
							try {
								final SingleInstanceService siService = (SingleInstanceService) ServiceManager
										.lookup("javax.jnlp.SingleInstanceService");
								siService.removeSingleInstanceListener(sil);
							} catch (UnavailableServiceException e) {
								LOG.warn("Unregister JWS single instance failed: " + e);
							}
						}
					}
				});
				msg.append("Client started in single instance mode.\n");
			}
		} catch (UnavailableServiceException e) {
			msg.append("Client cannot be started in single instance mode because there is no webstart context available: ");
			msg.append(e).append(".\n");
			loadClientPropertiesWithoutJWS(msg);
		} catch (MalformedURLException e) {
			msg.append("Client cannot be started in single instance mode because client.properties are not found: ");
			msg.append(e).append(".\n");
			loadClientPropertiesWithoutJWS(msg);
		} catch (IOException e) {
			msg.append("Client cannot be started in single instance mode because client.properties could not be loaded: ");
			msg.append(e).append(".\n");
			loadClientPropertiesWithoutJWS(msg);
		} catch (Exception e) {
			msg.append("Client cannot be started in single instance mode because of unexpected exception: ");
			msg.append(e).append(".\n");
			loadClientPropertiesWithoutJWS(msg);
		} finally {			
			// Ok! (tp)
			final String s = msg.toString();
			System.out.println(s);
			LOG.info(s);
		}

		checkLauncher();

		HttpURLConnection.setFollowRedirects(HTTP_CONNECTION_FOLLOW_REDIRECTIONS_ENABLED);
		try {
			final StartUp startUp = new StartUp(asArgs);
			startUp.init(splash);
		} catch (Exception e) {
			LOG.fatal("main failed: " + e, e);
			ErrorInfo ei = new ErrorInfo("Fatal Error", e.getMessage(), null, null, e, null, null);
			JXErrorPane.showDialog(null, ei);
			main.exit(ExitResult.ABNORMAL);
		}
	}

	private static void checkLauncher() {
		if (System.getProperty("launcher.port") != null && System.getProperty("launcher.clientId") != null) {
			final int launcherPort = Integer.parseInt(System.getProperty("launcher.port"));
			final BigInteger clientId = new BigInteger(System.getProperty("launcher.clientId"));

			try {
				startLauncherConnection(launcherPort, clientId);
			} catch (Exception e) {
				LOG.error("Failed to init launcher connection", e);
			}
		}
	}

	private static void startLauncherConnection(final int launcherPort, final BigInteger clientId) throws IOException {
		LauncherConnection launcherConnection = new LauncherConnection(launcherPort, clientId);

		new Thread(launcherConnection).start();
	}

	private static void loadClientPropertiesWithoutJWS(StringBuilder msg) throws IOException {
		final String server = System.getProperty(NuclosEnviromentConstants.SERVER_VARIABLE);
		if (server != null) {
			final URL base = new URL(server);
			final URL clientProperties = new URL(base, "app/client.properties");
			try {
				processClientProperties(clientProperties, msg);
			} catch (IOException e) {
				// no JWS: client.properties possible does not exist.
				msg.append("Unable to load client.properties on no-JWS start: " + e);
			}
		} else {
			msg.append("-Dserver=... is NOT set and not starting with Java Web Start. This is a SEVERE problem!");
		}
	}
	
	private static void processClientProperties(URL clientProperties, StringBuilder msg) throws IOException {
		final Properties props = new Properties();
		props.load(clientProperties.openStream());
		msg.append("client.properties loaded from ").append(clientProperties).append(":\n");
		for (Object key: props.keySet()) {
			final Object value = props.get(key);
			msg.append(key).append("=").append(value).append("\n");
			System.setProperty((String) key, (String) value);
		}
	}

	static void notifyListeners(String[] args) {
		final HashMap<String, String> parsedArgs = parseArguments(args);
		LOG.info("parsed arguments: " + parsedArgs);

		notifyListeners(parsedArgs);
	}

	public static void notifyListeners(final Map<String, String> parsedArgs) {

		if (parsedArgs != null && parsedArgs.size() > 0) {
			LOG.info("Client launched with arguments " + parsedArgs);
		} else {
			LOG.info("Client launched without arguments.");
		}

		final Map<String, LaunchListener> listeners = SpringApplicationSubContextsHolder.getInstance().getBeansOfType(LaunchListener.class);
		final String nuclosLaunchListener = "nuclosLaunchListener";
		LaunchListener defaultLaunchListener = SpringApplicationContextHolder.getBean(NuclosLaunchListener.class);
		if (defaultLaunchListener != null) {
			listeners.put(nuclosLaunchListener, defaultLaunchListener);
		}

		boolean disableNuclosLaunchListener = true;
		if (listeners.size() == 1 && listeners.containsKey(nuclosLaunchListener)) {
			// if there exists only one launch listener and the existing one is the nuclosLaunchListener enable it
			disableNuclosLaunchListener = false;
		}
		for (final String lname: listeners.keySet()) {
			
			// necessary if any extension defines its own launch listener
			if (disableNuclosLaunchListener) {
				LOG.info("Notify LaunchListener " + lname + " was ignored");
				if (nuclosLaunchListener.equals(lname)) {
					continue;
				}
			}
			
			LOG.info("Notify LaunchListener " + lname);
			final LaunchListener listener = listeners.get(lname);
			try {
				// defensive copy
				HashMap<String, String> copy = new HashMap<>(parsedArgs);
				listener.launched(copy);
			}
			catch (Exception t) {
				LOG.error("Exception in LaunchListener" + lname, t);
			}
		}
	}

	static HashMap<String, String> parseArguments(String[] args) {
		final HashMap<String, String> params = new HashMap<String, String>();
		for (String arg : args) {
			if (arg.contains("=")) {
				params.put(arg.substring(0, arg.indexOf("=")), arg.substring(arg.indexOf("=") + 1));
			}
			else {
				params.put(arg, null);
			}
		}
		return params;
	}

	public static boolean isMacOSX() {
		return MAC_OSX;
	}

	public static boolean isWindows() {
		return WINDOWS;
	}
	
	public static boolean isLinux() {
        return LINUX;
    }
	
	public static boolean isMacOSXSnowLeopardOrBetter() {
		return isMacOSXVersionOrBetter(6);
	}
	
	public static boolean isMacOSXLionOrBetter() {
	    return isMacOSXVersionOrBetter(7);
	}
	
	private static boolean isMacOSXVersionOrBetter(int version) {
	    if (!isMacOSX()) return false;

	    // split the "10.x.y" version number
	    String osVersion = System.getProperty("os.version");
	    String[] fragments = osVersion.split("\\.");

	    // sanity check the "10." part of the version
	    if (!fragments[0].equals("10")) return false;
	    if (fragments.length < 2) return false;

	    // check if Mac OS X 10.version
	    try {
	        int minorVers = Integer.parseInt(fragments[1]);
	        if (minorVers >= version) return true;
	    } catch (NumberFormatException e) {
	        // was not an integer
	    }

	    return false;
	}

	/**
	 *
	 * @param menuShortcutKey
	 * 			see Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()
	 * 		Do not use CTRL, Mac uses CMD as default
	 * @param shiftKey
	 * @param altKey
	 * @param key
	 * @param locale
	 * @return
	 */
	public static String getKeyboardShortcutDescription(boolean menuShortcutKey, boolean shiftKey, boolean altKey, String key, Locale locale) {
		StringBuilder result = new StringBuilder();
		if (menuShortcutKey) {
			if (isMacOSX()) {
				result.append("\u2318 ");
			} else {
				if (locale.getLanguage().equals(new Locale("de").getLanguage())) {
					result.append("Strg + ");
				} else {
					result.append("Ctrl + ");
				}
			}
		}
		if (shiftKey) {
			if (isMacOSX()) {
				result.append("\u21E7 ");
			} else {
				result.append("Shift + ");
			}
		}
		if (altKey) {
			if (isMacOSX()) {
				result.append("\u2325 ");
			} else {
				result.append("Alt + ");
			}
		}
		result.append(key.toUpperCase());
		return result.toString();
	}

}	// class Main
