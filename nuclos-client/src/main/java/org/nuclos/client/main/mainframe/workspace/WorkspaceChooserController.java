//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.main.mainframe.workspace;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.IllegalComponentStateException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.TransferHandler;

import org.apache.log4j.Logger;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.command.ResultListenerX;
import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.livesearch.LiveSearchController;
import org.nuclos.client.main.ActionWithMenuPath;
import org.nuclos.client.main.GenericAction;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.ui.*;
import org.nuclos.client.ui.model.ChoiceList;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.LafParameter;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescriptionDefaultsFactory;
import org.nuclos.common.WorkspaceParameter;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.preferences.WorkspaceManager;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;
import org.nuclos.server.maintenance.MaintenanceConstants;
import org.nuclos.server.maintenance.MaintenanceFacadeRemote;
import org.springframework.beans.factory.InitializingBean;

// @Component
public class WorkspaceChooserController implements InitializingBean {
	
	private static final Logger LOG = Logger.getLogger(WorkspaceChooserController.class); 
	
	public static final int ICON_SIZE = 16;
	private static final WorkspacePanel workspacePanel = new WorkspacePanel();
	private static final ContentPanel contentPanel = new ContentPanel();
	
	//	
	private static WorkspaceChooserController INSTANCE;
	private static WorkspaceManager _workspaceManager = null;
	private static WorkspaceManager getWorkspaceManager() {
		if (_workspaceManager == null) {
			_workspaceManager = WorkspaceManager.INSTANCE(SpringLocaleDelegate.getInstance());
		}
		return _workspaceManager;
	}
	
	// 
	
	private JLabel jlbLeft;
	private JLabel jlbRight;

	private boolean enabled;
	
	
	// Spring injection
	
	private PreferencesFacadeRemote preferencesFacadeRemote;
	
	private MaintenanceFacadeRemote maintenanceFacadeRemote;

	private RestoreUtils restoreUtils;
	
	// end of Spring injection
	
	WorkspaceChooserController() {
		INSTANCE = this;
		// initWorkspaceChooser();
	}
	
	public static WorkspaceChooserController getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}
	
	@Override
	public final void afterPropertiesSet() {
		contentPanel.setOpaque(false);
		contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 1, 5));
		
		workspacePanel.setOpaque(false);
		
		jlbLeft = new JLabel(Icons.getInstance().getWorkspaceChooser_left());
		jlbRight = new JLabel(Icons.getInstance().getWorkspaceChooser_right());
		jlbLeft.setBorder(BorderFactory.createEmptyBorder());
		jlbRight.setBorder(BorderFactory.createEmptyBorder());
		contentPanel.add(jlbLeft);
		contentPanel.add(workspacePanel);
		contentPanel.add(jlbRight);
		workspacePanel.setVisible(false);
	}
	
	// @Autowired
	public final void setPreferencesFacadeRemote(PreferencesFacadeRemote preferencesFacadeRemote) {
		this.preferencesFacadeRemote = preferencesFacadeRemote;
	}
	
	// @Autowired
	public final void setRestoreUtils(RestoreUtils restoreUtils) {
		this.restoreUtils = restoreUtils;
	}
	
	// @Autowired
	public void setMaintenanceFacadeRemote(MaintenanceFacadeRemote maintenanceFacadeRemote) {
		this.maintenanceFacadeRemote = maintenanceFacadeRemote;
	}
	
	/**
	 * 
	 */
	public void setupWorkspaces() {
		
		getWorkspaceManager().setup(preferencesFacadeRemote);
		refreshWorkspaces();
		
		int countVisibleWorkspaces = 0;
		for (WorkspaceVO wovo : getWorkspaceManager().getWorkspaces()) {
			if (!wovo.getWoDesc().isHide()) {
				countVisibleWorkspaces++;
			}
		}
		final boolean visible = countVisibleWorkspaces > 1 || SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_CREATE_NEW);
		workspacePanel.setVisible(visible);
		jlbLeft.setVisible(visible);
		jlbRight.setVisible(visible);
	}
	
	/**
	 * 
	 */
	private void refreshWorkspaces() {
		workspacePanel.removeAll();
		
		final List<WorkspaceVO> hidden = new ArrayList<WorkspaceVO>();
		for (final WorkspaceVO wovo : getWorkspaceManager().getWorkspaces()) {
			
			if (wovo.getWoDesc().isHide()) {
				if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN)) {
					hidden.add(wovo);
				}
			} else {
				final WorkspaceLabel wl = new WorkspaceLabel(wovo);
				wl.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						if (enabled && SwingUtilities.isLeftMouseButton(e) && !wl.isSelected()) {
							restoreWorkspace(wl.getWorkspaceVO());
						}
					}
				});
				setupContextMenu(wl);
				workspacePanel.add(wl);
			}
		}
		
		if (!hidden.isEmpty()) {
			final Label lbHidden = new Label(" + ", 
					null, //MainFrame.resizeAndCacheIcon(NuclosResourceCache.getNuclosResourceIcon("org.nuclos.client.resource.icon.glyphish-blue.174-imac.png"), ICON_SIZE)
					SwingConstants.LEFT) {
				@Override
				boolean isSelected() {
					return false;
				}
				@Override
				boolean isMaintenanceModeSelected() {
					return false;
				}
				@Override
				boolean drawDogEar() {
					return false;
				}
			};
			lbHidden.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					UIUtils.runCommand(Main.getInstance().getMainFrame(), new Runnable() {
						
						@Override
						public void run() {
							if (enabled) {
								final JPopupMenu popup = new JPopupMenu();
								for (WorkspaceVO hiddenWovo : CollectionUtils.sorted(hidden,
										new Comparator<WorkspaceVO>() {
											@Override
											public int compare(WorkspaceVO o1, WorkspaceVO o2) {
												return StringUtils.emptyIfNull(o1.getName()).compareToIgnoreCase(o2.getName());
											}
										})) {
									final JMenu menuHiddenWorkspace = new JMenu(hiddenWovo.getName());
									addMenuItems(new MenuItemContainer() {
										@Override
										public void addSeparator() {
											menuHiddenWorkspace.addSeparator();
										}
										@Override
										public void addLabel(String label) {
											menuHiddenWorkspace.add(new JLabel(label));
										}
										@Override
										public void add(JMenuItem mi) {
											menuHiddenWorkspace.add(mi);
										}
									}, hiddenWovo, lbHidden);
									popup.add(menuHiddenWorkspace);
								}
								popup.show(lbHidden, 10, 10);
							}
						}
					});
				}
			});
			workspacePanel.add(lbHidden);
		}
		
		contentPanel.invalidate();
		contentPanel.revalidate();
		contentPanel.repaint();
	}
	
	private void addWorkspace(WorkspaceVO wovo, boolean select) {
		if (wovo != null) {
			// add header only
			wovo.getWoDesc().removeAllEntityPreferences();
			wovo.getWoDesc().getFrames().clear();
			
			getWorkspaceManager().add(wovo);
			
			refreshWorkspaces();
			if (select)
				setSelectedWorkspace(wovo);
		}
	}
	
	public List<WorkspaceVO> getWorkspaceHeaders() {
		return getWorkspaceManager().getWorkspaceHeaders();
	}
	
	public void addGenericActions(List<GenericAction> genericActions) {
		if (genericActions != null) {
			for (final WorkspaceVO wovo : getWorkspaceManager().getWorkspaces()) {
				final Action action = new AbstractAction(wovo.getName(), getWorkspaceIcon(wovo, MainFrame.TAB_CONTENT_ICON_MAX)) {
					@Override
					public void actionPerformed(ActionEvent e) {
						restoreWorkspace(wovo);
					}
				};
				WorkspaceDescription2.Action wa = new WorkspaceDescription2.Action();
				wa.setAction(MainController.GENERIC_RESTORE_WORKSPACE_ACTION);
				wa.putStringParameter("workspace", (String) action.getValue(Action.NAME));
				wa.putBooleanParameter("assigned", wovo.isAssigned());
				genericActions.add(new GenericAction(wa, new ActionWithMenuPath(
						new String[]{
								SpringLocaleDelegate.getInstance().getMessage("nuclos.entity.workspace.label", "Arbeitsumgebung")
						}, action)));
			}
		}
	}
	
	private static class MaintenanceModePanel extends BackgroundPanel {
		
		private static final long serialVersionUID = 1L;
		private final JPanel pnlLogin = new JPanel();
		private final JProgressBar progressbar = new JProgressBar();
		private final JTextArea descriptionLabel = new JTextArea();
		private final JLabel userStatusLabel = new JLabel();
		private final JLabel userStatusValue = new JLabel();
		private final JLabel jobStatusLabel = new JLabel();
		private final JLabel jobStatusValue = new JLabel();

		public MaintenanceModePanel() {
			this.add(pnlLogin, BorderLayout.CENTER);

			pnlLogin.setLayout(new GridBagLayout());
			pnlLogin.setOpaque(false);
			pnlLogin.setBorder(BorderFactory.createEmptyBorder(1, 10, 1, 10));

			descriptionLabel.setText("Der Wartungsmodus ist aktiv, sobald alle laufenden Jobs beendet sind\n"
									+ "und keine weiteren Benutzer mehr eingelogt sind. Sollte der Wechsel\n"
									+ "nicht stattfinden, brechen Sie ab und probieren Sie es nochmals!");
			descriptionLabel.setBackground(pnlLogin.getBackground());
			descriptionLabel.setEditable(false);
			
			pnlLogin.add(descriptionLabel, new GridBagConstraints(0, 1, 3, 1, 0.0,
					0.0, GridBagConstraints.WEST,
					GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
					0));

			pnlLogin.add(userStatusLabel, new GridBagConstraints(0, 2, 2, 1, 0.0,
					0.0, GridBagConstraints.WEST,
					GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
					0));

			pnlLogin.add(userStatusValue, new GridBagConstraints(2, 2, 2, 1, 0.0,
					0.0, GridBagConstraints.WEST,
					GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
					0));

			pnlLogin.add(jobStatusLabel, new GridBagConstraints(0, 3, 2, 1, 0.0,
					0.0, GridBagConstraints.WEST,
					GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
					0));

			pnlLogin.add(jobStatusValue, new GridBagConstraints(2, 3, 2, 1, 0.0,
					0.0, GridBagConstraints.WEST,
					GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
					0));

			pnlLogin.add(progressbar, new GridBagConstraints(0, 4, 3, 1, 0.0,
					0.0, GridBagConstraints.WEST,
					GridBagConstraints.HORIZONTAL, new Insets(0, 0, 10, 0), 0,
					0));

			userStatusLabel.setText("Offene Benutzer Sessions: ");

			jobStatusLabel.setText("Laufende Jobs: ");
		}
		
	}

	private void restoreWorkspace(final WorkspaceVO wovo) {

		if(MaintenanceDelegate.getInstance().isSuperUserAndMaintenanceModeInitialized()) {
			showMaintenanceInitDialog(Main.getInstance().getMainFrame());
		}		
		
		if (wovo != null && !wovo.equals(getSelectedWorkspace())) {
			
			final MainFrame mf = Main.getInstance().getMainFrame();

			if (SecurityCache.getInstance().isSuperUser() || SecurityCache.getInstance().isMaintenanceUser()) {
				if(wovo.isMaintenance()) {
					if (MaintenanceDelegate.getInstance().isMaintenanceModeOff()) {
						Object[] options = {SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.21", "Ja, andere User ausloggen"), SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.22", "Nein")};
						if(JOptionPane.OK_OPTION != JOptionPane.showOptionDialog(
								mf, 
								SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.17", "Wollen Sie wirklich in den Wartungsmodus wechseln?"),
								SpringLocaleDelegate.getInstance().getMessage("maintenancemode", "Wartungsmodus"),
								JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1])
								) {
							return;
						} else {
							
							// initialize maintenance mode
							new Thread(() -> maintenanceFacadeRemote.enterMaintenanceMode(SecurityCache.getInstance().getUsername())).start();
		
							showMaintenanceInitDialog(mf);
							if (!MaintenanceConstants.MAINTENANCE_MODE_ON.equals(maintenanceFacadeRemote.getMaintenanceMode())) {
								JOptionPane.showMessageDialog(mf, 
										SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.20", "Wartungsmodus abgebrochen!"), 
										SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.19", "Zeitüberschreitung"), 
										JOptionPane.WARNING_MESSAGE);
								try {
									MaintenanceDelegate.getInstance().exitMaintenanceMode();
								} catch (CommonValidationException e) {
									Errors.getInstance().showExceptionDialog(MainController.getMainFrame(), e);
								}
								return;
							}
						}
					}
				} else {
					if (!MaintenanceDelegate.getInstance().isMaintenanceModeOff()) {
					
						if(JOptionPane.OK_OPTION != JOptionPane.showConfirmDialog(mf, 
								SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.18", "Wollen Sie das System wieder aus dem Wartungsmodus nehmen?"),
								SpringLocaleDelegate.getInstance().getMessage("maintenancemode", "Wartungsmodus"), JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE)) {
							return;
						}

						try {
							maintenanceFacadeRemote.exitMaintenanceMode();
						} catch (org.nuclos.common2.exception.CommonValidationException e) {
							Errors.getInstance().showExceptionDialog(MainController.getMainFrame(), e);
							return;
						}
					}
				}
			}

			try {
				restoreUtils.storeWorkspace(getSelectedWorkspace(), false);
				
			} catch (CommonBusinessException e1) {
				if ("Workspace.not.found".equals(e1.getMessage())) {
					getWorkspaceManager().remove(getSelectedWorkspace());
					refreshWorkspaces();
				} else {
					Errors.getInstance().showExceptionDialog(mf, e1);
				}
			}
			
			try {
				restoreUtils.clearAndRestoreWorkspace(preferencesFacadeRemote.getWorkspace(wovo.getPrimaryKey()), 
						new ResultListenerX<Boolean, CommonBusinessException>() {
					@Override
					public void done(Boolean result, CommonBusinessException exception) {
						if (exception != null) {
							Errors.getInstance().showExceptionDialog(mf, exception);
						}
					}
				});
			} catch (CommonBusinessException e) {
				Errors.getInstance().showExceptionDialog(mf, e);
			}
		}
	}

	private void showMaintenanceInitDialog(
			final MainFrame mf) {
		
		final MaintenanceModePanel maintenanceModePanel = new MaintenanceModePanel();

		// update panel information
		new Thread(() -> {
			long start = System.currentTimeMillis();
			do {
				int progressbarValue = 0;
				final Integer waittimeInSeconds = maintenanceFacadeRemote.getWaittimeInSeconds();
				if(waittimeInSeconds != null) {
					final double completeWaitTimeInSeconds = (maintenanceFacadeRemote.getCompleteWaittimeInSeconds());

					progressbarValue = (int)(100.0 * ((completeWaitTimeInSeconds - (double) waittimeInSeconds) / completeWaitTimeInSeconds));
				}
				maintenanceModePanel.progressbar.setValue(progressbarValue);

				final Integer numberOfOpenSessions = maintenanceFacadeRemote.getNumberOfOpenSessions();
				maintenanceModePanel.userStatusValue.setText("" + numberOfOpenSessions);
				maintenanceModePanel.jobStatusValue.setText("" + maintenanceFacadeRemote.getNumberOfRunningJobs());
				maintenanceModePanel.updateUI();

				try {Thread.sleep(500);} catch (InterruptedException e) { /* nothing */}
			} while( needToWaitUntilMaintenanceModeIsEntered(start) );

			if(SwingUtilities.getWindowAncestor(maintenanceModePanel) != null) {
				SwingUtilities.getWindowAncestor(maintenanceModePanel).setVisible(false);
			}
		}).start();
		
		Object[] options2 = {SpringLocaleDelegate.getInstance().getMessage("general.cancel", "Abbrechen")};
		int result = JOptionPane.showOptionDialog(mf, maintenanceModePanel, "Wechseln in Wartungsmodus", JOptionPane.PLAIN_MESSAGE, JOptionPane.WARNING_MESSAGE, null, options2, options2[0]);
		if(result == 0) {
			try {
				maintenanceFacadeRemote.exitMaintenanceMode();
			} catch (org.nuclos.common2.exception.CommonValidationException e) {
				Errors.getInstance().showExceptionDialog(MainController.getMainFrame(), e);
			}
			return;
		}
	}


	private boolean needToWaitUntilMaintenanceModeIsEntered(final long start) {
		final boolean mmInitialized = MaintenanceDelegate.getInstance().isSuperUserAndMaintenanceModeInitialized();
		final Integer waittimeInSeconds = maintenanceFacadeRemote.getCompleteWaittimeInSeconds();

		// maintenance mode not initialized await init phase
		final boolean initPhase = !mmInitialized  && (System.currentTimeMillis() - start < 5000);

		// maintenance mode is initialized wait until maintenance mode is on
		final boolean waitUntilMaintenanceModeIsOn = mmInitialized && (waittimeInSeconds != null && System.currentTimeMillis() - start < (waittimeInSeconds * 1000));

		return initPhase || waitUntilMaintenanceModeIsOn;
	}


	public WorkspaceVO getSelectedWorkspace() {
		return getWorkspaceManager().getSelected();
	}
	
	public void setSelectedWorkspace(WorkspaceVO wovo) {
		wovo = getWorkspaceManager().setSelected(wovo, preferencesFacadeRemote);
		if (wovo != null) {
			contentPanel.repaint();
			
			Main.getInstance().getMainFrame().setSplittingEnabled(
					wovo.getAssignedWorkspace() == null || 
					(wovo.getAssignedWorkspace() != null && SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN)));
			LiveSearchController.getInstance().setSearchComponentVisible(LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Live_Search_Enabled));			
		}
	}

	public JComponent getChooserComponent() {
		return contentPanel;
	}
	
	public void setEnabled(boolean b) {
		enabled = b;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	private void setupContextMenu(final WorkspaceLabel wl) {
		wl.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				UIUtils.runCommand(Main.getInstance().getMainFrame(), new Runnable() {
					
					@Override
					public void run() {
						if (enabled && SwingUtilities.isRightMouseButton(e)) {
							final JPopupMenu popup = new JPopupMenu();
							addMenuItems(new MenuItemContainer() {
								@Override
								public void addSeparator() {
									popup.addSeparator();
								}
								@Override
								public void addLabel(String label) {
									popup.add(new JLabel(label));
								}
								@Override
								public void add(JMenuItem mi) {
									popup.add(mi);
								}
							}, wl.getWorkspaceVO(), wl);
							popup.show(wl, 10, 10);
						}
					}
				});
			}
		});
	}
	
	private interface MenuItemContainer {
		
		void addLabel(String label);
		
		void add(JMenuItem mi);
		
		void addSeparator();
	}
	
	private void addMenuItems(final MenuItemContainer menu, final WorkspaceVO wovo, final Label lb) {		
		/**
		 * CHECK 
		 */
		try {
			preferencesFacadeRemote.getWorkspace(wovo.getPrimaryKey());
		} catch (Exception ex) {
			if ("Workspace.not.found".equals(ex.getMessage())) {
				getWorkspaceManager().remove(wovo);
				refreshWorkspaces();
			}
			Errors.getInstance().showExceptionDialog(Main.getInstance().getMainFrame(), ex);
			return;
		}
		
		menu.addLabel("<html><b>"
				+ SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.2","Arbeitsumgebung")
				+ "</b></html>");
		
		/**
		 * REFRESH
		 */
		final JMenuItem miRefresh = new JMenuItem(newRefreshAction());
		menu.add(miRefresh);
		
		if (!wovo.isAssigned() ||
				SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN) || 
				SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_CREATE_NEW))
			menu.addSeparator();
		
		/**
		 * NEW
		 */
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN) || 
				SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_CREATE_NEW)) {
			final JMenuItem miNew = new JMenuItem(newNewWorkspaceAction());
			menu.add(miNew);
		}
		
		/**
		 * EDIT
		 */
		if (!wovo.isAssigned() || SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN)) {
			final JMenuItem miEdit = new JMenuItem(newEditAction(wovo));
			menu.add(miEdit);
		}
		
		/**
		 * CLONE
		 */
		if ((SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN) || 
				SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_CREATE_NEW)) &&
				!wovo.isMaintenance()
				) {
			final JMenuItem miSaveAs = new JMenuItem(newCloneAction(wovo));
			menu.add(miSaveAs);
		}
		
		/**
		 * REMOVE
		 */
		if (!wovo.isAssigned() || SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN)) {
			menu.addSeparator();
			final JMenuItem miRemove = new JMenuItem(newRemoveAction(wovo));
			menu.add(miRemove);
		}
		
		if (wovo.getAssignedWorkspace() != null || 
				SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN))
			menu.addSeparator();
		
		if(SecurityCache.getInstance().isActionAllowed(Actions.ACTION_WORKSPACE_ASSIGN) && !wovo.isMaintenance()) {
			/**
			 * ASSIGN
			 */
			final JMenuItem miAssign = new JMenuItem(newAssignAction(wovo, lb));
				menu.add(miAssign);
				
				if (wovo.getAssignedWorkspace() != null) {
					/**
					 * PUBLISH
					 */
					final JMenuItem miUpdate = new JMenuItem(newPublishAction(wovo, lb));
					menu.add(miUpdate);
				}
		}
		
		/**
		 * RESTORE
		 */
		if(wovo.getAssignedWorkspace() != null) {
			final JMenuItem miUpdate = new JMenuItem(newRestoreAction(wovo));
			menu.add(miUpdate);
		}				
	}
	
	private Action newRefreshAction() {
		return new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage(
						"WorkspaceChooserController.13","Leiste aktualisieren"), 
				MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconRefresh16(), ICON_SIZE)) {

					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {
						UIUtils.runCommand(Main.getInstance().getMainFrame(), new Runnable() {
							@Override
							public void run() {
								getWorkspaceManager().reset();
								setupWorkspaces();
							}
						});
					}
			};
	}
	
	private Action newNewWorkspaceAction() {
		return new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.1","Neu"), 
				MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconNew16(), ICON_SIZE)) {

				@Override
				public void actionPerformed(ActionEvent e) {
					final MainFrame mf = Main.getInstance().getMainFrame();
					
					restoreUtils.closeTabs(true, new ResultListener<Boolean>() {
						@Override
						public void done(Boolean result) {
							if (Boolean.TRUE.equals(result)) {
								WorkspaceVO wovo = new WorkspaceVO(); // only for header information
								WorkspaceEditor we = new WorkspaceEditor(wovo);
								if (we.isSaved()) {
									try {
										restoreUtils.storeWorkspace(getSelectedWorkspace(), true);
									} catch (Exception e2) {
										Errors.getInstance().showExceptionDialog(mf, e2);
									}
									restoreUtils.clearAndRestoreToDefaultWorkspace(wovo.getWoDesc(), new ResultListenerX<WorkspaceVO, CommonBusinessException>() {
										@Override
										public void done(WorkspaceVO result, CommonBusinessException exception) {
											if (result != null) {
												addWorkspace(result, true);
											} else
											if (exception != null) {
												Errors.getInstance().showExceptionDialog(mf, exception);
											}
										}
									});
								}
							}
						}
					});
					
				}
			};
	}
	
	private Action newEditAction(final WorkspaceVO wovo) {
		return new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.3","Eigenschaften"), 
				MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconEdit16(), ICON_SIZE)) {

				@Override
				public void actionPerformed(ActionEvent e) {
					boolean hideMenuBar = wovo.getWoDesc().isHideMenuBar();
					String hideMenuItems = wovo.getWoDesc().getParameter(WorkspaceParameter.HIDE_MENU_ITEMS);
					WorkspaceEditor we = new WorkspaceEditor(wovo);
					try {
						if (we.isSaved()) {
							preferencesFacadeRemote.storeWorkspaceHeaderOnly(wovo);
							refreshWorkspaces();
							if (wovo == restoreUtils.getMainFrame().getWorkspace()) {
								if (hideMenuBar != wovo.getWoDesc().isHideMenuBar() || 
										!LangUtils.equal(hideMenuItems, wovo.getWoDesc().getParameter(WorkspaceParameter.HIDE_MENU_ITEMS))) {
									Main.getInstance().getMainController().setupMenuBar();
								}
							}

						}
					} catch (Exception e1) {
						we.revertChanges();
						Errors.getInstance().showExceptionDialog(Main.getInstance().getMainFrame(), e1);
					} 
				}
			};
	}
	
	private Action newCloneAction(final WorkspaceVO wovo) {
		return new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.4","Klonen"), 
				MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconClone16(), ICON_SIZE)) {

				@Override
				public void actionPerformed(ActionEvent e) {
					final MainFrame mf = Main.getInstance().getMainFrame();
					String newName = JOptionPane.showInputDialog(mf, 
						SpringLocaleDelegate.getInstance().getMessage(
								"WorkspaceChooserController.6","Arbeitsumgebung \"{0}\" klonen",wovo.getWoDesc().getName()) + ":", 
						SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.5","Neuer Name"), 
						JOptionPane.INFORMATION_MESSAGE);
					
					if (!StringUtils.looksEmpty(newName)) {
						if (getSelectedWorkspace().equals(wovo)) {
							WorkspaceVO wovo2 = new WorkspaceVO();
							wovo2.importHeader(wovo.getWoDesc());
							wovo2.setName(newName);
							wovo2.getWoDesc().addAllEntityPreferences(wovo.getWoDesc().getEntityPreferences());
							wovo2.getWoDesc().setAllParameters(wovo.getWoDesc().getParameters());
							wovo2.getWoDesc().addAllLayoutPreferences(wovo.getWoDesc().getLayoutPreferences());

							try {
								addWorkspace(restoreUtils.storeWorkspace(wovo2, true), false);
							} catch (CommonBusinessException e1) {
								Errors.getInstance().showExceptionDialog(mf, e1);									
							}
						} else {
							try {
								WorkspaceVO wovo2 = preferencesFacadeRemote.getWorkspace(wovo.getPrimaryKey());
								wovo2.setPrimaryKey(null);
								wovo2.setAssignedWorkspace(null);
								wovo2.setName(newName);
								wovo2 = preferencesFacadeRemote.storeWorkspace(wovo2);
								addWorkspace(wovo2, false);
							} catch (Exception e1) {
								Errors.getInstance().showExceptionDialog(mf, e1);
							} 
						}
					}
				}
			};
	}
	
	private Action newRemoveAction(final WorkspaceVO wovo) {
		return new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.7","Löschen"), 
				MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconRealDelete16(), ICON_SIZE)) {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					final MainFrame mf = Main.getInstance().getMainFrame();
					
					/*
					 * Helper
					 */
					class Helper {
						/**
						 * 
						 */
						public void remove() {
							preferencesFacadeRemote.removeWorkspace(wovo.getAssignedWorkspace()==null?
									wovo.getPrimaryKey():
										wovo.getAssignedWorkspace());
							getWorkspaceManager().remove(wovo);
							
							if (wovo.equals(getSelectedWorkspace())) {
								restoreNext(); 
							} else {
								refreshWorkspaces();
							}
						}
						
						private Iterator<WorkspaceVO> iter;
						
						/**
						 * 
						 */
						public void restoreNext() {
							if (iter == null) {
								iter = getWorkspaceManager().getWorkspaces().iterator();
							}
							
							if (iter.hasNext()) {
								final WorkspaceVO wovo = iter.next();
								try {
									restoreUtils.clearAndRestoreWorkspace(preferencesFacadeRemote.getWorkspace(wovo.getPrimaryKey()), 
											new ResultListenerX<Boolean, CommonBusinessException>() {
										@Override
										public void done(Boolean result, CommonBusinessException exception) {
											if (Boolean.TRUE.equals(result)) {
												setSelectedWorkspace(wovo);
												refreshWorkspaces();
											} else {
												restoreNext();
											}
										}
									});
								} catch (CommonBusinessException e) {
									// workspace removed
									restoreNext();
								}
							} else {	
								restoreUtils.clearAndRestoreToDefaultWorkspace(new ResultListenerX<WorkspaceVO, CommonBusinessException>() {
									@Override
									public void done(WorkspaceVO result, CommonBusinessException exception) {
										if (result != null) {
											addWorkspace(result, true);
										} else
										if (exception != null) {
											Errors.getInstance().showExceptionDialog(mf, exception);
										}
									}
								});
							}
						}
						
						public void start() {
							if (JOptionPane.YES_OPTION == 
								JOptionPane.showConfirmDialog(mf, 
									SpringLocaleDelegate.getInstance().getMessage(
											"WorkspaceChooserController.8","Möchten Sie wirklich die Arbeitsumgebung \"{0}\" löschen?",
											wovo.getWoDesc().getName()), 
									SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.9","Sind Sie sicher?"), 
									JOptionPane.YES_NO_OPTION)) {
								
								if (wovo.equals(getSelectedWorkspace())) {
									restoreUtils.clearWorkspace(new ResultListener<Boolean>() {
										@Override
										public void done(Boolean result) {
											if (Boolean.TRUE.equals(result)) {
												remove();
											}
										}
									});
								} else {
									remove();
								}
								
							}
						}
					}
					
					if (wovo.equals(getSelectedWorkspace())) {
						restoreUtils.closeTabs(true, new ResultListener<Boolean>() {
							@Override
							public void done(Boolean result) {
								if (Boolean.TRUE.equals(result)) {
									new Helper().start();
								}
							}
						});
					} else {
						new Helper().start();
					}
					
				}
			};
	}
	
	private Action newAssignAction(final WorkspaceVO wovo, final Label lb) {
		return new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage(
						"WorkspaceChooserController.10","Benutzergruppen zuweisen"), 
				MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconRelate(), ICON_SIZE)) {
				
				private static final long serialVersionUID = 1L;

				@Override
				public void actionPerformed(ActionEvent e) {
					final MainFrame mf = Main.getInstance().getMainFrame();
					// assigned and assignable roles
					final Collection<UID> assignedRoleUIDs = preferencesFacadeRemote.getAssignedRoleIds(wovo.getAssignedWorkspace());
					final Collection<RoleAssignment> assignableRoles = CollectionUtils.transform(preferencesFacadeRemote.getAssignableRoles(), 
							new Transformer<EntityObjectVO<UID>, RoleAssignment>() {
						@Override
						public RoleAssignment transform(EntityObjectVO<UID> eovo) {
							return new RoleAssignment(eovo.getFieldValue(E.ROLE.name.getUID(), String.class), eovo.getPrimaryKey());
						}
					});
					
					// select controller
					ChoiceList<RoleAssignment> cl = new ChoiceList<RoleAssignment>();
					Comparator<RoleAssignment> comp = new Comparator<RoleAssignment>(){
						@Override
						public int compare(RoleAssignment o1,
								RoleAssignment o2) {
							return o1.role.compareToIgnoreCase(o2.role);
						}};
					cl.set(assignableRoles, comp);
					cl.setSelectedFields(CollectionUtils.sorted(CollectionUtils.select(assignableRoles, new Predicate<RoleAssignment>() {
						@Override
						public boolean evaluate(RoleAssignment ra) {
							return assignedRoleUIDs.contains(ra.uid);
						}
					}), comp));
					final SelectObjectsController<RoleAssignment> selectCtrl = new SelectObjectsController<RoleAssignment>(
							mf, 
							new DefaultSelectObjectsPanel<RoleAssignment>());
					selectCtrl.setModel(cl);
					
					// result ok=true
					if (selectCtrl.run(SpringLocaleDelegate.getInstance().getMessage(
							"WorkspaceChooserController.11","Arbeitsumgebung Benutzergruppen zuweisen"))) {
						final Icon icoBackup = lb.getIcon();
						final String toolTipBackup = lb.getToolTipText();
						
						lb.setIcon(MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconSaveS16(), ICON_SIZE));
						lb.setToolTipText(SpringLocaleDelegate.getInstance().getResource(
								"WorkspaceChooserController.12","Zuweisung wird gespeichert") + "...");
						
						WorkspaceChooserController.this.setEnabled(false);
						
						SwingWorker<WorkspaceVO, WorkspaceVO> worker = new SwingWorker<WorkspaceVO, WorkspaceVO>() {
							@Override
							protected WorkspaceVO doInBackground() throws Exception {
								try {
									Thread.sleep(500); // otherwise eyesore flash of save icon
									if (getSelectedWorkspace().equals(wovo))
									restoreUtils.storeWorkspace(wovo, true);
								return preferencesFacadeRemote.assignWorkspace(
										wovo, CollectionUtils.transform(selectCtrl.getSelectedObjects(), new Transformer<RoleAssignment, UID>() {
										@Override
										public UID transform(RoleAssignment ra) {
											return ra.uid;
										}
									}));
								}
								catch (Exception ex) {
									LOG.error("newAssignAction failed: " + ex, ex);
								}
								return null;
							}
							@Override
							protected void done() {
								try {
									final WorkspaceVO assignedWorkspace = get();
									getWorkspaceManager().replace(wovo, assignedWorkspace);
									if (wovo.equals(getSelectedWorkspace())) {
										setSelectedWorkspace(assignedWorkspace);
									}
									refreshWorkspaces();
									
								} catch (ExecutionException e) {
									if (e.getCause() != null && e.getCause() instanceof CommonBusinessException) {
										Errors.getInstance().showExceptionDialog(mf, e.getCause());
									} else {
										Errors.getInstance().showExceptionDialog(mf, e);
									}
								} catch (Exception e) {
									Errors.getInstance().showExceptionDialog(mf, e);
								} finally {
									lb.setIcon(icoBackup);
									lb.setToolTipText(toolTipBackup);
									WorkspaceChooserController.this.setEnabled(true);
								} 
							}
						};
						
						worker.execute();
					}
					
				}
			};
	}
	
	private Action newPublishAction(final WorkspaceVO wovo, final Label lb) {
		final MainFrame mf = Main.getInstance().getMainFrame();
		return new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.14","Änderungen in Vorlage publizieren"), 
				MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconRedo16(), ICON_SIZE)) {

					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							if (getSelectedWorkspace().equals(wovo)) {
								restoreUtils.storeWorkspace(wovo, true);
							}
							boolean structureChanged = preferencesFacadeRemote.isWorkspaceStructureChanged(wovo.getAssignedWorkspace(), wovo.getPrimaryKey());
							WorkspacePublisher wp = new WorkspacePublisher(structureChanged);
							if (wp.isSaved()) {
								preferencesFacadeRemote.publishWorkspaceChanges(wovo, 
										wp.isPublishStructureChange(),
										wp.isPublishStructureUpdate(),
										wp.isPublishStarttabConfiguration(),
										wp.isPublishToolbarConfiguration());
								
								final String message = SpringLocaleDelegate.getInstance().getMessage(
										"WorkspaceChooserController.16","Änderungen erfolgreich publiziert.");
								SwingUtilities.invokeLater(new Runnable() {
									@Override
									public void run() {
										try {
											(new Bubble(lb, message, 8, BubbleUtils.Position.SW)).setVisible(true);
										} catch (IllegalComponentStateException e) {
											JOptionPane.showMessageDialog(mf, message);
										}
									}
								});
							}
						} catch (Exception e1) {
							Errors.getInstance().showExceptionDialog(mf, e1);
						}
					}
		};
	}
	
	public Action newRestoreAction(final WorkspaceVO wovo) {
		return new AbstractAction(
				SpringLocaleDelegate.getInstance().getMessage("WorkspaceChooserController.15","Auf Vorlage zurücksetzen"), 
				MainFrame.resizeAndCacheIcon(Icons.getInstance().getIconUndo16(), ICON_SIZE)) {

					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {
						restoreToAssigned(wovo);
					}
		};
	}
	
	/**
	 * restore selected workspace
	 */
	public void restoreSelectedWorkspace() {
		if (!restoreUtils.isRestoreRunning()) {
			if (getSelectedWorkspace().getAssignedWorkspace() == null) {
				
				restoreUtils.closeTabs(true, new ResultListener<Boolean>() {
					@Override
					public void done(Boolean result) {
						if (Boolean.TRUE.equals(result)) {
							final MainFrame mf = Main.getInstance().getMainFrame();
							if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(mf,
								SpringLocaleDelegate.getInstance().getMessage(
										"MainFrame.restoreDefaultWorkspace.2","Möchten Sie wirklich die Arbeitsumgebung auf den Ausgangszustand zurücksetzen?"),
								SpringLocaleDelegate.getInstance().getMessage(
										"MainFrame.restoreDefaultWorkspace.1","Arbeitsumgebung zurücksetzen"),
								JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)) {
								
								WorkspaceDescription2 wdBackup = getSelectedWorkspace().getWoDesc();
								getSelectedWorkspace().setWoDesc(WorkspaceDescriptionDefaultsFactory.createOldMdiStyle());
								getSelectedWorkspace().getWoDesc().importHeader(wdBackup);
								
								restoreUtils.clearAndRestoreWorkspace(getSelectedWorkspace(), 
										new ResultListenerX<Boolean, CommonBusinessException>() {
									@Override
									public void done(Boolean result, CommonBusinessException exception) {
										if (Boolean.TRUE.equals(result)) {
											try {
												restoreUtils.storeWorkspace(getSelectedWorkspace(), true);
												getSelectedWorkspace().getWoDesc().getFrames().clear();
											} catch (CommonBusinessException e) {
												exception = e;
											}
										}
										if (exception != null) {
											Errors.getInstance().showExceptionDialog(mf, exception);
										}
									}
								});
							}
						}
					}
				});
				
			} else {
				restoreToAssigned(getSelectedWorkspace());
			}
		}
	}
	
	/**
	 * 
	 * @param wovo
	 */
	private void restoreToAssigned(final WorkspaceVO wovo) {
		final MainFrame mf = Main.getInstance().getMainFrame();
		
		class Helper {
			public void start() {
				if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(mf,
						SpringLocaleDelegate.getInstance().getMessage(
								"MainFrame.restoreDefaultWorkspace.2","Möchten Sie wirklich die Arbeitsumgebung auf den Ausgangszustand zurücksetzen?"),
						SpringLocaleDelegate.getInstance().getMessage(
								"MainFrame.restoreDefaultWorkspace.1","Arbeitsumgebung zurücksetzen"),
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)) {
					try {
						WorkspaceVO restoredWovo = preferencesFacadeRemote.restoreWorkspace(wovo);
						wovo.importHeader(restoredWovo.getWoDesc());
						refreshWorkspaces();
						if (getSelectedWorkspace().equals(wovo)) {
							restoreUtils.clearAndRestoreWorkspace(restoredWovo, new ResultListenerX<Boolean, CommonBusinessException>() {
								@Override
								public void done(Boolean result, CommonBusinessException exception) {
									if (exception != null) {
										Errors.getInstance().showExceptionDialog(mf, exception);
									}
								}
							});
						}
					} catch (Exception e1) {
						Errors.getInstance().showExceptionDialog(mf, e1);
					}
				}
			}
		}
		
		if (getSelectedWorkspace().equals(wovo)) {
			restoreUtils.closeTabs(true, new ResultListener<Boolean>() {
				@Override
				public void done(Boolean result) {
					if (Boolean.TRUE.equals(result)) {
						new Helper().start();
					}
				}
			});
		} else {
			new Helper().start();
		}
		
	}
	
	private static class ContentPanel extends JPanel {
		
		public ContentPanel() {
			super(new FlowLayout(FlowLayout.RIGHT, 0, 0));
		}
		
		@Override
		public Dimension getPreferredSize() {
			return new Dimension (super.getPreferredSize().width, WorkspacePanel.imgBG_h);
		}
	}
	
	private static class WorkspacePanel extends JPanel{
		
		private static final ImageIcon imgBG = Icons.getInstance().getWorkspaceChooser_bg();
		private static final int imgBG_w = imgBG.getIconWidth();
		public  static final int imgBG_h = imgBG.getIconHeight();

		public WorkspacePanel() {
			super(new FlowLayout(FlowLayout.CENTER, 0, 0));
		}

		@Override
		public void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;
			RenderingHints oldRH = g2.getRenderingHints();
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			
			final Dimension size = getSize();			
			for (int i = 0; i < ((size.width / imgBG_w)+1); i++) {
				g2.drawImage(imgBG.getImage(), i * imgBG_w, 0, null);
			}
			
	        g2.setRenderingHints(oldRH);
		}
		
		@Override
		public Dimension getPreferredSize() {
			return new Dimension (super.getPreferredSize().width, imgBG_h);
		}
	}
	
	protected final static ImageIcon imgBG = Icons.getInstance().getWorkspaceChooser_buttonBG();
	protected final static ImageIcon imgBG_left = Icons.getInstance().getWorkspaceChooser_buttonLeft();
	protected final static ImageIcon imgBG_right = Icons.getInstance().getWorkspaceChooser_buttonRight();
	protected final static ImageIcon imgBG_maintenanceMode = Icons.getInstance().getIconByName("workspaceChooser_buttonBG_highlighted");
	protected final static ImageIcon imgBG_left_maintenanceMode = Icons.getInstance().getIconByName("workspaceChooser_buttonLeft_highlighted");
	protected final static ImageIcon imgBG_right_maintenanceMode = Icons.getInstance().getIconByName("workspaceChooser_buttonRight_highlighted");
	public  final static int imgBG_h = imgBG.getIconHeight();
	protected final static int imgBG_w = imgBG.getIconWidth();
	protected final static int imgBG_left_w = imgBG_left.getIconWidth();
	protected final static int imgBG_right_w = imgBG_right.getIconWidth();

	private abstract class Label extends JLabel {
		
		protected boolean mouseOver = false;
		
		public Label(String text, Icon icon, int horizontalAlignment) {
			super(text, icon, horizontalAlignment);
			addMouseListener(new MouseAdapter() {
				@Override
				public void mouseEntered(MouseEvent e) {
					if (!mouseOver) {
						mouseOver = true;
						repaint();
					} else {
						mouseOver = true;
					}
				}
				@Override
				public void mouseExited(MouseEvent e) {
					if (mouseOver) {
						mouseOver = false;
						repaint();
					} else {
						mouseOver = false;
					}
				}
			});
		}

		@Override
		protected void paintComponent(Graphics g) {
			Graphics2D g2 = (Graphics2D) g;

			RenderingHints oldRH = g2.getRenderingHints();
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

			boolean showMaintenanceModeTab = isSelected() && 
					isMaintenanceModeSelected() && 
					MaintenanceDelegate.getInstance().isSuperUserAndMaintenanceModeOn();

			boolean drawBG = (mouseOver && enabled) || isSelected(); 
			if (drawBG) {

				BufferedImage biBG = drawBuffImage(
						new BufferedImage(imgBG_w, imgBG_h, BufferedImage.TYPE_INT_ARGB),
						showMaintenanceModeTab ? imgBG_maintenanceMode.getImage() : imgBG.getImage());
				BufferedImage biBG_left = drawBuffImage(
						new BufferedImage(imgBG_left_w, imgBG_h, BufferedImage.TYPE_INT_ARGB),
						showMaintenanceModeTab ? imgBG_left_maintenanceMode.getImage() : imgBG_left.getImage());
				BufferedImage biBG_right = drawBuffImage(
						new BufferedImage(imgBG_right_w, imgBG_h, BufferedImage.TYPE_INT_ARGB),
						showMaintenanceModeTab ? imgBG_right_maintenanceMode.getImage() : imgBG_right.getImage());
				
				float[] scales = { 1f, 1f, 1f, ((mouseOver && enabled && !isSelected()))?0f:1f };
				float[] offsets = new float[4];
				RescaleOp rop = new RescaleOp(scales, offsets, null);
			   	
				g2.drawImage(biBG_left, rop, -1, 0);
			   	for (int i = 0; i < (getWidth()-imgBG_left_w-imgBG_right_w) + 2; i++) {
			   		g2.drawImage(biBG, null, imgBG_left_w -1 + (i * imgBG_w), 0);
			   	}
			   	g2.drawImage(biBG_right, rop, getWidth()-imgBG_right_w +1, 0);
			}
			g2.setRenderingHints(oldRH);
			
			super.paintComponent(g);
			
			if (drawBG) {
				if (drawDogEar()) {
					ImageIcon assignedIcon = Icons.getInstance().getWorkspaceChooser_assigned();
					g2.drawImage(assignedIcon.getImage(),
						getWidth()-assignedIcon.getIconWidth(),
						0, null);
				}
			}
		}
		
		private BufferedImage drawBuffImage(BufferedImage bi, Image img) {
			final Graphics gbi = bi.getGraphics();
			gbi.drawImage(img, 0, 0, null);
			gbi.dispose();
			return bi;
		}
		
		@Override
		public Dimension getPreferredSize() {
			return new Dimension (super.getPreferredSize().width, // + imgBG_left_w + imgBG_right_w,
					imgBG_h);
		}
		
		abstract boolean isSelected();
		abstract boolean isMaintenanceModeSelected();
		abstract boolean drawDogEar();
	}
	
	private Icon getWorkspaceIcon(final WorkspaceVO wovo, int size) {
		final Icon ico = wovo.getWoDesc().getNuclosResource()==null?
				Icons.getInstance().getIconTabGeneric():
				NuclosResourceCache.getNuclosResourceIcon(wovo.getWoDesc().getNuclosResource());
		return MainFrame.resizeAndCacheIcon(ico, size);
	}
	
	private class WorkspaceLabel extends Label implements DragGestureListener {
		
		private final WorkspaceVO wovo;		
		
		public WorkspaceLabel(WorkspaceVO wovo) {
			super(
					wovo.getWoDesc().isHideName()?
							null:
							wovo.getName(), 
					getWorkspaceIcon(wovo, ICON_SIZE), 
					SwingConstants.LEFT);
			this.wovo = wovo;
			setBorder(BorderFactory.createEmptyBorder(imgBG_left_w, imgBG_left_w, imgBG_left_w, imgBG_left_w));
			if (wovo.getWoDesc().isHideName()) {
				setToolTipText(wovo.getName());
			}
			DragSource dragSource = DragSource.getDefaultDragSource();
		    dragSource.createDefaultDragGestureRecognizer(WorkspaceLabel.this, DnDConstants.ACTION_COPY_OR_MOVE, WorkspaceLabel.this);
		    setTransferHandler(new TransferHandler() {
				@Override
				public boolean importData(JComponent comp, Transferable t) {
					try {
						WorkspaceVO other = (WorkspaceVO) t.getTransferData(workspaceFlavor);
						if (other != null &&
								!other.equals(WorkspaceLabel.this.wovo)) {
							WorkspaceVO wovoToRemove = null;
							for (WorkspaceVO wovoI : getWorkspaceManager().getWorkspaces()) {
								if (wovoI.equals(other)) {
									wovoToRemove = wovoI;
								}
							}
							int indexToAdd = 0;
							for (WorkspaceVO wovoI : getWorkspaceManager().getWorkspaces()) {
								if (wovoI.equals(WorkspaceLabel.this.wovo)) {
									indexToAdd = getWorkspaceManager().getWorkspaces().indexOf(wovoI);
								}
							}
							if (wovoToRemove != null) getWorkspaceManager().remove(wovoToRemove);
							getWorkspaceManager().add(indexToAdd, other);
							
							WorkspaceChooserController.this.refreshWorkspaces();
							return true;
						}
					} catch (Exception e) {} 
					
					return super.importData(comp, t);
				}
				@Override
				public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
					for (DataFlavor dataFlavor : transferFlavors) {
						if (workspaceFlavor.equals(dataFlavor)) {
							return true;
						}
					}
					return false;
				}
		    });
		}
		
		public WorkspaceVO getWorkspaceVO() {
			return wovo;
		}

		public boolean isSelected() {
			return wovo.equals(getSelectedWorkspace());
		}
		
		public boolean isMaintenanceModeSelected() {
			return wovo.isMaintenance();
		}

		@Override
		public void dragGestureRecognized(DragGestureEvent dge) {
			Transferable transferable = new WorkspaceLabelTransferable(WorkspaceLabel.this.wovo);
		    dge.startDrag(null, transferable, null);
		    mouseOver = false;
			repaint();
		}

		@Override
		boolean drawDogEar() {
			return wovo.getAssignedWorkspace() != null;
		}
	}
	
	private static class RoleAssignment {
		
		final String role;
		final UID uid;
		
		public RoleAssignment(String role, UID uid) {
			super();
			this.role = role;
			this.uid = uid;
		}
		@Override
		public int hashCode() {
			if (role == null) return 0;
			return role.hashCode();
		}
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof RoleAssignment)
				return this.uid.equals(((RoleAssignment) obj).uid);
			return super.equals(obj);
		}
		@Override
		public String toString() {
			return role;
		}
	}
	
	private static class WorkspaceLabelTransferable implements Transferable {
		
		final WorkspaceVO wovo;
		
		public WorkspaceLabelTransferable(WorkspaceVO wovo) {
			this.wovo = wovo;
		}

		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return flavors;
		}

		@Override
		public boolean isDataFlavorSupported(DataFlavor fl) {
			if (workspaceFlavor.equals(fl))
				return true;
			return false;
		}
		
		@Override
		public Object getTransferData(DataFlavor fl) {
		    if (workspaceFlavor.equals(fl)) {
		      return wovo;
		    }
		    return null;
		}
	}
	
	public static final DataFlavor workspaceFlavor = new DataFlavor(WorkspaceVO.class, "WorkspaceVO");
	private static final DataFlavor[] flavors = new DataFlavor[] {workspaceFlavor};
	
}
