//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.attribute;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.attribute.ejb3.AttributeFacadeRemote;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

/**
 * Business Delegate for <code>AttributeFacadeBean</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
// @Component
// @Lazy
public class AttributeDelegate {
	
	private static AttributeDelegate INSTANCE;

	// 
	
	private AttributeFacadeRemote attributeFacadeRemote;

	public static AttributeDelegate getInstance() {
		if (INSTANCE.attributeFacadeRemote == null) throw new NullPointerException("too early");
		return INSTANCE;
	}

	private AttributeDelegate() {
		INSTANCE = this;
	}
	
	// @Autowired
	public final void setAttributeFacadeRemote(AttributeFacadeRemote attributeFacadeRemote) {
		this.attributeFacadeRemote = attributeFacadeRemote;
	}

	/**
	 * @param groupUid if null, all attributes are returned.
	 * @return all dynamic attributes (for all modules) that belong to the given group (if any)
	 */
	public Collection<AttributeCVO> getAllAttributeCVOs(UID groupUid) {
		try {
			return attributeFacadeRemote.getAttributes(groupUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}	// getAttributes

	/**
	 * get the available calculation functions for dynamic attributes
	 */
	public Map<String, String> getCallableFunctions(final Class<?> result,final  Object... args) {
		try {
			return this.attributeFacadeRemote.getCallableFunctions(result, args);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * get the layouts that contained this attribute
	 */
	public Set<String> getAttributeLayouts(UID attributeUid){
		try{
			return this.attributeFacadeRemote.getAttributeLayouts(attributeUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
}	// class AttributeDelegate
