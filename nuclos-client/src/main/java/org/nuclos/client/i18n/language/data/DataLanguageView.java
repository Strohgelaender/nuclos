package org.nuclos.client.i18n.language.data;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;

public class DataLanguageView extends JPanel{

	private JToolBar tbar;
	private JButton saveButton = null;
	private JButton refreshButton = null;	

	private DataLanguageSelectionPanel selectionPanel;
	
	private DataLanguageViewListener listener;
	
	public DataLanguageView(DataLanguageViewListener listener) {
		
		TableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.FILL);

		this.listener = listener;
		this.tbar = createToolbar(); 
		
		initSelectionPanel();
		
		JPanel pnl = new JPanel(new BorderLayout());
	    pnl.add(this.tbar, BorderLayout.NORTH);
	    pnl.add(this.selectionPanel, BorderLayout.CENTER);
	    
	    tbllay.newRow(TableLayout.FILL).add(pnl);	    
	}
	
	public void refreshView() {
		this.selectionPanel.refreshView();
	}
	
	private void initSelectionPanel() {
		this.selectionPanel = new DataLanguageSelectionPanel(this.listener);		
	}
	
	private JToolBar createToolbar() {
		ToolTipManager toolTipManager = ToolTipManager.sharedInstance();
		 
		JToolBar tbar = new JToolBar();
		tbar.setFloatable(false);
		tbar.setPreferredSize(new Dimension(tbar.getPreferredSize().width, 36));
		
		saveButton = new JButton( Icons.getInstance().getIconSave16());
		saveButton.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("CollectController.6", "Save"));
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DataLanguageView.this.save();
			}
		});
		
		saveButton.setEnabled(false);
		tbar.add(saveButton);
		
		refreshButton = new JButton(Icons.getInstance().getIconRefresh16());
		refreshButton.setToolTipText(SpringLocaleDelegate.getInstance().getMessage("CollectController.4", "Refresh"));
		refreshButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DataLanguageView.this.refresh();
			}
		});
		refreshButton.setEnabled(false);
		tbar.add(refreshButton);
		
		return tbar;
	}
	
	public void save() {
		this.listener.save();
	}
	
	public void refresh() {
		this.listener.refresh();
	}

	public void setIsRefreshEnabled(Boolean isEnabled) {
		this.refreshButton.setEnabled(isEnabled);
	}
	
	public void setIsSaveEnabled(Boolean isEnabled) {
		this.saveButton.setEnabled(isEnabled);
	}
	
	public DataLanguageSelectionPanel getDataLanguageSelectionPanel() {
		return this.selectionPanel;
	}
}
