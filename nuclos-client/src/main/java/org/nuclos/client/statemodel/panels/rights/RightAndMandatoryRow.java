//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.panels.rights;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.nuclos.client.ui.Icons;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.slf4j.LoggerFactory;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstants;

/**
 * 
 * represents one row for a group (subform) or attribute (column)
 */
public class RightAndMandatoryRow implements RightAndMandatoryConstants {

	private final UID id;
	private final String name;
	
	private final Map<UID, Object> roleRights;
	List<UID> sortOrder = new ArrayList<UID>();
	boolean mandatory = false;
	boolean metaMandatory = false;
	boolean rightsEnabled = false;
	boolean collapsed = false;
	boolean nullRightAllowed = true;
	boolean subform = false;
	boolean expandable;
	boolean subgroup = false;
	RightAndMandatoryRow group = null;
	
	ExpandCollapseListener expandCollapseListener;
	SelectionListener selectionListener;
	
	ChangeListener detailsChangedListener;
	
	/**
	 * 
	 * @param id
	 * @param name
	 * @param rightsEnabled
	 * @param roleRights
	 * @param nullRightAllowed
	 * @param mandatory
	 * @param sortOrder
	 * @param group
	 * @param subform
	 * @param detailsChangedListener
	 */
	public RightAndMandatoryRow(UID id, String name, boolean rightsEnabled, Map<UID, Object> roleRights, boolean nullRightAllowed, boolean mandatory, boolean metaMandatory, List<UID> sortOrder, RightAndMandatoryRow group, boolean subform, boolean expandable, ChangeListener detailsChangedListener) {
	    super();
	    this.id = id;
	    this.name = name;
	    this.rightsEnabled = rightsEnabled;
	    this.roleRights = roleRights;
	    this.nullRightAllowed = nullRightAllowed;
	    this.sortOrder = sortOrder;
	    this.mandatory = mandatory;
	    this.metaMandatory = metaMandatory;
	    this.group = group;
	    this.subform = subform;
	    this.expandable = expandable;
	    this.detailsChangedListener = detailsChangedListener;
	    respectNullRightAllowed();
	    if (metaMandatory) {
			this.mandatory = false;
		}
    }
	
	View view = null;
	
	
	/**
	 * 
	 * @param namewidth initial value
	 * @return JPanel
	 */
	public View initView(double namewidth) {
		if (view == null) {
			view = new View(namewidth);
		}
		return view;
	}
	
	/**
	 * 
	 * @param namewidth
	 */
	public void updateView(double namewidth) {
		if (view != null) {
			((TableLayout)view.getLayout()).setColumn(0, namewidth);
		}
	}
	
	/**
	 * 
	 * @return preferred row header width
	 */
	public int getRowHeaderWidth() {
		return view!=null ? view.getRowHeaderWidth() : 0;
	}
	
	/**
	 * 
	 * @param role
	 * @param selected
	 */
	public void setSelected(Integer role, boolean selected) {
		if (view!=null)
			view.setSelected(role, selected);
	}
	
	/**
	 * remove selection from all right columns
	 */
	public void removeAllSelections() {
		if (view!=null)
			view.removeAllSelections();
	}
	
	/**
	 * @return map of role with right: Map&lt;UID, Boolean&gt; 
	 */
	public Map<UID, Object> getRoleRights() {
		return roleRights;
	}

	/**
	 * 
	 * @param role
	 * @param right
	 */
	public void setRoleRight(UID role, Object right) {
		if (!nullRightAllowed && right == null)
			right = new Boolean(false);
		
		roleRights.put(role, right);
		if (view != null) {
			if (view.rightButtons.get(role) != null) {
				view.rightButtons.get(role).right = right;
				view.rightButtons.get(role).updateIcon();
			}
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public ExpandCollapseListener getExpandCollapseListener() {
		return expandCollapseListener;
	}
	
	/**
	 * 
	 * @param ecl
	 */
	public void setExpandCollapseListener(ExpandCollapseListener ecl) {
		expandCollapseListener = ecl;
	}
	
	/**
	 * 
	 * @param sl
	 */
	public void setSelectionListener(SelectionListener sl) {
		selectionListener = sl;
	}
	
	/**
	 * 
	 * @return id of group (subform) / attribute (column)
	 */
	public UID getId() {
		return id;
	}
	
	/**
	 * 
	 * @return name of group (subform) / attribute (column)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @return true if row is a group or subform
	 */
	public boolean isGroup() {
		return group == null;
	}
	
	/**
	 * 
	 * @return group id 
	 */
	public UID getGroupId() {
		if (isGroup())
			return getId();
		else
			return group.getId();
	}
	
	/**
	 * 
	 * @return group name
	 */
	public String getGroupName() {
		if (isGroup())
			return name;
		else
			return group.getGroupName();
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isSubForm() {
		return subform;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isExpandable() {
		return expandable;
	}
	
	/**
	 * 
	 * @return true if no right is allowed
	 */
	public boolean isNullRightAllowed() {
		return nullRightAllowed;
	}
	
	/**
	 * 
	 * @return true if this row has right values
	 */
	public boolean isRightsEnabled() {
		return rightsEnabled;
	}
	
	/**
	 * 
	 * @param rightsEnabled 
	 */
	public void setRightsEnabled(boolean rightsEnabled) {
		this.rightsEnabled = rightsEnabled;
	}
	
	/**
	 * 
	 * @return true if attribute / column is mandatory
	 */
	public boolean isMandatory() {
		return mandatory;
	}
	
	/**
	 * 
	 * @param mandatory
	 */
	public void setMandatory(boolean mandatory) {
		setMandatory(mandatory, true);
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isCollapsed() {
		return collapsed;
	}

	/**
	 * 
	 * @param collapsed
	 */
	public void setCollapsed(boolean collapsed) {
		this.collapsed = collapsed;
		if (view!=null) {
			view.rowHeader.updateExpandCollapseIcon();
		}
	}

	private void setMandatory(boolean mandatory, boolean updateUI) {
		this.mandatory = mandatory;
		if (updateUI)
			if (view != null)
				view.rowHeader.checkbMandatory.setSelected(mandatory);
	}
	
	private void respectNullRightAllowed() {
		if (nullRightAllowed)
			return;
		
		for (UID role : roleRights.keySet()) {
			if (roleRights.get(role) == null) {
				setRoleRight(role, false);
			}
		}
	}
	
	/**
	 * 
	 * @param role
	 */
	public void hideRoleRight(UID role) {
		if (view != null) {
			((TableLayout)view.getLayout()).setColumn(view.findRoleInLayout(role), 0);
			view.revalidate();
			view.repaint();
		}
	}
	
	/**
	 * 
	 * @param role
	 */
	public void showRoleRight(UID role) {
		if (view != null) {
			((TableLayout)view.getLayout()).setColumn(view.findRoleInLayout(role), TableLayoutConstants.PREFERRED);
			view.revalidate();
			view.repaint();
		}
	}
	
	/**
	 * copy rights from group.
	 * row has to be attribute or column
	 */
	public void copyRightsFromGroup() {
		if (view != null && !isGroup()) {
			view.copyRightsFromGroup();
		}
	}
	
	/**
	 * 
	 * @param right
	 */
	public void setAllRoleRights(Boolean right) {
		if (!isRightsEnabled()) {
			setRightsEnabled(true);
		}
		for (UID role : sortOrder) {
			if (!role.equals(UID.UID_NULL))
				setRoleRight(role, right);
		}
	}

	public void setSubGroup(boolean bSubGroup) {
		this.subgroup = bSubGroup;
	}

	public boolean isSubGroup() {
		return subgroup;
	}

	/**
	 * 
	 * the main JPanel for this row
	 */
	public class View extends JPanel {

		private RowHeader rowHeader = null;
		private Map<UID, RightButton> rightButtons = new HashMap<UID, RightButton>();
		private RevertButton revert = new RevertButton();
		
		public View(double namewidth) {
			double[] cols = new double[1 + 1 + roleRights.size() + 1 + 1];
			cols[0] = TableLayoutConstants.PREFERRED;
			cols[1] = GAP_ROWHEADER;
			for (int i = 2; i < cols.length; i++) {
				cols[i] = TableLayoutConstants.PREFERRED;
			}
			cols[cols.length-2] = 1; // last grid on the right
			final double size [][] = {cols, new double[] {TableLayoutConstants.PREFERRED}};
			final TableLayout layout = new TableLayout(size);
			
			setLayout(layout);
			setBackground(COLOR_BACKGROUND);
			
			rowHeader = new RowHeader(COLOR_BACKGROUND, namewidth);
			add(rowHeader, "0,0");
			
			if (group == null && id == null)
				return; // is "system" group
			
			MouseOver mOver = new MouseOver();
			
			int col = 2;
			for (int i = 0; i < sortOrder.size(); i++) {
				UID role = sortOrder.get(i);
				
				rightButtons.put(role, new RightButton(role, roleRights.get(role)));
				rightButtons.get(role).setPreferredSize(new Dimension(
					CELL_WIDTH-1, CELL_HEIGHT));
				rightButtons.get(role).setBackground(subform ? COLOR_BACKGROUND : COLOR_MARKER_BACKGROUND);
				if (subform) rightButtons.get(role).addMouseListener(mOver);
				
				add(rightButtons.get(role), col+",0");
				col++;
			}
			JPanel lastGridOnTheRight = new JPanel() {


				@Override
				protected void paintComponent(Graphics g) {
					Graphics2D g2d = (Graphics2D) g;				
					g2d.setStroke(new BasicStroke(1.f));
					g2d.setColor(group != null ? COLOR_GRID : COLOR_BACKGROUND);
					g2d.drawLine(0, 0, 0, CELL_HEIGHT-1);
				}
				
			};
			add(lastGridOnTheRight, col+",0");
			col++;

			if (subform) {
				if (isGroup()) {
					add(new HelpButton(), col+",0");
				} else {
					revert.addMouseListener(mOver);
					add(revert, col+",0");
				}
			}
		}

		private void setSelected(Integer role, boolean selected) {
			if (view.rightButtons.get(role) != null) {
				rightButtons.get(role).setSelected(selected);
				rightButtons.get(role).revalidate();
				rightButtons.get(role).repaint();
			}
		}
		
		private void removeAllSelections() {
			for (RightButton rb : rightButtons.values()) {
				if (rb.isSelected()) {
					rb.setSelected(false);
				}
			}
		}
		
		private int getRowHeaderWidth() {
			return rowHeader!=null ? rowHeader.getPreferredSize().width : 0;
		}
		
		private void copyRightsFromGroup() {
			rightsEnabled = true;
			for (UID role : group.roleRights.keySet()) {
				setRoleRight(role, transRightsFromInt(group.roleRights.get(role)));
			}
		}
		
		private void revertRights() {
			rightsEnabled = false;
			for (RightButton rb : rightButtons.values()) {
				rb.updateIcon();
			}
		}
		
		private ImageIcon getTreeIcon(boolean collapsed) {
			Object uiTreeIcon = UIManager.get(collapsed?"Tree.collapsedIcon":"Tree.expandedIcon");
			
			if (uiTreeIcon != null && uiTreeIcon instanceof ImageIcon) {
				return (ImageIcon) uiTreeIcon;
			} else {
				throw new IllegalArgumentException("Look&Feel without Tree.expandedIcon/Tree.collapsedIcon");
			}
		}
		
		private int findRoleInLayout(UID role) {
			int posInLayout = 2;
			for (int i = 0; i < sortOrder.size(); i++) {
				if (sortOrder.get(i).equals(role)) {
					posInLayout = posInLayout + i;
					break;
				}
			}
			return posInLayout;
		}

		private Boolean transRightsFromInt(Object r) {
			if (r instanceof Integer) {
				Integer i = (Integer) r;
				return i == 0 ? null : (i < 3 ? false : true);
			}
			return null;
		}

		/**
		 * 
		 * for highlighting rights from group
		 */
		class MouseOver extends MouseAdapter {
			@Override
			public void mouseEntered(MouseEvent e) {
				if (group != null) {
					if (!rightsEnabled) {
						for (UID role : rightButtons.keySet()) {
							RightButton rb = rightButtons.get(role);
							if (!nullRightAllowed && group.roleRights.get(role) == null)
								rb.updateIcon(false, true);
							else
								rb.updateIcon(transRightsFromInt(group.roleRights.get(role)), true);
						}
					} else {
						revert.updateIcon(true);
					}
				}
			}
			@Override
			public void mouseExited(MouseEvent e) {
				if (group != null) {
					if (!rightsEnabled) {
						for (RightButton rb : rightButtons.values()) {
							rb.updateIcon();
						}
					} else {
						revert.updateIcon(false);
					}
				}
			}
		}
		
		/**
		 * 
		 * revert attribute / column rights
		 */
		class RevertButton extends JLabel {
			
			public RevertButton() {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent e) {
						revertRights();
						updateIcon(false);
						
						if (selectionListener != null) {
							selectionListener.actionPerformed(new ActionEvent(RightAndMandatoryRow.this, 0, 
								SelectionListener.COMMAND_DESELECT));
						}
						
						detailsChangedListener.stateChanged(new ChangeEvent(this));
					}
				});
				updateIcon(false);
			}
			
			private void updateIcon(boolean show) {
				if (show) {
					setIcon(Icons.getInstance().getIconPriorityCancel16());
				} else {
					setIcon(Icons.getInstance().getIconEmpty16());
				}
			}
		}

		/**
		 *
		 * help button
		 */
		class HelpButton extends JLabel {

			public HelpButton() {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
				addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent e) {
						try {
							URI uri = new URI("http://wiki.nuclos.de/pages/viewpage.action?pageId=5933108");
							Desktop.getDesktop().browse(uri);
						} catch (URISyntaxException | IOException ex) {
							LoggerFactory.getLogger(RightAndMandatoryRow.class).warn(ex.getMessage());
						}
					}
				});
				setIcon(Icons.getInstance().getIconAbout16());
			}
		}
		
		/**
		 * 
		 * one right/role button
		 */
		class RightButton extends JLabel {
			
			private UID role;
			private Object right;
			private boolean selected;
			
			/**
			 * 
			 * @return current right (could be null)
			 */
            public Object getRight() {
            	return right;
            }

            /**
             * 
             * @param role
             * @param right
             */
			public RightButton(final UID role, Object right) {
				this.role = role;
				this.right = right;

				if (subform || (isGroup() && !UID.UID_NULL.equals(getId()))) {
					setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					addMouseListener(new MouseAdapter() {
						@Override
	                    public void mousePressed(MouseEvent e) {
							if (!rightsEnabled) {
								copyRightsFromGroup();
							} else {
								if (RightButton.this.right == null) {
									RightButton.this.right = false;
								} else if (RightButton.this.right instanceof Integer) {
									Integer r = (Integer)RightButton.this.right;
									if (++r == 4) {
										r = 0;
									}
									RightButton.this.right = r;
								} else if (Boolean.FALSE.equals(RightButton.this.right)) {
									RightButton.this.right = true;
								} else if (Boolean.TRUE.equals(RightButton.this.right)) {
									if (nullRightAllowed)
										RightButton.this.right = null;
									else
										RightButton.this.right = false;
								} 
								roleRights.put(RightButton.this.role, RightButton.this.right);
								updateIcon();
							}
							
							if (selectionListener != null) {
								selectionListener.actionPerformed(new ActionEvent(RightAndMandatoryRow.this, RightButton.this.role.hashCode(), 
									SelectionListener.COMMAND_DESELECT));
							}
							
							detailsChangedListener.stateChanged(new ChangeEvent(this));
						}
					});
				}
				updateIcon();
			}
			
			private void setSelected(boolean selected) {
				this.selected = selected;
				revalidate();
				repaint();
			}
			
			private boolean isSelected() {
				return this.selected;
			}
			
			private void updateIcon() {
				updateIcon(right, rightsEnabled);
			}
			
			private void updateIcon(Object rightToShow, boolean rightsEnabled) {
				if (rightsEnabled) {
					if (rightToShow == null) {
						setIcon(ICON_NO_RIGHT);
					} else {
						if (rightToShow instanceof Integer) {
							setIcon(ICONS_PLUSMINUS[(Integer)rightToShow]);
						}
						else if (Boolean.TRUE.equals(rightToShow)) {
							setIcon(ICON_WRITE);
						} else {
							setIcon(ICON_READ);
						}
					}
				} else {
					setIcon(Icons.getInstance().getIconEmpty16());
				}
			}
			
			@Override
			public void paintComponent(Graphics g) {
				Graphics2D g2d = (Graphics2D) g;
				
				if (selected) {
					g2d.setColor(COLOR_SELECTION_BACKGROUND);
					g2d.fillRect(1, 0, CELL_WIDTH-2, CELL_HEIGHT);
				} else if (!subform && group != null) {
					g2d.setColor(COLOR_MARKER_BACKGROUND);
					g2d.fillRect(1, 0, CELL_WIDTH-2, CELL_HEIGHT);
				}
				
				super.paintComponent(g);
				
				if (group != null) {
					g2d.setStroke(new BasicStroke(1.f));
					g2d.setColor(COLOR_GRID);
					g2d.drawLine(0, 0, CELL_WIDTH-1, 0);
					g2d.drawLine(CELL_WIDTH-1, 0, CELL_WIDTH-1, CELL_HEIGHT-1);
					g2d.drawLine(CELL_WIDTH-1, CELL_HEIGHT-1, 0, CELL_HEIGHT-1);
					g2d.drawLine(0, CELL_HEIGHT-1, 0, 0);
				}
			}
		}
		
		/**
		 * 
		 * header for this row
		 */
		class RowHeader extends JPanel {
			
			private JLabel labExpandCollapse;
			private MouseListener mlExpandCollapse;
			private final JCheckBox checkbMandatory = new JCheckBox();
			
			public RowHeader(Color colorBackground, double namewidth) {
				double[] cols = new double[3];
				cols[0] = getTreeIcon(true).getIconWidth()+5;
				cols[1] = TableLayoutConstants.FILL;
				cols[2] = TableLayoutConstants.PREFERRED;
				
				final double size [][] = {cols, new double[] {TableLayoutConstants.PREFERRED}};
				final TableLayout layout = new TableLayout(size);
				
				setLayout(layout);
				setBackground(colorBackground);
				
				if (group == null) {
					if (isExpandable()) {
					JLabel ecLabel = getExpandCollapseLabel();
					ecLabel.setBackground(colorBackground);
					add(ecLabel, "0,0");}
				}
				
				JLabel labName;
				if (isGroup()) {
					labName = new JLabel("<html><b>"+name+"</b></html>");
				} else {
					labName = new JLabel(name);
				}
				labName.setBackground(colorBackground);
				labName.addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent e) {
						if(SwingUtilities.isRightMouseButton(e)) {
							if (getId() == null)
								return;
							
							final JPopupMenu menu = new JPopupMenu();
							if (nullRightAllowed) {
								JMenuItem itemNull = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
										"RightAndMandatory.1", "Alle nicht sichtbar"), ICON_NO_RIGHT);
								itemNull.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {setAllRoleRights(null);detailsChangedListener.stateChanged(new ChangeEvent(this));}
								});
								menu.add(itemNull);
							}
							JMenuItem itemRead = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
									"RightAndMandatory.2", "Alle lesen"), ICON_READ);
							itemRead.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {setAllRoleRights(false);detailsChangedListener.stateChanged(new ChangeEvent(this));}
							});
							menu.add(itemRead);
							JMenuItem itemWrite = new JMenuItem(SpringLocaleDelegate.getInstance().getMessage(
									"RightAndMandatory.3", "Alle schreiben"), ICON_WRITE);
							itemWrite.addActionListener(new ActionListener() {
								@Override
								public void actionPerformed(ActionEvent e) {setAllRoleRights(true);detailsChangedListener.stateChanged(new ChangeEvent(this));}
							});
							menu.add(itemWrite);
							menu.show(e.getComponent(), e.getX(), e.getY());
						}
					}
				});
				add(labName, "1,0,l,b");
				
				if (group != null && !subgroup) {
					checkbMandatory.setPreferredSize(new Dimension(checkbMandatory.getPreferredSize().width,labName.getPreferredSize().height));
					checkbMandatory.setBackground(colorBackground);
					if (metaMandatory) {
						checkbMandatory.setSelected(true);
						checkbMandatory.setEnabled(false);
					} else {
						checkbMandatory.setSelected(mandatory);
						checkbMandatory.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								setMandatory(checkbMandatory.isSelected(), false);
								detailsChangedListener.stateChanged(new ChangeEvent(this));
							}
						});
					}
					add(checkbMandatory, "2,0");
					
					setBorder(BorderFactory.createLineBorder(COLOR_GRID, 1));
				}
			}
			
			private JLabel getExpandCollapseLabel() {
				if (labExpandCollapse == null) {
					labExpandCollapse = new JLabel(getTreeIcon(collapsed));
					labExpandCollapse.addMouseListener(getExpandCollapseMouseListener());
				}
				return labExpandCollapse;
			}
			
			private void updateExpandCollapseIcon() {
				if (labExpandCollapse != null)
					labExpandCollapse.setIcon(getTreeIcon(collapsed));
			}
			
			private MouseListener getExpandCollapseMouseListener() {
				if (mlExpandCollapse == null)
					mlExpandCollapse = new MouseAdapter() {
						@Override
						public void mousePressed(MouseEvent e) {
							collapsed = !collapsed;
							getExpandCollapseLabel().setIcon(getTreeIcon(collapsed));
							if (expandCollapseListener != null)
								expandCollapseListener.actionPerformed(new ActionEvent(RightAndMandatoryRow.this, 0, 
									collapsed? ExpandCollapseListener.COMMAND_COLLAPSE : ExpandCollapseListener.COMMAND_EXPAND));
						}
					};
				return mlExpandCollapse;
			}

		}
	}
}
