//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.panels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.statemodel.StateModelEditor;
import org.nuclos.client.statemodel.controller.InsertRoleController;
import org.nuclos.client.statemodel.models.StateRoleTableModel;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.E;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * shows the rules attached to a transition.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class TransitionPropertiesPanel extends JPanel {
	
	private TransitionRulePanel pnlTransRules;
	private TransitionRolesPanel pnlTransRoles;
	private StateModelEditor parent;
	private final RolesActionListener alRoles = new RolesActionListener();
	
	private final TransitionDataChangeListener dataChangeListener = 
			new TransitionDataChangeListener() {
				@Override
				public void transitionAutomaticDataChange(boolean isAutomaticTransition) throws RemoteException {
					parent.setAutomaticTransition(isAutomaticTransition);
				}

				@Override
				public void transitionDefaultDataChange(boolean isDefaultTransition) throws RemoteException {
					parent.setDefaultTransition(isDefaultTransition);
				}
				
				@Override
				public void transitionNonstopDataChange(boolean isNonstopTransition) throws RemoteException {
					parent.setNonstopTransition(isNonstopTransition);
				}

				@Override
				public void attachedRuleDataChangeListenerEvent() {
					try {
						parent.removeRulesByType(StateChangeRule.class.getCanonicalName());
						parent.removeRulesByType(StateChangeFinalRule.class.getCanonicalName());
						
						parent.addRules(pnlTransRules.getStateChangeFinalRulePanel().getModel().getEntries());
						parent.addRules(pnlTransRules.getStateChangeRulePanel().getModel().getEntries());
					} catch (Exception e) {
						Errors.getInstance().showExceptionDialog(parent, e.getMessage(), e);
					}
				}
	}; 
	
	public TransitionPropertiesPanel(StateModelEditor parent) {
		super(new BorderLayout());
		
		this.parent = parent;
		pnlTransRules = new TransitionRulePanel(parent);
		pnlTransRoles = new TransitionRolesPanel();
		
		JTabbedPane tabPane = new JTabbedPane();
		
		tabPane.add(SpringLocaleDelegate.getInstance().getMessage("StatePropertiesPanel.6", "Eigenschaften", null), pnlTransRules);
		tabPane.add(SpringLocaleDelegate.getInstance().getMessage("StatePropertiesPanel.9", "Berechtigungen", null), pnlTransRoles);
		
		this.add(tabPane, BorderLayout.CENTER);
		
		this.pnlTransRules.setButtonWritable(
				SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));
		
		this.pnlTransRoles.getBtnAdd().addActionListener(alRoles);
		this.pnlTransRoles.getBtnDelete().addActionListener(alRoles);
		this.pnlTransRoles.getBtnDelete().setEnabled(false);
		this.pnlTransRoles.getTblRoles().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent ev) {
				rolesSelectionChanged();
			}
		});
		
		this.pnlTransRules.addTransitionRuleDataChangeListener(dataChangeListener);
	}
	
	public TransitionRulePanel getPnlTransRules() {
		return pnlTransRules;
	}

	public TransitionRolesPanel getPnlTransRoles() {
		return pnlTransRoles;
	}


	private void rolesSelectionChanged() {
		final int iSelIndex = this.pnlTransRoles.getTblRoles().getSelectedRow();
		this.pnlTransRoles.getBtnDelete().setEnabled(iSelIndex >= 0);
	}

	private void rolesActionPerformed(ActionEvent ev) {
		final StateRoleTableModel model = this.pnlTransRoles.getModel();
		try {
			if (ev.getActionCommand().equals("add")) {
				final InsertRoleController controller = new InsertRoleController(parent);
				if (controller.run(SpringLocaleDelegate.getInstance().getMessage(
						"TransitionPropertiesController.2","Verf\u00fcgbare Benutzergruppen"), model.getRoles())) {
					for (int i = 0; i < controller.pnlRoles.getTblRoles().getSelectedRowCount(); i++) {
						parent.addRole(controller.pnlRoles.getRow(controller.pnlRoles.getTblRoles().getSelectedRows()[i]));
					}
					model.fireTableDataChanged();
				}
			}
			else if (ev.getActionCommand().equals("remove")) {
				for (int i = 0; i < this.pnlTransRoles.getTblRoles().getSelectedRowCount(); i++) {
					parent.removeRole(model.getRow(this.pnlTransRoles.getTblRoles().getSelectedRows()[i]));
				}
				model.fireTableDataChanged();
			}
		}
		catch (RemoteException ex) {
			Errors.getInstance().showExceptionDialog(parent, ex.getMessage(), ex);
		}
	}
	
	private class RolesActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent ev) {
			rolesActionPerformed(ev);
		}
	}
	
}	// class TransitionRulesPanel

