//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.apache.log4j.Logger;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.gef.AbstractShapeController;
import org.nuclos.client.gef.DefaultShapeViewer;
import org.nuclos.client.gef.Shape;
import org.nuclos.client.gef.ShapeControllerException;
import org.nuclos.client.gef.ShapeModel;
import org.nuclos.client.gef.ShapeModelListener;
import org.nuclos.client.gef.ShapeViewer;
import org.nuclos.client.gef.layout.Extents2D;
import org.nuclos.client.gef.shapes.AbstractConnector;
import org.nuclos.client.gef.shapes.AbstractShape;
import org.nuclos.client.gef.shapes.ConnectionPoint;
import org.nuclos.client.rule.server.panel.EventSupportSelectionTableModel;
import org.nuclos.client.statemodel.admin.StateModelCollectController;
import org.nuclos.client.statemodel.models.NotePropertiesPanelModel;
import org.nuclos.client.statemodel.models.StatePropertiesPanelModel;
import org.nuclos.client.statemodel.panels.StateModelEditorPropertiesPanel;
import org.nuclos.client.statemodel.panels.TransitionRolesPanel.TransitionRolesDataFlavor;
import org.nuclos.client.statemodel.panels.TransitionRulePanel;
import org.nuclos.client.statemodel.panels.rights.RightTransfer;
import org.nuclos.client.statemodel.panels.rights.RightTransfer.RoleRights;
import org.nuclos.client.statemodel.shapes.NoteShape;
import org.nuclos.client.statemodel.shapes.StateModelStartShape;
import org.nuclos.client.statemodel.shapes.StateShape;
import org.nuclos.client.statemodel.shapes.StateTransition;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.statemodel.valueobject.NoteLayout;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateLayout;
import org.nuclos.server.statemodel.valueobject.StateModelLayout;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.statemodel.valueobject.TransitionLayout;

/**
 * The state model editor.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class StateModelEditor extends JPanel implements ShapeModelListener, FocusListener {

	private static final Logger LOG = Logger.getLogger(StateModelEditor.class);
	
	private class SelectAction extends AbstractAction {

		SelectAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.1","Auswahl"), 
					Icons.getInstance().getIconSelectObject());
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			btnSelection.setSelected(true);
			setSelectionTool();
		}
	}

	private class NewStateAction extends AbstractAction {

		NewStateAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.9","Neuer Status"), 
					Icons.getInstance().getIconState());
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			btnInsertState.setSelected(true);
			setStateTool();
		}
	}

	private class NewTransitionAction extends AbstractAction {

		NewTransitionAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.10","Neue Transition"), 
					Icons.getInstance().getIconStateTransition());
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			btnInsertTransition.setSelected(true);
			setTransitionTool();
		}
	}

	private class NewNoteAction extends AbstractAction {

		NewNoteAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.8","Neue Bemerkung"), 
					Icons.getInstance().getIconStateNewNote());
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			btnInsertNote.setSelected(true);
			setNoteTool();
		}
	}

	private class DeleteAction extends AbstractAction {

		DeleteAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.3","Auswahl l\u00f6schen"), 
					Icons.getInstance().getIconDelete16());
		}

		@Override
        public void actionPerformed(ActionEvent e) {
				deleteSelection();
		}
	}

	private class DefaultTransitionAction extends AbstractAction {

		DefaultTransitionAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.19","Als Standardpfad definieren"));
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			if (shapeSelected instanceof AbstractConnector) {
				((StateTransition) shapeSelected).getStateTransitionVO().setDefault(btnDefaultTransition.getSelectedObjects() != null);
			//	pnlProperties.getTransitionRulePanel().getBtnDefault().setSelected(btnDefaultTransition.getSelectedObjects() != null);
				
				getViewer().getModel().fireModelChanged();
				((Component) getViewer()).repaint();
			}	
		}
	}
	
	private class CopyStateRightsAction extends AbstractAction {

		CopyStateRightsAction() {
			super(SpringLocaleDelegate.getInstance().getMessage(
					"StateModelEditor.17","Rechte & Pflichten kopieren"), 
					Icons.getInstance().getIconCopy16());
		}
		
		@Override
        public void actionPerformed(ActionEvent e) {
			if (shapeSelected != null && shapeSelected instanceof StateTransition) {
				copyTransitionRoles();				
			} else if (shapeSelected != null && shapeSelected instanceof StateShape) {
				copyStateRights();
			}
		}
	}
	
	private class PasteStateRightsAction extends AbstractAction {

		PasteStateRightsAction() {
			super(SpringLocaleDelegate.getInstance().getMessage(
					"StateModelEditor.18","Rechte & Pflichten einfügen"), 
					Icons.getInstance().getIconPaste16());
		}
		
		@Override
        public void actionPerformed(ActionEvent e) {
			if (shapeSelected != null && shapeSelected instanceof StateTransition) {
				pasteTransitionRoles();				
			} else if (shapeSelected != null && shapeSelected instanceof StateShape) {
				pasteStateRights();
			}
		}
	}

	private class ZoomInAction extends AbstractAction {

		ZoomInAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.15","Zoom +"), 
					Icons.getInstance().getIconZoomIn());
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			zoomIn();
		}
	}

	private class ZoomOutAction extends AbstractAction {

		ZoomOutAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.13","Zoom -"), 
					Icons.getInstance().getIconZoomOut());
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			zoomOut();
		}
	}

	private class PrintAction extends AbstractAction {

		PrintAction() {
			super(SpringLocaleDelegate.getInstance().getMessage("StateModelEditor.5","Drucken..."), 
					Icons.getInstance().getIconPrint16());
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			printStateModel();
		}
	}

	private class NameDocumentListener implements DocumentListener {
		
		private Locale locale;
		
		public NameDocumentListener(Locale locale) {
			this.locale = locale;
		}
		
		@Override
        public void changedUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateName(locale);
		}

		@Override
        public void insertUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateName(locale);
		}

		@Override
        public void removeUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateName(locale);
		}
	}

	private class MnemonicDocumentListener implements DocumentListener {
		@Override
        public void changedUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateMnemonic();
		}

		@Override
        public void insertUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateMnemonic();
		}

		@Override
        public void removeUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateMnemonic();
		}
	}


	private class IconDocumentListener implements CollectableComponentModelListener {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			StateModelEditor.this.changeStateIcon();
		}

		@Override
		public void searchConditionChangedInModel(SearchComponentModelEvent ev) {
			// ...
			
		}

		@Override
		public void valueToBeChanged(DetailsComponentModelEvent ev) {
			// ...			
		}
	}
	
	private class ButtonLabelDocumentListener implements DocumentListener {
		
		private Locale locale;
		
		public ButtonLabelDocumentListener(Locale locale) {
			this.locale = locale;
		}

		@Override
        public void changedUpdate(DocumentEvent e) {
			StateModelEditor.this.changeButtonLabel(locale);
		}

		@Override
        public void insertUpdate(DocumentEvent e) {
			StateModelEditor.this.changeButtonLabel(locale);
		}

		@Override
        public void removeUpdate(DocumentEvent e) {
			StateModelEditor.this.changeButtonLabel(locale);
		}
	}
	
	private class ButtonIconChangeListener implements CollectableComponentModelListener {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			StateModelEditor.this.changeButtonIcon();
		}

		@Override
		public void searchConditionChangedInModel(SearchComponentModelEvent ev) {
			// ...
			
		}

		@Override
		public void valueToBeChanged(DetailsComponentModelEvent ev) {
			// ...			
		}
	}
	
	
	private class ColorChangeListener implements CollectableComponentModelListener {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			StateModelEditor.this.changeColor();
		}

		@Override
		public void searchConditionChangedInModel(SearchComponentModelEvent ev) {
			// ...
			
		}

		@Override
		public void valueToBeChanged(DetailsComponentModelEvent ev) {
			// ...			
		}
	}
	

	private class DescriptionDocumentListener implements DocumentListener {
		
		private Locale locale;
		
		public DescriptionDocumentListener(Locale locale) {
			this.locale = locale;
		}
		
		@Override
        public void changedUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateDescription(this.locale);
		}

		@Override
        public void insertUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateDescription(this.locale);
		}

		@Override
        public void removeUpdate(DocumentEvent e) {
			StateModelEditor.this.changeStateDescription(this.locale);
		}
	}

	private class NoteDocumentListener implements DocumentListener {
		@Override
        public void changedUpdate(DocumentEvent e) {
			StateModelEditor.this.changeNoteText();
		}

		@Override
        public void insertUpdate(DocumentEvent e) {
			StateModelEditor.this.changeNoteText();
		}

		@Override
        public void removeUpdate(DocumentEvent e) {
			StateModelEditor.this.changeNoteText();
		}
	}
	
	private class TabDataListener implements ListDataListener {
		@Override
        public void contentsChanged(ListDataEvent e) {
			StateModelEditor.this.changeTabData();
		}

		@Override
        public void intervalAdded(ListDataEvent e) {
		}

		@Override
        public void intervalRemoved(ListDataEvent e) {
		}
	}
	
	private final DefaultShapeViewer pnlShapeViewer;
	private final JScrollPane scrollPane;
	private final StateModelEditorPropertiesPanel pnlProperties;
/**
     * @return the pnlProperties
     */
    public StateModelEditorPropertiesPanel getPropertiesPanel() {
    	return pnlProperties;
    }

	private final JToolBar toolbar = new JToolBar();
	private final Action actSelect = new SelectAction();
	private final Action actNewState = new NewStateAction();
	private final Action actNewTransition = new NewTransitionAction();
	private final Action actNewNote = new NewNoteAction();
	private final Action actDelete = new DeleteAction();
	private final Action actDefaultTransition = new DefaultTransitionAction();
	private final Action actCopyStateRights = new CopyStateRightsAction();
	private final Action actPasteStateRights = new PasteStateRightsAction();
	private final Action actZoomIn = new ZoomInAction();
	private final Action actZoomOut = new ZoomOutAction();
	private final Action actPrint = new PrintAction();
	private final JLabel labZoom = new JLabel("100%");
	private final JToggleButton btnSelection = new JToggleButton(actSelect);
	private final JToggleButton btnInsertState = new JToggleButton(actNewState);
	private final JToggleButton btnInsertTransition = new JToggleButton(actNewTransition);
	private final JToggleButton btnInsertNote = new JToggleButton(actNewNote);
	private final JCheckBoxMenuItem btnDefaultTransition = new JCheckBoxMenuItem(actDefaultTransition);
	private final double[] adZoomSteps = {30d, 50d, 75d, 100d, 125d, 150d, 200d, 300d};
	private int iCurrentZoom = 3;
	private final List<ChangeListener> lstChangeListeners = new Vector<ChangeListener>();
	private Shape shapeSelected;

	/** 
	 * §todo eliminate this field - use local variables instead 
	 */
	private StateGraphVO stategraphvo;
	
	private List<CollectableEntityObject<UID>> usages;

	private StateModelLayout layoutinfo;
	private final List<ActionListener> lstPrintEventListeners = new Vector<ActionListener>();
	private final NameDocumentListener nameDocumentListenerDE = new NameDocumentListener(Locale.GERMAN);
	private final NameDocumentListener nameDocumentListenerEN = new NameDocumentListener(Locale.ENGLISH);
	private final MnemonicDocumentListener mnemonicDocumentListener = new MnemonicDocumentListener();
	private final IconDocumentListener iconDocumentListener = new IconDocumentListener();
	private final DescriptionDocumentListener descriptionDocumentListenerDE = new DescriptionDocumentListener(Locale.GERMAN);
	private final DescriptionDocumentListener descriptionDocumentListenerEN = new DescriptionDocumentListener(Locale.ENGLISH);
	private final NoteDocumentListener noteDocumentListener = new NoteDocumentListener();
	private final TabDataListener tabDataListener = new TabDataListener();
	private final ColorChangeListener colorChangeListener = new ColorChangeListener();
	private final ButtonLabelDocumentListener buttonLabelDocumentListenerDE = new ButtonLabelDocumentListener(Locale.GERMAN);
	private final ButtonLabelDocumentListener buttonLabelDocumentListenerEN = new ButtonLabelDocumentListener(Locale.ENGLISH);
	private final ButtonIconChangeListener buttonIconChangeListener = new ButtonIconChangeListener();

	private final StateModelCollectController stateModelCollectController;

	public StateModelEditor(StateModelCollectController stateModelCollectController) {
		super(new BorderLayout());

		this.stateModelCollectController = stateModelCollectController;
		
		pnlShapeViewer = new DefaultShapeViewer();
		pnlProperties = new StateModelEditorPropertiesPanel(this);
		scrollPane = new JScrollPane(pnlShapeViewer);

		pnlShapeViewer.getModel().addShapeModelListener(this);
		pnlShapeViewer.setExtents(new Extents2D(1024, 1024));

		this.init();
	}

	private void init() {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		final ButtonGroup bg = new ButtonGroup();

		btnSelection.setSelected(true);
		btnSelection.setText("");
		btnSelection.setToolTipText(localeDelegate.getMessage("StateModelEditor.2","Auswahl"));
		bg.add(btnSelection);
		toolbar.add(btnSelection);
		btnSelection.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));

		btnInsertState.setText("");
		btnInsertState.setToolTipText(localeDelegate.getMessage("StateModelEditor.11","Status einf\u00fcgen"));
		bg.add(btnInsertState);
		toolbar.add(btnInsertState);
		btnInsertState.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));

		btnInsertTransition.setText("");
		btnInsertTransition.setToolTipText(localeDelegate.getMessage("StateModelEditor.12","Transition einf\u00fcgen"));
		bg.add(btnInsertTransition);
		toolbar.add(btnInsertTransition);
		btnInsertTransition.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));

		btnInsertNote.setText("");
		btnInsertNote.setToolTipText(localeDelegate.getMessage("StateModelEditor.7","Kommentar einf\u00fcgen"));
		bg.add(btnInsertNote);
		toolbar.add(btnInsertNote);
		btnInsertNote.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));

		JButton btn = new JButton(actDelete);
		btn.setText("");
		btn.setToolTipText(localeDelegate.getMessage("StateModelEditor.4","Auswahl l\u00f6schen"));
		toolbar.addSeparator();
		toolbar.add(btn);
		btn.setEnabled(SecurityCache.getInstance().isWriteAllowedForMasterData(E.STATEMODEL.getUID()));

		toolbar.addSeparator();
		btn = toolbar.add(actPrint);
		btn.setText("");
		btn.setToolTipText(localeDelegate.getMessage("StateModelEditor.6","Drucken"));

		toolbar.addSeparator();
		labZoom.setFont(new Font("Dialog", Font.PLAIN, 8));
		btn = toolbar.add(actZoomIn);
		btn.setText("");
		btn.setToolTipText(localeDelegate.getMessage("StateModelEditor.16","Zoom +"));
		toolbar.add(labZoom);

		btn = toolbar.add(actZoomOut);
		btn.setText("");
		btn.setToolTipText(localeDelegate.getMessage("StateModelEditor.14","Zoom -"));


		toolbar.setOrientation(JToolBar.VERTICAL);

		this.add(toolbar, BorderLayout.WEST);		
		this.add(scrollPane, BorderLayout.CENTER);

		pnlProperties.setPanel("None");
		scrollPane.addFocusListener(this);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(16);

		final JPopupMenu popup = new JPopupMenu();
		popup.add(actSelect);
		popup.addSeparator();
		popup.add(actNewState);
		popup.add(actNewTransition);
		popup.add(actNewNote);
		popup.addSeparator();
		popup.add(btnDefaultTransition);
		popup.add(actDelete);
		popup.addSeparator();
		popup.add(actCopyStateRights);
		popup.add(actPasteStateRights);
		pnlShapeViewer.getController().setPopupMenu(popup);

		pnlShapeViewer.getModel().addLayer("Notes", true, 3);
		
	}

	public ShapeViewer getViewer() {
		return pnlShapeViewer;
	}

	public StateModelCollectController getStateModelCollectController() {
		return this.stateModelCollectController;
	}
	
	public StateModelEditorPropertiesPanel getStateModelEditorPropertiesPanel(){
		return this.pnlProperties;
	}

	public void addChangeListener(ChangeListener cl) {
		lstChangeListeners.add(cl);
		getStateModelEditorPropertiesPanel().getStatePropertiesPanel().getStateDependantRightsPanel().addDetailsChangedListener(cl);
	}

	public void removeChangeListener(ChangeListener cl) {
		lstChangeListeners.remove(cl);
		getStateModelEditorPropertiesPanel().getStatePropertiesPanel().getStateDependantRightsPanel().removeDetailsChangedListener(cl);
	}

	public void addPrintEventListener(ActionListener al) {
		lstPrintEventListeners.add(al);
	}

	public void removePrintEventListener(ActionListener al) {
		lstPrintEventListeners.remove(al);
	}

	/**
	 * adds listeners for the state (properties) panel.
	 */
	private void addStatePanelListeners() {
		pnlProperties.getStatePropertiesPanel().getModel().docNameDE.addDocumentListener(nameDocumentListenerDE);
		pnlProperties.getStatePropertiesPanel().getModel().docNameEN.addDocumentListener(nameDocumentListenerEN);
		pnlProperties.getStatePropertiesPanel().getModel().docMnemonic.addDocumentListener(mnemonicDocumentListener);
		pnlProperties.getStatePropertiesPanel().getModel().clctImage.getModel()
				.addCollectableComponentModelListener(null, iconDocumentListener);
		pnlProperties.getStatePropertiesPanel().getModel().docDescriptionDE.addDocumentListener(descriptionDocumentListenerDE);
		pnlProperties.getStatePropertiesPanel().getModel().docDescriptionEN.addDocumentListener(descriptionDocumentListenerEN);
		pnlProperties.getStatePropertiesPanel().getModel().modelTab.addListDataListener(tabDataListener);
		pnlProperties.getStatePropertiesPanel().getModel().clctColor.getModel()
				.addCollectableComponentModelListener(null, colorChangeListener);
		pnlProperties.getStatePropertiesPanel().getModel().docButtonLabelDE.addDocumentListener(buttonLabelDocumentListenerDE);
		pnlProperties.getStatePropertiesPanel().getModel().docButtonLabelEN.addDocumentListener(buttonLabelDocumentListenerEN);
		pnlProperties.getStatePropertiesPanel().getModel().clctButtonIcon.getModel()
				.addCollectableComponentModelListener(null, buttonIconChangeListener);
	}

	/**
	 * removes the listeners for the state (properties) panel.
	 */
	private void removeStatePanelListeners() {
		pnlProperties.getStatePropertiesPanel().getModel().docNameDE.removeDocumentListener(nameDocumentListenerDE);
		pnlProperties.getStatePropertiesPanel().getModel().docNameEN.removeDocumentListener(nameDocumentListenerEN);
		pnlProperties.getStatePropertiesPanel().getModel().docMnemonic.removeDocumentListener(mnemonicDocumentListener);
		pnlProperties.getStatePropertiesPanel().getModel().clctImage.getModel().removeCollectableComponentModelListener(iconDocumentListener);
		pnlProperties.getStatePropertiesPanel().getModel().docDescriptionDE.removeDocumentListener(descriptionDocumentListenerDE);
		pnlProperties.getStatePropertiesPanel().getModel().docDescriptionEN.removeDocumentListener(descriptionDocumentListenerEN);
		pnlProperties.getStatePropertiesPanel().getModel().modelTab.removeListDataListener(tabDataListener);
		pnlProperties.getStatePropertiesPanel().getModel().clctColor.getModel().removeCollectableComponentModelListener(colorChangeListener);
		pnlProperties.getStatePropertiesPanel().getModel().docButtonLabelDE.removeDocumentListener(buttonLabelDocumentListenerDE);
		pnlProperties.getStatePropertiesPanel().getModel().docButtonLabelEN.removeDocumentListener(buttonLabelDocumentListenerEN);
		pnlProperties.getStatePropertiesPanel().getModel().clctButtonIcon.getModel().removeCollectableComponentModelListener(buttonIconChangeListener);
	}

	/**
	 * adds listeners for the note (properties) panel.
	 */
	private void addNotePanelListeners() {
		pnlProperties.getNotePanel().getModel().docText.addDocumentListener(noteDocumentListener);
	}

	/**
	 * removes the listeners for the note (properties) panel.
	 */
	private void removeNotePanelListeners() {
		pnlProperties.getNotePanel().getModel().docText.removeDocumentListener(noteDocumentListener);
	}

	@Override
    public void modelChanged() {
		btnSelection.setSelected(true);

		for (ChangeListener cl : lstChangeListeners) {
			cl.stateChanged(new ChangeEvent(this));
		}
	}
	
	private void setDeleteActionEnabled(boolean enabled) {
		actDelete.setEnabled(enabled);
	}

	private void setDefaultTransitionActionEnabled(boolean enabled) {
		actDefaultTransition.setEnabled(enabled);
	}

	private void setDefaultTransitionActionSelected(boolean selected) {
		btnDefaultTransition.setSelected(selected);
	}
	
	private void setCopyAndPasteStateRightAction(boolean enabled) {
		actCopyStateRights.setEnabled(enabled);
		actPasteStateRights.setEnabled(enabled);
	}

	@Override
    public void selectionChanged(Shape shape) {
		setDeleteActionEnabled(shape != null || pnlShapeViewer.getModel().isMultiSelected());
		setDefaultTransitionActionEnabled(false);
		setDefaultTransitionActionSelected(false);
		setCopyAndPasteStateRightAction(shape != null && !pnlShapeViewer.getModel().isMultiSelected());

		this.handlePreviousSelection();

		if (pnlShapeViewer.getModel().isMultiSelected()) {
			/** @todo this seems never to be called */
			LOG.debug("selectionChanged: multi selection");
			/** @todo show rights editor for multiple states */
			pnlProperties.setPanel("None");
			shapeSelected = null;
		}
		else if (shape == null) {
			LOG.debug("selectionChanged: nothing selected");
			pnlProperties.setPanel("None");
			shapeSelected = null;
		}
		else if (shape instanceof StateShape) {
			LOG.debug("selectionChanged: single status shape selected");
			pnlProperties.setPanel("State");
			shapeSelected = shape;
			final StateShape stateshapeSelected = (StateShape) shape;
			stateshapeSelected.checkStatus();

			stateModelCollectController.parseLayout(stateshapeSelected.getStateVO().getId());

			final StatePropertiesPanelModel model = pnlProperties.getStatePropertiesPanel().getModel();
			model.setName(Locale.GERMAN, stateshapeSelected.getName(Locale.GERMAN));
			model.setName(Locale.ENGLISH, stateshapeSelected.getName(Locale.ENGLISH));
			model.setNumeral(stateshapeSelected.getNumeral());
			model.setIcon(stateshapeSelected.getIcon());
			model.setDescription(Locale.GERMAN, stateshapeSelected.getDescription(Locale.GERMAN));
			model.setDescription(Locale.ENGLISH, stateshapeSelected.getDescription(Locale.ENGLISH));
			model.setTab(stateshapeSelected.getStateVO().getTabbedPaneName());
			model.setColor(stateshapeSelected.getStateVO().getColor());
			model.setButtonLabel(Locale.GERMAN, stateshapeSelected.getButtonLabel(Locale.GERMAN));
			model.setButtonLabel(Locale.ENGLISH, stateshapeSelected.getButtonLabel(Locale.ENGLISH));
			model.setButtonIcon(stateshapeSelected.getStateVO().getButtonIcon());

			this.addStatePanelListeners();
			this.setupRightsPanel(stateshapeSelected.getStateVO());
//			this.setupSubforms(stateshapeSelected.getStateVO());
		}
		else if (shape instanceof NoteShape) {
			LOG.debug("selectionChanged: single note shape selected");
			pnlProperties.setPanel("Note");
			shapeSelected = shape;

			final NotePropertiesPanelModel model = pnlProperties.getNotePanel().getModel();
			model.setText(((NoteShape) shapeSelected).getText());

			this.addNotePanelListeners();
		}
		else if (shape instanceof AbstractConnector) {
			LOG.debug("selectionChanged: single transition shape selected");
			pnlProperties.setPanel("Transition");
			shapeSelected = shape;
			StateTransition stateTransSelected = (StateTransition) shapeSelected;
			
			setDefaultTransitionActionEnabled(!stateTransSelected.getStateTransitionVO().isAutomatic());
			setDefaultTransitionActionSelected(stateTransSelected.getStateTransitionVO().isDefault());

			try {
				TransitionRulePanel pnlTransRules = pnlProperties.getTransitionPropertiesPanel().getPnlTransRules();
				EventSupportSelectionTableModel<EventSupportTransitionVO> stateChangeRuleModel = pnlTransRules.getStateChangeRulePanel().getModel();
				EventSupportSelectionTableModel<EventSupportTransitionVO> stateChangeFinalRuleModel = pnlTransRules.getStateChangeFinalRulePanel().getModel();
				stateChangeRuleModel.clear();
				stateChangeFinalRuleModel.clear();
				
				for (EventSupportTransitionVO estVO : stateTransSelected.getRules()) {
					if (StateChangeRule.class.getCanonicalName().equals(estVO.getEventSupportClassType())) {
						stateChangeRuleModel.addEntry(estVO);
					}
					else if (StateChangeFinalRule.class.getCanonicalName().equals(estVO.getEventSupportClassType())) {
						stateChangeFinalRuleModel.addEntry(estVO);
					}
				}
				
				pnlTransRules.setAutomatic(stateTransSelected.getStateTransitionVO().isAutomatic());
				pnlTransRules.setDefault(stateTransSelected.getStateTransitionVO().isDefault());
				pnlTransRules.setNonstop(stateTransSelected.getStateTransitionVO().isNonstop());
				
				pnlProperties.getTransitionPropertiesPanel().getPnlTransRoles().getModel().setRoles(RoleRepository.getInstance().selectRolesById(((StateTransition) shape).getRoles()));
				TableUtils.setPreferredColumnWidth(pnlProperties.getTransitionPropertiesPanel().getPnlTransRoles().getTblRoles(), 10, 10);
			}
			catch (RemoteException ex) {
				Errors.getInstance().showExceptionDialog(this, ex.getMessage(), ex);
			}
		}
		else {
			LOG.debug("selectionChanged: something unknown was selected");
			pnlProperties.setPanel("None");
			shapeSelected = null;
		}
	}

	private void handlePreviousSelection() {
		if (shapeSelected instanceof StateShape) {
			LOG.debug("Status shape deselected.");

			// get user rights from properties panel and store them in the selected state's vo:
//			final StateShape stateshape = (StateShape) shapeSelected;
			/** @todo endEditing here? */
//			final StateVO statevo = stateshape.getStateVO();
//			final StateVO.UserRights userrights = getUserRightsFromPropertiesPanel();
//			statevo.setUserRights(userrights);
			updateStateProperties();
			
			// this.closeSubForms();
			 
			this.removeStatePanelListeners();
		}
		else if (shapeSelected instanceof NoteShape) {
			LOG.debug("Note shape deselected.");
			this.removeNotePanelListeners();
		}
		else {
			LOG.debug("Something unknown was deselected.");
			// do nothing
		}
	}
	
	private void setupRightsPanel(StateVO statevo) {
		if (usages != null) 
			pnlProperties.getStatePropertiesPanel().getStateDependantRightsPanel().setup(usages, statevo);
	}
	
	private void updateStateProperties() {
		pnlProperties.getStatePropertiesPanel().getStateDependantRightsPanel().updateStateVO();
	}
	
	public void setUsages(List<CollectableEntityObject<UID>> usages) {
		this.usages = usages;
		if (usages != null)
			pnlProperties.getStatePropertiesPanel().getStateDependantRightsPanel().setup(usages);
	}

	@Override
    public void multiSelectionChanged(Collection<Shape> collShapes) {
		LOG.debug("multiSelectionChanged");

		/** @todo this is always called when selecting a shape using "rubberbanding", even if a single shape
		 * was selected. In the latter case, selectionChanged() should be called for the selected shape. */

		setDeleteActionEnabled(collShapes.size() > 0);
		setCopyAndPasteStateRightAction(collShapes.size() == 1);
	}

	@Override
    public void shapeDeleted(Shape shape) {
		for (ChangeListener cl : lstChangeListeners) {
			cl.stateChanged(new ChangeEvent(this));
		}
		setDeleteActionEnabled(false);
		setCopyAndPasteStateRightAction(false);
	}

	@Override
    public void shapesDeleted(Collection<Shape> collShapes) {
		for (ChangeListener cl : lstChangeListeners) {
			cl.stateChanged(new ChangeEvent(this));
		}
		setDeleteActionEnabled(false);
		setCopyAndPasteStateRightAction(false);
	}

	public void changeStateName(Locale locale) {
		if (shapeSelected != null && shapeSelected instanceof StateShape) {
			((StateShape) shapeSelected).setName(locale ,pnlProperties.getStatePropertiesPanel().getModel().getName(locale));
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}

	public void changeStateMnemonic() {
		if (shapeSelected != null && shapeSelected instanceof StateShape) {
			((StateShape) shapeSelected).setNumeral(pnlProperties.getStatePropertiesPanel().getModel().getNumeral());
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}

	public void changeStateIcon() {
		if (shapeSelected != null && shapeSelected instanceof StateShape) {
			((StateShape) shapeSelected).setIcon(pnlProperties.getStatePropertiesPanel().getModel().getIcon());
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void changeButtonIcon() {
		if (shapeSelected != null && shapeSelected instanceof StateShape) {
			((StateShape) shapeSelected).getStateVO().setButtonIcon(pnlProperties.getStatePropertiesPanel().getModel().getButtonIcon());
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void changeButtonLabel(Locale locale) {
		if (shapeSelected != null && shapeSelected instanceof StateShape) {
			((StateShape) shapeSelected).setButtonLabel(locale, pnlProperties.getStatePropertiesPanel().getModel().getButtonLabel(locale));
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void changeColor() {
		if (shapeSelected != null && shapeSelected instanceof StateShape) {
			((StateShape) shapeSelected).setStateColor(pnlProperties.getStatePropertiesPanel().getModel().getColor());
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}

	public void changeStateDescription(Locale locale) {
		if (shapeSelected != null && shapeSelected instanceof StateShape) {
			((StateShape) shapeSelected).setDescription(locale, pnlProperties.getStatePropertiesPanel().getModel().getDescription(locale));
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}

	public void changeNoteText() {
		if (shapeSelected != null && shapeSelected instanceof NoteShape) {
			((NoteShape) shapeSelected).setText(pnlProperties.getNotePanel().getModel().getText());
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void changeTabData() {
		if (shapeSelected != null && shapeSelected instanceof StateShape) {
			((StateShape) shapeSelected).setTabbedPaneName(pnlProperties.getStatePropertiesPanel().getModel().getTab());
			pnlShapeViewer.getModel().fireModelChanged();
		}
	}


	public void removeRule(EventSupportTransitionVO vo) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			StateTransition stateTrans = (StateTransition) shapeSelected;
			
			stateTrans.removeRule(vo.getId());
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void setAutomaticTransition(boolean isAutomaticTransition) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			((StateTransition) shapeSelected).getStateTransitionVO().setAutomatic(isAutomaticTransition);
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
 
	public void setDefaultTransition(boolean isDefaultTransition) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			((StateTransition) shapeSelected).getStateTransitionVO().setDefault(isDefaultTransition);
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void setNonstopTransition(boolean isNonstopTransition) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			((StateTransition) shapeSelected).getStateTransitionVO().setNonstop(isNonstopTransition);
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void addRule(EventSupportTransitionVO vo) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			((StateTransition) shapeSelected).addRule(vo);
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void removeRulesByType(String ruleType) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			StateTransition st = ((StateTransition) shapeSelected);
			
			List<EventSupportTransitionVO> toRemove = new ArrayList<EventSupportTransitionVO>();
			
			for (EventSupportTransitionVO stvVO : st.getRules()) {
				if (stvVO.getEventSupportClassType().equals(ruleType)) {
					toRemove.add(stvVO);
				}
			}
			
			st.getRules().removeAll(toRemove);
		}
	}
	
	public void addRules(List<EventSupportTransitionVO> vo) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			StateTransition st = ((StateTransition) shapeSelected);
			for (EventSupportTransitionVO est : vo) {
				st.addRule(est);
			}
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}
	
	public void updateRule(EventSupportTransitionVO vo) {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) 
		{
			if (vo.getId() != null)
			{
				EventSupportTransitionVO rule = null;
				rule = ((StateTransition) shapeSelected).getRule(vo.getId());
				if (rule != null) {
					pnlShapeViewer.getModel().fireModelChanged();
					pnlShapeViewer.repaint();
				}
				
			}
		}
	}
	
	public List<EventSupportTransitionVO> getRules() {
		return ((StateTransition) shapeSelected).getRules();
	}

	public UID getSelectedTransition() {
		return ((StateTransition) shapeSelected).getStateTransitionVO().getPrimaryKey();
	}
	
	public void addRole(MasterDataVO<UID> mdvo) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			((StateTransition) shapeSelected).addRole(mdvo.getPrimaryKey());
			pnlProperties.getTransitionPropertiesPanel().getPnlTransRoles().getModel().setRoles(RoleRepository.getInstance().selectRolesById(((StateTransition) shapeSelected).getRoles()));
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}

	public void removeRole(MasterDataVO<UID> mdvo) throws RemoteException {
		if (shapeSelected != null && shapeSelected instanceof StateTransition) {
			((StateTransition) shapeSelected).removeRole(mdvo.getPrimaryKey());
			pnlProperties.getTransitionPropertiesPanel().getPnlTransRoles().getModel().setRoles(RoleRepository.getInstance().selectRolesById(((StateTransition) shapeSelected).getRoles()));
			pnlShapeViewer.getModel().fireModelChanged();
			pnlShapeViewer.repaint();
		}
	}

	@Override
    public void focusGained(FocusEvent e) {
		pnlShapeViewer.requestFocusInWindow();
	}

	@Override
    public void focusLost(FocusEvent e) {
	}

	/**
	 * @param stategraphvo
	 * @param pnlShapeViewer
	 * @param log
	 * @throws ShapeControllerException
	 */
	public static StateModelLayout showLayout(StateGraphVO stategraphvo, DefaultShapeViewer pnlShapeViewer, Logger log) throws ShapeControllerException{
		StateModelLayout layoutinfo = stategraphvo.getStateModel().getLayout();
		if (layoutinfo == null) {
			layoutinfo = StateGraphVO.newLayoutInfo(stategraphvo);
		} 
		
		final ShapeModel shapemodel = pnlShapeViewer.getModel();
		final Map<UID, StateEntry> mpShapes = CollectionUtils.newHashMap();

		shapemodel.clear();

		shapemodel.setActiveLayer("Default");
		shapemodel.addShape(newStartShape(layoutinfo));

		// add states to shape model:
		for (StateVO statevo : stategraphvo.getStates()) {
			final StateShape stateshape = new StateShape(statevo);
			shapemodel.addShape(stateshape);

			final StateLayout layout = layoutinfo.getStateLayout(statevo.getClientUID());
			if (layout != null) {
				stateshape.setDimension(new Rectangle2D.Double(layout.getX(), layout.getY(), layout.getWidth(), layout.getHeight()));
			}
			else {
				stateshape.setDimension(new Rectangle2D.Double(0d, 0d, 120d, 48d));
			}

			final StateEntry entry = new StateEntry();
			entry.setVo(statevo);
			entry.setShape(stateshape);
			mpShapes.put(statevo.getClientUID(), entry);
		}

		// add transitions to shape model:
		for (StateTransitionVO statetransitionvo : stategraphvo.getTransitions()) {
			final StateTransition statetransition = new StateTransition(statetransitionvo);
			statetransition.setView(pnlShapeViewer);
			final UID iSourceStateId = statetransitionvo.getStateSourceUID();
			final UID iTargetStateId = statetransitionvo.getStateTargetUID();

			final TransitionLayout transitionlayout = layoutinfo.getTransitionLayout(statetransitionvo.getId());
			int start = AbstractShape.CONNECTION_NE;
			int end = AbstractShape.CONNECTION_N;
			if (transitionlayout != null) {
				start = transitionlayout.getConnectionStart();
				end = transitionlayout.getConnectionEnd();
			}

			if (iSourceStateId == null && iTargetStateId != null) {
				// Initial transition to start state
				statetransition.setSourceConnection(new ConnectionPoint(newStartShape(layoutinfo), AbstractShape.CONNECTION_CENTER));
			}

			if (iSourceStateId != null) {
				final StateEntry entry = mpShapes.get(iSourceStateId);
				if (start < 0) {
					log.error("Startpunkt ist f\u00e4lschlicherweise < 0 (" + start + ", readModel())");
					start = 0;
				}
				if (entry != null)
					statetransition.setSourceConnection(new ConnectionPoint(entry.getShape(), start));
			}
			if (iTargetStateId != null) {
				final StateEntry entry = mpShapes.get(iTargetStateId);
				if (end < 0) {
					log.error("Endpunkt ist f\u00e4lschlicherweise < 0 (" + end + ", readModel())");
					end = 0;
				}
				if (entry != null)
					statetransition.setDestinationConnection(new ConnectionPoint(entry.getShape(), end));
			}

			shapemodel.setActiveLayer("Connectors");
			shapemodel.addShape(statetransition);
		}

		// add notes to shape model:
		for (NoteLayout notelayout : layoutinfo.getNotes()) {
			final NoteShape noteshape = new NoteShape();
			noteshape.setDimension(new Rectangle2D.Double(notelayout.getX(), notelayout.getY(), notelayout.getWidth(), notelayout.getHeight()));
			noteshape.setText(notelayout.getText());
			shapemodel.setActiveLayer("Notes");
			shapemodel.addShape(noteshape);
		}
		pnlShapeViewer.repaint();

		return layoutinfo;
	}

	boolean blnSetStateGraph = false;
	public StateModelVO setStateGraph(StateGraphVO stategraphvo) throws NuclosBusinessException {
		this.stategraphvo = stategraphvo;
		
		blnSetStateGraph = true;
		
		selectionChanged(null);
		
		try {
			layoutinfo = showLayout(stategraphvo, pnlShapeViewer, LOG);
		}
		catch (ShapeControllerException ex) {
			clear();
			throw new NuclosBusinessException(ex);
		}
		blnSetStateGraph = false;
		return stategraphvo.getStateModel();
	}

	private static StateModelStartShape newStartShape(StateModelLayout layoutinfo) {
		final StateLayout statelayoutStartShape = layoutinfo.getStateLayout(StateGraphVO.STARTING_STATE_UID);
		final double dStartShapeX = (statelayoutStartShape==null || statelayoutStartShape.getX()<8d) ? 8d : statelayoutStartShape.getX();
		final double dStartShapeY = (statelayoutStartShape==null || statelayoutStartShape.getY()<8d) ? 8d : statelayoutStartShape.getY();
		return new StateModelStartShape(dStartShapeX, dStartShapeY, 12d, 12d);
	}

	public void createNewStateModel(StateModelVO statemodelvo) {
		final ShapeModel model = pnlShapeViewer.getModel();
		model.clear();
		selectionChanged(null);

		stategraphvo = new StateGraphVO(statemodelvo);
		layoutinfo = StateGraphVO.newLayoutInfo(stategraphvo);
		try {
			model.setActiveLayer("Default");
		}
		catch (ShapeControllerException e) {
			LOG.warn("createNewStateModel failed: " + e, e);
		}
		model.addShape(new StateModelStartShape(8d, 8d, 12d, 12d));
		pnlShapeViewer.repaint();
	}

	// @todo document and/or refactor
	public StateGraphVO prepareForSaving(StateModelVO statemodelvo) throws CommonBusinessException {
		try {
			prepareForSaving(pnlShapeViewer.getModel(), stategraphvo, layoutinfo, statemodelvo);
			return stategraphvo;
		}
		catch (ShapeControllerException ex) {
			throw new NuclosBusinessException(ex);
		}
	}

	private static void prepareForSaving(ShapeModel model, StateGraphVO stategraphvo, StateModelLayout layoutinfo, StateModelVO statemodelvo) throws ShapeControllerException, CommonValidationException {
		// save states:
		model.setActiveLayer("Default");
		for (Iterator<Shape> iterShapes = model.getActiveLayer().getShapes().iterator(); iterShapes.hasNext();) {
			final Shape shape = iterShapes.next();
			if (shape instanceof StateShape) {
				final StateShape stateshape = (StateShape) shape;
				if (stateshape.getStateVO().getId() == null) {
					stategraphvo.getStates().add(stateshape.getStateVO());
				}
				if (stateshape.getStateVO().getClientUID() == null || layoutinfo.getStateLayout(stateshape.getStateVO().getClientUID()) == null) {
					StateLayout layout = new StateLayout(stateshape.getX(), stateshape.getY(), stateshape.getWidth(), stateshape.getHeight());

					UID iStateId = stateshape.getStateVO().getClientUID();
					layoutinfo.insertStateLayout(iStateId, layout);
				}
				else {
					UID iStateId = stateshape.getStateVO().getClientUID();
					layoutinfo.updateState(iStateId, stateshape.getX(), stateshape.getY(), stateshape.getWidth(),
							stateshape.getHeight());
				}
			}
			else if (shape instanceof StateModelStartShape) {
				layoutinfo.updateState(StateGraphVO.STARTING_STATE_UID, shape.getX(), shape.getY(), shape.getWidth(), shape.getHeight());
			}
		}

		// save transitions:
		model.setActiveLayer("Connectors");
		for (Iterator<Shape> iterTransitionShapes = model.getActiveLayer().getShapes().iterator(); iterTransitionShapes.hasNext();)
		{
			final StateTransition statetransition = (StateTransition) iterTransitionShapes.next();
			final UID iTempId = statetransition.getStateTransitionVO().getClientUID();

			if (iTempId == null || statetransition.getStateTransitionVO().getId() == null) {
				final TransitionLayout layout = new TransitionLayout(iTempId,
						statetransition.getSourceConnection() != null ? statetransition.getSourceConnection().getTargetPoint() : -2,
						statetransition.getDestinationConnection() != null ? statetransition.getDestinationConnection().getTargetPoint() : -2);
				layoutinfo.insertTransitionLayout(iTempId, layout);
				stategraphvo.getTransitions().add(statetransition.getStateTransitionVO());
			}
			else {
				layoutinfo.updateTransition(iTempId,
						statetransition.getSourceConnection() != null ? statetransition.getSourceConnection().getTargetPoint() : -2,
						statetransition.getDestinationConnection() != null ? statetransition.getDestinationConnection().getTargetPoint() : -2);
			}
		}

		// save notes:
		model.setActiveLayer("Notes");
		layoutinfo.getNotes().clear();
		for (Iterator<Shape> iterNoteShapes = model.getActiveLayer().getShapes().iterator(); iterNoteShapes.hasNext();) {
			final NoteShape note = (NoteShape) iterNoteShapes.next();
			layoutinfo.getNotes().add(new NoteLayout(note.getText(), note.getX(), note.getY(),
					note.getWidth(), note.getHeight()));
		}

		stategraphvo.getStateModel().setLayout(layoutinfo);
		stategraphvo.getStateModel().setName(statemodelvo.getName());
		stategraphvo.getStateModel().setDescription((statemodelvo.getDescription()));
		stategraphvo.validate();
	}

	public void setSelectionTool() {
		pnlShapeViewer.getController().setMouseMode(AbstractShapeController.MOUSE_SELECTION);
		pnlShapeViewer.getController().setDragMode(AbstractShapeController.DRAG_NONE);
		pnlShapeViewer.getController().setSelectedTool(null);
		pnlShapeViewer.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public void setStateTool() {
		pnlShapeViewer.getController().setMouseMode(AbstractShapeController.MOUSE_INSERT_SHAPE);
		pnlShapeViewer.getController().setDragMode(AbstractShapeController.DRAG_NONE);
		pnlShapeViewer.getController().setSelectedTool(StateShape.class);
		pnlShapeViewer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		try {
			pnlShapeViewer.getModel().setActiveLayer("Default");
		}
		catch (ShapeControllerException e) {
			LOG.warn("setStateTool failed: " + e, e);
		}
	}

	public void setTransitionTool() {
		pnlShapeViewer.getController().setMouseMode(AbstractShapeController.MOUSE_INSERT_CONNECTOR);
		pnlShapeViewer.getController().setDragMode(AbstractShapeController.DRAG_NONE);
		pnlShapeViewer.getController().setSelectedTool(StateTransition.class);
		pnlShapeViewer.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		try {
			pnlShapeViewer.getModel().setActiveLayer("Connectors");
		}
		catch (ShapeControllerException e) {
			LOG.warn("setTransitionTool failed: " + e, e);
		}
	}

	public void setNoteTool() {
		pnlShapeViewer.getController().setMouseMode(AbstractShapeController.MOUSE_INSERT_SHAPE);
		pnlShapeViewer.getController().setDragMode(AbstractShapeController.DRAG_NONE);
		pnlShapeViewer.getController().setSelectedTool(NoteShape.class);
		pnlShapeViewer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		try {
			pnlShapeViewer.getModel().setActiveLayer("Notes");
		}
		catch (ShapeControllerException e) {
			LOG.warn("setNoteTool failed: " + e, e);
		}
	}

	public void deleteSelection() {
		pnlShapeViewer.getModel().removeShapes(pnlShapeViewer.getModel().getSelection());
		pnlShapeViewer.repaint();
	}
	
	public void copyStateRights() {
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(pnlProperties.getStatePropertiesPanel().getStateDependantRightsPanel().getRightTransfer(), null);
	}
	
	public void pasteStateRights() {
		Clipboard sysClip = Toolkit.getDefaultToolkit().getSystemClipboard(); 
		Transferable transfer = sysClip.getContents(null); 
		if (pnlProperties.getStatePropertiesPanel().getStateDependantRightsPanel().getRightTransfer() != null) {
			try {
				Object transferO = transfer.getTransferData(RightTransfer.OneRoleRightsDataFlavor.FLAVOR);
				if (transferO instanceof RoleRights)
					pnlProperties.getStatePropertiesPanel().getStateDependantRightsPanel().getRightTransfer().setAllRoleRights((RoleRights) transferO);
				for (ChangeListener cl : lstChangeListeners) {
					cl.stateChanged(new ChangeEvent(this));
				}
			}
			catch(UnsupportedFlavorException e) {
				LOG.warn("pasteStateRights failed: " + e);
			}
			catch(IOException e) {
				LOG.warn("pasteStateRights failed: " + e);
			}
		}
	}

	public void copyTransitionRoles() {
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(pnlProperties.getTransitionPropertiesPanel().getPnlTransRoles().getRoleTransfer(),  null);
	}
	
	public void pasteTransitionRoles() {
		Clipboard sysClip = Toolkit.getDefaultToolkit().getSystemClipboard(); 
		Transferable transfer = sysClip.getContents(null); 
		try {
			Object transferO = transfer.getTransferData(TransitionRolesDataFlavor.FLAVOR);
			if (transferO instanceof List) {
				for (MasterDataVO mdvo : new ArrayList<MasterDataVO>(pnlProperties.getTransitionPropertiesPanel().getPnlTransRoles().getModel().getRoles())) {
					removeRole(mdvo);
				}
				for (MasterDataVO mdvo : (List<MasterDataVO>)transferO) {
					addRole(mdvo);
				}
			}
			for (ChangeListener cl : lstChangeListeners) {
				cl.stateChanged(new ChangeEvent(this));
			}
		}
		catch(UnsupportedFlavorException e) {
			LOG.warn("pasteStateRights failed: " + e);
		}
		catch(IOException e) {
			LOG.warn("pasteStateRights failed: " + e);
		}
	}

	public void zoomIn() {
		if (iCurrentZoom < adZoomSteps.length - 1) {
			iCurrentZoom++;
			actZoomOut.setEnabled(true);
			pnlShapeViewer.setZoom(adZoomSteps[iCurrentZoom] / 100d);
			labZoom.setText(new String(new Double(adZoomSteps[iCurrentZoom]).intValue() + "%"));
		}
		else {
			actZoomIn.setEnabled(false);
		}
	}

	public void zoomOut() {
		if (iCurrentZoom > 0) {
			iCurrentZoom--;
			actZoomIn.setEnabled(true);
			pnlShapeViewer.setZoom(adZoomSteps[iCurrentZoom] / 100d);
			labZoom.setText(new String(new Double(adZoomSteps[iCurrentZoom]).intValue() + "%"));
		}
		else {
			actZoomOut.setEnabled(false);
		}
	}

	public void printStateModel() {
		for (ActionListener al : lstPrintEventListeners) {
			al.actionPerformed(new ActionEvent(this, 0, ""));
		}
	}

	public void clear() {
		pnlShapeViewer.getModel().clear();
		pnlShapeViewer.repaint();
	}

	public boolean stopEditing() {
		/** @todo It might be better to programmatically deselect the currently selected object(s) here.
		 * closeSubForms() would then be called if needed. */
		updateStateProperties();
		// this.closeSubForms();
		return true;
	}

	/**
	 * Returns the Shapeviewer used by the editor
	 * @return DefaultShapeViewer
	 */
	public DefaultShapeViewer getPnlShapeViewer() {
		return pnlShapeViewer;
	}
	
	/**
	 * Returns the btnDefaultTransition used by the editor
	 * @return btnDefaultTransition
	 */
	public JCheckBoxMenuItem getBtnDefaultTransition() {
		return btnDefaultTransition;
	}

	class SortedRuleVOComparator implements Comparator<EventSupportTransitionVO> {

		@Override
		public int compare(EventSupportTransitionVO o1, EventSupportTransitionVO o2) {
			return o2.getOrder().compareTo(o1.getOrder());
		}
		
	}
	
}	// class StateModelEditor
