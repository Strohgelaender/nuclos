//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.gef.shapes.AbstractShape;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.startup.AbstractLocalUserCache;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.StateClientDelegate;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.statemodel.Statemodel;
import org.nuclos.common.statemodel.StatemodelClosure;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.NuclosSubsequentStateNotLegalException;
import org.nuclos.server.statemodel.ejb3.StateFacadeRemote;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateHistoryVO;
import org.nuclos.server.statemodel.valueobject.StateModelLayout;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.nuclos.server.statemodel.valueobject.TransitionLayout;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * Business Delegate for <code>StateFacadeBean</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO:
 * It is VERY strange that all RuntimeException coming for server are 
 * wrapped into a CommonFatalException. (tp)
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
@ManagedResource(
		objectName="nuclos.client.cache:name=StateDelegate",
		description="Nuclos Client StateDelegate",
		currencyTimeLimit=15,
		log=false)
public class StateDelegate extends AbstractLocalUserCache implements MessageListener, DisposableBean, StateClientDelegate {
	
	private static final Logger LOG = Logger.getLogger(StateDelegate.class);
	
	private static StateDelegate INSTANCE;
	
	//
	
	/**
	 * (UID) State -> (StateGraphVO) mapping
	 */
	private transient AtomicReference<Map<UID, StateGraphVO>> mpStateGraphVO;
	
	/**
	 * (UID) State -> StateModelClosure mapping
	 */
	private transient AtomicReference<Map<UID, StatemodelClosure>> mpStatemodelClosure;
	
	// Spring injection
	
	private transient StateFacadeRemote stateFacadeRemote;
	private transient TopicNotificationReceiver tnr;

	
	// end of Spring injection

	StateDelegate() {
		INSTANCE = this;
	}
	
	public final void setTopicNotificationReceiver(TopicNotificationReceiver tnr) {
		this.tnr = tnr;
	}

	public final void setStateFacadeRemote(StateFacadeRemote stateFacadeRemote) {
		this.stateFacadeRemote = stateFacadeRemote;
	}

	@Override
	public void afterPropertiesSet() {
		// Constructor might not be called - as this instance might be deserialized (tp)
		if (INSTANCE == null) {
			INSTANCE = this;
		}
		mpStateGraphVO = new AtomicReference<Map<UID,StateGraphVO>>(new ConcurrentHashMap<UID, StateGraphVO>());
		mpStatemodelClosure = new AtomicReference<Map<UID,StatemodelClosure>>(new ConcurrentHashMap<UID, StatemodelClosure>());
		
		if (!wasDeserialized() || !isValid()) {
			// we could not do a complete invalidation here.
			// statemodels and graphs needs the current user name to get allowed transition.
			// so we can not remember statemodels and transitions here. this depends on user. 
			// Load it lazy as it was before.
			//
			// invalidate(); 
		}
		tnr.subscribe(getCachingTopic(), this);
	}
	
	@ManagedOperation(description="invalidate/clear cache and fill it again")
	public void invalidate() {
		invalidateCache();
		final Map<UID, StateGraphVO> gMap = new ConcurrentHashMap<UID, StateGraphVO>();
		try {
			for (StateModelVO smvo : stateFacadeRemote.getStateModels()) {
				try {
					gMap.put(smvo.getId(), getStateGraph(smvo.getId()));
				} catch (Exception e) {
					// ignore here. this will be tried if no uid is found when trying to access a stategraph or statemodel.
				}
			}
		} catch (Exception e) {
			// ignore here. this will be tried if no uid is found when trying to access a stategraph or statemodel.
		}
		mpStateGraphVO.set(gMap);

		final Map<UID, StatemodelClosure> cMap = new ConcurrentHashMap<UID, StatemodelClosure>();
		for (EntityMeta<?> entityMeta : MetaProvider.getInstance().getAllEntities()) {
			if (entityMeta.isStateModel()) {
				cMap.put(entityMeta.getUID(), stateFacadeRemote.getStatemodelClosureForEntity(entityMeta.getUID()));
			}
		}
		mpStatemodelClosure.set(cMap);
	}
	
	@Override
	public String getCachingTopic() {
		return JMSConstants.TOPICNAME_STATEMODEL;
	}

	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static StateDelegate getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	@Override
	public void onMessage(Message arg0) {
		invalidate();
	}


	/**
	 * §postcondition result != null
	 * 
	 * @param iGenericObjectId
	 * @return the state history of the given leased object. The list is sorted by date, ascending.
	 * @throws NuclosFatalException
	 * @see StateFacadeRemote#getStateHistory(UID, Long)
	 */
	public List<StateHistoryVO> getStateHistory(UID moduleUid, Long iGenericObjectId)
			throws CommonPermissionException, CommonFinderException {
		try {
			final List<StateHistoryVO> result = new ArrayList<StateHistoryVO>(
					stateFacadeRemote.getStateHistory(moduleUid, iGenericObjectId));

			// sort by date, ascending:
			Collections.sort(result, new Comparator<StateHistoryVO>() {
				@Override
                public int compare(StateHistoryVO sh1, StateHistoryVO sh2) {
					return sh1.getCreatedAt().compareTo(sh2.getCreatedAt());
				}
			});

			assert result != null;
			return result;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @return Collection&lt;StateVO&gt; of all states in all state models for the given module
	 */
	public Collection<StateVO> getStatesByModule(UID moduleUid) {
		return getStatemodelClosure(moduleUid).getAllStates();
	}
	
	/**
	 * 
	 * @param moduleUid
	 * @param iStateId
	 * @return
	 */
	public StateVO getState(UID moduleUid, UID iStateId) {
		return getStatemodelClosure(moduleUid).getState(iStateId);
	}

	/**
	 * @return Collection&lt;StateVO&gt; of all states in all state models for the given module
	 */
	public Collection<StateVO> getStatesByModel(UID stateModelUid) {
		try {
			return stateFacadeRemote.getStatesByModel(stateModelUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public StateVO getStatesByModel(UID stateModelUid, UID stateUid) {
		StateVO retVal = null;
		
		try {
			for(StateVO sVO : stateFacadeRemote.getStatesByModel(stateModelUid))
			{
				if (sVO.getId().equals(stateUid))
				{
					retVal = sVO;
					break;
				}
			}
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
		
		return retVal;
	}

	
	/**
	 * @param targetStateUid
	 * @return
	 */
	public StateTransitionVO getStateTransitionByNullAndTargetState(UID targetStateUid) {
		return stateFacadeRemote.findStateTransitionByNullAndTargetState(targetStateUid);		
	}
	
	
	public StateTransitionVO getStateTransitionBySourceAndTargetState(UID sourceStateUid, UID targetStateUid) {
		return stateFacadeRemote.findStateTransitionBySourceAndTargetState(sourceStateUid, targetStateUid);
	}
	
	/**
	 * @param iGenericObjectId
	 * @param mewStateUid
	 * @throws NuclosFatalException
	 * @throws NuclosSubsequentStateNotLegalException
	 * @see StateFacadeRemote#changeStateByUser(UID, Long, UID, String)
	 */
	public void changeState(UID moduleUid, Long iGenericObjectId, UID mewStateUid, String customUsage)
			throws CommonPermissionException, NuclosSubsequentStateNotLegalException,
			CommonFinderException, NuclosBusinessException {
		try {
			stateFacadeRemote.changeStateByUser(moduleUid, iGenericObjectId, mewStateUid, customUsage);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
		catch (CommonCreateException ex) {
			throw new CommonFatalException(ex);
		}
	}	
	
	/**
	 * checks if the given target state id is contained in the list of subsequent states for the given leased objects:
	 * @param moduleUid
	 * @param iGenericObjectId
	 * @param targetStateUid
	 * @return true/false if state change is allowed
	 * @deprecated appears to not be in use
	 */
	public boolean checkTargetState(UID moduleUid, Long iGenericObjectId, UID targetStateUid) {
		try {
			return stateFacadeRemote.checkTargetState(moduleUid, iGenericObjectId, targetStateUid);
		} catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		} catch (CommonFinderException e) {
			throw new CommonFatalException(e.getMessage(), e);
		} catch (CommonPermissionException e) {
			throw new CommonFatalException(e.getMessage(), e);
		}
	}

	/**
	 * method to modify and change state of a given object
	 * 
	 * @param moduleUid module id for plausibility check
	 * @param gowdvo object to change status for
	 * @param newStateUid legal subsequent status id to set for given object
	 * @see StateFacadeRemote#changeStateAndModifyByUser(UID, GenericObjectWithDependantsVO, UID, String)
	 */
	public void changeStateAndModify(UID moduleUid, GenericObjectWithDependantsVO gowdvo, UID newStateUid, String customUsage)
			throws NuclosSubsequentStateNotLegalException, NuclosBusinessException,
			CommonPermissionException, CommonFinderException, CommonRemoveException, CommonStaleVersionException, 
			CommonValidationException {
		
		try {
			stateFacadeRemote.changeStateAndModifyByUser(moduleUid, gowdvo, newStateUid, customUsage);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
		catch (CommonCreateException ex) {
			throw new CommonFatalException(ex);
		} 
		catch (NuclosCompileException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @return Collection&lt;StateModelVO&gt;
	 */
	public Collection<StateModelVO> getAllStateModels() throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		try {
			return stateFacadeRemote.getStateModels();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public StateGraphVO getStateGraph(UID modelUid) throws CommonFinderException, CommonBusinessException {
		try {
			final Map<UID, StateGraphVO> map = mpStateGraphVO.get();
			if (!map.containsKey(modelUid)) {
				final StateGraphVO stateGraphVO = stateFacadeRemote.getStateGraph(modelUid);
	
				// moved from StateGraphVO: NUCLOSINT-844 (b) correct the wrong StateModel-Layouts (after migration due MigrationVm2m5.java)
				StateModelLayout layoutinfo = stateGraphVO.getStateModel().getLayout();
				if (layoutinfo == null) {
					layoutinfo = new StateModelLayout();
				}
				for (StateTransitionVO statetransitionvo : stateGraphVO.getTransitions()) {
					if (layoutinfo.getTransitionLayout(statetransitionvo.getId()) == null) {
						//insert default layout
						layoutinfo.insertTransitionLayout(statetransitionvo.getId(),
							new TransitionLayout(statetransitionvo.getId(), AbstractShape.CONNECTION_NE, AbstractShape.CONNECTION_N));
					}
				}
				map.put(modelUid, stateGraphVO);
			}
			final StateGraphVO result = map.get(modelUid);
			if (result != null && result.isEmpty()) {
				if (map.isEmpty()) {
					LOG.warn("Returning invalid graph value from StateDelegate intern cache because of empty map: " + map);
				} else {
					LOG.warn("Returning invalid graph value from StateDelegate intern cache: " + result);
				}
			}
			return result;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		} catch (CommonPermissionException e) {
			throw new CommonFatalException(e.getMessage(), e);
		}
	}

	public UID setStateGraph(StateGraphVO stategraphvo, IDependentDataMap mpDependants) throws CommonBusinessException {
		try {
			invalidateCache();
			return stateFacadeRemote.setStateGraph(stategraphvo, mpDependants);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @return StateModelId
	 */
	public UID getStateModelId(UsageCriteria usagecriteria) {
		try {
			return stateFacadeRemote.getStateModelId(usagecriteria);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public void removeStateModel(StateModelVO smvo) throws CommonRemoveException, CommonStaleVersionException,
			CommonFinderException, NuclosBusinessRuleException, CommonBusinessException {
		try {
			stateFacadeRemote.removeStateGraph(smvo);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		} catch (CommonPermissionException e) {
			throw new CommonFatalException(e.getMessage(), e);
		}
	}

	@ManagedOperation(description="invalidate/clear cache")
	public void invalidateCache(){
		try {
			mpResourceSIds = null;
			mpStateGraphVO = new AtomicReference<Map<UID,StateGraphVO>>(new ConcurrentHashMap<UID, StateGraphVO>());
			mpStatemodelClosure = new AtomicReference<Map<UID,StatemodelClosure>>(new ConcurrentHashMap<UID, StatemodelClosure>());
		} catch (RuntimeException e) {
			throw new CommonFatalException(e);
		}
	}

	private transient Map<UID, Map<FieldMeta<?>, String>> mpResourceSIds;
	private String getResourceSIdForNameAndKey(UID stateUid, FieldMeta<?> key) {
		try {
			if (mpResourceSIds == null) {
				mpResourceSIds = new HashMap<UID, Map<FieldMeta<?>, String>>();
			}
			if (!mpResourceSIds.containsKey(stateUid)) {
				mpResourceSIds.put(stateUid, stateFacadeRemote.getResourceSIdsForStateUid(stateUid));
			}
			return StringUtils.defaultIfEmpty(mpResourceSIds.get(stateUid).get(key), getState(stateUid).getStatename(LocaleDelegate.getInstance().getLocale()));
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public String getResourceSIdForName(UID stateUid) {
		return getResourceSIdForNameAndKey(stateUid, E.STATE.labelres);
	}

	public String getResourceSIdForDescription(UID stateUid) {
		return getResourceSIdForNameAndKey(stateUid, E.STATE.descriptionres);
	}
	
	public String getResourceSIdForButtonLabel(UID stateUid) {
		return getResourceSIdForNameAndKey(stateUid, E.STATE.buttonRes);
	}

	public StatemodelClosure getStatemodelClosure(UID moduleUid) {
		final Map<UID, StatemodelClosure> map = mpStatemodelClosure.get();
		if (!map.containsKey(moduleUid)) {
			map.put(moduleUid, stateFacadeRemote.getStatemodelClosureForEntity(moduleUid));
		}
		final StatemodelClosure result = map.get(moduleUid);
		if (result == null) {
			if (map == null || map.isEmpty()) {
				LOG.warn("Returning invalid closure value from StateDelegate intern cache because of empty map: " + map);				
			} else {
				LOG.warn("Returning invalid closure value from StateDelegate intern cache: " + result);
			}
		}
		return result;
	}

	public Statemodel getStatemodel(UsageCriteria ucrit) {
		return getStatemodelClosure(ucrit.getEntityUID()).getStatemodel(ucrit);
	}

	@Override
	public synchronized void destroy() {
		tnr.unsubscribe(this);
	}
	
	/**
	 * Returns a list of all transitions for a given statemodel ordered by db-field 'order'
	 * 
	 * @param moduleUid
	 * @return
	 */
	public List<StateTransitionVO> getOrderedStateTransitionsByStatemodel(UID moduleUid) {
		return stateFacadeRemote.getOrderedStateTransitionsByStatemodel(moduleUid);
	}

	/**
	 * Returns a state
	 * 
	 * @param stateUid
	 * @return
	 */
	public StateVO getState(UID stateUid) {
		try {
			return stateFacadeRemote.getState(stateUid);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * copies the permissions of an statemodel from one given role.
	 * @param roleToCopy
	 * @param roleToCopyTo
	 */ 
	public void copyRolePermissions(UID roleToCopy, UID roleToCopyTo) throws NuclosBusinessException {
		try {
			stateFacadeRemote.copyRolePermissions(roleToCopy, roleToCopyTo);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	@ManagedAttribute(description="get the size (number of states) of mpStateGraphVO")
	public int getNumberOfStatesWithGraphVOInCache() {
		return mpStateGraphVO.get().size();
	}
	
	@ManagedAttribute(description="get the size (number of states) of mpStateGraphVO")
	public int getNumberOfStatesWithClosureInCache() {
		return mpStatemodelClosure.get().size();
	}
	
	@ManagedOperation(description="get overview of the size of cached stuff")
	public Map<String,Integer> getSizeStatistics() {
		final Map<String,Integer> result = new TreeMap<String, Integer>();
		int minStates = 9999, maxStates = 0, minTransistions = 9999, maxTransistions = 0;
		int minUserRights = 9999, maxUserRights = 0;
		int minSfRights = 9999, maxSfRights = 0;
		int minRules = 9999, maxRules = 0, minRoles = 9999, maxRoles = 0;
		for (StateGraphVO sg: mpStateGraphVO.get().values()) {
			final int states = sg.getStates().size();
			final int transitions = sg.getTransitions().size();
			minStates = Math.min(minStates, states);
			maxStates = Math.max(maxStates, states);
			minTransistions = Math.min(minTransistions, transitions);
			maxTransistions = Math.max(maxTransistions, transitions);
			for (StateVO s: sg.getStates()) {
				final Pair<Integer,Integer> ur = s.getUserSubformRights().minMaxTuple();
				minUserRights = Math.min(minUserRights, ur.getX());
				maxUserRights = Math.max(maxUserRights, ur.getY());
				
				final Pair<Integer,Integer> sr = s.getUserSubformRights().minMaxTuple();
				minSfRights = Math.min(minSfRights, ur.getX());
				maxSfRights = Math.max(maxSfRights, ur.getY());
			}
			for (StateTransitionVO t: sg.getTransitions()) {
				final List<EventSupportTransitionVO> rules = t.getRules();
				if (rules != null) {
					final int r = rules.size();
					minRules = Math.min(minRules, r);
					maxRules = Math.max(maxRules, r);
				}
				final List<UID> roles = t.getRoleUIDs();
				if (roles != null) {
					final int r = rules.size();
					minRoles = Math.min(minRoles, r);
					maxRoles = Math.max(maxRoles, r);
				}
			}
		}
		result.put("minStates", minStates);
		result.put("maxStates", maxStates);
		result.put("minTransistion", minTransistions);
		result.put("maxTransistion", maxTransistions);
		result.put("minUserRights", minUserRights);
		result.put("maxUserRights", maxUserRights);
		result.put("minSfRights", minSfRights);
		result.put("maxSfRights", maxSfRights);
		result.put("minRules", minRules);
		result.put("maxRules", maxRules);
		result.put("minRoles", minRoles);
		result.put("maxRoles", maxRoles);
		
		int minStateModels = 9999, maxStateModels = 0;
		int minTransitions2 = 9999, maxTransitions2 = 0;
		for (StatemodelClosure c: mpStatemodelClosure.get().values()) {
			final int sm = c.getStatemodels().size();
			minStateModels = Math.min(minStateModels, sm);
			maxStateModels = Math.max(maxStateModels, sm);
			for (Statemodel stm: c.getStatemodels().values()) {
				final Map<UID, Collection<StateTransitionVO>> st = stm.getStateTransitions();
				if (st != null) {
					int sts = st.size();
					minTransitions2 = Math.min(minTransitions2, sts);
					maxTransitions2 = Math.max(maxTransitions2, sts);
				}
			}
		}
		result.put("minStateModels", minStateModels);
		result.put("maxStateModels", maxStateModels);
		result.put("minTransistion2", minTransitions2);
		result.put("maxTransistion2", maxTransitions2);
		
		return result;
	}
	
}	// class StateDelegate
