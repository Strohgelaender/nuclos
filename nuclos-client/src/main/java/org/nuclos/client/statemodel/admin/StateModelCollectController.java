//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.admin;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.prefs.Preferences;

import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;
import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.gef.DefaultShapeViewer;
import org.nuclos.client.gef.ShapeModel;
import org.nuclos.client.gef.shapes.TextShape;
import org.nuclos.client.genericobject.CollectableGenericObjectEntity;
import org.nuclos.client.genericobject.GenericObjectLayoutCache;
import org.nuclos.client.genericobject.valuelistprovider.ProcessCollectableFieldsProvider;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.masterdata.locale.LocaleCollectController;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.statemodel.StateModelEditor;
import org.nuclos.client.statemodel.panels.StateModelEditorPropertiesPanel;
import org.nuclos.client.statemodel.panels.TransitionRulePanel;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.DefaultEditView;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.RefreshValueListAction;
import org.nuclos.client.ui.collect.detail.DetailsPanel;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.client.valuelistprovider.EntityCollectableIdFieldsProvider;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.LafParameter;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateGraphVO;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Controller for collecting state models.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class StateModelCollectController extends NuclosCollectController<UID,CollectableStateModel> {

	private static final Logger LOG = Logger.getLogger(StateModelCollectController.class);

	private final CollectPanel<UID,CollectableStateModel> pnlCollect = new StateModelCollectPanel(E.STATEMODEL.getUID(), false, false);

	private final MasterDataSubFormController<UID> subformctlUsages;
	private final StateModelEditPanel pnlEdit;

	private class StateModelCollectStateListener extends CollectStateAdapter {
		@Override
		public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {

			StateModelCollectController smcc = StateModelCollectController.this;
			final boolean bWriteAllowed = SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid);

			smcc.pnlEdit.getHeader().getDescriptionField().setEnabled(bWriteAllowed);
			smcc.pnlEdit.getHeader().getNameField().setEnabled(bWriteAllowed);

			StateModelEditorPropertiesPanel smepp = 
					smcc.pnlEdit.getStateModelEditor().getStateModelEditorPropertiesPanel();
			smepp.getTransitionPropertiesPanel().getPnlTransRoles().getTblRoles().setEnabled(bWriteAllowed);
			smepp.getTransitionPropertiesPanel().getPnlTransRoles().getBtnAdd().setEnabled(bWriteAllowed);
			smepp.getTransitionPropertiesPanel().getPnlTransRoles().getBtnDelete().setEnabled(bWriteAllowed);
			
			smcc.subformctlUsages.getSubForm().setEnabled(bWriteAllowed);

		}

	}

	@Override
	public void refreshCurrentCollectable() throws CommonBusinessException {
		super.refreshCurrentCollectable();
		
		TransitionRulePanel pnlTransRules = this.pnlEdit.getStateModelEditor().getStateModelEditorPropertiesPanel().getTransitionPropertiesPanel().getPnlTransRules();
		
		List<EventSupportTransitionVO> stateChange = new ArrayList<EventSupportTransitionVO>();
		List<EventSupportTransitionVO> stateChangeFinal = new ArrayList<EventSupportTransitionVO>();
		
		for (EventSupportTransitionVO estVO : EventSupportRepository.getInstance().getEventSupportsByStateModelUid(getSelectedCollectableId())) {
			if (StateChangeRule.class.getCanonicalName().equals(estVO.getEventSupportClassType())) {
				stateChange.add(estVO);				
			}
			else if(StateChangeFinalRule.class.getCanonicalName().equals(estVO.getEventSupportClassType())) {
				stateChangeFinal.add(estVO);
			}
		}
		
		pnlTransRules.getStateChangeRulePanel().reloadModel(stateChange);
		pnlTransRules.getStateChangeFinalRulePanel().reloadModel(stateChangeFinal);
	}
	
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public StateModelCollectController(MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		super(CollectableStateModel.clcte, tabIfAny, state);

		// getSearchStrategy().setCompleteCollectablesStrategy(new CompleteCollectableStateModelsStrategy(this));

		this.initialize(this.pnlCollect);

		final SubForm subformUsages = new SubForm(E.STATEMODELUSAGE.getUID(), false, JToolBar.VERTICAL, UID.UID_NULL, E.STATEMODELUSAGE.statemodel.getUID());

		// the value list for process is to contain the processes belonging to the selected module:
		subformUsages.addColumn(new Column(E.STATEMODELUSAGE.process.getUID()));
		subformUsages.addColumn(new Column(E.STATEMODELUSAGE.nuclos_module.getUID()));
		subformUsages.getColumn(E.STATEMODELUSAGE.process.getUID()).setValueListProvider(new ProcessCollectableFieldsProvider());
		subformUsages.getColumn(E.STATEMODELUSAGE.process.getUID()).addRefreshValueListAction(
				new RefreshValueListAction(E.STATEMODELUSAGE.process.getUID(), E.STATEMODELUSAGE.getUID(), E.STATEMODELUSAGE.nuclos_module.getUID(), "moduleId"));
		EntityCollectableIdFieldsProvider moduleProvider = new EntityCollectableIdFieldsProvider();
		moduleProvider.setParameter(EntityCollectableIdFieldsProvider.PARAM_RESTRICTION, moduleProvider.VALUE_ENTITIES_WITH_STATEMODEL_ONLY);
		moduleProvider.setParameter(EntityCollectableIdFieldsProvider.PARAM_MENUPATH_TYPE, EntityCollectableIdFieldsProvider.VALUE_MENUPATH_TYPE_OPTIONAL);
		subformUsages.getColumn(E.STATEMODELUSAGE.nuclos_module.getUID()).setValueListProvider(moduleProvider);
		this.subformctlUsages = new MasterDataSubFormController(getTab(), this.getDetailsEditView().getModel(),
				this.getEntityName(), subformUsages, this.getPreferences(), this.getEntityPreferences(), valueListProviderCache, state, null);

		pnlEdit = new StateModelEditPanel(StateModelCollectController.this, subformUsages);

		getTab().setLayeredComponent(pnlCollect);

		this.getDetailsPanel().setEditView(DefaultEditView.newDetailsEditView(pnlEdit, pnlEdit.getHeader().newCollectableComponentsProvider()));

		setupShortcutsForTabs(getTab());

		this.pnlEdit.getStateModelEditor().addPrintEventListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent ev) {
				print(ev);
			}
		});

		this.getCollectStateModel().addCollectStateListener(new StateModelCollectStateListener());

		// dividerlocations
		readSplitPaneStateFromPrefs(getCurrentLayoutUid(), getPreferences(), getDetailsPanel());
		PropertyChangeListener dividerChangeListener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				UIUtils.writeSplitPaneStateToPrefs(getCurrentLayoutUid(), getPreferences(), getDetailsPanel());
			}
		};
		pnlEdit.splitpnMain.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, dividerChangeListener);
		pnlEdit.splitpn.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, dividerChangeListener);
	}
	
	protected void readSplitPaneStateFromPrefs(final UID uidLayout, final Preferences prefsParent, final Component rootComp) {
		UIUtils.readSplitPaneStateFromPrefs(getCurrentLayoutUid(), getPreferences(), getDetailsPanel());
	}

	@Override
	protected LayoutRoot<UID> getLayoutRoot() {
		return null;
	}
		
	public UID getCurrentLayoutUid() {
		return E.LAYOUT.getUID();
	}

	@Override
	public void close() {
		subformctlUsages.close();
		pnlEdit.getStateModelEditor().getStateModelEditorPropertiesPanel()
			.getStatePropertiesPanel().getStateDependantRightsPanel().close();
		super.close();
	}
	
	@Override
	protected void setupResultToolBar() {
		
	}

	@Override
	protected void setupDetailsToolBar(){
		super.setupDetailsToolBar();
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_TRANSLATION)) {
			/*final JToolBar toolbarCustomDetails = UIUtils.createNonFloatableToolBar();
			JButton btnLocale = new JButton();
			btnLocale.setIcon(Icons.getInstance().getIconRelate());
			btnLocale.setToolTipText(SpringLocaleDelegate.getMessage("MetaDataCollectController.27", "\u00dcbersetzungstool"));
			btnLocale.setAction(new CommonAbstractAction(btnLocale) {
				@Override
                public void actionPerformed(ActionEvent ev) {
					runLocaleCollectControllerFor();
				}
			});

			toolbarCustomDetails.add(Box.createHorizontalStrut(5));
			toolbarCustomDetails.add(btnLocale);
			toolbarCustomDetails.add(Box.createHorizontalGlue());
			this.getDetailsPanel().setCustomToolBarArea(toolbarCustomDetails);*/
		}

	}

	public void runLocaleCollectControllerFor() {
		UIUtils.runCommandForTabbedPane(this.getTabbedPane(), new CommonRunnable() {
			@Override
            public void run() throws CommonBusinessException {
				Collection<String> collres = new ArrayList<String>();
				UID iModelId = getSelectedCollectableId();
				if (iModelId != null) {
					StateGraphVO stateGraph = StateDelegate.getInstance().getStateGraph(iModelId);
					for (StateVO statevo : stateGraph.getStates()) {
						String sResourceIdForLabel = StateDelegate.getInstance().getResourceSIdForName(statevo.getId());
						String sResourceIdForDescription = StateDelegate.getInstance().getResourceSIdForDescription(statevo.getId());
						if (sResourceIdForLabel != null) collres.add(sResourceIdForLabel);
						if (sResourceIdForDescription != null) collres.add(sResourceIdForDescription);

					}
				}
				final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
				LocaleCollectController cntr = factory.newLocaleCollectController(collres, null);
				cntr.runViewSingleCollectableWithId(LocaleDelegate.getInstance().getDefaultLocale().getLocale());
			}
		});

	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Override
	public void addAdditionalChangeListenersForDetails() {
		this.pnlEdit.getStateModelEditor().addChangeListener(this.changelistenerDetailsChanged);
		this.subformctlUsages.getSubForm().addChangeListener(this.changelistenerDetailsChanged);
	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Override
	public void removeAdditionalChangeListenersForDetails() {
		if (this.pnlEdit != null && this.pnlEdit.getStateModelEditor() != null)
			this.pnlEdit.getStateModelEditor().removeChangeListener(this.changelistenerDetailsChanged);
		if (this.subformctlUsages != null && this.subformctlUsages.getSubForm() != null)
			this.subformctlUsages.getSubForm().removeChangeListener(this.changelistenerDetailsChanged);
	}

	@Override
	public CollectableStateModel findCollectableById(UID sEntity, UID oId) throws CommonBusinessException {
		return new CollectableStateModel(StateDelegate.getInstance().getStateGraph(oId).deepClone());
	}

	@Override
	protected CollectableStateModel findCollectableByIdWithoutDependants(UID sEntity, UID oId) throws CommonBusinessException {
		return findCollectableById(sEntity, oId);
	}

	@Override
	protected void deleteCollectable(CollectableStateModel clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {

		StateDelegate.getInstance().removeStateModel(clct.getStateModelVO());
		
		MasterDataCache.getInstance().invalidate(E.STATEMODEL.getUID());
		MasterDataCache.getInstance().invalidate(E.STATETRANSITION.getUID());
		MasterDataCache.getInstance().invalidate(E.STATE.getUID());
		
		EventSupportDelegate.getInstance().invalidateCaches(E.STATEMODEL);
	}

	@Override
	protected void unsafeFillDetailsPanel(CollectableStateModel clct) throws CommonBusinessException {
		// fill the textfields:
		super.unsafeFillDetailsPanel(clct);

		this.subformctlUsages.getSubForm().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				pnlEdit.getStateModelEditor().setUsages(subformctlUsages.getCollectables());
			}
		});

		// fill the state model editor:
		final StateModelVO statemodelvo = clct.getStateModelVO();
		if (statemodelvo.getId() == null) {
			// @todo this should also be done via setStateGraph:
			this.pnlEdit.getStateModelEditor().createNewStateModel(statemodelvo);
		}
		else {
			this.pnlEdit.getStateModelEditor().setStateGraph(clct.getStateGraphVO());
		}

		this.subformctlUsages.fillSubForm(statemodelvo.getId());

		parseLayout(null);
	}

	public void parseLayout(UID iStatus) {
		this.subformctlUsages.getSubForm().endEditing(); // invoke stop editing here.
		
		List<CollectableEntityObject<UID>> lstSub = this.subformctlUsages.getCollectables();
		for(CollectableEntityObject<UID> md : lstSub) {

			UID iModule = md.getEntityObjectVO().getFieldUids().get(E.STATEMODELUSAGE.nuclos_module.getUID());
			if (iModule == null)
				continue;
			
			UID process = (md.getEntityObjectVO().getFieldUid(E.STATEMODELUSAGE.process) == null) ? null : md.getEntityObjectVO().getFieldUid(E.STATEMODELUSAGE.process);
			
			try {
				String sModuleName = MetaProvider.getInstance().getEntity(iModule).getEntityName();
	
				CollectableGenericObjectEntity e = new CollectableGenericObjectEntity(iModule, sModuleName, Collections.singletonList(new UID()));
				UsageCriteria uc = new UsageCriteria(iModule, process, iStatus, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
				LayoutRoot<?> lRoot = GenericObjectLayoutCache.getInstance().getLayout(e, uc, false, false, null, valueListProviderCache);
	
				JComponent jcomp = lRoot.getRootComponent();
	
				List<JTabbedPane> lst = new ArrayList<JTabbedPane>();
	
				searchTabbedPanes(jcomp, lst);
	
				Map<String, String> mp = getTabbedPaneNames(lst);
	
				this.pnlEdit.getStateModelEditor().getStateModelEditorPropertiesPanel().getStatePropertiesPanel().getModel().setTabModelList(mp);
			} catch (NoSuchElementException e) {
				// ignore.
			}
			break;

		}
	}

	private Map<String, String> getTabbedPaneNames(List<JTabbedPane> lst) {
		final Map<String, String> mp = new HashMap<String, String>();
		for(JTabbedPane tab : lst) {
			for(int i = 0; i < tab.getTabCount(); i++) {
				if(tab.getComponentAt(i).getName() != null) {
					mp.put(tab.getComponentAt(i).getName(), tab.getTitleAt(i));
				}
			}
		}
		return mp;
	}

	private void searchTabbedPanes(JComponent comp, List<JTabbedPane> lst) {
		if(comp instanceof JTabbedPane) {
			lst.add((JTabbedPane)comp);
		}
		if(comp.getComponents().length == 0)
			return;
		for(Component c : comp.getComponents()) {
			if(c instanceof JComponent){
				searchTabbedPanes((JComponent)c, lst);
			}
		}
	}

	@Override
	protected void readValuesFromEditPanel(CollectableStateModel clct, boolean bSearchTab) throws CollectableValidationException {
		// This controller has no search tab:
		assert !bSearchTab;

		// read the text fields:
		super.readValuesFromEditPanel(clct, bSearchTab);
	}

	@Override
	public CollectableStateModel newCollectable() {
		return new CollectableStateModel(new StateGraphVO(new StateModelVO()));
	}

	@Override
	protected CollectableStateModel updateCollectable(CollectableStateModel clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext, boolean isCollectoveProcessing) throws CommonBusinessException {
		throw new CommonFatalException(getSpringLocaleDelegate().getMessage(
				"StateModelCollectController.2","Sammelbearbeitung ist hier noch nicht m\u00f6glich."));
	}

	@Override
	protected CollectableStateModel updateCurrentCollectable(CollectableStateModel clctEdited) throws CommonBusinessException {
		final StateModelEditor statemodeleditor = this.pnlEdit.getStateModelEditor();
		final UID iUpdatedStateModelId = saveStateModelAndUsages(statemodeleditor, clctEdited.getStateModelVO());
		return new CollectableStateModel(StateDelegate.getInstance().getStateGraph(iUpdatedStateModelId));
	}

	@Override
	protected CollectableStateModel insertCollectable(CollectableStateModel clctNew) throws CommonBusinessException {
		final StateModelEditor statemodeleditor = this.pnlEdit.getStateModelEditor();
		final UID iInsertedModelId = saveStateModelAndUsages(statemodeleditor, clctNew.getStateModelVO());
		return new CollectableStateModel(StateDelegate.getInstance().getStateGraph(iInsertedModelId));
	}

	private UID saveStateModelAndUsages(StateModelEditor statemodeleditor, StateModelVO statemodelvo) throws CommonBusinessException {
		// prepare the statemodeleditor for saving:
		final StateGraphVO stategraphvo = statemodeleditor.prepareForSaving(statemodelvo);

		final Collection<CollectableEntityObject<UID>> clctsUsages
			= statemodelvo.getId() == null ? subformctlUsages.getCollectables(true, true, true)
				: subformctlUsages.getAllCollectables(statemodelvo.getId(), null, true, null);

		final IDependentDataMap mpDependants = 
			new DependantCollectableMasterDataMap(E.STATEMODELUSAGE.statemodel, clctsUsages).toDependentDataMap();
		
		final UID intid = StateDelegate.getInstance().setStateGraph(stategraphvo, mpDependants);

		// invalidate client and server caches
		try {
			StateDelegate.getInstance().invalidateCache();
			StateDelegate.getInstance().invalidate();
			SecurityDelegate.getInstance().invalidateCache();
			SecurityCache.getInstance().revalidate();
			MasterDataCache.getInstance().invalidate(E.STATEMODEL.getUID());
			MasterDataCache.getInstance().invalidate(E.STATETRANSITION.getUID());
			MasterDataCache.getInstance().invalidate(E.STATE.getUID());
			EventSupportDelegate.getInstance().invalidateCaches(E.STATEMODEL);
		} catch (RuntimeException e) {
			throw new NuclosFatalException(getSpringLocaleDelegate().getMessage(
					"StateModelCollectController.1","Der serverseitige Cache konnte nicht invalidiert werden!"), e);
		}

		return intid;
	}
	
	/**
	 * Clone doesn't work (yet) for state models.
	 * @return false
	 */
	@Override
	protected boolean isCloneAllowed() {
		return false;
	}
	@Override
	protected boolean isSaveAllowed(){
		return SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid);
	}
	@Override
	protected boolean isDeleteSelectedCollectableAllowed(){
		return SecurityCache.getInstance().isDeleteAllowedForMasterData(entityUid);
	}
	@Override
	protected boolean isNewAllowed(){
		return isSaveAllowed();
	}

	@Override
	protected boolean stopEditingInDetails() {
		return this.pnlEdit.getStateModelEditor().stopEditing() && this.subformctlUsages.stopEditing();
	}

	@Override
	protected String getEntityLabel() {
		return getSpringLocaleDelegate().getMessage("StateModelCollectController.3","Statusmodell");
	}

	/**
	 * The printing of a statusmodel implemented by adding a title to the model
	 * and then the model is printed.
	 * @param ev ActionEvent
	 */
	private void print(ActionEvent ev) {
		final Object oSource = ev.getSource();
		if (!(oSource instanceof StateModelEditor)) {
			return;
		}

		// the changes to the model made here will be ignored
		final boolean bWasDetailsChangedIgnored = isDetailsChangedIgnored();
		setDetailsChangedIgnored(true);

		final StateModelEditor editor = (StateModelEditor) ev.getSource();
		final DefaultShapeViewer pnlShapeViewer = editor.getPnlShapeViewer();
		final ShapeModel model = pnlShapeViewer.getModel();

		final PrinterJob printerjob = PrinterJob.getPrinterJob();
		final PageFormat pageformat = printerjob.defaultPage();
		pageformat.setOrientation(PageFormat.LANDSCAPE);

		final TextShape shapeTitle = new TextShape();
		final Rectangle2D rectDim = model.getShapeDimension();
		final int x = 50;
		final int y = 0;
		final int iWidth = Math.max(50, (int) rectDim.getWidth() - 60);
		final int iHeight = 20;
		shapeTitle.setDimension(new Rectangle2D.Double(x, y, iWidth, iHeight));

		// the screenvalues of the name and the description are used
		final String sName = pnlEdit.getHeader().getNameField().getJTextField().getText();
		final String sDescription = pnlEdit.getHeader().getDescriptionField().getJTextField().getText();

		shapeTitle.setText(sName + " " + sDescription);
		shapeTitle.setBorderSize(0);
		shapeTitle.setColor(model.getView().getBgColor());

		// The title is temporary added to the model
		model.addShape(shapeTitle);

		printerjob.setPrintable(pnlShapeViewer, pageformat);
		if (printerjob.printDialog()) {
			try {
				printerjob.print();
			}
			catch (PrinterException ex) {
				Errors.getInstance().showExceptionDialog(null, ex.getMessage(), ex);
			}
		}
		model.removeShape(shapeTitle);
		setDetailsChangedIgnored(bWasDetailsChangedIgnored);
	}

	private class StateModelCollectPanel extends CollectPanel<UID,CollectableStateModel> {

		StateModelCollectPanel(UID entityId, boolean bSearchPanelAvailable, boolean bShowSearch) {
			super(entityId, bSearchPanelAvailable, bShowSearch, LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Details_Presentation, entityId), ControllerPresentation.DEFAULT);
		}

		@Override
		public ResultPanel<UID,CollectableStateModel> newResultPanel(UID entityId) {
			return new NuclosResultPanel<UID,CollectableStateModel>(entityId, ControllerPresentation.DEFAULT);
		}

		@Override
		public DetailsPanel newDetailsPanel(UID entityId) {
			return new DetailsPanel(entityId, getDetailsPresentation(), ControllerPresentation.DEFAULT, false);
		}
	}
	
	@Override
	public void init() {
		super.init();
		
		this.setupResultToolBar();
		this.setupDetailsToolBar();
		
		ToolBarConfiguration configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Detail, getEntityUid());
		this.pnlCollect.getDetailsPanel().setToolBarConfiguration(configuration);
		configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Result, getEntityUid());
		this.getResultPanel().setToolBarConfiguration(configuration);
		configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Search, getEntityUid());
		this.getSearchPanel().setToolBarConfiguration(configuration);
	}
	
	@Override
	protected void initialize(CollectPanel<UID, CollectableStateModel> pnlCollect) {
		super.initialize(pnlCollect);
		revalidateAdditionalSearchField();
	}
	
}	// class StateModelCollectController
