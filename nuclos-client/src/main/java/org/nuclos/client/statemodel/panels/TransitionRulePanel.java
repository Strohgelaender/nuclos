package org.nuclos.client.statemodel.panels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import org.nuclos.api.rule.StateChangeFinalRule;
import org.nuclos.api.rule.StateChangeRule;
import org.nuclos.client.rule.server.panel.EventSupportSelectionExceptionListener;
import org.nuclos.client.rule.server.panel.EventSupportSelectionPanel;
import org.nuclos.client.rule.server.panel.EventSupportTypeConverter;
import org.nuclos.client.statemodel.StateModelEditor;
import org.nuclos.client.ui.Errors;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;

public class TransitionRulePanel extends JPanel {

	private EventSupportSelectionPanel<EventSupportTransitionVO> pnlTransitions;
	private EventSupportSelectionPanel<EventSupportTransitionVO> pnlTransitionsFinal;
	
	private JCheckBox jckAutoStateChangeTrans;
	private JCheckBox jckDefaultStateChangeTrans;
	private JCheckBox jckNonstopStateChangeTrans;
	private StateModelEditor parent;
	private List<TransitionDataChangeListener> listener = new ArrayList<TransitionDataChangeListener>();
	
	public TransitionRulePanel(StateModelEditor parent) {
		super(new BorderLayout());
		
		this.parent = parent;
		
		// Common properties
		this.add(createCommonProperties(), BorderLayout.NORTH);
		
		// Rules
		this.add(createRulesPanel(), BorderLayout.CENTER);
	}

	private Component createCommonProperties() {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		
		JPanel pnlAutomaticChange = new JPanel(new BorderLayout());
		pnlAutomaticChange.setBorder(BorderFactory.createEmptyBorder(10,5,10,0));
		
		jckAutoStateChangeTrans = new JCheckBox(localeDelegate.getMessage("nuclos.entityfield.statetransition.automatic.label", "Automatischer Statuswechsel", null));
		jckAutoStateChangeTrans.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e != null && e.getSource() != null && e.getSource() instanceof JCheckBox) {
					JCheckBox jbx = (JCheckBox) e.getSource();
					// Only commit real changes
					fireTransitionAutomaticDataChange(jbx.isSelected());
				}
			}
		});
		pnlAutomaticChange.add(jckAutoStateChangeTrans, BorderLayout.NORTH);

		jckDefaultStateChangeTrans = new JCheckBox(localeDelegate.getMessage("nuclos.entityfield.statetransition.default.label", "Standard Statuswechsel", null));
		jckDefaultStateChangeTrans.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e != null && e.getSource() != null && e.getSource() instanceof JCheckBox) {
					JCheckBox jbx = (JCheckBox) e.getSource();
					fireTransitionDefaultDataChange(jbx.isSelected());
				}
			}
		});
		pnlAutomaticChange.add(jckDefaultStateChangeTrans, BorderLayout.CENTER);
		
		jckNonstopStateChangeTrans = new JCheckBox(localeDelegate.getMessage("nuclos.entityfield.statetransition.nonstop.label", "Durchgehender Statuswechsel", null));
		jckNonstopStateChangeTrans.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e != null && e.getSource() != null && e.getSource() instanceof JCheckBox) {
					JCheckBox jbx = (JCheckBox) e.getSource();
					fireTransitionNonstopDataChange(jbx.isSelected());
				}
			}
		});
		pnlAutomaticChange.add(jckNonstopStateChangeTrans, BorderLayout.SOUTH);
		
		return pnlAutomaticChange;
	}

	public void setAutomatic(boolean bAutomatic) {
		jckAutoStateChangeTrans.setSelected(bAutomatic);
	}

	public void setDefault(boolean bDefault) {
		this.jckDefaultStateChangeTrans.setSelected(bDefault);
	}
	
	public void setNonstop(boolean bNonstop) {
		this.jckNonstopStateChangeTrans.setSelected(bNonstop);
	}

	private JComponent createRulesPanel() {
		
		// Exception Listener
		EventSupportSelectionExceptionListener listener = new EventSupportSelectionExceptionListener() {
				@Override
				public void fireException(Exception firedException) {
					Errors.getInstance().showExceptionDialog(TransitionRulePanel.this, firedException.getMessage(), firedException);
				}
		};
		
		// StateChangeRules
		pnlTransitions = new EventSupportSelectionPanel<EventSupportTransitionVO>(
				parent.getStateModelCollectController(), "Regeln vor dem Statuswechsel");
		pnlTransitions.addEventSupportSelectionExceptionListener(listener);
		pnlTransitions.setEventSupportTypeConverter(new EventSupportTypeConverter<EventSupportTransitionVO>() {
			@Override
			public EventSupportTransitionVO convertData(EventSupportSourceVO esVO) {				
				return new EventSupportTransitionVO(esVO.getClassname(), StateChangeRule.class.getCanonicalName(),
					parent.getSelectedTransition(), pnlTransitions.getModel().getRowCount() + 1);
			}

			@Override
			public String getRuleTypeAsString() {
				return StateChangeRule.class.getCanonicalName();
			}
		});
		
		// StateChangeFinalRules		
		pnlTransitionsFinal = new EventSupportSelectionPanel<EventSupportTransitionVO>(
				parent.getStateModelCollectController(), "Regeln nach dem Statuswechsel");
		pnlTransitionsFinal.addEventSupportSelectionExceptionListener(listener);
		pnlTransitionsFinal.setEventSupportTypeConverter(new EventSupportTypeConverter<EventSupportTransitionVO>() {

			@Override
			public String getRuleTypeAsString() {
				return StateChangeFinalRule.class.getCanonicalName();
			}
			
			@Override
			public EventSupportTransitionVO convertData(EventSupportSourceVO esVO) {				
				return new EventSupportTransitionVO(esVO.getClassname(), StateChangeFinalRule.class.getCanonicalName(),
					parent.getSelectedTransition(), pnlTransitionsFinal.getModel().getRowCount() + 1);
			}
		});
		
		JPanel p = new JPanel(new GridLayout(2,1));
		p.add(pnlTransitions);
		p.add(pnlTransitionsFinal);
		return p;
	}

	public EventSupportSelectionPanel<EventSupportTransitionVO> getStateChangeRulePanel() {
		return this.pnlTransitions;
	}
	public EventSupportSelectionPanel<EventSupportTransitionVO> getStateChangeFinalRulePanel() {
		return this.pnlTransitionsFinal;
	}

	public void addTransitionRuleDataChangeListener(TransitionDataChangeListener listener) {
		// Listener for Automatic and default transition checkboxes
		this.listener.add(listener);
		// Listener for tables
		pnlTransitions.addEventSupportDataChangeListener(listener);
		pnlTransitionsFinal.addEventSupportDataChangeListener(listener);
	}
	
	public void fireTransitionAutomaticDataChange(boolean isAutomatic) {
		for (TransitionDataChangeListener tdcl : this.listener) {
			try {
				tdcl.transitionAutomaticDataChange(isAutomatic);				
			} catch (Exception e) {}
		}
	}
	
	public void fireTransitionDefaultDataChange(boolean isDefault) {
		for (TransitionDataChangeListener tdcl : this.listener) {
			try {
				tdcl.transitionDefaultDataChange(isDefault);
			} catch (Exception e) {}
		}
	}
	
	public void fireTransitionNonstopDataChange(boolean isNonstop) {
		for (TransitionDataChangeListener tdcl : this.listener) {
			try {
				tdcl.transitionNonstopDataChange(isNonstop);
			} catch (Exception e) {}
		}
	}
	
	public void setButtonWritable(boolean isWriteAllowed) {
		this.pnlTransitions.setButtonWritable(isWriteAllowed);
		this.pnlTransitionsFinal.setButtonWritable(isWriteAllowed);
	}
}
