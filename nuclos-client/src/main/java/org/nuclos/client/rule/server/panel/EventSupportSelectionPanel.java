package org.nuclos.client.rule.server.panel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.customcode.ServerCodeCollectController;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.statemodel.controller.InsertRuleController;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.common.E;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;

public class EventSupportSelectionPanel<T extends EventSupportVO> extends JPanel{
	
	/**  */
	private static final long serialVersionUID = 1L;

	private boolean isWriteAllowed = true;

	private final JToolBar toolbar = new JToolBar() {
		/**  */
		private static final long serialVersionUID = 1L;

		public void repaint() {
			updateButton(btnAdd);
			updateButton(btnDelete);
			updateButton(btnUp);
			updateButton(btnDown);
			super.repaint();
		}
		
		private void updateButton(JButton button) {
			if (getComponentIndex(button) > -1) {
				getComponent(getComponentIndex(button)).setEnabled(button.isEnabled());
			}
		}
	};

	private final JButton btnAdd = new JButton(new AddRuleAction()) {
		/**  */
		private static final long serialVersionUID = 1L;

		public boolean isEnabled() {
			return isWriteAllowed;
		};
	};
	private final JButton btnDelete = new JButton(new RemoveRuleAction()) {
		/**  */
		private static final long serialVersionUID = 1L;

		public boolean isEnabled() {
			return isWriteAllowed
					&& tblRules != null
					&& tblRules.getSelectedRowCount() > 0;
		};
	};
	private final JButton btnUp = new JButton(new MoveUpRuleAction()) {
		/**  */
		private static final long serialVersionUID = 1L;

		public boolean isEnabled() {
			return isWriteAllowed
					&& tblRules != null
					&& tblRules.getSelectedRow() > 0;
		};
	};
	private final JButton btnDown = new JButton(new MoveDownRuleAction()) {
		/**  */
		private static final long serialVersionUID = 1L;

		public boolean isEnabled() {
			return isWriteAllowed
					&& tblRules != null
					&& tblRules.getSelectedRow() > -1
					&& tblRules.getSelectedRow() < tblRules.getRowCount() - 1;
		};
	};
	private final JTable tblRules = new JTable();
	private final JScrollPane scrlpn = new JScrollPane(tblRules);
	
	private final List<EventSupportSelectionExceptionListener> errListeners = 
			new ArrayList<EventSupportSelectionExceptionListener>();

	private EventSupportSelectionTableModel<T> model = null;
	private NuclosCollectController<?,?> parent;
	private EventSupportTypeConverter<T> converter;
	
	public EventSupportSelectionPanel(NuclosCollectController<?,?> parent, String title) {
		this(parent, title, true);
	}
	
	public EventSupportSelectionPanel(NuclosCollectController<?,?> parent, String title, boolean enabled) {
		super(new BorderLayout());
		
		if (title != null)
			this.setBorder(BorderFactory.createTitledBorder(title));
		
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
	
		this.parent = parent;
		this.model = new EventSupportSelectionTableModel<T>(this);
		
		btnAdd.setIcon(Icons.getInstance().getIconNew16());
		btnAdd.setToolTipText(localeDelegate.getMessage("TransitionRulesPanel.4","Neue Regel zuordnen"));
		btnAdd.setEnabled(true);
		
		btnDelete.setIcon(Icons.getInstance().getIconDelete16());
		btnDelete.setToolTipText(localeDelegate.getMessage("TransitionRulesPanel.5","Zuordnung aufheben"));
		
		btnUp.setIcon(Icons.getInstance().getIconUp16());
		btnUp.setToolTipText(localeDelegate.getMessage("TransitionRulesPanel.2","Nach oben verschieben"));
		
		btnDown.setIcon(Icons.getInstance().getIconDown16());
		btnDown.setToolTipText(localeDelegate.getMessage("TransitionRulesPanel.3","Nach unten verschieben"));
		
		toolbar.setOrientation(JToolBar.HORIZONTAL);
		toolbar.setFloatable(false);
		toolbar.add(btnAdd);
		toolbar.add(btnDelete);
		toolbar.addSeparator();
		toolbar.add(btnDown);
		toolbar.add(btnUp);
		
		tblRules.setBorder(BorderFactory.createLoweredBevelBorder());
		tblRules.setModel(model);
		tblRules.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblRules.setRowMargin(4);
		tblRules.setRowHeight(18);
		tblRules.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblRules.setIntercellSpacing(new Dimension(3, 3));
		
		tblRules.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		
		tblRules.getColumnModel().getColumn(0).setMinWidth(50);
		tblRules.getColumnModel().getColumn(0).setPreferredWidth(180);
		tblRules.getColumnModel().getColumn(0).setMaxWidth(500);
		
		tblRules.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting()) {
					toolbar.repaint();
				}
			}
		});
		tblRules.getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				toolbar.repaint();
			}
		});
		
		tblRules.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					if (e.getClickCount() == 2) {
						final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
						final ServerCodeCollectController rcc = factory.newServerCodeCollectController(E.SERVERCODE.getUID(), null);
						EventSupportSourceVO eventSupportByClassname = 
								EventSupportRepository.getInstance().getEventSupportByClassname(model.getEntryByRowIndex(tblRules.getSelectedRow()).getEventSupportClass());
						if(eventSupportByClassname == null || eventSupportByClassname.getId() == null) {
							EventSupportSelectionPanel.this.fireException(new Exception("Cannot find and open serverrule."));
						}
						else {
							rcc.runViewSingleCollectableWithId(eventSupportByClassname.getId());																			
						}
					}
				} catch (Exception e2) {
					EventSupportSelectionPanel.this.fireException(e2);	
				}
			}
		});
		
		JPanel ruleStateChangePagel = new JPanel(new BorderLayout());
		if (enabled) {
			ruleStateChangePagel.add(toolbar, BorderLayout.NORTH);
		} else {
			ruleStateChangePagel.add(new JLabel(localeDelegate.getMessage("TransitionRulesPanel.7", "Zum Verwalten der Regeln benutzen Sie bitte den Server-Regelmanager.")), BorderLayout.NORTH);
		}
		ruleStateChangePagel.add(scrlpn, BorderLayout.CENTER);
		this.add(ruleStateChangePagel,BorderLayout.CENTER);
		
	}
	
	public void reloadModel(List<T> pTrans) {
		this.model.reloadTable(pTrans);
		toolbar.repaint();
	}
	
	public void setEventSupportTypeConverter(EventSupportTypeConverter<T> converter) {
		this.converter = converter;
	}
	
	public EventSupportSelectionTableModel<T> getModel() {
		return this.model;
	}
	
	public void addEventSupportDataChangeListener(EventSupportSelectionDataChangeListener listener) {
		this.model.addRuleDataChangeListener(listener);
	}
	
	public void addEventSupportSelectionExceptionListener(EventSupportSelectionExceptionListener listener) {
		errListeners.add(listener);
	}
	
	public void setButtonWritable(boolean isWriteAllowed) {
		this.isWriteAllowed = isWriteAllowed;
		toolbar.repaint();
	}
	
	
	public JToolBar getToolBar() {
		return toolbar;
	}

	public JButton getBtnAdd() {
		return btnAdd;
	}

	public JButton getBtnDelete() {
		return btnDelete;
	}

	public JTable getTblRules() {
		return tblRules;
	}

	public JButton getBtnUp() {
		return btnUp;
	}

	public JButton getBtnDown() {
		return btnDown;
	}
	
	public void fireException(Exception e) {
		for (EventSupportSelectionExceptionListener listener : this.errListeners) {
			listener.fireException(e);
		}
	}
	
	class AddRuleAction extends AbstractAction {
		/** */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (converter == null) {
				EventSupportSelectionPanel.this.fireException(new Exception("Converter not found"));	
			}
				
			InsertRuleController irc = new InsertRuleController(EventSupportSelectionPanel.this);
			
			boolean run = false;
			
			try {
				List<String> lstRuleTypes = new ArrayList<String>();
				lstRuleTypes.add(converter.getRuleTypeAsString());
				run = irc.run(SpringLocaleDelegate.getInstance().getMessage("TransitionRulesPanel.4", "Neue Regel zuordnen"),
						EventSupportSelectionPanel.this.model.getEntries(), lstRuleTypes);
				if (run) {
					
					
					for (int idx=0; idx < irc.getRulesPanel().getTblRules().getSelectedRows().length; idx++) {
						EventSupportSourceVO essVOToAdd = irc.getRulesPanel().getRow(irc.getRulesPanel().getTblRules().getSelectedRows()[idx]);
						
						T newEstVO = converter.convertData(essVOToAdd);

						EventSupportSelectionPanel.this.model.addEntry(
								EventSupportSelectionPanel.this.model.getRowCount(), newEstVO);
					}
				}
			} catch (Exception e2) {
				EventSupportSelectionPanel.this.fireException(e2);			
			}	
		}
	}	
	
	class RemoveRuleAction extends AbstractAction {
		/** */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			int selectedRow = EventSupportSelectionPanel.this.tblRules.getSelectedRow();
			EventSupportSelectionPanel.this.model.removeRow(selectedRow);
			if (selectedRow < EventSupportSelectionPanel.this.tblRules.getRowCount()) {
				EventSupportSelectionPanel.this.tblRules.getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
			}
			else if (selectedRow > 0) {
				EventSupportSelectionPanel.this.tblRules.getSelectionModel().setSelectionInterval(selectedRow-1, selectedRow-1);
			}
			toolbar.repaint();
		}
	}
	
	class MoveUpRuleAction extends AbstractAction {
		/** */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			int selectedRow = EventSupportSelectionPanel.this.tblRules.getSelectedRow();
			EventSupportSelectionPanel.this.model.moveUp(selectedRow);
			EventSupportSelectionPanel.this.tblRules.getSelectionModel().setSelectionInterval(selectedRow-1, selectedRow-1);
			toolbar.repaint();
		}
	}
	
	class MoveDownRuleAction extends AbstractAction {
		/** */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			int selectedRow = EventSupportSelectionPanel.this.tblRules.getSelectedRow();
			EventSupportSelectionPanel.this.model.moveDown(selectedRow);
			EventSupportSelectionPanel.this.tblRules.getSelectionModel().setSelectionInterval(selectedRow+1, selectedRow+1);
			toolbar.repaint();
		}
	}
}

