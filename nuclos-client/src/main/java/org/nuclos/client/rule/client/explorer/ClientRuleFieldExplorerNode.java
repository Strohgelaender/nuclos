package org.nuclos.client.rule.client.explorer;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import org.nuclos.client.customcode.ClientCodeCollectController;
import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.rule.client.panel.ClientRuleCapturePanel;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.ValidatingJOptionPane;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.client.ui.tree.TreeNodeAction;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleFieldExplorerNode extends
		ExplorerNode<ClientRuleFieldNode> {

	public ClientRuleFieldExplorerNode(TreeNode treenode) {
		super(treenode);
	}

	public List<TreeNodeAction> getTreeNodeActions(JTree tree) {
		final List<TreeNodeAction> result = new LinkedList<TreeNodeAction>();
		
		// Manual refresh
		result.add(new RefreshAction(tree));

		// Client rule actions
		result.addAll(getClientRuleTreeNodeActions(tree));
			
		final ShowInOwnTabAction actShowInOwnTab = new ShowInOwnTabAction(tree);
		actShowInOwnTab.setEnabled(!this.getTreeNode().needsParent());
		result.add(actShowInOwnTab);
		result.add(TreeNodeAction.newSeparatorAction());
		result.add(new ExpandAction(tree));
		result.add(new CollapseAction(tree));
		
		return result;
	}

	private List<TreeNodeAction> getClientRuleTreeNodeActions(JTree tree) {
		List<TreeNodeAction> retVal = new ArrayList<TreeNodeAction>();
				
		retVal.add(TreeNodeAction.newSeparatorAction());
		
		// Add background rule
		retVal.add(new BackgroundColorRuleAction(tree, getTreeNode()));
		
		// Add calculation rule
		retVal.add(new CalculationRuleAction(tree, getTreeNode()));
		
		retVal.add(TreeNodeAction.newSeparatorAction());
		
		return retVal;
	}
		
	class BackgroundColorRuleAction extends TreeNodeAction {
		private ClientRuleFieldNode node;
		private JTree tree;
		
		public BackgroundColorRuleAction(JTree tree, ClientRuleFieldNode node) {
			super("ADD_BACKGROUNDCOLOR_RULE", "Hintergrundfarbe berechen", tree);
			this.node = node;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			final ClientRuleCapturePanel crcp = 
					new ClientRuleCapturePanel(node.getEntityNode());
			
			ValidatingJOptionPane dialog = new ValidatingJOptionPane(
					null, SpringLocaleDelegate.getInstance().getMessage("nuclos.eventsupport.newInstance.headline", "Neue Regel anlegen"), crcp) {
				@Override
				protected void validateInput() throws ErrorInfo {
					crcp.validateInput();
				}
			};
			
			int answer = dialog.showDialog();
			if (answer == JOptionPane.YES_OPTION) {  
				List<AbstractClientRuleSelectionNode> selectedNodes = crcp.getSelectedNodes();
				
				String rulename = crcp.getRuleName();
				String ruledescription = crcp.getRuleDescription();
					
				UID nuclet = getNucletUid(node.getEntityNode().getNodeId());
				
				final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
				
				try {
					
					ClientCodeCollectController rcc = factory.newClientCodeCollectController(E.CLIENTCODE.getUID(), null);
					CollectableMasterDataWithDependants clltable = rcc.newCollectable();
					
					clltable.setField(E.CLIENTCODE.active.getUID(), new CollectableValueField(Boolean.TRUE));
					clltable.setField(E.CLIENTCODE.debug.getUID(), new CollectableValueField(Boolean.TRUE));
					clltable.setField(E.CLIENTCODE.description.getUID(), new CollectableValueField(ruledescription));
					clltable.setField(E.CLIENTCODE.contextsource.getUID(), new CollectableValueField(createClientRuleContextSource(selectedNodes, rulename, ruledescription)));
			//		clltable.setField(E.CLIENTCODE.source.getUID(), new CollectableValueField(createClientRuleSource(node.getEntityUID(), selectedNodes, rulename, ruledescription)));
					clltable.setField(E.CLIENTCODE.nuclet.getUID(), new CollectableValueIdField(nuclet, nuclet));
				//	clltable.setField(E.CLIENTCODE.type.getUID(),new CollectableValueField(FieldBackgroundColorRule.class.getCanonicalName()));
				
					rcc.runNewWith(clltable);
				} catch (CommonBusinessException e1) {
					Errors.getInstance().showExceptionDialog(tree, e1);
				}
			}	
		}

		private UID getNucletUid(UID nodeId) {
			UID retVal = UID.UID_NULL;
			
			if (nodeId != null) {
				MasterDataVO<UID> masterDataVO = 
						MasterDataCache.getInstance().get(E.ENTITY.getUID(), nodeId);
				
				if (masterDataVO != null && masterDataVO.getFieldUid(E.ENTITY.nuclet) !=null)
					retVal = masterDataVO.getFieldUid(E.ENTITY.nuclet);
				
			}
			
			return retVal;
		}
		
		private String getNucletPackage(UID nodeId) {
			String retVal = "org.nuclet";
			
			UID nuclet = getNucletUid(nodeId);
			if (nuclet.equals(UID.UID_NULL)) {
				MasterDataVO<UID> masterDataVO = 
						MasterDataCache.getInstance().get(E.NUCLET.getUID(), nodeId);
				
				if (masterDataVO != null && masterDataVO.getFieldValue(E.NUCLET.packagefield) !=null)
					retVal = masterDataVO.getFieldValue(E.NUCLET.packagefield);				
			}
			
			return retVal;	
		}
		
//		private String createClientRuleSource(UID nodeId,
//				List<AbstractClientRuleSelectionNode> selectedNodes, String rulename, String ruledescription) {
//			StringBuilder sb = new StringBuilder();
//			
//			String classname = rulename.trim().replace(" ", "");
//			String classContextname = classname.substring(0,1).toUpperCase() + classname.toLowerCase().substring(1) + "Context";
//			
//			sb.append("package " + getNucletPackage(nodeId) + "." + classname.toLowerCase() + ";\n\n");
//			sb.append("import " + FieldBackgroundColorRule.class.getCanonicalName() + ";\n\n");
//			sb.append("public class " + classname + " implements " + FieldBackgroundColorRule.class.getSimpleName() + " {\n");
//			sb.append("\tpublic String calculateBackgroundColor(" + classContextname + " context) {\n");
//			sb.append("\t}\n");
//			sb.append("}");
//			
//			return sb.toString();
//		}

		private String createClientRuleContextSource(
				List<AbstractClientRuleSelectionNode> selectedNodes, String rulename, String ruledescription) {
		
			return null;
		}
		
		
	}
	
	class CalculationRuleAction extends TreeNodeAction {
		private ClientRuleFieldNode node;
		public CalculationRuleAction(JTree tree, ClientRuleFieldNode node) {
			super("ADD_CALCULATION_RULE", "Feld berechen", tree);
			this.node = node;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			this.node.getField();
		}
	}

}
