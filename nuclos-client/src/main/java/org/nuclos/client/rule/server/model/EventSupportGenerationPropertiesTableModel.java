package org.nuclos.client.rule.server.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;

public class EventSupportGenerationPropertiesTableModel extends EventSupportPropertiesTableModel {
	
	private static final Logger LOG = Logger.getLogger(EventSupportGenerationPropertiesTableModel.class);

	// TODO: NEVER use spring stuff in static method or initializers. (tp)
	private static final String COL_EVENTSUPPORT = SpringLocaleDelegate.getInstance().getMessage(
			"EventSupportGenerationPropertyModelColumn.2","EventSupport");
	
	private static final String[] COLUMNS = new String[] {COL_EVENTSUPPORT};
	
	// 
	
	private final List<EventSupportGenerationVO> entries = new ArrayList<EventSupportGenerationVO>();
	
	public EventSupportGenerationPropertiesTableModel(JComponent panel) {
		super(panel);
	}
	
	public void addEntry(EventSupportGenerationVO newEntry) {
		this.entries.add(newEntry);
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object retVal = null;
		
		final EventSupportGenerationVO rowEntry = entries.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			String value = rowEntry.getEventSupportClass();
			final EventSupportSourceVO ese = EventSupportRepository.getInstance().getEventSupportByClassname(value);
			if (ese != null) {
				if (!ese.isActive()) {
					setRowColour(rowIndex, Color.GRAY);
				}
				else {
					setRowColour(rowIndex, Color.BLACK);
				}	
				if (ese.getName() != null) {
					value = ese.getName();
				}
			}
			else {
				// ???
				setRowColour(rowIndex, Color.BLACK);
				LOG.info("event source is null for " + value + " at: " + rowIndex + ", " + columnIndex);
			}
			retVal = value;
			break;
		default:
			break;
		}
		return retVal;
	}

	@Override
	public List<? extends EventSupportVO> getEntries() {
		return entries;
	}

	@Override
	public String[] getColumns() {
		return COLUMNS;
	}
	
	@Override
	public void addEntry(int rowId, EventSupportVO elm) {
		entries.add(rowId, (EventSupportGenerationVO) elm);
	}
	
}
