package org.nuclos.client.rule.server.panel;

import java.awt.*;
import java.util.Enumeration;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.model.EventSupportSourcePropertiesTableModel;

public class EventSupportSourcePropertyPanel extends JPanel{

	private EventSupportSourcePropertiesTableModel model;
	JTable propertyTable;
		
	public EventSupportSourcePropertyPanel() {

		this.model = new EventSupportSourcePropertiesTableModel();
		
		setLayout(new BorderLayout());
		
		createPropertiesTable();		
		
	}

	protected EventSupportSourcePropertiesTableModel getPropertyModel() {
		return this.model;
	}
	
	protected void createPropertiesTable() {
		
		final JTable table = new JTable(getPropertyModel());
		
		table.setFillsViewportHeight(true);
		table.setRowSelectionAllowed(true);
		
		JScrollPane scrollPane = new JScrollPane(table);
		
		setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.LIGHT_GRAY));
		add(scrollPane, BorderLayout.CENTER);
		
		for (int idx=0; idx < table.getColumnCount(); idx++) {
			String columnName = getPropertyModel().getColumnName(idx);
			int newColSize = EventSupportPreferenceHandler.getInstance().getColumnWidth(
					EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_SOURCE_PROPERTIES, columnName);
			table.getColumnModel().getColumn(idx).setPreferredWidth(newColSize);
		}

		table.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
			
			@Override
			public void columnSelectionChanged(ListSelectionEvent e) {}
			
			@Override
			public void columnRemoved(TableColumnModelEvent e) {}
			
			@Override
			public void columnMoved(TableColumnModelEvent e) {}
			
			@Override
			public void columnMarginChanged(ChangeEvent e) {
				DefaultTableColumnModel colModel = (DefaultTableColumnModel) e.getSource();
				Enumeration<TableColumn> columns = colModel.getColumns();
				while (columns.hasMoreElements()) {
					TableColumn col = columns.nextElement();
					EventSupportPreferenceHandler.getInstance().addColumnWidth(
							EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_SOURCE_PROPERTIES, col.getHeaderValue().toString(), col.getWidth());
				}
			}
			
			@Override
			public void columnAdded(TableColumnModelEvent e) {}
		
		});
		
		this.propertyTable = table;
	}

	public JTable getPropertyTable() {
		return this.propertyTable;
	}
	
}
