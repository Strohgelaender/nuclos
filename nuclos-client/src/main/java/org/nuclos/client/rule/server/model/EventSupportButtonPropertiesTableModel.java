package org.nuclos.client.rule.server.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportButtonVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;

public class EventSupportButtonPropertiesTableModel extends
		EventSupportPropertiesTableModel {

	private static final Logger LOG = Logger.getLogger(EventSupportButtonPropertiesTableModel.class);
	
	// TODO: NEVER use spring stuff in static method or initializers. (tp)
	private static final String COL_EVENTSUPPORT = SpringLocaleDelegate.getInstance().getMessage(
			"EventSupportButtonPropertyModelColumn.1","EventSupport");
	
	private static final String[] COLUMNS = new String[] {COL_EVENTSUPPORT};
		
	private final List<EventSupportButtonVO> entries = new ArrayList<EventSupportButtonVO>();
	
	public EventSupportButtonPropertiesTableModel(JComponent panel) {
		super(panel);
	}
	
	public void addEntry(EventSupportButtonVO eseVO)
	{
		entries.add(eseVO);
		fireTableRowsInserted(entries.size(), entries.size());
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object retVal = null;	
		EventSupportButtonVO eventSupportVO = entries.get(rowIndex);
		switch (columnIndex) {
		case 0:
			retVal = eventSupportVO.getEventSupportClass();
			
			final EventSupportSourceVO ese = EventSupportRepository.getInstance().getEventSupportByClassname(
					eventSupportVO.getEventSupportClass());
				
			if (ese != null) {
				if (!ese.isActive()) {
					setRowColour(rowIndex, Color.GRAY);
				}
				else {
					setRowColour(rowIndex, Color.BLACK);
				}
				if (ese.getName() != null) {
					retVal = ese.getName();
				}
			}
			else {
				// ???
				setRowColour(rowIndex, Color.BLACK);
				LOG.info("event source is null for " + eventSupportVO.getEventSupportClass() 
						+ " at: " + rowIndex + ", " + columnIndex);
			}
			break;
		default:
			break;
		}
		return retVal;
	}

	@Override
	public String[] getColumns() {
		return COLUMNS;
	}

	@Override
	public List<? extends EventSupportVO> getEntries() {
		return entries;
	}

	@Override
	public void addEntry(int rowId, EventSupportVO elm) {
		entries.add(rowId, (EventSupportButtonVO) elm);
	}
	
}
