package org.nuclos.client.rule.server.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;
import org.nuclos.server.eventsupport.valueobject.ProcessVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

public class EventSupportEntityPropertiesTableModel extends EventSupportPropertiesTableModel {
	
	private static final Logger LOG = Logger.getLogger(EventSupportEntityPropertiesTableModel.class);
	
	// TODO: NEVER use spring stuff in static method or initializers. (tp)
	private static final String COL_EVENTSUPPORT = SpringLocaleDelegate.getInstance().getMessage("EventSupportEntityPropertyModelColumn.4","EventSupport");
	private static final String COL_STATUS = SpringLocaleDelegate.getInstance().getMessage("EventSupportEntityPropertyModelColumn.2","Status");
	private static final String COL_PROCESS = SpringLocaleDelegate.getInstance().getMessage("EventSupportEntityPropertyModelColumn.3","Aktion");
	private static final String[] COLUMNS = new String[] {COL_EVENTSUPPORT, COL_STATUS, COL_PROCESS};
	private List<EventSupportEventVO> entries = new ArrayList<EventSupportEventVO>();
	
	private Map<UID, Map<UID, List<StateVO>>> states = new HashMap<UID, Map<UID,List<StateVO>>>();
	
	public EventSupportEntityPropertiesTableModel(JComponent panel) {
		super(panel);
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		boolean retVal = false;
		if (columnIndex == 1 || columnIndex == 2)
		{
			retVal = true;
		}
		return retVal;
	}
	
	public void addEntry (EventSupportEventVO eseVO) {
		entries.add(eseVO);
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		
		 String value = aValue == null ? null : (String) aValue;
		 EventSupportEventVO eventSupportEventVO = entries.get(rowIndex);
		 
		 if (columnIndex == 1) {
			 StateVO state = null; 
			 for (StateVO svo : getStatus(eventSupportEventVO.getEntityUID(), eventSupportEventVO.getProcessUID())) {
				 if (svo.toString().equals(value)) {
					 state = svo;
					 break;
				 }
			 }				 

			 UID oldState = eventSupportEventVO.getStateUID();
			 
			 eventSupportEventVO.setStateUID(state != null ? state.getId() : null);
			 try {
				 EventSupportEventVO newEvent = 
						 EventSupportDelegate.getInstance().modifyEventSupportEvent(eventSupportEventVO, oldState, eventSupportEventVO.getProcessUID());
				 entries.remove(rowIndex);
				 entries.add(rowIndex, newEvent);
			 } catch (Exception e) {
				 showAndLogException(e);
			 }
			
		 }			
		 else if (columnIndex == 2) {
			 ProcessVO process = null; 
			 for (ProcessVO pvo : getProcess(eventSupportEventVO.getEntityUID())) {
					if (pvo.getName().equals(value)) {
						process = pvo;
					 break;
				 }
			 }
	
			 UID oldProcess = eventSupportEventVO.getProcessUID();
			 
			 eventSupportEventVO.setProcessUID(process != null ? process.getId() : null);
			 try {
				EventSupportEventVO newEvent = 
						 EventSupportDelegate.getInstance().modifyEventSupportEvent(eventSupportEventVO, eventSupportEventVO.getStateUID(), oldProcess);
				entries.remove(rowIndex);
				 entries.add(rowIndex, newEvent);
			 } catch (Exception e) {
				 showAndLogException(e);
			 }
		
	 	 }
    }
	
	private Collection<ProcessVO> getProcess(UID entityUID) {
		return EventSupportRepository.getInstance().getProcessesByModuleUid(entityUID);
	}

	public List<StateVO> getStatus(UID entityUID, UID processUID) {
		List<StateVO> retVal = new ArrayList<StateVO>();
		
		if (MetaProvider.getInstance().getEntity(entityUID).isStateModel()) {
			
			if (!states.containsKey(entityUID)) {
				states.put(entityUID, new HashMap<UID, List<StateVO>>());
			}
			
			processUID = processUID == null ? UID.UID_NULL : processUID;
			
			if (!states.get(entityUID).containsKey(processUID)) {
				UID modelId = StateDelegate.getInstance().getStateModelId(new UsageCriteria(entityUID, processUID, null, null));
				states.get(entityUID).put(processUID, new ArrayList<StateVO>());
				if (modelId != null) {
					for (StateVO state : StateDelegate.getInstance().getStatesByModel(modelId)) {
						states.get(entityUID).get(processUID).add(state);
					}				
				}
			}
			
			retVal = states.get(entityUID).get(processUID);
			
			Collections.sort(retVal, new Comparator<StateVO>() {
					@Override
					public int compare(StateVO o1, StateVO o2) {
						return o1.getNumeral().compareTo(o2.getNumeral());
					}
			});	
		}
		
		return retVal;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object retVal = null;
	
		EventSupportEventVO esepe = entries.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			retVal = esepe.getEventSupportClass();
			final EventSupportSourceVO ese = EventSupportRepository.getInstance()
					.getEventSupportByClassname(esepe.getEventSupportClass());
			
			if (ese != null && ese.getName() != null && ese.isActive()) {
				setRowColour(rowIndex, Color.BLACK);
				retVal = ese.getName();
			} else {
				setRowColour(rowIndex, Color.GRAY);
			}
			
			break;
		case 1:
			if (esepe.getStateUID() != null) {
				StateVO state = null; 
				for (StateVO svo : getStatus(esepe.getEntityUID(), esepe.getProcessUID())) {
					if (svo.getId().equals(esepe.getStateUID())) {
						state = svo;
						break;
					}
				}	
				retVal = state == null ? " " : state.toString();
			}
			break;
		case 2:
			if (esepe.getProcessUID() != null) {
				ProcessVO process = null; 
				for (ProcessVO pvo : getProcess(esepe.getEntityUID())) {
					if (pvo.getId().equals(esepe.getProcessUID())) {
						process = pvo;
						break;
					}
				}	
				retVal = process == null ? " " : process.getName();
			}
			break;
		default:
			break;
		}
		return retVal;
	}


	@Override
	public List<? extends EventSupportVO> getEntries() {
		return entries;
	}

	@Override
	public String[] getColumns() {
		return COLUMNS;
	}

	@Override
	public void addEntry(int rowId, EventSupportVO elm) {
		entries.add(rowId, (EventSupportEventVO) elm);
	}
	
	public void clear() {
		super.clear();
	}
	
}
