package org.nuclos.client.rule.server.panel;

import java.awt.*;
import java.util.Map;

import javax.swing.*;

import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.model.EventSupportButtonPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;

public class EventSupportButtonProperyPanel extends
		AbstractEventSupportPropertyPanel {

	private EventSupportButtonPropertiesTableModel model;
	
	public EventSupportButtonProperyPanel() {

		this.model = new EventSupportButtonPropertiesTableModel(this);
			
		setLayout(new BorderLayout());
		
		createPropertiesTable();		
	}
	@Override
	protected EventSupportPropertiesTableModel getPropertyModel() {
		return this.model;
	}

	@Override
	protected Map<EventSupportActions, AbstractAction> getActionMapping() {
		return null;
	}

	@Override
	protected ActionToolBar[] getActionToolbarMapping() {
		return null;
	}

	@Override
	protected String getPreferenceNodeName() {
		return EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_LAYOUT;
	}

}
