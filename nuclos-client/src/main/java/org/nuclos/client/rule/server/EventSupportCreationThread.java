package org.nuclos.client.rule.server;

import java.awt.*;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;

public class EventSupportCreationThread extends Thread {
	
	private static final Logger LOG = Logger.getLogger(EventSupportCreationThread.class);
	
	private final Component parentComponent;
	
	public EventSupportCreationThread(Component pc) {
		this.parentComponent = pc;
	}
	
	@Override
	public void run() {
		LOG.info(">>> Class generation");
		try {
			EventSupportDelegate.getInstance().createBusinessObjects();
		} catch(NuclosCompileException nce) {
			LOG.error(nce);
			
			String msg = SpringLocaleDelegate.getInstance().getText("CodeCompilerError.detail");
			if (nce.getErrorMessages() != null) {
				for (ErrorMessage em : nce.getErrorMessages()) {
					msg += "\n" + em.getSource() + " (" + em.getLineNumber() + "," + em.getColumnNumber()+ "):\n" + em.getMessage(null);
				}
			}
			JOptionPane.showMessageDialog(parentComponent,msg, 
					SpringLocaleDelegate.getInstance().getText("CodeCompilerError.title"), JOptionPane.WARNING_MESSAGE);
		} catch (CommonBusinessException | InterruptedException e) {
			LOG.error("Error c", e);
			JOptionPane.showMessageDialog(parentComponent,e.getMessage(),
					SpringLocaleDelegate.getInstance().getText("CodeCompilerError.title"), JOptionPane.WARNING_MESSAGE);
		}
        }
	
}
