package org.nuclos.client.rule.client.panel;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.nuclos.client.rule.client.explorer.AbstractClientRuleSelectionNode;
import org.nuclos.client.rule.client.explorer.ClientRuleEntityNode;
import org.nuclos.client.rule.client.explorer.ClientRuleSelectionNode;
import org.nuclos.client.ui.ValidatingJOptionPane.ErrorInfo;
import org.nuclos.common2.SpringLocaleDelegate;

public class ClientRuleCapturePanel extends JPanel implements TreeSelectionListener{

	private ClientRuleSelectionNode entityNode;
	private JTree tree;
	private ClientRuleCaptureTreeSelectionModel selModel;
	private JTextField name;
	private JTextField beschreibung;
	final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
	
	public ClientRuleCapturePanel(ClientRuleEntityNode entityNode) {
		super(new BorderLayout());
		 
		JPanel layout = new JPanel(new GridBagLayout());
		
		this.entityNode = new ClientRuleSelectionNode(entityNode);
        GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTHWEST;
		c.weightx = 0.0;
		c.gridwidth = 1;
		c.insets = new Insets(5,5,0,5);
		
		c.gridx = 0;
		c.gridy = 0;
		
		layout.add(new JLabel(localeDelegate.getMessage(
				"ClientRuleSelection.2", "Name")), c);
		c.gridx = 1;
		c.weightx = 1.0;
		name = new JTextField();
		layout.add(name, c);
		c.weightx = 0.0;
		c.gridx = 0;
		c.gridy = 1;
		layout.add(new JLabel(localeDelegate.getMessage(
				"ClientRuleSelection.3", "Beschreibung")), c);
		c.gridx = 1;
		c.weightx = 1.0;
		beschreibung = new JTextField();
		layout.add(beschreibung, c);
		c.weightx = 0.0;
		c.gridx = 0;
		c.gridy = 2;
		
		layout.add(new JLabel(localeDelegate.getMessage(
				"ClientRuleSelection.4", "Parameter")), c);
		c.gridx = 1;
		c.weightx = 1.0;
		layout.add(createTreePanel(), c);
		
		this.add(layout, BorderLayout.NORTH);
		
		this.setPreferredSize(new Dimension(500, 500));
	}
		
	public void validateInput() throws ErrorInfo {
	
		if (name == null || name.getText() == null || name.getText().length() == 0) {
			throw new ErrorInfo(localeDelegate.getMessage(
					"ClientRuleSelection.1", "Bitte geben Sie einen Wert an für: ",
					localeDelegate.getMessage(
							"ClientRuleSelection.2", "Name")), name);			
		}
		if (beschreibung == null || beschreibung.getText() == null || beschreibung.getText().length() == 0) {
			throw new ErrorInfo(localeDelegate.getMessage(
					"ClientRuleSelection.1", "Bitte geben Sie einen Wert an für: ", 
						localeDelegate.getMessage(
							"ClientRuleSelection.3", "Beschreibung")), beschreibung);
		}
			
	}
	
	public List<AbstractClientRuleSelectionNode> getSelectedNodes() {
		List<AbstractClientRuleSelectionNode> retVal = new ArrayList<AbstractClientRuleSelectionNode>();
		
		for (TreePath tp : this.selModel.getSelectionPaths()) {
			retVal.add((AbstractClientRuleSelectionNode)tp.getLastPathComponent());
		}
		
		return retVal;
	}
	
	public String getRuleName() {
		return this.name.getText();
	}
	
	public String getRuleDescription() {
		return this.beschreibung.getText();
	}
	
	private JPanel createTreePanel() {
		TreeModel tm = new ClientRuleCaptureTreeModel(entityNode);
		
		selModel = new ClientRuleCaptureTreeSelectionModel(tm);
		tree = new JTree(tm);
		tree.setCellRenderer(new ClientRuleCaptureTreeCellRenderer(new DefaultClientRuleTreeCellRenderer(), selModel)); 
		tree.addMouseListener(new DefaultClientRuleCaptureMouseListener(this));
 
        selModel.addTreeSelectionListener(this); 
        
        JPanel pnl = new JPanel(new BorderLayout());
        JScrollPane jScrollPane = new JScrollPane(tree);
        jScrollPane.setPreferredSize(new Dimension(0, 435));
        pnl.add(jScrollPane, BorderLayout.CENTER);
        
        return pnl;
	}
	
	@Override
	public void valueChanged(TreeSelectionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	class DefaultClientRuleCaptureMouseListener extends MouseAdapter {
		
		private ClientRuleCapturePanel panel;
		private int hotspot = new JCheckBox().getPreferredSize().width; 
		public DefaultClientRuleCaptureMouseListener(ClientRuleCapturePanel panel) {
			this.panel = panel;
		}
		
	    public void mouseClicked(MouseEvent me){ 
	        TreePath path = tree.getPathForLocation(me.getX(), me.getY()); 
	        if(path==null) 
	            return; 
	        if(me.getX()>tree.getPathBounds(path).x+hotspot) 
	            return; 
	 
	        boolean selected = selModel.isPathSelected(path, true); 
	        selModel.removeTreeSelectionListener(panel); 
	 
	        try{ 
	            if(selected) 
	            	selModel.removeSelectionPath(path); 
	            else 
	            	if (path.getLastPathComponent() instanceof AbstractClientRuleSelectionNode && 
	            			((AbstractClientRuleSelectionNode)path.getLastPathComponent()).isSelectable())
	            				selModel.addSelectionPath(path); 
	        } finally{ 
	        	selModel.addTreeSelectionListener(panel); 
	            tree.treeDidChange(); 
	        } 
	    } 
	}

	class DefaultClientRuleTreeCellRenderer extends DefaultTreeCellRenderer  {
		 public Component getTreeCellRendererComponent(JTree tree, Object value,
                 boolean sel,
                 boolean expanded,
                 boolean leaf, int row,
                 boolean hasFocus) {
			 
			 Component retVal = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
			 
			 setClosedIcon(null);
			 setOpenIcon(null);
			 setLeafIcon(null);
			 
			 return retVal;
		 }
	}
	
}
