package org.nuclos.client.rule.client.explorer;

import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleEntityExplorerNode extends ExplorerNode<ClientRuleEntityNode> {

	public ClientRuleEntityExplorerNode(TreeNode treenode) {
		super(treenode);
	}

}
