package org.nuclos.client.rule.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.TransactionalJobRule;
import org.nuclos.client.layout.LayoutDelegate;
import org.nuclos.client.layout.wysiwyg.LayoutXMLContentWriter;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.nuclos.server.attribute.valueobject.LayoutVO;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeRemote;
import org.nuclos.server.eventsupport.valueobject.CommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportButtonVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTypeVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;
import org.nuclos.server.eventsupport.valueobject.ProcessVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.springframework.beans.factory.InitializingBean;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class EventSupportRepository implements InitializingBean {

	private static EventSupportRepository INSTANCE;
	private static final Logger LOG = Logger.getLogger(EventSupportRepository.class);	
	
	public static final UID DEFAULT_NUCLET_UID = UID.UID_NULL;
	
	private final List<EventSupportTypeVO> lstEventSupportTypes = new ArrayList<EventSupportTypeVO>();
	private final List<EventSupportSourceVO> lstEventSupports = new ArrayList<EventSupportSourceVO>();
	private final Map<String, EventSupportSourceVO> mpEventSupportsByClass = CollectionUtils.newHashMap();
	
	private final Map<String, List<EventSupportSourceVO>> mpEventSupportsByType = CollectionUtils.newHashMap();
	
	private final Map<UID, List<ProcessVO>> mpProcessesByEntity = CollectionUtils.newHashMap();
	private final Map<String, List<EventSupportEventVO>> mpEventSupportEventsByClass = CollectionUtils.newHashMap();
	private final Map<String, List<StateModelVO>> mpStatemodelsByClass = CollectionUtils.newHashMap();			
	private final Map<String, List<GeneratorActionVO>> mpGenerationsByClass = CollectionUtils.newHashMap();
	private final Map<String, List<JobVO>> mpJobsByClass = CollectionUtils.newHashMap();
	private final Map<String, List<CommunicationPortVO>> mpCommunicationPortByClass = CollectionUtils.newHashMap();
	
	private final Map<UID, List<EventSupportSourceVO>> mpEventSupportsByNuclet = CollectionUtils.newHashMap();
	private final Map<UID, List<LayoutVO>> mpLayoutsByNuclet = CollectionUtils.newHashMap();
	private final Map<UID, List<ButtonVO>> mpButtonsByLayout = CollectionUtils.newHashMap();
	
	private final Map<UID, List<EventSupportEventVO>> mpEventSupportsByEntity = CollectionUtils.newHashMap();
	private final Map<UID, List<EventSupportTransitionVO>> mpEventSupportsByTransition = CollectionUtils.newHashMap();
	private final Map<UID, List<EventSupportGenerationVO>> mpEventSupportsByGeneration = CollectionUtils.newHashMap();
	private final Map<UID, List<EventSupportJobVO>> mpEventSupportsByJob = CollectionUtils.newHashMap();
	private final Map<UID, List<EventSupportCommunicationPortVO>> mpEventSupportsByCommunicationPort = CollectionUtils.newHashMap();
	
	// all types of rules that are used
	private final Map<UID, List<String>> mpEventSupportTypesByGeneration = CollectionUtils.newHashMap();
	private final Map<UID, List<String>> mpEventSupportTypesByJob = CollectionUtils.newHashMap();
	private final Map<UID, List<String>> mpEventSupportTypesByStatemodel = CollectionUtils.newHashMap();
	
	private Map<String, ErrorMessage> compileErrors = CollectionUtils.newHashMap();
	
	private EventSupportFacadeRemote eventSupportFacadeRemote;

	private Map<UID,Map<UID, List<MasterDataVO<UID>>>> mpNuclosObjectsByNuclet = CollectionUtils.newHashMap();
			
	// Spring injection
	
	public final void setEventSupportFacadeRemote(EventSupportFacadeRemote eventSupportFacadeRemote) {
		this.eventSupportFacadeRemote = eventSupportFacadeRemote;
	}
	
	// end of Spring injection

	EventSupportRepository() {}

	public static EventSupportRepository getInstance() {
		if (INSTANCE == null) {
			INSTANCE = (EventSupportRepository)SpringApplicationContextHolder.getBean("eventSupportRepository");
			INSTANCE.updateEventSupports();
		}
		return INSTANCE;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		updateEventSupports();
	}
	
	public void updateEventSupports() {
		
		lstEventSupportTypes.clear();
		lstEventSupports.clear();
		mpEventSupportsByType.clear();
		mpEventSupportsByClass.clear();
		mpEventSupportsByEntity.clear();
		mpEventSupportsByTransition.clear();
		mpProcessesByEntity.clear();
		mpEventSupportsByJob.clear();
		mpEventSupportsByGeneration.clear();
		mpEventSupportsByCommunicationPort.clear();
		mpEventSupportTypesByGeneration.clear();
		mpEventSupportTypesByStatemodel.clear();
		mpEventSupportTypesByJob.clear();
		mpEventSupportEventsByClass.clear();
		mpStatemodelsByClass.clear();
		mpJobsByClass.clear();
		mpGenerationsByClass.clear();
		mpCommunicationPortByClass.clear();
		mpEventSupportsByNuclet.clear();
		mpLayoutsByNuclet.clear();
		mpButtonsByLayout.clear();

		mpNuclosObjectsByNuclet.clear();
		
		try {
			for (MasterDataVO<?> mdvo : MasterDataCache.getInstance().get(E.NUCLET.getUID())) {
				mpEventSupportsByNuclet.put((UID)mdvo.getPrimaryKey(), new ArrayList<EventSupportSourceVO>());
			}
			mpEventSupportsByNuclet.put(DEFAULT_NUCLET_UID, new ArrayList<EventSupportSourceVO>());
			
			// Cache all useable EventSupport ordered by type
			Collection<EventSupportSourceVO> allEventSupports = eventSupportFacadeRemote.getAllEventSupportsRemote();
			for (EventSupportSourceVO esVO : allEventSupports)
			{
				boolean toNucletPackage = false;
				// check wether EventSupport has been connected to nuclet
				for (UID uid : mpEventSupportsByNuclet.keySet()) {
					if (esVO.getNuclet() != null && !esVO.getNuclet().equals(UID.UID_NULL) && esVO.getNuclet().equals(uid)) {
						mpEventSupportsByNuclet.get(uid).add(esVO);
						toNucletPackage = true;
						break;
					}
				}
				// if not check wether there is pseudo-connection via package equality
				if (!toNucletPackage) {
					toNucletPackage = false;
					if (esVO.getPackage() != null && esVO.getPackage().trim().length() > 0) {
						for (UID uid : mpEventSupportsByNuclet.keySet()) {
							if (!uid.equals(DEFAULT_NUCLET_UID)) {
								MasterDataVO<UID> masterDataVO = 
										MasterDataCache.getInstance().get(E.NUCLET.getUID(), uid);
								if (masterDataVO != null && esVO.getPackage().equals(
										masterDataVO.getFieldValue(E.NUCLET.packagefield))) {
									mpEventSupportsByNuclet.get(uid).add(esVO);
									toNucletPackage = true;
									break;
								}								
							}
						}
					}
					if (!toNucletPackage) 
						mpEventSupportsByNuclet.get(DEFAULT_NUCLET_UID).add(esVO);
				}
				
				mpEventSupportsByClass.put(esVO.getClassname(), esVO);
				
				for (String sInterfaces : esVO.getInterface()) {
					if (!mpEventSupportsByType.containsKey(sInterfaces))
					{
						mpEventSupportsByType.put(sInterfaces, new ArrayList<EventSupportSourceVO>());
					}
					mpEventSupportsByType.get(sInterfaces).add(
							new EventSupportSourceVO(
									new NuclosValueObject<UID>(esVO.getPrimaryKey(), esVO.getCreatedAt(), esVO.getCreatedBy(), esVO.getChangedAt(), esVO.getChangedBy(), esVO.getVersion()), esVO.getName(), esVO.getDescription(), esVO.getClassname(),
									esVO.getInterface(), esVO.getPackage(), esVO.getCreatedAt(), esVO.getNuclet(), esVO.isActive()));
				}
			}
			 
			// Cache all registered EventSupport Types
			for (EventSupportTypeVO c: eventSupportFacadeRemote.getAllEventSupportTypes())
			{
				if (c.getClassname().equals(TransactionalJobRule.class.getCanonicalName())) {
					continue;
				}
				String mspLocaledName = SpringLocaleDelegate.getInstance().getMessage(c.getName(), c.getName());
				String mspLocaledDesc = SpringLocaleDelegate.getInstance().getMessage(c.getDescription(), c.getDescription());
				c.setName(mspLocaledName);
				c.setDescription(mspLocaledDesc);
				lstEventSupportTypes.add(c);
			}
		} catch (CommonPermissionException e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	public List<EventSupportSourceVO> getAllEventSupports() throws NuclosBusinessException, CommonPermissionException {
		final List<EventSupportSourceVO> retVal;
		if (lstEventSupports.isEmpty()) {
			lstEventSupports.addAll(eventSupportFacadeRemote.getAllEventSupportsRemote());
		} 
		retVal = Collections.unmodifiableList(lstEventSupports);
		return retVal;
	}

	public Collection<EventSupportSourceVO> getActiveCustomRules(UID entityUid) {
		//NUCLOSINT-743 get all the user driven rules for this entity
		final List<EventSupportSourceVO> collRules = new ArrayList<EventSupportSourceVO>();
		try {
			Collection<EventSupportEventVO> eventSupportsForEntity =
					EventSupportRepository.getInstance().getEventSupportsForEntity(entityUid);

			for (EventSupportEventVO source : eventSupportsForEntity ) {
				if(CustomRule.class.getCanonicalName().equals(source.getEventSupportClassType())) {
					collRules.add(EventSupportRepository.getInstance().getEventSupportByClassname(source.getEventSupportClass()));
				}
			}
		} catch (Exception e) {
			LOG.error("getActiveCustomRules() failed: " + e, e);
		}

		// remove inactive rules
		CollectionUtils.removeAll(collRules, new Predicate<EventSupportSourceVO>() {
			@Override
			public boolean evaluate(EventSupportSourceVO rulevo) {
				return rulevo == null || !rulevo.isActive();
			}
		});

		return collRules;
	}
	
	public List<EventSupportSourceVO> selectEventSupportsById(String EventSupportType) {
		return mpEventSupportsByType.get(EventSupportType);
	}
		
	public List<EventSupportTypeVO> getEventSupportTypes() {
		return lstEventSupportTypes;
	}
		
	public List<EventSupportSourceVO> getEventSupportsByType(String typename) {
		return mpEventSupportsByType.containsKey(typename) ? mpEventSupportsByType.get(typename) : new ArrayList<EventSupportSourceVO>();
	}
	
	public List<EventSupportSourceVO> getEventSupportsByTypes(String... typename) {
		List<EventSupportSourceVO> retVal = new ArrayList<EventSupportSourceVO>();
		for (int idx=0; idx < typename.length; idx++) {
			List<EventSupportSourceVO> lstEseVo = 
					mpEventSupportsByType.containsKey(typename[idx]) ? mpEventSupportsByType.get(typename[idx]) : new ArrayList<EventSupportSourceVO>();
			for (EventSupportSourceVO ese : lstEseVo) {
				retVal.add(ese);
			}
		}
		return retVal;
	}
	
	public Map<String, List<EventSupportSourceVO>> getEventSupportsByType() {
		return mpEventSupportsByType;
	}
	
	public EventSupportSourceVO getEventSupportByClassname(String classname) {
		EventSupportSourceVO retVal = null;
		if (classname != null && mpEventSupportsByClass.containsKey(classname))
		{
			retVal = mpEventSupportsByClass.get(classname);
		}
		return retVal;
	}
	
	public List<EventSupportSourceVO> searchForEventSupports(final String searchString) throws NuclosBusinessException, CommonPermissionException {
		List<EventSupportSourceVO> retVal = null;
		retVal = CollectionUtils.select(getAllEventSupports(), new Predicate<EventSupportSourceVO>() {
			@Override
			public boolean evaluate(EventSupportSourceVO t) {
				return t.getName().toLowerCase().contains(searchString.toLowerCase());
			}
		});
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}

	public EventSupportTypeVO getEventSupportTypeByName(String classname) {
		EventSupportTypeVO retVal = null;
		for (EventSupportTypeVO esvo : lstEventSupportTypes)
		{
			if (esvo.getClassname().equals(classname))
			{
				retVal = esvo;
				break;
			}
		}
		
		return retVal;
	}
	
	public List<String> getEventSupportTypesByGenerationId(UID genUid) throws CommonPermissionException {
		List<String> retVal = new ArrayList<String>();
		if (mpEventSupportTypesByGeneration.containsKey(genUid)) {
			retVal = mpEventSupportTypesByGeneration.get(genUid);
		} 
		else {
			Collection<EventSupportGenerationVO> lstesgVO = this.getEventSupportsByGenerationUid(genUid);
			for (EventSupportGenerationVO esgVO : lstesgVO) {
				if (!retVal.contains(esgVO.getEventSupportClassType()))
					retVal.add(esgVO.getEventSupportClassType());
			}
			mpEventSupportTypesByGeneration.put(genUid, retVal);
		}
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	

	public List<String> getEventSupportTypesByJobUid(UID jobUid) throws CommonPermissionException {
		List<String> retVal = new ArrayList<String>();
		if (mpEventSupportTypesByJob.containsKey(jobUid)) {
			retVal = mpEventSupportTypesByJob.get(jobUid);
		} 
		else {
			Collection<EventSupportJobVO> lstesgVO = getEventSupportsByJobUid(jobUid);
			for (EventSupportJobVO esgVO : lstesgVO) {
				if (!retVal.contains(esgVO.getEventSupportClassType()))
					retVal.add(esgVO.getEventSupportClassType());
			}
			mpEventSupportTypesByJob.put(jobUid, retVal);
		}
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<String> getEventSupportTypesByStateModelUid(UID modelUid) {
		List<String> retVal = new ArrayList<String>();
		if (mpEventSupportTypesByStatemodel.containsKey(modelUid)) {
			retVal = mpEventSupportTypesByStatemodel.get(modelUid);
		} 
		else try {
			List<EventSupportTransitionVO> lstesgVO = getEventSupportsByStateModelUid(modelUid);
			for (EventSupportTransitionVO esgVO : lstesgVO) {
				if (!retVal.contains(esgVO.getEventSupportClassType()))
					retVal.add(esgVO.getEventSupportClassType());
			}
			mpEventSupportTypesByStatemodel.put(modelUid, retVal);
		} catch (Exception e) {
			LOG.error(e);
		}
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public Collection<EventSupportEventVO> getEventSupportsForEntity(UID entityUid) throws CommonPermissionException {
		Collection<EventSupportEventVO> retVal = new ArrayList<EventSupportEventVO>();
		
		if (mpEventSupportsByEntity.isEmpty()) {
			for (EventSupportEventVO eseVO: eventSupportFacadeRemote.getEventSupportEntities()) {
				final UID targetId = eseVO.getEntityUID() != null ? eseVO.getEntityUID() : eseVO.getIntegrationPointUID();
				if (!mpEventSupportsByEntity.containsKey(targetId))
					mpEventSupportsByEntity.put(targetId, new ArrayList<EventSupportEventVO>());
				
				mpEventSupportsByEntity.get(targetId).add(eseVO);
			}
		}
		
		if (mpEventSupportsByEntity.containsKey(entityUid))
			retVal = mpEventSupportsByEntity.get(entityUid);
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public Collection<ProcessVO> getProcessesByModuleUid(UID entityUid) {
		Collection<ProcessVO> retVal = null;
		if (mpProcessesByEntity.containsKey(entityUid))
		{
			retVal = mpProcessesByEntity.get(entityUid);
		}
		else
		{
			retVal = eventSupportFacadeRemote.getProcessesByModule(entityUid);
			mpProcessesByEntity.put(entityUid, new ArrayList(retVal));				

		}
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}

	public Collection<EventSupportTransitionVO> getEventSupportsByTransitionUid(UID transUid) 
			throws CommonFinderException, CommonPermissionException {
		
		Collection<EventSupportTransitionVO> retVal = new ArrayList<EventSupportTransitionVO>();
		
		if (mpEventSupportsByTransition.isEmpty()) {
			Collection<EventSupportTransitionVO> eventSupportTransitions = eventSupportFacadeRemote.getEventSupportTransitions();
			for (EventSupportTransitionVO eseVO: eventSupportTransitions) {
				if (!mpEventSupportsByTransition.containsKey(eseVO.getTransition()))
					mpEventSupportsByTransition.put(eseVO.getTransition(), new ArrayList<EventSupportTransitionVO>());
				
				mpEventSupportsByTransition.get(eseVO.getTransition()).add(eseVO);
			}
		}
		
		if (mpEventSupportsByTransition.containsKey(transUid))
			retVal = mpEventSupportsByTransition.get(transUid);
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<EventSupportTransitionVO> getEventSupportsByStateModelUid(UID modelUid)
			throws CommonFinderException, CommonPermissionException {
		
		List<EventSupportTransitionVO> retVal = new ArrayList<EventSupportTransitionVO>();
		
		List<StateTransitionVO> orderedTrans = StateDelegate.getInstance().getOrderedStateTransitionsByStatemodel(modelUid);
		if (orderedTrans.size() > 0) {
			for (StateTransitionVO stVO : orderedTrans) {
				Collection<EventSupportTransitionVO> eventSupportsByTransitionId = 
						this.getEventSupportsByTransitionUid(stVO.getPrimaryKey());
				for (EventSupportTransitionVO vo : eventSupportsByTransitionId) {
					if (!retVal.contains(retVal)) {
						retVal.add(vo);
					}
				}
			}			
		}
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<EventSupportGenerationVO> getEventSupportsByGenerationUid(UID genUid) 
			throws CommonPermissionException {
		List<EventSupportGenerationVO> retVal = new ArrayList<EventSupportGenerationVO>();
		
		if (mpEventSupportsByGeneration.isEmpty()) {
			for (EventSupportGenerationVO eseVO: eventSupportFacadeRemote.getEventSupportGenerations()) {
				if (!mpEventSupportsByGeneration.containsKey(eseVO.getGeneration()))
					mpEventSupportsByGeneration.put(eseVO.getGeneration(), new ArrayList<EventSupportGenerationVO>());
				
				mpEventSupportsByGeneration.get(eseVO.getGeneration()).add(eseVO);
			}
		}
		
		if (mpEventSupportsByGeneration.containsKey(genUid))
			retVal = mpEventSupportsByGeneration.get(genUid);
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<EventSupportCommunicationPortVO> getEventSupportsByCommunicationPortUid(UID portUid) 
			throws CommonPermissionException {
		List<EventSupportCommunicationPortVO> retVal = new ArrayList<EventSupportCommunicationPortVO>();
		
		if (mpEventSupportsByCommunicationPort.isEmpty()) {
			for (EventSupportCommunicationPortVO eseVO: eventSupportFacadeRemote.getEventSupportCommunicationPorts()) {
				if (!mpEventSupportsByCommunicationPort.containsKey(eseVO.getPort()))
					mpEventSupportsByCommunicationPort.put(eseVO.getPort(), new ArrayList<EventSupportCommunicationPortVO>());
				
				mpEventSupportsByCommunicationPort.get(eseVO.getPort()).add(eseVO);
			}
		}
		
		if (mpEventSupportsByCommunicationPort.containsKey(portUid))
			retVal = mpEventSupportsByCommunicationPort.get(portUid);
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}

	public List<EventSupportJobVO> getEventSupportsByJobUid(UID jobUid) throws CommonPermissionException {
		List<EventSupportJobVO> retVal = null;
		
		if (mpEventSupportsByJob.isEmpty()) {
			for (EventSupportJobVO eseVO: eventSupportFacadeRemote.getEventSupportJobs()) {
				if (!mpEventSupportsByJob.containsKey(eseVO.getJobControllerUID()))
					mpEventSupportsByJob.put(eseVO.getJobControllerUID(), new ArrayList<EventSupportJobVO>());
				
				mpEventSupportsByJob.get(eseVO.getJobControllerUID()).add(eseVO);
			}
		}
		
		if (mpEventSupportsByJob.containsKey(jobUid)) {
			retVal = mpEventSupportsByJob.get(jobUid);
		}
		
		if (retVal != null) {
			Collections.sort(retVal);
			return retVal;
		}
		
		return Collections.EMPTY_LIST;
	}
	
	public List<EventSupportEventVO> getEventSupportEntitiesByClassname(String classname, String classtype) {
		List<EventSupportEventVO> retVal = new ArrayList<EventSupportEventVO>();
		
		if (mpEventSupportEventsByClass.containsKey(classname)) {
			List<EventSupportEventVO> lstEventSuppotEntities = mpEventSupportEventsByClass.get(classname);
			if (classtype != null) {
				for (EventSupportEventVO eseVO : lstEventSuppotEntities) {
					if (eseVO.getEventSupportClassType().equals(classtype)) {
						retVal.add(eseVO);
					}
				}				
			}
			else {
				retVal = lstEventSuppotEntities;
			}
		}
		else
		{
			List<EventSupportEventVO> lstEventSuppotEntities = eventSupportFacadeRemote.getEventSupportEntitiesByClassname(classname);
			
			mpEventSupportEventsByClass.put(classname, lstEventSuppotEntities);
			
			if (classtype != null) {
				for (EventSupportEventVO eseVO : lstEventSuppotEntities) {
					if (eseVO.getEventSupportClassType().equals(classtype)) {
						retVal.add(eseVO);
					}
				}	
			}
			else
			{
				retVal.addAll(eventSupportFacadeRemote.getEventSupportEntitiesByClassname(classname));				
			}
		}
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<StateModelVO> getStateModelByEventSupportClassname(String classname) throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		List<StateModelVO> retVal = new ArrayList<StateModelVO>();
		if (mpStatemodelsByClass.containsKey(classname)) {
			retVal = mpStatemodelsByClass.get(classname);
		} else {
			retVal  = eventSupportFacadeRemote.getStateModelsByEventSupportClassname(classname);
			mpStatemodelsByClass.put(classname, retVal);
		}
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<JobVO> getJobsByEventSupportClassname(String classname) throws CommonFinderException, CommonPermissionException {
		List<JobVO> retVal;
		if (mpJobsByClass.containsKey(classname)) {
			retVal = mpJobsByClass.get(classname);
		} else {
			retVal  = eventSupportFacadeRemote.getJobsByClassname(classname);
			mpJobsByClass.put(classname, retVal);
		}
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<GeneratorActionVO> getGenerationsByEventSupportClassname(String classname) throws CommonFinderException, CommonPermissionException {
		List<GeneratorActionVO> retVal ;
		
		if (mpGenerationsByClass.containsKey(classname)) {
			retVal = mpGenerationsByClass.get(classname);
		}
		else
		{
			retVal  = eventSupportFacadeRemote.getGenerationsByClassname(classname);
			mpGenerationsByClass.put(classname, retVal);
		}
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<CommunicationPortVO> getCommunicationPortsByEventSupportClassname(String classname) throws CommonFinderException, CommonPermissionException {
		List<CommunicationPortVO> retVal ;
		
		if (mpCommunicationPortByClass.containsKey(classname)) {
			retVal = mpCommunicationPortByClass.get(classname);
		}
		else
		{
			retVal  = eventSupportFacadeRemote.getCommunicationPortByClassname(classname);
			mpCommunicationPortByClass.put(classname, retVal);
		}
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public List<EventSupportSourceVO> getEventSupportSourcesByPackage(UID nucletUid, String sSeachText) {
		List<EventSupportSourceVO> retVal;
		
		if (sSeachText == null ) {
			retVal = mpEventSupportsByNuclet.get(nucletUid);
		}
		else
		{
			retVal = new ArrayList<EventSupportSourceVO>();
			for (EventSupportSourceVO eseVO : mpEventSupportsByNuclet.get(nucletUid)) {
				if (eseVO.getName().toLowerCase().contains(sSeachText.toLowerCase())) {
					retVal.add(eseVO);
				}
			}
		}
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	
	
	
	public boolean containsNuclosObjectsByNucletBySearchString(UID nucletUid, UID NuclosObjectUid, UID NuclosObjectNucletUid, UID fieldCompareUID, String sSearchString) throws CommonFinderException {
		boolean retVal = false;	
		
		if (nucletUid == null)
			nucletUid = UID.UID_NULL;
		
		// create cache if not set already
		if (mpNuclosObjectsByNuclet.isEmpty())
			cacheNuclosObjectsByNuclets(NuclosObjectUid, NuclosObjectNucletUid);
		
		if (mpNuclosObjectsByNuclet.containsKey(NuclosObjectUid) && 
				mpNuclosObjectsByNuclet.get(NuclosObjectUid).containsKey(nucletUid)) {
			for (MasterDataVO<UID> mdVO : mpNuclosObjectsByNuclet.get(NuclosObjectUid).get(nucletUid)) {
				UID nuclet = mdVO.getFieldUid(NuclosObjectNucletUid);
				
				if (((nuclet == null && nucletUid == null) ||  
					 (nuclet != null && nucletUid != null && nuclet.equals(nucletUid))) && 
					  mdVO != null && (sSearchString == null || (sSearchString != null && mdVO.getFieldValue(fieldCompareUID, String.class).toLowerCase().contains(sSearchString.toLowerCase())))) {
					retVal = true;
					break;
				}
			}
		}
		
		return retVal;
	}

	private void cacheNuclosObjectsByNuclets(UID NuclosObjectUid, UID NuclosObjectNucletUid) {
		for (MasterDataVO mdVO : MasterDataDelegate.getInstance().getMasterData(NuclosObjectUid)) {
		
			// Nuclet UID
			UID nuclet = mdVO.getFieldUid(NuclosObjectNucletUid);
			if (nuclet == null)
				nuclet = UID.UID_NULL;
			
			// Entities not cached yet
			if (!mpNuclosObjectsByNuclet.containsKey(NuclosObjectUid)) {
				mpNuclosObjectsByNuclet.put(NuclosObjectUid, new HashMap<UID, List<MasterDataVO<UID>>>());
			}
			
			Map<UID, List<MasterDataVO<UID>>> map = mpNuclosObjectsByNuclet.get(NuclosObjectUid);
			if (!map.containsKey(nuclet))
				map.put(nuclet, new ArrayList<MasterDataVO<UID>>());
			
			map.get(nuclet).add(mdVO);
		}
	}
	
	public boolean findLayoutsBySearchString(UID nucletUid, String sSearchString) 
			throws CommonFinderException, CommonPermissionException {
		boolean retVal = false;
		for (LayoutVO lay: getLayoutsByNuclet(nucletUid)) {
			if (lay.getName().toLowerCase().contains(sSearchString.toLowerCase()))
				return true;
		}
		return retVal;
	}
	
	public List<MasterDataVO<UID>> getNuclosObjectsByNuclet(UID nucletUid, UID NuclosObjectUid, UID NuclosObjectNucletUid) throws CommonFinderException {
		
		List<MasterDataVO<UID>> retVal = new ArrayList<MasterDataVO<UID>>();
		
		nucletUid = nucletUid != null ? nucletUid : UID.UID_NULL;
		
		// create cache if not set already
		if (mpNuclosObjectsByNuclet.isEmpty() || !mpNuclosObjectsByNuclet.containsKey(NuclosObjectUid))
			cacheNuclosObjectsByNuclets(NuclosObjectUid, NuclosObjectNucletUid);
		
		if (mpNuclosObjectsByNuclet.get(NuclosObjectUid) != null && mpNuclosObjectsByNuclet.get(NuclosObjectUid).containsKey(nucletUid))
			retVal = mpNuclosObjectsByNuclet.get(NuclosObjectUid).get(nucletUid);
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;		
	}
	
	public List<EventSupportVO> getEventSupportTargets(EventSupportSourceVO eventSupportByClassname) 
			throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		List<EventSupportVO> retVal = new ArrayList<EventSupportVO>();
		
		// Entities
		List<EventSupportEventVO> entities = getEventSupportEntitiesByClassname(eventSupportByClassname.getClassname(), null);
		if (entities != null && entities.size() > 0) {
			retVal.addAll(entities);
		}
		
		// Statemodels
		List<StateModelVO> states = getStateModelByEventSupportClassname(eventSupportByClassname.getClassname());
		if (states != null && states.size() > 0) {
			for (StateModelVO state : states) {
				Collection<EventSupportTransitionVO> eventSupportsByStateId = getEventSupportsByStateModelUid(state.getId());
				for (EventSupportTransitionVO trans : eventSupportsByStateId) {
					if (eventSupportByClassname.getClassname().equals(trans.getEventSupportClass()))
							retVal.add(trans);
				}
			}
		}
		
		// Generations
		List<GeneratorActionVO> gens = getGenerationsByEventSupportClassname(eventSupportByClassname.getClassname());
		if (gens != null && gens.size() > 0) {
			for (GeneratorActionVO state : gens) {
				Collection<EventSupportGenerationVO> eventSupportsByStateId = getEventSupportsByGenerationUid(state.getId());
				retVal.addAll(eventSupportsByStateId);
			}
		}
		
		// Communication ports
		List<CommunicationPortVO> ports = getCommunicationPortsByEventSupportClassname(eventSupportByClassname.getClassname());
		if (ports != null && ports.size() > 0) {
			for (CommunicationPortVO port : ports) {
				Collection<EventSupportCommunicationPortVO> eventSupportsByCommunicationPortId = getEventSupportsByCommunicationPortUid(port.getId());
				retVal.addAll(eventSupportsByCommunicationPortId);
			}
		}
		
		// Jobs
		List<JobVO> jobsByEventSupportClassname = getJobsByEventSupportClassname(eventSupportByClassname.getClassname());
		if (jobsByEventSupportClassname != null && jobsByEventSupportClassname.size() > 0) {
			for (JobVO job : jobsByEventSupportClassname) {
				Collection<EventSupportJobVO> eventSupportsByJobId = getEventSupportsByJobUid(job.getId());
				retVal.addAll(eventSupportsByJobId);
			}
		}

		return retVal;
	}

	
	public List<LayoutVO> getLayoutsByNuclet(UID nucletUid) throws CommonFinderException, CommonPermissionException {
		List<LayoutVO> retVal = new ArrayList<LayoutVO>();
		if (mpLayoutsByNuclet.isEmpty()) {
			for (LayoutVO lvo : LayoutDelegate.getInstance().getLayouts()) {
				boolean layoutHasButtons = false;
				List<ButtonVO> buttonsByLayout = getButtonsByLayout(lvo);
				if (buttonsByLayout.size() > 0) {
					if (!mpLayoutsByNuclet.containsKey(lvo.getNucletUid() == null ? 0 : lvo.getNucletUid()))
						mpLayoutsByNuclet.put(lvo.getNucletUid() == null ? UID.UID_NULL : lvo.getNucletUid(), new ArrayList<LayoutVO>());
					
					mpLayoutsByNuclet.get(lvo.getNucletUid() == null ? UID.UID_NULL : lvo.getNucletUid()).add(lvo);					
				}
			}
		}
		
		if (mpLayoutsByNuclet.containsKey(nucletUid == null ? 0 : nucletUid))
			retVal = mpLayoutsByNuclet.get(nucletUid == null ? 0 : nucletUid);
			
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}

	public List<ButtonVO> getButtonsByLayout(UID layoutUid) {
		List<ButtonVO> list = new ArrayList<ButtonVO>();
		if(!mpButtonsByLayout.isEmpty()) {
			list = mpButtonsByLayout.get(layoutUid);
		}
		
		return list;
	}
	
	public List<ButtonVO> getButtonsByLayout(LayoutVO layout) throws CommonFinderException, CommonPermissionException {	
		List<ButtonVO> retVal = new ArrayList<ButtonVO>();
		if(mpButtonsByLayout.containsKey(layout.getId())) {
			retVal = mpButtonsByLayout.get(layout.getId());	
		}
		else {
			LayoutXMLContentWriter writer = new LayoutXMLContentWriter();
			
			try {
				if(layout.getLayoutML() != null) {
					writer.parse(layout.getLayoutML());
					NodeList buttons = writer.getButtons();
					for (int idx=0; idx < buttons.getLength(); idx++) {
						
						Node curButton = buttons.item(idx);
						
						String sName = null;
						String sDescription = null;
						EventSupportButtonVO source = null;
						
						// Name des Button
						Node btnName = writer.getAttribute(curButton, LayoutMLConstants.ATTRIBUTE_NAME);
						if (btnName != null)			
							sName = btnName.getNodeValue();
						// Description
						Node btnDescription = writer.getAttribute(curButton, LayoutMLConstants.ATTRIBUTE_LABEL);
						if (btnDescription != null)			
							sDescription = btnDescription.getNodeValue();
						
						// Evtl. Regeln fue diesen Button
						Node child = writer.getChild(curButton, LayoutMLConstants.ELEMENT_PROPERTY);
						if (child != null) {
							Node name = writer.getAttribute(child, LayoutMLConstants.ATTRIBUTE_NAME);
							if (name != null && STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT.equals(name.getNodeValue())) {
								Node value = writer.getAttribute(child, LayoutMLConstants.ATTRIBUTE_VALUE);
								if (value != null) {
									EventSupportSourceVO eventSupportSourceByName = getEventSupportSourceByName(value.getNodeValue());
									if (eventSupportSourceByName != null)
										source = new EventSupportButtonVO(0, eventSupportSourceByName.getClassname(), null);
								}
							}
						}
						if (source != null) {
							retVal.add(new ButtonVO(sName, layout.getId(), sDescription, source));						
						}
					}
					mpButtonsByLayout.put(layout.getId(), retVal);
				}
			} catch (ParserConfigurationException e) {
				LOG.error("Error parsing layoutML: " + e);
			} catch (SAXException e) {
				LOG.error("Error parsing layoutML: " + e);
			} catch (IOException e) {
				LOG.error("Error reading layoutML: " + e);
			}
		}
		
		return retVal != null ? retVal : Collections.EMPTY_LIST;
	}
	
	public EventSupportSourceVO getEventSupportSourceByName(String name) {
		EventSupportSourceVO retVal = null;
		for (EventSupportSourceVO source : getEventSupportsByType(CustomRule.class.getCanonicalName())) {
			if (name.equals(source.getName()) || name.equals(source.getClassname())) {
				retVal = source;
				break;
			}
		}
		return retVal;
	}
	
	public static class ButtonVO {
		
		private String sName;
		private String sDescription;
		UID layoutUid;
		private EventSupportButtonVO eseVORule;
		
		public ButtonVO(String name, UID pLayoutUid, String description, EventSupportButtonVO rule) {
			this.sName = name;
			this.sDescription = description;
			this.layoutUid = pLayoutUid;
			this.eseVORule = rule;
		}

		public String getName() {
			return sName;
		}

		public String getDescription() {
			return sDescription;
		}

		public UID getLayoutUid() {
			return layoutUid;
		}

		public EventSupportButtonVO getRule() {
			return eseVORule;
		}
	}
	
	public Map<String, ErrorMessage> getCompileExceptionMessages(boolean reloadFromServer) {
		if (reloadFromServer)
			this.compileErrors = EventSupportDelegate.getInstance().getCompileExceptionMessages();
		
		return this.compileErrors;
	}
	
	public void invalidate(EventSupportEventVO event) {
		if (event != null) {
			mpEventSupportEventsByClass.remove(event.getEventSupportClass());
			mpEventSupportsByCommunicationPort.clear();
			mpEventSupportsByEntity.clear();
		}
	}
	
	public void invalidate(EventSupportTransitionVO transition) {
		if (transition != null) {
			mpEventSupportsByTransition.clear();
			mpEventSupportTypesByStatemodel.clear();
		}
	}
	
	public void invalidate(EventSupportJobVO job) {
		if (job != null) {
			mpEventSupportsByJob.clear();
			mpEventSupportTypesByJob.clear();
		}
	}
	
	public void invalidate(EventSupportGenerationVO generation) {
		if (generation != null) {
			mpEventSupportsByGeneration.clear();
			mpEventSupportTypesByGeneration.clear();
		}
	}
	
	public void invalidate(EventSupportCommunicationPortVO communicationPort) {
		if (communicationPort != null) {
			mpEventSupportsByCommunicationPort.clear();
		}
	}
}

