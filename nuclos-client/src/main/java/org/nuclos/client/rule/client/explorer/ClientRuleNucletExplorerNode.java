package org.nuclos.client.rule.client.explorer;

import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleNucletExplorerNode extends ExplorerNode<ClientRuleNucletNode> {

	public ClientRuleNucletExplorerNode(TreeNode treenode) {
		super(treenode);
	}
	
}
