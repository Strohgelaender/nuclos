package org.nuclos.client.rule.server;

import java.io.Reader;
import java.io.StringReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.log4j.Logger;

import jsyntaxpane.actions.ScriptAction;

/**
 * This class is a replacement for jsyntaxpane.actions.ScriptAction and is needed for Java 8
 * Jsyntaxpane is using jsyntaxpane.actions.ScriptAction to load a build in script 'insertdata.js'
 * Since Java 8 the new Nashorn-Javascript Engine is used, which is not fully compatible to Rhino-Engine. 
 * To ensure compatibility, a call of load('nashorn:mozilla_compat.js') is needed.
 * (see: https://wiki.openjdk.java.net/display/Nashorn/Rhino+Migration+Guide 
 *  and http://stackoverflow.com/questions/22502630/switching-from-rhino-to-nashorn)
 * 
 * This class overrides jsyntaxpane.actions.ScriptAction and loads content of SCRIPT_REPLACEMENT 
 * instead of insertdate.js.
 */
public class JSyntaxPaneReplaceScriptAction extends ScriptAction {
	
	static final Logger LOG = Logger.getLogger(JSyntaxPaneReplaceScriptAction.class);
	
	static ScriptEngine scriptEngine;
	
	/**
	 * This is a replacement for jsyntaxpane-0.9.5-b29.jar/META-INF/services/jsyntaxpane/scripts/insertdate.js
	 * It includes load('nashorn:mozilla_compat.js') to ensure compatibility to Rhino
	 */
	static String SCRIPT_REPLACEMENT = "load('nashorn:mozilla_compat.js')\n" +
			"importPackage(java.util)\n" +
			"importClass(javax.swing.JOptionPane)\n" +
			"function putDate() {\n" +
			"TARGET.replaceSelection('This is a dummy proc that inserts the Current Date:' + new Date());\n" +
			"TARGET.replaceSelection('Tab Size of doc = ' + AU.getTabSize(TARGET));\n" +
			"}";
	
	static  {
		scriptEngine = new ScriptEngineManager().getEngineByExtension("js");
	}
	
	public static boolean isReplacementNeeded() {
		return scriptEngine.getClass().getCanonicalName().contains("Nashorn");
	}
	
	@Override
	public void getScriptFromURL(String url) {
		if (!"jsyntaxpane/scripts/insertdate.js".equals(url)) {
			super.getScriptFromURL(url);
		}
		else {
			Reader reader = new StringReader(SCRIPT_REPLACEMENT);
			try {
				scriptEngine.eval(reader);
				LOG.info("jsyntaxpane.actions.ScriptAction is replaced by " + getClass());
			} catch (ScriptException se) {
				LOG.error(se);
			}
		}
	}
}
