package org.nuclos.client.rule.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;
import org.nuclos.common2.ClientPreferences;

public class EventSupportPreferenceHandler {
	
	private EventSupportPreferenceHandler() {}
	
	public static final String PREF_NODES_EVENTSUPPORTS = "eventsupports";
	
	
	public static final String PREF_NODE_EVENTSUPPORT_SOURCES = "eventsupportsSources";
	public static final String PREF_NODE_EVENTSUPPORTS_TARGETS = "eventsupportsTargets";
	
	public static final String PREF_NODE_EVENTSUPPORT_MAIN_SLIDER = "eventsupportMainSlider";
	public static final String PREF_NODE_EVENTSUPPORT_SOURCE_SLIDER = "eventsupportSourceSlider";
	public static final String PREF_NODE_EVENTSUPPORT_TARGET_SLIDER = "eventsupportTargetSlider";
	
	public static final String PREF_NODE_EVENTSUPPORT_SOURCE_PROPERTIES = "eventsupportSourceProperties";
	public static final String PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_ENTITIES = "eventsupportTargetPropertiesEntities";
	public static final String PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_STATEMODEL = "eventsupportTargetPropertiesStatemodel";
	public static final String PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_JOB = "eventsupportTargetPropertiesJob";
	public static final String PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_WORKINGSTEP = "eventsupportTargetPropertiesWorkingstep";
	public static final String PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_LAYOUT = "eventsupportTargetPropertiesLayout";
	
	private static final Logger LOG = Logger.getLogger(EventSupportPreferenceHandler.class);
	private static EventSupportPreferenceHandler INSTANCE;
	
	/**
	 * @deprecated Refactor this class to use spring injection!
	 */
	public static EventSupportPreferenceHandler getInstance() {
		if (INSTANCE == null)
			INSTANCE = new EventSupportPreferenceHandler();
		
		return INSTANCE;
	}
	/**
	 * Returns the preferences that are stored while working on the EventSupport-Management
	 * @return Preferences for EventSupport-Management
	 */
	Preferences getPreferences() {
		return ClientPreferences.getInstance().getUserPreferences().node(PREF_NODES_EVENTSUPPORTS);
	}
	
	public void removeAllTreePaths(String node) {
		Preferences subNode = getPreferences().node(node);
		try {
			subNode.clear();
		} catch (BackingStoreException e) {
		}
	}
	
	public void removeTreePath(String node, TreePath curTreePath) {
		Preferences subNode = getPreferences().node(node);
		String sElementToRemove = formatTreePath(curTreePath.getPath());
		
		// First remove all children of current treepath from prefs;
		// closing a node means to close all children too
		try {
			for (String sChild : subNode.keys()) {
				if (sChild.startsWith(sElementToRemove)) {
					subNode.remove(sChild);
				}
			}
		} catch (BackingStoreException e) {
			LOG.error(e.getMessage(), e);
		}
	}
	

	 public void addTreePath(String node, TreePath curTreePath) {
	        Preferences subNode = getPreferences().node(node);
	
	        // Store TreePath in prefs as an opened Node
	        subNode.putBoolean(formatTreePath(curTreePath.getPath()), true);
	}
	
	 public void addColumnWidth(String node, String columnName, int widthValue) {
	        Preferences subNode = getPreferences().node(node);
	        subNode.putInt(columnName, widthValue);
	}
	 
	public void addSliderSize(String node, String nameOfSlider, Integer value) {
	        Preferences subNode = getPreferences().node(node);
	
	       // Store TreePath in prefs as an opened Node
	   subNode.putInt(nameOfSlider, value);
	}
	 
	private String formatTreePath(Object[] treePath) {
	     String retVal = null;
	
	     for (Object obj : treePath) {

	    	 if (obj!= null && obj.toString().trim().length() > 0) 
    		 {
	    		 if (retVal == null) {
	    			 retVal = obj.toString();
	    		 }
	    		 else {
	    			 retVal += ";" + obj.toString(); 	    		
	    		 }
    		 }
	     }
	
	     return retVal;
	}
	    
	public int getSliderSize(String node, String sliderName) {
		int retVal = 0;
		Preferences subNode = getPreferences().node(node);
		retVal = subNode.getInt("mainSlider", 0);
		return retVal;
	}
	
	public int getColumnWidth(String node, String columnName) {
		int retVal = 0;
		Preferences subNode = getPreferences().node(node);
		retVal = subNode.getInt(columnName, 0);
		return retVal;
	}
	
	public TreePath[] getTreePaths(String node) {
		List<TreePath> retVal = new ArrayList<TreePath>();
		try {
			Preferences subNode = getPreferences().node(node);
			for (String curElement : subNode.keys()) {
				retVal.add(new TreePath(convertToTreePath(curElement)));
			}
		} catch (BackingStoreException e) {
			LOG.error(e.getMessage(), e);
		}
		return retVal.toArray(new TreePath[retVal.size()]);
	}

private TreePath convertToTreePath(String treePath) {
    Scanner sc = new Scanner(treePath).useDelimiter(";");
    List<Object> lstOfPathElements= new ArrayList<Object>();

    while (sc.hasNext()) {
         lstOfPathElements.add(sc.next());
    }

    return new TreePath(lstOfPathElements.toArray());
}
	
}
