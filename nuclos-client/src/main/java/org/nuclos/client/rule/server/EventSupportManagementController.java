package org.nuclos.client.rule.server;

import java.io.Serializable;
import java.text.Collator;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetTreeNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetType;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTreeNode;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.main.mainframe.workspace.TabRestoreController;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportRepository.ButtonVO;
import org.nuclos.client.rule.server.model.EventSupportButtonPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportCommunicationPortPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportEntityPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportGenerationPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportJobPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportSourcePropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportStatePropertiesTableModel;
import org.nuclos.client.rule.server.panel.EventSupportButtonProperyPanel;
import org.nuclos.client.rule.server.panel.EventSupportCommunicationPortPropertyPanel;
import org.nuclos.client.rule.server.panel.EventSupportEntityPropertyPanel;
import org.nuclos.client.rule.server.panel.EventSupportGenerationPropertyPanel;
import org.nuclos.client.rule.server.panel.EventSupportJobProperyPanel;
import org.nuclos.client.rule.server.panel.EventSupportSourceView;
import org.nuclos.client.rule.server.panel.EventSupportStatePropertyPanel;
import org.nuclos.client.rule.server.panel.EventSupportTargetView;
import org.nuclos.client.rule.server.panel.EventSupportView;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.ui.Controller;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EventSupportNotification;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.nuclos.server.attribute.valueobject.LayoutVO;
import org.nuclos.server.eventsupport.valueobject.CommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTypeVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.valueobject.StateModelVO;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;

public class EventSupportManagementController extends Controller<MainFrameTabbedPane> {
	
	//Logger
	private static final Logger LOG = Logger.getLogger(EventSupportManagementController.class);	
	private static final Collator textCollator = Collator.getInstance();
	
	private final EventSupportMessageListener messagelistener = new EventSupportMessageListener();
	
	private EventSupportView viewEventSupportManagement;
	private EventSupportActionHandler esActionHandler;
	private MainFrameTab ifrm;
	
	public EventSupportManagementController(MainFrameTabbedPane pParent) {
		super(pParent);
		
		if (!this.messagelistener.isSubscripted()) {
			SpringApplicationContextHolder.getBean(TopicNotificationReceiver.class).subscribe(
					JMSConstants.TOPICNAME_EVENTSUPPORTNOTIFICATION, this.messagelistener);
			this.messagelistener.setSubscripted(true);	
		}
	}
	
	public static class NuclosESMRunnable implements Runnable {
	
		private final MainFrameTabbedPane desktopPane;
		
		public NuclosESMRunnable(MainFrameTabbedPane desktopPane) {
			this.desktopPane = desktopPane;
		}
		
		@Override
		public void run() {
			try {
				EventSupportManagementController w = new EventSupportManagementController(desktopPane);
				w.showManagementPane(desktopPane);
			}
			catch (Exception e) {
				// Ok
				LOG.error("showWizard failed: " + e, e);
			}
		}
	}

	public EventSupportView getEventSupportView() {
		return viewEventSupportManagement;
	}
	
	public void showSourceSupportProperties(EventSupportTreeNode node) {
			switch (node.getTreeNodeType()) {
				case EVENTSUPPORT:
					EventSupportSourceVO essVO = 
						EventSupportRepository.getInstance().getEventSupportByClassname(node.getNodeName());
					String sPackage = essVO.getPackage();
					String sNuclet = null;
					if (essVO.getNuclet() != null) {
						MasterDataVO<UID> masterDataVO = MasterDataCache.getInstance().get(E.NUCLET.getUID(), essVO.getNuclet());
						if (masterDataVO != null && masterDataVO.getFieldValue(E.NUCLET.name) != null) {
							sNuclet = (String) masterDataVO.getFieldValue(E.NUCLET.name);
						}
					}
					loadProperty(essVO.getName(), essVO.getDescription(), sNuclet, sPackage, essVO.getClassname(), essVO.getDateOfCompilation(), node, false);
					break;
				case EVENTSUPPORT_TYPE:
					EventSupportTypeVO estVO = 
						EventSupportRepository.getInstance().getEventSupportTypeByName(node.getNodeName());
					loadProperty(estVO.getName(), estVO.getDescription(), null, estVO.getPackage(), estVO.getClassname(), estVO.getDateOfCompilation(), node, false);
					
					break;
				default:
					viewEventSupportManagement.getSourceViewPanel().showSourcePropertyPanel(null);
					break;
			}
	}
	
	private void loadProperty(String sName, String sDescription, String sNuclet, String sPackage, String sClassname,
			Date dCompilation, EventSupportTreeNode node, boolean isType) {
		EventSupportSourceView sourceViewPanel = viewEventSupportManagement.getSourceViewPanel();
		EventSupportSourcePropertiesTableModel propertyModel = (EventSupportSourcePropertiesTableModel)
				sourceViewPanel.getSourcePropertiesPanel().getPropertyTable().getModel();

		propertyModel.clear();

		DateFormat dateInstance = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);

		String sEventName = sName;
		String sEventDesc = sDescription;

		if (isType) {
			sEventName = getSpringLocaleDelegate().getMessage(sEventName, sEventName);
			sEventDesc = getSpringLocaleDelegate().getMessage(sEventDesc, sEventDesc);
		}

		propertyModel.addEntry(EventSupportSourcePropertiesTableModel.ELM_ES_NAME, sEventName);
		propertyModel.addEntry(EventSupportSourcePropertiesTableModel.ELM_ES_DESCRIPTION, sEventDesc);
		propertyModel.addEntry(EventSupportSourcePropertiesTableModel.ELM_ES_TYPE, sClassname);
		propertyModel.addEntry(EventSupportSourcePropertiesTableModel.ELM_ES_NUCLET, sNuclet);
		propertyModel.addEntry(EventSupportSourcePropertiesTableModel.ELM_ES_PATH, sPackage);
		propertyModel.addEntry(EventSupportSourcePropertiesTableModel.ELM_ES_CREATION_DATE,
				dCompilation != null ? dateInstance.format(dCompilation) : null);

		// Reload after chaning data 
		sourceViewPanel.showSourcePropertyPanel(sourceViewPanel.getSourcePropertiesPanel());
	}
	
	public void showTargetStateSupportPropertiesNotModified(final EventSupportTreeNode node) 
			throws CommonFinderException, CommonPermissionException {
		EventSupportTargetView targetViewPanel = viewEventSupportManagement.getTargetViewPanel();
		EventSupportStatePropertyPanel statePropertiesPanel = 
				targetViewPanel.getStatePropertiesPanel();
		
		
		final EventSupportStatePropertiesTableModel targetStateModel = 
				(EventSupportStatePropertiesTableModel) statePropertiesPanel.getPropertyTable().getModel();
		
		UID statemodelUid = (UID) node.getId();
		
		List<StateTransitionVO> lstTranses = StateDelegate.getInstance().getOrderedStateTransitionsByStatemodel(statemodelUid);
		List<EventSupportTransitionVO> lstSupportsForTransition = EventSupportRepository.getInstance().getEventSupportsByStateModelUid(statemodelUid);
					EventSupportRepository.getInstance().getEventSupportsByStateModelUid(statemodelUid);
		
		targetStateModel.clear();
		targetStateModel.addTransitions(lstTranses);
		
		String sEventSupportType = node.getNodeName();
		
		for(EventSupportTransitionVO estVO : lstSupportsForTransition) {
			if (sEventSupportType.equals(estVO.getEventSupportClassType()))
				targetStateModel.addEntry(estVO);
		}
		Collections.sort(targetStateModel.getEntries(), new Comparator<EventSupportVO>() {		
			@Override
			public int compare(EventSupportVO o1, EventSupportVO o2) {
				return o1.getOrder() > o2.getOrder() ? 1 : o1.getOrder() < o2.getOrder() ? -1: 0;
			}
		});
		
		// Reload after changing data	
		targetViewPanel.showPropertyPanel(targetViewPanel.getStatePropertiesPanel());
	}
	
	public void showTargetGenerationSupportPropertiesNotModified(final EventSupportTreeNode node) throws CommonPermissionException {
		EventSupportTargetView targetViewPanel = viewEventSupportManagement.getTargetViewPanel();
		EventSupportGenerationPropertyPanel genPropertiesPanel = 
				targetViewPanel.getGenerationPropertiesPanel();
		
		final EventSupportGenerationPropertiesTableModel targetGenerationModel = 
				(EventSupportGenerationPropertiesTableModel) genPropertiesPanel.getPropertyTable().getModel();
			
		Collection<EventSupportGenerationVO> lstEseGVOs;
		String sEventSupportType = node.getNodeName();
		
		lstEseGVOs = EventSupportRepository.getInstance().getEventSupportsByGenerationUid((UID)node.getId());
		targetGenerationModel.clear();
		for (EventSupportGenerationVO esgVO : lstEseGVOs) {
			if (sEventSupportType.equals(esgVO.getEventSupportClassType()))
				 targetGenerationModel.addEntry(esgVO);
		}
		Collections.sort(targetGenerationModel.getEntries(), new Comparator<EventSupportVO>() {		
			@Override
			public int compare(EventSupportVO o1, EventSupportVO o2) {
				return o1.getOrder() > o2.getOrder() ? 1 : o1.getOrder() < o2.getOrder() ? -1: 0;
			}
		});
		
		// Reload after changing data
		targetViewPanel.showPropertyPanel(targetViewPanel.getGenerationPropertiesPanel());
	}
	
	public void showTargetCommunicationPortPropertiesNotModified(final EventSupportTreeNode node) throws CommonPermissionException {
		EventSupportTargetView targetViewPanel = viewEventSupportManagement.getTargetViewPanel();
		EventSupportCommunicationPortPropertyPanel comportPropertiesPanel = 
				targetViewPanel.getCommunicationPropertiesPanel();
		
		final EventSupportCommunicationPortPropertiesTableModel targetComportModel = 
				(EventSupportCommunicationPortPropertiesTableModel) comportPropertiesPanel.getPropertyTable().getModel();
			
		Collection<EventSupportCommunicationPortVO> lstEseCPVOs;
		
		lstEseCPVOs = EventSupportRepository.getInstance().getEventSupportsByCommunicationPortUid((UID)node.getId());
		targetComportModel.clear();
		for (EventSupportCommunicationPortVO escpVO : lstEseCPVOs) {
			targetComportModel.addEntry(escpVO);
		}
		Collections.sort(targetComportModel.getEntries(), new Comparator<EventSupportVO>() {		
			@Override
			public int compare(EventSupportVO o1, EventSupportVO o2) {
				return o1.getOrder() > o2.getOrder() ? 1 : o1.getOrder() < o2.getOrder() ? -1: 0;
			}
		});
		
		// Reload after changing data
		targetViewPanel.showPropertyPanel(targetViewPanel.getCommunicationPropertiesPanel());
	}
	
	public void showTargetLayoutButtonPropertiesNotModified(final EventSupportTreeNode node) 
			throws CommonFinderException, CommonPermissionException {
		EventSupportTargetView targetViewPanel = viewEventSupportManagement.getTargetViewPanel();
		EventSupportButtonProperyPanel btnPropertyPanel =  
				targetViewPanel.getButtonPropertiesPanel();
		
		final EventSupportButtonPropertiesTableModel targetButtonModel = 
				(EventSupportButtonPropertiesTableModel) btnPropertyPanel.getPropertyTable().getModel();
		
		targetButtonModel.clear();
		
		List<ButtonVO> buttonsByLayout = 
				EventSupportRepository.getInstance().getButtonsByLayout((UID) node.getParentNode().getId());
		
		for (ButtonVO btn : buttonsByLayout) {
			if (btn.getRule() != null)
				targetButtonModel.addEntry(btn.getRule());				
		}
		
		// Reload after chaning data 
		targetViewPanel.showPropertyPanel(targetViewPanel.getButtonPropertiesPanel());
	}
	
	public void showTargetJobSupportPropertiesNotModified (final EventSupportTreeNode node) {
		EventSupportTargetView targetViewPanel = viewEventSupportManagement.getTargetViewPanel();
		EventSupportJobProperyPanel jobPropertiesPanel = 
				targetViewPanel.getJobPropertiesPanel();
		
		final EventSupportJobPropertiesTableModel targetJobModel = 
				(EventSupportJobPropertiesTableModel) jobPropertiesPanel.getPropertyTable().getModel();
		String sEventSupportType = node.getNodeName();
		targetJobModel.clear();
		
		if (node.getParentNode().getTreeNodeType().equals(EventSupportTargetType.JOB)) {
			try {
				Collection<EventSupportJobVO> eventSupportsForJob = 
						EventSupportRepository.getInstance().getEventSupportsByJobUid((UID) node.getParentNode().getId());
				
				for (EventSupportJobVO esjVO : eventSupportsForJob) {
					if (sEventSupportType.equals(esjVO.getEventSupportClassType()))
						targetJobModel.addEntry(esjVO);
				}
				
				Collections.sort(targetJobModel.getEntries(), new Comparator<EventSupportVO>() {		
					@Override
					public int compare(EventSupportVO o1, EventSupportVO o2) {
						return o1.getOrder() > o2.getOrder() ? 1 : o1.getOrder() < o2.getOrder() ? -1: 0;
					}
				});
				
				// Reload after chaning data 
				targetViewPanel.showPropertyPanel(targetViewPanel.getJobPropertiesPanel());
			} catch (Exception e) {
				showAndLogException(e);
			}
		}			
	}
	
	public void showTargetEntitySupportPropertiesNotModified(final EventSupportTreeNode node) {
		EventSupportTargetView targetViewPanel = viewEventSupportManagement.getTargetViewPanel();
		EventSupportEntityPropertyPanel entityPropertiesPanel = targetViewPanel.getEntityPropertiesPanel();
		
		final EventSupportEntityPropertiesTableModel targetEntityModel = 
				(EventSupportEntityPropertiesTableModel) entityPropertiesPanel.getPropertyTable().getModel();
		targetEntityModel.clear();
		
		if (node.getParentNode().getTreeNodeType().equals(EventSupportTargetType.ENTITY) ||
				node.getParentNode().getTreeNodeType().equals(EventSupportTargetType.ENTITY_INTEGRATION_POINT)) {
			try {
			    UID targetId = (UID) node.getParentNode().getId();
			  	entityPropertiesPanel.setSelectedModule(targetId);
			    
				Collection<EventSupportEventVO> eventSupportsForEntity = 
						EventSupportRepository.getInstance().getEventSupportsForEntity(targetId);
				
				for (EventSupportEventVO esevo : eventSupportsForEntity) {
					if (esevo.getEventSupportClassType().equals(node.getNodeName())) {
						targetEntityModel.addEntry(esevo);
					}
				}
				Collections.sort(targetEntityModel.getEntries(), new Comparator<EventSupportVO>() {		
					@Override
					public int compare(EventSupportVO o1, EventSupportVO o2) {
						return o1.getOrder() > o2.getOrder() ? 1 : o1.getOrder() < o2.getOrder() ? -1: 0;
					}
				});
				
				targetViewPanel.showPropertyPanel(targetViewPanel.getEntityPropertiesPanel());
			
			} catch (Exception e) {
				showAndLogException(e);
			}
		}
	}
		
	public void showTargetSupportProperties(final EventSupportTreeNode node) 
			throws CommonFinderException, CommonPermissionException {
		
		switch (node.getTreeNodeType()) {
				case EVENTSUPPORT_TYPE:
					showTargetEntitySupportPropertiesNotModified(node);
					break;
				case STATE_TRANSITION:
					showTargetStateSupportPropertiesNotModified(node);
					break;
				case JOB_EVENT:
					showTargetJobSupportPropertiesNotModified(node);
					break;
				case LAYOUT_BUTTONS:
					showTargetLayoutButtonPropertiesNotModified(node);
					break;
				case GENERATION_EVENT:
					showTargetGenerationSupportPropertiesNotModified(node);
					break;
				case COMMUNICATION_PORT:
					showTargetCommunicationPortPropertiesNotModified(node);
					break;
				default:
					viewEventSupportManagement.getTargetViewPanel().showPropertyPanel(null);
					break;
			}
	}
	
	public void showManagementPane(MainFrameTabbedPane desktopPane) {	
		// Explorer Panel including Toolbar and tree for eventsupports
		final EventSupportTreeNode treenodeRoot = new EventSupportTreeNode(this, null, null,
				"Regelsourcen", null, null,
				EventSupportTargetType.ROOT, true,false,null, true);
		
		final EventSupportTargetTreeNode treenodeRootTargets = new EventSupportTargetTreeNode(this, null, null, 
				getSpringLocaleDelegate().getMessage("ExplorerController.32","Regelzuweisung"),
				getSpringLocaleDelegate().getMessage("ExplorerController.32","Regelzuweisung"), 
				getSpringLocaleDelegate().getMessage("ExplorerController.32","Regelzuweisung"), 
				EventSupportTargetType.ROOT, false,false,null);
		
		ClientPreferences.getInstance().getUserPreferences();
		
		if (viewEventSupportManagement == null) {
			viewEventSupportManagement = new EventSupportView(treenodeRoot, treenodeRootTargets);
			esActionHandler = 
					new EventSupportActionHandler(viewEventSupportManagement);
			viewEventSupportManagement.setActionMap(esActionHandler.getActionMaps());
			viewEventSupportManagement.showGui();			
		}
		ifrm = Main.getInstance().getMainController().newMainFrameTab(null, 
				getSpringLocaleDelegate().getMessage("miServerRuleManager", "Server RegelManager"));
	
		ifrm.add(viewEventSupportManagement);		
		ifrm.setTabIconFromNuclosResource("org.nuclos.client.resource.icon.main-blue.code.png"); 
		ifrm.setTabRestoreController(new EventSupportTabRestoreController());
		
		desktopPane.add(ifrm);
		ifrm.setVisible(true);
	}

	public List<TreeNode> createSubNodesByType(EventSupportTreeNode esNode, final String sSearchText) {
		List<TreeNode> retVal = new ArrayList<TreeNode>();
		List<EventSupportTypeVO> eventSupportTypes;
		
		switch (esNode.getTreeNodeType()) {
			case ROOT:	
					retVal.add(new EventSupportTreeNode(this, esNode, null, getSpringLocaleDelegate().getMessage("ExplorerController.33","Regelbibliothek"), getSpringLocaleDelegate().getMessage("ExplorerController.33","Regelbibliothek"), getSpringLocaleDelegate().getMessage("ExplorerController.33","Regelbibliothek"), EventSupportTargetType.NUCLETS, true, false,sSearchText, true));
				break;
			case NUCLETS:
				// Default Node for all nuclos elements that are not attached to a nuclet
				if (sSearchText == null || EventSupportRepository.getInstance().getEventSupportSourcesByPackage(UID.UID_NULL, sSearchText).size() > 0)
					retVal.add(new EventSupportTreeNode(this, esNode, null, "<Default>", "<Default>", "Alle nicht zugewiesenen Elemente", EventSupportTargetType.NUCLET, false,false,sSearchText, true));
 
				try {
					List<MasterDataVO<?>> masterData = CollectionUtils.sorted(
							MasterDataCache.getInstance().get(E.NUCLET.getUID()), new EventSupportMasterDataComparator(E.NUCLET.name.getUID()));
					
					for (MasterDataVO<?> msvo : masterData)
					{
						if (sSearchText == null || EventSupportRepository.getInstance().getEventSupportSourcesByPackage((UID) msvo.getPrimaryKey(), sSearchText).size() > 0)
							retVal.add(new EventSupportTreeNode(this, esNode, msvo.getPrimaryKey(), 
									msvo.getFieldValue(E.NUCLET.packagefield),
									msvo.getFieldValue(E.NUCLET.name),
									msvo.getFieldValue(E.NUCLET.description),
									EventSupportTargetType.NUCLET, false,false,sSearchText, true));
					}
				} catch (Exception e) {
					showAndLogException(e);
				}
				break;
			case NUCLET:	
						
				eventSupportTypes = EventSupportRepository.getInstance().getEventSupportTypes();
				Collections.sort(eventSupportTypes, ESGenerateTypeComparator);
				for (EventSupportTypeVO s : eventSupportTypes) {						
					if (sSearchText == null) {
						retVal.add(new EventSupportTreeNode(this, esNode, null, s.getClassname(), 
								s.getName(),s.getDescription(), EventSupportTargetType.EVENTSUPPORT_TYPE, false,false,sSearchText, true));								
					} else {
						List<EventSupportSourceVO> eventSupportsByType = EventSupportRepository.getInstance().getEventSupportsByType(s.getClassname());
						List<EventSupportSourceVO> select = CollectionUtils.select(eventSupportsByType, new Predicate<EventSupportSourceVO>() {	
							@Override
							public boolean evaluate(EventSupportSourceVO t) {
								boolean retVal = false;
								if (t.getName().toLowerCase().contains(sSearchText.toLowerCase()))
									retVal = true;
								
								return retVal;
							}
						});	
						if (select.size() > 0)
							retVal.add(new EventSupportTreeNode(this, esNode, null, s.getClassname(), s.getName(),s.getDescription(), EventSupportTargetType.EVENTSUPPORT_TYPE, false,false,sSearchText, true));
					}
				}
				break;
			case EVENTSUPPORT_TYPE:
				List<EventSupportSourceVO> eventSupport = CollectionUtils.sorted(
						EventSupportRepository.getInstance().getEventSupportsByType(esNode.getNodeName()), ESSourceComparator);;
						
				if (eventSupport != null && eventSupport.size() > 0) {
					UID uid = (UID) (esNode.getParentNode().getId() == null ? UID.UID_NULL: esNode.getParentNode().getId());
					List<EventSupportSourceVO> lstSupportsOfNuclet = CollectionUtils.sorted(
							EventSupportRepository.getInstance().getEventSupportSourcesByPackage(uid, sSearchText), ESSourceComparator);
			
					Map<String, ErrorMessage> compileErrors = EventSupportRepository.getInstance().getCompileExceptionMessages(false);
					
					for (EventSupportSourceVO s : eventSupport) {
						if (lstSupportsOfNuclet.contains(s)) {
							boolean isCompilable = true;
							if (compileErrors.containsKey(s.getClassname()))
								isCompilable = false;
								
								EventSupportTreeNode eventSupportTreeNode = new EventSupportTreeNode(this, esNode, null, s.getClassname(), s.getName(), s.getDescription(), EventSupportTargetType.EVENTSUPPORT, false,false,sSearchText, isCompilable);
							if (sSearchText == null || containsIgnorCaseSensifity(s.getName(), sSearchText)) {
								retVal.add(eventSupportTreeNode); 																	
							}
						}
					}							
				}	
				break;
			default:
				break;
		}
		return retVal;
	}
	
	public EventSupportJobVO addEventSupportToJob(EventSupportTreeNode sourceNode, EventSupportTreeNode targetNode) {
		EventSupportJobVO retVal = null;
		if (targetNode != null && EventSupportTargetType.JOB.equals(targetNode.getTreeNodeType())) {
			try {
				String  sSupportClass = sourceNode.getNodeName();
				String  sSupportClassType = sourceNode.getParentNode().getNodeName();
				String  sDescription = sourceNode.getDescription();
				UID iJobControllerId = (UID) targetNode.getId();
				Integer iOrder = targetNode.getSubNodes().size() + 1;
					Collection<EventSupportJobVO> eventSupportsForJob = 
							EventSupportRepository.getInstance().getEventSupportsByJobUid(iJobControllerId);
					iOrder = eventSupportsForJob != null ? eventSupportsForJob.size() + 1: 1;
					retVal = EventSupportDelegate.getInstance().createEventSupportJob(
							new EventSupportJobVO(sDescription,sSupportClass, sSupportClassType, iOrder, iJobControllerId));
			} catch (NuclosBusinessRuleException e) {
				showAndLogException(e);
			} catch (CommonPermissionException e) {
				showAndLogException(e);
			} catch (CommonValidationException e) {
				showAndLogException(e);
			} catch (CommonCreateException e) {
				showAndLogException("EventSupportTargetTree.Transition.1", targetNode.getNodeName());
			}
		}
		return retVal;
	}
	
	public EventSupportGenerationVO addEventSupportToGeneration(EventSupportTreeNode sourceNode, EventSupportTreeNode targetNode) {
		EventSupportGenerationVO retVal = null;
		if (targetNode != null && EventSupportTargetType.GENERATION.equals(targetNode.getTreeNodeType())) {
			try {
				UID genId = (UID) (targetNode.getId());
				String eventsupportClassType = sourceNode.getParentNode().getNodeName();
				
				int newOrder = 1;
						
				Collection<EventSupportGenerationVO> lsteseg = 
						EventSupportRepository.getInstance().getEventSupportsByGenerationUid(genId);
				
				for (EventSupportGenerationVO esgVO : lsteseg) {
					if (esgVO.getEventSupportClassType().equals(eventsupportClassType))
						newOrder++;
				}
				
				String  sSupportClass = sourceNode.getNodeName();
				String  sSupportClassType = sourceNode.getParentNode().getNodeName();
				UID iGenerationId = (UID) targetNode.getId();
				Integer iOrder = newOrder;
				
				retVal = EventSupportDelegate.getInstance().createEventSupportGeneration(
						new EventSupportGenerationVO(iOrder,iGenerationId,sSupportClass, sSupportClassType));
								
			} catch (NuclosBusinessRuleException e) {
				showAndLogException(e);
			} catch (CommonPermissionException e) {
				showAndLogException(e);
			} catch (CommonValidationException e) {
				showAndLogException(e);
			} catch (CommonCreateException e) {
				showAndLogException(e);
			}
		}
		return retVal;
	}
	
	public EventSupportCommunicationPortVO addEventSupportToCommunicationPort(EventSupportTreeNode sourceNode, EventSupportTreeNode targetNode) {
		EventSupportCommunicationPortVO retVal = null;
		if (targetNode != null && EventSupportTargetType.COMMUNICATION_PORT.equals(targetNode.getTreeNodeType())) {
			try {
				UID portId = (UID) (targetNode.getId());
				String eventsupportClassType = sourceNode.getParentNode().getNodeName();
				
				int newOrder = 1;
						
				Collection<EventSupportCommunicationPortVO> lsteseg = 
						EventSupportRepository.getInstance().getEventSupportsByCommunicationPortUid(portId);
				
				for (EventSupportCommunicationPortVO esgVO : lsteseg) {
					if (esgVO.getEventSupportClassType().equals(eventsupportClassType))
						newOrder++;
				}
				
				String  sSupportClass = sourceNode.getNodeName();
				String  sSupportClassType = sourceNode.getParentNode().getNodeName();
				UID iPortId = (UID) targetNode.getId();
				Integer iOrder = newOrder;
				
				retVal = EventSupportDelegate.getInstance().createEventSupportCommunicationPort(
						new EventSupportCommunicationPortVO(iOrder,iPortId,sSupportClass, sSupportClassType));
								
			} catch (NuclosBusinessRuleException e) {
				showAndLogException(e);
			} catch (CommonPermissionException e) {
				showAndLogException(e);
			} catch (CommonValidationException e) {
				showAndLogException(e);
			} catch (CommonCreateException e) {
				showAndLogException(e);
			}
		}
		return retVal;
	}
	
	public EventSupportTransitionVO addEventSupportToStateTransition(EventSupportTreeNode sourceNode, EventSupportTreeNode targetNode) throws CommonFinderException {
		EventSupportTransitionVO retVal = null;
		if (targetNode != null && EventSupportTargetType.STATEMODEL.equals(targetNode.getTreeNodeType())) {
			List<StateTransitionVO> orderedStateTransitionsByStatemodel =  null;
			List<StateTransitionVO> notUsedStateTransitions =  null;
			try {
				String sSupportClass = sourceNode.getNodeName();
				String sSupportInterface = sourceNode.getParentNode().getNodeName();
				UID moduleUid =(UID) targetNode.getId();
					
				List<EventSupportTransitionVO> stateTransitionsBySupportType = 
						EventSupportDelegate.getInstance().getStateTransitionsBySupportType(moduleUid, sSupportInterface);
				
				orderedStateTransitionsByStatemodel = 
						StateDelegate.getInstance().getOrderedStateTransitionsByStatemodel(moduleUid);
				

				
				notUsedStateTransitions=getNotUsedStateTransitions(stateTransitionsBySupportType,orderedStateTransitionsByStatemodel);
				
				final Integer iOrder = stateTransitionsBySupportType.size() + 1;
				
				UID iTransId;
				if(notUsedStateTransitions.size()>0)
				{
					iTransId=notUsedStateTransitions.get(0).getId();
				}
				else
				{
					 iTransId = orderedStateTransitionsByStatemodel.size() > 0 ? orderedStateTransitionsByStatemodel.get(0).getId() : EventSupportRepository.DEFAULT_NUCLET_UID;
				}
				
				retVal = EventSupportDelegate.getInstance().createEventSupportTransition(
						new EventSupportTransitionVO(sSupportClass,sSupportInterface, iTransId, iOrder));				
			} catch (NuclosBusinessRuleException e) {
				showAndLogException(e);
			} catch (CommonPermissionException e) {
				showAndLogException(e);
			} catch (CommonValidationException e) {
				showAndLogException(e);
			} catch (CommonCreateException e) {
				String transText = getSpringLocaleDelegate().getMessage("datechooser.empty.label", "Leer");
				if (orderedStateTransitionsByStatemodel != null && orderedStateTransitionsByStatemodel.size() > 0)
					transText = EventSupportStatePropertiesTableModel.createTransitionString(orderedStateTransitionsByStatemodel.get(0));
				showAndLogException("EventSupportTargetTree.Transition.1", targetNode.getNodeName(), transText);
			}
		}
		return retVal;
	}
	
	private List<StateTransitionVO> getNotUsedStateTransitions(List<EventSupportTransitionVO> alreadyUsedTransitions,List<StateTransitionVO> allTransitions) {
		List<StateTransitionVO>  notUsed=new ArrayList<StateTransitionVO>();
		boolean currentused=false;
		for(StateTransitionVO allCurrent:allTransitions)
		{
			currentused=false;
			for(EventSupportTransitionVO usedCurrent:alreadyUsedTransitions)
			{
				
				if(usedCurrent.getTransition().equals(allCurrent.getPrimaryKey()))
				{
					currentused=true;
				}
			}
			
			if(!currentused)
				notUsed.add(allCurrent);
		}
		
		return notUsed;
	}

	public EventSupportEventVO addEventSupportToEntity(EventSupportTreeNode sourceNode, EventSupportTreeNode targetNode) {
		EventSupportEventVO retVal = null;
		if (targetNode != null && (EventSupportTargetType.ENTITY.equals(targetNode.getTreeNodeType())
			|| EventSupportTargetType.ENTITY_INTEGRATION_POINT.equals(targetNode.getTreeNodeType()))) {
			EventSupportEventVO savedESEntity = null;
			try {
				String sEventSupportClassname = sourceNode.getNodeName();
				String sEventSupportClassType = sourceNode.getParentNode().getNodeName();
				int idx = 0;
				Collection<EventSupportEventVO> eventSupportsForEntity = 
						EventSupportRepository.getInstance().getEventSupportsForEntity((UID) targetNode.getId());
				for (EventSupportEventVO eseVO : eventSupportsForEntity) {
					if (sourceNode.getParentNode().getNodeName().equals(eseVO.getEventSupportClassType()))
							idx++;
				}
				
				final UID targetId = (UID) targetNode.getId();
				UID entityId = null;
				UID integrationPointId = null;
				if (MetaProvider.getInstance().checkEntity(targetId)) {
					entityId = targetId;
				} else {
					integrationPointId = targetId;
				}

				EventSupportEventVO eseVO = new EventSupportEventVO(
						sEventSupportClassname, sEventSupportClassType, entityId, integrationPointId, null, null, null, null, ++idx);
				// attach eventsupport to entity
				savedESEntity = EventSupportDelegate.getInstance().createEventSupportEvent(eseVO);
			} catch (NuclosBusinessRuleException e) {
				showAndLogException(e);
			} catch (CommonPermissionException e) {
				showAndLogException(e);
			} catch (CommonValidationException e) {
				showAndLogException(e);
			} catch (CommonCreateException e) {
				showAndLogException("EventSupportTargetTree.Entity.1", targetNode.getNodeName());
			}
			if (savedESEntity != null) {
				retVal = savedESEntity;
			}			
		}
		return retVal;
	}
	
	public List<TreeNode> createTargetSubNodesByType(EventSupportTargetTreeNode esNode, String sSearchText) 
			throws CommonFinderException, CommonPermissionException {
		EventSupportTargetTreeNode node = (EventSupportTargetTreeNode) esNode;
		List<TreeNode> lstSubNodes = new ArrayList<TreeNode> ();
		EventSupportTargetType type = node.getTreeNodeType();
		Object nodeId = node.getId();
		
		String strStatechange = getSpringLocaleDelegate().getMessage("RuleNode.12","Statuswechsel");
		String strTimelimits = getSpringLocaleDelegate().getMessage("RuleNode.3","Job");
		String strLayouts = getSpringLocaleDelegate().getMessage("RuleNode.17","Layouts");
		String strWorksteps = getSpringLocaleDelegate().getMessage("RuleNode.7","Objektgenerator");
		String strEntity = getSpringLocaleDelegate().getMessage("nuclos.entity.entity.label","Entität");
		String strAllRules = getSpringLocaleDelegate().getMessage("DirectoryRuleNode.1","All Regeln");
		String strCommuniationPort = getSpringLocaleDelegate().getMessage("nuclos.entity.communicationPort.label","Anschluss");
		
		if (type != null) {
			switch (type) {
			case ROOT:
					List<MasterDataVO<?>> masterData = CollectionUtils.sorted(
							MasterDataCache.getInstance().get(E.NUCLET.getUID()), new EventSupportMasterDataComparator(E.NUCLET.name.getUID()));
					// show all old rules
					lstSubNodes.add(new EventSupportTargetTreeNode(
							this, node, null, strAllRules, strAllRules, strAllRules, 
							EventSupportTargetType.ALL_EVENTSUPPORTS, false,false,sSearchText));
					
					// show System elements
					lstSubNodes.add(new EventSupportTargetTreeNode(this, node, null, "<System>", "<System>", 
							SpringLocaleDelegate.getInstance().getMsg("all.system.elements"), EventSupportTargetType.SYSTEM, false, false, sSearchText));
					
					// show Default elements
					lstSubNodes.add(new EventSupportTargetTreeNode(this, node, null, "<Default>", "<Default>", 
							"Alle nicht zugewiesenen Elemente", EventSupportTargetType.NUCLET, false, false, sSearchText));
					
					for (MasterDataVO<?> msvo : masterData) {
						// Show all nuclets
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, msvo.getPrimaryKey(), 
								msvo.getFieldValue(E.NUCLET.packagefield) != null ? msvo.getFieldValue(E.NUCLET.packagefield) : "",
								msvo.getFieldValue(E.NUCLET.name) != null ? msvo.getFieldValue(E.NUCLET.name) : "",
								msvo.getFieldValue(E.NUCLET.description) != null ? msvo.getFieldValue(E.NUCLET.description) : "",
								EventSupportTargetType.NUCLET, false,false,sSearchText));
					}
				break;
			case ALL_EVENTSUPPORTS:
				try {
					List<EventSupportSourceVO> allEventSupports = CollectionUtils.sorted(
							EventSupportRepository.getInstance().getAllEventSupports(), ESSourceComparator);
					
					for (EventSupportSourceVO eseVO : allEventSupports) {
						if (sSearchText == null ||  containsIgnorCaseSensifity(eseVO.getName(), sSearchText)) {
							lstSubNodes.add(new EventSupportTargetTreeNode(this, node, eseVO.getId(), 
									eseVO.getClassname(), eseVO.getName(), eseVO.getDescription(), 
									EventSupportTargetType.EVENTSUPPORT, false,false,sSearchText));					
						}
					}	
				} catch (NuclosBusinessException e) {
					showAndLogException(e);
				}
				break;
			case SYSTEM: {
				String sLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(E.COMMUNICATION_PORT);
				lstSubNodes.add(new EventSupportTargetTreeNode(this, node, null, sLabel, sLabel, sLabel, 
						EventSupportTargetType.COMMUNICATION_PORT_CATEGORY, false, false, sSearchText));
				break;
			}
			case COMMUNICATION_PORT_CATEGORY: {
				List<MasterDataVO<UID>> dependantMasterData = EventSupportRepository.getInstance()
						.getNuclosObjectsByNuclet(null, E.COMMUNICATION_PORT.getUID(), null);
				Collections.sort(dependantMasterData, new EventSupportEntityObjectComparator(E.COMMUNICATION_PORT.portName.getUID()));
				
				for (MasterDataVO<?> eoVO : dependantMasterData)
				{
					String portName = eoVO.getFieldValue(E.COMMUNICATION_PORT.portName);
					if (sSearchText == null || containsIgnorCaseSensifity(portName, sSearchText))
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, eoVO.getPrimaryKey(), 
								portName, portName, portName, EventSupportTargetType.COMMUNICATION_PORT, false, false, sSearchText));
				}
				break;
			}
			case ALL_ENTITIES:
				List<EventSupportEventVO> eventSupportEventsByClassname = EventSupportRepository.getInstance()
						.getEventSupportEntitiesByClassname(node.getParentNode().getNodeName(), null);
				Collections.sort(eventSupportEventsByClassname, ESEventComparator);
				
				for (EventSupportEventVO eseVO : eventSupportEventsByClassname) {
					EntityMeta<?> entity = MetaProvider.getInstance().getEntity(eseVO.getEntityUID());
					if (sSearchText == null || containsIgnorCaseSensifity(entity.getEntityName(), sSearchText)) 
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, eseVO.getId(), entity.getUID(), entity.getEntityName(), entity.getEntityName(), EventSupportTargetType.ALL_ENTITIES_ENTITY,
								false, false, sSearchText));
				}					
				break;
			case ALL_STATEMODELS:
				try {
					List<StateModelVO> stateModelByEventSupportClassname =  CollectionUtils.sorted(
							EventSupportRepository.getInstance().getStateModelByEventSupportClassname(
									node.getParentNode().getNodeName()), ESStatemodelComparator);
					
					for (StateModelVO eseVO : stateModelByEventSupportClassname) {
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, eseVO.getId(), 
								eseVO.getName(), eseVO.getName(), eseVO.getDescription(), 
								EventSupportTargetType.ALL_STATEMODELS_STATEMODEL, false, false, sSearchText));
					}
				} catch (CommonFinderException e) {
					showAndLogException(e);
				} catch (CommonPermissionException e) {
					showAndLogException(e);
				} catch (NuclosBusinessException e) {
					showAndLogException(e);
				}
				break;
			case ALL_JOBS:
				try {
					List<JobVO> jobsByClassname = CollectionUtils.sorted(
							EventSupportRepository.getInstance().getJobsByEventSupportClassname(
									node.getParentNode().getNodeName()), ESJobComparator);
					
					for (JobVO eseVO : jobsByClassname) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node, eseVO.getId(), eseVO.getName(), eseVO.getName(), eseVO.getDescription(), 
								EventSupportTargetType.ALL_JOBS_JOB, false,false,sSearchText));
					}
				} catch (CommonFinderException e) {
					showAndLogException(e);
				} catch (CommonPermissionException e) {
					showAndLogException(e);
				}
				break;
			case ALL_GENERATIONS:
				try {
					List<GeneratorActionVO> gensByClassname = CollectionUtils.sorted(
							EventSupportRepository.getInstance().getGenerationsByEventSupportClassname(
									node.getParentNode().getNodeName()), ESGenerateActionComparator);
					
					for (GeneratorActionVO eseVO : gensByClassname) {
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, eseVO.getId(), eseVO.getName(), 
								eseVO.getName(), eseVO.getName(), EventSupportTargetType.ALL_GENERATIONS_GENERATION, 
								false, false, sSearchText));
					}
				} catch (CommonFinderException e) {
					showAndLogException(e);
				} catch (CommonPermissionException e) {
					showAndLogException(e);
				}
				break;
			case ALL_COMMUNICATION_PORTS:
				try {
					List<CommunicationPortVO> portsByClassname = CollectionUtils.sorted(
							EventSupportRepository.getInstance().getCommunicationPortsByEventSupportClassname(
									node.getParentNode().getNodeName()), ESCommunicationPortComparator);
					
					for (CommunicationPortVO eseVO : portsByClassname) {
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, eseVO.getId(), eseVO.getName(), 
								eseVO.getName(), eseVO.getDescription(), EventSupportTargetType.COMMUNICATION_PORT, 
								false, false, sSearchText));
					}
				} catch (CommonFinderException e) {
					showAndLogException(e);
				} catch (CommonPermissionException e) {
					showAndLogException(e);
				}
				break;
			case EVENTSUPPORT:
				 try {
					 // Entites
					List<EventSupportEventVO> lsteseByClassname = EventSupportRepository.getInstance()
							.getEventSupportEntitiesByClassname(node.getNodeName(), null);
					if (lsteseByClassname.size() > 0 )
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, null, strEntity, 
								strEntity, strEntity, EventSupportTargetType.ALL_ENTITIES, false, false, sSearchText));
									
					// StateModels
					List<StateModelVO> lstSmVOByClassname = EventSupportRepository.getInstance()
							.getStateModelByEventSupportClassname(node.getNodeName());
					if (lstSmVOByClassname.size() > 0) {
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, null, strStatechange,
								strStatechange, strStatechange, EventSupportTargetType.ALL_STATEMODELS, false, false, sSearchText));
					}
					
					// Job
					List<JobVO> lstJobVOByClassname = EventSupportRepository.getInstance()
							.getJobsByEventSupportClassname(node.getNodeName());
					if (lstJobVOByClassname.size() > 0) {
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, null, strTimelimits, 
								strTimelimits,strTimelimits, EventSupportTargetType.ALL_JOBS, false, false, sSearchText));
					}
					
					// Generation
					List<GeneratorActionVO> lstGaVOByClassname = EventSupportRepository.getInstance()
							.getGenerationsByEventSupportClassname(node.getNodeName());
					if (lstGaVOByClassname.size() > 0) {
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, null, strWorksteps, 
								strWorksteps,strWorksteps, EventSupportTargetType.ALL_GENERATIONS, false, false, sSearchText));
					}
					
					// Communication port
					List<CommunicationPortVO> lstComPortVOByClassname = EventSupportRepository.getInstance()
							.getCommunicationPortsByEventSupportClassname(node.getNodeName());
					if (lstComPortVOByClassname.size() > 0) {
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, null, strCommuniationPort, 
								strCommuniationPort,strCommuniationPort, EventSupportTargetType.ALL_COMMUNICATION_PORTS, false, false, sSearchText));
					}
				} catch (CommonFinderException e) {
					showAndLogException(e);
				} catch (CommonPermissionException e) {
					showAndLogException(e);
				} catch (NuclosBusinessException e) {
					showAndLogException(e);
				}
				break;
			case ENTITY_CATEGORIE: 
			{
				UID entityNucletuid = nodeId != null ? (UID) nodeId : UID.UID_NULL;
				
				List<MasterDataVO<UID>> dependantMasterData = new ArrayList<>();
				dependantMasterData.addAll(EventSupportRepository.getInstance()
						.getNuclosObjectsByNuclet(entityNucletuid, E.ENTITY.getUID(), E.ENTITY.nuclet.getUID()));
				dependantMasterData.addAll(EventSupportRepository.getInstance()
						.getNuclosObjectsByNuclet(entityNucletuid, E.NUCLET_INTEGRATION_POINT.getUID(), E.NUCLET_INTEGRATION_POINT.nuclet.getUID()));
				Collections.sort(dependantMasterData, new EventSupportEntityObjectComparator(E.ENTITY.entity.getUID(), E.NUCLET_INTEGRATION_POINT.name.getUID()));
				
				for (MasterDataVO<?> eoVO : dependantMasterData)
				{
					if (Boolean.TRUE.equals(eoVO.getFieldValue(E.ENTITY.generic))) {
						// No generic business objects yet
						continue;
					}
					EventSupportTargetType targettype = eoVO.getEntityObject().getDalEntity().equals(E.ENTITY.getUID()) ?
							EventSupportTargetType.ENTITY : EventSupportTargetType.ENTITY_INTEGRATION_POINT;
					String entityName = targettype==EventSupportTargetType.ENTITY ?
							eoVO.getFieldValue(E.ENTITY.entity) :
							eoVO.getFieldValue(E.NUCLET_INTEGRATION_POINT.name);
					if (sSearchText == null || containsIgnorCaseSensifity(entityName, sSearchText))
						lstSubNodes.add(new EventSupportTargetTreeNode(this, node, eoVO.getPrimaryKey(), 
								entityName, entityName, entityName, targettype, false, false, sSearchText));
				}
				break;
			}
			case NUCLET:
					UID nuclet = node.getId() != null ? (UID) node.getId() : UID.UID_NULL;

					// Entity
					if (sSearchText == null || EventSupportRepository.getInstance()
							.containsNuclosObjectsByNucletBySearchString(nuclet, E.ENTITY.getUID(), E.ENTITY.nuclet.getUID(), E.ENTITY.entity.getUID(), sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node, nodeId, strEntity, strEntity, strEntity, 
								EventSupportTargetType.ENTITY_CATEGORIE, false, false, sSearchText));
					}
					
					// Statemodel
					if (sSearchText == null || EventSupportRepository.getInstance()
							.containsNuclosObjectsByNucletBySearchString(nuclet, E.STATEMODEL.getUID(), E.STATEMODEL.nuclet.getUID(), E.STATEMODEL.name.getUID(), sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node,nodeId, strStatechange, strStatechange, 
								strStatechange, EventSupportTargetType.STATEMODEL_CATEGORIE, false, false, sSearchText));
					}
					
					// Generation
					if (sSearchText == null || EventSupportRepository.getInstance()
							.containsNuclosObjectsByNucletBySearchString(nuclet, E.GENERATION.getUID(), E.GENERATION.nuclet.getUID(), E.GENERATION.name.getUID(), sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node,nodeId, strWorksteps,strWorksteps, strWorksteps, 
								EventSupportTargetType.GENERATION_CATEGORIE, false, false, sSearchText));
					}
					
					// Jobs
					if (sSearchText == null || EventSupportRepository.getInstance()
							.containsNuclosObjectsByNucletBySearchString(nuclet, E.JOBCONTROLLER.getUID(), E.JOBCONTROLLER.nuclet.getUID(), E.JOBCONTROLLER.name.getUID(), sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node,nodeId, strTimelimits, strTimelimits,  strTimelimits, 
								EventSupportTargetType.JOB_CATEGORIE, false, false, sSearchText));
					}
					
					// Layouts
					if (sSearchText == null || EventSupportRepository.getInstance()
							.findLayoutsBySearchString(nuclet, sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node,nodeId, strLayouts, strLayouts,  strLayouts, 
								EventSupportTargetType.LAYOUT_CATEGORIE, false, false, sSearchText));
					}
				break;
			case JOB_CATEGORIE:
			{
				UID jobsUid = nodeId != null ? (UID) nodeId : UID.UID_NULL;
				
				List<MasterDataVO<UID>> dependantMasterData = EventSupportRepository.getInstance().
						getNuclosObjectsByNuclet(jobsUid, E.JOBCONTROLLER.getUID(), E.JOBCONTROLLER.nuclet.getUID());
				
				Collections.sort(dependantMasterData, new EventSupportEntityObjectComparator(E.JOBCONTROLLER.name.getUID()));
				
				for (MasterDataVO<?> eoVO : dependantMasterData) {
					String jobName = eoVO.getFieldValue(E.JOBCONTROLLER.name);
					if (sSearchText == null || containsIgnorCaseSensifity(jobName, sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node, eoVO.getPrimaryKey(), jobName, jobName, jobName, 
								EventSupportTargetType.JOB, false, false, sSearchText));
					}
				}
				break;
			}
			case GENERATION_CATEGORIE:
			{
				UID nodeUid = nodeId != null ? (UID) nodeId : UID.UID_NULL;
				
				List<MasterDataVO<UID>> dependantMasterData = EventSupportRepository.getInstance().
						getNuclosObjectsByNuclet(nodeUid, E.GENERATION.getUID(), E.GENERATION.nuclet.getUID());
				
				Collections.sort(dependantMasterData, new EventSupportEntityObjectComparator(E.GENERATION.name.getUID()));
				
				for (MasterDataVO<?> eoVO : dependantMasterData)
				{
					if (sSearchText == null || containsIgnorCaseSensifity(eoVO.getFieldValue(E.GENERATION.name), sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node, eoVO.getPrimaryKey(), eoVO.getFieldValue(E.GENERATION.name), 
								eoVO.getFieldValue(E.GENERATION.name), eoVO.getFieldValue(E.GENERATION.name), 
								EventSupportTargetType.GENERATION, false, false,sSearchText));
					}
				}
				break;
			}
			case GENERATION:
			{
				UID nodeUid = nodeId != null ? (UID) nodeId : UID.UID_NULL;
				
				if (nodeUid != null) {
					EventSupportTypeVO eventSupportTypeByName;
					
					List<String> lstesTypes = EventSupportRepository.getInstance().getEventSupportTypesByGenerationId(nodeUid);
					Collections.sort(lstesTypes);
					for (String s: lstesTypes) {
						eventSupportTypeByName = EventSupportRepository.getInstance().getEventSupportTypeByName(s);						
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node , nodeId, eventSupportTypeByName.getClassname(), 
								eventSupportTypeByName.getName(), eventSupportTypeByName.getDescription(), 
								EventSupportTargetType.GENERATION_EVENT, false,false,sSearchText));
					}
				}
				break;
			}
			case LAYOUT_CATEGORIE:
				List<LayoutVO> layoutsByNuclet = CollectionUtils.sorted(
						EventSupportRepository.getInstance().getLayoutsByNuclet(nodeId != null ? (UID) nodeId : null), ESLayoutComparator);
				
				for (LayoutVO lay : layoutsByNuclet) {
					if (sSearchText == null ||  containsIgnorCaseSensifity(lay.getName(), sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node, lay.getId(), lay.getName(), lay.getName(), lay.getDescription(), 
								EventSupportTargetType.LAYOUT, false, false, sSearchText));
					}
				}
				break;
			case LAYOUT:
				UID layoutUid = nodeId != null ? (UID) nodeId : UID.UID_NULL;
				
				if (layoutUid != null) {
					List<ButtonVO> buttonsByLayout = EventSupportRepository.getInstance().getButtonsByLayout(layoutUid);
					for (ButtonVO btn : CollectionUtils.sorted(buttonsByLayout, ESButtonComparator)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node , nodeId, btn.getName(), btn.getDescription(), btn.getDescription(), 
								EventSupportTargetType.LAYOUT_BUTTONS, false, false, sSearchText));
					}
				}
				break;
			case LAYOUT_BUTTONS:
				break;
		
			case JOB:
				UID jobUid = nodeId != null ? (UID) nodeId : UID.UID_NULL;
				
				if (jobUid != null)
				{
					EventSupportTypeVO eventSupportTypeByName;
					List<String> eventSupportsForJob = EventSupportRepository.getInstance()
							.getEventSupportTypesByJobUid(jobUid);
					Collections.sort(eventSupportsForJob);
					for (String s : eventSupportsForJob) {
						eventSupportTypeByName = EventSupportRepository.getInstance().getEventSupportTypeByName(s);
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node , nodeId, eventSupportTypeByName.getClassname(), 
								eventSupportTypeByName.getName(), eventSupportTypeByName.getDescription(),
								EventSupportTargetType.JOB_EVENT, false, false, sSearchText));																						
					}
				}
				break;
			case STATEMODEL_CATEGORIE:
			{
				UID statesUid = nodeId != null ? (UID) nodeId : UID.UID_NULL;
				
				List<MasterDataVO<UID>> dependantMasterData = EventSupportRepository.getInstance().
						getNuclosObjectsByNuclet(statesUid, E.STATEMODEL.getUID(), E.STATEMODEL.nuclet.getUID());
				
				Collections.sort(dependantMasterData, new EventSupportEntityObjectComparator(E.STATEMODEL.name.getUID()));
				
				for (MasterDataVO<?> eoVO : dependantMasterData) {
					String statemodelName = eoVO.getFieldValue(E.STATEMODEL.name);
					String description = eoVO.getFieldValue(E.STATEMODEL.description);
					if (sSearchText == null || containsIgnorCaseSensifity(statemodelName, sSearchText)) {
						lstSubNodes.add(new EventSupportTargetTreeNode(
								this, node,eoVO.getPrimaryKey(),statemodelName, statemodelName, description, 
								EventSupportTargetType.STATEMODEL, false, false, sSearchText));							
					}
				}
				break;
			}
			case STATEMODEL:
				UID stateUid = nodeId != null ? (UID) nodeId : UID.UID_NULL;
				
				List<String> eventSupportTypesByStateModelId = 
						EventSupportRepository.getInstance().getEventSupportTypesByStateModelUid(stateUid);
				Collections.sort(eventSupportTypesByStateModelId);
				
				for (String s: eventSupportTypesByStateModelId) {
					EventSupportTypeVO eventSupportTypeByName = EventSupportRepository.getInstance().getEventSupportTypeByName(s);
					lstSubNodes.add(new EventSupportTargetTreeNode(
							this, node, nodeId, eventSupportTypeByName.getClassname(), 
							eventSupportTypeByName.getName(), eventSupportTypeByName.getDescription(), 
							EventSupportTargetType.STATE_TRANSITION, false, true, sSearchText));
				}
				break;
			case ENTITY:
			case ENTITY_INTEGRATION_POINT:
				List<EventSupportTypeVO> eventSupportTypes = CollectionUtils.sorted(
						EventSupportRepository.getInstance().getEventSupportTypes(), ESGenerateTypeComparator);
				// Only display supporttypes if there are support classes with same typ attached
				Collection<EventSupportEventVO> eventSupportsForEntity = CollectionUtils.sorted(
						EventSupportRepository.getInstance().getEventSupportsForEntity((UID) node.getId()), ESEventComparator);
			
				for (EventSupportTypeVO s : eventSupportTypes)
				{
					boolean typeAlreadyInserted = false;
					for (EventSupportEventVO eseVO : eventSupportsForEntity)
					{
						if (eseVO.getEventSupportClassType().equals(s.getClassname()) && !typeAlreadyInserted)
						{
							lstSubNodes.add(new EventSupportTargetTreeNode(this, node, nodeId, 
									s.getClassname(),s.getName(), s.getDescription(), 
									EventSupportTargetType.EVENTSUPPORT_TYPE, false,false,sSearchText));									
							typeAlreadyInserted = true;
						}
					}
				}
				break;
			default:
				break;
			}			
		}
		return lstSubNodes;
	}

	private boolean containsIgnorCaseSensifity(String sCompleteString, String sSearchingFor) {
		boolean retVal = false;
		
		if (sCompleteString != null && sSearchingFor != null) {
			if (sCompleteString.toLowerCase().contains(sSearchingFor.toLowerCase()))
				retVal = true;
		}
		return retVal;
	}
	
	private void showAndLogException(Exception e) {
		String msg = e.getMessage() != null ? e.getMessage() : "Error at runtime. Please check Log-files";		
		JOptionPane.showMessageDialog(ifrm, 
				msg, "Fehler", JOptionPane.ERROR_MESSAGE);
		LOG.error(msg);
	}

	private void showAndLogException(String resource, Object... params) {
		String msg = getSpringLocaleDelegate().getMessage(resource, "Der Filter \"{0}\" existiert nicht mehr.", params);
		JOptionPane.showMessageDialog(ifrm, 
				msg, "Fehler", JOptionPane.ERROR_MESSAGE);
		LOG.error(msg);
	}
	
	private EventSupportSourceComparator ESSourceComparator = new EventSupportSourceComparator();
	private EventSupportEventComparator  ESEventComparator = new EventSupportEventComparator();
	private EventSupportStateModelComparator  ESStatemodelComparator = new EventSupportStateModelComparator();
	private EventSupportButtonComparator ESButtonComparator = new EventSupportButtonComparator();
	private EventSupportLayoutComparator ESLayoutComparator = new EventSupportLayoutComparator();
	private EventSupportJobComparator ESJobComparator = new EventSupportJobComparator();
	private EventSupportGeneratorActionComparator ESGenerateActionComparator = new EventSupportGeneratorActionComparator();
	private EventSupportTypComparator ESGenerateTypeComparator = new EventSupportTypComparator();
	private EventSupportCommunicationPortComparator ESCommunicationPortComparator = new EventSupportCommunicationPortComparator();

	private class EventSupportMessageListener implements MessageListener {
		private boolean isSubscripted = false;
		
		public boolean isSubscripted() {
			return isSubscripted;
		}

		public void setSubscripted(boolean isSubscripted) {
			this.isSubscripted = isSubscripted;
		}

		@Override
		public void onMessage(javax.jms.Message msg) {
			EventSupportNotification esn = null;
			
			try {
				if (msg != null) {
					if(msg instanceof ObjectMessage) {
						Serializable object = ((ObjectMessage) msg).getObject();
						if (object != null && object instanceof EventSupportNotification) {
							esn = (EventSupportNotification) object;

							final EntityMeta<UID> e = esn.getEntityChanged(); 
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									// Just reload the list of compile errors
									Map<String, ErrorMessage> compileExceptionMessages = EventSupportRepository.getInstance().getCompileExceptionMessages(true);

									AbstractAction showErrorDialog = esActionHandler.getActionMaps().get(EventSupportActions.ACTION_SHOW_ERRORCOMPILEDIALOG);

									if (compileExceptionMessages.size() > 0) {
										showErrorDialog.setEnabled(true);
										showErrorDialog.putValue(Action.SMALL_ICON, Icons.getInstance().getEventSupportCodeCompileErrorIcon());
									}
									else {
										showErrorDialog.setEnabled(false);
										showErrorDialog.putValue(Action.SMALL_ICON, Icons.getInstance().getEventSupportInactiveRuleIcon());
									}
									
									if (E.SERVERCODE.equals(e) ||
											E.NUCLET.equals(e)) {
										esActionHandler.fireAction(EventSupportActions.ACTION_REFRESH_SOURCETREE);													
									}
									
									if (!E.SERVERCODE.equals(e))
										esActionHandler.fireAction(EventSupportActions.ACTION_REFRESH_TARGETTREE);
								}
							});
						}
					}
					else if (msg instanceof TextMessage) {
						TextMessage tmsg = (TextMessage) msg;
						if (EventSupportNotification.COMPILERRORS.equals(tmsg.getText()) ||
								EventSupportNotification.COMPILED.equals(tmsg.getText())) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									// Just reload the list of compile errors
									Map<String, ErrorMessage> compileExceptionMessages = EventSupportRepository.getInstance().getCompileExceptionMessages(true);
		
									AbstractAction showErrorDialog = esActionHandler.getActionMaps().get(EventSupportActions.ACTION_SHOW_ERRORCOMPILEDIALOG);
		
									if (compileExceptionMessages.size() > 0) {
										showErrorDialog.setEnabled(true);
										showErrorDialog.putValue(Action.SMALL_ICON, Icons.getInstance().getEventSupportCodeCompileErrorIcon());
									}
									else {
										showErrorDialog.setEnabled(false);
										showErrorDialog.putValue(Action.SMALL_ICON, Icons.getInstance().getEventSupportInactiveRuleIcon());
									}
									// and refresh the sources; Caches except the compileExceptionCache dont have to be invalidated
									esActionHandler.fireAction(EventSupportActions.ACTION_REFRESH_SOURCETREE);
								}
							});
						}
					}
				}
				 
			} catch (Exception e) {
				showAndLogException(e);
			}
		}
	}

	private static class EventSupportTypComparator implements Comparator<EventSupportTypeVO> {

		@Override
		public int compare(EventSupportTypeVO o1, EventSupportTypeVO o2) {
			return textCollator.compare(o1.getName(), o2.getName());
		}
	}
	
	private static class EventSupportGeneratorActionComparator implements Comparator<GeneratorActionVO> {

		@Override
		public int compare(GeneratorActionVO o1, GeneratorActionVO o2) {
			return textCollator.compare(o1.getName(), o2.getName());
		}
	}
	
	private static class EventSupportCommunicationPortComparator implements Comparator<CommunicationPortVO> {

		@Override
		public int compare(CommunicationPortVO o1, CommunicationPortVO o2) {
			return textCollator.compare(o1.getName(), o2.getName());
		}
		
	}
	
	private static class EventSupportJobComparator implements Comparator<JobVO> {

		@Override
		public int compare(JobVO o1, JobVO o2) {
			return textCollator.compare(o1.getName(), o2.getName());
		}
	}
	
	private static class EventSupportSourceComparator implements Comparator<EventSupportSourceVO> {

		@Override
		public int compare(EventSupportSourceVO o1, EventSupportSourceVO o2) {
			return textCollator.compare(o1.getName(), o2.getName());
		}
	}
	
	private static class EventSupportLayoutComparator implements Comparator<LayoutVO> {

		@Override
		public int compare(LayoutVO o1, LayoutVO o2) {
			return textCollator.compare(o1.getName(), o2.getName());
		}
	}
	
	private static class EventSupportEventComparator implements Comparator<EventSupportEventVO> {

		@Override
		public int compare(EventSupportEventVO o1, EventSupportEventVO o2) {
			final MetaProvider metaProv = MetaProvider.getInstance();
			String entityNameO1 = metaProv.checkEntity(o1.getEntityUID()) ? metaProv.getEntity(o1.getEntityUID()).getEntityName() : "";
			String entityNameO2 = metaProv.checkEntity(o2.getEntityUID()) ? metaProv.getEntity(o2.getEntityUID()).getEntityName() : "";
			return textCollator.compare(entityNameO1, entityNameO2);
		}
	}
	
	private static class EventSupportStateModelComparator implements Comparator<StateModelVO> {

		@Override
		public int compare(StateModelVO o1, StateModelVO o2) {
			return textCollator.compare(o1.getName(), o2.getName());
		}
	}
	
	private static class EventSupportEntityObjectComparator implements Comparator<MasterDataVO> {

		private final UID[] fieldCompare;
		
		public EventSupportEntityObjectComparator(UID...pFieldCompare) {
			this.fieldCompare = pFieldCompare;
		}
		
		@Override
		public int compare(MasterDataVO o1, MasterDataVO o2) {
			String s1 = null;
			String s2 = null;

			for (UID fieldUID : fieldCompare) {
				if (s1 == null) {
					s1 = (String) o1.getFieldValue(fieldUID, String.class);
				}
				if (s2 == null) {
					s2 = (String) o2.getFieldValue(fieldUID, String.class);
				}
			}

			return textCollator.compare(s1, s2);
		}
	}
	
	private static class EventSupportMasterDataComparator implements Comparator<MasterDataVO<?>> {

		private final UID fieldCompare;
		
		public EventSupportMasterDataComparator(UID pFieldCompare) {
			this.fieldCompare = pFieldCompare;
		}
		
		@Override
		public int compare(MasterDataVO<?> o1, MasterDataVO<?> o2) {
			return textCollator.compare(o1.getFieldValue(fieldCompare, String.class), o2.getFieldValue(fieldCompare, String.class));
		}
	}
	
	private static class EventSupportButtonComparator implements Comparator<ButtonVO> {
		
		@Override
		public int compare(ButtonVO o1, ButtonVO o2) {
			return textCollator.compare(o1.getName(), o2.getName());
		}
	}

	public static class EventSupportTabRestoreController extends TabRestoreController {
		
		@Override
		public void restoreFromPreferences(String preferencesXML,
				MainFrameTab tab) throws Exception {
			
			final EventSupportManagementController ctrl = new EventSupportManagementController(tab.getTabbedPane());
			final EventSupportTreeNode treenodeRoot = new EventSupportTreeNode(ctrl, null, null,
					"Regelsourcen", null, null,
					EventSupportTargetType.ROOT, true,false,null, true);
			
			final EventSupportTargetTreeNode treenodeRootTargets = new EventSupportTargetTreeNode(ctrl, null, null, 
					SpringLocaleDelegate.getInstance().getMessage("ExplorerController.32","Regelzuweisung"),
					SpringLocaleDelegate.getInstance().getMessage("ExplorerController.32","Regelzuweisung"), 
					SpringLocaleDelegate.getInstance().getMessage("ExplorerController.32","Regelzuweisung"), 
					EventSupportTargetType.ROOT, false,false,null);
			
			ctrl.viewEventSupportManagement = new EventSupportView(treenodeRoot, treenodeRootTargets);
			ctrl.esActionHandler = new EventSupportActionHandler(ctrl.viewEventSupportManagement);
			ctrl.viewEventSupportManagement.setActionMap(ctrl.esActionHandler.getActionMaps());
			ctrl.viewEventSupportManagement.showGui();
			tab.add(ctrl.viewEventSupportManagement);
			tab.setTabRestoreController(new EventSupportTabRestoreController());
		}

		@Override
		public boolean validate(String preferencesXML) {
			return true;
		}
	}
}
