package org.nuclos.client.rule.server.panel;

import java.awt.*;
import java.util.Map;

import javax.swing.*;

import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.model.EventSupportCommunicationPortPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;

public class EventSupportCommunicationPortPropertyPanel extends
		AbstractEventSupportPropertyPanel {

	private final EventSupportCommunicationPortPropertiesTableModel model;
	private final Map<EventSupportActions, AbstractAction> actionMapping;
	
	public EventSupportCommunicationPortPropertyPanel(Map<EventSupportActions, AbstractAction> pActionMapping) {

		this.model = new EventSupportCommunicationPortPropertiesTableModel(this);
		this.actionMapping = pActionMapping;
		
		setLayout(new BorderLayout());
		
		createPropertiesTable();		
	}
	
	@Override
	protected EventSupportPropertiesTableModel getPropertyModel() {
		return this.model;
	}

	@Override
	public Map<EventSupportActions, AbstractAction> getActionMapping() {
		return this.actionMapping;
	}

	@Override
	public ActionToolBar[] getActionToolbarMapping() {
		return new ActionToolBar[] {
				new ActionToolBar(EventSupportActions.ACTION_DELETE_COMMUNICATION_PORT, true),
				new ActionToolBar(EventSupportActions.ACTION_MOVE_UP_COMMUNICATION_PORT, true),
				new ActionToolBar(EventSupportActions.ACTION_MOVE_DOWN_COMMUNICATION_PORT, true),
		};
	}

	@Override
	protected String getPreferenceNodeName() {
		return EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_WORKINGSTEP;
	}
}
