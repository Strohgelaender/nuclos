package org.nuclos.client.rule.client.panel;

import java.awt.*;

import javax.swing.*;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import org.nuclos.client.rule.client.explorer.AbstractClientRuleSelectionNode;
import org.nuclos.client.rule.client.panel.ClientRuleCapturePanel.DefaultClientRuleTreeCellRenderer;

public class ClientRuleCaptureTreeCellRenderer extends JPanel implements
		TreeCellRenderer {

	private DefaultClientRuleTreeCellRenderer delegate; 
	private ClientRuleCaptureTreeCheckBox checkBox = new ClientRuleCaptureTreeCheckBox(); 
	private ClientRuleCaptureTreeSelectionModel selectionModel;
	
	 public ClientRuleCaptureTreeCellRenderer(DefaultClientRuleTreeCellRenderer delegate, ClientRuleCaptureTreeSelectionModel selectionModel){ 
	        this.delegate = delegate; 
	        this.selectionModel = selectionModel; 
	        setLayout(new BorderLayout()); 
	        setOpaque(false); 
	        checkBox.setOpaque(false); 
	    } 
	 
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
	    	Component renderer = delegate.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus); 
	    	
	    	if (value instanceof AbstractClientRuleSelectionNode) {
	    		boolean isSelectable = ( (AbstractClientRuleSelectionNode) value).isSelectable();
	    		
	    		if(isSelectable) {
	    			TreePath path = tree.getPathForRow(row); 
	    			if(path!=null){ 
	    				if(selectionModel.isPathSelected(path, true)) 
	    					checkBox.setState(ClientRuleCaptureTreeCheckBox.SELECTED); 
	    				else 
	    					checkBox.setState(selectionModel.isPartiallySelected(path) ? ClientRuleCaptureTreeCheckBox.DONT_CARE : ClientRuleCaptureTreeCheckBox.NOT_SELECTED); 
	    			} 
	    			
	    			removeAll(); 
	    			Font myFont = new Font("Tahoma", Font.PLAIN, getFont().getSize());
	    			checkBox.setFont(myFont);
	    			renderer.setFont(myFont);
	    			add(checkBox, BorderLayout.WEST); 
	    			add(renderer, BorderLayout.CENTER);	    			    			
	    		}
	    		else {
	    			return renderer;
	    		}
	    	}
	    	
        return this; 
	}

}
