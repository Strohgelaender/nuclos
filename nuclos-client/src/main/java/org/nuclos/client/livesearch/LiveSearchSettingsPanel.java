//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Created on 20.08.2010
 */
package org.nuclos.client.livesearch;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.ui.DefaultSelectObjectsPanel;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.model.CommonDefaultListModel;
import org.nuclos.client.ui.model.MutableListModel;
import org.nuclos.client.ui.model.SortedListModel;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.PreferencesException;

public class LiveSearchSettingsPanel extends JPanel {

	private DefaultSelectObjectsPanel<?> selPanel;
	private JCheckBox orderBox;
	private JCheckBox expertBox;

	public LiveSearchSettingsPanel() {
		super(new BorderLayout());

		final ArrayList<UID> savedSelected = new ArrayList<UID>();
		final ArrayList<UID> savedDeselected = new ArrayList<UID>();
		boolean bOrderEntities = false;
		boolean bExpertMode = false;
		try {
			final Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node("livesearch");
			final String[] savedSelectedArray = PreferencesUtils.getStringArray(prefs, "selected");
			for (String s : savedSelectedArray) {
				savedSelected.add(UID.parseUID(s));
			}
			final String[] savedDeselectedArray = PreferencesUtils.getStringArray(prefs, "deselected");
			for (String s : savedDeselectedArray) {
				savedDeselected.add(UID.parseUID(s));
			}
			Object obj = PreferencesUtils.getSerializable(prefs, "orderentities");
			if (obj instanceof Boolean) {
				bOrderEntities = (Boolean) obj;
			}
			obj = PreferencesUtils.getSerializable(prefs, "expertmode");
			if (obj instanceof Boolean) {
				bExpertMode = (Boolean) obj;
			}
			
		} catch (PreferencesException e) {
			Errors.getInstance().showExceptionDialog(this, "Exception while reading the preferences.", e);
		}

		JLabel label = new JLabel(SpringLocaleDelegate.getInstance().getResource(
				"livesearch.settings.text", ""));
		label.setBorder(new EmptyBorder(20, 20, 20, 20));
		
		JLabel label2 = new JLabel(SpringLocaleDelegate.getInstance().getResource(
				"livesearch.settings.order", ""));
		label2.setBorder(new EmptyBorder(20, 20, 20, 20));
		
		orderBox = new JCheckBox(label2.getText());
		orderBox.setSelected(bOrderEntities);
		
		JLabel label3 = new JLabel(SpringLocaleDelegate.getInstance().getResource(
				"livesearch.settings.expert", ""));
		label3.setBorder(new EmptyBorder(20, 20, 20, 20));

		expertBox = new JCheckBox(label3.getText());
		expertBox.setSelected(bExpertMode);
		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BorderLayout());
		textPanel.add(label, BorderLayout.NORTH);
		textPanel.add(orderBox, BorderLayout.CENTER);
		textPanel.add(expertBox, BorderLayout.SOUTH);
				
		add(textPanel, BorderLayout.NORTH);

		selPanel = new DefaultSelectObjectsPanel<Object>();
		selPanel.btnUp.setVisible(true);
		selPanel.btnDown.setVisible(true);
		add(selPanel, BorderLayout.CENTER);

		List<UID> allEntities = new ArrayList<UID>();
		Collection<EntityMeta<?>> colSystemEntities = CollectionUtils.select(MetaProvider.getInstance().getAllEntities(), new Predicate<EntityMeta<?>>() {
			@Override
			public boolean evaluate(EntityMeta<?> t) {
				return E.isNuclosEntity(t.getUID());
			}			
		});				
				
		Set<UID> systemEntities = CollectionUtils.transformIntoSet(colSystemEntities, new Transformer<EntityMeta<?>, UID>() {
			@Override
			public UID transform(EntityMeta<?> i) {
				return i.getUID();
			}
		});
		for (EntityMeta<?> md : MetaProvider.getInstance().getAllEntities())
			if (!systemEntities.contains(md.getUID()) && SecurityCache.getInstance().isReadAllowedForEntity(md.getUID()))
				allEntities.add(md.getUID());
		Collections.sort(allEntities);

		final ArrayList<UID> unselected = new ArrayList<UID>();
		final ArrayList<UID> selected = new ArrayList<UID>();
		for (UID s : savedSelected)
			if (allEntities.contains(s)) { // still existant?
				selected.add(s);
				allEntities.remove(s);
			}
		for (UID uid : allEntities)
			// Remaining: either deselected or new -> select
			if (savedDeselected.contains(uid))
				unselected.add(MetaProvider.getInstance().getEntity(uid).getUID());
			else
				selected.add(MetaProvider.getInstance().getEntity(uid).getUID());

		MutableListModel<UID> availModel = new SortedListModel<UID>(unselected, true);
		MutableListModel<UID> selectModel = new CommonDefaultListModel<UID>(selected);

		selPanel.getJListAvailableObjects().setModel(availModel);
		selPanel.getJListAvailableObjects().setCellRenderer(new DefaultListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				final JLabel lbl = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				if (value instanceof UID) {
					lbl.setText(SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(
							MetaProvider.getInstance().getEntity((UID)value)));
				}
				return lbl;
			}
		});
		selPanel.getJListSelectedObjects().setModel(selectModel);
		selPanel.getJListSelectedObjects().setCellRenderer(new DefaultListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				final JLabel lbl = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				if (value instanceof UID) {
					lbl.setText(SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(
							MetaProvider.getInstance().getEntity((UID)value)));
				}
				return lbl;
			}
		});

		selPanel.getJListAvailableObjects().addListSelectionListener(listSelListener);
		selPanel.getJListSelectedObjects().addListSelectionListener(listSelListener);
		selPanel.btnUp.addActionListener(up);
		selPanel.btnDown.addActionListener(down);
		selPanel.btnLeft.addActionListener(left);
		selPanel.btnRight.addActionListener(right);
	}

	private ListSelectionListener listSelListener = new ListSelectionListener() {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			boolean leftSel = selPanel.getJListAvailableObjects().getSelectedIndices().length > 0;
			selPanel.btnRight.setEnabled(leftSel);

			int[] si = selPanel.getJListSelectedObjects().getSelectedIndices();
			boolean rightSel = si.length > 0;
			selPanel.btnLeft.setEnabled(rightSel);

			selPanel.btnUp.setEnabled(rightSel && si[0] > 0);
			selPanel.btnDown.setEnabled(rightSel && si[si.length - 1] < selPanel.getJListSelectedObjects().getModel().getSize() - 1);
		}
	};

	private ActionListener up = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			moveUpDown(-1);
		}
	};

	private ActionListener down = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			moveUpDown(1);
		}
	};

	private ActionListener left = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			JList list = selPanel.getJListSelectedObjects();
			moveLeftRight((MutableListModel<UID>) list.getModel(), (MutableListModel<UID>) selPanel.getJListAvailableObjects().getModel(), list.getSelectedIndices());
		}
	};

	private ActionListener right = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			JList list = selPanel.getJListAvailableObjects();
			moveLeftRight((MutableListModel<UID>) list.getModel(), (MutableListModel<UID>) selPanel.getJListSelectedObjects().getModel(), list.getSelectedIndices());
		}
	};

	private void moveLeftRight(MutableListModel<UID> srcModel, MutableListModel<UID> dstModel, int[] si) {
		final ArrayList<UID> toTransport = new ArrayList<UID>();
		for (int i = si.length - 1; i >= 0; i--)
			toTransport.add(srcModel.remove(si[i]));
		for (UID s : toTransport)
			dstModel.add(s);
	}

	private void moveUpDown(int offset) {
		final JList list = selPanel.getJListSelectedObjects();
		final CommonDefaultListModel<UID> model = (CommonDefaultListModel<UID>) list.getModel();
		int start = offset < 0 ? 0 : model.getSize() - 1;
		int end = offset < 0 ? model.getSize() : -1;
		int[] si = list.getSelectedIndices();
		for (int i = start; i != end; i -= offset) {
			if (Arrays.binarySearch(si, i) >= 0) {
				Object thing = model.getElementAt(i);
				model.remove(i);
				model.add(i + offset, thing);
			}
		}
		for (int i = 0; i < si.length; i++)
			si[i] += offset;
		list.setSelectedIndices(si);
	}

	private String[] toArray(JList l) {
		final ArrayList<String> a = new ArrayList<String>();
		final ListModel m = l.getModel();
		for (int i = 0; i < m.getSize(); i++)
			a.add(((UID) m.getElementAt(i)).getStringifiedDefinition());
		return a.toArray(new String[a.size()]);
	}

	public void save() throws PreferencesException {
		Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node("livesearch");
		PreferencesUtils.putStringArray(prefs, "selected", toArray(selPanel.getJListSelectedObjects()));
		PreferencesUtils.putStringArray(prefs, "deselected", toArray(selPanel.getJListAvailableObjects()));
		PreferencesUtils.putSerializable(prefs, "orderentities", orderBox != null ? orderBox.isSelected() : false);
		PreferencesUtils.putSerializable(prefs, "expertmode", expertBox != null ? expertBox.isSelected() : false);
	}
}
