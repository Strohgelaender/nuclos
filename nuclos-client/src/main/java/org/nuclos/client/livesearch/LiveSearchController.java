//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.livesearch;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.prefs.Preferences;

import javax.annotation.PostConstruct;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.EntityFacadeDelegate;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.ui.*;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldMetaVO;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Factories;
import org.nuclos.common.collection.LazyInitMapWrapper;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.livesearch.ejb3.LiveSearchFacadeRemote;
import org.nuclos.common.lucene.LuceneSearchResult;
import org.nuclos.common.lucene.NuclosCoordinate;
import org.nuclos.common.lucene.NuclosCoordinate.AttachInfo;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;

// @Component
public class LiveSearchController implements LiveSearchSearchPaneListener, LiveSearchResultPaneListener {
	
	private static final Logger LOG = Logger.getLogger(LiveSearchController.class);

	private static final int MAX_RESULTS = 500;
	
	private static LiveSearchController INSTANCE;
	
	//

    private SearchComponent                     _searchComponent;
    private LiveSearchPane                      resultPane;

    private volatile String                     currentSearchText;
    private ArrayList<LiveSearchResultRow>      currentResult;

    // Search queue and the consumer thread
    private LinkedBlockingQueue<SearchDef>	    searchQueue;
    private SearchThread	                    searchThread;

    // Preparation queue and the consumer thread
    private LinkedBlockingQueue<Pair<SearchDef, List<LuceneSearchResult>>>
                                                preparationQueue;
    private PreparationThread                   preparationThread;

    private Bubble                              overflowMessage;
    private String                              bubbleMessage;
    
	// Spring injection
	
	private JFrame parentFrame;

	private ResourceCache resourceCache;
	
	private LiveSearchFacadeRemote liveSearchFacadeRemote;
	
	// end of Spring injection

    LiveSearchController() {
    	INSTANCE = this;
	}
    
    // @Autowired
    public final void setResourceCache(ResourceCache resourceCache) {
    	this.resourceCache = resourceCache;
    }
    
    // @Autowired
    public final void setParentFrame(MainFrame parentFrame) {
    	this.parentFrame = parentFrame; // @see 	NUCLOSINT-1615 spring will set null as main frame. so we do it @wireing livesearch controller in main frame
    	if (parentFrame != null) {
    		Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
				
				@Override
				public void eventDispatched(AWTEvent event) {
	//				 if(searchComponent.getButtonSelection()) {
						 if (event instanceof MouseEvent) {
							 MouseEvent me = (MouseEvent)event;
							 if (me.getID() == MouseEvent.MOUSE_CLICKED
									 && resultPane != null
									 && !me.getComponent().equals(resultPane)
									 && !me.getComponent().equals(resultPane.getTable())
									 && _searchComponent != null
									 && !me.getComponent().equals(_searchComponent)
									 && !me.getComponent().equals(_searchComponent.getButton())) {
				            	_searchComponent.setButtonSelection(false);
					            if(overflowMessage != null) {
					            	overflowMessage.dispose();
					            	overflowMessage = null;
					            }
							 }
						 }
	//				 }	
				}
			}, AWTEvent.MOUSE_EVENT_MASK);
    	}
    }
	
	// @Autowired
	public final void setLiveSearchFacadeRemote(LiveSearchFacadeRemote liveSearchFacadeRemote) {
		this.liveSearchFacadeRemote = liveSearchFacadeRemote;
	}
    
    public static LiveSearchController getInstance() {
    	return INSTANCE;
    }
    
    @PostConstruct
    public final void init() {
    	// this.parentFrame = parentFrame;
        currentResult = new ArrayList<LiveSearchResultRow>();

        searchQueue = new LinkedBlockingQueue<SearchDef>();
        searchThread = new SearchThread();

        preparationQueue = new LinkedBlockingQueue<Pair<SearchDef, List<LuceneSearchResult>>>();
        preparationThread = new PreparationThread();

        resultPane = new LiveSearchPane();
        resultPane.addLiveSearchPaneListener(this);

        bubbleMessage = SpringLocaleDelegate.getInstance().getResource("livesearch.controller.overflow", null);
    }

    public SearchComponent getSearchComponent() {
    	if (_searchComponent == null) {
    		_searchComponent = new SearchComponent();
    		_searchComponent.setMaximumSize(_searchComponent.getPreferredSize());

    		_searchComponent.addLiveSearchSearchPaneListener(this);
    		_searchComponent.addKeyListener(searchFieldListener);
    		_searchComponent.addFocusListener(new FocusAdapter() {
				@Override
	            public void focusLost(FocusEvent e) {
		            if(_searchComponent.getButtonSelection())
		            	_searchComponent.setButtonSelection(false);
		            if(overflowMessage != null) {
		            	overflowMessage.dispose();
		            	overflowMessage = null;
		            }
	            }});
    	}
        return _searchComponent;
    }
    
    public void setSearchComponentVisible(boolean visible) {
    	getSearchComponent().setSearchComponentVisible(visible);
    }

    private KeyListener searchFieldListener = new KeyListener() {
        private void conditionalForwad(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER
                || e.getKeyCode() == KeyEvent.VK_UP
                || e.getKeyCode() == KeyEvent.VK_DOWN) {
            	if(!getSearchComponent().getButtonSelection() && !currentResult.isEmpty())
            		getSearchComponent().setButtonSelection(true);
            	resultPane.dispatchEvent(e);
            	e.consume();
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        	if(e.getKeyCode() != KeyEvent.VK_ENTER)
        		conditionalForwad(e);
        	else {
        		if (getSearchComponent().getButtonSelection())
        			conditionalForwad(e);
        		if(!getSearchComponent().getButtonSelection() && !currentResult.isEmpty())
        			getSearchComponent().setButtonSelection(true);
        		e.consume();
        	}
        }

        @Override
        public void keyPressed(KeyEvent e) {
        	if(e.getKeyCode() != KeyEvent.VK_ENTER)
        		conditionalForwad(e);
            	
        	if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
        		if(getSearchComponent().getButtonSelection() || resultPane.getParent() != null) {
        			getSearchComponent().setButtonSelection(false);
        			e.consume();
        		}
        }
    };

    /**
     * Callback from the panel when the text has changed. Cancels running
     * searches, then initiates a new search with the new search string.
     */
    @Override
    public synchronized void searchTextUpdated(String newSearchText) {
        if(currentSearchText != null && newSearchText.equals(currentSearchText))
            return;

        if(!preparationThread.isAlive())
            preparationThread.start();
        if(!searchThread.isAlive())
            searchThread.start();

        currentSearchText = newSearchText;

        searchQueue.clear();
        currentResult.clear();
        
    	if(newSearchText.length() > 0) {
    		List<EntityMeta<?>> toSearch = getSearchEntities();
    		boolean bOrderEntities = orderEntites();
    		boolean expert = expertMode();
            searchQueue.offer(new SearchDef(100L, 0, 1));
        	searchQueue.offer(new SearchDef(toSearch, newSearchText, 1, bOrderEntities, expert));
            getSearchComponent().setCurrentProgress(0);
            getSearchComponent().setMaxProgress(1);
        } else {
        	getSearchComponent().setBackground(Color.WHITE);
        	getSearchComponent().setMaxBackgroundProgress(0);
        	getSearchComponent().setMaxProgress(0);
        }
    }

    private boolean orderEntites() {
    	try {
	    	Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node("livesearch");
	    	boolean ok = (Boolean)PreferencesUtils.getSerializable(prefs, "orderentities");
	    	return ok;
    	} catch (Exception e) {
    	}
    	return false;
    }
    
    private boolean expertMode() {
    	try {
	    	Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node("livesearch");
	    	boolean ok = (Boolean)PreferencesUtils.getSerializable(prefs, "expertmode");
	    	return ok;
    	} catch (Exception e) {
    	}
    	return false;
    }

    private ArrayList<EntityMeta<?>> getSearchEntities() {
    	// Create a list of all readable entities first:
    	Collection<EntityMeta<?>> colAll = MetaProvider.getInstance().getAllEntities();
    	colAll = CollectionUtils.select(colAll, new Predicate<EntityMeta<?>>() {
			@Override
			public boolean evaluate(EntityMeta<?> t) {
				return E.isNuclosEntity(t.getUID());
			}
		});
    	
        Set<UID> systemEntities = CollectionUtils.transformIntoSet(colAll,
            new Transformer<EntityMeta<?>, UID>() {
                @Override
                public UID transform(EntityMeta<?> i) {
                    return i.getUID();
                }});

        List<EntityMeta<?>> allEntities = new ArrayList<EntityMeta<?>>();
        for(EntityMeta<?> md : MetaProvider.getInstance().getAllEntities())
            if(!systemEntities.contains(md.getUID()) && SecurityCache.getInstance().isReadAllowedForEntity(md.getUID()))
            	allEntities.add(md);

        Collections.sort(allEntities, new Comparator<EntityMeta<?>>() {
            @Override
            public int compare(EntityMeta<?> o1, EntityMeta<?> o2) {
                String l1 = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(o1);
                String l2 = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(o2);
                return l1.compareTo(l2);
            }});

        final Map<UID, EntityMeta<?>> entityLookup
        	= CollectionUtils.generateLookupMap(allEntities, new Transformer<EntityMeta<?>, UID>() {
			@Override
            public UID transform(EntityMeta<?> i) {
				return i.getUID();
            }});

        // Read the entity names, that have been explicitly selected or
        // deselected. Note, that savedSelected is ordered according to
        // the user's prefs
		final ArrayList<UID> savedSelected = new ArrayList<UID>();
		final ArrayList<UID> savedDeselected = new ArrayList<UID>();
		try {
			final String[] savedSelectedArray = PreferencesUtils.getStringArray(
	        	ClientPreferences.getInstance().getUserPreferences().node("livesearch"), "selected");
	        for (String s : savedSelectedArray) {
	        	savedSelected.add(UID.parseUID(s));
			}
	        final String[] savedDeselectedArray = PreferencesUtils.getStringArray(
        		ClientPreferences.getInstance().getUserPreferences().node("livesearch"), "deselected");
	        for (String s : savedDeselectedArray) {
	        	savedDeselected.add(UID.parseUID(s));
			}
        }
        catch(PreferencesException e) {
        	LOG.warn("getSearchEntities failed: " + e);
        }

        // Put it all together
        final ArrayList<EntityMeta<?>> selected = new ArrayList<EntityMeta<?>>();
        for(UID s : savedSelected)
        	if(entityLookup.containsKey(s)) {  // still existant?
        		selected.add(entityLookup.get(s));
        		entityLookup.remove(s);
        	}
        for(EntityMeta<?> e : allEntities)  // Remaining: either deselected or new -> select
        	if(entityLookup.containsKey(e.getUID()))
	        	if(!savedDeselected.contains(e.getUID()))
	        		selected.add(e);

        return selected;
    }


    /**
     * Callback, when the user has activated a function on a search result
     */
    @Override
    public void functionAction(final Function function, final List<LiveSearchResultRow> rows) {
        switch(function) {
        case OPEN:
        case KB_OPEN:
        case OPEN_DETAILS:
            UIUtils.runCommand(Main.getInstance().getMainFrame(), new CommonRunnable() {
                @Override
                public void run() throws CommonBusinessException {
                	functionOpen(rows, function == Function.OPEN_DETAILS);
                	getSearchComponent().setButtonSelection(false);
                	SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								getSearchComponent().setButtonSelection(false);
								getSearchComponent().requestFocusInWindow();
							}
							catch (Exception e) {
								LOG.error("functionAction failed: " + e, e);
							}
						}
					});
                }
            });
            break;
        }
    }


    private void functionOpen(List<LiveSearchResultRow> rows, boolean individual) throws CommonBusinessException {
    	final Main main = Main.getInstance();
    	final MainController mc = main.getMainController();
    	final MainFrame mf = main.getMainFrame();
    	
    	Map<UID, ArrayList<LiveSearchResultRow>> byEntity
    	= new LazyInitMapWrapper<UID, ArrayList<LiveSearchResultRow>>(
    		new LinkedHashMap<UID, ArrayList<LiveSearchResultRow>>(),
    		Factories.cloneFactory(new ArrayList<LiveSearchResultRow>()));

    	for(LiveSearchResultRow row : rows)
    		byEntity.get(row.entityUID).add(row);

    	for(UID entity : byEntity.keySet()) {
    		if(MasterDataLayoutHelper.isLayoutMLAvailable(entity, false)) {
    			ArrayList<LiveSearchResultRow> openRows = byEntity.get(entity);
    			if(openRows.size() == 1 || individual) {
    				for(LiveSearchResultRow resRow : openRows)
    					mc.showDetails(resRow.entityUID, resRow.theObject.getId());
    			}
    			else {
    				List<Object> ids = new ArrayList<Object>();
    				for(LiveSearchResultRow resRow : openRows)
    					ids.add(resRow.theObject.getId());
    				CollectableSearchCondition cond
    				= SearchConditionUtils.getCollectableSearchConditionForIds(ids);

    				// TODO Multinuclet Refactoring generics
    				NuclosCollectController<?,?> collectController
    				= NuclosCollectControllerFactory.getInstance()
    					.newCollectController(entity, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
    				collectController.runViewResults(cond);
    			}
    		}
    		else {
    			if(byEntity.get(entity).size() == 1 || individual) {
	    			for(LiveSearchResultRow row : byEntity.get(entity)) {
	    	    		// Check alternatives:
	    	    		// - selection for more than one
	    	    		// - error, if none is found
	    	    		// - if only one has been found: open directly
	    	    		List<FieldMeta<?>> fieldMetas = getParentFieldsWithLayout(row);
	    	    		if(fieldMetas.size() > 1) {
	    	    			new ShowAsDialog(mf, row.theObject, fieldMetas).setVisible(true);
	    	    		}
	    	    		else if(fieldMetas.isEmpty()) {
	    	    			JOptionPane.showMessageDialog(mf, 
	    	    					SpringLocaleDelegate.getInstance().getResource("livesearch.controller.nolayout", "No layout available"));
	    	    		}
	    	    		else {
	    	    			FieldMeta<?> fm = fieldMetas.get(0);
	    	    			mc.showDetails(
	    	    				fm.getForeignEntity(),
	    	    				row.theObject.getFieldIds().get(fm.getUID()));
	    	    		}
	    			}
    			}
    			else {
    				// For multi-selection, we only accept one definite parent
    				// layout.
    				HashSet<FieldMeta<?>> intersection = null;
    				for(LiveSearchResultRow row : byEntity.get(entity))
    					if(intersection == null)
    						intersection = new HashSet<FieldMeta<?>>(getParentFieldsWithLayout(row));
    					else
    						intersection.retainAll(getParentFieldsWithLayout(row));

    				if(intersection.size() == 1) {
    					FieldMeta<?> fm = intersection.iterator().next();

        				Set<Long> ids = new LinkedHashSet<Long>();
        				for(LiveSearchResultRow res : byEntity.get(entity))        					
        					ids.add(res.theObject.getFieldIds().get(fm.getUID()));
        				
        				CollectableSearchCondition cond	= SearchConditionUtils.getCollectableSearchConditionForIds(ids);

        				// TODO Multinuclet Refactoring generics        				
        				NuclosCollectController<?,?> collectController
        				= NuclosCollectControllerFactory.getInstance()
        					.newCollectController(fm.getForeignEntity(), null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
        				collectController.runViewResults(cond);
    				}
    				else {
    	    			JOptionPane.showMessageDialog(mf, 
    	    					SpringLocaleDelegate.getInstance().getResource("livesearch.controller.nolayout", "No layout available"));
    				}
    			}
    		}
    	}
    }


    /**
     * For an entity, which does not have its own layout, obtain a list of
     * parent entities, which contain the concrete object and have the layout
     * set.
     *
     * Maybe this should be remodeled to return the entity and id instead,
     * so that the function interface remains the same for a recursive parent
     * search?
     *
     * @param row    the result row
     * @return       a list of field-metas of the result row, which contain FK-
     *               references, which in turn lead to an object which contains
     *               the given row as part of a details view.
     * @throws CommonBusinessException
     */
    private List<FieldMeta<?>> getParentFieldsWithLayout(LiveSearchResultRow row) throws CommonBusinessException {
    	List<FieldMeta<?>> res = new ArrayList<FieldMeta<?>>();
    	Map<UID, FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(row.entityMeta.getUID());
		for(FieldMeta<?> fieldMeta : fields.values()) {
    		// Field is foreign key, the target entity has a layout, and the
    		// reference is not null?
    		if(fieldMeta.getForeignEntity() != null
    			&& MasterDataLayoutHelper.isLayoutMLAvailable(fieldMeta.getForeignEntity(), false)
    			&& row.theObject.getFieldIds().get(fieldMeta.getUID()) != null) {
    				// Check, whether the layout contains the row's entity
	                Map<EntityAndField, UID> subStuff
	                = EntityFacadeDelegate.getInstance().getSubFormEntityAndParentSubFormEntityNames(
	                		MasterDataDelegate.getInstance().getLayoutUid(fieldMeta.getForeignEntity(), false));
	                for (EntityAndField ef : subStuff.keySet()) {
	                	if(ef.getEntity().equals(row.entityMeta.getUID())) {
	                		res.add(fieldMeta);
	                		break;
	                	}
	                }
    		} else if (fieldMeta.getForeignEntity() != null
        			&& !MasterDataLayoutHelper.isLayoutMLAvailable(fieldMeta.getForeignEntity(), false)
        			&& row.theObject.getFieldIds().get(fieldMeta.getUID()) != null
        			&& row.entityMeta.isDynamic()) {
    			
	    				Long iGenericObjectId = row.theObject.getFieldIds().get(fieldMeta.getUID());
	        			UID iModuleId = GenericObjectDelegate.getInstance().get(iGenericObjectId).getModule();
	        			
	        			// Check, whether the layout contains the row's entity
		                Map<EntityAndField, UID> subStuff
		                = EntityFacadeDelegate.getInstance().getSubFormEntityAndParentSubFormEntityNames(
		                		MasterDataDelegate.getInstance().getLayoutUid(iModuleId, false));
		                for(EntityAndField ef : subStuff.keySet()) {
		                	if(ef.getEntity().equals(row.entityMeta.getUID())) {
		                		FieldMetaVO<?> voMeta = new FieldMetaVO(fieldMeta);	                		 
		                		voMeta.setForeignEntity(iModuleId);
		                		res.add(voMeta);
		                		break;
		                	}
		                }
    		}
    	}
    	return res;
    }


    /**
     * Called from the preparation thread: transfers the results to the view
     * @param searchDef the underlying search definition
     * @param newRows the prepared rows
     */
    private synchronized void addResults(final SearchDef searchDef, final List<LiveSearchResultRow> newRows) {
        // if the search term which produced the results is no more the current
        // search term, the results are discarded.
    	if(searchDef.search.equals(currentSearchText))
            SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                    	try {
	                    	if(searchDef.search.equals(currentSearchText)) {
		                        currentResult.addAll(newRows);
		                        resultPane.setResultData(currentResult);
		                        if(getSearchComponent().hasFocus()) {
		                        	getSearchComponent().setButtonSelection(true);
			                        showResultPane();
		                        }
	                    	}
						}
						catch (Exception e) {
							LOG.error("addResults failed: " + e, e);
						}
                    }
                });
    }

    private synchronized void finishedSearch(final boolean overflowed) {
        SwingUtilities.invokeLater(
            new Runnable() {
                @Override
                public void run() {
                	try {
                		getSearchComponent().setMaxProgress(0);
                		getSearchComponent().setMaxBackgroundProgress(0);
	                    if(currentResult.isEmpty())
	                    	getSearchComponent().setBackground(new Color(0xff6565));
	
	                   if(overflowed) {
	                		overflowMessage = new Bubble(
	                				getSearchComponent(),
	                			bubbleMessage,
	                			10,
									BubbleUtils.Position.SW);
	                		overflowMessage.setVisible(true);
	                	}
					}
					catch (Exception e) {
						LOG.error("finishedSearch failed: " + e, e);
					}
                }
            });
    }

    private synchronized void startedSearch() {
        SwingUtilities.invokeLater(
            new Runnable() {
                @Override
                public void run() {
                	try {
	                	if(overflowMessage != null) {
	                		overflowMessage.dispose();
	                		overflowMessage = null;
	                	}
	                	getSearchComponent().setBackground(Color.WHITE);
					}
					catch (Exception e) {
						LOG.error("startSearch failed: " + e, e);
					}
                }
            });
    }

    /**
     * The search definition objects come in two types: either a real search
     * definition, or a delay
     */
    private static enum SearchDefType {
        DELAY, OVERFLOW, INDEX;
    }

    /**
     * Struct containing a search definition in terms of entity and search term.
     */
    private class SearchDef {
        SearchDefType type;
        // SEARCH:
        String search;
        EntityMeta<?> entity;
        Map<UID, FieldMeta<?>> fields;
        // DELAY
        long delay;

        // both
        int index;        // index of current search
        int groupSize;    // total size of current group
        
        List<UID>					   idxEntities; //Entites for Index Search
        boolean							   orderEntities;
        boolean 						   expert;

        private SearchDef(long delay, int index, int groupSize) {
            this.type = SearchDefType.DELAY;
            this.delay = delay;
            this.index = index;
            this.groupSize = groupSize;
        }
        
        private SearchDef(List<EntityMeta<?>> entities, String search, int groupSize, boolean orderEntities, boolean expert) {
        	this.type = SearchDefType.INDEX;
            this.search = search;
        	this.index = 0;
        	this.groupSize = groupSize;
        	this.idxEntities = new ArrayList<UID>();
        	this.orderEntities = orderEntities;
        	this.expert = expert;
        	for (EntityMeta<?> emd : entities) this.idxEntities.add(emd.getUID());
        }
    }

    /**
     * take() on a blocking queue, which simple gets repeated, if an underlying
     * interrupted exception is thrown.
     */
    private static <T> T pollInfinite(BlockingQueue<T> q) {
        T t = null;
        while(t == null) {
            try {
                t = q.take();
            }
            catch(InterruptedException e) {
    			LOG.info("pollInfinite: " + e);
            }
        }
        return t;
    }

    private void setProgress(final int curr, final int max, final boolean isSearch) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	try {
	                if(isSearch) {
	                    if(getSearchComponent().getMaxBackgroundProgress() != max)
	                    	getSearchComponent().setMaxBackgroundProgress(max);
	                    if(getSearchComponent().getCurrentBackgroundProgress() != curr)
	                    	getSearchComponent().setCurrentBackgroundProgress(curr);
	                }
	                else {
	                    if(getSearchComponent().getMaxProgress() != max)
	                    	getSearchComponent().setMaxProgress(max);
	                    if(getSearchComponent().getCurrentProgress() != curr)
	                    	getSearchComponent().setCurrentProgress(curr);
	                }
				}
				catch (Exception e) {
					LOG.error("setProgress failed: " + e, e);
				}
            }
        });
    }


    /**
     * The search-thread takes search definitions from the searchQueue, and
     * does the server-communication. The results get added to the preparationQueue.
     */
    private class SearchThread extends Thread {
        public SearchThread() {
            super("LiveSearchAction.SearchThread");
            setDaemon(true);
        }

        @Override
        public void run() {
            while(true) try {
                SearchDef nowSearching = pollInfinite(searchQueue);
 
				if (nowSearching.type == SearchDefType.DELAY) {
					setProgress(nowSearching.index, nowSearching.groupSize, true);
					try {
						Thread.sleep(nowSearching.delay);
					} catch (InterruptedException e) {
						LOG.info("SearchThread.run: " + e);
					}
					preparationQueue.offer(new Pair<SearchDef, List<LuceneSearchResult>>(nowSearching, null));
					continue;
				}
 
            	if(nowSearching.search.equals(currentSearchText)) {
            		// Overflow breaker: if the result display is already
            		// bigger than MAX, we fetch the further searches from
            		// the queue and send an OVERFLOW message to the prep-
            		// thread
            		if(currentResult.size() > MAX_RESULTS) {
            			SearchDef consume = searchQueue.peek();
            			while(consume != null && consume.search != null && consume.search.equals(nowSearching.search)) {
            				searchQueue.poll();
            				consume = searchQueue.peek();
            			}
            			setProgress(0, 0, true);
            			nowSearching.type = SearchDefType.OVERFLOW;
            			preparationQueue.clear();
	                    preparationQueue.offer(new Pair<SearchDef, List<LuceneSearchResult>>(nowSearching, Collections.<LuceneSearchResult>emptyList()));
	                    continue;
            		}
            		
            		List<LuceneSearchResult> res
        						= liveSearchFacadeRemote.nuclosIndexSearch(nowSearching.search);
            		//= liveSearchFacadeRemote.idxSearch(nowSearching.idxEntities, nowSearching.search, nowSearching.orderEntities, nowSearching.expert);
            		
                    if(!nowSearching.search.equals(currentSearchText))
                        continue;

                    setProgress(nowSearching.index, nowSearching.groupSize, true);
                    preparationQueue.offer(new Pair<SearchDef, List<LuceneSearchResult>>(nowSearching, res));
            	}
            } catch(Exception e) {
            	Errors.getInstance().showExceptionDialog(Main.getInstance().getMainFrame(), e);
            }
        }
    }


    /**
     * The preparation thread takes search-results from the preparationQueue,
     * transforms these into search result structures, and passes these on to
     * the function addResults above (which in turn passes them to the view)
     */
    private class PreparationThread extends Thread {
        public PreparationThread() {
            super("LiveSearchAction.PreparationThread");
            setDaemon(true);
        }

        @Override
        public void run() {
            OUTER:
            while(true) try {
                Pair<SearchDef, List<LuceneSearchResult>> searchResult
                    = pollInfinite(preparationQueue);

                switch(searchResult.x.type) {
                case DELAY:
                    startedSearch();
                    break;
                case INDEX: {
                    if(!searchResult.x.search.equals(currentSearchText))
                        continue OUTER;
                    List<LiveSearchResultRow> preparedResults = prepareResults(searchResult.x, searchResult.y);
                    if(!searchResult.x.search.equals(currentSearchText))
                        continue OUTER;
                    addResults(searchResult.x, preparedResults);
                    break;
                }
                case OVERFLOW: {
                	setProgress(0, 0, false);
                	finishedSearch(true);
                	continue OUTER;
                }
                }

                setProgress(searchResult.x.index, searchResult.x.groupSize, false);
                if(searchResult.x.index == searchResult.x.groupSize - 1)
                    finishedSearch(false);
           	} catch(Exception e) {
            	Errors.getInstance().showExceptionDialog(Main.getInstance().getMainFrame(), e);
            }        
        }

        /**
         * Converts a list of entityObjectVO to a list of LiveSearchResultRows
         * @param searchDef the underlying search definition
         * @param res       the list of EntityObjectVO as returned by the server
         * @return a list of LiveSearchResultRow suitable for the view
         */
        private List<LiveSearchResultRow> prepareResults(SearchDef searchDef, List<LuceneSearchResult> res) {
            ArrayList<LiveSearchResultRow> newRows = new ArrayList<LiveSearchResultRow>();
            if (res.size() == 1 && (Object)res.get(0) instanceof String) {
            	String msg = (String)(Object)res.get(0);
            	newRows.add(LiveSearchResultRow.msgLiveSearchRow(msg));
            	return newRows;
            }
            
            List<String> searchStrings = new ArrayList<String>();
            String lcSearchString = searchDef.search.toLowerCase();
            boolean quoted = lcSearchString != null && lcSearchString.startsWith("\"") && lcSearchString.startsWith("\"") && lcSearchString.length() >= 2;
            if (quoted) {
            	lcSearchString= lcSearchString.substring(1, lcSearchString.length() - 1);
            } 
            if (expertMode()) {
            	lcSearchString = StringEscapeUtils.unescapeJava(lcSearchString);
            	char[] luceneSpecials = new char[]{'+', '-', '!', '(', ')', '{', '}', '[', ']', '^', '"', '~', '*', '?', ':', '\\'};
            	for (char c : luceneSpecials) {
            		lcSearchString = lcSearchString.replace(c, ' ');
            	}
            	lcSearchString = lcSearchString.trim();
            }
        	searchStrings.add(lcSearchString);
            if (!quoted && lcSearchString.contains(" ")) {
            	StringTokenizer st = new StringTokenizer(lcSearchString, " ");
            	while (st.hasMoreTokens()) searchStrings.add(st.nextToken());
            }

            for (LuceneSearchResult p : res) {
            	EntityObjectVO<?> entityObject = p.getEo();
            	Set<UID> hideFields = Collections.EMPTY_SET;
            	
            	final UID parentEntity = p.getEntity();
            	final Object parentPk = p.getPk();
            	boolean openParentObject = false;
            	String sParentTitle = null;
            	EntityObjectVO<?> parentObject = null;
            	EntityMeta<?> parentMeta = null;
            	if (parentEntity != null && parentPk != null && !parentEntity.equals(entityObject.getDalEntity())) {
            		try {
						parentObject = EntityObjectDelegate.getInstance().get(parentEntity, p.getPk());
						parentMeta = MetaProvider.getInstance().getEntity(parentEntity);
						sParentTitle = SpringLocaleDelegate.getInstance().getTreeViewLabel(parentObject, MetaProvider.getInstance(), DataLanguageContext.getLanguageToUse()) + " (" 
								+ SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(entityObject.getDalEntity())) + ")";
						openParentObject = true;
					} catch (CommonPermissionException e) {
						// Permissions Exception? Wrong result from Server!
						LOG.warn("LiveSearchResult contains unopenable entry entity=" + parentEntity + ", pk=" + parentPk, e);
						continue;
					}
            	}

                // Icon: either the standard folder icon, or whatever is configured.
                // In any case: scaled to 32 pix
                ImageIcon rowIcon = Icons.getInstance().getIconDesktopFolder();
                EntityMeta<?> emdvo = searchDef.entity;
                if (emdvo == null) {
                	UID sEntity = entityObject.getDalEntity();
                	emdvo = MetaProvider.getInstance().getEntity(sEntity);
                }
                Map<UID, FieldMeta<?>> mapFields = searchDef.fields;
                if (mapFields == null) {
                	mapFields = MetaProvider.getInstance().getAllEntityFieldsByEntity(emdvo.getUID());
                }
                
                UID iconId = emdvo.getResource();
                String nuclosResource = emdvo.getNuclosResource();
                if(iconId != null)
                    rowIcon = resourceCache.getIconResource(iconId);
                else if (nuclosResource != null)
    					rowIcon = NuclosResourceCache.getNuclosResourceIcon(nuclosResource);

                String title = "";
                if (!emdvo.isDynamic()) {
                	String treeRep = SpringLocaleDelegate.getInstance().getTreeViewFromMetaDataVO(emdvo);
                    title = StringUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, treeRep, new ParameterTransformer(entityObject));
                } else {
                	EntityMeta<?> eEntityMetaData = null;
                	Map<UID, FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(emdvo.getUID());
                	for(FieldMeta<?> fieldMeta : fields.values()) {
                		if(fieldMeta.getForeignEntity() != null
                    			&& !MasterDataLayoutHelper.isLayoutMLAvailable(fieldMeta.getForeignEntity(), false)
                    			&& entityObject.getFieldIds().get(fieldMeta.getUID()) != null) {
                			try {
                				Long iGenericObjectId = entityObject.getFieldIds().get(fieldMeta.getUID());
                				GenericObjectVO genericObjectVO = GenericObjectDelegate.getInstance().get(iGenericObjectId);
                				
                    			UID sForeignEntity = genericObjectVO.getModule();
                    			// Check, whether the layout contains the row's entity
            	                Map<EntityAndField, UID> subStuff
            	                = EntityFacadeDelegate.getInstance().getSubFormEntityAndParentSubFormEntityNames(
            	                		MasterDataDelegate.getInstance().getLayoutUid(sForeignEntity, false));
            	                for(EntityAndField ef : subStuff.keySet())
            	                	if(ef.getEntity().equals(emdvo.getUID())) {
            	                		eEntityMetaData = MetaProvider.getInstance().getEntity(sForeignEntity);
            	            			String treeRep = SpringLocaleDelegate.getInstance().getTreeViewFromMetaDataVO(eEntityMetaData);
            	                        title = StringUtils.replaceParameters(treeRep, new ParameterTransformer(wrapGenericObjectVO(genericObjectVO)));
            	                		break;
            	                	}
            	                if (eEntityMetaData != null)
            	                	break;
                            }
                            catch(Exception e) {
                            	// ignore.
                            }
                		}
                	}

                    if (eEntityMetaData == null)
                    	continue;
                }
                
                Map<String, String> matchMap = new HashMap<String, String>();
                Map<Integer, Pair<String, String>> additionalMap = new TreeMap<Integer, Pair<String, String>>();
                // Line 2: list of attribute matches
                for(UID fieldName : mapFields.keySet()) {
                    FieldMeta<?> fdef = mapFields.get(fieldName);
                    SF eoField = SF.getByField(fdef.getFieldName());
                    if(eoField == null || eoField.isForceValueSearch()) {
                    	boolean isDocument = fdef.getDataType().equals("org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile");
                        if((fdef.getDataType().equals(String.class.getName()) || fdef.getDataType().equals(Date.class.getName())
                        		|| isDocument) && !hideFields.contains(fdef.getUID())) {
                            String fieldValue;
                            Object fld = entityObject.getFieldValue(fieldName);
    						if (fld instanceof Date) {
    							DateFormat df = SpringLocaleDelegate.getInstance().getDateFormat();
    							fieldValue = fld == null ? "" : df.format((Date)fld);
    						} else if (fld instanceof String) {
    							fieldValue = StringUtils.emptyIfNull((String)fld);
    						} else {
    							fieldValue = fld == null ? "" : fld.toString();
    						}
    						
    						if (fieldValue.trim().isEmpty()) {
    							continue;
    						}
    						String lcFieldValue = fieldValue.toLowerCase();
                            String fieldLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(fdef);

    						for (String ss : searchStrings) {
	                            int matchIndex = lcFieldValue.indexOf(ss);
	                            if(matchIndex >= 0) {
	                                int endIndex = matchIndex + ss.length();
	                                String hilighedValue
	                                    = fieldValue.substring(0, matchIndex)
	                                    + "<b>"
	                                    + fieldValue.substring(matchIndex, endIndex)
	                                    + "</b>"
	                                    + fieldValue.substring(endIndex);

	                                if (!matchMap.values().contains(hilighedValue)) {
		                                matchMap.put(fieldLabel, hilighedValue);	                                	
	                                }
	                                break;
	                            } else if (isDocument && fld != null) {
	                                matchMap.put(fieldLabel, fld.toString());
	                                break;
	                            }
    						}
    						
    						if (!matchMap.containsKey(fieldLabel) && fdef.getOrder() != null && fdef != null && fld != null) {
    							additionalMap.put(fdef.getOrder(), new Pair<String, String>(fieldLabel, fld.toString()));
    						}
                        }
                    }
                }
                
                if (matchMap.isEmpty()) {
                	AttachInfo attInfo = (AttachInfo) entityObject.getFieldValue(NuclosCoordinate.ATTKEY);
                	if (attInfo != null) {
                		matchMap.put("Anhang", attInfo.toString());                		
                	}
                }
                
                for (Integer order : additionalMap.keySet()) {
                	if (matchMap.size() >= 3) {
                		break;
                	}
                	Pair<String, String> pair = additionalMap.get(order);
                	matchMap.put(pair.x, pair.y);
                }

                LiveSearchResultRow row;
                if (openParentObject) {
                	row = new LiveSearchResultRow(parentObject, rowIcon, parentMeta, sParentTitle+": "+title, matchMap);
                } else {
                	row = new LiveSearchResultRow(entityObject, rowIcon, emdvo, title, matchMap);
                }
                
                newRows.add(row);
            }
            if (searchDef.type != SearchDefType.INDEX) {
            	Collections.sort(newRows, new Comparator<LiveSearchResultRow>() {
                @Override
                public int compare(LiveSearchResultRow o1, LiveSearchResultRow o2) {
                    return StringUtils.compareIgnoreCase(o1.titleString, o2.titleString);
                }});
            }
            return newRows;
        }

        private class ParameterTransformer implements Transformer<String, String> {
        	
            private final EntityObjectVO<?>	evo;

            public ParameterTransformer(EntityObjectVO<?> evo) {
                this.evo = evo;
            }

            @Override
            public String transform(String rid) {
                // the first element is the key; all other are flags separated by
                // ':'
                String[] elems = rid.split(":");

                String resIfNull = "";
                for(int i = 1; i < elems.length; i++) {
                    if(elems[i].startsWith("ifnull="))
                        resIfNull = elems[i].substring(7);
                }
                Object value = null;
                if (elems.length >= 1) {
                	value = evo.getFieldValues().get(UID.parseUID(elems[0]));
                }
                return value != null ? value.toString() : resIfNull;
            }
        }
    }

	/**
	 */
	private static EntityObjectVO<Long> wrapGenericObjectVO(GenericObjectVO go) {
		EntityObjectVO<Long> eo = new EntityObjectVO<Long>(go.getModule());

		eo.setPrimaryKey(go.getId());
		eo.setCreatedBy(go.getCreatedBy());
		eo.setCreatedAt(InternalTimestamp.toInternalTimestamp(go.getCreatedAt()));
		eo.setChangedBy(go.getChangedBy());
		eo.setChangedAt(InternalTimestamp.toInternalTimestamp(go.getChangedAt()));
		eo.setVersion(go.getVersion());
		
		for (DynamicAttributeVO attr : go.getAttributes()) {
			final UID uid = attr.getAttributeUID();
			if (attr.isRemoved()) {
				eo.removeFieldValue(uid);
				eo.removeFieldId(uid);
				eo.removeFieldUid(uid);
			} else {
				eo.setFieldValue(uid, attr.getValue());
				if (attr.getValueId() != null) {
					eo.setFieldId(uid, attr.getValueId());
				}
				if (attr.getValueUid() != null) {
					eo.setFieldUid(uid, attr.getValueUid());
				}
			}
		}
		eo.setFieldValue(SF.LOGICALDELETED.getUID(go.getModule()), go.isDeleted());

		return eo;
	}

    @Override
    public void buttonSelectionChanged(boolean shallShowResult) {
        if(shallShowResult && resultPane.getParent() == null && !currentResult.isEmpty())
            showResultPane();
        else
            hideResultPane();
    }


    // in AWT-Thread
    private void showResultPane() {
    	JLayeredPane layeredPane = parentFrame.getLayeredPane();

        if(resultPane.getParent() == null)
            layeredPane.add(resultPane, JLayeredPane.PALETTE_LAYER);

        Dimension layeredPaneSize = layeredPane.getSize();
        Dimension d = new Dimension(500, layeredPaneSize.height *6/10);

        int maxHeight = currentResult.size() * LiveSearchPane.ROW_HEIGHT + 2;
        if(d.height > maxHeight)
            d.height = maxHeight;

        Rectangle searchBounds = getSearchComponent().getBounds();

        Rectangle targetBounds = new Rectangle(
            searchBounds.x + searchBounds.width - d.width,
            searchBounds.y + searchBounds.height + 1,
            d.width,
            d.height
            );
        if(targetBounds.x < 0)
            targetBounds.x = 0;

        resultPane.setBounds(targetBounds);
        resultPane.validate();
        resultPane.repaint();
    }


    private void hideResultPane() {
        if(resultPane.getParent() != null)
            resultPane.getParent().remove(resultPane);
        if (parentFrame != null)
        	parentFrame.repaint();
        if(getSearchComponent().getButtonSelection())
        	getSearchComponent().setButtonSelection(false);
    }
}
