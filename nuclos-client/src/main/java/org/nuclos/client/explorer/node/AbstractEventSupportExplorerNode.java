package org.nuclos.client.explorer.node;

import java.awt.Color;
import java.awt.Component;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

import org.apache.log4j.Logger;
import org.nuclos.client.customcode.CodeDelegate;
import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetType;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTreeNode;
import org.nuclos.client.rule.server.EventSupportActionHandler;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.theme.NuclosThemeSettings;
import org.nuclos.client.ui.tree.TreeNodeAction;
import org.nuclos.common.E;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.customcode.valueobject.CodeVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.navigation.treenode.TreeNode;

public abstract class AbstractEventSupportExplorerNode extends ExplorerNode<EventSupportTreeNode> {

	private static final Logger LOG = Logger.getLogger(AbstractEventSupportExplorerNode.class);	
	
	public abstract String getPreferenceNodeName ();
	
	protected AbstractEventSupportExplorerNode(TreeNode treenode) {
		super(treenode);
	}

	public boolean importTransferData(Component parent, Transferable transferable, JTree tree) throws IOException, UnsupportedFlavorException {
		return true;
	}
	
	@Override
	public int getDataTransferSourceActions() {

		return DnDConstants.ACTION_COPY_OR_MOVE;
	}
	
	@Override
	public boolean getAllowsChildren() {
		return isLeaf() ? false : super.getAllowsChildren();
	}
	
	public List<TreeNodeAction> getTreeNodeActions(JTree tree) {
		
		final List<TreeNodeAction> result = new LinkedList<TreeNodeAction>();
		result.add(new RefreshAction(tree));

		// Add all node dependant Actions
		result.addAll(loadEventSupportDependentActions(tree));
			
		final ShowInOwnTabAction actShowInOwnTab = new ShowInOwnTabAction(tree);
		actShowInOwnTab.setEnabled(!this.getTreeNode().needsParent());
		result.add(actShowInOwnTab);
		result.add(TreeNodeAction.newSeparatorAction());
		result.add(new ExpandAction(tree));
		result.add(new CollapseAction(tree));
		
		return result;
	}
	

	/**
	 * refreshes the current node (and its children) and notifies the given tree model
	 * 
	 * @param tree the DefaultTreeModel to notify. Must contain this node.
	 * @throws CommonFinderException if the object presented by this node no longer exists.
	 */
	public void refresh(final JTree tree, boolean fullRefreshCurrent) throws CommonFinderException {
	
		DefaultTreeModel dtm = (DefaultTreeModel) tree.getModel();
		unloadChildren();

		this.getTreeNode().refresh();
		loadChildren(true);
		dtm.nodeStructureChanged(this);		
		tree.setSelectionRow(0);
	}
	
	protected List<TreeNodeAction> loadEventSupportDependentActions(JTree tree) {
		
		List<TreeNodeAction> result = new ArrayList<TreeNodeAction>();
		EventSupportTreeNode node = (EventSupportTreeNode) getTreeNode();
		
		if (EventSupportTargetType.EVENTSUPPORT_TYPE.equals(node.getTreeNodeType())) {
			result.add(new EventSupportActionHandler.EventSupportEditor(tree));
			result.add(TreeNodeAction.newSeparatorAction());
		}

		try {
			if (EventSupportTargetType.EVENTSUPPORT.equals(node.getTreeNodeType())) {				
				EventSupportSourceVO eventSupportByClassname = 
						EventSupportRepository.getInstance().getEventSupportByClassname(node.getNodeName());
				CodeVO code = internCreatedEventSupport(eventSupportByClassname.getClassname(), eventSupportByClassname.getPackage());
				if (code != null) {
					
					result.add(new EventSupportActionHandler.OpenEventSupportAction(tree, code));
					result.add(new EventSupportActionHandler.DeleteEventSupportAction(tree, code));
					result.add(TreeNodeAction.newSeparatorAction());					
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return result;
	}
	
	private static CodeVO internCreatedEventSupport(String classname, String package1) throws CommonFinderException {
		
		CodeVO retVal = null;
		
		List<CodeVO> allCodes = CodeDelegate.getInstance().getAll(E.SERVERCODE.getUID());
		for (CodeVO code : allCodes) {
			String fullName = (package1 != null && package1.trim().length() > 0) ? classname : classname;
			if (code.getName().equals(fullName)) {
				retVal = code;
				break;
			}
		}
		
		return retVal;
	}

	@Override
	public Color getColor() {
		EventSupportTreeNode node = ((EventSupportTreeNode) getUserObject());
		EventSupportTargetType treeNodeType = node.getTreeNodeType();
		if (treeNodeType != null) {
			switch (treeNodeType)
			{
				case ENTITY_INTEGRATION_POINT:
					return NuclosThemeSettings.HOME_TABBED_PANE_BACKGROUND.darker();
				default:
			}
		}
		return super.getColor();
	}

}
