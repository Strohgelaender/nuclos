//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class TreeTransferHandler extends TransferHandler {

	public class NodesTransferable implements Transferable {
		ConfigurationEntityTreeSubNode[] nodes;

		public NodesTransferable(List<ConfigurationEntityTreeSubNode> lstNodes) {
			this.nodes = lstNodes.toArray(new ConfigurationEntityTreeSubNode[lstNodes.size()]);
		}

		public Object getTransferData(DataFlavor flavor)
				throws UnsupportedFlavorException {
			if(!isDataFlavorSupported(flavor))
				throw new UnsupportedFlavorException(flavor);
			return nodes;
		}

		public DataFlavor[] getTransferDataFlavors() {
			return flavors;
		}

		public boolean isDataFlavorSupported(DataFlavor flavor) {
			return nodesFlavor.equals(flavor);
		}
	}

	private final static Logger LOG = LoggerFactory.getLogger(TreeTransferHandler.class);
	private final DataFlavor nodesFlavor;
	private final DataFlavor[] flavors;
	private List<ConfigurationEntityTreeSubNode> lstNodesToRemove;

	public TreeTransferHandler() {
		final String mimeType = DataFlavor.javaJVMLocalObjectMimeType + ";class=\"" + ConfigurationEntityTreeSubNode.class.getName() + "\"";
		try {
			nodesFlavor = new DataFlavor(mimeType);

			// support 1 flavor
			flavors = new DataFlavor[1];
			flavors[0] = nodesFlavor;
		} catch (final ClassNotFoundException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean canImport(final TransferSupport support) {
		if (!support.isDrop()) {
			return false;
		}
		support.setShowDropLocation(true);
		final JTree.DropLocation dl = (JTree.DropLocation)support.getDropLocation();
		final JTree tree = (JTree)support.getComponent();

		int dropRow = tree.getRowForPath(dl.getPath());
		int[] selRows = tree.getSelectionRows();
		
		for (int i = 0; i< selRows.length; ++i) {
			if(selRows[i] == dropRow) {
				return false;
			}
		}
		

		// do not allow COPY-action
		int action = support.getDropAction();
		if (action == COPY) {
			return false;
		}
		
		final Object parent = dl.getPath().getLastPathComponent();
		
		// nodes must be siblings
		// parent must be the same
		ConfigurationEntityTreeSubNode prevNode = null;
		for (final ConfigurationEntityTreeSubNode node : lstNodesToRemove) {
			if (!ObjectUtils.equals(parent, node.getParent())) {
				return false;
			}
			
			if (null != prevNode) {
				if (!prevNode.isNodeSibling(node)) {
					return false;
				}
			}
			prevNode = node;
		}
        
		return true;
	}

	@Override
	protected Transferable createTransferable(final JComponent c) {
		final JTree tree = (JTree)c;
		final TreePath[] paths = tree.getSelectionPaths();
		Transferable transferable = null;
		if (null != paths) {

			// copy transfer nodes, copy removed nodes
			final List<ConfigurationEntityTreeSubNode> lstCopyForTransfer = new ArrayList<ConfigurationEntityTreeSubNode>();
			final List<ConfigurationEntityTreeSubNode> lstRemoveAfterTransfer = new ArrayList<ConfigurationEntityTreeSubNode>();

			final ConfigurationEntityTreeSubNode node = (ConfigurationEntityTreeSubNode)paths[0].getLastPathComponent();
			final ConfigurationEntityTreeSubNode copy = copy(node);
			lstCopyForTransfer.add(copy);
			lstRemoveAfterTransfer.add(node);

			for(int i = 1; i < paths.length; i++) {
				ConfigurationEntityTreeSubNode next =
						(ConfigurationEntityTreeSubNode)paths[i].getLastPathComponent();
				// Do not allow higher level nodes to be added to list.
				if(next.getLevel() < node.getLevel()) {
					break;
				} else {
					ConfigurationEntityTreeSubNode nodeCopy = copy(next);
					if(next.getLevel() > node.getLevel()) {  // child node
						copy.add(nodeCopy);
						// node already contains child
					} else {                                        // sibling
						lstCopyForTransfer.add(nodeCopy);
						lstRemoveAfterTransfer.add(next);
					}
				}
			}
			
			lstNodesToRemove = lstRemoveAfterTransfer;
			transferable = new NodesTransferable(lstCopyForTransfer);
		}
		return transferable;
	}
	
	@Override
	protected void exportDone(JComponent source, Transferable data, int action) {
		/*
		if((action & MOVE) == MOVE) {
			JTree tree = (JTree)source;
			DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
			// Remove nodes saved in nodesToRemove in createTransferable.
			
			for(int i = 0; i < lstNodesToRemove.length; i++) {
				model.removeNodeFromParent(lstNodesToRemove[i]);
			}
			
			
		}
		*/
	}
	
	@Override
	public int getSourceActions(JComponent c) {
		return COPY_OR_MOVE;
	}
	
	@Override
	public boolean importData(TransferSupport support) {
		if (!canImport(support)) {
			return false;
		}
		
		// Extract transfer data.
		ConfigurationEntityTreeSubNode[] nodes = null;
        try {
            Transferable t = support.getTransferable();
            nodes = (ConfigurationEntityTreeSubNode[])t.getTransferData(nodesFlavor);
        } catch(final UnsupportedFlavorException ex) {
            LOG.error("UnsupportedFlavor: " + ex.getMessage(), ex);
        } catch(final java.io.IOException ex) {
            LOG.error("I/O error: " + ex.getMessage(), ex);
        }
        
        // Get drop location info.
        JTree.DropLocation dl =
                (JTree.DropLocation)support.getDropLocation();
        int childIndex = dl.getChildIndex();
        TreePath dest = dl.getPath();
        final DefaultConfigurationNode parent =
            (DefaultConfigurationNode)dest.getLastPathComponent();
        JTree tree = (JTree)support.getComponent();
        final ConfigurationTreeModel model = (ConfigurationTreeModel)tree.getModel();
        // Configure for drop mode.
        int index = childIndex;    // DropMode.INSERT
        if(childIndex == -1) {     // DropMode.ON
            index = parent.getChildCount();
        }
        
        // Add data to model.
        for(int i = 0; i < nodes.length; i++) {
            final ConfigurationEntityTreeSubNode cn = nodes[i];
			final int newIndex = index++;
			cn.getContent().setSortOrder(newIndex);
			if (cn.getContent().isFlagUnchanged()) {
				cn.getContent().flagUpdate();
			}
			model.insertNodeInto(cn, parent, newIndex);
        }
        
        for(ConfigurationEntityTreeSubNode node : lstNodesToRemove) {
			model.removeNodeFromParent(node, false);
		}
        
        fixSortOrder(parent);
        return true;
		
	}

	private final void fixSortOrder(final ConfigurationTreeNode<?> node) {
		LOG.info("fix configuration tree sort order {}", node);
		int countChild = node.getChildCount();
		for (int i = 0; i<countChild; ++i) {
			final ConfigurationEntityTreeSubNode childVO = (ConfigurationEntityTreeSubNode)node.getChildAt(i);
			childVO.getContent().setSortOrder(i);
			if (childVO.getContent().isFlagUnchanged()) {
				childVO.getContent().flagUpdate();
			}
		}
	}
	
	protected ConfigurationEntityTreeSubNode copy(ConfigurationEntityTreeSubNode node) {
		final ConfigurationEntityTreeSubNode clone = (ConfigurationEntityTreeSubNode)node.clone();
		// clone childs recursive
		for (int i = 0 ; i<node.getChildCount(); ++i) {
			final TreeNode childNode = node.getChildAt(i);
			if (childNode instanceof ConfigurationEntityTreeSubNode) {
				clone.add(copy((ConfigurationEntityTreeSubNode)childNode));
			}
		}
		return clone;
	}
	
	

}
