//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.node;

import java.awt.*;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.swing.*;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.datatransfer.MasterDataIdAndEntity;
import org.nuclos.client.remote.NoConnectionTimeoutRunner;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.rule.server.EventSupportCreationThread;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.E;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.server.dbtransfer.TransferFacadeRemote;
import org.nuclos.server.navigation.ejb3.TreeNodeFacadeRemote;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.AbstractNucletContentEntryTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentTreeNode;
/**
 * <code>ExplorerNode</code> presenting a <code>NucletContentTreeNode</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 *
 * @author	<a href="mailto:maik.stueker@nuclos.de">maik.stueker</a>
 * @version 01.00.00
 */
public class NucletContentExplorerNode extends ExplorerNode<NucletContentTreeNode> {

	private static final Logger LOG = Logger.getLogger(NucletContentExplorerNode.class);
	
	// former Spring injection
	
	private TreeNodeFacadeRemote treeNodeFacadeRemote;
	
	private TransferFacadeRemote transferFacadeRemote;
	
	// end of former Spring injection

	public NucletContentExplorerNode(TreeNode treenode) {
		super(treenode);
		
		setTreeNodeFacadeRemote(SpringApplicationContextHolder.getBean(TreeNodeFacadeRemote.class));
		setTransferFacadeRemote(SpringApplicationContextHolder.getBean(TransferFacadeRemote.class));
	}
	
	final void setTreeNodeFacadeRemote(TreeNodeFacadeRemote treeNodeFacadeRemote) {
		this.treeNodeFacadeRemote = treeNodeFacadeRemote;
	}

	final TreeNodeFacadeRemote getTreeNodeFacadeRemote() {
		return treeNodeFacadeRemote;
	}
	
	final void setTransferFacadeRemote(TransferFacadeRemote transferFacadeRemote) {
		this.transferFacadeRemote = transferFacadeRemote;
	}

	final TransferFacadeRemote getTransferFacadeRemote() {
		return transferFacadeRemote;
	}

	@Override
	public String getDefaultTreeNodeActionCommand(JTree tree) {
		final TreePath treePath = new TreePath(NucletContentExplorerNode.this);
		return tree.isCollapsed(treePath) ? ACTIONCOMMAND_EXPAND : ACTIONCOMMAND_COLLAPSE;
	}

	@Override
	public boolean importTransferData(Component parent,	Transferable transferable, JTree tree) throws IOException, UnsupportedFlavorException {

		if (transferable.isDataFlavorSupported(MasterDataIdAndEntity.dataFlavor)) {
			final Object transferData = transferable.getTransferData(MasterDataIdAndEntity.dataFlavor);
			final Collection<MasterDataIdAndEntity> collimp;
			if (transferData instanceof Collection) {
				collimp = (Collection<MasterDataIdAndEntity>)transferData;
			} else {
				collimp = Collections.singletonList((MasterDataIdAndEntity) transferData);
			}

			final TreeNodeFacadeRemote treeNodeFacadeRemote = getTreeNodeFacadeRemote();
			final TransferFacadeRemote transferFacadeRemote = getTransferFacadeRemote();
			final Map<AbstractNucletContentEntryTreeNode, MasterDataIdAndEntity> contents = 
					new HashMap<AbstractNucletContentEntryTreeNode, MasterDataIdAndEntity>();
			for (MasterDataIdAndEntity mdiden : collimp) {
				if (getTreeNode().getEntity().equals(mdiden.getEntityUid())) {
					UID eoid = (mdiden.getId() instanceof UID) ? (UID) mdiden.getId() : null;
					if (eoid != null) {
						contents.put(treeNodeFacadeRemote.getNucletContentEntryNode(getTreeNode().getEntity(), eoid),
							mdiden);
					}
				}
			}
			if (!contents.isEmpty()) {
				try {
					for (final AbstractNucletContentEntryTreeNode node : contents.keySet()) {
						NoConnectionTimeoutRunner.runSynchronized(() -> {
								transferFacadeRemote.removeNucletContents(Collections.singleton(node));
								return null;
						});
						contents.get(node).removeFromSourceTree();
					}
					NoConnectionTimeoutRunner.runSynchronized(() -> {
						transferFacadeRemote.addNucletContents(getTreeNode().getNuclet(), new HashSet<>(contents.keySet()));
						return null;
					});
					
					refresh(tree);
					
					EventSupportDelegate.getInstance().invalidateCaches();
					EventSupportRepository.getInstance().updateEventSupports();
					MasterDataCache.getInstance().invalidate(E.NUCLET.getUID());
					MasterDataCache.getInstance().invalidate(E.STATEMODEL.getUID());
					MasterDataCache.getInstance().invalidate(E.ENTITY.getUID());
					MasterDataCache.getInstance().invalidate(E.SERVERCODE.getUID());
					MasterDataCache.getInstance().invalidate(E.GENERATION.getUID());
					MasterDataCache.getInstance().invalidate(E.JOBCONTROLLER.getUID());
					
					// no errors here; we have to generate all BusinessObjects
					EventSupportCreationThread t = new EventSupportCreationThread(
							getExplorerController().getTabbedPane().getComponentPanel());
					t.start();

				} catch(Exception e) {
					Errors.getInstance().showExceptionDialog(getExplorerController().getTabbedPane().getComponentPanel(), e);
				}
			}

			return true;
		} else {
			return super.importTransferData(parent, transferable, tree);
		}
	}

	@Override
	public Icon getIcon() {
		return getIcon(getTreeNode().getEntity().getUID());
	}

	public static Icon getIcon(UID entityUID) {
		UID resId = MetaProvider.getInstance().getEntity(entityUID).getResource();
		String nuclosResource = MetaProvider.getInstance().getEntity(entityUID).getNuclosResource();
		if(resId != null) {
			ImageIcon standardIcon = ResourceCache.getInstance().getIconResource(resId);
			return MainFrame.resizeAndCacheTabIcon(standardIcon);
		} else if (nuclosResource != null){
			ImageIcon nuclosIcon = NuclosResourceCache.getNuclosResourceIcon(nuclosResource);
			if (nuclosIcon != null) return MainFrame.resizeAndCacheTabIcon(nuclosIcon);
		}
		return Icons.getInstance().getIconGenericObject16();
	}

}
