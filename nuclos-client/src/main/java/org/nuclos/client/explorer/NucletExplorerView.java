//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.explorer.node.NucletExplorerNode;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.maintenance.MaintenanceUtils;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.remote.NoConnectionTimeoutRunner;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.rule.server.EventSupportCreationThread;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.DefaultSelectObjectsPanel;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.SelectObjectsController;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.model.ChoiceList;
import org.nuclos.common.E;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.dbtransfer.TransferFacadeRemote;
import org.nuclos.server.navigation.ejb3.TreeNodeFacadeRemote;
import org.nuclos.server.navigation.treenode.nuclet.NucletTreeNode;
import org.nuclos.server.navigation.treenode.nuclet.content.AbstractNucletContentEntryTreeNode;

public class NucletExplorerView extends DefaultExplorerView {

	private static final long serialVersionUID = 5014512105916869563L;

	private static final Logger LOG = Logger.getLogger(NucletExplorerNode.class);

	private static final String PREFS_NODE_NUCLET_EXPLORER = "nucletExplorer";

	private static final String PREFS_NODE_ADDREMOVE_DIALOG_SIZE = "addRemoveDialogSize";

	private final Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node(PREFS_NODE_NUCLET_EXPLORER);

	private final NucletTreeNode nucletnode;

	// former Spring injection

	private TreeNodeFacadeRemote treeNodeFacadeRemote;

	private TransferFacadeRemote transferFacadeRemote;
	
	// end of former Spring injection

	public NucletExplorerView(NucletTreeNode treenode) {
		super(treenode, true);
		this.nucletnode = treenode;

		setTreeNodeFacadeRemote(SpringApplicationContextHolder.getBean(TreeNodeFacadeRemote.class));
		setTransferFacadeRemote(SpringApplicationContextHolder.getBean(TransferFacadeRemote.class));
	}

	final void setTreeNodeFacadeRemote(TreeNodeFacadeRemote treeNodeFacadeRemote) {
		this.treeNodeFacadeRemote = treeNodeFacadeRemote;
	}

	final TreeNodeFacadeRemote getTreeNodeFacadeRemote() {
		return treeNodeFacadeRemote;
	}
	
	final void setTransferFacadeRemote(TransferFacadeRemote transferFacadeRemote) {
		this.transferFacadeRemote = transferFacadeRemote;
	}

	final TransferFacadeRemote getTransferFacadeRemote() {
		return transferFacadeRemote;
	}

	@Override
	public List<JComponent> getToolBarComponents() {
		final JButton btnAddContent = new JButton();

		btnAddContent.setFocusable(false);

		btnAddContent.setAction(new AbstractAction(SpringLocaleDelegate.getInstance().getMessage(
				"NucletExplorerNode.1", "Zuweisen"), Icons.getInstance().getIconRelate()) {

			@Override
			public void actionPerformed(ActionEvent e) {
				cmdShowAddRemoveDialog(getJTree());
			}
			
			@Override
			public boolean isEnabled() {
				EntityObjectVO<?> nucletEO;
				try {
					nucletEO = EntityObjectDelegate.getInstance().get(E.NUCLET.getUID(), nucletnode.getId());
				} catch (CommonPermissionException e) {
					return false;
				}
				return super.isEnabled() && (
						Boolean.TRUE.equals(nucletEO.getFieldValue(E.NUCLET.source)) ||
						Boolean.TRUE.equals(nucletEO.getFieldValue(E.NUCLET.nuclon)));
			}
		});

		List<JComponent> result = new ArrayList<JComponent>();

		result.add(btnAddContent);

		return result;
	}

	private void cmdShowAddRemoveDialog(final JTree jTree) {
		final SelectObjectsController<AbstractNucletContentEntryTreeNode> selectCtrl =
				new SelectObjectsController<AbstractNucletContentEntryTreeNode>(jTree, new NucletContentSelectObjectPanel());

		if (!MaintenanceUtils.isNucletAssignmentPossible()) {
			final String msg = SpringLocaleDelegate.getInstance().getMsg("nuclet.assignment.outside.maintenance");
			JOptionPane.showMessageDialog(MainController.getMainFrame(), msg);
			return;
		}

		final TreeNodeFacadeRemote treeNodeFacadeRemote = getTreeNodeFacadeRemote();
		final TransferFacadeRemote transferFacadeRemote = getTransferFacadeRemote();
		List<AbstractNucletContentEntryTreeNode> curAvailable = treeNodeFacadeRemote.getAvailableNucletContents();
		final List<AbstractNucletContentEntryTreeNode> curSelected = treeNodeFacadeRemote.getNucletContent(nucletnode);

		ChoiceList<AbstractNucletContentEntryTreeNode> ro = new ChoiceList<AbstractNucletContentEntryTreeNode>();
		ro.set(new ArrayList<AbstractNucletContentEntryTreeNode>(curAvailable),	new AbstractNucletContentEntryTreeNode.Comparator());
		ro.setSelectedFields(new ArrayList<AbstractNucletContentEntryTreeNode>(curSelected));

		selectCtrl.setModel(ro);
		final boolean userPressedOk = selectCtrl.run(
				SpringLocaleDelegate.getInstance().getMessage(
						"NucletExplorerNode.2", "Nuclet Zuweisung"));
		final NucletContentSelectObjectPanel selectPanel = (NucletContentSelectObjectPanel) selectCtrl.getPanel();
		PreferencesUtils.putRectangle(prefs, PREFS_NODE_ADDREMOVE_DIALOG_SIZE, selectPanel.getBounds());

		if (userPressedOk) {
			final MainFrameTab parentFrame = getExplorerController().getTabFor(NucletExplorerView.this);
			// lock screen
			final LayerLock lock = parentFrame.lockLayer();
			final SwingWorker<Boolean, Void> worker = new SwingWorker<Boolean, Void>() {

				@Override
				protected Boolean doInBackground() throws Exception {
					Boolean result = Boolean.FALSE;
					try {

						final Collection<AbstractNucletContentEntryTreeNode> removed = CollectionUtils.subtract(curSelected, selectCtrl.getSelectedObjects());
						final Collection<AbstractNucletContentEntryTreeNode> added = CollectionUtils.subtract(selectCtrl.getSelectedObjects(), curSelected);

						NoConnectionTimeoutRunner.runSynchronized(() -> {
							transferFacadeRemote.updateNucletContents(nucletnode.getId(),
									new HashSet<>(added),
									new HashSet<>(removed));
							return null;
						});

				getExplorerController().refreshTab(NucletExplorerView.this);
				
				EventSupportDelegate.getInstance().invalidateCaches();
				EventSupportRepository.getInstance().updateEventSupports();
				MasterDataCache.getInstance().invalidate(E.NUCLET.getUID());
				MasterDataCache.getInstance().invalidate(E.STATEMODEL.getUID());
				MasterDataCache.getInstance().invalidate(E.ENTITY.getUID());
				MasterDataCache.getInstance().invalidate(E.SERVERCODE.getUID());
				MasterDataCache.getInstance().invalidate(E.GENERATION.getUID());
				MasterDataCache.getInstance().invalidate(E.JOBCONTROLLER.getUID());
				
				// no error here; so we can recompile BOs, SMOs and all rules
				EventSupportCreationThread t = new EventSupportCreationThread(NucletExplorerView.this);
				t.start();

					} catch(Exception e) {
						Errors.getInstance().showExceptionDialog(getExplorerController().getTabbedPane().getComponentPanel(), e);
					} finally {
						// lock screen
						parentFrame.unlockLayer(lock);
						result = true;
					}
					return result;
				}
				
				
			};
			worker.run();
		}
	}

	private class NucletContentSelectObjectPanel<T> extends DefaultSelectObjectsPanel<T> {

		public NucletContentSelectObjectPanel() {
			btnDown.setVisible(false);
			btnUp.setVisible(false);
			setPreferredSize(PreferencesUtils.getRectangle(prefs, PREFS_NODE_ADDREMOVE_DIALOG_SIZE, 720, 480).getSize());
		}

		@Override
		protected JList newList() {
			final JList result = super.newList();
			result.setCellRenderer(new DefaultListCellRenderer() {

				@Override
				public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
					if (value instanceof AbstractNucletContentEntryTreeNode) {
						AbstractNucletContentEntryTreeNode node = (AbstractNucletContentEntryTreeNode) value;
						String text = node.getLabelWithEntity();
						JLabel lb = new JLabel(text, NucletExplorerView.getIcon(node.getEntityUID()), SwingConstants.LEFT);
						if (isSelected) {
							lb.setOpaque(true);
							lb.setBackground(result.getSelectionBackground());
							lb.setForeground(result.getSelectionForeground());
						}
						return lb;
					}
					return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				}
			});
			return result;
		}
	}

	private static Icon getIcon(UID entity) {
		UID resId = MetaProvider.getInstance().getEntity(entity).getResource();
		String nuclosResource = MetaProvider.getInstance().getEntity(entity).getNuclosResource();
		if(resId != null) {
			ImageIcon standardIcon = ResourceCache.getInstance().getIconResource(resId);
			return MainFrame.resizeAndCacheTabIcon(standardIcon);
		} else if (nuclosResource != null){
			ImageIcon nuclosIcon = NuclosResourceCache.getNuclosResourceIcon(nuclosResource);
			if (nuclosIcon != null) return MainFrame.resizeAndCacheTabIcon(nuclosIcon);
		}
		return Icons.getInstance().getIconGenericObject16();
	}

	private ExplorerController getExplorerController() {
		return Main.getInstance().getMainController().getExplorerController();
	}
	
	@Override
	public boolean isRestoreExpandendState() {
		return false;
	}
}
