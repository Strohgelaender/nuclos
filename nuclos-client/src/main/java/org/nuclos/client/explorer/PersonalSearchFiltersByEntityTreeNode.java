//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer;



import java.util.ArrayList;
import java.util.List;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.EntitySearchFilters;
import org.nuclos.client.searchfilter.SearchFilterTreeNode;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.navigation.treenode.AbstractTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;

/**
 * Tree node containing the personal search filters for a module.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 00.01.000
 */
public class PersonalSearchFiltersByEntityTreeNode extends AbstractTreeNode<Object> {
	
	private static final long serialVersionUID = -1980756173915510766L;

	private final UID entityUid;

	/**
	 * @param entityUid the module id, if any.
	 */
	public PersonalSearchFiltersByEntityTreeNode(UID entityUid) {
		super(null);
		this.entityUid = entityUid;
	}

	/**
	 * @return the entity id, if any.
	 */
	public UID getEntityUID() {
		return this.entityUid;
	}

	@Override
	public String getLabel() {
		return this.getEntityLabel();
	}

	@Override
	public String getDescription() {
		return getSpringLocaleDelegate().getMessage("PersonalSearchFiltersByEntityTreeNode.1","Pers\u00f6nliche Suchfilter f\u00fcr die Entit\u00e4t") + " \"" + this.getEntityLabel() + "\"";
	}

	private String getEntityLabel() {
		if (Modules.getInstance().isModule(this.getEntityUID())) {
			return MetaProvider.getInstance().getEntity(this.getEntityUID()).getEntityName();
		}
		return getSpringLocaleDelegate().getLabelFromMetaDataVO(MasterDataDelegate.getInstance().getMetaData(this.getEntityUID()));
	}

	@Override
	public List<? extends TreeNode> getSubNodesImpl() throws NuclosFatalException {
		final List<SearchFilterTreeNode> result = new ArrayList<SearchFilterTreeNode>();

		final List<EntitySearchFilter> lstFilter;
		try {
			lstFilter = EntitySearchFilters.forEntity(this.getEntityUID()).getAll();
		}
		catch (PreferencesException ex) {
			throw new NuclosFatalException(ex);
		}

		for (EntitySearchFilter searchfilter : lstFilter) {
			result.add(new SearchFilterTreeNode(searchfilter));
		}
		assert result != null;
		return result;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		// no refresh necessary as personal search filter tree nodes are static:
		return this;
	}

	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = super.hashCode();
	    result = prime * result + ((entityUid == null) ? 0 : entityUid.hashCode());
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if(this == obj)
		    return true;
	    if(!super.equals(obj))
		    return false;
	    if(getClass() != obj.getClass())
		    return false;
	    PersonalSearchFiltersByEntityTreeNode other = (PersonalSearchFiltersByEntityTreeNode) obj;
	    if(entityUid == null) {
		    if(other.entityUid != null)
			    return false;
	    }
	    else if(!entityUid.equals(other.entityUid))
		    return false;
	    return true;
    }

}	// class PersonalSearchFiltersByModuleTreeNode
