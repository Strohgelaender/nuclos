//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.relation.EntityRelationshipModel;
import org.nuclos.client.relation.EntityRelationshipModelEditPanel;
import org.nuclos.client.statemodel.admin.CollectableStateModel;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.DefaultEditView;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.common.E;
import org.nuclos.common.LafParameter;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Controller for collecting state models.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 * 
 * @deprecated Probably not in use.
 */
@Deprecated
public class ProzessWizardCollectController extends NuclosCollectController<UID,EntityRelationshipModel> {
	
	final CollectableTextField clcttfName = new CollectableTextField(
		EntityRelationshipModel.clcte.getEntityField(E.PROCESS.name.getUID()));

	final CollectableTextField clcttfDescription = new CollectableTextField(
		EntityRelationshipModel.clcte.getEntityField(E.PROCESS.description.getUID()));

	private final CollectPanel<UID,EntityRelationshipModel> pnlCollect = new EntityRelationshipCollectPanel(UID.UID_NULL, false, false);
	private final EntityRelationshipModelEditPanel pnlEdit;
	
	private MainFrame mf;
	
	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	protected ProzessWizardCollectController(MainFrame mf, MainFrameTab tabIfAny, NuclosCollectControllerCommonState state) {
		super(CollectableStateModel.clcte, tabIfAny, state);
		
		this.initialize(this.pnlCollect);
		
		this.mf = mf;
		
		getTab().setLayeredComponent(pnlCollect);
		
		pnlEdit = new EntityRelationshipModelEditPanel(mf);
		
		this.getDetailsPanel().setEditView(DefaultEditView.newDetailsEditView(pnlEdit, pnlEdit.newCollectableComponentsProvider()));
		
		getTab().setTitle("Relationen-Editor");
	}
	
	
	private class EntityRelationshipCollectPanel extends CollectPanel<UID,EntityRelationshipModel> {

		EntityRelationshipCollectPanel(UID entityId, boolean bSearchPanelAvailable, boolean bShowSearch) {
			super(entityId, bSearchPanelAvailable, bShowSearch, LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Details_Presentation, entityId), ControllerPresentation.DEFAULT);
		}

		@Override
		public ResultPanel<UID,EntityRelationshipModel> newResultPanel(UID entityId) {
			return new NuclosResultPanel<UID,EntityRelationshipModel>(entityId, ControllerPresentation.DEFAULT);
		}
	}
	
	
	@Override
	protected LayoutRoot<UID> getLayoutRoot() {
		return null;
	}
	
	@Override
	protected boolean isDeleteAllowed(EntityRelationshipModel clct) {
		return false;
	}

	@Override
	protected boolean isDeleteSelectedCollectableAllowed() {
		return false;
	}

	@Override
	protected boolean isCloneAllowed() {
		return false;
	}

	@Override
	protected boolean isNewAllowed() {
		return false;
	}

	@Override
	protected void deleteCollectable(EntityRelationshipModel clct, final Map<String, Serializable> applyMultiEditContext)
		throws CommonBusinessException {
	}

	@Override
	protected String getEntityLabel() {
		return null;
	}

	@Override
	protected EntityRelationshipModel insertCollectable(
		EntityRelationshipModel clctNew) throws CommonBusinessException {
		return null;
	}

	@Override
	public EntityRelationshipModel newCollectable() {
		return null;
	}

	@Override
	protected EntityRelationshipModel updateCollectable(
		EntityRelationshipModel clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext, boolean isCollectiveProcessing)
		throws CommonBusinessException {
		return null;
	}
	
	@Override
	protected void initialize(CollectPanel<UID, EntityRelationshipModel> pnlCollect) {
		super.initialize(pnlCollect);
		revalidateAdditionalSearchField();
	}


}	// class StateModelCollectController
