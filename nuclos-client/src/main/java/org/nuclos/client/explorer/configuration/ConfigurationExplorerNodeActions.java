//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.*;

import org.apache.commons.lang.ObjectUtils;
import org.jfree.util.Log;
import org.nuclos.client.explorer.NodeActionContext;
import org.nuclos.client.explorer.configuration.CreateNewExplorerNodeDialog.EditMode;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;

public class ConfigurationExplorerNodeActions {
	public static abstract class AbstractMultiNodeAction extends AbstractAction {

		private List<NodeActionContext<ConfigurationTreeNode<?>>> lstNodeContext;
		public AbstractMultiNodeAction(String name) {
			super(name);
		}

		public AbstractMultiNodeAction(String name, List<NodeActionContext<ConfigurationTreeNode<?>>> lstNodeContext) {
			super(name);
			this.lstNodeContext = lstNodeContext;
		}

		public List<NodeActionContext<ConfigurationTreeNode<?>>> getAllNodeContext() {
			return lstNodeContext;
		}

		@Override
		public abstract void actionPerformed(ActionEvent e);

	}

	public static abstract class AbstractNodeAction extends AbstractAction {

		private NodeActionContext<ConfigurationTreeNode<?>> nodeContext;
		public AbstractNodeAction(String name) {
			super(name);
		}

		public AbstractNodeAction(String name, NodeActionContext<ConfigurationTreeNode<?>> nodeContext) {
			super(name);
			this.nodeContext = nodeContext;
		}

		public NodeActionContext<ConfigurationTreeNode<?>> getNodeContext() {
			return nodeContext;
		}

		@Override
		public abstract void actionPerformed(ActionEvent e);

	}

	public static enum MenuAction {
		NEW_NODE,
		REMOVE_NODE,
		EDIT_NODE
	}
	
	
	
	
	
	

	public static Action editNodeAction(final MainFrameTab parent, final NodeActionContext<ConfigurationTreeNode<?>> nodeContext, final DefaultConfigurationExplorerView view) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		return new AbstractNodeAction(localeDelegate.getMessage("ConfigurationExplorer.editSubNode","Unterknoten bearbeiten"), nodeContext ) {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(getNodeContext().getNode().getParent() != null) {
					new CreateNewExplorerNodeDialog(parent, view, nodeContext, EditMode.EDIT);
				}
			}
		};
	}

	public static Action newNodeAction(final MainFrameTab parent, final NodeActionContext<ConfigurationTreeNode<?>> nodeContext, final DefaultConfigurationExplorerView view) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		return new AbstractNodeAction(localeDelegate.getMessage("ConfigurationExplorer.addSubNode","Unterknoten hinzufügen"), nodeContext ) {

			@Override
			public void actionPerformed(ActionEvent e) {
				new CreateNewExplorerNodeDialog(parent, view, nodeContext, EditMode.NEW);
			}
		};
	}

	public static Action removeNodeAction(final List<NodeActionContext<ConfigurationTreeNode<?>>> lstNodeContexts, final DefaultConfigurationExplorerView view) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		return new AbstractMultiNodeAction(localeDelegate.getMessage("ConfigurationExplorer.removeSubNode","Unterknoten entfernen"), lstNodeContexts) {

			@Override
			public void actionPerformed(ActionEvent e) {
				final String strNodes = StringUtils.join(", ", lstNodeContexts);
				final String message = localeDelegate.getMessage("ConfigurationExplorer.removeSubNode.askBeforeRemove", "Sollen folgende Knoten wirklich entfernt werden: \"{0}\"?", strNodes);
				final Object[] params = {message};
				int result = JOptionPane.showConfirmDialog(Main.getInstance().getMainController().getMainFrame(), params, localeDelegate.getMessage("ConfigurationExplorer.removeSubNode.warning", "Achtung!"), JOptionPane.YES_NO_OPTION);
				if (JOptionPane.YES_OPTION == result) {
					final ConfigurationTreeModel model = (ConfigurationTreeModel) view.getJTree().getModel();
					for (final NodeActionContext<ConfigurationTreeNode<?>> nodeContext : lstNodeContexts) {
						// omit root node
						if (ObjectUtils.equals(view.getJTree().getModel().getRoot(), nodeContext.getNode())) {
							continue;
						}
						model.removeNodeFromParent(nodeContext.getNode());
					}
				} else {
					Log.warn("remove nodes aborted");
				}

			}
		};
	}
}
