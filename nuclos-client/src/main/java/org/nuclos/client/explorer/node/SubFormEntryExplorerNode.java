//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.node;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.EntityFacadeDelegate;
import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.ui.*;
import org.nuclos.client.ui.tree.TreeNodeAction;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.metadata.TreeMetaProvider;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.navigation.treenode.SubFormEntryTreeNode;
import org.nuclos.server.navigation.treenode.SubFormTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;

/**
 * <code>ExplorerNode</code> presenting a <code>OldMasterDataTreeNode</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class SubFormEntryExplorerNode<TN extends SubFormEntryTreeNode> extends ExplorerNode<TN> {

	private static final Logger LOG = Logger.getLogger(SubFormEntryExplorerNode.class);
	private transient TreeMetaProvider treeProvider;

	public SubFormEntryExplorerNode(TreeNode treenode) {
		super(treenode);
		setTreeProvider((TreeMetaProvider)SpringApplicationContextHolder.getBean("treeService"));
	}

	//@Autowired
	public void setTreeProvider(TreeMetaProvider treeProvider) {
		this.treeProvider = treeProvider;
	}


	public TreeMetaProvider getTreeProvider() {
		return treeProvider;
	}

	@Override
	public List<TreeNodeAction> getTreeNodeActions(JTree tree) {
		final List<TreeNodeAction> result = new LinkedList<TreeNodeAction>(super.getTreeNodeActions(tree));

		result.add(TreeNodeAction.newSeparatorAction());
		result.add(this.newShowDetailsAction(tree, false));
		result.add(this.newShowDetailsAction(tree, true));

		Map<UID, Long> foreignReferences = getForeignReferences();
		for (UID sEntity : foreignReferences.keySet()) {
			result.add(new ShowReferenceAction(
					sEntity,
					foreignReferences.get(sEntity),
					tree, sEntity.getString()));
		}

		result.add(new RemoveAction(tree));
		return result;
	}

	protected class RemoveAction extends TreeNodeAction {

		public static final int KEY = KeyEvent.VK_DELETE;

		private Long iObjectId = null;
		private UID sEntity = null;
		private UID sSubFormEntity = null;
		private UID sSubFormForeignField = null;

		public RemoveAction(JTree tree) {
			super(ACTIONCOMMAND_REMOVE, 
					getSpringLocaleDelegate().getMessage("MasterDataExplorerNode.1", "L\u00f6schen")+ "...", tree);

			try {
				javax.swing.tree.TreeNode tnParent = SubFormEntryExplorerNode.this.getParent();
				if (tnParent instanceof SubFormExplorerNode && ((SubFormExplorerNode) tnParent).getTreeNode() instanceof SubFormTreeNode) {
					SubFormTreeNode sfTreeNode = (SubFormTreeNode) ((SubFormExplorerNode) tnParent).getTreeNode();

					iObjectId = IdUtils.toLongId(sfTreeNode.getTreeNodeObject().getId());
					sEntity = sfTreeNode.getTreeNodeObject().getEntityUID();
					sSubFormEntity = sfTreeNode.getMasterDataVO().getEntityObject().getDalEntity();
					//@TODO MultiNuclet.
					//sSubFormForeignField = sfTreeNode.getMasterDataVO().getFieldUid(
					//	MetaProvider.getInstance().getEntity(sfTreeNode.getMasterDataVO().getEntityObject().getDalEntity()).getFieldUnsafe("entity").getUID());

					if(Modules.getInstance().isModule(sEntity)) {
						// TODO Refactor: Not every time (GenericObjectTreeNode), could also be (SubFormExplorerNode) and others?
						UID iStateId = ((GenericObjectTreeNode)sfTreeNode.getTreeNodeObject()).getStateUID();
						setEnabled(SecurityCache.getInstance().isWriteAllowedForModule(sEntity, iObjectId) &&
								SecurityCache.getInstance().getSubFormPermission(sSubFormEntity, iStateId).includesWriting());
					} else {
						// @todo have we not permissions on subform in a masterdata object?
						setEnabled(SecurityCache.getInstance().isWriteAllowedForModule(sEntity, iObjectId));
					}
				} else {
					setEnabled(false);
				}
			} catch (Exception ex) {
				// TODO Refactor: This is a quick fix
				setEnabled(false);
				LOG.warn("TreeNodeAction.REMOVE disabled because of exception: " + ex.getMessage(), ex);
			}
		}



		@Override
		protected void customizeComponent(JComponent comp, boolean bDefault) {
			super.customizeComponent(comp, bDefault);
			comp.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if(e.getKeyChar() == KEY) {
						askAndRemove();
					}
				}
			});

			if (comp instanceof JMenuItem) {
				((JMenuItem)comp).setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
			}
		}

		@Override
		public void actionPerformed(ActionEvent ev) {
			askAndRemove();
		}

		public void askAndRemove() {
			if (isEnabled()) {
				final String sName = getTreeNode().getLabel();
				final String sMessage = getSpringLocaleDelegate().getMessage("SubFormEntryExplorerNode.3", "Wollen Sie den Unterformulareintrag \"{0}\" wirklich l\u00f6schen?", sName);
				final int iBtn = JOptionPane.showConfirmDialog(this.getParent(), sMessage, 
						getSpringLocaleDelegate().getMessage("SubFormEntryExplorerNode.2", "Unterformulareintrag l\u00f6schen"),
						JOptionPane.OK_CANCEL_OPTION);
				if (iBtn == JOptionPane.OK_OPTION) {
					remove(getJTree());
				}
			}
		}

		private void remove(JTree tree){
			if (iObjectId != null && sEntity != null && sSubFormEntity != null && sSubFormForeignField != null) {
				IDependentKey dependentKey = DependentDataMap.createDependentKey(sSubFormForeignField);
				try {
					if(Modules.getInstance().isModule(sEntity)) {
						GenericObjectWithDependantsVO gowdVO = GenericObjectDelegate.getInstance().getWithDependants(sEntity,
								iObjectId, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
						for (EntityObjectVO mdVO : gowdVO.getDependents().getData(dependentKey)) {
							if (mdVO.getId().equals(getTreeNode().getId())) {
								mdVO.flagRemove();
							}
						}
						GenericObjectDelegate.getInstance().update(gowdVO, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), false);
					} else {
						if (sSubFormForeignField != null) {
							MasterDataVO mdvo = MasterDataDelegate.getInstance().get(sEntity, iObjectId);

							Collection<EntityObjectVO<Long>> dependants = 
									MasterDataDelegate.getInstance().getDependentDataCollection(sSubFormEntity, sSubFormForeignField, null, iObjectId);

							for (EntityObjectVO<Long> mdVO : dependants) {
								if (mdVO.getPrimaryKey().equals(getTreeNode().getId())) {
									mdVO.flagRemove();
								}
							}
							DependentDataMap ddm = new DependentDataMap();
							
							ddm.setData(dependentKey, dependants);

							MasterDataDelegate.getInstance().update(sEntity, mdvo, ddm, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), false);
						}
					}

					javax.swing.tree.TreeNode tnParent = SubFormEntryExplorerNode.this.getParent();
					if (tnParent instanceof SubFormExplorerNode){
						((SubFormExplorerNode)tnParent).refresh(tree);
					}
				}
				catch(CommonBusinessException e) {
					LOG.warn("remove failed: " + e);
					showBubble(tree, "<html>"+Errors.formatErrorForBubble(e.getMessage())+"</html>");
				}
			}
		}
	}

	@Override
	public void handleKeyEvent(JTree tree, KeyEvent ev) {
		if(ev.getKeyChar() == RemoveAction.KEY) {
			(new RemoveAction(tree)).askAndRemove();

		}
	}

	private Map<UID, Long> getForeignReferences(){
		Map<UID, Long> result = new HashMap<UID, Long>();

		javax.swing.tree.TreeNode tnParent = getParent();
		if (tnParent instanceof SubFormExplorerNode && ((SubFormExplorerNode) tnParent).getTreeNode() instanceof SubFormTreeNode) {
			SubFormTreeNode sfTreeNode = (SubFormTreeNode) ((SubFormExplorerNode) tnParent).getTreeNode();

			try {
			final EntityTreeViewVO moduleEtvVO = getTreeProvider().getNode(sfTreeNode.getTreeNodeObject().getNodeId());
			final EntityTreeViewVO subFormEtvVO = getTreeProvider().getNode(sfTreeNode.getNodeId());
			

			//@TODO MultiNuclet.
			UID sModuleEntity = moduleEtvVO.getEntity();
			UID sSubFormEntity = subFormEtvVO.getEntity();
			
			for (FieldMeta efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(sSubFormEntity).values()) {
				if (efMeta.getForeignEntity() != null && !efMeta.getForeignEntity().equals(sModuleEntity)) {
					// Reference to other entity:
					if (MetaProvider.getInstance().getEntity(sSubFormEntity).isDynamic()) {
						result.put(EntityFacadeDelegate.getInstance().getBaseEntity(sSubFormEntity), (Long) getTreeNode().getId());
					} else {
						try {
							MasterDataVO mdVO = MasterDataDelegate.getInstance().get(sSubFormEntity, getTreeNode().getId());
							Object oId = mdVO.getFieldValue(efMeta.getUID());
							if (oId != null)
								result.put(efMeta.getForeignEntity(), (Long) oId);
						} catch (CommonFinderException e) {
							// do nothing
							LOG.warn("getForeignReferences failed: " + e);
						} catch (CommonPermissionException e) {
							// do nothing
							LOG.warn("getForeignReferences failed: " + e);
						}
					}
				}
			}
			} catch (CommonPermissionException ex) {
				LOG.error(ex.getMessage(), ex);
				result = Collections.emptyMap();
			}
		}

		return result;
	}

	private class ShowReferenceAction extends TreeNodeAction {

		private final UID entity;
		private final Long id;

		ShowReferenceAction(final UID entityUid, final Long id, JTree tree, String action) {
			super(action, 
					getSpringLocaleDelegate().getMessage("SubFormEntryExplorerNode.1", "{0} öffnen", 
							getSpringLocaleDelegate().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(entityUid))), tree);
			this.entity = entityUid;
			this.id = id;
		}

		@Override
		public void actionPerformed(ActionEvent ev) {
			this.cmdShowDetails(this.entity, this.id);
		}

		/**
		 * command: show the details of the leased object represented by the given explorernode
		 * @param explorernode
		 */
		private void cmdShowDetails(final UID entity, final Long id) {
			UIUtils.runCommand(this.getParent(), new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					Main.getInstance().getMainController().showDetails(entity, id);
				}
			});
		}
	}

	@Override
	public String getDefaultTreeNodeActionCommand(JTree tree) {
		if (newShowDetailsAction(tree, false).isEnabled()) {
			return getDefaultObjectNodeAction();
		}
		else if (getForeignReferences().size() == 1) {
			return getForeignReferences().keySet().iterator().next().getString();
		}
		return ACTIONCOMMAND_EXPAND;
	}

	private void showBubble(JComponent component, String message) {
		new Bubble(
				component,
				message,
				10,
				BubbleUtils.Position.NE)
		.setVisible(true);
	}

	@Override
	public Icon getIcon() {
		UID resId = MetaProvider.getInstance().getEntity(getTreeNode().getEntityUID()).getResource();
		String nuclosResource = MetaProvider.getInstance().getEntity(getTreeNode().getEntityUID()).getNuclosResource();
		if(resId != null) {
			ImageIcon standardIcon = ResourceCache.getInstance().getIconResource(resId);
			return MainFrame.resizeAndCacheTabIcon(standardIcon);
		} else if (nuclosResource != null){
			ImageIcon nuclosIcon = NuclosResourceCache.getNuclosResourceIcon(nuclosResource);
			if (nuclosIcon != null) return MainFrame.resizeAndCacheTabIcon(nuclosIcon);
		}
		return Icons.getInstance().getIconGenericObject16();
	}
}
