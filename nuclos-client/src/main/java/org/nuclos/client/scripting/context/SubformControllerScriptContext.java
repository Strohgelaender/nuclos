//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting.context;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.language.bm.Lang;
import org.apache.log4j.Logger;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.common.AbstractDetailsSubFormController;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.Utils;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.scripting.expressions.EntityExpression;
import org.nuclos.client.scripting.expressions.FieldIdExpression;
import org.nuclos.client.scripting.expressions.FieldPkExpression;
import org.nuclos.client.scripting.expressions.FieldRefObjectExpression;
import org.nuclos.client.scripting.expressions.FieldUidExpression;
import org.nuclos.client.scripting.expressions.FieldValueExpression;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;

public class SubformControllerScriptContext<PK> extends AbstractScriptContext<PK> {

	private static final Logger LOG = Logger.getLogger(SubformControllerScriptContext.class);

	private final CollectController<?,?> parent;
	private final AbstractDetailsSubFormController<?,?> parentSfc;
	private final AbstractDetailsSubFormController<?,?> sfc;
	private final Collectable<?> c;

	public SubformControllerScriptContext(CollectController<?,?> parent, AbstractDetailsSubFormController<?,?> parentSfc, 
			AbstractDetailsSubFormController<?,?> sfc, Collectable<?> c) {
		this.parent = parent;
		this.parentSfc = parentSfc;
		this.sfc = sfc;
		this.c = c;
	}

	public SubformControllerScriptContext(CollectController<?,?> parent, AbstractDetailsSubFormController<?,?> sfc, Collectable<?> c) {
		this(parent, null, sfc, c);
	}

	@Override
	public Object evaluate(FieldValueExpression exp) {
		return c.getValue(exp.getField());
	}

	@Override
	public PK evaluate(FieldIdExpression exp) {
		return (PK) c.getValueId(exp.getField());
	}
	
	@Override
	public PK evaluate(FieldPkExpression exp) {
		return (PK) c.getPrimaryKey();
	}
	@Override
	public UID evaluate(FieldUidExpression exp) {
		return (UID) c.getValueId(exp.getField());
	}

	@Override
	public ScriptContext evaluate(FieldRefObjectExpression exp) {
		if (!sfc.getEntityAndForeignKeyField().getEntity().equals(exp.getEntity())) {
			// this is supported by else case
			//throw new UnsupportedOperationException("Context reference expressions require current entity as source entity.");
		}

		FieldMeta<?> fieldmeta = MetaProvider.getInstance().getEntityField(exp.getField());
		if (fieldmeta.getForeignEntity() == null) {
			throw new UnsupportedOperationException("Context reference expressions require a reference field.");
		}

		if (parent != null && parent.getEntityUid().equals(fieldmeta.getForeignEntity()) 
				&& sfc.getEntityAndForeignKeyField().getField().equals(exp.getField())) {
			return new CollectControllerScriptContext(parent, parent.getDetailsController().getSubFormControllers());
		}
		else if (parentSfc != null && parentSfc.getEntityAndForeignKeyField().getEntity().equals(fieldmeta.getForeignEntity()) 
				&& sfc.getEntityAndForeignKeyField().getField().equals(exp.getField())) {
			return new SubformControllerScriptContext(parent, parentSfc, parentSfc.getCollectableTableModel().getRow(
					parentSfc.getSubForm().getJTable().getSelectionModel().getLeadSelectionIndex()));
		} else if(parentSfc != null && parentSfc.getEntityAndForeignKeyField().getEntity().equals(fieldmeta.getForeignEntity())
				){
			return new SubformControllerScriptContext(parent, parentSfc, parentSfc.getCollectableTableModel().getRow(
					parentSfc.getSubForm().getJTable().getSelectionModel().getLeadSelectionIndex()));
		}
		else {
			PK refId = evaluate(new FieldIdExpression(exp.getNuclet(), exp.getEntity(), exp.getField()));
			if (refId != null) {
				Collectable<PK> clct;
				try {
					clct = Utils.getCachedReferencedCollectable(exp.getEntity(), exp.getField(), refId);
				}
				catch (CommonBusinessException e) {
					LOG.warn("Failed to retrieve reference context.", e);
					return new NullCollectableScriptContext();
				}
				return new CollectableScriptContext<PK>(clct);
			}
			else {
				return new NullCollectableScriptContext();
			}
		}
	}

	@Override
	public List<ScriptContext> evaluate(EntityExpression exp) {
		if (sfc instanceof MasterDataSubFormController) {
			MasterDataSubFormController<?> msfc = (MasterDataSubFormController<?>) sfc;
			for (DetailsSubFormController<?,?> ctl : msfc.getChildSubFormController()) {
				if (ctl.getEntityAndForeignKeyField().getEntity().equals(exp.getEntity())) {
					List<ScriptContext> contexts = new ArrayList<ScriptContext>();
					try {
						for (Collectable<?> clct : ctl.getCollectables(true, false, false)) {
							contexts.add(new SubformControllerScriptContext<PK>(parent, sfc, ctl, clct));
						}
					} catch (CommonValidationException e) {
						LOG.error(e);
					}
					return contexts;
				}
			}
		}
		return null;
	}

	public CollectController<?, ?> getParentCollectController() {
		return parent;
	}

	public AbstractDetailsSubFormController<?, ?> getParentSubFormController() {
		return parentSfc;
	}

	public AbstractDetailsSubFormController<?, ?> getSubFormController() {
		return sfc;
	}

	public Collectable<?> getCollectable() {
		return c;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof SubformControllerScriptContext) {
			SubformControllerScriptContext that = (SubformControllerScriptContext)obj;
			return LangUtils.equal(parent, that.parent)
					&& LangUtils.equal(parentSfc, that.parentSfc)
					&& LangUtils.equal(sfc, that.sfc)
					&& LangUtils.equal(c, that .c);

		}
		return false;
	}

	@Override
	public int hashCode() {
		return LangUtils.hash(parent, parentSfc, sfc, c);
	}

	@Override
	public String toString() {
		return  "ParentCtlr:" + parent
				+ "\nParentSubCtrl:" + parentSfc
				+ "\nSubformCtrl:" + sfc
				+ "\nCollectable:" + c;
	}
}
