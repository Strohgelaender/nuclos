//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting.context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.Utils;
import org.nuclos.client.scripting.expressions.EntityExpression;
import org.nuclos.client.scripting.expressions.FieldIdExpression;
import org.nuclos.client.scripting.expressions.FieldPkExpression;
import org.nuclos.client.scripting.expressions.FieldRefObjectExpression;
import org.nuclos.client.scripting.expressions.FieldUidExpression;
import org.nuclos.client.scripting.expressions.FieldValueExpression;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;

public class CollectControllerScriptContext<PK> extends AbstractScriptContext<PK> {

	private static final Logger LOG = Logger.getLogger(CollectControllerScriptContext.class);

	private final CollectController<?,?> controller;
	private final List<DetailsSubFormController<PK,?>> sfcs;

	public CollectControllerScriptContext(CollectController<?,?> controller, List<DetailsSubFormController<PK, ?>> arrayList) {
		this.controller = controller;
		this.sfcs = arrayList;
	}

	@Override
	public Object evaluate(FieldValueExpression exp) {
		for (CollectableComponent comp : controller.getDetailCollectableComponentsFor(exp.getField())) {
			 // use getModel().getField() here.
			// getField() of collectablecomponent invokes makeCollectable(). so object could be set dirty.
			return comp.getModel().getField().getValue(); 
		}
		// enable access for intid
		final UID sControllerEntity = controller.getCollectableEntity().getUID();
		SF<?> idField = MetaProvider.getInstance().getEntity(sControllerEntity).getPk();
		UID uidPk = idField.getUID(sControllerEntity);
		if (RigidUtils.equal(uidPk, exp.getField())) {
			final UID sScriptEntity = exp.getEntity();			
			if(sScriptEntity != null && sControllerEntity != null && sScriptEntity.equals(sControllerEntity)) {
				return controller.getSelectedCollectableId();
			}
		}
		
		return null;
	}

	@Override
	public PK evaluate(FieldIdExpression exp) {
		for (CollectableComponent comp : controller.getDetailCollectableComponentsFor(exp.getField())) {
			try {
				return (PK) comp.getField().getValueId();
			} catch (CollectableFieldFormatException e) {
				LOG.warn("Failed to retrieve context value.", e);
				return null;
			}
		}
		return null;
	}
	
	@Override
	public PK evaluate(FieldPkExpression exp) {
		return (PK)controller.getSelectedCollectableId();
	}

	@Override
	public UID evaluate(FieldUidExpression exp) {
		for (CollectableComponent comp : controller.getDetailCollectableComponentsFor(exp.getField())) {
			try {
				return (UID) comp.getField().getValueId();
			} catch (CollectableFieldFormatException e) {
				LOG.warn("Failed to retrieve context value.", e);
				return null;
			}
		}
		return null;
	}

	@Override
	public ScriptContext evaluate(FieldRefObjectExpression exp) {
		if (!controller.getEntityUid().equals(exp.getEntity())) {
			throw new UnsupportedOperationException("Context reference expressions require current entity as source entity.");
		}

		FieldMeta<?> fieldmeta = MetaProvider.getInstance().getEntityField(exp.getField());
		if (fieldmeta.getForeignEntity() == null) {
			throw new UnsupportedOperationException("Context reference expressions require a reference field.");
		}

		PK refId = evaluate(new FieldIdExpression(exp.getNuclet(), exp.getEntity(), exp.getField()));
		if (refId != null) {
			Collectable<PK> clct;
			try {
				clct = Utils.getCachedReferencedCollectable(exp.getEntity(), exp.getField(), refId);
			}
			catch (CommonBusinessException e) {
				LOG.warn("Failed to retrieve reference context.", e);
				return new NullCollectableScriptContext();
			}
			return new CollectableScriptContext<PK>(clct);
		}
		else {
			return new NullCollectableScriptContext();
		}
	}

	@Override
	public List<ScriptContext> evaluate(EntityExpression exp) {
		for (DetailsSubFormController<?,?> ctl : sfcs) {
			if (ctl.getEntityAndForeignKeyField().getEntity().equals(exp.getEntity())) {
				List<ScriptContext> contexts = new ArrayList<ScriptContext>();

				try {
					//also add incomplete collectables to the context. @see NUCLOS-2749
					for (Collectable<?> clct : ctl.getCollectables(true, false, false)) {
						contexts.add(new SubformControllerScriptContext<Object>(controller, ctl, clct));
					}
				} catch (CommonValidationException e) {
					LOG.warn("Failed to retrieve child contexts.", e);
					return Collections.emptyList();
				}
				return contexts;
			}
		}
		return null;
	}
}
