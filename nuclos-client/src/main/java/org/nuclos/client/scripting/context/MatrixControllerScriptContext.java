//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting.context;

import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.common.MatrixController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.scripting.expressions.EntityExpression;
import org.nuclos.client.scripting.expressions.ExpressionEvaluator;
import org.nuclos.client.scripting.expressions.FieldIdExpression;
import org.nuclos.client.scripting.expressions.FieldPkExpression;
import org.nuclos.client.scripting.expressions.FieldRefObjectExpression;
import org.nuclos.client.scripting.expressions.FieldUidExpression;
import org.nuclos.client.scripting.expressions.FieldValueExpression;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.matrix.MatrixCollectable;
import org.nuclos.client.ui.matrix.MatrixTableModel;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NucletConstants;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.IdUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class MatrixControllerScriptContext extends AbstractScriptContext implements ExpressionEvaluator {

	private static final Logger LOG = Logger.getLogger(MatrixControllerScriptContext.class);

	private final MatrixController matrixController;
	private final Collectable cField;
	private final Collectable collectableRow;
	private final UID entity;
	private final UID field;
	private final int row;

	public MatrixControllerScriptContext(MatrixController ctl, Collectable collectableRow, Collectable c, UID entity, UID field, int row) {
		this.matrixController = ctl;
		this.collectableRow = collectableRow;
		this.cField = c;
		this.entity = entity;
		this.field = field;
		this.row = row;
	}
	
	public String getField() {
		EntityMeta<?> md = MetaProvider.getInstance().getEntity(entity);
		UID uidNuclet = md.getNuclet();
		String sLocalIdentifier = NucletConstants.DEFAULT_LOCALIDENTIFIER;
		
		for (MasterDataVO<?> mdvo : MasterDataCache.getInstance().get(E.NUCLET.getUID())) {
			if(mdvo.getPrimaryKey().equals(uidNuclet)){
				sLocalIdentifier = mdvo.getFieldValue(E.NUCLET.localidentifier);
				break;
			}
		}

		String sFieldname = MetaProvider.getInstance().getEntityField(field).getFieldName();
		return "#{" + sLocalIdentifier + "." + md.getEntityName() + "." + sFieldname + "}";
	}

	@Override
	public Object evaluate(FieldValueExpression exp) {
		UID sField = exp.getField();
		UID sEntity = exp.getEntity();
		Collectable clParent = matrixController.getParentObject();
		matrixController.stopEditing();
		if(clParent instanceof CollectableGenericObject) {
			UID sParentEntity = ((CollectableGenericObject)clParent).getCollectableEntity().getUID();
			if(sParentEntity.equals(sEntity)) {
				for (CollectableComponent comp : matrixController.getCollectController().getDetailCollectableComponentsFor(exp.getField())) {
					return comp.getModel().getField().getValue(); 
				}
			}
		}
		else if(clParent instanceof CollectableMasterData) {
			UID sParentEntity = ((CollectableMasterData)clParent).getCollectableEntity().getUID();
			if(sParentEntity.equals(sEntity)) {
				for (CollectableComponent comp : matrixController.getCollectController().getDetailCollectableComponentsFor(exp.getField())) {
					return comp.getModel().getField().getValue(); 
				}
			}
		}
		
		MatrixTableModel model = (MatrixTableModel) matrixController.getMatrixComponent().getTableFixed().getModel();
		int col = -1;
		for(Collectable cl : model.getContent().keySet()) {
			col++;
			MatrixCollectable mc = (MatrixCollectable)cl;
			if(mc.getEntityFieldMetaDataVO() == null) continue;
			if(mc.getEntityFieldMetaDataVO().getUID().equals(sField)) {
				break;
			}			
		}
		if(matrixController.getMatrixComponent().getTableFixed().getModel().getRowCount() == 0) {
			return null;
		}
		MatrixCollectable mc = (MatrixCollectable) matrixController.getMatrixComponent().getTableFixed().getValueAt(row, col);
		return mc.getValue(new UID());

	}

	@Override
	public Long evaluate(FieldIdExpression exp) {
		return IdUtils.toLongId(cField.getValueId(exp.getField()));
	}
	
	@Override
	public Object evaluate(FieldPkExpression exp) {
		return null;
	}

	@Override
	public ScriptContext evaluate(FieldRefObjectExpression exp) {		
		return new MatrixControllerScriptContext(matrixController, collectableRow, cField, entity, field, row);		
	}

	@Override
	public List<ScriptContext> evaluate(EntityExpression exp) {		
		return null;
	}

	@Override
	public UID evaluate(FieldUidExpression exp) {
		return exp.getField();
	}
}
