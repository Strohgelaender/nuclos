//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource.admin;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.nuclos.client.command.CommonClientWorker;
import org.nuclos.client.command.CommonClientWorkerAdapter;
import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.console.ClientConsole;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.datasource.querybuilder.QueryBuilderConstants;
import org.nuclos.client.datasource.querybuilder.QueryBuilderEditor;
import org.nuclos.client.datasource.querybuilder.gui.ColumnEntry;
import org.nuclos.client.genericobject.ReportController;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.report.PrintController;
import org.nuclos.client.report.PrintController.PrintControllerEventListener;
import org.nuclos.client.report.PrintController.PrintControllerEventType;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.DefaultEditView;
import org.nuclos.client.ui.collect.detail.DetailsPanel;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.collect.search.ISearchStrategy;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.common.Actions;
import org.nuclos.common.ConsoleCommand;
import org.nuclos.common.E;
import org.nuclos.common.LafParameter;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.PrintResultTO;
import org.nuclos.common.report.valueobject.RecordGrantVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.IOUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPrintException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.NuclosUpdateException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

@SuppressWarnings("unused")
public abstract class AbstractDatasourceCollectController<T extends DatasourceVO> extends NuclosCollectController<UID,CollectableDataSource<T>> implements DatasourceEditController {

	private static final Logger LOG = Logger.getLogger(AbstractDatasourceCollectController.class);

	protected static final String PREFS_KEY_LASTIMPORTEXPORTPATH = "lastImportExportPath";

	protected final DatasourceDelegate datasourcedelegate = DatasourceDelegate.getInstance();

	protected DatasourceEditPanel pnlEdit;
	protected CollectPanel<UID,CollectableDataSource<T>> pnlCollect;
	
	protected ResultPanel.StatusBarButton btnExportResults = new ResultPanel.StatusBarButton(SpringLocaleDelegate.getInstance()
			.getText("MasterDataCollectController.7"));

	protected JButton btnImport;
	protected JButton btnExport;
	protected JButton btnValidate;

	/**
	 * Don't make this public!
	 *
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	@Deprecated
	protected AbstractDatasourceCollectController(CollectableEntity clcte, MainFrameTab tabIfAny) {
		super(clcte, tabIfAny, new NuclosCollectControllerCommonState());
		pnlCollect = new DatasourceCollectPanel(MetaProvider.getInstance().getEntity(clcte.getUID()).getUID(), false, false);
		
	}

	@Override
	public void detailsChanged(Component compSource) {
		super.detailsChanged(compSource);
	}
	
	@Override
	public void close() {
		if (!isClosed()) {
			pnlEdit = null;
			pnlCollect = null;
			btnImport = null;
			btnExport = null;
			btnValidate = null;
			
			super.close();
		}
	}
	
	@Override
	protected LayoutRoot<UID> getLayoutRoot() {
		return null;
	}
	
	protected void initializeDatasourceCollectController(DatasourceEditPanel pnlEdit) {
		this.pnlEdit = pnlEdit;
		this.initialize(this.pnlCollect);
		getTab().setLayeredComponent(pnlCollect);
		this.setupShortcutsForTabs(getTab());
		this.getDetailsPanel().setEditView(DefaultEditView.newDetailsEditView(pnlEdit, pnlEdit.getHeader().newCollectableComponentsProvider()));
		this.getCollectStateModel().addCollectStateListener(new DatasourcesCollectStateListener());
	}
	
	/**
	 * §todo This method probably shouldn't popup an option pane. @see CollectModel for a discussion
	 */
	@Override
	protected void deleteCollectable(CollectableDataSource<T> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final List<DatasourceVO> lstUsages = DatasourceDelegate.getInstance().getUsagesForDatasource(clct.getDatasourceVO());
		if (!lstUsages.isEmpty()) {
			final int iBtn = JOptionPane.showConfirmDialog(this.getTab(), getSpringLocaleDelegate().getMessage(
					"DatasourceCollectController.8","Diese Datenquelle wird in anderen Datenquellen verwendet.") + "\n" +
					getSpringLocaleDelegate().getMessage(
							"DatasourceCollectController.1","Das L\u00f6schen f\u00fchrt dazu, dass folgende Datenquellen nicht mehr ausf\u00fchrbar sind") + ":\n" + getUsagesAsString(lstUsages) +
					"\n" + getSpringLocaleDelegate().getMessage(
							"DatasourceCollectController.24","Wollen sie die Datenquelle dennoch l\u00f6schen?"), 
							getSpringLocaleDelegate().getMessage("DatasourceCollectController.20","Umbenennung best\u00e4tigen"), 
							JOptionPane.YES_NO_OPTION);
			if (iBtn != JOptionPane.OK_OPTION) {
				throw new CommonRemoveException(getSpringLocaleDelegate().getMessage(
						"DatasourceCollectController.15","L\u00f6schen wurde durch den Benutzer abgebrochen."));
			}
			DatasourceDelegate.getInstance().setInvalid(lstUsages);
		}

		datasourcedelegate.remove(clct.getDatasourceVO());

		pnlEdit.getQueryEditor().getController().refreshSchema();
	}

	/**
	 * @param clctEdited
	 * @return
	 * @throws NuclosBusinessException
	 */
//	@Override
	protected CollectableDataSource<T> updateCurrentCollectable(CollectableDataSource<T> clctEdited) throws CommonBusinessException {
		validate(clctEdited);

		final List<UID> lstUsedDatasources = pnlEdit.getQueryEditor().getUsedDatasources();

		final DatasourceVO datasourceVO = clctEdited.getDatasourceVO();

		CollectableDataSource<? extends DatasourceVO> db = findCollectableById(getEntityUid(), clctEdited.getId());
		if (db != null && !db.getDatasourceVO().getName().equals(datasourceVO.getName())) {
			final List<DatasourceVO> lstUsages = DatasourceDelegate.getInstance().getUsagesForDatasource(clctEdited.getDatasourceVO());
			if (!lstUsages.isEmpty()) {
				final int iBtn = JOptionPane.showConfirmDialog(this.getTab(), getSpringLocaleDelegate().getMessage(
						"DatasourceCollectController.9","Diese Datenquelle wird in anderen Datenquellen verwendet.") + "\n" +
						getSpringLocaleDelegate().getMessage("DatasourceCollectController.11","Eine Umbenennung f\u00fchrt dazu, dass folgende Datenquellen nicht mehr ausf\u00fchrbar sind:") + "\n" +
						getUsagesAsString(lstUsages) + "\n" + getSpringLocaleDelegate().getMessage(
								"DatasourceCollectController.23","Wollen sie dennoch speichern?"), 
								getSpringLocaleDelegate().getMessage("DatasourceCollectController.21","Umbenennung best\u00e4tigen"), 
								JOptionPane.YES_NO_OPTION);
				if (iBtn != JOptionPane.OK_OPTION) {
					throw new NuclosUpdateException(getSpringLocaleDelegate().getMessage(
							"DatasourceCollectController.18","Speichern wurde durch den Benutzer abgebrochen."));
				}
				DatasourceDelegate.getInstance().setInvalid(lstUsages);
			}
		}

		final DatasourceVO datasourceVOUpdated = datasourcedelegate.modify(datasourceVO, null, lstUsedDatasources);

		pnlEdit.getQueryEditor().getController().refreshSchema();

		return new CollectableDataSource<T>((T) datasourceVOUpdated);
	}

	@Override
	protected final CollectableDataSource<T> updateCollectable(CollectableDataSource<T> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext, boolean isCollectiveProcessing) throws CommonBusinessException {
		/** @todo implement */
		throw new NuclosFatalException(getSpringLocaleDelegate().getMessage(
				"DatasourceCollectController.17","Sammelbearbeitung ist hier noch nicht m\u00f6glich."));
	}

	/**
	 * @param clctNew
	 * @return
	 * @throws NuclosBusinessException
	 */
	@Override
	protected CollectableDataSource<T> insertCollectable(CollectableDataSource<T> clctNew) throws CommonBusinessException {
		if (clctNew.getId() != null) {
			throw new IllegalArgumentException("clctNew");
		}

		validate(clctNew);

		final DatasourceVO datasourceVO = datasourcedelegate.create(clctNew.getDatasourceVO(), null,
				pnlEdit.getQueryEditor().getUsedDatasources());

		pnlEdit.getQueryEditor().getController().refreshSchema();

		return new CollectableDataSource<T>((T) datasourceVO);
	}
	
	@Override
	protected String getEntityLabel() {
		return getSpringLocaleDelegate().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(entityUid));
	}

	/**
	 * @param clct
	 * @param bSearchTab
	 * @throws CollectableValidationException
	 */
	@Override
	protected void readValuesFromEditPanel(CollectableDataSource<T> clct, boolean bSearchTab) throws CollectableValidationException {
		// if we have a search tab here, this method must be extended.
		assert !bSearchTab;

		super.readValuesFromEditPanel(clct, bSearchTab);

		try {
			clct.getDatasourceVO().setSource(this.pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(false)));
		}
		catch (CommonBusinessException ex) {
			throw new CollectableValidationException(this.getCollectableEntity().getEntityField(E.DATASOURCE.source.getUID()), ex);
		}
	}

	/**
	 * @param clct
	 * @throws NuclosBusinessException
	 */
	@Override
	protected void validate(CollectableDataSource<T> clct) throws CommonValidationException {
		if (!pnlEdit.isModelUsed() && !pnlEdit.getSql().trim().toUpperCase().startsWith("SELECT")) {
			throw new CommonValidationException(
					getSpringLocaleDelegate().getMessage(
							"DatasourceCollectController.22","Ung\u00fcltiges SQL Statement. Es d\u00fcrfen nur SELECT Anweisungen ausgef\u00fchrt werden."));
		}
		validateParameters();
	}

	/** validate datasource parameters
	 *
	 */
	private void validateParameters() throws CommonValidationException {
		for (final DatasourceParameterVO paramvo : pnlEdit.getQueryEditor().getParameters()) {
			paramvo.validate();
		}
	}
	
	private void _setupResultToolBar() {
		//this.getResultPanel().registerToolBarAction(NuclosToolBarItems.INFO, getPointerAction());
	}

	/**
	 *
	 */
	@Override
	protected void setupDetailsToolBar() {
		super.setupDetailsToolBar();
		//final JToolBar toolbarCustomDetails = UIUtils.createNonFloatableToolBar();

		btnImport = new JButton(Icons.getInstance().getIconImport16());
		btnImport.setToolTipText(getSpringLocaleDelegate().getMessage("DatasourceCollectController.3","Datenquelle importieren"));
		btnImport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				cmdImport();
			}
		});
		//toolbarCustomDetails.add(btnImport);
		this.getDetailsPanel().addToolBarComponent(btnImport);

		btnExport = new JButton(Icons.getInstance().getIconExport16());
		btnExport.setToolTipText(getSpringLocaleDelegate().getMessage("DatasourceCollectController.2","Datenquelle exportieren"));
		btnExport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				cmdExport();
			}
		});
		//toolbarCustomDetails.add(btnExport);
		this.getDetailsPanel().addToolBarComponent(btnExport);

		btnValidate = new JButton(Icons.getInstance().getIconValidate16());
		btnValidate.setToolTipText(getSpringLocaleDelegate().getMessage(
				"DatasourceCollectController.19","Syntax der SQL Abfrage pr\u00fcfen"));
		btnValidate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				cmdValidateSql();
			}
		});
		//toolbarCustomDetails.add(btnValidate);
		this.getDetailsPanel().addToolBarComponent(btnValidate);

		//toolbarCustomDetails.addSeparator();

		for(JButton jb : getAdditionalToolbarButtons()){
			//toolbarCustomDetails.add(jb);
			this.getDetailsPanel().addToolBarComponent(jb);
		}

		//this.getDetailsPanel().setCustomToolBarArea(toolbarCustomDetails);
		
		//@see NUCLOS-1679
//		getResultPanel().btnPointer.setIcon(Icons.getInstance().getIconAbout16());
//		getResultPanel().btnPointer.setEnabled(false);
	}

	protected List<JButton> getAdditionalToolbarButtons() {
		return Collections.<JButton>emptyList();
	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Deprecated
	@Override
	public void addAdditionalChangeListenersForDetails() {
		pnlEdit.getQueryEditor().addChangeListener(this.changelistenerDetailsChanged);
	}

	/**
	 * @deprecated Move to DetailsController and make protected again.
	 */
	@Deprecated
	@Override
	public void removeAdditionalChangeListenersForDetails() {
		if (pnlEdit != null) {
			final QueryBuilderEditor qbe = pnlEdit.getQueryEditor();
			if (qbe != null) {
				qbe.removeChangeListener(this.changelistenerDetailsChanged);
			}
		}
	}

	protected static String getUsagesAsString(List<DatasourceVO> lstUsages) {
		/** @todo Use	CollectionUtils.getSeparatedList() */

		final StringBuffer sb = new StringBuffer();
		for (DatasourceVO datasourcevo : lstUsages) {
			sb.append(datasourcevo.getName());
			sb.append(", ");
		}
		sb.deleteCharAt(sb.length() - 2);

		return sb.toString();
	}

	@Override
	protected CollectableDataSource<T> findCollectableByIdWithoutDependants(UID sEntity, UID oId) throws CommonBusinessException {
		return findCollectableById(sEntity, oId);
	}

	/**
	 * import a datasource from XML
	 */
	protected void cmdImport() {
		final String sCurrentDirectory = this.getPreferences().get(PREFS_KEY_LASTIMPORTEXPORTPATH, null);
		final JFileChooser filechooser = new JFileChooser(sCurrentDirectory);
		filechooser.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return file.isDirectory() || file.getName().toLowerCase().endsWith("xml");
			}

			@Override
			public String getDescription() {
				return "*.xml";
			}
		});
		final int iBtn = filechooser.showOpenDialog(this.getTab());
		if (iBtn == JFileChooser.APPROVE_OPTION) {
			final File file = filechooser.getSelectedFile();
			if (file != null) {
				this.getPreferences().put(PREFS_KEY_LASTIMPORTEXPORTPATH, file.getParent());
				UIUtils.runCommand(this.getTab(), new Runnable() {
					@Override
					public void run() {
						try {
							importXML(IOUtils.readFromTextFile(file, "UTF-8"));
						}
						catch (FileNotFoundException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						}
						catch (IOException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						}
						catch (NuclosBusinessException e) {
							Errors.getInstance().showExceptionDialog(getTab(), e);
						}
					}
				});
			}
		}
	}

	protected abstract void importXML(String sXML) throws NuclosBusinessException;

	/**
	 * export datasource to XML
	 */
	protected void cmdExport() {
		final String sCurrentDirectory = this.getPreferences().get(PREFS_KEY_LASTIMPORTEXPORTPATH, null);
		JFileChooser filechooser;
		try {
			filechooser = new JFileChooser(sCurrentDirectory);
		}
		catch (Exception e) {
			filechooser = new JFileChooser();
		}
		filechooser.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().toLowerCase().endsWith("xml");
			}

			@Override
			public String getDescription() {
				return "*.xml";
			}
		});
		final int iBtn = filechooser.showSaveDialog(this.getTab());
		if (iBtn == JFileChooser.APPROVE_OPTION) {
			final File file = filechooser.getSelectedFile();
			if (file != null) {
				this.getPreferences().put(PREFS_KEY_LASTIMPORTEXPORTPATH, file.getParent());
				String sPathName = filechooser.getSelectedFile().getAbsolutePath();
				if (!sPathName.toLowerCase().endsWith(".xml")) {
					sPathName += ".xml";
				}
				final File xmlFile = new File(sPathName);
				UIUtils.runCommand(this.getTab(), new Runnable() {
					@Override
					public void run() {
						try {
							exportXML(xmlFile);
						}
						catch (IOException ex) {
							Errors.getInstance().showExceptionDialog(getTab(), ex);
						}
						catch (CommonBusinessException ex) {
							Errors.getInstance().showExceptionDialog(getTab(), ex);
						}
					}
				});
			}
		}
	}

	protected abstract void exportXML(File file) throws IOException, CommonBusinessException ;

	protected void cmdValidateSql() {
		UIUtils.runCommand(this.getTab(), new Runnable() {
			@Override
			public void run() {
				validateSQL();
			}
		});
	}


	protected abstract void validateSQL();

	protected List<ColumnEntry> getDefaultColumns() {
		return Collections.emptyList();
	}

	protected List<DatasourceParameterVO> getDefaultParameters() {
		return Collections.emptyList();
	}

	/**
	 * @param clct
	 * @throws NuclosBusinessException
	 */
	@Override
	protected void unsafeFillDetailsPanel(CollectableDataSource<T> clct) throws CommonBusinessException {
		// fill the textfields:
		super.unsafeFillDetailsPanel(clct);

		final DatasourceVO datasourceVO = clct.getDatasourceVO();

		if (datasourceVO.getId() == null) {
			pnlEdit.getQueryEditor().newDatasource(getDefaultColumns());
			pnlEdit.getQueryEditor().setParameter(getDefaultParameters());
		}
		else {
			if (datasourceVO.getSource() != null) {
				final Map<String, List<String>> mpWarnings = pnlEdit.getQueryEditor().setXML(datasourceVO.getSource());
				final String sWarnings = QueryBuilderEditor.getSkippedElements(mpWarnings);
				if (sWarnings.length() > 0) {
					JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
							"DatasourceCollectController.13","Folgende Elemente existieren nicht mehr in dem aktuellen Datenbankschema und wurden daher entfernt") + ":\n" + sWarnings);
				}
				final List<DatasourceParameterVO> lstParams = datasourcedelegate.getParametersFromXML(datasourceVO.getSource());
				pnlEdit.getQueryEditor().setParameter(lstParams);

				pnlEdit.setDatasourceName(datasourceVO.getName());
				pnlEdit.setDatasourceDescription(datasourceVO.getDescription());

				final Set<String> stColumnParameters = new HashSet<String>(DatasourceUtils.getParametersFromString(datasourceVO.getSource()));
				final Set<String> stDefinedParameters = new HashSet<String>();

				for (DatasourceParameterVO paramvo : lstParams) {
					final String sParameter = paramvo.getParameter();
					stDefinedParameters.add(sParameter);
					if (paramvo.getValueListProvider() != null && paramvo.getValueListProvider().getType() != null) {
						stDefinedParameters.add(sParameter + "Id");
					}

					if (pnlEdit.isModelUsed() && !(stColumnParameters.contains(sParameter) || stColumnParameters.contains(sParameter + "Id")) ) {
						if (datasourceVO instanceof RecordGrantVO &&
							QueryBuilderConstants.RECORDGRANT_SYSTEMPARAMETER_NAMES.contains(sParameter)) {
							continue;
						}

						JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
								"DatasourceCollectController.5","Der Parameter \"{0}\" ist definiert, wird aber nicht verwendet.", sParameter));
					}
				}
				for (String sParameter : stColumnParameters) {
					// username should always be available
					if (sParameter.equals("username")) {
						continue;
					}
					if (pnlEdit.isModelUsed() && !stDefinedParameters.contains(sParameter)) {
						JOptionPane.showMessageDialog(getTab(), getSpringLocaleDelegate().getMessage(
								"DatasourceCollectController.6","Der Parameter \"{0}\" ist nicht definiert.", sParameter));
					}
				}
			}
		}
		UIUtils.invokeOnDispatchThread(new Runnable() {
			@Override
			public void run() {
				pnlEdit.refreshView();
			}
		});
	}


	/** ask the user for parameter values
	 * @param sDatasourceXML
	 * @param mpParams
	 * @return
	 * @throws CommonBusinessException
	 */
	public static boolean createParamMap(String sDatasourceXML, Map<String, Object> mpParams, Component parent) throws CommonBusinessException {
		final List<DatasourceParameterVO> lstParams = DatasourceDelegate.getInstance().getParametersFromXML(sDatasourceXML);

		return createParamMap(lstParams, mpParams, parent);
	}

	public static boolean createParamMap(List<DatasourceParameterVO> lstParams, Map<String, Object> mpParams, Component parent) throws CommonBusinessException {
		return createParamMap(null, lstParams, mpParams, parent, null);
	}
	public static boolean createParamMap(String sReportName, List<DatasourceParameterVO> lstParams, Map<String, Object> mpParams, Component parent, Long vlpParamObjectId) throws CommonBusinessException {
		boolean result = true;
		final ParameterPanel panel = new ParameterPanel(lstParams, vlpParamObjectId);

		if (lstParams.iterator().hasNext()) {
			result = (panel.showOptionDialog(parent, panel, sReportName != null ? sReportName : 
					SpringLocaleDelegate.getInstance().getMessage("DatasourceCollectController.16","Parameter"), 
					JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.PLAIN_MESSAGE, null, null, null) == JOptionPane.OK_OPTION);
			if (result) {
				panel.fillParameterMap(lstParams, mpParams);
			}
		}
		return result;
	}
	
	public void execute(final CommonClientWorker worker) {
		CommonMultiThreader.getInstance().execute(new CommonClientWorkerAdapter<UID,CollectableDataSource<T>>(this) {

			@Override
			public void init() throws CommonBusinessException {
				super.init();
				worker.init();
			}

			@Override
			public void work() throws CommonBusinessException {
				worker.work();
			}

			@Override
			public void paint() throws CommonBusinessException {
				worker.paint();
				super.paint();
			}
		});
	}

	protected class DatasourcesCollectStateListener extends CollectStateAdapter {
		@Override
		public void detailsModeEntered(CollectStateEvent ev) throws NuclosBusinessException {
			final int iDetailsMode = ev.getNewCollectState().getInnerState();
			if (iDetailsMode == CollectState.DETAILSMODE_NEW) {
				pnlEdit.setIsModelUsed(true);
				pnlEdit.refreshView();
			}
			AbstractDatasourceCollectController<?> dscc = AbstractDatasourceCollectController.this;
			final boolean bWriteAllowed = SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid);
			final boolean bSuperUser = Boolean.TRUE.equals(SecurityCache.getInstance().isSuperUser());

			final QueryBuilderEditor editor = dscc.pnlEdit.getQueryEditor();
			if (iDetailsMode == CollectState.DETAILSMODE_EDIT || iDetailsMode == CollectState.DETAILSMODE_MULTIEDIT) {
				editor.getTableSelectionPanel().getParameterPanel().getDeleteParameterAction().setEnabled(bWriteAllowed);
			}
			dscc.btnImport.setEnabled(bWriteAllowed);
			dscc.btnValidate.setEnabled(bWriteAllowed);
			final DatasourceHeaderPanel header = dscc.pnlEdit.getHeader();
			header.getDescriptionField().setEnabled(bWriteAllowed);
			header.getNameField().setEnabled(bWriteAllowed);
			if (header.getDetailsearchdescriptionDeField() != null) {
				header.getDetailsearchdescriptionDeField().setEnabled(bWriteAllowed);
			}
			if (header.getDetailsearchdescriptionEnField() != null) {
				header.getDetailsearchdescriptionEnField().setEnabled(bWriteAllowed);
			}
			if (header.getEntityComboBox() != null) {
				header.getEntityComboBox().setEnabled(bWriteAllowed);
			}
			if (header.getParentEntityComboBox() != null) {
				header.getParentEntityComboBox().setEnabled(bWriteAllowed);
			}
			if (header.getWithRuleClassCheckBox() != null) {
				header.getWithRuleClassCheckBox().setEnabled(bWriteAllowed && bSuperUser);
			}

			editor.getTableSelectionPanel().getParameterPanel().getNewParameterAction().setEnabled(bWriteAllowed);
			editor.getTableSelectionPanel().getParameterPanel().getParameterTable().setEnabled(bWriteAllowed);
			editor.getColumnSelectionPanel().getTable().setEnabled(bWriteAllowed);
			dscc.pnlEdit.sqlPanel.getBtnGenerateSql().setEnabled(bWriteAllowed);
		}
	}
	
	private static class DatasourceResultPanel<T extends DatasourceVO> extends NuclosResultPanel<UID,CollectableDataSource<T>> {
		
		private DatasourceResultPanel(UID entityUid) {
			super(entityUid, ControllerPresentation.DEFAULT);
		}
		
		private SpringLocaleDelegate getSpringLocaleDelegate() {
			return SpringLocaleDelegate.getInstance();
		}

		@Override
		protected void postXMLImport(final CollectController<UID,CollectableDataSource<T>> clctctl) {
			// initialize attribute cache on server side
			try {
				ClientConsole.getInstance().parseAndInvoke(new String[]{ConsoleCommand.INVALIDATE_ALL_CACHES.getCommand()});
			}
			catch(Exception e) {
				throw new NuclosFatalException(getSpringLocaleDelegate().getMessage(
						"DatasourceCollectController.7","Der serverseitige DatasourceCache konnte nicht invalidiert werden!"), e);
			}
			super.postXMLImport(clctctl);
		}
	}

	private static class DatasourceCollectPanel<T extends DatasourceVO> extends CollectPanel<UID,CollectableDataSource<T>> {

		DatasourceCollectPanel(UID entityId, boolean bSearchPanelAvailable, boolean bShowSearch) {
			super(entityId, bSearchPanelAvailable, bShowSearch, LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Details_Presentation, entityId), ControllerPresentation.DEFAULT);
		}

		private SpringLocaleDelegate getSpringLocaleDelegate() {
			return SpringLocaleDelegate.getInstance();
		}

		@Override
		public ResultPanel<UID,CollectableDataSource<T>> newResultPanel(UID entityId) {
			return new DatasourceResultPanel<T>(entityId);
		}

		@Override
		public DetailsPanel newDetailsPanel(UID entityId) {
			return new DetailsPanel(entityId, getDetailsPresentation(), ControllerPresentation.DEFAULT, false);
		}

	}
	
	@Override
	public void init() {
		super.init();
		
		this.setupResultToolBar();
		this.setupDetailsToolBar();
		
		btnImport.setEnabled(true);
		btnExport.setEnabled(true);
		btnValidate.setEnabled(true);
		
		ToolBarConfiguration configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Detail, getEntityUid());
		this.getDetailsPanel().setToolBarConfiguration(configuration);
		configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Result, getEntityUid());
		this.getResultPanel().setToolBarConfiguration(configuration);
		configuration = 
				LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Search, getEntityUid());
		this.getSearchPanel().setToolBarConfiguration(configuration);
	}
	
	@Override
	protected void initialize(CollectPanel<UID, CollectableDataSource<T>> pnlCollect) {
		super.initialize(pnlCollect);
		revalidateAdditionalSearchField();
	}
	
	protected void setupResultToolBar() {
		super.setupResultToolBar();
		final ResultPanel rp = getResultPanel();
		rp.addStatusBarComponent(btnExportResults);

		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_EXPORT_SEARCHRESULT)) {
			btnExportResults.setEnabled(true);
			// action: Print results
			btnExportResults.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					cmdExportResult();
				}
			});
		}
		else {
			btnExportResults.setEnabled(false);
			btnExportResults.setToolTipText(getSpringLocaleDelegate().getMessage("MasterDataCollectController.8",
					"Liste exportieren - Sie verf\u00fcgen nicht \u00fcber ausreichende Rechte."));
		}
	}
	
	public ResultVO cmdExecuteCurrentStatement(Integer iMaxRowCount) {
		ResultVO result = null;
		LayerLock layerLock = null;
		try {
			layerLock = getTab().lockLayer();
			layerLock.setLocked(true);
			final String sDatasourceXML = pnlEdit.getQueryEditor().getXML(new DatasourceEntityOptions(true));
			final T datasourcevo = newCollectable().getDatasourceVO();
			datasourcevo.setSource(sDatasourceXML);
			final Map<String, Object> mpParams = CollectionUtils.newHashMap();

			UID language = DataLanguageContext.getDataUserLanguage() != null ? 
					DataLanguageContext.getDataUserLanguage() : DataLanguageContext.getDataSystemLanguage();
					
			if (createParamMap(sDatasourceXML, mpParams, getTab())) {
				result = datasourcedelegate.executeQueryForDataSource(datasourcevo, mpParams, getEntityUid(), iMaxRowCount, language);
			}
			getTab().unlockLayer(layerLock);
		}
		catch (Exception ex) {
			if (layerLock != null) {
				getTab().unlockLayer(layerLock);
			}
			Errors.getInstance().showExceptionDialog(getTab(), ex);
		}
		return result;
	}
	
	protected void cmdExportResult() {
		assert getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_RESULT;
		final List<CollectableDataSource<T>> lstclctlo = getSelectedCollectables();
		final UsageCriteria usagecriteria = new UsageCriteria(entityUid, null, null, "");
		UIUtils.runCommand(getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final ISearchStrategy<UID, CollectableDataSource<T>> ss = getSearchStrategy();
				final CollectableSearchExpression clctexprInternal = ss.getSearchExpressionForExport();
				// restrict to readable data sources 
				if (Boolean.FALSE.equals(SecurityCache.getInstance().isSuperUser())) {
					List<UID> readableDatasourceUIDs = new ArrayList<>();
					for (DatasourceVO datasource : datasourcedelegate.getAllDatasources()) {
						readableDatasourceUIDs.add(datasource.getId());
					}
					clctexprInternal.setSearchCondition(new CollectableInIdCondition<UID>(new CollectableEOEntityField(E.DATASOURCE.getPk().getMetaData(E.DATASOURCE)), new ArrayList<UID>(readableDatasourceUIDs)));
				}
				new ReportController(getTab()).export(getCollectableEntity(), clctexprInternal, getFields().getSelectedFields(), getSelectedFieldsWidth(),
					!lstclctlo.isEmpty(), usagecriteria, new Runnable() {
						@SuppressWarnings({ "rawtypes", "unchecked" })
						@Override
						public void run() {
							final PrintController printController = new PrintController(getTab());
							printController.show(new UsageCriteria(entityUid, null, null, ""), 
									lstclctlo, null, null, 
									new PrintControllerEventListener() {
										@Override
										public void execute(final PrintControllerEventType event, final Object value) {
											if (PrintControllerEventType.SUBMIT_ERROR == event) {
												Collection col = (Collection)value;
												if (col != null && col.size() >= 1) {
													PrintResultTO printResult = (PrintResultTO)col.iterator().next();
													Exception ex = printResult.getException();
													String msg = ex.getMessage();
													if (ex instanceof CommonPrintException) {
														Errors.getInstance().showExceptionDialog(getParent(), new NuclosBusinessException(msg));
													}
												}
											}
										}
									});
						}
					}, null, null, null);
			}
		});
		
	}
	
	public List<Integer> getSelectedFieldsWidth() {
		List<Integer> lstWidth = new ArrayList<Integer>();
		JTable tblResult = getResultTable();
		final TableColumnModel columnmodel = tblResult.getTableHeader().getColumnModel();
		for (int iColumn = 0; iColumn < columnmodel.getColumnCount(); iColumn++) {
			TableColumn tableColumn = columnmodel.getColumn(iColumn);
			int width = tableColumn.getPreferredWidth();
			lstWidth.add(width);
		}
		return lstWidth;
	}
}
