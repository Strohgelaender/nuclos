//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.datasource.admin;

import static org.nuclos.client.datasource.querybuilder.QueryBuilderConstants.PARAMETER_TYPE_INTERNAL_INTEGER_LIST;
import static org.nuclos.client.datasource.querybuilder.QueryBuilderConstants.PARAMETER_TYPE_INTERNAL_SQL;
import static org.nuclos.client.datasource.querybuilder.QueryBuilderConstants.PARAMETER_TYPE_INTERNAL_STRING_LIST;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.genericobject.valuelistprovider.GenericObjectCollectableFieldsProviderFactory;
import org.nuclos.client.masterdata.valuelistprovider.DefaultValueProvider;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.ui.DateChooser;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.valuelistprovider.VLPClientUtils;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;

/**
 * Panel for administrating data source parameters. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class ParameterPanel extends JPanel {

	
	private static final Logger LOG = Logger.getLogger(ParameterPanel.class);
	
	private static final String PREFS_NODE_DATASOURCE_PARAMETER_VALUES = "datasource.parameter.values";
			
	private final Preferences prefs = ClientPreferences.getInstance().getUserPreferences().node(PREFS_NODE_DATASOURCE_PARAMETER_VALUES);

	private static final int iComponentWidth = 200;

	private final Map<String, JComponent> mpFields = CollectionUtils
			.newHashMap();

	private final List<DatasourceParameterVO> lstParams;
	private final Long vlpParamObjectId;

	private final ChangeListener changeListener;
	private ChangeListener changeListenerInternal;

	/**
	 * 
	 * @param lstParams
	 * @param vlpParamObjectId 
	 */
	public ParameterPanel(List<DatasourceParameterVO> lstParams) {
		this(lstParams, null);
	}
	
	/**
	 * 
	 * @param lstParams
	 * @param vlpParamObjectId 
	 */
	public ParameterPanel(List<DatasourceParameterVO> lstParams, Long vlpParamObjectId) {
		this(lstParams, vlpParamObjectId, null);
	}

	/**
	 * 
	 * @param lstParams
	 * @param chgListener
	 */
	public ParameterPanel(List<DatasourceParameterVO> lstParams, Long vlpParamObjectId, 
			ChangeListener chgListener) {
		this(lstParams, vlpParamObjectId, chgListener, null);
	}
	
	public ParameterPanel(List<DatasourceParameterVO> lstParams, Long vlpParamObjectId,
			ChangeListener chgListener, Map<String, Object> initialValues) {
		super(new GridBagLayout());

		this.lstParams = lstParams;
		this.vlpParamObjectId = vlpParamObjectId;
		this.changeListener = chgListener;

		// NUCLEUSINT-182/NUCLEUSiNT-577
		Collections.sort(lstParams, new Comparator<DatasourceParameterVO>() {
			@Override
			public int compare(DatasourceParameterVO o1,
					DatasourceParameterVO o2) {
				if (o1 != null && o2 != null)
					return StringUtils.emptyIfNull(o1.getParameter())
							.compareTo(
									StringUtils.emptyIfNull(o2.getParameter()));
				return 0;
			}
		});

		int iRow = 0;
		for (DatasourceParameterVO paramvo : lstParams) {

			// internal datatypes
			if (PARAMETER_TYPE_INTERNAL_STRING_LIST.equals(paramvo.getDatatype()) ||
				PARAMETER_TYPE_INTERNAL_INTEGER_LIST.equals(paramvo.getDatatype()) ||
				PARAMETER_TYPE_INTERNAL_SQL.equals(paramvo.getDatatype())) {
				continue;
			}

			final Object init;
			if (initialValues != null) {
				init = initialValues.get(paramvo.getParameter());
			} else {
				init = null;
			}

			final JLabel lab = new JLabel(
					paramvo.getDescription() == null
							|| paramvo.getDescription().equals("") ? paramvo
							.getParameter() : paramvo.getDescription());
			add(lab, new GridBagConstraints(0, iRow, 1, 1, 0, 0,
					GridBagConstraints.WEST, GridBagConstraints.NONE,
					new Insets(4, 4, 4, 4), 0, 0));

			final JComponent comp;
			final JTextComponent compText;
			
			final String prefsKey = getPrefsKey(paramvo);
			final boolean prefsEnabled = prefsKey != null;
			final String prefsValue;
			if (prefsEnabled) {
				String tmp = prefs.get(prefsKey, "");
				prefsValue = "".equals(tmp) ? null : tmp;
			} else {
				prefsValue = null;
			}

			CollectableFieldsProvider vlp = null;
			
			if (paramvo.getValueListProvider() != null) {
				List<CollectableField> lst;
				try {
					if (paramvo.getValueListProvider().getType() == null
							&& paramvo.getValueListProvider().getParameters().containsKey("valuelistProvider")) {
							// old style. should be done by migration.
							final DatasourceVO dsvo = DatasourceDelegate.getInstance().getValuelistProviderByName(paramvo.getValueListProvider().getParameters().get("valuelistProvider"));
							paramvo.getValueListProvider().setName(dsvo.getName());
							paramvo.getValueListProvider().setType(ValuelistProviderVO.Type.DS);
							paramvo.getValueListProvider().setValue(dsvo.getPrimaryKey().getStringifiedDefinitionWithEntity(E.VALUELISTPROVIDER));
						}
	
					final String value = paramvo.getValueListProvider().getValue();
					
					try {
						vlp = GenericObjectCollectableFieldsProviderFactory.newFactory(null).newCustomCollectableFieldsProvider(value, null);
					} catch (Exception ex) {
						vlp = MasterDataCollectableFieldsProviderFactory.newFactory(null).newCustomCollectableFieldsProvider(value, null);
					}
	
					for (Map.Entry<String, String> param : paramvo
							.getValueListProvider().getParameters().entrySet()) {
						vlp.setParameter(param.getKey(), param.getValue());
					}
					if (VLPClientUtils.vlpRequiresParameter(vlp, VLPClientUtils.PARAM_INTID)) {
						vlp.setParameter(VLPClientUtils.PARAM_INTID, vlpParamObjectId);
					}
					lst = vlp.getCollectableFields();
				} catch (CommonBusinessException e) {
					Errors.getInstance().showExceptionDialog(this, e);
					lst = new ArrayList<CollectableField>();
				}
				lst.add(null);

				Collections.sort(lst, new Comparator<CollectableField>() {
					@Override
					public int compare(CollectableField clctf1,
							CollectableField clctf2) {
						return LangUtils.compareComparables(clctf1, clctf2);
					}
				});

				final DefaultComboBoxModel model = new DefaultComboBoxModel(
						new Vector<CollectableField>(lst));
				final JComboBox cmbbx = new JComboBox(model);
				boolean bInit = false;
				if (lst.size() > 0) {
					if (vlp instanceof DefaultValueProvider) {
						try {
							final CollectableField cf = ((DefaultValueProvider) vlp).getDefaultValue();
							cmbbx.setSelectedItem(cf);	
							bInit = cf == null;
						} catch (Exception e) {
							bInit = true;
							LOG.warn("could not set default value of vlp.", e);
						}
					} else {
						cmbbx.setSelectedIndex(0);
						bInit = true;
					}
				}
				if (bInit) { 
					if (init != null) {
						for (CollectableField cf : lst) {
							if (cf != null && cf.getValueId() instanceof Number && init instanceof Number) {
								if (((Number)cf.getValueId()).longValue() == ((Number)init).longValue()) {
									cmbbx.setSelectedItem(cf);
									break;
								}
							}
						}
					} else {
						int sel = 0;
						if (prefsValue != null) {
							for (int i = 0; i < lst.size(); i++) {
								CollectableField item = lst.get(i);
								if (item != null && prefsValue.equals(item.getValue())) {
									sel = i;
									break;
								}
							}
						}
						cmbbx.setSelectedIndex(sel);
					}
				}
				cmbbx.setName(paramvo.getParameter());
				comp = cmbbx;
				compText = (JTextComponent)cmbbx.getEditor().getEditorComponent();
			} else {
				if (paramvo.getDatatype() == null) {
					comp = null;
					compText = null;
				} else if (paramvo.getDatatype().equals("java.lang.String")) {
					final JTextField tf = new JTextField();
					if (init instanceof String) {
						tf.setText((String) init);
					} else {
						if (prefsValue != null) {
							tf.setText(prefsValue);
						}
					}
					tf.setName(paramvo.getParameter());
					tf.getDocument().addDocumentListener(new DocumentListener() {
						@Override
						public void changedUpdate(DocumentEvent e) {
							if (changeListenerInternal != null)
								changeListenerInternal.stateChanged(new ChangeEvent(
													ParameterPanel.this));
						}
						@Override
						public void insertUpdate(DocumentEvent e) {
							changedUpdate(e);
						}
						@Override
						public void removeUpdate(DocumentEvent e) {
							changedUpdate(e);							
						}
					});
					comp = tf;
					compText = tf;
					
				} else if (paramvo.getDatatype().equals("java.lang.Boolean")) {
					final Boolean[] abValues = { Boolean.TRUE, Boolean.FALSE };
					final JComboBox cmbbx = new JComboBox(abValues);
					if (init instanceof Boolean) {
						cmbbx.setSelectedIndex(((Boolean)init) ? 0 : 1);
					} else if (init instanceof Integer) {
						cmbbx.setSelectedIndex(((Integer) init).intValue()==1 ? 0 : 1);
					} else {
						if (prefsValue != null) {
							boolean b = Boolean.TRUE.toString().equals(prefsValue);
							cmbbx.setSelectedIndex(b ? 0 : 1);
						} else {
							cmbbx.setSelectedIndex(0);
						}
					}
					cmbbx.setName(paramvo.getParameter());
					if (cmbbx.getEditor() != null && cmbbx.getEditor().getEditorComponent() instanceof JTextField) {
						((JTextField)cmbbx.getEditor().getEditorComponent()).getDocument().addDocumentListener(new DocumentListener() {
							@Override
							public void changedUpdate(DocumentEvent e) {
								if (changeListenerInternal != null)
									changeListenerInternal.stateChanged(new ChangeEvent(
														ParameterPanel.this));
							}
							@Override
							public void insertUpdate(DocumentEvent e) {
								changedUpdate(e);
							}
							@Override
							public void removeUpdate(DocumentEvent e) {
								changedUpdate(e);							
							}
						});
						}
					comp = cmbbx;
					compText = (JTextComponent)cmbbx.getEditor().getEditorComponent();
					
				} else if (paramvo.getDatatype().equals("java.lang.Double")) {
					//final JTextField tf = new JFormattedTextField(
						//	new NumberFormatter(
							//		NumberFormat.getNumberInstance()));
					final JTextField tf = new JTextField();
					if (init instanceof Double) {
						tf.setText(((Double)init).toString());
					} else {
						if (prefsValue != null) {
							tf.setText(prefsValue);
						}
					}
					tf.setName(paramvo.getParameter());
					tf.getDocument().addDocumentListener(new DocumentListener() {
						@Override
						public void changedUpdate(DocumentEvent e) {
							if (changeListenerInternal != null)
								changeListenerInternal.stateChanged(new ChangeEvent(
													ParameterPanel.this));
						}
						@Override
						public void insertUpdate(DocumentEvent e) {
							changedUpdate(e);
						}
						@Override
						public void removeUpdate(DocumentEvent e) {
							changedUpdate(e);							
						}
					});
					comp = tf;
					compText = tf;
					
				} else if (paramvo.getDatatype().equals("java.lang.Integer")) {
					//final JTextField tf = new JFormattedTextField(
						//	new NumberFormatter(
							//		NumberFormat.getIntegerInstance()));
					final JTextField tf = new JTextField();
					if (init instanceof Integer) {
						tf.setText(((Integer)init).toString());
					} else {
						if (prefsValue != null) {
							tf.setText(prefsValue);
						}
					}
					tf.setName(paramvo.getParameter());
					tf.getDocument().addDocumentListener(new DocumentListener() {
						@Override
						public void changedUpdate(DocumentEvent e) {
							if (changeListenerInternal != null)
								changeListenerInternal.stateChanged(new ChangeEvent(
													ParameterPanel.this));
						}
						@Override
						public void insertUpdate(DocumentEvent e) {
							changedUpdate(e);
						}
						@Override
						public void removeUpdate(DocumentEvent e) {
							changedUpdate(e);							
						}
					});
					comp = tf;
					compText = tf;
					
				} else if (paramvo.getDatatype().equals("java.util.Date")) {
					final LabeledComponentSupport support = new LabeledComponentSupport();
					final DateChooser datechooser = new DateChooser(support);
					if (init instanceof Date) {
						datechooser.setDate((Date) init);
					} else {
						if (prefsValue != null) {
							try {
								Date date = new SimpleDateFormat("yyyy-MM-dd").parse(prefsValue); 
								datechooser.setDate(date);
							} catch (Exception ex) {
								LOG.error(ex.getMessage(), ex);
							}
						}
					}
					datechooser.setName(paramvo.getParameter());
					datechooser.getJTextField().getDocument().addDocumentListener(new DocumentListener() {
						@Override
						public void changedUpdate(DocumentEvent e) {
							if (changeListenerInternal != null)
								changeListenerInternal.stateChanged(new ChangeEvent(
													ParameterPanel.this));
						}
						@Override
						public void insertUpdate(DocumentEvent e) {
							changedUpdate(e);
						}
						@Override
						public void removeUpdate(DocumentEvent e) {
							changedUpdate(e);							
						}
					});
					comp = datechooser;
					compText = datechooser.getJTextField();
					
				} else {
					comp = null;
					compText = null;
				}
			}

			if (comp != null) {
				if (paramvo.getMandatory())
					comp.setBackground(ClientParameterProvider
							.getInstance()
							.getColorValue(
									ParameterProvider.KEY_MANDATORY_ITEM_BACKGROUND_COLOR,
									new Color(255, 255, 200)));
			}
			if (compText != null) {
				compText.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent e) {
						if (changeListener != null)
							changeListener.stateChanged(new ChangeEvent(
									ParameterPanel.this));
						if (changeListenerInternal != null)
							changeListenerInternal.stateChanged(new ChangeEvent(
									ParameterPanel.this));
					}
				});
				compText.addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						if (e.getKeyChar() == '\n' && changeListener != null)
							changeListener.stateChanged(new ChangeEvent(
									ParameterPanel.this));
						if (e.getKeyChar() == '\n' && changeListenerInternal != null)
							changeListenerInternal.stateChanged(new ChangeEvent(
												ParameterPanel.this));
					}
				});
			}
			if (comp != null) {
				if (comp instanceof JComboBox) {
					((JComboBox) comp).addItemListener(new ItemListener() {
						@Override
						public void itemStateChanged(ItemEvent e) {
							if (changeListener != null)
								changeListener.stateChanged(new ChangeEvent(
										ParameterPanel.this));
							if (changeListenerInternal != null)
								changeListenerInternal.stateChanged(new ChangeEvent(
										ParameterPanel.this));
						}
					});
				}
				comp.setPreferredSize(new Dimension(iComponentWidth, comp
						.getPreferredSize().height));
			}
			if (comp != null) {
				add(comp, new GridBagConstraints(1, iRow, 1, 1, 1.0, 0,
						GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
						new Insets(4, 4, 4, 4), 0, 0));
				if (iRow == 0) {
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							try {
								comp.requestFocusInWindow();
							} catch (Exception e) {
								LOG.error("ParameterPanel failed: " + e, e);
							}
						}
					});
				}
				mpFields.put(paramvo.getParameter(), comp);
				iRow++;
			}
		}

		if (changeListener != null)
			changeListener.stateChanged(new ChangeEvent(
					ParameterPanel.this));

		if (changeListenerInternal != null)
			changeListenerInternal.stateChanged(new ChangeEvent(
								ParameterPanel.this));
	}

	public static int showOptionDialog(
			Component parentComponent, ParameterPanel parameterPanel, String title,
			int optionType, int messageType, Icon icon, Object[] options,
			Object initialValue) throws HeadlessException {
		JOptionPane pane = new JOptionPane(parameterPanel, messageType, optionType,
				icon, options, initialValue);

		pane.setInitialValue(initialValue);
		pane.setComponentOrientation(((parentComponent == null) ? JOptionPane
				.getRootFrame() : parentComponent).getComponentOrientation());

		final int style;
		switch (messageType) {
		case JOptionPane.ERROR_MESSAGE:
			style = JRootPane.ERROR_DIALOG;
			break;
		case JOptionPane.QUESTION_MESSAGE:
			style = JRootPane.QUESTION_DIALOG;
			break;
		case JOptionPane.WARNING_MESSAGE:
			style = JRootPane.WARNING_DIALOG;
			break;
		case JOptionPane.INFORMATION_MESSAGE:
			style = JRootPane.INFORMATION_DIALOG;
			break;
		case JOptionPane.PLAIN_MESSAGE:
		default:
			style = JRootPane.PLAIN_DIALOG;
		}

		JDialog dialog;
		Window window = getWindowForComponent(parentComponent);
		if (window instanceof Frame) {
			dialog = new JDialog((Frame) window, title, true);
		} else {
			dialog = new JDialog((Dialog) window, title, true);
		}

		/*
		 * if (window instanceof SwingUtilities.SharedOwnerFrame) {
		 * WindowListener ownerShutdownListener = (WindowListener)
		 * SwingUtilities .getSharedOwnerFrameShutdownListener();
		 * dialog.addWindowListener(ownerShutdownListener); }
		 */
		initDialog(pane, dialog, style, parentComponent);

		pane.selectInitialValue();

		JButton btnOK = null;
		Collection<JButton> colButton = UIUtils.findAllInstancesOf(dialog, JButton.class);
		for (JButton btn : colButton) {
			if (btn.getText().equalsIgnoreCase("ok")) {
				btnOK = btn;
				break;
			}
		}
		
		btnOK.setEnabled(parameterPanel.lstParams.isEmpty());
		boolean bHasMandatory = false;
		for (DatasourceParameterVO paramvo : parameterPanel.lstParams) {
			if (paramvo.getMandatory()) {
				bHasMandatory = true;
				break;
			}
		}
		btnOK.setEnabled(!bHasMandatory);
		
		final JButton btnOKFinal = btnOK;
		parameterPanel.changeListenerInternal = new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (e != null && e.getSource() instanceof ParameterPanel) {
					if (btnOKFinal != null) {
						boolean bEnable = true; 
						for (DatasourceParameterVO paramvo : ((ParameterPanel)e.getSource()).lstParams) {
							if (paramvo.getMandatory()) {
								final JComponent comp = ((ParameterPanel)e.getSource()).getComponentForParameter(paramvo
										.getParameter());
								if (comp != null) {
									if (paramvo.getValueListProvider() != null) {
										CollectableField selectedCollectableField = (CollectableField) ((JComboBox) comp)
												.getSelectedItem();
										if (selectedCollectableField == null)
											bEnable = false;
										else {
											if (selectedCollectableField.getFieldType() == CollectableField.TYPE_VALUEIDFIELD) {
												if (selectedCollectableField.getValueId() == null)
													bEnable = false;
											} else {
												if (StringUtils.isNullOrEmpty((String)selectedCollectableField.getValue()))
													bEnable = false;
											}
										}
									} else {
										if (paramvo.getDatatype().equals("java.lang.String")) {
											String text = ((JTextField) comp).getText();
											if (text != null) {
												text = text.trim();
											}
											if (StringUtils.isNullOrEmpty(text))
												bEnable = false;
										} else if (paramvo.getDatatype()
												.equals("java.lang.Boolean")) {
											// Physically there are no boolean parameters in the
											// database
											// So convert them to numbers instead
											;// do nothing
										} else if (paramvo.getDatatype().equals("java.lang.Double")) {
											try {
												String text = ((JTextField) comp).getText();
												if (text != null) {
													text = text.trim();
												}
												Double value = LocaleDelegate.getInstance()
														.getNumberFormat()
														.parse(((JTextField) comp).getText())
														.doubleValue();
												if (StringUtils.isNullOrEmpty(text))
													bEnable = false;
											} catch (ParseException ex) {
												bEnable = false;
											}
										} else if (paramvo.getDatatype()
												.equals("java.lang.Integer")) {
											try {
												String text = ((JTextField) comp).getText();
												if (text != null) {
													text = text.trim();
												}
												Integer value = LocaleDelegate.getInstance()
														.getNumberFormat()
														.parse(((JTextField) comp).getText())
														.intValue();
												if (StringUtils.isNullOrEmpty(text))
													bEnable = false;
											} catch (ParseException ex) {
												bEnable = false;
											}
										} else if (paramvo.getDatatype().equals("java.util.Date")) {
											try {
												if (((DateChooser) comp).getDate() == null)
													bEnable = false;
											} catch (CommonValidationException ex) {
												bEnable = false;
											}
										}
									}
								}
							}
						}
						btnOKFinal.setEnabled(bEnable);
					}
				}
			}
		};
		
		// NUCLOS-4765:
		parameterPanel.changeListenerInternal.stateChanged(new ChangeEvent(parameterPanel));
		
		dialog.show();
		dialog.dispose();

		Object selectedValue = pane.getValue();

		if (selectedValue == null)
			return JOptionPane.CLOSED_OPTION;
		if (options == null) {
			if (selectedValue instanceof Integer)
				return ((Integer) selectedValue).intValue();
			return JOptionPane.CLOSED_OPTION;
		}
		for (int counter = 0, maxCounter = options.length; counter < maxCounter; counter++) {
			if (options[counter].equals(selectedValue))
				return counter;
		}
		return JOptionPane.CLOSED_OPTION;
	}

	private static void initDialog(final JOptionPane pane,
			final JDialog dialog, int style, Component parentComponent) {
		dialog.setComponentOrientation(pane.getComponentOrientation());
		Container contentPane = dialog.getContentPane();

		contentPane.setLayout(new BorderLayout());
		contentPane.add(pane, BorderLayout.CENTER);
		dialog.setResizable(false);
		if (JDialog.isDefaultLookAndFeelDecorated()) {
			boolean supportsWindowDecorations = UIManager.getLookAndFeel()
					.getSupportsWindowDecorations();
			if (supportsWindowDecorations) {
				dialog.setUndecorated(true);
				pane.getRootPane().setWindowDecorationStyle(style);
			}
		}
		dialog.pack();
		dialog.setLocationRelativeTo(parentComponent);
		WindowAdapter adapter = new WindowAdapter() {
			private boolean gotFocus = false;

			public void windowClosing(WindowEvent we) {
				pane.setValue(null);
			}

			public void windowGainedFocus(WindowEvent we) {
				// Once window gets focus, set initial focus
				if (!gotFocus) {
					pane.selectInitialValue();
					gotFocus = true;
				}
			}
		};
		dialog.addWindowListener(adapter);
		dialog.addWindowFocusListener(adapter);
		dialog.addComponentListener(new ComponentAdapter() {
			public void componentShown(ComponentEvent ce) {
				// reset value to ensure closing works properly
				pane.setValue(JOptionPane.UNINITIALIZED_VALUE);
			}
		});
		pane.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent event) {
				// Let the defaultCloseOperation handle the closing
				// if the user closed the window without selecting a button
				// (newValue = null in that case). Otherwise, close the dialog.
				if (dialog.isVisible()
						&& event.getSource() == pane
						&& (event.getPropertyName()
								.equals(JOptionPane.VALUE_PROPERTY))
						&& event.getNewValue() != null
						&& event.getNewValue() != JOptionPane.UNINITIALIZED_VALUE) {
					dialog.setVisible(false);
				}
			}
		});
	}

	static Window getWindowForComponent(Component parentComponent)
			throws HeadlessException {
		if (parentComponent == null)
			return JOptionPane.getRootFrame();
		if (parentComponent instanceof Frame
				|| parentComponent instanceof Dialog)
			return (Window) parentComponent;
		return getWindowForComponent(parentComponent.getParent());
	}

	/**
	 * 
	 * @param sParameter
	 * @return Jcomponent fitting for input of parameter
	 */
	public JComponent getComponentForParameter(String sParameter) {
		for (JComponent comp : mpFields.values()) {
			if (comp.getName().equals(sParameter)) {
				return comp;
			}
		}
		return null;
	}

	public void fillParameterMap(Collection<DatasourceParameterVO> lstParams,
			Map<String, Object> mpParams) throws CommonValidationException {
	
		for (DatasourceParameterVO paramvo : lstParams) {
			final JComponent comp = this.getComponentForParameter(paramvo
					.getParameter());
			if (comp != null) {
				final String prefsKey = getPrefsKey(paramvo);
				final boolean prefsEnabled = prefsKey != null;
				if (paramvo.getValueListProvider() != null) {
					final CollectableField selectedCollectableField = (CollectableField) ((JComboBox) comp)
							.getSelectedItem();
					final Object value = selectedCollectableField == null ? null : selectedCollectableField.getValue();
					mpParams.put(paramvo.getParameter(), value);
					if (selectedCollectableField != null && selectedCollectableField.getFieldType() == CollectableField.TYPE_VALUEIDFIELD) {
						mpParams.put(paramvo.getParameter() + "Id", selectedCollectableField.getValueId());
					}else
					{
						mpParams.put(paramvo.getParameter() + "Id", null);

					}
					if (prefsEnabled) {
						if (value == null) {
							prefs.remove(prefsKey);
						} else {
							prefs.put(prefsKey, value.toString());
						}
					}
				} else {
					if (paramvo.getDatatype().equals("java.lang.String")) {
						String text = ((JTextField) comp).getText();
						if (text != null) {
							text = text.trim();
						}
						mpParams.put(paramvo.getParameter(),
								"".equals(text) ? null : text);
						if (prefsEnabled) {
							if (text == null) {
								prefs.remove(prefsKey);
							} else {
								prefs.put(prefsKey, text);
							}
						}
					} else if (paramvo.getDatatype()
							.equals("java.lang.Boolean")) {
						// Physically there are no boolean parameters in the
						// database
						// So convert them to numbers instead
						boolean b = ((JComboBox) comp).getSelectedItem().equals(Boolean.TRUE);
						mpParams.put(paramvo.getParameter(), b ? 1 : 0);
						if (prefsEnabled) {
							prefs.put(prefsKey, Boolean.toString(b));
						}
					} else if (paramvo.getDatatype().equals("java.lang.Double")) {
						try {
							String text = ((JTextField) comp).getText();
							if (text != null) {
								text = text.trim();
							}
							Double value = StringUtils.isNullOrEmpty(text) ? null :
								LocaleDelegate.getInstance().getNumberFormat().parse(((JTextField) comp).getText()).doubleValue();
							mpParams.put(paramvo.getParameter(), value);
							if (prefsEnabled) {
								if (value == null) {
									prefs.remove(prefsKey);
								} else {
									prefs.put(prefsKey, LocaleDelegate.getInstance().getNumberFormat().format(value));
								}
							}
						} catch (ParseException ex) {
							throw new CommonValidationException(
									SpringLocaleDelegate
											.getInstance()
											.getMessage(
													"ParameterPanel.3",
													"{0}: \"{1}\" ist keine g\u00fcltige Zahl.",
													paramvo.getParameter(),
													((JTextField) comp)
															.getText()), ex);
						}
					} else if (paramvo.getDatatype()
							.equals("java.lang.Integer")) {
						try {
							String text = ((JTextField) comp).getText();
							if (text != null) {
								text = text.trim();
							}
							Integer value = StringUtils.isNullOrEmpty(text) ? null :
									LocaleDelegate.getInstance().getNumberFormat().parse(((JTextField) comp).getText()).intValue();
							mpParams.put(paramvo.getParameter(), value);
							if (prefsEnabled) {
								if (value == null) {
									prefs.remove(prefsKey);
								} else {
									prefs.put(prefsKey, value.toString());
								}
							}
						} catch (ParseException ex) {
							throw new CommonValidationException(
									SpringLocaleDelegate
											.getInstance()
											.getMessage(
													"ParameterPanel.3",
													"{0}: \"{1}\" ist keine g\u00fcltige Zahl.",
													paramvo.getParameter(),
													((JTextField) comp)
															.getText()), ex);
						}
					} else if (paramvo.getDatatype().equals("java.util.Date")) {
						try {
							Date value = ((DateChooser) comp).getDate();
							mpParams.put(paramvo.getParameter(), value);
							if (prefsEnabled) {
								if (value == null) {
									prefs.remove(prefsKey);
								} else {
									prefs.put(prefsKey, new SimpleDateFormat("yyyy-MM-dd").format(value));
								}
							}
						} catch (CommonValidationException ex) {
							throw new CommonValidationException(
									SpringLocaleDelegate
											.getInstance()
											.getMessage(
													"ParameterPanel.5",
													"{0}: \"{1}\" ist kein g\u00fcltiges Datum.",
													paramvo.getParameter(),
													((JTextField) comp)
															.getText()), ex);
						}
					}
				}
			}
		}
	}
	
	private static String getPrefsKey(DatasourceParameterVO paramvo) {
		if (paramvo != null && paramvo.getDatasourceUID() != null && paramvo.getParameter() != null) {
			return paramvo.getDatasourceUID().getString() + "_" + paramvo.getParameter();
		}
		return null;
	}
	
} // class ParameterPanel
