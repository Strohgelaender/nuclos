//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General License for more details.
//
//You should have received a copy of the GNU Affero General License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.gef;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.nuclos.client.gef.layout.Extents2D;
import org.nuclos.client.gef.layout.Insets2D;

/**
 * Shape interface
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */

public interface Shape {

	/**
	 *
	 */
	int RESIZE_NW = 0;
	/**
	 *
	 */
	int RESIZE_N = 1;
	/**
	 *
	 */
	int RESIZE_NE = 2;
	/**
	 *
	 */
	int RESIZE_E = 3;
	/**
	 *
	 */
	int RESIZE_SE = 4;
	/**
	 *
	 */
	int RESIZE_S = 5;
	/**
	 *
	 */
	int RESIZE_SW = 6;
	/**
	 *
	 */
	int RESIZE_W = 7;
	/**
	 *
	 */
	int RESIZE_COUNT = 8;

	/**
	 * Perform special actions after the shape has been created.
	 */
	void afterCreate();

	/**
	 * Perform special actions before the shape will be created.
	 */
	void beforeDelete();

	/**
	 *
	 * @return
	 */
	Color getColor();

	/**
	 *
	 * @param color
	 */
	void setColor(Color color);

	/**
	 *
	 * @return
	 */
	Rectangle2D getDimension();

	/**
	 *
	 * @param dimension
	 */
	void setDimension(Rectangle2D dimension);

	/**
	 *
	 * @return
	 */
	Point2D getLocation();

	/**
	 *
	 * @param p
	 */
	void setLocation(Point2D p);

	/**
	 *
	 * @param dX
	 * @param dY
	 */
	void setLocation(double dX, double dY);

	/**
	 *
	 * @param p
	 * @return
	 */
	boolean isPointInside(Point2D p);

	/**
	 *
	 * @param r
	 * @return
	 */
	boolean isInside(Rectangle2D r);

	/**
	 *
	 * @param pIn
	 * @param pOut
	 * @return
	 */
	int isInsideConnector(Point2D pIn, Point2D pOut);

	/**
	 *
	 * @param pIn
	 * @return
	 */
	int isInsideResizer(Point2D pIn);

	/**
	 *
	 * @return
	 */
	boolean isSelected();

	/**
	 *
	 * @param value
	 */
	void setSelection(boolean value);

	/**
	 *
	 * @param gfx
	 */
	void paint(Graphics2D gfx);

	/**
	 *
	 * @param gfx
	 */
	void paintConnectionPoints(Graphics2D gfx);

	/**
	 *
	 * @param p
	 * @return
	 */
	boolean move(Point2D p);

	/**
	 *
	 * @param p
	 * @return
	 */
	boolean resize(Point2D p);

	/**
	 *
	 * @param dX
	 * @param dY
	 * @return
	 */
	boolean move(double dX, double dY);

	/**
	 *
	 * @param dX
	 * @param dY
	 * @return
	 */
	boolean resize(double dX, double dY);

	/**
	 *
	 * @param size
	 */
	void setSize(Extents2D size);

	/**
	 *
	 */
	Extents2D getSize();

	/**
	 *
	 * @return
	 */
	double getX();

	/**
	 *
	 * @return
	 */
	double getY();

	/**
	 *
	 * @return
	 */
	double getWidth();

	/**
	 *
	 * @return
	 */
	double getHeight();

	/**
	 *
	 * @param value
	 */
	void setConnectionPointsVisible(boolean value);

	/**
	 *
	 * @return
	 */
	boolean getConnectionPointsVisible();

	/**
	 *
	 * @param value
	 */
	void setResizePointsVisible(boolean value);

	/**
	 *
	 * @return
	 */
	boolean getResizePointsVisible();

	/**
	 *
	 * @return
	 */
	boolean isConnectable();

	/**
	 *
	 * @param value
	 */
	void setConnectable(boolean value);

	/**
	 *
	 * @param point
	 * @return
	 */
	Point2D getConnectionPoint(int point);

	/**
	 *
	 * @return
	 */
	int getConnectionPointCount();

	/**
	 *
	 * @param view
	 */
	void setView(ShapeViewer view);

	/**
	 *
	 * @return
	 */
	ShapeViewer getView();

	/**
	 *
	 * @return
	 */
	boolean isVisible();

	/**
	 *
	 * @param bVisible
	 */
	void setVisible(boolean bVisible);

	/**
	 *
	 */
	boolean isSelectable();

	/**
	 *
	 * @param bSelectable
	 */
	void setSelectable(boolean bSelectable);

	/**
	 *
	 * @param bValue
	 */
	void setMoveable(boolean bValue);

	/**
	 *
	 * @return
	 */
	boolean isMoveable();

	/**
	 *
	 * @param bValue
	 */
	void setResizeable(boolean bValue);

	/**
	 *
	 * @return
	 */
	boolean isResizeable();

	/**
	 *
	 * @return
	 */
	Insets2D getInsets();

	/**
	 *
	 * @param insets
	 */
	void setInsets(Insets2D insets);

	/**
	 *
	 * @return
	 */
	Extents2D getPreferredSize();

	/**
	 *
	 * @param d
	 */
	void setPreferredSize(Extents2D d);

	/**
	 *
	 * @return
	 */
	Extents2D getMinimumSize();

	/**
	 *
	 * @param d
	 */
	void setMinimumSize(Extents2D d);

	/**
	 *
	 * @return
	 */
	Extents2D getMaximumSize();

	/**
	 *
	 * @param d
	 */
	void setMaximumSize(Extents2D d);

	int getId();
}
