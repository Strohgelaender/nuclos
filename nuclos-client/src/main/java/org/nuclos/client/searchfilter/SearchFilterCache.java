//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.searchfilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * SearchFilterCache containing all searchfilters (entity + global searchfilters)
 * which are assigned to the current user.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 00.01.000
 */
public class SearchFilterCache {
	
	private static final Logger LOG = Logger.getLogger(SearchFilterCache.class);

	private static SearchFilterCache INSTANCE;
	
	//
	
	private static final class Key implements Serializable {
		final String name;
		final String owner;
		final UID entity;
		public Key(String name, String owner, UID entity) {
			this.name = name;
			this.owner = owner;
			this.entity = entity;
		}
		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof Key))
				return false;
			return LangUtils.equal(name, ((Key)obj).name) && LangUtils.equal(owner, ((Key)obj).owner) && LangUtils.equal(entity, ((Key)obj).entity);
		}
		@Override
		public int hashCode() {
			return LangUtils.hashCode(name) ^ LangUtils.hashCode(owner) ^ LangUtils.hashCode(entity);
		}
	}

	private Map<Key, EntitySearchFilter> mpEntitySearchFilter = new HashMap<Key, EntitySearchFilter>();
	
	private MessageListener messageListener;
	
	private String userName;
	
	// Spring injection
	
	private SearchFilterDelegate searchFilterDelegate;
	
	private TopicNotificationReceiver tnr;
	
	// end of Spring injection
	
	private SearchFilterCache() {
		INSTANCE = this;
	}
		
	@Autowired
	final void setSearchFilterDelegate(SearchFilterDelegate searchFilterDelegate) {
		this.searchFilterDelegate = searchFilterDelegate;
	}
	
	final TopicNotificationReceiver getTopicNotificationReceiver() {
		// Maybe null because client-cache (de)serialization (tp)
		if (tnr == null) {
			tnr = SpringApplicationContextHolder.getBean(TopicNotificationReceiver.class);
		}
		return tnr;
	}

	@Autowired
	final void setTopicNotificationReceiver(TopicNotificationReceiver tnr) {
		this.tnr = tnr;
	}

	final SearchFilterDelegate getSearchFilterDelegate() {
		// Maybe null because client-cache (de)serialization (tp)
		if (searchFilterDelegate == null) {
			searchFilterDelegate = SpringApplicationContextHolder.getBean(SearchFilterDelegate.class);
		}
		return searchFilterDelegate;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
		init();
	}
	
	public final void initMessageListener() {
		if (messageListener != null)
			return;

		messageListener = new MessageListener() {
			@Override
	        public void onMessage(Message msg) {
				LOG.info("onMessage: Received notification from server: search filter cache changed.");
				SearchFilterCache.this.invalidate();
				if (msg instanceof ObjectMessage) {
					try {
						final UID[] idAndUsers = (UID[])((ObjectMessage)msg).getObject();
						final UID taskViewId = idAndUsers[0];
						final Main main = Main.getInstance();
						final MainController mc = main.getMainController();
						UID userUid = SecurityDelegate.getInstance().getUserUid(userName);
						boolean refresh = false;
						for (UID userId : idAndUsers) {
							if (userId.equals(userUid)) {
								refresh = true;
							}
						}

						if (refresh) {
							UIUtils.runCommandLater(main.getMainFrame(), new CommonRunnable() {			
								@Override
	                            public void run() throws CommonBusinessException {
									LOG.info("onMessage " + this + " refreshTaskController...");
									mc.refreshTaskController(taskViewId);
								}
							});	
						}
					}
					catch (JMSException ex) {
						LOG.warn("onMessage: Exception thrown in JMS message listener.", ex);
					}
				}
				else {
					LOG.warn("onMessage: Message of type " + msg.getClass().getName() + " received, while a TextMessage was expected.");
				}			
			}
		};
		getTopicNotificationReceiver().subscribe(getCachingTopic(), messageListener);	
	}
	
	public String getCachingTopic() {
		return JMSConstants.TOPICNAME_SEARCHFILTERCACHE;
	}
	
	/**
	 * @return the one (and only) instance of SearchFilterCache
	 */
	public static SearchFilterCache getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	/**
	 * initializes the cache for all entity searchfilters
	 */
	private Map<Key, EntitySearchFilter> loadSearchFilters() {
		Map<Key, EntitySearchFilter> mpSearchFilters = new HashMap<SearchFilterCache.Key, EntitySearchFilter>();
		for (SearchFilter searchFilter : getSearchFilterDelegate().getAllSearchFilterByCurrentUser()) {
			if (isFilterValid(searchFilter)) {
				if (searchFilter instanceof EntitySearchFilter) {
					final SearchFilterVO vo = searchFilter.getSearchFilterVO();
					mpSearchFilters.put(new Key(vo.getFilterName(), vo.getOwner(), vo.getEntity()), (EntitySearchFilter)searchFilter);
				}
			}
		}
		return mpSearchFilters;
	}
	
	void init() {
		Map<Key, EntitySearchFilter> searchFilters = loadSearchFilters();
		mpEntitySearchFilter.clear();
		mpEntitySearchFilter.putAll(searchFilters);
		LOG.info("Filled cache " + this);
	}


	/**
	 * gets the searchfilter for the given filter name and owner
	 * @param sFilterName
	 * @param sOwner
	 * @return SearchFilter
	 */
	public EntitySearchFilter getSearchFilter(String sFilterName, String sOwner, UID sEntity) {
		return getEntitySearchFilter(sFilterName, sOwner, sEntity);
	}

	/**
	 * gets the entity searchfilter for the given filter name and owner
	 * @param sFilterName
	 * @param sOwner
	 * @return GlobalSearchFilter
	 */
	public EntitySearchFilter getEntitySearchFilter(String sFilterName, String sOwner, UID sEntity) {
		if (StringUtils.isNullOrEmpty(sOwner)) {
			sOwner = userName;
		}

		for (Key pKey : mpEntitySearchFilter.keySet()) {
			if (pKey.equals(new Key(sFilterName, sOwner, sEntity))) {
				return mpEntitySearchFilter.get(pKey);
			}
		}
		return null;
	}

	/**
	 * gets all entity searchfilters
	 * 
	 * @return Collection&lt;EntitySearchFilter&gt;
	 */
	public Collection<EntitySearchFilter> getAllEntitySearchFilters() {
		final Collection<EntitySearchFilter> collSearchFilter = new ArrayList<EntitySearchFilter>();
		for (EntitySearchFilter searchFilter : this.mpEntitySearchFilter.values()) {
			collSearchFilter.add(searchFilter);
		}
		return collSearchFilter;
	}

	/**
	 * checks whether a filter with the given name and owner exists
	 * @param sFilterName
	 * @return boolean
	 */
	public boolean filterExists(String sFilterName, String sOwner, UID sEntity) {
		if (sOwner == null) {
			sOwner = userName;
		}

		return (getEntitySearchFilter(sFilterName, sOwner, sEntity) != null);
	}

	/**
	 * gets all entity searchfilters for the given entity
	 * 
	 * @param sEntity
	 * @return Collection&lt;EntitySearchFilter&gt;
	 */
	public Collection<EntitySearchFilter> getEntitySearchFilterByEntity(UID sEntity) {
		final Collection<EntitySearchFilter> collEntitySearchFilter = new ArrayList<EntitySearchFilter>();
		if (sEntity == null) {
			return collEntitySearchFilter;
		}

		for (EntitySearchFilter entitySearchFilter : getAllEntitySearchFilters()) {
			if (sEntity.equals(entitySearchFilter.getSearchFilterVO().getEntity())) {
				collEntitySearchFilter.add(entitySearchFilter);
			}
		}

		return collEntitySearchFilter;
	}

	/**
	 * gets the entity searchfilter by the given id
	 * @param iId
	 * @return EntitySearchFilter
	 */
	public EntitySearchFilter getEntitySearchFilterById(UID iId) {
		for (EntitySearchFilter entitySearchFilter : getAllEntitySearchFilters()) {
			if (iId.equals(entitySearchFilter.getSearchFilterVO().getId())) {
				return entitySearchFilter;
			}
		}

		return null;
	}

	/**
	 * adds a filter to he cache
	 * @param searchFilter
	 */
	public void addFilter(final SearchFilter searchFilter) {
		if (searchFilter instanceof EntitySearchFilter) {
			final SearchFilterVO vo = searchFilter.getSearchFilterVO();
			this.mpEntitySearchFilter.put(new Key(vo.getFilterName(), vo.getOwner(), vo.getEntity()), (EntitySearchFilter)searchFilter);
		}
	}

	/**
	 * removes the given filter from the cache
	 */
	public void removeFilter(String sFilterName, String sOwner, UID sEntity) {
		removeFilter(getSearchFilter(sFilterName, sOwner, sEntity));
	}

	public void removeFilter(SearchFilter searchFilter) {
		if (searchFilter == null) 
			return;
		
		if (searchFilter instanceof EntitySearchFilter) {
			final SearchFilterVO vo = searchFilter.getSearchFilterVO();
			final Key pIdentifier = new Key(vo.getFilterName(), vo.getOwner(), vo.getEntity());
			for (Key pKey : mpEntitySearchFilter.keySet()) {
				if (pKey.equals(pIdentifier)) {
					mpEntitySearchFilter.remove(pKey);
					break;
				}
			}
		}
	}
	
	/**
	 * gets all entity searchfilters of the current user
	 * 
	 * @return List&lt;EntitySearchFilter&gt;
	 */
	public List<EntitySearchFilter> getAllUserFilters() {
		return CollectionUtils.applyFilter(getAllEntitySearchFilters(),
			new Predicate<EntitySearchFilter>() {
				@Override
                public boolean evaluate(EntitySearchFilter t) {
	                return isFilterValid(t);
                }});
		}

	/**
	 * checks whether a searchfilter is valid
	 * @param searchFilter
	 * @return boolean
	 */
	private boolean isFilterValid(SearchFilter searchFilter) {
		boolean bValid = false;
		final SearchFilterVO vo = searchFilter.getSearchFilterVO();
		final Date dValidFrom = vo.getValidFrom();
		final Date dValidUntil = vo.getValidUntil();

		Date dToday = DateUtils.today();

		Integer iFrom = DateUtils.compareDateFrom(dToday, dValidFrom);
		Integer iUntil = DateUtils.compareDateUntil(dToday, dValidUntil);

		if (iFrom >= 0 && iUntil <= 0) {
			bValid = true;
		}

		return bValid;
	}

	/**
	 * clears and fills this cache again.
	 */
	public void invalidate() {
		Map<Key, EntitySearchFilter> searchFilters = loadSearchFilters();
		mpEntitySearchFilter.clear();
		mpEntitySearchFilter.putAll(searchFilters);
		LOG.info("Filled cache " + this);
	}
}
