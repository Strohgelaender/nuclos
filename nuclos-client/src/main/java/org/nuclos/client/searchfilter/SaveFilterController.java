//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.searchfilter;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.genericobject.ui.EnterNameDescriptionPanel;
import org.nuclos.client.genericobject.ui.SaveNameDescriptionPanel;
import org.nuclos.client.ui.ValidatingJOptionPane;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.search.SearchController;
import org.nuclos.client.ui.profile.ProfileModel;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

/**
 * Lets the user save a filter.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public class SaveFilterController {

	private static final Logger LOG = Logger.getLogger(SaveFilterController.class);

	public static enum Command {
		None, Overwrite, New
	}

	private final JComponent parent;

	private final EntitySearchFilters searchfilters;
	
	private SearchFilterVO searchFilter;
	
	private final ProfileModel profileModel;
	
	private final CollectController cltctrl;
	
	public SaveFilterController(JComponent parent, EntitySearchFilters searchfilters, SearchFilterVO searchFilter, ProfileModel profileModel, CollectController cltctrl) {
		this.parent = parent;
		this.searchfilters = searchfilters;
		this.searchFilter = searchFilter;
		this.profileModel = profileModel;
		this.cltctrl = cltctrl;
	}

	/**
	 * saves the current filter, possibly overwriting the selected filter.
	 * @param filterSelected
	 * @param filterCurrent contains the written filter when returning overwrite or new.
	 * @return the executed command (None, Overwrite, New)
	 */
	public Command runSave(final UID entityIfNew, final String ownerIfNew, final EntitySearchFilter filterSelected, 
			final EntitySearchFilter filterCurrent) throws NuclosBusinessException {
		
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		Command result = Command.None;
		final boolean bRegularFilterSelected = (filterSelected != null && !filterSelected.isDefaultFilter());
		final SearchFilterVO selectedVo = filterSelected != null ? filterSelected.getSearchFilterVO() : null;

		if (!bRegularFilterSelected) {
			// it's not possible to change the default filter.
			result = Command.New;
		}
		else {
			// Ask the user if he wants to overwrite the selected filter or add a new one:
			final SaveNameDescriptionPanel pnl = new SaveNameDescriptionPanel();

			pnl.getRadioButtonOverwrite().setText(localeDelegate.getMessage(
					"SaveFilterController.9", "Bestehenden Filter \"{0}\" \u00e4ndern", selectedVo.getFilterName()));
			pnl.getRadioButtonNew().setText(localeDelegate.getMessage(
					"SaveFilterController.7","Neuen Filter anlegen"));
			// default is overwrite:
			pnl.getRadioButtonOverwrite().setSelected(true);

			final String sTitle = localeDelegate.getMessage(
					"SaveFilterController.1","Aktuelle Sucheinstellung speichern");

			final int iBtn = JOptionPane.showConfirmDialog(parent, pnl, sTitle, JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);

			if (iBtn == JOptionPane.OK_OPTION) {
				result = (pnl.getRadioButtonNew().isSelected() ? Command.New : Command.Overwrite);
			}
		}

		if (result != Command.None) {
			if (result == Command.Overwrite && !selectedVo.isEditable()) {
				throw new NuclosBusinessException(localeDelegate.getMessage(
						"SaveFilterController.4","Der Suchfilter darf von Ihnen nicht ge\u00e4ndert werden."));
			}
			final String resultProfile = searchFilter.getResultProfile();
			final EnterNameDescriptionPanel pnlEnterFilter = new EnterNameDescriptionPanel(resultProfile, profileModel);
			String sTitleEnterFilter;
			int iBtnEnterFilter;
			switch (result) {
			case New:
				sTitleEnterFilter = localeDelegate.getMessage("SaveFilterController.8","Neuen Filter anlegen");
				iBtnEnterFilter = showDialog(parent, sTitleEnterFilter, pnlEnterFilter,
						null, ownerIfNew, entityIfNew, result);
				break;
			case Overwrite:
				sTitleEnterFilter = localeDelegate.getMessage("SaveFilterController.2","Bestehenden Filter \u00e4ndern");
				pnlEnterFilter.getTextFieldName().setText(selectedVo.getFilterName());
				pnlEnterFilter.getTextFieldDescription().setText(selectedVo.getDescription());
				iBtnEnterFilter = showDialog(parent, sTitleEnterFilter, pnlEnterFilter,
						selectedVo.getFilterName(), selectedVo.getOwner(), selectedVo.getEntity(), result);
				break;
			default:
				throw new NuclosFatalException();
			}  // switch

			if (iBtnEnterFilter == JOptionPane.OK_OPTION) {
				SearchFilterVO currentVo = filterCurrent.getSearchFilterVO();
				/*if (this.cltctrl instanceof GenericObjectCollectController) { // auskommentiert wegen NUCLOS-6556 (NUCLOS-5205 ist fehlerhaft)
					GenericObjectCollectController gocc = (GenericObjectCollectController) this.cltctrl;
					if (gocc.getProcess() != null && gocc.isCalledByProcessMenu()) {
						filterCurrent.setSearchCondition(SearchController.cleanSearchConditionFromProcess(filterCurrent.getSearchCondition(), gocc));
					}
				}*/
				if (bRegularFilterSelected && result == Command.Overwrite) {
					selectedVo.setSearchDeleted(currentVo.getSearchDeleted());
					currentVo = new SearchFilterVO(
							filterSelected.getSearchFilterVO().getId(),
							filterSelected.getSearchFilterVO().getVersion(),
							filterSelected.getSearchFilterVO());
					filterCurrent.setSearchFilterVO(currentVo);
				}

				currentVo.setFilterName(pnlEnterFilter.getTextFieldName().getText());
				currentVo.setDescription(pnlEnterFilter.getTextFieldDescription().getText());
				currentVo.setResultProfile(pnlEnterFilter.getSelectedResultProfile());
				if (result == Command.New) {
					currentVo.setFastSelectInResult(Boolean.FALSE);
				}
			}
			else {
				result = Command.None;
			}
		}

		if (result != Command.None) {
			if (result == Command.Overwrite) {
				final String sOldFilterName = selectedVo.getFilterName();
				final String sOwner = selectedVo.getOwner();
				final UID sEntity = selectedVo.getEntity();
				try {
					SearchFilterDelegate.getInstance().updateSearchFilter(filterCurrent, sOldFilterName, sOwner, sEntity);
				}
				catch (NuclosBusinessException e) {
					// filterCurrent = SearchFilterCache.getInstance().getSearchFilter(sOldFilterName, sOwner, sEntity);
					throw e;
				}
			}
			else {
				try {
					getSearchFilters().put(filterCurrent);
				}
				catch (PreferencesException ex) {
					final String sMessage = localeDelegate.getMessage("SaveFilterController.5","Die Benutzereinstellungen konnten nicht geschrieben werden") 
							+ ".";
					throw new NuclosFatalException(sMessage, ex);
				}
			}
			// write changes to field
			searchFilter = filterCurrent.getSearchFilterVO();
		}
		return result;
	}

	private EntitySearchFilters getSearchFilters() {
		return searchfilters;
	}
	
	public SearchFilterVO getSearchFilterVO() {
		return searchFilter;
	}

	public static int showDialog(Component parent, String sTitleEnterFilter, final EnterNameDescriptionPanel pnlEnterFilter, final String oldName, final String sOwner, final UID sEntity, final Command cmd) {
		final ValidatingJOptionPane optpn = new ValidatingJOptionPane(parent, sTitleEnterFilter, pnlEnterFilter) {

			@Override
			protected void validateInput() throws ErrorInfo {
				final String sFilterName = pnlEnterFilter.getTextFieldName().getText();
				if (StringUtils.isNullOrEmpty(sFilterName)) {
					throw new ErrorInfo(SpringLocaleDelegate.getInstance().getMessage(
							"SaveFilterController.3","Bitte geben Sie einen Namen f\u00fcr den Filter an") + ".", pnlEnterFilter.getTextFieldName());
				}
				try {
					SearchFilter.validate(sFilterName);
				}
				catch(Exception ex) {
					throw new ErrorInfo(ex.getMessage(), pnlEnterFilter.getTextFieldName());
				}
				// Check whether the filter name exists already (in another module)
				/** @todo change the data structure so a filter name must be unique per module only */

				// Uniqueness must be checked for a new filter or if the filter name was changed:
				boolean bMustCheckUniqueness = cmd == Command.New || (cmd == Command.Overwrite && !LangUtils.equal(oldName, pnlEnterFilter.getTextFieldName().getText()));

				if (bMustCheckUniqueness && SearchFilterCache.getInstance().filterExists(sFilterName, sOwner, sEntity)) {
					throw new ErrorInfo(SpringLocaleDelegate.getInstance().getMessage(
							"SaveFilterController.6","Ein Filter mit diesem Namen existiert bereits") + ".", pnlEnterFilter.getTextFieldName());
				}
			}
		};

		return optpn.showDialog();
	}
}  // class SaveFilterControlle
