//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.searchfilter;

import java.rmi.RemoteException;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.Main;
import org.nuclos.common.UID;
import org.nuclos.common.Utils;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.searchfilter.ISeachFilterWithId;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.navigation.treenode.EntitySearchResultTreeNode;
import org.nuclos.server.navigation.treenode.TreeNode;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

/**
 * Tree node implementation representing an entity search filter.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 00.01.000
 */
public class SearchFilterTreeNode extends EntitySearchResultTreeNode implements ISeachFilterWithId {

	private static final long serialVersionUID = -5356067512724852062L;

	private static final Logger log = Logger.getLogger(SearchFilterTreeNode.class);

	private transient EntitySearchFilter searchfilter;
	private UID searchFilterId;

	public SearchFilterTreeNode(EntitySearchFilter searchfilter) {
		super(searchfilter.getSearchFilterVO().getFilterName(),
				(searchfilter.getSearchFilterVO().getOwner() != null && !searchfilter.getSearchFilterVO().getOwner().equals(
						Main.getInstance().getMainController().getUserName())) 
						? searchfilter.getSearchFilterVO().getDescription() + "(" + searchfilter.getSearchFilterVO().getOwner() + ")" 
						: searchfilter.getSearchFilterVO().getDescription(),
				getInternalSearchExpression(searchfilter),
				searchfilter.getSearchFilterVO().getFilterName(), 
				searchfilter.getSearchFilterVO().getOwner(), 
				searchfilter.getSearchFilterVO().getEntity(), null, null);
		this.searchfilter = searchfilter;
		this.searchFilterId = searchfilter.getSearchFilterVO().getId();
	}

	@Override
	protected List<? extends TreeNode> getSubNodesImpl() throws RemoteException, CommonPermissionException {
		// need to build a EntitySearchResultTreeNode because the server does not know SearchFilterTreeNodes:
		final EntitySearchResultTreeNode node = new EntitySearchResultTreeNode(
				this.getLabel(), this.getDescription(), this.getSearchExpression(), this.getFilterName(), this.getOwner(), this.getEntityUID(), getNodeId(), IdUtils.toLongId(getRootId()));

		return Utils.getTreeNodeFacade().getSubNodes(node);
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		try {
			EntitySearchFilter searchFilter = (EntitySearchFilter)EntitySearchFilters.forEntity(this.getEntityUID()).get(this.getFilterName(), this.getOwner(), this.getEntityUID());
			if (searchFilter != null)
				return new SearchFilterTreeNode(searchFilter);
			else
				throw new CommonFinderException(getSpringLocaleDelegate().getMessage(
						"SearchFilterTreeNode.1","Der Suchfilter existiert nicht mehr") + ".");
		}
		catch (NoSuchElementException ex) {
			throw new CommonFinderException(getSpringLocaleDelegate().getMessage(
					"SearchFilterTreeNode.1","Der Suchfilter existiert nicht mehr") + ".", ex);
		}
		catch (PreferencesException ex) {
			throw new CommonFatalException(ex);
		}
	}

	private static CollectableSearchExpression getInternalSearchExpression(EntitySearchFilter searchfilter) {
		/** @todo respect sorting order! */
		final SearchFilterVO vo = searchfilter.getSearchFilterVO();
		if (Modules.getInstance().isModule(vo.getEntity())) {
			return new CollectableGenericObjectSearchExpression(searchfilter.getInternalSearchCondition(), vo.getSearchDeleted());
		}
		return new CollectableSearchExpression(searchfilter.getInternalSearchCondition());
	}

	public EntitySearchFilter getSearchFilter() {
		if (searchfilter == null && searchFilterId != null)
			searchfilter = SearchFilterCache.getInstance().getEntitySearchFilterById(searchFilterId);
		return searchfilter;
	}

	@Override
	public String getLabel() {
		try {
			if (getSearchFilter() != null) { 
				final SearchFilterVO vo = getSearchFilter().getSearchFilterVO();
				if (vo.getLabelResourceId() != null) {
					return getSpringLocaleDelegate().getTextFallback(vo.getLabelResourceId(), super.getLabel());
				}
			}
		}
		catch (Exception ex) {
			log.warn(ex.getMessage(), ex);
		}
		return super.getLabel();
	}

	@Override
	public String getDescription() {
		try {
			if (getSearchFilter() != null) {
				final SearchFilterVO vo = getSearchFilter().getSearchFilterVO();
				if (vo.getDescriptionResourceId() != null) {
					return getSpringLocaleDelegate().getTextFallback(vo.getDescriptionResourceId(), super.getDescription());
				}
			}
		}
		catch (Exception ex) {
			log.warn(ex.getMessage(), ex);
		}
		return super.getDescription();
	}

	@Override
	public UID getSearchFilterId() {
		return searchFilterId;
	}

}	// class SearchFilterTreeNode
