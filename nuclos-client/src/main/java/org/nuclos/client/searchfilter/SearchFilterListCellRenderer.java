//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.searchfilter;

import java.awt.*;

import javax.swing.*;

import org.nuclos.client.main.Main;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

public class SearchFilterListCellRenderer extends DefaultListCellRenderer {

	@Override
	public Component getListCellRendererComponent(JList lst, Object oValue, int index, boolean bSelected, boolean bCellHasFocus) {
		final JComponent result = (JComponent) super.getListCellRendererComponent(lst, oValue, index, bSelected, bCellHasFocus);
		String sToolTip = null;
		if (oValue != null && oValue instanceof SearchFilter) {
			final SearchFilter filter = (SearchFilter) oValue;
			final SearchFilterVO vo = filter.getSearchFilterVO();

			if (result instanceof JLabel && !StringUtils.isNullOrEmpty(vo.getLabelResourceId())) {
				((JLabel) result).setText(SpringLocaleDelegate.getInstance().getTextFallback(
						vo.getLabelResourceId(), vo.getFilterName()));
			}

			if (!StringUtils.isNullOrEmpty(vo.getDescriptionResourceId())) {
				sToolTip = SpringLocaleDelegate.getInstance().getTextFallback(
						vo.getDescriptionResourceId(), vo.getDescriptionResourceId());
			}
			else {
				sToolTip = vo.getDescription();
			}

			if (vo.getOwner() != null && !(vo.getOwner().equals(Main.getInstance().getMainController().getUserName()))) {
				sToolTip = sToolTip + " (" + vo.getOwner() + ")";
			}

			result.setToolTipText(sToolTip);
		}
		return result;
	}
}
