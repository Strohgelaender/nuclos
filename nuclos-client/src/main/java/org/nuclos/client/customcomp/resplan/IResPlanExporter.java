package org.nuclos.client.customcomp.resplan;

import java.io.File;
import java.io.IOException;

import javax.xml.xpath.XPathExpressionException;

import org.nuclos.client.fileexport.FileType;
import org.nuclos.client.image.ImageType;
import org.nuclos.client.image.SVGDOMDocumentSupport;
import org.nuclos.common.report.NuclosReportException;

public interface IResPlanExporter<R, E, L> {

	SVGDOMDocumentSupport getSVGDOMDocumentSupport();

	void run(String template, int startCategory, boolean skipInsideCats) throws IOException, XPathExpressionException, NuclosReportException;

	void save(ImageType imageType, File save) throws IOException;

	void save(FileType fileType, File save) throws IOException;

}
