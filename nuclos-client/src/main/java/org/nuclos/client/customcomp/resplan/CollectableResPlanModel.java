//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp.resplan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.renderer.ComponentProvider;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.collect.CollectController.CollectableEventListener;
import org.nuclos.client.ui.collect.CollectController.MessageType;
import org.nuclos.client.ui.resplan.AbstractResPlanModel;
import org.nuclos.client.ui.resplan.JResPlanComponent.JMilestone;
import org.nuclos.client.ui.resplan.JResPlanComponent.JRelation;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common.security.Permission;
import org.nuclos.common.time.LocalTime;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.interval.Interval;

//Version
public class CollectableResPlanModel<PK,R extends Collectable<PK>> extends AbstractResPlanModel<PK, R, Date, Collectable<PK>, Collectable<PK>> {

	protected static final Logger LOG = Logger.getLogger(CollectableResPlanModel.class);

	private boolean readOnly = false;
	private ResPlanController<PK,R> controller;
	private ClientResPlanConfigVO<PK,R,Collectable<PK>> configVO;
	private List<R> resources = new ArrayList<R>();
	private Map<Object, R> resourceMap = new HashMap<Object, R>();
	private Map<Object, List<EntryWPElem<R>>> entryMap = new HashMap<Object, List<EntryWPElem<R>>>();
	private Map<Object, List<Collectable<PK>>> relationMap = new HashMap<Object, List<Collectable<PK>>>();
	private List<Collectable<PK>> relations = new ArrayList<Collectable<PK>>();

	public CollectableResPlanModel(ResPlanController<PK,R> controller) {
		this.controller = controller;
		this.configVO = controller.getConfigVO();
	}
	
	private Map<ClientPlanElement<PK,R,Collectable<PK>>, ComponentProvider<?>> mapLabelProvider = 
			new HashMap<ClientPlanElement<PK,R,Collectable<PK>>, ComponentProvider<?>>();
	public void setMapLabelProvider(Map<ClientPlanElement<PK,R,Collectable<PK>>, ComponentProvider<?>>  provider) {
		this.mapLabelProvider = provider;
	}
	
	private Map<ClientPlanElement<PK,R,R>, JMilestone> mapMileStoneRenderer = new HashMap<ClientPlanElement<PK,R,R>, JMilestone>();
	public void setMapMileStoneRenderer(Map<ClientPlanElement<PK,R,R>, JMilestone> provider) {
		this.mapMileStoneRenderer = provider;
	}
	
	private Map<ClientPlanElement<PK,R,R>, JRelation> mapRelationRenderer = new HashMap<ClientPlanElement<PK,R,R>, JRelation>();
	public void setRelationRenderer(Map<ClientPlanElement<PK,R,R>, JRelation> provider) {
		this.mapRelationRenderer = provider;
	}
	
	@Override
	public ComponentProvider<?> getComponentProvider(ClientPlanElement<PK,R,Collectable<PK>> pElem) {
		return mapLabelProvider.get(pElem);
	}

	@Override
	public JMilestone getMileStoneRenderer(ClientPlanElement<PK,R,Collectable<PK>> pElem) {
		return mapMileStoneRenderer.get(pElem);
	}
	
	@Override
	public JRelation getRelationRenderer(Collectable<PK> relation) {
		return mapRelationRenderer.get(getFRelation());
	}

	@Override
	public Class getEntryType() {
		return Collectable.class;
	}
	
	public UID getResourceEntity() {
		return configVO.getResourceEntity();
	}
	
	//TODO This will be multi-dimensional
	@Deprecated
	ClientPlanElement<PK,R,Collectable<PK>> getFRelation() {
		return configVO.getFirstRelation();
	}

	public void setData(Collection<R> resources, 
			Map<ClientPlanElement<PK,R,Collectable<PK>>, List<? extends Collectable<PK>>> mapEntries, Collection<? extends Collectable<PK>> relations) {
		this.resources = new ArrayList<R>(resources);
		this.resourceMap = new HashMap<Object, R>();
		for (R res : this.resources) {
			resourceMap.put(res.getId(), res);
		}
		this.entryMap.clear();
		for (ClientPlanElement<PK,R,Collectable<PK>> pElem : mapEntries.keySet()) {
			for (Collectable<PK> clct : mapEntries.get(pElem)) {
				addEntryToMap(clct, pElem);					
			}			
		}
		if (relations != null) {
			this.relationMap = new HashMap<Object, List<Collectable<PK>>>();
			this.relations = new ArrayList<Collectable<PK>>(relations);
			for (Collectable<PK> clct : relations) {
				addRelationToMap(clct);
			}
		}
		fireResourcesChanged();
	}
	
	private void addEntryToMap(Collectable<PK> clct, ClientPlanElement<PK,R,Collectable<PK>> pElem) {
		Object parentId = clct.getValueId(pElem.getPrimaryField());
		List<EntryWPElem<R>> parentEntries = entryMap.get(parentId);
		if (parentEntries == null) {
			parentEntries = new ArrayList<EntryWPElem<R>>();
			entryMap.put(parentId, parentEntries);
		}
		parentEntries.add(new EntryWPElem<R>(clct, pElem));
	}
	
	private void removeEntryFromMap(Collectable<PK> entry, ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		UID sPrimaryField = pElement.getPrimaryField();
		Object valueId = entry.getValueId(sPrimaryField);
		List<EntryWPElem<R>> list = entryMap.get(valueId);
		if (list != null) {
			List<EntryWPElem<R>> toDelete = new ArrayList<EntryWPElem<R>>();
			for (EntryWPElem<R> wpElem : list) {
				if (wpElem.getCollectable().equals(entry)) {
					toDelete.add(wpElem);
				}
			}
			list.removeAll(toDelete);
		}
	}
	
	private void replaceEntries(Collectable<PK> oldEntry, Collectable<PK> newEntry, ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		UID sPrimaryField = pElement.getPrimaryField();
		Object oldValueId = oldEntry.getValueId(sPrimaryField);
		Object newValueId = newEntry.getValueId(sPrimaryField);
		List<EntryWPElem<R>> lstOld = entryMap.get(oldValueId);
		List<EntryWPElem<R>> lstChanged = new ArrayList<EntryWPElem<R>>();
		if (lstOld != null) {
			for (EntryWPElem<R> wpElem : lstOld) {
				if (wpElem.getCollectable().equals(oldEntry)) {
					wpElem.setCollectable(newEntry);
					lstChanged.add(wpElem);
				}
			}
		}
		List<EntryWPElem<R>> lstNew = entryMap.get(newValueId);
		if (lstOld != null && lstNew != lstOld) {
			lstOld.removeAll(lstChanged);
			if (lstNew == null) {
				entryMap.put(newValueId, lstNew = new ArrayList<EntryWPElem<R>>());
			}
			lstNew.addAll(lstChanged);									
		}
	}
	
	private void addRelationToMap(Collectable<PK> clct) {
		ClientPlanElement<PK,R,Collectable<PK>> rpRelation = getFRelation();
		final Object fromId = clct.getValueId(rpRelation.getPrimaryField());
		final Object toId = clct.getValueId(rpRelation.getSecondaryField());
		List<Collectable<PK>> fromRelations = relationMap.get(fromId);
		if (fromRelations == null) {
			fromRelations = new ArrayList<Collectable<PK>>();
			relationMap.put(fromId, fromRelations);
		}
		fromRelations.add(clct);
		List<Collectable<PK>> toRelations = relationMap.get(toId);
		if (toRelations == null) {
			toRelations = new ArrayList<Collectable<PK>>();
			relationMap.put(toId, toRelations);
		}
		toRelations.add(clct);
	}

	private void removeRelationFromMap(Collectable<PK> relation) {
		for (List<Collectable<PK>> list : relationMap.values()) {
			if (list != null) {
				list.remove(relation);
			}
		}
	}
	
	public List<ClientPlanElement<PK,R,Collectable<PK>>> getListEntriesOrMileStones(boolean withMileStones) {
		return configVO.getListEntriesOrMileStones(withMileStones); 
	}

	@Override
	public List<R> getResources() {
		return resources;
	}

	@Override
	public List<EntryWPElem<R>> getWPElements(R resource) {
		List<EntryWPElem<R>> list = entryMap.get(resource.getId());
		return (list != null) ? list : Collections.<EntryWPElem<R>>emptyList();
	}

	public Date getDefaultViewFrom() {
		try {
			if (configVO.getDefaultViewFrom() != null) {
				return DateUtils.calc(configVO.getDefaultViewFrom());
			}
		} catch (Exception ex) {
			LOG.warn("Error parsing default view from " + configVO.getDefaultViewFrom(), ex);
		}
		return DateUtils.getPureDate(DateUtils.today());
	}
	
	public Date getDefaultViewUntil() {
		try {
			if (configVO.getDefaultViewUntil() != null) {
				return DateUtils.calc(configVO.getDefaultViewUntil());
			}
		} catch (Exception ex) {
			LOG.warn("Error parsing default view from " + configVO.getDefaultViewUntil(), ex);
		}
		return DateUtils.getPureDate(DateUtils.addMonths(DateUtils.today(), 1));
	}

	@Override
	public Interval<Date> getInterval(Collectable<PK> entry, ClientPlanElement<PK,R,Collectable<PK>> rpEntry) {
		Date start = getDateTime(entry, rpEntry.getDateFromField(), rpEntry.getTimeFromField(), rpEntry, false);
		Date end = rpEntry.isMileStone() ? start 
				: getDateTime(entry, rpEntry.getDateUntilField(), rpEntry.getTimeUntilField(), rpEntry, true);
		return new Interval<Date>(start, end, true);
	}

	@Override
	public void updateEntry(final Collectable<PK> entry, final R resource, final Interval<Date> interval, 
			final ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		controller.runCommandWithSpecialHandler(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				CollectableHelper<PK,R,Collectable<PK>> collHelper = pElement.getCollHelper();
				Collectable<PK> clone = collHelper.copyCollectable(entry);
				prepareCollectableEntry(clone, resource, interval, pElement);
				
				Collectable<PK> modified = collHelper.modify(clone);
				replaceEntries(entry, modified, pElement);
				fireResourcesChanged();
			}
		}, entry);
	}

	@Override
	public void removeEntry(final Collectable<PK> entry, final ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		controller.runCommandWithSpecialHandler(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				CollectableHelper<PK,R,Collectable<PK>> collHelper = pElement.getCollHelper();
				collHelper.remove(entry);
				removeEntryFromMap(entry, pElement);
				fireResourcesChanged();
			}
		}, entry);
	}

	@Override
	public void createEntry(final R resource, final Interval<Date> interval, Object value, 
			final ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		if (value instanceof Collectable) {
			final Collectable<PK> entry = (Collectable<PK>) value;
			final CollectableHelper<PK,R,Collectable<PK>> collHelper = pElement.getCollHelper();
			final Collectable<PK> clct = collHelper.check(entry);
			if (clct != null) {
				controller.runCommandWithSpecialHandler(new CommonRunnable() {
					@Override
					public void run() throws CommonBusinessException {
						Collectable<PK> clone = collHelper.cloneCollectable(clct);
						prepareCollectableEntry(clone, resource, interval, pElement);
						clone = collHelper.create(clone);
						addEntryToMap(clone, pElement);
						fireResourcesChanged();
					}
				}, clct);
			}
		}
	}
	
	@Override
	public boolean isCreateRelationAllowed() {
		ClientPlanElement<PK,R,Collectable<PK>> peRelation = getFRelation();
		if (readOnly || peRelation == null)
			return false;
		return peRelation.getCollHelper().isNewAllowed();
	}
	
	@Override
	public boolean isRemoveRelationAllowed(Collectable<PK> relation) {
		ClientPlanElement<PK,R,Collectable<PK>> peRelation = getFRelation();
		if (readOnly || peRelation == null)
			return false;
		relation = peRelation.getCollHelper().check(relation);
		return relation != null && peRelation.getCollHelper().isRemoveAllowed(relation);
	}
	
	@Override
	public boolean isUpdateRelationAllowed(Collectable<PK> relation) {
		ClientPlanElement<PK,R,Collectable<PK>> peRelation = getFRelation();
		if (readOnly || peRelation == null)
			return false;
		relation = peRelation.getCollHelper().check(relation);
		return relation != null && peRelation.getCollHelper().isModifyAllowed(relation);
	}
	
	@Override
	public boolean isCreateEntryAllowed(Collectable<PK> entry, ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		return isCreateEntryAllowed(pElement);
	}

	public boolean isCreateEntryAllowed(ClientPlanElement<PK,R,Collectable<PK>> pElem) {
		if (readOnly)
			return false;
		return pElem.getCollHelper().isNewAllowed();
	}

	@Override
	public boolean isRemoveEntryAllowed(Collectable<PK> entry, ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		if (readOnly)
			return false;
		entry = pElement.getCollHelper().check(entry);
		return entry != null && pElement.getCollHelper().isRemoveAllowed(entry);
	}
	
	@Override
	public boolean isUpdateEntryTimeAllowed(Collectable<PK> entry, ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		if (readOnly)
			return false;
		entry = pElement.getCollHelper().check(entry);
		boolean modifyAllowed = pElement.getCollHelper().isModifyAllowed(entry);
		boolean timeModifyAllowed = true;
		if (entry instanceof CollectableGenericObject) {
			CollectableGenericObject clctGO = (CollectableGenericObject) entry;
			Permission dateFromPerm = SecurityCache.getInstance().getAttributePermission(clctGO.getEntityUID(), pElement.getDateFromField(), clctGO.getGenericObjectCVO().getStatus());
			Permission dateUntilPerm = SecurityCache.getInstance().getAttributePermission(clctGO.getEntityUID(), pElement.getDateUntilField(), clctGO.getGenericObjectCVO().getStatus());
			timeModifyAllowed = Permission.READWRITE.equals(dateFromPerm) && Permission.READWRITE.equals(dateUntilPerm);
			if (pElement.getTimeFromField() != null && pElement.getTimeUntilField() != null) {
				Permission timeFromPerm = SecurityCache.getInstance().getAttributePermission(clctGO.getEntityUID(), pElement.getTimeFromField(), clctGO.getGenericObjectCVO().getStatus());
				Permission timeUntilPerm = SecurityCache.getInstance().getAttributePermission(clctGO.getEntityUID(), pElement.getTimeUntilField(), clctGO.getGenericObjectCVO().getStatus());
				timeModifyAllowed &= Permission.READWRITE.equals(timeFromPerm) && Permission.READWRITE.equals(timeUntilPerm);
			}
		}
		return entry != null && modifyAllowed && timeModifyAllowed;
	}
	
	@Override
	public boolean isUpdateEntryResourceAllowed(Collectable<PK> entry, ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		if (readOnly)
			return false;
		entry = pElement.getCollHelper().check(entry);
		boolean modifyAllowed = pElement.getCollHelper().isModifyAllowed(entry);
		boolean resourceModifyAllowed = true;
		if (entry instanceof CollectableGenericObject) {
			CollectableGenericObject clctGO = (CollectableGenericObject) entry;
			Permission refPerm = SecurityCache.getInstance().getAttributePermission(clctGO.getEntityUID(), pElement.getPrimaryField(), clctGO.getGenericObjectCVO().getStatus());
			resourceModifyAllowed = Permission.READWRITE.equals(refPerm);
		}
		return entry != null && modifyAllowed && resourceModifyAllowed;
	}
	
	public Collectable<PK> createCollectableEntry(Collectable<PK> resource, Interval<Date> interval, 
			ClientPlanElement<PK,R,Collectable<PK>> pElement) {
		
		Collectable<PK> clct = pElement.getCollHelper().newCollectable(true);
		prepareCollectableEntry(clct, resource, interval, pElement);
		return clct;
	}
	
	public boolean isCollectableEntryValid(Collectable<PK> entry, ClientPlanElement<PK,R,Collectable<PK>> rpEntry) {
		if (rpEntry == null) {
			return false;
		}
		Object dateFrom = entry.getValue(rpEntry.getDateFromField());
		Object dateUntil = rpEntry.isMileStone() ? dateFrom : entry.getValue(rpEntry.getDateUntilField());
		if (!(dateFrom instanceof Date) || !(dateUntil instanceof Date))
			return false;
		if (rpEntry.hasTime()) {
			final Object timeFrom = entry.getValue(rpEntry.getTimeFromField());
			Object timeUntil = rpEntry.isMileStone() ? timeFrom : entry.getValue(rpEntry.getTimeUntilField());
			return isValidTimeValue(timeFrom) && isValidTimeValue(timeUntil);
		}
		return true;
	}

	private static boolean isValidTimeValue(Object value) {
		// null is valid (see getDateTime and means start resp. end of the whole day)
		if (value == null)
			return true;
		if (value instanceof String) {
			try {
				// Test if time string is parseable
				LocalTime.parse((String) value);
				return true;
			} catch (IllegalArgumentException ex) {
				return false;
			}
		}
		return false;
	}

	private Collectable<PK> prepareCollectableEntry(Collectable<PK> entry, Collectable<PK> resource, 
			Interval<Date> interval, ClientPlanElement<PK,R,Collectable<PK>> pElem) {
		
		setDateTime(entry, pElem.getDateFromField(), pElem.getTimeFromField(), pElem, interval.getStart(), false);
		if (!pElem.isMileStone()) {
			setDateTime(entry, pElem.getDateUntilField(), pElem.getTimeUntilField(), pElem, interval.getEnd(), true);			
		}
		if (resource != null) {
			String resourceLabel = getResourceLabel(resource, pElem);
			if (resourceLabel.startsWith("${")) {
				resourceLabel = resourceLabel.substring(2);
				if (resourceLabel.endsWith("}")) {
					resourceLabel = resourceLabel.substring(0, resourceLabel.length() - 1);
				}
			}
			entry.setField(pElem.getPrimaryField(), new CollectableValueIdField(resource.getId(), resourceLabel));
		}
		return entry;
	}
	
	private String getResourceLabel(Collectable<PK> resource, ClientPlanElement<PK,R,Collectable<PK>> pElem) {
		FieldMeta<?> entityField = MetaProvider.getInstance().getEntityField(pElem.getPrimaryField());
		String resourceLabel = CollectableUtils.formatFieldExpression(entityField.getForeignEntityField(), resource);
		return resourceLabel;
	}
	
	private Date getDateTime(Collectable<PK> clct, UID dateField, UID timeField, ClientPlanElement<PK,R,Collectable<PK>> pElem, boolean end) {
		Date date = (Date) clct.getValue(dateField);
		date = DateUtils.getPureDate(date);
		boolean add1Day = false;
		if (pElem.hasTime()) {
			final String timeString = (String) clct.getValue(timeField);
			long secondOfDay = 0;
			if (timeString != null) {
				secondOfDay = LocalTime.parse(timeString).toSecondOfDay();
				date = new Date(date.getTime() + secondOfDay * 1000);
			} else {
				add1Day = end;
			}
		} else {
			add1Day = end;
		}
		if (add1Day)
			date = DateUtils.addDays(date, 1);
		return date;
	}
	
	private void setDateTime(Collectable<PK> clct, UID dateField, UID timeField, 
			ClientPlanElement<PK,R,Collectable<PK>> pElem, Date date, boolean end) {
		
		Date datePart = DateUtils.getPureDate(date);
		if (pElem.hasTime()) {
			final GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(date);
			final LocalTime time = LocalTime.ofSecondOfDay((date.getTime() - datePart.getTime()) / 1000);
			final String timeString = time.toString();
			clct.setField(timeField, new CollectableValueField(timeString));
		} else {
			if (end) {
				datePart = DateUtils.addDays(datePart, -1);
			}
		}
		clct.setField(dateField, new CollectableValueField(datePart));
	}

	@Override
	public List<? extends Collectable<PK>> getRelations(Collectable<PK> entry) {
		if (entry == null) {
			return Collections.<Collectable<PK>>emptyList();
		}
		final List<Collectable<PK>> list = relationMap.get(entry.getId());
		return (list != null) ? list : Collections.<Collectable<PK>>emptyList();
	}

	@Override
	public List<? extends Collectable<PK>> getAllRelations() {
		return relations;
	}

	@Override
	public Object getRelationFromId(Collectable<PK> relation) {
		PlanElement<R> rpRelation = getFRelation();
		final Object fromId = relation.getValueId(rpRelation.getPrimaryField());
		return fromId;
	}

	@Override
	public Object getRelationToId(Collectable<PK> relation) {
		PlanElement<R> rpRelation = getFRelation();
		final Object toId = relation.getValueId(rpRelation.getSecondaryField());
		return toId;
	}

	@Override
	public Object getEntryId(Collectable<PK> entry) {
		return entry.getId();
	}

	@Override
	public Object getRelationId(Collectable<PK> relation) {
		return relation.getId();
	}

	@Override
	public void removeRelation(final Collectable<PK> relation) {
		controller.runCommandWithSpecialHandler(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				ClientPlanElement<PK,R,Collectable<PK>> peRelation = getFRelation();
				peRelation.getCollHelper().remove(relation);
				relations.remove(relation);
				removeRelationFromMap(relation);
				fireResourcesChanged();
			}
		}, relation);
	}

	@Override
	public void createRelation(Collectable<PK> entryFrom, Collectable<PK> entryTo, ClientPlanElement<PK,R,Collectable<PK>> pFrom, ClientPlanElement<PK,R,Collectable<PK>> pTo) {
		CollectableHelper<PK,R,Collectable<PK>> collHelperEntryFrom = pFrom.getCollHelper();
		CollectableHelper<PK,R,Collectable<PK>> collHelperEntryTo = pTo.getCollHelper();
		final Collectable<PK> clctFrom = collHelperEntryFrom.check(entryFrom);
		final Collectable<PK> clctTo = collHelperEntryTo.check(entryTo);
		
		if (clctFrom != null && clctTo != null) {
			controller.runCommandWithSpecialHandler(new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					ClientPlanElement<PK,R,Collectable<PK>> rpRelation = configVO.getFirstRelation();
					CollectableHelper<PK,R,Collectable<PK>> collHelperRelation = rpRelation.getCollHelper();

					if (rpRelation.isNewRelationFromController()) {
						Collectable<PK> clctRelation = collHelperRelation.newCollectable(true);
						prepareCollectableRelation(clctRelation, clctFrom, clctTo);
						final MainFrameTab tabIfAny = new MainFrameTab();
						final NuclosCollectController<PK,Collectable<PK>> cntrl = (NuclosCollectController<PK, Collectable<PK>>) 
								NuclosCollectControllerFactory.getInstance().newCollectController(
										collHelperRelation.getCollectableEntity().getUID(), tabIfAny, 
										ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
						Main.getInstance().getMainController().initMainFrameTab(controller, tabIfAny);
						cntrl.addCollectableEventListener(new CollectControllerEventHandler<PK>(cntrl, controller));
						controller.getHolderComponent().add(tabIfAny);
						cntrl.runNewWith(clctRelation);
					} else {
						Collectable<PK> clctRelation = collHelperRelation.newCollectable(true);
						prepareCollectableRelation(clctRelation, clctFrom, clctTo);
						clctRelation = collHelperRelation.create(clctRelation);
						relations.add(clctRelation);
						addRelationToMap(clctRelation);
						fireResourcesChanged();
					}
				}
			}, clctTo);
		}
	}
	
	private static final class CollectControllerEventHandler<PK> implements CollectableEventListener<PK> {

		private final NuclosCollectController<PK,?> collectController;
		
		private final ResPlanController<PK,?> controller;

		public CollectControllerEventHandler(NuclosCollectController<PK,?> clctCntrl, ResPlanController<PK,?> controller) {
			this.collectController = clctCntrl;
			this.controller = controller;
		}

		@Override
		public void handleCollectableEvent(Collectable<PK> collectable, MessageType messageType) {
			switch (messageType) {
			case EDIT_DONE:
			case NEW_DONE :
			case DELETE_DONE:
				try {
					collectController.getTab().close();
				} finally {
					controller.refresh(true);
				}
			}
		}
	}

	private Collectable<PK> prepareCollectableRelation(Collectable<PK> relation, Collectable<PK> from, Collectable<PK> to) {
		PlanElement<R> rpRelation = getFRelation();
		FieldMeta<?> ef = MetaProvider.getInstance().getEntityField(rpRelation.getPrimaryField());
		String label = CollectableUtils.formatFieldExpression(ef.getForeignEntityField(), from);
		relation.setField(rpRelation.getPrimaryField(), new CollectableValueIdField(getEntryId(from), label));
		
		ef = MetaProvider.getInstance().getEntityField(rpRelation.getSecondaryField());
		label = CollectableUtils.formatFieldExpression(ef.getForeignEntityField(), to);
		relation.setField(rpRelation.getSecondaryField(), new CollectableValueIdField(getEntryId(to), label));
		
		return relation;
	}

	@Override
	public R getResourceFromEntry(Collectable<PK> entry, ClientPlanElement<PK,R,Collectable<PK>> rpEntry) {
		final Object resId = entry.getValueId(rpEntry.getPrimaryField());
		if (resId != null) {
			return resourceMap.get(resId);
		}
		return null;
	}

	@Override
	public List<? extends Collectable<PK>> getEntries(R resource) {
		return null;
	}

	@Override
	public void notifyInvalidHeader() {
		try {
			controller.storeSharedState();
		} catch (PreferencesException | CommonFinderException | CommonPermissionException e) {
			LOG.warn("notifyInvalidHeader: " + e, e);
		}
	}
	
	public ResPlanController<PK, R> getController() {
		return controller;
	}
}
