//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp.resplan;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.renderer.ComponentProvider;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.client.command.BackgroundTask;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.customcomp.CustomComponentController;
import org.nuclos.client.customcomp.resplan.ResPlanPanel.ModifyCustomSearchFilter;
import org.nuclos.client.customcomp.resplan.ResPlanPanel.NewCustomSearchFilter;
import org.nuclos.client.customcomp.wizard.CustomComponentWizardModel;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.scripting.CompiledGroovy;
import org.nuclos.client.scripting.GroovySupport;
import org.nuclos.client.scripting.InvocableMethod;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.EntitySearchFilters;
import org.nuclos.client.ui.AskAndSaveResult;
import org.nuclos.client.ui.DefaultSelectObjectsPanel;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.SelectObjectsController;
import org.nuclos.client.ui.SelectObjectsPanel;
import org.nuclos.client.ui.collect.CalcAttributeClientHelper;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateModel;
import org.nuclos.client.ui.model.ChoiceList;
import org.nuclos.client.ui.model.MutableListModel;
import org.nuclos.client.ui.renderer.TopTableCellRendererDelegate;
import org.nuclos.client.ui.resplan.JResPlanComponent.CellView;
import org.nuclos.client.ui.resplan.JResPlanComponent.JMilestone;
import org.nuclos.client.ui.resplan.JResPlanComponent.JRelation;
import org.nuclos.client.ui.resplan.TimeModel;
import org.nuclos.client.ui.resplan.header.GroupMapper;
import org.nuclos.client.ui.resplan.header.JHeaderGrid;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.ui.util.Orientation;
import org.nuclos.client.ui.util.PainterUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common.customcomp.resplan.PlanElementLocaleVO;
import org.nuclos.common.customcomp.resplan.ResourceLocaleVO;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.preferences.PlanningTablePreferences;
import org.nuclos.common.preferences.PlanningTablePreferencesManager;
import org.nuclos.common.preferences.PreferencesProvider;
import org.nuclos.common.time.LocalTime;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.interval.GranularityType;
import org.nuclos.common2.interval.Interval;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.thoughtworks.xstream.XStream;

public class ResPlanController<PK, R extends Collectable<PK>> extends CustomComponentController {

	private static final Logger LOG = Logger.getLogger(ResPlanController.class);
	
	private static final int DEFAULT_RESOURCE_LIMIT = 256;
	
	//
	
	private final GroovySupport groovySupport = GroovySupport.getInstance();
	
	private CompiledGroovy compiledGroovy = null;

	private final PlanningTablePreferencesManager planningTablePreferencesManager;

	private ClientResPlanConfigVO<PK,R,Collectable<PK>> configVO;
	private ResPlanPanel<PK,R> resPlanPanel;
	private CollectableResPlanModel<PK,R> resPlanModel;
	private BackgroundPainter<PK> backgroundPainter;

	public ResPlanController(CustomComponentVO componentVO, MainFrameTab tabIfAny, boolean editable) {
		super(componentVO.getPrimaryKey(), componentVO.getInternalName(), tabIfAny);
		planningTablePreferencesManager = PreferencesProvider.getInstance().getPlanningTablePreferencesManager(componentVO.getPrimaryKey(), SpringApplicationContextHolder.getBean(SecurityCache.class).getUsername());
		init(componentVO, editable);
	}
	
	@Override
	public void askAndSaveIfNecessary(ResultListener<AskAndSaveResult> rl) {
		rl.done(AskAndSaveResult.NOT_SAVED);
	}

	void init(CustomComponentVO vo, boolean editable) {
		final Jaxb2Marshaller marshaller = SpringApplicationContextHolder.getBean(Jaxb2Marshaller.class);
		configVO = ClientResPlanConfigVO.fromBytes(vo.getData(), marshaller);
		LocaleInfo localeInfo = SpringLocaleDelegate.getInstance().getUserLocaleInfo();
		ResourceLocaleVO resourceLocaleVO = configVO.getResourceLocale(localeInfo);

		CollectableLabelProvider resourceLabelProvider = new CollectableLabelProvider();
		resourceLabelProvider.setLabelTemplate(restoreUserResourceLabel());
		resourceLabelProvider.setToolTipTemplate(resourceLocaleVO.getResourceTooltip());
		
		JXLabel captionLabel = new JXLabel();
		captionLabel.setVerticalAlignment(JLabel.TOP);
		captionLabel.setText(restoreUserLegendLabel());
		captionLabel.setToolTipText(StringUtils.nullIfEmpty(resourceLocaleVO.getLegendTooltip()));
		captionLabel.setBackgroundPainter(new PainterUtils.HeaderPainter());
		JPanel captionPanel = new JPanel(new BorderLayout());
		captionPanel.add(captionLabel);
		captionPanel.setBackground(new Color(163, 172, 187));
		captionPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 1, 1));
		
		JPopupMenu captionMenu = new JPopupMenu();
		captionMenu.add(new AbstractAction(SpringLocaleDelegate.getInstance().getResource("nuclos.resplan.caption.menu.1", "Legende anpassen...")) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getTab() instanceof MainFrameTab) {
					cmdConfigureResourceHeader((MainFrameTab) getTab());
				}
			}
		});
		captionMenu.add(new AbstractAction(SpringLocaleDelegate.getInstance().getResource("nuclos.resplan.caption.menu.2", "Sortierung anpassen...")) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (getTab() instanceof MainFrameTab) {
					cmdConfigureResourceSortOrder((MainFrameTab) getTab());
				}
			}
		});
		captionPanel.setComponentPopupMenu(captionMenu);

		backgroundPainter = new BackgroundPainter<PK>();
		Map<Date, List<String>> mpHolidays = null;
		
		if (configVO.getWithHolidays()) {
			if (configVO.getHolidaysEntity() != null && configVO.getHolidaysDate() != null) {
				mpHolidays = new HashMap<>();
				for (MasterDataVO<?> mdvo : MasterDataDelegate.getInstance().getMasterData(configVO.getHolidaysEntity())) {
					if (!mpHolidays.containsKey((Date) mdvo.getFieldValue(configVO.getHolidaysDate()))) {
						mpHolidays.put((Date) mdvo.getFieldValue(configVO.getHolidaysDate()), new ArrayList<String>());
					}
					List<String> lstTooltips = mpHolidays.get((Date) mdvo.getFieldValue(configVO.getHolidaysDate()));
					String sName = "<b>" + (String) mdvo.getFieldValue(configVO.getHolidaysName()) + "</b>";
					String sTip = (String) mdvo.getFieldValue(configVO.getHolidaysTip());
					lstTooltips.add((sName != null ? sName : "") + (sName != null && sTip != null ? "<br>" : "") + (sTip != null ? sTip : ""));
				}
				backgroundPainter.setHolidays(mpHolidays);
			}
		}
		if (configVO.isScriptingActivated()) {
			String code = configVO.getScriptingCode();
			try {
				if (code != null && !code.trim().isEmpty()) {
					compiledGroovy = groovySupport.compile(code);
					final InvocableMethod cellMethod = compiledGroovy.getInvocable(
							configVO.getScriptingResourceCellMethod(), CollectableLabelProvider.SCRIPTING_SIGNATURE);
					final InvocableMethod backgroundMethod = compiledGroovy.getInvocable(
							configVO.getScriptingBackgroundPaintMethod(), BackgroundPainter.SCRIPTING_SIGNATURE);

					resourceLabelProvider.setGroovyMethod(cellMethod);
//TODO We switch off Scripting for the entries for now.
//					entryLabelProvider.setGroovyMethod(support.getInvocable(
//					pEntry.getScriptingEntryCellMethod(), CollectableLabelProvider.SCRIPTING_SIGNATURE));
					backgroundPainter.setGroovyMethod(backgroundMethod);
				} else {
//					String entryLabelCode = rpEntry.getScriptingEntryCellMethod();
//					if (entryLabelCode != null) {
//						JOptionPane.showMessageDialog(Main.getInstance().getMainFrame(), "Kein Code hinterlegt, angegebene Regeln werden ignoriert");
//					}
				}
			} catch (Exception ex) {
				Errors.getInstance().showExceptionDialog(Main.getInstance().getMainFrame(), "Fehler beim Initialisieren des Skripting", ex);
			}
		}

		resPlanModel = new CollectableResPlanModel<PK,R>(this);

		DateTimeModel dateTimeModel = new DateTimeModel(configVO.getParsedTimePeriods());

		resPlanPanel = new ResPlanPanel<PK,R>(this, resPlanModel, dateTimeModel, mpHolidays, editable);

		try {
			migrate();
		} catch (BackingStoreException e) {
			handleSpecialException(e);
		}
		PlanningTablePreferences prefs = planningTablePreferencesManager.getSelected(false);

		resPlanPanel.setResourceRenderer(resourceLabelProvider);
		resPlanPanel.setBackgroundPainter(backgroundPainter);
		resPlanPanel.setCaptionComponent(captionPanel);
		resPlanPanel.setOwnBookings(prefs.isOwnBooking());
		resPlanPanel.setSearchFilter(prefs.getSearchFilter());
		if (resPlanPanel.getSearchFilter() == null) {
			String searchCondition = prefs.getSearchcondition();
			if (!StringUtils.isNullOrEmpty(searchCondition)) {
				CollectableSearchCondition sc = searchconditionFromXML(searchCondition);
				resPlanPanel.setCustomSearchFilter(sc);
			}
		}
		
//Start of the multiple Planelements.
		
		Map<ClientPlanElement<PK,R,Collectable<PK>>, ComponentProvider<?>> mapLabelProvider = 
				new HashMap<ClientPlanElement<PK,R,Collectable<PK>>, ComponentProvider<?>>();
		for (ClientPlanElement<PK,R,Collectable<PK>> rpEntry : configVO.getListEntriesOrMileStones(false)) {
			CollectableLabelProvider entryLabelProvider = new CollectableLabelProvider();
			if (compiledGroovy != null) {
				try {
					final InvocableMethod cellMethod = compiledGroovy.getInvocable(
							rpEntry.getScriptingEntryCellMethod(), CollectableLabelProvider.SCRIPTING_SIGNATURE);
					entryLabelProvider.setGroovyMethod(cellMethod);
				} catch (Exception e) {
					LOG.error("Groovy error: " + e, e);
				}
			}
			PlanElementLocaleVO planElementLocaleVO = rpEntry.getPlanElementLocale(localeInfo);
			entryLabelProvider.setLabelTemplate(planElementLocaleVO.getBookingLabel());
			entryLabelProvider.setToolTipTemplate(planElementLocaleVO.getBookingTooltip());
			mapLabelProvider.put(rpEntry, (ComponentProvider<?>)entryLabelProvider);
		}
		resPlanModel.setMapLabelProvider(mapLabelProvider);
		
		Map<ClientPlanElement<PK,R,R>, JMilestone> mapMileStoneRenderer = new HashMap<ClientPlanElement<PK,R,R>, JMilestone>();
		for (ClientPlanElement pElem : configVO.getListEntriesOrMileStones(true)) {
			if (pElem.isMileStone()) {
				JMilestone mileStoneRenderer = new JMilestone();
				Color c = GraphicElements.decodeColor(pElem.getColor());
				if (c != null) mileStoneRenderer.setBaseColor(c);
				ImageIcon i = GraphicElements.getIconFromResourceFile(UID.parseUID(pElem.getFromIcon()));
				if (i != null) mileStoneRenderer.setIcon(i);
				mapMileStoneRenderer.put(pElem, mileStoneRenderer);
			}
		}
		resPlanModel.setMapMileStoneRenderer(mapMileStoneRenderer);

		Map<ClientPlanElement<PK,R,R>, JRelation> mapRelationRenderer = new HashMap<ClientPlanElement<PK,R,R>, JRelation>();
		ClientPlanElement rpRelation = configVO.getFirstRelation();
		if (rpRelation != null) {
			resPlanPanel.setRelationPresentation(rpRelation.getPresentation());
			resPlanPanel.setRelationFromPresentation(rpRelation.getFromPresentation());
			resPlanPanel.setRelationToPresentation(rpRelation.getToPresentation());
			JRelation relationRenderer = new JRelation();
			String sColor = rpRelation.getColor();
			Color c = GraphicElements.decodeColor(sColor);
			if (c != null) relationRenderer.setBaseColor(c);
			mapRelationRenderer.put(rpRelation, relationRenderer);
		}
		resPlanModel.setRelationRenderer(mapRelationRenderer);
	}

	public void setSearchCondition(CollectableSearchCondition newSearchCondition) {
		resPlanPanel.setSearchCondition(newSearchCondition);
	}

	/**
	 * {@inheritDoc}.
	 * Additionally to this method, the {@link ResPlanController} supports the alternative
	 * entry points {@link #runWith(Collection)} and {@link #runWith(CollectableSearchCondition)}.
	 * @see #runWith(Collection)
	 * @see #runWith(CollectableSearchCondition)
	 */
	@Override
	public void run() {
		resPlanPanel.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				panelPropertyChanged(evt);
			}
		});
		refresh(false);

		super.run();
	}
	
	private CollectController<?,?> entController;
	private Collection<CollectableEntityObject<PK>> dataSet;
	
	public void setEntController(CollectController<?,?> ec, Collection<CollectableEntityObject<PK>> dataSet) {
		this.entController = ec;
		this.dataSet = dataSet;
	}
	
	@Override
	public <PK2> void runCommandWithSpecialHandler(CommonRunnable runnable, Collectable<PK2> clct) {
		try {
			super.runCommandWithSpecialHandler(runnable, clct);
			if (entController != null) try {
				CollectState cs = entController.getCollectState();
				entController.setCollectState(cs.getOuterState(), CollectStateModel.DETAILSMODE_EDIT);
				Set<EntityObjectVO<PK>> setStuff = new HashSet<EntityObjectVO<PK>>();
				if (dataSet != null) {
					for (CollectableEntityObject<PK> ceo : dataSet) {
						if (clct.getId().equals(ceo.getId())) {
							EntityObjectVO<PK> eo = ceo.getEntityObjectVO();
							if (eo.isComplete()) {
								setStuff.add(eo);
							}
							eo.setComplete(false);
						}
					}
				}
				entController.save();
				for (EntityObjectVO<PK> eo : setStuff) {
					eo.setComplete(true);
				}
			} catch (CommonBusinessException e) {
				LOG.warn("runCommandWithSpecialHandler failed: " + e, e);
			}
		} catch (Exception ex) {
			boolean expectionHandled = handleSpecialException(ex, clct);
			if (!expectionHandled) {
				Errors.getInstance().showExceptionDialog(getComponent(), ex);
			}
		}
	}

	/**
	 * Alternative entry point which starts this controller with the specified ressources.
	 * @param ids resource ids
	 * @see #run()
	 */
	public void runWith(Collection<Object> ids) {
		resPlanPanel.setCustomSearchFilter(new CollectableIdListCondition(new ArrayList<Object>(ids)));
		run();
	}

	/**
	 * Alternative entry point which starts this controller with the ressources specified
	 * by the given search condition.
	 * @param searchCondition resource search condition
	 */
	public void runWith(CollectableSearchCondition searchCondition) {
		resPlanPanel.setCustomSearchFilter(searchCondition);
		run();
	}
	
	public void executeBusinessRules(Collectable<PK> clct, List<EventSupportSourceVO> rules) throws CommonBusinessException {
		EntityObjectVO<PK> eo = EntityObjectDelegate.getInstance().get(clct.getEntityUID(), clct.getPrimaryKey());
		EntityObjectDelegate.getInstance().executeBusinessRules(rules, eo, null, false);
	}

	private void panelPropertyChanged(PropertyChangeEvent evt) {
		if (ResPlanPanel.TIME_HORIZON_PROPERTY.equals(evt.getPropertyName())) {
			refresh(true);
		} else if (ResPlanPanel.SEARCH_CONDITION_PROPERTY.equals(evt.getPropertyName())) {
			saveSearchFilter();
			refresh(false);
		} else if (resPlanPanel.OWN_BOOKINGS_PROPERTY.equals(evt.getPropertyName())) {
			saveOwnBookingsState();
			refresh(false);
		}
	}

	ClientResPlanConfigVO getConfigVO() {
		return configVO;
	}
	
	List<TimeGranularity> getTimeGranularityOptions() {
		List<TimeGranularity> options = new ArrayList<TimeGranularity>();
		List<Pair<LocalTime, LocalTime>> timePeriods = configVO.getParsedTimePeriods();
		if (timePeriods != null && !timePeriods.isEmpty()) {
			options.add(new TimeGranularity(GranularityType.TIME, new DateTimeModel(timePeriods), configVO.getDayOfWeek(), backgroundPainter));
		}
		options.add(new TimeGranularity(GranularityType.DAY, new DateTimeModel(), configVO.getDayOfWeek(), backgroundPainter));
		options.add(new TimeGranularity(GranularityType.WEEK, new WeekModel(), configVO.getDayOfWeek(), backgroundPainter));
		options.add(new TimeGranularity(GranularityType.MONTH, new MonthModel(), configVO.getDayOfWeek(), backgroundPainter));
		options.add(new TimeGranularity(GranularityType.QUARTER, new QuarterModel(), configVO.getDayOfWeek(), backgroundPainter));
		return options;
	}

	private CollectableSearchExpression getInternalResourceSearchExpression() {
		List<CollectableSorting> sorting = new ArrayList<CollectableSorting>();
		Pair<List<CollectableEntityField>, Set<CollectableEntityField>> sortOrder = restoreResourceSortOrder();
		for (CollectableEntityField clctef : sortOrder.getX()) {
			if (clctef != null) {
				sorting.add(new CollectableSorting(SystemFields.BASE_ALIAS, configVO.getResourceEntity(), true, clctef.getUID(), sortOrder.getY().contains(clctef) ? false : true));
			}
		}
//		if (configVO.getResourceSortField() != null) {
//			sorting.add(new CollectableSorting(SystemFields.BASE_ALIAS, configVO.getResourceEntity(), true, configVO.getResourceSortField(), true));
//		}
		return new CollectableSearchExpression(resPlanPanel.getSearchCondition(), sorting);
	}
	
	private CollectableSearchCondition getInternalEntrySearchCondition(Interval<Date> interval, boolean ownBookings, ClientPlanElement<PK,R,Collectable<PK>> rpEntry) {
		CollectableHelper<PK,R,?> collHelper = rpEntry.getCollHelper();
		CollectableEntityField dateFromField = collHelper.getCollectableEntity().getEntityField(rpEntry.getDateFromField());
		CollectableEntityField dateUntilField = rpEntry.isMileStone() ? dateFromField 
				: collHelper.getCollectableEntity().getEntityField(rpEntry.getDateUntilField());
		CollectableEntityField bookerField = rpEntry.getBookerField() != null ?
				collHelper.getCollectableEntity().getEntityField(rpEntry.getBookerField()) : null;
		//CollectableEntityObjectField
				
		CompositeCollectableSearchCondition searchCondition = new CompositeCollectableSearchCondition(LogicalOperator.AND, Arrays.asList(
				new CollectableComparison(dateFromField, ComparisonOperator.LESS_OR_EQUAL, new CollectableValueField(interval.getEnd())),
				new CollectableComparison(dateUntilField, ComparisonOperator.GREATER_OR_EQUAL, new CollectableValueField(interval.getStart()))));
		if (bookerField != null && ownBookings) {
			searchCondition.addOperand(new CollectableInIdCondition<UID>(bookerField, Arrays.asList(SecurityDelegate.getInstance().getUserUid(Main.getInstance().getMainController().getUserName()))));
//			searchCondition.addOperand(new CollectableComparison(bookerField, ComparisonOperator.EQUAL, 
//					new CollectableValueField(SecurityDelegate.getInstance().getUserUid(Main.getInstance().getMainController().getUserName()))));
		}
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(rpEntry.getEntity());
		if (eMeta.isStateModel()) {
			searchCondition.addOperand(new CollectableComparison(new CollectableEOEntityField(MetaProvider.getInstance().getEntityField(SF.LOGICALDELETED.getUID(eMeta))), ComparisonOperator.EQUAL, new CollectableValueField(Boolean.FALSE)));
		}
		return searchCondition;
	}
	
	private CollectableSearchCondition getRelationSearchCondition(CollectableSearchCondition entrySearch, PlanElement<R> rpEntry, PlanElement<R> rpRelation) {
		return new CollectableSubCondition(rpRelation.getPrimaryField(), rpEntry.getEntity(), null, entrySearch);
	}

	public void refresh(boolean timeHorizonChanged) {
		execute(new RefreshTask(true, timeHorizonChanged));
	}
	
	@Override
	public void close() {
	}

	@Override
	public JComponent getComponent() {
		return resPlanPanel;
	}

	@Override
	protected void storeSharedState() throws PreferencesException, CommonFinderException, CommonPermissionException {
		if (resPlanPanel == null) {
			return;
		}
		super.storeSharedState();

		PlanningTablePreferences p = planningTablePreferencesManager.getSelected(false);

		p.setGranularity(resPlanPanel.getTimeGranularity());
		resPlanPanel.storeViewPreferences(p, null);
		planningTablePreferencesManager.update(p);
	}

	@Override
	protected void restoreSharedState() throws PreferencesException, BackingStoreException, CommonFinderException, CommonPermissionException {
		super.restoreSharedState();

		PlanningTablePreferences p = planningTablePreferencesManager.getSelected(false);

		GranularityType granularity = p.getGranularity();
		if (granularity != null) {
			resPlanPanel.setTimeGranularity(granularity);
		}
		resPlanPanel.restoreViewPreferences(p, null);
	}

	private void migrate() throws BackingStoreException {
		boolean migrate = true;
		Preferences userPrefs = getPreferences();
		if (userPrefs.nodeExists("default")) {
			PlanningTablePreferences p = planningTablePreferencesManager.getSelected(true);
			Preferences defaultPrefs = userPrefs.node("default");
			//ask to migrate if new preferences already exists
			if (p != null) {
				int option = JOptionPane.showConfirmDialog(getComponent(), SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class).getMessage("nuclos.resplan.migratepreferences.message","Es existieren bereits Plantafeleinstellungen aus dem Webclient." +
								"Möchten Sie diese mit Ihren Einstellungen aus dem Desktopclient überschreiben?"),
						SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class).getMessage("nuclos.resplan.migratepreferences.title","Einstellungen übernehmen?"), JOptionPane.YES_NO_CANCEL_OPTION);
				switch (option) {
					case JOptionPane.NO_OPTION:
						migrate = false;
						defaultPrefs.removeNode();
						defaultPrefs.flush();
						break;
					case JOptionPane.CANCEL_OPTION:
						migrate = false;
				}
			}
			if (migrate) {
				defaultPrefs.putInt("subsearchfiltercount", userPrefs.getInt("subsearchfiltercount", 0));
				for (int i = 0; i < userPrefs.getInt("subsearchfiltercount",0); i++) {
					defaultPrefs.put("subsearchfilter" + i, userPrefs.get("subsearchfilter" + i, ""));
					userPrefs.remove("subsearchfilter" + i);
				}
				defaultPrefs.putInt("subsearchconditioncount", userPrefs.getInt("subsearchconditioncount", 0));
				for (int i = 0; i < userPrefs.getInt("subsearchfiltercount",0); i++) {
					defaultPrefs.put("subsearchcondition" + i, userPrefs.get("subsearchcondition" + i, ""));
					userPrefs.remove("subsearchcondition" + i);
				}
				userPrefs.remove("subsearchfiltercount");
				userPrefs.remove("subsearchconditioncount");
				userPrefs.flush();

				defaultPrefs.putBoolean("ownbooking", userPrefs.getBoolean("ownbooking", false));
				defaultPrefs.flush();
				boolean bNew = p == null;
				p = planningTablePreferencesManager.migrate(defaultPrefs, p);
				if (bNew) {
					planningTablePreferencesManager.insert(p);
				} else {
					planningTablePreferencesManager.update(p);
				}
				defaultPrefs.removeNode();
				defaultPrefs.flush();
			}
		}
	}

	@Override
	public boolean isRestoreTab() {
		return true;
	}
	
	public void removeSearchFilter() {
		if (resPlanPanel != null) resPlanPanel.removeSearchFilter();
	}
	
	private void saveSearchFilter() {
		PlanningTablePreferences prefs = planningTablePreferencesManager.getSelected(false);
		String filter = resPlanPanel.getSearchFilter();
		if (filter != null) {
			prefs.setSearchcondition(null);
			prefs.setSearchFilter(filter);
		} else {
			prefs.setSearchFilter(null);
			CollectableSearchCondition sc = resPlanPanel.getSearchCondition();
			if (sc != null) {
				//user defined filter, not saved
				String strSc = toXML(sc);
				prefs.setSearchcondition(strSc);
			} else {
				prefs.setSearchcondition(null);
			}
		}
		planningTablePreferencesManager.update(prefs);
	}
	
	private void saveOwnBookingsState() {
		PlanningTablePreferences prefs = planningTablePreferencesManager.getSelected(false);
		prefs.setOwnBooking(resPlanPanel.getOwnBookings());
		planningTablePreferencesManager.update(prefs);
	}

	public void removeDateChooser() {
		if (resPlanPanel != null) resPlanPanel.removeDataChooser();
	}
	
	public void adjustWidth() {
		if (resPlanPanel != null) resPlanPanel.adjustWidthAndHeight();
	}
	
	public void setTimeHorizon(Interval<Date> newTimeHorizon, boolean notify) {
		if (resPlanPanel != null) resPlanPanel.setTimeHorizon(newTimeHorizon, notify);
	}

	public void setOrientation(Orientation orientation) {
		if (resPlanPanel != null) resPlanPanel.setOrientation(orientation);
	}

	@Override
	protected String storeInstanceStateToXML() {
		RestorePreferences rp = new RestorePreferences();
		Interval<Date> timeHorizon = resPlanPanel.getTimeHorizon();
		String searchFilter = resPlanPanel.getSearchFilter();
		CollectableSearchCondition searchCondition = resPlanPanel.getSearchCondition();

		rp.granularity = resPlanPanel.getTimeGranularity().getValue();
		rp.startDate =  String.format("%tF", timeHorizon.getStart());
		rp.endDate = String.format("%tF", timeHorizon.getEnd());
		rp.searchFilter = searchFilter;
		rp.searchCondition = searchCondition;
		rp.viewRect = resPlanPanel.getViewRect();
		rp.ownBooking = resPlanPanel.getOwnBookings();

		try {
			resPlanPanel.storeViewPreferences(null, rp);
		} catch(PreferencesException e) {
			LOG.warn("storeInstanceStateToXML failed: " + e);
		}

		return toXML(rp);
	}

	@Override
	protected void restoreInstanceStateFromXML(String xml) {
		RestorePreferences rp = fromXML(xml);

		GranularityType granularity = KeyEnum.Utils.findEnum(GranularityType.class, rp.granularity);
		if (granularity != null) {
			resPlanPanel.setTimeGranularity(granularity);
		}

		if (resPlanPanel != null && rp.startDate != null && rp.endDate != null && configVO.getSpecialViewFromField() == null && configVO.getSpecialViewUntilField() == null) {
			Date startDate = java.sql.Date.valueOf(rp.startDate);
			Date endDate = java.sql.Date.valueOf(rp.endDate);
			resPlanPanel.setTimeHorizon(new Interval<Date>(startDate, endDate, true), true);
		}

		if (rp.searchFilter == null && rp.searchCondition != null) {
			resPlanPanel.setCustomSearchFilter(rp.searchCondition);
		} else {
			resPlanPanel.setSearchFilter(rp.searchFilter);
		}
		if (rp.ownBooking != null) {
			resPlanPanel.setOwnBookings(rp.ownBooking);
		}
		try {
			resPlanPanel.restoreViewPreferences(null, rp);
		} catch(PreferencesException e) {
			LOG.warn("restoreInstanceStateFromXML failed: " + e);
		}
	}

	/**
	 *
	 *
	 */
	public static class RestorePreferences implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		String granularity;
		String startDate;
		String endDate;
		String searchFilter;
		Rectangle viewRect;
		CollectableSearchCondition searchCondition;
		Boolean ownBooking;

		int orientation;
		final Map<String, Integer> resourceCellExtent = new HashMap<String, Integer>();
		final Map<String, Integer> timelineCellExtent = new HashMap<String, Integer>();
		int resourceHeaderExtent;
		int timelineHeaderExtent;
	}

	private static String toXML(RestorePreferences rp) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(rp);
		}
	}

	private static String toXML(CollectableSearchCondition sc) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(sc);
		}
	}

	private static RestorePreferences fromXML(String xml) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return (RestorePreferences) xstream.fromXML(xml);
		}
	}

	private static CollectableSearchCondition searchconditionFromXML(String xml) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return (CollectableSearchCondition) xstream.fromXML(xml);
		}
	}

//	@Override
	protected boolean handleSpecialException(Exception ex) {
		return handleSpecialException(ex, null);
	}
	
	protected <PK2> boolean handleSpecialException(Exception ex, Collectable<PK2> clct) {
		NuclosBusinessRuleException.extractNuclosBusinessRuleExceptionIfAny(ex);
		if (ex != null) {
			final String exceptionMessage;
			if (ex instanceof NuclosBusinessRuleException) {
				final NuclosBusinessRuleException nbre = (NuclosBusinessRuleException) ex;
				final BusinessException be = nbre.getCausingBusinessException();
				if (be != null) {
					exceptionMessage = be.getMessage();
				} else {
					exceptionMessage = nbre.getMessage();
				}
			} else {
				exceptionMessage = NuclosExceptions.getReasonableMessage(ex);
			}
			if (exceptionMessage != null) {
				log.error(exceptionMessage, ex);
				List<String> messages = new ArrayList<>(Arrays.asList(Errors.formatErrorMessage(exceptionMessage)));
				resPlanPanel.setInfoMessages(messages, true);
				if (clct != null) {
					CellView cellView = resPlanPanel.getResPlan().findCellView((Collectable<PK>) clct, false);
					resPlanPanel.getResPlan().setExceptionalCellView(cellView);
				}
				resPlanPanel.repaint();
				return true;
			}
		}
//		return super.handleSpecialException(ex);
		return false;
	}

	class RefreshTask extends BackgroundTask {

		final int resourceLimit;
		final boolean showMessage;
		final CollectableSearchExpression resourceSearchExpr;
		final boolean timeHorizonChanged;

		volatile List<R> resources;
		private volatile Map<ClientPlanElement<PK,R,Collectable<PK>>, List<? extends Collectable<PK>>> mapEntries = 
				new HashMap<ClientPlanElement<PK,R,Collectable<PK>>, List<? extends Collectable<PK>>>();

//		volatile List<? extends Collectable> entries;
		volatile List<R> relations;
		volatile boolean truncated;
		final private Map<ClientPlanElement<PK,R,Collectable<PK>>, CollectableSearchCondition> mapEntryCond = 
				new HashMap<ClientPlanElement<PK,R,Collectable<PK>>, CollectableSearchCondition>();

		List<EntitySearchFilter> searchFilters;

		public RefreshTask(boolean show, boolean timeHorizonChanged) {
			this.timeHorizonChanged = timeHorizonChanged;
			resourceLimit = getResourceLimit();
			showMessage = show;
			resourceSearchExpr = getInternalResourceSearchExpression();
			for (ClientPlanElement<PK,R,Collectable<PK>> peEntry : configVO.getListEntriesOrMileStones(true)) {
				CollectableSearchCondition csc = getInternalEntrySearchCondition(resPlanPanel.getTimeHorizon(), resPlanPanel.getOwnBookings(), peEntry);
				mapEntryCond.put(peEntry, csc);
			}
		}

		@Override
		public void doInBackground() throws CommonBusinessException {
			CollectableHelper<PK,?,?> collHelperRes = configVO.getCollHelper();
			
			Long count = collHelperRes.getCount(resourceSearchExpr);
			resPlanPanel.setNumberOfResources(count.intValue());
			
			Long limitSave = resourceSearchExpr.getLimit();
			if (count > resourceLimit) {
				resourceSearchExpr.setLimit(Long.valueOf(resourceLimit));
				truncated = true;
			}
			
			List<PK> resourceIds = collHelperRes.getIds(resourceSearchExpr);
			if (resourceIds.size() > resourceLimit) {
				resourceIds.subList(resourceLimit, resourceIds.size()).clear();
			}
			resourceSearchExpr.setLimit(limitSave);
			resources = (List<R>) collHelperRes.get(resourceIds);
			
			if (!timeHorizonChanged) {
				Date datFrom = null;
				Date datUntil = null;
				Date defaultFrom = resPlanModel.getDefaultViewFrom();
				Date defaultUntil = resPlanModel.getDefaultViewUntil();
				if (configVO.getSpecialViewFromField() != null || configVO.getSpecialViewUntilField() != null) {
					for (R resource : resources) {
						try {
							CollectableField cltfFrom = configVO.getSpecialViewFromField() != null ? resource.getField(configVO.getSpecialViewFromField()) : null;
							CollectableField cltfUntil = configVO.getSpecialViewUntilField() != null ? resource.getField(configVO.getSpecialViewUntilField()) : null;
							if (cltfFrom != null && cltfFrom.getValue() instanceof Date) {
								Date datCurrent = (Date) cltfFrom.getValue();
								if (datFrom == null || datFrom.after(datCurrent)) {
									datFrom = datCurrent;
								}
							}
							if (cltfUntil != null && cltfUntil.getValue() instanceof Date) {
								Date datCurrent = (Date) cltfUntil.getValue();
								if (datUntil == null || datUntil.before(datCurrent)) {
									datUntil = datCurrent;
								}
							}
						} catch (CommonFatalException e) {
							log.error(String.format("field with UID %s not found in resource (%s).", configVO.getSpecialViewFromField(), resource.getEntityUID()), e);
						}
					}
				}
				if (datFrom != null && configVO.getSpecialViewFrom() != null) {
					datFrom = DateUtils.calc(datFrom, configVO.getSpecialViewFrom());
				}
				if (datUntil != null && configVO.getSpecialViewUntil() != null) {
					datUntil = DateUtils.calc(datUntil, configVO.getSpecialViewUntil());
				}

				Date date1 = datFrom == null || (datUntil != null && datFrom.after(datUntil)) ? defaultFrom : datFrom;
				Date date2 = datUntil == null || (datFrom != null && datUntil.before(datFrom)) ? defaultUntil : datUntil;

				Interval<Date> timeHorizon = new Interval<>(date1, date2);
				if (!resPlanPanel.getTimeHorizon().equals(timeHorizon)) {
					setTimeHorizon(timeHorizon, false);
					for (ClientPlanElement<PK,R,Collectable<PK>> peEntry : configVO.getListEntriesOrMileStones(true)) {
						CollectableSearchCondition csc = getInternalEntrySearchCondition(resPlanPanel.getTimeHorizon(), resPlanPanel.getOwnBookings(), peEntry);
						mapEntryCond.put(peEntry, csc);
					}
				}
			}
			
			for (ClientPlanElement<PK,R,Collectable<PK>> peEntry : configVO.getListEntriesOrMileStones(true)) {
				if((peEntry.getType() == PlanElement.ENTRY) && (peEntry.getBookerField() != null)) {
					resPlanPanel.showOwnBookingsCheckbox(true);
				}
				CollectableSearchCondition csc = mapEntryCond.get(peEntry);
				CollectableHelper<PK,R,Collectable<PK>> collHelperEntry = peEntry.getCollHelper();
				csc = new CompositeCollectableSearchCondition(LogicalOperator.AND, csc, new CollectableInIdCondition<>(collHelperEntry.getCollectableEntity().getEntityField(peEntry.getPrimaryField()), resourceIds));
				List<PK> ids = collHelperEntry.getIds(csc);
				List<? extends Collectable<PK>> lstEntries = (List<? extends Collectable<PK>>) collHelperEntry.get(ids);
				mapEntries.put(peEntry, lstEntries);
			}
			
			searchFilters = getSearchFilters();
			
			ClientPlanElement<PK,R,Collectable<PK>> rpEntry = configVO.getFirstEntry(); //TODO
			ClientPlanElement<PK,R,Collectable<PK>> rpRelation = configVO.getFirstRelation(); //TODO
			if (rpRelation != null) {
				CollectableHelper<PK,R,Collectable<PK>> collHelperRelation = rpRelation.getCollHelper();
				CollectableSearchCondition csc = getInternalEntrySearchCondition(resPlanPanel.getTimeHorizon(), resPlanPanel.getOwnBookings(), rpEntry);
				List<PK> ids =  collHelperRelation.getIds(getRelationSearchCondition(csc, rpEntry, rpRelation));
				relations = (List<R>) collHelperRelation.get(ids);
			}
		}

		@Override
		public void done() throws CommonBusinessException {
			resPlanPanel.refreshSearchFilter(searchFilters);
			List<String> messages = new ArrayList<String>();
			//TODO nur temporärer Fix, Reihenfolge von doInBackground() und done() wird offenbar nicht eingehalten
			if (resources == null) {
				doInBackground();
			}
			resPlanModel.setData(resources, mapEntries, relations);
			for (ClientPlanElement<PK,R,Collectable<PK>> peEntry : mapEntries.keySet()) {
				List<? extends Collectable<PK>> lstEntries = mapEntries.get(peEntry);
				for (Iterator<? extends Collectable<PK>> iter = lstEntries.iterator(); iter.hasNext(); ) {
					Collectable<PK> entry = iter.next();
					if (!resPlanModel.isCollectableEntryValid(entry, peEntry)) {
						// if entry is invalid, remove it from the collection and add an error message instead
						iter.remove();
					messages.add(getSpringLocaleDelegate().getMessage("nuclos.resplan.invalidEntry", null,
							SpringLocaleDelegate.getInstance().getIdentifierLabel(
									entry, entry.getEntityUID(), MetaProvider.getInstance())));
					}
				}				
			}
			if (truncated) {
				messages.add(getSpringLocaleDelegate().getMessage("nuclos.resplan.limitSearchResult", null, resourceLimit));
			}
			if (messages.size() > 0) {
				resPlanPanel.setInfoMessages(messages, showMessage);
			} else {
				resPlanPanel.setInfoMessages(null, false);
			}			
			calcAndAdjustHeight(resources);
			resPlanPanel.restoreViewPreferences(planningTablePreferencesManager.getSelected(false), null);
			resPlanPanel.getResPlan().getResourceHeader().firePropertyChange(JHeaderGrid.HEADER_EXTENT_PROPERTY, 0, resPlanPanel.getResPlan().getResourceHeader().getHeaderExtent());
		}
	}
	
	public void calcAndAdjustHeight(List<R> resources) {
		try {
			if (resources == null) {
				CollectableHelper<PK,R,Collectable<PK>> collHelperRes = configVO.getCollHelper();
				List<PK> resourceIds = collHelperRes.getIds(getInternalResourceSearchExpression());
				int resourceLimit = getResourceLimit();
				if (resourceIds.size() > resourceLimit) {
					resourceIds.subList(resourceLimit, resourceIds.size()).clear();
				}
				resources = (List<R>) collHelperRes.get(resourceIds);
			}

			int fixedExtendCascade = 0;
			int fixedExtendNonCascade = 0;
			for (R res : resources) {
				List<EntryWPElem<R>> lstWPEl = resPlanModel.getWPElements(res);
				for (EntryWPElem<R> wpElem : lstWPEl) {
					PlanElement<R> pElem = wpElem.getPlanElement();
					int f1 = pElem.getFixedExtend();
					if (pElem.isCascade()) {
						fixedExtendCascade += f1 + 2;
					} else {
						if (f1 > fixedExtendNonCascade + 2) fixedExtendNonCascade = f1 + 2;
					}
				}
			}
			int fixedExtend = fixedExtendCascade + fixedExtendNonCascade;
			if (fixedExtend == 0) fixedExtend = 60;
			resPlanPanel.adjustHeight(fixedExtend);
		} catch (Exception e) {
			LOG.warn("calcAndAdjustHeight failed: " + e, e);
		}
	}

	public int getResourceLimit() {
		return Math.max(1, ClientParameterProvider.getInstance().getIntValue(
			ParameterProvider.KEY_RESPLAN_RESOURCE_LIMIT, DEFAULT_RESOURCE_LIMIT));
	}

	public List<EntitySearchFilter> getSearchFilters() {
		List<EntitySearchFilter> entitySearchFilters = new ArrayList<EntitySearchFilter>();
		entitySearchFilters.add(EntitySearchFilter.newDefaultFilter());
		try {
			entitySearchFilters.addAll(EntitySearchFilters.forEntity(configVO.getCollHelper().getEntityUid()).getAll());
		} catch (PreferencesException e) {
			LOG.warn("getSearchFilters failed: " + e, e);
		}
		
		// add only if entity has searchpanel.
		if (MetaProvider.getInstance().getEntity(
				configVO.getCollHelper().getEntityUid()).isSearchable()) {
			entitySearchFilters.add(new ModifyCustomSearchFilter());
			entitySearchFilters.add(new NewCustomSearchFilter());
		}

		return entitySearchFilters;
	}
	
	public static class TimeGranularity implements GroupMapper<Interval<Date>> {

		private final GregorianCalendar calendar = new GregorianCalendar();

		private final TimeModel<Date> timeModel;
		private final GranularityType type;
		private final Map<Orientation, Integer> mapCellExtent = new HashMap<Orientation, Integer>();
		private final String cwLabel;
		private final boolean dayOfWeek;
		private final CalendarBackgroundProvider calBgProvider;

		public TimeGranularity(GranularityType type, TimeModel<Date> timeModel, boolean dayOfWeek, CalendarBackgroundProvider calBgProvider) {
			this.type = type;
			this.timeModel = timeModel;
			this.cwLabel = SpringLocaleDelegate.getInstance().getText("nuclos.resplan.cw.label");
			this.dayOfWeek = dayOfWeek;
			this.calBgProvider = calBgProvider;
		}

		public GranularityType getType() {
			return type;
		}

		public TimeModel<Date> getTimeModel() {
			return timeModel;
		}

		public GroupMapper<Interval<Date>> getHeaderCategories() {
			// TODO_RESPLAN: refactor
			return this;
		}

		@Override
		public int getCategoryCount() {
			return type.getLevel() + 1;
		}

		@Override
		public String getCategoryName(int category) {
			final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
			switch (category) {
			case -2: return localeDelegate.getText("nuclos.resplan.granularity.year", null);
			case -1: return localeDelegate.getText("nuclos.resplan.granularity.quarter", null);
			case 0: return localeDelegate.getText("nuclos.resplan.granularity.month", null);
			case 1: return localeDelegate.getText("nuclos.resplan.granularity.calendarWeek", null);
			case 2: return localeDelegate.getText("nuclos.resplan.granularity.weekday", null);
			case 3: return localeDelegate.getText("nuclos.resplan.granularity.day", null);
			case 4: return localeDelegate.getText("nuclos.resplan.granularity.time", null);
			default:
				throw new IllegalArgumentException();
			}
			// return null;
		}

		@Override
		public Object getCategoryValue(int category, Interval<Date> interval, int startindex) {
			switch (category) {
			case -2:
				calendar.setTime(interval.getStart());
				return String.format("%1$TY", calendar);
			case -1:
				calendar.setTime(interval.getStart());
				final int quarter = DateUtils.getQuant(calendar, DateUtils.QUARTER);
				if (startindex <= -2) {
					return String.format("Q%1$d", quarter + 1);
				}
				return String.format("Q%1$d", quarter + 1) + " " + String.format("%1$TY", calendar);
			case 2:
				if (!dayOfWeek) {
					return "";
				}
			case 0:
			case 1:
			case 3:
				String html = "<html><center>";
				if (calBgProvider != null && (category >= 2 || category == type.getLevel())) {
					// NUCLOS-6596 use special background colors for today and weekend/holidays 
					Color color = calBgProvider.getBackgroundColorForInterval(interval.getStart(), interval.getEnd());
					if (color != null) {
						// This is a user defined tag for internal use of JHeaderGrid
						html += CalendarBackgroundProvider.BGCOLOR_TAG + color.getRGB() + ">";
					}
				}
				calendar.setTime(interval.getStart());
				if (category == 0) {
					if (startindex <= -1) {
						html += String.format("%1$Tb", calendar);
					} else {
						html += String.format("%1$Tb %1$Ty", calendar);						
					}
				} else if (category == 1) {
					html += cwLabel + String.format("%02d", calendar.get(GregorianCalendar.WEEK_OF_YEAR));
				} else if (category == 2) {
					html += String.format("%1$Ta <br>", calendar);					
				} else {
					html += String.format("%1$Td.%1$Tm", calendar);
				}
				return html + "</center></html>";
			case 4:
				return String.format("%Tk-%Tk", interval.getStart(), interval.getEnd());
			default:
				throw new IllegalArgumentException();
			}
		}

		public String getCategoryValue(int category, Date start) {
			switch (category) {
			case -2:
				calendar.setTime(start);
				return String.format("%1$TY", calendar);
			case -1:
				calendar.setTime(start);
				final int quarter = DateUtils.getQuant(calendar, DateUtils.QUARTER);
				// BMWFDM-283: no year on quarter granularity
				// return String.format("Q%1$d", quarter + 1) + " " + String.format("%1$TY", calendar);
				return String.format("Q%1$d", quarter + 1);
			case 0:
				calendar.setTime(start);
				// BMWFDM-283: no year on month granularity
				// return String.format("%1$Tb %1$Ty", calendar);
				return String.format("%1$tb", calendar);
			case 1:
				calendar.setTime(start);
				return cwLabel + String.format("%02d", calendar.get(GregorianCalendar.WEEK_OF_YEAR));
			case 2:
				return "<html><center>" + String.format((dayOfWeek ? "%1$TA<br>" : "") + "%1$Td.%1$Tm", start) + "</center></html>";
			case 3:
				return String.format("%Tk", start);
			default:
				throw new IllegalArgumentException();
			}
		}
		
		@Override
		public void setCellExtent(Orientation o, int extent) {
			this.mapCellExtent.put(o, extent);
		}

		@Override
		public int getCellExtent(Orientation o) {
			return LangUtils.defaultIfNull(mapCellExtent.get(o), 0);
		}

		@Override
		public String toString() {
			return getCategoryName(type.getLevel());
		}
	}
	
	private class SelectHeaderController extends SelectObjectsController<CollectableEntityField> {

		private boolean bSourceEdited = false;;
		
		public SelectHeaderController(JComponent parent) {
			super(parent, new SelectHeaderPanel());
			final SelectHeaderPanel panel = (SelectHeaderPanel) getPanel();
			panel.btnUp.setEnabled(true);
			panel.btnDown.setEnabled(true);
			panel.btnUp.setVisible(true);
			panel.btnDown.setVisible(true);
			
			panel.btnEditSource.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					bSourceEdited = true;
					panel.taSource.setEnabled(true);
					panel.taSource.setText(translateEntityFieldListToLabel(getSelectedObjects()));
				}
			});
			
			panel.btnDefault.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					SortedSet<CollectableEntityField> setAllFields = new TreeSet<>();
					HeaderLabelChoiceList choiceList = (HeaderLabelChoiceList) getModel();
					setAllFields.addAll(choiceList.getAvailableFields());
					setAllFields.addAll(choiceList.getSelectedFields());
					List<CollectableEntityField> lstSelected = getEntityFieldsFromResourceLabel(configVO.getResourceLocale(SpringLocaleDelegate.getInstance().getUserLocaleInfo()).getResourceLabel());
					setAllFields.removeAll(lstSelected);
					choiceList.set(setAllFields, lstSelected, choiceList.getComparatorForAvaible());
					setModel(choiceList);
					
					bSourceEdited = true;
					panel.taSource.setEnabled(true);
					panel.taSource.setText(
							translateStringfiedUidToResourceLabel(
									configVO.getResourceLocale(SpringLocaleDelegate.getInstance().getUserLocaleInfo()).getResourceLabel(),
									convertFieldMetaIntoEntityField(MetaProvider.getInstance().getAllEntityFieldsByEntity(resPlanModel.getResourceEntity()).values())));
				}
			});
			
			ActionListener actionListener = new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					bSourceEdited = false;
					panel.taSource.setEnabled(false);
					panel.btnEditSource.setEnabled(true);
				}
			};
			
			panel.btnUp.addActionListener(actionListener);
			panel.btnDown.addActionListener(actionListener);
			panel.btnLeft.addActionListener(actionListener);
			panel.btnRight.addActionListener(actionListener);
		}

		@Override
		protected void setupListeners() {
			super.setupListeners();

			final SelectObjectsPanel panel = getPanel();
			panel.getJListAvailableObjects().addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent ev) {
					if (ev.getClickCount() == 2) {
						bSourceEdited = false;
						((SelectHeaderPanel)panel).taSource.setEnabled(false);
						((SelectHeaderPanel)panel).btnEditSource.setEnabled(true);
					}
				}
			});

			panel.getJListSelectedObjects().addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent ev) {
					if (ev.getClickCount() == 2) {
						bSourceEdited = false;
						((SelectHeaderPanel)panel).taSource.setEnabled(false);
						((SelectHeaderPanel)panel).btnEditSource.setEnabled(true);
					}
				}
			});
		}
		
		SelectHeaderPanel getSelectHeaderPanel() {
			return (SelectHeaderPanel) getPanel();
		}
		
		public boolean isSourceEdited() {
			return bSourceEdited;
		}
		
		public void setModel(HeaderLabelChoiceList ro) {
			super.setModel(ro);
			getSelectHeaderPanel().taSource.setText(ro.getSource() != null ? ro.getSource() : translateEntityFieldListToLabel(ro.getSelectedFields()));
			if (ro.getSource() != null && ro.getSource().equals(translateEntityFieldListToLabel(ro.getSelectedFields()))) {
				bSourceEdited = false;
				getSelectHeaderPanel().taSource.setEnabled(false);
				getSelectHeaderPanel().btnEditSource.setEnabled(true);
			} else {
				getSelectHeaderPanel().btnEditSource.setEnabled(false);
				bSourceEdited = true;
			}
		}
	}
	
	private class SelectHeaderPanel extends DefaultSelectObjectsPanel<CollectableEntityField> {
		private final JScrollPane scrlpnSource = new JScrollPane();
		private final JTextArea taSource = new JTextArea();
		private final JButton btnEditSource = new JButton();
		private final JButton btnDefault = new JButton();
		private final JLabel lbSource = new JLabel();
		
		public SelectHeaderPanel() {
			super();
			myInit();
		}
		
		protected void myInit() {
			btnEditSource.setText(SpringLocaleDelegate.getInstance().getMessage(
					"SelectHeaderPanel.1","HTML bearbeiten"));
			btnDefault.setText(SpringLocaleDelegate.getInstance().getMessage(
					"SelectHeaderPanel.3","Standardwert übernehmen"));
			lbSource.setText(SpringLocaleDelegate.getInstance().getMessage(
					"SelectHeaderPanel.2","HTML-Quellcode"));
			
			scrlpnSource.getViewport().add(taSource);
			scrlpnSource.setPreferredSize(new Dimension(200, 300));
			
			this.btnEditSource.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SelectHeaderPanel.1","HTML bearbeiten"));
			this.btnDefault.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SelectHeaderPanel.3","Standardwert übernehmen"));
						
//			this.pnlRightButtons.add(btnSortAsc, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
//					   , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
//			this.pnlRightButtons.add(btnSortDesc, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
//					   , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
			this.pnlMain.add(btnEditSource, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0
					   , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
			this.pnlMain.add(btnDefault, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0
					   , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
			this.pnlMain.add(pnlRightButtons, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0
					 , GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 10, 0, 10), 0, 0));
			this.pnlMain.add(lbSource, new GridBagConstraints(4, 0, 1, 1, 1.0, 1.0
					 , GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
			this.pnlMain.add(scrlpnSource, new GridBagConstraints(4, 1, 3, 1, 1.0, 1.0
					 , GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
		}
		
		public void addSelectedListSelectionListener(ListSelectionListener listener) {
			this.jlstSelectedColumns.getSelectionModel().addListSelectionListener(listener);
		}
		
		String getSource() {
			return taSource.getText();
		}
	}
	
	private class HeaderLabelChoiceList extends ChoiceList<CollectableEntityField> {
		
		private String labelSource;
		
		@Override
		public void set(SortedSet<CollectableEntityField> lstclctefAvailable, List<CollectableEntityField> lstclctefSelected, Comparator<? super CollectableEntityField> comp) {
			set(lstclctefAvailable, lstclctefSelected, null, comp);
		}
		
		public void set(SortedSet<CollectableEntityField> lstclctefAvailable, List<CollectableEntityField> lstclctefSelected, String labelSource, Comparator<? super CollectableEntityField> comp) {
			super.set(lstclctefAvailable, lstclctefSelected, comp);
			this.labelSource = labelSource;
		}
		
		public String getSource() {
			return this.labelSource;
		}
	}
	
	private class SelectSortOrderController extends SelectObjectsController<CollectableEntityField> {

		public SelectSortOrderController(Component parent) {
			super(parent, new SelectSortOrderPanel());
			
			SelectSortOrderPanel panel = getSelectSortOrderPanel();
			
			panel.btnUp.setEnabled(true);
			panel.btnDown.setEnabled(true);
			panel.btnUp.setVisible(true);
			panel.btnDown.setVisible(true);
			
			panel.btnDefault.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					SortedSet<CollectableEntityField> setAllFields = new TreeSet<>();
					SortOrderChoiceList choiceList = (SortOrderChoiceList) getModel();
					setAllFields.addAll(choiceList.getAvailableFields());
					setAllFields.addAll(choiceList.getSelectedFields());
					CollectableEntityField sortField = new CollectableEOEntityField(FieldMeta.NULL);
					if (configVO.getResourceSortField() != null) {
						sortField = new CollectableEOEntityField(MetaProvider.getInstance().getEntityField(configVO.getResourceSortField()));
						setAllFields.remove(sortField);
					}
					choiceList.set(setAllFields, Arrays.asList(sortField), new HashSet<CollectableEntityField>(), choiceList.getComparatorForAvaible());
					setModel(choiceList);
				}
			});
			
			panel.tblSelectedColumn.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent ev) {
					final ListSelectionModel lsm = (ListSelectionModel) ev.getSource();

					boolean bEnable = !lsm.isSelectionEmpty();

					getPanel().btnUp.setEnabled(bEnable);
					getPanel().btnDown.setEnabled(bEnable);

					for (Object o : getPanel().getJListSelectedObjects().getSelectedValues()) {
						if (getModel().getFixed().contains(o) || !isMovable(o)) {
							bEnable = false;
							break;
						}
					}

					getPanel().btnLeft.setEnabled(bEnable);
				}  // valueChanged
			});
		}
		
		@Override
		protected void setupListeners() {
			final SelectSortOrderPanel panel = getSelectSortOrderPanel();
			
			// add list selection listener for "right" button:
			panel.getJListAvailableObjects().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent ev) {

					final ListSelectionModel lsm = (ListSelectionModel) ev.getSource();
					final boolean bEnable = !lsm.isSelectionEmpty();
					getPanel().btnRight.setEnabled(bEnable);
				}	// valueChanged
			});

			// add list selectioners for "left", "up" and "down" buttons:
			panel.addSelectionListnerSelectedJCmponent(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent ev) {
					final ListSelectionModel lsm = (ListSelectionModel) ev.getSource();

					final boolean bEnable = !lsm.isSelectionEmpty();

					getPanel().btnLeft.setEnabled(bEnable);
					getPanel().btnUp.setEnabled(bEnable);
					getPanel().btnDown.setEnabled(bEnable);
				}	// valueChanged
			});

			panel.btnRight.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					moveRight();
				}
			});
			panel.btnLeft.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					moveLeft();
				}
			});

			// double click on list entry as shortcut for pressing the corresponding button:
			panel.addMouseListenerAvailableJComponent(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent ev) {
					if (ev.getClickCount() == 2) {
						moveRight();
					}
				}
			});

			panel.addMouseListenerSelectedJComponent(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent ev) {
					if (ev.getClickCount() == 2) {
						moveLeft();
					}
				}
			});

			panel.btnUp.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					moveUpDown(-1);
				}
			});
			panel.btnDown.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					moveUpDown(+1);
				}
			});
		}
		
		private void moveLeft() {
			final SelectSortOrderPanel panel = getSelectSortOrderPanel();
			MutableListModel<CollectableEntityField> modelSrc = panel.getSelectedColumnsModel();
			ListSelectionModel selectionModel = panel.getSelectedModelSelectedJComponent();
			MutableListModel<CollectableEntityField> modelDest = panel.getAvailableColumnsModel();
			
			moveLeftRight(panel, modelSrc, modelDest, selectionModel, true);
		}

		private void moveRight() {
			final SelectSortOrderPanel panel = getSelectSortOrderPanel();
			moveLeftRight(panel,
					panel.getAvailableColumnsModel(),
					panel.getSelectedColumnsModel(),
					panel.getSelectedModelAvailabelJComponent(), false);
		}
		
		private void moveLeftRight(SelectSortOrderPanel panel, MutableListModel<CollectableEntityField> modelSrc, MutableListModel<CollectableEntityField> modelDest, ListSelectionModel selectionModel, boolean moveLeft) {
			final int[] aiSelectedIndices = getSelectedIndices(selectionModel);
			
			boolean updateRightTable = false;

			// 1. add the selected rows to the dest list, in increasing order:
			for (int iSelectedIndex : aiSelectedIndices) {
				CollectableEntityField clctef = (CollectableEntityField) modelSrc.getElementAt(iSelectedIndex);
				if (moveLeft) {
					if (!clctef.isCalcAttributeAllowCustomization()) {
						modelDest.add((CollectableEntityField) modelSrc.getElementAt(iSelectedIndex));
					}
				} else {
					if (clctef.isCalcAttributeAllowCustomization()) {			
						FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(clctef.getUID());
						String paramValues = CalcAttributeClientHelper.showCalcAttributesParamValueEditor(panel, clctef.getLabel(), fMeta.getCalcAttributeDS(), fMeta.getCalcAttributeParamValues());
						if (paramValues == null) {
							continue;
						}
						FieldMeta<?> calcMeta = MetaProvider.getInstance().getCalcAttributeCustomization(fMeta.getUID(), paramValues);
						modelDest.add(new CollectableEOEntityField(calcMeta));
						updateRightTable = true;
					} else {
						modelDest.add((CollectableEntityField) modelSrc.getElementAt(iSelectedIndex));
					}
				}
			}	// for

			// 2. remove the selected rows from the source list, in decreasing order:
			for (int i = aiSelectedIndices.length - 1; i >= 0; --i) {
				int index = aiSelectedIndices[i];
				if (!moveLeft) {
					CollectableEntityField clctef = (CollectableEntityField) modelSrc.getElementAt(index);
					if (clctef.isCalcAttributeAllowCustomization()) {
						continue;
					}
				}
				modelSrc.remove(index);
				index = Math.min(index, modelSrc.getSize() - 1);
				if (index >= 0) {
					selectionModel.setSelectionInterval(index, index);
				}
			}	// for
		}

		public final void moveUpDown(int iDirection) {
			final SelectSortOrderPanel panel = getSelectSortOrderPanel();
			final MutableListModel<CollectableEntityField> listmodelSelectedFields = panel.getSelectedColumnsModel();

			final int iIndex = panel.getSelectedModelSelectedJComponent().getAnchorSelectionIndex();
			final int iNewIndex = iIndex + iDirection;
			if (iNewIndex >= 0 && iNewIndex < listmodelSelectedFields.getSize()) {
				final Object o = listmodelSelectedFields.getElementAt(iIndex);
				listmodelSelectedFields.remove(iIndex);
				listmodelSelectedFields.add(iNewIndex, (CollectableEntityField) o);
				panel.getSelectedModelSelectedJComponent().setSelectionInterval(iNewIndex, iNewIndex);
			}
		}
		
		private int[] getSelectedIndices(ListSelectionModel sm) {
			final int iMinIndex = sm.getMinSelectionIndex();
			final int iMaxIndex = sm.getMaxSelectionIndex();

			if ((iMinIndex < 0) || (iMaxIndex < 0)) {
				return new int[0];
			}

			final int[] aiTemp = new int[1 + (iMaxIndex - iMinIndex)];
			int i = 0;
			for (int iIndex = iMinIndex; iIndex <= iMaxIndex; iIndex++) {
				if (sm.isSelectedIndex(iIndex)) {
					aiTemp[i++] = iIndex;
				}
			}
			final int[] result = new int[i];
			System.arraycopy(aiTemp, 0, result, 0, i);
			return result;
		}
		
		public void setModel(SortOrderChoiceList ro) {
			super.setModel(ro);
			getSelectSortOrderPanel().setDescColumns(ro.getDescColumns());
		}
		
		private SelectSortOrderPanel getSelectSortOrderPanel() {
			return (SelectSortOrderPanel) getPanel();
		}
		
		public Set<CollectableEntityField> getDescColumns() {
			return getSelectSortOrderPanel().getDescColumns();
		}
	}
	
	private class SelectSortOrderPanel extends DefaultSelectObjectsPanel<CollectableEntityField> {
		
		private JTable tblSelectedColumn;
		private final JButton btnDefault = new JButton();
		
		public SelectSortOrderPanel() {
			super();
			myInit();
		}
		
		private void myInit() {
			tblSelectedColumn = new JTable() {
				@Override
				public TableCellRenderer getCellRenderer(int row, int column) {
					return new TopTableCellRendererDelegate(super.getCellRenderer(row, column));
				}
			};
			tblSelectedColumn.setShowHorizontalLines(false);
			tblSelectedColumn.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			
			scrlpnSelectedColumns.getViewport().add(tblSelectedColumn, null);
			scrlpnSelectedColumns.getViewport().setBackground(tblSelectedColumn.getBackground());
			scrlpnSelectedColumns.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			
			labSelectedColumns.setText(SpringLocaleDelegate.getInstance().getResource("SelectSortOrderPanel.1", "Sortiert nach"));
			
			tblSelectedColumn.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				
				@Override
				public void valueChanged(ListSelectionEvent e) {
					setOptimalColumnWidth();
				}
			});
			
			btnDefault.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
					"SelectSortOrderPanel.2","Standardwert übernehmen"));
			btnDefault.setIcon(Icons.getInstance().getIconUndo16());
			
			this.pnlRightButtons.add(btnDefault, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
			   , GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));
		}

		public Set<CollectableEntityField> getDescColumns() {
			return ((SortOrderTableModel) this.tblSelectedColumn.getModel()).getDescColumns();
		}

		@Override
		public void setSelectedColumnsModel(MutableListModel<CollectableEntityField> listmodelSelectedFields) {
			this.tblSelectedColumn.setModel(new SortOrderTableModel(listmodelSelectedFields));
		}
		
		public MutableListModel<CollectableEntityField> getAvailableColumnsModel() {
			return (MutableListModel<CollectableEntityField>) this.getJListAvailableObjects().getModel();
		}
		
		public MutableListModel<CollectableEntityField> getSelectedColumnsModel() {
			return ((SortOrderTableModel) this.tblSelectedColumn.getModel()).getObjectListModel();
		}
		
		public ListSelectionModel getSelectedModelSelectedJComponent() {
			return this.tblSelectedColumn.getSelectionModel();
		}

		public ListSelectionModel getSelectedModelAvailabelJComponent() {
			return this.getJListAvailableObjects().getSelectionModel();
		}
		
		public void addSelectionListnerSelectedJCmponent(ListSelectionListener aListener) {
			this.tblSelectedColumn.getSelectionModel().addListSelectionListener(aListener);
		}
		
		public void addMouseListenerAvailableJComponent(MouseListener aListener) {
			this.getJListAvailableObjects().addMouseListener(aListener);
		}
		
		public void addMouseListenerSelectedJComponent(MouseListener aListener) {
			this.getJListSelectedObjects().addMouseListener(aListener);
			this.tblSelectedColumn.addMouseListener(aListener);
		}
		
		public void setDescColumns(Set<CollectableEntityField> descColumns) {
			((SortOrderTableModel) this.tblSelectedColumn.getModel()).setDescColumns(descColumns);
			TableColumnModel cm = tblSelectedColumn.getColumnModel();
			cm.getColumn(0).setMinWidth(100);
			cm.getColumn(1).setPreferredWidth(70);
			cm.getColumn(1).setMaxWidth(70);
			cm.getColumn(1).setMinWidth(70);
			setOptimalColumnWidth();
		}
		
		private void setOptimalColumnWidth() {
			TableUtils.setOptimalColumnWidth(tblSelectedColumn, 0);
		}
	}
	
	private class SortOrderChoiceList extends ChoiceList<CollectableEntityField> {
		
		private Set<CollectableEntityField> descColumns;
		
		public void set(SortedSet<CollectableEntityField> lstclctefAvailable, List<CollectableEntityField> lstclctefSelected, Set<CollectableEntityField> descColumns, Comparator<? super CollectableEntityField> comp) {
			super.set(lstclctefAvailable, lstclctefSelected, comp);
			this.descColumns = descColumns;
		}
		
		public Set<CollectableEntityField> getDescColumns() {
			return descColumns;
		}
	}
	
	private class SortOrderTableModel extends AbstractTableModel {

		private MutableListModel<CollectableEntityField> objectListModel;
		private final Set<CollectableEntityField> descColumns;
		
		public SortOrderTableModel(MutableListModel<CollectableEntityField> objectColl) {
			super();
			this.descColumns = new HashSet<CollectableEntityField>();
			this.objectListModel = objectColl;
			this.objectListModel.addListDataListener(new ListDataListener() {

				@Override
				public void intervalAdded(ListDataEvent e) {
					SortOrderTableModel.this.fireTableRowsInserted(e.getIndex0(),
							e.getIndex1());
				}

				@Override
				public void intervalRemoved(ListDataEvent e) {
					SortOrderTableModel.this.fireTableRowsDeleted(e.getIndex0(),
							e.getIndex1());
				}

				@Override
				public void contentsChanged(ListDataEvent e) {
					SortOrderTableModel.this.fireTableRowsUpdated(e.getIndex0(),
							e.getIndex1());
				}
			});
		}
		
		@Override
		public int getRowCount() {
			return objectListModel.getSize();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return (columnIndex == 0) ? objectListModel.getElementAt(rowIndex)
					: isColumnDescending(objectListModel.getElementAt(rowIndex));
		}
		
		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			if (columnIndex == 1 && aValue instanceof Boolean) {
				if (((Boolean) aValue).booleanValue()) {
					this.descColumns.add((CollectableEntityField) objectListModel.getElementAt(rowIndex));
				} else {
					this.descColumns.remove(objectListModel.getElementAt(rowIndex));
				}
			}
		}
		
		@Override
		public String getColumnName(int col) {
			return (col == 0) 	? SpringLocaleDelegate.getInstance().getResource("SortOrderTableModel.1", "Attribut")
								: SpringLocaleDelegate.getInstance().getResource("SortOrderTableModel.2", "Absteigend");
		}
		
		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return (columnIndex != 0);
		}
		
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return (columnIndex == 0) ? Object.class : Boolean.class;
		}
		
		public void setDescColumns(Set<CollectableEntityField> descColumns) {
			this.descColumns.clear();
			this.descColumns.addAll(descColumns);
			SortOrderTableModel.this.fireTableStructureChanged();
		}
		
		private Boolean isColumnDescending(Object rowObject) {
			return descColumns.contains(rowObject);
		}

		public MutableListModel<CollectableEntityField> getObjectListModel() {
			return objectListModel;
		}

		public Set<CollectableEntityField> getDescColumns() {
			return descColumns;
		}
	}
	
	public void cmdConfigureResourceHeader(MainFrameTab iFrame) {
		SelectHeaderController controller = new SelectHeaderController(iFrame);
		Collection<FieldMeta<?>> collFieldMeta = MetaProvider.getInstance().getAllEntityFieldsByEntity(resPlanModel.getResourceEntity()).values();
		SortedSet<CollectableEntityField> setefAll = new TreeSet<CollectableEntityField>();
		SortedSet<CollectableEntityField> setefAvailable = new TreeSet<CollectableEntityField>();
		String resourceLabel = restoreUserResourceLabel();
		List<CollectableEntityField> lstefSelected = getEntityFieldsFromResourceLabel(resourceLabel);
		for (FieldMeta<?> meta : collFieldMeta) {
			CollectableEntityField clctef = new CollectableEOEntityField(meta);
			if (!lstefSelected.contains(clctef)) {
				setefAvailable.add(clctef);
			}
			setefAll.add(clctef);
		}
		HeaderLabelChoiceList ro = new HeaderLabelChoiceList();
		ro.set(setefAvailable, lstefSelected, translateStringfiedUidToResourceLabel(resourceLabel, setefAll), new Comparator<CollectableEntityField>() {
			@Override
			public int compare(CollectableEntityField o1, CollectableEntityField o2) {
				return o1.compareTo(o2);
			}			
		});
		controller.setModel(ro);
		if (controller.run(SpringLocaleDelegate.getInstance().getResource(
				"nuclos.resplan.configureheader.title", "Legende anpassen"))) {
			storeUserResourceLabel(
					CustomComponentWizardModel.translateLabelToStringifiedUid(
							controller.isSourceEdited() ? controller.getSelectHeaderPanel().getSource() : translateEntityFieldListToLabel(controller.getSelectedObjects()),
							collFieldMeta
							));
			refreshLabels();
		};
	}
	
	public void cmdConfigureResourceSortOrder(MainFrameTab iFrame) {
		SelectSortOrderController controller = new SelectSortOrderController(iFrame);
		Collection<FieldMeta<?>> collFieldMeta = MetaProvider.getInstance().getAllEntityFieldsByEntity(resPlanModel.getResourceEntity()).values();
		SortedSet<CollectableEntityField> setefAll = new TreeSet<CollectableEntityField>();
		SortedSet<CollectableEntityField> setefAvailable = new TreeSet<CollectableEntityField>();
		List<CollectableEntityField> lstefSelected = new ArrayList<>();
		Pair<List<CollectableEntityField>, Set<CollectableEntityField>> sortOrder = restoreResourceSortOrder();
		lstefSelected.addAll(sortOrder.getX());
		for (FieldMeta<?> meta : collFieldMeta) {
			CollectableEntityField clctef = new CollectableEOEntityField(meta);
			if (!lstefSelected.contains(clctef)) {
				setefAvailable.add(clctef);
			}
			setefAll.add(clctef);
		}
		SortOrderChoiceList ro = new SortOrderChoiceList();
		Set<CollectableEntityField> setDescColumns = sortOrder.getY();
		ro.set(setefAvailable, lstefSelected, setDescColumns, new Comparator<CollectableEntityField>() {
			@Override
			public int compare(CollectableEntityField o1, CollectableEntityField o2) {
				return o1.compareTo(o2);
			}			
		});
		controller.setModel(ro);
		if (controller.run(SpringLocaleDelegate.getInstance().getResource(
				"nuclos.resplan.configuresortorder.title", "Sortierung anpassen"))) {
			List<CollectableEntityField> lstSelected = controller.getSelectedObjects();
			setDescColumns = controller.getDescColumns();
			storeResourceSortOrder(lstSelected, setDescColumns);
			refresh(false);
		};
	}
	
	public List<CollectableEntityField> getEntityFieldsFromResourceLabel(String label) {
		List<CollectableEntityField> lstef = new ArrayList<>();
		ForeignEntityFieldUIDParser uidParser = new ForeignEntityFieldUIDParser(label, null, null);
		Iterator<IFieldUIDRef> itParser = uidParser.iterator();
		while (itParser.hasNext()) {
			IFieldUIDRef ref = itParser.next();
			if (ref.isUID()) {
				UID uid = ref.getUID();
				FieldMeta<?> meta = MetaProvider.getInstance().getEntityField(uid);
				lstef.add(new CollectableEOEntityField(meta));
			}
		}
		return lstef;
	}
	
	private String translateEntityFieldListToLabel(List<CollectableEntityField> lstef) {
		StringBuffer label = new StringBuffer();
		label.append("<html>\n");
		for (CollectableEntityField clctef : lstef) {
			label.append("<nobr>");
			label.append("${");
			label.append(clctef.getName());
			label.append("}");
			label.append("</nobr>");
			label.append("<br>\n");
		}
		label.append("</html>");
		return label.toString();
	}
	
	private String translateStringfiedUidToResourceLabel(String stringifiedUid, Collection<CollectableEntityField> collef) {
		StringBuffer label = new StringBuffer();
		ForeignEntityFieldUIDParser uidParser = new ForeignEntityFieldUIDParser(stringifiedUid, null, null);
		Iterator<IFieldUIDRef> itParser = uidParser.iterator();
		while (itParser.hasNext()) {
			IFieldUIDRef part = itParser.next();
			if (part.isUID()) {
				UID uid = part.getUID();
				for (CollectableEntityField clctef : collef) {
					if (uid.equals(clctef.getUID())) {
						label.append("${");
						label.append(clctef.getName());
						label.append("}");
						break;
					}
				}
			} else {
				label.append(part.getConstant());
			}
		}
		return label.toString();
	}
	
	private String translateStringfiedUidToLegendLabel(String stringifiedUid) {
		StringBuffer label = new StringBuffer();
		ForeignEntityFieldUIDParser uidParser = new ForeignEntityFieldUIDParser(stringifiedUid, null, null);
		Iterator<IFieldUIDRef> itParser = uidParser.iterator();
		while (itParser.hasNext()) {
			IFieldUIDRef part = itParser.next();
			if (part.isUID()) {
				UID uid = part.getUID();
				CollectableEntityField clctef = new CollectableEOEntityField(MetaProvider.getInstance().getEntityField(uid));
				if (uid.equals(clctef.getUID())) {
					label.append(clctef.getLabel());
				}
			} else {
				label.append(part.getConstant());
			}
		}
		return label.toString();
	}
	
	private String stripHtml(String text) {
		if (text == null || !text.startsWith("<html>")) {
			return text;
		}
		
		Document document = Jsoup.parse(text);
		Whitelist whitelist = new Whitelist();
		whitelist.addTags("html", "br");
		Cleaner htmlCleaner = new Cleaner(whitelist);
		return htmlCleaner.clean(document).toString();
	}
	
	public String restoreUserResourceLabel() {
		String rLabel = null;
		PlanningTablePreferences prefs = planningTablePreferencesManager.getSelected(false);
		rLabel = prefs.getHeaderLabel().get(SpringLocaleDelegate.getInstance().getUserLocaleInfo().getTag());
		return rLabel == null ? configVO.getResourceLocale(SpringLocaleDelegate.getInstance().getUserLocaleInfo()).getResourceLabel() : rLabel;
	}
	
	private void storeUserResourceLabel(String label) {
		PlanningTablePreferences prefs = planningTablePreferencesManager.getSelected(false);
		prefs.getHeaderLabel().put(SpringLocaleDelegate.getInstance().getUserLocaleInfo().getTag(), label);
		planningTablePreferencesManager.update(prefs);
	}
	
	private String restoreUserLegendLabel() {
		String rLabel = null;
		PlanningTablePreferences prefs = planningTablePreferencesManager.getSelected(false);
		rLabel = prefs.getHeaderLabel().get(SpringLocaleDelegate.getInstance().getUserLocaleInfo().getTag());
		return rLabel == null ? configVO.getResourceLocale(SpringLocaleDelegate.getInstance().getUserLocaleInfo()).getLegendLabel() : translateStringfiedUidToLegendLabel(stripHtml(rLabel));
	}
	
	private void storeResourceSortOrder(List<CollectableEntityField> lstOrderColumns, Set<CollectableEntityField> setDescColumns) {
		PlanningTablePreferences prefs = planningTablePreferencesManager.getSelected(false);
		List<String> lstColumnOrder = new ArrayList<>();
		
		for (CollectableEntityField clctef : lstOrderColumns) {
			lstColumnOrder.add(clctef.getUID() + "," + (setDescColumns.contains(clctef) ? Boolean.TRUE.toString() : Boolean.FALSE.toString()));
		}
		
		prefs.setResourceSortOrder(org.apache.commons.lang.StringUtils.join(lstColumnOrder, "|"));
		planningTablePreferencesManager.update(prefs);
	}
	
	private Pair<List<CollectableEntityField>, Set<CollectableEntityField>> restoreResourceSortOrder() {
		MetaProvider metaProvider = SpringApplicationContextHolder.getBean(MetaProvider.class);
		List<CollectableEntityField> lstOrderBy = new ArrayList<>();
		Set<CollectableEntityField> setDescColumns = new HashSet<>();
		Pair<List<CollectableEntityField>, Set<CollectableEntityField>> result = new Pair<>();

		PlanningTablePreferences defaultPrefs = planningTablePreferencesManager.getSelected(false);

		if (defaultPrefs.getResourceSortOrder() != null) {
			String sSortOrder = defaultPrefs.getResourceSortOrder();
			for (String sColumnOrder : sSortOrder.split("\\|")) {
				String[] parts = sColumnOrder.split(",");
				if (parts.length == 2) {
					if (!"null".equals(parts[0])) {
						CollectableEntityField clctef = new CollectableEOEntityField(metaProvider.getEntityField(UID.parseUID(parts[0])));
						lstOrderBy.add(clctef);
						if (Boolean.TRUE.equals(Boolean.parseBoolean(parts[1]))) {
							setDescColumns.add(clctef);
						}
					}
				}
			}
		} else {
			lstOrderBy.clear();
			setDescColumns.clear();
			if (configVO.getResourceSortField() != null) {
				lstOrderBy.add(new CollectableEOEntityField(metaProvider.getEntityField(configVO.getResourceSortField())));
			}
		}
		result.setX(lstOrderBy);
		result.setY(setDescColumns);
		return result;
	}
	
	private void refreshLabels() {
		ComponentProvider<?> resourceLabelProvider = resPlanPanel.getResourceRenderer();
		if (resourceLabelProvider instanceof CollectableLabelProvider) {
			((CollectableLabelProvider) resourceLabelProvider).setLabelTemplate(restoreUserResourceLabel());
		}
		
		Component comp = resPlanPanel.getCaptionComponent();
		if (comp instanceof JPanel) {
			for (Component innerComp : ((JPanel)comp).getComponents()) {
				if (innerComp instanceof JLabel) {
					((JLabel)innerComp).setText(restoreUserLegendLabel());
				}
			}
		}
		
		resPlanPanel.refreshHeaderExtent();
	}
	
	private Collection<CollectableEntityField> convertFieldMetaIntoEntityField(Collection<FieldMeta<?>> collmeta) {
		Collection<CollectableEntityField> collef = new HashSet<>();
		for (FieldMeta<?> meta : collmeta) {
			collef.add(new CollectableEOEntityField(meta));
		}
		return collef;
	}
}
