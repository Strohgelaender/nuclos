//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp.wizard;

import static info.clearthought.layout.TableLayoutConstants.FILL;
import static info.clearthought.layout.TableLayoutConstants.PREFERRED;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.Document;
import javax.tools.Diagnostic.Kind;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.ObjectUtils;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.customcomp.CustomComponentCache;
import org.nuclos.client.customcomp.CustomComponentDelegate;
import org.nuclos.client.customcomp.resplan.BackgroundPainter;
import org.nuclos.client.customcomp.resplan.ClientPlanElement;
import org.nuclos.client.customcomp.resplan.ClientResPlanConfigVO;
import org.nuclos.client.customcomp.resplan.CollectableLabelProvider;
import org.nuclos.client.customcomp.resplan.PlanElementsTableModel;
import org.nuclos.client.customcomp.resplan.ResourceTranslationTableModel;
import org.nuclos.client.main.Main;
import org.nuclos.client.rule.server.RuleEditPanel;
import org.nuclos.client.scripting.CompiledGroovy;
import org.nuclos.client.scripting.GroovySupport;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.BubbleUtils;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.model.AbstractListTableModel;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.client.wizard.util.NuclosWizardUtils;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SourceResultHelper;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common.customcomp.resplan.PlanElementLocaleVO;
import org.nuclos.common.customcomp.resplan.ResourceLocaleVO;
import org.nuclos.common.spring.AnnotationJaxb2Marshaller;
import org.nuclos.common.time.LocalTime;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.nuclos.server.customcomp.valueobject.CustomComponentVO;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.PanelWizardStep;
import org.pietschy.wizard.Wizard;
import org.pietschy.wizard.WizardEvent;
import org.pietschy.wizard.WizardListener;
import org.pietschy.wizard.WizardModel;
import org.pietschy.wizard.models.StaticModel;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

//Version
public class CustomComponentWizardModel extends StaticModel {

	private final AnnotationJaxb2Marshaller jaxb2Marshaller;
	
	Wizard wizard;
	boolean completed;
	CustomComponentVO componentVO;
	ClientResPlanConfigVO configVO;
	List<TranslationVO> translations;
	
	CustomComponentWizardModel(AnnotationJaxb2Marshaller jaxb2Marshaller) {
		if (jaxb2Marshaller == null) {
			throw new NullPointerException();
		}
		this.jaxb2Marshaller = jaxb2Marshaller;
		add(new CustomComponentWizardStep1());
		add(new CustomComponentWizardStep2());
		add(new CustomComponentWizardStep3a());
		add(new CustomComponentWizardStep3b());
		add(new CustomComponentWizardStep5());
		add(new CustomComponentWizardStep6());
		setLastAvailable(false);
		setLastVisible(false);
	}
	
	protected SpringLocaleDelegate getSpringLocaleDelegate() {
		return SpringLocaleDelegate.getInstance();
	}

	void setWizard(Wizard wizard) {
		this.wizard = wizard;
	}

	void setCustomComponentVO(CustomComponentVO vo) {
		this.componentVO = vo;
		if (vo.getData() != null) {
			final Jaxb2Marshaller marshaller = SpringApplicationContextHolder.getBean(Jaxb2Marshaller.class);
			this.configVO = ClientResPlanConfigVO.unsafeFromBytes(vo.getData(), marshaller);
		} else {
			this.configVO = new ClientResPlanConfigVO();
		}
	}

	void setTranslations(List<TranslationVO> translations) {
		this.translations = translations;
	}

	@Override
	public void refreshModelState() {
		super.refreshModelState();
		if (((CustomComponentWizardAbstractStep) getActiveStep()).isFinishStep()) {
			setPreviousAvailable(false);
			setCancelAvailable(false);
		}
	}

	public void finish() {
		wizard.getCancelAction().setEnabled(false);

		final StreamResult bytes = SourceResultHelper.newResultForByteArray();
		jaxb2Marshaller.marshal(configVO, bytes);
		componentVO.setData(((ByteArrayOutputStream) bytes.getOutputStream()).toByteArray());
		UIUtils.runShortCommandForTabbedPane(null, new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				if (componentVO.getId() != null) {
					CustomComponentDelegate.getInstance().modify(componentVO, translations);
				} else {
					CustomComponentDelegate.getInstance().create(componentVO, translations);
				}
				MetaProvider.getInstance().revalidate();
			}
		});
	}

	public void cancelWizard() {
		this.wizard.getCancelAction().actionPerformed(null);
	}

	//
	// Steps
	//

	@SuppressWarnings("serial")
	abstract static class CustomComponentWizardAbstractStep extends PanelWizardStep implements DocumentListener {

		CustomComponentWizardModel model;
		
		// former Spring injection
		
		private SpringLocaleDelegate localeDelegate;
		
		// end of former Spring injection

		CustomComponentWizardAbstractStep(String titleResId) {
			super(SpringLocaleDelegate.getInstance().getText(titleResId, null), null);
		}

		CustomComponentWizardAbstractStep(String titleResId, String summaryResId) {
			super(SpringLocaleDelegate.getInstance().getText(titleResId, null), 
					SpringLocaleDelegate.getInstance().getText(summaryResId, null));
			
			setSpringLocaleDelegate(SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class));
		}
		
		final void setSpringLocaleDelegate(SpringLocaleDelegate cld) {
			this.localeDelegate = cld;
		}

		final SpringLocaleDelegate getSpringLocaleDelegate() {
			return localeDelegate;
		}

		@Override
		public void init(WizardModel model) {
			this.model = (CustomComponentWizardModel) model;
		}

		protected boolean isFinishStep() {
			return false;
		}

		protected abstract void updateState();

		@Override
		public void insertUpdate(DocumentEvent e) {
			updateState();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			updateState();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			updateState();
		}

		protected void invalidStateLocalized(JComponent comp, String resourceId, Object...args) throws InvalidStateException {
			invalidState(comp, getSpringLocaleDelegate().getMessage(resourceId, null, args));
		}

		protected void invalidState(JComponent comp, String message) throws InvalidStateException {
			BubbleUtils.Position position = BubbleUtils.Position.SW;
			if (comp == null) {
				comp = this;
				position = BubbleUtils.Position.NW;
			}
			new Bubble(comp, message, 8, position).setVisible(true);
			throw new InvalidStateException(message, false);
		}
	}

	@SuppressWarnings("serial")
	static class CustomComponentWizardStep1 extends CustomComponentWizardAbstractStep {
		private static final Pattern INTERNAL_NAME_PATTERN = Pattern.compile("[a-zA-Z][a-zA-Z0-9]*");

		JTextField internalNameTextField;
		JComboBox existingComponents;
		JButton removeButton;

		CustomComponentWizardStep1() {
			super("nuclos.resplan.wizard.step1.title", "nuclos.resplan.wizard.step1.summary");
			final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
					
			internalNameTextField = new JTextField(40);
			internalNameTextField.getDocument().addDocumentListener(this);

			existingComponents = new JComboBox();
			existingComponents.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					updateState();
				}
			});
			removeButton = new JButton(new AbstractAction(localeDelegate.getText("nuclos.resplan.wizard.step1.remove", null)) {
				@Override
				public void actionPerformed(ActionEvent e) {
					final CustomComponentVO selectedComponent = (CustomComponentVO) existingComponents.getSelectedItem();
					if (selectedComponent == null)
						return;
					int opt = JOptionPane.showConfirmDialog(CustomComponentWizardStep1.this, 
							localeDelegate.getText("nuclos.resplan.wizard.step1.remove.check", null));
					if (opt == JOptionPane.OK_OPTION) {
						UIUtils.runShortCommandForTabbedPane(null, new CommonRunnable() {
							@Override
							public void run() throws CommonBusinessException {
								CustomComponentDelegate.getInstance().remove(selectedComponent);
							};
						});
						model.cancelWizard();
					}
				}
			});

			new TableLayoutBuilder(this)
				.columns(PREFERRED, PREFERRED, FILL).gaps(5, 5)
				.newRow().addLocalizedLabel("nuclos.resplan.wizard.step1.internalName").add(internalNameTextField)
				.newRow().addLocalizedLabel("nuclos.resplan.wizard.step1.existingConfiguration").add(existingComponents)
				.newRow().add(removeButton);
			;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void prepare() {
			final List<CustomComponentVO> list = new ArrayList<CustomComponentVO>(CustomComponentCache.getInstance().getAll());
			list.add(0, null);
			existingComponents.setModel(new ListComboBoxModel<CustomComponentVO>(list));
			existingComponents.setRenderer(new DefaultListCellRenderer() {
				@Override
				public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
					return super.getListCellRendererComponent(list, value == null ? null : ((CustomComponentVO)value).getInternalName(), index, isSelected, cellHasFocus);
				}
			});
			updateState();
		}

		@Override
		protected void updateState() {
			boolean reconfigure = existingComponents.getSelectedItem() != null;
			removeButton.setVisible(reconfigure);
			internalNameTextField.setEnabled(!reconfigure);
			boolean complete = !internalNameTextField.getText().isEmpty() || reconfigure;
			setComplete(complete);
		}

		@Override
		public void applyState() throws InvalidStateException {
			CustomComponentVO componentVO = (CustomComponentVO) existingComponents.getSelectedItem();
			if (componentVO == null) {
				String internalName = internalNameTextField.getText();
				if (!INTERNAL_NAME_PATTERN.matcher(internalName).matches()) {
					invalidStateLocalized(internalNameTextField, "nuclos.resplan.wizard.step1.check.invalidInternalName");
				}
				componentVO = new CustomComponentVO();
				componentVO.setInternalName(internalNameTextField.getText());
				componentVO.setComponentType("org.nuclos.resplan");
				componentVO.setComponentVersion("0.9");
			} else {
				componentVO = CustomComponentCache.getInstance().getByUID(componentVO.getPrimaryKey()).clone();
				if (!"org.nuclos.resplan".equals(componentVO.getComponentType())) {
					invalidState(null, "Only resource components can be reconfigured");
				}
				// Load translations
				try {
					model.setTranslations(CustomComponentDelegate.getInstance().getTranslations(componentVO.getId()));
				} catch (CommonBusinessException e) {
					throw new InvalidStateException(e.getMessage());
				}
			}
			model.setCustomComponentVO(componentVO);
		}
	}

	@SuppressWarnings("serial")
	static class CustomComponentWizardStep2 extends CustomComponentWizardAbstractStep {
		JScrollPane scrollPane;
		JTable translationTable;

		Collection<LocaleInfo> locales;
		CustomComponentTranslationTableModel tablemodel;

		public static String[] labels = TranslationVO.LABELS_CUSTOM_COMPONENT;

		CustomComponentWizardStep2() {
			super("nuclos.resplan.wizard.step2.title", "nuclos.resplan.wizard.step2.summary");

			tablemodel = new CustomComponentTranslationTableModel();
			List<TranslationVO> lstTranslation = new ArrayList<TranslationVO>();

			locales = LocaleDelegate.getInstance().getAllLocales(false);
			for (LocaleInfo voLocale : locales) {
				final Map<String, String> map = new HashMap<String, String>();
				final TranslationVO translation = new TranslationVO(voLocale, map);
				for(String sLabel : labels) {
					translation.getLabels().put(sLabel, "");
				}
				lstTranslation.add(translation);
			}
			tablemodel.setRows(lstTranslation);

			translationTable = new JTable(tablemodel);
			JTextField txtField = new JTextField();
			txtField.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
			DefaultCellEditor editor = new DefaultCellEditor(txtField);
			editor.setClickCountToStart(1);

			for(TableColumn col : CollectionUtils.iterableEnum(translationTable.getColumnModel().getColumns())) {
				col.setCellEditor(editor);
			}

			translationTable.getTableHeader().addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					stopCellEditing();
				}
			});

			scrollPane = new JScrollPane(translationTable);

			new TableLayoutBuilder(this).columns(FILL).gaps(5, 5).newRow(FILL).add(scrollPane);
		}

		private void stopCellEditing() {
	        for(TableColumn col : CollectionUtils.iterableEnum(translationTable.getColumnModel().getColumns())) {
	        	TableCellEditor cellEditor = col.getCellEditor();
				if(cellEditor != null)
	        		cellEditor.stopCellEditing();
	        }
		}

		@Override
		public void prepare() {
			if(model.translations != null && model.translations.size() > 0) {
				tablemodel.setRows(model.translations);
			}
			setComplete(true);
		}

		@Override
		protected void updateState() {

		}

		@Override
		public void applyState() throws InvalidStateException {
			stopCellEditing();
			model.translations = tablemodel.getRows();
		}
	}

	@SuppressWarnings("serial")
	static class CustomComponentWizardStep3a extends CustomComponentWizardAbstractStep implements ItemListener, ChangeListener {

		JComboBox resEntityComboBox;
		JComboBox resSortFieldComboBox;
		
		JCheckBox dayOfWeekCheckBox;
		
		JCheckBox withHolidaysCheckBox;
		JComboBox holidaysEntityCombobox;
		JComboBox holidaysDateCombobox;
		JComboBox holidaysNameCombobox;
		JComboBox holidaysTipCombobox;

		JTextField tfDefaultViewFrom;
		JTextField tfDefaultViewUntil;
		
		JComboBox specialViewFromFieldCombobox;
		JComboBox specialViewUntilFieldCombobox;
		JTextField tfSpecialViewFrom;
		JTextField tfSpecialViewUntil;
		
		JScrollPane scrollPane;
		JTable translationTable;
		
		Collection<LocaleInfo> locales;
		ResourceTranslationTableModel tablemodel;

		CustomComponentWizardStep3a() {
			super("nuclos.resplan.wizard.step3a.title", "nuclos.resplan.wizard.step3a.summary");
			final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();

			resEntityComboBox = createJComboBox(30);
			resSortFieldComboBox = createJComboBox(30);
			
			dayOfWeekCheckBox = new JCheckBox(localeDelegate.getText("nuclos.resplan.wizard.step3.dayOfWeek"));
			dayOfWeekCheckBox.setToolTipText(localeDelegate.getText("nuclos.resplan.wizard.step3.dayOfWeek.tooltip"));
			
			withHolidaysCheckBox = new JCheckBox(localeDelegate.getText("nuclos.resplan.wizard.step3.withHolidays"));
			withHolidaysCheckBox.setToolTipText(localeDelegate.getText("nuclos.resplan.wizard.step3.withHolidays.tooltip"));
			withHolidaysCheckBox.addItemListener(this);
			holidaysEntityCombobox = createJComboBox(30);
			holidaysDateCombobox = createJComboBox(30);
			holidaysNameCombobox = createJComboBox(30);
			holidaysTipCombobox = createJComboBox(30);

			tfDefaultViewFrom = new JTextField(10);
			tfDefaultViewUntil = new JTextField(10);
			
			specialViewFromFieldCombobox = createJComboBox(30); 
			specialViewUntilFieldCombobox = createJComboBox(30);
			tfSpecialViewFrom = new JTextField(10);           
			tfSpecialViewUntil = new JTextField(10);          
			
			locales = LocaleDelegate.getInstance().getAllLocales(false);
			tablemodel = new ResourceTranslationTableModel(locales);

			translationTable = new JTable(tablemodel);
			TableCellEditor editor = new ResourceCellEditor();

			for(TableColumn col : CollectionUtils.iterableEnum(translationTable.getColumnModel().getColumns())) {
				col.setCellEditor(editor);
			}

			translationTable.getTableHeader().addMouseListener(new MouseAdapter() {
				@Override
				public void mousePressed(MouseEvent e) {
					stopCellEditing();
				}
			});

			translationTable.setRowHeight(50);
			scrollPane = new JScrollPane(translationTable);
			scrollPane.setSize(new Dimension(540, 180));
			scrollPane.setPreferredSize(new Dimension(540, 180));

			ITableLayoutBuilder tlb = new TableLayoutBuilder(this)
				.columns(PREFERRED, PREFERRED, PREFERRED, PREFERRED, PREFERRED, FILL).gaps(5, 5)
				.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.resourceEntity").add(resEntityComboBox, 2)
				.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.resourceSortField").add(resSortFieldComboBox, 2)
			;
			tlb.newRow(10).add(new JSeparator(), 4);
			tlb.newRow(PREFERRED).
				addLabel(localeDelegate.getMessage("nuclos.resplan.wizard.step4.defaultViewFrom", "Standard Zeitraum von (HEUTE")).
				add(tfDefaultViewFrom).
				add(new JLabel(localeDelegate.getMessage("nuclos.resplan.wizard.step4.defaultViewFrom2", ")"), 2));
			tlb.newRow(PREFERRED).
				addLabel(localeDelegate.getMessage("nuclos.resplan.wizard.step4.defaultViewUntil", "Standard Zeitraum bis (HEUTE")).
				add(tfDefaultViewUntil).
				add(new JLabel(localeDelegate.getMessage("nuclos.resplan.wizard.step4.defaultViewUntil2", ")"), 2));
			tlb.newRow(PREFERRED)
				.addLocalizedLabel("nuclos.resplan.wizard.step4.specialViewFrom")
				.add(specialViewFromFieldCombobox)
				.add(tfSpecialViewFrom);
			tlb.newRow(PREFERRED)
				.addLocalizedLabel("nuclos.resplan.wizard.step4.specialViewUntil")
				.add(specialViewUntilFieldCombobox)
				.add(tfSpecialViewUntil);
			tlb.newRow(PREFERRED).add(new JLabel(" (" + localeDelegate.getMessage("nuclos.resplan.wizard.step4.defaultView", "t=Tag,w=Woche,m=Monat,j=Jahr") + ")"), 4);
			tlb.newRow(PREFERRED);
			tlb.newRow(10).add(new JSeparator(), 4);
			tlb.newRow(PREFERRED)
				.add(dayOfWeekCheckBox);
			tlb.newRow(PREFERRED)
				.add(withHolidaysCheckBox);
			tlb.newRow(PREFERRED)
				.addLocalizedLabel("nuclos.resplan.wizard.step3.holidayEntity").add(holidaysEntityCombobox);
			tlb.newRow(PREFERRED)
				.addLocalizedLabel("nuclos.resplan.wizard.step3.holidaysDate").add(holidaysDateCombobox);
			tlb.newRow(PREFERRED)
				.addLocalizedLabel("nuclos.resplan.wizard.step3.holidaysName").add(holidaysNameCombobox);
			tlb.newRow(PREFERRED)
				.addLocalizedLabel("nuclos.resplan.wizard.step3.holidaysTip").add(holidaysTipCombobox);
			tlb.newRow(PREFERRED);
			tlb.newRow(10).add(new JSeparator(), 4);
			tlb.newRow(FILL).add(scrollPane, 4);
			tlb.newRow(5);
			
			holidaysEntityCombobox.setEnabled(withHolidaysCheckBox.isSelected());
			holidaysDateCombobox.setEnabled(withHolidaysCheckBox.isSelected());
			holidaysNameCombobox.setEnabled(withHolidaysCheckBox.isSelected());
			holidaysTipCombobox.setEnabled(withHolidaysCheckBox.isSelected());
		}

		private JComboBox createJComboBox(int width) {
			JComboBox comboBox = new JComboBox();
			if (width > 0) {
				char ch [] = new char[width]; Arrays.fill(ch, 'x');
				comboBox.setPrototypeDisplayValue(new String(ch));
			}
			comboBox.addItemListener(this);
			return comboBox;
		}


		private void stopCellEditing() {
	        for(TableColumn col : CollectionUtils.iterableEnum(translationTable.getColumnModel().getColumns())) {
	        	TableCellEditor cellEditor = col.getCellEditor();
				if(cellEditor != null) {
	        		cellEditor.stopCellEditing();
				}
	        }
		}
		
		@Override
		public void prepare() {
			final ClientResPlanConfigVO configVO = model.configVO;
			final List<ResourceLocaleVO> resources = new ArrayList<ResourceLocaleVO>();
			for (LocaleInfo locale : locales) {
				boolean found = false;
				if (configVO != null) {
					for (ResourceLocaleVO vo : (List<ResourceLocaleVO>) configVO.getOrMigrateResourceLocales()) {
						if (locale.getLocale().equals(vo.getLocaleId())) {
							found = true;
							resources.add(vo);
						}
					}
				}
				if (!found) {
					ResourceLocaleVO vo = new ResourceLocaleVO();
					vo.setLocaleId(locale.getLocale());
					resources.add(vo);
				}
			}
			tablemodel.setRows(resources);
			
			final List<EntityMeta<?>> lstEntities = new ArrayList<EntityMeta<?>>();
			for (EntityMeta<?> entity : MetaProvider.getInstance().getAllEntities()) {
				if (E.isNuclosEntity(entity.getUID()))
					continue;
				lstEntities.add(entity);
			}
			lstEntities.add(0, EntityUtils.wrapMetaData(EntityMeta.NULL));
			Collections.sort(lstEntities, EntityUtils.getMetaComparator(EntityMeta.class));

			resEntityComboBox.setModel(new ListComboBoxModel<EntityMeta<?>>(lstEntities));
			if (configVO.getResourceEntity() != null)
				resEntityComboBox.setSelectedItem(
						MetaProvider.getInstance().getEntity(configVO.getResourceEntity()));

			configureResourceSortFieldComboBox();
			if (configVO.getResourceSortField() != null)
				resSortFieldComboBox.setSelectedItem(
						MetaProvider.getInstance().getEntityField(configVO.getResourceSortField()));
			
			tfDefaultViewFrom.setText(configVO.getDefaultViewFrom());
			tfDefaultViewUntil.setText(configVO.getDefaultViewUntil());
			
			configureSpecialViewFieldComboboxes();
			specialViewFromFieldCombobox.setSelectedItem(configVO.getSpecialViewFromField() == null
					? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(configVO.getSpecialViewFromField()));
			specialViewUntilFieldCombobox.setSelectedItem(configVO.getSpecialViewUntilField() == null
					? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(configVO.getSpecialViewUntilField()));
			tfSpecialViewFrom.setText(configVO.getSpecialViewFrom());
			tfSpecialViewUntil.setText(configVO.getSpecialViewUntil());
			
			dayOfWeekCheckBox.setSelected(configVO.getDayOfWeek());
			
			withHolidaysCheckBox.setSelected(configVO.getWithHolidays());
			configureHolidaysEntityComboBox();
			configureHolidaysComboBoxes();
			holidaysEntityCombobox.setSelectedItem(configVO.getHolidaysEntity() == null
					? EntityMeta.NULL : MetaProvider.getInstance().getEntity(configVO.getHolidaysEntity()));
			holidaysDateCombobox.setSelectedItem(configVO.getHolidaysDate() == null
					? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(configVO.getHolidaysDate()));
			holidaysNameCombobox.setSelectedItem(configVO.getHolidaysName() == null
					? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(configVO.getHolidaysName()));
			holidaysTipCombobox.setSelectedItem(configVO.getHolidaysTip() == null
					? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(configVO.getHolidaysTip()));
			
			updateState();
		}

		private void configureResourceSortFieldComboBox() {
			final EntityMeta<?> resEntity = (EntityMeta<?>) resEntityComboBox.getSelectedItem();
			final List<FieldMeta<?>> lstFields = new ArrayList<FieldMeta<?>>();
			if (resEntity != null) {
				List<FieldMeta<?>> lstFieldItems = new ArrayList<FieldMeta<?>>();
				for(FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(resEntity.getUID()).values()) {
					//@see NUCLOSINT-1232
					if(field.getForeignEntity() == null
							//&& voField.getLookupEntity() == null
							&& !field.isCalculated()
							&& field.getCalculationScript() == null)
						lstFieldItems.add(EntityUtils.wrapMetaData(field));
				}
				Collections.sort(lstFieldItems, EntityUtils.getMetaComparator(FieldMeta.class));
				lstFieldItems.add(0, EntityUtils.wrapMetaData(FieldMeta.NULL));
				
				lstFields.addAll(lstFieldItems);
			}
			resSortFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(lstFields));
		}
		
		private void configureSpecialViewFieldComboboxes() {
			EntityMeta<?> resEntity = (EntityMeta<?>) resEntityComboBox.getSelectedItem();
			List<FieldMeta<?>> lstFields = new ArrayList<>();
			if (resEntity != null) {
				for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(resEntity.getUID()).values()) {
					if (Date.class.getName().equals(field.getDataType()) || InternalTimestamp.class.getName().equals(field.getDataType())) {
						lstFields.add(EntityUtils.wrapMetaData(field));
					}
					Collections.sort(lstFields, EntityUtils.getMetaComparator(FieldMeta.class));
				}
				lstFields.add(0, EntityUtils.wrapMetaData(FieldMeta.NULL));
			}
			specialViewFromFieldCombobox.setModel(new ListComboBoxModel<FieldMeta<?>>(lstFields));
			specialViewUntilFieldCombobox.setModel(new ListComboBoxModel<FieldMeta<?>>(lstFields));
		}
		
		private void configureHolidaysEntityComboBox() {
			
			holidaysEntityCombobox.setEnabled(withHolidaysCheckBox.isSelected());
			holidaysDateCombobox.setEnabled(withHolidaysCheckBox.isSelected());
			holidaysNameCombobox.setEnabled(withHolidaysCheckBox.isSelected());
			holidaysTipCombobox.setEnabled(withHolidaysCheckBox.isSelected());
			
			if (!withHolidaysCheckBox.isSelected()) {
				return;
			}
			
			EntityMeta<?> selectedEntity = (EntityMeta<?>) holidaysEntityCombobox.getSelectedItem();
			
			final List<EntityMeta<?>> entities = new ArrayList<EntityMeta<?>>();
			for (EntityMeta<?> entity : MetaProvider.getInstance().getAllEntities()) {
				if (E.isNuclosEntity(entity.getUID()))
					continue;
				
				boolean bDateField = false;
				boolean bTextfield = false;
				for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values()) {
					if (!field.isSystemField()) {
						if (Date.class.getName().equals(field.getDataType())
								|| org.nuclos.common2.InternalTimestamp.class.getName().equals(field.getDataType())) {
							bDateField = true;
						}
						if (String.class.getName().equals(field.getDataType())) {
							bTextfield = true;
						}
						if (bDateField && bTextfield) {
							break;
						}
					}
				}
				
				if (bDateField && bTextfield) {
					entities.add(EntityUtils.wrapMetaData(entity));
				}
			}
			
			Collections.sort(entities, EntityUtils.getMetaComparator(EntityMeta.class));
			entities.add(0, EntityMeta.NULL);
			holidaysEntityCombobox.setModel(new ListComboBoxModel<EntityMeta<?>>(entities));
			holidaysEntityCombobox.setSelectedItem(selectedEntity != null ? selectedEntity : EntityMeta.NULL);
		}
		
		private void configureHolidaysComboBoxes() {
			
			final List<FieldMeta<?>> dateFields = new ArrayList<FieldMeta<?>>();
			final List<FieldMeta<?>> textFields = new ArrayList<FieldMeta<?>>();
			EntityMeta<?> entity = (EntityMeta<?>)holidaysEntityCombobox.getSelectedItem();
			if (entity == null || entity.equals(EntityMeta.NULL)) {
					return;
			}
			
			FieldMeta<?> selectedDateField = (FieldMeta<?>) holidaysDateCombobox.getSelectedItem();
			FieldMeta<?> selectedNameField = (FieldMeta<?>) holidaysNameCombobox.getSelectedItem();
			FieldMeta<?> selectedTipField = (FieldMeta<?>) holidaysTipCombobox.getSelectedItem();
			
			for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values()) {
				if (!field.isSystemField()) {
					if (Date.class.getName().equals(field.getDataType())
							|| org.nuclos.common2.InternalTimestamp.class.getName().equals(field.getDataType())) {
						dateFields.add(EntityUtils.wrapMetaData(field));
					}
					if (String.class.getName().equals(field.getDataType())) {
						textFields.add(EntityUtils.wrapMetaData(field));
					}
				}
			}
			
			Collections.sort(dateFields, EntityUtils.getMetaComparator(FieldMeta.class));
			Collections.sort(textFields, EntityUtils.getMetaComparator(FieldMeta.class));
			dateFields.add(0, FieldMeta.NULL);
			textFields.add(0, FieldMeta.NULL);
			
			holidaysDateCombobox.setModel(new ListComboBoxModel<FieldMeta<?>>(dateFields));
			holidaysNameCombobox.setModel(new ListComboBoxModel<FieldMeta<?>>(textFields));
			holidaysTipCombobox.setModel(new ListComboBoxModel<FieldMeta<?>>(textFields));
			
			holidaysDateCombobox.setSelectedItem(selectedDateField != null ? selectedDateField : FieldMeta.NULL);
			holidaysNameCombobox.setSelectedItem(selectedNameField != null ? selectedNameField : FieldMeta.NULL);
			holidaysTipCombobox.setSelectedItem(selectedTipField != null ? selectedTipField : FieldMeta.NULL);
		}
	
		@Override
		public void itemStateChanged(ItemEvent e) {
			ItemSelectable source = e.getItemSelectable();
			if (source == resEntityComboBox) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					final Object obj = e.getItem();
					if(obj instanceof EntityMeta<?>) {
						configureResourceSortFieldComboBox(); // update sortfield combo.
						configureSpecialViewFieldComboboxes();
						
						//now check translation fields.
						final EntityMeta<?> entity = (EntityMeta<?>)obj;
						final Collection<FieldMeta<?>> fields = getDisplayableFields(model.configVO.getResourceEntity());
						/*final List<FieldMeta<?>> lstFields = new ArrayList<FieldMeta<?>>();
						for(FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values()) {
							//@see NUCLOSINT-1232
							if(field.getForeignEntity() == null
									//&& voField.getLookupEntity() == null
									&& !field.isCalculated()
									&& field.getCalculationScript() == null)
								lstFields.add(EntityUtils.wrapMetaData(field));
						}*/

						for (ResourceLocaleVO resLocale : tablemodel.getRows()) {
							final String sLabel = resLocale.getResourceLabel() == null ? "" : resLocale.getResourceLabel();
							if (!StringUtils.isNullOrEmpty(sLabel)) {
								final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, sLabel, new Transformer<String, String>() {
									@Override
									public String transform(String i) {
										for(FieldMeta<?> field : fields) {
											if (field.getUID().getStringifiedDefinition().equals(i))
												return "${" + field.getFieldName() + "}";
										}
										return "";
									}					
								});
								resLocale.setResourceLabel(sFieldUids);
							}
							final String sTooltip = resLocale.getResourceTooltip() == null ? "" : resLocale.getResourceTooltip();
							if (!StringUtils.isNullOrEmpty(sTooltip)) {
								final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, sTooltip, new Transformer<String, String>() {
									@Override
									public String transform(String i) {
										for(FieldMeta<?> field : fields) {
											if (field.getUID().getStringifiedDefinition().equals(i))
												return "${" + field.getFieldName() + "}";
										}
										return "";
									}					
								});
								resLocale.setResourceTooltip(sFieldUids);
							}
						}
					}
				}
			}
			if (source == withHolidaysCheckBox) {
				configureHolidaysEntityComboBox();
			}
			if (source == holidaysEntityCombobox) {
				configureHolidaysComboBoxes();
			}
			updateState();
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			updateState();
		}

		@Override
		protected void updateState() {
			boolean complete = resEntityComboBox.getSelectedItem() != null;
			setComplete(complete);
		}

		@Override
		public void applyState() throws InvalidStateException {
			stopCellEditing();
			model.configVO.setResourceEntity(
					((EntityMeta<?>) resEntityComboBox.getSelectedItem()).getUID());
			if (resSortFieldComboBox.getSelectedItem() != null) {
				model.configVO.setResourceSortField(
					((FieldMeta<?>) resSortFieldComboBox.getSelectedItem()).getUID());
			}

			// translate resourcelabel, etc to uid conform string.
			final Collection<FieldMeta<?>> fields = getDisplayableFields(model.configVO.getResourceEntity());

			for (ResourceLocaleVO resLocale : tablemodel.getRows()) {
				
				resLocale.setResourceLabel(translateLabelToStringifiedUid(resLocale.getResourceLabel(), fields));				
				resLocale.setResourceTooltip(translateLabelToStringifiedUid(resLocale.getResourceTooltip(), fields));
			}
			
			model.configVO.setDefaultViewFrom(tfDefaultViewFrom.getText());
			model.configVO.setDefaultViewUntil(tfDefaultViewUntil.getText());
			
			model.configVO.setSpecialViewFromField(specialViewFromFieldCombobox.getSelectedItem() == null
					? null : ((FieldMeta<?>)specialViewFromFieldCombobox.getSelectedItem()).getUID());
			model.configVO.setSpecialViewUntilField(specialViewUntilFieldCombobox.getSelectedItem() == null
					? null : ((FieldMeta<?>)specialViewUntilFieldCombobox.getSelectedItem()).getUID());
			model.configVO.setSpecialViewFrom(tfSpecialViewFrom.getText());
			model.configVO.setSpecialViewUntil(tfSpecialViewUntil.getText());
			
			model.configVO.setResourceLocales(tablemodel.getRows());
			
			model.configVO.setHolidaysEntity(holidaysEntityCombobox.getSelectedItem() == null
					? null : ((EntityMeta<?>) holidaysEntityCombobox.getSelectedItem()).getUID());
			model.configVO.setHolidaysDate(holidaysDateCombobox.getSelectedItem() == null
					? null : ((FieldMeta<?>) holidaysDateCombobox.getSelectedItem()).getUID());
			model.configVO.setHolidaysName(holidaysNameCombobox.getSelectedItem() == null
					? null : ((FieldMeta<?>) holidaysNameCombobox.getSelectedItem()).getUID());
			model.configVO.setHolidaysTip(holidaysTipCombobox.getSelectedItem() == null
					? null : ((FieldMeta<?>) holidaysTipCombobox.getSelectedItem()).getUID());
			model.configVO.setWithHolidays(withHolidaysCheckBox.isSelected());
			
			model.configVO.setDayOfWeek(dayOfWeekCheckBox.isSelected());
		}
	}
	
	private static Collection<FieldMeta<?>> getDisplayableFields(UID entity) {
		final List<FieldMeta<?>> lstFields = new ArrayList<FieldMeta<?>>();
		for(FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity).values()) {
			//@see NUCLOSINT-1232
			/*if (!field.isCalculated() && field.getCalculationScript() == null) {
				lstFields.add(EntityUtils.wrapMetaData(field));				
			}*/
			lstFields.add(EntityUtils.wrapMetaData(field));
		}
		return lstFields;
	}
	
	public static String translateLabelToStringifiedUid(final String sLabel, final Collection<FieldMeta<?>> fields) {
		if (!StringUtils.isNullOrEmpty(sLabel)) {
			final String sFieldUids = RigidUtils.replaceParameters(RigidUtils.PARAM_PATTERN_$, sLabel, new Transformer<String, String>() {
				@Override
				public String transform(String i) {
					for(FieldMeta<?> field : fields) {
						if (field.getFieldName().equals(i))
							return field.getUID().getStringifiedDefinition();
					}
					return sLabel;
				}					
			});
			return sFieldUids;
		}
		return sLabel == null ? "" : sLabel;
	}

	@SuppressWarnings("serial")
	static class CustomComponentWizardStep3b<PK,R,C extends Collectable<PK>> extends CustomComponentWizardAbstractStep implements ChangeListener, SaveCancelListener {
		JPanel panel;
		JPanel mainPanel;
		
		JScrollPane scrollPane;
		JTable planElementTable;
		PlanElementsTableModel<PK,R,Collectable<PK>> tablemodel;
		
		private JButton btnNewPlanElement;
		private JButton btnDropPlanElement;
		private JButton btnEditPlanElement;

		CustomComponentWizardStep3b() {
			super("nuclos.resplan.wizard.step3b.title", "nuclos.resplan.wizard.step3b.summary");
			double size [][] = {{160,160,160,160,TableLayout.FILL}, {TableLayout.FILL, 25,10}};
			
			this.setLayout(new BorderLayout());
			this.add(panel = new JPanel(), BorderLayout.CENTER);

			TableLayout layout = new TableLayout(size);
			layout.setVGap(3);
			layout.setHGap(5);
			panel.setLayout(layout);

			tablemodel = new PlanElementsTableModel();
			planElementTable = new JTable(tablemodel);
			planElementTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					if (!e.getValueIsAdjusting()) {
						if (planElementTable.getSelectedRowCount() < 1) {
							btnDropPlanElement.setEnabled(false);
							btnEditPlanElement.setEnabled(false);
						} else {
							btnDropPlanElement.setEnabled(true);
							btnEditPlanElement.setEnabled(true);
						}
					}
				}
			});
			scrollPane = new JScrollPane(planElementTable);
			scrollPane.setSize(new Dimension(540, 180));
			scrollPane.setPreferredSize(new Dimension(540, 180));

			mainPanel = new JPanel();
			double sizePanel [][] = {{TableLayout.FILL, 3, 20}, {20,20,3,20,3,TableLayout.FILL}};
			mainPanel.setLayout(new TableLayout(sizePanel));
			mainPanel.add(scrollPane, "0,0, 0,5");
			
			btnNewPlanElement = new JButton(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputplanelement.1", "Planelement hinzuf\u00fcgen"));
			btnNewPlanElement.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					ClientPlanElement<PK,R,Collectable<PK>> elem = ClientResPlanConfigVO.newPlanElement();
					planElementBefore = planElementTable.getSelectedRow() == -1 ? null : new ClientPlanElement<>(tablemodel.getRows().get(planElementTable.getSelectedRow()));
					showPlanElementEditor(elem, MODE_INSERT, -1);
				}
			});

			btnDropPlanElement = new JButton(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputplanelement.2", "Planelement entfernen"));
			btnDropPlanElement.setEnabled(false);
			btnDropPlanElement.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int selected = planElementTable.getSelectedRow();
					if(selected < 0) {
						return;
					}
					model.configVO.getPlanElements().remove(selected);
					tablemodel.fireTableDataChanged();	
				}
			});

			btnEditPlanElement = new JButton(SpringLocaleDelegate.getInstance().getMessage(
					"wizard.step.inputplanelement.3", "Planelement bearbeiten"));
			btnEditPlanElement.setEnabled(false);
			btnEditPlanElement.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int selected = planElementTable.getSelectedRow();
					if(selected < 0) {
						return;
					}

					PlanElement<R> elem = tablemodel.getRows().get(selected);
					planElementBefore = new ClientPlanElement<>(elem);
					showPlanElementEditor(elem, MODE_UPDATE, selected);
				}
			});

			panel.add(mainPanel, new TableLayoutConstraints(0, 0, 4, 0));
			panel.add(btnNewPlanElement, "0,1");
			panel.add(btnDropPlanElement, "2,1");
			panel.add(btnEditPlanElement, "1,1");
			
			planElementTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					btnDropPlanElement.setEnabled(e.getFirstIndex() >= 0);
					btnEditPlanElement.setEnabled(e.getFirstIndex() >= 0);
				}
			});
			
			planElementTable.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if(e.getClickCount() == 2) {
						int selected = planElementTable.getSelectedRow();
						if (selected < 0) {
							return;
						}
						
						PlanElement<R> elem = tablemodel.getRows().get(selected);
						planElementBefore = new ClientPlanElement<>(elem);
						showPlanElementEditor(elem, MODE_UPDATE, selected);
					}
				}
			});
		}

		final private static int MODE_READONLY = 0;
		final private static int MODE_UPDATE = 1;
		final private static int MODE_INSERT = 2;
		
		private PlanElementWizardStep peWStep = null;
		private PlanElement<R> planElementInWork = null;
		private PlanElement<R> planElementBefore = null;
		private int editMode = MODE_READONLY;
		protected void showPlanElementEditor(final PlanElement<R> elem, int editMode, final int row) {
			planElementInWork = elem;
			this.editMode = editMode;
			peWStep = new PlanElementWizardStep(elem, model.configVO, this); 
			StaticModel sModel = new StaticModel();
			sModel.add(peWStep);

			Wizard wizard = new Wizard(sModel);

			wizard.addWizardListener(new WizardListener() {
				@Override
				public void wizardClosed(WizardEvent e) {
					CustomComponentWizardStep3b.this.setComplete(true);
				}
				
				@Override
				public void wizardCancelled(WizardEvent e) {
				}
			});
			
			this.remove(panel);
			this.add(peWStep, BorderLayout.CENTER);
			setComplete(false);
			this.revalidate();
			this.repaint();
		}
		
		private void unShowPlanElementEditor() {
			this.remove(peWStep);
			this.add(panel, BorderLayout.CENTER);
			peWStep = null;
			planElementInWork = null;
			editMode = MODE_READONLY;
			updateState();
			this.revalidate();
			this.repaint();
		}
		
		@Override
		protected boolean isFinishStep() {
			return editMode != MODE_READONLY;
		}
		
		@Override
		public void cancel() {
			if (planElementTable.getSelectedRow() > -1) {
				tablemodel.getRows().set(planElementTable.getSelectedRow(), planElementBefore);
			}
			unShowPlanElementEditor();
		}
		
		@Override
		public void save() {
			if (planElementInWork != null) {
				if (!peWStep.isComplete()) {
					return;
				}
				Collection<FieldMeta<?>> fields = getDisplayableFields(planElementInWork.getEntity());
				
				List<PlanElementLocaleVO> lstPlanElementLocaleVO = planElementInWork.getPlanElementLocaleVO();
				
				if (lstPlanElementLocaleVO != null) {
					for (PlanElementLocaleVO pelVO : lstPlanElementLocaleVO) {
						pelVO.setBookingLabel(translateLabelToStringifiedUid(pelVO.getBookingLabel(), fields));
						pelVO.setBookingTooltip(translateLabelToStringifiedUid(pelVO.getBookingTooltip(), fields));					
					}					
				}

				if (editMode == MODE_INSERT) {
					model.configVO.getPlanElements().add(planElementInWork);					
				}
			}
			unShowPlanElementEditor();			
		}
		
		private void stopCellEditing() {
		}
		
		@Override
		public void prepare() {
			if (peWStep != null) {
				this.remove(peWStep);
				peWStep = null;
				this.add(panel, BorderLayout.CENTER);
			}
			tablemodel.setRows(model.configVO.getOrMigratePlanElements());
			updateState();
		}
		
		List<String> getEntityNames() {
			List<String> entityNames = new ArrayList<String>();
			for (EntityMeta<?> entity : MetaProvider.getInstance().getAllEntities()) {
				if (entity.getEntityName().startsWith("nuclos_") || entity.isDynamic())
					continue;
				entityNames.add(entity.getEntityName());
			}
			Collections.sort(entityNames);
			return entityNames;
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			updateState();
		}

		@Override
		protected void updateState() {
			boolean complete = tablemodel.getRowCount() > 0;
			setComplete(complete);
		}

		@Override
		public void applyState() throws InvalidStateException {
			stopCellEditing();	
		}
	}
	
	static class CustomComponentWizardStep5 extends CustomComponentWizardAbstractStep implements ItemListener {

		// this item is used for comboboxes in order to reset the box to an empty value
		private static final String RESET_ITEM = "";
		
		JCheckBox scriptActiveCheckBox;
		JButton editCodeButton;
		JLabel codeStateLabel;

		CustomComponentCodeEditor codeEditor;
		JComboBox backgroundPaintMethod;
		JComboBox resourceCellMethod;
		JComboBox entryCellMethod;

		CustomComponentWizardStep5() {
			super("nuclos.resplan.wizard.step5.title", "nuclos.resplan.wizard.step5.summary");
			final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();

			codeEditor = new CustomComponentCodeEditor();

			scriptActiveCheckBox = new JCheckBox(localeDelegate.getText("nuclos.resplan.wizard.step5.scriptingActivated", null));
			scriptActiveCheckBox.addItemListener(this);
			codeStateLabel = new JLabel("");
			editCodeButton = new JButton("Skriptcode editieren");
			editCodeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					codeEditor.run();
					codeEditor.compile();
					updateState();
				}
			});

			backgroundPaintMethod = new JComboBox();
			backgroundPaintMethod.setEditable(true);
			backgroundPaintMethod.addItemListener(this);

			resourceCellMethod = new JComboBox();
			resourceCellMethod.setEditable(true);
			resourceCellMethod.addItemListener(this);

			entryCellMethod = new JComboBox();
			entryCellMethod.setEditable(true);
			entryCellMethod.addItemListener(this);

			new TableLayoutBuilder(this)
				.columns(PREFERRED, PREFERRED, FILL).gaps(5, 5)
				.newRow().add(scriptActiveCheckBox, 2)
				.newRow(5)
				.newRow().add(editCodeButton)
				.newRow().addFullSpan(codeStateLabel)
				.newRow(5)
				.newRow().addLocalizedLabel("nuclos.resplan.wizard.step5.ruleFormatBackground", "nuclos.resplan.wizard.step5.ruleFormatBackground.toolTip")
					.add(backgroundPaintMethod, 2)
				.newRow().addLocalizedLabel("nuclos.resplan.wizard.step5.ruleFormatResource", "nuclos.resplan.wizard.step5.ruleFormatResource.toolTip")
					.add(resourceCellMethod, 2)
				.newRow().addLocalizedLabel("nuclos.resplan.wizard.step5.ruleFormatEntry", "nuclos.resplan.wizard.step5.ruleFormatEntryd.toolTip")
					.add(entryCellMethod, 2);
		}

		@Override
		public void prepare() {
			final PlanElement rpEntry = model.configVO.getFirstEntry();
			codeEditor.setCode(StringUtils.emptyIfNull(model.configVO.getScriptingCode()));
			scriptActiveCheckBox.setSelected(model.configVO.isScriptingActivated());
			backgroundPaintMethod.setSelectedItem(model.configVO.getScriptingBackgroundPaintMethod());
			resourceCellMethod.setSelectedItem(model.configVO.getScriptingResourceCellMethod());
			if (rpEntry != null) entryCellMethod.setSelectedItem(rpEntry.getScriptingEntryCellMethod());
			updateState();
		}

		@Override
		public void applyState() throws InvalidStateException {
			final PlanElement rpEntry = model.configVO.getFirstEntry();
			boolean withScripting = scriptActiveCheckBox.isSelected();
			model.configVO.setScriptingActivated(withScripting);
			final CompiledGroovy compiledGroovy = codeEditor.getCompiledGroovy();
			if (withScripting && compiledGroovy == null) {
				invalidStateLocalized(scriptActiveCheckBox, "nuclos.resplan.wizard.step5.scriptError");
			}
			model.configVO.setScriptingCode(StringUtils.nullIfEmpty(codeEditor.getCode()));
			model.configVO.setScriptingBackgroundPaintMethod(getAndCheckMethod(backgroundPaintMethod, withScripting, BackgroundPainter.SCRIPTING_SIGNATURE));
			model.configVO.setScriptingResourceCellMethod(getAndCheckMethod(resourceCellMethod, withScripting, CollectableLabelProvider.SCRIPTING_SIGNATURE));
			if (rpEntry != null) rpEntry.setScriptingEntryCellMethod(getAndCheckMethod(entryCellMethod, withScripting, CollectableLabelProvider.SCRIPTING_SIGNATURE));
		}

		private String getAndCheckMethod(JComboBox comboBox, boolean check, Class<?>... argumentTypes) throws InvalidStateException {
			String methodName = StringUtils.nullIfEmpty((String) comboBox.getSelectedItem());
			if (methodName != null && check) {
				final CompiledGroovy cg = codeEditor.getCompiledGroovy();
				final SortedSet<String> supportedMethods = new TreeSet<String>();
				if (null != cg) {
					supportedMethods.addAll(cg.findMethodNames(argumentTypes));
				}
				if (!supportedMethods.contains(methodName)) {
					if (cg.methodExists(methodName)) {
						invalidStateLocalized(scriptActiveCheckBox, "nuclos.resplan.wizard.step5.scriptMethodIncompatible", methodName);
					} else {
						invalidStateLocalized(scriptActiveCheckBox, "nuclos.resplan.wizard.step5.scriptMethodNotFound", methodName);
					}
				}
			}
			return methodName;
		}

		@Override
		public void itemStateChanged(ItemEvent e) {
			//if (e.getStateChange() == ItemEvent.SELECTED)
				updateState();
		}

		@Override
		protected void updateState() {
			boolean scriptingActivated = scriptActiveCheckBox.isSelected();
			boolean compiled = false;
			final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
			if (scriptingActivated) {
				codeEditor.compile();
				final CompiledGroovy compiledGroovy = codeEditor.getCompiledGroovy();
				compiled = true;
				codeStateLabel.setText(compiled
						? localeDelegate.getText("nuclos.resplan.wizard.step5.scriptOk", null)
						: localeDelegate.getText("nuclos.resplan.wizard.step5.scriptError", null));
				setComplete(compiledGroovy != null);
				final CompiledGroovy cg = codeEditor.getCompiledGroovy();
				replaceModel(backgroundPaintMethod, (null != cg) ? cg.findMethodNames(BackgroundPainter.SCRIPTING_SIGNATURE) : new TreeSet<String>());
				replaceModel(resourceCellMethod, (null != cg) ? cg.findMethodNames(CollectableLabelProvider.SCRIPTING_SIGNATURE) : new TreeSet<String>());
				replaceModel(entryCellMethod, (null != cg) ? cg.findMethodNames(CollectableLabelProvider.SCRIPTING_SIGNATURE) : new TreeSet<String>());
			} else {
				codeStateLabel.setText(localeDelegate.getText("nuclos.resplan.wizard.step5.scriptDisabled", null));
				setComplete(true);
			}
			
			
			backgroundPaintMethod.setEnabled(compiled);
			resourceCellMethod.setEnabled(compiled);
			entryCellMethod.setEnabled(compiled);
			// only enable the editor button if scripting is enabled
			editCodeButton.setEnabled(scriptingActivated);
		}

		private void replaceModel(final JComboBox comboBox, final SortedSet<String> values) {
			/*
			ListComboBoxModel<String> model = new ListComboBoxModel<String>(new ArrayList<String>(values));
			model.setSelectedItem(comboBox.getSelectedItem());
			comboBox.setModel(model);
			*/
			final List<String> newModel = new ArrayList<String>();
			newModel.add(RESET_ITEM);
			newModel.addAll(values);
			
			final ListComboBoxModel<String> model = new ListComboBoxModel<String>(newModel);
			final String selectedItem = (String)comboBox.getSelectedItem();
			if (newModel.contains(selectedItem)) {
				model.setSelectedItem(selectedItem);
			} else {
				model.setSelectedItem(RESET_ITEM);
			}
			comboBox.setModel(model);
		}
	}

	static class CustomComponentCodeEditor extends JPanel {

		private final RuleEditPanel editPanel;
		private CompiledGroovy compiledGroovy;

		public CustomComponentCodeEditor() {
			super(new BorderLayout());
			final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
			
			editPanel = new RuleEditPanel(null);
			editPanel.getJavaEditorPanel().setContentType("text/groovy");

			JToolBar toolBar = new JToolBar();
			toolBar.add(new AbstractAction(localeDelegate.getText("nuclos.resplan.wizard.step5.scriptEditor.compile", null)) {
				@Override
				public void actionPerformed(ActionEvent e) {
					compile();
				}
			});
			toolBar.add(new AbstractAction(localeDelegate.getText("nuclos.resplan.wizard.step5.scriptEditor.close", null)) {
				@Override
				public void actionPerformed(ActionEvent e) {
					compile();
					Window window = SwingUtilities.getWindowAncestor(CustomComponentCodeEditor.this);
					if (window != null)
						window.dispose();
				}
			});

			add(toolBar, BorderLayout.NORTH);
			add(new JScrollPane(editPanel));
		}

		public void run() {
			JDialog dialog = new JDialog(Main.getInstance().getMainFrame(), 
					SpringLocaleDelegate.getInstance().getText("nuclos.resplan.wizard.step5.scriptEditor.title", null));
			dialog.setModal(true);
			dialog.getContentPane().add(this);
			dialog.pack();
			dialog.setLocationByPlatform(true);
			dialog.setVisible(true);
		}
		 
		private final UndoableEditListener undoableEditListener = new UndoableEditListener() {
			@Override
			public void undoableEditHappened(UndoableEditEvent evt) {
				editPanel.getJavaEditorUndoManager().addEdit(evt.getEdit());
			}
		};

		public String getCode() {
			return editPanel.getJavaEditorPanel().getText();
		}

		public void setCode(String code) {
			Document doc1 = editPanel.getJavaEditorPanel().getDocument();
			doc1.removeUndoableEditListener(undoableEditListener);
			
			editPanel.getJavaEditorPanel().setText(code);
			compile();
			
			doc1.addUndoableEditListener(undoableEditListener);
		}

		public void compile() {
			editPanel.clearMessages();
			final String code = getCode().trim();
			if (!StringUtils.isNullOrEmpty(code)) {
				try {
					final GroovySupport support = GroovySupport.getInstance();
					compiledGroovy = support.compile(code);
				} catch (Exception ex) {
					editPanel.setMessages(Arrays.asList(new ErrorMessage(Kind.ERROR, "Skript", ex.getMessage())));
				}
			} else {
				compiledGroovy = null;
			}
		}

		public CompiledGroovy getCompiledGroovy() {
			return compiledGroovy;
		}
	}

	static class CustomComponentWizardStep6 extends CustomComponentWizardAbstractStep {

		CustomComponentWizardStep6() {
			super("nuclos.resplan.wizard.step6.title", "nuclos.resplan.wizard.step6.summary");
			final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();

			add(new JLabel(localeDelegate.getText("nuclos.resplan.wizard.step6.summary", null)));
		}

		@Override
		protected void updateState() {
		}

		@Override
		protected boolean isFinishStep() {
			return true;
		}

		@Override
		public void prepare() {
			setComplete(true);
			model.finish();
		}

		@Override
		public void applyState() throws InvalidStateException {
			model.wizard.close();
		}
	}

	static class LocalTimeSpanPane extends JPanel {

		private final LocalTimeSpanTableModel tableModel;
		private final JTable table;
		private final JToolBar toolBar;
		private final List<ChangeListener> changeListeners = new ArrayList<ChangeListener>();

		public LocalTimeSpanPane() {
			super(new BorderLayout());

			setBorder(BorderFactory.createEtchedBorder());
			tableModel = new LocalTimeSpanTableModel();
			tableModel.addTableModelListener(new TableModelListener() {
				@Override
				public void tableChanged(TableModelEvent e) {
					for (ChangeListener c : changeListeners)
						c.stateChanged(new ChangeEvent(LocalTimeSpanPane.class));
				}
			});
			table = new JTable(tableModel);
			table.setDefaultEditor(Object.class, new LocalTimeSpinnerCellEditor());
//			table.setDefaultRenderer(Object.class, new LocalTimeSpinnerCellEditor());
			table.setBorder(null);
			TableUtils.setOptimalColumnWidths(table);

			toolBar = new JToolBar(JToolBar.VERTICAL);
			toolBar.setFloatable(false);
			toolBar.setBackground(getBackground());
			toolBar.setOpaque(true);
			toolBar.add(new AbstractAction("Add", Icons.getInstance().getIconNew16()) {
				@Override
				public void actionPerformed(ActionEvent e) {
					int row = table.getSelectedRow();
					if (row != -1) {
						row = table.convertRowIndexToModel(row) + 1;
					} else {
						row = table.getRowCount();
					}
					LocalTime time = new LocalTime(0);
					//if (row > 0) {
					//	time = tableModel.getValueAt(row - 1, 1);
					//}
					tableModel.add(row, Pair.makePair(time, time));
					int viewRow = table.convertRowIndexToView(row);
					if (viewRow != -1) {
						table.editCellAt(viewRow, 0);
					}
				}
			});
			toolBar.add(new AbstractAction("Remove", Icons.getInstance().getIconDelete16()) {
				@Override
				public void actionPerformed(ActionEvent e) {
					int row = table.getSelectedRow();
					if (row != -1) {
						boolean canRemove = false;
						TableCellEditor cellEditor = table.getCellEditor();
						if (cellEditor != null)
							canRemove = cellEditor.stopCellEditing();
						if (canRemove)
							tableModel.remove(table.convertRowIndexToModel(row));
					}
				}
			});

			JScrollPane sp = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			sp.setBorder(null);

			add(toolBar, BorderLayout.WEST);
			add(sp);
			setPreferredSize(new Dimension(220, 100));
		}

		public void addChangeListener(ChangeListener listener) {
			changeListeners.add(listener);
		}

		public void removeChangeListener(ChangeListener listener) {
			changeListeners.remove(listener);
		}

		public List<Pair<LocalTime, LocalTime>> getLocalTimeSpans() {
			TableCellEditor cellEditor = table.getCellEditor();
			if (cellEditor != null)
				cellEditor.stopCellEditing();
			return tableModel.getRows();
		}

		public String getText() {
			StringBuilder sb = new StringBuilder();
			for (Pair<LocalTime, LocalTime> p : getLocalTimeSpans()) {
				if (sb.length() > 0)
					sb.append(';');
				sb.append(String.format("%s-%s", p.x, p.y));
			}
			return sb.toString().trim();
		}

		public void setText(String text) {
			tableModel.setRows(ClientResPlanConfigVO.parseTimePeriodsString(text));
		}
	}

	static class LocalTimeSpanTableModel extends AbstractListTableModel<Pair<LocalTime, LocalTime>> {

		public LocalTimeSpanTableModel() {
		}

		@Override
		public List<Pair<LocalTime, LocalTime>> getRows() {
			return super.getRows();
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public String getColumnName(int column) {
			return getSpringLocaleDelegate().getText("nuclos.resplan.wizard.step3.timeSpans." + (column == 0 ? "from" : "until"), null);
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return true;
		}

		@Override
		public LocalTime getValueAt(int rowIndex, int columnIndex) {
			Pair<LocalTime, LocalTime> p = getRow(rowIndex);
			return columnIndex == 0 ? p.x : p.y;
		}

		@Override
		public void setValueAt(Object value, int rowIndex, int columnIndex) {
			LocalTime oldValue = getValueAt(rowIndex, columnIndex);
			if (ObjectUtils.equals(value, oldValue))
				return;
			Pair<LocalTime, LocalTime> p = getRow(rowIndex);
			switch (columnIndex) {
			case 0: p.x = (LocalTime) value; break;
			case 1: p.y = (LocalTime) value; break;
			}
			fireTableCellUpdated(rowIndex, columnIndex);
		}
	}

	static class LocalTimeSpinnerCellEditor extends AbstractCellEditor implements TableCellEditor, TableCellRenderer, ActionListener, ChangeListener {

		private final JSpinner spinner;
		private final JSpinner.DateEditor dateEditor;

		public LocalTimeSpinnerCellEditor() {
			SpinnerDateModel sm = new SpinnerDateModel(new Date(0), null, null, Calendar.HOUR_OF_DAY);
			spinner = new JSpinner(sm);
			spinner.setBorder(null);
			dateEditor = new JSpinner.DateEditor(spinner, "HH:mm");
			dateEditor.getTextField().setHorizontalAlignment(JTextField.LEFT);
			spinner.setEditor(dateEditor);
			spinner.addFocusListener(new FocusListener() {
				@Override
				public void focusGained(FocusEvent e) {
					dateEditor.getTextField().requestFocusInWindow();
				}
				@Override
				public void focusLost(FocusEvent e) {
				}
			});
			dateEditor.getTextField().addActionListener(this);
			spinner.addChangeListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			stopCellEditing();
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			// nothing
		}

		@Override
		public boolean stopCellEditing() {
			try {
				spinner.commitEdit();
				return super.stopCellEditing();
			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		public Object getCellEditorValue() {
			return LocalTime.parse(String.format("%tT", spinner.getValue()));
		}

		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			configure(table, value, false);
			if (dateEditor.getTextField().getText().length() >= 2) {
				dateEditor.getTextField().select(0, 2);
			}
			return spinner;
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			configure(table, value, true);
			return spinner;
		}

		private void configure(JTable table, Object value, boolean focused) {
			spinner.setValue(((LocalTime) value).toDate(new Date(0)));
		}
	}

	static class ResourceCellEditor extends AbstractCellEditor implements TableCellEditor, KeyListener {

		private final JScrollPane scroll;
		private final JTextArea ta;
		private JTable table;

		public ResourceCellEditor() {
			ta = new JTextArea();
			ta.setLineWrap(true);
			ta.setWrapStyleWord(true);
			ta.addFocusListener(NuclosWizardUtils.createWizardFocusAdapter());
			ta.addKeyListener(this);
			scroll = new JScrollPane(ta);
			scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		}

		@Override
		public Object getCellEditorValue() {
			return ta.getText();
		}

		@Override
		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			if (this.table == null) {
				this.table = table;
			}
			String text = LangUtils.defaultIfNull((String)value, "");
	        ta.setText(text);
	        return scroll;
		}

		@Override
		public void keyTyped(KeyEvent e) { }

		@Override
		public void keyPressed(KeyEvent e) {
	        if (e.getKeyCode() == KeyEvent.VK_TAB && !e.isShiftDown() && table != null) {
	            e.consume();

	            int column = table.getEditingColumn();
	            int row = table.getEditingRow();
	            stopCellEditing();
	            if ((column + 1)  >= table.getColumnCount()) {
	                if ((row + 1) >= table.getRowCount()) {
	                	row = -1;
	                }
	                else {
	                    row++;
	                    column = 0;
	                }

	            }
	            else {
	            	column++;
	            }

	            if (row > -1 && column > -1) {
	            	table.changeSelection(row, column, false, false);
	            }
	        }
		}

		@Override
		public void keyReleased(KeyEvent e) { }
	}
}

