//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp.resplan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.SerializationUtils;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.genericobject.CollectableGenericObjectEntity;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFactory;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public abstract class CollectableHelper<PK,R,C extends Collectable<PK>> implements CollectableFactory<C> {

	protected final EntityMeta<PK> entity;

	protected CollectableHelper(EntityMeta<PK> entity) {
		this.entity = entity;
	}

	public UID getEntityUid() {
		return getCollectableEntity().getUID();
	}
	
	public abstract CollectableEntity getCollectableEntity();
	
	public CollectableEntityField findEntityFieldReferencing(final String refEntity) {
		List<CollectableEntityField> refs = CollectionUtils.applyFilter(CollectableUtils.getCollectableEntityFields(getCollectableEntity()),
			new Predicate<CollectableEntityField>() {
				@Override public boolean evaluate(CollectableEntityField f) {
					return f.isReferencing() && refEntity.equals(f.getReferencedEntityUID());
				}
		});
		if (refs.isEmpty()) {
			throw new NuclosFatalException("No reference to " + refEntity + " in " + entity.getEntityName());
		} else if (refs.size() > 1) {
			throw new NuclosFatalException("Ambigiuous reference fields to " + refEntity + " in " + entity.getEntityName());
		}
		return CollectionUtils.getFirst(refs);
	}
	
	public abstract C check(Collectable<PK> clct);
	
	public abstract Collectable<PK> create(Collectable<PK> clct) throws CommonBusinessException;

	public abstract Collectable<PK> get(PK id) throws CommonBusinessException;
	
	public abstract List<? extends Collectable<PK>> get(List<PK> ids) throws CommonBusinessException;
	
	public final List<? extends Collectable<PK>> get() throws CommonBusinessException {
		return get((List<PK>) getIds(new CollectableSearchExpression(null)));
	}
	
	public abstract List<PK> getIds(CollectableSearchExpression searchExpr) throws CommonBusinessException;
	
	public abstract Long getCount(CollectableSearchExpression searchExpr) throws CommonBusinessException;

	public final List<PK> getIds(CollectableSearchCondition cond) throws CommonBusinessException {
		return getIds((new CollectableSearchExpression(cond)));
	}

	public abstract boolean isNewAllowed();

	public abstract boolean isModifyAllowed(Collectable<PK> clct);

	public abstract boolean isRemoveAllowed(Collectable<PK> clct);
	
	@Override
	public abstract C newCollectable();
	
	public abstract C modify(Collectable<PK> clct) throws CommonBusinessException;
	
	public abstract void remove(C clct) throws CommonBusinessException, NuclosBusinessException;
	
	public final C newCollectable(boolean withDefaults) {
		C clct = newCollectable();
		if (withDefaults)
			org.nuclos.client.common.Utils.setDefaultValues(clct, getCollectableEntity());
		return clct;
	}

	/** Copy the given Collectable. The returned Collectable is identical but independant of the given
	 * instance.  You can use the copy if you do not want to modify.
	 */
	public abstract C copyCollectable(C clct);
	
	/** "Clones" the given Collectable. "Clones" in Nuclos means make a copy which can be used
	 * for creating a new record, i.e. the returned Collectable has no id.
	 */
	public abstract C cloneCollectable(C clct) throws CommonBusinessException;

	public abstract CollectController<PK,C> newCollectController() throws CommonBusinessException;
	
	public static CollectableHelper<?,?,?> getForEntity(UID entityUid) {
		return CollectableHelper.<Object,Object,Collectable<Object>>getForEntity(entityUid, false);
	}

	public static <PK2,R2,C2 extends Collectable<PK2>> CollectableHelper<PK2,R2,?> getForEntity(UID entityUid, boolean withDependants) {
		EntityMeta<PK2> entity = (EntityMeta<PK2>) MetaProvider.getInstance().getEntity(entityUid);
		if (entity.isStateModel()) {
			return (CollectableHelper<PK2,R2,C2>) new CollectableGenericObjectHelper(entity);
		} else {
			return new CollectableMasterDataHelper<PK2,R2>(entity, withDependants);
		}
	}
	
	private static class CollectableMasterDataHelper<PK,R> extends CollectableHelper<PK,R,CollectableMasterDataWithDependants<PK>> {
		
		// formal Spring injection
		
		private MasterDataFacadeRemote masterDataFacadeRemote;
		
		// end of former Spring injection

		private final UID entityUid;
		private final CollectableMasterDataEntity collectableEntity;
		private final List<EntityAndField> dependantEntities;

		public CollectableMasterDataHelper(EntityMeta entity, boolean withDependants) {
			super(entity);
			entityUid = entity.getUID();
			collectableEntity = new CollectableMasterDataEntity(MetaProvider.getInstance().getEntity(entityUid));
			if (withDependants) {
				dependantEntities = new ArrayList<EntityAndField>(
					MasterDataDelegate.getInstance().getSubFormEntitiesByMasterDataEntity(entityUid, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY)));
			} else {
				dependantEntities = new ArrayList<EntityAndField>();
			}
			
			setMasterDataFacadeRemote(SpringApplicationContextHolder.getBean(MasterDataFacadeRemote.class));
		}
		
		final void setMasterDataFacadeRemote(MasterDataFacadeRemote masterDataFacadeRemote) {
			this.masterDataFacadeRemote = masterDataFacadeRemote;
		}
		
		final MasterDataFacadeRemote getMasterDataFacadeRemote() {
			return masterDataFacadeRemote;
		}
		
		@Override
		public CollectableMasterDataEntity getCollectableEntity() {
			return collectableEntity;
		}

		@Override
		public CollectableMasterDataWithDependants<PK> check(Collectable<PK> clct) {
			if (clct instanceof CollectableMasterDataWithDependants
				&& ((CollectableMasterDataWithDependants<PK>) clct).getCollectableEntity().getUID().equals(entityUid))
			{
				return (CollectableMasterDataWithDependants<PK>) clct;
			}
			return null;
		}
		
		@Override
		public boolean isNewAllowed() {
			return SecurityCache.getInstance().isWriteAllowedForMasterData(getEntityUid());
		}

		@Override
		public boolean isModifyAllowed(Collectable<PK> clct) {
			return SecurityCache.getInstance().isWriteAllowedForMasterData(getEntityUid());
		}

		@Override
		public boolean isRemoveAllowed(Collectable<PK> clct) {
			return SecurityCache.getInstance().isDeleteAllowedForMasterData(getEntityUid());
		}

		@Override
		public Collectable<PK> create(Collectable<PK> clct) throws CommonBusinessException {
			CollectableMasterDataWithDependants<PK> clctMasterData = (CollectableMasterDataWithDependants<PK>) clct;
			MasterDataVO<PK> createdMdvo = MasterDataDelegate.getInstance().create(
					entity.getUID(), clctMasterData.getMasterDataCVO(), clctMasterData.getDependantMasterDataMap(), ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			return get(createdMdvo.getId());
		}
		
		@Override
		public CollectableMasterDataWithDependants<PK> get(PK id) throws CommonBusinessException {
			return new CollectableMasterDataWithDependants<PK>(collectableEntity,
					MasterDataDelegate.getInstance().getWithDependants(entity.getUID(), id));
		}

		@Override
		public List<PK> getIds(CollectableSearchExpression searchExpr) throws CommonBusinessException {
			if (searchExpr == null)
				searchExpr = new CollectableSearchExpression(null);
			return getMasterDataFacadeRemote().getMasterDataIds(entityUid,searchExpr);
		}

		@Override
		public Long getCount(CollectableSearchExpression searchExpr) throws CommonBusinessException {
			if (searchExpr == null) {
				searchExpr = new CollectableSearchExpression(null);
			}
			return EntityObjectDelegate.getInstance().countEntityObjectRows(entityUid, searchExpr);
		}
		
		@Override
		public List<? extends Collectable<PK>> get(List<PK> ids) throws CommonBusinessException {
			List<MasterDataVO<PK>> lstMdvo = new ArrayList<MasterDataVO<PK>>();
			int start = 0;
			int end = 0;
			while (start < ids.size()) {
				end = start + 5000;
				List<PK> lstPart = new ArrayList<PK>(ids.subList(start, end > ids.size() ? ids.size() : end));
				CollectableSearchCondition clctcond = new CollectableIdListCondition(lstPart);
				CollectableSearchExpression clctexpr = new CollectableSearchExpression(clctcond);
				Set<UID> fields = getFieldUIDs();
				ResultParams resultParams = new ResultParams(fields, Long.valueOf(start), 5000L, true);
				List<MasterDataVO<PK>> lstTemp = getMasterDataFacadeRemote().getMasterDataChunk(entityUid, clctexpr, resultParams);
				lstMdvo.addAll(lstTemp);
				start = end;
			}
			List<MasterDataVO<PK>> list = new ArrayList<>();
			for (int i = 0; i < ids.size(); i++) {
				list.add(null);
			}
			for (MasterDataVO<PK> mdvo : lstMdvo) {
				list.set(ids.indexOf(mdvo.getPrimaryKey()), mdvo);
			}
			return CollectionUtils.transform(
					list,
					new CollectableMasterDataWithDependants.MakeCollectable<PK>(collectableEntity));
		}

		/**
		 * Returns all entity fields in a serializable Set.
		 *
		 * @return
		 */
		private Set<UID> getFieldUIDs() {
			// HashMap$KeySet is not serializable, see http://bugs.java.com/bugdatabase/view_bug.do?bug_id=6750650
			Set<UID> nonserializableSet = MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUid).keySet();

			// New HashSet is serializable
			return new HashSet(nonserializableSet);
		}

		@Override
		public CollectableMasterDataWithDependants<PK> modify(Collectable<PK> clct) throws CommonBusinessException {
			CollectableMasterDataWithDependants<PK> clctMasterData = (CollectableMasterDataWithDependants<PK>) clct;
			MasterDataDelegate.getInstance().update(
					entity.getUID(), clctMasterData.getMasterDataCVO(), clctMasterData.getDependantMasterDataMap(), ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), false);
			return get(clct.getId());
		}
		
		@Override
		public void remove(CollectableMasterDataWithDependants<PK> clct) throws CommonBusinessException, NuclosBusinessException {
			MasterDataVO<PK> mdvo = ((CollectableMasterDataWithDependants<PK>) clct).getMasterDataCVO();
			MasterDataDelegate.getInstance().remove(
					entity.getUID(), mdvo, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		}
		
		@Override
		public CollectableMasterDataWithDependants<PK> newCollectable() {
			return CollectableMasterDataWithDependants.newInstance(
					collectableEntity, new MasterDataVO<PK>(collectableEntity.getMeta(), false));
		}
		
		@Override
		public CollectableMasterDataWithDependants<PK> copyCollectable(CollectableMasterDataWithDependants<PK> clct) {
			CollectableMasterDataWithDependants<PK> clctMasterData = (CollectableMasterDataWithDependants<PK>) clct;
			MasterDataVO<PK> clone = (MasterDataVO<PK>) SerializationUtils.clone(clctMasterData.getMasterDataWithDependantsCVO());
			return new CollectableMasterDataWithDependants<PK>(collectableEntity, clone);
		}
		
		@Override
		public CollectableMasterDataWithDependants<PK> cloneCollectable(CollectableMasterDataWithDependants<PK> clct) throws CommonBusinessException {
			CollectableMasterDataWithDependants<PK> clctMasterData = (CollectableMasterDataWithDependants<PK>) clct;
			HashMap<UID, Object> fieldValues = new HashMap<UID, Object>(clctMasterData.getMasterDataCVO().getFieldValues());
			HashMap<UID, UID> fieldUids = new HashMap<UID, UID>(clctMasterData.getMasterDataCVO().getFieldUids());
			HashMap<UID, Long> fieldIds = new HashMap<UID, Long>(clctMasterData.getMasterDataCVO().getFieldIds());
			
			return CollectableMasterDataWithDependants.newInstance(clctMasterData.getCollectableEntity(),
					new MasterDataVO<PK>(entity.getUID(), null, null, null, null, null, null, fieldValues, fieldIds, fieldUids, entity.isUidEntity()));
		}
		
		@Override
		public MasterDataCollectController<PK> newCollectController() throws CommonBusinessException {
			return NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(
					entity.getUID(), null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		}

	}

	private static class CollectableGenericObjectHelper<R> extends CollectableHelper<Long,R,CollectableGenericObjectWithDependants> {
		
		// former Spring injection
		
		private GenericObjectFacadeRemote genericObjectFacadeRemote;
		
		// end of former Spring injection

		private final UID moduleUid;
		private final CollectableGenericObjectEntity collectableEntity;

		public CollectableGenericObjectHelper(EntityMeta<Long> entity) {
			super(entity);
			moduleUid = entity.getUID();
			collectableEntity = (CollectableGenericObjectEntity) CollectableGenericObjectEntity.getByModuleUid(moduleUid);
			
			setGenericObjectFacadeRemote(SpringApplicationContextHolder.getBean(GenericObjectFacadeRemote.class));
		}
		
		final void setGenericObjectFacadeRemote(GenericObjectFacadeRemote genericObjectFacadeRemote) {
			this.genericObjectFacadeRemote = genericObjectFacadeRemote;
		}

		final GenericObjectFacadeRemote getGenericObjectFacadeRemote() {
			return genericObjectFacadeRemote;
		}

		@Override
		public CollectableGenericObjectEntity getCollectableEntity() {
			return collectableEntity;
		}

		@Override
		public CollectableGenericObjectWithDependants check(Collectable<Long> clct) {
			if (clct instanceof CollectableGenericObjectWithDependants) {
				CollectableGenericObjectWithDependants clctWd = (CollectableGenericObjectWithDependants) clct;
				if (LangUtils.equal(clctWd.getGenericObjectCVO().getModule(), moduleUid)) {
					return clctWd;
				}
			}
			return null;
		}

		@Override
		public boolean isNewAllowed() {
			return SecurityCache.getInstance().isNewAllowedForModule(getEntityUid());
		}

		@Override
		public boolean isModifyAllowed(Collectable<Long> clct) {
			Long id = (clct != null) ? clct.getId() : null;
			return SecurityCache.getInstance().isWriteAllowedForModule(getEntityUid(), id);
		}

		@Override
		public boolean isRemoveAllowed(Collectable<Long> clct) {
			Long id = (clct != null) ? clct.getId() : null;
			return SecurityCache.getInstance().isDeleteAllowedForModule(getEntityUid(), id, false);
		}

		@Override
		public Collectable<Long> create(Collectable<Long> clct) throws CommonBusinessException {
			GenericObjectWithDependantsVO gowdvo = ((CollectableGenericObjectWithDependants) clct).getGenericObjectWithDependantsCVO();
			Long id = getGenericObjectFacadeRemote().create(gowdvo, ClientParameterProvider.getInstance().getValue(
					ParameterProvider.KEY_LAYOUT_CUSTOM_KEY)).getId();
			return get(id);
		}

		@Override
		public CollectableGenericObjectWithDependants get(Long id) throws CommonBusinessException {
			GenericObjectWithDependantsVO gowdvo = getGenericObjectFacadeRemote().getWithDependants(
					null, id, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			return new CollectableGenericObjectWithDependants(gowdvo);
		}
		
		@Override
		public List<Long> getIds(CollectableSearchExpression searchExpr) throws CommonBusinessException {
			if (searchExpr == null)
				searchExpr = new CollectableSearchExpression(null);
			return getGenericObjectFacadeRemote().getGenericObjectIds(moduleUid, searchExpr);
		}
		
		@Override
		public List<? extends Collectable<Long>> get(final List<Long> ids) throws CommonBusinessException {
			List<Long> intIds = CollectionUtils.typecheck(ids, Long.class);
			Collection<GenericObjectWithDependantsVO> gowdvos = 
					getGenericObjectFacadeRemote().getGenericObjectsMore(
							moduleUid, intIds, Collections.<UID>emptySet(),
							ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			List<Collectable<Long>> result = new ArrayList<Collectable<Long>>(gowdvos.size());
			for (GenericObjectWithDependantsVO gowdvo : gowdvos) {
				result.add(new CollectableGenericObjectWithDependants(gowdvo));
			}
//			Collections.sort(result, new Comparator<Collectable<Long>>() {
//				@Override
//				public int compare(Collectable<Long> c1, Collectable<Long> c2) {
//					int id1Index = ids.indexOf(c1.getId());
//					int id2Index = ids.indexOf(c2.getId());
//					return id1Index < id2Index ? -1 : (id1Index == id2Index ? 0 : 1);
//				}
//			});
			List<Collectable<Long>> list = new ArrayList<>();
			for (int i = 0; i < ids.size(); i++) {
				list.add(null);
			}
			for (Collectable<Long> clct : result) {
				list.set(ids.indexOf(clct.getPrimaryKey()), clct);
			}
			return list;
		}

		@Override
		public CollectableGenericObjectWithDependants modify(Collectable<Long> clct) throws CommonBusinessException {
			GenericObjectWithDependantsVO gowdvo = ((CollectableGenericObjectWithDependants) clct).getGenericObjectWithDependantsCVO();
			getGenericObjectFacadeRemote().modify(moduleUid, gowdvo, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			return get(clct.getId());
		}
		
		@Override
		public void remove(CollectableGenericObjectWithDependants clct) throws CommonBusinessException, NuclosBusinessException {
			GenericObjectWithDependantsVO gowdvo = ((CollectableGenericObjectWithDependants) clct).getGenericObjectWithDependantsCVO();
			getGenericObjectFacadeRemote().remove(gowdvo.getModule(), gowdvo.getPrimaryKey(), false, 
					ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		}

		@Override
		public CollectableGenericObjectWithDependants newCollectable() {
			GenericObjectVO govo = new GenericObjectVO(moduleUid, null);
			return CollectableGenericObjectWithDependants.newCollectableGenericObject(govo);
		}
		
		@Override
		public CollectableGenericObjectWithDependants copyCollectable(CollectableGenericObjectWithDependants clct) {
			CollectableGenericObjectWithDependants clctGenericObject = (CollectableGenericObjectWithDependants) clct;
			GenericObjectWithDependantsVO clone = (GenericObjectWithDependantsVO) SerializationUtils.clone(clctGenericObject.getGenericObjectWithDependantsCVO());
			return new CollectableGenericObjectWithDependants(clone);
		}
		
		@Override
		public CollectableGenericObjectWithDependants cloneCollectable(CollectableGenericObjectWithDependants clct) throws CommonBusinessException {
			CollectableGenericObjectWithDependants clctGenericObject = (CollectableGenericObjectWithDependants) clct;
			GenericObjectVO clonedGovo = clctGenericObject.getGenericObjectCVO().copy();
			return CollectableGenericObjectWithDependants.newCollectableGenericObject(clonedGovo);
		}

		@Override
		public GenericObjectCollectController newCollectController() throws CommonBusinessException {
			return NuclosCollectControllerFactory.getInstance().newGenericObjectCollectController(moduleUid, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		}

		@Override
		public Long getCount(CollectableSearchExpression searchExpr) throws CommonBusinessException {
			if (searchExpr == null) {
				searchExpr = CollectableSearchExpression.TRUE_SEARCH_EXPR;
			}
			return EntityObjectDelegate.getInstance().countEntityObjectRows(moduleUid, searchExpr);
		}

	}
}
