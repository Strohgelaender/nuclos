//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.customcomp.resplan;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Future;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXFindPanel;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.jdesktop.swingx.renderer.ComponentProvider;
import org.jdesktop.swingx.search.SearchFactory;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.KeyBindingProvider;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.customcomp.resplan.ResPlanController.RestorePreferences;
import org.nuclos.client.customcomp.resplan.ResPlanController.TimeGranularity;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.SearchFilterListCellRenderer;
import org.nuclos.client.ui.*;
import org.nuclos.client.ui.collect.CollectController.CollectableEventListener;
import org.nuclos.client.ui.collect.CollectController.MessageType;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.popupmenu.AbstractJPopupMenuListener;
import org.nuclos.client.ui.resplan.JResPlanComponent;
import org.nuclos.client.ui.resplan.JResPlanComponent.Area;
import org.nuclos.client.ui.resplan.JResPlanComponent.CellView;
import org.nuclos.client.ui.resplan.JResPlanComponent.EntryCellView;
import org.nuclos.client.ui.resplan.ResPlanModel;
import org.nuclos.client.ui.resplan.header.JHeaderGrid;
import org.nuclos.client.ui.util.Orientation;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common.preferences.PlanningTablePreferences;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.interval.GranularityType;
import org.nuclos.common2.interval.Interval;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterUserVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

public class ResPlanPanel<PK, R extends Collectable<PK>> extends JPanel {
	
	private static final Logger LOG = Logger.getLogger(ResPlanPanel.class);

	public static final String TIME_HORIZON_PROPERTY = "timeHorizon";
	
	public static final String OWN_BOOKINGS_PROPERTY = "ownBookings";

	public static final String SEARCH_CONDITION_PROPERTY = "searchCondition";
	
	//
	private ResPlanController<PK,R> controller;

	private DateChooser startDateChooser;
	private JLabel startDateLabel;
	private DateChooser endDateChooser;
	private JLabel endDateLabel;

	private JResPlanComponent<PK, R, Date, Collectable<PK>, Collectable<PK>> resPlan;
	protected CollectableResPlanModel<PK,R> resPlanModel;
	private DateTimeModel timeModel;
	private Map<Date, List<String>> holidays;

	private CollectableSearchCondition searchCondition;
	private Interval<Date> timeHorizon;

	private List<EntitySearchFilter> entitySearchFilters;
	private JComboBox searchFilterComboBox;
	private JLabel searchFilterLabel;
	private ListComboBoxModel<ResPlanController.TimeGranularity> timeGranularityModel;
	private JComboBox timeGranularityComboBox;
	private JScrollPane scrollPane;
	private final JButton infoButton;
	private JLabel numberOfRessources;
	private JCheckBox ownBookingsCheckBox;

	private EntitySearchFilter lastSelectedFilter;

	private List<String> infoMessages;
	private long infoMessageLastModified;
	Timer infoMessageTimer;
	Bubble infoMessagesBubble;

	// Last known user-specified extents
	private Dimension userResourceCellExtent = new Dimension(-1, -1);
	private Dimension userTimelineCellExtent = new Dimension(-1, -1);
	private Dimension userResourceHeaderExtent = new Dimension(-1,-1);
	private Dimension userTimelineHeaderExtent = new Dimension(-1,-1);
	
	public ResPlanPanel(ResPlanController<PK,R> cntrl, CollectableResPlanModel<PK,R> model, DateTimeModel timeModel, Map<Date, List<String>> holidays, boolean editable) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		
		this.controller = cntrl;
		this.timeModel = timeModel;
		this.resPlanModel = model;
		this.holidays = holidays;
		setLayout(new BorderLayout());

		JToolBar tb = UIUtils.createNonFloatableToolBar();
		tb.setFloatable(false);
		Action refreshAction = new AbstractAction(localeDelegate.getText("nuclos.resplan.action.refresh"), 
				Icons.getInstance().getIconRefresh16()) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.refresh(true);
			}
		};
		refreshAction.putValue(AbstractAction.SHORT_DESCRIPTION, localeDelegate.getText("nuclos.resplan.action.refresh"));
		tb.add(refreshAction);
		exportAction.putValue(AbstractAction.SHORT_DESCRIPTION, localeDelegate.getText("nuclos.resplan.action.export"));
		tb.add(exportAction);
		tb.addSeparator();
		
		this.timeHorizon = new Interval<Date>(model.getDefaultViewFrom(), model.getDefaultViewUntil());

		final LabeledComponentSupport support = new LabeledComponentSupport();
		startDateChooser = new DateChooser(support, timeHorizon.getStart());
		startDateChooser.setMinimumSize(startDateChooser.getPreferredSize());
		startDateChooser.setMaximumSize(startDateChooser.getPreferredSize());
		endDateChooser = new DateChooser(support, timeHorizon.getEnd());
		endDateChooser.setMinimumSize(endDateChooser.getPreferredSize());
		endDateChooser.setMaximumSize(endDateChooser.getPreferredSize());

		startDateLabel = new JLabel(localeDelegate.getText("nuclos.resplan.toolbar.from"));
		tb.add(startDateLabel);
		tb.add(startDateChooser);
		tb.add(Box.createHorizontalStrut(5));
		endDateLabel = new JLabel(localeDelegate.getText("nuclos.resplan.toolbar.until"));
		tb.add(endDateLabel);
		tb.add(endDateChooser);

		timeGranularityModel = new ListComboBoxModel<ResPlanController.TimeGranularity>(controller.getTimeGranularityOptions());
		tb.addSeparator();
		tb.add(new JLabel(localeDelegate.getText("nuclos.resplan.toolbar.granularity")));
		timeGranularityComboBox = new JComboBox(timeGranularityModel);
		tb.add(timeGranularityComboBox);
		timeGranularityComboBox.setMaximumSize(Orientation.VERTICAL.updateExtent(timeGranularityComboBox.getPreferredSize(), 20));

		tb.addSeparator();
		searchFilterLabel = new JLabel(localeDelegate.getText("nuclos.resplan.toolbar.resourceFilter"));
		tb.add(searchFilterLabel);
		searchFilterComboBox = new JComboBox();
		searchFilterComboBox.setRenderer(new SearchFilterListCellRenderer());
		refreshSearchFilter();
		tb.add(searchFilterComboBox);
		searchFilterComboBox.setMaximumSize(Orientation.VERTICAL.updateExtent(searchFilterComboBox.getPreferredSize(), 20));

		tb.addSeparator();
		ownBookingsCheckBox = new JCheckBox(localeDelegate.getText("nuclos.resplan.toolbar.ownBookings"), false);
		ownBookingsCheckBox.setVisible(false);
		ownBookingsCheckBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				toggleOwnBookings();
			}
		});
		tb.add(ownBookingsCheckBox);
		//tb.add(ownBookingsLabel);
		
		tb.add(Box.createGlue());

		infoButton = new JButton(infoAction);
		infoButton.setVisible(true);
		infoButton.setEnabled(false);
		tb.add(infoButton);
		
		
		numberOfRessources = new JLabel();
		numberOfRessources.setVisible(true);
		tb.add(numberOfRessources);

		tb.add(Box.createHorizontalStrut(3));

		initJResPlan(editable);

		ActionListener dateChooserListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				timeHorizonChanged(true);
			}
		};
		startDateChooser.addActionListener(dateChooserListener);
		endDateChooser.addActionListener(dateChooserListener);

		timeGranularityComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Runnable runnable = createScrollToCurrentAreaRunnable();
					ResPlanController.TimeGranularity granularity = timeGranularityModel.getSelectedItem();
					resPlan.setTimeModel(granularity.getTimeModel());
					resPlan.getTimelineHeader().setCategoryModel(granularity.getHeaderCategories());
					SwingUtilities.invokeLater(runnable);
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							try {
								controller.storeSharedState();
							} catch (PreferencesException | CommonFinderException | CommonPermissionException e) {
								LOG.warn("storeSharedState failed: " + e, e);
							}
							controller.calcAndAdjustHeight(null);
							ResPlanPanel.this.getResPlan().getTimelineHeader().setPreferredSize(null);
							ResPlanPanel.this.getResPlan().getResourceHeader().setPreferredSize(null);
							ResPlanPanel.this.getResPlan().getTimelineHeader().setDefaultHeaderExtent();
						}
					});
				}
			}
		});
		searchFilterComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					lastSelectedFilter = (EntitySearchFilter) e.getItem();
				} else if (e.getStateChange() == ItemEvent.SELECTED) {
					EntitySearchFilter filter = (EntitySearchFilter) searchFilterComboBox.getSelectedItem();
					if (filter instanceof NewCustomSearchFilter) {
						runCustomSearch();
						return;
					}
					setSearchCondition(filter.getSearchCondition());
				}
			}
		});
		
		getResPlan().getTimelineHeader().addPropertyChangeListener(new HeaderChangeListener());
		getResPlan().getResourceHeader().addPropertyChangeListener(new HeaderChangeListener());

		scrollPane = new JScrollPane(resPlan);

		JButton corner = new JButton(switchOrientationAction);
//		JButton corner = new JButton();
		corner.setBorderPainted(false);
		scrollPane.setCorner(JScrollPane.LOWER_RIGHT_CORNER, corner);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		add(tb, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);

		if (StringUtils.isNullOrEmpty(cntrl.getConfigVO().getSpecialViewFrom()) && StringUtils.isNullOrEmpty(cntrl.getConfigVO().getSpecialViewUntil())) {
			resPlan.setTimeHorizon(this.timeHorizon, true);
			resPlan.invalidate();
		}

		setFocusable(true);
		setFocusCycleRoot(true);
		getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("DELETE"), "delete");
		//getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("control F"), "find");
		getActionMap().put("delete", removeAction);
		//getActionMap().put("find", findAction);
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.ACTIVATE_SEARCH_PANEL_2, findAction, this);
	}
	
	public JResPlanComponent<PK, R, Date, Collectable<PK>, Collectable<PK>> getResPlan() {
		return resPlan;
	}
	
	ResPlanController<PK,R> getController() {
		return controller;
	}

	public ListComboBoxModel<EntitySearchFilter> getSearchFilterModel() {
		return (ListComboBoxModel<EntitySearchFilter>) searchFilterComboBox.getModel();
	}

	public void refreshSearchFilter() {
		this.refreshSearchFilter(controller.getSearchFilters());
	}

	public void refreshSearchFilter(List<EntitySearchFilter> searchFilters) {
		if (searchFilters == null) {
			System.out.println("ERROR: SEARCHFILTER == NULL!");
			return;
		}
		Object selectedItem = searchFilterComboBox.getSelectedItem();
		entitySearchFilters = searchFilters;
		
		List<EntitySearchFilter> filters = new ArrayList<EntitySearchFilter>(entitySearchFilters);
		if (searchFilterComboBox.getModel() != null) {
			for (int i = 0; i < searchFilterComboBox.getModel().getSize(); i++) {
				EntitySearchFilter filter = (EntitySearchFilter)searchFilterComboBox.getModel().getElementAt(i);
				if (filter instanceof CustomSearchFilter) {
					filters.add(filters.size() - 2, filter);
				}
			}
		}
		searchFilterComboBox.setModel(new ListComboBoxModel<EntitySearchFilter>(filters));
		searchFilterComboBox.setMaximumSize(Orientation.VERTICAL.updateExtent(searchFilterComboBox.getPreferredSize(), 20));
		if (selectedItem != null)
			searchFilterComboBox.setSelectedItem(selectedItem);
	}

	public CollectableSearchCondition getSearchCondition() {
		return cleanSearchCondition(searchCondition);
	}

	void setSearchCondition(CollectableSearchCondition newSearchCondition) {
		CollectableSearchCondition oldSearchCondition = this.searchCondition;
		if (!ObjectUtils.equals(oldSearchCondition, newSearchCondition)) {
			this.searchCondition = newSearchCondition;
			firePropertyChange(SEARCH_CONDITION_PROPERTY, oldSearchCondition, newSearchCondition);
		}
	}
	

	private CollectableSearchCondition cleanSearchCondition(CollectableSearchCondition cond) {
		if (cond instanceof CompositeCollectableSearchCondition) {
			List<CollectableSearchCondition> lstOperands = ((CompositeCollectableSearchCondition)cond).getOperands();
			List<CollectableSearchCondition> lstCleanOperands = new ArrayList<CollectableSearchCondition>();
			for (CollectableSearchCondition operand : lstOperands) {
				lstCleanOperands.add(cleanSearchCondition(operand));
			}
			return new CompositeCollectableSearchCondition(((CompositeCollectableSearchCondition) cond).getLogicalOperator(), lstCleanOperands);
		} else {
			if (cond instanceof CollectableInCondition) {
				List<String> lstComparands = ((CollectableInCondition<String>)cond).getInComparands();
				List<String> lstCleanComparands = new ArrayList<>();
				for (String comparand : lstComparands) {
					if (comparand.startsWith("'") && comparand.endsWith("'")) {
						lstCleanComparands.add(comparand.substring(1, comparand.length() - 1));
					} else {
						lstCleanComparands.add(comparand);
					}
				}
				return new CollectableInCondition<>(((CollectableInCondition) cond).getEntityField(), lstCleanComparands);
			} else {
				return cond;
			}
		}
	}

	public String getSearchFilter() {
		EntitySearchFilter filter = (EntitySearchFilter) searchFilterComboBox.getSelectedItem();
		if (filter == null || filter.getSearchCondition() == null || filter instanceof CustomSearchFilter) {
			return null;
		}
		return filter.getSearchFilterVO().getFilterName();
	}

	public void setSearchFilter(String name) {
		for (EntitySearchFilter filter : entitySearchFilters) {
			if ((name == null && filter.isDefaultFilter()) || name.equals(filter.getSearchFilterVO().getFilterName())) {
				searchFilterComboBox.setSelectedItem(filter);
				return;
			}
		}
	}

	public void setCustomSearchFilter(CollectableSearchCondition searchCondition) {
		CustomSearchFilter customFilter = new CustomSearchFilter(searchCondition);
		List<EntitySearchFilter> filters = new ArrayList<EntitySearchFilter>(entitySearchFilters);
		filters.add(filters.size() - 2, customFilter);
		searchFilterComboBox.setModel(new ListComboBoxModel<EntitySearchFilter>(filters));
		searchFilterComboBox.setSelectedItem(customFilter);
		// -> fires event -> setSearchCondition
	}

	public GranularityType getTimeGranularity() {
		ResPlanController.TimeGranularity granularity = timeGranularityModel.getSelectedItem();
		return (granularity != null) ? granularity.getType() : null;
	}

	public void setTimeGranularity(GranularityType granularity) {
		for (int i = 0; i < timeGranularityModel.getSize(); i++) {
			ResPlanController.TimeGranularity option = timeGranularityModel.getElementAt(i);
			if (option.getType() == granularity) {
				timeGranularityModel.setSelectedItem(option);
				break;
			}
		}
	}

	public Interval<Date> getTimeHorizon() {
		return timeHorizon;
	}

	public void setTimeHorizon(Interval<Date> newTimeHorizon, boolean notify) {
		Interval<Date> oldTimeHorizon = this.timeHorizon;
		if (!ObjectUtils.equals(oldTimeHorizon, newTimeHorizon)) {
			this.timeHorizon = newTimeHorizon;
			startDateChooser.setDate(newTimeHorizon.getStart());
			endDateChooser.setDate(newTimeHorizon.getEnd());
			resPlan.setTimeHorizon(timeHorizon, notify);
			if (notify) {
				firePropertyChange(TIME_HORIZON_PROPERTY, oldTimeHorizon, newTimeHorizon);
			}
		}
	}
	
	private void timeHorizonChanged(boolean notify) {
		try {
			Date startDate = startDateChooser.getDate();
			Date endDate = endDateChooser.getDate();
			setTimeHorizon(new Interval<Date>(startDate, endDate, true), notify);
		} catch (CommonValidationException ex) {
			Errors.getInstance().showExceptionDialog(ResPlanPanel.this, ex);
			return;
		}
	}
	
	private void toggleOwnBookings() {
		firePropertyChange(OWN_BOOKINGS_PROPERTY, !ownBookingsCheckBox.isSelected(), ownBookingsCheckBox.isSelected());
	}
	
	public boolean getOwnBookings() {
		return ownBookingsCheckBox.isSelected();
	}
	
	public void setOwnBookings(boolean own) {
		ownBookingsCheckBox.setSelected(own);
	}

	public void setInfoMessages(List<String> messages, final boolean show) {
		if (System.currentTimeMillis() - infoMessageLastModified < 500) {
			if (messages != null && !messages.isEmpty()) {
				this.infoMessages.addAll(0, Arrays.asList("------------------------------"));
				this.infoMessages.addAll(0, messages);
				infoMessageLastModified = System.currentTimeMillis();
			}
		} else {
			this.infoMessages = messages == null || messages.isEmpty() ? null : messages;
			infoMessageLastModified = System.currentTimeMillis();
		}
		if (infoMessageTimer != null) {
			infoMessageTimer.cancel();
		}
		infoMessageTimer = new Timer();
		infoMessageTimer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				infoButton.setEnabled(infoMessages != null);
				if (show)
					showInfoMessages();
			}
		}, 500);
	}

	private void showInfoMessages() {
		if (infoMessages != null && infoButton.isEnabled()) {
			StringBuilder sb = new StringBuilder("<html>");
			for (String message : infoMessages) {
				String escapedMessage = StringUtils.xmlEncode(message).replaceAll("\n", "<br/>");
				sb.append("<p>").append(escapedMessage).append("</p>");
			}
			final String message = sb.toString();
			EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						infoMessagesBubble = new Bubble(infoButton, message, 8, BubbleUtils.Position.SW);
						infoMessagesBubble.setVisible(true);
					}
					catch (Exception e) {
						LOG.error("showInfoMessages failed: " + e, e);
					}
				}
			});
		}
	}
	
	public void showOwnBookingsCheckbox(boolean show) {
		ownBookingsCheckBox.setVisible(show);
	}
	
	public void setNumberOfResources(Integer number) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		this.numberOfRessources.setText(number.toString() + " " + localeDelegate.getText("nuclos.resplan.toolbar.resources"));
	}

	private void initJResPlan(boolean editable) {
		resPlan = new JResPlanComponent<PK, R, Date, Collectable<PK>, Collectable<PK>>(resPlanModel, timeModel, holidays, editable);
		resPlan.getTimelineHeader().setCategoryModel(timeGranularityModel.getSelectedItem());
		resPlan.addMouseListener(new AbstractJPopupMenuListener() {
			@Override
			protected JPopupMenu getJPopupMenu(MouseEvent evt) {
				JPopupMenu popupMenu = new JPopupMenu();
				Point pt = evt.getPoint();

				Area<R, Date> blankSelection = resPlan.getSelectedBlankArea();
				if (blankSelection != null) {
				    addActionsToPopup(popupMenu, blankSelection.getResource(), blankSelection.getInterval());
				} else {
				    addActionsToPopup(popupMenu, resPlan.getResourceAt(pt), resPlan.getTimeIntervalAt(pt));
				}

				final List<CellView<PK,R,Date,Collectable<PK>,Collectable<PK>>> selectedEntries =
						resPlan.getSelectedCellViews();				
				List<Collectable<PK>> selectedRelations = selectRelationsForEvent(pt);
				if (resPlan.isEditable() && (!selectedEntries.isEmpty() || !selectedRelations.isEmpty())) {
					JMenuItem menuItem = popupMenu.add(removeAction);
					boolean enabled = true;
					List<Collectable<PK>> clcts = new ArrayList<>();
					for (CellView<PK,R,Date,Collectable<PK>,Collectable<PK>> cellView : selectedEntries) {
						Collectable<PK> clct = (Collectable<PK>) cellView.getEntry();
						ClientPlanElement<PK,R,Collectable<PK>> pElem = cellView.getPlanElement();
						if (!MetaProvider.getInstance().getEntity(pElem.getEntity()).isEditable() || !resPlanModel.isRemoveEntryAllowed(clct, pElem)) {
							enabled = false;
							break;
						}
						if (cellView instanceof EntryCellView) {
							clcts.add(clct);
						}
					}
					addCustomRuleActions(clcts, popupMenu);
					for (Collectable<PK> clct : selectedRelations) {
						if (!resPlanModel.isRemoveRelationAllowed(clct)) {
							enabled = false;
							break;
						}
					}
					// Note: just change the state of the menu item (and leave the action as is)
					menuItem.setEnabled(enabled);
				}
				if (!selectedEntries.isEmpty() || !selectedRelations.isEmpty()) {
					boolean addDetailsAction = false; 
					for (CellView cellView : selectedEntries) {
						try {
							MasterDataLayoutHelper.checkLayoutMLExistence(cellView.getPlanElement().getEntity(), false);
							addDetailsAction = true;
						} catch (NuclosBusinessException e) {
							//layout does not exist, do nothin
						}
					}
					if (addDetailsAction) {
						if (selectedEntries.size() > 1) {
							detailsAction.setEnabled(false);
						} else {
							detailsAction.setEnabled(true);
						}
						popupMenu.add(detailsAction);
					}
				}
				
				if (selectedEntries.size() == 1 && resPlanModel.isCreateRelationAllowed()) {
					popupMenu.addSeparator();
					if (resPlan.getRelateBegin() != null) {
						Collectable<PK> to = selectedEntries.get(0).getEntry();
						if (to != resPlan.getRelateBegin()) {
							popupMenu.add(relateFinishAction);
						}
					}
					popupMenu.add(relateBeginAction);
				}

				return popupMenu;
			}

			private List<Collectable<PK>> selectRelationsForEvent(Point pt) {
				List<Collectable<PK>> selection = resPlan.getSelectedRelations();
				Collectable<PK> relAt = resPlan.getRelationAt(pt);
				if (relAt != null && (selection.isEmpty() || !selection.contains(relAt))) {
					selection = Collections.singletonList(relAt);
					resPlan.setSelectedRelations(selection);
				}
				return selection;
			}

			@Override
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					Point p = evt.getPoint();
					CellView<PK,R,Date,Collectable<PK>,Collectable<PK>> view = resPlan.getCellViewAt(p);
					if (view != null) {
						Collectable<PK> clct = (Collectable<PK>) view.getEntry();
						PlanElement<R> pElem = view.getPlanElement();
						runDetailsCollectable(pElem.getEntity(), clct);						
					} else {
						Collectable<PK> clct = resPlan.getRelationAt(p);
						if (clct != null) {
							runDetailsCollectable(resPlanModel.getFRelation().getEntity(), clct);															
						}
					}
				}
			}
		});
		resPlan.getResourceHeader().addMouseListener(new AbstractJPopupMenuListener() {
			
			@Override
			protected JPopupMenu getJPopupMenu(MouseEvent evt) {
				final Collectable<PK> clct = resPlan.getResourceHeader().getValueAt(evt.getPoint());
				if (clct != null) {
					JPopupMenu popupMenu = new JPopupMenu();
					popupMenu.add(new AbstractAction(SpringLocaleDelegate.getInstance().getText(
							"nuclos.resplan.action.showDetails")) {
						@Override
						public void actionPerformed(ActionEvent e) {
							runDetailsCollectable(resPlanModel.getResourceEntity(), clct);
						}
					});
					return popupMenu;
				}
				return null;
			}
		});
		
		Date start = DateUtils.addDays(DateUtils.getPureDate(new Date()), -5);
		Date end = DateUtils.addDays(start, 30);
		resPlan.setTimeHorizon(new Interval<Date>(start, end), controller.getConfigVO().getSpecialViewFromField() == null && controller.getConfigVO().getSpecialViewUntilField() == null);		
	}

	public void setResourceRenderer(ComponentProvider<?> renderer) {
		resPlan.getResourceHeader().setCellRendererProvider(renderer);
	}
	
	public ComponentProvider<?> getResourceRenderer() {
		return resPlan.getResourceHeader().getCellRendererProvider();
	}

	public void setBackgroundPainter(BackgroundPainter<PK> painter) {
		resPlan.setTimeslotBackgroundPainter(painter);
	}

	public void setCaptionComponent(Component c) {
		scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, c);
		resPlan.getResourceHeader().setCornerComponent(c);
		resPlan.getTimelineHeader().setCornerComponent(c);
	}

	public Component getCaptionComponent() {
		return scrollPane.getCorner(JScrollPane.UPPER_LEFT_CORNER);
	}
	
	private void runCustomSearch() {
		try {
			final NuclosCollectController<PK,R> ctl = (NuclosCollectController<PK,R>) 
					NuclosCollectControllerFactory.getInstance().newCollectController(
					resPlanModel.getResourceEntity(), null, 
					ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			ctl.setCancelRunningStatementsOnClose(false);
			
			final MainFrameTab mfTab = ctl.getTab();
			final MainFrameTab.OverlayCloseListener mftListener = mfTab.new OverlayCloseListener() {
				
				@Override
				public void tabClosed(MainFrameTab tab) {
					searchFilterComboBox.setSelectedItem(lastSelectedFilter);
					super.tabClosed(mfTab);
				}
				
				@Override
				public void tabAdded(MainFrameTab tab) {}
			};
			ctl.getTab().addMainFrameTabListener(mftListener);
			
			final Action actSearch = new CommonAbstractAction(Icons.getInstance().getIconFind16(), 
					SpringLocaleDelegate.getInstance().getText("CollectController.30")) {
				@Override
				public void actionPerformed(ActionEvent ev) {
					UIUtils.runCommand(getParent(), new CommonRunnable() {
						@Override
						public void run() throws CommonBusinessException {
							try {
								ctl.makeConsistent(true);
								final CollectableSearchCondition result = ctl.getSearchStrategy().getCollectableSearchCondition();
								setCustomSearchFilter(result);
								ctl.getTab().removeMainFrameTabListener(mftListener);
							} catch (CollectableFieldFormatException e) {
								throw new NuclosFatalException(e);
							}
							ctl.getTab().close();
						}
					});
				}
			};
			final Component[] comps = ctl.getSearchPanel().getToolBar().getComponents(NuclosToolBarItems.SEARCH);
			for (int i = 0; i < comps.length; i++) {
				if (comps[i] instanceof JButton) {
					((JButton)comps[i]).setAction(actSearch);
					//EntitySearchFilter filter = (EntitySearchFilter) searchFilterComboBox.getItemAt(searchFilterComboBox.getSelectedIndex()-1);
					EntitySearchFilter lastFilter = lastSelectedFilter;
					EntitySearchFilter filter = (EntitySearchFilter) searchFilterComboBox.getSelectedItem();
					if (filter instanceof ModifyCustomSearchFilter) {
						ctl.setSearchFieldsAccordingToSearchCondition(lastFilter.getSearchCondition(), true);
					}
					ctl.runSearch();
					break;
				}
			}
		} catch (CommonBusinessException e) {
			LOG.warn("runCustomSearch failed: " + e, e);
		}
	}
	
	private void addCustomRuleActions(final List<Collectable<PK>> clcts, JPopupMenu menu) {
		Set<EventSupportSourceVO> activeRules = null;
		for (Collectable<PK> clct : clcts) {
			UsageCriteria uc = new UsageCriteria(clct.getEntityUID(), null, null, null);
	
			// add EventSupports if existing
			final Collection<EventSupportSourceVO> rules = EventSupportDelegate.getInstance().findEventSupportsByUsageAndEvent(CustomRule.class.getCanonicalName(), uc);
			
			// remove inactive rules
			CollectionUtils.removeAll(rules, new Predicate<EventSupportSourceVO>() {
				@Override
				public boolean evaluate(EventSupportSourceVO rulevo) {
					return !rulevo.isActive() || !rulevo.getName().startsWith("ResPlanMenuItem");
				}
			});
			if (activeRules == null) {
				activeRules = new HashSet<>();
				activeRules.addAll(rules);
			} else {
				activeRules.retainAll(rules);
			}
		}
		
		if (activeRules.size() > 0) {
			menu.addSeparator();
		}
		for (final EventSupportSourceVO rule : activeRules) {
			menu.add(new AbstractAction(rule.getDescription()) {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						for (Collectable<PK> clct : clcts) {
							controller.executeBusinessRules(clct, Arrays.asList(rule));
						}
						controller.refresh(true);
					} catch (CommonBusinessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
		}
		if (activeRules.size() > 0) {
			menu.addSeparator();
		}
	}
	
	public JScrollPane getResPlanScrollPane() {
		return scrollPane;
	}

	public Rectangle getViewRect() {
		return scrollPane.getViewport().getViewRect();
	}

	public void setViewRect(Rectangle rect) {
		scrollPane.getViewport().setViewPosition(rect.getLocation());
	}

	public void storeViewPreferences(PlanningTablePreferences prefs, RestorePreferences rp) throws PreferencesException {
		preserveCellExtent(userResourceCellExtent, resPlan.getResourceHeader());
		preserveCellExtent(userTimelineCellExtent, resPlan.getTimelineHeader());
		
		preserveHeaderExtent(userResourceHeaderExtent, resPlan.getResourceHeader());
		preserveHeaderExtent(userTimelineHeaderExtent, resPlan.getTimelineHeader());

		int swingConstant = resPlan.getOrientation().swingConstant();
		if (prefs != null) {
			prefs.setOrientation(swingConstant);
		}
		if (rp != null) {
			rp.orientation = swingConstant;
		}
		for (Orientation orientation : Orientation.values()) {
			String vh = orientation.select("H", "V");
			if (prefs != null) {
				if ("H".equals(vh)) {
					prefs.setResourceCellExtentH(orientation.extentFrom(userResourceCellExtent));
					prefs.setTimelineCellExtentH(orientation.extentFrom(userTimelineCellExtent));
				} else {
					prefs.setResourceCellExtentV(orientation.extentFrom(userResourceCellExtent));
					prefs.setTimelineCellExtentV(orientation.extentFrom(userTimelineCellExtent));
				}
			}
			if (rp != null) {
				rp.resourceCellExtent.put("resourceCellExtent" + vh, orientation.extentFrom(userResourceCellExtent));

				rp.timelineCellExtent.put("timelineCellExtent" + vh, orientation.extentFrom(userTimelineCellExtent));
				for (TimeGranularity tg : this.getTimeGranularities()) {
					rp.timelineCellExtent.put(tg.getType().getValue()+"_timelineCellExtent" + "H", tg.getCellExtent(Orientation.HORIZONTAL));
					rp.timelineCellExtent.put(tg.getType().getValue()+"_timelineCellExtent" + "V", tg.getCellExtent(Orientation.VERTICAL));
				}
				rp.resourceHeaderExtent = getResPlan().getResourceHeader().getHeaderExtent();
				rp.timelineHeaderExtent = getResPlan().getTimelineHeader().getHeaderExtent();
			}
		}
		if (prefs != null) {
			prefs.setResourceHeaderExtent(getResPlan().getResourceHeader().getHeaderExtent());
			prefs.setTimelineHeaderExtent(getResPlan().getTimelineHeader().getHeaderExtent());
		}
	}

	public void restoreViewPreferences(PlanningTablePreferences prefs, RestorePreferences rp) throws PreferencesException {
		if (prefs != null) {
			int orientation = prefs.getOrientation();
			userResourceCellExtent = new Dimension(prefs.getResourceCellExtentH(), prefs.getResourceCellExtentV());
			userTimelineCellExtent = new Dimension(prefs.getTimelineCellExtentH(), prefs.getTimelineCellExtentV());
			if (orientation != -1)
				resPlan.setOrientation(Orientation.fromSwingConstant(orientation));
			setCellExtent(resPlan.getResourceHeader(), userResourceCellExtent);			
			setCellExtent(resPlan.getTimelineHeader(), userTimelineCellExtent);
			if (prefs.getTimelineHeaderExtent() != -1) {
				getResPlan().getTimelineHeader().setHeaderExtent(prefs.getTimelineHeaderExtent());
			} else {
				getResPlan().getTimelineHeader().setDefaultHeaderExtent();
			}
			if (prefs.getResourceHeaderExtent() != -1) {
				getResPlan().getResourceHeader().setHeaderExtent(prefs.getResourceHeaderExtent());
			} else {
				getResPlan().getResourceHeader().setDefaultHeaderExtent();
			}
		}

		if (rp != null) {
			userResourceCellExtent = new Dimension(LangUtils.defaultIfNull(rp.resourceCellExtent.get("resourceCellExtentH"), -1),
				LangUtils.defaultIfNull(rp.resourceCellExtent.get("resourceCellExtentV"), -1));
			userTimelineCellExtent = new Dimension(LangUtils.defaultIfNull(rp.timelineCellExtent.get("timelineCellExtentH"), -1),
				LangUtils.defaultIfNull(rp.timelineCellExtent.get("timelineCellExtentV"), -1));
			if (rp.orientation != -1)
				resPlan.setOrientation(Orientation.fromSwingConstant(rp.orientation));
			setCellExtent(resPlan.getResourceHeader(), userResourceCellExtent);

			setCellExtent(resPlan.getTimelineHeader(), userTimelineCellExtent);
			for (TimeGranularity tg : this.getTimeGranularities()) {
				tg.setCellExtent(Orientation.HORIZONTAL, rp.timelineCellExtent.get(tg.getType().getValue()+"_timelineCellExtent" + "H"));
				tg.setCellExtent(Orientation.VERTICAL, rp.timelineCellExtent.get(tg.getType().getValue()+"_timelineCellExtent" + "V"));
				if (getTimeGranularity() == tg.getType()) {
					setCellExtent(resPlan.getTimelineHeader(), tg.getCellExtent(resPlan.getOrientation()));
				}
			}
		}
	}
	
	public void setOrientation(Orientation orientation) {
		resPlan.setOrientation(orientation);
	}
	
	public void refreshHeaderExtent() {
		if (resPlan.getOrientation() == Orientation.VERTICAL) {
			resPlan.getResourceHeader().invalidate();
			resPlan.getResourceHeader().setDefaultHeaderExtent();
		} else {
			resPlan.getTimelineHeader().invalidate();
			resPlan.getTimelineHeader().setDefaultHeaderExtent();
		}
	}
	
	private static void preserveCellExtent(Dimension dim, JHeaderGrid<?> header) {
		header.getOrientation().updateExtent(dim, header.getCellExtent());
	}
	
	private static void preserveHeaderExtent(Dimension dim, JHeaderGrid<?> header) {
		header.getOrientation().opposite().updateExtent(dim, header.getHeaderExtent());
	}

	private static void setCellExtent(JHeaderGrid<?> header, Dimension dim) {
		header.setCellExtent(header.getOrientation().extentFrom(dim));
	}
	
	private static void setHeaderExtent(JHeaderGrid<?> header, Dimension dim) {
		header.setHeaderExtent(header.getOrientation().opposite().extentFrom(dim));
	}

	private static void setCellExtent(JHeaderGrid<?> header, int extent) {
		header.setCellExtent(extent);
	}
	
	void removeSearchFilter() {
		searchFilterLabel.setVisible(false);
		searchFilterComboBox.setVisible(false);		
	}
	
	void removeDataChooser() {
		startDateLabel.setVisible(false);
		startDateChooser.setVisible(false);
		endDateLabel.setVisible(false);
		endDateChooser.setVisible(false);		
	}
	
	boolean bAdjustWidthAndHeight = false;
	void adjustWidthAndHeight() {
		bAdjustWidthAndHeight = true;
		int test1 = resPlan.getTimelineHeader().getCellExtent();
		if (test1 <= 0) {
			int numberCells = resPlan.getTimelineHeader().maxGroupElements();
			if (numberCells < 1) numberCells = 1;
			int w = scrollPane.getPreferredSize().width;
			int newExtend = (w - 24)/numberCells;
			setCellExtent(resPlan.getTimelineHeader(), newExtend);
		}
		
		int test2 = resPlan.getResourceHeader().getCellExtent();
		if (test2 <= 0) {
	  		int h = scrollPane.getPreferredSize().height;
	 		int numberRessources = resPlan.getResourceHeader().maxGroupElements();
			if (numberRessources < 1) numberRessources = 1;
			int newExtend = 2*(h - 80)/numberRessources/3;
			adjustHeight(newExtend);
		}
	}
	
	void adjustHeight(int height) {
		if (bAdjustWidthAndHeight) {
			setCellExtent(resPlan.getResourceHeader(), height);
		}
	}


	/**
	 * Determines the current top-left area (resource/time) and creates a Runnable
	 * which can be used to scroll the view to that area.
	 */
	private Runnable createScrollToCurrentAreaRunnable() {
		Point pt = resPlan.getVisibleRect().getLocation();
		pt.translate(5, 5);
		final R resource = resPlan.getResourceAt(pt);
		final Interval<Date> timeInterval = resPlan.getTimeIntervalAt(pt);
		return new Runnable() {
			@Override
			public void run() {
				try {
					resPlan.scrollRectToArea(resource, timeInterval, true);
					if (bAdjustWidthAndHeight) adjustWidthAndHeight();
				}
				catch (Exception e) {
					LOG.error("createScrollToCurrentAreaRunnable failed: " + e, e);
				}
			}
		};
	}

	private Action switchOrientationAction = new AbstractAction(
			SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.switchOrientation"), 
			Icons.getInstance().getIconRelate()) {
		@Override
		public void actionPerformed(ActionEvent e) {
			Runnable runnable = createScrollToCurrentAreaRunnable();
			preserveCellExtent(userResourceCellExtent, resPlan.getResourceHeader());
			preserveHeaderExtent(userResourceHeaderExtent, resPlan.getResourceHeader());
			preserveHeaderExtent(userTimelineHeaderExtent, resPlan.getResourceHeader());
//			preserveCellExtent(userTimelineCellExtent, resPlan.getTimelineHeader());
			Orientation orientation = resPlan.getOrientation();
			resPlan.setOrientation(orientation.opposite());
			setCellExtent(resPlan.getResourceHeader(), userResourceCellExtent);
//			setCellExtent(resPlan.getTimelineHeader(), userTimelineCellExtent);
			if (timeGranularityModel.getSelectedItem() != null &&
				timeGranularityModel.getSelectedItem().getCellExtent(resPlan.getOrientation()) > 0) {
				setCellExtent(resPlan.getTimelineHeader(), timeGranularityModel.getSelectedItem().getCellExtent(resPlan.getOrientation()));
			}

			resPlan.getTimelineHeader().setPreferredSize(null);
			resPlan.getResourceHeader().setPreferredSize(null);
			resPlan.getTimelineHeader().setDefaultHeaderExtent();
			resPlan.getResourceHeader().setDefaultHeaderExtent();
			resPlan.invalidate();
			SwingUtilities.invokeLater(runnable);
		}
	};

	private Action removeAction = new AbstractAction(SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.remove")) {
		@Override
		public void actionPerformed(ActionEvent e) {
			final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
			ResPlanModel<PK, R, Date, Collectable<PK>, Collectable<PK>> model = resPlan.getModel();
			final List<CellView<PK,R,Date,Collectable<PK>,Collectable<PK>>> selectedEntries = 
					resPlan.getSelectedCellViews();
			final List<Collectable<PK>> selectedRelations = resPlan.getSelectedRelations();
			String title, message;
			if (selectedEntries.isEmpty() && selectedRelations.isEmpty()) {
				return;
			} else if (selectedEntries.size() == 1 && selectedRelations.isEmpty()) {
				title = localeDelegate.getMessage("ResultController.8", null);
				message = localeDelegate.getMessage("ResultController.12", null, 
						SpringLocaleDelegate.getInstance().getIdentifierLabel(
								selectedEntries.get(0).getEntry(), selectedEntries.get(0).getEntry().getEntityUID(), MetaProvider.getInstance()));
			} else if (selectedRelations.size() == 1 && selectedEntries.isEmpty()) {
				title = localeDelegate.getMessage("ResultController.8", null);
				message = localeDelegate.getMessage("ResultController.12", null, 
						SpringLocaleDelegate.getInstance().getIdentifierLabel(
								selectedRelations.get(0), selectedRelations.get(0).getEntityUID(), MetaProvider.getInstance()));
			} else { // selectedEntries.size() > 1
				title = localeDelegate.getMessage("ResultController.7", null);
				message = localeDelegate.getMessage("ResultController.13", null, 
						selectedEntries.size() + selectedRelations.size());
			}
			int opt = JOptionPane.showConfirmDialog(ResPlanPanel.this, message, title,JOptionPane.YES_NO_OPTION);
			if (opt == JOptionPane.YES_OPTION) {
				for (Collectable<PK> clct : selectedRelations) {
					if (model.isRemoveRelationAllowed(clct)) {
						model.removeRelation(clct);
					}
				}
				
				List<CellView<PK,R,Date,Collectable<PK>,Collectable<PK>>> lstCellView 
					= new ArrayList<CellView<PK,R,Date,Collectable<PK>,Collectable<PK>>>(selectedEntries);
				for (CellView<PK,R,Date,Collectable<PK>,Collectable<PK>> cellView : lstCellView) {
					Collectable<PK> clct = (Collectable<PK>) cellView.getEntry();
					ClientPlanElement<PK,R,Collectable<PK>> pElem = cellView.getPlanElement();
					if (model.isRemoveEntryAllowed(clct, pElem)) {
						model.removeEntry(clct, pElem);
					}
				}
			}
		}
	};

	private Action detailsAction = new AbstractAction(SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.showDetails")) {
		@Override
		public void actionPerformed(ActionEvent e) {
			final List<CellView<PK,R,Date,Collectable<PK>,Collectable<PK>>> selectedEntries =
					resPlan.getSelectedCellViews();				
			for (CellView<PK,R,Date,Collectable<PK>,Collectable<PK>> cellView : selectedEntries) {
				Collectable<PK> clct = (Collectable<PK>) cellView.getEntry();
				ClientPlanElement<PK,R,Collectable<PK>> pElem = cellView.getPlanElement();
				runDetailsCollectable(pElem.getEntity(), clct);
			}
			final List<Collectable<PK>> selectedRelations = resPlan.getSelectedRelations();
			for (Collectable<PK> clct : selectedRelations) {
				runDetailsCollectable(resPlanModel.getFRelation().getEntity(), clct);
			}
		}
	};
	
	private Action relateBeginAction = new AbstractAction(SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.relateBegin")) {
		@Override
		public void actionPerformed(ActionEvent e) {
			final List<CellView<PK,R,Date,Collectable<PK>,Collectable<PK>>> selectedEntries =
					resPlan.getSelectedCellViews();				
			if (selectedEntries.size() == 1) {
				CellView<PK,R,Date,Collectable<PK>,Collectable<PK>> view = selectedEntries.get(0);
				EntryWPElem<R> wp = new EntryWPElem<R>((Collectable<PK>)view.getEntry(), view.getPlanElement());
				resPlan.setRelateBegin(wp);
			}
		}
	};
	
	private Action relateFinishAction = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (resPlan.getRelateBegin() != null) {
				final List<CellView<PK,R,Date,Collectable<PK>,Collectable<PK>>> selectedEntries =
						resPlan.getSelectedCellViews();				
				if (selectedEntries.size() == 1) {
					CellView<PK,R,Date,Collectable<PK>,Collectable<PK>> view = selectedEntries.get(0);
					Collectable<PK> to = (Collectable<PK>) view.getEntry();
					ClientPlanElement<PK,R,Collectable<PK>> pTo = view.getPlanElement();
					EntryWPElem<R> wpElem = resPlan.getRelateBegin();
					if (wpElem != null && to != wpElem.getCollectable()) {
						resPlanModel.createRelation(wpElem.getCollectable(), 
								to,
								wpElem.getPlanElement(),
								pTo);
						resPlan.setRelateBegin(null);
					}
				}
			}
		}
		@Override
		public Object getValue(String key) {
			if (Action.NAME.equals(key)) {
				String resourceLabel = "";
				if (resPlan.getRelateBegin() != null) {
					resourceLabel = resPlan.getEntryAsText(resPlan.getRelateBegin().getCollectable());
					if (resourceLabel != null && resourceLabel.length() > 130) {
						resourceLabel = resourceLabel.substring(0, 128)+ "...";
					}
				}
				return String.format(SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.relateFinish"), resourceLabel);
			}
			return super.getValue(key);
		}
	};

	private Action infoAction = new AbstractAction(null, Icons.getInstance().getIconAbout16()) {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (!infoMessagesBubble.isVisible()) {
				showInfoMessages();
			} else {
				infoMessagesBubble.setVisible(false);
			}
		}
	};

	private Action findAction = new FindAction();

	class FindAction extends AbstractAction implements AncestorListener {

		private final JXFindPanel findPanel;

		public FindAction() {
			super("find");
			findPanel = SearchFactory.getInstance().getSharedFindPanel();
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!Arrays.asList(findPanel.getAncestorListeners()).contains(this)) {
				findPanel.addAncestorListener(this);
			}
			SearchFactory.getInstance().showFindDialog(ResPlanPanel.this, resPlan.new ResPlanSearchable());
		}

		@Override
		public void ancestorAdded(AncestorEvent event) {
		}

		@Override
		public void ancestorRemoved(AncestorEvent event) {
			// Remove this listener
			findPanel.removeAncestorListener(this);
			// Clear resource selection
			resPlan.getResourceHeader().setSelectedValue(null);
		}

		@Override
		public void ancestorMoved(AncestorEvent event) {
		}
	}

	private Action exportAction = new ExportAction();

	class ExportAction extends AbstractAction {

		ExportAction() {
			super(SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.export"),
					Icons.getInstance().getIconExport16());
			// setEnabled(resPlanModel.isCreateAllowed());
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// controller.getFrame().add(new ResPlanExportPanel());
			final Future<LayerLock> lock = UIUtils.lockFrame(getController().getHolderComponent());
			final SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

				@Override
				protected Void doInBackground() throws Exception {
					try {
						final ResPlanExportDialog2 d = new ResPlanExportDialog2(ResPlanPanel.this, controller.getHolderComponent());
					}
					catch (Exception ex) {
						Errors.getInstance().showExceptionDialog(ResPlanPanel.this, ex);
					}
					return null;
				}
				
				@Override
				protected void done() {
					UIUtils.unLockFrame(getController().getHolderComponent(), lock);
				}
				
			};
			worker.execute();
		}
	}

	class RemoveAction extends AbstractAction {

		public RemoveAction() {
			super(SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.remove"));
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
			ResPlanModel<PK,R, Date, Collectable<PK>, Collectable<PK>> model = resPlan.getModel();
			final List<CellView<PK,R,Date,Collectable<PK>,Collectable<PK>>> selectedEntries =
					resPlan.getSelectedCellViews();
			String title, message;
			if (selectedEntries.isEmpty()) {
				return;
			} else if (selectedEntries.size() == 1) {
				title = localeDelegate.getMessage("ResultController.8", null);
				message = localeDelegate.getMessage("ResultController.12", null, selectedEntries.get(0).getEntry().toString());
						SpringLocaleDelegate.getInstance().getIdentifierLabel(
								selectedEntries.get(0).getEntry(), selectedEntries.get(0).getEntry().getEntityUID(), MetaProvider.getInstance());
			} else { // selectedEntries.size() > 1
				title = localeDelegate.getMessage("ResultController.7", null);
				message = localeDelegate.getMessage("ResultController.13", null, selectedEntries.size());
			}
			int opt = JOptionPane.showConfirmDialog(ResPlanPanel.this, message, title,JOptionPane.YES_NO_OPTION);
			if (opt == JOptionPane.YES_OPTION) {
				for (CellView<PK,R,Date,Collectable<PK>,Collectable<PK>> cellView : selectedEntries) {
					Collectable<PK> clct = (Collectable<PK>) cellView.getEntry();
					ClientPlanElement<PK,R,Collectable<PK>> pElem = cellView.getPlanElement();
					if (model.isRemoveEntryAllowed(clct, pElem)) {
						model.removeEntry(clct, pElem);
					}
				}
			}
		}
	}
	
	private void addActionsToPopup(JPopupMenu popup, R resource, Interval<Date> interval) {
		String addText = SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.add");
		List<ClientPlanElement<PK,R,Collectable<PK>>> lstPElements = resPlanModel.getListEntriesOrMileStones(true);
		Collections.sort(lstPElements, new Comparator<ClientPlanElement<PK, R, Collectable<PK>>>() {
			@Override
			public int compare(ClientPlanElement<PK, R, Collectable<PK>> o1, ClientPlanElement<PK, R, Collectable<PK>> o2) {
				return o1.getOrder() == o2.getOrder() ? 0 : o1.getOrder() < o2.getOrder() ? -1 : 1;
			}
		});
		for (ClientPlanElement<PK,R,Collectable<PK>> pElem : lstPElements) {
			if (MetaProvider.getInstance().getEntity(pElem.getEntity()).isEditable()) {
				String text = LocaleDelegate.getInstance().getLocale().equals(Locale.GERMAN)
						? LocaleDelegate.getInstance().getResource(MetaProvider.getInstance().getEntity(pElem.getEntity()).getLocaleResourceIdForLabel()) + " " + addText
						: addText + " " + LocaleDelegate.getInstance().getResource(MetaProvider.getInstance().getEntity(pElem.getEntity()).getLocaleResourceIdForLabel());
				popup.add(new AddAction(resource, interval, text, pElem));
			}
		}
	}

	@SuppressWarnings("serial")
	protected class AddAction extends AbstractAction {

		private ClientPlanElement<PK,R,Collectable<PK>> pElement;
		private Interval<Date> interval;
		private R resource;

		public AddAction(R resource, Interval<Date> interval, String text, ClientPlanElement<PK,R,Collectable<PK>> pElem) {
			super(text);
			setEnabled(resPlanModel.isCreateEntryAllowed(pElem));
			this.resource = resource;
			this.interval = interval;
			this.pElement = pElem;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			UIUtils.runCommand(resPlan, new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					final MainFrameTab tabIfAny = new MainFrameTab();
					final NuclosCollectController<PK,R> cntrl = (NuclosCollectController<PK,R>) 
							NuclosCollectControllerFactory.getInstance().newCollectController(
							pElement.getEntity(), tabIfAny, 
							ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
					Main.getInstance().getMainController().initMainFrameTab(controller, tabIfAny);
					controller.getHolderComponent().add(tabIfAny);
					
					cntrl.addCollectableEventListener(new CollectControllerEventHandler<PK,R>(cntrl, controller));
					Collectable<PK> template = null;
					if (interval != null && resource != null) {
						template = resPlanModel.createCollectableEntry(resource, interval, pElement);
					}
					cntrl.runNewWith((R) template);
				}
			});
		}
	}

	public void runDetailsCollectable(final UID entity, final Collectable<PK> clct) {
		if (clct == null)
			return;

		UIUtils.runCommand(Main.getInstance().getMainFrame(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final MainFrameTab tabIfAny = new MainFrameTab();
				final NuclosCollectController<PK,R> cntrl = (NuclosCollectController<PK, R>) 
						NuclosCollectControllerFactory.getInstance().newCollectController(
						entity, tabIfAny, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
				
				Main.getInstance().getMainController().initMainFrameTab(cntrl, tabIfAny);
				controller.getHolderComponent().add(tabIfAny);
				
				cntrl.addCollectableEventListener(new CollectControllerEventHandler<PK,R>(cntrl, controller));
				cntrl.runViewSingleCollectableWithId(clct.getId());
			}
		});
	}

	private final static class CollectControllerEventHandler<PK2,R2 extends Collectable<PK2>> implements CollectableEventListener<PK2> {

		private final NuclosCollectController<PK2,R2> collectController;
		
		private final ResPlanController<PK2,R2> controller;

		public CollectControllerEventHandler(NuclosCollectController<PK2,R2> clctCntrl, ResPlanController<PK2,R2> controller) {
			this.collectController = clctCntrl;
			this.controller = controller;
		}

		@Override
		public void handleCollectableEvent(Collectable<PK2> collectable, MessageType messageType) {
			switch (messageType) {
			case EDIT_DONE:
			case NEW_DONE :
			case DELETE_DONE:
				try {
					collectController.getTab().close();
				} finally {
					controller.refresh(true);
				}
			break;
			case STATECHANGE_DONE:
				controller.refresh(true);
			}
		}
	}

	public static class NewCustomSearchFilter extends EntitySearchFilter {

		public NewCustomSearchFilter() {
			getSearchFilterVO().setFilterName(SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.newSearch"));
			// set id, so itemlistener will react on item change event...
			getSearchFilterVO().setPrimaryKey(UID.UID_NULL);
		}

	}
	
	public static class ModifyCustomSearchFilter extends NewCustomSearchFilter {

		public ModifyCustomSearchFilter() {
			getSearchFilterVO().setFilterName(SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.modifySearch"));
			// set id, so itemlistener will react on item change event...
			getSearchFilterVO().setPrimaryKey(new UID("-1"));
		}

	}

	public static class CustomSearchFilter extends EntitySearchFilter {

		private static int ID_COUNTER = -3;

		private boolean initialized;

		public CustomSearchFilter() {
			final SearchFilterVO vo = new SearchFilterVO(new UID(Integer.toString(ID_COUNTER--)), 
					SpringLocaleDelegate.getInstance().getText("nuclos.resplan.action.customSearch"),
					null, null, null, null, null, null,
					null, 0, false, null, null,
					false, null, null, null, new SearchFilterUserVO(), null, null, null,
					null, 0);
			setSearchFilterVO(vo);
		}

		public CustomSearchFilter(CollectableSearchCondition cond) {
			this();
			setSearchCondition(cond);
		}

		@Override
		public void setSearchCondition(CollectableSearchCondition searchcond) {
			super.setSearchCondition(searchcond);
			this.initialized = searchcond != null;
		}

		public boolean isInitialized() {
			return initialized;
		}

	}

	public List<TimeGranularity> getTimeGranularities() {
		List<TimeGranularity> result = new ArrayList<TimeGranularity>();
		for (int i = 0; i < timeGranularityModel.getSize(); i++) {
			result.add(timeGranularityModel.getElementAt(i));
		}
		return result;
	}

	public void setRelationPresentation(int relationPresentation) {
		resPlan.setRelationPresentation(relationPresentation);
	}

	public void setRelationFromPresentation(int relationFromPresentation) {
		resPlan.setRelationFromPresentation(relationFromPresentation);
	}

	public void setRelationToPresentation(int relationToPresentation) {
		resPlan.setRelationToPresentation(relationToPresentation);
	}
	
	private class HeaderChangeListener implements PropertyChangeListener{

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (JHeaderGrid.HEADER_EXTENT_PROPERTY.equals(evt.getPropertyName()) || JHeaderGrid.CELL_EXTENT_PROPERTY.equals(evt.getPropertyName())) {
				Orientation orientation = getResPlan().getOrientation();
				JHeaderGrid timelineHeader = getResPlan().getTimelineHeader();
				JHeaderGrid resourceHeader = getResPlan().getResourceHeader();
				int widthTimeline = orientation.isVertical() ? timelineHeader.getHeaderExtent() : getSize(timelineHeader);
				int heightTimeline = orientation.isVertical() ? getSize(timelineHeader) : timelineHeader.getHeaderExtent();
				int widthResource = orientation.isVertical() ? getSize(resourceHeader) : resourceHeader.getHeaderExtent();
				int heightResource = orientation.isVertical() ? resourceHeader.getHeaderExtent() : getSize(resourceHeader);
				timelineHeader.setPreferredSize(new Dimension(widthTimeline, heightTimeline));
				resourceHeader.setPreferredSize(new Dimension(widthResource, heightResource));
				revalidate();
			}
		}
		
		private int getSize(JHeaderGrid header) {
			return (header.getCellExtent() + header.getGridSize()) * header.getCount() - header.getGridSize();
		}
		
	}
}
