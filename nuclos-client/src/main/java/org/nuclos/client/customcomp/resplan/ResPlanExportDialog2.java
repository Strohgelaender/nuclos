//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.customcomp.resplan;

import java.util.concurrent.Future;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.fileexport.FileType;
import org.nuclos.client.image.ImageType;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.common2.ClientPreferences;

/**
 * A SVG (and more) export dialog for {@link org.nuclos.client.ui.resplan.JResPlanComponent}s.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.6
 */
@SuppressWarnings("serial")
public class ResPlanExportDialog2 extends AbstractResPlanExportDialog {
	
	private static final Logger LOG = Logger.getLogger(ResPlanExportDialog2.class);
	
	public static final String SVG_TEMPLATE = "templates/svg/resplan.svg";
	public static final String XLSX_TEMPLATE = "templates/xls/resplan.xls";
	
	//
	
	public ResPlanExportDialog2(ResPlanPanel panel, JComponent parent) {
		super(ClientPreferences.getInstance().getUserPreferences().node("resPlan"), panel.getController().getTitle(), parent,
				new ResPlanExporter2(panel.getResPlan()));
	}
	
	@Override
	protected void export() {
		final Future<LayerLock> lock = UIUtils.lockFrame(getParent());
		final SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				final ImageType imageType = ImageType.getFromFileExtension((String) fileTypes.getSelectedItem());
				final FileType fileType = FileType.getFromFileExtension((String) fileTypes.getSelectedItem());
				try {
					if (imageType != null) {
						ResPlanExportDialog2.super.getExporter().run(SVG_TEMPLATE, 0, false);
						ResPlanExportDialog2.super.getExporter().save(imageType, save);
					} else if (fileType != null) {
						ResPlanExportDialog2.super.getExporter().run(XLSX_TEMPLATE, 0, false);
						ResPlanExportDialog2.super.getExporter().save(fileType, save);
					}
				}
				catch (Exception ex) {
					LOG.warn("ResPlan export failed: " + ex.toString(), ex);
					Errors.getInstance().showExceptionDialog(ResPlanExportDialog2.this, "Can' save " + save, ex);
				}
				return null;
			}
			
			@Override
			protected void done() {
				UIUtils.unLockFrame(getParent(), lock);
			}
			
		};
		worker.execute();
	}	
}
