package org.nuclos.client.customcomp.resplan;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPathExpressionException;

import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.nuclos.client.fileexport.FileType;
import org.nuclos.client.image.ImageType;
import org.nuclos.client.image.SVGDOMDocumentSupport;
import org.nuclos.client.report.ReportDelegate;
import org.nuclos.client.report.reportrunner.ReportRunner;
import org.nuclos.client.ui.resplan.JResPlanComponent;
import org.nuclos.client.ui.resplan.JResPlanComponent.CellView;
import org.nuclos.client.ui.resplan.ResPlanModel;
import org.nuclos.client.ui.resplan.header.GroupMapper;
import org.nuclos.client.ui.resplan.header.JHeaderGrid;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.interval.Interval;
import org.w3c.dom.Element;

/**
 * A SVG (and more) exporter for {@link JResPlanComponent}s.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.6
 */
public class ResPlanExporter2<PK> implements IResPlanExporter<Collectable<PK>, Collectable<PK>, Collectable<PK>> {
	
	private final JResPlanComponent<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> jrpc;
	
	private SVGDOMDocumentSupport sdds;
	private NuclosFile nf;
	
	ResPlanExporter2(JResPlanComponent<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> jrpc) {
		this.jrpc = jrpc;
	}

	@Override
	public SVGDOMDocumentSupport getSVGDOMDocumentSupport() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void run(String template, int startCategory, boolean skipInsideCats) throws IOException, XPathExpressionException, NuclosReportException {
		
		if (template.equals(ResPlanExportDialog2.SVG_TEMPLATE)) {
			/*
			final String uri = LangUtils.getClassLoaderThatWorksForWebStart().getResource(template).toExternalForm();
			sdds = new SVGDOMDocumentSupport(uri);
			 */
			
			// ignore uri and startCategory
			sdds = new SVGDOMDocumentSupport();
			final SVGGraphics2D g2d = sdds.asGraphics2D();
			
			final JHeaderGrid<?> columnHeader, rowHeader;
			switch (jrpc.getOrientation()) {
			case VERTICAL:
				columnHeader = jrpc.getResourceHeader();
				rowHeader = jrpc.getTimelineHeader();
				break;
			case HORIZONTAL:
				columnHeader = jrpc.getTimelineHeader();
				rowHeader = jrpc.getResourceHeader();
				break;
			default:
				throw new IllegalStateException();
			}
			final Dimension columnDim = new Dimension(columnHeader.getWidth(), columnHeader.getHeight());
			final Dimension rowDim = new Dimension(rowHeader.getWidth(), rowHeader.getHeight());
			final Dimension mainDim = new Dimension(jrpc.getWidth(), jrpc.getHeight());
			
			g2d.translate(rowDim.width, 0);
			columnHeader.paint(g2d);
			g2d.translate(-rowDim.width, columnDim.height);
			rowHeader.paint(g2d);
			g2d.translate(rowDim.width, 0);
			jrpc.paint(g2d);
			g2d.translate(-rowDim.width, -columnDim.height);
			
			// g2d.setSVGCanvasSize(dim);
			sdds.fromGraphics2D(g2d, false);
	
			final Element svg = sdds.getDocument().getDocumentElement();
			svg.setAttribute("width", Integer.toString(rowDim.width + mainDim.width + 20));
			svg.setAttribute("height", Integer.toString(columnDim.height + mainDim.height + 20));
		} else if (template.equals(ResPlanExportDialog2.XLSX_TEMPLATE)) {
			Pair<ResultVO, List<Pair<Pair<Integer, Integer>,Pair<Integer, Integer>>>> result = createResultVO();
			nf = ReportDelegate.getInstance().prepareExport(result.getX(), ReportOutputVO.Format.XLSX);
			try {
				nf = mergeCells(nf, result.getY());
			} catch (InvalidFormatException e) {
				new NuclosReportException(e);
			}
		}
	}

	private NuclosFile mergeCells(NuclosFile nf, List<Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> lstMergedCells) throws InvalidFormatException, IOException, NuclosReportException {
		Workbook wb = WorkbookFactory.create(new ByteArrayInputStream(nf.getContent()));
		Sheet sheet = wb.getSheetAt(0);
		for (Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> mergeRange : lstMergedCells) {
			sheet.addMergedRegion(new CellRangeAddress(mergeRange.x.x, mergeRange.y.x, mergeRange.x.y, mergeRange.y.y));
			Cell cell = CellUtil.createCell(sheet.getRow(mergeRange.x.x), mergeRange.x.y, sheet.getRow(mergeRange.x.x).getCell(mergeRange.x.y).getStringCellValue());
			CellStyle style = wb.createCellStyle();
			style.setAlignment(CellStyle.ALIGN_CENTER);
			style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			cell.setCellStyle(style);
		}
		
		for (int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
		
		CellStyle style = wb.createCellStyle();
		style.setWrapText(true);
		
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			sheet.getRow(i).setRowStyle(style);
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
		try {
			wb.write(baos);
			nf.setContent(baos.toByteArray());
		}
		catch (IOException e) {
			throw new NuclosReportException(e);
		}
		return nf;
	}

	private Pair<ResultVO, List<Pair<Pair<Integer, Integer>,Pair<Integer, Integer>>>> createResultVO() {
		int columnIndex = 0;
		int rowIndex = 0;
		List<Pair<Pair<Integer, Integer>,Pair<Integer, Integer>>> lstMergedCells = new ArrayList<>(); 
		ResultVO result = new ResultVO();
		ResPlanModel<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> model = jrpc.getModel();
		List<? extends Interval<Date>> lstIntervals = jrpc.getTimeIntervals();
		Map<Collectable, Integer> mpResourceMultiples = new HashMap<>();
		
		if (model instanceof CollectableResPlanModel) {
			ResPlanController<PK, ?> ctrl = ((CollectableResPlanModel<PK, ?>) model).getController();
			List<CollectableEntityField> lstClctef = ctrl.getEntityFieldsFromResourceLabel(ctrl.restoreUserResourceLabel());
			
			int startindex;
			if (jrpc.getTimeModel() instanceof MonthModel) {
				startindex = 0;
			} else if (jrpc.getTimeModel() instanceof QuarterModel) {
				startindex = -2;
			} else {
				startindex = 0;
			}
			
			GroupMapper<Interval<Date>> catModel = jrpc.getTimelineHeader().getCategoryModel();
			
			if (jrpc.getOrientation().isHorizontal()) {
				//resplanheader == rowheader
				//set columns
				for (CollectableEntityField ef : lstClctef) {
					ResultColumnVO colvo = new ResultColumnVO();
					colvo.setColumnClassName("String");
					colvo.setColumnLabel(ef.getLabel());
					result.addColumn(colvo);
					columnIndex++;
				}
				
				String oldCategoryValue;
				String categoryValue = null;
				Pair<Integer, Integer> mergeStart = null;
				boolean merging = false;
				for (Interval<Date> interval : lstIntervals) {
					ResultColumnVO colvo = new ResultColumnVO();
					colvo.setColumnClassName("String");
					oldCategoryValue = categoryValue;

					Document doc = Jsoup.parse((String)catModel.getCategoryValue(startindex, interval,0));
					categoryValue = doc.text();
					colvo.setColumnLabel(categoryValue);
					result.addColumn(colvo);
					if (categoryValue.equals(oldCategoryValue)) {
						if (!merging) {
							mergeStart = new Pair<>(rowIndex,columnIndex-1);
							merging = true;
						}
					} else {
						if (merging && mergeStart != null) {
							Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex,columnIndex-1);
							lstMergedCells.add(new Pair<>(mergeStart, mergeEnd));
							merging = false;
						}
					}
					columnIndex++;
				}
				if (merging) {
					if (mergeStart != null) {
						Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex,columnIndex-1);
						lstMergedCells.add(new Pair<>(mergeStart, mergeEnd));
						merging = false;
					}
				}
				columnIndex = 0;
				rowIndex++;
				
				//set rows
				//intervalls
				for (int i = startindex + 1; i < catModel.getCategoryCount(); i++) {
					List<Object> row = new ArrayList<>();
					for (CollectableEntityField ef : lstClctef) {
						row.add(null);
						columnIndex++;
					}

					categoryValue = null;
					mergeStart = null;
					merging = false;
					
					for (Interval<Date> interval : lstIntervals) {
						Document doc = Jsoup.parse((String)catModel.getCategoryValue(i, interval,startindex));
						oldCategoryValue = categoryValue;
						categoryValue = doc.text();
						row.add(categoryValue);
						if (categoryValue.equals(oldCategoryValue)) {
							if (!merging) {
								mergeStart = new Pair<>(rowIndex,columnIndex-1);
								merging = true;
							}
						} else {
							if (merging && mergeStart != null) {
								Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex,columnIndex-1);
								lstMergedCells.add(new Pair<>(mergeStart, mergeEnd));
								merging = false;
							}
						}
						columnIndex++;
					}
					if (merging && mergeStart != null) {
						Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex,columnIndex-1);
						lstMergedCells.add(new Pair<>(mergeStart, mergeEnd));
						merging = false;
					}
					padRows(row, result.getColumnCount());
					result.addRow(row.toArray());
					columnIndex = 0;
					rowIndex++;
				}

				//ressources
				for (Collectable<PK> resource : model.getResources()) {

					double multiples = 1;

					for (Interval<Date> interval : lstIntervals) {
						Rectangle rect = jrpc.getCellRect(resource, interval);
						Point cellLocation = new Point(rect.getLocation());
						cellLocation.x += 1;
						cellLocation.y += 1;

						CellView<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> cellview = jrpc.getCellViewAt(cellLocation);
						if (cellview != null && cellview instanceof JResPlanComponent.EntryCellView) {
							multiples = rect.getHeight() / cellview.getRect().getHeight();
						}
					}

					if (multiples > 2) {
						mpResourceMultiples.put(resource, Integer.valueOf((int) multiples));
					}

					for (int i = 0; i < lstClctef.size(); i++) {
						lstMergedCells.add(new Pair<>(new Pair<>(rowIndex, columnIndex + i), new Pair<>(rowIndex + (int) multiples - 1, columnIndex + i)));
					}

					for (int i = 1; i <= (int) multiples; i++) {

						List<Object> row = new ArrayList<>();
						for (CollectableEntityField ef : lstClctef) {
							Object value = resource.getValue(ef.getUID());
							row.add(value != null ? value.toString() : null);
							columnIndex++;
						}

						CellView<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> oldCellview;
						CellView<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> cellview = null;
						mergeStart = null;
						merging = false;

						for (Interval<Date> interval : lstIntervals) {
							Rectangle rect = jrpc.getCellRect(resource, interval);
							Point cellLocation = new Point(rect.getLocation());
							cellLocation.x += 1;
							cellLocation.y += 1;

							int height = (int) (rect.getHeight()/multiples - 1);
							int y = cellLocation.y + i*height;

							oldCellview = cellview;
							cellview = jrpc.getCellViewAt(new Point(rect.x + 1, y));
							if (cellview != null && cellview instanceof JResPlanComponent.EntryCellView) {
								row.add(cellview.getAsText().trim());
								if (cellview.equals(oldCellview)) {
									if (!merging) {
										mergeStart = new Pair<>(rowIndex, columnIndex - 1);
										merging = true;
									}
								} else {
									if (merging && mergeStart != null) {
										Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex, columnIndex - 1);
										lstMergedCells.add(new Pair<>(mergeStart, mergeEnd));
										merging = false;
									}
								}
							} else {
								row.add(null);
								if (merging && mergeStart != null) {
									Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex, columnIndex - 1);
									lstMergedCells.add(new Pair<>(mergeStart, mergeEnd));
									merging = false;
								}
							}
							columnIndex++;
						}
						if (merging && mergeStart != null) {
							Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex, columnIndex - 1);
							lstMergedCells.add(new Pair<>(mergeStart, mergeEnd));
						}
						padRows(row, result.getColumnCount());
						result.addRow(row.toArray());
						columnIndex = 0;
						rowIndex++;
					}
				}
			} else {
				Map<Integer, Object> oldValues = new HashMap<>();
				Map<Integer, Boolean> merging = new HashMap<>();
				Map<Integer, Pair<Integer, Integer>> mergeStart = new HashMap<>();
				rowIndex = 1;
				
				//set columns
				//date columns
				for (int i = startindex; i < catModel.getCategoryCount(); i++) {
					ResultColumnVO colvo = new ResultColumnVO();
					colvo.setColumnClassName("String");
					colvo.setColumnLabel("");
					result.addColumn(colvo);
					columnIndex++;
				}

				//resource columns
				int r = 1;
				for (Collectable<PK> resource : model.getResources()) {
					ResultColumnVO colvo = new ResultColumnVO();
					colvo.setColumnClassName("String");
					colvo.setColumnLabel("Resource " + r++);
					result.addColumn(colvo);
					columnIndex++;

					double multiples = 1;

					for (Interval<Date> interval : lstIntervals) {
						Rectangle rect = jrpc.getCellRect(resource, interval);
						Point cellLocation = new Point(rect.getLocation());
						cellLocation.x += 1;
						cellLocation.y += 1;
						CellView<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> cellview = jrpc.getCellViewAt(cellLocation);
						if (cellview != null && cellview instanceof JResPlanComponent.EntryCellView) {
							multiples = rect.getWidth() / cellview.getRect().getWidth();
						}
					}

					if (multiples > 2) {
						mpResourceMultiples.put(resource, Integer.valueOf((int)multiples));
						for (int i = 1; i < (int) multiples; i++) {
							colvo = new ResultColumnVO();
							colvo.setColumnClassName("String");
							colvo.setColumnLabel("");
							result.addColumn(colvo);
							columnIndex++;
						}
					}
				}
				
				//set rows
				//attribute rows
				for (CollectableEntityField ef : lstClctef) {
					columnIndex = 0;
					List<Object> row = new ArrayList<>();
					row.add(ef.getLabel());
					for (int i = startindex+1; i < catModel.getCategoryCount(); i++) {
						row.add(null);
						columnIndex++;
					}
					for (Collectable<PK> resource : model.getResources()) {
						Object value = resource.getValue(ef.getUID());
						row.add(value != null ? value.toString() : null);
						if (mpResourceMultiples.get(resource) != null) {
							for (int i = 1; i < mpResourceMultiples.get(resource); i++) {
								row.add(null);
								columnIndex++;
							}
						}
						columnIndex++;

						lstMergedCells.add(new Pair<>(
								new Pair<>(rowIndex,1 + columnIndex - (mpResourceMultiples.get(resource) != null ? mpResourceMultiples.get(resource) : 1)),
								new Pair<>(rowIndex, columnIndex)
						));
					}

					padRows(row, result.getColumnCount());
					result.addRow(row.toArray());
					rowIndex++;
				}

				//data rows
				for (Interval<Date> interval : lstIntervals) {
					columnIndex = 0;
					List<Object> row = new ArrayList<>();
					String categoryValue = null;

					//intervall rows
					for (int i = startindex; i < catModel.getCategoryCount(); i++) {
						Document doc = Jsoup.parse((String)catModel.getCategoryValue(i, interval,startindex));
						categoryValue = doc.text();
						row.add(categoryValue);
						_mergeVertical(columnIndex, rowIndex, lstMergedCells, oldValues, merging, mergeStart, categoryValue);
						oldValues.put(columnIndex, categoryValue);
						columnIndex++;
					}

					//entry rows
					for (Collectable<PK> resource : model.getResources()) {
						Rectangle rect = jrpc.getCellRect(resource, interval);
						Point cellLocation = new Point(rect.getLocation());
						cellLocation.x += 1;
						cellLocation.y += 1;
						CellView<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> cellview = jrpc.getCellViewAt(cellLocation);
						mergeCells(columnIndex, rowIndex, lstMergedCells, oldValues, merging, mergeStart, row, cellview);
						oldValues.put(columnIndex, cellview);
						columnIndex++;
						Integer multiples = mpResourceMultiples.get(resource);
						if (multiples != null && multiples >= 2) {
							int width = (int) (rect.getWidth()/multiples - 1);
							for (int i = 1; i < multiples; i++) {
								int x = cellLocation.x + i*width;
								cellview = jrpc.getCellViewAt(new Point(x, rect.y + 1));
								mergeCells(columnIndex, rowIndex, lstMergedCells, oldValues, merging, mergeStart, row, cellview);
								oldValues.put(columnIndex, cellview);
								columnIndex++;
							}
						}
					}
					padRows(row, result.getColumnCount());
					result.addRow(row.toArray());
					rowIndex++;
				}
				for (int i = 0; i < columnIndex; i++) {
					if (merging.get(i) != null && merging.get(i) && mergeStart.get(i) != null) {
						Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex-1,i);
						lstMergedCells.add(new Pair<>(mergeStart.get(i), mergeEnd));
						merging.put(i, false);
					}
				}
			}
		}

		return new Pair<>(result,lstMergedCells);
	}

	private void mergeCells(int columnIndex, int rowIndex, List<Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> lstMergedCells, Map<Integer, Object> oldValues, Map<Integer, Boolean> merging, Map<Integer, Pair<Integer, Integer>> mergeStart, List<Object> row, CellView<PK, Collectable<PK>, Date, Collectable<PK>, Collectable<PK>> cellview) {
		if (cellview != null && cellview instanceof JResPlanComponent.EntryCellView) {
            row.add(cellview.getAsText().trim());
            _mergeVertical(columnIndex, rowIndex, lstMergedCells, oldValues, merging, mergeStart, cellview);
        } else {
            row.add(null);
            if (merging.get(columnIndex) != null && merging.get(columnIndex) && mergeStart.get(columnIndex) != null) {
                Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex-1,columnIndex);
                lstMergedCells.add(new Pair<>(mergeStart.get(columnIndex), mergeEnd));
                merging.put(columnIndex, false);
            }
        }
	}

	private void padRows(List<Object> row, int columnCount) {
		for (int i = row.size(); i < columnCount; i++) {
			row.add(null);
		}
	}

	private void _mergeVertical(int columnIndex, int rowIndex, List<Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> lstMergedCells, Map<Integer, Object> oldValues, Map<Integer, Boolean> merging, Map<Integer, Pair<Integer, Integer>> mergeStart, Object cellContent) {
		if (cellContent.equals(oldValues.get(columnIndex))) {
            if (merging.get(columnIndex) == null || !merging.get(columnIndex)) {
                mergeStart.put(columnIndex, new Pair<>(rowIndex-1,columnIndex));
                merging.put(columnIndex, true);
            }
        } else {
            if (merging.get(columnIndex) != null && merging.get(columnIndex) && mergeStart.get(columnIndex) != null) {
                Pair<Integer, Integer> mergeEnd = new Pair<>(rowIndex-1,columnIndex);
                lstMergedCells.add(new Pair<>(mergeStart.get(columnIndex), mergeEnd));
                merging.put(columnIndex, false);
            }
        }
	}

	@Override
	public void save(ImageType imageType, File save) throws IOException {
		sdds.writeAs(save, imageType);
	}
	
	@Override
	public void save(FileType fileType, File save) throws IOException {
		ReportRunner.saveFile(nf, save.getAbsolutePath(), false);
	}

}
