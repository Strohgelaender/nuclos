package org.nuclos.client.customcomp.wizard;

public interface SaveCancelListener {
	void save();
	void cancel();
}
