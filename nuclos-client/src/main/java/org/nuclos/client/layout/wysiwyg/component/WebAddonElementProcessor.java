//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.*;

import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyUtils;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueString;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.layoutml.LayoutMLConstants;

/**
 * This class creates a {@link WebAddonElementProcessor}
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:maik.stueker@nuclos.de">Maik Stueker</a>
 * @version 01.00.00
 */
public class WebAddonElementProcessor implements ComponentProcessor, LayoutMLConstants {

	private final UID webAddonUID;

	private EntityObjectVO<UID> eoWebAddon;

	public WebAddonElementProcessor(String uid) {
		this.webAddonUID = UID.parseUID(uid);
		init();
	}
	
	void init() {
		try {
			eoWebAddon = EntityObjectDelegate.getInstance().get(E.WEBADDON.getUID(), webAddonUID);
		} catch (CommonPermissionException e) {
			Errors.getInstance().showExceptionDialog(null, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.nuclos.client.layout.wysiwyg.component.ComponentProcessors.ComponentProcessor#createEmptyComponent(java.lang.Integer, org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation, java.lang.String)
	 */
	@Override
	public Component createEmptyComponent(Integer iNumber, WYSIWYGMetaInformation metaInf, String name, boolean bDefault) throws CommonBusinessException {
		final WYSIWYGWebAddon element = new WYSIWYGWebAddon(this.eoWebAddon, metaInf);
		
		final String sComponentName = eoWebAddon != null ? eoWebAddon.getFieldValue(E.WEBADDON.name) : "NOT FOUND";
		final ComponentProperties properties = PropertyUtils.getEmptyProperties(element, metaInf);
		properties.setProperty(WYSIWYGWebAddon.PROPERTY_NAME, new PropertyValueString(sComponentName), String.class);
		element.setProperties(properties);

		((Component)element).setMinimumSize(DEFAULTVALUE_LAYOUTCOMPONENT_MINIMUMSIZE);
		return (Component)element;
	}
}
