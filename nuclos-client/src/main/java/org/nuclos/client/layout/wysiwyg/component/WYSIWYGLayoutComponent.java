//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component;

import java.awt.*;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.border.Border;

import org.nuclos.api.Preferences;
import org.nuclos.api.Property;
import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.Alignment;
import org.nuclos.api.ui.AlignmentH;
import org.nuclos.api.ui.AlignmentV;
import org.nuclos.api.ui.DefaultAlignment;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponentFactory;
import org.nuclos.api.ui.layout.LayoutComponentListener;
import org.nuclos.api.ui.layout.TabController;
import org.nuclos.client.layout.LayoutComponentContextImpl;
import org.nuclos.client.layout.LayoutComponentHolder;
import org.nuclos.client.layout.wysiwyg.WYSIWYGMetaInformation;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.ERROR_MESSAGES;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.PROPERTY_LABELS;
import org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.DnDUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.TableLayoutPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * 
 * 
 * 
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:maik.stueker@nuclos.de">Maik Stueker</a>
 * @version 01.00.00
 */
public class WYSIWYGLayoutComponent<PK> extends LayoutComponentHolder<PK> implements WYSIWYGComponent, LayoutComponent<PK>, DefaultAlignment {
	
	public static final Alignment DEFAULT_ALIGNMENT = new Alignment(AlignmentH.FULL, AlignmentV.CENTER);

	public static final String PROPERTY_NAME = PROPERTY_LABELS.NAME;

	public static final String[][] PROPERTIES_TO_LAYOUTML_ATTRIBUTES = new String[][]{
		{PROPERTY_NAME, ATTRIBUTE_NAME}
	};
	
	/**
	 * &lt;!ELEMENT layoutcomponent
	 * ((%layoutconstraints;)?,property*,(%borders;),(%sizes;),font?,description?)&gt;
	 * <p>
	 * &lt;!ATTLIST layoutcomponent class CDATA #IMPLIED enabled (%boolean;) #IMPLIED
	 * editable (%boolean;) #IMPLIED &gt;
	 */
	private ComponentProperties properties;
	
	private final LayoutComponentFactory<PK> lcf;
	
	private final LayoutComponent<PK> lc;
	
	private final JComponent dc;
	
	private LayoutComponentContext context;
	
	public WYSIWYGLayoutComponent(LayoutComponentFactory<PK> lcf, WYSIWYGMetaInformation metaInf) {
		super(lcf.newInstance(new LayoutComponentContextImpl(metaInf.getCollectableEntity().getUID(), LayoutComponentType.DESIGN)), true);
		this.lc = super.getLayoutComponent();
		this.dc = super.getHoldingComponent();
		this.lcf = lcf;
		this.context = new LayoutComponentContextImpl(metaInf.getCollectableEntity().getUID(), LayoutComponentType.DESIGN);
		DnDUtil.addDragGestureListener(this);
	}
	
	public Font getDefaultFont() {
		return dc.getFont();
	}
	
	@Override
	public void setComponentPopupMenu(JPopupMenu popup) {
		super.setComponentPopupMenu(popup);
		setComponentPopupMenu(popup, dc);
	}
	
	private void setComponentPopupMenu(JPopupMenu popup, Component c) {
		if (c instanceof JComponent) {
			((JComponent) c).setComponentPopupMenu(popup);
		}
		if (c instanceof Container) {
			for (Component child : ((Container)c).getComponents()) {
				setComponentPopupMenu(popup, child);
			}
		}
	}

	@Override
	public synchronized void addMouseListener(MouseListener l) {
		super.addMouseListener(l);
		addMouseListener(l, dc);
	}
	
	private void addMouseListener(MouseListener l, Component c) {
		if (c instanceof JComponent) {
			((JComponent) c).addMouseListener(l);
		}
		if (c instanceof Container) {
			for (Component child : ((Container)c).getComponents()) {
				addMouseListener(l, child);
			}
		}
	}

	public Property[] getAdditionalProperties() {
		return this.lc.getComponentProperties();
	}

	public String getLayoutComponentFactoryClass() {
		return this.lcf.getClass().getName();
	}

	@Override
	public List<JMenuItem> getAdditionalContextMenuItems(int click) {
		return null;
	}

	@Override
	public LayoutMLRules getLayoutMLRulesIfCapable() {
		return null;
	}

	@Override
	public WYSIWYGLayoutEditorPanel getParentEditor() {
		if (this.getParent() instanceof TableLayoutPanel) {
			return (WYSIWYGLayoutEditorPanel) this.getParent().getParent();
		}

		throw new CommonFatalException(ERROR_MESSAGES.PARENT_NO_WYSIWYG);
	}

	@Override
	public ComponentProperties getProperties() {
		return properties;
	}

	@Override
	public String[][] getPropertyAttributeLink() {
		return PROPERTIES_TO_LAYOUTML_ATTRIBUTES;
	}

	@Override
	public PropertyClass[] getPropertyClasses() {
		List<PropertyClass> result = new ArrayList<PropertyClass>();
		result.add(new PropertyClass(PROPERTY_NAME, String.class));
		result.add(new PropertyClass(PROPERTY_BORDER, Border.class));
		result.add(new PropertyClass(PROPERTY_PREFFEREDSIZE, Dimension.class));
		
		if (lc.getComponentProperties() != null) {
			for (Property pt : lc.getComponentProperties()) {
				result.add(new PropertyClass(pt.name, pt.type));
			}
		}
		
		return result.toArray(new PropertyClass[]{});
	}

	@Override
	public String[] getPropertyNames() {
		List<String> result = new ArrayList<String>();
		result.add(PROPERTY_NAME);
		result.add(PROPERTY_BORDER);
		result.add(PROPERTY_PREFFEREDSIZE);
		
		if (lc.getComponentProperties() != null) {
			for (Property pt : lc.getComponentProperties()) {
				result.add(pt.name);
			}
		}				
		return result.toArray(new String[]{});
	}

	@Override
	public PropertySetMethod[] getPropertySetMethods() {
		List<PropertySetMethod> result = new ArrayList<WYSIWYGComponent.PropertySetMethod>();
		result.add(new PropertySetMethod(PROPERTY_NAME, "setName"));
		result.add(new PropertySetMethod(PROPERTY_BORDER, "setBorder"));
		result.add(new PropertySetMethod(PROPERTY_PREFFEREDSIZE, "setPreferredSize"));
		
		return result.toArray(new PropertySetMethod[]{});
	}

	@Override
	public String[][] getPropertyValuesFromMetaInformation() {
		return null;
	}

	@Override
	public String[][] getPropertyValuesStatic() {
		return null;
	}

	@Override
	public void setProperties(ComponentProperties properties) {
		this.properties = properties;
		String[] labels = lc.getComponentPropertyLabels();
		if (labels != null && lc.getComponentProperties() != null) {
			for (int i = 0; i < lc.getComponentProperties().length; i++) {
				if (labels.length > i) {
					Property pt = lc.getComponentProperties()[i];
					String label = labels[i];
					this.properties.setPropertyLabel(pt.name, label);
				}
			}
		}
	}

	@Override
	public void setProperty(String property, PropertyValue<?> value, Class<?> valueClass) throws CommonBusinessException {
		properties.setProperty(property, value, valueClass);
	}

	@Override
	public void validateProperties(Map<String, PropertyValue<Object>> values) throws NuclosBusinessException {};
	
	@Override
	public PropertyFilter[] getPropertyFilters() {
		List<PropertyFilter> result = new ArrayList<WYSIWYGComponent.PropertyFilter>();
		result.add(new PropertyFilter(PROPERTY_NAME, ENABLED));
		result.add(new PropertyFilter(PROPERTY_BORDER, ENABLED));
		result.add(new PropertyFilter(PROPERTY_PREFFEREDSIZE, ENABLED));
		
		if (lc.getComponentProperties() != null) {
			for (Property pt : lc.getComponentProperties()) {
				result.add(new PropertyFilter(pt.name, ENABLED));
			}
		}
		
		return result.toArray(new PropertyFilter[]{});
	}

	@Override
	public void setPreferredSize(Dimension preferredSize) {
		if (lc != null)
			lc.setPreferredSize(preferredSize);
	}
	
	@Override
	public void setBorder(Border border) {
		super.setBorder(border);
	}

	@Override
	public void setProperty(String name, Object value) {
		lc.setProperty(name, value);
	}
	
	@Override
	public Property[] getComponentProperties() {
		return lc.getComponentProperties();
	}

	@Override
	public String[] getComponentPropertyLabels() {
		return lc.getComponentPropertyLabels();
	}

	@Override
	public Alignment getDefaultAlignment() {
		Alignment def = lcf.getDefaulAlignment();
		if (def == null) {
			return DEFAULT_ALIGNMENT;
		} else {
			return def;
		}
	}	
	
	@Override
	public JComponent getComponent(LayoutComponentType type) {
		return null; // not here
	}
	
	@Override
	public void setPreferences(Preferences prefs) {
	}

	private boolean bSelected;

	public boolean isSelected() {
		return bSelected;
	}
	public void setSelected(boolean bSelected) {
		this.bSelected = bSelected;
	}
	
	/**
	 * This Method draws a small red box on the {@link WYSIWYGComponent} to indicate existing {@link LayoutMLRules}
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		//Note: this i a replace for false, as this block is essentially commented out
		if (getAlignmentX() == 4.711) {
			UIUtils.fillRectangleForWysiwyg(g, getSize());
		}	
	}

	@Override
	public LayoutComponentContext getContext() {
		return this.context;
	}

	@Override
	public EnumSet<LayoutComponentType> getSupportedTypes() {
		return EnumSet.noneOf(LayoutComponentType.class);
	}
	@Override
	public Collection<LayoutComponentListener<PK>> getLayoutComponentListeners() {
		return Collections.emptyList();
	}

	@Override
	public void addLayoutComponentListener(LayoutComponentListener<PK> listener) {
	}

	@Override
	public void removeLayoutComponentListener(LayoutComponentListener<PK> listener) {
	}

	@Override
	public void setTabController(TabController tabController) {
	}
	
}
