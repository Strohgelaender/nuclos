//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.genericobject.GeneratorActions;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.COMMON_LABELS;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.client.layout.wysiwyg.component.AbstractWYSIWYGTableColumn;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGChart;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableCheckBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComboBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableDateChooser;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableListOfValues;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableTextArea;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGMatrix;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGMatrixColumn;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticButton;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticComboBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticTextarea;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticTextfield;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubForm;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubFormColumn;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGUniversalComponent;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertiesPanel;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueString;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGValuelistProvider;
import org.nuclos.client.resource.ResourceDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.common.DefaultComponentTypes;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.FieldUtils;
import org.nuclos.common.NuclosAttributeNotFoundException;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableEntityProvider;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.Localizable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * This class connects the WYSIWYG Editor with the Backbone.
 * It collects all Data for CollectableComponents, Subforms, Columns shown in Subforms and Attributes.
 *
 * Every WYSIWYG Layout depends on set Metainformation. Otherwise collecting of fieldnames and Subformcolumns would not work.
 *
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class WYSIWYGMetaInformation implements LayoutMLConstants {

	private static final Logger LOG = Logger.getLogger(WYSIWYGMetaInformation.class);
	
	public static final String META_FIELD_NAMES = "meta_field_names";
	public static final String META_COMPONENT_NAMES = "meta_component_names";
	public static final String META_ENTITY_NAMES = "meta_entity_names";
	public static final String META_ENTITY_FIELD_NAMES = "meta_entity_field_names";
	public static final String META_ENTITY_FIELD_NAMES_REFERENCING = "meta_entity_field_names_referencing";
	public static final String META_CONTROLTYPE = "meta_controltype";
	public static final String META_SHOWONLY = "meta_showonly";
	public static final String META_ACTIONCOMMAND_PROPERTIES = "meta_actioncommand_properties";
	public static final String META_TEXTMODULE_ENTITY_NAMES = "meta_textmodule_entity_names";
	public static final String META_TEXTMODULE_ENTITY_FIELD_NAMES = "meta_textmodule_entity_field_names";
	public static final String META_TEXTMODULE_ENTITY_FIELD_SORT = "meta_textmodule_entity_field_sort";

	//NUCLEUSINT-390
	public static final String META_POSSIBLE_PARENT_SUBFORMS = "meta_subforms";
	public static final String META_ICONS = "meta_icons";

	private final Map<String, Integer> mpEnumeratedControlTypes = new HashMap<String, Integer>(5);
	{
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_TEXTFIELD, CollectableComponentTypes.TYPE_TEXTFIELD);
		//NUCLEUSINT-1142
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_PASSWORDFIELD, CollectableComponentTypes.TYPE_PASSWORDFIELD);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_IDTEXTFIELD, CollectableComponentTypes.TYPE_IDTEXTFIELD);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_TEXTAREA, CollectableComponentTypes.TYPE_TEXTAREA);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_COMBOBOX, CollectableComponentTypes.TYPE_COMBOBOX);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_CHECKBOX, CollectableComponentTypes.TYPE_CHECKBOX);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_DATECHOOSER, CollectableComponentTypes.TYPE_DATECHOOSER);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_HYPERLINK, CollectableComponentTypes.TYPE_HYPERLINK);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_EMAIL, CollectableComponentTypes.TYPE_EMAIL);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_PHONENUMBER, CollectableComponentTypes.TYPE_PHONENUMBER);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_OPTIONGROUP, CollectableComponentTypes.TYPE_OPTIONGROUP);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_LISTOFVALUES, CollectableComponentTypes.TYPE_LISTOFVALUES);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_FILECHOOSER, CollectableComponentTypes.TYPE_FILECHOOSER);
		this.mpEnumeratedControlTypes.put(ATTRIBUTEVALUE_IMAGE, CollectableComponentTypes.TYPE_IMAGE);
	}

	private CollectableEntity clcte;
	
	/** used for collecting the informations */
	private CollectableEntityProvider provider = NuclosCollectableEntityProvider.getInstance();

	private UID layoutUID;
	
	public WYSIWYGMetaInformation(UID layoutUID) {
		this.layoutUID = layoutUID;
	}

	/**
	 * Setting the CollectableEntity for this Metainformation.
	 * Without a set CollectableEntity the Metainformation is not correctly initialized.
	 */
	public void setCollectableEntity(CollectableEntity clcte) {
		this.clcte = clcte;
	}

	/**
	 * Method returning the fitting Entity for an attribute
	 *
	 * @param attribute
	 * @return
	 * @throws NuclosAttributeNotFoundException
	 */
	public UID getLinkedEntityForAttribute(UID entity, UID attribute) throws NuclosAttributeNotFoundException{
		//FIX NUCLOSINT-864
		//AttributeCVO attributeVo = AttributeCache.getInstance().getAttribute(entity == null ? this.MetaProvider.getInstance().getEntity(entity.getUID()).getEntityUID() : entity, attribute);
		//return attributeVo.getExternalEntity();
		final FieldMeta<?> field = MetaProvider.getInstance().getEntityField(attribute);
		final UID linkedEntity = field.getForeignEntity() != null ? field.getForeignEntity() : field.getLookupEntity();
		return linkedEntity;
	}

	/**
	 *
	 * @param entity
	 * @param subformColumn
	 * @return
	 * @throws NuclosAttributeNotFoundException
	 * NUCLEUSINT-341
	 */
	public UID getLinkedEntityForSubformColumn(UID entity, UID subformColumn) throws NuclosAttributeNotFoundException{
		CollectableEntityField column = null;
		
		final CollectableEntity e = provider.getCollectableEntity(MetaProvider.getInstance().getEntity(entity).getUID());
		if (e != null) {
			for (FieldMeta<?> f : MetaProvider.getInstance().getEntity(entity).getFields()) {
				if (f.getUID().equals(subformColumn)) {
					column = e.getEntityField(f.getUID());
				}
			}
		}
		if (column != null)
			return column.getReferencedEntityUID();

		return null;
	}

	/**
	 * Gets the {@link CollectableComponentType} for a Subform Column
	 *
	 * @param entity the Entity of the Subform
	 * @param subformColumn the Subform Column which type should be found
	 * @return {@link CollectableComponentTypes}
	 */
	public int getTypeOfSubformField(String entity, String subformColumn) {
		CollectableEntityField column = null;
		
		final CollectableEntity e = provider.getCollectableEntity(MetaProvider.getInstance().getEntity(UID.parseUID(entity)).getUID());
		if (e != null) {
			column = e.getEntityField(MetaProvider.getInstance().getEntity(UID.parseUID(entity)).getField(UID.parseUID(subformColumn)).getUID());
		}
		if (column != null)
			return column.getFieldType();

		return -1;
	}

	public List<FieldMeta<?>> getDependingAttributes(UID entityUid) {
		final List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		for (FieldMeta<?> field : MetaProvider.getInstance().getEntity(entityUid).getFields()) {
			result.add(field);
		}
		Collections.sort(result, new Comparator<FieldMeta<?>>() {
			@Override
			public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
				//return SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o1)
					//	.compareToIgnoreCase(SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o2));
				return o1.getFieldName().compareToIgnoreCase(o2.getFieldName());
			}
		});
		return result;
	}

	/**
	 * Simple getter Method returning the stored CollectableEntity
	 * @return
	 */
	public CollectableEntity getCollectableEntity() {
		return this.clcte;
	}

	/**
	 * Returns the Java Class for a Attribute
	 * @param attributeName
	 * @return {@link Class} of found {@link AttributeCVO}, else null
	 * NUCLEUSINT-811
	 */
	public Class<?> getDatatypeForAttribute(String attributeName) {
		return MetaProvider.getInstance().getEntity(getCollectableEntity().getUID()).getField(UID.parseUID(attributeName)).getJavaClass();
	}

	private static String[] forbiddenEntities = new String[] {
		E.STATEHISTORY.getEntityName(), E.GENERICOBJECTLOGBOOK.getEntityName(), E.GENERICOBJECTRELATION.getEntityName()
	};
	
	/**
	 * Method collecting different Kinds of Data.
	 */
	public List<StringResourceIdPair> getListOfMetaValues(WYSIWYGComponent c, String[] valueFromMeta, PropertiesPanel dialog) {
		List<StringResourceIdPair> result = new ArrayList<StringResourceIdPair>();
		String prop = valueFromMeta[0];
		String meta = valueFromMeta[1];
		if (META_COMPONENT_NAMES.equals(meta)) {
			if (prop.equals(WYSIWYGComponent.PROPERTY_NEXTFOCUSCOMPONENT)) {
				if (!(c instanceof AbstractWYSIWYGTableColumn)) {
					// get static components.
					for (WYSIWYGStaticButton wysiwygStaticComponent : UIUtils.findAllInstancesOf(c.getParentEditor(), WYSIWYGStaticButton.class)) {
						result.add(new StringResourceIdPair(wysiwygStaticComponent.getName(), null));
					}
					for (WYSIWYGStaticComboBox wysiwygStaticComponent : UIUtils.findAllInstancesOf(c.getParentEditor(), WYSIWYGStaticComboBox.class)) {
						result.add(new StringResourceIdPair(wysiwygStaticComponent.getName(), null));
					}
					for (WYSIWYGStaticTextarea wysiwygStaticComponent : UIUtils.findAllInstancesOf(c.getParentEditor(), WYSIWYGStaticTextarea.class)) {
						result.add(new StringResourceIdPair(wysiwygStaticComponent.getName(), null));
					}
					for (WYSIWYGStaticTextfield wysiwygStaticComponent : UIUtils.findAllInstancesOf(c.getParentEditor(), WYSIWYGStaticTextfield.class)) {
						result.add(new StringResourceIdPair(wysiwygStaticComponent.getName(), null));
					}
					//should be possible to unset the next focus component
					result.add(new StringResourceIdPair("", null));
				}
			}
		}
		if (META_FIELD_NAMES.equals(meta)) {
			if (prop.equals(WYSIWYGComponent.PROPERTY_NEXTFOCUSFIELD)) {
				if (c instanceof WYSIWYGSubFormColumn) {
					result = getFittingFieldnames(((WYSIWYGSubFormColumn)c).getSubForm().getEntityUID());
				}
				else if(c instanceof WYSIWYGMatrixColumn) {
					result = getFittingFieldnames(((WYSIWYGMatrixColumn)c).getMatrix().getEntityY());
				}
				else {
					result = getFittingFieldnames();
					// get all entity
					Map<UID, StringResourceIdPair> entityResourceIdPairs = new HashMap<UID, StringResourceIdPair>();
					
					final Collection<EntityMeta<?>> entities = MetaProvider.getInstance().getAllEntities();
					for (EntityMeta<?> entity : entities) {
						entityResourceIdPairs.put(entity.getUID(),
								new StringResourceIdPair(entity.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), //entity.getLocaleResourceIdForLabel()));
										entity.getEntityName()));
					}
					
					// Iterate through Subforms.
					MetaProvider metaProvider = MetaProvider.getInstance();
					Map<UID, WYSIWYGComponent> subformEntitys = c.getParentEditor().getMapSubForms();
					for (Entry<UID, WYSIWYGComponent> subformEntry : subformEntitys.entrySet()) {
						WYSIWYGComponent subform = subformEntry.getValue();
						if (subform instanceof WYSIWYGSubForm && subformEntry.getKey() != null) {
							List<StringResourceIdPair> entityFieldnames = getFittingFieldnames(subformEntry.getKey());
							for (StringResourceIdPair pair : entityFieldnames) {
								result.add(new StringResourceIdPair(
										//metaProvider.getEntityField(UID.parseUID(pair.getX())).getUID().getStringifiedDefinitionWithEntity(subformEntry.getKey()), 
										metaProvider.getEntityField(UID.parseUID(pair.getX())).getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), 
											metaProvider.getEntity(subformEntry.getKey()).getEntityName() + "." + 
												metaProvider.getEntityField(UID.parseUID(pair.getX())).getFieldName()));
							}
						}
					}
					
				}
				//should be possible to unset the next focus component
				result.add(new StringResourceIdPair("", null));
			}
			else
				result = getFittingFieldnamesForControlType(c);
		} else if (META_ENTITY_NAMES.equals(meta)) {
			final Collection<EntityMeta<?>> entities = MetaProvider.getInstance().getAllEntities();
			final Set<UID> allUsedEntities = new HashSet<UID>(c.getParentEditor().getMainEditorPanel().getSubFormAndChartAndMatrixEntityUIDs());
			if (c instanceof WYSIWYGSubForm) {
				UID currentUsedEntity = ((WYSIWYGSubForm)c).getEntityUID();
				allUsedEntities.remove(currentUsedEntity); // null is ok for a HashSet
			} else if (c instanceof WYSIWYGChart) {
				UID currentUsedEntity = ((WYSIWYGChart)c).getEntityUID();
				allUsedEntities.remove(currentUsedEntity); // null is ok for a HashSet
			} else if (c instanceof WYSIWYGMatrix) {
				UID currentUsedEntity = null;
				currentUsedEntity = getEntityFromMatrixComponent(c,	valueFromMeta, currentUsedEntity);				
				allUsedEntities.remove(currentUsedEntity); // null is ok for a HashSet
			}
			
			for (EntityMeta<?> entity : entities) {
				if (!allUsedEntities.contains(entity.getUID())) {
					if (c instanceof WYSIWYGChart) {
						// only allow chart entities
						if (entity.isChart())
							result.add(new StringResourceIdPair(entity.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), //entity.getLocaleResourceIdForLabel()));
									entity.getEntityName()));
					} else if(c instanceof WYSIWYGMatrix && valueFromMeta != null && WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_MATRIX.equals(valueFromMeta[0])) {
						// matrix cell BO needs at least 2 references
						if (entity.getFields().stream().filter(fieldVO -> fieldVO.getForeignEntityField() != null).count() > 1) {
							result.add(new StringResourceIdPair(entity.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), entity.getEntityName()));
						}
					}
					else if(c instanceof WYSIWYGMatrix && valueFromMeta != null && WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_Y.equals(valueFromMeta[0])) {
						int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_MATRIX);
						String matrixEntity = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
						int iField = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_MATRIX_PARENT_FIELD);
						String field = iField == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iField, 1)).getValue();
						UID uidField = UID.parseUID(field);
						if(matrixEntity == null || field == null) {
							continue;
						}
						
						try {
							FieldMeta<?> fieldVO = MetaProvider.getInstance().getEntityField(uidField);
							UID sForeignEntity = fieldVO.getForeignEntity();
							if(entity.getUID().equals(sForeignEntity)) {
								result.add(new StringResourceIdPair(entity.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), entity.getEntityName()));
							}	
						}
						catch(CommonFatalException ex) {
							LOG.warn("Entity and Field combination not allowed");
						}
					}
					else if(c instanceof WYSIWYGMatrix && valueFromMeta != null && WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_X.equals(valueFromMeta[0])) {
						int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_MATRIX);
						String matrixEntity = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
						int iField = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_MATRIX_X_REF_FIELD);
						String field = iField == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iField, 1)).getValue();
						if(matrixEntity == null || field == null) {
							continue;
						}
						UID uidField = UID.parseUID(field);
						try {
							FieldMeta<?> fieldVO = MetaProvider.getInstance().getEntityField(uidField);
							UID sForeignEntity = fieldVO.getForeignEntity();
							if(entity.getUID().equals(sForeignEntity)) {
								result.add(new StringResourceIdPair(entity.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), entity.getEntityName()));
							}	
						}
						catch(CommonFatalException ex) {
							LOG.warn("Entity and Field combination not allowed");
						}
					}
					
					else {
						// remove all chart entities
						if (!entity.isChart())
							result.add(new StringResourceIdPair(entity.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), //entity.getLocaleResourceIdForLabel()));
									entity.getEntityName()));
					}					
				}
			}

			//@see NUCLOS-1327
			UID sComponentEntity = null;
			final UID sParentEntity = clcte.getUID();
			if (c instanceof WYSIWYGSubForm) {
				sComponentEntity = ((WYSIWYGSubForm)c).getEntityUID();
			}
			if (c instanceof WYSIWYGChart) {
				sComponentEntity = ((WYSIWYGChart)c).getEntityUID();
			}
			if (c instanceof WYSIWYGMatrix) {
				sComponentEntity = getEntityFromMatrixComponent(c, valueFromMeta, sComponentEntity);		
			}
			boolean isVirtual = !RigidUtils.looksEmpty(MetaProvider.getInstance().getEntity(clcte.getUID()).getVirtualEntity());
			for (Iterator iterator = result.iterator(); iterator.hasNext();) {
				final String sEntityName = ((StringResourceIdPair) iterator.next()).getX();
				if (LangUtils.equal(UID.parseUID(sEntityName), sComponentEntity))
					continue;
				if (Arrays.asList(forbiddenEntities).contains(sEntityName)) {
					iterator.remove();
					continue;
				}
				
				if (c instanceof WYSIWYGMatrix) {
					continue;
				}
				
				try {
					// Virtuelle Entitaeten koennen alle moeglichen Unterformulare enthalten. NUCLOS-2761
					if (!isVirtual) {
						UID uidEntity = UID.parseUID(sEntityName);
						Collection<FieldMeta<?>> metaFieldVOs = FieldUtils.getFieldsReferencing(MetaProvider.getInstance().getAllEntityFieldsByEntity(uidEntity).values(), sParentEntity); 
						if (metaFieldVOs.isEmpty()) {
							FieldMeta<?> metaFieldVO = null;
							for (FieldMeta<?> fmeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(uidEntity).values()) {
								if (LangUtils.equal(fmeta.getForeignEntity(), E.GENERICOBJECT.getUID())) {
									metaFieldVO = fmeta;
									break;
								}
							}
							if (metaFieldVO == null) {
								boolean bRemove = true;
								for (UID sUsedEntity : allUsedEntities) {
									metaFieldVOs = FieldUtils.getFieldsReferencing(MetaProvider.getInstance().getAllEntityFieldsByEntity(uidEntity).values(), sUsedEntity);
									if (!metaFieldVOs.isEmpty()) {
										bRemove = false;
										break;
									}
								}
								if (bRemove) {
									iterator.remove();
									continue;
								}
							}
							else {
								boolean bRemove = true;
								for (UID s : provider.getCollectableEntity(uidEntity).getFieldUIDs()) {
									final CollectableEntityField clctef = provider.getCollectableEntity(uidEntity).getEntityField(s); 
									if (clctef.isReferencing()) {
										if (clctef.getReferencedEntityUID() != null
												&& (clctef.getReferencedEntityUID().equals(sParentEntity)
														|| clctef.getReferencedEntityUID().equals(E.GENERICOBJECT.getUID()))) {
											bRemove = false;
											break;
										}
									}
								}
	
								if (bRemove) {
									iterator.remove();
									continue;
								}
							}
						}
					}
				} catch (Exception e) {
					iterator.remove();
				}
			}	
		} else if (META_TEXTMODULE_ENTITY_NAMES.equals(meta)) {
			for (final EntityMeta<?> metaEntity : MetaProvider.getInstance().getAllEntities()) {
				if (E.isNuclosEntity(metaEntity.getUID())) {
					continue;
				}
				
				result.add(new StringResourceIdPair(metaEntity.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), //metaEntity.getLocaleResourceIdForLabel()));
						metaEntity.getEntityName()));
			}
		} else if (META_ENTITY_FIELD_NAMES.equals(meta)) {
			if (c instanceof WYSIWYGSubForm || c instanceof WYSIWYGChart) {
				String e = ((PropertyValueString)dialog.getModel().getValueAt(getDialogTableModelPropertyRowIndex(dialog, WYSIWYGSubForm.PROPERTY_ENTITY), 1)).getValue();
				if (!StringUtils.isNullOrEmpty(e)) {
					for (FieldMeta<?> f : MetaProvider.getInstance().getEntity(UID.parseUID(e)).getFields()) {
						result.add(new StringResourceIdPair(f.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), //f.getLocaleResourceIdForLabel()));
								f.getFieldName()));
					}
				}
			}
		} else if(META_TEXTMODULE_ENTITY_FIELD_NAMES.equals(meta) || META_TEXTMODULE_ENTITY_FIELD_SORT.equals(meta)) {
			if (c instanceof WYSIWYGCollectableTextArea || c instanceof WYSIWYGSubFormColumn || c instanceof WYSIWYGUniversalComponent) {
				String e = ((PropertyValueString)dialog.getModel().getValueAt(getDialogTableModelPropertyRowIndex(dialog, WYSIWYGCollectableTextArea.PROPERTY_TEXTMODULE_ENTITY), 1)).getValue();
				if (!StringUtils.isNullOrEmpty(e)) {
					UID eUID = UID.parseUID(e);
					for (UID s : provider.getCollectableEntity(eUID).getFieldUIDs()) {
						// filter text fields
						final FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(s);
						if (META_TEXTMODULE_ENTITY_FIELD_SORT.equals(meta) || String.class.getName().equals(fMeta.getDataType())) {
							result.add(new StringResourceIdPair(fMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), //fMeta.getLocaleResourceIdForLabel()));
									fMeta.getFieldName()));
						}
					}
				}
			}
		} else if (META_ENTITY_FIELD_NAMES_REFERENCING.equals(meta)) {
			if (c instanceof WYSIWYGSubForm || c instanceof WYSIWYGChart) {
				String e = ((PropertyValueString)dialog.getModel().getValueAt(getDialogTableModelPropertyRowIndex(dialog, WYSIWYGSubForm.PROPERTY_ENTITY), 1)).getValue();
				if (!StringUtils.isNullOrEmpty(e)) {
					final int parentSubform = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGSubForm.PROPERTY_PARENT_SUBFORM); // in charts there is no parent subform.
					final UID eParent = parentSubform == -1 ? null : UID.parseUID(((PropertyValueString)dialog.getModel().getValueAt(parentSubform, 1)).getValue());
					UID eParentUID = clcte.getUID();
					if (eParent != null)
						eParentUID = eParent;
					final UID eUID = UID.parseUID(e); 
					boolean isParentVirtual = !RigidUtils.looksEmpty(MetaProvider.getInstance().getEntity(eParentUID).getVirtualEntity());
					for (UID s : provider.getCollectableEntity(eUID).getFieldUIDs()) {
						if (provider.getCollectableEntity(eUID).getEntityField(s).isReferencing()) {
							final FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(s);
							boolean add = false;
							if (isParentVirtual) {
								// Wenn Parent eine virtuelle Entitaet ist koennte es jedes referenzierende Feld sein. NUCLOS-2761
								add = true;
							} else if (prop.equals("Unique Mastercolumn")) {
								add = true;
							} else if (provider.getCollectableEntity(eUID).getEntityField(s).getReferencedEntityUID() != null
									&& (provider.getCollectableEntity(eUID).getEntityField(s).getReferencedEntityUID().equals(eParentUID)
											|| provider.getCollectableEntity(eUID).getEntityField(s).getReferencedEntityUID().equals(E.GENERICOBJECT.getUID()))) {
								add = true;
							}
							if (add) {
								result.add(new StringResourceIdPair(fMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), //fMeta.getLocaleResourceIdForLabel()));
										fMeta.getFieldName()));
							}
						}
					}
					//should be possible to unset the unique mastercolum
					result.add(new StringResourceIdPair("", null));
				}
			} else if(c instanceof WYSIWYGMatrix) {
				String e = null;
				if(WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_FIELD_X.equals(valueFromMeta[0])) {
					int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_X);
					e = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
				} else if(WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_FIELD_CATEGORIE.equals(valueFromMeta[0])) {
					int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_X);
					e = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
				} else if(WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_MATRIX_PARENT_FIELD.equals(valueFromMeta[0])) {
					int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_MATRIX);
					e = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
				} else if(WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_Y_PARENT_FIELD.equals(valueFromMeta[0])) {
					int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_Y);
					e = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
				} else if(WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_FIELD_Y.equals(valueFromMeta[0])) {
					int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_Y);
					e = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
				} else if(WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_MATRIX_X_REF_FIELD.equals(valueFromMeta[0])) {
					int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_MATRIX);
					e = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
				} else if(WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_MATRIX_VALUE_FIELD.equals(valueFromMeta[0])) {
					int iEntity = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGMatrix.PROPERTY_ENTITY_MATRIX);
					e = iEntity == -1 ? null : ((PropertyValueString)dialog.getModel().getValueAt(iEntity, 1)).getValue();
				}  else if(WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_FIELD_PREFERENCES.equals(valueFromMeta[0])) {
					String sEntity = getCollectableEntity() != null ? getCollectableEntity().getUID().getStringifiedDefinitionWithEntity(E.ENTITY) : null;
					e = sEntity;
				} 
				
				if (!StringUtils.isNullOrEmpty(e)) {		
					
					for (UID s : provider.getCollectableEntity(UID.parseUID(e)).getFieldUIDs()) {
						result.add(new StringResourceIdPair(s.getStringifiedDefinitionWithEntity(E.ENTITYFIELD), MetaProvider.getInstance().getEntityField(s).getFieldName()));
						//result.add(new StringResourceIdPair(MetaProvider.getInstance().getEntityField(s).getFieldName(), null));
					}
				}
			}
		} else if (META_CONTROLTYPE.equals(meta)) {
			result = makeStringsWithNullResourceId(getValidControlTypesForCollectableComponent(c, dialog));
		} else if (META_SHOWONLY.equals(meta)) {
			result = makeStringsWithNullResourceId(getShowOnlyTag(dialog,c));
		} else if (META_POSSIBLE_PARENT_SUBFORMS.equals(meta)) {
			//NUCLEUSINT-390
			WYSIWYGLayoutEditorPanel mainPanel = c.getParentEditor().getMainEditorPanel();
			List<Object> subforms = new ArrayList<Object>();
			c.getParentEditor().getWYSIWYGComponents(WYSIWYGSubForm.class, mainPanel, subforms);
			UID ownEntity = ((WYSIWYGSubForm)c).getEntityUID();
			UID entity = null;
			for (Object subformFromPanel : subforms) {
				entity = ((WYSIWYGSubForm) subformFromPanel).getEntityUID();
				if (entity == null)
					continue;
				final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(entity);
				//filter the own entity, only other subforms are valid entries
				if (ownEntity == null)
					result.add(new StringResourceIdPair(eMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), //eMeta.getLocaleResourceIdForLabel()));
							eMeta.getEntityName()));
				else if (!ownEntity.equals(entity))
					result.add(new StringResourceIdPair(eMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITY), //eMeta.getLocaleResourceIdForLabel()));
							eMeta.getEntityName()));
			}
			// should be possible to deselect a parent subform
			result.add(new StringResourceIdPair("", null));
		} else if (META_ACTIONCOMMAND_PROPERTIES.equals(meta)) {
			PropertyValue propActionCommand = c.getProperties().getProperty(WYSIWYGStaticButton.PROPERTY_ACTIONCOMMAND);
			if (propActionCommand != null) {
				String actionCommand = ((PropertyValueString)dialog.getModel().getValueAt(getDialogTableModelPropertyRowIndex(dialog, WYSIWYGStaticButton.PROPERTY_ACTIONCOMMAND), 1)).getValue();
				if (actionCommand != null) {
					if (STATIC_BUTTON.DUMMY_BUTTON_ACTION_LABEL.equals(actionCommand))
						; // do nothing
					else if (STATIC_BUTTON.STATE_CHANGE_ACTION_LABEL.equals(actionCommand)) {
						Collection<StateVO> collStates = getStates();
						for (StateVO state: collStates) {
							result.add(new StringResourceIdPair(state.getId().getStringifiedDefinitionWithEntity(E.STATE), 
									state.getNumeral().toString() + " " + state.getStatename(LocaleDelegate.getInstance().getLocale())));
						}
					} else if (STATIC_BUTTON.EXECUTE_RULE_ACTION_LABEL.equals(actionCommand)) {
						//NUCLOSINT-743 get all the user driven rules for this entity
						Collection<EventSupportSourceVO> collRules = getRules();
						for (EventSupportSourceVO rule: collRules) {
							result.add(new StringResourceIdPair(rule.getClassname(), rule.getName()));
						}		
					} else if (STATIC_BUTTON.GENERATOR_ACTION_LABEL.equals(actionCommand)) {
						Collection<GeneratorActionVO> collGenerators = getGeneratorActions();
						for (GeneratorActionVO generator: collGenerators) {
							result.add(new StringResourceIdPair(generator.getId().getStringifiedDefinitionWithEntity(E.GENERATION), generator.getName()));
						}
					}  else if (STATIC_BUTTON.HYPERLINK_ACTION_LABEL.equals(actionCommand)) {
						Collection<FieldMeta<?>> collHyperlinks = getFieldsByControlType(ATTRIBUTEVALUE_HYPERLINK);
						for (FieldMeta<?> efMeta : collHyperlinks) {
							result.add(new StringResourceIdPair(efMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), efMeta.getFieldName()));
								//efMeta.getLocaleResourceIdForLabel()));
						}
					} 
				}
			}
		} else if (META_ICONS.equals(meta)) {
			result.addAll(CollectionUtils.transform(ResourceDelegate.getInstance().getIconResources(), new Transformer<UID, StringResourceIdPair>() {
				@Override
				public StringResourceIdPair transform(UID i) {
					return new StringResourceIdPair(i.getStringifiedDefinitionWithEntity(E.RESOURCE), ResourceDelegate.getInstance().getResourceById(i).getName());
				}
			}));
		}

		return result;
	}

	private Collection<EventSupportSourceVO> getRules() {
		return EventSupportRepository.getInstance().getActiveCustomRules(getCollectableEntity().getUID());
	}

	private Collection<StateVO> getStates() {
		if (Modules.getInstance().isModule(getCollectableEntity().getUID())) {
			return StateDelegate.getInstance().getStatesByModule(getCollectableEntity().getUID());
		}

		return Collections.emptyList();
	}

	private Collection<GeneratorActionVO> getGeneratorActions() {
		final List<GeneratorActionVO> result = GeneratorActions.getGeneratorActions(getCollectableEntity().getUID());
		
		if (result == null)
			return Collections.EMPTY_LIST;
		
		/*
		 * NUCLOS-4546
		// remove inactive actions
		CollectionUtils.removeAll(result, new Predicate<GeneratorActionVO>() {
			@Override
			public boolean evaluate(GeneratorActionVO genvo) {
				return genvo.isRuleOnly();
			}
		});
		*/
		return GeneratorActions.sort(result);
	}

	/**
	 * Externalized Method called by public List<String> getListOfMetaValues(WYSIWYGComponent c, String meta, PropertiesDialog dialog)
	 *
	 * Gets values to display in the show-only combobox in the PropertiesDialog
	 * @param dialog
	 * @return
	 */
	private List<String> getShowOnlyTag(PropertiesPanel dialog, WYSIWYGComponent c){
		List<String> result = new ArrayList<String>();
		result.add(COMMON_LABELS.EMPTY);
		result.add(ATTRIBUTEVALUE_LABEL);
		result.add(ATTRIBUTEVALUE_CONTROL);

		UID fieldUID;
		int modelIndex = getDialogTableModelPropertyRowIndex(dialog, WYSIWYGUniversalComponent.PROPERTY_NAME);
		if (modelIndex != -1) {
			fieldUID = UID.parseUID(((PropertyValue<String>)dialog.getModel().getValueAt(modelIndex, 1)).getValue());
		}
		else {
			fieldUID = UID.parseUID(((PropertyValue<String>)c.getProperties().getProperty(WYSIWYGUniversalComponent.PROPERTY_NAME)).getValue());
		}

		if (fieldUID != null){
			if (getEntityField(clcte.getUID(), fieldUID).getDefaultCollectableComponentType() == CollectableComponentTypes.TYPE_LISTOFVALUES) {
				result.add(ATTRIBUTEVALUE_BROWSEBUTTON);
			}
		}
		return result;
	}

	/**
	 * Externalized Method called by public List<String> getListOfMetaValues(WYSIWYGComponent c, String meta, PropertiesDialog dialog)
	 * Collects fitting ControlTypes for choosing in PropertiesDialog.
	 * @param c
	 * @param dialog
	 * @return List<String> with all fitting ControlTypes found
	 */
	private List<String> getValidControlTypesForCollectableComponent(WYSIWYGComponent c, PropertiesPanel dialog){
		final List<String> result = new ArrayList<String>();
		
		UID fieldUID = null;
		UID entityUID = clcte.getUID();
		result.add(COMMON_LABELS.EMPTY);
		if (c instanceof WYSIWYGSubFormColumn) {
			entityUID = ((WYSIWYGSubFormColumn)c).getSubForm().getEntityUID();
			fieldUID = ((WYSIWYGSubFormColumn)c).getEntityField().getUID();
		}
		else if(c instanceof WYSIWYGMatrixColumn) {
			entityUID = ((WYSIWYGMatrixColumn)c).getMatrix().getEntityY();
			fieldUID = ((WYSIWYGMatrixColumn)c).getMatrix().getEntityYParentField();
		}
		else if (c instanceof WYSIWYGUniversalComponent) {
			fieldUID = UID.parseUID(((PropertyValue<String>)dialog.getModel().getValueAt(getDialogTableModelPropertyRowIndex(dialog, WYSIWYGUniversalComponent.PROPERTY_NAME), 1)).getValue());
		}
		if (fieldUID != null) {
			CollectableEntityField field = getEntityField(entityUID, fieldUID);
			switch (field.getDefaultCollectableComponentType()) {
				case CollectableComponentTypes.TYPE_COMBOBOX:
				case CollectableComponentTypes.TYPE_LISTOFVALUES:
				case CollectableComponentTypes.TYPE_IDTEXTFIELD:
					result.add(ATTRIBUTEVALUE_COMBOBOX);
					result.add(ATTRIBUTEVALUE_LISTOFVALUES);
					result.add(ATTRIBUTEVALUE_IDTEXTFIELD);
					break;
				case CollectableComponentTypes.TYPE_TEXTAREA:
				case CollectableComponentTypes.TYPE_TEXTFIELD:
					result.add(ATTRIBUTEVALUE_TEXTFIELD);
					result.add(ATTRIBUTEVALUE_TEXTAREA);
					result.add(ATTRIBUTEVALUE_HYPERLINK);
					result.add(ATTRIBUTEVALUE_EMAIL);
					break;
				case CollectableComponentTypes.TYPE_CHECKBOX:
					result.add(ATTRIBUTEVALUE_CHECKBOX);
					break;
				case CollectableComponentTypes.TYPE_DATECHOOSER:
					result.add(ATTRIBUTEVALUE_DATECHOOSER);
					break;
				case CollectableComponentTypes.TYPE_HYPERLINK:
					result.add(ATTRIBUTEVALUE_TEXTFIELD);
					result.add(ATTRIBUTEVALUE_HYPERLINK);
					result.add(ATTRIBUTEVALUE_EMAIL);
					break;
				case CollectableComponentTypes.TYPE_EMAIL:
					result.add(ATTRIBUTEVALUE_TEXTFIELD);
					result.add(ATTRIBUTEVALUE_HYPERLINK);
					result.add(ATTRIBUTEVALUE_EMAIL);
					break;
				case CollectableComponentTypes.TYPE_PHONENUMBER:
					result.add(ATTRIBUTEVALUE_TEXTFIELD);
					result.add(ATTRIBUTEVALUE_PHONENUMBER);
					break;
				case CollectableComponentTypes.TYPE_OPTIONGROUP:
					result.add(ATTRIBUTEVALUE_OPTIONGROUP);
					break;
				case CollectableComponentTypes.TYPE_FILECHOOSER:
					result.add(ATTRIBUTEVALUE_FILECHOOSER);
					break;
				case CollectableComponentTypes.TYPE_IMAGE:
					result.add(ATTRIBUTEVALUE_IMAGE);
					break;
				case CollectableComponentTypes.TYPE_PASSWORDFIELD:
					//NUCLEUSINT-1142
					result.add(ATTRIBUTEVALUE_PASSWORDFIELD);
					break;

			}
		}

		//NUCLEUSINT-429 if valuelist provider defined for subform colum a checkbox is also valid
		if (c instanceof WYSIWYGSubFormColumn){
			WYSIWYGValuelistProvider wysiwygStaticValuelistProvider = (WYSIWYGValuelistProvider) c.getProperties().getProperty(WYSIWYGSubFormColumn.PROPERTY_VALUELISTPROVIDER).getValue();
			if (wysiwygStaticValuelistProvider != null)
				if (wysiwygStaticValuelistProvider.getType() != null){
					if (!result.contains(ATTRIBUTEVALUE_COMBOBOX))
						result.add(ATTRIBUTEVALUE_COMBOBOX);
				}

		}
		Collections.sort(result, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return LangUtils.compare(o1, o2);
			}
		});
		return result;
	}

	/**
	 * Externalized Method called by public List<String> getListOfMetaValues(WYSIWYGComponent c, String meta, PropertiesDialog dialog)
	 * Performs a lookup on fitting Fields for a ControlType
	 *
	 * @param c
	 * @return
	 */
	private List<StringResourceIdPair> getFittingFieldnames(){
		return getFittingFieldnames(clcte.getUID());
	}

	/**
	 * Externalized Method called by public List<String> getListOfMetaValues(WYSIWYGComponent c, String meta, PropertiesDialog dialog)
	 * Performs a lookup on fitting Fields for a ControlType
	 *
	 * @param c
	 * @return
	 */
	private List<StringResourceIdPair> getFittingFieldnames(UID entityUID){
		List<StringResourceIdPair> result = new ArrayList<StringResourceIdPair>();
		for (FieldMeta<?> fMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUID).values()) {
			result.add(new StringResourceIdPair(fMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), //fMeta.getLocaleResourceIdForLabel()));
					fMeta.getFieldName()));
		}

		return result;
	}

	/**
	 * Externalized Method called by public List<String> getListOfMetaValues(WYSIWYGComponent c, String meta, PropertiesDialog dialog)
	 * Performs a lookup on fitting Fields for a ControlType
	 *
	 * @param c
	 * @return
	 */
	private List<StringResourceIdPair> getFittingFieldnamesForControlType(WYSIWYGComponent c){
		List<StringResourceIdPair> result = new ArrayList<StringResourceIdPair>();
		if (Modules.getInstance().isModule(clcte.getUID())) {
			for (AttributeCVO a : AttributeCache.getInstance().getAttributes()) {
				if (c instanceof WYSIWYGCollectableComboBox) {
					if (a.isIdField()) {
						result.add(new StringResourceIdPair(a.getId().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), a.getResourceSIdForLabel()));
					}
				}
				else if (c instanceof WYSIWYGCollectableListOfValues) {
					if (a.getExternalEntity() != null) {
						result.add(new StringResourceIdPair(a.getId().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), a.getResourceSIdForLabel()));
					}
				}
				else if (c instanceof WYSIWYGCollectableDateChooser) {
					if (Date.class.isAssignableFrom(a.getJavaClass())) {
						result.add(new StringResourceIdPair(a.getId().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), a.getResourceSIdForLabel()));
					}
				}
				else if (c instanceof WYSIWYGCollectableCheckBox) {
					if (Boolean.class.isAssignableFrom(a.getJavaClass())) {
						result.add(new StringResourceIdPair(a.getId().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), a.getResourceSIdForLabel()));
					}
				}
				else {
					result.add(new StringResourceIdPair(a.getId().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), a.getResourceSIdForLabel()));
				}
			}
		}
		else {
			for (FieldMeta<?> fMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(clcte.getUID()).values()) {
				result.add(new StringResourceIdPair(fMeta.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD), //fMeta.getLocaleResourceIdForLabel()));
						fMeta.getFieldName()));
			}
		}

		return result;
	}
	
	private UID getEntityFromMatrixComponent(WYSIWYGComponent c, String[] valueFromMeta, UID currentUsedEntity) {
		if(valueFromMeta != null && WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_X.equals(valueFromMeta[0])) {
			currentUsedEntity = ((WYSIWYGMatrix)c).getEntityX();
		} else if(valueFromMeta != null && WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_Y.equals(valueFromMeta[0])) {
			currentUsedEntity = ((WYSIWYGMatrix)c).getEntityY();
		} else if(valueFromMeta != null && WYSIWYGStringsAndLabels.PROPERTY_LABELS.ENTITY_MATRIX.equals(valueFromMeta[0])) {
			currentUsedEntity = ((WYSIWYGMatrix)c).getEntityMatrix();
		}
		return currentUsedEntity;
	}

	/**
	 * Method for checking if the Metainformation is correctly initialized.
	 * Without a set CollectableEntity it is not initialized.
	 * @return
	 */
	public synchronized boolean isMetaInformationSet(){
		return clcte != null;
	}

	/**
	 * This Method returns the Label for a CollectableComponent
	 * 
	 * @param fieldUID the CollectableComponents name
	 */
	public synchronized String getLabelForCollectableComponent(UID fieldUID) {
		try {
			final FieldMeta<?> field = MetaProvider.getInstance().getEntityField(fieldUID);
			//return field.getFieldName();
			return SpringLocaleDelegate.getInstance().getResource(
					field.getLocaleResourceIdForLabel(), field.getFallbackLabel());
		} catch (Exception e) {
			// ignore
		}
		return null;
	}

	/**
	 * returns the SubformColumns for a SubformEntity
	 * @param entity
	 * @return
	 */
	public synchronized List<FieldMeta<?>> getSubFormColumns(UID entity){
		final List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		if (entity != null) {
			CollectableEntity e = provider.getCollectableEntity(MetaProvider.getInstance().getEntity(entity).getUID());
			for (UID fieldUid : e.getFieldUIDs()) {
				result.add(MetaProvider.getInstance().getEntityField(fieldUid));
			}
		}
		Collections.sort(result, new Comparator<FieldMeta<?>>() {
			@Override
			public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
				//return SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o1)
					//	.compareToIgnoreCase(SpringLocaleDelegate.getInstance().getLabelFromMetaFieldDataVO(o2));
				return o1.getFieldName().compareToIgnoreCase(o2.getFieldName());
			}
		});
		return result;
	}

	public CollectableEntityField getEntityField(UID entityUID, UID fieldUID) {
		if (entityUID == null) {
			return null;
		}

		if (Modules.getInstance().isModule(entityUID)) {
			final FieldMeta<?> efMeta = MetaProvider.getInstance().getEntityField(fieldUID);
			CollectableEntityField cefStateReplacement = null;
			Map<UID, Permission> mpPermissions = null;
			if (efMeta.isCalcAttributeAllowCustomization()) {
				mpPermissions = AttributeCache.getInstance().getAttribute(LangUtils.defaultIfNull(efMeta.getCalcBaseFieldUID(), efMeta.getUID())).getPermissions();
			} else {
				mpPermissions = AttributeCache.getInstance().getAttribute(efMeta.getUID()).getPermissions();
			}
			return new CollectableGenericObjectEntityField(mpPermissions, efMeta, entityUID);
		} else {
			return provider.getCollectableEntity(entityUID).getEntityField(fieldUID);
		}
	}

	public List<FieldMeta<?>> getFieldsByControlType(String controlType) {
		List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>();
		try {
			for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(clcte.getUID()).values()) {
				if (efMeta.isCalcOndemand()) {
					continue;
				}
				if (ATTRIBUTEVALUE_COMBOBOX.equals(controlType)) {
					if (efMeta.getForeignEntity()!=null) {
						result.add(efMeta);
					}
					if (efMeta.getLookupEntity() != null) {
						result.add(efMeta);
					}
				}
				else if (ATTRIBUTEVALUE_LISTOFVALUES.equals(controlType)) {
					if (efMeta.getForeignEntity()!=null) {
						result.add(efMeta);
					}
					if (efMeta.getLookupEntity() != null) {
						result.add(efMeta);
					}
				}
				else if (ATTRIBUTEVALUE_DATECHOOSER.equals(controlType)) {
					if (Date.class.isAssignableFrom(Class.forName(efMeta.getDataType()))) {
						result.add(efMeta);
					}
				}
				else if (ATTRIBUTEVALUE_CHECKBOX.equals(controlType)) {
					if (Boolean.class.isAssignableFrom(Class.forName(efMeta.getDataType()))) {
						result.add(efMeta);
					}
				}
				else if(ATTRIBUTEVALUE_IMAGE.equals(controlType)) {
					if(NuclosImage.class.isAssignableFrom(Class.forName(efMeta.getDataType()))) {
						result.add(efMeta);
					}
				}
				else if(ATTRIBUTEVALUE_PASSWORDFIELD.equals(controlType)) {
					//NUCLEUSINT-1142
					if(NuclosPassword.class.isAssignableFrom(Class.forName(efMeta.getDataType()))) {
						result.add(efMeta);
					}
				}
				else if (ATTRIBUTEVALUE_HYPERLINK.equals(controlType)) {
					if (DefaultComponentTypes.HYPERLINK.equals(efMeta.getDefaultComponentType())) {
						result.add(efMeta);
					}
					else if (efMeta.getForeignEntity()!=null) {
						result.add(efMeta);
					}
				}
				else if (ATTRIBUTEVALUE_EMAIL.equals(controlType)) {
					if (DefaultComponentTypes.EMAIL.equals(efMeta.getDefaultComponentType())) {
						result.add(efMeta);
					}
				}
				else if (ATTRIBUTEVALUE_PHONENUMBER.equals(controlType)) {
					if (DefaultComponentTypes.PHONENUMBER.equals(efMeta.getDefaultComponentType())) {
						result.add(efMeta);
					}
				}
				else {
					result.add(efMeta);
				}
			}
		} catch(ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
		return result;
	}

	/**
	 * Find the table model row index for a certain property
	 *
	 * @param dialog The current property dialog
	 * @param property The name of the property
	 * @return the index of a certain property, -1 if property was not found in table model
	 */
	private static int getDialogTableModelPropertyRowIndex(PropertiesPanel dialog, String property) {
		for (int i = 0; i < dialog.getModel().getRowCount(); i++) {
			if (((String)dialog.getModel().getValueAt(i, 0)).equals(property)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Maps the displayed controltype to the int that is used internal
	 * @param attributeValue
	 * @return
	 */
	public Integer getCollectableComponentType(String attributeValue) {
		return mpEnumeratedControlTypes.get(attributeValue);
	}

	public static List<StringResourceIdPair> makeStringsWithNullResourceId(Collection<String> values) {
		return CollectionUtils.transform(values, new Transformer<String, StringResourceIdPair>() {
			@Override
			public StringResourceIdPair transform(String value) {
				return new StringResourceIdPair(value, null);
			}
		});
	}
	
	/**
	 * for internal entities only!
	 */
	private static Map<String, UID> compatibilityMap;
	
	public static Map<String, UID> getCompatibilityMap() {
		if (compatibilityMap == null) {
			compatibilityMap = new HashMap<String, UID>();
			for (EntityMeta<?> eMeta : E.getAllEntities()) {
				compatibilityMap.put(eMeta.getEntityName(), eMeta.getUID());
			}
		}
		return compatibilityMap;
	}

	public List<FieldMeta<?>> getAllFields() {
		List<FieldMeta<?>> result = new ArrayList<FieldMeta<?>>(MetaProvider.getInstance().getAllEntityFieldsByEntity(clcte.getUID()).values());
		return result;
	}
	
	public UID getFieldUIDFromCollectableEntity(String sFieldName) {
		return getFieldUID(clcte == null ? null : clcte.getUID(), sFieldName);
	}

	public static UID getFieldUID(UID entity, String sFieldName) {
		if (sFieldName == null) {
			return null;
		}
		UID fieldUID;
		if (UID.isStringifiedUID(sFieldName)) {
			fieldUID = UID.parseUID(sFieldName);
			// check if field exists, otherwise warn (NUCLOS-2735)
			try {
				MetaProvider.getInstance().getEntityField(fieldUID);
			} catch (Exception ex) {
				LOG.warn("Layout contains unknown field UID: " + sFieldName, ex);
				fieldUID = null;
			}
		} else {
			if (entity == null) {
				LOG.warn(String.format("No entity found while parsing field uid for \"%s\"", sFieldName));
				return null;
			}
			// no UID? try to find field in entity 
			// backward compatibility for system layouts...
			FieldMeta<?> fMeta = null;
			for (FieldMeta<?> f : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity).values()) {
				if (LangUtils.equal(f.getFieldName(), sFieldName)) {
					fMeta = f;
					break;
				}
			}
			if (fMeta != null) {
				fieldUID = fMeta.getUID();
			} else {
				fieldUID = UID.parseUID(sFieldName);
				LOG.warn(String.format("Found unstringified fieldUID \"%s\" in layout for entity %s", sFieldName, entity));
			}
		}
		return fieldUID;
	}

	public UID getLayout() {
		return layoutUID;
	}

	public void setLayoutUID(final UID layoutUID) {
		this.layoutUID = layoutUID;
	}

	/**
	 * A wrapper class for a string value with an associated resource id (x is the string value,
	 * y is the resource id or null).
	 */
	public static class StringResourceIdPair extends Pair<String, String> implements Localizable, Serializable {

		public StringResourceIdPair(String value, String resourceId) {
			super(value, resourceId);
		}

		@Override
		public String getResourceId() {
			return this.y;
		}
		@Override
		public String getX() {
			return super.getX();
		}
	}
}