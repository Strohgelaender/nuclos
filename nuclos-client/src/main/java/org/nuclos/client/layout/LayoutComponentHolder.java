//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout;

import java.awt.*;

import javax.swing.*;

import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponent.LayoutComponentType;

public class LayoutComponentHolder<PK> extends JPanel {

	private final LayoutComponent<PK> layoutComponent;
	
	private final JComponent realComponent;
	
	private boolean transferName = true;

	public LayoutComponentHolder(LayoutComponent<PK> layoutComponent, boolean design) {
		super(new BorderLayout());
		
		this.layoutComponent = layoutComponent;
		this.realComponent = design 
				? layoutComponent.getComponent(LayoutComponentType.DESIGN) 
				: layoutComponent.getComponent(LayoutComponentType.DETAIL);
		
		super.add(this.realComponent, BorderLayout.CENTER);
	}
	
	public JComponent getHoldingComponent() {
		return realComponent;
	}
	
	public LayoutComponent<PK> getLayoutComponent() {
		return layoutComponent;
	}
	
	@Override
	public void setName(String name) {
		super.setName(name);
		if (transferName) {
			layoutComponent.setName(name);
		}
	}

}
