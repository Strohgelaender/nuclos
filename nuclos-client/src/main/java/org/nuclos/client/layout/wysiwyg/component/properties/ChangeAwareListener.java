//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.component.properties;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ChangeAwareListener implements ItemListener, FocusListener, KeyListener, ChangeListener {
	
	

	PropertiesPanel propertiesPanel = null;
	public ChangeAwareListener(PropertiesPanel propertiespanel) {
		this.propertiesPanel = propertiespanel;	
	}
	
	private void doPerformSaveAction() {
		propertiesPanel.performSaveAction(true);
	}
	
	@Override
	public void focusGained(FocusEvent e) {
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		doPerformSaveAction();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if(e.getSource() instanceof JCheckBox) {
			doPerformSaveAction();
		}
		else if(e.getSource() instanceof JComboBox) {
			if(e.getStateChange() == ItemEvent.SELECTED) {
				propertiesPanel.performSaveActionLiveMode(e.getItem().toString());
			}
		}
		
	}

	@Override
	public void keyTyped(final KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
				
	}

	@Override
	public void keyReleased(KeyEvent e) {
		JTextField field = (JTextField) e.getComponent();
		String value = field.getText();
		value = value.trim();		
		propertiesPanel.performSaveActionLiveMode(value);	
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		doPerformSaveAction();		
	}

}
