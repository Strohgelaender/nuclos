//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JSplitPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;
import org.nuclos.api.Property;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.GeneratorActions;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.JTABBEDPANE;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.client.layout.wysiwyg.component.AbstractWYSIWYGTableColumn;
import org.nuclos.client.layout.wysiwyg.component.TitledBorderWithTranslations;
import org.nuclos.client.layout.wysiwyg.component.TranslationMap;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGChart;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGChartColumn;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableCheckBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComboBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableDateChooser;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableEmail;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableHyperlink;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableImage;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableLabel;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableListOfValues;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableOptionGroup;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectablePasswordfield;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectablePhoneNumber;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableTextArea;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGCollectableTextfield;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGLayoutComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGMatrix;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGMatrixColumn;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGScrollPane;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSplitPane;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticButton;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticComboBox;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticImage;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticLabel;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticSeparator;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticTextarea;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticTextfield;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGStaticTitledSeparator;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubForm;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGSubFormColumn;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGTabbedPane;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGUniversalComponent;
import org.nuclos.client.layout.wysiwyg.component.WYSIWYGWebAddon;
import org.nuclos.client.layout.wysiwyg.component.properties.ComponentProperties;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValue;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueBoolean;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueBorder;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueColor;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueDimension;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueDouble;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueFont;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueInitialSortingOrder;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueInteger;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueScript;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueString;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueTranslations;
import org.nuclos.client.layout.wysiwyg.component.properties.PropertyValueValuelistProvider;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.WYSIWYGLayoutEditorPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.TableLayoutPanel;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGInitialFocusComponent;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGInitialSortingOrder;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGOption;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGOptions;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGParameter;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGProperty;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGPropertySet;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.WYSIWYGValuelistProvider;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRule;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleAction;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleActions;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleCondition;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRuleEventType;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.layoutmlrules.LayoutMLRules;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * This Class generates the LayoutML.
 *
 * Every {@link WYSIWYGLayoutEditorPanel} returns its {@link TableLayout} and
 * {@link WYSIWYGComponent} and the {@link WYSIWYGComponent} returns its
 * {@link ComponentProperties}.
 *
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class LayoutMLGenerator implements LayoutMLConstants {

	private static final Logger LOG = Logger.getLogger(LayoutMLGenerator.class);

	private LayoutMLRules layoutMLRules = new LayoutMLRules();

	/**
	 * Everything that is called here is in the sequence like it is in the Layoutml
	 * Header
	 * Components
	 * Rules
	 *
	 * @param editorPanel
	 * @return
	 * @throws CommonValidationException
	 */
	public synchronized String getLayoutML(WYSIWYGLayoutEditorPanel editorPanel) throws CommonValidationException {
		int blockDeep = 0;

		LayoutMLBlock block = new LayoutMLBlock(blockDeep, false);
		block.append("<?xml version=\"1.0\" encoding=\"ISO-8859-15\"?>");
		block.linebreak();
		block.append("<!DOCTYPE layoutml SYSTEM \"http://www.novabit.de/technologies/layoutml/layoutml.dtd\">");
		block.linebreak();
		block.append("<layoutml>");
		block.linebreak();
		block.append("<layout>");
		block.append(getLayoutMLForInitialFocusComponent(editorPanel, blockDeep + 1));
		// no constraint needed for the first panel
		layoutMLRules = new LayoutMLRules();
		/** at first the panel and its components */
		block.append(getLayoutMLForPanel(editorPanel, blockDeep + 1, new StringBuffer()));

		block.linebreak();
		block.append("</layout>");

		// if (editorPanel.getLayoutMLDependencies().getAllDependencies().size()
		// > 0) {
		// block.append(getLayoutMLLayoutMLDependencies(editorPanel.
		// getLayoutMLDependencies(), blockDeep + 1));
		// if (layoutMLRules.getRules().size() < 1)
		// block.linebreak();
		// }

		/** now the rules */
		if (layoutMLRules.getRules().size() > 0) {
			block.append(getLayoutMLLayoutMLRules(layoutMLRules, blockDeep + 1));
		}

		block.linebreak();

		block.append("</layoutml>");

		return block.toString();
	}

	/**
	 * Method returning the Layoutml for the Property {@link WYSIWYGInitialFocusComponent}
	 * @param c
	 * @param blockDeep
	 * @return
	 */
	private synchronized StringBuffer getLayoutMLForInitialFocusComponent(WYSIWYGLayoutEditorPanel c, int blockDeep) {
		if (c.getInitialFocusComponent() != null) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);
			block.append("<" + ELEMENT_INITIALFOCUSCOMPONENT);
			if (c.getInitialFocusComponent().getEntityUID() != null) {
				block.append(" " + ATTRIBUTE_ENTITY + "=\"" + c.getInitialFocusComponent().getEntityUID().getStringifiedDefinitionWithEntity(E.ENTITY) + "\"");
			}
			block.append(" " + ATTRIBUTE_NAME + "=\"" + c.getInitialFocusComponent().getEntityFieldUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\" />");
			return block.getStringBuffer();
		} else {
			return new StringBuffer();
		}
	}

	/**
	 * Method returning the Layoutml for {@link WYSIWYGComponent}
	 * Does decide what kind of processor is needed (chooses with "instanceof")
	 *
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return
	 */
	private synchronized StringBuffer getLayoutMLForComponent(WYSIWYGComponent c, TableLayout tableLayout, int blockDeep) throws CommonValidationException {
		StringBuffer sb = new StringBuffer();

		if (c instanceof WYSIWYGCollectableComponent) {
			sb.append(getLayoutMLForCollectableComponent((WYSIWYGCollectableComponent) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGLayoutEditorPanel) {
			sb.append(getLayoutMLForPanel((WYSIWYGLayoutEditorPanel) c, blockDeep, getLayoutMLTableLayoutConstraints((Component) c, tableLayout, blockDeep + 1)));
		} else if (c instanceof WYSIWYGTabbedPane) {
			sb.append(getLayoutMLForTabbedPane((WYSIWYGTabbedPane) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGSubForm) {
			sb.append(getLayoutMLForSubForm((WYSIWYGSubForm) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGChart) {
			sb.append(getLayoutMLForChart((WYSIWYGChart) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGMatrix) {
			sb.append(getLayoutMLForMatrix((WYSIWYGMatrix) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGScrollPane) {
			sb.append(getLayoutMLForScrollPane((WYSIWYGScrollPane) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGSplitPane) {
			sb.append(getLayoutMLForSplitPane((WYSIWYGSplitPane) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGStaticComboBox) {
			sb.append(getLayoutMLForStaticComboBox((WYSIWYGStaticComboBox) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGStaticLabel) {
			sb.append(getLayoutMLForStaticLabel((WYSIWYGStaticLabel) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGStaticSeparator) {
			sb.append(getLayoutMLForStaticSeparator((WYSIWYGStaticSeparator) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGStaticTextarea) {
			sb.append(getLayoutMLForStaticTextarea((WYSIWYGStaticTextarea) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGStaticTextfield) {
			sb.append(getLayoutMLForStaticTextfield((WYSIWYGStaticTextfield) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGStaticTitledSeparator) {
			sb.append(getLayoutMLForStaticTitledSeparator((WYSIWYGStaticTitledSeparator) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGStaticButton) {
			sb.append(getLayoutMLForStaticButton((WYSIWYGStaticButton) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGStaticImage) {
			sb.append(getLayoutMLForStaticImage((WYSIWYGStaticImage) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGLayoutComponent) {
			sb.append(getLayoutMLForLayoutComponent((WYSIWYGLayoutComponent) c, tableLayout, blockDeep));
		} else if (c instanceof WYSIWYGWebAddon) {
			sb.append(getLayoutMLForWebAddon((WYSIWYGWebAddon) c, tableLayout, blockDeep));
		}

		return sb;
	}
	
	/**
	 * Method converting the {@link WYSIWYGSubForm} to LayoutML XML.
	 *
	 * @see LayoutMLBlock
	 * @param matrix
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForMatrix(WYSIWYGMatrix matrix, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_MATRIX);
		block.append(getLayoutMLAttributesFromProperties(WYSIWYGMatrix.PROPERTIES_TO_LAYOUTML_ATTRIBUTES, matrix.getProperties()));
		block.append(" >");
		block.append(getLayoutMLTableLayoutConstraints(matrix, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(matrix.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(matrix, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(matrix.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(matrix.getProperties(), blockDeep + 1));
		
		for (Iterator<WYSIWYGMatrixColumn> it = matrix.getColumnsInOrder().iterator(); it.hasNext();) {
			WYSIWYGMatrixColumn column = it.next();
//			if (!(Boolean) column.getProperties().getProperty(WYSIWYGMatrixColumn.PROPERTY_DEFAULTVALUES).getValue()) {
				block.append(getLayoutMLForMatrixColumn(column, blockDeep + 1));
//			}
		}
		
		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_NEW_ENABLED, ELEMENT_NEW_ENABLED, matrix.getProperties(), blockDeep + 1));
		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_EDIT_ENABLED, ELEMENT_EDIT_ENABLED, matrix.getProperties(), blockDeep + 1));
		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_DELETE_ENABLED, ELEMENT_DELETE_ENABLED, matrix.getProperties(), blockDeep + 1));
		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_CLONE_ENABLED, ELEMENT_CLONE_ENABLED, matrix.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_MATRIX + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGSubForm} to LayoutML XML.
	 *
	 * @see LayoutMLBlock
	 * @param subform
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForSubForm(WYSIWYGSubForm subform, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_SUBFORM);
		block.append(getLayoutMLAttributesFromProperties(WYSIWYGSubForm.PROPERTIES_TO_LAYOUTML_ATTRIBUTES, subform.getProperties()));
		block.append(" >");
		block.append(getLayoutMLTableLayoutConstraints(subform, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(subform.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(subform, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(subform.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(subform.getProperties(), blockDeep + 1));
		//NUCLEUSINT-563
		List<FieldMeta<?>> subformColumns = subform.getParentEditor().getMetaInformation().getSubFormColumns(subform.getEntityUID());
		if (subform.getProperties().getProperty(WYSIWYGSubForm.PROPERTY_INITIALSORTINGORDER).getValue() != null)
			if (subformColumns.contains(MetaProvider.getInstance().getEntityField(
					((PropertyValueInitialSortingOrder) subform.getProperties().getProperty(
							WYSIWYGSubForm.PROPERTY_INITIALSORTINGORDER)).getValue().getName())))
		block.append(getLayoutMLInitialSortingOrder(((PropertyValueInitialSortingOrder) subform.getProperties().getProperty(WYSIWYGSubForm.PROPERTY_INITIALSORTINGORDER)).getValue(), blockDeep + 1));
		block.append(getLayoutMLBackgroundColorFromProperty(subform.getProperties(), blockDeep + 1));

		for (Iterator<WYSIWYGSubFormColumn> it = subform.getColumnsInOrder().iterator(); it.hasNext();) {
			WYSIWYGSubFormColumn column = it.next();
			if (!(Boolean) column.getProperties().getProperty(WYSIWYGSubFormColumn.PROPERTY_DEFAULTVALUES).getValue()) {
				block.append(getLayoutMLForSubFormColumn(column, blockDeep + 1));
			}
		}

		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_NEW_ENABLED, ELEMENT_NEW_ENABLED, subform.getProperties(), blockDeep + 1));
		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_EDIT_ENABLED, ELEMENT_EDIT_ENABLED, subform.getProperties(), blockDeep + 1));
		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_DELETE_ENABLED, ELEMENT_DELETE_ENABLED, subform.getProperties(), blockDeep + 1));
		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_CLONE_ENABLED, ELEMENT_CLONE_ENABLED, subform.getProperties(), blockDeep + 1));
		block.append(getLayoutMLScriptFromProperty(WYSIWYGSubForm.PROPERTY_DYNAMIC_ROW_COLOR, ELEMENT_DYNAMIC_ROW_COLOR, subform.getProperties(), blockDeep + 1));
		
		if (subform.getProperties().getProperty(WYSIWYGSubForm.PROPERTY_PROPERTIES) != null) {
			WYSIWYGProperty collectableComponentProperties = (WYSIWYGProperty) subform.getProperties().getProperty(WYSIWYGSubForm.PROPERTY_PROPERTIES).getValue();
			if (collectableComponentProperties != null) {
				if (collectableComponentProperties.getAllPropertyEntries().size() > 0) {
					block.append(getLayoutMLCollectableComponentProperty(collectableComponentProperties, blockDeep + 1));
				}
			}
		}
		
		block.linebreak();
		block.append("</" + ELEMENT_SUBFORM + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGSubFormColumn} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param column
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForSubFormColumn(WYSIWYGSubFormColumn column, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_SUBFORMCOLUMN);
		block.append(getLayoutMLAttributesFromProperties(WYSIWYGSubFormColumn.PROPERTIES_TO_LAYOUTML_ATTRIBUTES, column.getProperties()));
		block.append(">");

		block.append(getLayoutMLTranslationsFromProperty(column.getProperties(), blockDeep + 1));

		boolean lb = false;
		WYSIWYGValuelistProvider wysiwygStaticValuelistProvider = (WYSIWYGValuelistProvider) column.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_VALUELISTPROVIDER).getValue();
		if (wysiwygStaticValuelistProvider != null && wysiwygStaticValuelistProvider.getType() != null) {
			block.append(getLayoutMLValueListProvider(wysiwygStaticValuelistProvider, blockDeep + 1));
			lb = true;
		}
		WYSIWYGProperty collectableComponentProperties = (WYSIWYGProperty) column.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_COLLECTABLECOMPONENTPROPERTY).getValue();
		if (collectableComponentProperties != null) {
			if (collectableComponentProperties.getAllPropertyEntries().size() > 0) {
				block.append(getLayoutMLCollectableComponentProperty(collectableComponentProperties, blockDeep + 1));
				lb = true;
			}
		}

		// text module
		
		if (column.getProperties().getProperty(WYSIWYGSubFormColumn.PROPERTY_TEXTMODULE_ENTITY) != null) {
			block.append(getLayoutMLForTextModule(column.getProperties(),blockDeep + 1));
		}
		

		if (lb) block.linebreak();
		block.append("</" + ELEMENT_SUBFORMCOLUMN + ">");
		return block.getStringBuffer();
	}
	
	/**
	 * Method converting the {@link WYSIWYGMatrixColumn} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param column
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForMatrixColumn(WYSIWYGMatrixColumn column, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_MATRIXCOLUMN);
		block.append(getLayoutMLAttributesFromProperties(WYSIWYGMatrixColumn.PROPERTIES_TO_LAYOUTML_ATTRIBUTES, column.getProperties()));
		block.append(">");

		block.append(getLayoutMLTranslationsFromProperty(column.getProperties(), blockDeep + 1));

		boolean lb = false;
		WYSIWYGValuelistProvider wysiwygStaticValuelistProvider = (WYSIWYGValuelistProvider) column.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_VALUELISTPROVIDER).getValue();
		if (wysiwygStaticValuelistProvider != null && wysiwygStaticValuelistProvider.getType() != null) {
			block.append(getLayoutMLValueListProvider(wysiwygStaticValuelistProvider, blockDeep + 1));
			lb = true;
		}
		WYSIWYGProperty collectableComponentProperties = (WYSIWYGProperty) column.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_COLLECTABLECOMPONENTPROPERTY).getValue();
		if (collectableComponentProperties != null) {
			if (collectableComponentProperties.getAllPropertyEntries().size() > 0) {
				block.append(getLayoutMLCollectableComponentProperty(collectableComponentProperties, blockDeep + 1));
				lb = true;
			}
		}

		// text module
		
		if (column.getProperties().getProperty(WYSIWYGMatrixColumn.PROPERTY_TEXTMODULE_ENTITY) != null) {
			block.append(getLayoutMLForTextModule(column.getProperties(),blockDeep + 1));
		}
		

		if (lb) block.linebreak();
		block.append("</" + ELEMENT_MATRIXCOLUMN + ">");
		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGChart} to LayoutML XML.
	 * 
	 * @see LayoutMLBlock
	 * @param chart
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForChart(WYSIWYGChart chart, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_CHART);
		block.append(getLayoutMLAttributesFromProperties(WYSIWYGChart.PROPERTIES_TO_LAYOUTML_ATTRIBUTES, chart.getProperties()));
		block.append(" >");
		block.append(getLayoutMLTableLayoutConstraints(chart, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(chart.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(chart, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(chart.getProperties(), blockDeep + 1));
		if (chart.getProperties().getProperty(WYSIWYGChart.PROPERTY_PROPERTIES) != null) {
			WYSIWYGProperty collectableComponentProperties = (WYSIWYGProperty) chart.getProperties().getProperty(WYSIWYGChart.PROPERTY_PROPERTIES).getValue();
			if (collectableComponentProperties != null) {
				if (collectableComponentProperties.getAllPropertyEntries().size() > 0) {
					block.append(getLayoutMLCollectableComponentProperty(collectableComponentProperties, blockDeep + 1));
				}
			}
		}
		block.linebreak();
		block.append("</" + ELEMENT_CHART + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGSplitPane} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param splitPane
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForSplitPane(WYSIWYGSplitPane splitPane, TableLayout tableLayout, int blockDeep) throws CommonValidationException {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_SPLITPANE);
		block.append(getLayoutMLAttributesFromProperties(WYSIWYGSplitPane.PROPERTIES_TO_LAYOUTML_ATTRIBUTES, splitPane.getProperties()));
		block.append(" >");
		block.append(getLayoutMLTableLayoutConstraints(splitPane, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(splitPane.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(splitPane, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(splitPane.getProperties(), blockDeep + 1));

		Component[] allComponents = splitPane.getComponents();
		for (int i = 0; i < allComponents.length; i++) {
			Component c = allComponents[i];
			if (c instanceof JSplitPane) {
				JSplitPane jpnSplit = ((JSplitPane) c);

				Object propOrientation = splitPane.getProperties().getProperty(WYSIWYGSplitPane.PROPERTY_ORIENTATION).getValue();

				if (propOrientation != null) {
					if (ATTRIBUTEVALUE_HORIZONTAL.equals(propOrientation)) {
						block.append(getLayoutMLForPanel((WYSIWYGLayoutEditorPanel) jpnSplit.getLeftComponent(), blockDeep + 1, getLayoutMLSplitPaneConstraints(ATTRIBUTEVALUE_LEFT, blockDeep + 2)));
						block.append(getLayoutMLForPanel((WYSIWYGLayoutEditorPanel) jpnSplit.getRightComponent(), blockDeep + 1, getLayoutMLSplitPaneConstraints(ATTRIBUTEVALUE_RIGHT, blockDeep + 2)));
					} else {
						block.append(getLayoutMLForPanel((WYSIWYGLayoutEditorPanel) jpnSplit.getTopComponent(), blockDeep + 1, getLayoutMLSplitPaneConstraints(ATTRIBUTEVALUE_TOP, blockDeep + 2)));
						block.append(getLayoutMLForPanel((WYSIWYGLayoutEditorPanel) jpnSplit.getBottomComponent(), blockDeep + 1, getLayoutMLSplitPaneConstraints(ATTRIBUTEVALUE_BOTTOM, blockDeep + 2)));
					}
				}
			}
		}

		block.linebreak();
		block.append("</" + ELEMENT_SPLITPANE + ">");
		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGSplitPane} Constraints to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param position
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLSplitPaneConstraints(String position, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + ELEMENT_SPLITPANECONSTRAINTS + " ");
		block.append(ATTRIBUTE_POSITION + "=\"");
		block.append(position);
		block.append("\" />");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGScrollPane} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param scrollPane
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForScrollPane(WYSIWYGScrollPane scrollPane, TableLayout tableLayout, int blockDeep) throws CommonValidationException {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_SCROLLPANE);
		block.append(getLayoutMLAttributesFromProperties(scrollPane.getPropertyAttributeLink(), scrollPane.getProperties()));
		block.append(" >");
		block.append(getLayoutMLTableLayoutConstraints(scrollPane, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(scrollPane.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(scrollPane, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(scrollPane.getProperties(), blockDeep + 1));

		Component[] allComponents = scrollPane.getViewport().getComponents();
		for (int i = 0; i < allComponents.length; i++) {
			Component c = allComponents[i];
			if (c instanceof WYSIWYGLayoutEditorPanel) {
				block.append(getLayoutMLForPanel((WYSIWYGLayoutEditorPanel) c, blockDeep + 1, new StringBuffer()));
			}
		}

		block.linebreak();
		block.append("</" + ELEMENT_SCROLLPANE + ">");
		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGTabbedPane} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param tabPane
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForTabbedPane(WYSIWYGTabbedPane tabPane, TableLayout tableLayout, int blockDeep) throws CommonValidationException {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_TABBEDPANE + " ");
		block.append(getLayoutMLAttributesFromProperties(tabPane.getPropertyAttributeLink(), tabPane.getProperties()));
		block.append(">");
		block.append(getLayoutMLTableLayoutConstraints(tabPane, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(tabPane.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(tabPane, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(tabPane.getProperties(), blockDeep + 1));

		for (int i = 0; i < tabPane.getTabCount(); i++) {
			Component c = tabPane.getComponentAt(i);
			if (c instanceof WYSIWYGLayoutEditorPanel) {
				WYSIWYGLayoutEditorPanel panel = (WYSIWYGLayoutEditorPanel)c;
				String sTitle = null;
				if(panel.getName() != null) {
					sTitle = c.getName();
				}
				else {
					sTitle = tabPane.getTitleAt(i);
				}
				block.append(getLayoutMLForPanel((WYSIWYGLayoutEditorPanel) c, blockDeep + 1, getLayoutMLTabbedPaneConstraints(tabPane.getTitleAt(i), tabPane.isEnabledAt(i), blockDeep + 1, tabPane.getTabTranslations().get(i), sTitle, tabPane.getMnemonicAt(i))));
			}
		}

		block.linebreak();
		block.append("</" + ELEMENT_TABBEDPANE + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGLayoutEditorPanel} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param panel
	 * @param blockDeep
	 * @param sbLayoutConstraints
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForPanel(WYSIWYGLayoutEditorPanel panel, int blockDeep, StringBuffer sbLayoutConstraints) throws CommonValidationException {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_PANEL);
		block.append(getLayoutMLAttributesFromProperties(WYSIWYGLayoutEditorPanel.PROPERTIES_TO_LAYOUTML_ATTRIBUTES, panel.getProperties()));
		block.append(">");
		block.append(sbLayoutConstraints);

		LayoutManager layoutManager = panel.getLayout();
		if (!(layoutManager instanceof TableLayout)) {
			throw new CommonValidationException("Only LayoutManagers of type TableLayout are allowed for a WYSIWYGLayout!");
		}
		TableLayout tableLayout = (TableLayout) layoutManager;
		block.append(getLayoutMLForTableLayoutColumnsAndRows(tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(panel.getProperties(), blockDeep + 1));
		// block.append(getLayoutMLMinimumSizeFromComponent(panel, blockDeep +
		// 1));
		//block.append(getLayoutMLPreferredSizeFromProperty(panel.getProperties(
		// ), blockDeep + 1));
		block.append(getLayoutMLBackgroundColorFromProperty(panel.getProperties(), blockDeep + 1));

		block.append(getLayoutMLForTableLayoutPanel(panel.getTableLayoutPanel(), blockDeep + 1, getLayoutMLTableLayoutConstraints(panel.getTableLayoutPanel(), tableLayout, blockDeep + 2)));

		block.linebreak();
		block.append("</" + ELEMENT_PANEL + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link TableLayoutPanel} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param tableLayoutPanel
	 * @param blockDeep
	 * @param sbLayoutConstraints
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForTableLayoutPanel(TableLayoutPanel tableLayoutPanel, int blockDeep, StringBuffer sbLayoutConstraints) throws CommonValidationException {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_PANEL + ">");
		block.append(sbLayoutConstraints);

		TableLayout tableLayout = (TableLayout) tableLayoutPanel.getLayout();
		block.append(getLayoutMLForTableLayoutColumnsAndRows(tableLayout, blockDeep + 1));

		Component[] allComponents = tableLayoutPanel.getComponents();
		for (int i = 0; i < allComponents.length; i++) {
			Component c = allComponents[i];
			if (c instanceof WYSIWYGComponent) {
				block.append(getLayoutMLForComponent((WYSIWYGComponent) c, tableLayout, blockDeep + 1));

				LayoutMLRules tempRules = null;
				if (c instanceof WYSIWYGSubForm) {
					Collection<WYSIWYGSubFormColumn> subformColumns = ((WYSIWYGSubForm) c).getColumnsInOrder();
					for (WYSIWYGSubFormColumn subformColumn : subformColumns) {
						tempRules = subformColumn.getLayoutMLRulesIfCapable();
						if (tempRules != null) {
							//NUCLEUSINT-435
							addRules(tempRules);
						}
					}
				} else {
					tempRules = ((WYSIWYGComponent) c).getLayoutMLRulesIfCapable();
					if (tempRules != null) {
						//NUCLEUSINT-435
						addRules(tempRules);
					}
				}
			}
		}

		block.linebreak();
		block.append("</" + ELEMENT_PANEL + ">");
		return block.getStringBuffer();
	}

	/**
	 * NUCLEUSINT-435
	 * Avoid duplicate Rules
	 * @param rulesToAdd
	 */
	private synchronized void addRules(LayoutMLRules rulesToAdd){
		boolean contains = false;
		for (LayoutMLRule singleRule : rulesToAdd.getRules()) {
			contains = false;
			if (this.layoutMLRules.getRules().size() == 0){
				this.layoutMLRules.addRule(singleRule);
				continue;
			}

			for (LayoutMLRule otherRule : this.layoutMLRules.getRules()){
				if (flatCompareRules(singleRule, otherRule)){
					contains = true;
					break;
				}
			}

			if (!contains)
				this.layoutMLRules.addRule(singleRule);

		}
	}

	/**
	 * NUCLEUSINT-435
	 * A Rule is the same if the Eventtype (and its fields), the Actions and the Rulename are equal
	 * @param oneRule
	 * @param otherRule
	 * @return true if equal, otherwise false
	 */
	private boolean flatCompareRules(LayoutMLRule oneRule, LayoutMLRule otherRule) {
		boolean areEqual = true;

		if (!oneRule.getRuleName().equals(otherRule.getRuleName()))
			areEqual = false;

		if (!oneRule.getLayoutMLRuleEventType().equals(otherRule.getLayoutMLRuleEventType()))
			areEqual = false;

		boolean actionsEqual = false;

		if (oneRule.getLayoutMLRuleActions().getSingleActions().size() != otherRule.getLayoutMLRuleActions().getSingleActions().size()) {
			areEqual = false;
		} else {
			for (LayoutMLRuleAction action : oneRule.getLayoutMLRuleActions().getSingleActions()) {
				for (LayoutMLRuleAction otherAction : otherRule.getLayoutMLRuleActions().getSingleActions()) {
					if (action.equals(otherAction))
						actionsEqual = true;
				}
				if (!actionsEqual)
					areEqual = false;
			}
		}

		return areEqual;
	}

	/**
	 * Method converting the {@link TableLayout} columns and rows to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForTableLayoutColumnsAndRows(TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		double[] columns = tableLayout.getColumn();
		double[] rows = tableLayout.getRow();

		block.append("<" + ELEMENT_TABLELAYOUT + " " + ATTRIBUTE_COLUMNS + "=\"");
		for (int i = 0; i < columns.length; i++) {
			block.append(columns[i]);
			if (i < columns.length - 1) {
				block.append("|");
			}
		}
		block.append("\" " + ATTRIBUTE_ROWS + "=\"");
		for (int i = 0; i < rows.length; i++) {
			block.append(rows[i]);
			if (i < rows.length - 1) {
				block.append("|");
			}
		}
		block.append("\" />");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGUniversalComponent} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForCollectableComponent(WYSIWYGCollectableComponent c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		
		// check existens of an uid here. otherwise skip this.
		final PropertyValue propertyUid = c.getProperties().getProperty(WYSIWYGComponent.PROPERTY_UID);
		if (propertyUid != null && StringUtils.isNullOrEmpty((String)propertyUid.getValue())) {
			LOG.error("can set no name attribute set. - skip this. !!!");
			return block.getStringBuffer();
		}
	
		block.append("<" + ELEMENT_COLLECTABLECOMPONENT);
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		//NUCLEUSINT-385
		/**
		 * The type WYSIWYGCollectableOptionGroup is a WYSIWYGUniversalComponent.
		 * There is no need to generate the
		 */
		if (c instanceof WYSIWYGCollectableTextfield) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_TEXTFIELD + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectableLabel) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_TEXTFIELD + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_LABEL + "\"");
			/** use Textfield because DTD dont know a CollectableLabel */
		} else if (c instanceof WYSIWYGCollectableListOfValues) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_LISTOFVALUES + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectableCheckBox) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_CHECKBOX + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectableDateChooser) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_DATECHOOSER + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectableComboBox) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_COMBOBOX + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectableTextArea) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_TEXTAREA + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectableImage) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_IMAGE + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectablePasswordfield) {
			//NUCLEUSINT-1142
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_PASSWORDFIELD + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectableHyperlink) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_HYPERLINK + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectableEmail) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_EMAIL + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		} else if (c instanceof WYSIWYGCollectablePhoneNumber) {
			block.append(" " + ATTRIBUTE_CONTROLTYPE + "=\"" + CONTROLTYPE_PHONENUMBER + "\" " + ATTRIBUTE_SHOWONLY + "=\"" + ATTRIBUTEVALUE_CONTROL + "\"");
		}

		block.append(">");


		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		WYSIWYGProperty collectableComponentProperties = (WYSIWYGProperty) c.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_COLLECTABLECOMPONENTPROPERTY).getValue();
		if (collectableComponentProperties != null)
			if (collectableComponentProperties.getAllPropertyEntries().size() > 0)
				block.append(getLayoutMLCollectableComponentProperty(collectableComponentProperties, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		if (c.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_STRICTSIZE) != null) {
			block.append(getLayoutMLStrictSizeFromProperty(c.getProperties(), blockDeep + 1));
		} else {
			block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
			block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		}
		block.append(getLayoutMLBackgroundColorFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLDescription(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLTextFormatFromProperty(c.getProperties(), blockDeep + 1));
		if (c.getProperties().getProperty(WYSIWYGCollectableOptionGroup.PROPERTY_OPTIONS) != null) {
			WYSIWYGOptions options = (WYSIWYGOptions) c.getProperties().getProperty(WYSIWYGCollectableOptionGroup.PROPERTY_OPTIONS).getValue();
			if (options != null) {
				block.append(getLayoutMLOptions(options, blockDeep + 1));
			}
		}
		block.append(getLayoutMLTranslationsFromProperty(c.getProperties(), blockDeep + 1));
		if (c.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_VALUELISTPROVIDER) != null) {
			WYSIWYGValuelistProvider wysiwygStaticValuelistProvider = (WYSIWYGValuelistProvider) c.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_VALUELISTPROVIDER).getValue();
			if (wysiwygStaticValuelistProvider != null && wysiwygStaticValuelistProvider.getType() != null) {
				block.append(getLayoutMLValueListProvider(wysiwygStaticValuelistProvider, blockDeep + 1));
			}
		}
		block.append(getLayoutMLScriptFromProperty(WYSIWYGCollectableComponent.PROPERTY_ENABLED_DYNAMIC, ELEMENT_ENABLED, c.getProperties(), blockDeep + 1));
		if (isTextModuleSupportingComponent(c)) {
			// text module
			if (c.getProperties().getProperty(WYSIWYGCollectableTextArea.PROPERTY_TEXTMODULE_ENTITY) != null) {
				block.append(getLayoutMLForTextModule(c.getProperties(),blockDeep + 1));
			}
		}
		block.linebreak();
		block.append("</" + ELEMENT_COLLECTABLECOMPONENT + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGStaticTitledSeparator} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForStaticTitledSeparator(WYSIWYGStaticTitledSeparator c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + ELEMENT_TITLEDSEPARATOR);
		block.append(" " + ATTRIBUTE_TITLE + "=\"" +
			StringUtils.xmlEncode((String) c.getProperties().getProperty(
				WYSIWYGStaticTitledSeparator.PROPERTY_TITLE).getValue()) + "\"");
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLTranslationsFromProperty(c.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_TITLEDSEPARATOR + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGStaticSeparator} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForStaticSeparator(WYSIWYGStaticSeparator c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_SEPARATOR);
		block.append(" " + ATTRIBUTE_ORIENTATION + "=\"");

		switch (c.getOrientation()) {
			case WYSIWYGStaticSeparator.HORIZONTAL :
				block.append(ATTRIBUTEVALUE_HORIZONTAL + "\"");
				break;
			case WYSIWYGStaticSeparator.VERTICAL :
				block.append(ATTRIBUTEVALUE_VERTICAL + "\"");
				break;
		}
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_SEPARATOR + ">");
		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGStaticButton} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForStaticButton(WYSIWYGStaticButton c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_BUTTON);
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLDescription(c.getProperties(), blockDeep + 1));
		
		//NUCLOSINT-743 if a rule is set generate a property for it
		String actionCommand = (String)c.getProperties().getProperty(WYSIWYGStaticButton.PROPERTY_ACTIONCOMMAND).getValue();
		final String sArgument = (String) c.getProperties().getProperty(WYSIWYGStaticButton.PROPERTY_ACTIONCOMMAND_PROPERTIES).getValue();
		if (STATIC_BUTTON.EXECUTE_RULE_ACTION_LABEL.equals(actionCommand)) {
			// is a execute rule action command
			try {
				String sRuleClass = null;
				if (sArgument != null && sArgument.startsWith("org.nuclos.businessentity.rule")) {
					// allow Nuclos internal rules (system entity)
					sRuleClass = sArgument;
				} else {
					final EventSupportSourceVO eventVO = EventSupportRepository.getInstance().getEventSupportByClassname(sArgument);
					if (eventVO != null) {
						sRuleClass = eventVO.getClassname();
					}
				}
				if (sRuleClass != null) {
					WYSIWYGProperty temp = new WYSIWYGProperty();
					temp.addWYSIYWYGPropertySet(new WYSIWYGPropertySet(STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT, sRuleClass));
					block.append(getLayoutMLCollectableComponentProperty(temp, blockDeep + 1));
				}
			} catch (Exception e) {
				LOG.warn("getLayoutMLForStaticButton failed: " + e, e);
			}
		}
		if (STATIC_BUTTON.GENERATOR_ACTION_LABEL.equals(actionCommand)) {
			// is a execute generator action command
			try {
				final UID entityUid = c.getParentEditor().getMetaInformation().getCollectableEntity().getUID();

				UID generator = UID.parseUID(sArgument);
				Collection<GeneratorActionVO> collGenerators = GeneratorActions.getGeneratorActions(entityUid);

				GeneratorActionVO generatorActionVO = null;
				for (GeneratorActionVO gen: collGenerators) {
					if (LangUtils.equal(gen.getId(), generator)) {
						generatorActionVO = gen;
						break;
					}
				}
				if (generatorActionVO != null) {
					WYSIWYGProperty temp = new WYSIWYGProperty();
					temp.addWYSIYWYGPropertySet(new WYSIWYGPropertySet(STATIC_BUTTON.GENERATOR_ACTION_ARGUMENT, generatorActionVO.getId().getStringifiedDefinitionWithEntity(E.GENERATION)));
					block.append(getLayoutMLCollectableComponentProperty(temp, blockDeep + 1));
				}
			} catch (Exception e) {
				LOG.warn("getLayoutMLForStaticButton failed: " + e, e);
			}
		}
		if (STATIC_BUTTON.STATE_CHANGE_ACTION_LABEL.equals(actionCommand)) {
			// is a execute generator action command
			try {
				final Collection<StateVO> collStates;
				final UID entityUid = c.getParentEditor().getMetaInformation().getCollectableEntity().getUID();
				if (Modules.getInstance().isModule(entityUid)) {
					collStates = StateDelegate.getInstance().getStatesByModule(entityUid);
				} else {
					collStates = Collections.emptyList();
				}

				UID targetState = UID.parseUID(sArgument);
				StateVO stateVO = null;
				for (StateVO state: collStates) {
					if (LangUtils.equal(state.getId(), targetState)) {
						stateVO = state;
						break;
					}
				}

				if (stateVO != null) {
					WYSIWYGProperty temp = new WYSIWYGProperty();
					temp.addWYSIYWYGPropertySet(new WYSIWYGPropertySet(STATIC_BUTTON.STATE_CHANGE_ACTION_ARGUMENT, stateVO.getId().getStringifiedDefinitionWithEntity(E.STATE)));
					block.append(getLayoutMLCollectableComponentProperty(temp, blockDeep + 1));
				}
			} catch (Exception e) {
				LOG.warn("getLayoutMLForStaticButton failed: " + e, e);
			}
		}

		if (STATIC_BUTTON.HYPERLINK_ACTION_LABEL.equals(actionCommand)) {
			// is a execute hyperlink action command
			try {
				final List<FieldMeta<?>> collFields = c.getParentEditor().getMetaInformation().getFieldsByControlType(ATTRIBUTEVALUE_HYPERLINK);

				UID hyperlinkField = UID.parseUID(sArgument);

				FieldMeta<?> linkField = null;
				for (FieldMeta<?> link : collFields) {
					if (LangUtils.equal(link.getUID(), hyperlinkField)) {
						linkField = link;
						break;
					}
				}

				if (linkField != null) {
					WYSIWYGProperty temp = new WYSIWYGProperty();
					temp.addWYSIYWYGPropertySet(new WYSIWYGPropertySet(STATIC_BUTTON.HYPERLINK_ACTION_ARGUMENT, linkField.getUID().getStringifiedDefinitionWithEntity(E.ENTITYFIELD)));
					block.append(getLayoutMLCollectableComponentProperty(temp, blockDeep + 1));
				}
			} catch (Exception e) {
				LOG.warn("getLayoutMLForStaticButton failed: " + e, e);
			}
		}
		
		//NUCLEUSINT-1159
		WYSIWYGProperty collectableComponentProperties = (WYSIWYGProperty) c.getProperties().getProperty(WYSIWYGCollectableComponent.PROPERTY_COLLECTABLECOMPONENTPROPERTY).getValue();
		if (collectableComponentProperties != null) {
			if (collectableComponentProperties.getAllPropertyEntries().size() > 0) {
				block.append(getLayoutMLCollectableComponentProperty(collectableComponentProperties, blockDeep + 1));
			}
		}
		
		block.append(getLayoutMLTranslationsFromProperty(c.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_BUTTON + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGStaticTextfield} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForStaticImage(WYSIWYGStaticImage c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_IMAGE);
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLDescription(c.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_IMAGE + ">");

		return block.getStringBuffer();
	}


	/**
	 * Method converting the {@link WYSIWYGStaticTextfield} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForStaticTextfield(WYSIWYGStaticTextfield c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_TEXTFIELD);
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLDescription(c.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_TEXTFIELD + ">");

		return block.getStringBuffer();
	}

	private synchronized StringBuffer getLayoutMLForLayoutComponent(WYSIWYGLayoutComponent c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_LAYOUTCOMPONENT);
		block.append(" ");
		block.append(ATTRIBUTE_CLASS);
		block.append("=\"");
		block.append(c.getLayoutComponentFactoryClass());
		block.append("\"");
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		appendAdditionalProperties(c, block, blockDeep);
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));

		block.linebreak();
		block.append("</" + ELEMENT_LAYOUTCOMPONENT + ">");

		return block.getStringBuffer();
	}

	private synchronized StringBuffer getLayoutMLForWebAddon(WYSIWYGWebAddon c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_WEBADDON);
		block.append(" ");
		block.append(ATTRIBUTE_ID);
		block.append("=\"");
		block.append(c.getId());
		block.append("\"");
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		appendAdditionalProperties(c, block, blockDeep);
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));

		block.linebreak();
		block.append("</" + ELEMENT_WEBADDON + ">");

		return block.getStringBuffer();
	}

	private synchronized void appendAdditionalProperties(WYSIWYGComponent c, LayoutMLBlock block, int blockDeep) {
		Property[] properties = null;
		if (c instanceof WYSIWYGLayoutComponent) {
			if (((WYSIWYGLayoutComponent) c).getAdditionalProperties() != null && ((WYSIWYGLayoutComponent) c).getAdditionalProperties().length > 0) {
				properties = ((WYSIWYGLayoutComponent) c).getAdditionalProperties();
			}
		} else if (c instanceof WYSIWYGWebAddon) {
			if (((WYSIWYGWebAddon) c).getAdditionalProperties() != null && ((WYSIWYGWebAddon) c).getAdditionalProperties().length > 0) {
				properties = ((WYSIWYGWebAddon) c).getAdditionalProperties();
			}
		}
		if (properties != null) {
			Collections.sort(Arrays.asList(properties), Comparator.comparing(Property::getName));
			for (Property pt : properties) {
				PropertyValue<?> property = c.getProperties().getProperty(pt.name);
				if (property != null && property.getValue() != null) {
					block.append(getLayoutMLForProperty(pt.name, property, blockDeep + 1));
				}
			}
		}
	}

	/**
	 * Method converting the {@link WYSIWYGStaticTextarea} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForStaticTextarea(WYSIWYGStaticTextarea c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_TEXTAREA);
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLDescription(c.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_TEXTAREA + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGStaticLabel} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForStaticLabel(WYSIWYGStaticLabel c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_LABEL);
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLTextFormatFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLDescription(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLTranslationsFromProperty(c.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_LABEL + ">");
		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link WYSIWYGStaticComboBox} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForStaticComboBox(WYSIWYGStaticComboBox c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_COMBOBOX);
		block.append(getLayoutMLAttributesFromProperties(c.getPropertyAttributeLink(), c.getProperties()));
		block.append(">");

		block.append(getLayoutMLTableLayoutConstraints(c, tableLayout, blockDeep + 1));
		block.append(getLayoutMLBordersFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLMinimumSizeFromComponent(c, blockDeep + 1));
		block.append(getLayoutMLPreferredSizeFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLFontFromProperty(c.getProperties(), blockDeep + 1));
		block.append(getLayoutMLDescription(c.getProperties(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_COMBOBOX + ">");
		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link ComponentProperties} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param propertyAttributeLink
	 * @param properties
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLAttributesFromProperties(String[][] propertyAttributeLink, ComponentProperties properties) {
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < propertyAttributeLink.length; i++) {
			final String attributeName = propertyAttributeLink[i][1];
			PropertyValue<?> propertyValue = properties.getProperty(propertyAttributeLink[i][0]);
			if (ATTRIBUTE_NAME.equals(attributeName)
					&& (properties.getComponent() instanceof WYSIWYGCollectableComponent
							|| properties.getComponent() instanceof WYSIWYGChartColumn
							|| properties.getComponent() instanceof AbstractWYSIWYGTableColumn)) {
				propertyValue = properties.getProperty(WYSIWYGComponent.PROPERTY_UID);
			}
			sb.append(getLayoutMLAttributeFromProperty(properties.getMetaInformation(), propertyValue, attributeName));
		}

		return sb;
	}

	/**
	 * Method converting the {@link PropertyValue} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param property
	 * @param attributeName
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLAttributeFromProperty(WYSIWYGMetaInformation metaInformation, PropertyValue<?> property, String attributeName) {
		StringBuffer sb = new StringBuffer();

		if (property != null && property.getValue() != null) {
			// special handling for attribute "name" here...
			/*if (ATTRIBUTE_NAME.equals(attributeName)) {
				sb.append(" ");
				sb.append(attributeName);
				sb.append("=\"");
				
				final String propertyValue = property.getValue().toString();
				sb.append(metaInformation.getFieldUIDFromCollectableEntity(propertyValue).getStringifiedDefinitionWithEntity());

				sb.append("\"");
				return sb;
			}*/
			
			//NUCLEUSINT-288 the display differs from the value to be set, if label and control should be shown, show only is empty
			if (ATTRIBUTE_SHOWONLY.equals(attributeName))
				if (WYSIWYGUniversalComponent.ATTRIBUTEVALUE_LABEL_AND_CONTROL.equals(property.getValue().toString()))
					return sb;

			sb.append(" ");
			sb.append(attributeName);
			sb.append("=\"");
			
			if (property instanceof PropertyValueString || property instanceof PropertyValueInteger) {
				//NUCLEUSINT-453
				final String propertyValue = property.getValue().toString();
				if (JTABBEDPANE.SCROLL_TAB_LAYOUT.equals(propertyValue))
					sb.append(ATTRIBUTEVALUE_SCROLL);
				else if (JTABBEDPANE.WRAP_TAB_LAYOUT.equals(propertyValue))
					sb.append(ATTRIBUTEVALUE_WRAP);
				else if (STATIC_BUTTON.DUMMY_BUTTON_ACTION_LABEL.equals(propertyValue)) {
					//NUCLEUSINT-1159
					sb.append(STATIC_BUTTON.DUMMY_BUTTON_ACTION);
				} else if (STATIC_BUTTON.STATE_CHANGE_ACTION_LABEL.equals(propertyValue)) {
					//NUCLEUSINT-1159
					sb.append(STATIC_BUTTON.STATE_CHANGE_ACTION);
				} else if (STATIC_BUTTON.EXECUTE_RULE_ACTION_LABEL.equals(propertyValue)) {
					//NUCLOSINT-743
					sb.append(STATIC_BUTTON.EXECUTE_RULE_ACTION);
				} else if (STATIC_BUTTON.GENERATOR_ACTION_LABEL.equals(propertyValue)) {
					sb.append(STATIC_BUTTON.GENERATOR_ACTION);
				} else if (STATIC_BUTTON.HYPERLINK_ACTION_LABEL.equals(propertyValue)) {
					sb.append(STATIC_BUTTON.HYPERLINK_ACTION);
				} else
 					sb.append(StringUtils.xmlEncode(propertyValue));
			} else if (property instanceof PropertyValueBoolean) {
				if (((Boolean) property.getValue())) {
					sb.append(ATTRIBUTEVALUE_YES);
				} else {
					sb.append(ATTRIBUTEVALUE_NO);
				}
			} else if (property instanceof PropertyValueInteger) {
				sb.append(property.getValue());
			} else if (property instanceof PropertyValueDouble) {
				sb.append(property.getValue());
			}
			sb.append("\"");
		}

		return sb;
	}

	/**
	 * Method converting the {@link WYSIWYGTabbedPane} Constraints to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param title
	 * @param enabled
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLTabbedPaneConstraints(String title, boolean enabled, int blockDeep, TranslationMap translations, String internalname, Integer  mnemonic) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + ELEMENT_TABBEDPANECONSTRAINTS + " ");
		block.append(ATTRIBUTE_TITLE + "=\"");
		block.append(StringUtils.xmlEncode(title));
		block.append("\" " + ATTRIBUTE_ENABLED + "=\"");
		block.append(enabled ? ATTRIBUTEVALUE_YES : ATTRIBUTEVALUE_NO);
		if(internalname != null) {
			block.append("\" " + ATTRIBUTE_INTERNALNAME + "=\"");
			block.append(StringUtils.xmlEncode(internalname));
		}
		if(mnemonic != null && mnemonic > 0) {
			block.append("\" " + ATTRIBUTE_MNEMONIC + "=\"");
			block.append(mnemonic);
		}
		block.append("\"");
		if (translations != null && !translations.isEmpty()) {
			block.append(">");
			block.append(getLayoutMLTranslations(translations, blockDeep + 1));
			block.linebreak();
			block.append("</" + ELEMENT_TABBEDPANECONSTRAINTS + ">");
		} else {
			block.append(" />");
		}

		return block.getStringBuffer();
	}

	/**
	 * Method converting the {@link TableLayoutConstraints} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param c
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLTableLayoutConstraints(Component c, TableLayout tableLayout, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		TableLayoutConstraints constraints = tableLayout.getConstraints(c);

		// <tablelayout-constraints col1="0" col2="0" row1="0" row2="0"
		// hAlign="0" vAlign="0" />
		block.append("<" + ELEMENT_TABLELAYOUTCONSTRAINTS + " ");
		block.append(ATTRIBUTE_COL1 + "=\"");
		block.append(constraints.col1);
		block.append("\" " + ATTRIBUTE_COL2 + "=\"");
		block.append(constraints.col2);
		block.append("\" " + ATTRIBUTE_ROW1 + "=\"");
		block.append(constraints.row1);
		block.append("\" " + ATTRIBUTE_ROW2 + "=\"");
		block.append(constraints.row2);
		block.append("\" " + ATTRIBUTE_HALIGN + "=\"");
		block.append(constraints.hAlign);
		block.append("\" " + ATTRIBUTE_VALIGN + "=\"");
		block.append(constraints.vAlign);
		block.append("\" />");

		return block.getStringBuffer();
	}

	/**
	 * Method converting PreferredSize to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param cp
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLPreferredSizeFromProperty(ComponentProperties cp, int blockDeep) {
		return getLayoutMLSize(ELEMENT_PREFERREDSIZE, null, (PropertyValueDimension) cp.getProperty(WYSIWYGCollectableComponent.PROPERTY_PREFFEREDSIZE), blockDeep);
	}

	/**
	 * Method converting PreferredSize to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param cp
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLSize(String element, String attributeName, PropertyValueDimension propertyValue, int blockDeep) {
		Dimension dim = null;
		if (propertyValue != null)
			dim = (Dimension) propertyValue.getValue();
		if (dim != null) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);
			// <preferred-size height="30" width="80" />
			block.append("<" + element + " ");
			if (attributeName != null) {
				block.append(ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(attributeName) + "\" ");
			}
			block.append(ATTRIBUTE_HEIGHT + "=\"");
			block.append(dim.height);
			block.append("\" " + ATTRIBUTE_WIDTH + "=\"");
			block.append(dim.width);
			block.append("\" />");
			return block.getStringBuffer();
		}
		return new StringBuffer();
	}

	/**
	 * Method converting StrictSize to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param cp
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLStrictSizeFromProperty(ComponentProperties cp, int blockDeep) {
		Dimension dim = null;
		if (cp.getProperty(WYSIWYGCollectableComponent.PROPERTY_STRICTSIZE) != null)
			dim = (Dimension) cp.getProperty(WYSIWYGCollectableComponent.PROPERTY_STRICTSIZE).getValue();
		if (dim != null) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);
			// <preferred-size height="30" width="80" />
			block.append("<" + ELEMENT_STRICTSIZE + " ");
			block.append(ATTRIBUTE_HEIGHT + "=\"");
			block.append(dim.height);
			block.append("\" " + ATTRIBUTE_WIDTH + "=\"");
			block.append(dim.width);
			block.append("\" />");
			return block.getStringBuffer();
		}
		return new StringBuffer();
	}

	private synchronized StringBuffer getLayoutMLTextFormatFromProperty(ComponentProperties cp, int blockDeep) {
		final LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		boolean isEmpty = true;
		block.append("<" + ELEMENT_TEXTFORMAT);
		final PropertyValueBoolean textBoldProperty = (PropertyValueBoolean) cp.getProperty(WYSIWYGComponent.PROPERTY_BOLD);
		if (textBoldProperty != null && Boolean.TRUE.equals(textBoldProperty.getValue())) {
			isEmpty = false;
			block.append(" " + ATTRIBUTE_BOLD + "=\"" + ATTRIBUTEVALUE_YES + "\"");
		}
		final PropertyValueBoolean textItalicProperty = (PropertyValueBoolean) cp.getProperty(WYSIWYGComponent.PROPERTY_ITALIC);
		if (textItalicProperty != null && Boolean.TRUE.equals(textItalicProperty.getValue())) {
			isEmpty = false;
			block.append(" " + ATTRIBUTE_ITALIC + "=\"" + ATTRIBUTEVALUE_YES + "\"");
		}
		final PropertyValueBoolean textUnderlineProperty = (PropertyValueBoolean) cp.getProperty(WYSIWYGComponent.PROPERTY_UNDERLINE);
		if (textUnderlineProperty != null && Boolean.TRUE.equals(textUnderlineProperty.getValue())) {
			isEmpty = false;
			block.append(" " + ATTRIBUTE_UNDERLINE + "=\"" + ATTRIBUTEVALUE_YES + "\"");
		}
		block.append(">");
		final PropertyValueColor textColorProperty = (PropertyValueColor) cp.getProperty(WYSIWYGComponent.PROPERTY_TEXTCOLOR);
		if (textColorProperty != null) {
			final StringBuffer sColor = getLayoutMLColor(ELEMENT_TEXTCOLOR, null, textColorProperty, blockDeep + 1);
			if (sColor.length() > 0) {
				isEmpty = false;
				block.append(sColor);
				block.linebreak();
			}
		}
		block.append("</" + ELEMENT_TEXTFORMAT + ">");
		if (isEmpty) {
			return new StringBuffer();
		}
		return block.getStringBuffer();
	}

	/**
	 * Only for WYSIWYGLayoutEditorPanel!
	 *
	 * @param cp
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLBackgroundColorFromProperty(ComponentProperties cp, int blockDeep) {
		return getLayoutMLColor(ELEMENT_BACKGROUND, null, (PropertyValueColor) cp.getProperty(WYSIWYGComponent.PROPERTY_BACKGROUNDCOLOR), blockDeep);
	}

	/**
	 * Only for WYSIWYGLayoutEditorPanel!
	 *
	 * @param cp
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLColor(String element, String attributeName, PropertyValueColor propertyValue, int blockDeep) {

		if (propertyValue != null && propertyValue.getValue() != null) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);

			if (propertyValue.getValue() instanceof Color) {
				block.append("<" + element);
				if (attributeName != null) {
					block.append(" " +ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(attributeName) + "\"");
				}
				block.append(getLayoutMLColorAttributes((Color) propertyValue.getValue()));
				block.append(" />");
			}

			return block.getStringBuffer();
		}

		return new StringBuffer();
	}

	/**
	 * Method converting the Description {@link PropertyValueDescription} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param subform
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLDescription(ComponentProperties cp, int blockDeep) {
		if (cp.getProperty(WYSIWYGStaticTextfield.PROPERTY_DESCRIPTION) != null && cp.getProperty(WYSIWYGStaticTextfield.PROPERTY_DESCRIPTION).getValue() != null) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);
			block.append("<" + ELEMENT_DESCRIPTION + ">");
			block.append(StringUtils.xmlEncode((String) cp.getProperty(WYSIWYGStaticTextfield.PROPERTY_DESCRIPTION).getValue()));
			block.append("</" + ELEMENT_DESCRIPTION + ">");
			return block.getStringBuffer();
		}
		return new StringBuffer();
	}

	/**
	 * Method converting the Description {@link PropertyValueFont} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param subform
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLFontFromProperty(ComponentProperties cp, int blockDeep) {
		return getLayoutMLFont(ELEMENT_FONT, null, (PropertyValueFont) cp.getProperty(WYSIWYGCollectableComponent.PROPERTY_FONT), blockDeep);
	}

	/**
	 * Method converting the Description {@link PropertyValueFont} to LayoutML XML.
	 *
	 *
	 * @see LayoutMLBlock
	 * @param subform
	 * @param tableLayout
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLFont(String element, String attributeName, PropertyValueFont propertyValue, int blockDeep) {
		if (propertyValue != null && propertyValue.getValue() != null) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);
			block.append("<" + element + " ");
			if (attributeName != null) {
				block.append(ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(attributeName) + "\" ");
			}
			block.append(ATTRIBUTE_SIZE + "=\"" + ((Integer) propertyValue.getValue()).intValue() + "\"/>");
			return block.getStringBuffer();
		}
		return new StringBuffer();
	}

	/**
	 * Only for WYSIWYGLayoutEditorPanel!
	 *
	 * @param cp
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLBordersFromProperty(ComponentProperties cp, int blockDeep) {
		if (cp.getProperty(WYSIWYGComponent.PROPERTY_BORDER) != null && (cp.getProperty(WYSIWYGComponent.PROPERTY_BORDER).getValue() != null || ((PropertyValueBorder) cp.getProperty(WYSIWYGComponent.PROPERTY_BORDER)).isClearBorder())) {
			StringBuffer block = new StringBuffer();

			PropertyValueBorder value = (PropertyValueBorder) cp.getProperty(WYSIWYGComponent.PROPERTY_BORDER);


			if (value.getValue() != null) {
				Border border = value.getValue();
				/** Borders are nested, there can be one or more border */
				while (border instanceof CompoundBorder) {
					block = getLayoutMLBorderFromBorder(((CompoundBorder) border).getOutsideBorder(), blockDeep).append(block);
					border = ((CompoundBorder) border).getInsideBorder();
				}
				/** this single border is converted to layoutML */
				block = getLayoutMLBorderFromBorder(border, blockDeep).append(block);
			}

			if (value.isClearBorder()) {
				block = getLayoutMLClearBorder(blockDeep).append(block);
			}

			return block;
		}
		return new StringBuffer();
	}

	/**
	 * Method converting the ClearBorder to LayoutML XML.
	 *
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLClearBorder(int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		block.append("<" + ELEMENT_CLEARBORDER + " />");
		return block.getStringBuffer();
	}

	/**
	 * Method converting Border(s) to LayoutML XML.
	 *
	 * @param blockDeep
	 * @param border
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLBorderFromBorder(Border border, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		if (border instanceof TitledBorder) {
			block.append("<" + ELEMENT_TITLEDBORDER + " ");
			block.append(ATTRIBUTE_TITLE + "=\"");
			block.append(StringUtils.xmlEncode(((TitledBorder) border).getTitle()) + "\">");
			block.append(getLayoutMLTranslations(((TitledBorderWithTranslations) border).getTranslations(), blockDeep + 1));
			block.linebreak();
			block.append("</" + ELEMENT_TITLEDBORDER + ">");
		} else if (border instanceof LineBorder) {
			block.append("<" + ELEMENT_LINEBORDER);
			block.append(getLayoutMLColorAttributes(((LineBorder) border).getLineColor()));
			block.append(" " + ATTRIBUTE_THICKNESS + "=\"");
			block.append(((LineBorder) border).getThickness());
			block.append("\" />");
		} else if (border instanceof BevelBorder) {
			block.append("<" + ELEMENT_BEVELBORDER + " ");
			block.append(ATTRIBUTE_TYPE + "=\"");
			block.append(((BevelBorder) border).getBevelType() == BevelBorder.RAISED ? ATTRIBUTEVALUE_RAISED : ATTRIBUTEVALUE_LOWERED);
			block.append("\" />");
		} else if (border instanceof EtchedBorder) {
			block.append("<" + ELEMENT_ETCHEDBORDER + " ");
			block.append(ATTRIBUTE_TYPE + "=\"");
			block.append(((EtchedBorder) border).getEtchType() == EtchedBorder.RAISED ? ATTRIBUTEVALUE_RAISED : ATTRIBUTEVALUE_LOWERED);
			block.append("\" />");
		} else if (border instanceof EmptyBorder) {
			block.append("<" + ELEMENT_EMPTYBORDER + " ");
			block.append(ATTRIBUTE_TOP + "=\"");
			block.append(((EmptyBorder) border).getBorderInsets().top + "\" ");
			block.append(ATTRIBUTE_LEFT + "=\"");
			block.append(((EmptyBorder) border).getBorderInsets().left + "\" ");
			block.append(ATTRIBUTE_BOTTOM + "=\"");
			block.append(((EmptyBorder) border).getBorderInsets().bottom + "\" ");
			block.append(ATTRIBUTE_RIGHT + "=\"");
			block.append(((EmptyBorder) border).getBorderInsets().right + "\" ");
			block.append("/>");
		}
		return block.getStringBuffer();
	}

	/**
	 * Method converting {@link LayoutMLRules}
	 *
	 * @param layoutMLRules
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLLayoutMLRules(LayoutMLRules layoutMLRules, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + ELEMENT_RULES + ">");

		List<LayoutMLRule> lstRules = new ArrayList<>();
		layoutMLRules.getRules().forEach((rule) -> {
			lstRules.add(rule);
		});
		Collections.sort(lstRules, Comparator.comparing(LayoutMLRule::toString));

		for (LayoutMLRule layoutMLRule : layoutMLRules.getRules()) {
			block.append(getLayoutMLLayoutMLRule(layoutMLRule, blockDeep + 1));
		}
		block.linebreak();
		block.append("</" + ELEMENT_RULES + ">");
		return block.getStringBuffer();
	}

	/**
	 * Method converting {@link LayoutMLRule}
	 *
	 * @param layoutMLRule
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLLayoutMLRule(LayoutMLRule layoutMLRule, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		if (layoutMLRule.getRuleName().length() > 0)
			block.append("<" + ELEMENT_RULE + " " + ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(layoutMLRule.getRuleName()) + "\">");
		else
			block.append("<" + ELEMENT_RULE + ">");

		block.append(getLayoutMLLayoutMLRuleEventType(layoutMLRule.getLayoutMLRuleEventType(), blockDeep + 1));
		if (layoutMLRule.getLayoutMLRuleCondition().getConditionString().length() > 0)
			block.append(getLayoutMLLayoutMLRuleCondition(layoutMLRule.getLayoutMLRuleCondition(), blockDeep + 1));

		block.append(getLayoutMLLayoutMLRuleActions(layoutMLRule.getLayoutMLRuleActions(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_RULE + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting {@link LayoutMLRuleEventType}
	 *
	 * @param layoutMLRuleEventType
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLLayoutMLRuleEventType(LayoutMLRuleEventType layoutMLRuleEventType, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + ELEMENT_EVENT + " " + ATTRIBUTE_TYPE + "=\"");
		block.append(layoutMLRuleEventType.getEventType());
		block.append("\" ");

		if (layoutMLRuleEventType.getEntity() != null) {
			block.append(ATTRIBUTE_ENTITY + "=\"" + layoutMLRuleEventType.getEntity().getStringifiedDefinitionWithEntity(E.ENTITY) + "\" ");
		}

		block.append(ATTRIBUTE_SOURCECOMPONENT + "=\"" + layoutMLRuleEventType.getSourceComponent().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\"");

		block.append("/>");

		return block.getStringBuffer();
	}

	/**
	 * Method converting {@link LayoutMLRuleCondition}
	 *
	 * NOT INTERPRETED BY LAYOUTMLPARSER!
	 *
	 * @param layoutMLRuleCondition
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLLayoutMLRuleCondition(LayoutMLRuleCondition layoutMLRuleCondition, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		if (layoutMLRuleCondition.getConditionString().length() > 0) {
			block.append("<" + ELEMENT_CONDITION + ">");
			block.linebreak();
			block.append(StringUtils.xmlEncode(layoutMLRuleCondition.getConditionString()));
			block.linebreak();
			block.append("<" + ELEMENT_CONDITION + ">");
			block.linebreak();
		}

		return block.getStringBuffer();
	}

	/**
	 * Method converting {@link LayoutMLRuleActions}
	 *
	 * NOT INTERPRETED BY LAYOUTMLPARSER!
	 *
	 * @param layoutMLRuleActions
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLLayoutMLRuleActions(LayoutMLRuleActions layoutMLRuleActions, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + ELEMENT_ACTIONS + ">");
		if (layoutMLRuleActions.getSingleActions().size() > 0) {
			List<LayoutMLRuleAction> lstActions = new ArrayList<>();
			layoutMLRuleActions.getSingleActions().forEach((action) -> {
				lstActions.add(action);
			});
			Collections.sort(lstActions, Comparator.comparing(LayoutMLRuleAction::toString));
			for (LayoutMLRuleAction singleAction : lstActions) {
				block.append(getLayoutMLLayoutMLRuleAction(singleAction, blockDeep + 1));
			}

		}
		block.linebreak();
		block.append("</" + ELEMENT_ACTIONS + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method converting {@link LayoutMLRuleAction}
	 *
	 * @param layoutMLRuleAction
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLLayoutMLRuleAction(LayoutMLRuleAction layoutMLRuleAction, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		if (!layoutMLRuleAction.getRuleAction().equals("")) {
			if (layoutMLRuleAction.getRuleAction().equals(LayoutMLRuleAction.CLEAR)) {
				block.append("<" + ELEMENT_CLEAR + " ");
				if (layoutMLRuleAction.getEntity() != null) {
					block.append(ATTRIBUTE_ENTITY + "=\"" + layoutMLRuleAction.getEntity().getStringifiedDefinitionWithEntity(E.ENTITY) + "\" ");
				}
				block.append(ATTRIBUTE_TARGETCOMPONENT + "=\"" + layoutMLRuleAction.getTargetComponent().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\"");
				block.append("/>");
			} else if (layoutMLRuleAction.getRuleAction().equals(LayoutMLRuleAction.ENABLE)) {
				block.append("<" + ELEMENT_ENABLE + " ");
				block.append(ATTRIBUTE_TARGETCOMPONENT + "=\"" + layoutMLRuleAction.getTargetComponent().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\" ");
				String invertableValue = layoutMLRuleAction.isInvertable() ? "yes" : "no";
				block.append(ATTRIBUTE_INVERTABLE + "=\"" + invertableValue + "\"");
				block.append("/>");
			} else if (layoutMLRuleAction.getRuleAction().equals(LayoutMLRuleAction.REFRESH_VALUELIST)) {
				block.append("<" + ELEMENT_REFRESHVALUELIST + " ");
				if (layoutMLRuleAction.getEntity() != null) {
					block.append(ATTRIBUTE_ENTITY + "=\"" + layoutMLRuleAction.getEntity().getStringifiedDefinitionWithEntity(E.ENTITY) + "\" ");
				}
				block.append(ATTRIBUTE_TARGETCOMPONENT + "=\"" + layoutMLRuleAction.getTargetComponent().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\" ");
				if (!StringUtils.isNullOrEmpty(layoutMLRuleAction.getParameterForSourceComponent())) {
					block.append(ATTRIBUTE_PARAMETER_FOR_SOURCECOMPONENT + "=\"" + layoutMLRuleAction.getParameterForSourceComponent() + "\"");
				}
				block.append("/>");
			} else if (layoutMLRuleAction.getRuleAction().equals(LayoutMLRuleAction.REINIT_SUBFORM)) {
				block.append("<" + ELEMENT_REINIT_SUBFORM + " ");
				if (layoutMLRuleAction.getEntity() != null) {
					block.append(ATTRIBUTE_ENTITY + "=\"" + layoutMLRuleAction.getEntity().getStringifiedDefinitionWithEntity(E.ENTITY) + "\" ");
				}
				block.append(ATTRIBUTE_TARGETCOMPONENT + "=\"" + layoutMLRuleAction.getTargetComponent().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\"");
				block.append("/>");
			} 
			else if (layoutMLRuleAction.getRuleAction().equals(LayoutMLRuleAction.TRANSFER_LOOKEDUP_VALUE)) {
				block.append("<" + ELEMENT_TRANSFERLOOKEDUPVALUE + " ");
				if (layoutMLRuleAction.getSourceField() != null) {
					// FIX NUCLEUSINT-305
					block.append(ATTRIBUTE_SOURCEFIELD + "=\"" + layoutMLRuleAction.getSourceField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\" ");
				}
				block.append(ATTRIBUTE_TARGETCOMPONENT + "=\"" + layoutMLRuleAction.getTargetComponent().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\"");
				block.append("/>");
			}
		}

		return block.getStringBuffer();
	}

	/**
	 * Method for converting {@link WYSIWYGValuelistProvider} to LayoutML XML
	 *
	 * @param wysiwygStaticValuelistProvider
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLValueListProvider(WYSIWYGValuelistProvider wysiwygStaticValuelistProvider, int blockDeep) {
		return getLayoutMLValueListProvider(ELEMENT_VALUELISTPROVIDER, null, wysiwygStaticValuelistProvider, blockDeep);
	}

	/**
	 * Method for converting {@link WYSIWYGValuelistProvider} to LayoutML XML
	 *
	 * @param wysiwygStaticValuelistProvider
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLValueListProvider(String element, String attributeName, PropertyValueValuelistProvider propertyValue, int blockDeep) {
		if (propertyValue != null && propertyValue.getValue().getType() != null) {
			return getLayoutMLValueListProvider(element, attributeName, propertyValue.getValue(), blockDeep);
		}
		return new StringBuffer();
	}

	/**
	 * Method for converting {@link WYSIWYGValuelistProvider} to LayoutML XML
	 *
	 * @param wysiwygStaticValuelistProvider
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLValueListProvider(String element, String attributeName, WYSIWYGValuelistProvider wysiwygStaticValuelistProvider, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + element);
		if (attributeName != null) {
			block.append(" " + ATTRIBUTE_PROP_NAME + "=\"" + StringUtils.xmlEncode(attributeName) + "\"");
		}
		block.append(" " + ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(wysiwygStaticValuelistProvider.getName()) + "\"");
		block.append(" " + ATTRIBUTE_TYPE + "=\"" + StringUtils.xmlEncode(wysiwygStaticValuelistProvider.getType().getType()) + "\"");
		block.append(" " + ATTRIBUTE_VALUE + "=\"" + StringUtils.xmlEncode(wysiwygStaticValuelistProvider.getValue()) + "\"");
		if (wysiwygStaticValuelistProvider.isEntityAndFieldAvailable()) {
			block.append(" " + ATTRIBUTE_ENTITY + "=\"" + StringUtils.xmlEncode(wysiwygStaticValuelistProvider.getEntity().getStringifiedDefinitionWithEntity(E.ENTITY)) + "\"");
			block.append(" " + ATTRIBUTE_FIELD + "=\"" + StringUtils.xmlEncode(wysiwygStaticValuelistProvider.getField().getStringifiedDefinitionWithEntity(E.ENTITYFIELD)) + "\"");
		}

		if (wysiwygStaticValuelistProvider.getAllWYSIYWYGParameter().size() == 0) {
			block.append("/>");
		} else {
			block.append(">");
			for (WYSIWYGParameter wysiwygParameter : wysiwygStaticValuelistProvider.getAllWYSIYWYGParameterSorted(false)) {
				block.append(getLayoutMLParameter(wysiwygParameter, blockDeep + 1));
			}
			block.linebreak();
			block.append("</" + element + ">");
		}

		return block.getStringBuffer();
	}

	/**
	 * Method for converting {@link WYSIWYGOptions} to LayoutML XML
	 *
	 * @param options
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLOptions(WYSIWYGOptions options, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + ELEMENT_OPTIONS);
		block.append(" name=\"" + StringUtils.xmlEncode(options.getName()) + "\"");
		block.append(" default=\"" + StringUtils.xmlEncode(options.getDefaultValue()) + "\"");
		block.append(" orientation=\"" + StringUtils.xmlEncode(options.getOrientation()) + "\"");
		block.append(">");

		for (WYSIWYGOption option : options.getAllOptionValues()) {
			block.append(getLayoutMLOption(option, blockDeep + 1));
		}

		block.linebreak();
		block.append("</" + ELEMENT_OPTIONS + ">");

		return block.getStringBuffer();
	}

	/**
	 * Method for converting {@link WYSIWYGOption} to LayoutML XML.
	 *
	 * @param option
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLOption(WYSIWYGOption option, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		block.append("<" + ELEMENT_OPTION);
		if (!option.getName().equals(""))
			block.append(" " + ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(option.getName()) + "\"");
		block.append(" " + ATTRIBUTE_VALUE + "=\"" + StringUtils.xmlEncode(option.getValue()) + "\"");
		block.append(" " + ATTRIBUTE_LABEL + "=\"" + StringUtils.xmlEncode(option.getLabel()) + "\"");
		if (!option.getMnemonic().equals(""))
			block.append(" " + ATTRIBUTE_MNEMONIC + "=\"" + StringUtils.xmlEncode(option.getMnemonic()) + "\"");
		block.append(">");
		block.append(getLayoutMLTranslations(option.getTranslations(), blockDeep + 1));
		block.linebreak();
		block.append("</" + ELEMENT_OPTION + ">");

		return block.getStringBuffer();
	}

	/**
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLForProperty(String name, PropertyValue<?> property, int blockDeep) {

		if (property instanceof PropertyValueString ||
				property instanceof PropertyValueInteger ||
				property instanceof PropertyValueBoolean) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);
			block.append("<" + ELEMENT_PROPERTY);
			block.append(" " + ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(name) + "\"");
			block.append(" " + ATTRIBUTE_VALUE + "=\"");
			if (property instanceof PropertyValueString || property instanceof PropertyValueInteger) {
				block.append(StringUtils.xmlEncode(property.getValue().toString()));
			} else if (property instanceof PropertyValueBoolean) {
				if (((Boolean) property.getValue())) {
					block.append(ATTRIBUTEVALUE_YES);
				} else {
					block.append(ATTRIBUTEVALUE_NO);
				}
			}
			block.append("\"/>");
			return block.getStringBuffer();
		} else if (property instanceof PropertyValueDimension) {
			return getLayoutMLSize(ELEMENT_PROPERTY_SIZE, name, (PropertyValueDimension) property, blockDeep);
		} else if (property instanceof PropertyValueColor) {
			return getLayoutMLColor(ELEMENT_PROPERTY_COLOR, name, (PropertyValueColor) property, blockDeep);
		} else if (property instanceof PropertyValueFont) {
			return getLayoutMLFont(ELEMENT_PROPERTY_FONT, name, (PropertyValueFont) property, blockDeep);
		} else if (property instanceof PropertyValueScript) {
			return getLayoutMLScript(ELEMENT_PROPERTY_SCRIPT, name, (PropertyValueScript) property, blockDeep);
		} else if (property instanceof PropertyValueTranslations) {
			return getLayoutMLTranslations(ELEMENT_PROPERTY_TRANSLATIONS, name, (PropertyValueTranslations) property, blockDeep);
		} else if (property instanceof PropertyValueValuelistProvider) {
			return getLayoutMLValueListProvider(ELEMENT_PROPERTY_VALUELIST_PROVIDER, name, (PropertyValueValuelistProvider) property, blockDeep);
		} else  {
			throw new NotImplementedException("getLayoutMLForProperty with type " + property.getClass().getName());
		}

	}

	/**
	 * Method for converting {@link WYSIWYGProperty} to LayoutML XML.
	 *
	 * @param wysiwygProperty
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLCollectableComponentProperty(WYSIWYGProperty wysiwygProperty, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		Vector<WYSIWYGPropertySet> vector = wysiwygProperty.getAllPropertyEntries();

		//NUCLOS-2845 ensure deterministic order
		Collections.sort(vector, Comparator.comparing(WYSIWYGPropertySet::getPropertyName));

		for (Iterator<WYSIWYGPropertySet> it = vector.iterator(); it.hasNext(); ) {
			WYSIWYGPropertySet propertySet = it.next();
			block.append("<" + ELEMENT_PROPERTY);
			block.append(" " + ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(propertySet.getPropertyName()) + "\"");
			block.append(" " + ATTRIBUTE_VALUE + "=\"" + StringUtils.xmlEncode(propertySet.getPropertyValue()) + "\"");
			block.append("/>");
			if (it.hasNext())
				block.linebreak();
		}

		return block.getStringBuffer();
	}

	/**
	 * Method for converting {@link WYSIWYGParameter} to LayoutML XML.
	 *
	 * @param wysiwygParameter
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLParameter(WYSIWYGParameter wysiwygParameter, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		/** <dependency dependant-field="process" depends-on="module"/> */

		block.append("<" + ELEMENT_PARAMETER + " ");
		block.append(ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(wysiwygParameter.getParameterName()) + "\" ");
		block.append(ATTRIBUTE_VALUE + "=\"" + StringUtils.xmlEncode(wysiwygParameter.getParameterValue()) + "\" ");
		block.append("/>");

		return block.getStringBuffer();
	}

	/**
	 * Method for converting {@link Color} to LayoutML XML.
	 *
	 * @param color
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLColorAttributes(Color color) {
		StringBuffer sb = new StringBuffer();

		sb.append(" " + ATTRIBUTE_RED + "=\"");
		sb.append(color.getRed());
		sb.append("\" " + ATTRIBUTE_GREEN + "=\"");
		sb.append(color.getGreen());
		sb.append("\" " + ATTRIBUTE_BLUE + "=\"");
		sb.append(color.getBlue());
		sb.append("\"");

		return sb;

	}

	/**
	 *  Method for converting {@link Dimension} minimumSize to LayoutML XML.
	 *
	 * @param c
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLMinimumSizeFromComponent(Component c, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		Dimension dim = c.getMinimumSize();
		if (dim != null) {
			// <minimum-size height="30" width="80" />
			block.append("<" + ELEMENT_MINIMUMSIZE + " ");
			block.append(ATTRIBUTE_HEIGHT + "=\"");
			block.append(dim.height);
			block.append("\" " + ATTRIBUTE_WIDTH + "=\"");
			block.append(dim.width);
			block.append("\" />");
		}

		return block.getStringBuffer();
	}

	/**
	 *  Method for converting {@link WYSIWYGInitialSortingOrder} minimumSize to LayoutML XML.
	 *
	 * @param value
	 * @param blockDeep
	 * @return {@link StringBuffer} with the LayoutML
	 */
	private synchronized StringBuffer getLayoutMLInitialSortingOrder(WYSIWYGInitialSortingOrder value, int blockDeep) {
		if (value != null) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);
			block.append("<" + ELEMENT_INITIALSORTINGORDER + " " + ATTRIBUTE_NAME + "=\"" + value.getName().getStringifiedDefinitionWithEntity(E.ENTITYFIELD) + "\" ");
			block.append(ATTRIBUTE_SORTINGORDER + "=\"" + value.getSortingOrder() + "\" />");
			return block.getStringBuffer();
		} else {
			return new StringBuffer();
		}
	}
	
	private synchronized StringBuffer getLayoutMLForTextModule(ComponentProperties props, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);
		final String sTextModuleEntity = (String) props.getProperty(WYSIWYGCollectableTextArea.PROPERTY_TEXTMODULE_ENTITY).getValue();
		final String sTextModuleEntityFieldName = (String) props.getProperty(WYSIWYGCollectableTextArea.PROPERTY_TEXTMODULE_ENTITYFIELD_NAME).getValue();
		final String sTextModuleEntityField = (String) props.getProperty(WYSIWYGCollectableTextArea.PROPERTY_TEXTMODULE_ENTITYFIELD).getValue();
		final String sTextModuleEntityFieldSort = (String) props.getProperty(WYSIWYGCollectableTextArea.PROPERTY_TEXTMODULE_ENTITYFIELD_SORT).getValue();

		if (null != sTextModuleEntity &&
				null != sTextModuleEntityField) {
			
			block.append("<" + ELEMENT_TEXTMODULE + " ");
			block.append(ATTRIBUTE_TEXTMODULE_ENTITY + "=\"");
			block.append(sTextModuleEntity);
			block.append("\" ");
			
			block.append(ATTRIBUTE_TEXTMODULE_ENTITY_FIELD + "=\"");
			block.append(sTextModuleEntityField);
			block.append("\" ");

			// name is optional
			if (null != sTextModuleEntityFieldName) {
				block.append(ATTRIBUTE_TEXTMODULE_ENTITY_FIELD_NAME+ "=\"");
				block.append(sTextModuleEntityFieldName);
				block.append("\" ");
			}
			
			// sort is optional
			if (null != sTextModuleEntityFieldSort) {
				block.append(ATTRIBUTE_TEXTMODULE_ENTITY_FIELD_SORT + "=\"");
				block.append(sTextModuleEntityFieldSort);
				block.append("\" ");
			}
			
			block.append("/>");
		}
		return block.getStringBuffer();
	}

	private synchronized StringBuffer getLayoutMLTranslations(TranslationMap translations, int blockDeep) {
		return getLayoutMLTranslations(ELEMENT_TRANSLATIONS, null, translations, blockDeep);
	}

	private synchronized StringBuffer getLayoutMLTranslationsFromProperty(ComponentProperties cp, int blockDeep) {
		return getLayoutMLTranslations(ELEMENT_TRANSLATIONS, null, (PropertyValueTranslations) cp.getProperty(WYSIWYGCollectableComponent.PROPERTY_TRANSLATIONS), blockDeep);
	}

	private synchronized StringBuffer getLayoutMLTranslations(String element, String attributeName, PropertyValueTranslations propertyValue, int blockDeep) {
		if (propertyValue != null) {
			TranslationMap translations = (TranslationMap) propertyValue.getValue();
			return getLayoutMLTranslations(element, attributeName, translations, blockDeep);
		}
		return new StringBuffer();
	}

	private synchronized StringBuffer getLayoutMLTranslations(String element, String attributeName, TranslationMap translations, int blockDeep) {
		if (translations != null && !translations.isEmpty()) {
			LayoutMLBlock block = new LayoutMLBlock(blockDeep);
			block.append("<" + element);
			if (attributeName != null) {
				block.append(" " + ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(attributeName) + "\"");
			}
			block.append(">");
			SortedSet<Map.Entry<String, String>> sortedTranslations = new TreeSet<>(Comparator.comparing(Map.Entry::getKey));
			translations.entrySet().forEach((entry) -> {
				sortedTranslations.add(entry);
			});
			for (Map.Entry<String, String> e : sortedTranslations) {
				String lang = e.getKey();
				String text = e.getValue();
				LayoutMLBlock block2 = new LayoutMLBlock(blockDeep + 1);
				block2.append("<" + ELEMENT_TRANSLATION);
				block2.append(" " + ATTRIBUTE_LANG + "=\"" + StringUtils.xmlEncode(lang) + "\"");
				block2.append(" " + ATTRIBUTE_TEXT + "=\"" + StringUtils.xmlEncode(text) + "\"");
				block2.append("/>");
				block.append(block2.getStringBuffer());
			}
			block.linebreak();
			block.append("</" + element + ">");
			return block.getStringBuffer();
		}
		return new StringBuffer();
	}


	private synchronized StringBuffer getLayoutMLScriptFromProperty(String property, String element, ComponentProperties cp, int blockDeep) {
		PropertyValue<?> propertyValue = cp.getProperty(property);
		if (propertyValue == null) {
			return new StringBuffer();
		}
		return getLayoutMLScript(element, null, (PropertyValueScript) propertyValue, blockDeep);
	}

	private synchronized StringBuffer getLayoutMLScript(String element, String attributeName, PropertyValueScript propertyValue, int blockDeep) {
		LayoutMLBlock block = new LayoutMLBlock(blockDeep);

		NuclosScript script = (NuclosScript) propertyValue.getValue();
		if (script == null) {
			return new StringBuffer();
		}

		block.append("<" + element + " ");
		if (attributeName != null) {
			block.append(ATTRIBUTE_NAME + "=\"" + StringUtils.xmlEncode(attributeName) + "\" ");
		}
		block.append(ATTRIBUTE_LANGUAGE + "=\"");
		block.append(StringUtils.xmlEncode(script.getLanguage()));
		block.append("\">");
		block.linebreak();
		block.append("<![CDATA[");
		block.linebreak();
		block.append(script.getSource());
		block.linebreak();
		block.append("]]>");
		block.linebreak();
		block.append("</" + element + ">");
		return block.getStringBuffer();
	}

	/** LayoutMLDependencies are not supported by the Parser, is therefor commented out */
	// private synchronized StringBuffer
	// getLayoutMLLayoutMLDependencies(LayoutMLDependencies
	// layoutMLDependencies, int blockDeep) {
	// LayoutMLBlock block = new LayoutMLBlock(blockDeep);
	//
	// block.append("<" + ELEMENT_DEPENDENCIES + ">");
	// for (LayoutMLDependency layoutMLDependency :
	// layoutMLDependencies.getAllDependencies()) {
	// block.append(getLayoutMLLayoutMLDependency(layoutMLDependency, blockDeep
	// + 1));
	// }
	// block.linebreak();
	// block.append("</" + ELEMENT_DEPENDENCIES + ">");
	// return block.getStringBuffer();
	// }
	//
	// private synchronized StringBuffer
	// getLayoutMLLayoutMLDependency(LayoutMLDependency layoutMLDependency, int
	// blockDeep) {
	// LayoutMLBlock block = new LayoutMLBlock(blockDeep);
	//
	// /** <dependency dependant-field="process" depends-on="module"/> */
	//
	// block.append("<" + ELEMENT_DEPENDENCY + " ");
	// block.append(ATTRIBUTE_DEPENDANTFIELD + "=\"" +
	// layoutMLDependency.getDependendField() + "\" ");
	// block.append(ATTRIBUTE_DEPENDSONFIELD + "=\"" +
	// layoutMLDependency.getDependsOnField() + "\" ");
	// block.append("/>");
	//
	// return block.getStringBuffer();
	// }

	/**
	 * Small Helperclass used for containing the LayoutML XML and making "pretty" XML by Indendation and new lines.
	 *
	 * Is using a StringBuffer internal so there is no need to work on Strings in the LayoutML Generationprocess
	 *
	 * <br>
	 * Created by Novabit Informationssysteme GmbH <br>
	 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
	 *
	 * @author <a href="mailto:maik.stueker@novabit.de">maik.stueker</a>
	 * @version 01.00.00
	 */


	private static class LayoutMLBlock {

		private static final String distanceString = "  ";

		private int blockDeep;

		private StringBuffer sb;

		LayoutMLBlock(int blockDeep) {
			this(blockDeep, true);
		}

		LayoutMLBlock(int blockDeep, boolean withInitialLineBreak) {
			this.blockDeep = blockDeep;
			this.sb = new StringBuffer();
			if (withInitialLineBreak) {
				this.linebreak();
			} else {
				sb.append(getDistanceString());
			}
		}

		public void append(double d) {
			sb.append(d);
		}

		public void append(int i) {
			sb.append(i);
		}

		public void append(String s) {
			sb.append(s);
		}

		public void append(StringBuffer s) {
			sb.append(s);
		}

		/**
		 * appends a linebreak to the String
		 */
		public void linebreak() {
			sb.append("\n");
			sb.append(getDistanceString());
		}

		@Override
		public String toString() {
			return sb.toString();
		}

		public StringBuffer getStringBuffer() {
			return sb;
		}

		private StringBuffer getDistanceString() {
			StringBuffer sbDistance = new StringBuffer();
			for (int i = 0; i < blockDeep; i++) {
				sbDistance.append(distanceString);
			}
			return sbDistance;
		}
	}
	
	private final boolean isTextModuleSupportingComponent(final Object objComponent) {
		return (objComponent instanceof WYSIWYGCollectableTextArea || objComponent instanceof WYSIWYGUniversalComponent);
	}
}
