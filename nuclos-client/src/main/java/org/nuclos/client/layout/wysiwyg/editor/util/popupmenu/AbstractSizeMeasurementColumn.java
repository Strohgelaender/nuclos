package org.nuclos.client.layout.wysiwyg.editor.util.popupmenu;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.layout.wysiwyg.editor.util.TableLayoutUtil;
import org.nuclos.client.layout.wysiwyg.editor.util.valueobjects.TableLayoutPanel;

/**
 * Created by Oliver Brausch on 23.11.17.
 */
public abstract class AbstractSizeMeasurementColumn implements ActionListener {

	protected final JPopupMenu changeSizeMeasurementPopupMenu = new JPopupMenu();;

	protected TableLayoutUtil tableLayoutUtil;

	protected final static String[] STR_VALUE = { "ABSOLUTE", "PERCENTUAL", "MINIMUM", "FILL" };

	protected final static String[] STR_MENU_ITEMS = {  WYSIWYGStringsAndLabels.MEASUREMENT_DESCRIPTIONS.NAME_ABSOLUTE_SIZE,
			WYSIWYGStringsAndLabels.MEASUREMENT_DESCRIPTIONS.NAME_PERCENTUAL_SIZE,
			WYSIWYGStringsAndLabels.MEASUREMENT_DESCRIPTIONS.NAME_MINIMUM_SIZE, WYSIWYGStringsAndLabels.MEASUREMENT_DESCRIPTIONS.NAME_FILL };

	protected JMenuItem delete;

	protected void enablePreferredAndMinimum(boolean itemInColum) {
		JMenuItem temp;

		for (int i = 0; i < changeSizeMeasurementPopupMenu.getComponentCount(); i++) {
			Component c = changeSizeMeasurementPopupMenu.getComponent(i);
			if (c instanceof JMenuItem) {
				temp = (JMenuItem) changeSizeMeasurementPopupMenu.getComponent(i);
				if (itemInColum) {
					if (temp.getActionCommand().equals("MINIMUM")) {
						temp.setEnabled(true);
					}
				} else if (temp.getActionCommand().equals("MINIMUM")) {
					temp.setEnabled(false);
				}
			}
		}
	}

	private TableLayoutPanel getContainer() {
		return tableLayoutUtil.getContainer();
	}

	protected final void showPopupMenu() {
		Point loc;
		Point mousePosition = getContainer().getMousePosition();
		if (mousePosition != null) {
			loc = mousePosition.getLocation();
		} else {
			loc = getContainer().getLocation();
		}
		changeSizeMeasurementPopupMenu.show(getContainer(), loc.x, loc.y);
	}

}
