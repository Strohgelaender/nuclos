package org.nuclos.client.layout.wysiwyg.component.properties;

import java.awt.event.KeyEvent;

import javax.swing.*;

public class ChangeAwareDimensionListener extends ChangeAwareListener {
	
	public static int IHEIGHT = 0;
	public static int IWIDTH = 1;
	
	private int coordinate;

	public ChangeAwareDimensionListener(PropertiesPanel propertiespanel, int coordinate) {
		super(propertiespanel);
		this.coordinate = coordinate;
	}
	
	public int getCoordinate() {
		return this.coordinate;
	}
	
	
	@Override
	public void keyReleased(final KeyEvent e) {
		JTextField field = (JTextField) e.getComponent();
		String value = field.getText();
		value = value.trim();
		propertiesPanel.performSaveActionLiveMode(value, this);
	}

}
