package org.nuclos.client.layout;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.concurrent.Future;

import org.nuclos.api.ui.layout.TabController;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.layer.LockingResultListener;
import org.nuclos.common.Actions;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;

public class TabControllerImpl implements TabController {

	private final WeakReference<CollectController> controller;
	
	public TabControllerImpl(CollectController controller) {
		this.controller = new WeakReference<CollectController>(controller);
	}
	
	public CollectController getCollectController() {
		return this.controller.get();
	}
	
	public void refreshCurrentObject() throws CommonBusinessException {
		this.getCollectController().refreshCurrentCollectable(false);
	}
	
	public void runRule(String rule, LockingResultListener<Exception> exResult) {
		if (!SecurityCache.getInstance().isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER)) {
			return;
		}
		
		final EventSupportSourceVO ruleToExecute = EventSupportRepository.getInstance().getEventSupportByClassname(rule);
		if (ruleToExecute != null) {	
			this.getCollectController().executeBusinessRules(Collections.singletonList(ruleToExecute), exResult);
		}
	}
	
	public Future<LayerLock> lockTab() {
		return this.getCollectController().lockFrame();
	}
	
	public void unlockTab(Future<LayerLock> lock) {
		this.getCollectController().unLockFrame(lock);
	}

	@Override
	public void fireChangesArePending() {
		final CollectState state = getCollectController().getCollectState();
		if (state != null && CollectState.OUTERSTATE_DETAILS == state.getOuterState() && CollectState.DETAILSMODE_VIEW == state.getInnerState()) {
			try {
				getCollectController().setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_EDIT);
			} catch (CommonBusinessException e) {
				Errors.getInstance().showExceptionDialog(getCollectController().getTab(), e);
			}
		}
	}

}
