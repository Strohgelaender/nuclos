//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.admin;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Map;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.genericobject.CollectableGenericObjectEntity;
import org.nuclos.client.genericobject.GenericObjectLayoutCache;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.client.genericobject.valuelistprovider.GenericObjectCollectableFieldsProviderFactory;
import org.nuclos.client.layout.wysiwyg.CollectableWYSIWYGLayoutEditor.WYSIWYGDetailsComponentModel;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.layoutml.LayoutMLParser;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.fileimport.CommonParseException;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.xml.sax.InputSource;
/**
 * Controller for collecting the layouts for generic object dialogs.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class GenericObjectLayoutCollectController extends LayoutCollectController {

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public GenericObjectLayoutCollectController(MainFrameTab tabIfAny) {
		super(E.LAYOUT.getUID(), tabIfAny);
	}

	@Override
	protected CollectableMasterDataWithDependants insertCollectable(CollectableMasterDataWithDependants clctNew) throws CommonBusinessException {
		clctNew.setField(E.LAYOUT.layoutML.getUID(), new CollectableValueField(this.getLayoutMLFromEditor()));
		
		final CollectableMasterDataWithDependants result = super.insertCollectable(clctNew);

		GenericObjectLayoutCache.getInstance().invalidate();
		GenericObjectMetaDataCache.getInstance().revalidate();
		MasterDataDelegate.getInstance().invalidateLayoutCache();

		return result;
	}

	@Override
	protected CollectableMasterDataWithDependants updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oDependantData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		clct.setField(E.LAYOUT.layoutML.getUID(), new CollectableValueField(this.getLayoutMLFromEditor()));
		
		final CollectableMasterDataWithDependants result = super.updateCollectable(clct, oDependantData, applyMultiEditContext);

		GenericObjectLayoutCache.getInstance().invalidate();
		GenericObjectMetaDataCache.getInstance().revalidate();
		MasterDataDelegate.getInstance().invalidateLayoutCache();

		return result;
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		super.deleteCollectable(clct, applyMultiEditContext);	

		GenericObjectLayoutCache.getInstance().invalidate();
		GenericObjectMetaDataCache.getInstance().revalidate();
		MasterDataDelegate.getInstance().invalidateLayoutCache();
	}

	@Override
	protected LayoutRoot parseLayoutML() throws LayoutMLException {
		try {
			final String sLayoutML = this.getLayoutMLFromEditor();
			if (sLayoutML == null) {
				throw new LayoutMLException(getSpringLocaleDelegate().getMessage(
						"GenericObjectLayoutCollectController.1","Die LayoutML-Definition ist leer."));
			}
			final LayoutMLParser parser = new LayoutMLParser();
		
			final EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(getUsedEntityUID());
			final String label = getSpringLocaleDelegate().getLabelFromMetaDataVO(eMeta);
			final Map<UID, FieldMeta<?>> mapEfMeta = MetaProvider.getInstance().getAllEntityFieldsByEntity(eMeta.getUID());		
			
			if (eMeta.isStateModel()) {
				final CollectableGenericObjectEntity clcte = new CollectableGenericObjectEntity(eMeta.getUID(), label, mapEfMeta.keySet());
				return parser.getResult(getSelectedCollectableId(),
						new InputSource(new StringReader(sLayoutML)), clcte, false, false, null, GenericObjectCollectableFieldsProviderFactory.newFactory(clcte.getUID()), CollectableComponentFactory.getInstance());
			} else {
				final CollectableEntity clcte = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(this.getUsedEntityUID());
				return parser.getResult(getSelectedCollectableId(),
						new InputSource(new StringReader(sLayoutML)), clcte, false, false, null, MasterDataCollectableFieldsProviderFactory.newFactory(clcte.getUID()), CollectableComponentFactory.getInstance());
			}
		} catch (CommonParseException e) {
			throw new LayoutMLException(e, LayoutMLException.UNDEF);
		} catch (IOException e) {
			throw new LayoutMLException(e, LayoutMLException.UNDEF);
		} catch (CollectableFieldFormatException e) {
			throw new LayoutMLException(e, LayoutMLException.UNDEF);
		}
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the entity name the layout is used for, as specified in the layouts's usages. If there are several
	 * usages, the first one is taken.
	 * @throws CommonBusinessException if there are no usages defined for this layout.
	 */
	@Override
	protected UID getUsedEntityUID() throws CommonParseException {
		UID entityUID = null;
		
		if (this.getSelectedCollectableId() != null) {
			for (EntityObjectVO<?> mdvo : MasterDataDelegate.getInstance().getDependentDataCollection(E.LAYOUTUSAGE.getUID(), E.LAYOUTUSAGE.layout.getUID(), 
					null, this.getSelectedCollectableId())) {
				entityUID = mdvo.getFieldUid(E.LAYOUTUSAGE.entity);
				break;
			}
		}
		
		if(entityUID == null) {
			for (Collectable<?> clct : this.getSubFormController(E.LAYOUTUSAGE.getUID()).getCollectables()) {
				entityUID = (UID) clct.getValueId(E.LAYOUTUSAGE.entity.getUID());
			}
		}
		if(entityUID == null) {
			for (Collectable<?> clct : this.getSelectedCollectable().getDependants(E.LAYOUTUSAGE.layout)) {
				entityUID = (UID) clct.getValueId(E.LAYOUTUSAGE.entity.getUID());
			}
		}
		
		if (entityUID == null) {
			//NUCLEUSINT-261
			throw new CommonParseException(WYSIWYGStringsAndLabels.ERROR_MESSAGES.MISSING_ASSIGNMENT_MD);
		}

		assert entityUID != null;
		
		return entityUID;
	}

	@Override
	public void setMetaInformation(WYSIWYGDetailsComponentModel wysiwygModel) {
		try {
			CollectableEntity entity = NuclosCollectableEntityProvider.getInstance().getCollectableEntity(getUsedEntityUID());
			wysiwygModel.setCollectableEntity(entity);
			wysiwygModel.setLayoutUID(getSelectedCollectableId());
		} catch (CommonBusinessException e) {
			// do nothing
		}
	}

}	// class GenericObjectLayoutCollectController
