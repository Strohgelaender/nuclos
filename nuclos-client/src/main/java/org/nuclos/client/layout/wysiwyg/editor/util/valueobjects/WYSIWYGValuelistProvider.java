//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.util.valueobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import org.nuclos.common.NuclosValueListProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.layoutml.LayoutMLConstants;

/**
 * This class stores {@link LayoutMLConstants#ELEMENT_VALUELISTPROVIDER}.<br>
 * It may contain several {@link WYSIWYGParameter}.<br>
 *
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class WYSIWYGValuelistProvider implements Cloneable, Serializable, NuclosValueListProvider {

	private ValuelistProviderVO.Type type = null;
	private String name = "";
	private String value = "";
	
	private final boolean entityAndFieldAvailable;
	
	private UID entity = null;
	private UID field = null;

	/** this is where the Parameters are stored */
	private Vector<WYSIWYGParameter> parameter = null;

	/**
	 * Default Constructor
	 */
	public WYSIWYGValuelistProvider(boolean editEntityAndField){
		this.entityAndFieldAvailable = editEntityAndField;
		parameter = new Vector<WYSIWYGParameter>(1);
	}

	/**
	 * Constructor, setting {@link LayoutMLConstants#ATTRIBUTE_TYPE}
	 * @param type the Name of the {@link CollectableFieldsProvider}
	 */
	public WYSIWYGValuelistProvider(String name, ValuelistProviderVO.Type type, String value, boolean editEntityAndField){
		this(editEntityAndField);
		setName(name);
		setType(type);
		setValue(value);
	}

	/**
	 * @return the Name of the {@link CollectableFieldsProvider}
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * param name the Name of the {@link CollectableFieldsProvider}
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the Type of the {@link CollectableFieldsProvider}
	 */
	public ValuelistProviderVO.Type getType() {
		return type;
	}

	/**
	 * @param type the {@link CollectableFieldsProvider} to set as {@link LayoutMLConstants#ATTRIBUTE_TYPE}
	 */
	public void setType(ValuelistProviderVO.Type type) {
		this.type = type;
	}
	
	/**
	 * @return the value of the {@link CollectableFieldsProvider}
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * param value the value of the {@link CollectableFieldsProvider}
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public UID getEntity() {
		return entity;
	}

	public void setEntity(UID entity) {
		this.entity = entity;
	}

	public UID getField() {
		return field;
	}

	public void setField(UID field) {
		this.field = field;
	}

	public boolean isEntityAndFieldAvailable() {
		return entityAndFieldAvailable;
	}

	/**
	 * @param wysiwygParameter the {@link WYSIWYGParameter} to add
	 */
	public void addWYSIYWYGParameter(WYSIWYGParameter wysiwygParameter) {
		if (!parameter.contains(wysiwygParameter))
			this.parameter.add(wysiwygParameter);
	}

	/**
	 * @param wysiwygParameter the {@link WYSIWYGParameter} to remove from this {@link CollectableFieldsProvider}
	 */
	public void removeWYSIYWYGParameter(WYSIWYGParameter wysiwygParameter){
		this.parameter.remove(wysiwygParameter);
	}

	/**
	 * @return all {@link WYSIWYGParameter} set for this {@link WYSIWYGValuelistProvider}
	 */
	public Vector<WYSIWYGParameter> getAllWYSIYWYGParameter(){
		return this.parameter;
	}
	
	public List<WYSIWYGParameter> getAllWYSIYWYGParameterSorted(final boolean descend){
		List<WYSIWYGParameter> result = new ArrayList<WYSIWYGParameter>(getAllWYSIYWYGParameter());
		Collections.sort(result, (o1, o2) -> {
            if (o1 == null) {
                return descend?1:-1;
            }
            if (o2 == null) {
                return descend?-1:1;
            }
            return descend?o2.getParameterName().compareToIgnoreCase(o1.getParameterName())
                :o1.getParameterName().compareToIgnoreCase(o2.getParameterName());
        });
		return result;
	}

	/**
	 * Overwritten clone Method. Creating a new Instance of this {@link WYSIWYGValuelistProvider}<br>
	 * calls {@link WYSIWYGParameter#clone()}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		WYSIWYGValuelistProvider clonedValuelistProvider = new WYSIWYGValuelistProvider(this.isEntityAndFieldAvailable());

		if (this.getName() != null) {
			String clonedName = new String(this.getName());
			clonedValuelistProvider.setName(clonedName);
		}
		if (this.getType() != null) {
			clonedValuelistProvider.setType(this.getType());
		}
		if (this.getValue() != null) {
			String clonedValue = new String(this.getValue());
			clonedValuelistProvider.setValue(clonedValue);
		}
		if (this.isEntityAndFieldAvailable()) {
			clonedValuelistProvider.setEntity(this.getEntity());
			clonedValuelistProvider.setField(this.getField());
		}

		for (WYSIWYGParameter parameter : getAllWYSIYWYGParameter()) {
			clonedValuelistProvider.addWYSIYWYGParameter((WYSIWYGParameter)parameter.clone());
		}

		return clonedValuelistProvider;
	}

	@Override
	public void setParameter(String sName, Object oValue) {
		// ignore here
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// ignore here
		return null;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
