package org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event;

import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.LayoutMLRuleSingleRulePanel;

/**
 * Attach Rule 
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class RuleAttachEvent implements RuleListEvent {

	private final LayoutMLRuleSingleRulePanel rule;

	public RuleAttachEvent(final LayoutMLRuleSingleRulePanel rule) {
		super();
		this.rule = rule;
	}

	/**
	 * get rule
	 * 
	 * @return
	 */
	public LayoutMLRuleSingleRulePanel getRule() {
		return rule;
	}
	
	@Override
	public String toString() {
		return "attach rule " + getRule().toString();
	}
	
}
