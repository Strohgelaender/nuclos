package org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event;

import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.LayoutMLRuleSingleRulePanel;

/**
 * Remove Rule 
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class RuleRemoveEvent implements RuleListEvent {
	private final LayoutMLRuleSingleRulePanel rule;

	public RuleRemoveEvent(final LayoutMLRuleSingleRulePanel rule) {
		super();
		this.rule = rule;
	}

	/**
	 * get rule
	 * 
	 * @return
	 */
	public LayoutMLRuleSingleRulePanel getRule() {
		return rule;
	}

	@Override
	public String toString() {
		return "remove rule " + getRule().toString();
	}
}
