package org.nuclos.client.textmodule;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

import org.nuclos.client.ui.collect.component.TextModuleSupport;
import org.nuclos.common.textmodule.TextModule;
import org.nuclos.common2.SpringLocaleDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextModulePopupListener implements PopupMenuListener{

	private static Logger LOG = LoggerFactory.getLogger(TextModulePopupListener.class);
	public static class TextModuleMenuItem extends JMenuItem {
		private final TextModule tm;

		public TextModuleMenuItem(TextModule tm) {
			this.tm = tm;

		}

		public TextModule getTextModule() {
			return tm;
		}

	}

	private TextModuleSupport tmSupport;
	private JPopupMenu pm;


	public TextModulePopupListener(JPopupMenu pm, TextModuleSupport tmSupport) {
		this.tmSupport = tmSupport;
		this.pm = pm;
	}
	@Override
	public void popupMenuWillBecomeVisible(final PopupMenuEvent e) {

		final JTextComponent comp = tmSupport.getTargetTextComponent();
		List<TextModule>lstTextModules = TextModuleDelegate.getInstance().findTextModules(tmSupport.getTextModuleSettings());
		final List<JMenuItem> lstMenuItems = new ArrayList<JMenuItem>();
		for (final TextModule tm : lstTextModules) {
			final TextModuleMenuItem 	mi = new TextModuleMenuItem(tm);
			mi.setAction(new AbstractAction(tm.getLabel()) {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() instanceof TextModuleMenuItem) {

						TextModuleMenuItem mi = (TextModuleMenuItem) e.getSource();
						int position = (comp.getCaretPosition() > -1 ) ? comp.getCaretPosition() : 0;
						try {
							comp.getDocument().insertString(position, mi.getTextModule().getText(), null);
						} catch (BadLocationException ex) {
							LOG.error(ex.getMessage(), ex);
						}

						// Highlight inserted text
						comp.setSelectionStart(position);
						comp.setSelectionEnd(position + mi.getTextModule().getText().length());
					}
				}
			});
			lstMenuItems.add(mi);
		}	
		JComponent um = null;
		if (pm.getComponents().length > 0) {
			um = new JMenu(SpringLocaleDelegate.getInstance().getMessage("TextModulePopup.1","Textbausteine"));
			pm.add(um);
		} else {
			um = pm;
		}
		for (final JMenuItem mi : lstMenuItems) {
			um.add(mi);
		}
	}
	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void popupMenuCanceled(PopupMenuEvent e) {
		// TODO Auto-generated method stub
		
	}
}
