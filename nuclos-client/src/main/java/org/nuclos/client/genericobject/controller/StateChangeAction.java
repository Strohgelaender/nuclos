package org.nuclos.client.genericobject.controller;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.*;

import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.statemodel.StateWrapper;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class StateChangeAction extends AbstractAction {

	private MainFrameTab tab;
	private final GenericObjectCollectController ctrl;

	private final StateVO statevoSource;
	private final StateVO statevoTarget;

	public StateChangeAction(GenericObjectCollectController ctrl,
							 StateVO statevoSource, StateVO statevoTarget) {
		super(
				StringUtils.looksEmpty(statevoTarget.getButtonLabel(LocaleDelegate.getInstance().getLocale())) ? statevoTarget
								.getStatename(LocaleDelegate.getInstance().getLocale()) : SpringLocaleDelegate
						.getInstance().getResource(statevoTarget.getButtonLabel(LocaleDelegate.getInstance().getLocale()),
								statevoTarget.getStatename(LocaleDelegate.getInstance().getLocale())),
				statevoTarget.getButtonIcon() == null ? null
						: ResourceCache.getInstance().getIconResource(
						statevoTarget.getButtonIcon().getId()));
		this.ctrl = ctrl;
		this.tab = ctrl.getTab();

		this.statevoSource = statevoSource;
		this.statevoTarget = statevoTarget;

		putValue("Color", statevoTarget.getColor());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		StateWrapper swSource = null;
		if (statevoSource != null) {
			swSource = new StateWrapper(statevoSource.getId(),
					statevoSource.getNumeral(), SpringLocaleDelegate.getInstance()
					.getResource(
							StateDelegate
									.getInstance()
									.getStatemodelClosure(
											ctrl.getModuleId())
									.getResourceSIdForLabel(
											statevoSource.getId()),
							statevoSource.getStatename(LocaleDelegate.getInstance().getLocale())),
					statevoSource.getIcon(), SpringLocaleDelegate
					.getInstance().getResource(
							StateDelegate.getInstance()
									.getResourceSIdForDescription(
											statevoSource.getId()),
							statevoSource.getDescription(LocaleDelegate.getInstance().getLocale())),
					statevoSource.getColor(),
					statevoSource.getButtonIcon(),
					statevoSource.isFromAutomatic(),
					statevoSource.getNonStopSourceStates(),
					Collections.singletonList(statevoSource.getId()));
		}

		List<UID> newStates = new ArrayList<UID>(1);
		newStates.add(statevoTarget.getId());
		StateWrapper swTarget = new StateWrapper(statevoTarget.getId(),
				statevoTarget.getNumeral(), SpringLocaleDelegate.getInstance()
				.getResource(
						StateDelegate
								.getInstance()
								.getStatemodelClosure(
										ctrl.getModuleId())
								.getResourceSIdForLabel(
										statevoTarget.getId()),
						statevoTarget.getStatename(LocaleDelegate.getInstance().getLocale())),
				statevoTarget.getIcon(), SpringLocaleDelegate.getInstance()
				.getResource(
						StateDelegate.getInstance()
								.getResourceSIdForDescription(
										statevoTarget.getId()),
						statevoTarget.getDescription(LocaleDelegate.getInstance().getLocale())),
				statevoTarget.getColor(), statevoTarget.getButtonIcon(),
				statevoTarget.isFromAutomatic(), statevoTarget.getNonStopSourceStates(), newStates);
		ctrl.cmdChangeStates(tab, swSource, swTarget, swTarget.getStatesBefore());
	}

	public void setMainFrameTab(MainFrameTab tab) {
		this.tab = tab;
	}

	public StateVO getStateSource() {
		return statevoSource;
	}

	public StateVO getStateTarget() {
		return statevoTarget;
	}

}
