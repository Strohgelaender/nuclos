//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.swing.*;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.Utils;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.access.CgoWithDependantsSecurityAgentImpl;
import org.nuclos.client.main.Main;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.access.CefAllowAllSecurityAgentImpl;
import org.nuclos.common.collect.collectable.access.CefSecurityAgent;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.GeneralJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collect.collectable.searchcondition.ReferencingCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * Utility methods for leased objects, client specific.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class GenericObjectClientUtils {

	private GenericObjectClientUtils() {
	}

	/**
	 * TODO: For me, it is complete miracle why this is needed... (tp)
	 *
	 * @param instance Could be another entity than the entity the fields 
	 * 		belongs to, because it is a subform field display in the result panel.
	 * @param field to set the SecurityAgent on
	 * @param bFieldBelongsToSubEntity true if fields belongs to a subform
	 */
	public static <PK> void setSecurityAgent(final Collectable<PK> instance, final CollectableEntityField field, final boolean bFieldBelongsToSubEntity) {
		
		final CefSecurityAgent<PK> sa;
		if (instance instanceof CollectableGenericObject) {
			// If instance is not CollectableGenericObjectWithDependants
			// but only CollectableGenericObject we could not find the state of the GenericObject.
			// Hence it is impossible to get the rights. (tp)
			// 
			try {
				sa = new CgoWithDependantsSecurityAgentImpl<PK>((CollectableGenericObject) instance, field, bFieldBelongsToSubEntity);
			}
			catch (ClassCastException e) {
				throw new NuclosFatalException("Permission can only be deduced from CollectableGenericObjectWithDependants, not from "
						+ instance.getClass().getName() + ": " + instance);
			}
		}
		else {
			sa = new CefAllowAllSecurityAgentImpl<PK>();
		}
		field.setSecurityAgent(sa);
	}

	/**
	 * @param moduleUid
	 * @param clctcond the "official" search condition that is visible to the user.
	 * @return the internal version of the collectable search condition, that is used for performing the actual search.
	 * The search condition for "general search" is treated specially here.
	 */
	public static CollectableSearchCondition getInternalSearchCondition(UID moduleUid, CollectableSearchCondition clctcond) {
		return (moduleUid == null) ? getInternalSearchConditionForGeneralSearch(clctcond) : clctcond;
	}

	/**
	 * @param clctcond
	 * @return the internal search condition for the "general search" module. Attributes which have corresponding
	 * columns in subforms are treated specially.
	 */
	private static CollectableSearchCondition getInternalSearchConditionForGeneralSearch(CollectableSearchCondition clctcond) {
		return SearchConditionUtils.trueIfNull(clctcond).accept(new GetInternalSearchConditionVisitor());
	}

	/**
	 * inner class GetInternalSearchConditionVisitor
	 */
	private static class GetInternalSearchConditionVisitor implements Visitor<CollectableSearchCondition, RuntimeException> {

		@Override
		public CollectableSearchCondition visitTrueCondition(TrueCondition truecond) throws RuntimeException {
			return null;
		}

		@Override
		public CollectableSearchCondition visitAtomicCondition(AtomicCollectableSearchCondition atomiccond) throws RuntimeException {
			return atomiccond;
		}

		@Override
		public CollectableSearchCondition visitCompositeCondition(CompositeCollectableSearchCondition compositecond) throws RuntimeException {
			final List<CollectableSearchCondition> lstOperands = CollectionUtils.transform(compositecond.getOperands(), new Transformer<CollectableSearchCondition, CollectableSearchCondition>() {
				@Override
				public CollectableSearchCondition transform(CollectableSearchCondition cond) {
					return getInternalSearchConditionForGeneralSearch(cond);
				}
			});
			return new CompositeCollectableSearchCondition(compositecond.getLogicalOperator(), lstOperands);
		}

		@Override
		public CollectableSearchCondition visitIdCondition(CollectableIdCondition idcond) throws RuntimeException {
			return idcond;
		}

		@Override
		public CollectableSearchCondition visitSubCondition(CollectableSubCondition subcond) throws RuntimeException {
			return subcond;
		}

		@Override
		public CollectableSearchCondition visitRefJoinCondition(RefJoinCondition joincond) throws RuntimeException {
			return joincond;
		}

		@Override
		public CollectableSearchCondition visitGeneralJoinCondition(GeneralJoinCondition joincond) throws RuntimeException {
			return joincond;
		}

		@Override
		public CollectableSearchCondition visitReferencingCondition(ReferencingCollectableSearchCondition refcond) throws RuntimeException {
			throw new NotImplementedException("refcond");
		}

		@Override
        public CollectableSearchCondition visitIdListCondition(CollectableIdListCondition collectableIdListCondition) throws RuntimeException {
	        return collectableIdListCondition;
        }

		@Override
		public <T> CollectableSearchCondition visitInCondition(CollectableInCondition<T> collectableInCondition) throws RuntimeException {
			return collectableInCondition;
		}

	}	// inner class GetInternalSearchConditionVisitor

	/**
	 * Open a generic object of a certain module in the details view of a new GenericObjectCollectController
	 * @param moduleUid
	 * @param iGenericObjectId
	 * @throws CommonBusinessException
	 */
	public static void showDetails(UID moduleUid, Long iGenericObjectId) throws CommonBusinessException {
		if (!SecurityCache.getInstance().isReadAllowedForModule(moduleUid, iGenericObjectId)) {
			throw new CommonPermissionException(SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectclientUtils.1", "Sie haben nicht das Recht, dieses Objekt anzuzeigen."));
		}
		Main.getInstance().getMainController().showDetails(moduleUid, iGenericObjectId);
	}

	/**
	 * Open a generic object of a certain module in the details view of a new GenericObjectCollectController
	 * 
	 * @param parent
	 * @param moduleUid
	 * @throws CommonBusinessException
	 */
	public static <PK> CollectController<PK,? extends Collectable<PK>> showDetails(JComponent parent, UID moduleUid) throws CommonBusinessException {
		if (!SecurityCache.getInstance().isWriteAllowedForModule(moduleUid, null)) {
			throw new CommonPermissionException(SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectclientUtils.2", "Sie haben nicht das Recht, dieses Objekt anzulegen."));
		}
		return Main.getInstance().getMainController().<PK>showDetails(moduleUid);
	}

	/**
	 * Open a collection of generic objects of unknown and possibly different modules in the appropriate view of the appropriate CollectController.
	 * @param collLeasedObjectIds
	 * @throws CommonBusinessException
	 */
	public static void showDetails(Collection<Long> collLeasedObjectIds) throws CommonBusinessException {
		final Collection<UID> collAssetModuleIds = new HashSet<UID>(collLeasedObjectIds.size());
		for(Long iAssetId : collLeasedObjectIds) {
			collAssetModuleIds.add(GenericObjectDelegate.getInstance().getModuleContainingGenericObject(iAssetId));
		}
		
		final UID iCommonModuleId = Utils.getCommonObject(collAssetModuleIds);
		if(collLeasedObjectIds.size() == 1) {
			// One related object must have a defined module, so open it in its detail view of the GenericObjectCollectController
			final GenericObjectCollectController ctlLeasedObject = NuclosCollectControllerFactory.getInstance().newGenericObjectCollectController(iCommonModuleId, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			ctlLeasedObject.runViewSingleCollectableWithId(collLeasedObjectIds.iterator().next());
		}
		else if(iCommonModuleId != null) {
			// If more than one related objects share one module, open them in the result view of the GenericObjectCollectController
			final GenericObjectCollectController ctlLeasedObject = NuclosCollectControllerFactory.getInstance().newGenericObjectCollectController(iCommonModuleId, null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			ctlLeasedObject.runViewResults(getSearchConditionForRelatedObjects(collLeasedObjectIds));
		}
	}

	/**
	 * Build a search condition if there are more than one related objects
	 * @param collLeasedObjectIds
	 * @return OR condition over all related object ids
	 */
	private static CollectableSearchCondition getSearchConditionForRelatedObjects(Collection<Long> collLeasedObjectIds) {
		final CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.OR);
		for(Long iId : collLeasedObjectIds) {
			cond.addOperand(new CollectableIdCondition(iId));
		}
		return cond;
	}

}	// class GenericObjectClientUtils
