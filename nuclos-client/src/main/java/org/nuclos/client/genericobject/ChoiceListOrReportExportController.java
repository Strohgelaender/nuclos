//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.awt.*;

import javax.swing.*;

import org.nuclos.common.UID;

/**
 * Controller class for for choice panel (export of search list or of selected reports).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:dirk.funke@novabit.de">Dirk Funke</a>
 * @version 01.00.00
 */
public class ChoiceListOrReportExportController extends ReportFormatController {

	final ChoiceListOrReportExportPanel pnlChoiceExport;

	public ChoiceListOrReportExportController(UID entityUid, Component parent) {
		super(entityUid, parent);
		
		this.pnlChoiceExport = new ChoiceListOrReportExportPanel(entityUid, pnlFormat);
	}

	@Override
	public boolean run(String sDialogTitle) {
        JOptionPane pane = new JOptionPane(pnlChoiceExport, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, null, null);
        JDialog dialog = pane.createDialog(this.parent, sDialogTitle);
        dialog.setMinimumSize(new Dimension(400, dialog.getHeight()));
        dialog.setResizable(true);
        dialog.pack();
        dialog.setVisible(true);
        
		return ((pane.getValue() == null? -1: (Integer)pane.getValue()) == JOptionPane.OK_OPTION);
	}

}	// class ChoiceListOrReportExportController
