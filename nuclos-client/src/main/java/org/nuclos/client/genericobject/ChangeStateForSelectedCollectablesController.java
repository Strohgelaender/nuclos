package org.nuclos.client.genericobject;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.*;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.controller.StringifyHelper;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.statemodel.StateWrapper;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.UpdateSelectedIdsController;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.search.GenericObjectViaEntityObjectSearchStrategy;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.LoggerFactory;

/**
 * inner class ChangeStateForSelectedCollectablesController
 *
 * @deprecated Move to ResultController hierarchy.
 */
@Deprecated
class ChangeStateForSelectedCollectablesController
extends MultiCollectablesActionController<Long,Long, Object> {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ChangeStateForSelectedCollectablesController.class);

	private static class ChangeStateAction extends UpdateSelectedIdsController.UpdateAction<Long,CollectableGenericObjectWithDependants,GenericObjectCollectController> {
		private final GenericObjectCollectController ctl;
		private final StateWrapper stateFinal;
		private final List<UID> statesNew;

		private CommonBusinessException ex;

		ChangeStateAction(final GenericObjectCollectController ctl, StateWrapper stateFinal, List<UID> statesNew, final Set<Long> pks) throws CommonBusinessException {
			super(ctl, pks);
			this.ctl = ctl;
			this.stateFinal = stateFinal;
			this.statesNew = statesNew;
			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					ctl.stateChangeStartInit();
				}
			});
		}

		@Override
		public Object perform(Long pk, final Map<String, Serializable> applyMultiEditContext)
				throws CommonBusinessException {
			ctl.isProcessingStateChange.set(true);
			super.perform(pk, applyMultiEditContext);
			CollectableGenericObjectWithDependants clctlo = getCollectable(pk);
			final GenericObjectVO govo = clctlo.getGenericObjectCVO();
			boolean hasError = false;
			ChangeStateAction.this.ex = null;
			try {
				for (final UID stateNew : statesNew) {
					UIUtils.invokeOnDispatchThread(new Runnable() {
						@Override
						public void run() {
							try {
								ctl.invoke(new CommonRunnable() {
									@Override
									public void run() throws CommonBusinessException {
										final StateDelegate stateDelegate = StateDelegate.getInstance();
										if (LOG.isTraceEnabled()) {

											final StateVO stateVOCurrent = stateDelegate.getState(govo.getStatus());
											final StateVO stateVONew = stateDelegate.getState(stateNew);
											LOG.trace("change {} status from {} to {}",
													govo.getId(),
													StringifyHelper.toString(stateVOCurrent),
													StringifyHelper.toString(stateVONew));
										}
										stateDelegate.changeState(
												govo.getModule(), govo.getId(), stateNew,
												ctl.getCustomUsage());
										ctl.getResultController().replaceCollectableInTableModel(ctl.findCollectableById(govo.getModule(), govo.getId()));
									}
								}, applyMultiEditContext);
							} catch (CommonBusinessException e) {
								ChangeStateAction.this.ex = e;
							}
						}
					});
					if (ChangeStateAction.this.ex != null) {
						throw ChangeStateAction.this.ex;
					}
				}
			} catch (CommonBusinessException e) {
				hasError = true;
				throw e;
			} finally {
				final boolean bOk = !hasError;
				UIUtils.invokeOnDispatchThread(new Runnable() {
					@Override
					public void run() {
						if (bOk) {
							ctl.stateChangeFinishedOk();
						} else {
							ctl.stateChangeFinishedWithError();
						}
					}
				});
				ctl.isProcessingStateChange.set(false);
			}
			return null;
		}

		@Override
		public String getText(Long pk) {
			final CollectableGenericObjectWithDependants clctlo = getCollectable(pk);
			assert clctlo != null && ObjectUtils.equals(clctlo.getPrimaryKey(), pk);


			return SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectCollectController.86",
					"Statuswechsel f\u00fcr Datensatz {0}...",
					SpringLocaleDelegate.getInstance().getIdentifierLabel(clctlo, ctl.getEntityName(),
							MetaProvider.getInstance(), ctl.getLanguage()));
		}

		@Override
		public String getSuccessfulMessage(final Long pk,
				Object oResult) {
			final CollectableGenericObjectWithDependants clctlo = getCollectable(pk);
			assert clctlo != null && ObjectUtils.equals(clctlo.getPrimaryKey(), pk);
			return SpringLocaleDelegate
					.getInstance()
					.getMessage(
							"GenericObjectCollectController.87",
							"Statuswechsel f\u00fcr Datensatz {0} war erfolgreich.",
							SpringLocaleDelegate.getInstance().getIdentifierLabel(clctlo, ctl.getEntityName(),
									MetaProvider.getInstance(), ctl.getLanguage()));
		}

		@Override
		public String getConfirmStopMessage() {
			return SpringLocaleDelegate
					.getInstance()
					.getMessage(
							"GenericObjectCollectController.101",
							"Wollen Sie den Statuswechsel f\u00fcr die ausgew\u00e4hlten Datens\u00e4tze an dieser Stelle beenden?\n(Die bisher ge\u00e4nderten Datens\u00e4tze bleiben in jedem Fall ge\u00e4ndert.)");
		}

		@Override
		public String getExceptionMessage(final Long pk, Exception ex) {
			final CollectableGenericObjectWithDependants clctlo = getCollectable(pk);
			assert clctlo != null && ObjectUtils.equals(clctlo.getPrimaryKey(), pk);
			final SpringLocaleDelegate sld = SpringLocaleDelegate.getInstance();
			final String message;
			if (ex instanceof NuclosBusinessRuleException) {
				final NuclosBusinessRuleException nbre = (NuclosBusinessRuleException) ex;
				final BusinessException be = nbre.getCausingBusinessException();
				if (be != null) {
					message = be.getMessage();
				} else {
					message = nbre.getMessage();
				}
			} else if (ex instanceof RuntimeException) {
				final RuntimeException re = (RuntimeException) ex;
				if (re.getCause() != null && re.getCause() instanceof InvocationTargetException) {
					InvocationTargetException ie = (InvocationTargetException) re.getCause();
					if (ie.getCause() != null && ie.getCause() instanceof CommonFatalException) {
						CommonFatalException ce = (CommonFatalException) ie.getCause();
						if (ce.getCause() != null && ce.getCause() instanceof NuclosFatalException) {
							NuclosFatalException fe = (NuclosFatalException) ce.getCause();
							if (fe.getCause() != null && fe.getCause() instanceof CommonValidationException) {
								CommonValidationException cve = (CommonValidationException) fe.getCause();
								message = StringUtils.removeHtml(cve.getFullMessage());
							} else {
								message = fe.getMessage();
							}
						} else {
							message = ce.getMessage();
						}
					} else {
						message = ie.getMessage();
					}
				} else {
					message = re.getMessage();
				}
			} else {
				message = ex.getMessage();
			}
			return sld.getMessage(
					"GenericObjectCollectController.89",
					"Statuswechsel ist fehlgeschlagen f\u00fcr Datensatz {0}. {1}",
					sld.getIdentifierLabel(clctlo, ctl.getEntityName(),
							MetaProvider.getInstance(), ctl.getLanguage()), message);
		}

		@Override
		public void executeFinalAction() throws CommonBusinessException {
			super.executeFinalAction();

			/*
			 * Explicitly include the IDs of the changed collectables in the search as they may not match
			 * the search condition anymore after the state change.
			 */
			if (ctl.getSearchStrategy().getCollectableSearchCondition() != null) {
				searchWithAdditionalIds(pks);
			}
			selectResultsById(pks);

			ctl.updateStatesForMultiEdit();
		}

		/**
		 * Selects rows in the result table based on the given entity IDs.
		 *
		 * @param ids
		 */
		private void selectResultsById(final Set<Long> ids) {
			SortableCollectableTableModel<Long, CollectableGenericObjectWithDependants> resultTableModel = ctl.getResultTableModel();
			ListSelectionModel selectionModel = ctl.getResultTable().getSelectionModel();
			for (int i=0, rowCount=resultTableModel.getRowCount(); i<rowCount; i++) {
				CollectableGenericObjectWithDependants clct = resultTableModel.getRow(i);
				for (Long pk: ids) {
					if (pk.equals(clct.getPrimaryKey())) {
						selectionModel.addSelectionInterval(i, i);
						break;
					}
				}
			}
		}

		/**
		 * Includes additional IDs in the original search via OR-condition.
		 *
		 * @param ids
		 * @throws CommonBusinessException
		 */
		private void searchWithAdditionalIds(final Set<Long> ids) throws CommonBusinessException {
			GenericObjectViaEntityObjectSearchStrategy newSearchStrategy = new GenericObjectViaEntityObjectSearchStrategy(ctl.getCollectableEntity()) {
				@Override
				public CollectableSearchCondition getCollectableSearchCondition() throws CollectableFieldFormatException {
					CompositeCollectableSearchCondition newSearch = new CompositeCollectableSearchCondition(LogicalOperator.OR);
					newSearch.addOperand(ctl.getSearchStrategy().getCollectableSearchCondition());
					newSearch.addOperand(new CollectableIdListCondition(ids));
					return newSearch;
				}
			};
			newSearchStrategy.setCollectController(ctl);
			newSearchStrategy.search(true);
		}
	}

	public ChangeStateForSelectedCollectablesController(GenericObjectCollectController ctl, StateWrapper stateNew, final List<UID> statesNew, final Set<Long> pks) throws CommonBusinessException {
		super(ctl,
		SpringLocaleDelegate.getInstance().getMessage("GenericObjectCollectController.88","Statuswechsel in Status \"{0}\"",
		stateNew.getStatusText()),
		new ChangeStateAction(ctl, stateNew, statesNew,pks),
		pks);

	}

} // class ChangeStateForSelectedCollectablesController
