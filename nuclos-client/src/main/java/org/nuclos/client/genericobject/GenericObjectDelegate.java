//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.GenericObjectMetaDataVO;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.server.attribute.BadGenericObjectException;
import org.nuclos.server.common.NuclosUpdateException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.genericobject.valueobject.LogbookVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

/**
 * Business Delegate for <code>GenericObjectFacadeBean</code> and <code>GeneratorFacadeBean</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
// @Component
public class GenericObjectDelegate {
	
	private static final Logger LOG = Logger.getLogger(GenericObjectDelegate.class);
	
	private static GenericObjectDelegate INSTANCE;

	// 
	
	private GenericObjectFacadeRemote gofacade;

	private Map<UID, UID> resourceMapCache;

	/**
	 * Use getInstance() to get the one and only instance of this class.
	 */
	GenericObjectDelegate() {
		INSTANCE = this;
	}
	
	public static GenericObjectDelegate getInstance() {
		if (INSTANCE == null) {
			// lazy support
			INSTANCE = SpringApplicationContextHolder.getBean(GenericObjectDelegate.class);			
		}
		return INSTANCE;
	}
	
	// @Autowired
	public final void setGenericObjectFacadeRemote(GenericObjectFacadeRemote genericObjectFacadeRemote) {
		this.gofacade = genericObjectFacadeRemote;
	}

	protected GenericObjectFacadeRemote getGenericObjectFacade() {
		return this.gofacade;
	}

	/**
	 * @return the leased object meta data
	 *
	 * @deprecated As GenericObjectMetaDataVO is deprecated.
	 */
	public synchronized GenericObjectMetaDataVO getMetaDataCVO() {
		try {
			return this.getGenericObjectFacade().getMetaData();
		}
		catch (RuntimeException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	/**
	 * gets all leased objects with the given module id, that match the given search condition.
	 * @param moduleUid May be <code>null</code>.
	 * @param clctcond May be <code>null</code>.
	 * @return list of leased object value objects including dependants, but excluding parent objects
	 */
	public List<GenericObjectWithDependantsVO> getCompleteGenericObjectsWithDependants(
			UID moduleUid, CollectableSearchCondition clctcond, Set<UID> stRequiredSubEntityUids, String customUsage) {
		try {
			LOG.debug("START getCompleteGenericObjectsWithDependants");
			return getGenericObjectFacade().getGenericObjectsWithDependants(
					moduleUid, new CollectableSearchExpression(clctcond), null, stRequiredSubEntityUids, customUsage);
		}
		catch (RuntimeException | CommonPermissionException ex) {
			throw new CommonFatalException(ex);
		}
		finally {
			LOG.debug("FINISHED getCompleteGenericObjectsWithDependants");
		}
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the generic object with the given id.
	 * @deprecated Use get(UID, Long) instead
	 */
	@Deprecated
	public GenericObjectVO get(Long iGenericObjectId) throws CommonFinderException, CommonPermissionException {
		try {
			return this.getGenericObjectFacade().get(null, iGenericObjectId);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * gets the leased objects with the given module id, that match the given search condition, respecting the given max row count.
	 * 
	 * §precondition iMaxRowCount &gt; 0
	 * §precondition stRequiredSubEntityNames != null
	 * §postcondition result != null
	 * §postcondition result.size() &lt;= iMaxRowCount
	 * 
	 * @param moduleUid May be null.
	 * @param clctexpr
	 * @param requiredSubEntityUids Set&lt;String&gt;
	 * @param iMaxRowCount the maximum number of rows to return
	 * @return List&lt;GenericObjectVO&gt;
	 */
	public TruncatableCollection<GenericObjectWithDependantsVO> getRestrictedNumberOfGenericObjects(UID moduleUid,
			CollectableSearchExpression clctexpr, Set<UID> requiredSubEntityUids,
			String customUsage, Long iMaxRowCount) {
		try {
			final TruncatableCollection<GenericObjectWithDependantsVO> result =
					this.getGenericObjectFacade().getRestrictedNumberOfGenericObjects(moduleUid,
							clctexpr, requiredSubEntityUids, customUsage, iMaxRowCount);
			assert result != null;
			assert result.size() <= iMaxRowCount;
			return result;
		}
		catch (RuntimeException | CommonPermissionException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * gets the leased object with the given id
	 * @param moduleUid (redundant, but needed for security)
	 * @param iGenericObjectId
	 * @return
	 * @throws CommonFinderException
	 */
	public GenericObjectVO get(UID moduleUid, Long iGenericObjectId) throws CommonBusinessException {
		try {
			LOG.debug("get start");
			final GenericObjectVO result = getGenericObjectFacade().get(moduleUid, iGenericObjectId);
			LOG.debug("get done");
			return result;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * gets the leased object with the given id
	 * @param entity
	 * @param iGenericObjectId
	 * @param customUsage
	 * @return
	 * @throws CommonFinderException
	 */
	public GenericObjectWithDependantsVO getWithDependants(UID entity, Long iGenericObjectId, String customUsage) throws CommonBusinessException {
		try {
			LOG.debug("get start");
			final GenericObjectWithDependantsVO result = getGenericObjectFacade().getWithDependants(entity, iGenericObjectId, customUsage);
			LOG.debug("get done");
			return result;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}
	
	/**
	 * gets the (historical) leased object with the given id at the historical date.
	 * 
	 * §precondition dateHistorical != null
	 * §postcondition result != null
	 * 
	 * @param iGenericObjectId
	 * @param dateHistorical
	 * @return the contents of the leased object at the given point in time.
	 * @throws CommonFinderException if the given object didn't exist at the given point in time.
	 */
	public GenericObjectWithDependantsVO getHistorical(Long iGenericObjectId, Date dateHistorical, String customUsage)
			throws CommonFinderException, CommonPermissionException {

		if (dateHistorical == null) {
			throw new NullArgumentException("dateHistorical");
		}
		try {
			final GenericObjectWithDependantsVO result = getGenericObjectFacade().getHistorical(iGenericObjectId, dateHistorical, customUsage);
			assert result != null;
			return result;
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * updates the given leased object along with its dependants.
	 * 
	 * §precondition lowdcvo.getId() != null
	 * 
	 * @param lowdcvo
	 * @return the updated leased object (from the server) along with its dependants.
	 * @throws NuclosFatalException if a fatal error occured
	 * @throws CommonPermissionException if we have no permission to update the object
	 * @throws CommonStaleVersionException if the object was changed in the meantime by another user
	 * @throws NuclosBusinessRuleException
	 */
	public GenericObjectWithDependantsVO update(GenericObjectWithDependantsVO lowdcvo, String customUsage, boolean isCollectiveProcessing) throws CommonBusinessException {
		if (lowdcvo.getId() == null) {
			throw new IllegalArgumentException("lowdcvo");
		}
		try {
			LOG.debug("update start");
			final GenericObjectWithDependantsVO result = this.getGenericObjectFacade().modify(lowdcvo.getModule(), lowdcvo, customUsage, isCollectiveProcessing);
			LOG.debug("update done");
			return result;
		}
		catch (RuntimeException ex) {
			final Throwable tCause = ex.getCause();
			if (tCause instanceof BadGenericObjectException) {
				throw (BadGenericObjectException) tCause;
			}
			// RuntimeException has the BAD habit to include its cause' message in its own message.
			// the default message of NuclosUpdateException is not always correct ("duplicate key").
			// cause of the exception will be added at Errors.java
			throw new NuclosUpdateException("GenericObjectDelegate.1", ex);
		}
	}

	/**
	 * creates the given leased object and its dependants.
	 * 
	 * §precondition lowdcvo.getId() == null
	 * §precondition mpDependants != null --&gt; for(m : mpDependants.values()) { m.getId() == null }
	 * §precondition stRequiredSubEntityNames != null
	 * 
	 * @param lowdcvo must have an empty (<code>null</code>) id.
	 * @return the updated leased object (from the server)
	 */
	public GenericObjectWithDependantsVO create(GenericObjectWithDependantsVO lowdcvo, Set<UID> requiredSubEntityUids, String customUsage) 
			throws CommonBusinessException {
		
		if (lowdcvo.getId() != null) {
			throw new IllegalArgumentException("lowdcvo");
		}
		MasterDataDelegate.checkDependantsAreNew(lowdcvo.getDependents());
		if (requiredSubEntityUids == null) {
			throw new NullArgumentException("stRequiredSubEntityNames");
		}
		debug(lowdcvo);
		try {
			LOG.debug("create start");
			final GenericObjectWithDependantsVO result = this.getGenericObjectFacade().create(lowdcvo, requiredSubEntityUids, customUsage);
			LOG.debug("create done");
			return result;
		}
		catch (CommonFatalException ex) {
			if (ex.getCause() != null) // CreateException
			{
				final Throwable tCause = ex.getCause().getCause();
				if (tCause instanceof BadGenericObjectException) {
					throw (BadGenericObjectException) tCause;
				}
			}
			// RuntimeException has the BAD habit to include its cause' message in its own message.
			// the default message of NuclosUpdateException is not always correct ("duplicate key").
			// cause of the exception will be added at Errors.java
			throw new CommonCreateException("GenericObjectDelegate.2", ex);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * creates the given leased object and its dependants.
	 * 
	 * §precondition lowdcvo.getId() == null
	 * §precondition mpDependants != null --&gt; for(m : mpDependants.values()) { m.getId() == null }
	 * 
	 * @param lowdcvo must have an empty (<code>null</code>) id.
	 * @return the updated leased object (from the server)
	 */
	public GenericObjectVO create(GenericObjectWithDependantsVO lowdcvo, String customUsage) throws CommonBusinessException {
		if (lowdcvo.getId() != null) {
			throw new IllegalArgumentException("lowdcvo");
		}
		MasterDataDelegate.checkDependantsAreNew(lowdcvo.getDependents());

		debug(lowdcvo);
		try {
			LOG.debug("create start");
			final GenericObjectVO result = this.getGenericObjectFacade().create(lowdcvo, customUsage);
			LOG.debug("create done");
			return result;
		}
		catch (CommonFatalException ex) {
			if (ex.getCause() != null) // CreateException
			{
				final Throwable tCause = ex.getCause().getCause();
				if (tCause instanceof BadGenericObjectException) {
					throw (BadGenericObjectException) tCause;
				}
			}
			// RuntimeException has the BAD habit to include its cause' message in its own message.
			// the default message of NuclosUpdateException is not always correct ("duplicate key").
			/** @todo find a better solution */
			throw new CommonCreateException(SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectDelegate.2", "Der Datensatz konnte nicht erzeugt werden.") + "\n" + ex.getCause().getMessage(), ex);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}


	private void debug(GenericObjectVO govo) {
		/** @todo + GenericObjectVO.getDebugInfo() */
		final Collection<DynamicAttributeVO> collattrvo = govo.getAttributes();
		for (DynamicAttributeVO attrvo : collattrvo) {
			final StringBuffer sb = new StringBuffer();
			sb.append("name: " + AttributeCache.getInstance().getAttribute(attrvo.getAttributeUID()).getName());
			sb.append(" - value: " + attrvo.getValue() + " - valueId: " + attrvo.getValueId());
			if (attrvo.isRemoved()) {
				sb.append(" - REMOVED");
			}
			LOG.debug(sb);
		}
	}

	/**
	 * removes the given leased object.
	 * @param gowdvo
	 * @param bDeletePhysically Remove from DB?
	 * @throws CommonPermissionException if we have no permission to remove the object
	 */
	public void remove(GenericObjectWithDependantsVO gowdvo, boolean bDeletePhysically, String customUsage) throws CommonBusinessException {
		try {
			getGenericObjectFacade().remove(gowdvo.getModule(), gowdvo.getPrimaryKey(), bDeletePhysically, customUsage);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * restore the given generic object.
	 * @param gowdvo
	 * @throws CommonPermissionException if we have no permission to restore the object
	 */
	public void restore(GenericObjectWithDependantsVO gowdvo, String customUsage) throws CommonBusinessException {
		try {
			getGenericObjectFacade().restore(gowdvo.getModule(), gowdvo.getId(), customUsage);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public void relate(Long iGenericObjectIdSource, String relationType, Long iGenericObjectIdTarget, UID iModuleIdTarget,
			Date dateValidFrom, Date dateValidUntil, String sDescription)
			throws CommonFinderException, CommonCreateException, CommonPermissionException, NuclosBusinessRuleException {
		try {
			getGenericObjectFacade().relate(iModuleIdTarget, iGenericObjectIdTarget,
					iGenericObjectIdSource, relationType, dateValidFrom, dateValidUntil, sDescription);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * removes the relation with the given id.
	 * @param iRelationId
	 * @param iGenericObjectIdTarget
	 * @param iModuleIdTarget
	 * @throws CommonFinderException
	 * @throws CommonRemoveException
	 */
	public void removeRelation(Long iRelationId, Long iGenericObjectIdTarget, UID iModuleIdTarget)
			throws CommonBusinessException {
		try {
			getGenericObjectFacade().removeRelation(iRelationId, iGenericObjectIdTarget, iModuleIdTarget);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * removes the relations with the given ids found in the map.
	 * @param mpGOTreeNodeRelation
	 * @throws CommonFinderException
	 * @throws CommonRemoveException
	 * @throws CommonPermissionException
	 */
	public void removeRelation(Map<Long, GenericObjectTreeNode> mpGOTreeNodeRelation)
			throws CommonBusinessException {
		try {
			getGenericObjectFacade().removeRelation(mpGOTreeNodeRelation);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * gets the logbook for the leased object with the given id.
	 * 
	 * @param iGenericObjectId
	 * @return Collection&lt;LogbookVO&gt;
	 * @throws CommonFinderException
	 */
	public Collection<LogbookVO> getLogbook(Long iGenericObjectId) throws CommonFinderException, CommonPermissionException {
		try {
			return getGenericObjectFacade().getLogbook(iGenericObjectId, null);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @param iGenericObjectId
	 * @return the id of the module containing the object with the given id.
	 * @throws CommonFinderException if there is no generic object with the given id.
	 */
	public UID getModuleContainingGenericObject(Long iGenericObjectId) throws CommonFinderException {
		try {
			return getGenericObjectFacade().getModuleContainingGenericObject(iGenericObjectId);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * @param iGenericObjectId
	 * @return the id of the state of the generic object with the given id
	 */
	public UID getStateIdByGenericObject(Long iGenericObjectId) {
		try {
			return getGenericObjectFacade().getStateByGenericObject(iGenericObjectId);
		}
		catch(CommonFinderException ex) {
			throw new CommonFatalException(ex);
		}
		catch(RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * attach a document to a generic object
 	 */
	public void attachDocumentToObject(MasterDataVO<Long> mdvoDocument, UID parentAttribute) throws CommonBusinessException {
		try {
			getGenericObjectFacade().attachDocumentToObject(mdvoDocument, parentAttribute);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public void executeBusinessRules(List<EventSupportSourceVO>lstRuleVO, GenericObjectVO govo, String customUsage) throws CommonBusinessException {
		try {
			EntityObjectDelegate.getInstance().executeBusinessRules(lstRuleVO, DalSupportForGO.wrapGenericObjectVO(govo), customUsage, false);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	public Map<UID, UID> getResourceMap() {
		try {
			return getResourceMapCache();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	private Map<UID, UID> getResourceMapCache() throws RuntimeException {
		if(resourceMapCache == null) {
			LOG.debug("Initializing resourceMap cache");
			resourceMapCache = getGenericObjectFacade().getResourceMap();
		}
		return resourceMapCache;
	}

	public void invalidateCaches() {
		LOG.debug("Invalidating resourceMap cache.");
		resourceMapCache = null;
	}
	
	public Long countGenericObjectRows(UID iModuleId, final CollectableSearchExpression clctexpr) {
		try {
			return getGenericObjectFacade().countGenericObjectRows(iModuleId, clctexpr);
		}
		catch (RuntimeException | CommonPermissionException ex) {
			throw new CommonFatalException(ex);
		}
	}

}	// class GenericObjectDelegate
