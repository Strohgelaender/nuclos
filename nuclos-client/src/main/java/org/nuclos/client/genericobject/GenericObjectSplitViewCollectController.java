package org.nuclos.client.genericobject;

import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;

public class GenericObjectSplitViewCollectController extends GenericObjectCollectController {
	
	public GenericObjectSplitViewCollectController(UID moduleUid, boolean bAutoInit, MainFrameTab tabIfAny, String customUsage) {
		super(moduleUid, bAutoInit, tabIfAny, customUsage, ControllerPresentation.SPLIT_RESULT);
		
		GenericObjectCollectController detailController = CollectControllerFactorySingleton.getInstance().newGenericObjectCollectController(moduleUid, true, new MainFrameTab(), customUsage, ControllerPresentation.SPLIT_DETAIL);
		setSplitDetailController(detailController);
	}
	
	@Override
	public void init() {
		super.init();
		initSplitView();
	}
	
}
