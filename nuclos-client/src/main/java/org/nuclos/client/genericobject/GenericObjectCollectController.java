//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.FocusManager;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.RowSorter.SortKey;
import javax.swing.SwingWorker;
import javax.swing.TransferHandler;
import javax.swing.event.ChangeEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.command.CommonClientWorkerAdapter;
import org.nuclos.client.command.OvOpAdapter;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.KeyBindingProvider;
import org.nuclos.client.common.LafParameterProvider;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MatrixController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.MultiUpdateOfDependants;
import org.nuclos.client.common.NuclosCollectController;
import org.nuclos.client.common.NuclosCollectableStateComboBox;
import org.nuclos.client.common.NuclosFocusTraversalPolicy;
import org.nuclos.client.common.SearchConditionSubFormController;
import org.nuclos.client.common.SubFormController;
import org.nuclos.client.common.TableRowIndicator;
import org.nuclos.client.common.Utils;
import org.nuclos.client.common.controller.NuclosCollectControllerCommonState;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.entityobject.CollectableEOEntityClientProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.explorer.ExplorerController;
import org.nuclos.client.explorer.ExplorerDelegate;
import org.nuclos.client.genericobject.actionlisteners.ResetToTemplateUserActionListener;
import org.nuclos.client.genericobject.controller.GOUtils;
import org.nuclos.client.genericobject.controller.GenericObjectCollectableComponentsProvider;
import org.nuclos.client.genericobject.controller.GenericObjectDetailsPanel;
import org.nuclos.client.genericobject.controller.GenericObjectEditView;
import org.nuclos.client.genericobject.controller.GenericObjectResultPanel;
import org.nuclos.client.genericobject.controller.GetUserRulesProvider;
import org.nuclos.client.genericobject.controller.MyDetailsController;
import org.nuclos.client.genericobject.controller.NoSuchElementException;
import org.nuclos.client.genericobject.controller.RemovedTab;
import org.nuclos.client.genericobject.controller.StateChangeAction;
import org.nuclos.client.genericobject.controller.StringifyHelper;
import org.nuclos.client.genericobject.datatransfer.GenericObjectIdModuleProcess;
import org.nuclos.client.genericobject.datatransfer.TransferableGenericObjects;
import org.nuclos.client.genericobject.logbook.LogbookController;
import org.nuclos.client.genericobject.resulttemplate.SearchResultTemplate;
import org.nuclos.client.genericobject.statehistory.StateHistoryController;
import org.nuclos.client.genericobject.valuelistprovider.GenericObjectCollectableFieldsProviderFactory;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.CollectableWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.scripting.context.CollectControllerScriptContext;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.EntitySearchFilters;
import org.nuclos.client.searchfilter.SearchFilter;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.statemodel.StateViewComponent;
import org.nuclos.client.statemodel.StateWrapper;
import org.nuclos.client.ui.ColoredLabel;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.DateChooser;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.JInfoTabbedPane;
import org.nuclos.client.ui.OverlayOptionPane;
import org.nuclos.client.ui.SizeKnownEvent;
import org.nuclos.client.ui.SizeKnownListener;
import org.nuclos.client.ui.SplitPanePropertyChangeListener;
import org.nuclos.client.ui.TitledSeparator;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateConstants;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.CollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultEditView;
import org.nuclos.client.ui.collect.DeleteSelectedCollectablesController;
import org.nuclos.client.ui.collect.EditView;
import org.nuclos.client.ui.collect.SearchOrDetailsPanel;
import org.nuclos.client.ui.collect.UserCancelledException;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentTableCellEditor;
import org.nuclos.client.ui.collect.component.CollectableComponentWithValueListProvider;
import org.nuclos.client.ui.collect.component.CollectableDateChooser;
import org.nuclos.client.ui.collect.component.CollectableLocalizedComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelProvider;
import org.nuclos.client.ui.collect.component.model.CollectableLocalizedComponentModel;
import org.nuclos.client.ui.collect.component.model.DefaultDetailsEditModel;
import org.nuclos.client.ui.collect.component.model.DefaultSearchEditModel;
import org.nuclos.client.ui.collect.component.model.DetailsComponentModel;
import org.nuclos.client.ui.collect.component.model.DetailsEditModel;
import org.nuclos.client.ui.collect.component.model.DetailsLocalizedComponentModel;
import org.nuclos.client.ui.collect.component.model.EditModel;
import org.nuclos.client.ui.collect.component.model.SearchComponentModel;
import org.nuclos.client.ui.collect.component.model.SearchComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.SearchEditModel;
import org.nuclos.client.ui.collect.detail.DetailsController;
import org.nuclos.client.ui.collect.detail.DetailsPanel;
import org.nuclos.client.ui.collect.model.CollectableTableModel;
import org.nuclos.client.ui.collect.model.GenericObjectsResultTableModel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.collect.result.GenericObjectResultController;
import org.nuclos.client.ui.collect.result.NuclosSearchResultStrategy;
import org.nuclos.client.ui.collect.result.ResultActionCollection;
import org.nuclos.client.ui.collect.result.ResultActionCollection.ResultActionType;
import org.nuclos.client.ui.collect.result.ResultController;
import org.nuclos.client.ui.collect.result.ResultPanel;
import org.nuclos.client.ui.collect.search.ISearchStrategy;
import org.nuclos.client.ui.collect.search.SearchPanel;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.subform.SubForm.ParameterChangeListener;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.collect.subform.ToolbarFunctionState;
import org.nuclos.client.ui.collect.toolbar.DeleteOrRestoreToggleAction;
import org.nuclos.client.ui.collect.toolbar.DeleteOrRestoreToggleAction.Mode;
import org.nuclos.client.ui.collect.toolbar.NuclosToolBarActions;
import org.nuclos.client.ui.collect.toolbar.ToolBarItemAction;
import org.nuclos.client.ui.gc.ListenerUtil;
import org.nuclos.client.ui.labeled.LabeledComponent;
import org.nuclos.client.ui.layoutml.LayoutRoot;
import org.nuclos.client.ui.multiaction.MultiActionProgressLine;
import org.nuclos.client.ui.multiaction.MultiActionProgressPanel;
import org.nuclos.client.ui.multiaction.MultiActionProgressResultHandler;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.client.valuelistprovider.VLPClientUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.CollectableEntityFieldWithEntity;
import org.nuclos.common.DefaultComponentTypes;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.LafParameter;
import org.nuclos.common.NuclosAmbiguousMandatorException;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.caching.TimedCache;
import org.nuclos.common.collect.ControllerPresentation;
import org.nuclos.common.collect.DetailsPresentation;
import org.nuclos.common.collect.NuclosToolBarItems;
import org.nuclos.common.collect.ToolBarConfiguration;
import org.nuclos.common.collect.ToolBarItem;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.CollectableUtils;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableLikeCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collect.exception.CollectableFieldFormatException;
import org.nuclos.common.collect.exception.CollectableValidationException;
import org.nuclos.common.collection.BinaryPredicate;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.PredicateUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DataLanguageMap;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common.security.IPermission;
import org.nuclos.common.security.Permission;
import org.nuclos.common.statemodel.Statemodel;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonSortingException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.genericobject.valueobject.LogbookVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.EntitySearchResultTreeNode;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.resource.valueobject.ResourceVO;
import org.nuclos.server.statemodel.valueobject.MandatoryFieldVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

/**
 * Controller for collecting generic objects. Contains the necessary logic to
 * search for, view and edit generic objects. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class GenericObjectCollectController extends EntityCollectController<Long,CollectableGenericObjectWithDependants> {

	private static final Logger LOG = LoggerFactory.getLogger(GenericObjectCollectController.class);
	
	private final Logger logSecurity = LoggerFactory.getLogger(LOG.getName() + ".security");

	private static final String PREFS_KEY_FILTERNAME = "filterName";

	private static final String PREFS_KEY_SEARCHRESULTTEMPLATENAME = "searchResultTemplateName";

	private static final UID CALCULATED_ATTRS_WORKER_UID = new UID("calcAttrsWorker");

	private LabeledCollectableComponentWithVLP clctSearchState;

	private final Set<ReloadLayoutWorker> runningRefreshs = Collections.synchronizedSet(new HashSet<ReloadLayoutWorker>());

	private SplitPanePropertyChangeListener pclSplitPane;

	private CollectableGenericObjectWithDependants lowdCurrent = null;

	private final class ReloadLayoutWorker extends SwingWorker<Object, Object> {

		private final Runnable runnable;

		private ReloadLayoutWorker(Runnable runnable) {
			this.runnable = runnable;
		}

		@Override
		protected Object doInBackground() throws Exception {
			try {
				Thread.sleep(500);
			} catch (Exception ex) {
				// ignore.
			}
			return null;
		}

		@Override
		protected void done() {
			try {
				if (!isCancelled()) {
					UIUtils.runCommandLater(getTab(), runnable);
				}
			} finally {
				runningRefreshs.remove(this);
			}
		}
	}

	private void releasePreviousRefreshs() {
		synchronized (runningRefreshs) {
			for (ReloadLayoutWorker refresh : new HashSet<ReloadLayoutWorker>(
					runningRefreshs)) {
				if (refresh.isDone())
					continue;

				if (!refresh.isCancelled()) {
					if (!refresh.cancel(true)) {
						LOG.debug("Failed to cancel refresh for layout");
					}
				}
			}
		}
	};

	private final CollectableComponentModelListener ccmlistenerUsageCriteriaFieldsForDetails = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(
				CollectableComponentModelEvent ev) {
			if (ev.collectableFieldHasChanged()
					|| ev.getNewValue().getValue() == null) {
				final UID fieldUID = ev.getCollectableComponentModel().getFieldUID();
				LOG.debug("UsageCriteria field " + fieldUID
						+ " changed in Details panel.");

				if (isProcessField(fieldUID)) {
					setProcessId(ev.getNewValue() == null ? null : (UID) ev.getNewValue().getValueId());
				}

				releasePreviousRefreshs();

				ReloadLayoutWorker rvlr = new ReloadLayoutWorker(
						new Runnable() {
							@Override
							public void run() {
								try {
									// assert
									// getCollectStateModel().getOuterState() ==
									// CollectStateConstants.OUTERSTATE_DETAILS;
									if (getCollectStateModel().getOuterState() != CollectStateConstants.OUTERSTATE_DETAILS)
										return; // @see NUCLOS-708 assertation
												// error on delete object
												// physically. invoke later...

									GenericObjectCollectController.this
											.stopEditingInDetails();

									CollectableGenericObjectWithDependants selectedCollectable = GenericObjectCollectController.this
											.getSelectedCollectable();
									if (selectedCollectable != null
											&& selectedCollectable.getId() != null) {
										subFormsLoader
												.suspendRunningClientWorkers();
										subFormsLoader
												.addSuspendedClientWorker(CALCULATED_ATTRS_WORKER_UID); 
										// explicit mark for reloading of calc. attrs. for new layout
									}

									synchronized (this) {
										iCurrentLayoutId = null; 
										// reset current loaded layout
										// @see NUCLOS-1085
										reloadLayoutForDetailsTab(true);

										if (SF.PROCESS_UID.checkField(MetaProvider.getInstance().getEntityField(fieldUID).getFieldName()))
											showCustomActions(getCollectStateModel()
													.getDetailsMode());
									}
									if (selectedCollectable != null
											&& selectedCollectable.getId() != null) {
										subFormsLoader.startLoading();
										for (final SubFormController subformctl : GenericObjectCollectController.this
												.getSubFormControllersInDetails()) {
											// TODO try to eliminate this cast
											final MasterDataSubFormController mdsubformctl = (MasterDataSubFormController) subformctl;
											fillSubformMultithreaded(
													selectedCollectable,
													mdsubformctl);
										}
										subFormsLoader.resume();
									}

									UIUtils.runShortCommandLater(getTab(),
											new CommonRunnable() {
												@Override
												public void run()
														throws CommonBusinessException {
													Utils.setComponentFocus(
															fieldUID,
															getDetailsPanel()
																	.getEditView(),
															null, false);
												}
											});
								} catch (Exception ex) {
									Errors.getInstance()
											.showExceptionDialog(
													getTab(),
													getSpringLocaleDelegate()
															.getMessage(
																	"GenericObjectCollectController.15",
																	"Beim Nachladen eines Layouts ist ein Fehler aufgetreten."),
													ex);
								}
							}
						});

				if (checkMustReloadLayout()) {
					runningRefreshs.add(rvlr);
					rvlr.execute();
				}
			}
		}
	};

	private boolean isProcessField(UID fieldUID) {
		return fieldUID != null && fieldUID.equals(SF.PROCESS.getUID(this.entityUid));
	}

	private final CollectableComponentModelListener ccmlistenerUsageCriteriaFieldsForSearch = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(
				final CollectableComponentModelEvent ev) {
			if (ev.collectableFieldHasChanged()) {
				final UID fieldUID = ev.getCollectableComponentModel().getFieldUID();
				LOG.debug("UsageCriteria field " + fieldUID
						+ " changed in Search panel. New value: "
						+ ev.getNewValue());

				if (isProcessField(fieldUID)) {
					UID processId = (UID) ev.getCollectableComponentModel().getField().getValueId(); // getUsageCriteriaProcessIdFromView(true);
					setProcessId(processId);
				}

				releasePreviousRefreshs();

				ReloadLayoutWorker rvlr = new ReloadLayoutWorker(
						new Runnable() {
							@Override
							public void run() {
								try {
									reloadLayoutForSearchTab();

									Utils.setComponentFocus(fieldUID,
											getSearchPanel().getEditView(),
											null, false);
								} catch (Exception ex) {
									Errors.getInstance()
											.showExceptionDialog(
													getTab(),
													getSpringLocaleDelegate()
															.getMessage(
																	"GenericObjectCollectController.16",
																	"Beim Nachladen eines Layouts ist ein Fehler aufgetreten."),
													ex);
								}
							}
						});

				if (checkMustReloadLayout()) {
					runningRefreshs.add(rvlr);
					rvlr.execute();
				}
			}
		}

	};

	private final CollectableComponentModelListener ccmlistenerSearchChanged = new CollectableComponentModelAdapter() {
		@Override
		public void searchConditionChangedInModel(SearchComponentModelEvent ev) {

			// Note that we want to call "searchChanged()" on every change, not
			// only valid changes:
			GenericObjectCollectController.this.searchChanged(ev.getSearchComponentModel());
		}
	};

	private static int iFilter;


	private final UID moduleUid;

	private boolean bGenerated = false;
	private GeneratorActionVO oGeneratorAction;
	private Collection<Long> iGenericObjectIdSources;

	private final GenericObjectDelegate lodelegate = GenericObjectDelegate.getInstance();

	private final CollectableEntityField clctefHistoricalDate = new DefaultCollectableEntityField(
			new UID("historicalDate"), Date.class, null, null, null, null, true,
			CollectableField.TYPE_VALUEFIELD, null, null, new UID("historicalDate"),
			null);

	private final CollectableDateChooser clctdatechooserHistorical = new CollectableDateChooser(
			clctefHistoricalDate, false);

	protected final Action actDeletePhysicallyInDetails = NuclosToolBarActions.DELETE_REAL.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdDeletePhysically();
				}
			});
	protected final Action actDeletePhysicallyInResult = NuclosToolBarActions.MULTI_DELETE_REAL.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdDeleteSelectedCollectablesPhysically();
				}
			});

	protected final String[] itemsSearchDeleted = new String[] {
			getSpringLocaleDelegate().getMessage(
					"GenericObjectCollectController.59",
					"Nur ungel\u00f6schte suchen"),
			getSpringLocaleDelegate().getMessage(
					"GenericObjectCollectController.58",
					"Nur gel\u00f6schte suchen"),
			getSpringLocaleDelegate().getMessage(
					"GenericObjectCollectController.49",
					"Gel\u00f6schte und ungel\u00f6schte suchen") };

	// protected final JComboBox cmbbxSearchDeleted = new JComboBox();
	protected final Action actSearchDeleted = new AbstractAction() {
		@Override
		public void actionPerformed(ActionEvent e) {
			iSearchDeleted = e.getID();
			getResultController().getSearchResultStrategy().cmdRefreshResult();
		}
	};

	private final Action actMakeTreeRoot = NuclosToolBarActions.SHOW_IN_EXPLORER.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent ev) {
					cmdJumpToTree();
				}
			});
	
	protected final Action actShowStateHistory = NuclosToolBarActions.SHOW_STATE_HISTORY.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdShowStateHistory();
				}
			});
	
	// private JMenuItem btnShowHistory = new JMenuItem();
	protected final Action actShowHistory = NuclosToolBarActions.SHOW_HISTORY.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdShowHistory();
				}
			});
	
	private JMenuItem btnShowLogBook = new JMenuItem();
	
	private final DateChooser datechooserOldHistorical = clctdatechooserHistorical.getDateChooser();
	
	private final JComponent jOldHistoricalDatechooser = new ColoredLabel(
			datechooserOldHistorical, getSpringLocaleDelegate().getMessage(
					"GenericObjectCollectController.7",
					"Aktueller/historischer Zustand"));
	
	private JMenuItem btnResetViewToTemplateUser = new JMenuItem();
	
	protected final Action actExecuteRule = NuclosToolBarActions.EXECUTE_RULE.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdExecuteRuleByUser(GenericObjectCollectController.this
							.getTab(), GenericObjectCollectController.this
							.getEntityName(),
							GenericObjectCollectController.this
									.getSelectedCollectable());
				}
			});
	
	protected Action actShowResultInExplorer = NuclosToolBarActions.MULTI_SHOW_IN_EXPLORER.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdShowResultsInExplorer();
				}
			});

	// end of Menu Items

	private final List<StateWrapper> states = new ArrayList<StateWrapper>();
	protected final Action actChangeState = NuclosToolBarActions.CHANGE_STATE.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					StateWrapper selected = states.get(e.getID());
					actChangeState.putValue(
							ToolBarItemAction.ITEM_SELECTED_INDEX,
							states.indexOf(selected));
					cmdChangeState(selected);
				}
			});
	
	/**
	 * Non-final for close (tp)
	 */
	private StateViewComponent cmpStateStandardView = new StateViewComponent();
	
	private final List<Component> toolbarCustomActionsDetails = new ArrayList<Component>();
	
	private LayoutRoot<?> layoutrootDetails;

	/**
	 * Map<String sEntityName, DetailsSubFormController>
	 */
	private Map<UID, DetailsSubFormController<Long,CollectableEntityObject<Long>>> mpsubformctlDetails;

	/**
	 * Map<String sEntityName, SearchConditionSubFormController>
	 */

	private final CollectableComponentModelListener documentlistenerHistoricalDateChooser = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(
				CollectableComponentModelEvent ev) {
			if (!isNewHistoricalView())
				cmdHistoricalDateChanged();
		}
	};

	/**
	 * shortcut for performance. After update, the layout needn't be reloaded if
	 * the quintuple fields didn't change.
	 */
	protected boolean bReloadLayout = true;

	private boolean bInitialSearchLayout = false;

	/**
	 * avoids recursively calling reloadLayout
	 */
	private boolean bReloadingLayout = false;

	/**
	 * remember adding/removing quintuple field listeners
	 */
	private final boolean[] abUsageCriteriaFieldListenersAdded = new boolean[2];

	/**
	 * remember if current record is readable/writable
	 */
	private final Object lockCurrRecReadable = new Object();
	private Boolean blnCurrentRecordReadable = null;
	private final Object lockCurrRecWritable = new Object();
	private Boolean blnCurrentRecordWritable = null;

	private Integer iSearchDeleted = CollectableGenericObjectSearchExpression.SEARCH_UNDELETED;

	/**
	 * action: Delete selected Collectable (in Result panel)
	 */
	final Action actDeleteSelectedCollectablesPhysically = NuclosToolBarActions.MULTI_DELETE_REAL.init(new CommonAbstractAction() {

				@Override
				public void actionPerformed(ActionEvent ev) {
					cmdDeleteSelectedCollectablesPhysically();
				}
			});

	/**
	 * Delete/Restore Collectable 
	 */
	final DeleteOrRestoreToggleAction actDeleteSelectedCollectables = NuclosToolBarActions.MULTI_DELETE.init(new DeleteOrRestoreToggleAction() {

				@Override
				public void actionPerformed(ActionEvent ev) {
					if (DeleteOrRestoreToggleAction.Mode.RESTORE == getMode()) {
						cmdRestoreSelectedCollectables();
					} else {
						cmdDeleteSelectedCollectables();
					}
				}
			});


	private MultiUpdateOfDependants multiupdateofdependants;

	/**
	 * controller for search result templates
	 * 
	 * @deprecated Move to GenericObjectResultController.
	 */
	@Deprecated
	private SearchResultTemplateController searchResultTemplatesController;

	/**
	 * is current thread (multi-update?) processing a state change?
	 */
	final ThreadLocal<Boolean> isProcessingStateChange = new ThreadLocal<Boolean>() {
		@Override
		protected Boolean initialValue() {
			return Boolean.FALSE;
		}
	};

	private CollectableField process;
	private boolean calledByProcessMenu;

	/**
	 * Use the static method <code>newGenericObjectCollectController</code> to
	 * create new instances.
	 * 
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} to
	 * get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 * 
	 * @deprecated bAutoInit is deprecated
	 */
	@Deprecated
	public GenericObjectCollectController(UID moduleUid, boolean bAutoInit, MainFrameTab tabIfAny, String customUsage) {
		super(
				CollectableGenericObjectEntity.getByModuleUid(moduleUid),
				tabIfAny,
				newGoRc(moduleUid),
				customUsage, ControllerPresentation.DEFAULT);
		this.moduleUid = moduleUid;
		if (bAutoInit) {
			init();
		}
	}
	
	public GenericObjectCollectController(UID moduleUid, boolean bAutoInit, MainFrameTab tabIfAny, String customUsage, ControllerPresentation viewmode) {
		super(CollectableGenericObjectEntity.getByModuleUid(moduleUid),
				tabIfAny,
				newGoRc(moduleUid), 
				customUsage, viewmode);
		this.moduleUid = moduleUid;
		if (bAutoInit) {
			init();
		}
	}

	/**
	 * You should use
	 * {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} to
	 * get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	@Deprecated
	protected GenericObjectCollectController(UID pModuleUid, boolean bAutoInit,
			MainFrameTab tabIfAny, ResultController<Long,CollectableGenericObjectWithDependants> rc, String customUsage) {
		super(CollectableGenericObjectEntity.getByModuleUid(pModuleUid), tabIfAny, rc, customUsage, ControllerPresentation.DEFAULT);
		this.moduleUid = pModuleUid;
		if (bAutoInit) {
			init();
		}
	}
	
	private static GenericObjectResultController<CollectableGenericObjectWithDependants> newGoRc(UID moduleUid) {
		final NuclosCollectControllerCommonState state = new NuclosCollectControllerCommonState();
		return new GenericObjectResultController<>(CollectableGenericObjectEntity.getByModuleUid(moduleUid),
				new NuclosSearchResultStrategy<Long,CollectableGenericObjectWithDependants>(),
				state);
	}

	protected LabeledCollectableComponentWithVLP getSearchStateBox() {
		if (clctSearchState == null) {
			// "[status_num_plus_name]" - this is ugly. we have to think about that construct. @see NuclosCollectableStateComboBox.java
			final CollectableEntityField clctefSearchState = new DefaultCollectableEntityField(SF.STATE.getUID(getCollectableEntity().getUID()), String.class,
				getSpringLocaleDelegate().getLabelFromAttributeCVO(AttributeCache.getInstance().getAttribute(SF.STATE.getUID(getCollectableEntity().getUID()))),
				null, 255, null, true, CollectableField.TYPE_VALUEIDFIELD, null, null, getCollectableEntity().getUID(), "[status_num_plus_name]");
			clctSearchState = new NuclosCollectableStateComboBox(clctefSearchState, true);
		}
		return clctSearchState;
	}

	public final Integer getSearchDeleted() {
		return iSearchDeleted;
	}

	/**
	 * @deprecated Move to GenericObjectResultController.
	 */
	@Deprecated
	public final SearchResultTemplateController getSearchResultTemplateController() {
		return searchResultTemplatesController;
	}

	/**
	 * refactoring to avoid dirty use of SwingUtilities.invokeLater() allows
	 * local variable initialization of extenders [FS]
	 * 
	 * TODO: Make this protected again.
	 */
	@Override
	public void init() {
		try {
			_setCollectPanel(newCollectPanel(getModuleId()));
			super.init();
			
			final CollectPanel<Long, CollectableGenericObjectWithDependants> collectPanel = getCollectPanel();
			initialize(collectPanel);
	
			final MainFrameTab tab = getTab();
			tab.setLayeredComponent(collectPanel);
	
			setupEditPanels();
			setupKeyActionsForResultPanelVerticalScrollBar();
			setupShortcutsForTabs(tab);
			setupToolbars();
			
			initMatrixControllers();
	
			setupResultTableHeaderRenderer();
			setupDataTransfer();
			getCollectStateModel().addCollectStateListener(new GenericObjectCollectStateListener());
	
			setupResultContextMenuGeneration();
			setupResultContextMenuStates();
			setupResultContextExecuteMenuCustomRule();
			this.pclSplitPane = new SplitPanePropertyChangeListener(getDetailsPanel(), this);

		} catch (RuntimeException re) {
			super.errorDuringInit(re);
		}
	}

	private void setupToolbars() {
		actSearchDeleted.putValue(ToolBarItemAction.ITEM_ARRAY,
				itemsSearchDeleted);

		setupSearchToolBar(); // setup search for filter selection in result -> refs NOAINT-452
		if (this.isSearchPanelAvailable()) {
			setupResultToolBar();
		}
		_setupResultToolBar();
		_setupDetailsToolBar();
		refreshFastFilter();
	}

	@Override
	protected void setupSearchToolBar() {
		super.setupSearchToolBar();

		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_READ_DELETED_RECORD)) {
			this.getSearchPanel().registerToolBarAction(NuclosToolBarItems.SEARCH_DELETED, actSearchDeleted);
		}
		refreshSearchFilterView();
		
		final JComboBox jComboBox = getSearchStateBox().getJComboBox();
		jComboBox.setToolTipText(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.8", "Aktueller Status"));
		
		final ColoredLabel lbSearchBox = new ColoredLabel(jComboBox,
				getSpringLocaleDelegate().getMessage("GenericObjectCollectController.106", "Status"));
		lbSearchBox.setName("blSearchState");
		lbSearchBox.putClientProperty(ToolBarItem.COMPONENT_PROPERTY_KEY, NuclosToolBarItems.SEARCH_STATE);
		this.getSearchPanel().addToolBarComponent(lbSearchBox);

		setSearchStatesAccordingToUsageCriteria(new UsageCriteria(moduleUid,null, null, getCustomUsage()));

		ToolBarConfiguration configuration = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Search, moduleUid);
		this.getSearchPanel().setToolBarConfiguration(configuration);
	}

	/**
	 * sets up a modified renderer for the result table header that uses the
	 * HTML formatted toString() value of the column's entity field rather than
	 * its unformatted getLabel() value.
	 */
	private void setupResultTableHeaderRenderer() {
		final JTableHeader header = getResultTable().getTableHeader();
		final TableCellRenderer headerrendererDefault = header.getDefaultRenderer();

		header.setDefaultRenderer(new TableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable tbl,
					Object oValue, boolean bSelected, boolean bHasFocus,
					int iRow, int iColumn) {
			final CollectableEntityField f = GenericObjectCollectController.this
					.getResultTableModel().getCollectableEntityField(tbl.convertColumnIndexToModel(iColumn));
			final String value;
			if (f instanceof CollectableEOEntityField) {
				value = f.getLabel();
			} else {
				value = "";
			}
			return headerrendererDefault.getTableCellRendererComponent(tbl,value, bSelected, bHasFocus, iRow, iColumn);
			}
		});
	}

	@Override
	public void close() {
		getSubFormsLoader().interruptAllClientWorkers();

		closeSubFormControllersInSearch();
		closeSubFormControllers(getSubFormControllersInDetails());
		closeMatrixControllers(getMatrixControllers());

		toolbarCustomActionsDetails.clear();

		btnShowLogBook = null;
		btnResetViewToTemplateUser = null;
		layoutrootDetails = null;
		cmpStateStandardView = null;
		toolbarCustomActionsDetails.clear();
		
		if (mpsubformctlDetails != null) {
			for (DetailsSubFormController<Long, CollectableEntityObject<Long>> dsc: mpsubformctlDetails.values()) {
				dsc.close();
			}
			mpsubformctlDetails.clear();
		}
		closeRemovedTabs();

		super.close();
	}
	
	protected void resetLayout() {
		iCurrentLayoutId = null;
	}

	private void setupKeyActionsForResultPanelVerticalScrollBar() {
		// maps the default key strokes for JTable to set the vertical
		// scrollbar, so we can intervent:
		final InputMap inputmap = getResultTable().getInputMap(
				JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputmap.put(
				KeyStroke.getKeyStroke(KeyEvent.VK_END, InputEvent.CTRL_MASK),
				"last");
		inputmap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "nextrow");
		inputmap.put(KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0),
				"nextpage");

		final JScrollBar scrlbarVertical = getResultPanel()
				.getResultTableScrollPane().getVerticalScrollBar();
		final DefaultBoundedRangeModel model = (DefaultBoundedRangeModel) scrlbarVertical
				.getModel();

		getResultTable().getActionMap().put("last", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent ev) {
			final int iSupposedValue = model.getMaximum()
					- model.getExtent();
			model.setValue(iSupposedValue);
			// this causes the necessary rows to be loaded. Loading may be
			// cancelled by the user.
			LOG.debug("NOW it's time to select the row...");
			if (model.getValue() == iSupposedValue)
				getCollectNavigationModel().selectLastElement();
			}
		});

		getResultTable().getActionMap().put("nextrow", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent ev) {
			final int iSelectedRow = getResultTable().getSelectedRow();
			final int iLastVisibleRow = TableUtils.getLastVisibleRow(getResultTable());
			if (iSelectedRow + 1 < iLastVisibleRow) {
				// next row is still visible: just select it:
				if (!getCollectNavigationModel().isLastElementSelected())
					getCollectNavigationModel().selectNextElement();
			} else {
				// we have to move the viewport before we can select the
				// next row:
				final int iSupposedValue = Math.min(model.getValue()
						+ getResultTable().getRowHeight(),
						model.getMaximum() - model.getExtent());
				model.setValue(iSupposedValue);
				// this causes the necessary rows to be loaded. Loading may
				// be cancelled by the user.
				LOG.debug("NOW it's time to select the row...");
				if (model.getValue() == iSupposedValue)
					if (!getCollectNavigationModel().isLastElementSelected())
						getCollectNavigationModel().selectNextElement();
			}
			}
		});

		getResultTable().getActionMap().put("nextpage", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent ev) {
			final int iSupposedValue = Math.min(model.getValue() + model.getExtent(),
					model.getMaximum() - model.getExtent());
			model.setValue(iSupposedValue);
			// this causes the necessary rows to be loaded. Loading may be
			// cancelled by the user.
			LOG.debug("NOW it's time to select the row...");
			if (model.getValue() == iSupposedValue) {
				final int iShiftRowCount = (int) Math.ceil((double) model.getExtent()
						/ (double) getResultTable().getRowHeight());
				final int iRow = Math.min(getResultTable().getSelectionModel().getAnchorSelectionIndex()
						+ iShiftRowCount, getResultTable().getRowCount() - 1);
				getResultTable().setRowSelectionInterval(iRow, iRow);
			}
			}
		});

		final Action actShowHistory = new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent ev) {
			cmdShowHistory();
			getDetailsPanel().grabFocus();
			}
		};
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SHOW_HISTORY, actShowHistory, getDetailsPanel());

		final Action actShowLogBook = new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent ev) {
				cmdShowLogBook();
				getDetailsPanel().grabFocus();
			}
		};

		final String sActivateLogbook = ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_ACTIVATE_LOGBOOK);
		actShowLogBook.setEnabled(StringUtils.equalsIgnoreCase(sActivateLogbook, "true"));
		KeyBindingProvider.bindActionToComponent(
				KeyBindingProvider.SHOW_LOGBOOK, actShowLogBook,
				getDetailsPanel());

		final Action actShowStateHistory = new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent ev) {
				cmdShowStateHistory();
				getDetailsPanel().grabFocus();
			}
		};
		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.SHOW_STATE_HISTORY, actShowStateHistory,
				getDetailsPanel());

		KeyBindingProvider.bindActionToComponent(KeyBindingProvider.PRINT_LEASED_OBJECT,
				actPrintDetails, getDetailsPanel());
	}

	/**
	 * enables drag and copy from rows.
	 */
	private void setupDataTransfer() {
		setupDropListener(getResultTable());
		// enable drag&drop:
		prepareTableForDragAndDrop(getResultPanel().getResultTable());
		prepareTableForDragAndDrop((getResultPanel()).getFixedResultTable());
	}

	private void prepareTableForDragAndDrop(final JTable tbl) {
		tbl.setDragEnabled(true);
		tbl.setTransferHandler(new TransferHandler() {

			@Override
			public int getSourceActions(JComponent comp) {
				int result = NONE;
				if (comp == tbl)
					if (GenericObjectCollectController.this.getSelectedCollectable() != null)
						result = COPY;
				return result;
			}

			@Override
			protected Transferable createTransferable(JComponent comp) {
				Transferable result = null;
				if (comp == tbl) {
					final int[] aiSelectedRows = tbl.getSelectedRows();

					final List<GenericObjectIdModuleProcess> lstgoimp = new ArrayList<GenericObjectIdModuleProcess>(aiSelectedRows.length);
					for (int iSelectedRow : aiSelectedRows) {
						final CollectableGenericObjectWithDependants clct = getResultTableModel().getCollectable(iSelectedRow);
						GenericObjectIdModuleProcess transferableObject = getTransferableObject(tbl, iSelectedRow, clct);
						lstgoimp.add(transferableObject);
					}
					if (!lstgoimp.isEmpty())
						result = new TransferableGenericObjects(lstgoimp);
				}
				return result;
			}

			private GenericObjectIdModuleProcess getTransferableObject(final JTable tbl, int iSelectedRow,
					final CollectableGenericObjectWithDependants clct) {
				return new GenericObjectIdModuleProcess(clct, SpringLocaleDelegate.getInstance().getIdentifierLabel(
						clct, clct.getEntityUID(), MetaProvider.getInstance(), getLanguage()), getContents(tbl, iSelectedRow));
			}

			private String getContents(JTable tbl, int iRow) {
				final StringBuffer sb = new StringBuffer();
				final int iColumnCount = tbl.getColumnCount();

				for (int iColumn = 0; iColumn < iColumnCount; iColumn++) {
					final String sValue = tbl.getValueAt(iRow, iColumn).toString();
					if (sValue.indexOf("\n") >= 0 || sValue.indexOf("\t") >= 0) {
						sb.append("\"");
						sb.append(sValue.replaceAll("\"", "\"\""));
						sb.append("\"");
					} else {
						sb.append(sValue);
					}
					if (iColumn < iColumnCount - 1)
						sb.append("\t");
				}
				return sb.toString();
			}
		});
	}

	@Override
	protected void setHistoricalDate(Date dateHistorical) {
		super.setHistoricalDate(dateHistorical);

		updateHistoricalDateChooser(dateHistorical);
	}

	private CollectableDateChooser getHistoricalCollectableDateChooser() {
		return clctdatechooserHistorical;
	}

	private void updateHistoricalDateChooser(final Date date) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				synchronized (GenericObjectCollectController.this) {
					unregisterHistoricalDateChooserListener();
					clctdatechooserHistorical.getDateChooser().setDate(date);
					registerHistoricalDateChooserListener();
				}
			}
		});
	}

	private void registerHistoricalDateChooserListener() {
		clctdatechooserHistorical.getModel().addCollectableComponentModelListener(null, documentlistenerHistoricalDateChooser);
	}

	private void unregisterHistoricalDateChooserListener() {
		clctdatechooserHistorical.getModel().removeCollectableComponentModelListener(
						documentlistenerHistoricalDateChooser);
	}

	private synchronized void cmdHistoricalDateChanged() {
		boolean bDateValid;
		Date dateHistorical = null;
		try {
			dateHistorical = (Date) clctdatechooserHistorical.getField().getValue();
			if (dateHistorical == null
					&& getHistoricalCollectableDateChooser().getDateChooser().getDate() != null)
				dateHistorical = getHistoricalCollectableDateChooser().getDateChooser().getDate();
			bDateValid = true;
		} catch (CommonValidationException ex) {
			bDateValid = false;
		}

		if (bDateValid) {
			// if the historical date is greater than "today", set it to null,
			// in order to show the current object:
			if (dateHistorical != null)
				if (DateUtils.getPureDate(dateHistorical).compareTo(DateUtils.getPureDate(new Date())) >= 0)
					dateHistorical = null;

			if (!LangUtils.equal(getHistoricalDate(), dateHistorical)) {
				final Date dateLastValid = getHistoricalDate();
				setHistoricalDate(dateHistorical);

				UIUtils.runCommandLater(getTab(), new CommonRunnable() {
					@Override
					public void run() throws CommonBusinessException {
						final CollectableGenericObjectWithDependants clct;
						if (GenericObjectCollectController.this.getHistoricalDate() == null)
							clct = readSelectedCollectable();
						else {
							final GenericObjectWithDependantsVO lowdcvoHistorical;
							try {
								assert GenericObjectCollectController.this.getHistoricalDate() != null;
								lowdcvoHistorical = lodelegate.getHistorical(getSelectedGenericObjectId(),
										GenericObjectCollectController.this.getHistoricalDate(),
										getCustomUsage());

								// remember if the layout needs to be reloaded
								// afterwards:
								if (lowdcvoHistorical.getUsageCriteria(getCustomUsage())
										.equals(getUsageCriteria(getSelectedCollectable())))
									removeUsageCriteriaFieldListeners(false);

							} catch (CommonFinderException ex) {
								// "rollback":
								setHistoricalDate(dateLastValid);
								throw ex;
							}
							clct = new CollectableGenericObjectWithDependants(lowdcvoHistorical);
						}
						getResultController().replaceSelectedCollectableInTableModel(clct);
						cmdEnterViewMode();
						disableToolbarButtonsForHistoricalView();
					}
				});
			} else
				try {
					// check if we need to update the datechooser view:
					if (!LangUtils.equal(clctdatechooserHistorical.getDateChooser().getDate(), dateHistorical))
						updateHistoricalDateChooser(dateHistorical);
				} catch (CommonValidationException ex) {
					// Historical date chooser can contain only valid dates, as
					// it is only possible to choose them from the popup; so
					// this may not occur!
					throw new CommonFatalException(ex);
				}
		}
	}

	@Override
	protected boolean isSaveAllowed() {
		synchronized (lockCurrRecWritable) {
			blnCurrentRecordWritable = null;
		}
		return isCurrentRecordWritable(true) && isNotLoadingSubForms();
	}

	/**
	 * @return true
	 */
	@Override
	protected boolean isMultiEditAllowed() {
		return super.isMultiEditAllowed();
	}

	private boolean isCurrentRecordReadable() {
		CollectableGenericObjectWithDependants clctgowd = getSelectedCollectable();

		synchronized (lockCurrRecReadable) {
			if (blnCurrentRecordReadable == null)
				blnCurrentRecordReadable = this.isReadAllowed(clctgowd);
			return blnCurrentRecordReadable;
		}
	}

	protected boolean isCurrentRecordWritable() {
		return isCurrentRecordWritable(true);
	}
	
	protected boolean isCurrentRecordWritable(boolean checkWithRecordGrant) {
		if (isSelectedCollectableMarkedAsDeleted())
			return false;

		if(!MetaProvider.getInstance().getEntity(getEntityUid()).isEditable())
			return false;
		
		final UID iModuleIdSelected = getSelectedCollectableModuleId();
		final Long iGenericObjectId = getSelectedCollectableId();

		synchronized (lockCurrRecWritable) {
			if (blnCurrentRecordWritable == null)
				blnCurrentRecordWritable = SecurityCache.getInstance().isWriteAllowedForModule(iModuleIdSelected, iGenericObjectId);

			if (!blnCurrentRecordWritable) {
				LOG.debug("Speichern nicht erlaubt fuer das Modul " + getModuleLabel(iModuleIdSelected));
				return false;
			}
		}
		
		if (checkWithRecordGrant) {
			return canWriteFromRecordGrant(getSelectedGenericObjectCVO());
		}
		return true;
	}

	/**
	 * generic objects may deleted if we are not in historical view
	 * <code>AND</code> ((Delete is allowed for this module and the current user
	 * and the user has the right to write the object) <code>OR</code> this
	 * object is in its initial state and the current user created this object.)
	 * 
	 * @param clct
	 * @return Is the "Delete" action for the given Collectable allowed?
	 */
	@Override
	protected boolean isDeleteAllowed(CollectableGenericObjectWithDependants clct) {
		return clct != null && !this.isHistoricalView() && clct.getGenericObjectCVO().canDelete()
				&& MetaProvider.getInstance().getEntity(getEntityUid()).isEditable()
				&& hasCurrentUserDeletionRights(clct, false);
	}

	/**
	 * §precondition clct != null
	 * @return Is the "Read" action for the given Collectable allowed? May be
	 *         overridden by subclasses.
	 */
	@Override
	protected boolean isReadAllowed(CollectableGenericObjectWithDependants clct) {
		return SecurityCache.getInstance().isReadAllowedForModule(getEntityUid(), (clct == null) ? null : clct.getId());
	}

	/**
	 * §precondition clct != null
	 * @return Is the "Read" action for the given set of Collectables allowed?
	 *         May be overridden by subclasses.
	 */
	@Override
	protected boolean isReadAllowed(List<CollectableGenericObjectWithDependants> lsClct) {
		for (CollectableGenericObjectWithDependants collgowd : lsClct)
			if (!SecurityCache.getInstance().isReadAllowedForModule(getEntityUid(), collgowd.getId()))
				return false;
		return true;
	}

	private boolean hasCurrentUserDeletionRights(CollectableGenericObject clct, boolean physically) {
		boolean result = false;
		if (getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_RESULT)
			result = SecurityCache.getInstance().isDeleteAllowedForModule(this.getEntityUid(), null, physically);
		else
			result = SecurityCache.getInstance().isDeleteAllowedForModule(this.getEntityUid(), clct == null ? null : clct.getId(), physically);

		if (result)
			LOG.debug("isDeleteAllowed: delete allowed for module and permission is readwrite.");
		
		return result;
	}

	protected boolean isPhysicallyDeleteAllowed(CollectableGenericObjectWithDependants clct) {
		boolean result = SecurityCache.getInstance().isActionAllowed(Actions.ACTION_DELETE_RECORD);
		if (clct != null)
			result = result && isDeleteAllowed(clct) && hasCurrentUserDeletionRights(clct, true);
		return result;
	}

	@Override
	public void setSearchDeleted(Integer iSearchDeleted) {
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_READ_DELETED_RECORD)) {
			actSearchDeleted.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, iSearchDeleted);
		} else
			this.iSearchDeleted = CollectableGenericObjectSearchExpression.SEARCH_UNDELETED;
	}

	/**
	 * @deprecated Move to SearchController and make protected again.
	 */
	@Deprecated
	@Override
	public CollectableFieldsProviderFactory getCollectableFieldsProviderFactoryForSearchEditor() {
		return GenericObjectCollectableFieldsProviderFactory.newFactory(getCollectableEntity().getUID(), valueListProviderCache);
	}

	/**
	 * This method is called by <code>cmdClearSearchFields</code>, that is when
	 * the user clicks the "Clear Search Fields" button. This implementation
	 * selects the default search filter.
	 */
	@Override
	protected void clearSearchCondition() {
		super.clearSearchCondition();
		// select the default filter (the first entry):
		selectDefaultFilter();
	}

	/**
	 * @return the search filters that can be edited in this collect controller.
	 *         By default, it's the search filters for this collect controller's
	 *         module.
	 */
	@Override
	protected EntitySearchFilters getSearchFilters() {
		return EntitySearchFilters.forEntity(this.getEntityUid());
	}

	@Override
	protected EntitySearchFilter retrieveCurrentSearchFilterFromSearchPanel()
			throws CommonBusinessException {
		final EntitySearchFilter result = super.retrieveCurrentSearchFilterFromSearchPanel();
		result.getSearchFilterVO().setSearchDeleted(iSearchDeleted);

		// set selected columns:
		result.setVisibleColumns(getSelectedFields());

		/** @todo set sorting column names */

		return result;
	}

	private void _setupResultToolBar() {
		
		final GenericObjectResultPanel rp = getResultPanel();
		rp.registerToolBarAction(NuclosToolBarItems.INFO, getPointerAction());
		ToolBarConfiguration configuration = null;
		
		rp.registerToolBarAction(NuclosToolBarItems.MULTI_SHOW_IN_EXPLORER,
				actShowResultInExplorer);

		if (isPhysicallyDeleteAllowed(getSelectedCollectable())) {
			rp.registerToolBarAction(NuclosToolBarItems.MULTI_DELETE_REAL,
					actDeletePhysicallyInResult);
		}

		addAndSetupExportResultButton();

		actDeletePhysicallyInResult.setEnabled(true);

		searchResultTemplatesController = new SearchResultTemplateController(getResultPanel(), this);


		// adding reset to template user to toolbar in result view
		String templateUser = ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_TEMPLATE_USER);
		if (templateUser != null) {
			btnResetViewToTemplateUser.setText("Standardansicht wiederherstellen");
			btnResetViewToTemplateUser.addActionListener(new ResetToTemplateUserActionListener(this));
			rp.addPopupExtraMenuItem(btnResetViewToTemplateUser);
		}
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_READ_DELETED_RECORD)) {
			
			if (this.isSearchPanelAvailable()) {
				rp.registerToolBarAction(NuclosToolBarItems.SEARCH_DELETED, actSearchDeleted);
			}
		}
		configuration = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Tool_Bar_Result, getEntityUid());
		
		rp.setToolBarConfiguration(configuration);
	}

	/**
	 * Command: Delete selected <code>Collectable</code>s in the Result panel.
	 * This is mainly a copy of cmdDeleteSelectedCollectables from the
	 * ResultController, but with different messages and different actions.
	 */
	protected void cmdDeleteSelectedCollectablesPhysically() {
		assert getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_RESULT;
		assert CollectState.isResultModeSelected(getCollectStateModel().getResultMode());

		if (multipleCollectablesSelected()) {
			final int iCount = getResultTable().getSelectedRowCount();
			final String sMessage = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.81",
							"Sollen die ausgew\u00e4hlten {0} Datens\u00e4tze wirklich endg\u00fcltig gel\u00f6scht werden?\nDieser Vorgang kann nicht r\u00fcckg\u00e4ngig gemacht werden!",
							iCount);
			final int btn = JOptionPane.showConfirmDialog(getTab(),
					sMessage,
					getSpringLocaleDelegate().getMessage("GenericObjectCollectController.20",
							"Datens\u00e4tze endg\u00fcltig l\u00f6schen"),
					JOptionPane.YES_NO_OPTION);
			if (btn == JOptionPane.YES_OPTION)
				new DeleteSelectedCollectablesPhysicallyController(this).run(getMultiActionProgressPanel(iCount));
		} else {
			final String sMessage = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.76",
							"Soll der ausgew\u00e4hlte Datensatz ({0}) wirklich endg\u00fcltig gel\u00f6scht werden?\nDieser Vorgang kann nicht r\u00fcckg\u00e4ngig gemacht werden!",
							SpringLocaleDelegate.getInstance().getIdentifierLabel(
									getResultController().getSelectedCollectableFromTableModel(),getSelectedCollectable().getEntityUID(), MetaProvider.getInstance(),
									getLanguage()));
			final int btn = JOptionPane.showConfirmDialog(getTab(),
					sMessage, getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.23",
							"Datensatz endg\u00fcltig l\u00f6schen"),
					JOptionPane.YES_NO_OPTION);

			if (btn == JOptionPane.YES_OPTION)
				UIUtils.runCommand(getTab(), new Runnable() {
					@Override
					public void run() {
						try {
							checkedDeleteCollectablePhysically(getSelectedCollectable());
						} catch (CommonPermissionException ex) {
							final String sErrorMsg = "Sie verf\u00fcgen nicht \u00fcber die ausreichenden Rechte, um diesen Datensatz endg\u00fcltig zu l\u00f6schen.";
							Errors.getInstance().showExceptionDialog(getTab(), sErrorMsg, ex);
						} catch (CommonBusinessException ex) {
							Errors.getInstance() .showExceptionDialog(
											getTab(),
											getSpringLocaleDelegate().getMessage("GenericObjectCollectController.29",
															"Der Datensatz konnte nicht endg\u00fcltig gel\u00f6scht werden."),
											ex);
						}
					}
				});
		}
	}

	/**
	 * Command: Delete selected <code>Collectable</code>s in the Result panel.
	 */
	private void cmdDeleteSelectedCollectables() {
		assert getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_RESULT;
		assert CollectState.isResultModeSelected(getCollectStateModel()
				.getResultMode());

		if (multipleCollectablesSelected()) {
			final int iCount = getResultTable().getSelectedRowCount();
			final String sMessage = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.82",
							"Sollen die ausgew\u00e4hlten {0} Datens\u00e4tze wirklich gel\u00f6scht werden?",
							iCount);
			final int btn = JOptionPane.showConfirmDialog(getTab(),
					sMessage, getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.21",
							"Datens\u00e4tze l\u00f6schen"),
					JOptionPane.YES_NO_OPTION);
			if (btn == JOptionPane.YES_OPTION)
				new DeleteSelectedCollectablesController<Long,CollectableGenericObjectWithDependants>(this).run(getMultiActionProgressPanel(iCount));
			else if (btn == JOptionPane.NO_OPTION)
				getResultController().getDeleteSelectedCollectablesAction().putValue(Action.SELECTED_KEY, Boolean.FALSE);
		} else {
			final String sMessage = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.77",
							"Soll der ausgew\u00e4hlte Datensatz ({0}) wirklich gel\u00f6scht werden?",
							SpringLocaleDelegate.getInstance().getIdentifierLabel(
									getResultController().getSelectedCollectableFromTableModel(),
									getSelectedCollectable().getEntityUID(), MetaProvider.getInstance(), getLanguage()));
			final int btn = JOptionPane.showConfirmDialog(getTab(),
					sMessage,
					getSpringLocaleDelegate().getMessage("GenericObjectCollectController.25",
							"Datensatz l\u00f6schen"),
					JOptionPane.YES_NO_OPTION);

			if (btn == JOptionPane.YES_OPTION)
				UIUtils.runCommand(getTab(), new Runnable() {
					@Override
					public void run() {
						try {
							checkedDeleteSelectedCollectable();
							// refreshResult();
						} catch (CommonPermissionException ex) {
							final String sErrorMsg = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.68",
											"Sie verf\u00fcgen nicht \u00fcber die ausreichenden Rechte, um diesen Datensatz zu l\u00f6schen.");
							getResultController().getDeleteSelectedCollectablesAction()
									.putValue(Action.SELECTED_KEY, Boolean.FALSE);
							Errors.getInstance().showExceptionDialog(getTab(), sErrorMsg, ex);
						} catch (CommonBusinessException ex) {
							getResultController() .getDeleteSelectedCollectablesAction()
									.putValue(Action.SELECTED_KEY, Boolean.FALSE);
							final String msg = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.30",
									"Der Datensatz konnte nicht gel\u00f6scht werden.");
							Errors.getInstance().showExceptionDialog(getTab(), msg, new CommonRemoveException(msg, ex));
						}
					}
				});
			else if (btn == JOptionPane.NO_OPTION)
				// getResultPanel().btnDelete.setSelected(false);
				getResultController().getDeleteSelectedCollectablesAction().putValue(Action.SELECTED_KEY, Boolean.FALSE);
		}
	}

	/**
	 * @deprecated Move to ResultController hierarchy.
	 */
	@Deprecated
	private void cmdRestoreSelectedCollectables() {
		assert getCollectStateModel().getOuterState() == CollectState.OUTERSTATE_RESULT;
		assert CollectState.isResultModeSelected(getCollectStateModel().getResultMode());

		if (multipleCollectablesSelected()) {
			final int iCount = getResultTable().getSelectedRowCount();
			final String sMessage = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.83",
							"Sollen die ausgew\u00e4hlten {0} Datens\u00e4tze wirklich wiederhergestellt werden?",
							iCount);
			final int btn = JOptionPane.showConfirmDialog(getTab(),
					sMessage, getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.22",
							"Datens\u00e4tze wiederherstellen"),
					JOptionPane.YES_NO_OPTION);
			if (btn == JOptionPane.YES_OPTION)
				new RestoreSelectedCollectablesController(GenericObjectCollectController.this).run(getMultiActionProgressPanel(iCount));
			else if (btn == JOptionPane.NO_OPTION)
				// getResultPanel().btnDelete.setSelected(true);
				getResultController().getDeleteSelectedCollectablesAction().putValue(Action.SELECTED_KEY, Boolean.TRUE);
		} else {
			final String sMessage = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.78",
							"Soll der ausgew\u00e4hlte Datensatz ({0}) wirklich wiederhergestellt werden?",
							SpringLocaleDelegate.getInstance().getIdentifierLabel(
									getResultController().getSelectedCollectableFromTableModel(),
									getSelectedCollectable().getEntityUID(), MetaProvider.getInstance(), getLanguage()));
			final int btn = JOptionPane.showConfirmDialog(getTab(),
					sMessage, getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.27",
							"Datensatz wiederherstellen"),
					JOptionPane.YES_NO_OPTION);

			if (btn == JOptionPane.YES_OPTION)
				UIUtils.runCommand(getTab(), new Runnable() {
					@Override
					public void run() {
						try {
							checkedRestoreCollectable(getSelectedCollectable(), null);
							getResultController().getSearchResultStrategy()
									.refreshResult();
						} catch (CommonPermissionException ex) {
							final String sErrorMsg = getSpringLocaleDelegate().getMessage(
											"GenericObjectCollectController.66",
											"Sie verf\u00fcgen nicht \u00fcber die ausreichenden Rechte, um diesen Datensatz wiederherzustellen.");
							getResultController()
									.getDeleteSelectedCollectablesAction()
									.putValue(Action.SELECTED_KEY, Boolean.TRUE);
							Errors.getInstance().showExceptionDialog(getTab(),
									sErrorMsg, ex);
						} catch (CommonBusinessException ex) {
							getResultController()
									.getDeleteSelectedCollectablesAction()
									.putValue(Action.SELECTED_KEY, Boolean.TRUE);
							Errors.getInstance().showExceptionDialog(getTab(), getSpringLocaleDelegate().getMessage(
															"GenericObjectCollectController.32",
															"Der Datensatz konnte nicht wiederhergestellt werden."),
											ex);
						}
					}
				});
			else if (btn == JOptionPane.NO_OPTION)
				getResultController().getDeleteSelectedCollectablesAction().putValue(Action.SELECTED_KEY, Boolean.TRUE);
		}
	}

	/**
	 * @deprecated Move to ResultController hierarchy.
	 */
	@Deprecated
	private void cmdRestoreCurrentCollectableInDetails() {
		assert getCollectStateModel().getCollectState().equals(
				new CollectState(CollectState.OUTERSTATE_DETAILS,
						CollectState.DETAILSMODE_VIEW));

		if (stopEditingInDetails()) {
			final String sMessage = getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.75",
							"Soll der angezeigte Datensatz ({0}) wirklich wiederhergestellt werden?",
							SpringLocaleDelegate.getInstance().getIdentifierLabel(
									getResultController().getSelectedCollectableFromTableModel(),
									getSelectedCollectable().getEntityUID(), MetaProvider.getInstance(),getLanguage()));
			final int iBtn = JOptionPane.showConfirmDialog(getTab(), sMessage, getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.28",
							"Datensatz wiederherstellen"),
					JOptionPane.YES_NO_OPTION);

			if (iBtn == JOptionPane.OK_OPTION)
				UIUtils.runCommand(getTab(), new Runnable() {
					@Override
					public void run() {
						try {
							// try to find next or previous object:
							final JTable tblResult = getResultTable();
							final int iSelectedRow = tblResult.getSelectedRow();
							if (iSelectedRow < 0) throw new IllegalStateException();

							final int iNewSelectedRow = GOUtils.getNewSelectedRow(iSelectedRow, tblResult.getRowCount());

							checkedRestoreCollectable(getSelectedCollectable(), null);
							getResultTableModel().remove(getSelectedCollectable());
							getSearchStrategy().search(true);

							if (iNewSelectedRow == -1) {
								tblResult.clearSelection();
								// switch to new mode:
								getResultController().getSearchResultStrategy().refreshResult();
							} else {
								tblResult.setRowSelectionInterval(iNewSelectedRow, iNewSelectedRow);
								// go into view mode again:
								cmdEnterViewMode();
							}
						} catch (CommonPermissionException ex) {
							final String sErrorMessage = getSpringLocaleDelegate().getMessage(
											"GenericObjectCollectController.67",
											"Sie verf\u00fcgen nicht \u00fcber die ausreichenden Rechte, um diesen Datensatz wiederherzustellen.");
							Errors.getInstance().showExceptionDialog(getTab(), sErrorMessage, ex);
						} catch (CommonBusinessException ex) {
							Errors.getInstance().showExceptionDialog(
											getTab(),
											getSpringLocaleDelegate().getMessage(
															"GenericObjectCollectController.33",
															"Der Datensatz konnte nicht wiederhergestellt werden."),
											ex);
						}
					}
				});
		}
		setDeleteButtonToggleInDetails();
	}

	/**
	 * @deprecated Move to ResultController hierarchy.
	 * 
	 * @deprecated Move to a specialization of DetailsController and make
	 *             private again.
	 */
	@Deprecated
	@Override
	public void cmdDeleteCurrentCollectableInDetails() {
		assert getCollectStateModel().getCollectState().equals(new CollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_VIEW));

		if (stopEditingInDetails()) {
			final String sMessage = getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.74",
							"Soll der angezeigte Datensatz ({0}) wirklich gel\u00f6scht werden?",
							SpringLocaleDelegate.getInstance().getIdentifierLabel(
									getResultController().getSelectedCollectableFromTableModel(),
									getSelectedCollectable().getEntityUID(), MetaProvider.getInstance(), getLanguage()));
			final int iBtn = JOptionPane.showConfirmDialog(getTab(),
					sMessage,
					getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.26",
							"Datensatz l\u00f6schen"),
					JOptionPane.YES_NO_OPTION);

			if (iBtn == JOptionPane.OK_OPTION)
				UIUtils.runCommand(getTab(), new Runnable() {
					@Override
					public void run() {
						cmdDeleteCurrentCollectableInDetailsImpl();
					}
				});
		}
		setDeleteButtonToggleInDetails();
	}

	// Add Localization Button totoolbar we neceaary
	private void addLocalizationButton() {
		if (MetaProvider.getInstance().getEntity(getCollectableEntity().getUID()).IsLocalized()) {
			final Action actShowLocalization = NuclosToolBarActions.LOCALIZATION.init(new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdShowLocalization();
				}


			});
			actShowLocalization.setEnabled(true);
			this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.LOCALIZATION, actShowLocalization);
		}
	}

	/**
	 * @return the toolbar that contains the buttons in the fixed left part of
	 *         the custom toolbar area). Successors may add their own buttons
	 *         here and should call revalidate() afterwards.
	 */
	private void _setupDetailsToolBar() {
		final SecurityCache securitycache = SecurityCache.getInstance();

		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.SHOW_IN_EXPLORER, actMakeTreeRoot);

		if (securitycache.isActionAllowed(Actions.ACTION_DELETE_RECORD)) {
			this.getDetailsPanel().registerToolBarAction(
					NuclosToolBarItems.DELETE_REAL,
					actDeletePhysicallyInDetails);
		}

		// Execute rule by user only for authorized personnel
		if (securitycache.isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER)) {
			this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.EXECUTE_RULE, actExecuteRule);
			actExecuteRule.setEnabled(securitycache.isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER));
		}

		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_USE_INVALID_MASTERDATA)) {

			addButtonInvalidMasterdata();
			addLocalizationButton();
		}

		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.SHOW_STATE_HISTORY, actShowStateHistory);

		// add the history components only if loogbook tracking is enabled in
		// this module
		if (Modules.getInstance().isLogbookTracking(moduleUid)) {
			this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.SHOW_HISTORY, actShowHistory);
		}

		// add the loogbook and historical components only if loogbook tracking
		// is enabled in this module
		if (Modules.getInstance().isLogbookTracking(moduleUid)) {
			btnShowLogBook.setName("btnShowLogBook");
			btnShowLogBook.setIcon(Icons.getInstance().getIconLogBook16());
			btnShowLogBook.setText(getSpringLocaleDelegate().getMessage(
					"GenericObjectCollectController.54", "Logbuch anzeigen"));
			btnShowLogBook.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cmdShowLogBook();					
				}
			});

			this.getDetailsPanel().addPopupExtraMenuItem(btnShowLogBook);

			datechooserOldHistorical.setHistoricalState(true);
			this.getDetailsPanel().addPopupExtraComponent(
					jOldHistoricalDatechooser);
			registerHistoricalDateChooserListener();
		}

		actChangeState.putValue(ToolBarItemAction.MENU_LABEL,
				getSpringLocaleDelegate().getMessage("GenericObjectCollectController.106", "Status"));
		actChangeState.putValue(ToolBarItemAction.VISIBLE, Boolean.FALSE);
		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.CHANGE_STATE, actChangeState);

		cmpStateStandardView.setName("cmpStateStandardView");
		cmpStateStandardView.setVisible(false);
		cmpStateStandardView.setToolTipText(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.105",
						"Standard Statuspfad"));

		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.PRINT_DETAILS, actPrintDetails);
		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.LOCK, actLock);

		this.getDetailsPanel().registerToolBarAction(NuclosToolBarItems.INFO, getPointerAction());

		ToolBarConfiguration configuration = LafParameterProvider.getInstance()
				.getValue(LafParameter.nuclos_LAF_Tool_Bar_Detail, moduleUid);
		this.getDetailsPanel().setToolBarConfiguration(configuration);

		addLocalizationButton();
	}

	private void setOldHistoryVisible(boolean visible) {
		btnShowLogBook.setVisible(visible);
		jOldHistoricalDatechooser.setVisible(visible);
	}

	/**
	 * Reload layout when specialized flags (e.g. bUseInvalidMasterData) are set
	 * (method only for overriding).
	 */
	protected void loadSpecializedLayoutForDetails() {
		try {
			reloadLayoutForDetailsTab(true);
		} catch (CommonBusinessException ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	private void cmdShowLocalization() {
		
		UID locField = null;
		for (UID uid : getCollectableEntityForDetails().getFieldUIDs()) {
			if (getCollectableEntityForDetails().getEntityField(uid).isLocalized() && 
					!getCollectableEntityForDetails().getEntityField(uid).isCalculated()) {
				locField = uid;
			}
		}
		
		if (locField != null) {
			CollectableLocalizedComponentModel model = (CollectableLocalizedComponentModel)getDetailsComponentModel(locField);
			showLocalizationDialog(MetaProvider.getInstance().getEntity(getCollectableEntity().getUID()), model.getPrimaryKey(), model.getDataLanguageMap());			
		}
	}

	@Override
	protected void delegateBusinessRulesExecution(List<EventSupportSourceVO> lstRuleVO, CollectableGenericObjectWithDependants clct,
												  String custom, boolean bCollectiveProcessing) throws CommonBusinessException {
		GenericObjectDelegate.getInstance().executeBusinessRules(lstRuleVO, clct.getGenericObjectCVO(), getCustomUsage());
	}

	/**
	 * §todo pull down to CollectController 
	 */
	protected WeakReference<CollectPanel<Long, CollectableGenericObjectWithDependants>> newCollectPanel(UID entityUid) {
		EntityMeta<?> emdvo = MetaProvider.getInstance().getEntity(this.entityUid);
		boolean bSearch = MetaProvider.getInstance().getEntity(this.entityUid).isSearchable() || MainController.isOverrideSearchableForEntity(entityUid);
		boolean bShowSearch = emdvo.isShowSearch();
		DetailsPresentation detailsPresent = DetailsPresentation.DEFAULT;
		if (getViewMode() == ControllerPresentation.DEFAULT) {
			detailsPresent = LafParameterProvider.getInstance().getValue(LafParameter.nuclos_LAF_Details_Presentation, entityUid);
		}
		return new WeakReference<CollectPanel<Long,CollectableGenericObjectWithDependants>>(
				new GenericObjectCollectPanel(entityUid, getSearchStateBox(), bSearch, bShowSearch, detailsPresent, getViewMode()));
	}

	@Override
	public GenericObjectSearchPanel getSearchPanel() {
		return (GenericObjectSearchPanel) super.getSearchPanel();
	}

	@Override
	public GenericObjectDetailsPanel getDetailsPanel() {
		return (GenericObjectDetailsPanel) super.getDetailsPanel();
	}

	@Override
	public GenericObjectResultPanel getResultPanel() {
		return (GenericObjectResultPanel) super.getResultPanel();
	}

	private void setupEditPanels() {
		// get the layout for the Search and Details panels out of the Layout ML
		// definition:
		if (this.isSearchPanelAvailable())
			setupEditPanelForSearchTab(LayoutRoot.newEmptyLayoutRoot(true));
		setupEditPanelForDetailsTab();
	}

	public UID getCurrentLayoutUid() {
		return iCurrentLayoutId;
	}

	private void setupEditPanelForSearchTab(LayoutRoot layoutrootSearch) {
		layoutrootSearch.getRootComponent().setFocusCycleRoot(true);
		getSearchPanel().setEditView(newSearchEditView(layoutrootSearch));
		revalidateAdditionalSearchField();
		// create a controller for each subform:
		closeSubFormControllersInSearch();
		Map<UID, SubForm> mpSubForm = layoutrootSearch.getMapOfSubForms();
		mpsubformctlSearch = newSearchConditionSubFormControllers(mpSubForm);
		getSearchPanel().getEditView().setComponentsEnabled(true);
		if (!getUsageCriteriaFieldListenersAdded(true))
			this.addUsageCriteriaFieldListeners(true);
		setupSubFormController(mpSubForm, mpsubformctlSearch);
		addAdditionalChangeListeners(true);

		readSplitPaneStateFromPrefs(getCurrentLayoutUid(), getPreferences(), getSearchPanel());
	}

	private void setupEditPanelForDetailsTab() {
		// Optimization: loading the details panel isn't necessary here, so we
		// install dummies instead.
		final LayoutRoot layoutrootDetails = getInitialLayoutMLDefinitionForDetailsPanel();
		final JComponent compEdit = getDetailsPanel().newEditComponent(layoutrootDetails.getRootComponent());
		getDetailsPanel().setEditView(DefaultEditView.newDetailsEditView(compEdit, layoutrootDetails,
						layoutrootDetails.getInitialFocusEntityAndField()));
		this.layoutrootDetails = layoutrootDetails;
		// create a controller for each subform (after closing the old ones):
		closeSubFormControllers(getSubFormControllersInDetails());
		closeMatrixControllers(getMatrixControllers());

		Map<UID, SubForm> mpSubForm = layoutrootDetails.getMapOfSubForms();
		Map<UID, DetailsSubFormController<Long,CollectableEntityObject<Long>>> mpSubFormController = newDetailsSubFormControllers(mpSubForm);
		setMapOfSubFormControllersInDetails(mpSubFormController);
		setupSubFormController(mpSubForm, mpSubFormController);
		
		initMatrixControllers();
		
		readSplitPaneStateFromPrefs(getCurrentLayoutUid(), getPreferences(), getDetailsPanel());
	}


	@Override
	protected void setupSubFormController(Map<UID, SubForm> mpSubForm, Map<UID, ? extends SubFormController> mpSubFormController) {
		Map<SubForm, MasterDataSubFormController> mpSubFormController_tmp = new HashMap<SubForm, MasterDataSubFormController>();
		final ParameterChangeListener changeListener = new ParameterChangeListener() {
			@Override
			public void stateChanged(final ChangeEvent e) {
				if (e != null && e.getSource() instanceof SubForm) {
					UIUtils.invokeOnDispatchThread(new Runnable() {
						@Override
						public void run() {
							SubForm subform = (SubForm) e.getSource();
							GenericObjectCollectController.this.getSubFormsLoader().startLoading(subform.getEntityUID());
						}
					});
				}
			}
		};

		// create a map of subforms and their controllers
    	for (UID sSubFormEntityName : mpSubFormController.keySet()) {
			SubFormController subformcontroller = mpSubFormController
					.get(sSubFormEntityName);
			if (subformcontroller instanceof SearchConditionSubFormController) {
				((SearchConditionSubFormController) subformcontroller).clear();

				subformcontroller.getSubForm()
					.setRowHeight(subformcontroller.getPrefs().getInt(TableRowIndicator.SUBFORM_ROW_HEIGHT,
									subformcontroller.getSubForm()
											.isDynamicRowHeightsDefault() ? SubForm.DYNAMIC_ROW_HEIGHTS
											: subformcontroller.getSubForm().getMinRowHeight()));
				
			}

			SubForm subform = subformcontroller.getSubForm();
			if (!GenericObjectCollectController.this.getSubFormsLoader().isLoadingSubForms()
					&& !(subformcontroller instanceof SearchConditionSubFormController)) {
				SizeKnownListener listener = subform.getSizeKnownListener();
				if (listener != null) {
					boolean bWasIgnorePreferencesUpdate = subformcontroller.isIgnorePreferencesUpdate();
					subformcontroller.setIgnorePreferencesUpdate(true);
					listener.actionPerformed(new SizeKnownEvent(subform, subform.getJTable().getRowCount()));
					subformcontroller.setIgnorePreferencesUpdate(bWasIgnorePreferencesUpdate);
				}
			}

			if (subformcontroller instanceof DetailsSubFormController<?,?>) {
				subform.addParameterListener(changeListener);
				
				((DetailsSubFormController<Long, CollectableGenericObjectWithDependants>)subformcontroller).setCollectController(this);
				((DetailsSubFormController<Long, CollectableGenericObjectWithDependants>)subformcontroller).setupHistoryLookUp(this);
				
				mpSubFormController_tmp.put(subform, (MasterDataSubFormController) subformcontroller);
			}
			// disable child subforms in searchpanel, because it's not possible
			// to search for data in those subforms
			else if (subformcontroller instanceof SearchConditionSubFormController) {
				if (subform.getParentSubForm() != null)
					subform.setEnabled(false);
				
				if (MetaProvider.getInstance().getEntity(sSubFormEntityName).isProxy()) {
					subform.setVisible(false);
				}
			}
		}

		// assign child subforms to their parents
		for (SubForm subform : mpSubFormController_tmp.keySet()) {
			SubForm parentsubform = mpSubForm.get(subform.getParentSubForm());
			if (parentsubform != null) {
				MasterDataSubFormController subformcontroller = mpSubFormController_tmp.get(parentsubform);
				subformcontroller.addChildSubFormController(mpSubFormController_tmp.get(subform));
			}
		}
	}

	@Override
	protected void selectTab() {
		super.selectTab();

		setInitialComponentFocusInSearchTab();
	}

	@Override
	protected void setInitialComponentFocusInSearchTab() {
		if (getSearchPanel() != null)
			Utils.setInitialComponentFocus(getSearchPanel().getEditView(), mpsubformctlSearch);
	}

	@Override
	protected void setInitialComponentFocusInDetailsTab() {
		if (getDetailsPanel() != null)
			Utils.setInitialComponentFocus(getDetailsPanel().getEditView(), mpsubformctlDetails);
	}

	/**
	 * @return Map<String sFieldName, DetailsComponentModel> the map of
	 *         parsed/constructed collectable component models. Maps a field
	 *         name to a <code>DetailsComponentModel</code>.
	 * @todo move to LayoutRoot
	 */
	private static Map<UID, DetailsComponentModel> getMapOfDetailsComponentModels(LayoutRoot layoutroot) {
		return CollectionUtils.typecheck(layoutroot.getMapOfCollectableComponentModels(),
				DetailsComponentModel.class);
	}

	/**
	 * parses the LayoutML definition and gets the layout information
	 * 
	 * @return the LayoutRoot containing the layout information
	 */
	@Override
	protected LayoutRoot getInitialLayoutMLDefinitionForSearchPanel() {
		final org.nuclos.common.collect.collectable.CollectableEntity clcte = getCollectableEntity();
		iCurrentLayoutId = GenericObjectLayoutCache.getInstance().getLayoutUid(
				clcte,
				new UsageCriteria(getModuleId(), getProcess() == null ? null
						: (UID) getProcess().getValueId(), null,
						getCustomUsage()), true);

		final LayoutRoot layoutRoot = getLayoutFromCache(new UsageCriteria(
				getModuleId(), getProcess() == null ? null
						: (UID) getProcess().getValueId(), null,
				getCustomUsage()), new CollectState(
				CollectState.OUTERSTATE_SEARCH,
				CollectState.SEARCHMODE_UNSYNCHED));

		getLayoutMLButtonsActionListener().setComponentsEnabled(false);

		// adjustTabbedPanes - remove empty tabs.
		JComponent jcomp = layoutRoot.getRootComponent();
		List<JTabbedPane> lst = new ArrayList<JTabbedPane>();

		searchTabbedPanes(jcomp, lst);

		adjustTabbedPanes(lst, true);
		return layoutRoot;
	}

	protected LayoutRoot getInitialLayoutMLDefinitionForDetailsPanel() {
		return LayoutRoot.newEmptyLayoutRoot(false);
	}

	/**
	 * creates a searchable subform ctl for each subform. If the subform is
	 * disabled, the controller will be disabled.
	 * 
	 * @param mpSubForms
	 */
	@Override
	protected Map<UID, SearchConditionSubFormController> newSearchConditionSubFormControllers(Map<UID, SubForm> mpSubForms) {
		final UID sParentEntityName = this.getEntityUid();

		final SearchEditModel editmodelSearch = getSearchPanel().getEditModel();

		return CollectionUtils.transformMap(mpSubForms,
				new Transformer<SubForm, SearchConditionSubFormController>() {
					@Override
					public SearchConditionSubFormController transform(
							SubForm subform) {
						return newSearchConditionSubFormController(subform,
								sParentEntityName, editmodelSearch);
					}
				});
	}

	/**
	 * creates an editable subform ctl for each subform. If the subform is
	 * disabled, the controller will be disabled.
	 * 
	 * @param mpSubForms
	 */
	protected Map<UID, DetailsSubFormController<Long,CollectableEntityObject<Long>>> newDetailsSubFormControllers(
			Map<UID, SubForm> mpSubForms) {
		final Long iParentId;
		final UID sParentEntityName = getSelectedCollectableModuleId();

		final DetailsEditModel editmodelDetails = getDetailsPanel().getEditModel();
		if (getCollectState().isDetailsModeNew()) {
			iParentId = null;
		} else {
			iParentId = getSelectedGenericObjectId();
		}

		Map<UID, DetailsSubFormController<Long, CollectableEntityObject<Long>>> result = 
				CollectionUtils.transformMap(mpSubForms, new Transformer<SubForm, DetailsSubFormController<Long, CollectableEntityObject<Long>>>() {
			@Override
			public DetailsSubFormController<Long, CollectableEntityObject<Long>> transform(SubForm subform) {
				final DetailsSubFormController<Long, CollectableEntityObject<Long>> result = 
						newDetailsSubFormController(subform, sParentEntityName, editmodelDetails);
				result.setParentId(iParentId);
				return result;
			}
		});
		getDetailsController().setSubFormControllers(result.values());
		return result;
	}

	/**
	 * §todo maybe move to CollectController?
	 * 
	 * @param subform
	 * @param clctcompmodelprovider
	 */
	@Override
	protected SearchConditionSubFormController newSearchConditionSubFormController(SubForm subform, UID parentEntityUid,
			CollectableComponentModelProvider clctcompmodelprovider) {

		final String sControllerType = subform.getControllerType();
		if (sControllerType != null && !sControllerType.equals("default"))
			LOG.warn("Kein spezieller SearchConditionSubFormController f?r Controllertyp " + sControllerType + " vorhanden.");
		return _newSearchConditionSubFormController(clctcompmodelprovider, parentEntityUid, subform);
	}

	@Override
	protected SearchConditionSubFormController _newSearchConditionSubFormController(CollectableComponentModelProvider clctcompmodelprovider,
			UID parentEntityUid, SubForm subform) {

		// if parent of subform is another subform, change given parent entity name
		UID sParentSubForm = subform.getParentSubForm();
		if (sParentSubForm != null)
			parentEntityUid = sParentSubForm;

		return new SearchConditionSubFormController(getTab(),
				clctcompmodelprovider, parentEntityUid, subform,
				getPreferences(), getEntityPreferences(),
				MasterDataCollectableFieldsProviderFactory.newFactory(null,
						valueListProviderCache));
	}

	/**
	 * §postcondition result != null
	 * §todo maybe pull down to CollectController?
	 * 
	 * @param subform
	 * @param clctcompmodelprovider
	 */
	private MasterDataSubFormController newDetailsSubFormController(SubForm subform, UID parentEntityUid,
			CollectableComponentModelProvider clctcompmodelprovider) {

		// if parent of subform is another subform, change given parent entity
		// name
		UID sParentSubForm = subform.getParentSubForm();
		if (sParentSubForm != null)
			parentEntityUid = sParentSubForm;

		MasterDataSubFormController result = newDetailsSubFormController(
				subform, parentEntityUid, clctcompmodelprovider, getTab(),
				getDetailsPanel(), getPreferences(), getEntityPreferences(), this);

		return result;
	}

	private void removeAdditionalChangeListeners(boolean bSearchable) {
		for (SubFormController subformctl : getSubFormControllers(bSearchable))
			subformctl.getSubForm().removeChangeListener(
					getChangeListener(bSearchable));
	}

	/**
	 * @deprecated Move to SearchController specialization.
	 */
	@Deprecated
	@Override
	public void addAdditionalChangeListenersForSearch() {
		super.addAdditionalChangeListenersForSearch();
		ListenerUtil.registerCollectableComponentModelListener(
				getSearchStateBox().getSearchModel(), null,
				ccmlistenerSearchChanged);
	}

	/**
	 * @deprecated Move to SearchController and make protected again.
	 */
	@Deprecated
	@Override
	public void removeAdditionalChangeListenersForSearch() {
		removeAdditionalChangeListeners(true);
		getSearchStateBox().getSearchModel().removeCollectableComponentModelListener(ccmlistenerSearchChanged);
	}

	/**
	 * @deprecated Move to DetailsController hierarchy and make protected again.
	 */
	@Deprecated
	@Override
	public void addAdditionalChangeListenersForDetails() {
		addAdditionalChangeListeners(false);
	}

	/**
	 * @deprecated Move to DetailsController and make protected again.
	 */
	@Deprecated
	@Override
	public void removeAdditionalChangeListenersForDetails() {
		removeAdditionalChangeListeners(false);
	}

	@Override
	protected Collection<? extends SubFormController> getSubFormControllers(boolean bSearchTab) {
		return bSearchTab ? getSubFormControllersInSearch() : getSubFormControllersInDetails();
	}

	/**
	 * @return Map&lt;UID, DetailsSubFormController&gt;. May be <code>null</code>.
	 */
	@Override
	protected Map<UID, DetailsSubFormController<Long,CollectableEntityObject<Long>>> getMapOfSubFormControllersInDetails() {
		return mpsubformctlDetails;
	}

	public void setMapOfSubFormControllersInDetails(
			Map<UID, DetailsSubFormController<Long,CollectableEntityObject<Long>>> mpSubFormControllersInDetails) {
		mpsubformctlDetails = mpSubFormControllersInDetails;
	}

	@Override
	protected Map<UID, SearchConditionSubFormController> getMapOfSubFormControllersInSearch() {
		return this.mpsubformctlSearch;
	}

	/**
	 * §postcondition result != null
	 */
	private Collection<SearchConditionSubFormController> getSubFormControllersInSearch() {
		return CollectionUtils
				.valuesOrEmptySet(getMapOfSubFormControllersInSearch());
	}

	/**
	 * §precondition this.isSearchPanelAvailable()
	 * §postcondition result == null || result.isSyntacticallyCorrect()
	 * 
	 * @param bMakeConsistent
	 * @return the search condition contained in the search panel's fields
	 *         (including the subforms' search fields).
	 * 
	 * @deprecated Move to SearchController or SearchPanel and make protected
	 *             again.
	 */
	@Deprecated
	@Override
	public CollectableSearchCondition getCollectableSearchConditionFromSearchFields(
			boolean bMakeConsistent) throws CollectableFieldFormatException {
		if (!isSearchPanelAvailable())
			throw new IllegalStateException("!this.isSearchPanelAvailable()");
		final CollectableSearchCondition cond = super.getCollectableSearchConditionFromSearchFields(bMakeConsistent);

		final CompositeCollectableSearchCondition condAnd = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		if (cond != null)
			condAnd.addOperand(cond);

		assert getMapOfSubFormControllersInSearch() != null;
		for (SearchConditionSubFormController subformctl : getMapOfSubFormControllersInSearch().values()) {
			for (CollectableSearchCondition subCond : subformctl.getCollectableSubformSearchConditions()) {
				if (subCond != null) {
					if (subformctl.getCollectableEntity().getFieldUIDs().contains(SF.LOGICALDELETED.getUID(subformctl.getCollectableEntity().getUID()))) {
						final CollectableEntityField clctEOEFdeleted = 
								new CollectableEOEntityField(SF.LOGICALDELETED.getMetaData(subformctl.getCollectableEntity().getUID()));
						final CollectableSearchCondition condSearchDeleted = new CollectableComparison(clctEOEFdeleted, ComparisonOperator.EQUAL, new CollectableValueField(false));
						subCond = SearchConditionUtils.and(subCond, condSearchDeleted);
					}

					condAnd.addOperand(
							new CollectableSubCondition(subformctl.getCollectableEntity().getUID(), subformctl.getForeignKeyFieldUID(), subCond));
				}
			}
			/*
			 * Subcondition or old code do not delete please final
			 * CollectableSearchCondition condSub =
			 * subformctl.getCollectableSearchCondition(); if (condSub != null)
			 * condAnd.addOperand(new
			 * CollectableSubCondition(subformctl.getCollectableEntity
			 * ().getName(), subformctl.getForeignKeyFieldName(), condSub));
			 */
		}

		final CollectableSearchCondition result = SearchConditionUtils.simplified(condAnd);
		assert result == null || result.isSyntacticallyCorrect();
		return result;
	}

	/**
	 * @return the search condition to display. Includes the currently selected
	 *         global search filter's search condition (if any).
	 * @throws CollectableFieldFormatException
	 * 
	 * @deprecated Move to SearchController and make protected again.
	 */
	@Deprecated
	@Override
	public CollectableSearchCondition getCollectableSearchConditionToDisplay()throws CollectableFieldFormatException {
		final CompositeCollectableSearchCondition compositecond = new CompositeCollectableSearchCondition(LogicalOperator.AND);

		final CollectableSearchCondition clctcond = super.getCollectableSearchConditionToDisplay();
		if (clctcond != null)
			compositecond.addOperand(clctcond);

		return SearchConditionUtils.simplified(compositecond);
	}

	/**
	 * @return a Comparator that compares the entity labels first, then the
	 *         field labels. Fields of the main entity are sorted lower than all
	 *         other fields.
	 * 
	 * @deprecated Remove this. private Comparator<CollectableEntityField>
	 *             getCollectableEntityFieldComparator() { return new
	 *             Comparator<CollectableEntityField>() { final Collator
	 *             collator = LangUtils.getDefaultCollator();
	 */
	/**
	 * @return a specific table model with an overridden getValueAt method,
	 *         providing access to subform entries.
	 * 
	 * @deprecated Move to ResultController hierarchy.
	 */
	@Deprecated
	@Override
	protected SortableCollectableTableModel<Long,CollectableGenericObjectWithDependants> newResultTableModel() {
		final SortableCollectableTableModel<Long, CollectableGenericObjectWithDependants> result =
				new GenericObjectsResultTableModel<Long, CollectableGenericObjectWithDependants>(getCollectableEntity(), getFields().getSelectedFields());

		/**
		 * @deprecated Move to ResultController hierarchy.
		 */
		@Deprecated
		class GenericObjectSortingRunnable implements CommonRunnable {
			@Override
			public void run() throws CommonSortingException {
				final UID baseEntity = getCollectableEntity().getUID();
				boolean canSort = true;
				for (SortKey sk : result.getSortKeys()) {
					final CollectableEntityField sortField = result
							.getCollectableEntityField(sk.getColumn());
					if (!sortField.getEntityUID().equals(baseEntity)) {
						canSort = false;
						break;
					}
				}
				if (canSort) {
					// NUCLEUSINT-1039
					getResultController().getSearchResultStrategy().cmdSearch(true);
				} else {
					result.setSortKeys(Collections.<SortKey> emptyList(), false);
					throw new CommonSortingException(getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.19",
							"Das Suchergebnis kann nicht nach Unterformularspalten sortiert werden."));
				}
			}
		}

		final JTable tbl = getResultTable();
		// clicking header does not mean column selection, but sorting:
		tbl.setColumnSelectionAllowed(false);

		final GenericObjectSortingRunnable runnable = new GenericObjectSortingRunnable();

		// clicking a column header is to cause a new search on the server:
		TableUtils.addMouseListenerForSortingToTableHeader(tbl, result, runnable);

		// @todo: this is not so nice a construct... maybe this should be moved
		// to the result panel, because we use insight into the implementation
		// of fixed tables here.
		TableUtils.addMouseListenerForSortingToTableHeader(getFixedResultTable(), result, runnable);

		return result;
	}

	/**
	 * Get the fixed part of the result table, if any.
	 */
	@Override
	public JTable getFixedResultTable() {
		return getResultPanel().getFixedResultTable();
	}

	/**
	 * sets up the change listener for the vertical scrollbar of the result
	 * table, only if the proxy list has been set (that is not before the first
	 * search).
	 * 
	 * TODO: Make this protected again.
	 */
	public void setupChangeListenerForResultTableVerticalScrollBar() {
		final ProxyList<Long,? extends Collectable> lstclct = getSearchStrategy().getCollectableProxyList();
		if (lstclct != null)
			getResultPanel().setupChangeListenerForResultTableVerticalScrollBar(lstclct, getTab());
	}

	/**
	 * TODO: Make this proteced again.
	 * 
	 * @deprecated Move to ResultPanel???
	 */
	@Deprecated
	public void removePreviousChangeListenersForResultTableVerticalScrollBar() {
		final JScrollBar scrlbarVertical = getResultPanel().getResultTableScrollPane().getVerticalScrollBar();
		final DefaultBoundedRangeModel model = (DefaultBoundedRangeModel) scrlbarVertical.getModel();
		GenericObjectResultPanel.removePreviousChangeListenersForResultTableVerticalScrollBar(model);
	}

	@Override
	protected void unsafeFillDetailsPanel(CollectableGenericObjectWithDependants clct) throws CommonBusinessException {
		if (getCollectState().isDetailsModeNew())
			iCurrentLayoutId = null; 
		// reset current loaded layout. @see NUCLOS-1085
		this.unsafeFillDetailsPanel(clct, getCollectState().isDetailsModeNew()); 
		// @see NUCLOS-656 respecting getProcess is not needed here.
		// If process is not null, it will be set via newCollectableWithDefaultValues
	}

	@Override
	public boolean inHistoricalView(UID subformEntityUID, UID subformFieldUID, UID foreignKeyFieldToParent, int row) {
		boolean retVal = false;
		
		if(isHistoricalView()) {
			if (foreignKeyFieldToParent != null && this.lowdCurrent != null) {
				CollectableEntityObject<Long> selectedRow = this.getSubFormController(subformEntityUID).getCollectableTableModel().getRow(row);
				IDependentKey dependentKey = DependentDataMap.createDependentKey(foreignKeyFieldToParent);
				if (lowdCurrent.getDependants(dependentKey) != null) {
					Iterator<CollectableEntityObject<Long>> iterator = lowdCurrent.getDependants(dependentKey).iterator();
					boolean foundTheEntry = false;					
					while(iterator.hasNext()) {
						CollectableEntityObject<Long> next = iterator.next();
						if (next.getPrimaryKey().equals(selectedRow.getPrimaryKey())) {
							if (!next.getField(subformFieldUID).equals(selectedRow.getField(subformFieldUID))) {
								retVal = true;
							}
							foundTheEntry = true;
							break;
						}
					}
					if (!foundTheEntry) {
						retVal = true;
					}
				}			
			}
		}
		
		return retVal;
	}

	private void initDataLanguageMap(CollectableLocalizedComponent<Long> locTxtfield, CollectableGenericObjectWithDependants clct) {
		locTxtfield.getDetailsComponentModel().setPrimaryKey(clct.getPrimaryKey());
		if (isFillDuringClone())  {
			locTxtfield.getDetailsComponentModel().initDataLanguageMap(
					clct.getGenericObjectCVO().getDataLanguageMap().copy(), getTab());
		} else {
			locTxtfield.getDetailsComponentModel().initDataLanguageMap(
					clct.getGenericObjectCVO().getDataLanguageMap(), getTab());
		}
	}

	protected void unsafeFillDetailsPanel(CollectableGenericObjectWithDependants clct,
			boolean getUsageCriteriaFromClctOnly) throws CommonBusinessException {
		LOG.debug("GenericObjectCollectController.unsafeFillDetailsPanel start");
		
		// stop loading subforms (NUCLOS-5023)
		getSubFormsLoader().interruptAllClientWorkers();

		// Don't reload the layout after update - only if quintuple fields
		// changed. */
		if (bReloadLayout) {
			// Load the right layout if it is not already there:
			// Note that quintuple field listeners are removed here:
			loadLayoutForDetailsTab(clct, getCollectStateModel().getCollectState(), getUsageCriteriaFromClctOnly);
		} else {
			// Restore the default setting (Next time, reload the layout):
			bReloadLayout = true;
			// for Groovy resetting...
			Collection<DetailsSubFormController<Long, CollectableEntityObject<Long>>> collsubformctls = getSubFormControllersInDetails();

			final CollectState collectstate = getCollectStateModel().getCollectState();
			final UsageCriteria usagecriteria = getUsageCriteria(clct);
			respectRights(getDetailsPanel().getEditView().getCollectableComponents(), collsubformctls, usagecriteria, collectstate);
			respectRights(getDetailsPanel().getEditView().getCollectableLabels(), collsubformctls, usagecriteria, collectstate);
		}

		/**
		 * @todo this doesn't seem right: when the layout changes, the fields are
		 *       transferred from the old mask (in loadLayoutForDetailsTab),
		 *       then they are overwritten with the values of clct?
		 */

		try {
			if (isHistoricalView())
				lowdCurrent = new CollectableGenericObjectWithDependants(GenericObjectDelegate.getInstance().getWithDependants(
						getModuleId(), getSelectedCollectableId(), getCustomUsage()));

			MetaProvider metaprov = MetaProvider.getInstance();
			UID dataMandator = null;
			if (metaprov.getEntity(entityUid).isMandator()) {
				dataMandator = (UID) clct.getValueId(SF.MANDATOR_UID.getUID(entityUid));
			}
			
			for (UID sFieldName : getOrderedFieldNamesInDetails()) {
				
				for (CollectableComponent cc : layoutrootDetails.getCollectableComponentsFor(sFieldName)) {
					if (cc instanceof CollectableComponentWithValueListProvider) {
						CollectableComponentWithValueListProvider ccwvlp = (CollectableComponentWithValueListProvider) cc;
						if (VLPClientUtils.setVLPBaseRestrictions(sFieldName, ccwvlp.getValueListProvider(), clct.getId(), dataMandator)) {
							ccwvlp.refreshValueList(true, true, true);
						}
					}
					else if (cc instanceof CollectableLocalizedComponent) {
						initDataLanguageMap((CollectableLocalizedComponent<Long>) cc, clct);
					}
				}
				
				// iterate over the models rather than over the components:
				final CollectableComponentModel clctcompmodel = layoutrootDetails
						.getCollectableComponentModelFor(sFieldName);
				final CollectableField clctfShown = clct.getField(sFieldName);
				setParameterMatrixVLP(sFieldName, clctfShown);
				if (isDetailmodeAnyNew()) {
					if (!clctfShown.isNull()) {
						if (isFillDuringClone() || isDetailsmodeNewChanged()) {
							clctcompmodel.setFieldInitial(clctfShown, true, false);
						} else {
							clctcompmodel.setField(clctfShown, false); // @see NUCLOS-704
						}
					}
				} else {
					clctcompmodel.setFieldInitial(clctfShown, true, true);			
				}

				// NUCLOS-1477
				final String dcType = clctcompmodel.getEntityField()
						.getDefaultComponentType();
				if (DefaultComponentTypes.AUTONUMBER.equals(dcType)) {
					final Collection<CollectableComponent> clctComponents = layoutrootDetails.getCollectableComponentsFor(sFieldName);
					doAutoNumberStuff(clctComponents);
				}

				if (isHistoricalView()) {
					markFieldInHistoricalView(layoutrootDetails, lowdCurrent, sFieldName, clctfShown);
				}
			}

			// fill subforms:
			LOG.debug("fill subforms start");
			Collection<LogbookVO> colllogbookvo = null;
			if (isHistoricalView() && !isNewHistoricalView())
				try {
					colllogbookvo = GenericObjectDelegate.getInstance().getLogbook(lowdCurrent.getId());
				} catch (CommonFinderException ex) {
					// No logbook, no changes
				}

			selectTabPane(clct);

			if (!getCollectState().isDetailsModeMultiViewOrEdit()) {
				if (clct.getId() != null) {
					UIUtils.invokeOnDispatchThread(new Runnable() {
						@Override
						public void run() {
							GenericObjectCollectController.this.getSubFormsLoader().startLoading();
						}
					});
					if (isHistoricalView())
						showLoading(false);
				}

				// loading subforms
				for (final SubFormController subformctl : getSubFormControllersInDetails()) {
					subformctl.setMandatorUID(dataMandator);
					
					// TODO try to eliminate this cast
					final MasterDataSubFormController mdsubformctl = (MasterDataSubFormController) subformctl;

					if (clct.getId() instanceof Long) {
						mdsubformctl.setIntidForVLP(clct.getId());
					}

					if (isHistoricalView() && !isNewHistoricalView()) {
						markSubformInHistoricalView(mdsubformctl, clct, colllogbookvo);
					} else {
						// by object generation or clone
						final IDependentDataMap dependents = clct.getGenericObjectWithDependantsCVO().getDependents();
						final UID foreignKeyFieldUID = mdsubformctl.getForeignKeyFieldUID();
						final FieldMeta<?> foreignKeyField = MetaProvider.getInstance().getEntityField(foreignKeyFieldUID);
						if (clct.getId() == null && !dependents.getData(foreignKeyField).isEmpty()) {
							mdsubformctl.fillSubForm(null, dependents.getData(foreignKeyField));
							mdsubformctl.getSubForm().setNewEnabled(
									new CollectControllerScriptContext(
											GenericObjectCollectController.this,
											new ArrayList<DetailsSubFormController<?, ?>>(getSubFormControllersInDetails())
									)
							);
						}
						else if (clct.getId() == null) {
							mdsubformctl.clear();
							mdsubformctl.getSubForm().getJTable().setBackground(Color.WHITE);
							mdsubformctl.fillSubForm(null, new ArrayList<EntityObjectVO>());
							mdsubformctl.getSubForm().setNewEnabled(
									new CollectControllerScriptContext(
											GenericObjectCollectController.this,
											new ArrayList<DetailsSubFormController<?,?>>(getSubFormControllersInDetails())
									)
							);
						}
						else {
							if (mdsubformctl.isChildSubForm())
								continue;

							fillSubformMultithreaded(clct, mdsubformctl);
						}
					}
				}

				loadMatrices(clct, clct.getGenericObjectCVO().getStatus());

			}
			LOG.debug("fill subforms done");

			getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(false);

			// @see NUCLOS-1391
			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					ScriptContext ctx = new CollectControllerScriptContext(
							GenericObjectCollectController.this, new ArrayList<DetailsSubFormController<?,?>>(getSubFormControllersInDetails()));
					if (getDetailsPanel() != null) {
						for (CollectableComponent c : getDetailsPanel().getEditView().getCollectableComponents()) {
							c.setComponentState(ctx, null);
						}
					}
				}
			});
			// update Layout Components
			notifyLayoutComponentListeners(layoutrootDetails, true);
		} finally {
			/** @todo this doesn't seem to belong here... */
			if (!getUsageCriteriaFieldListenersAdded(false))
				this.addUsageCriteriaFieldListeners(false);
		}
		showMandator(clct);
		updateLockState(clct);
		LOG.debug("GenericObjectCollectController.unsafeFillDetailsPanel done");
	}

	private void selectTabPane(CollectableGenericObjectWithDependants clct) {
		String strTabName = getSelectedTabName();

		if (strTabName == null) {
			//try to select tab in state
			DynamicAttributeVO vo = clct.getGenericObjectCVO().getAttribute(SF.STATENUMBER.getMetaData(clct.getEntityUID()).getUID());
			if (vo != null) {
				Integer iState = (Integer) vo.getValue();
				for (StateVO voState : StateDelegate.getInstance().getStatesByModule(getModuleId())) {
					if (voState.getNumeral().equals(iState)) {
						if (voState.getTabbedPaneName() != null) {
							strTabName = voState.getTabbedPaneName();
						}
						break;
					}
				}
			}
		}

		if (strTabName != null) {
			notifyAndSelectTabs(strTabName, false);
		}
	}
	
	private void notifyAndSelectTabs(String strTabName, boolean adjustTabbedPanes) {
		JComponent jcomp = layoutrootDetails.getRootComponent();		
		List<JTabbedPane> lst = new ArrayList<JTabbedPane>();

		searchTabbedPanes(jcomp, lst);
		if (adjustTabbedPanes) {
			adjustTabbedPanes(lst, false);
		}

		for (JTabbedPane tabPane : lst) {
			if (tabPane instanceof JInfoTabbedPane) {
				((JInfoTabbedPane)tabPane).notifyForInitialTabSelection();
			}

			if (strTabName != null) {
				for (int i = 0; i < tabPane.getTabCount(); i++) {
					if (StringUtils.equals(tabPane.getComponentAt(i).getName(), strTabName)) {
						tabPane.setSelectedIndex(i);
						break;
					}
				}
			}
		}
		
	}

	private Map<String, Collection<RemovedTab>> mpRemovedTabs = new HashMap<String, Collection<RemovedTab>>();

	private void clearRemovedTabs() {
		if (mpRemovedTabs != null) {
			for (Collection<RemovedTab> col : mpRemovedTabs.values()) {
				col.clear();
			}
			mpRemovedTabs.clear();
		}
	}

	private void closeRemovedTabs() {
		clearRemovedTabs();
		mpRemovedTabs = null;
	}

	private void adjustTabbedPanes(List<JTabbedPane> lst, boolean bIsSearch) {
		//do not store tab selection during adjusting
		setSelectTabNameListenerEnabled(false);
		for (JTabbedPane tabPane : lst) {
			Collection<RemovedTab> removedTabs = mpRemovedTabs.get(tabPane
					.getName());
			if (removedTabs != null) {
				if (!bIsSearch) {
					for (RemovedTab removedTab : removedTabs) {
						if (removedTab.getIdx() >= tabPane.getTabCount())
							tabPane.insertTab(removedTab.getTitle(),
									removedTab.getIcon(), removedTab.getC(),
									removedTab.getTooltip(),
									tabPane.getTabCount() == 0 ? 0 : tabPane.getTabCount() - 1);
						else
							tabPane.insertTab(removedTab.getTitle(),
									removedTab.getIcon(), removedTab.getC(),
									removedTab.getTooltip(), removedTab.getIdx());
							// re-add missing tabs.
					}
				}
				mpRemovedTabs.remove(tabPane.getName());
			}
			removedTabs = new ArrayList<RemovedTab>();

			List<Component> lstTabToRemove = new ArrayList<Component>(tabPane.getTabCount());
			for (int i = 0; i < tabPane.getTabCount(); i++) {
				Component c = tabPane.getComponentAt(i);
				if (c instanceof JComponent) {
					List<JComponent> lstComponents = new ArrayList<JComponent>();
					collectComponents((JComponent) c, lstComponents);
					if (lstComponents.size() == 0) {
						tabPane.setEnabledAt(i, false);
						lstTabToRemove.add(c);
						break;
					}

					boolean blnVisible = false;
					for (JComponent jc : lstComponents) {
						if (jc instanceof LabeledComponent) {
							LabeledComponent lc = (LabeledComponent) jc;
							blnVisible = lc.isVisible();
						} else if (jc instanceof SubForm)
							blnVisible = jc.isVisible();
						else if (jc instanceof JPanel)
							blnVisible = false;
						else
							blnVisible = jc.isVisible();
						if (blnVisible)
							break;
					}

					tabPane.setEnabledAt(i, !blnVisible ? blnVisible : blnVisible && tabPane.isEnabledAt(i));
					if (!blnVisible) {
						lstTabToRemove.add(c);
						removedTabs.add(new RemovedTab(i, c, tabPane.getTitleAt(i), tabPane.getToolTipTextAt(i),
								tabPane.getIconAt(i)));
					}
				}
			}
			for (Component c : lstTabToRemove) {
				tabPane.remove(c);
			}
			if (!bIsSearch)
				mpRemovedTabs.put(tabPane.getName(), removedTabs);
		}
		setSelectTabNameListenerEnabled(true);
	}

	private void collectComponents(JComponent comp, List<JComponent> lst) {
		for (Component c : comp.getComponents())
			if (c instanceof JComponent) {
				JComponent jc = (JComponent) c;
				if (jc.getComponentCount() == 0)
					lst.add(jc);
				if (jc instanceof LabeledComponent) {
					lst.add(jc);
					continue;
				}
				if (jc instanceof SubForm) {
					lst.add(jc);
					continue;
				}
				if (!(jc instanceof TitledSeparator)) {
					collectComponents(jc, lst);
				}
			}
	}

	private void fillSubformMultithreaded(final CollectableGenericObjectWithDependants clct,
			final MasterDataSubFormController mdsubformctl) throws NuclosBusinessException {
		final SubFormsInterruptableClientWorker sfClientWorker = new SubFormsInterruptableClientWorker() {
			Collection<EntityObjectVO<?>> collmdcvo;
			Long iParentId;
			boolean limited = false;

			@Override
			public void init() throws CommonBusinessException {
				if (!interrupted) {
					boolean wasDetailsChangedIgnored = isDetailsChangedIgnored();
					setDetailsChangedIgnored(true);
					mdsubformctl.clear();
					setDetailsChangedIgnored(wasDetailsChangedIgnored);
					mdsubformctl.getSubForm().setLockedLayer();
				}
			}

			@Override
			public void work() throws NuclosBusinessException {
				if (interrupted || isClosed()) {
					return;
				}

				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// ignore
				}
				
				iParentId = clct.getPrimaryKey();
				if (iParentId == null)
					collmdcvo = new ArrayList<EntityObjectVO<?>>();
				else {
					boolean bLoad = false;
					if (clct instanceof CollectableGenericObjectWithDependants)  {
						IDependentKey dependentKey = DependentDataMap.createDependentKey(mdsubformctl.getForeignKeyFieldUID());
						if (clct.getGenericObjectWithDependantsCVO().getDependents().hasData(dependentKey)) {
							collmdcvo = clct.getGenericObjectWithDependantsCVO().getDependents().getData(dependentKey);
							bLoad = false;
						}
					}
					if (bLoad || collmdcvo == null) {
						if (clct.getId() == null) {
							collmdcvo = new ArrayList<EntityObjectVO<?>>();
						} else {
							Integer maxEntries = mdsubformctl.getSubForm().getMaxEntries();
							limited = isDetailsViewAvailable(mdsubformctl) && maxEntries != null && maxEntries >= 0;
							collmdcvo = new ArrayList<EntityObjectVO<?>>(MasterDataDelegate.getInstance().getDependentDataCollectionWithLimit(
									mdsubformctl.getForeignKeyFieldUID(), null, mdsubformctl.getSubForm().getMapParams(),
									getCurrentLayoutUid(), limited ? maxEntries : null, clct.getId()));
						}
					}
				}
				if (collmdcvo == null)
					collmdcvo = new ArrayList<EntityObjectVO<?>>();

			}

			@Override
			public void handleError(Exception ex) {
				if (!interrupted)
					Errors.getInstance().showExceptionDialog(Main.getInstance().getMainFrame(), ex);
			}

			@Override
			public JComponent getResultsComponent() {
				return mdsubformctl.getSubForm();
			}

			@Override
			public void paint() throws CommonBusinessException {
				// The data schould not be published to sub form! otherwise we
				// will see a sub form data of another object!
				if (!interrupted && !isClosed())
					synchronized (GenericObjectCollectController.this) {
						// GenericObjectCollectController.this.wait();
						final boolean bWasDetailsChangedIgnored = GenericObjectCollectController.this.isDetailsChangedIgnored();
						GenericObjectCollectController.this.setDetailsChangedIgnored(true);
						try {
							final CollectableField clctfield = clct.getField(SF.STATE.getUID(clct.getEntityUID()));
							final UID iStatusId = (clctfield != null) ? (UID) clctfield.getValueId() : null;

							if (!mdsubformctl.isClosed()) {
								UID entity = mdsubformctl.getEntityAndForeignKeyField().getEntity();
								IPermission permission = SecurityCache.getInstance().getSubFormPermission(entity, iStatusId);

								if (permission == null) {
									mdsubformctl.clear();
								} else {
									int originSize = collmdcvo.size();
									final Integer limit = mdsubformctl.getSubForm().getMaxEntries();
									final boolean truncated = limited && limit > 0 && originSize == limit + 1;

									if (truncated) {
										mdsubformctl.getSubForm().setReadOnlyWithDetailView(true, true);

										if (collmdcvo instanceof ArrayList && collmdcvo.size() > 0) {
											((ArrayList) collmdcvo).remove(collmdcvo.size() - 1);
										}
									}

									mdsubformctl.fillSubForm(iParentId, collmdcvo);
									
									if (truncated) {
										mdsubformctl.displayLimitBubble(limit);
									}
								}
								
								if (isHistoricalView() && isNewHistoricalView()) {
									//@todo performance.
									CollectableGenericObjectWithDependants clctlowdCurrent = new CollectableGenericObjectWithDependants(
											GenericObjectDelegate.getInstance().getWithDependants(getModuleId(), getSelectedCollectableId(), getCustomUsage()));
									mdsubformctl.setHistoricalDate(getHistoricalDate());
									markSubformInHistoricalView(mdsubformctl, clct, clctlowdCurrent);
								}

								mdsubformctl.getSubForm().setNewEnabled(
										new CollectControllerScriptContext(
												GenericObjectCollectController.this,
												new ArrayList<DetailsSubFormController<?,?>>(getSubFormControllersInDetails())
										)
								);
							}
						} finally {
							collmdcvo = null;
							if (!bWasDetailsChangedIgnored)
								GenericObjectCollectController.this.setDetailsChangedIgnored(bWasDetailsChangedIgnored);
						}
						GenericObjectCollectController.this.getSubFormsLoader().setSubFormLoaded(mdsubformctl.getCollectableEntity().getUID(), true);
						if (!mdsubformctl.isClosed()) {
							mdsubformctl.getSubForm().forceUnlockFrame();
						}
					}
			}
		};
		GenericObjectCollectController.this.getSubFormsLoader().addSubFormClientWorker(mdsubformctl.getCollectableEntity().getUID(), sfClientWorker);
	}

	@Deprecated
	private void markSubformInHistoricalView(MasterDataSubFormController subformctl,
			CollectableGenericObjectWithDependants clct, Collection<LogbookVO> logbookEntries) throws NuclosBusinessException {

		UID entityName = subformctl.getCollectableEntity().getUID();
		IDependentKey dependentKey = DependentDataMap.createDependentKey(subformctl.getForeignKeyFieldUID());
		Collection<EntityObjectVO<?>> collmdvo = clct.getGenericObjectWithDependantsCVO().getDependents().getData(dependentKey);
		/**
		 * @todo check if this is correct: Shouldn't we initialize the subform
		 *       even if it is empty?
		 */
		if (!collmdvo.isEmpty())
			subformctl.fillSubForm(null, collmdvo);

		EntityMeta entityMeta = MetaProvider.getInstance().getEntity(entityName);
		boolean hasLoggingFields = false;
		for(FieldMeta fieldMeta :
				MetaProvider.getInstance().getAllEntityFieldsByEntity(entityName).values())
			hasLoggingFields |= fieldMeta.isLogBookTracking();

		if (!hasLoggingFields) {
			// if no field of the entity is logged, mark subform table as not
			// logged
			subformctl.getSubForm().getJTable().setBackground(colorHistoricalNotTracked);
			subformctl.getSubForm().getJTable().getParent().setBackground(colorHistoricalNotTracked);
		} else {
			for (LogbookVO logbookvo : logbookEntries) {
				UID mmId = logbookvo.getMasterDataMeta();
				if(mmId != null && mmId.equals(entityMeta.getUID())) {
					// if there is any entry in the logbook for this entity done
					// after the date of the historical view, mark the subform
					// table as changed
					if (getHistoricalDate().compareTo(logbookvo.getCreatedAt()) < 0) {
						subformctl.getSubForm().getJTable().setBackground(colorHistoricalChanged);
						subformctl.getSubForm().getJTable().getParent().setBackground(colorHistoricalChanged);
						break;
					}
				}
			}
		}
	}

	private List<UID> getOrderedFieldNamesInDetails() {
		return layoutrootDetails.getOrderedFieldUIDs();
	}

	@Override
	protected void unsafeFillMultiEditDetailsPanel(Collection<CollectableGenericObjectWithDependants> collclct)
			throws CommonBusinessException {
		
		UID dataMandator = getMandatorUID(collclct);
		if (dataMandator != null && "-1".equals(dataMandator.toString())) {
			throw new NuclosAmbiguousMandatorException("Could not determine a unique mandator");
		}
		
		// set the right layout:
		final UsageCriteria usagecriteria = getGreatestCommonUsageCriteriaFromCollectables(collclct);
		this.reloadLayout(usagecriteria, getCollectStateModel().getCollectState(), true, false);

		for (UID sFieldName : getOrderedFieldNamesInDetails()) {
			for (CollectableComponent cc : layoutrootDetails.getCollectableComponentsFor(sFieldName)) {
				if (cc instanceof CollectableComponentWithValueListProvider) {
					CollectableComponentWithValueListProvider ccwvlp = (CollectableComponentWithValueListProvider) cc;
					if (VLPClientUtils.setVLPBaseRestrictions(sFieldName, ccwvlp.getValueListProvider(), new Long(-1), dataMandator)) {
						ccwvlp.refreshValueList(true);
					}
				}
			}
		}

		// fill the details panel with the common values:
		super.unsafeFillMultiEditDetailsPanel(collclct);

		for (CollectableGenericObjectWithDependants clct : collclct) {			
			for (UID sFieldName : getOrderedFieldNamesInDetails()) {
				for (CollectableComponent cc : layoutrootDetails.getCollectableComponentsFor(sFieldName)) {
					if (cc instanceof CollectableLocalizedComponent) {
						initDataLanguageMap((CollectableLocalizedComponent<Long>) cc, clct);
					}
				}
			}
		}

		// begin multi-update of dependants:
		multiupdateofdependants = new MultiUpdateOfDependants(getSubFormControllersInDetails(), collclct);

		getLayoutMLButtonsActionListener().setComponentsEnabled(false);
		// update Layout Components
		notifyLayoutComponentListeners(layoutrootDetails, false);
	}

	/**
	 * Get also changes in subforms
	 * 
	 * TODO: move to DetailsPanel and protect
	 */
	@Override
	public String getMultiEditChangeString() {
		final String sChangesSoFar = super.getMultiEditChangeString();
		final StringBuilder sbResult = new StringBuilder(sChangesSoFar);

		try {
			for (DetailsSubFormController<Long,CollectableEntityObject<Long>> subformctl : getSubFormControllersInDetails())
				if (subformctl.getSubForm().isEnabled()) {
					boolean bChanged = false;
					boolean bNew = false;
					boolean bRemoved = false;
					if (!subformctl.getSubForm().getJTable().isEditing())
						for (CollectableEntityObject clctmd : subformctl
								.getCollectables(true, true, false)) {
							EntityObjectVO mdvo = clctmd.getEntityObjectVO();
							if (mdvo.isFlagUpdated())
								if (mdvo.getCreatedBy() == null)
									bNew = true;
								else
									bChanged = true;
							if (mdvo.isFlagRemoved())
								bRemoved = true;
						}
					else
						bChanged = true;

					if (bNew || bChanged || bRemoved) {
						if (sbResult.length() > 0)
							sbResult.append(", ");
						sbResult.append("Eintr?ge in ").append(
								subformctl.getCollectableEntity().getLabel());
						if (bNew)
							sbResult.append(" hinzugef?gt");
						if (bChanged) {
							sbResult.append(bNew ? "/" : " ");
							sbResult.append("ver?ndert");
						}
						if (bRemoved) {
							sbResult.append(bNew || bChanged ? "/" : " ");
							sbResult.append("gel?scht");
						}
					}
				}
		} catch (CommonValidationException e) {
			// This will never occur, as there will not be validated in
			// getCollectables, when bPrepareForSavingAndValidate ist false
			LOG.warn("getMultiEditChangeString failed: " + e, e);
		}

		return sbResult.toString();
	}

	private void loadLayoutForDetailsTab(CollectableGenericObject clct,
			CollectState collectstate, boolean getUsageCriteriaFromClctOnly)
			throws CommonBusinessException {
		LOG.debug("loadLayoutForDetailsTab start");

		boolean transferContents = !collectstate.isDetailsModeNew();
		UsageCriteria uc = null;
		if (collectstate.isDetailsModeNew())
			uc = getUsageCriteriaFromClctOnly ? getUsageCriteria(clct) : getUsageCriteriaFromView(false);
		else
			uc = getUsageCriteria(clct);

		this.reloadLayout(uc, collectstate, transferContents, false);

		LOG.debug("loadLayoutForDetailsTab done");
	}
	
	@Override
	protected CollectableGenericObjectWithDependants readSelectedCollectable() throws CommonBusinessException {
		final CollectableGenericObjectWithDependants result;

		if (isHistoricalView() && !isNewHistoricalView()) {
			/**
			 * @todo OPTIMIZE: Is it really necessary to read the current object first?
			 */
			// What about this? UA/020206
			// final int iGenericObjectId = ((Integer)
			// this.getSelectedCollectableId()).intValue();
			// final int iModuleId =
			// this.getSelectedCollectableModuleId().intValue();
			final GenericObjectVO govo = getSelectedGenericObjectCVO();
			final Long iGenericObjectId = govo.getId();
			assert getHistoricalDate() != null;
			result = new CollectableGenericObjectWithDependants(
					lodelegate.getHistorical(iGenericObjectId, getHistoricalDate(), getCustomUsage()));
		} else
			result = super.readSelectedCollectable();

		return result;
	}

	@Override
	public CollectableGenericObjectWithDependants readCollectable(CollectableGenericObjectWithDependants clct) throws CommonBusinessException {
		return findCollectableById(clct.getCollectableEntity().getUID(), clct.getId());
	}
	
	@Override
	protected CollectableField readFromAdditionalFields(UID fieldUID) {
		CollectableField result = super.readFromAdditionalFields(fieldUID);
		if (result != null) {
			return result;
		}
		if (SF.STATE.checkField(getEntityUid(), fieldUID)) {
			StateWrapper state = getStateWrapperFromSelectedGenericObject();
			if (state != null) {
				return new CollectableValueIdField(state.getId(), state.getName());
			}
		} else if (SF.STATENUMBER.checkField(getEntityUid(), fieldUID)) {
			StateWrapper state = getStateWrapperFromSelectedGenericObject();
			if (state != null) {
				return new CollectableValueIdField(state.getId(), state.getNumeral());
			}
		} 
		return null;
	}

	public CollectableGenericObjectWithDependants findCollectableById(UID entityUid, Long oId) throws CommonBusinessException {
		if (entityUid == null)
			throw new IllegalArgumentException("sEntityName");
		if (oId == null)
			throw new IllegalArgumentException("oId");

		final CollectableGenericObjectWithDependants result =
				new CollectableGenericObjectWithDependants(lodelegate.getWithDependants(entityUid, oId, getCustomUsage()));
		if (!LangUtils.equal(result.getEntityUID(), getEntityUid())) {
			throw new CommonFinderException(StringUtils.getParameterizedExceptionMessage(
					"common.exception.novabitfinderexception.message", SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(getEntityUid())), oId));
		}
		assert getSearchStrategy().isCollectableComplete(result);
		return result;
	}
	
	@Override
	protected CollectableGenericObjectWithDependants findCollectableById(UID entity, Long oId, Collection<FieldMeta<?>> fields) throws CommonBusinessException {
		if (fields == null) {
			return findCollectableById(entity, oId);
		}
		
		Collection<UID> fieldUids = new HashSet<UID>();
		for (FieldMeta<?> fm : fields) {
			fieldUids.add(fm.getUID());
		}
		
		final EntityObjectVO<Long> eovo = EntityObjectDelegate.getInstance().getByIdWithDependents(entity, oId, fieldUids, getCustomUsage());
		
		if (eovo == null) {
			return null;
		}
		
		final CollectableGenericObjectWithDependants result = new CollectableGenericObjectWithDependants(
				DalSupportForGO.getGenericObjectWithDependantsVO(eovo,
				(CollectableEOEntity) CollectableEOEntityClientProvider.getInstance().getCollectableEntity(entity)));
		
		if (!LangUtils.equal(result.getEntityUID(), getEntityUid())) {
			throw new CommonFinderException(StringUtils.getParameterizedExceptionMessage(
					"common.exception.novabitfinderexception.message", SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(getEntityUid())), oId));
		}
		
		return result;
	}

	@Override
	protected CollectableGenericObjectWithDependants findCollectableByIdWithoutDependants(
		UID entity, Long oId) throws CommonBusinessException {
		if (entity == null)
			throw new IllegalArgumentException("entity");
		if (oId == null)
			throw new IllegalArgumentException("oId");

		final CollectableGenericObjectWithDependants result = CollectableGenericObjectWithDependants
				.newCollectableGenericObject(lodelegate.get(entity, oId));
		if (!LangUtils.equal(result.getEntityUID(), getEntityUid())) {
			throw new CommonFinderException(StringUtils.getParameterizedExceptionMessage(
					"common.exception.novabitfinderexception.message", SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(getEntityUid())), oId));
		}
		assert getSearchStrategy().isCollectableComplete(result);
		return result;
	}

	@Override
	protected boolean isDetailsModeViewLoadingWithoutDependants() {
		return true;
	}

	@Override
	public Integer getVersionOfCollectableById(UID entity, Long oId) throws CommonBusinessException {
		if (entity == null)
			throw new IllegalArgumentException("sEntityName");
		if (oId == null)
			throw new IllegalArgumentException("oId");

		return EntityObjectDelegate.getInstance().getVersion(entity, oId);
	}

	@Override
	protected CollectableGenericObjectWithDependants updateCurrentCollectable(CollectableGenericObjectWithDependants clctCurrent) throws CommonBusinessException {
		final CollectableGenericObjectWithDependants result = updateCollectable(clctCurrent,
				!isHistoricalView() ? getAllSubFormData(clctCurrent.getPrimaryKey())
						: new DependantCollectableMasterDataMap(clctCurrent.getGenericObjectWithDependantsCVO().getDependents()), null, false);

		// remember if the layout needs to be reloaded afterwards:
		if (!isProcessingStateChange.get() && getUsageCriteria(result).equals(getUsageCriteria(clctCurrent)))
			bReloadLayout = false;

		return result;
	}

	@Override
	protected CollectableGenericObjectWithDependants updateCollectable(CollectableGenericObjectWithDependants clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext, final boolean isCollectiveProcessing) throws CommonBusinessException {
		final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap)oAdditionalData;
		
		// @todo validate?

		// read the datalanguage contents from the model annd merge it into the clct
		if(isCollectiveProcessing) {
			final EditView view = this.getEditView(false);
			for (CollectableComponent clctcomp : view.getCollectableComponents()) {				
				if (clctcomp.getEntityField().isLocalized()) {
                    if(clctcomp.isReadOnly()) 
                    	continue;
                    
					DetailsLocalizedComponentModel model = (DetailsLocalizedComponentModel) clctcomp.getModel();						
					clct.getGenericObjectCVO().updateDataLanguageMap(model.getDataLanguageMap());									
				}
			}//for
		}//if
	
	
		if (mpclctDependants != null)
			Utils.mapNullBooleansToFalseInDependents(mpclctDependants);
		
		final IDependentDataMap mpmdvoDependants = mpclctDependants.toDependentDataMap();
		
		this.getAllMatrixData(mpmdvoDependants);
		
		final UID sMatrixPreferencesField = this.getMatrixPreferencesField();
		if (sMatrixPreferencesField != null) {
			String sPrefs = this.getMatrixPreferences();
			if(sPrefs != null) {
				clct.setField(sMatrixPreferencesField, new CollectableValueField(sPrefs));
			}
		}

		final GenericObjectWithDependantsVO lowdcvo = clct.getGenericObjectWithDependantsCVO();
		final GenericObjectWithDependantsVO lowdcvoCurrent = 
				new GenericObjectWithDependantsVO(lowdcvo, mpmdvoDependants, lowdcvo.getDataLanguageMap());

		// update the whole thing (only if no state change is processing):
		final AtomicReference<GenericObjectWithDependantsVO> lowdcvoUpdated = new AtomicReference<GenericObjectWithDependantsVO>();
		if (isProcessingStateChange.get()) {
			lowdcvoCurrent.setComplete(true); // lowdcvoCurrent will be updated by StateFacade. but we need to set complete here. otherwise an assert will fail.
			lowdcvoUpdated.set(lowdcvoCurrent);
		}
		else {
			invoke(new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					try {
						lowdcvoUpdated.set(lodelegate.update(lowdcvoCurrent, getCustomUsage(), isCollectiveProcessing));
						clearRemovedMatrixData();
					} catch (CommonBusinessException cbe) {
						handleBusinessExceptionAfterSafe(cbe);
						throw cbe;
					}
				}
			}, applyMultiEditContext);
		}
		// and return the updated version:
		return new CollectableGenericObjectWithDependants(lowdcvoUpdated.get());
	}

	/**
	 * @param govo
	 * @return a new <code>CollectableGenericObjectWithDependants</code>
	 *         wrapping <code>govo</code>.
	 */
	private static CollectableGenericObjectWithDependants newCollectableGenericObject(GenericObjectVO govo) {
		return CollectableGenericObjectWithDependants.newCollectableGenericObjectWithDependants(govo);
	}

	/**
	 * @param clct
	 * @return DependantCollectableMap containing the dependants of the given
	 *         Collectable relevant for multiple updates additional data (if
	 *         any) needed for multiple updates.
	 * @throws CommonValidationException if some subform data is invalid.
	 */
	@Override
	protected DependantCollectableMasterDataMap getAdditionalDataForMultiUpdate(CollectableGenericObjectWithDependants clct)
			throws CommonValidationException {
		return multiupdateofdependants.getDependantCollectableMapForUpdate(getSubFormControllersInDetails(), clct);
	}

	/**
	 * @return the CollectableEntity that contains exactly the fields that are
	 *         contained in the details tab.
	 */
	@Override
	protected final CollectableEntity getCollectableEntityForDetails() {
		final Collection<UID> collFieldNames = layoutrootDetails.getFieldUids();
		return new CollectableGenericObjectEntity(this.getEntityUid(), getEntityLabel(), collFieldNames);
	}

	@Override
	protected CollectableGenericObjectWithDependants insertCollectable(CollectableGenericObjectWithDependants clctNew) throws CommonBusinessException {
		if (clctNew.getId() != null)
			throw new IllegalArgumentException("clctNew");

		final IDependentDataMap mpclctDependants = getAllSubFormData(null).toDependentDataMap();

		// We have to clear the ids for cloned objects:
		// @todo eliminate this workaround - this is the wrong place. The right place is the Clone action!
		final IDependentDataMap mpDependants = org.nuclos.common.Utils.clearIds(mpclctDependants);

		this.getAllMatrixData(mpDependants);
		
		final UID sMatrixPreferencesField = this.getMatrixPreferencesField();
		if (sMatrixPreferencesField != null) {
			String sPrefs = this.getMatrixPreferences();
			if(sPrefs != null) {
				clctNew.setField(sMatrixPreferencesField, new CollectableValueField(sPrefs));
			}
		}

		final GenericObjectVO govo = clctNew.getGenericObjectCVO();
		final GenericObjectWithDependantsVO lowdcvoNew = new GenericObjectWithDependantsVO(govo, mpDependants, govo.getDataLanguageMap());

		final AtomicReference<GenericObjectWithDependantsVO> lowdcvoInserted = new AtomicReference<GenericObjectWithDependantsVO>();
		invoke(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				try {
					lowdcvoInserted.set(lodelegate.create(lowdcvoNew, getSelectedSubEntityUIDs(), getCustomUsage()));
					clearRemovedMatrixData();
				} catch (CommonBusinessException cbe) {
					handleBusinessExceptionAfterSafe(cbe);
					throw cbe;
				}
			}
		}, null);
		return new CollectableGenericObjectWithDependants(lowdcvoInserted.get());
	}

	@Override
	protected void cloneSelectedCollectable() throws CommonBusinessException {
		super.cloneSelectedCollectable();

		final EditModel modelDetails = getDetailsPanel().getEditModel();
		
		// Some attributes must be cleared for a new entity object:
		// Remember the origin of the cloned object
		CollectableComponentModel clctcompmodelIdentifier = 
				modelDetails.getCollectableComponentModelFor(SF.SYSTEMIDENTIFIER.getMetaData(this.entityUid).getUID());
		if (clctcompmodelIdentifier != null) {
			CollectableComponentModel clctcompmodelOrigin = modelDetails.getCollectableComponentModelFor(SF.ORIGIN.getMetaData(this.entityUid).getUID());
			if(clctcompmodelOrigin != null)
				clctcompmodelOrigin.setField(clctcompmodelIdentifier.getField());
			clctcompmodelIdentifier.clear();
		}

		CollectableComponentModel clctcompmodelStatus = modelDetails.getCollectableComponentModelFor(SF.STATE.getMetaData(this.entityUid).getUID());
		if (clctcompmodelStatus != null)
			clctcompmodelStatus.clear();
		CollectableComponentModel clctcompmodelStatusNumeral = modelDetails.getCollectableComponentModelFor(SF.STATENUMBER.getMetaData(this.entityUid).getUID());
		if (clctcompmodelStatusNumeral != null)
			clctcompmodelStatusNumeral.clear();
		CollectableComponentModel clctcompmodelStatusIcon = modelDetails.getCollectableComponentModelFor(SF.STATEICON.getMetaData(this.entityUid).getUID());
		if (clctcompmodelStatusIcon != null)
			clctcompmodelStatusIcon.clear();

		final UID statusId = getUsageCriteriaStatusIdFromView(false);
		if (statusId != null) {
			// check permission of attributes
			for (CollectableComponentModel clctcompmodel : modelDetails.getCollectableComponentModels()) {
				if(clctcompmodel != null ) {
					final CollectableEntityField clctef = clctcompmodel.getEntityField();
					final Permission permission = SecurityCache.getInstance().getAttributePermission(entityUid, clctef.getUID(), statusId);
					if(permission == null)
						clctcompmodel.clear();
				}
			}
		}
	}

	/**
	 * §postcondition result != null
	 * §postcondition result.isComplete()
	 */
	@Override
	public CollectableGenericObjectWithDependants newCollectable() {
		final UID moduleUid = getModuleId();
		assert moduleUid != null;
		final CollectableGenericObjectWithDependants result = newCollectableGenericObject(new GenericObjectVO(moduleUid, null));
		assert result != null;
		setSubsequentStatesVisible(false, false);
		setStatesDefaultPathVisible(true, false);
		return result;
	}

	@Override
	protected void deleteCollectable(CollectableGenericObjectWithDependants clct, final Map<String, Serializable> applyMultiEditContext)
			throws CommonBusinessException {
		assert !isHistoricalView();

		markCollectableAsDeleted(clct, applyMultiEditContext);
	}

	private GenericObjectVO getSelectedGenericObjectCVO() {
		final CollectableGenericObject clctlo = getSelectedCollectable();
		return (clctlo == null) ? null : clctlo.getGenericObjectCVO();
	}

	private void markCollectableAsDeleted(final CollectableGenericObjectWithDependants clctlo,
										  final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		invoke(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				try {
					lodelegate.remove(clctlo.getGenericObjectWithDependantsCVO(),
							false, getCustomUsage());
					clearRemovedMatrixData();
				} catch (CommonBusinessException cbe) {
					handleBusinessExceptionAfterSafe(cbe);
					throw cbe;
				}
			}
		}, applyMultiEditContext);
	}

	/**
	 * @deprecated Move to ResultController hierarchy.
	 */
	@Deprecated
	protected void checkedDeleteCollectablePhysically(
			final CollectableGenericObjectWithDependants clctlo)
			throws CommonBusinessException {
		if (!isPhysicallyDeleteAllowed(clctlo))
			throw new CommonPermissionException(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.41",
							"Endg\u00fcltiges L\u00f6schen ist nicht erlaubt."));

		invoke(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				lodelegate.remove(clctlo.getGenericObjectWithDependantsCVO(),
						true, getCustomUsage());
			}
		}, null);
		broadcastCollectableEvent(clctlo, MessageType.DELETE_DONE);

		if(!isClosed()) {
			UIUtils.invokeOnDispatchThread(new Runnable() {
				@Override
				public void run() {
					GenericObjectCollectController.this.getResultTableModel().remove(clctlo);
				}
			});
	
			setHistoricalDate(null);
	
			getResultController().getSearchResultStrategy().refreshResult();
		}
	}

	@Override
	public void checkedRestoreCollectable(final CollectableGenericObjectWithDependants clct, final
		Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		// NUCLOS-1850
		// if (!isPhysicallyDeleteAllowed(clct))
		if (!isDeleteAllowed(clct))
			throw new CommonPermissionException(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.100",
							"Wiederherstellen ist nicht erlaubt."));
		invoke(new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				lodelegate.restore(clct.getGenericObjectWithDependantsCVO(), getCustomUsage());
			}
		}, applyMultiEditContext);
	}

	/**
	 * §todo Check if we need to call getCompleteSelectedCollectables for
	 *       deletion
	 *       
	 * @deprecated This is a workaround for visibility issues of
	 *             getSelectedCollectables(), only used for deletion of objects.
	 */
	@Deprecated
	public final List<CollectableGenericObjectWithDependants> getListOfSelectedCollectables() {
		return super.getSelectedCollectables();
	}

	/**
	 * @return the module id for leased objects collected in this controller.
	 *         <code>null</code> for "general search".
	 */
	public final UID getModuleId() {
		return moduleUid;
	}

	/**
	 * @return the module id of the selected Collectable, if any. The module id
	 *         of this controller, otherwise.
	 */
	protected UID getSelectedCollectableModuleId() {
		final GenericObjectVO govo = getSelectedGenericObjectCVO();
		return (govo == null) ? getModuleId() : govo.getModule();
	}

	/**
	 * @deprecated Not in use - remove it.
	 */
	@Deprecated
	private Set<UID> getSelectedSubEntityUIDs() {
		return Collections.emptySet();
	}

	@Override
	public final UID getEntityName() {
		return getModuleId();
	}

	protected final String getEntityName(UID moduleUid) {
		return Modules.getInstance().getModule(moduleUid).getEntityName();
	}

	@Override
	protected String getEntityLabel() {
		return getModuleLabel(getModuleId());
	}

	private final String getModuleLabel(UID moduleUid) {
		return SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(getModuleId()));
	}

	/**
	 * @return the id of the current (selected) entity object, if any.
	 */
	private Long getSelectedGenericObjectId() {
		return getSelectedCollectableId();
	}

	/**
	 * @return the state of the selected entity object, if any.
	 */
	private CollectableField getSelectedGenericObjectState() {
		final CollectableField result;
		final Collectable clct = getSelectedCollectable();
		if (clct == null)
			result = null;
		else {
			result = clct.getField(SF.STATE_UID.getUID(clct.getEntityUID()));
			LOG.debug("getCurrentGenericObjectState: (value=" + result.getValue() + ", id=" + result.getValueId() + ")");
		}
		return result;
	}

	/**
	 * @return the state name of the selected entity object, if any.
	 */
	private String getSelectedGenericObjectStateName() {
		final CollectableField clctfState = getSelectedGenericObjectState();
		return (clctfState == null) ? null : getSpringLocaleDelegate().getResource(
				StateDelegate.getInstance().getStatemodelClosure(getModuleId()).getResourceSIdForLabel((UID)clctfState.getValue()),
				LangUtils.toString(clctfState.getValue()));
	}

	/**
	 * @return the state id of the selected entity object, if any.
	 */
	private UID getSelectedGenericObjectStateId() {
		final CollectableField clctfState = getSelectedGenericObjectState();
		return (clctfState == null) ? null : (UID) clctfState.getValueId();
	}

	/**
	 * @return the state numeral of the selected entity object, if any.
	 */
	private UID getSelectedGenericObjectStateUid() {
		final UID result;
		final Collectable clct = getSelectedCollectable();
		if (clct == null)
			result = null;
		else
			result = (UID) clct.getValueId(SF.STATE_UID.getMetaData(clct.getEntityUID()).getUID());

		return result;
	}
	
	private UID getSelectedGenericObjectMandatorUid() {
		final UID result;
		if (!MetaProvider.getInstance().getEntity(getModuleId()).isMandator()) {
			return null;
		}
		final Collectable clct = getSelectedCollectable();
		if (clct == null)
			result = null;
		else
			result = (UID) clct.getValueId(SF.MANDATOR_UID.getMetaData(clct.getEntityUID()).getUID());

		return result;
	}

	public StateWrapper getStateWrapperForState(UID stateUID) {UID id = null;
		Integer iNumeral = null;
		String sName = null;
		String sDescription = null;
		NuclosImage icon = null;
		String color = null;
		ResourceVO resButtonIcon = null;
		
		StateVO statevo = StateDelegate.getInstance().getState(getModuleId(), stateUID);
		id = statevo.getId();
		iNumeral = statevo.getNumeral();
		sName = statevo.getStatename(LocaleDelegate.getInstance().getLocale());
		sDescription = SpringLocaleDelegate.getInstance().getResource(
				StateDelegate.getInstance().getResourceSIdForDescription(statevo.getId()),
				statevo.getDescription(LocaleDelegate.getInstance().getLocale()));
		icon = statevo.getIcon();
		color = statevo.getColor();
		resButtonIcon = statevo.getButtonIcon();
		
		return new StateWrapper(id, iNumeral, sName, icon, sDescription, color,resButtonIcon);
	}
	
	public StateWrapper getStateWrapperFromSelectedGenericObject() {
		final Collectable clct = getSelectedCollectable();
		if (clct != null) {
			return getStateWrapperForState((UID) clct.getValueId(SF.STATE.getMetaData(clct.getEntityUID()).getUID()));
		}
		return new StateWrapper(null, null, null, null, null, null, null);
	}

	/**
	 * Delete the selected object physically. This is mostly a copy from
	 * CollectController.cmdDelete; just the message and the called delete
	 * method are different.
	 */
	protected void cmdDeletePhysically() {
		assert getCollectStateModel().getCollectState().equals(
				new CollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_VIEW));

		if (stopEditingInDetails()) {
			final String sMessage = getSpringLocaleDelegate().getMessage("GenericObjectCollectController.73","Soll der angezeigte Datensatz ({0}) wirklich endg\u00fcltig aus der Datenbank gel\u00f6scht werden?\nDieser Vorgang kann nicht r\u00fcckg\u00e4ngig gemacht werden! ", 
					SpringLocaleDelegate.getInstance().getIdentifierLabel(
							getSelectedCollectable(), getSelectedCollectable().getEntityUID(), MetaProvider.getInstance(), getLanguage()));			
			
			final int iBtn = JOptionPane.showConfirmDialog(getTab(), sMessage,									
					getSpringLocaleDelegate().getMessage("GenericObjectCollectController.24","Datensatz endg\u00fcltig l\u00f6schen"), JOptionPane.YES_NO_OPTION);

			if (iBtn == JOptionPane.OK_OPTION)
				UIUtils.runCommand(getTab(), new Runnable() {
					@Override
					public void run() {
						try {
							// try to find next or previous object:
							final JTable tblResult = getResultTable();
							final int iSelectedRow = tblResult.getSelectedRow();
							if (iSelectedRow < 0)
								throw new IllegalStateException();

							final int iNewSelectedRow = GOUtils.getNewSelectedRow(iSelectedRow, tblResult.getRowCount());

							checkedDeleteCollectablePhysically(getSelectedCollectable());

							if(GenericObjectCollectController.this.isClosed()) {
								return;
							}
							
							if (iNewSelectedRow == -1) {
								setCollectState(CollectState.OUTERSTATE_RESULT, CollectState.RESULTMODE_NOSELECTION);
							} else {
								tblResult.setRowSelectionInterval(iNewSelectedRow, iNewSelectedRow);
								// go into view mode again:
								setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_VIEW);
							}
						} catch (CommonPermissionException ex) {
							final String sErrorMessage = getSpringLocaleDelegate().getMessage(
											"GenericObjectCollectController.70",
											"Sie verf\u00fcgen nicht \u00fcber die ausreichenden Rechte, um dieses Objekt zu l\u00f6schen.");
							Errors.getInstance().showExceptionDialog(getTab(), sErrorMessage, ex);
						} catch (CommonBusinessException ex) {
							if (!handlePointerException(ex))
								Errors.getInstance().showExceptionDialog(getTab(),
										getSpringLocaleDelegate().getMessage(
														"GenericObjectCollectController.18",
														"Das Objekt konnte nicht gel\u00f6scht werden."),
										ex);
						}
					}
				});
		}
	}

	private void cmdJumpToTree() {
		UIUtils.runCommand(getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonFinderException, CommonPermissionException {
				CollectableGenericObjectWithDependants cgo = getSelectedCollectable();
				if( cgo != null)
					getExplorerController().showInOwnTab(ExplorerDelegate.getInstance().getGenericObjectTreeNode(cgo.getId(),
							cgo.getGenericObjectWithDependantsCVO().getModule()));
			}
		});
	}

	private void cmdShowStateHistory() {
		UIUtils.runCommand(getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final CollectableGenericObject clctSelected = getSelectedCollectable();

				final Long iGenericObjectId = clctSelected.getGenericObjectCVO().getId();
				final String sIdentifier =  SpringLocaleDelegate.getInstance().getIdentifierLabel(
						clctSelected, clctSelected.getEntityUID(), MetaProvider.getInstance(), getLanguage());
						
				new StateHistoryController(getTab()).run(getSelectedCollectableModuleId(), iGenericObjectId,
						sIdentifier);
			}
		});
	}

	private void cmdShowLogBook() {
		if (getCollectState().isDetailsMode() && getCollectState().getInnerState() == CollectState.DETAILSMODE_VIEW) {
			UIUtils.runCommand(getTab(), new Runnable() {
				@Override
				public void run() {
					final CollectableGenericObject clctSelected = getSelectedCollectable();

					assert clctSelected != null;

					final Long iGenericObjectId = clctSelected.getGenericObjectCVO().getId();
					final String sIdentifier =  SpringLocaleDelegate.getInstance().getIdentifierLabel(
							clctSelected, clctSelected.getEntityUID(), MetaProvider.getInstance(), getLanguage());

					try {
						/** @todo frame vs. parent */
						new LogbookController(getTab(), getSelectedCollectableModuleId(),
								iGenericObjectId, getPreferences()).run(sIdentifier);
					} catch (CommonBusinessException ex) {
						Errors.getInstance().showExceptionDialog(getTab(), ex);
					}
				}
			});
		}
	}

	/**
	 * command: switch to View mode
	 */
	@Override
	protected void cmdEnterViewMode() {
		if (isSelectedCollectableMarkedAsDeleted())
			if (!SecurityCache.getInstance().isActionAllowed(Actions.ACTION_READ_DELETED_RECORD))
				throw new NuclosFatalException(
						getSpringLocaleDelegate().getMessage(
										"GenericObjectCollectController.65",
										"Sie haben nicht die erforderlichen Rechte, gel\u00f6schte Datens\u00e4tze zu lesen."));

		final UsageCriteria uc = getUsageCriteria(getSelectedCollectable());
		final Statemodel sm = StateDelegate.getInstance().getStatemodel(uc);
		if(sm == null) {
			JOptionPane.showMessageDialog(Main.getInstance().getMainFrame(),
					SpringLocaleDelegate.getInstance().getMessage(
							"GenericObjectCollectController.109", "Der Datensatz kann nicht angezeigt werden, da kein gültiges Statusmodell ermittelt werden konnte." +
									"\\nDie Statusmodellverwaltung finden Sie im Menü Konfiguration."),
					MetaProvider.getInstance().getEntity(entityUid).getEntityName(),
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		super.cmdEnterViewMode();
	}

	/**
	 * alternative entry point: view single historical object
	 * 
	 * @param clct
	 *            the object to view in details
	 */
	@Override
	public final void runViewSingleHistoricalCollectable(CollectableWithDependants clct, Date dateHistorical,
			boolean bUseNewHistory) {
		this.bUseNewHistory = bUseNewHistory;
		viewSingleHistoricalCollectable(clct, dateHistorical);

		try {
			// switch to edit mode:
			setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_EDIT);
		} catch (CommonBusinessException e) {
			LOG.warn("Error setting detailsmode edit.", e);
		}

		getTab().setVisible(true);
	}

	private void viewSingleHistoricalCollectable(CollectableWithDependants clct, Date dateHistorical) {
		// fill result table:
		final List<CollectableGenericObjectWithDependants> lstResult = new ArrayList<CollectableGenericObjectWithDependants>();
		lstResult.add((CollectableGenericObjectWithDependants) clct);
		this.fillResultPanel(lstResult);

		getCollectPanel().getResultPanel().getResultTable().setRowSelectionInterval(0, 0);
		// select the one result row

		setHistoricalDate(dateHistorical);

		cmdEnterViewMode();
		disableToolbarButtonsForHistoricalView();
	}

	private void disableToolbarButtonsForHistoricalView() {
		final CollectPanel<Long,CollectableGenericObjectWithDependants> collectPanel = getCollectPanel();
		collectPanel.setTabbedPaneEnabledAt(CollectState.OUTERSTATE_SEARCH,false);
		collectPanel.setTabbedPaneEnabledAt(CollectState.OUTERSTATE_RESULT,false);
		getDetailsController().getDeleteCurrentCollectableAction().setEnabled(false);
		getCloneAction().setEnabled(false);
		actDeleteSelectedCollectablesPhysically.setEnabled(false);
		actExecuteRule.setEnabled(false);
		actMakeTreeRoot.setEnabled(false);
		getRefreshCurrentCollectableAction().setEnabled(false);
		actShowHistory.setEnabled(false);
		setOldHistoryVisible(false);
		actShowStateHistory.setEnabled(false);
		getLastAction().setEnabled(false);
		getFirstAction().setEnabled(false);
		getNewAction().setEnabled(false);
	}

	/**
	 * @param lstclct the objects to view in Result or Details panel
	 * @param bShowInDetails Show in Details panel? (false: show in Result panel)
	 */
	private void viewMultipleCollectables(List<CollectableGenericObjectWithDependants> lstclct,
			boolean bShowInDetails) {
		// set search condition to match the result (so that refresh will give
		// the same result):
		final Collection<Object> collIds = CollectionUtils.transform(lstclct, new Collectable.GetId());
		final CollectableSearchCondition cond = SearchConditionUtils.getCollectableSearchConditionForIds(collIds);
		setCollectableSearchConditionInSearchPanel(cond);

		// fill result table:
		this.fillResultPanel(lstclct);

		// select all result rows:
		getCollectPanel().getResultPanel().getResultTable().setRowSelectionInterval(0, lstclct.size() - 1);

		if (bShowInDetails)
			cmdEnterMultiViewMode();
		else
			UIUtils.runCommand(getTab(), new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					GenericObjectCollectController.this.setCollectState(CollectState.OUTERSTATE_RESULT,
							CollectState.RESULTMODE_NOSELECTION);
				}
			});
	}

	/**
	 * shows/hides the buttons for switching to subsequent states
	 * 
	 * @param bVisible
	 * @param bEnableButtons
	 */
	private void setSubsequentStatesVisible(boolean bVisible, boolean bEnableButtons) {
		actChangeState.putValue(ToolBarItemAction.ACTION_DISABLED, Boolean.TRUE);

		// initialize:
		states.clear();

		if (!bVisible)
			actChangeState.putValue(ToolBarItemAction.VISIBLE, Boolean.FALSE);
		else {
			actChangeState.putValue(ToolBarItemAction.VISIBLE, Boolean.TRUE);
			actChangeState.putValue(ToolBarItemAction.ACTION_DISABLED,
					(!bEnableButtons || states.size() == 0) ? Boolean.TRUE : Boolean.FALSE);
			final Long iGenericObjectId = getSelectedGenericObjectId();
			final UID genObjUid = getSelectedCollectable().getGenericObjectCVO().getModule();
			
			if (iGenericObjectId != null) {
				// Create a temporary list for sorting the entries before
				// entering into combo box
				final List<StateWrapper> lstComboEntries = new ArrayList<StateWrapper>();

				final StateWrapper stateCurrent = getStateWrapperFromSelectedGenericObject();
				if (stateCurrent != null)
					lstComboEntries.add(stateCurrent);

				UsageCriteria uc = getUsageCriteria(getSelectedCollectable());
				DynamicAttributeVO av = getSelectedCollectable().getGenericObjectCVO().getAttribute(SF.STATE.getUID(genObjUid));

				List<StateVO> lstSubsequentStates;
				if (canStateChangeFromRecordGrant(getSelectedCollectable().getGenericObjectCVO())) {
					lstSubsequentStates = StateDelegate.getInstance().getStatemodel(uc).getSubsequentStates(av.getValueUid(), false);
				} else {
					lstSubsequentStates = new ArrayList<StateVO>();
				}

				// Copy all subsequent states to the sorting list:
				GOUtils.copySubsequentStates(lstSubsequentStates, stateCurrent, lstComboEntries, uc, getSelectedCollectable());

				// Sort and finally enter the items into the combo box:
				Collections.sort(lstComboEntries);
				for (StateWrapper state : lstComboEntries)
					if (state != null)
						states.add(state);

				Object[] items = new Object[states.size()];
				for (int i = 0; i < states.size(); i++) {
					StateWrapper sw = states.get(i);
					items[i] = sw;
				}

				actChangeState.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, null); // reset item selected index.
				actChangeState.putValue(ToolBarItemAction.ITEM_ARRAY, items);
				actChangeState.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX, states.indexOf(stateCurrent));
				actChangeState.putValue(ToolBarItemAction.MENU_LABEL_COLOR, stateCurrent != null ? stateCurrent.getColor() : null);

				actChangeState.putValue(ToolBarItemAction.ACTION_DISABLED,
						(!bEnableButtons || states.size() == 0) ? Boolean.TRUE : Boolean.FALSE);
			}
		}

		actChangeState.putValue(ToolBarItemAction.ACTION_DISABLED,
				(!bEnableButtons || states.size() == 0) ? Boolean.TRUE : Boolean.FALSE);
	}

	void updateStatesForMultiEdit() {
		// show the buttons for subsequent states only if all objects are in the same state:
		final int iDetailsMode = getCollectStateModel().getDetailsMode();
		if (doTheSelectedGenericObjectsShareACommonState()) {
			setSubsequentStatesVisible(true, iDetailsMode == CollectState.DETAILSMODE_MULTIVIEW);
			setStatesDefaultPathVisible(true, iDetailsMode == CollectState.DETAILSMODE_MULTIVIEW);
		} else {
			setSubsequentStatesVisible(false, false);
			setStatesDefaultPathVisible(false, false);
		}
	}
	
	protected void respectMultiEditable(Collection<CollectableComponent> collclctcomp, CollectState collectstate) {
		if (SecurityCache.getInstance().isWriteAllowedForMasterData(entityUid) || getCollectableEntity().getMeta().isEditable()) {
			for (CollectableComponent clctcomp : collclctcomp) {
				if (!clctcomp.isMultiEditable()) {
					clctcomp.setReadOnly(true);
				}
			} // for
		}
	}

	/**
	 * shows/hides the buttons for switching to states default path
	 * 
	 * @param bVisible
	 * @param bEnableButtons
	 */
	private void setStatesDefaultPathVisible(boolean bVisible, boolean bEnableButtons) {
		// remove all previous listeners:
		cmpStateStandardView.removeActionListeners();

		// initialize:
		cmpStateStandardView.removeAllItems();
		cmpStateStandardView.setSelectedItem(null);

		if (!bVisible)
			cmpStateStandardView.setVisible(false);
		else {
			cmpStateStandardView.setVisible(true);
			cmpStateStandardView.setEnabled(bEnableButtons);

			// Create a temporary list for sorting the entries before entering into combo box
			final List<StateWrapper> lstDefaultPathEntries = new ArrayList<StateWrapper>();

			final StateWrapper stateCurrent = getStateWrapperFromSelectedGenericObject();

			UsageCriteria uc = null;
			List<StateVO> lstDefaultPathStates = new ArrayList<StateVO>();

			try {
				uc = getSelectedCollectable() == null ? getUsageCriteriaFromView(false)
						: getUsageCriteria(getSelectedCollectable());
				lstDefaultPathStates.addAll(StateDelegate.getInstance()
						.getStatemodel(uc).getDefaultStatePath());
			} catch (Exception e) {
				// ignore
			}

			// Copy all subsequent states to the sorting list:
			GOUtils.copySubsequentStates(lstDefaultPathStates, stateCurrent, lstDefaultPathEntries, uc, getSelectedCollectable());

			cmpStateStandardView.setSelectedItem(getSelectedCollectable() == null
							|| getSelectedCollectable().getId() == null ? null : stateCurrent);
			cmpStateStandardView.addItems(lstDefaultPathEntries);

			for (Iterator<StateWrapper> iterator = lstDefaultPathEntries.iterator(); iterator.hasNext();) {
				final StateWrapper item = iterator.next();
				final ActionListener al = new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent ev) {
						UIUtils.runShortCommand(getTab(), new CommonRunnable() {
							@Override
							public void run() {
								if (item != stateCurrent && item != null
										&& item.getId() != null) {
									cmdChangeStates(getTab(), stateCurrent, item, item.getStatesBefore());
								}
							}
						});
					}
				};
				cmpStateStandardView.addActionListener(al, item);

				if (stateCurrent != null && LangUtils.equal(stateCurrent.getNumeral(), item.getNumeral())) {
					final List<StateWrapper> lstSubsequentEntries = new ArrayList<StateWrapper>();
					List<StateVO> lstSubsequentStates;
					if (canStateChangeFromRecordGrant(getSelectedCollectable().getGenericObjectCVO())) {
						lstSubsequentStates = StateDelegate.getInstance().getStatemodel(uc).getSubsequentStates(item.getId(), false);
					} else {
						lstSubsequentStates = new ArrayList<StateVO>();
					}

					// Copy all subsequent states to the sorting list:
					for (StateVO statevo : lstSubsequentStates)
						if (statevo == null)
							// we don't want to throw an exception here, so we
							// just log the error:
							LOG.error("Die Liste der Folgestatus enth\u00e4lt ein null-Objekt.");
						else if (!lstSubsequentEntries
								.contains(new StateWrapper(
										statevo.getId(),
										statevo.getNumeral(),
										statevo.getStatename(LocaleDelegate.getInstance().getLocale()),
										statevo.getIcon(),
										SpringLocaleDelegate
												.getInstance()
												.getResource(
														StateDelegate
																.getInstance()
																.getResourceSIdForDescription(
																		statevo.getId()),
														statevo.getDescription(LocaleDelegate.getInstance().getLocale())),
										null, null)))
							lstSubsequentEntries
									.add(new StateWrapper(
											statevo.getId(),
											statevo.getNumeral(),
											getSpringLocaleDelegate()
													.getResource(
															StateDelegate
																	.getInstance()
																	.getStatemodelClosure(
																			getModuleId())
																	.getResourceSIdForLabel(
																			statevo.getId()),
															statevo.getStatename(LocaleDelegate.getInstance().getLocale())),
											statevo.getIcon(),
											SpringLocaleDelegate
													.getInstance()
													.getResource(
															StateDelegate
																	.getInstance()
																	.getResourceSIdForDescription(
																			statevo.getId()),
															statevo.getDescription(LocaleDelegate.getInstance().getLocale())),
											statevo.getColor(), statevo
													.getButtonIcon()));

					Map<StateWrapper, Action> mpSubsequentStatesAction = new HashMap<StateWrapper, Action>();
					for (Iterator<StateWrapper> iterator2 = lstSubsequentEntries
							.iterator(); iterator2.hasNext();) {
						final StateWrapper subsequentState = iterator2.next();
						if (!subsequentState.getNumeral().equals(
								item.getNumeral())) {
							Action act = new AbstractAction() {

								@Override
								public void actionPerformed(ActionEvent e) {
									cmdChangeState(subsequentState);
								}
							};
							mpSubsequentStatesAction.put(subsequentState, act);
						}
					}
					cmpStateStandardView.addSubsequentStatesActionListener(
							item, mpSubsequentStatesAction);
				}
			}
		}

		cmpStateStandardView
				.setEnabled(getSelectedCollectable() == null ? false
						: (bEnableButtons && cmpStateStandardView
								.getItemCount() != 0));
	}

	public List<StateVO> getPossibleSubsequentStates() {
		if (canStateChangeFromRecordGrant(getSelectedCollectable().getGenericObjectCVO())) {
			UsageCriteria uc = getUsageCriteria(getSelectedCollectable());
			DynamicAttributeVO av = getSelectedCollectable().getGenericObjectCVO().getAttribute(SF.STATE.getUID(getSelectedCollectable().getGenericObjectCVO().getModule()));
			return StateDelegate.getInstance().getStatemodel(uc).getSubsequentStates(av.getValueUid(), true);
		} else {
			return new ArrayList<StateVO>();
		}
	}

	private void showCustomActions(int iDetailsMode) {
		final boolean bWasDetailsChangedIgnored = isDetailsChangedIgnored();
		setDetailsChangedIgnored(true);

		try {
			final boolean bSingle = CollectState
					.isDetailsModeViewOrEdit(iDetailsMode);
			final boolean bMulti = CollectState
					.isDetailsModeMultiViewOrEdit(iDetailsMode);
			final boolean bViewOrEdit = bSingle || bMulti;
			final boolean bView = bViewOrEdit
					&& !CollectState.isDetailsModeChangesPending(iDetailsMode);

			this.getDetailsPanel().removeToolBarComponents(toolbarCustomActionsDetails);
			
			toolbarCustomActionsDetails.clear();
			
			// button: "print details":
			if (bMulti) {
				actPrintDetails.setEnabled(bView && hasFormsAssigned(getSelectedCollectables()));
			} else {
				actPrintDetails.setEnabled(!isHistoricalView() && bView && hasFormsAssigned(getSelectedCollectable()));
			}
			
			if (!bViewOrEdit) {
				actPrintDetails.setEnabled(bView
						&& hasFormsAssigned(getSelectedCollectable()));
			} else {
				// button: "print details":
				if (bSingle) {
					actPrintDetails.setEnabled(!isHistoricalView()
							&& bView && hasFormsAssigned(getSelectedCollectable()));
				}
			}
			
			if (bViewOrEdit) {
				if (!isHistoricalView()) {
					// buttons/actions for "generate leased object":
					if (!isSelectedCollectableMarkedAsDeleted()) {
						setupGeneratorActions(bView);
					}
				}
				UIUtils.ensureMinimumSize(getTab());
			}

			this.getDetailsPanel().addToolBarComponents(
					toolbarCustomActionsDetails);
		} finally {
			setDetailsChangedIgnored(bWasDetailsChangedIgnored);
		}
	}

	protected final ExplorerController getExplorerController() {
		return getMainController().getExplorerController();
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the common field "value" of the given Collectables. This value is
	 *         built using the given <code>transformer</code> on each
	 *         <code>Collectable</code>'s field with the given name. The
	 *         transformer will usually be <code>GetValue</code> or
	 *         <code>GetValueId</code>.
	 * @throws NoSuchElementException
	 *             if there is no common field "value" (or if the given
	 *             <code>Collection</code> is empty)
	 */
	private static Object getCollectablesCommonField(Collection<? extends Collectable> collclct, UID field,
		Transformer<CollectableField, ?> transformer) throws NoSuchElementException {
		if (collclct.isEmpty())
			throw new NoSuchElementException();
		final Iterator<? extends Collectable> iter = collclct.iterator();
		final Object result =  transformer.transform(iter.next().getField(field));
		while (iter.hasNext())
			if (!LangUtils.equal(result, transformer.transform(iter.next().getField(field))))
				throw new NoSuchElementException();
		return result;
	}

	private UID getSelectedGenericObjectsCommonFieldIdByFieldName(UID field) throws NoSuchElementException {
		return (UID) getCollectablesCommonField(getSelectedCollectables(), field, new CollectableField.GetValueId());
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return the common state numeral of the selected leased objects.
	 * @throws NoSuchElementException
	 *             if there is no common state numeral (or if there is no
	 *             selected object at all)
	 */
	private Integer getSelectedGenericObjectsCommonStateNumeral() throws NoSuchElementException {
		final Integer result = (Integer) getCollectablesCommonField(getSelectedCollectables(), SF.STATENUMBER.getMetaData(getModuleId()).getUID(), new CollectableField.GetValue());
		assert result != null;
		return result;
	}

	/**
	 * @return do the selected leased objects share a common state?
	 */
	private boolean doTheSelectedGenericObjectsShareACommonState() {
		boolean result;
		try {
			getSelectedGenericObjectsCommonStateNumeral();
			result = true;
		} catch (NoSuchElementException ex) {
			result = false;
		}
		return result;
	}

	/**
	 * §precondition !this.isHistoricalView()
	 * §precondition this.getCollectState().isDetailsMode() NUCLEUSINT-1159
	 *               needed for accessing the statechange for status button
	 *               
	 * @param stateNew
	 */
	public void cmdChangeState(final StateWrapper stateNew) {
		if (isHistoricalView()) {
			throw new IllegalStateException(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.90",
					"Statuswechsel ist in historischer Ansicht nicht m\u00f6glich."));			
		}
		
		if (!getCollectState().isDetailsMode() && !getCollectState().isResultMode()) {
			throw new IllegalStateException(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.91",
					"Statuswechsel ist nur in Detail- order Ergebnisansicht m\u00f6glich."));			
		}

		final StateWrapper stateCurrent = getStateWrapperFromSelectedGenericObject();
		if (LangUtils.equal(stateNew, stateCurrent)) {
			return;					
		}

		//ARCD-1571
		if (stateNew.isTransitionNonstop(stateCurrent.getId())) {
			stopEditingInDetails();
			changeState(stateNew);
			return;
		}

		
		OverlayOptionPane.showConfirmDialog(getTab(),
			new ChangeStatePanel(stateCurrent, stateNew),
			getSpringLocaleDelegate().getMessage("GenericObjectCollectController.85", "Statuswechsel durchf\u00fchren")
					+ "?", JOptionPane.YES_NO_OPTION, true, new OvOpAdapter() {
				@Override
				public void done(int result) {
					final boolean ok = (result == JOptionPane.YES_OPTION);

					stopEditingInDetails();

					if (ok) {
						// RSWORGA-390 Vor dem Statuswechsel prüfen ob der Datensatz noch aktuell ist
						// wenn nicht, ist ein Statuswechsel nicht erlaubt
						if(!getCollectStateModel().performVersionCheck(true))
						{
							resetState(stateCurrent);
							try {
								refreshCurrentCollectable();
							} catch (CommonBusinessException e) {
								throw new CommonFatalException(e);
							}
							return;
						}
						changeState(stateNew);
					} else {
						if (stateCurrent != null) {
							resetState(stateCurrent);
						}
					}
				}
			});
	}

	private void resetState(final StateWrapper stateCurrent) {
		actChangeState.putValue(
				ToolBarItemAction.ACTION_DISABLED,
				Boolean.TRUE);
		actChangeState.putValue(
				ToolBarItemAction.ITEM_SELECTED_INDEX,
				states.indexOf(stateCurrent));
		actChangeState.putValue(
				ToolBarItemAction.ACTION_DISABLED,
				Boolean.FALSE);
		cmpStateStandardView.setSelectedItem(stateCurrent);
	}

	/**
	 * §precondition !this.isHistoricalView()
	 * §precondition this.getCollectState().isDetailsMode() NUCLEUSINT-1159
	 *               needed for accessing the statechange for status button
	 *               
	 * @param stateNew
	 * @return Did the user press ok?
	 */
	public void cmdChangeStates(final MainFrameTab tab, final StateWrapper stateSource, final StateWrapper stateFinal, final List<UID> statesNew) {
		if (isHistoricalView()) {
			throw new IllegalStateException(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.90",
					"Statuswechsel ist in historischer Ansicht nicht m\u00f6glich."));			
		}

		final boolean bMultiEdit;
		if (getCollectState().isDetailsMode()) {
			bMultiEdit = getCollectState().isDetailsModeMultiViewOrEdit();
		} else if (getCollectState().isResultMode()) {
			bMultiEdit = getSelectedCollectables().size() > 1;
		}
		
		if (stateFinal.isTransitionNonstop(stateSource.getId())) {
			stopEditingInDetails();
			changeStates(stateFinal, statesNew);
			return;
		}

		OverlayOptionPane.showConfirmDialog(tab, new ChangeStatePanel(stateSource, stateFinal),
			getSpringLocaleDelegate().getMessage("GenericObjectCollectController.85", "Statuswechsel durchf\u00fchren")
					+ "?", JOptionPane.YES_NO_OPTION, true, new OvOpAdapter() {
				@Override
				public void done(int result) {
					final boolean ok = (result == JOptionPane.YES_OPTION);

					stopEditingInDetails();

					if (ok) {
						changeStates(stateFinal, statesNew);
					} else {
						try {
							if (stateSource != null) {
								cmpStateStandardView.setSelectedItem(stateSource);
							}
							broadcastCollectableEvent(GenericObjectCollectController.this.getCompleteSelectedCollectable(), 
									MessageType.STATECHANGE_UNDONE);
						} catch (Exception e) {
							// ignore.
						}
					}
				}
			});
	}

	/**
	 * §precondition !this.isHistoricalView()
	 * §precondition this.getCollectState().isDetailsMode() NUCLEUSINT-1159
	 *               needed for accessing the statechange for status button
	 */
	protected void changeState(final StateWrapper stateNew) {
		final boolean bMultiEdit = getCollectState()
				.isDetailsModeMultiViewOrEdit();

		stopEditingInDetails();

		final StateWrapper stateCurrent = getStateWrapperFromSelectedGenericObject();

		class ChangeStateWorker1 implements CommonRunnable {

			private ChangeStateWorker1() {
			}

			@Override
			public void run() throws CommonBusinessException {
				if (getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_EDIT
						|| getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_NEW_CHANGED) {
					if (bMultiEdit) {
						CommonRunnable cr = new CommonRunnable() {
							@Override
							public void run()
									throws CommonBusinessException {
								GenericObjectCollectController.this
										.changeStateForMultipleObjects(stateNew);
							}
						};
						getSubFormsLoader().addAfterLoadingRunnable(cr);
						try {
							save();
						} catch (CommonBusinessException ex) {
							getSubFormsLoader().removeAfterLoadingRunnable(cr);
							final String sErrorMsg = getSpringLocaleDelegate()
									.getMessage(
											"GenericObjectCollectController.34",
											"Der Statuswechsel konnte nicht vollzogen werden.");
							handleSaveException(ex, sErrorMsg);

							// redisplay the old status
							// cmbbxCurrentState.setSelectedItem(stateCurrent);
							actChangeState.putValue(
									ToolBarItemAction.ACTION_DISABLED,
									Boolean.TRUE);
							actChangeState.putValue(
									ToolBarItemAction.ITEM_SELECTED_INDEX,
									states.indexOf(stateCurrent));
							actChangeState.putValue(
									ToolBarItemAction.ACTION_DISABLED,
									Boolean.FALSE);
							cmpStateStandardView.setSelectedItem(stateCurrent);
						}
					} else {
						GenericObjectCollectController.this
								.changeStateForSingleObjectAndSave(stateNew);
					}

				} else if (bMultiEdit)
					GenericObjectCollectController.this
							.changeStateForMultipleObjects(stateNew);
				else
					GenericObjectCollectController.this
							.changeStateForSingleObjectAndSave(stateNew);
			}
		}

		UIUtils.runShortCommand(getTab(), new ChangeStateWorker1());
	}

	/**
	 * §precondition !this.isHistoricalView()
	 * §precondition this.getCollectState().isDetailsMode() NUCLEUSINT-1159
	 *               needed for accessing the statechange for status button
	 */
	private void changeStates(final StateWrapper stateFinal, final List<UID> statesNew) {
		final boolean bMultiEdit;
		if (getCollectState().isDetailsMode()) {
			bMultiEdit = getCollectState().isDetailsModeMultiViewOrEdit();
		} else if (getCollectState().isResultMode()) {
			bMultiEdit = getSelectedCollectables().size() > 1;
		} else {
			throw new IllegalArgumentException("Illegal collect state");
		}

		stopEditingInDetails();

		final StateWrapper stateCurrent = getStateWrapperFromSelectedGenericObject();

		class ChangeStateWorker2 implements CommonRunnable {

			private ChangeStateWorker2() {
			}

			@Override
			public void run() throws CommonBusinessException {
				if (getCollectState().isResultMode()) {
					if (bMultiEdit)
						GenericObjectCollectController.this
								.changeStatesForMultipleObjects(stateFinal,
										statesNew);
					else
						GenericObjectCollectController.this
								.changeStatesForSingleObjectAndSave(statesNew);
				} else if (getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_EDIT
						|| getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_NEW_CHANGED) {
					if (bMultiEdit) {
						CommonRunnable cr = new CommonRunnable() {
							@Override
							public void run()
									throws CommonBusinessException {
								GenericObjectCollectController.this
										.changeStatesForMultipleObjects(
												stateFinal, statesNew);
							}
						};
						getSubFormsLoader().addAfterLoadingRunnable(cr);
						try {
							save();
						} catch (CommonBusinessException ex) {
							getSubFormsLoader().removeAfterLoadingRunnable(cr);
							final String sErrorMsg = getSpringLocaleDelegate()
									.getMessage(
											"GenericObjectCollectController.34",
											"Der Statuswechsel konnte nicht vollzogen werden.");
							handleSaveException(ex, sErrorMsg);

							// redisplay the old status
							// cmbbxCurrentState.setSelectedItem(stateCurrent);
							actChangeState.putValue(
									ToolBarItemAction.ACTION_DISABLED,
									Boolean.TRUE);
							actChangeState.putValue(
									ToolBarItemAction.ITEM_SELECTED_INDEX,
									states.indexOf(stateCurrent));
							actChangeState.putValue(
									ToolBarItemAction.ACTION_DISABLED,
									Boolean.FALSE);
							cmpStateStandardView.setSelectedItem(stateCurrent);
						}
					} else {
						GenericObjectCollectController.this
								.changeStatesForSingleObjectAndSave(statesNew);
					}

				} else if (bMultiEdit)
					GenericObjectCollectController.this
							.changeStatesForMultipleObjects(stateFinal,
									statesNew);
				else
					GenericObjectCollectController.this
							.changeStatesForSingleObjectAndSave(statesNew);
			}
		}

		UIUtils.runShortCommand(getTab(), new ChangeStateWorker2());
	}

	private void changeStateForSingleObjectAndSave(final StateWrapper stateNew) {
		class ChangeStateWorker3 extends CommonClientWorkerAdapter<Long,CollectableGenericObjectWithDependants> {

			Long iGenericObjectId;
			UID iModuleId;
			StateWrapper stateCurrent;
			CollectableGenericObjectWithDependants clct;

			boolean errorOccurred = false;


			private ChangeStateWorker3(CollectController<Long,CollectableGenericObjectWithDependants> clt) {
				super(clt);
			}

			@Override
			public void init() throws CommonBusinessException {
				super.init();
				UIUtils.invokeOnDispatchThread(new Runnable() {				
					@Override
					public void run() {
						stateChangeStartInit();
					}
				});
				iGenericObjectId = GenericObjectCollectController.this
						.getSelectedGenericObjectId();
				iModuleId = getSelectedCollectableModuleId();
				assert iGenericObjectId != null;

				stateCurrent = getStateWrapperFromSelectedGenericObject();
				clct = GenericObjectCollectController.this
						.getCompleteSelectedCollectable();
			}

			@Override
			public void work() throws CommonBusinessException {
				boolean hasError = false;
				try {
					isProcessingStateChange.set(true);
					if (GenericObjectCollectController.this.changesArePending()) {
						// NUCLOSINT-1114:
						// Value must be 'true' to save the changed SubForm data
						// to DB. (Thomas Pasch)
						// tsc: the implementation with dbUpdate-parameter
						// skipped a lot of data collection logic, which was the
						// reason for the NUCLOSINT-1114.
						// Final servercall for database update is now skipped
						// by setting a ThreadLocal variable.
						// TODO Best solution would be to refactor and call all
						// data collection logic in
						// prepareCollectableForSaving(), but this would take
						// some time.
						final CollectableGenericObjectWithDependants updated = GenericObjectCollectController.this
								.updateCurrentCollectable();
						invoke(new CommonRunnable() {
							@Override
							public void run() throws CommonBusinessException {
								StateDelegate
										.getInstance()
										.changeStateAndModify(
												iModuleId,
												updated.getGenericObjectWithDependantsCVO(),
												stateNew.getId(),
												getCustomUsage());
								ChangeStateWorker3.this.finishAndReload();
							}
						}, null);
					} else {
						invoke(new CommonRunnable() {
							@Override
							public void run() throws CommonBusinessException {
								StateDelegate.getInstance().changeState(
										iModuleId, iGenericObjectId,
										stateNew.getId(), getCustomUsage());
								ChangeStateWorker3.this.finishAndReload();
							}
						}, null);
					}
				} catch (CommonBusinessException e) {
					hasError = true;
					throw e;
				} finally {
					isProcessingStateChange.set(false);
					final boolean bOk = !hasError;
					UIUtils.invokeOnDispatchThread(new Runnable() {				
						@Override
						public void run() {
							if (bOk) {
								stateChangeFinishedOk();
							} else {
								stateChangeFinishedWithError();
							}
						}
					});
				}
			}

			private void finishAndReload() throws CommonBusinessException {
				broadcastCollectableEvent(clct, MessageType.STATECHANGE_DONE);

				// We have to reload the current leased object, as some
				// fields might have changed:
				// . nuclosState because of the status change
				// . intVersion not to forget (NUCLOS-5049)
				// . other fields because of business rules
				if (!errorOccurred) {
					GenericObjectCollectController.this.refreshCurrentCollectable(false);
				}
			}

			@Override
			public void paint() throws CommonBusinessException {
				super.paint();
			}

			@Override
			public void handleError(Exception ex) {
				errorOccurred = true;
				if (!handleCommonValidationException(ex)) {
					if (GenericObjectCollectController.this
							.handlePointerException(ex)) {
						//this does not work anymore.
						/*final PointerException pex = PointerException
								.extractPointerExceptionIfAny(ex);
						if (pex != null) {
							GenericObjectCollectController.this
									.setCollectableComponentModelsInDetailsMandatoryAdded(pex
											.getPointerCollection().getFields());
						}*/
					} else if (!(ex instanceof UserCancelledException)) {
						if (!handleSpecialException(ex)) {
							final String sErrorMsg = getSpringLocaleDelegate()
									.getMessage(
											"GenericObjectCollectController.34",
											"Der Statuswechsel konnte nicht vollzogen werden.");
							Errors.getInstance().showExceptionDialog(getTab(),
									sErrorMsg, ex);
						}
					}
				} else {
					final CommonValidationException cve = NuclosExceptions.getCause(ex, CommonValidationException.class);
					if (cve != null && cve.getFieldErrors() != null) {
						final Set<UID> stNotMandatoryByState = new HashSet<UID>();

						try {
							readValuesFromEditPanel(clct, false);
							prepareCollectableForSaving(clct, getCollectableEntityForDetails());
						} catch (CollectableValidationException e2) {
							// ignore. should not occure.
						}

						final EntityObjectVO object = DalSupportForGO.wrapGenericObjectVO(clct.getGenericObjectCVO());
						final EntityMeta<?> meta = MetaProvider.getInstance().getEntity(getEntityUid());
						for (FieldMeta<?> fieldmeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(getEntityUid()).values()) {
							if (!fieldmeta.isNullable()) {
								final Object value;
								if (fieldmeta.getForeignEntity() != null) {
									if ("genericObject".equalsIgnoreCase(fieldmeta.getFieldName())) {
										continue;
									}
									value = object.getFieldId(fieldmeta.getUID());
								}
								else {
									value = object.getFieldValue(fieldmeta.getUID());
								}
								if (value == null) {
									stNotMandatoryByState.add(fieldmeta.getUID());
								}
							}
						}

						final Set<UID> fields = new HashSet<UID>();
						for (FieldValidationError error : cve.getFieldErrors()) {
							if (getEntityName().equals(error.getEntity()))
								if (!stNotMandatoryByState.contains(error.getField()))
									fields.add(error.getField());
						}
						GenericObjectCollectController.this.setCollectableComponentModelsInDetailsMandatoryAdded(fields);
					}
				}

				// redisplay the old status
				// cmbbxCurrentState.setSelectedItem(stateCurrent);
				actChangeState.putValue(ToolBarItemAction.ACTION_DISABLED,
						Boolean.TRUE);
				actChangeState.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX,
						states.indexOf(stateCurrent));
				actChangeState.putValue(ToolBarItemAction.ACTION_DISABLED,
						Boolean.FALSE);
				cmpStateStandardView.setSelectedItem(stateCurrent);
			}
		}

		CommonMultiThreader.getInstance().execute(
				new ChangeStateWorker3(GenericObjectCollectController.this));
	}

	private void changeStatesForSingleObjectAndSave(final List<UID> statesNew) {
		class ChangeStatesWorker4 extends CommonClientWorkerAdapter<Long,CollectableGenericObjectWithDependants> {

			Long iGenericObjectId;
			UID iModuleId;
			StateWrapper stateInitial;
			StateWrapper stateCurrent;
			CollectableGenericObjectWithDependants clct;

			boolean errorOccurred = false;

			private ChangeStatesWorker4(CollectController<Long,CollectableGenericObjectWithDependants> clt) {
				super(clt);
			}

			@Override
			public void init() throws CommonBusinessException {
				super.init();
				UIUtils.invokeOnDispatchThread(new Runnable() {				
					@Override
					public void run() {
						stateChangeStartInit();
					}
				});
				iGenericObjectId = GenericObjectCollectController.this
						.getSelectedGenericObjectId();
				iModuleId = getSelectedCollectableModuleId();
				assert iGenericObjectId != null;

				stateInitial = getStateWrapperFromSelectedGenericObject();
				stateCurrent = stateInitial;
				clct = GenericObjectCollectController.this
						.getCompleteSelectedCollectable();
			}

			@Override
			public void work() throws CommonBusinessException {
				boolean hasError = false;
				try {
					isProcessingStateChange.set(true);
					for (final UID stateNew : statesNew) {
						if (GenericObjectCollectController.this
								.changesArePending()) {
							// NUCLOSINT-1114:
							// Value must be 'true' to save the changed SubForm
							// data to DB. (Thomas Pasch)
							// tsc: the implementation with dbUpdate-parameter
							// skipped a lot of data collection logic, which was
							// the reason for the NUCLOSINT-1114.
							// Final servercall for database update is now
							// skipped by setting a ThreadLocal variable.
							// TODO Best solution would be to refactor and call
							// all data collection logic in
							// prepareCollectableForSaving(), but this would
							// take some time.
							final CollectableGenericObjectWithDependants updated = GenericObjectCollectController.this
									.updateCurrentCollectable();
							invoke(new CommonRunnable() {
								@Override
								public void run()
										throws CommonBusinessException {
									StateDelegate.getInstance().changeStateAndModify(
													iModuleId,
													updated.getGenericObjectWithDependantsCVO(),
													stateNew, getCustomUsage());
									// NUCLOS-5049 This must happen within the same thread!
									ChangeStatesWorker4.this.finishAndReload(stateNew);
								}
							}, null);
						} else {
							invoke(new CommonRunnable() {
								@Override
								public void run()
										throws CommonBusinessException {
									StateDelegate.getInstance().changeState(
											iModuleId, iGenericObjectId,
											stateNew, getCustomUsage());
									ChangeStatesWorker4.this.finishAndReload(stateNew);
								}
							}, null);
						}
					}

				} catch (CommonBusinessException e) {
					hasError = true;
					throw e;
				} finally {
					isProcessingStateChange.set(false);
					final boolean bOk = !hasError;
					UIUtils.invokeOnDispatchThread(new Runnable() {				
						@Override
						public void run() {
						if (bOk) {
							stateChangeFinishedOk();
						} else {
							stateChangeFinishedWithError();
						}
						}
					});
				}
			}

			private void finishAndReload(UID stateNew) throws CommonBusinessException {
				stateCurrent = getStateWrapperForState(stateNew);
				broadcastCollectableEvent(clct, MessageType.STATECHANGE_DONE);

				// We have to reload the current leased object, as some
				// fields might have changed:
				// . nuclosState because of the status change
				// . intVersion not to forget (NUCLOS-5049)
				// . other fields because of business rules
				if (!errorOccurred) {
					if (getCollectState().isResultMode()) {
						getResultController().getSearchResultStrategy().refreshResult();
						//setCollectState(CollectState.OUTERSTATE_RESULT,
						//CollectState.RESULTMODE_NOSELECTION); // will be done by refreshResult().
					} else {
						GenericObjectCollectController.this.refreshCurrentCollectable(false);
					}
				}
			}

			@Override
			public void paint() throws CommonBusinessException {
				super.paint();
			}

			@Override
			public void handleError(Exception ex) {
				errorOccurred = true;
				if (!handleCommonValidationException(ex)) {
					if (GenericObjectCollectController.this
							.handlePointerException(ex)) {
						//this does not work anymore.
					} else if (!(ex instanceof UserCancelledException)) {
						if (!handleSpecialException(ex)) {
							final String sErrorMsg = getSpringLocaleDelegate()
									.getMessage(
											"GenericObjectCollectController.34",
											"Der Statuswechsel konnte nicht vollzogen werden.");
							Errors.getInstance().showExceptionDialog(getTab(),
									sErrorMsg, ex);
						}
					}
				} else {
					final CommonValidationException cve = NuclosExceptions.getCause(ex, CommonValidationException.class);
					if (cve != null && cve.getFieldErrors() != null) {
						final Set<UID> stNotMandatoryByState = new HashSet<UID>();

						try {
							readValuesFromEditPanel(clct, false);
							prepareCollectableForSaving(clct, getCollectableEntityForDetails());
						} catch (CollectableValidationException e2) {
							// ignore. should not occure.
						}

						final EntityObjectVO object = DalSupportForGO.wrapGenericObjectVO(clct.getGenericObjectCVO());
						final EntityMeta<?> meta = MetaProvider.getInstance().getEntity(getEntityUid());
						for (FieldMeta<?> fieldmeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(getEntityUid()).values()) {
							if (!fieldmeta.isNullable()) {
								final Object value;
								if (fieldmeta.getForeignEntity() != null) {
									if ("genericObject".equalsIgnoreCase(fieldmeta.getFieldName())) {
										continue;
									}
									value = object.getFieldId(fieldmeta.getUID());
								}
								else {
									value = object.getFieldValue(fieldmeta.getUID());
								}
								if (value == null) {
									stNotMandatoryByState.add(fieldmeta.getUID());
								}
							}
						}

						final Set<UID> fields = new HashSet<UID>();
						for (FieldValidationError error : cve.getFieldErrors()) {
							if (getEntityName().equals(error.getEntity()))
								if (!stNotMandatoryByState.contains(error.getField()))
									fields.add(error.getField());
						}
						GenericObjectCollectController.this.setCollectableComponentModelsInDetailsMandatoryAdded(fields);
					}
				}
				// redisplay the old status
				// cmbbxCurrentState.setSelectedItem(stateCurrent);
				
				actChangeState.putValue(ToolBarItemAction.ACTION_DISABLED,
						Boolean.TRUE);
				actChangeState.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX,
						states.indexOf(stateCurrent));
				actChangeState.putValue(ToolBarItemAction.ACTION_DISABLED,
						Boolean.FALSE);
				cmpStateStandardView.setSelectedItem(stateCurrent);
				// if a state change occured, refresh is needed
				if (!stateCurrent.equals(stateInitial)) {
					cmdRefreshCurrentCollectable();
				}
			}
		}

		CommonMultiThreader.getInstance().execute(
				new ChangeStatesWorker4(GenericObjectCollectController.this));
	}

	@Override
	public void enableToolbarButtonsForDetailsMode(final int iDetailsMode) {
		super.enableToolbarButtonsForDetailsMode(iDetailsMode);
		if (iDetailsMode == CollectState.DETAILSMODE_VIEW) {
			setInitialComponentFocusInDetailsTab(); // @see NUCLOS-1027
		}
	}

	private void logTraceChangeStateForMultipleObjects(final StateWrapper stateNew, int sizeSelectedCollectables) {
		final LinkedList<UID> statesNew = new LinkedList<UID>(Collections.singleton(stateNew.getId()));
		
			final StateDelegate stateDelegate = StateDelegate.getInstance();
			final List<String> states = new ArrayList<String>();
			for (UID uid : statesNew) {
				states.add(StringifyHelper.toString(stateDelegate.getState(uid)));
			}
			LOG.trace("new states {}. Selected Collectables {}", StringUtils.join("," ,states), sizeSelectedCollectables);
			
		
	}
	
	private void changeStateForMultipleObjects(final StateWrapper stateNew) throws CommonBusinessException {
		final LinkedList<UID> statesNew = new LinkedList<UID>(Collections.singleton(stateNew.getId()));
		final List<CollectableGenericObjectWithDependants> selectedCollectables = getSelectedCollectables();
		int sizeSelectedCollectables = selectedCollectables.size();
		if (LOG.isTraceEnabled()) {
			// provide additional information for state transformation
			logTraceChangeStateForMultipleObjects(stateNew, sizeSelectedCollectables);
			
		}
		// transform CollectableGenericObjectWithDependants => Long (primary key)
		final Set<Long> pks = CollectionUtils.transformIntoSet(selectedCollectables, new Transformer<CollectableGenericObjectWithDependants, Long>(){

			@Override
			public Long transform(CollectableGenericObjectWithDependants clt) {
				return clt.getPrimaryKey();
			}
			
		});
		// assert the aggregated amount to be the same as the selected amount
		assert selectedCollectables.size() == pks.size();
		
		final ChangeStateForSelectedCollectablesController changeStateForSelectedCollectablesController = new ChangeStateForSelectedCollectablesController(this, stateNew, statesNew, pks);
		changeStateForSelectedCollectablesController.run(getMultiActionProgressPanel(sizeSelectedCollectables));
	}

	private void changeStatesForMultipleObjects(final StateWrapper stateFinal, final List<UID> statesNew) throws CommonBusinessException {
		final List<CollectableGenericObjectWithDependants> selectedCollectables = getSelectedCollectables();
		int sizeSelectedCollectables = getSelectedCollectables().size();
		if (LOG.isTraceEnabled()) {
			// provide additional information for state transformation
			logTraceChangeStateForMultipleObjects(stateFinal, sizeSelectedCollectables);
			
		}
		
		// transform CollectableGenericObjectWithDependants => Long (primary key)
		final Set<Long> pks = CollectionUtils.transformIntoSetRetainOrder(selectedCollectables, new Transformer<CollectableGenericObjectWithDependants, Long>(){

			@Override
			public Long transform(CollectableGenericObjectWithDependants clt) {
				return clt.getPrimaryKey();
			}
			
		});
		// assert the aggregated amount to be the same as the selected amount
		assert selectedCollectables.size() == pks.size();
				
		final ChangeStateForSelectedCollectablesController changeStateForSelectedCollectablesController = new ChangeStateForSelectedCollectablesController(this, stateFinal, statesNew, pks);
		changeStateForSelectedCollectablesController.run(getMultiActionProgressPanel(pks.size()));
	}

	protected boolean showObjectGenerationWarningIfNewObjectIsNotSaveable() {
		return true;
	}

	@Override
	public boolean save() throws CommonBusinessException {
		
		if (isHistoricalView()) {
			final int btn = JOptionPane.showConfirmDialog(
					getTab(),
					getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.102",
							"Wiederherstellen eines historischen Wertes"),
					getSpringLocaleDelegate().getMessage(
							"GenericObjectCollectController.103",
							"Mit dem Wiederherstellen eines historischen Wertes werden aktuell bestehende Werte in der Datanbank überschrieben.\nSind Sie sicher?"),
					JOptionPane.YES_NO_OPTION);
			
			if (btn != JOptionPane.YES_OPTION) {
				return false;	
			}
		}
	
		boolean saved = super.save();
		if (saved && bGenerated && iGenericObjectIdSources != null
			&& oGeneratorAction != null) {
			final EntityMeta sourceMeta = MetaProvider.getInstance().getEntity(oGeneratorAction.getSourceModule());
			if (sourceMeta.isStateModel()) {
				for (Long iGenericObjectIdSource : iGenericObjectIdSources) {
					lodelegate.relate(iGenericObjectIdSource, GenericObjectTreeNode.SystemRelationType.PREDECESSOR_OF.getValue(), getSelectedGenericObjectId(), getModuleId(), null, null, null);
				}
			}
			bGenerated = false;
			setGenerationSource(null, null);
		}
		return saved;
	}

	/**
	 * get the subform entity name which is used to attache documents to the
	 * generic object
	 * 
	 * @return entity name
	 */
	protected UID getDocumentSubformEntityUID() {
		return getDocumentSubformEntityName();
	}
	
	/**
	 * for backwards compatibility only...
	 */
	@Deprecated
	protected UID getDocumentSubformEntityName() {
		final EntityMeta<?> entityMeta = E.getByName(this.getEntityName(getModuleId()) + "document"); 
		if (entityMeta != null) {
			final UID sEntity = entityMeta.getUID();
			if(MasterDataDelegate.getInstance().hasEntity(sEntity))
				return sEntity;
		}

		return E.GENERALSEARCHDOCUMENT.getUID();
	}

	/**
	 * 
	 * @return the 4 column names of the document subform (comment, createdDate,
	 *         createdUser, filename). used for exporting a form.
	 */
	@Override
	protected UID[] getDocumentSubformColumns() {
		UID comment = UID.UID_NULL;
		UID createdDate = SF.CREATEDAT.getUID(getDocumentSubformEntityName());
		UID createdUser = SF.CREATEDBY.getUID(getDocumentSubformEntityName());;
		UID filename = UID.UID_NULL;
		
		for (FieldMeta<?> fieldMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(getDocumentSubformEntityName()).values()) {
			if ("comment".equals(fieldMeta.getFieldName())) {
				comment = fieldMeta.getUID();
			} else if ("filename".equals(fieldMeta.getFieldName())) {
				filename = fieldMeta.getUID();
			}
		}
		
		List<UID> result = new ArrayList<UID>();
		
		result.add(comment);
		result.add(createdDate);
		result.add(createdUser);
		result.add(filename);
		
		return result.toArray(new UID[] {});
	}

	/**
	 * @param bSearchPanel
	 *            Use SearchPanel? (false: use Details panel)
	 * @return the value ids of the quintuple fields from the view (Search or
	 *         Details panel)
	 * @throws CollectableFieldFormatException
	 */
	private UsageCriteria getUsageCriteriaFromView(boolean bSearchPanel) throws CollectableFieldFormatException {
		final UID iModuleId = bSearchPanel ? getModuleId() : getSelectedCollectableModuleId();
		UID processUid = getUsageCriteriaProcessIdFromView(bSearchPanel);
		if (processUid == null) {
			processUid = process != null ? (UID) process.getValueId() : null;
		}
		return new UsageCriteria(iModuleId, 
				processUid,
//						(getProcess() != null) ? (UID) getProcess().getValueId() : getUsageCriteriaProcessIdFromView(bSearchPanel),
				getUsageCriteriaStatusIdFromView(bSearchPanel), getCustomUsage()
		);
	}

	/**
	 * @param bSearchPanel
	 *            Use SearchPanel? (false: use Details panel)
	 * @param sSystemAttributeKey
	 *            key of the quintuple field name in system parameters
	 * @return the value id of the given quintuple field
	 * @throws CollectableFieldFormatException
	 */
	private UID getUsageCriteriaProcessIdFromView(boolean bSearchPanel)
			throws CollectableFieldFormatException {
		
		// 1. makeConsistent:
		for (CollectableComponent clctcomp : getEditView(bSearchPanel).getCollectableComponentsFor(SF.PROCESS.getUID(this.entityUid)))
			clctcomp.makeConsistent();

		// 2. read model:
		final CollectableComponentModel clctcompmodel = getEditView(bSearchPanel).getModel().getCollectableComponentModelFor(SF.PROCESS.getUID(this.entityUid));
		if (clctcompmodel == null) {
			return null;
		}
		if (bSearchPanel) {
			SearchComponentModel searchclctcompmodel = (SearchComponentModel) clctcompmodel;
			final CollectableSearchCondition searchCondition = searchclctcompmodel.getSearchCondition();
			if (searchCondition instanceof AtomicCollectableSearchCondition) {
				if (ComparisonOperator.EQUAL != ((AtomicCollectableSearchCondition)searchCondition).getComparisonOperator()) {
					// NUCLOS-7222
					return null;
				}
			}
		}
		return (UID) clctcompmodel.getField().getValueId();
	}

	/**
	 * @param bSearchPanel
	 *            Use SearchPanel? (false: use Details panel)
	 * @param sSystemAttributeKey
	 *            key of the quintuple field name in system parameters
	 * @return the value id of the given quintuple field
	 * @throws CollectableFieldFormatException
	 */
	private UID getUsageCriteriaStatusIdFromView(boolean bSearchPanel)
			throws CollectableFieldFormatException {
		if (bSearchPanel) {
			// 1. makeConsistent:
			for (CollectableComponent clctcomp : getEditView(bSearchPanel).getCollectableComponentsFor(SF.STATE.getMetaData(getModuleId()).getUID()))
				clctcomp.makeConsistent();

			// 2. read model:
			final CollectableComponentModel clctcompmodel = getEditView(bSearchPanel).getModel().getCollectableComponentModelFor(SF.STATE.getMetaData(getModuleId()).getUID());
			return (clctcompmodel == null) ? null : (UID) clctcompmodel.getField().getValueId();
		} else {
			Collectable clct = getSelectedCollectable();
			final UID iModuleId = bSearchPanel ? getModuleId() : getSelectedCollectableModuleId();
			if (clct == null || getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_NEW) { // it is a new collectable.
				UsageCriteria uc = new UsageCriteria(iModuleId,
						getUsageCriteriaProcessIdFromView(bSearchPanel),
						null, getCustomUsage()
				);
				return getInitialStateId(uc);
			}

			return getSystemAttributeId(getSelectedCollectable(), SF.STATE.getMetaData(iModuleId).getUID());
		}
	}

	/**
	 * @param clct
	 * @return the UsageCriteria contained in the given Collectable.
	 */
	@Override
	protected UsageCriteria getUsageCriteria(Collectable clct) {
		return new UsageCriteria(((CollectableGenericObject)clct).getGenericObjectCVO().getModule(),
				getSystemAttributeId(((CollectableGenericObject)clct), SF.PROCESS_UID.getMetaData(
						((CollectableGenericObject)clct).getGenericObjectCVO().getModule()).getUID()),
				getSystemAttributeId(((CollectableGenericObject)clct), SF.STATE_UID.getMetaData(
						((CollectableGenericObject)clct).getGenericObjectCVO().getModule()).getUID()), getCustomUsage()
		);
	}

	private static UID getSystemAttributeId(CollectableGenericObject clct, UID field) {
		return clct.getCollectableEntity().getFieldUIDs().contains(field) ? (UID) clct.getField(field).getValueId() : null;
	}

	/**
	 * §precondition CollectionUtils.isNonEmpty(collclct)
	 * 
	 * @return the greatest common quintuple contained in the given Collection
	 */
	@Override
	protected UsageCriteria getGreatestCommonUsageCriteriaFromCollectables(Collection<CollectableGenericObjectWithDependants> collclct) {
		class GetUsageCriteria implements Transformer<CollectableGenericObject, UsageCriteria> {
			@Override
			public UsageCriteria transform(CollectableGenericObject clct) {
				return getUsageCriteria(clct);
			}
		}
		return UsageCriteria.getGreatestCommonUsageCriteria(CollectionUtils.transform(collclct, new GetUsageCriteria()));
	}

	private static boolean isUsageCriteriaField(UID entity, UID field) {
		boolean result = false;
		
		if (SF.STATE_UID.getUID(entity).equals(field) || SF.PROCESS_UID.getUID(entity).equals(field))
			result = true;
		
		return result;
	}

	private CollectableComponentModelListener getCollectableComponentModelListenerForUsageCriteriaFields(
			boolean bSearchPanel) {
		return bSearchPanel ? ccmlistenerUsageCriteriaFieldsForSearch
				: ccmlistenerUsageCriteriaFieldsForDetails;
	}

	private static int getPanelIndex(boolean bSearchPanel) {
		return bSearchPanel ? 0 : 1;
	}

	/**
	 * @param bSearchPanel
	 * @return Have the quintuple field listeners for the given panel been added
	 *         already?
	 */
	private boolean getUsageCriteriaFieldListenersAdded(boolean bSearchPanel) {
		return abUsageCriteriaFieldListenersAdded[getPanelIndex(bSearchPanel)];
	}

	private void setUsageCriteriaFieldListenersAdded(boolean bSearchPanel,
			boolean bValue) {
		abUsageCriteriaFieldListenersAdded[getPanelIndex(bSearchPanel)] = bValue;
	}

	/**
	 * adds the quintuple field listeners to the given panel.
	 * 
	 * §precondition !this.getUsageCriteriaFieldListenersAdded(bSearchPanel)
	 * §postcondition this.getUsageCriteriaFieldListenersAdded(bSearchPanel)
	 */
	private void addUsageCriteriaFieldListeners(boolean bSearchPanel) {
		if (getUsageCriteriaFieldListenersAdded(bSearchPanel))
			throw new IllegalStateException();
		this.addUsageCriteriaFieldListeners(
				getEditView(bSearchPanel).getModel(),
				getCollectableComponentModelListenerForUsageCriteriaFields(bSearchPanel));

		setUsageCriteriaFieldListenersAdded(bSearchPanel, true);

		assert getUsageCriteriaFieldListenersAdded(bSearchPanel);
	}

	/**
	 * removes the quintuple field listeners to the given panel, if they have
	 * been added before.
	 * 
	 * §postcondition !this.getUsageCriteriaFieldListenersAdded(bSearchPanel)
	 */
	private void removeUsageCriteriaFieldListeners(boolean bSearchPanel) {
		if (getUsageCriteriaFieldListenersAdded(bSearchPanel)) {
			final EditView view = getEditView(bSearchPanel);
			if (view != null) {
				removeUsageCriteriaFieldListeners(
						getEditView(bSearchPanel).getModel(),
						getCollectableComponentModelListenerForUsageCriteriaFields(bSearchPanel));
			}
			setUsageCriteriaFieldListenersAdded(bSearchPanel, false);
		}
		assert !getUsageCriteriaFieldListenersAdded(bSearchPanel);
	}

	private void addUsageCriteriaFieldListeners(
			CollectableComponentModelProvider clctcompmodelprovider,
			CollectableComponentModelListener clctcomplistener) {
		for (UID sUsageCriteriaFieldName : getUsageCriteriaFieldNames(getModuleId())) {
			final CollectableComponentModel clctcompmodel = clctcompmodelprovider
					.getCollectableComponentModelFor(sUsageCriteriaFieldName);
			if (clctcompmodel != null) {
				LOG.debug("add listener for field " + clctcompmodel.getFieldUID());
//				clctcompmodel.addCollectableComponentModelListener(null, clctcomplistener);
				ListenerUtil.registerCollectableComponentModelListener(
						clctcompmodel, null, clctcomplistener);
			}
		}
	}

	private List<UID> getUsageCriteriaFieldNames(UID entity) {
		List<UID> retVal = new ArrayList();
		retVal.add(SF.PROCESS_UID.getUID(entity));
		retVal.add(SF.STATE_UID.getUID(entity));
		return retVal;
	}

	private void removeUsageCriteriaFieldListeners(
			CollectableComponentModelProvider clctcompmodelprovider,
			CollectableComponentModelListener clctcomplistener) {
		for (UID sUsageCriteriaFieldName : getUsageCriteriaFieldNames(getModuleId())) {
			final CollectableComponentModel clctcompmodel = clctcompmodelprovider
					.getCollectableComponentModelFor(sUsageCriteriaFieldName);
			if (clctcompmodel != null) {
				LOG.debug("remove listener for field " + clctcompmodel.getFieldUID());
				clctcompmodel
						.removeCollectableComponentModelListener(clctcomplistener);
			}
		}
	}

	protected void setSearchStatesAccordingToUsageCriteria(
			UsageCriteria usagecriteria) {
		getSearchStateBox().setProperty("usagecriteria", usagecriteria);
	}

	/**
	 * @param cond
	 * @param bClearSearchFields
	 * @throws CommonBusinessException
	 * 
	 * @deprecated Move to SearchController hierarchy and make protected again.
	 */
	@Deprecated
	@Override
	public void setSearchFieldsAccordingToSearchCondition(
			CollectableSearchCondition cond, boolean bClearSearchFields)
			throws CommonBusinessException {
		final boolean bUsageCriteriaFieldListenersWereAdded = getUsageCriteriaFieldListenersAdded(true);
		this.removeUsageCriteriaFieldListeners(true);

		final boolean bUsageCriteriaChanged;
		if (bReloadingLayout)
			bUsageCriteriaChanged = true;
		else {
			// 1. If we are not reloading the layout already, do that first:
			final UsageCriteria usagecriteriaOld = getUsageCriteriaFromView(true);
			final UsageCriteria usagecriteria = getUsageCriteriaFromSearchCondition(cond);
			bUsageCriteriaChanged = !usagecriteria.equals(usagecriteriaOld);

			if (bUsageCriteriaChanged) {
				// Note that the usagecriteria field listeners are removed here:
				this.reloadLayout(usagecriteria, getCollectState(), false,
						false);
				setSearchStatesAccordingToUsageCriteria(usagecriteria);
			}
		}

		assert !getUsageCriteriaFieldListenersAdded(true);

		try {
			clearSearchCondition();
			// 2. fill in fields, ignoring changes in the quintuple fields as
			// the right layout is loaded already:
			super.setSearchFieldsAccordingToSearchCondition(cond,
					bClearSearchFields && !bUsageCriteriaChanged);
		} finally {
			if (bUsageCriteriaFieldListenersWereAdded)
				this.addUsageCriteriaFieldListeners(true);
		}
	}

	@Override
	protected void _setSearchFieldsAccordingToSubCondition(CollectableSubCondition cond) throws CommonBusinessException {
		final UID sEntityNameSub = cond.getSubEntityUID();
		final SearchConditionSubFormController subformctl = mpsubformctlSearch
				.get(sEntityNameSub);
		if (subformctl == null)
			throw new NuclosFatalException(getSpringLocaleDelegate().getMessage("GenericObjectCollectController.40","Ein Unterformular f\u00fcr die Entit\u00e4t {0} ist in der Suchbedingung, aber nicht im aktuellen Layout enthalten.", sEntityNameSub));
		subformctl.setCollectableSearchCondition(cond.getSubCondition());
	}

	@Override
	protected void _clearSearchFields() {
		final boolean bUsageCriteriaFieldListenersWereAdded = getUsageCriteriaFieldListenersAdded(true);
		this.removeUsageCriteriaFieldListeners(true);

		CollectableComponentModel clctcompmodelUsageCriteriaFieldChanged = null;
		// remember if field have to change
		for (UID sUsageCriteriaFieldName : getUsageCriteriaFieldNames(getModuleId())) {
			final CollectableComponentModel clctcompmodel = 
					getEditView(true).getModel().getCollectableComponentModelFor(sUsageCriteriaFieldName);
			if (clctcompmodel != null) {
				if (!clctcompmodel.getField().isNull()) {
					clctcompmodelUsageCriteriaFieldChanged = clctcompmodel;
					break;
				}
			}
		}
		CollectableField oldUsageCriteriaField = clctcompmodelUsageCriteriaFieldChanged == null ? null
				: clctcompmodelUsageCriteriaFieldChanged.getField();

		try {
			for (CollectableComponent clctcomp : getSearchPanel().getEditView().getCollectableComponents()) {
				if (getProcess() == null || !isCalledByProcessMenu() || !isProcessField(clctcomp.getFieldUID())) {
					clctcomp.clear();
				}
			}
		} finally {
			if (bUsageCriteriaFieldListenersWereAdded)
				this.addUsageCriteriaFieldListeners(true);
		}

		for (SearchConditionSubFormController subformctl : getSubFormControllersInSearch())
			subformctl.clear();

		getSearchStateBox().getJComboBox().setSelectedIndex(-1);

		// invoke.
		if (clctcompmodelUsageCriteriaFieldChanged != null) {
			LinkedHashSet<CollectableComponentModel> source = new LinkedHashSet<>();
			source.add(clctcompmodelUsageCriteriaFieldChanged);
			getCollectableComponentModelListenerForUsageCriteriaFields(true)
					.collectableFieldChangedInModel(
							new CollectableComponentModelEvent(
									source,
									oldUsageCriteriaField,
									clctcompmodelUsageCriteriaFieldChanged
											.getField()));
		}
	}

	/**
	 * @param cond
	 * @return the quintuple contained in the atomic fields of the given
	 *         condition.
	 */
	private UsageCriteria getUsageCriteriaFromSearchCondition(
			CollectableSearchCondition cond) {
		return getUsageCriteriaFromFieldsMap(SearchConditionUtils
				.getAtomicFieldsMap(cond));
	}

	private UsageCriteria getUsageCriteriaFromFieldsMap(Map<UID, CollectableField> mpFields) {
		return new UsageCriteria(getModuleId(),
				getProcessIdFromUsageCriteriaField(mpFields),
				getStatusIdFromUsageCriteriaField(mpFields), getCustomUsage());
	}

	private UID getProcessIdFromUsageCriteriaField(Map<UID, CollectableField> mpFields) {
		final CollectableField clctfField = mpFields.get(SF.PROCESS_UID.getMetaData(this.entityUid).getUID());
		if (clctfField != null && !clctfField.isIdField()) return null;
		return (clctfField == null) ? null : (UID) clctfField.getValueId();
	}

	private UID getStatusIdFromUsageCriteriaField(Map<UID, CollectableField> mpFields) {
		final CollectableField clctfField = mpFields.get(SF.STATE_UID.getMetaData(this.entityUid).getUID());
		if (clctfField != null && !clctfField.isIdField()) return null;
		return (clctfField == null) ? null : (UID) clctfField.getValueId();
	}
	
	private void cmdExportXml() {
		LOG.info("cmdExportXml (go) pressed");
	}

	private void addTabbedPaneListener(LayoutRoot root) {
		List<JTabbedPane> lstTabs = new ArrayList<JTabbedPane>();
		searchTabbedPanes(root.getRootComponent(), lstTabs);
		for (JTabbedPane tabPane : lstTabs) {
			tabPane.addChangeListener(new JTabbedPaneChangeListener());
		}
	}

	private void cmdShowResultsInExplorer() {
		UIUtils.runCommand(getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				final ISearchStrategy<Long,CollectableGenericObjectWithDependants> ss = getSearchStrategy();
				String sFilterName = getEntityLabel() + " " + Integer.toString(++iFilter);

				SearchFilter current = state != null ? state.getCurrentSearchFilter() : null;
				if (current != null) {
					sFilterName = sFilterName + ": " + current.toString();
				}
				getExplorerController().showInOwnTab(new EntitySearchResultTreeNode(
						getSpringLocaleDelegate().getMessage("GenericObjectCollectController.93","Suchergebnis ({0})", 
						sFilterName),
					    null, 
					    ss.getInternalSearchExpression(), 
					    sFilterName, 
					    getMainController().getUserName(), 
					    getEntityUid(), null, null));
			}
		});
	}
	
	/**
	 * transfers the values/states of the given old collectable component models
	 * to the given new collectable component models.
	 */
	private void transferCollectableComponentModelsInDetailsPanel(
		Map<UID, DetailsComponentModel> mpclctcompmodelOld,
		Map<UID, DetailsComponentModel> mpclctcompmodelNew) {

		final Map<UID, DetailsComponentModel> transferredDetailsData = new HashMap<UID, DetailsComponentModel>();
		transferredDetailsData.putAll(mpclctcompmodelOld);
		for (DetailsComponentModel clctcompmodelNew : mpclctcompmodelNew.values()) {
			final UID field = clctcompmodelNew.getFieldUID();

			DetailsComponentModel clctcompmodelOld = transferredDetailsData.get(field);
			if (clctcompmodelOld != null)
				// 1. Transfer all fields from the old models to the new models
				clctcompmodelNew.assign(clctcompmodelOld);
			else {
				// 2. Set all additional fields (those contained in the new
				// models, but not in the old models) to their default values:
				final CollectableEntityField clctefNew = clctcompmodelNew
						.getEntityField();
				final CollectableField clctfDefault = clctefNew.getDefault();
				if (!clctfDefault.isNull()) {
					final boolean bFieldExistsInOldPanel = mpclctcompmodelOld.containsKey(clctcompmodelNew.getFieldUID());
					if (bFieldExistsInOldPanel)
						LOG.debug("Skipping field " + clctefNew.getUID().getString() + " as it is contained in the old panel.");
					else {
						LOG.debug("Setting field " + clctefNew.getUID().getString() + " to default value " + clctfDefault + ".");
						clctcompmodelNew.setField(clctfDefault);
					}
				}
			}
		}
	}

	private void transferSubFormData(
		Map<UID, DetailsSubFormController<Long, CollectableEntityObject<Long>>> mpOldSubFormControllers,
		Map<UID, DetailsSubFormController<Long, CollectableEntityObject<Long>>> mpNewSubFormControllers) {
		for (UID entity : mpOldSubFormControllers.keySet())
			if (!mpNewSubFormControllers.containsKey(entity))
				LOG.warn("Unterformular f\u00c3\u00bcr Entit\u00c3\u00a4t " + entity + " ist in der neuen Maske nicht enthalten.");
			else
				DetailsSubFormController.copyModel(mpOldSubFormControllers.get(entity), mpNewSubFormControllers.get(entity));
	}

	private void reloadLayoutForDetailsTab(
			final boolean bAddUsageCriteriaFieldListeners)
			throws CommonBusinessException {
		LOG.debug("GenericObjectCollectController.reloadLayoutForDetailsTab");
		this.reloadLayout(getCollectStateModel().getCollectState(),
				bAddUsageCriteriaFieldListeners);
	}

	private void reloadLayoutForSearchTab() throws CommonBusinessException {
		LOG.debug("BEGIN reloadLayoutForSearchTab");
		this.reloadLayout(getCollectStateModel().getCollectState(),
				getUsageCriteriaFieldListenersAdded(true));
		setSearchStatesAccordingToUsageCriteria(getUsageCriteriaFromView(true));
		LOG.debug("FINISHED reloadLayoutForSearchTab");
	}

	private void reloadLayout(CollectState collectstate,
			boolean bAddUsageCriteriaFieldListeners)
			throws CommonBusinessException {
		final boolean bSearchPanel = collectstate.isSearchMode();
		final UsageCriteria usagecriteria = getUsageCriteriaFromView(bSearchPanel);

		this.reloadLayout(usagecriteria, collectstate, true,
				bAddUsageCriteriaFieldListeners);
	}

	private UID iCurrentLayoutId = null;

	private boolean checkMustReloadLayout() {
		final CollectState collectstate = getCollectStateModel()
				.getCollectState();
		try {
			final boolean bSearchPanel = collectstate.isSearchMode();
			final UsageCriteria usagecriteria = getUsageCriteriaFromView(bSearchPanel);
			return checkMustReloadLayout(usagecriteria, collectstate);
		} catch (CollectableFieldFormatException e) {
			return false;
		}
	}

	private boolean checkMustReloadLayout(final UsageCriteria usagecriteria,
			final CollectState collectstate) {
		final boolean bSearchMode = collectstate.isSearchMode();
		/** @todo maybe factor this out in a protected method and override in GeneralSearchCollectController */
		final org.nuclos.common.collect.collectable.CollectableEntity clcte = bSearchMode ? getCollectableEntity() : CollectableGenericObjectEntity.getByModuleUid(usagecriteria.getEntityUID());
		final UID iLayout = GenericObjectLayoutCache.getInstance().getLayoutUid(clcte, usagecriteria, bSearchMode);
		return !LangUtils.equal(iCurrentLayoutId, iLayout);
	}

	protected void reloadLayout(final UsageCriteria usagecriteria,
			final CollectState collectstate, final boolean bTransferContents,
			final boolean bAddUsageCriteriaFieldListeners)
			throws CommonBusinessException {
		/** @todo don't do this unless the layout really changed! */
		if (bReloadingLayout)
			throw new IllegalStateException(
					"reloadLayout must not be called recursively!");

		boolean layoutSwitched = false;
		final boolean bSearchPanel = collectstate.isSearchMode();
		try {
			bReloadingLayout = true;

			final boolean bSearchMode = collectstate.isSearchMode();
			/** @todo maybe factor this out in a protected method and override in GeneralSearchCollectController */
			final org.nuclos.common.collect.collectable.CollectableEntity clcte = bSearchMode ? getCollectableEntity() : CollectableGenericObjectEntity.getByModuleUid(usagecriteria.getEntityUID());
			final UID iLayout = GenericObjectLayoutCache.getInstance().getLayoutUid(clcte, usagecriteria, bSearchMode);
			if (LangUtils.equal(iCurrentLayoutId, iLayout)) {
				getLayoutMLButtonsActionListener().clearInputMapForParentPanel(getCollectPanel());

				setupShortcutsForTabs(getTab());
				
				final CollectPanel<Long, CollectableGenericObjectWithDependants> panel = getCollectPanel();
				final JComponent comp = panel.getDetailsPanel().getEditComponent();
				if (collectstate.isDetailsMode() && comp != null) {
					getLayoutMLButtonsActionListener().setInputMapForParentPanel(panel, comp);
				}

				Collection<DetailsSubFormController<Long, CollectableEntityObject<Long>>> collsubformctls = getSubFormControllersInDetails();

				respectRights(getDetailsPanel().getEditView().getCollectableComponents(), collsubformctls, usagecriteria, collectstate);
				respectRights(getDetailsPanel().getEditView().getCollectableLabels(), collsubformctls, usagecriteria, collectstate);
				
				return;
			} else {//Classical "ReloadLayout For Change Process"
				layoutSwitched = true;
			}
			iCurrentLayoutId = iLayout;

			final LayoutRoot layoutroot = getLayoutFromCache(usagecriteria,
					collectstate);

			addTabbedPaneListener(layoutroot);

			this.removeUsageCriteriaFieldListeners(bSearchPanel);

			try {
				if (bSearchPanel)
					transferSearchPanel(layoutroot, bTransferContents);
				else {
					final JComponent compEditOld = getDetailsPanel()
							.getEditComponent();
					if (bTransferContents)
						transferDetailsPanel(layoutroot, compEditOld);
					else
						transferDetailsPanel(layoutroot,
								bTransferContents ? compEditOld : null);

					initMatrixControllers();
				}

				Collection<DetailsSubFormController<Long, CollectableEntityObject<Long>>> collsubformctls = getSubFormControllersInDetails();

				respectRights(getDetailsPanel().getEditView()
						.getCollectableComponents(), collsubformctls,
						usagecriteria, collectstate);
				respectRights(getDetailsPanel().getEditView()
						.getCollectableLabels(), collsubformctls, usagecriteria,
						collectstate);

				// ensure the (possibly new) edit panel is shown completely:
				UIUtils.ensureMinimumSize(getTab());

				setupSplitPane();

				// always revalidate:
				getTab().revalidate();
			} finally {
				if (bAddUsageCriteriaFieldListeners)
					GenericObjectCollectController.this
							.addUsageCriteriaFieldListeners(bSearchPanel);
			}
		} finally {
			bReloadingLayout = false;
			if (!bSearchPanel) {
				if (getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_NEW
						|| getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_NEW_CHANGED
						|| getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_NEW_SEARCHVALUE) {

					resetCollectableComponentModelsInDetailsMandatory();
					resetCollectableComponentModelsInDetailsMandatoryAdded();
					super.highlightMandatory();
					highlightMandatoryByState(getInitialStateId(usagecriteria));
				}
			}
			
			getLayoutMLButtonsActionListener()
					.fireComponentEnabledStateUpdate(
							getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_EDIT);
			
			if (layoutSwitched) {
				clearRemovedTabs();
				notifyAndSelectTabs(null, true);
			}
		}
	}
	
	protected void setupSplitPane() {
		final Collection<JSplitPane> colSp = UIUtils.findAllInstancesOf(getDetailsPanel(), JSplitPane.class);

		// add listener for split pane divider changes (update workspace preferences on change)
		for (final JSplitPane sp : colSp) {
			sp.removePropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, pclSplitPane);
			sp.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, pclSplitPane);
		}

		// initialize split pane settings 
		readSplitPaneStateFromPrefs(getCurrentLayoutUid(), getPreferences(),
				getDetailsPanel());
	}

	/**
	 * transfers the contents of the Details panel after reloading the layout.
	 * 
	 * @param layoutroot
	 * @param compEditOld
	 *            the old Edit panel for the Details tab (or null)
	 * @param compEditNew
	 *            the new Edit panel for the Details tab, as in
	 *            getEditPanel(..., false)
	 */
	private void transferDetailsPanel(final LayoutRoot layoutroot,
			JComponent compEditOld) {
		// transfer field contents from the old components to the new ones:
		final boolean bChangeListenersWereAdded = changeListenersForDetailsAdded();

		if (bChangeListenersWereAdded)
			removeChangeListenersForDetails();

		try {
			final Map<UID, DetailsComponentModel> mpclctcompNew = getMapOfDetailsComponentModels(layoutroot);

			// 0. set multi edit mode for CollectableComponentModels:
			final boolean bMultiEditMode = CollectState
					.isDetailsModeMultiViewOrEdit(getCollectStateModel()
							.getDetailsMode());
			if (bMultiEditMode)
				for (DetailsComponentModel clctcompmodel : mpclctcompNew
						.values())
					clctcompmodel.setMultiEditable(true);

			// 1. transfer data in collectable components:
			if (compEditOld != null) {
				final Map<UID, DetailsComponentModel> mpclctcompOld =
						CollectionUtils.typecheck(layoutrootDetails
								.getMapOfCollectableComponentModels(),
								DetailsComponentModel.class);
				transferCollectableComponentModelsInDetailsPanel(mpclctcompOld,
						mpclctcompNew);
			}

			final JComponent compEditNew = getDetailsPanel().newEditComponent(
					layoutroot.getRootComponent());
			JPanel pnl = new JPanel(new BorderLayout());
			pnl.add(cmpStateStandardView, BorderLayout.NORTH);
			pnl.add(compEditNew, BorderLayout.CENTER);
			getDetailsPanel().setEditView(DefaultEditView.newDetailsEditView(pnl, layoutroot, layoutroot.getInitialFocusEntityAndField()));

			// layoutrootDetails is used for the ordered field names:
			layoutrootDetails = layoutroot;

			final Collection<DetailsSubFormController<Long, CollectableEntityObject<Long>>> oldDetailsControllers
				= getSubFormControllersInDetails();

			// 2. transfer data in subforms:
			Map<UID, SubForm> mpSubForm = layoutroot.getMapOfSubForms();
			
			if(bMultiEditMode) {
				for(UID subformUid : mpSubForm.keySet()) {
					SubForm subform = mpSubForm.get(subformUid);
					subform.setMultiEdit(bMultiEditMode);
				}
			}
			final Map<UID, DetailsSubFormController<Long, CollectableEntityObject<Long>>> mpNewSubFormControllers =
				newDetailsSubFormControllers(mpSubForm);

			if (compEditOld != null)
				transferSubFormData(getMapOfSubFormControllersInDetails(),
						mpNewSubFormControllers);
			setMapOfSubFormControllersInDetails(mpNewSubFormControllers);
			
			CollectableField mandator = getMandator();
			if (mandator != null && mandator.getValueId() != null) {
				UID mandatorUID = (UID) mandator.getValueId();
				for (DetailsSubFormController<Long, CollectableEntityObject<Long>> subctrl : mpNewSubFormControllers.values()) {
					subctrl.setMandatorUID(mandatorUID);
				}
				for (UID sFieldName : getOrderedFieldNamesInDetails()) {
					for (CollectableComponent cc : layoutrootDetails.getCollectableComponentsFor(sFieldName)) {
						if (cc instanceof CollectableComponentWithValueListProvider) {
							CollectableComponentWithValueListProvider ccwvlp = (CollectableComponentWithValueListProvider) cc;
							if (VLPClientUtils.setVLPBaseRestrictions(sFieldName, ccwvlp.getValueListProvider(), getSelectedGenericObjectId(), mandatorUID)) {
								ccwvlp.refreshValueList(true);
							}
						}
					}
				}
			}

			// Note that the old subform controllers must be closed before
			// creating the new ones, so that the
			// column order and widths are preserved (first stored and then
			// read):
			closeSubFormControllers(oldDetailsControllers);
			closeMatrixControllers(getMatrixControllers());

			// 3. setup subform controllers
			setupSubFormController(mpSubForm, mpNewSubFormControllers);

			// 4. transfer custom data:
			if (compEditOld != null)
				transferCustomDataInDetailsPanel(layoutroot, compEditOld,
						compEditNew);
			
			// 5. add tab controller (collect controller) to layout components
			setTabControllerInLayoutComponents();
		} finally {
			if (bChangeListenersWereAdded)
				addChangeListenersForDetails();
		}
	}

	/**
	 * closes all subform controllers in the Search panel.
	 */
	private void closeSubFormControllersInSearch() {
		closeSubFormControllers(getSubFormControllersInSearch());
	}

	/**
	 * §precondition collsubformctl != null
	 */
	private static void closeSubFormControllers(
			Collection<? extends SubFormController> collsubformctl) {
		for (SubFormController subformctl : collsubformctl) {
			subformctl.close();
		}
		collsubformctl.clear();
	}
	
	private static void closeMatrixControllers(Collection<MatrixController> colMatrixController) {
		for(MatrixController mc : colMatrixController) {
			mc.close();
		}
	}

	/**
	 * Successors may transfer custom data from the old edit panel to the new
	 * edit panel after reloading the layout of the Details panel. The default
	 * implementation does nothing (no custom data).
	 * 
	 * @param layoutroot
	 * @param compEditOld
	 *            the old Edit panel for the Details tab
	 * @param compEditNew
	 *            the new Edit panel for the Details tab, as in
	 *            getEditPanel(..., false)
	 * @deprecated seems to be unused.
	 */
	@Deprecated
	protected void transferCustomDataInDetailsPanel(LayoutRoot layoutroot,
			JComponent compEditOld, JComponent compEditNew) {
		// Default: no custom data
	}

	/**
	 * transfers the (contents of the) Search panel after reloading the layout.
	 * 
	 * §precondition this.isSearchPanelVisible()
	 * 
	 * @param layoutroot
	 * @param bTransferContents
	 *            transfer contents also?
	 * @throws CommonBusinessException
	 */
	private void transferSearchPanel(final LayoutRoot layoutroot,
			boolean bTransferContents) throws CommonBusinessException {
		if (!isSearchPanelAvailable())
			throw new IllegalStateException("!this.isSearchPanelAvailable()");

		removeChangeListenersForSearch();

		EntitySearchFilter searchFilter = selectedSearchFilter;
		try {
			final CollectableSearchCondition searchcond = bTransferContents ? getCollectableSearchConditionFromSearchPanel(true)
					: null;

			// Note that the old subform controllers must be closed before
			// creating the new ones, so that the
			// column order and widths are preserved (first stored and then
			// read):
			closeSubFormControllersInSearch();

			getSearchPanel().setEditView(newSearchEditView(layoutroot));
			revalidateAdditionalSearchField();

			mpsubformctlSearch = newSearchConditionSubFormControllers(layoutroot
					.getMapOfSubForms());

			if (bTransferContents)
				// try to transfer search condition:
				try {
					assert isSearchPanelAvailable();
					setCollectableSearchConditionInSearchPanel(searchcond);

					if (LOG.isWarnEnabled()) {
						final CollectableSearchCondition searchcondNew = getCollectableSearchConditionFromSearchPanel(false);
						if (!LangUtils.equal(searchcondNew, searchcond))
							LOG.warn("Die Suchbedingung wurde nicht korrekt \u00fcbertragen. Alte Bedingung: "
									+ searchcond
									+ " - Neue Bedingung: "
									+ searchcondNew);
						/** @todo Is warning sufficient here? */
					}
				} catch (CommonBusinessException ex) {
					throw new NuclosBusinessException(
							"Die Suchbedingung konnte nicht \u00fcbertragen werden.",
							ex);
				}
			getSearchPanel().getEditView().setComponentsEnabled(true);
		} finally {
			addChangeListenersForSearch();
			ctlSearch.cmdDisplayCurrentSearchConditionInSearchPanelStatusBar(getTab());
			setSelectedSearchFilter(searchFilter);
		}
	}

	protected LayoutRoot getLayoutFromCache(UsageCriteria usagecriteria, CollectState collectstate) {
		final boolean bSearchMode = collectstate.isSearchMode();
		
		resetLayoutMLButtonsActionListener();

		/** @todo maybe factor this out in a protected method and override in GeneralSearchCollectController */
		final org.nuclos.common.collect.collectable.CollectableEntity clcte = bSearchMode ? getCollectableEntity() : CollectableGenericObjectEntity.getByModuleUid(usagecriteria.getEntityUID());
		final LayoutRoot<?> result = GenericObjectLayoutCache.getInstance().getLayout(clcte, usagecriteria, bSearchMode, !isDetailmodeAnyNew(), getLayoutMLButtonsActionListener(), valueListProviderCache);

		if (bSearchMode) {
			for (CollectableComponent comp : result.getCollectableComponents()) {
				comp.getControlComponent().addFocusListener(collectableComponentSearchFocusListener);
			}
		}

		result.getRootComponent().setFocusCycleRoot(true);
		result.getRootComponent().setFocusTraversalPolicyProvider(true);
		result.getRootComponent().setFocusTraversalPolicy(new NuclosFocusTraversalPolicy(result));


		customizeLayout(result, usagecriteria, collectstate);

		return result;
	}

	/**
	 * customizes the given layout, respecting the user rights.
	 * 
	 * @param layoutroot
	 */
	protected void customizeLayout(LayoutRoot layoutroot,
			UsageCriteria usagecriteria, CollectState collectstate) {
		// respect rights in view mode is handled in the
		// GenericObjectCollectStateListener.detailsModeEntered()
		if (collectstate.isSearchMode()) {
			/*
			 * uncommented. @see NUCLOSINT-1558
			 * respectRights(layoutroot.getCollectableComponents(),
			 * layoutroot.getMapOfSubForms().values(), usagecriteria,
			 * collectstate); //NUCLEUSINT-442
			 * respectRights(layoutroot.getCollectableLabels(),
			 * layoutroot.getMapOfSubForms().values(), usagecriteria,
			 * collectstate);
			 * 
			 * // why didn´t we use this workaround in other than
			 * collectstate.isSearchMode()????
			 * fixUsageCriteriaSearchFields(layoutroot);
			 */
		}
		// iterate through the component tree and clear the keymaps of all
		// JSplitPanes
		UIUtils.clearJComponentKeymap(layoutroot.getRootComponent(),
				JSplitPane.class);
		getLayoutMLButtonsActionListener().clearInputMapForParentPanel(
				getCollectPanel());
		getLayoutMLButtonsActionListener().clearInputMapForParentPanel(
				layoutroot.getRootComponent());

		setupShortcutsForTabs(getTab());

		if (collectstate.isDetailsMode()) {
			getLayoutMLButtonsActionListener().setInputMapForParentPanel(
					getCollectPanel(), layoutroot.getRootComponent());
		}
	}

	/**
	 * workaround: ComboBoxes for quintuple fields must be pure dropdowns
	 * otherwise quintuple field listeners don't work.
	 * 
	 * @todo This is a bug in CollectableComboBox - it fires two "changed"
	 *       events instead of one
	 */
	private void fixUsageCriteriaSearchFields(CollectableComponentsProvider clctcompprovider) {
		for (UID sUsageCriteriaFieldName : getUsageCriteriaFieldNames(getModuleId()))
			for (CollectableComponent clctcomp : clctcompprovider.getCollectableComponentsFor(sUsageCriteriaFieldName))
				if (clctcomp instanceof LabeledCollectableComponentWithVLP) {
					clctcomp.setInsertable(false);					
				}
	}

	/**
	 * adjusts the visibility and "enability" ;) of the fields according to the
	 * user rights.
	 * 
	 * @param collclctcomp
	 * @param usagecriteria
	 * @param collectstate
	 */
	protected void respectRights(Collection<CollectableComponent> collclctcomp,
			Collection<DetailsSubFormController<Long, CollectableEntityObject<Long>>> collsubformctls, UsageCriteria usagecriteria,
			CollectState collectstate) {

		synchronized (lockCurrRecReadable) {
			blnCurrentRecordReadable = null;
		}
		synchronized (lockCurrRecWritable) {
			blnCurrentRecordWritable = null;
		}

		final Collection<UID> collStateIds;
		if (collectstate.isSearchMode() || collectstate.isDetailsModeNew())
			collStateIds = Collections
					.singletonList(getInitialStateId(usagecriteria));
		else if (collectstate.isDetailsModeViewOrEdit())
			collStateIds = Collections
					.singletonList(getSelectedGenericObjectStateId());
		else if (collectstate.isDetailsModeMultiViewOrEdit())
			collStateIds = getStateIds(getSelectedCollectables());
		else
			/**
			 * @todo this occurs when the user changes a usagecriteria field in
			 *       the search panel and pushes the Search button
			 *       "too quickly". Must be deferred to Elisa 1.1 as changing
			 *       the reloading behaviour (runCommand instead of
			 *       runCommandLater) is too dangerous now. 7.9.2004
			 */
			// above comment is an old comment from elisa. we do not have
			// problems here anymore.
			// except switching workspaces with open resultcontroller and a
			// search criteria with a usagecriteria field.
			// switching workspace back will produces that collectstate. but we
			// can ignore it here. all panels are loaded correctly.
			return;// throw new IllegalStateException("collectstate: " +
					// collectstate);

		for (CollectableComponent clctcomp : collclctcomp) {
			final Permission permission = getLeastCommonPermission(clctcomp,
					collStateIds);

			if (!permission.includesReading())
				clctcomp.setVisible(false);
			else
				clctcomp.setVisible(!clctcomp.isHidden());
			if (permission.includesWriting())
				clctcomp.setReadOnly(!clctcomp.getEntityField().isModifiable());
			else
				clctcomp.setReadOnly(true);
		} // for

		// adjust subforms:
		adjustSubformForPermission(collsubformctls, collStateIds, getSelectedGenericObjectId(), collectstate.isSearchMode());
	}

	@Override
	protected boolean isNewAllowed() {
		return super.isNewAllowed() && SecurityCache.getInstance().isNewAllowedForModule(getEntityUid());
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param clctcomp
	 * @param collStateIds
	 * @return the least common permission for the
	 */
	private Permission getLeastCommonPermission(CollectableComponent clctcomp, Collection<UID> collStateIds) {
		if (CollectionUtils.isNullOrEmpty(collStateIds))
			throw new IllegalArgumentException("collStateIds");
		Permission result = Permission.READWRITE;
		for (Iterator<UID> iter = collStateIds.iterator(); iter.hasNext() && (result != Permission.NONE);) {
			final UID iStateId = iter.next();
			result = LangUtils.min(result, getPermission(clctcomp, iStateId));
		}
		return result;
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param clctcomp
	 * @param stateId
	 * @return the permission (read/write) for the given component in the state
	 *         with the given id.
	 */
	private Permission getPermission(CollectableComponent component, UID stateId) {
		// @todo This is not as clean. isCurrentRecordReadable() is called for search/new also.
		final Permission permissionRecord = Permission.getPermissionReadingOverrides(isCurrentRecordReadable(), isCurrentRecordWritable(true));

		logSecurity.debug("getPermission: Erlaubnis fuer den Datensatz: " + permissionRecord);

		final CollectableGenericObjectEntityField clctloef
			= (CollectableGenericObjectEntityField) component.getEntityField();
		final Map<UID, Permission> mpPermissions = clctloef.getPermissions();

		final Permission result;
		if(mpPermissions.get(stateId) == null) {
			result = Permission.NONE;
		}
		else {
			result = LangUtils.min(permissionRecord, mpPermissions.get(stateId));
		}
		 

		logSecurity.debug("getPermission: Erlaubnis fuer das Attribut "
				+ clctloef.getFieldMeta().getFieldName() + ": " + result);

		return result;
	}

	private static Collection<UID> getStateIds(Collection<? extends CollectableGenericObject> collclct) {
		return CollectionUtils.transform(collclct, new Transformer<CollectableGenericObject, UID>() {
			@Override
			public UID transform(CollectableGenericObject clct) {
				return GenericObjectDelegate.getInstance().getStateIdByGenericObject(clct.getId());
				//return getSystemAttributeId(clct, ParameterProvider.KEY_SYSTEMATTRIBUTE_STATUS);
			}
		});
	}

	/**
	 * §precondition usagecriteria.getModuleId() != null
	 */
	private UID getInitialStateId(UsageCriteria usagecriteria) {
		return StateDelegate.getInstance().getStatemodel(usagecriteria).getInitialState();
	}

	@Override
	protected void detailsChangedInComponentModel(DetailsComponentModel model) {
		if (SF.PROCESS_UID.getUID(getEntityUid()).equals(model.getFieldUID())) {
			if (!getUsageCriteriaFieldListenersAdded(false)) {
				this.addUsageCriteriaFieldListeners(false);				
			}
		}
	}
	
	@Override
	protected boolean stopEditingInDetails() {
		boolean result = super.stopEditingInDetails();
		if (result)
			for (SubFormController subformctl : getSubFormControllersInDetails())
				result = result && subformctl.stopEditing();
		return result;
	}

	/**
	 * TODO: Make this protected again.
	 */
	@Override
	public boolean stopEditingInSearch() {
		boolean result = super.stopEditingInSearch();
		if (result)
			for (SubFormController subformctl : getSubFormControllersInSearch())
				result = result && subformctl.stopEditing();
		return result;
	}

	@Override
	protected String getTitle(int iTab, int iMode) {
		final SpringLocaleDelegate localeDelegate = getSpringLocaleDelegate();
		final String[] asTabs = {
				localeDelegate.getMessage("GenericObjectCollectController.92",
						"Suche"),
				localeDelegate.getMessage("GenericObjectCollectController.42",
						"Ergebnis"),
				localeDelegate.getMessage("GenericObjectCollectController.35",
						"Details") };
		final String[] asDetailsMode = {
				localeDelegate.getMessage("GenericObjectCollectController.94",
						"Undefiniert"),
				localeDelegate.getMessage("GenericObjectCollectController.36",
						"Details"),
				localeDelegate.getMessage("GenericObjectCollectController.14",
						"Bearbeiten"),
				localeDelegate.getMessage("GenericObjectCollectController.55",
						"Neueingabe"),
				localeDelegate.getMessage("GenericObjectCollectController.57",
						"Neueingabe (Ge\u00e4ndert)"),
				localeDelegate.getMessage("GenericObjectCollectController.63",
						"Sammelbearbeitung"),
				localeDelegate.getMessage("GenericObjectCollectController.64",
						"Sammelbearbeitung (Ge\u00e4ndert)"),
				localeDelegate.getMessage("GenericObjectCollectController.56",
						"Neueingabe (\u00dcbernahme Suchwerte)") };

		String sPrefix;
		String sSuffix = "";
		final String sMode;

		switch (iTab) {
		case CollectState.OUTERSTATE_DETAILS:
			sPrefix = (iMode == 1 || iMode == 2) ? "" : this.getEntityLabel() + " ";
			sMode = asDetailsMode[iMode];
			if (CollectState.isDetailsModeViewOrEdit(iMode)) {
				sPrefix += SpringLocaleDelegate.getInstance().getIdentifierLabel(getSelectedCollectable(), 
						getSelectedCollectable().getEntityUID(), MetaProvider.getInstance(),getLanguage());
			} else if (CollectState.isDetailsModeMultiViewOrEdit(iMode))
				sSuffix = localeDelegate.getMessage(
						"GenericObjectCollectController.97",
						" von {0} Objekten", getSelectedCollectables().size());
			break;
		default:
			sPrefix = this.getEntityLabel();
			if (getProcess() != null) {
				sPrefix += " (" + getProcess().getValue() + ")";
			}
			sMode = asTabs[iTab];
		}

		final StringBuilder sb;
		if (iTab == CollectState.OUTERSTATE_DETAILS
				&& (iMode == 1 || iMode == 2))
			sb = new StringBuilder(sPrefix + (iMode == 2 ? " - " + sMode : "")
					+ sSuffix);
		else
			sb = new StringBuilder(sPrefix
					+ (sPrefix.length() > 0 ? " - " : "") + sMode + sSuffix);

		if (isSelectedCollectableMarkedAsDeleted())
			sb.append(getSpringLocaleDelegate().getMessage(
					"GenericObjectCollectController.2",
					" (Objekt ist als gel\u00f6scht markiert)"));
		else if (isHistoricalView())
			sb.append(getSpringLocaleDelegate().getMessage(
					"GenericObjectCollectController.1",
					" (Historischer Zustand vom {0})", getHistoricalDate()));

		return sb.toString();
	}
	
	@Override	
	protected LayoutRoot getLayoutRoot() {
		return layoutrootDetails;
	}

	@Override
	protected String getLabelForStartTab() {
		String result = null;

		boolean buildTreeView = false;
		switch (getCollectState().getOuterState()) {
		case CollectState.OUTERSTATE_DETAILS:
			buildTreeView = this.getCollectState().isDetailsModeViewOrEdit();
			break;
		case CollectState.OUTERSTATE_RESULT:
			buildTreeView = this.getSelectedCollectables().size() == 1;
			break;
		}

		if (buildTreeView) {
			result = SpringLocaleDelegate.getInstance().getIdentifierLabel(getSelectedCollectable(), 
					getSelectedCollectable().getEntityUID(), MetaProvider.getInstance(),getLanguage());
		}

		if (result == null) {
			return super.getLabelForStartTab();
		} else {
			return result.trim();
		}
	}

	private boolean isSelectedCollectableMarkedAsDeleted() {
		boolean result = false;
		if (getCollectState().getInnerState() == CollectState.DETAILSMODE_VIEW)
			if (!multipleCollectablesSelected()) {
				final CollectableGenericObject clct = getSelectedCollectable();
				if (clct != null && clct.getGenericObjectCVO().isDeleted())
					result = true;
			}
		return result;
	}

	// NOTE: You cannot refactor the path and name of this class as it is serialized and stored into workspace
	private static class RestorePreferences implements Serializable {
		private static final long serialVersionUID = 6637996725938917463L;

		UID searchFilterUID;
		UID processUID;
	}

	private static String toXML(RestorePreferences rp) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(rp);
		}
	}

	private static RestorePreferences fromXML(String xml) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return (RestorePreferences) xstream.fromXML(xml);
		}
	}

	@Override
	protected void storeInstanceStateToPreferences(
			Map<String, String> inheritControllerPreferences) {
		RestorePreferences rp = new RestorePreferences();

		final SearchFilter filter = state != null ? state.getCurrentSearchFilter() : null;
		rp.searchFilterUID = (filter == null || filter.isDefaultFilter()) ? null
				: filter.getSearchFilterVO().getId();

		if (getProcess() != null) {
			rp.processUID = (UID) getProcess().getValueId();
		}

		inheritControllerPreferences.put(
				GenericObjectCollectController.class.getName(), toXML(rp));
		super.storeInstanceStateToPreferences(inheritControllerPreferences);
	}

	@Override
	protected void restoreInstanceStateFromPreferences(
			Map<String, String> inheritControllerPreferences) {
		String prefXml = inheritControllerPreferences
				.get(GenericObjectCollectController.class.getName());
		if (prefXml != null) {
			RestorePreferences rp = fromXML(prefXml);

			// Restore the settings for the chosen search filter in this module
			// window (may override the global settings)
			if (rp.searchFilterUID == null)
				selectDefaultFilter();
			else {
				// find filter by name:
				final List<EntitySearchFilter> sf = getSearchFilterFromView();
				for (int i = 1; i < sf.size(); ++i)
					if (sf.get(i).getSearchFilterVO().getId().equals(rp.searchFilterUID)) {
						setSelectedSearchFilter(sf.get(i));
						break;
					}
			}
			if (rp.processUID != null) {
				process = new CollectableValueIdField(rp.processUID,
						MasterDataCache.getInstance().get(E.PROCESS.getUID(), rp.processUID).getFieldValue(E.PROCESS.name));
			}
		}
		super.restoreInstanceStateFromPreferences(inheritControllerPreferences);
	}

	@Override
	@Deprecated
	protected int restoreStateFromPreferences(Preferences prefs)
			throws CommonBusinessException {
		// Restore the settings for the chosen search result template in this
		// module window
		restoreSelectedSearchResultTemplateFromPreferences(prefs);
		return super.restoreStateFromPreferences(prefs);
	}

	/**
	 * §precondition this.isSearchPanelVisible()
	 */
	@Override
	@Deprecated
	protected void restoreSearchCriteriaFromPreferences(Preferences prefs)
			throws CommonBusinessException {
		if (!isSearchPanelAvailable())
			throw new IllegalStateException("!isSearchPanelVisible()");

		// Restore the settings for the chosen search filter in this module
		// window (may override the global settings)
		restoreSelectedSearchFilterFromPreferences(prefs);

		super.restoreSearchCriteriaFromPreferences(prefs);
	}

	@Deprecated
	private void restoreSelectedSearchFilterFromPreferences(Preferences prefs) {
		// restore search filter:
		final String sFilterName = prefs.get(PREFS_KEY_FILTERNAME, null);
		if (sFilterName == null)
			selectDefaultFilter();
		else {
			// find filter by name:
			List<? extends SearchFilter> sf = getSearchFilterFromView();
			for (int i = 1; i < sf.size(); ++i)
				if (sf.get(i).getSearchFilterVO().getFilterName().equals(sFilterName)) {
					setSelectedSearchFilter(sf.get(0));
					break;
				}
		}
	}

	@Deprecated
	private void restoreSelectedSearchResultTemplateFromPreferences(
			Preferences prefs) {
		// restore search result template:
		final String sTemplateName = prefs.get(
				PREFS_KEY_SEARCHRESULTTEMPLATENAME, null);
		if (sTemplateName == null)
			searchResultTemplatesController.selectDefaultTemplate();
		else
			// find search result template by name:
			searchResultTemplatesController
					.setSelectedSearchResultTemplate(sTemplateName);
	}

	/**
	 * inner class GenericObjectCollectStateListener
	 */
	private class GenericObjectCollectStateListener extends CollectStateAdapter {
		@Override
		public void searchModeEntered(CollectStateEvent ev)
				throws CommonBusinessException {
			iCurrentLayoutId = null;
			if (!bInitialSearchLayout) {
				removeChangeListenersForSearch();
				removeUsageCriteriaFieldListeners(true);
				setupEditPanelForSearchTab(getInitialLayoutMLDefinitionForSearchPanel());
				setProcessSearchCondition();
				addChangeListenersForSearch();

				bInitialSearchLayout = true; // do not load layout for search
												// anymore.
			}
			setInitialComponentFocusInSearchTab();
			bGenerated = false;
			setGenerationSource(null, null);
			
		}

		@Override
		public void searchModeLeft(CollectStateEvent ev)
				throws CommonBusinessException {
			iCurrentLayoutId = null;
		}

		@Override
		public void resultModeEntered(CollectStateEvent ev)
				throws NuclosBusinessException {
			bGenerated = false;
			setGenerationSource(null, null);

			if (ev.getOldCollectState().getOuterState() != CollectState.OUTERSTATE_RESULT)
				setupChangeListenerForResultTableVerticalScrollBar();

			final int iResultMode = ev.getNewCollectState().getInnerState();
			final boolean bOneRowSelected = (iResultMode == CollectState.RESULTMODE_SINGLESELECTION);
			final boolean bMoreThanOneRowsSelected = (iResultMode == CollectState.RESULTMODE_MULTISELECTION);
			final boolean bRowsSelected = bOneRowSelected
					|| bMoreThanOneRowsSelected;

			actDeletePhysicallyInResult.setEnabled(bRowsSelected
					&& isDeleteSelectedCollectableAllowed()
					&& hasCurrentUserDeletionRights(getSelectedCollectable(),
							true));

			refreshResultFilterView();

			actSearchDeleted.putValue(ToolBarItemAction.ACTION_DISABLED, true);
			actSearchDeleted.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX,
					iSearchDeleted);
			actSearchDeleted.putValue(ToolBarItemAction.ACTION_DISABLED, false);

			refreshResultFilterView();
			
			actSearchDeleted.putValue(ToolBarItemAction.ACTION_DISABLED, true);
			actSearchDeleted.putValue(ToolBarItemAction.ITEM_SELECTED_INDEX,
					iSearchDeleted);
			actSearchDeleted.putValue(ToolBarItemAction.ACTION_DISABLED, false);
			
			if (LOG.isDebugEnabled())
				if (CollectState.isResultModeSelected(ev.getNewCollectState()
						.getInnerState())) {
					final UsageCriteria usagecriteria = getGreatestCommonUsageCriteriaFromCollectables(getSelectedCollectables());
					LOG.debug("Greatest common usagecriteria: " + usagecriteria);
				}
		}

		@Override
		public void resultModeLeft(CollectStateEvent ev)
				throws NuclosBusinessException {
			if (ev.getNewCollectState().getOuterState() != CollectState.OUTERSTATE_RESULT)
				removePreviousChangeListenersForResultTableVerticalScrollBar();
		}

		@Override
		public void detailsModeEntered(CollectStateEvent ev)
				throws CommonBusinessException {
			
			final int iDetailsMode = ev.getNewCollectState().getInnerState();

			final boolean bViewingExistingRecord = (iDetailsMode == CollectState.DETAILSMODE_VIEW);

			final GenericObjectCollectController ctl = GenericObjectCollectController.this;

			ctl.actDeletePhysicallyInDetails.setEnabled(bViewingExistingRecord
					&& isPhysicallyDeleteAllowed(getSelectedCollectable()));
			ctl.actMakeTreeRoot.setEnabled(bViewingExistingRecord);
			ctl.chkbxUseInvalidMasterData.setEnabled(bViewingExistingRecord);
			ctl.actExecuteRule.setEnabled(bViewingExistingRecord);

			ctl.actShowHistory.setEnabled(bViewingExistingRecord);

			final String sActivateLogbook = ClientParameterProvider
					.getInstance().getValue(
							ParameterProvider.KEY_ACTIVATE_LOGBOOK);
			ctl.setOldHistoryVisible(!isNewHistoricalView()
					&& StringUtils.equalsIgnoreCase(sActivateLogbook, "true")
					&& bViewingExistingRecord);
			ctl.actShowStateHistory.setEnabled(!isNewHistoricalView()
					&& bViewingExistingRecord);
			ctl.clctdatechooserHistorical.setEnabled(!isNewHistoricalView()
					&& StringUtils.equalsIgnoreCase(sActivateLogbook, "true")
					&& bViewingExistingRecord);
			ctl.clctdatechooserHistorical.getDateChooser().getJTextField()
					.setEditable(false);

			// current state, subsequent states and custom actions:
			switch (iDetailsMode) {
			case CollectState.DETAILSMODE_NEW:
				setInitialComponentFocusInDetailsTab();
			case CollectState.DETAILSMODE_NEW_CHANGED:
			case CollectState.DETAILSMODE_NEW_SEARCHVALUE:
				iCurrentLayoutId = null;
				// deselect potentially previously selected entry
				getResultTable().clearSelection();
				
				setSubsequentStatesVisible(false, false);
				setStatesDefaultPathVisible(false, false);

				if (iDetailsMode == CollectState.DETAILSMODE_NEW)
					getLayoutMLButtonsActionListener().setComponentsEnabled(false);
				else
					getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(true);

				cmpStateStandardView.setSelectedItem(null);
				
				for(DetailsSubFormController dsc : getSubFormControllersInDetails()) {		
					if((dsc.getSubForm().isIgnoreSubLayout() || dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != null)
							&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != ToolbarFunctionState.HIDDEN) {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.DISABLED);
					} 
					if(dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != null 
							&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != ToolbarFunctionState.HIDDEN) {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.DISABLED);
					}
				}
				break;
			case CollectState.DETAILSMODE_VIEW:
				{
					boolean bEnableButtons = isStateButtonEnabled();
					setSubsequentStatesVisible(true, bEnableButtons);
					setStatesDefaultPathVisible(true, bEnableButtons);
				}

				if (isHistoricalView()) {
					getLayoutMLButtonsActionListener().setComponentsEnabled(false);
				} else {
					getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(false);
				}
				
				for(DetailsSubFormController dsc : getSubFormControllersInDetails()) {			
					UID sEntity = dsc.getSubForm().getEntityUID();
					
					boolean hasScripts = 
							dsc.getSubForm().getDeleteEnabledScript() != null || 
									dsc.getSubForm().getNewEnabledScript() != null ||
											dsc.getSubForm().getCloneEnabledScript() != null ||
													dsc.getSubForm().getEditEnabledScript() != null;
					
					if(SecurityCache.getInstance().isReadAllowedForEntity(sEntity) && MasterDataLayoutHelper.isLayoutMLAvailable(sEntity, false)) {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW,
								(!hasScripts && !dsc.getSubForm().isIgnoreSubLayout() && dsc.getSubForm().isEnabled()) ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
					} else {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.HIDDEN);
 					}
					if (MasterDataLayoutHelper.isLayoutMLAvailable(sEntity, false) && MetaProvider.getInstance().getEntity(sEntity).isStateModel()) {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, 
								(!dsc.getSubForm().isReadOnly() && dsc.getSubForm().isEnabled()) ? ToolbarFunctionState.ACTIVE : ToolbarFunctionState.DISABLED);
					} else {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.HIDDEN);
					}
				}
				break;
			case CollectState.DETAILSMODE_EDIT:
				{
					boolean bEnableButtons = isStateButtonEnabled();
					setSubsequentStatesVisible(true, bEnableButtons);
					setStatesDefaultPathVisible(true, bEnableButtons);
				}
				
				if (isHistoricalView()) {
					getLayoutMLButtonsActionListener().setComponentsEnabled(false);
				} else {
					getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(true);
				}
				
				for(DetailsSubFormController dsc : getSubFormControllersInDetails()) {						
					if((dsc.getSubForm().isIgnoreSubLayout() || dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != null)
							&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != ToolbarFunctionState.HIDDEN) {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.DISABLED);
					}
					if(dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != null 
							&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != ToolbarFunctionState.HIDDEN) {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.DISABLED);
					}
				}
				break;
			case CollectState.DETAILSMODE_MULTIVIEW:
			case CollectState.DETAILSMODE_MULTIEDIT:
				iCurrentLayoutId = null;
				updateStatesForMultiEdit();
				if (iDetailsMode == CollectState.DETAILSMODE_MULTIVIEW) {
					getLayoutMLButtonsActionListener().setComponentsEnabled(false);
					getLayoutMLButtonsActionListener().setRuleButtonComponentsEnabled(true);
				} else {
					getLayoutMLButtonsActionListener().fireComponentEnabledStateUpdate(true);
				}
				respectMultiEditable(getDetailsPanel().getEditView().getCollectableComponents(), ev.getNewCollectState());

				for(DetailsSubFormController dsc : getSubFormControllersInDetails()) {
					if((dsc.getSubForm().isIgnoreSubLayout() || dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != null)
							&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_DETAILS_VIEW) != ToolbarFunctionState.HIDDEN) {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_DETAILS_VIEW, ToolbarFunctionState.HIDDEN);
					} 
					if(dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != null 
							&& dsc.getSubForm().getToolbarManager().getStateIfAvailable(ToolbarFunction.SUBFORM_CHANGE_STATE) != ToolbarFunctionState.HIDDEN) {
						dsc.getSubForm().setToolbarFunctionState(ToolbarFunction.SUBFORM_CHANGE_STATE, ToolbarFunctionState.HIDDEN);
					}
				}
				break;

			default:
			} // switch

			if (iDetailsMode != CollectState.DETAILSMODE_NEW_CHANGED
					&& iDetailsMode != CollectState.DETAILSMODE_VIEW) {
				bGenerated = false;
				setGenerationSource(null, null);
			}

			setDeleteButtonToggleInDetails();
			// show custom actions only in view mode:
			showCustomActions(iDetailsMode);

			final Collection<SubForm> collsubform = new HashSet<SubForm>();
			for (SubFormController subformctl : getSubFormControllersInDetails())
				collsubform.add(subformctl.getSubForm());

			// dynamic reloading of layouts:
			if (CollectState.isDetailsModeChangesPending(iDetailsMode)) {
				// check if a change in a usagecriteria field caused the state
				// change:
				final Object oSource = getSourceOfLastDetailsChange();
				if (oSource != null
						&& oSource instanceof CollectableComponentModel) {
					final CollectableComponentModel clctcompmodelSource = (CollectableComponentModel) oSource;
					if (isUsageCriteriaField(getModuleId(), clctcompmodelSource.getFieldUID()))
						// don't add usagecriteria field listeners here. They
						// are added later (see below).
						reloadLayoutForDetailsTab(false);
				}

				// add listeners for usagecriteria fields:
				/** @todo don't do this asynchronously - it's not safe!!! */
				EventQueue.invokeLater(new Runnable() {
					@Override
					public void run() {
						if (!getUsageCriteriaFieldListenersAdded(false))
							// add usagecriteria field listeners here:
							addUsageCriteriaFieldListeners(false);
					}
				});
			}
			// reset the source of last details change in order to prevent a
			// memory leak:
			resetSourceOfLastDetailsChange();

			if (ev.hasOuterStateChanged())
				setInitialComponentFocusInDetailsTab();
			
			setupShortcutsForTabs(getTab());
		}

		@Override
		public void detailsModeLeft(CollectStateEvent ev) {
			final int iDetailsMode = ev.getOldCollectState().getInnerState();
			final boolean bDetailsChanged = (iDetailsMode == CollectState.DETAILSMODE_EDIT)
					|| (iDetailsMode == CollectState.DETAILSMODE_NEW_CHANGED);

			if (bDetailsChanged) {
				// remove listeners for quintuple fields:
				LOG.debug("removeUsageCriteriaFieldListeners");
				removeUsageCriteriaFieldListeners(false);
			}

			if (!LangUtils.equal(ev.getNewCollectState().getOuterState(), ev
					.getOldCollectState().getOuterState())) {
				// remove keystrokes only if outer state change
				getLayoutMLButtonsActionListener().clearInputMapForParentPanel(
						getCollectPanel());
				setupShortcutsForTabs(getTab()); // reset all actions here.
			}
		}
	} // inner class GenericObjectCollectStateListener

	/**
	 * set toggle delete button in details mode
	 */
	private void setDeleteButtonToggleInDetails() {
		final CollectableGenericObjectWithDependants gowd = getSelectedCollectable();
		final DeleteOrRestoreToggleAction action = getDetailsController().getDeleteCurrentCollectableAction();
		if (gowd == null) {
			NuclosToolBarActions.DELETE
					.init(action);
			action.setMode(Mode.DELETE);
			action.putValue(Action.SELECTED_KEY,
					Boolean.FALSE);
		} else if (gowd.getGenericObjectCVO().isDeleted()) {
			NuclosToolBarActions.RESTORE
					.init(action);
			action.setMode(Mode.RESTORE);
			action.putValue(Action.SELECTED_KEY,
					Boolean.TRUE);
		} else {
			NuclosToolBarActions.DELETE
					.init(action);
			action.setMode(Mode.DELETE);
			action.putValue(Action.SELECTED_KEY,
					Boolean.FALSE);
		}
	}

	@Override
	protected void setDeleteActionEnabled(boolean enabled) {
		getDetailsController().getDeleteCurrentCollectableAction().setEnabled(enabled);
	}

	protected class GenericObjectCollectPanel extends
			CollectPanel<Long, CollectableGenericObjectWithDependants> {

		private final LabeledCollectableComponentWithVLP searchStateBox;

		protected GenericObjectCollectPanel(UID entityId,
				LabeledCollectableComponentWithVLP searchStateBox, boolean bSearch, boolean bShowSearch,
				DetailsPresentation detailsPresentation, ControllerPresentation controllerPresentation) {
			super(entityId, bSearch, bShowSearch, detailsPresentation, controllerPresentation, getFilterAction());
			this.searchStateBox = searchStateBox;
		}

		@Override
		public SearchPanel newSearchPanel(UID entityId) {
			final Collection<CollectableComponent> additionalSearchComponents = new ArrayList<CollectableComponent>();
			if (searchStateBox != null) {
				additionalSearchComponents.add(searchStateBox);
			}
			final GenericObjectSearchPanel result = new GenericObjectSearchPanel(entityId,
					additionalSearchComponents);
			registerPanelForClosing(result);
			return result;
		}

		@Override
		public ResultPanel<Long, CollectableGenericObjectWithDependants> newResultPanel(UID entityId) {
			return new GenericObjectResultPanel(entityId, getViewMode(),
					GenericObjectCollectController.this.getFilterAction(), GenericObjectCollectController.this);
		}

		@Override
		public DetailsPanel newDetailsPanel(UID entityId) {
			final DetailsPanel result = new GenericObjectDetailsPanel(entityId,
					getEntityPreferences(), getDetailsPresentation(), getViewMode());
			registerPanelForClosing(result);
			return result;
		}
	}

	/**
	 * @return <code>MultiActionProgressPanel</code> for the
	 *         MultiObjectsActionController.
	 * 
	 *         TODO: Make protected again.
	 */
	@Override
	public MultiActionProgressPanel getMultiActionProgressPanel(int iCount) {
		MultiActionProgressPanel multiActionProgressPanel = new MultiActionProgressPanel(iCount);
		multiActionProgressPanel.setResultHandler(new MultiActionProgressResultHandler(this) {
			@Override
			public void handleMultiSelection(Collection<MultiActionProgressLine> selection) {
				Collection<Long> ids = CollectionUtils.transform(selection, new Transformer<MultiActionProgressLine, Long>() {
					@Override
					public Long transform(MultiActionProgressLine i) {
						if (i.getSourceObject() instanceof Collectable) {
							return (Long) ((Collectable) i.getSourceObject() ).getId();
						}
						else if (i.getSourceObject() instanceof Long) {
							return (Long) i.getSourceObject();
						}
						return null;
					}
				});
				((GenericObjectCollectController) controller).openGenericObjectController(ids);
			}
		});
		return multiActionProgressPanel;
	}

	/**
	 * open the GenericObjectCollectController for an entity. if
	 * collGenericObjectIds only contains one generic object id the controller
	 * is opened in details mode if there are more than one id a search
	 * expression is created and the search result is shown
	 * 
	 * @param collGenericObjectIds
	 */
	private void openGenericObjectController(final Collection<Long> collGenericObjectIds) {
		UIUtils.runCommand(getTab(), new Runnable() {
			@Override
			public void run() {
				try {
					GenericObjectClientUtils.showDetails(collGenericObjectIds);
				} catch (CommonBusinessException ex) {
					Errors.getInstance().showExceptionDialog(getTab(), ex);
				}
			}
		});
	}

	/**
	 * creates a SearchResultTemplate accordng to selected columns in search
	 * result
	 * 
	 * @throws CommonBusinessException
	 */
	protected SearchResultTemplate getCurrentSearchResultFormatFromResultPanel()
			throws CommonBusinessException {
		final SearchResultTemplate result = new SearchResultTemplate(
				getModuleId());
		result.setVisibleColumns(getSelectedFields());
		// TODO set sorting column names
		List<CollectableSorting> lstSortingColumnNames = Collections
				.emptyList();
		result.setSortingOrder(lstSortingColumnNames);
		Map<UID, Integer> currentFieldWiths = getResultPanel().getCurrentFieldWithsMap();
		result.setListColumnsWidths(currentFieldWiths);
		
		final List<UID> fixedColumnsNames = CollectableUtils.getFieldUidsFromCollectableEntityFields(
				getResultPanel().getFixedColumns());
		
		final List<CollectableEntityField> filteredFixedColumns = CollectionUtils.select(
			getSelectedFields(),
			PredicateUtils.transformedInputPredicate(new CollectableEntityFieldWithEntity.GetUID(),
				PredicateUtils.valuesCollection(fixedColumnsNames)
			)
		);
		result.setFixedColumns(filteredFixedColumns);
		return result;
	}

	/**
	 * @param currclct
	 * @return the current collectable filled with the values which are set in
	 *         the search panel, but first reload layout if process field is
	 *         available and filled in search panel
	 * @throws CommonBusinessException
	 */
	@Override
	protected CollectableGenericObjectWithDependants newCollectableWithSearchValues(CollectableGenericObjectWithDependants currclct) throws CommonBusinessException {
		final Collection<SearchComponentModel> collscm = getSearchCollectableComponentModels();

		// iterate over each component in search panel to set process field in details panel
		for (SearchComponentModel scm : collscm)
			if (scm.getFieldUID().equals(SF.PROCESS.getMetaData(currclct.getEntityUID()).getUID())) {
				// set 'process' field in details panel if any found
				for (CollectableComponent clctcomp : getDetailCollectableComponentsFor(scm.getFieldUID())) {
					if (scm.getField().getValue() != null) {
						currclct.setField(scm.getFieldUID(), scm.getField());
						for (CollectableField clctField : MasterDataDelegate.getInstance().getProcessByEntity(getModuleId(), false))
							if (((String)clctField.getValue()).equals(scm.getField().getValue())) {
								// reload layout according to the 'process' field
								reloadLayout(new UsageCriteria(getModuleId(), (UID) clctField.getValueId(), null, getCustomUsage()), getCollectState(), true, true);
								detailsChanged(clctcomp);
								break;
							}
					}
					break;
				}
				break;
			}
		return super.newCollectableWithSearchValues(currclct);
	}

	/**
	 * complete the current collectable with the subform values which are set in
	 * the search panel
	 */
	@Override
	protected void newCollectableWithDependantSearchValues()
			throws NuclosBusinessException {
		Collection<SearchConditionSubFormController> collscsfc = getSubFormControllersInSearch();
		Collection<DetailsSubFormController<Long, CollectableEntityObject<Long>>> colldsfc = getSubFormControllersInDetails();
		// iterate over each search subform
		for (SearchConditionSubFormController scsfc : collscsfc)
			// handel only subforms of the first hierarchie
			if (scsfc.getSubForm().getParentSubForm() == null)
				// iterate over each detail subform
				for (DetailsSubFormController<Long, CollectableEntityObject<Long>> dsfc : colldsfc) {
					if(dsfc.getEntityAndForeignKeyField().getEntity().equals(scsfc.getEntityAndForeignKeyField().getEntity()))
						if (dsfc.getSubForm().isEnabled()) {
							SubFormTableModel searchTableModel = scsfc.getSearchConditionTableModel();
							CollectableTableModel<Long, CollectableEntityObject<Long>> detailsTableModel = dsfc.getCollectableTableModel();
							Collection<CollectableMasterData> newCollectables = new ArrayList<CollectableMasterData>();
							// iterate over each row found in the search subform
							for (int iSearchRow = 0; iSearchRow < searchTableModel
									.getRowCount(); iSearchRow++) {
								CollectableMasterData clctmd = dsfc
										.insertNewRow();
								newCollectables.add(clctmd);
								// iterate over each column found in the search
								// subform
								for (int iSearchColumn = 0; iSearchColumn < searchTableModel
										.getColumnCount(); iSearchColumn++)
									// iterate over each coresponding column
									// found in the detail subform
									for (int columnDetail = 0; columnDetail < detailsTableModel
											.getColumnCount(); columnDetail++)
										if (searchTableModel
												.getColumnName(iSearchColumn)
												.equals(detailsTableModel
														.getColumnName(columnDetail))) {
											int viewColumn = dsfc.getSubForm().getJTable().convertColumnIndexToView(columnDetail);
											if (viewColumn == -1) {
												continue;
											}
											TableCellEditor tce = dsfc
													.getSubForm()
													.getJTable()
													.getCellEditor(iSearchRow,
															viewColumn);

											if (tce instanceof CollectableComponentTableCellEditor) {
												boolean bSetAllowed = true;

												if (!isSetAllowedForClctComponent(((CollectableComponentTableCellEditor) tce)
														.getCollectableComponent()))
													bSetAllowed = false;

												if (bSetAllowed) {
													Object oClctSearchCondition = searchTableModel
															.getValueAt(
																	iSearchRow,
																	iSearchColumn);
													if (oClctSearchCondition != null) {
														UID sFieldName = ((AtomicCollectableSearchCondition)oClctSearchCondition).getFieldUID();
														Object oSearchValue = null;
														Object oSearchValueId = null;
														if (oClctSearchCondition instanceof CollectableComparison) {
															CollectableField clctField = ((CollectableComparison)oClctSearchCondition).getComparand();
															if (clctField instanceof CollectableValueIdField)
																oSearchValueId = ((CollectableValueIdField)clctField).getValueId();

															oSearchValue = clctField.getValue();
														}
														else if (oClctSearchCondition instanceof CollectableLikeCondition)
															oSearchValue = ((CollectableLikeCondition)oClctSearchCondition).getLikeComparand();

														if (oSearchValue != null) {
															if (oSearchValueId != null) {
																clctmd.setField(sFieldName, new CollectableValueIdField(oSearchValueId, oSearchValue));
																if (clctmd.getMasterDataCVO() != null)
																	clctmd.getMasterDataCVO().setFieldValue(sFieldName, oSearchValueId);
															} else {
																clctmd.setField(sFieldName, new CollectableValueField(oSearchValue));
																if (clctmd.getMasterDataCVO() != null)
																	clctmd.getMasterDataCVO().setFieldValue(sFieldName, oSearchValue);
															}
															newCollectables.remove(clctmd);
															detailsChanged(dsfc.getSubForm());
														}
													}
												}
											}
										}
							}
							for (CollectableMasterData clctmd : newCollectables) {
								dsfc.getCollectableTableModel().remove(clctmd);
							}
						}
				}
	}

	/**
	 * @return whether the current collectable is generated
	 */
	public boolean isCollectableGenerated() {
		return bGenerated;
	}

	/**
	 * @return the source object's ids if current collectable is generated
	 */
	public Collection<Long> getSourceObjectId() {
		return iGenericObjectIdSources;
	}

	/**
	 * @deprecated Move to SearchController and make protected again.
	 */
	@Deprecated
	@Override
	public Collection<CollectableEntityField> getAdditionalSearchFields() {
		Collection<CollectableEntityField> additionalFields = new HashSet<CollectableEntityField>();
		if (super.getAdditionalSearchFields() != null)
			additionalFields.addAll(super.getAdditionalSearchFields());
		return additionalFields;

	}

	@Override
	public GenericObjectEditView newSearchEditView(LayoutRoot layoutroot) {
		final JComponent compEdit = getSearchPanel().newEditComponent(
				layoutroot.getRootComponent());
		final GenericObjectCollectableComponentsProvider prov = new GenericObjectCollectableComponentsProvider(getModuleId(), layoutroot, getSearchStateBox());
		final boolean forSearch = true;
		return new GenericObjectEditView(compEdit, prov,
				newGenericObjectEditModel(prov, forSearch), forSearch,
				layoutroot.getInitialFocusEntityAndField());
	}

	private EditModel newGenericObjectEditModel(
			CollectableComponentsProvider clctcompprovider, boolean bForSearch) {
		final Collection<CollectableComponent> clctcomp = clctcompprovider
				.getCollectableComponents();
		return bForSearch ? new GenericObjectSearchEditModel(clctcomp)
				: new DefaultDetailsEditModel(clctcomp);
	}

	public class GenericObjectSearchEditModel extends DefaultSearchEditModel {

		public GenericObjectSearchEditModel(
				Collection<CollectableComponent> collclctcomp) {
			super(collclctcomp);
		}

		@Override
		public SearchComponentModel getCollectableComponentModelFor(UID fieldUID) {
			if (SF.STATE.getMetaData(getEntityUid()).getUID().equals(fieldUID)
					|| SF.STATENUMBER.getMetaData(getEntityUid()).getUID().equals(fieldUID)
					|| SF.STATEICON.getMetaData(getEntityUid()).getUID().equals(fieldUID))
				return getSearchStateBox().getSearchModel();
			return super.getCollectableComponentModelFor(fieldUID);
		}

		@Override
		public Collection<SearchComponentModel> getCollectableComponentModels() {
			Collection<SearchComponentModel> result = new HashSet<SearchComponentModel>();
			result.addAll(super.getCollectableComponentModels());
			result.add(getSearchStateBox().getSearchModel());
			return result;
		}

		@Override
		public Collection<UID> getFieldUids() {
			Collection<UID> result = new HashSet<UID>();
			result.addAll(super.getFieldUids());
			result.add(SF.STATE.getMetaData(getEntityUid()).getUID());
			result.add(SF.STATENUMBER.getMetaData(getEntityUid()).getUID());
			result.add(SF.STATEICON.getMetaData(getEntityUid()).getUID());
			return result;
		}

	} // inner class GenericObjectSearchEditModel

    protected void highlightMandatoryByState(UID stateId) {
    	if (stateId != null) {
			for (StateVO statevo : StateDelegate.getInstance().getStatemodelClosure(getModuleId()).getAllStates()) {
				if (stateId.equals(statevo.getId())) {
					final Set<UID> mandatoryfields = CollectionUtils.transformIntoSet(statevo.getMandatoryFields(), new Transformer<MandatoryFieldVO, UID>() {
						@Override
						public UID transform(MandatoryFieldVO i) {
							FieldMeta efMeta = MetaProvider.getInstance().getEntityField(i.getField());
							return efMeta != null ? efMeta.getUID() : null;
						}});
					setCollectableComponentModelsInDetailsMandatory(mandatoryfields);
				}
			}
		}
    }

	@Override
	protected void highlightMandatory() {
		super.highlightMandatory();
		// getUsageCriteria(getSelectedCollectable());

		if (getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_VIEW
				|| getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_EDIT) {
			highlightMandatoryByState(getSelectedGenericObjectStateId());

		} else if (getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_MULTIEDIT
				|| getCollectStateModel().getDetailsMode() == CollectState.DETAILSMODE_MULTIVIEW) {
			final Collection<UID> collStateIds = getStateIds(getSelectedCollectables());
			if (collStateIds.size() == 1) {
				highlightMandatoryByState(collStateIds.iterator().next());
			}
		}
	}

	@Override
	public Map<UID, DetailsSubFormController<Long,CollectableEntityObject<Long>>> getDetailsSubforms() {
		return this.mpsubformctlDetails;
	}

	protected void setGenerationSource(Collection<Long> ids,
			GeneratorActionVO action) {
		iGenericObjectIdSources = ids;
		oGeneratorAction = action;
		bGenerated = true;
	}

	@Override
	public List<GeneratorActionVO> getGeneratorActions() {
		List<GeneratorActionVO> result = null;
		try {
			if (getCollectState().isResultMode()) {
				result = getGeneratorActions(getResultController()
						.getSelectedCollectablesFromTableModel());
			} else if (CollectState
					.isDetailsModeViewOrEdit(getCollectStateModel()
							.getDetailsMode())) {
				final UsageCriteria usagecriteria = getUsageCriteriaFromView(false);
				final UID iState = getSelectedGenericObjectStateUid();
				final UID mandatorUid = getSelectedGenericObjectMandatorUid();
				result = GeneratorActions.getActions(getModuleId(), iState, usagecriteria.getProcessUID(), mandatorUid);
			} else {
				final UID iProcessId = getSelectedGenericObjectsCommonFieldIdByFieldName(
						SF.PROCESS_UID.getMetaData(getModuleId()).getUID());
				final UID iStateUid = getSelectedGenericObjectStateUid();
				final UID mandatorUid = getSelectedGenericObjectMandatorUid();
				result = GeneratorActions.getActions(getModuleId(), iStateUid, iProcessId, mandatorUid);
			}

			if (result == null)
				return Collections.EMPTY_LIST;
			
			// remove inactive actions
			CollectionUtils.removeAll(result, new Predicate<GeneratorActionVO>() {
				@Override
				public boolean evaluate(GeneratorActionVO genvo) {
					return genvo.isRuleOnly();
				}
			});
			return GeneratorActions.sort(result);
		}
		catch (NoSuchElementException ex) {
			LOG.info("Keinen aktuellen Zustand gefunden f\u00fcr GenericObject mit Id " + getSelectedGenericObjectId() + ".");
			return Collections.emptyList();
		}
		catch (CollectableFieldFormatException ex) {
			throw new NuclosFatalException(
					getSpringLocaleDelegate().getMessage("GenericObjectCollectController.61","Prozess-Id ist ung\u00fcltig."), ex);
		}
	}

	@Override
	public List<GeneratorActionVO> getGeneratorActions(
			Collection<CollectableGenericObjectWithDependants> selectedCollectablesFromResult) {
		List<GeneratorActionVO> result = null;
		boolean mandator = MetaProvider.getInstance().getEntity(getModuleId()).isMandator();
		for (CollectableGenericObjectWithDependants go : selectedCollectablesFromResult) {
			DynamicAttributeVO dynState = go.getGenericObjectCVO().getAttribute(SF.STATE.getMetaData(go.getEntityUID()).getUID());
			DynamicAttributeVO dynProcess = go.getGenericObjectCVO().getAttribute(SF.PROCESS.getMetaData(go.getEntityUID()).getUID());
			DynamicAttributeVO dynMandator = null;
			if (mandator) {
				dynMandator = go.getGenericObjectCVO().getAttribute(SF.MANDATOR.getMetaData(go.getEntityUID()).getUID());
			}
			
			UID state = null;
			UID iProcessId = null;
			UID mandatorUid = null;
			if (dynState != null) {
				StateVO statevo = StateDelegate.getInstance().getState(getModuleId(), dynState.getValueUid());
				if (statevo != null) {
					state = statevo.getId();
				}
			}
			if (dynProcess != null) {
				iProcessId = dynProcess.getValueUid();
			}
			if (dynMandator != null) {
				mandatorUid = dynMandator.getValueUid();
			}

			List<GeneratorActionVO> goActions = GeneratorActions.getActions(getModuleId(), state, iProcessId, mandatorUid);
			if (result == null) {
				result = goActions;
			} else {
				result = new ArrayList<GeneratorActionVO>(
						CollectionUtils.intersection(result, goActions));
			}
		}

		if (result == null)
			return Collections.EMPTY_LIST;
		
		// remove inactive actions
		CollectionUtils.removeAll(result, new Predicate<GeneratorActionVO>() {
			@Override
			public boolean evaluate(GeneratorActionVO genvo) {
				return genvo.isRuleOnly();
			}
		});
		return GeneratorActions.sort(result);
	}

	@Override
	public void cmdGenerateObject(GeneratorActionVO generatoractionvo) {
		cmdGenerateObject(generatoractionvo, null);
	}
	
	public void cmdGenerateObject(GeneratorActionVO generatoractionvo, ResultListener<?> resultListener) {
		Map<Long, UsageCriteria> sources = new HashMap<Long, UsageCriteria>();
		for (CollectableGenericObjectWithDependants clct : getSelectedCollectables()) {
			sources.put(IdUtils.toLongId(clct.getId()), getUsageCriteria(clct));
		}
		GenerationController controller = new GenerationController(sources,
				generatoractionvo, this, getTab());
		controller.generateGenericObject(resultListener);
	}

	private static final TimedCache<UsageCriteria, Collection<EventSupportSourceVO>> TIMEDURCACHE = 
			new TimedCache<UsageCriteria, Collection<EventSupportSourceVO>>(new GetUserRulesProvider(), 60);

	@Override
	public Collection<EventSupportSourceVO> getUserRules() {
		UsageCriteria uc;
		try {
			uc = getUsageCriteriaFromView(false);
			return TIMEDURCACHE.get(uc);
		} catch (CollectableFieldFormatException e) {
			LOG.error(e.getMessage(), e);
			return Collections.EMPTY_LIST;
		}
	}

	public CollectableField getProcess() {
		return process;
	}

	public void setProcessId(UID processId) {
		MasterDataVO<?> processVO = MasterDataCache.getInstance().get(E.PROCESS.getUID(), processId);
		if (processVO == null) {
			setProcess(null);
			return;
		}

		final CollectableField processField = new CollectableValueIdField(processId, processVO.getFieldValue(E.PROCESS.name.getUID()));
		setProcess(processField);
	}

	public void setProcess(CollectableField process) {
		this.process = process;
		setTitle();
		setProcessSearchCondition();
	}
	
	public void setCalledByProcessMenu(boolean calledByProcessMenu) {
		this.calledByProcessMenu = calledByProcessMenu;
	}
	
	public boolean isCalledByProcessMenu() {
		return this.calledByProcessMenu;
	}

	@Override
	protected CollectableGenericObjectWithDependants newCollectableWithDefaultValues(boolean forInsert) {
		final CollectableGenericObjectWithDependants result = super.newCollectableWithDefaultValues(forInsert);

		if (isCalledByProcessMenu() && getProcess() != null) {
			result.setField(
					SF.PROCESS_UID.getUID(result.getEntityUID()),
					new CollectableValueIdField(getProcess().getValueId(), getProcess().getValue())
			);
		}

		return result;
	}

	@Override
	protected void clearSearchFields() {
		super.clearSearchFields();
		setProcessSearchCondition();
	}
	
	@Override
	protected void viewAll() throws CommonBusinessException {
		getResultController().getSearchResultStrategy().refreshResult();
	}

	private void setProcessSearchCondition() {
		if (isSearchPanelAvailable()) {
			CollectableComponentModel m = getSearchEditView().getModel().getCollectableComponentModelFor(SF.PROCESS.getUID(this.entityUid));
			if (m != null) {
				if (getProcess() != null) {
					m.setField(new CollectableValueIdField(getProcess()
							.getValueId(), getProcess().getValue()));
				}
				try {
					if (!bReloadingLayout) {
						reloadLayoutForSearchTab();
					}
				} catch (CommonBusinessException e) {
				}
			}
		}
	}
	
	private static boolean canWriteFromRecordGrant(GenericObjectVO go) {
		return go==null?true:go.canWrite();
	}
	
	private static boolean canStateChangeFromRecordGrant(GenericObjectVO go) {
		return go==null?true:go.canStateChange();
	}
	
	private boolean isStateButtonEnabled() {
		// show subsequent state buttons only in current
		// (non-historical) view and only if the current record is
		// writable:
		boolean bEnableButtons = !isHistoricalView();
		if (bEnableButtons) {
			boolean currentRecordWritable = isCurrentRecordWritable(false); 
			if (!currentRecordWritable) {
				bEnableButtons = false;
			} else {
				// true in canStateChange overwrites a false in canWrite in RecardGrant
				boolean canStateChangeFromRecordGrant = canStateChangeFromRecordGrant(getSelectedGenericObjectCVO());
				bEnableButtons = canStateChangeFromRecordGrant;
			}
		}
		return bEnableButtons;
	}

	protected List<StateVO> getSubsequentStates(
			Collection<CollectableGenericObjectWithDependants> selectedCollectablesFromResult, boolean includeAutomatic) {
		List<StateVO> result = null;

		for (CollectableGenericObjectWithDependants go : selectedCollectablesFromResult) {
			if (!canStateChangeFromRecordGrant(go.getGenericObjectCVO())) {
				result = null;
				break;
			}
			UsageCriteria uc = getUsageCriteria(go);
			DynamicAttributeVO av = go.getGenericObjectCVO().getAttribute(SF.STATE.getMetaData(go.getEntityUID()).getUID());
			List<StateVO> lstSubsequentStates = StateDelegate.getInstance().getStatemodel(uc).getSubsequentStates(av.getValueUid(), includeAutomatic);
			if (result == null) {
				result = lstSubsequentStates;
			} else {
				result = new ArrayList<StateVO>(CollectionUtils.intersection(
						result, lstSubsequentStates,
						new BinaryPredicate<StateVO, StateVO>() {
							@Override
							public boolean evaluate(StateVO t1, StateVO t2) {
								return LangUtils.equal(t1.getId(), t2.getId());
							}
						}));
			}
		}

		if (result == null) {
			return new ArrayList<StateVO>();
		} else {
			return CollectionUtils.sorted(result, new Comparator<StateVO>() {
				@Override
				public int compare(StateVO o1, StateVO o2) {
					return LangUtils.compare(o1.getNumeral(), o2.getNumeral());
				}
			});
		}
	}

	protected void setupResultContextMenuStates() {
		final GenericObjectResultPanel rp = getResultPanel();
		final JPopupMenu rowWidget = rp.getRowWidget();
		final JMenuItem stateWidget = rp.getStateWidget();
		rowWidget.addPopupMenuListener(getPopupMenuListenerForStates(stateWidget));

		final JPopupMenu rowWidgetFixed = rp.getRowWidgetFixed();
		final JMenuItem stateWidgetFixed = rp.getFixedStateWidget();
		rowWidgetFixed.addPopupMenuListener(getPopupMenuListenerForStates(stateWidgetFixed));

	}

	private PopupMenuListener getPopupMenuListenerForStates(final JMenuItem menuItem) {
		return new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				try {
					for (ResultActionCollection rac : getResultActionsMultiThreaded(getSelectedCollectables())) {
						if (rac.getType().equals(ResultActionType.CHANGE_STATE)) {
							menuItem.setVisible(!rac.getActions().isEmpty());
							for (Action act : rac.getActions()) {
								final JMenuItem miStateChange = new JMenuItem(act);
								miStateChange.setIcon(null);
								menuItem.add(miStateChange);
							}
						}
					}
				} catch (Exception e1) {
					menuItem.setVisible(false);
					LOG.warn("popupMenuWillBecomeVisible failed: " + e1 + ", setting it invisible");
				}
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				clearStatesMenu();
			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				clearStatesMenu();
			}

			private void clearStatesMenu() {
				menuItem.removeAll();
			}
		};
	}

	@Override
	public List<ResultActionCollection> getResultActionsMultiThreaded(
			Collection<CollectableGenericObjectWithDependants> selectedCollectablesFromResult) {
		List<ResultActionCollection> result = super
				.getResultActionsMultiThreaded(selectedCollectablesFromResult);
		if (result == null) {
			result = new ArrayList<ResultActionCollection>();
		}
		ResultActionCollection rac = new ResultActionCollection(ResultActionType.CHANGE_STATE,
				SpringLocaleDelegate.getInstance().getMessage("ResultPanel.15", "Statuswechsel"));

		// try to find single source state...
		StateVO statevoSource = getSingleSourceStateIfAny(selectedCollectablesFromResult);

		for (StateVO statevoTarget : getSubsequentStates(selectedCollectablesFromResult, false)) {
			rac.addAction(new StateChangeAction(this, statevoSource, statevoTarget));
		}
		result.add(0, rac);
		return result;
	}
	
	private StateVO getSingleSourceStateIfAny(Collection<CollectableGenericObjectWithDependants> selectedCollectables) {
		UID sourceStateId = null;
		for (CollectableGenericObjectWithDependants go : selectedCollectables) {
			UID currentStateId = (UID) go.getValueId(SF.STATE.getUID(go.getEntityUID())); 
			if (sourceStateId == null) {
				sourceStateId = currentStateId;
			} else {
				if (!sourceStateId.equals(currentStateId)) {
					sourceStateId = null;
					break;
				}
			}
		}
		return sourceStateId == null ? null : StateDelegate.getInstance()
				.getState(getModuleId(), sourceStateId);
	}

	public void changeCustomUsage(String customUsage)
			throws CommonBusinessException {
		super.setCustomUsage(customUsage);
		CollectState state = getCollectStateModel().getCollectState();
		if (state.isSearchMode()) {
			this.reloadLayoutForSearchTab();
		} else if (state.isDetailsMode()) {
			this.reloadLayoutForDetailsTab(false);
		}
	}

	@Override
	protected void restoreLocalizedValuesIntoModel(CollectableGenericObjectWithDependants clct, CollectableComponent clctcomp) {
		
		DetailsLocalizedComponentModel model = (DetailsLocalizedComponentModel) clctcomp.getModel();
		
		if (clct.getGenericObjectCVO().getDataLanguageMap() == null) {
			clct.getGenericObjectCVO().setDataLanguageMap(new DataLanguageMap());			
		}
		
		if (model.getDataLanguageMap() != null) {
			clct.getGenericObjectCVO().setDataLanguageMap(
					model.getDataLanguageMap());			
		}
	}
	
	
	@Override
	protected DetailsController<Long, CollectableGenericObjectWithDependants> createDetailsController() {
		return new MyDetailsController(this, createDeleteOrRestoreAction());
	}

	protected void stateChangeStartInit() {
	}

	protected void stateChangeFinishedOk() {
	}

	protected void stateChangeFinishedWithError() {
	}
	
	@Override
	protected void setupShortcutsForTabs(MainFrameTab frame) {
		super.setupShortcutsForTabs(frame);
		
		// the focus actions
		int selectedIndex;
		try {
			selectedIndex = getCollectPanel().getTabbedPaneSelectedIndex();
		} catch (IllegalArgumentException ex) {
			// BMWFDM-585: ignore the focus action after delete here
			selectedIndex = -1;
		}
		
		final SearchOrDetailsPanel searchOrDetailsPanel;
		if (selectedIndex >= 0) {
			searchOrDetailsPanel =
				selectedIndex == CollectPanel.TAB_SEARCH ? getSearchPanel()
						: (selectedIndex == CollectPanel.TAB_DETAILS ? getDetailsPanel() : null);
		} else {
			searchOrDetailsPanel = null;
		}
		
		if (searchOrDetailsPanel != null) {
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.FOCUS_ON_INITIAL_COMPONENT, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					final Collection<JTabbedPane> colTabbedPanes = UIUtils.findAllInstancesOf(searchOrDetailsPanel, JTabbedPane.class);
					for (JTabbedPane jTabbedPane : colTabbedPanes) {
						if (UIUtils.findJComponent(jTabbedPane, FocusManager.getCurrentManager().getFocusOwner()) != null) {
							if (getCollectPanel().getTabbedPaneSelectedIndex() == CollectPanel.TAB_SEARCH)
								setInitialComponentFocusInSearchTab();
							else if (getCollectPanel().getTabbedPaneSelectedIndex() == CollectPanel.TAB_DETAILS)
								setInitialComponentFocusInDetailsTab();
							return;
						}	
					}
				}
			}, searchOrDetailsPanel);
			
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.FOCUS_ON_STATE_COMPONENT, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					EventQueue.invokeLater(new Runnable() {
						@Override
			            public void run() {
							if (getCollectPanel().getTabbedPaneSelectedIndex() == CollectPanel.TAB_SEARCH)
								clctSearchState.getFocusableComponent().requestFocusInWindow();
							else if (getCollectPanel().getTabbedPaneSelectedIndex() == CollectPanel.TAB_DETAILS) {
								final JComponent[] comps = getDetailsPanel().getToolBar().getComponents(NuclosToolBarItems.CHANGE_STATE);
								for (int i = 0; i < comps.length; i++) {
									final JComponent jc = comps[i];
									for (int j = 0; j < jc.getComponentCount(); j++) {
										jc.getComponent(j).requestFocusInWindow();
									}
								}
							}							
						}
					});
				}
			}, searchOrDetailsPanel);
		
			KeyBindingProvider.bindActionToComponent(KeyBindingProvider.FOCUS_ON_GENERATION_COMPONENT, new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					EventQueue.invokeLater(new Runnable() {
						@Override
			            public void run() {
							if (getCollectPanel().getTabbedPaneSelectedIndex() == CollectPanel.TAB_DETAILS) {
								final JComponent[] comps = getDetailsPanel().getToolBar().getComponents(NuclosToolBarItems.GENERATE);
								for (int i = 0; i < comps.length; i++) {
									comps[i].requestFocusInWindow();
								}
							}							
						}
					});
				}
			}, searchOrDetailsPanel);
		}
	}
	
	/**
	 * create action for delete/restore toolbar button
	 * 
	 * @return Action
	 */
	protected DeleteOrRestoreToggleAction createDeleteOrRestoreAction() {
		 return NuclosToolBarActions.DELETE
			.init(new DeleteOrRestoreToggleAction() {

				@Override
				public void actionPerformed(ActionEvent ev) {
					if (DeleteOrRestoreToggleAction.Mode.RESTORE == getMode()) {
						cmdRestoreCurrentCollectableInDetails();
					} else {
						cmdDeleteCurrentCollectableInDetails();
					}
				}
			});
	}

	@Override
	protected void writeCurrentCollectableToPreferences(NuclosCollectController.RestorePreferences rp) {
		super.writeCurrentCollectableToPreferences(rp);

		final GenericObjectVO genericObject = getSelectedGenericObjectCVO();
		if (genericObject != null) {
			rp.setProcess(genericObject.getProcess());
		}
	}
} // class GenericObjectCollectController
