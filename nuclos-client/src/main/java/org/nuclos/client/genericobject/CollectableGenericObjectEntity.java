//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.CacheableListener;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.genericobject.CollectableGenericObjectEntityField;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.LangUtils;

/**
 * Contains meta information about a leased object.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableGenericObjectEntity implements CollectableEntity, Serializable {

	/**
	 * Caches CollectableGenericObjectEntities by module.
	 */
	public static class Cache implements CacheableListener, Serializable {
		private static Cache singleton;

		private Map<UID, CollectableGenericObjectEntity> mpByModuleId =
				new HashMap<UID, CollectableGenericObjectEntity>(Modules.getInstance().getModuleCount());

		public synchronized static Cache getInstance() {
			if (singleton == null) {
				singleton = new Cache();
			}
			return singleton;
		}

		private Cache() {
			// Note that this listener is never removed. However this should not be an issue.
			GenericObjectMetaDataCache.getInstance().addCacheableListener(this);

			/** @todo Is this still necessary? */
			AttributeCache.getInstance().addCacheableListener(this);
		}

		public void cacheableChanged() {
			this.invalidate();
		}

		public synchronized void invalidate() {
			mpByModuleId.clear();
		}

		/**
		 * §precondition iModuleId != null
		 * §postcondition result != null
		 */
		public synchronized CollectableEntity getCollectableEntityByModule(UID moduleUid) {
			if (moduleUid == null) {
				throw new NullArgumentException("iModuleId");
			}
			if (!mpByModuleId.containsKey(moduleUid)) {
				final Modules modules = Modules.getInstance();
				final UID nameUid = modules.getModule(moduleUid).getUID();
				final String sLabel = modules.getModule(moduleUid).getEntityName();
				// Note that only attributes occuring in Details layouts are taken into account for building the entity:
				/* the collection from the AttributeCache must not be modified. A new one is created instead */
				final Collection<UID> collFieldUids = 
						new HashSet<UID>(GenericObjectMetaDataCache.getInstance().getAttributeUids(moduleUid, Boolean.FALSE));
				mpByModuleId.put(moduleUid, new CollectableGenericObjectEntity(nameUid, sLabel, collFieldUids));
			}
			final CollectableEntity result = mpByModuleId.get(moduleUid);
			assert result != null;
			return result;
		}
	}	// inner class Cache

	private final UID entityUid;
	private final String sLabel;
	private final Set<UID> fieldUids;
	private final EntityMeta<?> meta;

	/**
	 * creates a leased object entity with the given field names.
	 * 
	 * §precondition collFieldNames != null
	 */
	public CollectableGenericObjectEntity(UID entityUid, String sLabel, Collection<UID> collFieldUids) {
		if (collFieldUids == null) {
			throw new NullArgumentException("collFieldNames");
		}
		this.entityUid = entityUid;
		this.sLabel = sLabel;
		this.fieldUids = new HashSet<UID>(collFieldUids);
		this.meta = MetaProvider.getInstance().getEntity(entityUid);
	}

	/**
	 * creates a leased object entity for the given module.
	 * 
	 * §postcondition result != null
	 * 
	 * @param moduleUid If <code>null</code>, returns the module-independent entity for all attributes.
	 */
	public static CollectableEntity getByModuleUid(UID moduleUid) {
		final CollectableEntity result;
		result = Cache.getInstance().getCollectableEntityByModule(moduleUid);

		assert result != null;
		return result;
	}

	@Override
	public UID getUID() {
		return entityUid;
	}

	@Override
	public String getLabel() {
		return sLabel;
	}

	@Override
	public CollectableEntityField getEntityField(UID fieldUid) {
		checkContained(fieldUid);
		
		CollectableEntityField result = null;
		FieldMeta<?> fMeta = MetaProvider.getInstance().getEntityField(fieldUid);
		Map<UID, Permission> mpPermissions = null;
		if (fMeta.isCalcAttributeAllowCustomization()) {
			mpPermissions = AttributeCache.getInstance().getAttribute(LangUtils.defaultIfNull(fMeta.getCalcBaseFieldUID(), fMeta.getUID())).getPermissions();
		} else {
			mpPermissions = AttributeCache.getInstance().getAttribute(fieldUid).getPermissions();
		}
		result = new CollectableGenericObjectEntityField(mpPermissions, fMeta, getUID());
		
		result.setCollectableEntity(this);
		return result;
	}

	private void checkContained(UID fieldUid) {
		/** @todo shouldn't this throw an exception when stFieldNames == null? */
		if (fieldUids != null) {
			if (!fieldUids.contains(fieldUid)) {
				/** @todo ENABLE THIS AGAIN, DOES NOT WORK FOR COMMON SEARCH HERE YET */
				//throw new NuclosFatalException("Das Attribut " + sFieldName + " ist nicht im Modul " + this.getLabel() + " vorhanden.");
			}
		}
	}

	@Override
	public Set<UID> getFieldUIDs() {
		return Collections.unmodifiableSet(this.fieldUids);
	}

	/**
	 * @deprecated
	 */
	//@Override
	public UID getIdentifierFieldUid() {
		return SF.SYSTEMIDENTIFIER.getMetaData(entityUid).getUID();
	}


	@Override
	public int getFieldCount() {
		return this.fieldUids.size();
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("name=").append(entityUid);
		//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
		//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//				result.append(",label=").append(getIdentifierLabel());
		result.append("]");
		return result.toString();
	}

	@Override
	public EntityMeta<?> getMeta() {
		return meta;
	}

}	// class CollectableGenericObjectEntity
