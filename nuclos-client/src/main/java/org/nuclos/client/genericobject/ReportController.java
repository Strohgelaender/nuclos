//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.datasource.admin.DatasourceCollectController;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.main.MainController;
import org.nuclos.client.report.ReportDelegate;
import org.nuclos.client.report.reportrunner.BackgroundProcessStatusController;
import org.nuclos.client.report.reportrunner.ReportAttachmentInfo;
import org.nuclos.client.report.reportrunner.ReportRunner;
import org.nuclos.client.report.reportrunner.ReportThread;
import org.nuclos.client.ui.Controller;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.format.FormattingTransformer;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ejb3.DatasourceFacadeRemote;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.DefaultReportOutputVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.PageOrientation;
import org.nuclos.common.report.valueobject.ReportVO;
import org.nuclos.common.report.valueobject.ReportVO.OutputType;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.genericobject.AbstractProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

import net.sf.jasperreports.engine.JRReport;

/**
 * ReportController for exporting leased object forms.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class ReportController extends Controller<JComponent> {

	private static final Logger LOG = Logger.getLogger(ReportController.class);
	
	private String sLastGeneratedFileName;

	private final Map<UID, List<String>> mpGeneratedFileNames = CollectionUtils.newHashMap();

	private boolean bFilesBeAttached = false;

	UID moduleUid;
	
	public static String PAGE_ORIENTATION_PREFS = "reportpageorientation";
	public static String COLUMN_SCALED_PREFS = "columnscaled";

	/**
	 * @param parent
	 */
	public ReportController(JComponent parent) throws NuclosFatalException {
		super(parent);
	}

	private static class ReportSelectionPanelMouseListener extends MouseAdapter {
		final JOptionPane pane;
        final JDialog dialog;
        
        private ReportSelectionPanelMouseListener(JOptionPane pane, JDialog dialog) {
        	this.pane = pane;
        	this.dialog = dialog;
        }
        
		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getClickCount() == 2) {
				pane.setValue(JOptionPane.OK_OPTION);
				dialog.hide();
			}
		}
	}

	/**
	 * shows dialog to choose export form for a list of selected leased objects, using only common forms of selection
	 */
	public void exportForm(final List<? extends CollectableGenericObject> lstclctlo, UsageCriteria usagecriteria,
			final  UID documentEntityUID, final UID[] documentFieldUIDs)
			throws NuclosFatalException, NuclosReportException {
		try {
			final ReportSelectionPanel pnlSelection = prepareReportSelectionPanel(usagecriteria, lstclctlo.size());
			if (pnlSelection.getReportsTable().getModel().getRowCount() == 1) {
				executeForm(pnlSelection, lstclctlo, documentEntityUID, documentFieldUIDs);
				return;
			}
			
			String sDialogTitle = getSpringLocaleDelegate().getMessage("ReportController.15","Verf\u00fcgbare Formulare");
			//int btnValue = JOptionPane.showConfirmDialog(this.getParent(), pnlSelection, sDialogTitle, JOptionPane.OK_CANCEL_OPTION);
	        final JOptionPane pane = new JOptionPane(pnlSelection, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, null, null);
	        final JDialog dialog = pane.createDialog(getParent(), sDialogTitle);
	        	    
	        pnlSelection.addDoubleClickListener(new ReportSelectionPanelMouseListener(pane, dialog));
	       
	        dialog.setResizable(true);
	        dialog.setVisible(true);
			if ((pane.getValue() == null? -1: (Integer)pane.getValue()) == JOptionPane.OK_OPTION)
			{
				executeForm(pnlSelection, lstclctlo, documentEntityUID, documentFieldUIDs);
			}
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	/**
	 * prepares the report selection panel for use in print (or better: export) dialog box
	 */
	public static ReportSelectionPanel prepareReportSelectionPanel(UsageCriteria usagecriteria, int iObjectCount) throws NuclosReportException {

		final ReportFacadeRemote reportFacadeRemote = SpringApplicationContextHolder.getBean(ReportFacadeRemote.class);
		final Collection<DefaultReportVO> collReports = reportFacadeRemote.findReportsByUsage(usagecriteria);
		final boolean bSingleSelection = (iObjectCount == 1);
		if (collReports.size() <= 0) {
			final String sExceptionText = (bSingleSelection ?
				SpringLocaleDelegate.getInstance().getMessage("ReportController.9","Es ist noch kein Formular zugeordnet.") : 
				SpringLocaleDelegate.getInstance().getMessage("ReportController.8","Dieser Auswahl sind keine gemeinsamen Formulare zugeordnet."));
			throw new NuclosReportException(sExceptionText);
		}

		return newReportSelectionPanel(usagecriteria.getEntityUID(), collReports, true);
	}

	/**
	 * get if exists assigned forms for the given module
	 * @param usagecriteria
	 */
	public boolean hasFormsAssigned(UsageCriteria usagecriteria){
		try {
			return (ReportDelegate.getInstance().findReportsByUsage(usagecriteria).size() > 0);
		} catch (RuntimeException e) {
			throw new CommonFatalException(e);
		}
	}

	// show Background Process dialog for file attachment information.
	private static class StatusDialogRunnable implements Runnable {
		
		private final ReportSelectionPanel pnlSelection;
		
		private StatusDialogRunnable(ReportSelectionPanel pnlSelection) {
			this.pnlSelection = pnlSelection;
		}
		
		@Override
		public void run() {
			try {
				BackgroundProcessStatusController.getStatusDialog(UIUtils.getFrameForComponent(pnlSelection)).setVisible(true);
			} catch (Exception e) {
				LOG.error("executeForm failed: " + e, e);
			}																									
		}
	}
	
	/**
	 * executes the selected form for each single leased object, one after the other
	 * @param pnlSelection
	 * @param lstclctlo
	 * @param documentFieldNames
	 * @param facade
	 * @throws RemoteException
	 */
	private void executeForm(final ReportSelectionPanel pnlSelection, final List<? extends CollectableGenericObject> lstclctlo, final UID documentEntityUID, final UID[] documentFieldUIDs) {

		final ReportSelectionPanel.ReportEntry entry = pnlSelection.getSelectedPrintProfiles();
		final DatasourceFacadeRemote dsFacadeRemote = SpringApplicationContextHolder.getBean(DatasourceFacadeRemote.class);
		
		/** this is a precondition */
		if (entry == null) {
			return;
		}
		
		moduleUid = null;

		this.bFilesBeAttached = pnlSelection.getAttachReport();

		// show Background Process dialog for file attachment information.
		if (bFilesBeAttached) {
			// do not set dialog visible per default. @see NUCLOS-1064
			//SwingUtilities.invokeLater(new StatusDialogRunnable(pnlSelection));
		}

		final DefaultReportVO reportvo = entry.getReport();
		
		
		final Thread thread;
		switch (reportvo.getOutputType()) {
			case SINGLE: {
				final DefaultReportOutputVO outputvo = entry.getOutput();
				thread = new Thread() {
					@Override
					public void run() {
						try {
							// loop over list of Leased Objects
							for (CollectableGenericObject clctlo : lstclctlo) {
								final String sGenericObjectId = clctlo.getId().toString();
								final String sGenericObjectIdentifier = clctlo.getGenericObjectCVO().getSystemIdentifier();

								ReportAttachmentInfo info = null;
								if (bFilesBeAttached) {
									info = new ReportAttachmentInfo(clctlo.getId(), sGenericObjectIdentifier, documentEntityUID, documentFieldUIDs);
								}

								final Map<String, Object> params = CollectionUtils.newHashMap();
								List<DatasourceParameterVO> lstParameters = DatasourceDelegate.getInstance().getParametersFromXML(DatasourceDelegate.getInstance().getDatasource(reportvo.getDatasourceId()).getSource());
								List<DatasourceParameterVO> lstNewParameters = new ArrayList<DatasourceParameterVO>();
								for (DatasourceParameterVO dspvo: lstParameters) {
									if (!dspvo.getParameter().equals("intid")) {
										lstNewParameters.add(dspvo);
									}
								}
								boolean result = DatasourceCollectController.createParamMap(reportvo.getName(), lstNewParameters, params, getParent(), null);
								if (result) {
									params.put("GenericObjectID", sGenericObjectId);
									params.put("intid", sGenericObjectId);
									if (!StringUtils.isNullOrEmpty(sGenericObjectIdentifier)) {
										params.put("GenericObjectIdentifier", sGenericObjectIdentifier);
									}
									moduleUid = clctlo.getGenericObjectCVO().getModule();

									UID foundlanguage = null;
									
									try {
										foundlanguage = dsFacadeRemote.getReportLanguageToUse(reportvo.getPrimaryKey(), 
												DataLanguageContext.getLanguageToUse(),new Long(sGenericObjectId));
									} catch (NuclosReportException e) {
										throw new CommonFatalException(e);
									}
									
									final UID language = foundlanguage;
									
									
									final ReportThread reportThread = ReportRunner.createJob(getParent(), params, reportvo, outputvo, true, null, null, info, language);
									//we need to wait, because excel/pdf can't open many files in 1 second (NUCLEUSINT-333)
									Thread.sleep(1000);
									reportThread.start();
									reportThread.join();
								}
							}
						}
						catch (final Exception ex) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									Errors.getInstance().showExceptionDialog(getParent(), 
											getSpringLocaleDelegate().getMessage("ReportController.10","Fehler beim Ausf\u00fchren des Reports") + ":", ex);
								}
							});
						}
					}
				};
				break;
			}
			default: {
				// COLLECTIVE or EXCEL:
				thread = new Thread() {
					private Collection<DefaultReportOutputVO> collFormat;
					{
						try {
							collFormat = ReportDelegate.getInstance().getReportOutputs(reportvo.getId());
						}
						catch (RuntimeException ex) {
							throw new CommonFatalException(ex);
						}
					}
					@Override
					public void run() {
						try {
							for (CollectableGenericObject clctlo : lstclctlo) {
								final String sGenericObjectId = clctlo.getId().toString();
								final String sGenericObjectIdentifier = clctlo.getGenericObjectCVO().getSystemIdentifier();

								ReportAttachmentInfo info = null;
								if (bFilesBeAttached) {
									info = new ReportAttachmentInfo(clctlo.getId(), sGenericObjectIdentifier, documentEntityUID, documentFieldUIDs);
								}

								final Map<String, Object> mpParams = CollectionUtils.newHashMap();
								mpParams.put("GenericObjectID", sGenericObjectId);
								mpParams.put("intid", sGenericObjectId);
								if (!StringUtils.isNullOrEmpty(sGenericObjectIdentifier)) {
									mpParams.put("GenericObjectIdentifier", sGenericObjectIdentifier);
								}
								// if the report is a collective report, execute all contained outputs
								boolean bIsFirstOfMany = true;
								for (Iterator<DefaultReportOutputVO> iterOutput = collFormat.iterator(); iterOutput.hasNext();) {
									final DefaultReportOutputVO outputvo = iterOutput.next();
									outputvo.setIsFirstOfMany(bIsFirstOfMany);
									outputvo.setIsLastOfMany(!iterOutput.hasNext());

									// Remember Excel document name for Excel collective reports
									if (!bIsFirstOfMany && reportvo.getOutputType() == ReportVO.OutputType.EXCEL && getLastGeneratedFileName() != null)
									{
										outputvo.setParameter(getLastGeneratedFileName());
									}
									else {
										setLastGeneratedFileName(null);
									}

									UID foundlanguage = null;
									
									try {
										foundlanguage = dsFacadeRemote.getReportLanguageToUse(reportvo.getPrimaryKey(), 
												DataLanguageContext.getLanguageToUse(),new Long(sGenericObjectId));
									} catch (NuclosReportException e) {
										throw new CommonFatalException(e);
									}
									
									final UID language = foundlanguage;
									mpParams.put("dataLocale", language.toString());
									
									final ReportThread threadReport = ReportRunner.createJob(getParent(), mpParams, reportvo, outputvo, true,
											Integer.getInteger(ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_REPORT_MAXROWCOUNT)), null, info, language);
									threadReport.start();
									threadReport.join();
									final String sFileName = threadReport.getDocumentName();
									if (sFileName != null) {
										setLastGeneratedFileName(sFileName);
									}

									bIsFirstOfMany = false;
									
									// execute only one reportoutput for "Excel-Sammelausgabe"
									if (reportvo.getOutputType() == OutputType.EXCEL) {
										break;
									}
								}
							}
						}
						catch (final InterruptedException ex) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									Errors.getInstance().showExceptionDialog(getParent(), 
											getSpringLocaleDelegate().getMessage(
													"ReportController.6","Die Ausf\u00fchrung des Reports wurde unerwartet unterbrochen") + ": ", ex);
								}
							});
						}
						catch (final Exception ex) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									Errors.getInstance().showExceptionDialog(getParent(), 
											getSpringLocaleDelegate().getMessage(
													"ReportController.11","Fehler beim Ausf\u00fchren des Reports") + ":", ex);
								}
							});
						}
					}
				};
			}
		}	// switch
		thread.start();
	}

	private String getPath(String path, final CollectableGenericObject oParent) {
		return StringUtils.replaceParameters(RigidUtils.PARAM_PATTERN_UID, path, new FormattingTransformer() {
			@Override
			protected Object getValue(UID field) {
				return oParent.getValue(field);
			}
		});
	}

	/**
	 * preparation of ReportSelectionPanel for futher use
	 * @param collreportvo Collection of appropriate reports
	 * @param facade Report facade
	 * @param bShowAttachReport Decides, whether checkbox 'Dokument anh\u00e4ngen' will be shown.
	 * @return ReportSelectionPanel
	 * @throws RemoteException
	 */
	private static ReportSelectionPanel newReportSelectionPanel(UID entityUid, Collection<DefaultReportVO> collreportvo, boolean bShowAttachReport) {

		final ReportFacadeRemote reportFacadeRemote = SpringApplicationContextHolder.getBean(ReportFacadeRemote.class);
		final ReportSelectionPanel result = new ReportSelectionPanel(entityUid, bShowAttachReport);
		try {
			for (DefaultReportVO reportvo : collreportvo) {
				if (reportvo.getOutputType() == ReportVO.OutputType.SINGLE) {
					for (DefaultReportOutputVO outputvo : reportFacadeRemote.getReportOutputs(reportvo.getId())) {
						result.addReport(reportvo, outputvo);
					}
				} else {
					// collective reports/forms should occur only once in list
					result.addReport(reportvo, null);
				}
			}
			result.restoreColumnOrderAndWidths();
			result.selectFirstReport();
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}

		return result;
	}

	public Map<UID, List<String>> getGeneratedFileNames() {
		return this.mpGeneratedFileNames;
	}

	public void setLastGeneratedFileName(String sFileName) {
		this.sLastGeneratedFileName = sFileName;
	}

	public String getLastGeneratedFileName() {
		return this.sLastGeneratedFileName;
	}

	public boolean getFilesBeAttached() {
		return this.bFilesBeAttached;
	}

	/**
	 * @param clcteMain the main collectable entity
	 * @param searchexpr
	 * @param lstclctefweSelected List&lt;CollectableEntityFieldWithEntity&gt; 
	 * 		the collectable entity fields (along with their entities) that are to be exported.
	 * @param lstclctlo
	 * @param usagecriteria
	 */
	public boolean export(CollectableEntity clcteMain, CollectableSearchExpression searchexpr,
			List<CollectableEntityField> lstclctefweSelected, List<Integer> selectedFieldWidth,
			final boolean resultsSelected, final UsageCriteria usagecriteria,
			Runnable printForm, String customUsage, final Integer iSpreedSheetLimit, Runnable finishCallback)
			throws NuclosBusinessException {
		final ReportFormatController formatController;
		boolean exportLaunched = false;

		if (!resultsSelected) {
			formatController = new ReportFormatController(clcteMain.getUID(), getParent());
			String title = getSpringLocaleDelegate().getMessage("ReportController.13","Suchergebnis exportieren");

			while (formatController.run(title)) {
				if (formatController.isSpreadSheet() && iSpreedSheetLimit != null) {
					String msg = getSpringLocaleDelegate().getMessage("EntityCollectController.SpreadSheetLimit",
							"The list is too large for a spreedsheet. Please limit to a max number of {0}!", iSpreedSheetLimit - 1);
					JOptionPane.showMessageDialog(MainController.getMainFrame(), msg);
					continue;
				}

				exportLaunched = export(clcteMain, searchexpr, lstclctefweSelected, selectedFieldWidth, formatController.getFormat(), customUsage, formatController.getPageOrientation(), formatController.isColumnScaled(), finishCallback);
				break;
			}
		}
		else {

			formatController = new ChoiceListOrReportExportController(clcteMain.getUID(), getParent());
			String title = getSpringLocaleDelegate().getMessage("ReportController.12","Suchergebnis exportieren / Formulardruck");

			while (formatController.run(title)) {

				if (formatController.isSpreadSheet() && iSpreedSheetLimit != null) {
					String msg = getSpringLocaleDelegate().getMessage("EntityCollectController.SpreadSheetLimit",
							"The list is too large for a spreedsheet. Please change format or limit to a max number of {0}!", iSpreedSheetLimit);
					JOptionPane.showMessageDialog(MainController.getMainFrame(), msg);
					continue;
				}

				final ChoiceListOrReportExportPanel pnlChoiceExport = ((ChoiceListOrReportExportController) formatController).pnlChoiceExport;
				if (pnlChoiceExport.getReportButton().isSelected()) {
					try {
						printForm.run();
					}
					catch (RuntimeException ex) {
						throw new NuclosBusinessException("RemoteException: Collection of report outputs couldn't be created.", ex);
					}
				}
				else if (pnlChoiceExport.getListButton().isSelected()) {
					formatController.pnlFormat.writeFormatSettingsFromPreferences(clcteMain.getUID()); // write preferences here.
					exportLaunched = export(clcteMain, searchexpr, lstclctefweSelected, selectedFieldWidth, formatController.getFormat(), customUsage, formatController.getPageOrientation(), formatController.isColumnScaled(), finishCallback);
				}

				break;
			}
		}

		return exportLaunched;
	}

	/**
	 * @param clcteMain the main collectable entity
	 * @param searchexpr
	 * @param lstclctefweSelected List<CollectableEntityFieldWithEntity> the collectable entity fields (along with their entities) that are to be exported.
	 * @param format
	 * @throws NuclosBusinessException
	 */
	private boolean export(CollectableEntity clcteMain, CollectableSearchExpression searchexpr, List<? extends CollectableEntityField> lstclctefweSelected, List<Integer> selectedFieldWidth,
			ReportOutputVO.Format format, String customUsage, byte page_orientation, boolean columnScaled, Runnable finishCallback) throws NuclosBusinessException {
		boolean exportLaunched = false;
		try {
			getParent().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			PageOrientation orientation = PageOrientation.PORTRAIT;
			if (page_orientation == JRReport.ORIENTATION_LANDSCAPE) {
				orientation = PageOrientation.LANDSCAPE;
			}

			ReportThread t = ReportRunner.createExportJob(getParent(), format, searchexpr, lstclctefweSelected, selectedFieldWidth, clcteMain.getUID(), customUsage, orientation, columnScaled, finishCallback);
			t.start();
			exportLaunched = true;
		}
		finally {
			getParent().setCursor(Cursor.getDefaultCursor());
		}
		return exportLaunched;
	}

	public void export(UID entityUid, String searchCondition, JTable table, String sDatasourceName) throws NuclosBusinessException {
		export(entityUid, searchCondition, table, null, sDatasourceName);
	}

	public void export(UID entityUid, String searchCondition, JTable table, JTable headertable, String sDatasourceName) throws NuclosBusinessException {
		Future<LayerLock> lock = null;
		try {
			final ReportFormatController formatctl = new ReportFormatController(entityUid, getParent());
			if (formatctl.run(getSpringLocaleDelegate().getMessage("ReportController.14","Tabelle exportieren"))) {
				lock = UIUtils.lockFrame(getParent());
				
				formatctl.pnlFormat.writeFormatSettingsFromPreferences(entityUid);
				
				final ResultVO resultvo = getResultVOForTable(table, headertable);
				
				ReportRunner.createExportJob(getParent(), searchCondition, resultvo, formatctl.getFormat(), 
						formatctl.getPageOrientation(), formatctl.isColumnScaled(), null, sDatasourceName).start();

			}
		}
		finally {
			UIUtils.unLockFrame(getParent(), lock);
		}
	}
	
	private static ResultVO getResultVOForTable (JTable tbl, JTable htbl) {
		final ResultVO result = new ResultVO();
			// fill the columns:
		final TableColumnModel columnmodel = tbl.getTableHeader().getColumnModel();

		if (htbl != null) {
			final TableColumnModel hcolumnmodel = htbl.getTableHeader().getColumnModel();

			for (int iColumn = 1; iColumn < hcolumnmodel.getColumnCount(); iColumn++) {
				final ResultColumnVO resultcolumnvo = new ResultColumnVO();
				resultcolumnvo.setColumnLabel(hcolumnmodel.getColumn(iColumn).getHeaderValue().toString());
				resultcolumnvo.setColumnClassName("java.lang.Object");

				int width = hcolumnmodel.getColumn(iColumn).getPreferredWidth();
				resultcolumnvo.setColumnWidth(width);

				result.addColumn(resultcolumnvo);
			}
		}

		for (int iColumn = 0; iColumn < columnmodel.getColumnCount(); iColumn++) {
			final ResultColumnVO resultcolumnvo = new ResultColumnVO();
			resultcolumnvo.setColumnLabel(columnmodel.getColumn(iColumn).getHeaderValue().toString());
			resultcolumnvo.setColumnClassName("java.lang.Object");
			
			int width = columnmodel.getColumn(iColumn).getPreferredWidth();
			resultcolumnvo.setColumnWidth(width);
			
			result.addColumn(resultcolumnvo);
		}

		// fill the rows:
		for (int iRow = 0; iRow < tbl.getRowCount(); iRow++) {
			int columnOffset = 0;
			Object[] aoData =null;
			
			if (htbl != null) {
				aoData = new Object[tbl.getColumnCount() + htbl.getColumnCount() - 1];

				for (int iColumn = 1; iColumn < htbl.getColumnCount(); iColumn++) {
					final Object oValue = htbl.getValueAt(iRow, iColumn);
					if (oValue instanceof CollectableField) {
						aoData[iColumn-1] = ((CollectableField) oValue).getValue();
					}
					else if (oValue instanceof String) {
						aoData[iColumn-1] = (oValue == null) ? null : ((String) oValue).replace('\n', ' ').trim();
					}
					else {
						aoData[iColumn-1] = oValue;
					}
					final ResultColumnVO resultcolumnvo = result.getColumns().get(iColumn-1);
					if (resultcolumnvo.getColumnClassName().equals(java.lang.Object.class.getName()) && aoData[iColumn-1] != null) {
						resultcolumnvo.setColumnClassName(aoData[iColumn-1].getClass().getName());
					}
					columnOffset++;
				}
			}else
			{
				aoData = new Object[tbl.getColumnCount() ];
			}			
			
			for (int iColumn = 0; iColumn < tbl.getColumnCount(); iColumn++) {
				final Object oValue = tbl.getValueAt(iRow, iColumn);
				if (oValue instanceof CollectableField) {
					aoData[iColumn+columnOffset] = ((CollectableField) oValue).getValue();
				}
				else if (oValue instanceof String) {
					aoData[iColumn+columnOffset] = (oValue == null) ? null : ((String) oValue).replace('\n', ' ').trim();
				}
				else {
					aoData[iColumn+columnOffset] = oValue;
				}
				final ResultColumnVO resultcolumnvo = result.getColumns().get(iColumn+columnOffset);
				if (resultcolumnvo.getColumnClassName().equals(java.lang.Object.class.getName()) && aoData[iColumn+columnOffset] != null) {
					resultcolumnvo.setColumnClassName(aoData[iColumn+columnOffset].getClass().getName());
				}
			}
			result.addRow(aoData);
		}
		return result;
	}


}	// class ReportController
