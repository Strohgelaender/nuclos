package org.nuclos.client.genericobject.controller;

import javax.swing.*;

import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.common.controller.SearchConditionSource;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.common.UID;
import org.nuclos.common.collect.ControllerPresentation;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class GenericObjectResultPanel extends NuclosResultPanel<Long, CollectableGenericObjectWithDependants> {

	public GenericObjectResultPanel(UID entityId, ControllerPresentation viewmode, Action actFilter, SearchConditionSource searchConditionSource) {
		super(entityId, viewmode, actFilter, searchConditionSource);
	}

}
