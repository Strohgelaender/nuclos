//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.awt.*;
import java.util.prefs.Preferences;

import javax.swing.*;
import javax.swing.border.Border;

import org.apache.log4j.Logger;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.ClientPreferences;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;
import net.sf.jasperreports.engine.JRReport;

/**
 * Panel for selection of report output format.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris@novabit.de">Boris</a>
 * @version 01.00.00
 */
public class ReportFormatPanel extends JPanel {
	
	private static final Logger LOG = Logger.getLogger(ReportFormatPanel.class);
	
	public static final String LANDSCAPE = "landscape";
	public static final String PORTRAIT = "portrait";

	private final JLabel lblHeadline = new JLabel();
	private final ButtonGroup bgFormat = new ButtonGroup();
	
	private final JPanel pnlPageOrientation = new JPanel();
	private final ButtonGroup bgPageOrientation = new ButtonGroup();
	private final JRadioButton rbLandscape = new JRadioButton();
	private final JRadioButton rbPortrait = new JRadioButton();
	
	private final JCheckBox cbColumnScale = new JCheckBox();

	public ReportFormatPanel(UID entityUid) {
		final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
		final GridBagLayout gridBagLayout1 = new GridBagLayout();
		final Border border1 = BorderFactory.createEmptyBorder(1, 1, 1, 1);
		
		final JRadioButton rbPdf = new JRadioButton();
		final JRadioButton rbXls = new JRadioButton();
		final JRadioButton rbXlsx = new JRadioButton();
		final JRadioButton rbCsv = new JRadioButton();
		rbPdf.setActionCommand("PDF");
		rbCsv.setActionCommand("CSV");
		rbXls.setActionCommand("XLS");
		rbXlsx.setActionCommand("XLSX");
		rbPdf.setText(localeDelegate.getMessage("ReportFormatPanel.1","Adobe(tm) Acrobat Reader (PDF)"));
		rbCsv.setText(localeDelegate.getMessage("ReportFormatPanel.3","CSV"));
		rbCsv.setPreferredSize(new Dimension(200,20));
		rbXlsx.setText(localeDelegate.getMessage("ReportFormatPanel.10","Microsoft(tm) Excel XML Worksheet (XLSX)"));
		rbXls.setText(localeDelegate.getMessage("ReportFormatPanel.4","Microsoft(tm) Excel Worksheet (XLS)"));
		rbPdf.setSelected(true);
		
		lblHeadline.setToolTipText("");
		lblHeadline.setText(localeDelegate.getMessage("ReportFormatPanel.2","Bitte w\u00e4hlen Sie das Zielformat aus:"));
		setBorder(border1);
		setLayout(gridBagLayout1);
		
		final double size [][] = {{10, TableLayout.FILL, 10, TableLayout.FILL}, {TableLayout.PREFERRED, 10, TableLayout.PREFERRED}};
		final TableLayout layout = new TableLayout(size);	
		pnlPageOrientation.setLayout(layout);
		pnlPageOrientation.setBorder(BorderFactory.createTitledBorder(localeDelegate.getMessage("ReportFormatPanel.8", "PDF Einstellungen")));
		
		rbLandscape.setText(localeDelegate.getMessage("ReportFormatPanel.5","Landscape"));		
		rbLandscape.setActionCommand(LANDSCAPE);
		rbPortrait.setText(localeDelegate.getMessage("ReportFormatPanel.6","Portrait"));
		rbPortrait.setActionCommand(PORTRAIT);
		rbPortrait.setSelected(true);
		cbColumnScale.setText(localeDelegate.getMessage("ReportFormatPanel.7", "Auf Seitenbreite skalieren"));
		
		readFormatSettingsFromPreferences(entityUid);
		
		pnlPageOrientation.add(rbPortrait, "1,0");
		pnlPageOrientation.add(rbLandscape, "3,0");
		pnlPageOrientation.add(cbColumnScale, "1,2");
		
		bgPageOrientation.add(rbLandscape);
		bgPageOrientation.add(rbPortrait);
		
		add(lblHeadline, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 4, 0), 0, 0));
		add(rbPdf, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		add(pnlPageOrientation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		add(rbCsv, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		add(rbXlsx, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		add(rbXls, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0
				, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		bgFormat.add(rbPdf);
		bgFormat.add(rbCsv);
		bgFormat.add(rbXlsx);
		bgFormat.add(rbXls);		
	}
	
	ButtonGroup getPageOrientation() {
		return bgPageOrientation;
	}
	
	public byte getBytePageOrientation() {
		ButtonModel bm = getPageOrientation().getSelection();
		if (bm != null) {
			final String sSelectedFormat = bm.getActionCommand();
			if(sSelectedFormat.equals(ReportFormatPanel.PORTRAIT)){
				return JRReport.ORIENTATION_PORTRAIT;
			}
			else {
				return JRReport.ORIENTATION_LANDSCAPE;
			}
		}
		else {
			return JRReport.ORIENTATION_PORTRAIT;
		}
	}
	
	ButtonGroup getFormatButtonGroup() {
		return bgFormat;
	}
	
	boolean isColumnScaled() {
		return cbColumnScale.isSelected();
	}
	
	private void readFormatSettingsFromPreferences(UID entityUid) {
		try {
			final Preferences prefs
				= ClientPreferences.getInstance().getUserPreferences()
					.node("collect").node("entity").node(entityUid.getStringifiedDefinitionWithEntity(E.ENTITY.getUID()));
			
			final Byte pageOrientationPrefs = (Byte) PreferencesUtils.getSerializable(prefs, ReportController.PAGE_ORIENTATION_PREFS);
			if(pageOrientationPrefs != null && pageOrientationPrefs == JRReport.ORIENTATION_LANDSCAPE) {
				rbLandscape.setSelected(true);
			}
			final Boolean columnScaledPrefs = (Boolean) PreferencesUtils.getSerializable(prefs, ReportController.COLUMN_SCALED_PREFS);
			if(columnScaledPrefs != null && columnScaledPrefs){
				cbColumnScale.setSelected(true);
			}
		} catch (Exception e) {
			LOG.warn("error getting report format settings from preferences. " + e.getMessage() + " - setting defaults.");
			rbLandscape.setSelected(true);
			cbColumnScale.setSelected(true);
		}
	}
	
	void writeFormatSettingsFromPreferences(UID entityUid) {
		try {
			final Preferences prefs
				= ClientPreferences.getInstance().getUserPreferences()
					.node("collect").node("entity").node(entityUid.getStringifiedDefinitionWithEntity(E.ENTITY.getUID()));
			
			PreferencesUtils.putSerializable(prefs,ReportController.PAGE_ORIENTATION_PREFS, getBytePageOrientation());
			PreferencesUtils.putSerializable(prefs, ReportController.COLUMN_SCALED_PREFS, isColumnScaled());
		} catch (Exception e) {
			LOG.warn("error writing report format settings from preferences. " + e.getMessage());
		}
	}

}
