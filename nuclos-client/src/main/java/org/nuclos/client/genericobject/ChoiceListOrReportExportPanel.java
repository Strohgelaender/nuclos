//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.awt.*;

import javax.swing.*;

import org.nuclos.client.ui.LineLayout;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * Panel for choice, whether a search list or the reports of selection shall be exported; for the choosen
 * selection of reports one can select the output format.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:dirk.funke@novabit.de">Dirk Funke</a>
 * @version 01.00.00
 */
public class ChoiceListOrReportExportPanel extends JPanel {

	private final JLabel lblHeadline = new JLabel();

	private final ButtonGroup bgPrechoice = new ButtonGroup();
	private final JRadioButton rbList = new JRadioButton();
	private final JRadioButton rbReport = new JRadioButton();

	private final JPanel pnlPrechoice = new JPanel();
	private final JPanel pnlPrechoiceHelp = new JPanel(new FlowLayout(FlowLayout.CENTER));
	private final JPanel pnlSelection = new JPanel(new CardLayout());
	
	private ReportSelectionPanel pnlReport;

	public ChoiceListOrReportExportPanel(UID entityUid, ReportFormatPanel pnlList) {
		setLayout(new BorderLayout());

		pnlReport = new ReportSelectionPanel(entityUid);
		
		pnlPrechoice.setLayout(new LineLayout(LineLayout.VERTICAL));
		lblHeadline.setToolTipText("");
		lblHeadline.setText(SpringLocaleDelegate.getInstance().getMessage(
				"ChoiceListOrReportExportPanel.1", "Bitte Aktion ausw\u00e4hlen:"));
		rbList.setActionCommand("List");
		rbReport.setActionCommand("Report");
		rbList.setText(SpringLocaleDelegate.getInstance().getMessage(
				"ChoiceListOrReportExportPanel.2", "Liste exportieren"));
		rbReport.setText(SpringLocaleDelegate.getInstance().getMessage(
				"ChoiceListOrReportExportPanel.3", "Formulare f\u00fcr ausgew\u00e4hlte Objekte drucken"));
		rbList.setSelected(true);
		bgPrechoice.add(rbList);
		bgPrechoice.add(rbReport);
		pnlPrechoice.add(lblHeadline);
		pnlPrechoice.add(rbList);
		pnlPrechoice.add(rbReport);

		pnlPrechoiceHelp.add(pnlPrechoice);
		pnlPrechoice.add(new JPanel());
		pnlPrechoice.add(new JSeparator());
		pnlList.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		pnlSelection.add(pnlList, "List");

		add(pnlPrechoiceHelp, BorderLayout.NORTH);
		add(pnlList, BorderLayout.CENTER);
	}

	@Override
	public Dimension getPreferredSize() {
		Dimension result = super.getPreferredSize();
		result.width = Math.max(400, result.width);
		result.height = result.height + 25;

		return result;
	}

	public void prepareSelectionPanel(UsageCriteria usagecriteria, int iObjectCount, String sSelectedFormat)
			throws NuclosBusinessException {
		try {
			pnlSelection.remove(pnlReport);
			pnlReport = ReportController.prepareReportSelectionPanel(usagecriteria, iObjectCount);
			pnlSelection.add(pnlReport, "Report");
		}
		catch (RuntimeException ex) {
			throw new NuclosBusinessException(ex);
		}
		catch (NuclosReportException ex) {
			throw new NuclosBusinessException(ex);
		}
	}
	
	ReportSelectionPanel getSelectionPanel() {
		return pnlReport;
	}
	
	JPanel getPanel() {
		return pnlSelection;
	}
	
	JRadioButton getReportButton() {
		return rbReport;
	}
	
	JRadioButton getListButton() {
		return rbList;
	}
}
