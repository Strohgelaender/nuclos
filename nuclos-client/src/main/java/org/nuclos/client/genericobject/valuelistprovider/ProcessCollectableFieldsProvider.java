//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.caching.NBCache.LookupProvider;
import org.nuclos.common.caching.TimedCache;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider to get processes by usage.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:christoph.radig@novabit.de">Christoph Radig</a>
 * @version 00.01.000
 */
public class ProcessCollectableFieldsProvider implements CollectableFieldsProvider {
	
	private static final Logger LOG = Logger.getLogger(ProcessCollectableFieldsProvider.class);

	public static final String MODULE = "module";
	public static final String MODULE_UID = "moduleId";
	public static final String ENTITY_UID = "entityUid";
	public static final String ENTITY_NAME = "entityName";
	public static final String PROCESS_UID = "process";
	public static final String RELATED_ID = "relatedId";
	public static final String SEARCHMODE = "_searchmode";
	
	//	
	public static final String NAME = "process"; // used in wysiwyg
	//
	
	private UID iModuleId;
	private boolean bSearchmode = false;
	
	/**
	 * @deprecated
	 */
	public ProcessCollectableFieldsProvider() {
	}
	
	public ProcessCollectableFieldsProvider(UID entityWithStatemodelUid) {
		setParameter(MODULE_UID, entityWithStatemodelUid);
	}

	/**
	 * valid parameters:
	 * <ul>
	 *   <li>"module" = name of module entity</li>
	 *   <li>"moduleId" = module id</li>
	 *   <li>"_searchmode" = collectable in search mask?
	 *       (now org.nuclos.common.NuclosConstants.VLP_SEARCHMODE_PARAMETER)
	 * </ul>
	 * @param sName parameter name
	 * @param oValue parameter value
	 * 
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		//LOG.debug("setParameter - sName = " + sName + " - oValue = " + oValue);

		if (sName.equals(MODULE)) {
			try {
				if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
					iModuleId = UID.parseUID((String) oValue);
				} else if (oValue instanceof UID) {
					iModuleId = oValue == null ? null : (UID) oValue;
				}
				if (iModuleId == null) {
					throw new IllegalArgumentException("oValue");
				}
			} catch (Exception ex) {
				throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
						"ProcessCollectableFieldsProvider.1", "Der Parameter \"module\" muss den Namen einer Modul-Entit\u00e4t enthalten.\n\"{0}\" ist keine g\u00fcltige Modul-Entit\u00e4t.", oValue), ex);
			}
		} else if (sName.equals(ENTITY_NAME)) {
			try {
				if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
					iModuleId = UID.parseUID((String) oValue);
				} else if (oValue instanceof UID) {
					iModuleId = oValue == null ? null : (UID) oValue;
				}
				if (iModuleId == null) {
					throw new IllegalArgumentException("oValue");
				}
			} catch (Exception ex) {
				throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
						"ProcessCollectableFieldsProvider.2", "Der Parameter \"entityName\" muss den Namen einer Entit\u00e4t enthalten.\n\"{0}\" ist keine g\u00fcltige Modul-Entit\u00e4t.", oValue), ex);
			}
		} else if (sName.equals(MODULE_UID) || sName.equals(ENTITY_UID) || sName.equals(RELATED_ID)) {
			if (oValue instanceof String && UID.isStringifiedUID((String) oValue)) {
				iModuleId = UID.parseUID((String) oValue);
			} else if (oValue instanceof UID) {
				iModuleId = oValue == null ? null : (UID) oValue;
			} else {
				iModuleId = null;
			}
		} else if (sName.equals(SEARCHMODE)) {
			bSearchmode = (Boolean) oValue;
		} else {
			// ignore
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}
	
	final private static TimedCache<ProcessCollectableFieldsProvider, List<CollectableField>> timedCache = 
			new TimedCache<ProcessCollectableFieldsProvider, List<CollectableField>>(new GetCollectableFieldsProvider(), 10);

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		if (iModuleId == null) {
			return null;
		}
		return timedCache.get(this);
	}
	
	private static class GetCollectableFieldsProvider implements LookupProvider<ProcessCollectableFieldsProvider, List<CollectableField>>{
		
		@Override
		public List<CollectableField> lookup(ProcessCollectableFieldsProvider msm) {
			final List<CollectableField> result = new ArrayList<CollectableField>();
			final UID mUid = msm.iModuleId;
			if (mUid == null) {
				LOG.info("action/process value list provider: module uid not set (many parameters)");
			}
			for (final CollectableField processField : MasterDataDelegate.getInstance().getProcessByEntity(mUid, msm.bSearchmode)) {
				if (msm.bSearchmode || SecurityCache.getInstance().isNewAllowedForModuleAndProcess(mUid, (UID) processField.getValueId())) {
					result.add(processField);
				}
			}
			Collections.sort(result);
			return result;		
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProcessCollectableFieldsProvider) {
			ProcessCollectableFieldsProvider pcfp = (ProcessCollectableFieldsProvider) obj;
			return LangUtils.equal(iModuleId, pcfp.iModuleId) && LangUtils.equal(bSearchmode, pcfp.bSearchmode);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(iModuleId) ^ LangUtils.hashCode(bSearchmode);
	}
}	// class ProcessCollectableFieldsProvider
