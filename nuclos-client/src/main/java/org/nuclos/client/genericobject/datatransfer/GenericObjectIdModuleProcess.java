//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.datatransfer;

import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.client.genericobject.CollectableGenericObjectEntity;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Predicate;

/**
 * A generic object id together with its module and process (if any).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class GenericObjectIdModuleProcess {

	private final Long goId;
	private final UID moduleUid;
	private final String sGenericObjectIdentifier;
	private final UID processUid;
	private final String sContents;

	public GenericObjectIdModuleProcess(Long goId, UID moduleUid, UID processUid, String sGenericObjectIdentifier) {
		this.goId = goId;
		this.moduleUid = moduleUid;
		this.processUid = processUid;
		this.sGenericObjectIdentifier = sGenericObjectIdentifier;
		this.sContents = sGenericObjectIdentifier;
	}

	/**
	 * @param clctlo
	 * @param sContents a String representation compatible with Excel (optional).
	 */
	public GenericObjectIdModuleProcess(CollectableGenericObject clctlo, String sGenericObjectIdentifier, String sContents) {
		this.goId = clctlo.getId();
		this.moduleUid = clctlo.getGenericObjectCVO().getModule();

		final ParameterProvider paramprovider = ClientParameterProvider.getInstance();
		final UID sFieldNameProcess = SF.PROCESS.getMetaData(this.moduleUid).getUID();
		if (CollectableGenericObjectEntity.getByModuleUid(this.moduleUid).getFieldUIDs().contains(sFieldNameProcess)) {
			this.processUid = (UID) clctlo.getField(sFieldNameProcess).getValueId();
		}
		else {
			this.processUid = null;
		}

		final UID sFieldNameIdentifier = SF.SYSTEMIDENTIFIER.getMetaData(this.moduleUid).getUID();
		this.sGenericObjectIdentifier = (sGenericObjectIdentifier == null? (String) clctlo.getValue(sFieldNameIdentifier) : sGenericObjectIdentifier);
		this.sContents = sContents;
	}

	public static class GenericObjectIdDataFlavor extends java.awt.datatransfer.DataFlavor {
		
		public static final GenericObjectIdDataFlavor FLAVOR = new GenericObjectIdDataFlavor();
		
		private GenericObjectIdDataFlavor() {
			super(GenericObjectIdModuleProcess.class, "generic object");
		}
	}

	/**
	 * @return the id of this generic object.
	 */
	public Long getGenericObjectId() {
		return goId;
	}

	/**
	 * @return the id of the module this generic object belongs to.
	 */
	public UID getModuleUid() {
		return moduleUid;
	}

	/**
	 * @return the process id (if any).
	 */
	public UID getProcessId() {
		return processUid;
	}

	/**
	 * @return the human readable identifier
	 */
	public String getGenericObjectIdentifier() {
		return this.sGenericObjectIdentifier;
	}

	/**
	 * @return a String representation compatible with Excel so we can copy &amp; paste generic objects to Excel.
	 */
	@Override
	public String toString() {
		return this.sContents;
	}

	public static class HasModuleId implements Predicate<GenericObjectIdModuleProcess> {
		
		private final UID moduleUid;

		public HasModuleId(UID moduleUid) {
			this.moduleUid = moduleUid;
		}

		@Override
		public boolean evaluate(GenericObjectIdModuleProcess goimp) {
			return goimp.getModuleUid().equals(moduleUid);
		}
	}

}	// class GenericObjectIdModuleProcess
