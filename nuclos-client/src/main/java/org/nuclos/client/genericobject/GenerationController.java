//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.client.command.CommonClientWorkerAdapter;
import org.nuclos.client.command.OvOpAdapter;
import org.nuclos.client.command.ResultListener;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.EntityCollectController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.dal.DalSupportForGO;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.entityobject.EntityObjectDelegate;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.MainFrameTabAdapter;
import org.nuclos.client.ui.MainFrameTabListener;
import org.nuclos.client.ui.OvOpListener;
import org.nuclos.client.ui.OverlayOptionPane;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.InvokeWithInputRequiredSupport;
import org.nuclos.client.ui.collect.component.EntityListOfValues;
import org.nuclos.client.ui.collect.component.ICollectableListOfValues;
import org.nuclos.client.ui.collect.component.LookupEvent;
import org.nuclos.client.ui.collect.component.LookupListener;
import org.nuclos.client.ui.multiaction.MultiActionProgressLine;
import org.nuclos.client.ui.multiaction.MultiActionProgressPanel;
import org.nuclos.client.ui.multiaction.MultiActionProgressResultHandler;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosVLPException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.PointerCollection;
import org.nuclos.common.PointerException;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.entityobject.CollectableEOEntity;
import org.nuclos.common.report.valueobject.DatasourceParameterVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.context.GenerationContext;
import org.nuclos.server.genericobject.context.GenerationContextBuilder;
import org.nuclos.server.genericobject.ejb3.GenerationResult;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public class GenerationController {

	private static final Logger LOG = Logger.getLogger(GenerationController.class);
	
	//

	private final Map<Long, UsageCriteria> sources;
	private final GeneratorActionVO action;

	private final EntityCollectController<?,?> parentController;
	private final MainFrameTab parent;

	private MainFrameTab parentForLookup;

	private final List<GenerationListener> listeners = new ArrayList<GenerationListener>();

	private boolean confirmationEnabled = true;
	private boolean headless = false;
	
	// former Spring injection
	
	private InvokeWithInputRequiredSupport invokeWithInputRequiredSupport;
	
	// end of former Spring injection

	private final MainFrameTabListener tabListener = new MainFrameTabAdapter() {
		@Override
		public void tabClosed(MainFrameTab tab) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					// parent.unlockLayer(lock);
					parentController.unLockAllLayers();
				}
			});
		}
	};

	public GenerationController(Map<Long, UsageCriteria> sources, GeneratorActionVO action, 
			EntityCollectController<?,?> parentController, MainFrameTab parent) {
		super();
		this.sources = sources;
		this.action = action;
		this.parentController = parentController;
		this.parent = parent;
		
		setInvokeWithInputRequiredSupport(SpringApplicationContextHolder.getBean(InvokeWithInputRequiredSupport.class));
	}
	
	final void setInvokeWithInputRequiredSupport(InvokeWithInputRequiredSupport invokeWithInputRequiredSupport) {
		this.invokeWithInputRequiredSupport = invokeWithInputRequiredSupport;
	}

	final InvokeWithInputRequiredSupport getInvokeWithInputRequiredSupport() {
		return invokeWithInputRequiredSupport;
	}

	public boolean isConfirmationEnabled() {
		return confirmationEnabled;
	}

	public void setConfirmationEnabled(boolean confirmationEnabled) {
		this.confirmationEnabled = confirmationEnabled;
	}

	public boolean isHeadless() {
		return headless;
	}

	public void setHeadless(boolean headless) {
		this.headless = headless;
	}

	public void addGenerationListener(GenerationListener l) {
		listeners.add(l);
	}

	public void removeGenerationListener(GenerationListener l) {
		listeners.remove(l);
	}

	public MainFrameTab getParentForLookup() {
		return parentForLookup;
	}

	public void setParentForLookup(MainFrameTab parentForLookup) {
		this.parentForLookup = parentForLookup;
	}

	/**
	 * generates one or more object(s) from current.
	 */
	public void generateGenericObject(ResultListener<?> resultListener) {
			generateGenericObject(false, resultListener);
	}
	
	public void generateGenericObject(final boolean bCloningAction, final ResultListener<?> resultListener) {
		try {
			final boolean bMulti = sources.size() > 1;
			final ValuelistProviderVO vlp;
			final Map<String, Object> params = new HashMap<String, Object>();
			if (action.getValuelistProvider() != null) {
				vlp = DatasourceDelegate.getInstance().getValuelistProvider(action.getValuelistProvider());
				List<DatasourceParameterVO> parameters = DatasourceDelegate.getInstance().getParametersFromXML(vlp.getSource());
				if (parameters != null && parameters.size() > 0) {
					if (parameters.size() == 1) {
						DatasourceParameterVO parameter = parameters.get(0);
						if (parameter.getParameter().toUpperCase().equals("INTID")) {
							if (!bMulti) {
								params.put(parameter.getParameter(), sources.keySet().iterator().next());
							}
							else {
								throw new NuclosVLPException("GenerationController.vlp.intid.singlesource");
							}
						}
						else {
							throw new NuclosVLPException("GenerationController.vlp.wrong.parameter");
						}
					}
					else {
						throw new NuclosVLPException("GenerationController.vlp.wrong.parameter");
					}
				}
			}
			else {
				vlp = null;
			}

			if (isConfirmationEnabled() && !action.isNonstop()) {
				confirmGenerationType(bMulti, action.getSourceModule(), action.getTargetModule(), action, new OvOpAdapter() {
					@Override
					public void done(int result) {
						if (result == OverlayOptionPane.OK_OPTION) {
							run(vlp, params, bCloningAction);
							if (resultListener != null) {
								resultListener.done(null); // inform generation is done.								
							}
						} else {
//							unlock(parent);
						}
					}
				});
			} else {
				run(vlp, params, bCloningAction);
				if (resultListener != null) {
					resultListener.done(null); // inform generation is done.					
				}
			}
		}
		catch (Exception ex) {
			Errors.getInstance().showExceptionDialog(parent, ex);
//			unlock(parent);
		}
	}
	
	private void run(final ValuelistProviderVO vlp, final Map<String, Object> params, final boolean bCloningAction) {
		final AtomicReference<List<Long>> parameterObjectIdRef = new AtomicReference<List<Long>>();
		final CommonRunnable generateRunnable = new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				List<Long> parameterObjectId = parameterObjectIdRef.get();
				generate(parameterObjectId, bCloningAction);
			}
		};

		if (action.getParameterEntity() != null) {
			UIUtils.runShortCommand(parent, new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					final EntityMeta<?> parameterEntity =
						MetaProvider.getInstance().getEntity(action.getParameterEntity());
				
					final MainFrameTab lookupParent = getParentForLookup() != null ? getParentForLookup() : parent;
					final ICollectableListOfValues<Long> lov = new EntityListOfValues(lookupParent);
					final CollectController<Long,?> ctl = 
							(CollectController<Long, ?>) NuclosCollectControllerFactory.getInstance().newCollectController(parameterEntity.getUID(), null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
					if (vlp != null) {
						ctl.getSearchStrategy().setValueListProviderDatasource(vlp);
						ctl.getSearchStrategy().setValueListProviderDatasourceParameter(params);
					}
					ctl.getTab().addMainFrameTabListener(tabListener);

					lov.addLookupListener(new LookupListener<Long>() {
						@Override
						public void lookupSuccessful(LookupEvent<Long> ev) {
							if (ev.getAdditionalCollectables() != null && ev.getAdditionalCollectables().size() > 0) {
								List<Long> parameterIds = CollectionUtils.transform(ev.getAdditionalCollectables(), new Transformer<Collectable<Long>, Long>() {
									@Override
									public Long transform(Collectable<Long> i) {
										return i.getPrimaryKey();
									}
								});
								parameterIds.add(ev.getSelectedCollectable().getId());
								parameterObjectIdRef.set(parameterIds);
							}
							else {
								Collectable<Long> clct = ev.getSelectedCollectable();
								if (clct != null) {
									parameterObjectIdRef.set(Collections.singletonList(clct.getId()));
								}
							}
							UIUtils.runShortCommand(parent, generateRunnable);
						}
						@Override
						public int getPriority() {
							return 1;
						}
					});
					ctl.runLookupCollectable(lov);
				}
			});
		}
		else {
			UIUtils.runShortCommand(parent, generateRunnable);
		}
	}

	/*
	private void unlock(final MainFrameTab tab) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tab.unlockLayer();
			}
		});
	}
	 */

	public static String getModuleLabel(UID uid) {
		EntityMeta meta = MetaProvider.getInstance().getEntity(uid);
		return SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(meta);
	}

	/**
	 * Ask the user for confirmation of object generation.
	 * @param bMulti
	 * @param sSourceModuleName
	 * @param sTargetModuleName
	 * @param generatoractionvo
	 * @return selected option
	 */
	private void confirmGenerationType(boolean bMulti, UID sourceModule, UID targetModule, GeneratorActionVO generatoractionvo, OvOpListener listener) {
		// use labels not the internal names of the entities. @see NUCLOS-2554
		String sTargetModuleName = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(targetModule));
		String sSourceModuleName = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(sourceModule));
		
		if (generatoractionvo.getTargetProcess() != null) {
			sTargetModuleName = MessageFormat.format("{0} ({1})", sTargetModuleName, 
					MasterDataCache.getInstance().get(E.PROCESS.getUID(), 
							generatoractionvo.getTargetProcess()).getFieldValue(E.PROCESS.name));
		}
		if (bMulti) {
			String message;
			if(generatoractionvo.isGroupAttributes()) {
				message = SpringLocaleDelegate.getInstance().getMessage(
						"GenericObjectCollectController.71a","Create one or more grouped objects of type \"{1}\" from the selected objects of type \"{0}\"?", sSourceModuleName, sTargetModuleName);
			}
			else {
				message = SpringLocaleDelegate.getInstance().getMessage(
						"GenericObjectCollectController.72","Soll aus den markierten Objekten vom Typ \"{0}\" jeweils ein Objekt vom Typ \"{1}\" erzeugt werden?", sSourceModuleName, sTargetModuleName);
			}
			OverlayOptionPane.showConfirmDialog(parent, message, 
					SpringLocaleDelegate.getInstance().getMessage(
							"GenericObjectCollectController.5","{0} erzeugen", sTargetModuleName), 
							JOptionPane.OK_CANCEL_OPTION, 
							generatoractionvo.getButtonIcon()==null
								? null
								: ResourceCache.getInstance().getIconResource(generatoractionvo.getButtonIcon().getId()), 
							true, listener);
		}
		else {
			UsageCriteria uc = sources.values().iterator().next();
			if (uc != null && uc.getProcessUID() != null) {
				UID processUid = sources.values().iterator().next().getProcessUID();
				String process;
				try {
					process = MasterDataDelegate.getInstance().get(E.PROCESS.getUID(), processUid).getFieldValue(E.PROCESS.name);
					sSourceModuleName = MessageFormat.format("{0} ({1})", sSourceModuleName, process);
				} catch (CommonBusinessException e) {
					LOG.error(e);
				}
			}
			final String sMessage = SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectCollectController.71","Soll aus dem/der aktuellen {0} ein(e) {1} erzeugt werden?", sSourceModuleName, sTargetModuleName);
			OverlayOptionPane.showConfirmDialog(parent, sMessage, 
					SpringLocaleDelegate.getInstance().getMessage("GenericObjectCollectController.5","{0} erzeugen", sTargetModuleName), 
					JOptionPane.OK_CANCEL_OPTION, 
					generatoractionvo.getButtonIcon()==null
						? null
						: ResourceCache.getInstance().getIconResource(generatoractionvo.getButtonIcon().getId()), 
					true, listener);
		}
		
	}

	/**
	 * Execute object generation once for every single selected leased object.
	 * Performed in an own thread.
	 */
	public void generate(final List<Long> parameterObjectIds, final boolean bCloningAction) throws CommonBusinessException {
		if (parentController != null) {
			CommonMultiThreader.getInstance().execute(new GenerationClientWorker(parentController, parameterObjectIds, bCloningAction));
		}
		else  {
			generateImpl(parameterObjectIds, bCloningAction);
		}
	}

	private void generateImpl(final List<Long> parameterObjectIds, final boolean bCloningAction) throws CommonBusinessException {
		final Collection<Collection<EntityObjectVO<Long>>> sources;

		if (action.isGroupAttributes()) {
			sources = GeneratorDelegate.getInstance().groupObjects(this.sources.keySet(), action).values();
		}
		else {
			sources = new ArrayList<Collection<EntityObjectVO<Long>>>();
			for (Long uid : this.sources.keySet()) {
				Set<EntityObjectVO<Long>> singleton = Collections.singleton(EntityObjectDelegate.getInstance().get(action.getSourceModule(), uid));
				sources.add(singleton);
			}
		}

		final Collection<Pair<Collection<EntityObjectVO<Long>>, Long>> sourceWithParameters = new ArrayList<Pair<Collection<EntityObjectVO<Long>>,Long>>();

		for (Collection<EntityObjectVO<Long>> sourceGroup : sources) {
			if (parameterObjectIds != null && parameterObjectIds.size() > 0) {
				for (Long parameterObjectId : parameterObjectIds) {
					sourceWithParameters.add(new Pair<Collection<EntityObjectVO<Long>>, Long>(sourceGroup, parameterObjectId));
				}
			}
			else {
				sourceWithParameters.add(new Pair<Collection<EntityObjectVO<Long>>, Long>(sourceGroup, null));
			}
		}

		if (sourceWithParameters.size() == 1 || isHeadless()) {
			UIUtils.runCommandLater(parent, new Runnable() {

				@Override
				public void run() {
					final Map<String, Serializable> applyMultiEditContext = new HashMap<String, Serializable>();
					for (final Pair<Collection<EntityObjectVO<Long>>, Long> pair : sourceWithParameters) {
						final HashMap<String, Serializable> context = new HashMap<String, Serializable>();
						final AtomicReference<GenerationResult> result = new AtomicReference<GenerationResult>();
						try {

							final GenerationContext<Long> generationContext = new GenerationContextBuilder<>(pair.x, action)
									.parameterObjectId(pair.y)
									.customUsage(ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY))
									.cloning(bCloningAction)
									.build();

							getInvokeWithInputRequiredSupport().invoke(new CommonRunnable() {
								@Override
								public void run() throws CommonBusinessException {
									result.set(GeneratorDelegate.getInstance().generateGenericObject(generationContext));
								}
							}, context, applyMultiEditContext, parent);
							fireGenerationSucessfulEvent(result.get());
							if (!isHeadless() && !bCloningAction) {
								showGenerationSucessfulResult(result.get());
								try {
									// Refreshing the details view takes a lot of time if there are many comboboxes in the layout, see NUCLOS-4467
									if (parentController.getCollectState().getOuterState() == CollectState.OUTERSTATE_DETAILS)
										parentController.refreshCurrentCollectable();
									if (parentController.getCollectState().getOuterState() == CollectState.OUTERSTATE_RESULT) {
										parentController.refreshCurrentCollectableInResult();										
									}
								}
								catch (CommonBusinessException e2) {
									Errors.getInstance().showExceptionDialog(parent, e2);
								}
							}
						}
						catch (GeneratorFailedException e) {
							fireGenerationWithExceptionEvent(e);
							if (!isHeadless()) {
								if (!action.isCloseOnException()) {
									showGenerationWithExceptionResult(e);
									if (action.isRefreshSrcObject()) {
										try {
											if (parentController.getCollectState().getOuterState() == CollectState.OUTERSTATE_DETAILS)
												parentController.refreshCurrentCollectable();
											if (parentController.getCollectState().getOuterState() == CollectState.OUTERSTATE_RESULT)
												parentController.refreshCurrentCollectableInResult();
										} catch (CommonBusinessException e2) {
											Errors.getInstance().showExceptionDialog(parent, e2);
										}
									}
								} else {
									selectTabAndShowError(parentController, e.getGenerationResult().getError(), e.getCause());
								}
							}
						}
						catch (CommonBusinessException e) {
							Errors.getInstance().showExceptionDialog(parent, 
									"ATTENTION: Displayed generated objected might be incomplete!", e);
						}
					}
				}
			});
		}
		else {
			MultiActionProgressPanel panel = new MultiActionProgressPanel(sourceWithParameters.size());
			panel.setResultHandler(new MultiActionProgressResultHandler(null) {
				@Override
				public void handleMultiSelection(Collection<MultiActionProgressLine> selection) {
					try {
						for (MultiActionProgressLine o : selection) {
							if (o.getResultObject() instanceof GenerationResult) {
								GenerationResult result = (GenerationResult) o.getResultObject();
								if (((EntityObjectVO)result.getGeneratedObject()).getId() != null) {
									showGenericObject(result.getGeneratedObject(), action.getTargetModule());
								}
								else {
									// dead code
									assert false;
									showIncompleteGenericObject(null, (EntityObjectVO)result.getGeneratedObject(), action, result.getError(), null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
								}
							}
						}	
					} 
					catch (GeneratorFailedException e) {
						final GenerationResult result = e.getGenerationResult();
						try {
							showIncompleteGenericObject(null, (EntityObjectVO)result.getGeneratedObject(), action, result.getError(), null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
						}
						catch (CommonBusinessException e2) {
							Errors.getInstance().showExceptionDialog(parent, e2);
						}
					}
					catch (CommonBusinessException e) {
						Errors.getInstance().showExceptionDialog(parent, e);
					}
				}
			});
			MultiGenerateAction multiGenerateAction = new MultiGenerateAction(parent, action) {
				public void executeFinalAction() throws CommonBusinessException {
					//@todo add open objects if action.isShowObject()
					if (action.isRefreshSrcObject()) {
						try {
							if (parentController.getCollectState().getOuterState() == CollectState.OUTERSTATE_RESULT)
								parentController.refreshSelectedCollectablesInResult();
						}
						catch (CommonBusinessException e2) {
							Errors.getInstance().showExceptionDialog(parent, e2);
						}
					}
				}
			};
			new MultiCollectablesActionController<Long, Pair<Collection<EntityObjectVO<Long>>, Long>, GenerationResult>(
				parent, sourceWithParameters, 
				SpringLocaleDelegate.getInstance().getMessage("R00022892", "Objektgenerierung"), parent.getTabIcon(),
				multiGenerateAction).run(panel);
		}
	}

	private void fireGenerationSucessfulEvent(GenerationResult result) {
		for (GenerationListener l : listeners) {
			l.generatedSucessful(result);
		}
	}

	private void fireGenerationWithExceptionEvent(GeneratorFailedException result) {
		for (GenerationListener l : listeners) {
			l.generatedWithException(result);
		}
	}

	private void showGenerationSucessfulResult(GenerationResult result) {
		try {
			Long generatedGoId = (Long)result.getGeneratedObject().getPrimaryKey();
			EntityMeta<Long> meta = (EntityMeta<Long>) MetaProvider.getInstance().getEntity(action.getTargetModule());
			if ((meta.isStateModel() && SecurityCache.getInstance().isWriteAllowedForModule(action.getTargetModule(),generatedGoId))
					|| (!meta.isStateModel() && SecurityCache.getInstance().isWriteAllowedForMasterData(meta.getUID())) ) {
				
				if (generatedGoId != null) {
					if (action.isShowObject()) {
						showGenericObject(result.getGeneratedObject(), action.getTargetModule());						
					}
				}
				else {
					// dead code
					assert false;
					showIncompleteGenericObject(result.getSourceIds(), (EntityObjectVO)result.getGeneratedObject(), action, result.getError(), null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
				}
			}
		}
		catch (CommonBusinessException ex) {
			Errors.getInstance().showExceptionDialog(parent, ex);
		}
	}

	private void showGenerationWithExceptionResult(GeneratorFailedException ex) {
		final GenerationResult result = ex.getGenerationResult();
		try {
			final EntityMeta<?> meta = MetaProvider.getInstance().getEntity(action.getTargetModule());
			final SecurityCache sc = SecurityCache.getInstance();
			final boolean hasState = meta.isStateModel();
			final boolean newAllowedForModule = sc.isNewAllowedForModule(meta.getUID());
			final boolean writeAllowedForMd = sc.isWriteAllowedForMasterData(meta.getUID());

			// NUCLOS-1772
			showIncompleteGenericObject(result.getSourceIds(), (EntityObjectVO)result.getGeneratedObject(), 
					action, result.getError(), ex.getCause(), ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			LOG.warn("Generation of " + meta + " is incomplete: hasState=" + hasState 
					+ " newAllowedForModule=" + newAllowedForModule + " writeAllowedForMd=" + writeAllowedForMd);
		}
		catch (CommonBusinessException e) {
			Errors.getInstance().showExceptionDialog(parent, e);
		}
	}

	/**
	 * Open the generated leased object in an own controller, if possible.
	 *
	 * @throws CommonBusinessException
	 */
	private void showGenericObject(EntityObjectVO<Long> result, final UID moduleUid) throws CommonBusinessException {
		if (result.getFieldUid(SF.PROCESS_UID) != null ) {
			Main.getInstance().getMainController().showDetailsWithProcess(action.getTargetModule(), result.getPrimaryKey(), result.getFieldUid(SF.PROCESS_UID));
		} else {
			Main.getInstance().getMainController().showDetails(action.getTargetModule(), result.getPrimaryKey());
		}
	}
	
	private void selectTabAndShowError(final EntityCollectController clct, final String message, final Throwable cause) {
		MainFrame.setSelectedTab(clct.getTab());

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				showError(clct, message, cause);
			}
		});
	}
	
	private void showError(EntityCollectController clct, final String message, Throwable cause) {
		if (!handleCommonValidationException(clct, cause)) {
			PointerException pointerException = null;
			if (cause instanceof Exception) {
				pointerException = PointerException.extractPointerExceptionIfAny((Exception) cause);
			}
			
			if (pointerException != null) {
				clct.setPointerInformation(pointerException.getPointerCollection(), null);
				
			} else {
				NuclosBusinessRuleException nbre = null;
				if (cause instanceof BusinessException) {
					nbre = new NuclosBusinessRuleException((BusinessException)cause);
				}
				
				clct.setPointerInformation(new PointerCollection(message), nbre);
				
			}
		}
	}

	/**
	 * Open an incomplete generated object in its own controller.
	 * @param cause 
	 * @throws CommonBusinessException
	 */
	private void showIncompleteGenericObject(Collection<Long> sourceIds, EntityObjectVO<Long> result, GeneratorActionVO action, final String message, final Throwable cause, String customUsage) throws CommonBusinessException {
	
		final UID entity = result.getDalEntity();
		
		final EntityMeta<Long> metaVO = (EntityMeta<Long>) MetaProvider.getInstance().getEntity(entity);
		
		UID mandatorUID = null;
		
		if (metaVO.isMandator()) {
			mandatorUID = result.getFieldUid(SF.MANDATOR_UID);
			if (mandatorUID == null) {
				throw new CommonValidationException(SpringLocaleDelegate.getInstance().getMsg("mandator.not.set"));
			}
		}
		
		final Map<UID, FieldMeta<?>> allEntityFieldsByEntity = MetaProvider.getInstance().getAllEntityFieldsByEntity(entity);

		if (metaVO.isStateModel()) {
			final GenericObjectCollectController goclct = 
					NuclosCollectControllerFactory.getInstance().newGenericObjectCollectController(metaVO.getUID(), null, customUsage);
			goclct.setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_NEW_CHANGED);
			goclct.setGenerationSource(sourceIds, action);
			if (metaVO.isMandator()) {
				goclct.setMandator(new CollectableValueIdField(mandatorUID, SecurityCache.getInstance().getMandatorById(mandatorUID).getName()));
			}
			
			CollectableEOEntity meta = new CollectableEOEntity(metaVO);
			
			if (result.getFieldUid(SF.STATE_UID) == null) {
				UsageCriteria usagecriteria = 
						new UsageCriteria(metaVO.getUID(), result.getFieldUid(SF.PROCESS_UID), result.getFieldUid(SF.STATE_UID),customUsage);
				result.setFieldUid(SF.STATE_UID.getUID(metaVO.getUID()), 
						StateDelegate.getInstance().getStatemodel(usagecriteria).getInitialState());
			}
			
			CollectableGenericObjectWithDependants clct = new CollectableGenericObjectWithDependants(DalSupportForGO.getGenericObjectWithDependantsVO(result, meta));
			goclct.unsafeFillDetailsPanel(clct, true);
			if (metaVO.isMandator()) {
				CollectableField mandator = clct.getField(SF.MANDATOR.getUID(entity));
				goclct.setMandator(mandator);
			}
			goclct.selectTab();
			
			selectTabAndShowError(goclct, message, cause);
			
		}
		else {
			final MasterDataCollectController mdclct = 
					NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(metaVO.getUID(), null, ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			if (metaVO.isMandator()) {
				mdclct.setMandator(new CollectableValueIdField(mandatorUID, SecurityCache.getInstance().getMandatorById(mandatorUID).getName()));
			}
			final CollectableEOEntity meta = new CollectableEOEntity(metaVO);
			final CollectableMasterDataWithDependants clctmdwd = new CollectableMasterDataWithDependants(meta, new MasterDataVO<Long>(result));
			final IDependentDataMap deps = result.getDependents();
			clctmdwd.setDependantMasterDataMap(deps);
			mdclct.runNewWith(clctmdwd);
			if (metaVO.isMandator()) {
				CollectableField mandator = clctmdwd.getField(SF.MANDATOR.getUID(entity));
				mdclct.setMandator(mandator);
			}
			mdclct.setCollectState(CollectState.OUTERSTATE_DETAILS, CollectState.DETAILSMODE_NEW_CHANGED);
			
			selectTabAndShowError(mdclct, message, cause);
			
		}
	}
	
	private boolean handleCommonValidationException(EntityCollectController<?, ?> clctctr, Throwable ex) {
		CommonValidationException cve = NuclosExceptions.getCause(ex, CommonValidationException.class);
		if (cve != null) {
			List<String> messages = new ArrayList<String>();
			messages.add(SpringLocaleDelegate.getInstance().getText("common.exception.novabitvalidationexception"));

			if (cve.getErrors() != null) {
				for (String error : cve.getErrors()) {
					messages.add(SpringLocaleDelegate.getInstance().getMessageFromResource(error));
				}
			}

			final PointerCollection pointerCollection = new PointerCollection("");
			if (cve.getFieldErrors() != null) {
				for (FieldValidationError error : cve.getFieldErrors()) {
					if (!clctctr.getEntityUid().equals(error.getEntity())) {
						messages.add(SpringLocaleDelegate.getInstance().getMessage("EntityCollectController.Subform", "Subform \"{0}\": ",
								SpringLocaleDelegate.getInstance().getResource(
										MetaProvider.getInstance().getEntity(error.getEntity()).getLocaleResourceIdForLabel(), 
										MetaProvider.getInstance().getEntity(error.getEntity()).getEntityName()))
								+ SpringLocaleDelegate.getInstance().getMessageFromResource(error.getMessage()));
					}
					else {
						pointerCollection.addEmptyFieldPointer(error.getField());
						messages.add(SpringLocaleDelegate.getInstance().getMessageFromResource(error.getMessage()));
					}
				}
			}

			if (messages.size() == 1) {
				messages.add(SpringLocaleDelegate.getInstance().getText(cve.getMessage()));
			}

			String message = StringUtils.concatHtml(true, messages.toArray(new String[messages.size()]));
			pointerCollection.setMainPointer(message);
			
			clctctr.setPointerInformation(pointerCollection, null);
			return true;
		}
		else {
			return false;
		}
	}

	private class GenerationClientWorker<PK,T extends Collectable<PK>> extends CommonClientWorkerAdapter<PK,T> {

		private final boolean bCloningAction;
		private final List<Long> parameterObjectIds;

		public GenerationClientWorker(CollectController<PK,T> ctl, List<Long> parameterObjectIds, final boolean bCloningAction) {
			super(ctl);
			this.bCloningAction = bCloningAction;
			this.parameterObjectIds = parameterObjectIds;
		}

		@Override
		public void init() throws CommonBusinessException {
			super.init();
		}

		@Override
		public void work() throws CommonBusinessException {
			generateImpl(parameterObjectIds, bCloningAction);
		}
	}

	public interface GenerationListener {
		
		void generatedSucessful(GenerationResult result);
		
		void generatedWithException(GeneratorFailedException result);
		
	}
}
