package org.nuclos.client.entityobject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.dal.vo.EntityObjectVO;

@SuppressWarnings("serial")
public class CollectableEntityTemplate<PK> extends CollectableEntityObject<PK> {
	
	private Set<CollectableEntityObject<PK>> slaves = new HashSet<CollectableEntityObject<PK>>();

	public CollectableEntityTemplate(CollectableEntity ce, EntityObjectVO<PK> vo) {
		super(ce, vo);
	}
	
	public void addSlave(CollectableEntityObject<PK> ceo) {
		slaves.add(ceo);
	}
	
	@Override
	public void setField(UID fieldUid, CollectableField clctfValue) {
		Iterator<CollectableEntityObject<PK>> it = slaves.iterator();
		while (it.hasNext()) it.next().setField(fieldUid, clctfValue);
		super.setField(fieldUid, clctfValue);
	}
	
	@Override
	public void markRemoved() {
		Iterator<CollectableEntityObject<PK>> it = slaves.iterator();
		while (it.hasNext()) it.next().markRemoved();
		super.markRemoved();
	}
}
