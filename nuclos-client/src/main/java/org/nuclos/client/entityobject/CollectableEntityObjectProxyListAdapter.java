//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.entityobject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.swing.event.ChangeListener;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Logger;
import org.nuclos.client.ui.collect.CollectableProxyListAdapter;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.server.genericobject.ProxyList;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * Makes a <code>List&lt;EntityObjectVO&gt;</code> look like a <code>List&lt;CollectableEntityObject&gt;</code>.
 * <p>
 * This is needed for lazy loading of CollectableEntityObjects on the client side.
 * </p><p>
 * Created by Novabit Informationssysteme GmbH
 * </p><p>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
public class CollectableEntityObjectProxyListAdapter<PK> extends
		CollectableProxyListAdapter<PK,EntityObjectVO<PK>, CollectableEntityObject<PK>> {
	
	private static final Logger LOG = Logger.getLogger(CollectableEntityObjectProxyListAdapter.class);
	
	private final CollectableEntity meta;

	/**
	 * Caches results of calls to {@link #makeCollectable}
	 */
	private final Cache<MultiKey, CollectableEntityObject<PK>> cache = CacheBuilder.newBuilder()
			.expireAfterAccess(5, TimeUnit.MINUTES)
			.maximumSize(1000)
			.build();

	public CollectableEntityObjectProxyListAdapter(ProxyList<PK,EntityObjectVO<PK>> list, CollectableEntity meta) {
		super(list);
		this.meta = meta;
	}

	@Override
	protected ProxyList<PK,EntityObjectVO<PK>> adaptee() {
		return (ProxyList<PK,EntityObjectVO<PK>>) super.adaptee();
	}

	@Override
	protected CollectableEntityObject<PK> makeCollectable(EntityObjectVO<PK> vo) {
		if (vo == null) {
			LOG.warn("makeCollectable: EntityObjectVO is null");
			return null;
		}

		MultiKey cacheKey = new MultiKey(
				vo.getPrimaryKey().hashCode(),
				vo.getFieldValues().hashCode()
		);
		try {
			return cache.get(
					cacheKey,
					() -> {
						final CollectableEntityObject<PK> result = new CollectableEntityObject<>(meta, vo);
						final IDependentDataMap deps = vo.getDependents();
						result.setDependantMasterDataMap(deps);

						return result;
					}
			);
		} catch (ExecutionException e) {
			LOG.error("makeCollectable failed", e);
		}

		return null;
	}

	@Override
	protected EntityObjectVO<PK> extractAdaptee(CollectableEntityObject<PK> clct) {
		final EntityObjectVO<PK> result = clct.getEntityObjectVO();
		// TODO: what todo with the dependants stuff???
		return clct.getEntityObjectVO();
	}

	@Override
	public void fetchDataIfNecessary(int iIndex, ChangeListener changelistener) {
		adaptee().fetchDataIfNecessary(iIndex, changelistener);
	}

	@Override
	public int getLastIndexRead() {
		return adaptee().getLastIndexRead();
	}

	@Override
	public boolean hasObjectBeenReadForIndex(int index) {
		return this.adaptee().hasObjectBeenReadForIndex(index);
	}

	@Override
	public int getIndexById(Object oId) {
		return this.adaptee().getIndexById((PK) oId);
	}
	
	@Override
	public boolean isOnlySequentialPaging() {
		return this.adaptee().isOnlySequentialPaging();
	}

}
