//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.dal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.CalcAttributeUtils;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;

/**
 * Client-side functions to transform a GenericObjectVO into a EntityObjectVO and vice versa.
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
public class DalSupportForGO {
	
	private static final Logger LOG = Logger.getLogger(DalSupportForGO.class);

	private DalSupportForGO() {
		// Never invoked.
	}

	public static GenericObjectWithDependantsVO getGenericObjectWithDependantsVO(EntityObjectVO<Long> eo, CollectableEntity meta) throws CommonValidationException {
		final UID entityUid = meta.getMeta().getUID();
		final GenericObjectVO base = getGenericObjectVO(eo, meta);
		
		final GenericObjectWithDependantsVO result
			= new GenericObjectWithDependantsVO(base, eo.getDependents(), eo.getDataLanguageMap());
		return result;
	}
	
	public static GenericObjectVO getGenericObjectVO(EntityObjectVO<Long> eo, CollectableEntity meta) throws CommonValidationException {
		final UID entityUid = meta.getMeta().getUID();
		final NuclosValueObject<Long> nvo;
		if (eo.getPrimaryKey() != null) {
			nvo = new NuclosValueObject<Long>(eo.getPrimaryKey(), eo.getCreatedAt(), eo.getCreatedBy(),
					eo.getChangedAt(), eo.getChangedBy(), eo.getVersion());
		} else {
			nvo = new NuclosValueObject<Long>();
		}

		final GenericObjectVO go = new GenericObjectVO(nvo, entityUid, (Boolean) eo.getFieldValue(SF.LOGICALDELETED));

		go.setComplete(eo.isComplete());
		Collection<DynamicAttributeVO> attrVOList = new ArrayList<DynamicAttributeVO>();

		Set<UID> setFields = new HashSet<UID>();
		setFields.addAll(eo.getFieldValues().keySet());
		setFields.addAll(eo.getFieldIds().keySet());
		setFields.addAll(eo.getFieldUids().keySet());

		final MetaProvider mdcp = MetaProvider.getInstance();
		for (UID field : setFields) {
			if (mdcp.getAllEntityFieldsByEntity(eo.getDalEntity()).containsKey(field)) {
				DynamicAttributeVO attr = new DynamicAttributeVO(field, eo.getFieldIds().get(field), eo.getFieldUids().get(field), eo.getFieldValues().get(field));
				attrVOList.add(attr);
			} else {
				if (CalcAttributeUtils.isCalcAttributeCustomization(field)) {
					attrVOList.add(new DynamicAttributeVO(field, eo.getFieldId(field), eo.getFieldUid(field), eo.getFieldValue(field)));
				}
			}
		}

		attrVOList.add(new DynamicAttributeVO(
				SF.CREATEDAT.getMetaData(entityUid).getUID(), null, null, eo.getCreatedAt()));
		attrVOList.add(new DynamicAttributeVO(
				SF.CREATEDBY.getMetaData(entityUid).getUID(), null, null, eo.getCreatedBy()));
		attrVOList.add(new DynamicAttributeVO(
				SF.CHANGEDAT.getMetaData(entityUid).getUID(), null, null, eo.getChangedAt()));
		attrVOList.add(new DynamicAttributeVO(
				SF.CHANGEDBY.getMetaData(entityUid).getUID(), null, null, eo.getChangedBy()));
		
		go.setCanWrite(eo.getCanWrite());
		go.setCanStateChange(eo.getCanStateChange());
		go.setCanDelete(eo.getCanDelete());
		
		go.setAttributes(attrVOList);
		
		go.setDataLanguageMap(eo.getDataLanguageMap());
		
		return go;
	}
	
	public static EntityObjectVO<Long> wrapGenericObjectVO(GenericObjectVO go) {
		final UID entityUid = go.getModule();
		final EntityObjectVO<Long> eo = new EntityObjectVO<>(entityUid);
		// eo.setEntity(metaVo.getEntity());
		eo.setPrimaryKey(go.getId());
		eo.setCreatedBy(go.getCreatedBy());
		eo.setCreatedAt(InternalTimestamp.toInternalTimestamp(go.getCreatedAt()));
		eo.setChangedBy(go.getChangedBy());
		eo.setChangedAt(InternalTimestamp.toInternalTimestamp(go.getChangedAt()));
		eo.setVersion(go.getVersion());

		// eo.initFields(go.getAttributes().size(), go.getAttributes().size());
		for (DynamicAttributeVO attr : go.getAttributes()) {
			final UID fieldUid = attr.getAttributeUID();
			if (LOG.isDebugEnabled()) {
				LOG.debug("fieldId " + fieldUid + " for " + attr + " (attrId=" 
						+ attr.getAttributeUID() + ") in " + entityUid);
			}
			if (attr.isRemoved()) {
				eo.removeFieldValue(fieldUid);
				eo.removeFieldId(fieldUid);
				eo.removeFieldUid(fieldUid);
			} else {
				eo.setFieldValue(fieldUid, attr.getValue());
				if (attr.getValueId() != null) {
					eo.setFieldId(fieldUid, attr.getValueId());
				}
				if (attr.getValueUid() != null) {
					eo.setFieldUid(fieldUid, attr.getValueUid());
				}
			}
		}
		eo.setFieldValue(SF.LOGICALDELETED, go.isDeleted());
		
		eo.setCanWrite(go.getCanWrite());
		eo.setCanStateChange(go.getCanStateChange());
		eo.setCanDelete(go.getCanDelete());
				
		if (go instanceof GenericObjectWithDependantsVO) {
			final GenericObjectWithDependantsVO gowd = (GenericObjectWithDependantsVO) go;
			eo.setDependents(gowd.getDependents());
		}
		
		eo.setDataLanguageMap(go.getDataLanguageMap());
		
		return eo;
	}
	
}
