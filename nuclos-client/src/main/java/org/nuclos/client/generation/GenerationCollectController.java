//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.generation;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.genericobject.GeneratorActions;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectPanel;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.Utils;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;


/**
 * <code>MasterDataCollectController</code> for entity "generation".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class GenerationCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(GenerationCollectController.class);

	private GenerationRulesController generationRulesController;

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public GenerationCollectController(MainFrameTab tabIfAny) {
		super(E.GENERATION, tabIfAny, null);
	}

	@Override
	protected void initialize(CollectPanel<UID,CollectableMasterDataWithDependants<UID>> pnlCollect) {
		super.initialize(pnlCollect);
		getDetailsComponentModel(E.GENERATION.sourceModule.getUID()).addCollectableComponentModelListener(
				GenerationCollectController.this,
				new CollectableComponentModelAdapter() {
					
			@Override
			public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
				UID newValue = (UID) ev.getNewValue().getValueId();
				boolean enabled = false;
				if (newValue != null && MetaProvider.getInstance().getEntity(newValue).isStateModel()) {
					enabled = true;
				}
				DetailsSubFormController<UID,CollectableEntityObject<UID>> sfctl = getDetailsSubforms().get(E.GENERATIONUSAGE.getUID());
				if (!enabled) {
					for (CollectableEntityObject<UID> clct : new ArrayList<CollectableEntityObject<UID>>(sfctl.getCollectables())) {
						sfctl.getCollectableTableModel().remove(clct);
					}
				}
				sfctl.getSubForm().setEnabled(enabled);
			}
		});
		// todo: Add listener to target module combo box to enable/disable details check box
		//getSelectedCollectable().getField("targetModule").   //?!
	}

	@Override
	public void setupEditPanelForDetailsTab() {
		super.setupEditPanelForDetailsTab();

		GenerationRulePanel grp = new GenerationRulePanel(this);
		generationRulesController = new GenerationRulesController(grp, getTab(), this);

		final JPanel pnlRules = (JPanel) UIUtils.findJComponent(this.getDetailsPanel(), "pnlRules");
		if (pnlRules != null) {
			pnlRules.removeAll();
			pnlRules.setLayout(new BorderLayout());
			pnlRules.add(grp, BorderLayout.CENTER);
		}
	}

	@Override
	protected void unsafeFillDetailsPanel(CollectableMasterDataWithDependants<UID> clct) throws NuclosBusinessException {
		super.unsafeFillDetailsPanel(clct);
		
		try {
			final Collection<EventSupportGenerationVO> ruleUsages
				= EventSupportRepository.getInstance().getEventSupportsByGenerationUid(clct.getPrimaryKey());
			if (ruleUsages != null)
				generationRulesController.setRuleUsages(ruleUsages);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this.getDetailsPanel(),e.getMessage(), 
					SpringLocaleDelegate.getInstance().getText("CodeCompilerError.title"), JOptionPane.WARNING_MESSAGE);
		}
	}
	
	@Override
	protected CollectableMasterDataWithDependants<UID> newCollectableWithDefaultValues(boolean forInsert) {
		final CollectableMasterDataWithDependants<UID> result = super.newCollectableWithDefaultValues(forInsert);
		result.setField(E.GENERATION.showobject.getUID(), new CollectableValueField(Boolean.TRUE));
		return result;
	}

	/**
	 * invalidates the generator actions cache after successful insert.
	 * @param clctNew
	 * @return
	 * @throws NuclosBusinessException
	 */
	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		final IDependentDataMap mpmdvoDependants = Utils.clearIds(getAllSubFormData(null).toDependentDataMap());
		Boolean grouping = LangUtils.defaultIfNull((Boolean)clctNew.getField(E.GENERATION.groupattributes.getUID()).getValue(), Boolean.FALSE);
		validateAttributes(grouping, mpmdvoDependants.<UID>getDataPk(E.GENERATIONATTRIBUTE.generation));
		validateSubformAttributes(mpmdvoDependants.<UID>getDataPk(E.GENERATIONSUBENTITY.generation));

		final CollectableMasterDataWithDependants<UID> result = super.insertCollectable(clctNew);
		List<EventSupportGenerationVO> ruleUsages = generationRulesController.getRuleUsages();
		for (EventSupportGenerationVO esgVO : ruleUsages) {
			EventSupportDelegate.getInstance().modifyEventSupportGeneration(esgVO);
		}

		GeneratorActions.invalidateCache();
		
		EventSupportDelegate.getInstance().invalidateCaches(E.GENERATION);
		MasterDataCache.getInstance().invalidate(E.GENERATION.getUID());

		return result;
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oDependantData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final IDependentDataMap mpDependantMasterData = getAllSubFormData(clct.getId()).toDependentDataMap();
		Boolean grouping = LangUtils.defaultIfNull((Boolean)clct.getField(E.GENERATION.groupattributes.getUID()).getValue(), Boolean.FALSE);
		validateAttributes(grouping, mpDependantMasterData.<UID>getDataPk(E.GENERATIONATTRIBUTE.generation));
		validateSubformAttributes(mpDependantMasterData.<UID>getDataPk(E.GENERATIONSUBENTITY.generation));

		final CollectableMasterDataWithDependants<UID> result = super.updateCollectable(clct, oDependantData, applyMultiEditContext);
		// remove all attached rules
		for (EventSupportGenerationVO esgVO : EventSupportRepository.getInstance().getEventSupportsByGenerationUid(clct.getId())){
			EventSupportDelegate.getInstance().deleteEventSupportGeneration(esgVO, false);
		}
		// reattach all new rules
		List<EventSupportGenerationVO> ruleUsages = generationRulesController.getRuleUsages();
		for (EventSupportGenerationVO esgVO : ruleUsages) {
			EventSupportDelegate.getInstance().createEventSupportGeneration(esgVO);
		}
		
		EventSupportRepository.getInstance().updateEventSupports();
		GeneratorActions.invalidateCache();
		EventSupportDelegate.getInstance().invalidateCaches(E.GENERATION);
		MasterDataCache.getInstance().invalidate(E.GENERATION.getUID());

		return result;
	}
	
	private void validateSubformAttributes(Collection<EntityObjectVO<UID>> coll) throws CommonValidationException {
		for (EntityObjectVO<UID> vo : coll) {
			if (vo.getFieldValue(E.GENERATIONSUBENTITY.groupAttributes.getUID()) != null
					&& (Boolean) vo.getFieldValue(E.GENERATIONSUBENTITY.groupAttributes.getUID()) == false) {
				continue;
			}
			final IDependentDataMap mp = vo.getDependents();
			for (Collection<EntityObjectVO<?>> voAttributes : mp.getRoDataMap().values()) {
				for (EntityObjectVO<?> va: voAttributes) {
					final EntityObjectVO<UID> voAttribute = (EntityObjectVO<UID>) va;
					if (voAttribute.getFieldValue(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeGrouping) == null) {
						throw new CommonValidationException(getSpringLocaleDelegate().getMessage(
								"GenerationCollectController.2",
								"Sie müssen Gruppierungsfunktionen für die Unterformular Attribute angeben!"));
					}
					String sGroup = (String) voAttribute.getFieldValue(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeGrouping);
					if (sGroup == null || sGroup.length() < 1) {
						throw new CommonValidationException(getSpringLocaleDelegate().getMessage(
								"GenerationCollectController.2",
								"Sie müssen Gruppierungsfunktionen für die Unterformular Attribute angeben!"));
					}
				}
			}
		}
	}

	private void validateAttributes(boolean grouping, Collection<EntityObjectVO<UID>> coll) throws CommonValidationException {
		final HashSet<UID> stAllTargetAttributes = new HashSet<UID>(coll.size());

		for(EntityObjectVO<UID> mdVOTarget : coll){
			if (mdVOTarget.isFlagRemoved()) {
				continue;
			}
			final UID targetAttributeUid = mdVOTarget.getFieldUid(E.GENERATIONATTRIBUTE.attributeTarget);
			if (!(stAllTargetAttributes.add(targetAttributeUid))) {
				throw new CommonValidationException(getSpringLocaleDelegate().getMessage("GenerationCollectController.1",
					"Das Zielattribut '{0}' darf nicht mehrfach vorkommen. W\u00e4hlen Sie bitte ein anderes Zielattribut aus.", targetAttributeUid));
			}

			boolean hasSourceType = mdVOTarget.getFieldValue(org.nuclos.common.E.GENERATIONATTRIBUTE.sourceType.getUID()) != null;
			boolean hasFunction = mdVOTarget.getFieldValue(E.GENERATIONATTRIBUTE.groupfunction) != null;
			if (grouping && !hasSourceType && !hasFunction) {
				throw new CommonValidationException("GenerationCollectController.groupfunction.mandatory");
			}
			else if (hasFunction && !grouping) {
				throw new CommonValidationException("GenerationCollectController.groupfunction.notallowed");
			}
			else if (hasFunction && hasSourceType) {
				throw new CommonValidationException("GenerationCollectController.groupfunction.notallowed.sourcetype");
			}
		}
	}

	public void runWithNewCollectableWithSomeFields(MasterDataVO<UID> vo) {
		try {
			runNew();
			for (CollectableComponent cc : getDetailCollectableComponentsFor(E.GENERATION.sourceModule.getUID())) {
				CollectableValueIdField valueId = new CollectableValueIdField(
						vo.getFieldId(E.GENERATION.sourceModule.getEntity()), vo.getFieldValue(E.GENERATION.sourceModule.getUID()));
				cc.setField(valueId);
			}
			for (CollectableComponent cc : getDetailCollectableComponentsFor(E.GENERATION.targetModule.getUID())) {
				CollectableValueIdField valueId = new CollectableValueIdField(
						vo.getFieldId(E.GENERATION.targetModule.getUID()),vo.getFieldValue(E.GENERATION.targetModule.getUID()));
				cc.setField(valueId);
			}
		}
		catch(CommonBusinessException e) {
			LOG.warn("runWithNewCollectableWithSomeFields: " + e);
		}
	}

	/**
	 * invalidates the generator actions cache after successful delete.
	 * @throws org.nuclos.common.NuclosBusinessException
	 */
	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		super.deleteCollectable(clct, applyMultiEditContext);
		GeneratorActions.invalidateCache();
		MasterDataCache.getInstance().invalidate(E.GENERATION.getUID());
		EventSupportDelegate.getInstance().invalidateCaches(E.GENERATION);
	}

	public void detailsChanged(Component cSource) {
		super.detailsChanged(cSource);
	}

}	// class GenerationCollectController
