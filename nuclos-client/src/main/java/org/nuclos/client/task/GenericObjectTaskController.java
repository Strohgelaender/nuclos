//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.task;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.Future;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.nuclos.client.attribute.AttributeCache;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.KeyBinding;
import org.nuclos.client.common.KeyBindingProvider;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.common.Utils;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectClientUtils;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.genericobject.ReportController;
import org.nuclos.client.genericobject.ui.EnterNameDescriptionPanel;
import org.nuclos.client.main.Main;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.MasterDataLayoutHelper;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.SaveFilterController;
import org.nuclos.client.searchfilter.SaveFilterController.Command;
import org.nuclos.client.searchfilter.SearchFilterCache;
import org.nuclos.client.searchfilter.SearchFilterDelegate;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableComponentFactory;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.client.ui.popupmenu.DefaultJPopupMenuListener;
import org.nuclos.client.ui.popupmenu.JPopupMenuFactory;
import org.nuclos.client.ui.popupmenu.JPopupMenuListener;
import org.nuclos.client.ui.table.TableUtils;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common.genericobject.GenericObjectUtils;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.preferences.ColumnPreferences;
import org.nuclos.common.preferences.ColumnSortingPreferences;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFilterException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableGenericObjectSearchExpression;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

/**
 * Controller for <code>GenericObjectTaskView</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Corina.Mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 */
public class GenericObjectTaskController extends RefreshableTaskController<GenericObjectTaskView> {
	
	private static final Logger LOG = Logger.getLogger(GenericObjectTaskController.class);
	
	private Map<UID, GenericObjectTaskView> mpTaskViews;
	private List<Future<LayerLock>> allLocks = new ArrayList<>();
	
	GenericObjectTaskController() {
		super();
		mpTaskViews= new HashMap<UID, GenericObjectTaskView>();
	}
	
	public GenericObjectTaskView newGenericObjectTaskView(EntitySearchFilter filter) {
		checkFilter(filter);
		final GenericObjectTaskView gotaskview = new GenericObjectTaskView(filter);
		gotaskview.init();
		this.mpTaskViews.put(filter.getSearchFilterVO().getId(), gotaskview);
		refresh(gotaskview);
		setupActions(gotaskview);
		
		KeyBinding keybinding = KeyBindingProvider.REFRESH;
		gotaskview.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(keybinding.getKeystroke(), keybinding.getKey());
		gotaskview.getActionMap().put(keybinding.getKey(), new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				refresh(gotaskview);
			}
		});

		return gotaskview;
	}
	
    private void checkFilter(EntitySearchFilter filter) {
		List<CollectableEntityField> lstRemovedFields = new ArrayList<CollectableEntityField>();
		List<? extends CollectableEntityField> visibleColumns = filter.getVisibleColumns();
		for (CollectableEntityField column : visibleColumns) {
			final UID entity = column.getEntityUID();
			if (Modules.getInstance().isModule(entity)) {
				try {
					MetaProvider.getInstance().getEntityField(column.getUID());
				} catch (Exception e) {
					lstRemovedFields.add(column);
					LOG.error("checkFilter for " + filter + " failed on column " + column.getUID(), e);
				}
			}
			else {
				if (MetaProvider.getInstance().getEntityField(column.getUID()) == null) { 
					lstRemovedFields.add(column);
				}
			}
		}
		visibleColumns.removeAll(lstRemovedFields);
		filter.setVisibleColumns(visibleColumns);
	}

	private void setupActions(final GenericObjectTaskView gotaskview) {
		super.addRefreshIntervalActions(gotaskview);
		
		//add mouse listener for double click in table:
		gotaskview.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ev) {
				if (ev.getClickCount() == 2) {
					cmdShowDetails(gotaskview);
				}
			}
		});
		
		gotaskview.getRefreshButton().setAction(new AbstractAction("", Icons.getInstance().getIconRefresh16()) {

			@Override
			public void actionPerformed(ActionEvent e) {
				cmdRefresh(gotaskview);
			}
		});

		gotaskview.getPrintMenuItem().setAction(new AbstractAction(getSpringLocaleDelegate().getMessage(
				"PersonalTaskController.4","Aufgabenliste drucken"), 
			Icons.getInstance().getIconPrintReport16()) {

			@Override
			public void actionPerformed(ActionEvent e) {
				cmdPrint(gotaskview);
			}
		});
		
		gotaskview.getPrintMenuItem().setEnabled(false);
		
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_PRINT_TASKLIST)) {
			gotaskview.getPrintMenuItem().setEnabled(true);
		}
		
		gotaskview.getRenameMenuItem().setAction(
				new AbstractAction(getSpringLocaleDelegate().getMessage("ExplorerController.31", "Umbenennen"), 
						Icons.getInstance().getIconEdit16()) {

			@Override
			public void actionPerformed(ActionEvent ev) {
				EntitySearchFilter filter = gotaskview.getFilter();
				String newName = cmdRenameFilter(filter);
				if(newName != null)
					Main.getInstance().getMainController().getTaskController().getTabFor(gotaskview).setTitle(newName);
			}
		});
		
		gotaskview.getResetMenuItem().setAction(new AbstractAction(
					getSpringLocaleDelegate().getMessage("ExplorerController.34", "Listenansicht zurücksetzen"),
					Icons.getInstance().getIconUndo16()) {
				@Override
				public void actionPerformed(ActionEvent e) {
					gotaskview.resetPreferences();
					cmdRefresh(gotaskview);
				}
				@Override
				public boolean isEnabled() {
					return gotaskview.isSharedTablePreference()
							&& gotaskview.isCustomizedTablePreference();
				}
		});

		gotaskview.getTransferMenuItem().setAction(new AbstractAction(
				getSpringLocaleDelegate().getMessage("GenericObjectTaskController.4", "Listenansicht von Businessobjekt übernehmen"),
				Icons.getInstance().getIconCopy16()
		) {
			@Override
			public void actionPerformed(final ActionEvent e) {
				gotaskview.transferPreferences();
				cmdRefresh(gotaskview);
			}
		});
		
		this.setupPopupMenuListener(gotaskview);
	}	
	
	private void setupRenderers(GenericObjectTaskView view) {
		// setup a table cell renderer for each column:
		for (Enumeration<TableColumn> enumeration = view.getTable().getColumnModel().getColumns(); enumeration.hasMoreElements();) {
			final TableColumn column = enumeration.nextElement();
			final int iModelIndex = column.getModelIndex();
			final CollectableEntityField clctef = view.getTableModel().getCollectableEntityField(iModelIndex);
			final CollectableComponent clctcomp = CollectableComponentFactory.getInstance().newCollectableComponent(clctef, null, false);
			column.setCellRenderer(clctcomp.getTableCellRenderer(false));
		}
	}

	@Override
	protected List<Object> getIdsForCustomRule(final GenericObjectTaskView taskView) {
		List<Collectable<?>> lstClct = getSelectedCollectables(taskView);
		List<Object> obj = new ArrayList<>();
		for (Collectable<?> clct : lstClct) {
			obj.add(clct.getPrimaryKey());
		}
		return obj;
	}

	private void setupPopupMenuListener(final GenericObjectTaskView gotaskview){
		//context menu:		
		final JMenuItem miDetails = new JMenuItem(getSpringLocaleDelegate().getMessage(
				"AbstractCollectableComponent.7","Details anzeigen..."));
		final JMenuItem miDefineAsNewSearchResult = new JMenuItem(getSpringLocaleDelegate().getMessage(
				"ExplorerController.22", "In Liste anzeigen"));
		final JMenuItem miCopyCells = new JMenuItem(getSpringLocaleDelegate().getMessage(
				"ResultPanel.13","Kopiere markierte Zellen"));
		final JMenuItem miCopyRows = new JMenuItem(getSpringLocaleDelegate().getMessage(
				"ResultPanel.14","Kopiere markierte Zeilen"));
		
		final JPopupMenuFactory factory = new JPopupMenuFactory() {
			@Override
            public JPopupMenu newJPopupMenu() {
				final JPopupMenu result = new JPopupMenu();
				miDetails.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent ev){
						cmdShowDetails(gotaskview);
					}
				});
				miDefineAsNewSearchResult.addActionListener(new ActionListener() {
					@Override
                    public void actionPerformed(ActionEvent ev) {
						cmdDefineSelectedCollectablesAsNewSearchResult(gotaskview);
					}
				});
				miCopyCells.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						JTable table = gotaskview.getTable();
						UIUtils.copyCells(table);
					}
				});
				miCopyRows.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						JTable table = gotaskview.getTable();
						UIUtils.copyRows(table);						
					}
				});
				
				result.add(miDetails);

				if (gotaskview.offerCustomRules()) {
					final UID crEntity = gotaskview.getFilter().getSearchFilterVO().getEntity();
					GenericObjectTaskController.this.addCustomRulesToPopupMenuIfAny(crEntity, gotaskview, result);
				}

				result.addSeparator();
				result.add(miDefineAsNewSearchResult);
				result.addSeparator();
				result.add(miCopyCells);
				result.add(miCopyRows);
				return result;
			}
		};
		JPopupMenuListener popupMenuListener = new DefaultJPopupMenuListener(factory, true) {
			@Override
			public void mousePressed(MouseEvent ev) {
				if (ev.getClickCount() == 1) {
					// select current row before opening the menu:
					/** @todo factor out this default selection behavior if possible */
					final int iRow = gotaskview.getTable().rowAtPoint(ev.getPoint());
					if (iRow >= 0) {							
						//Nur, wenn nicht selektiert, selektieren:
						if (!gotaskview.getTable().isRowSelected(iRow)) {
							if ((ev.getModifiers() & MouseEvent.CTRL_MASK) != 0) {
								// Control gedr\u00fcckt:
								// Zeile zur Selektion hinzuf\u00fcgen:
								gotaskview.getTable().addRowSelectionInterval(iRow, iRow);
							}
							else {
								// Sonst nur diese Zeile selektieren:
								gotaskview.getTable().setRowSelectionInterval(iRow, iRow);
							}
						}  // if	
					}
					
					final int iSelectedRowCount = gotaskview.getTable().getSelectedRowCount();
					final Collectable<?> selectedCollectable = gotaskview.getTableModel().getCollectable(gotaskview.getTable().getSelectedRow());
					
					boolean bShowDetailsEnabled;
					try {
						bShowDetailsEnabled = iSelectedRowCount == 1
							&& selectedCollectable.getId() != null
								&& MasterDataLayoutHelper.isLayoutMLAvailable(gotaskview.getTableModel().getBaseEntityUid(), false);
					}
					catch (Exception ex) {
						bShowDetailsEnabled = false;
					}
					if (bShowDetailsEnabled) {
						bShowDetailsEnabled = SecurityCache.getInstance().isReadAllowedForEntity(gotaskview.getTableModel().getBaseEntityUid());
					}
					miDetails.setEnabled(bShowDetailsEnabled);
					
					boolean bDefineAsNewSearchResultEnabled;
					try {
						bDefineAsNewSearchResultEnabled = iSelectedRowCount > 1
							&& MasterDataLayoutHelper.isLayoutMLAvailable(gotaskview.getTableModel().getBaseEntityUid(), false);
					}
					catch (Exception ex) {
						bDefineAsNewSearchResultEnabled = false;
					}
					if (bDefineAsNewSearchResultEnabled) {
						bDefineAsNewSearchResultEnabled = SecurityCache.getInstance().isReadAllowedForEntity(gotaskview.getTableModel().getBaseEntityUid());
					}
					miDefineAsNewSearchResult.setEnabled(bDefineAsNewSearchResultEnabled);								
				}
				
				super.mousePressed(ev);
			}
		};
		
		gotaskview.getTable().addMouseListener(popupMenuListener);
	}
	
	private void cmdRefresh(final GenericObjectTaskView gotaskview) {
		UIUtils.runCommand(gotaskview, new CommonRunnable() {
			@Override
            public void run() {
				refresh(gotaskview);
			}
		});
	}

	private void cmdPrint(final GenericObjectTaskView gotaskview) {
		UIUtils.runCommand(gotaskview, new CommonRunnable() {
			@Override
            public void run() {
				print(gotaskview);
			}
		});
	}

	@Override
	protected void refresh(GenericObjectTaskView gotaskview) {
		EntitySearchFilter filter = gotaskview.getFilter();
		// the filter definition might have changed:
		try {
			filter = SearchFilterCache.getInstance().getEntitySearchFilterById(filter.getSearchFilterVO().getId());
		}
		catch (NoSuchElementException ex) {
			// when the name of the filter has been changed or when the filter has been deleted we don't do anything
		}
		
		final SearchFilterVO filterVo = filter != null ? filter.getSearchFilterVO() : null;
		final String sFilterName = filterVo != null ? filterVo.getFilterName() : null;
		if (filter == null) {
			//the filter has been deleted
			Errors.getInstance().showExceptionDialog(this.getTabbedPane().getSelectedComponent(), new CommonFilterException(
					getSpringLocaleDelegate().getMessage("GenericObjectTaskController.1", "Der Filter \"{0}\" existiert nicht mehr.", sFilterName)));
		}		
		else {
			if (Modules.getInstance().isModule(filterVo.getEntity())) {
				this.refreshGenericObjectTaskView(filter);
			}
			else {
				this.refreshMasterDataTaskView(filter);
			}
		}
	}
	
	private void refreshGenericObjectTaskView(final EntitySearchFilter filter) {

		final GOClientWorkerTaskControllerAdapter clientWorker = new GOClientWorkerTaskControllerAdapter(this, filter.getSearchFilterVO().getId()) {

			SearchFilterVO vo = null;
			TruncatableCollection<GenericObjectWithDependantsVO> trunccollgovo = null;
			List<CollectableGenericObjectWithDependants> lstclct = null;

			@Override
			public void work() throws CommonBusinessException {
				/**
				 * @todo eliminate this workaround
				 */
				vo = filter.getSearchFilterVO();
				final CollectableGenericObjectSearchExpression clctexpr = new CollectableGenericObjectSearchExpression(
						filter.getInternalSearchCondition(), filter.getSortingOrder(), vo.getSearchDeleted());

				final Set<UID> stRequiredAttributeIds = new HashSet<UID>(GenericObjectUtils.getAttributeUids(
						filter.getVisibleColumns(), vo.getEntity(), AttributeCache.getInstance()));
				final Set<UID> stRequiredSubEntityNames = Collections.emptySet();

				// add the attributes 'nuclosState' and 'nuclosStateNumber' to the set of required attributes to be able
				// to check the permission for the resultset

				stRequiredAttributeIds.add(SF.STATE.getMetaData(E.SEARCHFILTER.getUID()).

						getUID());
				stRequiredAttributeIds.add(SF.STATEICON.getMetaData(E.SEARCHFILTER.getUID()).

						getUID());
				stRequiredAttributeIds.add(SF.STATENUMBER.getMetaData(E.SEARCHFILTER.getUID()).

						getUID());

				final Integer iMaxRowCount = ClientParameterProvider.getInstance().getIntValue(ParameterProvider.KEY_MAX_ROWCOUNT_FOR_SEARCHRESULT_IN_TASKLIST, 500);

				trunccollgovo = GenericObjectDelegate.getInstance().getRestrictedNumberOfGenericObjects(
						Modules.getInstance().getModule(vo.getEntity()).getUID(),
						clctexpr,
						stRequiredSubEntityNames,
						ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY),
						iMaxRowCount.longValue());

				lstclct = CollectionUtils.transform(trunccollgovo, new CollectableGenericObjectWithDependants.MakeCollectable());

				for (
						CollectableGenericObjectWithDependants clctgowd : lstclct)

				{
					final UID iStatus = clctgowd.getGenericObjectCVO().getStatus();
					// check permission for attributes
					for (DynamicAttributeVO dynamicAttributeVO : clctgowd.getGenericObjectCVO().getAttributes()) {
						final UID sAtributeName = AttributeCache.getInstance().getAttribute(dynamicAttributeVO.getAttributeUID()).getPrimaryKey();
						if (SecurityCache.getInstance().getAttributePermission(Modules.getInstance().getModule(clctgowd.getGenericObjectCVO().getModule()).getUID(), sAtributeName, iStatus) == null) {
							dynamicAttributeVO.setValue(null);
						}
					}

					// check permission for subforms
					IDependentDataMap mpDependants = clctgowd.getGenericObjectWithDependantsCVO().getDependents();
					if (mpDependants != null) {
						for (IDependentKey dependenceKey : new ArrayList<IDependentKey>(mpDependants.getKeySet())) {
							FieldMeta<?> refFieldMeta = MetaProvider.getInstance().getEntityField(dependenceKey.getDependentRefFieldUID());
							Map<UID, SubformPermission> mpPermission = SecurityCache.getInstance().getSubFormPermission(refFieldMeta.getEntity());

							if (mpPermission.get(iStatus) == null) {
								clctgowd.getGenericObjectWithDependantsCVO().getDependents().removeKey(dependenceKey);
							}
						}
					}
				}
			}

			@Override
			public void paint() throws CommonBusinessException {
				if (vo == null || trunccollgovo == null || lstclct == null) {
					return;
				}

				String sLabel = getSpringLocaleDelegate().getMessage(
						"GenericObjectTaskController.2", "{0} Datens\u00e4tze gefunden.", trunccollgovo.totalSize());
				if (trunccollgovo.isTruncated())

				{
					sLabel += " " + getSpringLocaleDelegate().getMessage(
							"GenericObjectTaskController.3", "Das Ergebnis wurde nach {0} Zeilen abgeschnitten.", trunccollgovo.size());
				}
				//this.mpTaskViews.get(filter).tfStatusBar.setText(sLabel);
				GenericObjectTaskController.this.mpTaskViews.get(vo.getId()).tfStatusBar.setText(sLabel);

				final GenericObjectTaskView genericObjectTaskView = GenericObjectTaskController.this.mpTaskViews.get(vo.getId());
				genericObjectTaskView.setIgnorePreferencesUpdates(true);
				// remove listener from old model, if any:
				final TableModel modelOld = genericObjectTaskView.getTable().getModel();
				if (modelOld != null && modelOld instanceof SortableCollectableTableModel<?, ?>)

				{
					TableUtils.removeMouseListenersForSortingFromTableHeader(genericObjectTaskView.getTable());
					//does not make any sense, already saved by clicking in the column header
					//genericObjectTaskView.storeOrderBySelectedColumnToPreferences();
				}

				// create a new table model:
				final SortableCollectableTableModel<Long, Collectable<Long>> tblmdl = genericObjectTaskView.newResultTableModel(filter, lstclct);
				genericObjectTaskView.getTable().

						setModel(tblmdl);
				//		genericObjectTaskView.getTable().setTableHeader(new ToolTipsTableHeader(genericObjectTaskView.getTable().getColumnModel()));
				TableUtils.addMouseListenerForSortingToTableHeader(genericObjectTaskView.getTable(), tblmdl, new

						CommonRunnable() {
							@Override
							public void run() {
								tblmdl.sort();
								genericObjectTaskView.storeOrderBySelectedColumnToPreferences();
							}
						});

				//TableUtils.setOptimalColumnWidths(this.mpTaskViews.get(filter.getId()).getJTable());
				TaskController.setColumnWidths(genericObjectTaskView.readColumnWidthsFromPreferences(), genericObjectTaskView.getTable());

				// setup renderer
				setupRenderers(genericObjectTaskView);
				genericObjectTaskView.setIgnorePreferencesUpdates(false);
				super.paint();
			}
		};

		CommonMultiThreader.getInstance().execute(clientWorker);
	}
	
	private void refreshMasterDataTaskView(final EntitySearchFilter filter) {
		final GOClientWorkerTaskControllerAdapter clientWorker = new GOClientWorkerTaskControllerAdapter(this, filter.getSearchFilterVO().getId()) {

			SearchFilterVO vo = null;
			ProxyList<Object,MasterDataVO<Object>> lst = null;
			List<CollectableMasterDataWithDependants<Long>> lstclct = null;

			@Override
			public void work() throws CommonBusinessException {
				CollectableSearchExpression clctsexpr = new CollectableSearchExpression(filter.getInternalSearchCondition(), filter.getSortingOrder());

				vo = filter.getSearchFilterVO();
				List<CollectableEntityField> cefs = new ArrayList<>();
				List<CollectableSorting> sortings = filter.getSortingOrder();

				for (ColumnSortingPreferences colsortpref : GenericObjectTaskController.this.mpTaskViews.get(vo.getId()).getPreferences().getColumnSortings()) {
					sortings.add(new CollectableSorting(colsortpref.getColumn(), colsortpref.isAsc()));
				}
				clctsexpr.setSortingOrder(sortings);

				for (ColumnPreferences colpref : GenericObjectTaskController.this.mpTaskViews.get(vo.getId()).getPreferences().getSelectedColumnPreferences()) {
					cefs.add(new CollectableEOEntityField(MetaProvider.getInstance().getEntityField(colpref.getColumn())));
				}
//
//				for (FieldMeta<?> fMeta : AllEntityFieldsByEntity(filter.getSearchFilterVO().getEntity()).values()) {
//					cefs.add(new CollectableEOEntityField(fMeta));
//				}
				lst = MasterDataDelegate.getInstance().getMasterDataProxyList(vo.getEntity(), cefs, clctsexpr);

				lstclct = CollectionUtils.transform(lst,
						new CollectableMasterDataWithDependants.MakeCollectable((CollectableMasterDataEntity)
								NuclosCollectableEntityProvider.getInstance().getCollectableEntity(vo.getEntity())));

			}

			@Override
			public void paint() throws CommonBusinessException {
				if (vo == null || lst == null || lstclct == null) {
					return;
				}

				String sLabel = getSpringLocaleDelegate().getMessage("GenericObjectTaskController.2", "{0} Datens\u00e4tze gefunden.", lst.size());

				GenericObjectTaskController.this.mpTaskViews.get(vo.getId()).tfStatusBar.setText(sLabel);

				final GenericObjectTaskView genericObjectTaskView = GenericObjectTaskController.this.mpTaskViews.get(vo.getId());
				genericObjectTaskView.setIgnorePreferencesUpdates(true);
				// remove listener from old model, if any:
				final TableModel modelOld = GenericObjectTaskController.this.mpTaskViews.get(vo.getId()).getTable().getModel();
				if (modelOld != null && modelOld instanceof SortableCollectableTableModel<?,?>) {
					TableUtils.removeMouseListenersForSortingFromTableHeader(genericObjectTaskView.getTable());
					genericObjectTaskView.storeOrderBySelectedColumnToPreferences();
				}

				// create a new table model:
				final SortableCollectableTableModel<Long,Collectable<Long>> tblmdl = genericObjectTaskView.newResultTableModel(filter, lstclct);
				genericObjectTaskView.getTable().setModel(tblmdl);
				//genericObjectTaskView.getTable().setTableHeader(new ToolTipsTableHeader(genericObjectTaskView.getTable().getColumnModel()));
				TableUtils.addMouseListenerForSortingToTableHeader(genericObjectTaskView.getTable(), tblmdl, new CommonRunnable() {
					@Override
					public void run() {
						tblmdl.sort();
						genericObjectTaskView.storeOrderBySelectedColumnToPreferences();
					}
				});

				//TableUtils.setOptimalColumnWidths(this.mpTaskViews.get(filter.getId()).getJTable());
				TaskController.setColumnWidths(genericObjectTaskView.readColumnWidthsFromPreferences(), genericObjectTaskView.getTable());

				// setup renderer
				setupRenderers(genericObjectTaskView);
				genericObjectTaskView.setIgnorePreferencesUpdates(false);
				super.paint();
			}
		};

		CommonMultiThreader.getInstance().execute(clientWorker);
	}
	
	void print(GenericObjectTaskView gotaskview) {
		try {
			new ReportController(getTabbedPane().getComponentPanel()).export(
					gotaskview.getFilter().getSearchFilterVO().getEntity(), null, gotaskview.getTable(), null);
		}
		catch (CommonBusinessException ex) {
			Errors.getInstance().showExceptionDialog(this.getTabbedPane().getComponentPanel(), ex);
		}
	}

	private void cmdShowDetails(final GenericObjectTaskView gotaskview) {
		UIUtils.runCommandForTabbedPane(getTabbedPane(), new CommonRunnable() {
			@Override
            public void run() throws CommonBusinessException {
				UIUtils.setWaitCursor();
				final Collectable<?> clctSelected = getSelectedCollectable(gotaskview);
				if (clctSelected != null) {
					if (Modules.getInstance().isModule(gotaskview.getFilter().getSearchFilterVO().getEntity())) {
						final CollectableGenericObject clctloSelected = (CollectableGenericObject) clctSelected;
						// we must reload the partially loaded object:
						final UID iModuleId = clctloSelected .getGenericObjectCVO().getModule();
						GenericObjectClientUtils.showDetails(iModuleId, clctloSelected.getId());
					}
					else {
						final CollectableMasterDataWithDependants<?> clctmdSelected = (CollectableMasterDataWithDependants<?>) clctSelected;
						Main.getInstance().getMainController().showDetails(clctmdSelected.getCollectableEntity().getUID(), clctmdSelected.getPrimaryKey());
					}					
				}
				UIUtils.setDefaultCursor();
			}
		});
	}
	
	private void cmdDefineSelectedCollectablesAsNewSearchResult(final GenericObjectTaskView gotaskview) {
		UIUtils.runCommandForTabbedPane(getTabbedPane(), new CommonRunnable(){
			@Override
            public void run() throws CommonBusinessException {
				final Collection<Collectable<?>> collclct = getSelectedCollectables(gotaskview);

				assert CollectionUtils.isNonEmpty(collclct);
				
				final CollectableSearchCondition cond = getCollectableSearchCondition(collclct);
				final EntitySearchFilter filter = gotaskview.getFilter();
				final SearchFilterVO vo = filter.getSearchFilterVO();
				if (Modules.getInstance().isModule(vo.getEntity())) {
					final UID iModuleId = getCommonModuleId(collclct);
					final GenericObjectCollectController ctlGenericObject = NuclosCollectControllerFactory.getInstance().
							newGenericObjectCollectController(iModuleId, null, 
							ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
					ctlGenericObject.setSearchDeleted(CollectableGenericObjectSearchExpression.SEARCH_BOTH);
					ctlGenericObject.runViewResults(cond);
				}
				else {
					MasterDataCollectController<?> ctlMasterData = NuclosCollectControllerFactory.getInstance().
						newMasterDataCollectController(vo.getEntity(), null, 
						ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
					ctlMasterData.runViewResults(cond);
				}				
			}
		});
		
	}
	
	/**
	 * @return the selected collectable, if any.
	 */
	Collectable<?> getSelectedCollectable(final GenericObjectTaskView gotaskview) {
		final int iSelectedRow = gotaskview.getTable().getSelectedRow();
		Collectable<?> result = null;
		if (iSelectedRow != -1) {
			result = gotaskview.getTableModel().getCollectable(iSelectedRow);
		}
		return result;
	}

	/**
	 * @return the selected collectables, if any
	 */
	List<Collectable<?>> getSelectedCollectables(final GenericObjectTaskView gotaskview) {
		final List<Integer> lstSelectedRowNumbers = CollectionUtils.asList(gotaskview.getTable().getSelectedRows());
		final List<Collectable<?>> result = CollectionUtils.transform(lstSelectedRowNumbers, new Transformer<Integer, Collectable<?>>() {
			@Override
            public Collectable<?> transform(Integer iRowNo) {
				return gotaskview.getTableModel().getCollectable(iRowNo);
			}
		});
		assert result != null;
		return result;
	}
	
	
	/**
	 * §precondition !CollectionUtils.isNullOrEmpty(collclct)
	 */
	private static CollectableSearchCondition getCollectableSearchCondition(Collection<Collectable<?>> collclct) {
		final Collection<Object> collIds = CollectionUtils.transform(collclct, new Transformer<Collectable<?>, Object>() {
			@Override
            public Object transform(Collectable<?> clct) {
				return clct.getId();
			}
		});

		return SearchConditionUtils.getCollectableSearchConditionForIds(collIds);
	}
	
	/**
	 * @return the module id shared by all collectables, if any.
	 */
	private static UID getCommonModuleId(Collection<Collectable<?>> collclct) throws CommonPermissionException{
		return Utils.getCommonObject(CollectionUtils.transform(collclct, new Transformer<Collectable<?>, UID>() {
			@Override
            public UID transform(Collectable<?> clct) {
				try {								
					return GenericObjectDelegate.getInstance().get((Long) clct.getId()).getModule();
				}catch(CommonBusinessException e){
					LOG.warn("getCommonModuleId failed: " + e);
					return null;
				}						 
			}
		}));
	}
	
	public void removeGenericObjectTaskView(GenericObjectTaskView gotaskview) {
		mpTaskViews.remove(gotaskview.getFilter().getSearchFilterVO().getId());
	}

	@Override
	public ScheduledRefreshable getSingleScheduledRefreshableView() {
		throw new NuclosFatalException("Undefined View in getScheduledRefreshableView in GenericObjectTaskController"); 
	}

	@Override
	public void refreshScheduled(ScheduledRefreshable isRefreshable) {
		if(isRefreshable instanceof GenericObjectTaskView){
			refresh((GenericObjectTaskView)isRefreshable);
		} else {
			throw new NuclosFatalException("Wrong ScheduledRefreshable in refreshScheduled in GenericObjectTaskController");
		}
	}
	
	private String cmdRenameFilter(EntitySearchFilter viewfilter) {
		final SearchFilterVO vo = viewfilter.getSearchFilterVO();
		final EntitySearchFilter oldFilter = SearchFilterCache.getInstance().getEntitySearchFilterById(vo.getId());
		final SearchFilterVO oldVo = oldFilter.getSearchFilterVO();
		final String oldFilterName = oldVo.getFilterName();
		final String sOwner = oldVo.getOwner();
		final UID entity = oldVo.getEntity();

		try {
			if(!oldVo.isEditable())
				throw new NuclosBusinessException(getSpringLocaleDelegate().getMessage(
						"SaveFilterController.4","Der Suchfilter darf von Ihnen nicht ge\u00e4ndert werden."));

			EnterNameDescriptionPanel newNamePanel = new EnterNameDescriptionPanel(null, null);
			newNamePanel.getTextFieldName().setText(oldVo.getFilterName());
			newNamePanel.getTextFieldDescription().setText(oldVo.getDescription());

			int result = SaveFilterController.showDialog(getTabbedPane().getComponentPanel(), getSpringLocaleDelegate().getMessage(
					"SaveFilterController.9", "Bestehenden Filter \"{0}\" \u00e4ndern", oldFilterName), newNamePanel, oldFilterName, sOwner, entity, Command.Overwrite);
			if(result == JOptionPane.OK_OPTION) {
				EntitySearchFilter newFilter = new EntitySearchFilter();

				newFilter.setSearchFilterVO(new SearchFilterVO(oldVo.getId(), oldVo.getVersion(), oldVo));
				newFilter.setSearchCondition(oldFilter.getSearchCondition());
				newFilter.setVisibleColumns(oldFilter.getVisibleColumns());

				vo.setFilterName(newNamePanel.getTextFieldName().getText());
				vo.setDescription(newNamePanel.getTextFieldDescription().getText());

				SearchFilterDelegate.getInstance().updateSearchFilter(newFilter, oldFilterName, oldVo.getOwner(), oldVo.getEntity());
				return vo.getFilterName();
			}
		}
		catch(NoSuchElementException e) {
			LOG.info("cmdRenameFilter failed: " + e);
		}
		catch(NuclosBusinessException e) {
			throw new NuclosFatalException(e);
		}
		return null;
	}

	private static abstract class GOClientWorkerTaskControllerAdapter extends CommonClientWorkerTaskControllerAdapter {

		GenericObjectTaskController gotCtrl = null;

		public GOClientWorkerTaskControllerAdapter(GenericObjectTaskController gotCtrl, UID filterUID) {
			super(gotCtrl, filterUID);
			this.gotCtrl = gotCtrl;
		}

		@Override
		protected final TaskView getTaskView(final UID filterUID) {
			return gotCtrl.mpTaskViews.get(filterUID);
		}

		@Override
		protected final List<Future<LayerLock>> getAllLocksList() {
			return gotCtrl.allLocks;
		}
	}

}
