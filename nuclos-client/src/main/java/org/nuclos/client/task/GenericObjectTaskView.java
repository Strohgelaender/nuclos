//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.task;

import java.awt.Point;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.NuclosDropTargetVisitor;
import org.nuclos.client.common.OneDropNuclosDropTargetListener;
import org.nuclos.client.common.TablePreferencesUtils;
import org.nuclos.client.common.TimedPreferencesUpdater;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.genericobject.CollectableGenericObject;
import org.nuclos.client.genericobject.GenericObjectClientUtils;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.Main;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.ui.IMainFrameTabClosableController;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.model.CollectableTableModel;
import org.nuclos.client.ui.collect.model.GenericObjectsResultTableModel;
import org.nuclos.client.ui.collect.model.MasterDataResultTableModel;
import org.nuclos.client.ui.collect.model.SortableCollectableTableModel;
import org.nuclos.client.ui.table.CommonJTableWithIcons;
import org.nuclos.common.Actions;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.preferences.ColumnPreferences;
import org.nuclos.common.preferences.PreferencesProvider;
import org.nuclos.common.preferences.SearchFilterTaskListTablePreferencesManager;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

/**
 * View on a list of leased objects.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class GenericObjectTaskView extends TaskView implements IMainFrameTabClosableController, NuclosDropTargetVisitor{

	private static final Logger LOG = Logger.getLogger(GenericObjectTaskView.class);

	private final JMenuItem btnPrint = new JMenuItem();
	private final JMenuItem btnRename = new JMenuItem();
	private final JMenuItem btnReset = new JMenuItem();
	private final JMenuItem btnTransfer = new JMenuItem();

	private final JTable tbl = new CommonJTableWithIcons();

	private EntitySearchFilter filter;

	private SearchFilterTaskListTablePreferencesManager tblprefManager;

	public GenericObjectTaskView(EntitySearchFilter filter) {
		this.setFilter(filter);
		
		this.tbl.setRowHeight(SubForm.MIN_ROWHEIGHT);
		this.tbl.setTableHeader(new TaskViewTableHeader(tbl.getColumnModel()));

		setColumnModelListener();
	}

	@Override
	public void init() {
		super.init();
		setupDragDrop();
	}
	
	JMenuItem getPrintMenuItem() {
		return btnPrint;
	}
	
	JMenuItem getRenameMenuItem() {
		return btnRename;
	}
	
	JMenuItem getResetMenuItem() {
		return btnReset;
	}

	JMenuItem getTransferMenuItem() {
		return btnTransfer;
	}
	
	protected void setupDragDrop() {		
		OneDropNuclosDropTargetListener listener = new OneDropNuclosDropTargetListener(this, ClientParameterProvider.getInstance().getIntValue(ParameterProvider.KEY_DRAG_CURSOR_HOLDING_TIME, 600));		
		DropTarget drop = new DropTarget(tbl, listener);
		drop.setActive(true);		
	}

	public CollectableTableModel<Long,Collectable<Long>> getTableModel() {
		return (CollectableTableModel<Long,Collectable<Long>>) this.tbl.getModel();
	}

	/**
	 * sets the filter for this view
	 * @param filter
	 */
	public void setFilter(EntitySearchFilter filter) {
		if (this.filter != filter) {
			this.filter = filter;
			//this.refresh();
		}
	}

	/**
	 * @return the filter for this view
	 */
	public EntitySearchFilter getFilter() {
		return this.filter;
	}

	public boolean offerCustomRules() {
		Boolean b = this.filter != null ? this.filter.getSearchFilterVO().isCustomRules() : Boolean.FALSE;
		return b != null ? b : false;
	}

	/**
	 * @param filter
	 * @param collclct
	 * @return a new collectable table model containing <code>collclct</code>.
	 */
	public final SortableCollectableTableModel<Long,Collectable<Long>> newResultTableModel(
			final EntitySearchFilter filter, Collection<? extends Collectable<Long>> collclct) {
		final boolean bIgnorePreferencesUpdatesBefore = isIgnorePreferencesUpdates();
		setIgnorePreferencesUpdates(true);
		final SearchFilterVO filterVo = filter.getSearchFilterVO();
		final CollectableEntity clcteMain = DefaultCollectableEntityProvider.getInstance().getCollectableEntity(filterVo.getEntity());
		final SortableCollectableTableModel<Long,Collectable<Long>> result;
		if (Modules.getInstance().isModule(filterVo.getEntity())) {
			result = new GenericObjectsResultTableModel<Long,Collectable<Long>>(clcteMain, getColumnOrderFromPreferences());
		}
		else {
			result = new MasterDataResultTableModel<Long,Collectable<Long>>(clcteMain, getColumnOrderFromPreferences());
		}
		result.setCollectables(new ArrayList<Collectable<Long>>(collclct));

		getTable().setModel(result);
		if (result.getColumnCount() > 0) {
			// setup sorted fields and sorting order from preferences
			try {
				result.setSortKeys(readColumnSortingFromPreferences(), true);
			} catch (IllegalArgumentException e) {
				result.setSortKeys(Collections.singletonList(new SortKey(0, SortOrder.ASCENDING)), false);
			}
		}
		setIgnorePreferencesUpdates(bIgnorePreferencesUpdatesBefore);
		return result;
	}

	private List<? extends CollectableEntityField> getColumnOrderFromPreferences() {
		return TablePreferencesUtils.getCollectableEntityFieldsForGenericObject(getTablePreferencesManager());
	}

	private SearchFilterTaskListTablePreferencesManager getTablePreferencesManager() {
		if (tblprefManager == null) {
			tblprefManager = PreferencesProvider.getInstance().getTablePreferencesManagerForSearchFilterTaskList(
					filter.getSearchFilterVO(),
					Main.getInstance().getMainController().getUserName(),
					ClientMandatorContext.getMandatorUID());
		}
		return tblprefManager;
	}

	public boolean isSharedTablePreference() {
		return getTablePreferencesManager().getSelected().isShared();
	}

	public boolean isCustomizedTablePreference() {
		return getTablePreferencesManager().getSelected().isCustomized();
	}

	/**
	 * Reads the user-preferences for the sorting order.
	 */
	private List<SortKey> readColumnSortingFromPreferences() {
		List<SortKey> result = getTablePreferencesManager().getSortKeys(new TablePreferencesManager.IColumnIndexResolver() {
			@Override
			public int getColumnIndex(UID columnIdentifier) {
				try {
					return getTableModel().findColumnByFieldUid(columnIdentifier);
				} catch (ClassCastException cce) {
					LOG.error("ResultTableModel is not sortable", cce);
					return -1;
				}
			}
		});
		if (result == null) {
			result = Collections.emptyList();
		}
		return result;
	}
	
	public List<Integer> readColumnWidthsFromPreferences() {
		final List<Integer> lstColumnWidths = new ArrayList<>();;
		List<ColumnPreferences> lstColumnPref = getTablePreferencesManager().getSelectedColumnPreferences();
		for (ColumnPreferences pref : lstColumnPref) {
			lstColumnWidths.add(pref.getWidth());
		}
		return lstColumnWidths;
	}

	@Override
	public boolean isClosable() {
		return true;
	}

	@Override
	public void visitDragEnter(DropTargetDragEvent dtde) {}

	@Override
	public void visitDragExit(DropTargetEvent dte) {}

	@Override
	public void visitDragOver(DropTargetDragEvent dtde) {
		Point here = dtde.getLocation();
		int hereRow = tbl.rowAtPoint(here);
		
		CollectableTableModel<Long,Collectable<Long>> model = getTableModel();							
		
		final Collectable<Long> clctSelected = model.getCollectable(hereRow);
		try {
			if (clctSelected != null) {
				if (Modules.getInstance().isModule(getFilter().getSearchFilterVO().getEntity())) {
					final CollectableGenericObject clctloSelected = (CollectableGenericObject) clctSelected;
					// we must reload the partially loaded object:
					final UID iModuleId = clctloSelected .getGenericObjectCVO().getModule();
					GenericObjectClientUtils.showDetails(iModuleId, clctloSelected.getId());
				}
				else {
					final CollectableMasterDataWithDependants clctmdSelected = (CollectableMasterDataWithDependants) clctSelected;
					Main.getInstance().getMainController().showDetails(clctmdSelected.getCollectableEntity().getUID(), clctmdSelected.getPrimaryKey());
				}					
			}
		}
		catch(Exception e) {
			throw new NuclosFatalException(e);
		}	
	}

	@Override
	public void visitDrop(DropTargetDropEvent dtde) {}

	@Override
	public void visitDropActionChanged(DropTargetDragEvent dtde) {}

	@Override
	protected List<JComponent> getToolbarComponents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List<JComponent> getExtrasMenuComponents() {
		List<JComponent> result = new ArrayList<JComponent>();
		result.add(btnPrint);
		result.add(btnRename);
		result.add(btnReset);
		if (SecurityCache.getInstance().isActionAllowed(Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS)) {
			result.add(btnTransfer);
		}
		return result;
	}

	@Override
	protected JTable getTable() {
		return tbl;
	}

	public void storeOrderBySelectedColumnToPreferences() {
		if (isIgnorePreferencesUpdates()) {
			return;
		}
		if (getTableModel() instanceof SortableCollectableTableModel<?,?>) {
			final TablePreferencesManager tblprefManager = getTablePreferencesManager();
			final SortableCollectableTableModel<?,?> tblModel = (SortableCollectableTableModel<?, ?>) getTableModel();
			tblprefManager.setSortKeys(((SortableCollectableTableModel<?,?>) getTableModel()).getSortKeys(),
					new TablePreferencesManager.IColumnFieldUidResolver() {
						@Override
						public UID getColumnFieldUid(int iColumn) {
							return tblModel.getColumnFieldUid(iColumn);
						}
					});
		}
	}

	private void storePreferences() {
		if (isIgnorePreferencesUpdates()) {
			return;
		}
		if (getTableModel() instanceof SortableCollectableTableModel<?,?>) {
			final List<CollectableEntityField> lstClctFields = new ArrayList<>();
			final List<Integer> lstWidths = new ArrayList<>();
			final SortableCollectableTableModel<?,?> tblModel = (SortableCollectableTableModel<?, ?>) getTableModel();
			final TableColumnModel columnModel = tbl.getColumnModel();
			final Enumeration<TableColumn> columns = columnModel.getColumns();
			for(int colViewIdx = 0; columns.hasMoreElements(); colViewIdx++) {
				TableColumn column = columns.nextElement();
				final int colModelIdx = tbl.convertColumnIndexToModel(colViewIdx);
				final CollectableEntityField clctField = tblModel.getCollectableEntityField(colModelIdx);
				lstClctFields.add(clctField);
				lstWidths.add(column.getWidth());
			}
			final TablePreferencesManager tblprefManager = getTablePreferencesManager();

			final TablePreferences tp = tblprefManager.getSelected();
			if (!tp.isUserdefinedName() || StringUtils.equals(getFilter().getSearchFilterVO().getFilterName(), tp.getName())) {
				// new prefs, we have to set a name, in order to differentiate it later from other tasklist prefs
				tp.setName(StringUtils.defaultIfNull(getFilter().getSearchFilterVO().getFilterName(), getFilter().getSearchFilterVO().getId().getString()));
				tp.setUserdefinedName(true);
			}

			TablePreferencesUtils.setCollectableEntityFieldsForGenericObject(tblprefManager, lstClctFields, lstWidths, null);
		}
	}

	private void setColumnModelListener() {
		tbl.getColumnModel().addColumnModelListener(new PreferencesUpdateListener(this));
	}

	public void resetPreferences() {
		final TablePreferencesManager tblprefManager = getTablePreferencesManager();
		final TablePreferences tp = tblprefManager.getSelected();
		if (tp.isShared()) {
			tblprefManager.reset(tp);
		}
	}

	public void transferPreferences() {
		final SearchFilterTaskListTablePreferencesManager tblprefManager = getTablePreferencesManager();
		final TablePreferences tp = tblprefManager.getSelected();
		tblprefManager.transferFromEntity(tp);
	}

	public TablePreferences getPreferences() {
		return getTablePreferencesManager().getSelected();
	}

	protected class PreferencesUpdateListener extends TimedPreferencesUpdater implements TableColumnModelListener {

		private final WeakReference<GenericObjectTaskView> weakView;

		public PreferencesUpdateListener(final GenericObjectTaskView view) {
			weakView = new WeakReference<>(view);
		}

		@Override
		public void columnSelectionChanged(ListSelectionEvent ev) {
		}

		@Override
		public void columnMoved(TableColumnModelEvent ev) {
			storePreferences();
		}

		@Override
		public void columnMarginChanged(ChangeEvent ev) {
			storePreferences();
		}

		@Override
		public void columnAdded(TableColumnModelEvent ev) {
		}

		@Override
		public void columnRemoved(TableColumnModelEvent ev) {
		}

		private void storePreferences() {
			final GenericObjectTaskView taskView = weakView.get();
			if (taskView != null) {
				if (taskView.isIgnorePreferencesUpdates()) {
					return;
				}
				super.registerWorker(new PreferencesStoreWorker(taskView.getFilter().getSearchFilterVO().getId()) {
					@Override
					public void storePreferences() {
						GenericObjectTaskView.this.storePreferences();
					}
				});
			}
		}
	}

}	// class GenericObjectTaskView
