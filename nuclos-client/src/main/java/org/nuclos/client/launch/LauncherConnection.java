package org.nuclos.client.launch;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.Socket;

import org.nuclos.schema.launcher.LauncherMessage;
import org.nuclos.schema.launcher.LauncherPidMessage;
import org.nuclos.schema.launcher.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LauncherConnection implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(LauncherConnection.class);

	private final int launcherPort;
	private final BigInteger clientId;

	private ObjectOutputStream out;

	private final LauncherMessageHandler messageHandler = new LauncherMessageHandler();

	public LauncherConnection(final int launcherPort, final BigInteger clientId) throws IOException {
		this.launcherPort = launcherPort;
		this.clientId = clientId;
	}

	@Override
	public void run() {
		Socket socket = null;
		try {
			log.info("Initializing launcher connection");

			socket = new Socket("localhost", launcherPort);
			out = new ObjectOutputStream(socket.getOutputStream());

			log.info("Launcher connection initialized");

			write(createPidMessage());

			final ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
			Object message = in.readObject();

			while (message != null) {
				log.info("Received launcher message: {}", message);

				try {
					messageHandler.handleMessage((LauncherMessage) message);
				} catch (Exception e) {
					log.warn("Failed to handle Launcher message", e);
				}

				message = in.readObject();
			}
		} catch (IOException | ClassNotFoundException e) {
			log.error("Failed to read from launcher", e);
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	private void write(final Object message) throws IOException {
		out.writeObject(message);
		out.flush();
		out.reset();
	}

	private LauncherPidMessage createPidMessage() {
		ObjectFactory factory = new ObjectFactory();
		LauncherPidMessage message = factory.createLauncherPidMessage();
		message.setClientId(clientId);

		// TODO: There is currently no safe, platform-independent way to get the current PID.
		// Starting from Java 9, we could use: long pid = ProcessHandle.current().getPid();
		message.setPid(BigInteger.ZERO);

		return message;
	}
}
