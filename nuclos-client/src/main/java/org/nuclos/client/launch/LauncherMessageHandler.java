package org.nuclos.client.launch;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import org.nuclos.client.main.Main;
import org.nuclos.client.main.MainController;
import org.nuclos.schema.launcher.LauncherLaunchMessage;
import org.nuclos.schema.launcher.LauncherPidMessage;
import org.nuclos.schema.launcher.Parameter;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LauncherMessageHandler extends AbstractLauncherMessageHandler {

	@Override
	protected void handleLaunchMessage(final LauncherLaunchMessage message) {
		final List<Parameter> parameters = message.getParameters();
		final Map<String, String> parameterMap = parametersToMap(parameters);

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Main.notifyListeners(parameterMap);
				MainController.getMainFrame().toFront();
			}
		});
	}

	@Override
	protected void handlePidMessage(final LauncherPidMessage message) {
	}

	private static Map<String, String> parametersToMap(List<Parameter> parameters) {
		Map<String, String> map = new LinkedHashMap<>();
		for (Parameter param : parameters) {
			map.put(param.getKey(), param.getValue());
		}
		return map;
	}
}
