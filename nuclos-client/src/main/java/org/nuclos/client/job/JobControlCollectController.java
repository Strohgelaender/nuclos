//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.job;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.nuclos.api.rule.JobRule;
import org.nuclos.client.command.CommonClientWorker;
import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.SubFormController;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.rule.server.EventSupportDelegate;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.rule.server.panel.EventSupportSelectionDataChangeListener;
import org.nuclos.client.rule.server.panel.EventSupportSelectionPanel;
import org.nuclos.client.rule.server.panel.EventSupportTypeConverter;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.CommonMultiThreader;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelAdapter;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelEvent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModelListener;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.job.IntervalUnit;
import org.nuclos.common.job.JobResult;
import org.nuclos.common.job.JobType;
import org.nuclos.common.job.JobUtils;
import org.nuclos.common.report.ejb3.IJobKey;
import org.nuclos.common.report.ejb3.JobKeyImpl;
import org.nuclos.common.report.ejb3.SchedulerControlFacadeRemote;
import org.nuclos.common2.CommonRunnable;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.NuclosUpdateException;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * Controller for nucleus quartz jobs.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 */
public class JobControlCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(JobControlCollectController.class);

	// former Spring injection
	
	private SchedulerControlFacadeRemote schedulerControlFacadeRemote;
	private EventSupportSelectionPanel<EventSupportJobVO> pnlJobs;
	
	// end of former Spring injection
	
	ButtonGroup intervalOrCronGroup;
	
	JRadioButton radiobuttonInterval;
	
	JRadioButton radiobuttonCron;
	
	private final JobControlDelegate delegate = JobControlDelegate.getInstance();

	private final Action actSchedule = new CommonAbstractAction(Icons.getInstance().getIconPlay16(), getSpringLocaleDelegate().getMessage(
			"JobControlCollectController.1","Aktivieren")) {

		@Override
		public void actionPerformed(ActionEvent ev) {
			try {
				cmdScheduleJob();
			}
			catch (CommonBusinessException e) {
				JOptionPane.showMessageDialog(JobControlCollectController.this.getDetailsPanel(), getSpringLocaleDelegate().getMessageFromResource(
						e.getMessage()), "", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	};

	@Override
	protected void unsafeFillDetailsPanel(final CollectableMasterDataWithDependants<UID> clct)
			throws NuclosBusinessException {
		super.unsafeFillDetailsPanel(clct);

		try {
			this.pnlJobs.reloadModel(EventSupportRepository.getInstance()
					.getEventSupportsByJobUid(clct.getPrimaryKey()));
		} catch (Exception e) {
			Errors.getInstance().showExceptionDialog(this.getDetailsPanel(), e);
		}

	}
	
	@Override
	public void refreshCurrentCollectable() throws CommonBusinessException {
		super.refreshCurrentCollectable();
		this.pnlJobs.reloadModel(EventSupportRepository.getInstance().getEventSupportsByJobUid(getSelectedCollectableId()));
	}

	@Override
	protected CollectableMasterDataWithDependants updateCurrentCollectable(CollectableMasterDataWithDependants clctCurrent) throws CommonBusinessException {
		CollectableMasterDataWithDependants updateCurrentCollectable = super.updateCurrentCollectable(clctCurrent);
		
		EventSupportRepository.getInstance().updateEventSupports();
		MasterDataCache.getInstance().invalidate(E.JOBCONTROLLER.getUID());
		EventSupportDelegate.getInstance().invalidateCaches(E.JOBCONTROLLER, E.SERVERCODE);
		
		return updateCurrentCollectable;
	}
	
	private final Action actUnschedule = new CommonAbstractAction(Icons.getInstance().getIconStop16(), getSpringLocaleDelegate().getMessage(
			"JobControlCollectController.3","Deaktivieren")) {

		@Override
		public void actionPerformed(ActionEvent ev) {
			try {
				cmdUnscheduleJob();
			}
			catch (CommonBusinessException e) {
				JOptionPane.showMessageDialog(JobControlCollectController.this.getDetailsPanel(), getSpringLocaleDelegate().getMessageFromResource(
						e.getMessage()), "", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	};

	private final Action actStartImmediately = new CommonAbstractAction(Icons.getInstance().getIconNext16(), getSpringLocaleDelegate().getMessage(
			"JobControlCollectController.2","Ausf\u00fchren")) {

		@Override
		public void actionPerformed(ActionEvent ev) {
			cmdStartJobImmediately();
		}
	};

	private final CollectableComponentModelListener ccml_jobtype = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			String sType = (String)ev.getCollectableComponentModel().getField().getValue();
			sType = (String)ev.getNewValue().getValue();
			enableParameterSubForm(sType);
		}
	};

	private final CollectableComponentModelListener ccml_cronexpression = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			CollectableComponentModel usecron = getDetailsEditView().getModel().getCollectableComponentModelFor(
					E.JOBCONTROLLER.cronexpression.getUID());
			if (isCronSelected()) {
				Date startdate;
				String starttime;
				Integer interval;
				IntervalUnit unit;

				if (ev.getCollectableComponentModel().getEntityField().getUID().equals(E.JOBCONTROLLER.startdate.getUID())) {
					startdate = (Date) ev.getNewValue().getValue();
				}
				else {
					startdate = (Date) getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.startdate.getUID()).getField().getValue();
				}

				if (ev.getCollectableComponentModel().getEntityField().getUID().equals(E.JOBCONTROLLER.starttime.getUID())) {
					starttime = (String) ev.getNewValue().getValue();
				}
				else {
					starttime = (String) getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.starttime.getUID()).getField().getValue();
				}

				if (ev.getCollectableComponentModel().getEntityField().getUID().equals(E.JOBCONTROLLER.interval.getUID())) {
					interval = (Integer) ev.getNewValue().getValue();
				}
				else {
					interval = (Integer) getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.interval.getUID()).getField().getValue();
				}

				if (ev.getCollectableComponentModel().getEntityField().getUID().equals(E.JOBCONTROLLER.unit.getUID())) {
					unit = org.nuclos.common2.KeyEnum.Utils.findEnum(IntervalUnit.class, (String) ev.getNewValue().getValue());
				}
				else {
					unit = org.nuclos.common2.KeyEnum.Utils.findEnum(IntervalUnit.class,
						(String) getDetailsEditView().getModel().getCollectableComponentModelFor(
								E.JOBCONTROLLER.unit.getUID()).getField().getValue());
				}

				if (startdate != null && starttime != null && interval != null && unit != null) {
	                try {
	                	Calendar c = Calendar.getInstance(getSpringLocaleDelegate().getUserLocaleInfo().toLocale());
	    				c.setTime(startdate);
	    				int iHour = Integer.parseInt(starttime.split(":")[0]);
	    				int iMinute = Integer.parseInt(starttime.split(":")[1]);
	    				c.set(Calendar.HOUR_OF_DAY, iHour);
	    				c.set(Calendar.MINUTE, iMinute);
	    				String cronexpression = JobUtils.getCronExpressionFromInterval(unit, interval, c);
		                getDetailsEditView().getModel().getCollectableComponentModelFor(
		                		E.JOBCONTROLLER.cronexpression.getUID()).setField(new CollectableValueField(cronexpression));
	                }
	                catch(Exception e) {
						LOG.warn("collectableFieldChangedInModel failed: " + e, e);
	                }
				}
			}
		}
	};

	private final CollectableComponentModelListener ccml_usecronexpression = new CollectableComponentModelAdapter() {
		@Override
		public void collectableFieldChangedInModel(CollectableComponentModelEvent ev) {
			boolean cronexpression = ev.getNewValue().isNull() ? false : (Boolean)ev.getNewValue().getValue();
			setCronOrIntervalInputFieldsEnabled(cronexpression);
		}
	};

	private final ActionListener intervalListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			final UID fieldUid = new UID(e.getActionCommand());
			if (E.JOBCONTROLLER.interval.getUID().equals(fieldUid)) {
				setCronSelected(false);
			} else {
				LOG.warn("Unexpected field UID " + fieldUid);
			}
			setCronOrIntervalInputFieldsEnabled(isCronSelected());
		}		
	};

	private final ActionListener cronListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			final UID fieldUid = new UID(e.getActionCommand());
			if (E.JOBCONTROLLER.cronexpression.getUID().equals(fieldUid)) {
				setCronSelected(true);
			} else {
				LOG.warn("Unexpected field UID " + fieldUid);
			}
			setCronOrIntervalInputFieldsEnabled(isCronSelected());
		}		
	};

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton}
	 * to get an instance.
	 *
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public JobControlCollectController(MainFrameTab tabIfAny) {
		super(E.JOBCONTROLLER.getUID(), tabIfAny, null);
		
		this.getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				int iDetailsMode = ev.getNewCollectState().getInnerState();
				final boolean bEnable = (iDetailsMode == CollectState.DETAILSMODE_NEW || iDetailsMode == CollectState.DETAILSMODE_NEW_CHANGED ||
						iDetailsMode == CollectState.DETAILSMODE_EDIT || iDetailsMode == CollectState.DETAILSMODE_MULTIEDIT);
				actSchedule.setEnabled(!bEnable);
				actUnschedule.setEnabled(!bEnable);
				actStartImmediately.setEnabled(!bEnable);

				setResultLastRunInDetails();
				setupRadioButtons();

				for (CollectableComponent clctcomp : getDetailCollectableComponentsFor(E.JOBCONTROLLER.type.getUID())) {
					enableParameterSubForm((String)clctcomp.getField().getValue());
				}

				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.type.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.type.getUID()).addCollectableComponentModelListener(null, ccml_jobtype);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.startdate.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.startdate.getUID()).addCollectableComponentModelListener(null, ccml_cronexpression);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.starttime.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.starttime.getUID()).addCollectableComponentModelListener(null, ccml_cronexpression);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.interval.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.interval.getUID()).addCollectableComponentModelListener(null, ccml_cronexpression);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.unit.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.unit.getUID()).addCollectableComponentModelListener(null, ccml_cronexpression);
				}

				CollectableComponentModel cronExprModel = getDetailsEditView().getModel().getCollectableComponentModelFor(
						E.JOBCONTROLLER.usecronexpression.getUID());
				if (cronExprModel != null) {
					cronExprModel.addCollectableComponentModelListener(null, ccml_usecronexpression);
				}
				setCronOrIntervalInputFieldsEnabled(getDetailsComponentModel(E.JOBCONTROLLER.usecronexpression.getUID()).getField().getValue() != null
						&& (Boolean)getDetailsComponentModel(E.JOBCONTROLLER.usecronexpression.getUID()).getField().getValue());
			}

			
			@Override
			public void detailsModeLeft(CollectStateEvent ev) throws CommonBusinessException {
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.type.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.type.getUID()).removeCollectableComponentModelListener(ccml_jobtype);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.startdate.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.startdate.getUID()).removeCollectableComponentModelListener(ccml_cronexpression);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.starttime.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.starttime.getUID()).removeCollectableComponentModelListener(ccml_cronexpression);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.interval.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.interval.getUID()).removeCollectableComponentModelListener(ccml_cronexpression);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.unit.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.unit.getUID()).removeCollectableComponentModelListener(ccml_cronexpression);
				}
				if (getDetailsEditView().getModel().getCollectableComponentModelFor(E.JOBCONTROLLER.cronexpression.getUID()) != null) {
					getDetailsEditView().getModel().getCollectableComponentModelFor(
							E.JOBCONTROLLER.cronexpression.getUID()).removeCollectableComponentModelListener(ccml_usecronexpression);
				}
			}
		});

		setResultLastRunRendererInResultTable();
        getResultTable().getModel().addTableModelListener( new TableModelListener() {
 			@Override
			public void tableChanged(TableModelEvent e) {
				setResultLastRunRendererInResultTable();
			}
        });
	}
	
	@Override
	public void setupEditPanelForDetailsTab() {
		super.setupEditPanelForDetailsTab();

		pnlJobs = new EventSupportSelectionPanel<EventSupportJobVO>(JobControlCollectController.this, null, false);
		pnlJobs.addEventSupportDataChangeListener(new EventSupportSelectionDataChangeListener() {
			
			@Override
			public void attachedRuleDataChangeListenerEvent() {
				JobControlCollectController.this.detailsChanged(this);
			}
		});		
		pnlJobs.setEventSupportTypeConverter(new EventSupportTypeConverter<EventSupportJobVO>() {
			
			@Override
			public EventSupportJobVO convertData(EventSupportSourceVO esVO) {
				Integer order= pnlJobs.getModel().getRowCount() + 1;
				return new EventSupportJobVO(esVO.getDescription(), esVO.getClassname(),
						JobRule.class.getCanonicalName(), order, 
						getSelectedCollectableId());
			}

			@Override
			public String getRuleTypeAsString() {
				return JobRule.class.getCanonicalName();
			}
		});
		final JPanel pnlRules = (JPanel) UIUtils.findJComponent(this.getDetailsPanel(), "pnlRules");
		if (pnlRules != null) {
			pnlRules.removeAll();
			pnlRules.setLayout(new BorderLayout());
			pnlRules.add(pnlJobs, BorderLayout.CENTER);
		}
	}
	
	@Override
	public CollectableMasterDataWithDependants<UID> findCollectableById(UID sEntity, UID oId) throws CommonBusinessException {
		assert this.getCollectableEntity() != null;

		final MasterDataVO<UID> mdwdcvo = this.mddelegate.get(sEntity, oId);
		final CollectableMasterDataWithDependants<UID> result = new CollectableMasterDataWithDependants<UID>(this.getCollectableEntity(), mdwdcvo);
		assert getSearchStrategy().isCollectableComplete(result);
		return result;
	}

	final SchedulerControlFacadeRemote getSchedulerControlFacadeRemote() {
		if (schedulerControlFacadeRemote == null) {
			schedulerControlFacadeRemote = SpringApplicationContextHolder.getBean(SchedulerControlFacadeRemote.class);
		}
		return schedulerControlFacadeRemote;
	}

	private void setCronOrIntervalInputFieldsEnabled(boolean useCronExpression) {
		for (CollectableComponent c : getDetailCollectableComponentsFor(E.JOBCONTROLLER.interval.getUID())) {
			c.setEnabled(!useCronExpression);
		}
		for (CollectableComponent c : getDetailCollectableComponentsFor(E.JOBCONTROLLER.cronexpression.getUID())) {
			c.setEnabled(useCronExpression);
		}
	}
	
	private boolean isCronSelected() {
		return getDetailsComponentModel(E.JOBCONTROLLER.usecronexpression.getUID()).getField().getValue() != null
				&& (Boolean)getDetailsComponentModel(E.JOBCONTROLLER.usecronexpression.getUID()).getField().getValue();
	}
	
	private void setCronSelected(boolean cronSelected) {
		getDetailsEditView().getModel().getCollectableComponentModelFor(
        		E.JOBCONTROLLER.usecronexpression.getUID()).setField(new CollectableValueField(Boolean.valueOf(cronSelected)));
	}

	protected void setupRadioButtons() {
		if (intervalOrCronGroup == null) {
			intervalOrCronGroup = new ButtonGroup();
			for (CollectableComponent label : getDetailsEditView().getCollectableLabels()) {
				if (label.getFieldUID().equals(E.JOBCONTROLLER.interval.getUID())) {
					radiobuttonInterval = new JRadioButton(label.getEntityField().getLabel());
					radiobuttonInterval.setActionCommand(label.getFieldUID().getString());
					radiobuttonInterval.addActionListener(intervalListener);
					
					Component placeHolder = label.getJComponent();
					if (placeHolder.getParent() != null) {
						Container container = placeHolder.getParent();
						TableLayout layoutManager = (TableLayout)container.getLayout();
						TableLayoutConstraints constraints = layoutManager.getConstraints(placeHolder);

						container.remove(placeHolder);
						container.add(radiobuttonInterval, constraints);
					}
				}
				else if (label.getFieldUID().equals(E.JOBCONTROLLER.cronexpression.getUID())) {
					radiobuttonCron = new JRadioButton(label.getEntityField().getLabel());
					radiobuttonCron.setActionCommand(label.getFieldUID().getString());
					radiobuttonCron.addActionListener(cronListener);
					
					Component placeHolder = label.getJComponent();
					if (placeHolder.getParent() != null) {
						Container container = placeHolder.getParent();
						TableLayout layoutManager = (TableLayout)container.getLayout();
						TableLayoutConstraints constraints = layoutManager.getConstraints(placeHolder);

						container.remove(placeHolder);
						container.add(radiobuttonCron, constraints);
					}
				}
			}
			intervalOrCronGroup.add(radiobuttonInterval);
			intervalOrCronGroup.add(radiobuttonCron);
		}
		radiobuttonInterval.setSelected(!isCronSelected());
		radiobuttonCron.setSelected(isCronSelected());
    }

	@Override
	public CollectableMasterDataWithDependants<UID> insertCollectable(CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		if(clctNew.getId() != null) {
			throw new IllegalArgumentException("clctNew");
		}

		// We have to clear the ids for cloned objects:
		/**
		 * @todo eliminate this workaround - this is the wrong place. The right
		 *       place is the Clone action!
		 */
		final IDependentDataMap mpmdvoDependants = org.nuclos.common.Utils.clearIds(this.getAllSubFormData(null).toDependentDataMap());
		mpmdvoDependants.setData(E.JOBRUN.parent, new ArrayList<EntityObjectVO<Object>>());

		final MasterDataVO<UID> mdvoInserted = delegate.create(new JobVO(clctNew.getMasterDataCVO(), mpmdvoDependants));

		// Add new rules, if there are any
		for (EventSupportJobVO eventSupportsByJobUid : this.pnlJobs.getModel().getEntries()) {
			// These rules are new as the job itself, so we have to set the jobUid
			eventSupportsByJobUid.setJobControllerUID(mdvoInserted.getPrimaryKey());			
			EventSupportDelegate.getInstance().createEventSupportJob(eventSupportsByJobUid);
		}
				
		MasterDataCache.getInstance().invalidate(E.JOBCONTROLLER.getUID());
		EventSupportDelegate.getInstance().invalidateCaches(E.JOBCONTROLLER, E.SERVERCODE);
		return new CollectableMasterDataWithDependants(clctNew.getCollectableEntity(), new MasterDataVO(mdvoInserted, this.readDependants(mdvoInserted.getId())));
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap)oAdditionalData;

		// Don't send JOBRUN to server - they are handle server only. (tp)
		final IDependentDataMap deps = mpclctDependants.toDependentDataMap();
		deps.removeKey(E.JOBRUN.parent);
		final JobVO toSave = new JobVO(clct.getMasterDataCVO(), deps);
		
		// adjust what to save
		if (isCronSelected()) {
			toSave.setInterval(null);
			toSave.setUnit(null);
			toSave.setUseCronExpression(Boolean.TRUE);
		} else {
			toSave.setCronExpression(null);
			toSave.setUseCronExpression(Boolean.FALSE);
		}
		final UID oId = (UID)this.delegate.modify(toSave);

		final MasterDataVO<UID> mdvoUpdated = mddelegate.get(getEntityUid(), oId);
		
		MasterDataCache.getInstance().invalidate(E.JOBCONTROLLER.getUID());
		EventSupportDelegate.getInstance().invalidateCaches(E.JOBCONTROLLER, E.SERVERCODE);
		
		return new CollectableMasterDataWithDependants(clct.getCollectableEntity(), new MasterDataVO(mdvoUpdated, this.readDependants(mdvoUpdated.getId())));
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		this.delegate.remove(new JobVO(clct.getMasterDataCVO(), clct.getDependantMasterDataMap()));
		
		// Remove existing attached rules
		for (EventSupportJobVO eventSupportsByJobUid : EventSupportRepository.getInstance().getEventSupportsByJobUid(getSelectedCollectableId())) {
			EventSupportDelegate.getInstance().deleteEventSupportJob(eventSupportsByJobUid, false);			
		}
				
		EventSupportRepository.getInstance().updateEventSupports();
		MasterDataCache.getInstance().invalidate(E.JOBCONTROLLER.getUID());
		EventSupportDelegate.getInstance().invalidateCaches(E.JOBCONTROLLER, E.SERVERCODE);
	}

	private void attachNewRules() throws CommonBusinessException {
		// First remove existing attached rules
		for (EventSupportJobVO eventSupportsByJobUid : EventSupportRepository.getInstance().getEventSupportsByJobUid(getSelectedCollectableId())) {
			EventSupportDelegate.getInstance().deleteEventSupportJob(eventSupportsByJobUid, false);			
		}
		// reattach new rules
		for (EventSupportJobVO eventSupportsByJobUid :this.pnlJobs.getModel().getEntries()) {
			EventSupportDelegate.getInstance().createEventSupportJob(eventSupportsByJobUid);
		}
		
		EventSupportRepository.getInstance().updateEventSupports();
	}
	
	private void checkNameWithAlreadyScheduledJobs(CollectableMasterDataWithDependants<UID> clct) throws CommonValidationException {
		final IJobKey jk = new JobKeyImpl(E.JOBCONTROLLER.name.getUID(), "DEFAULT" /* Scheduler.DEFAULT_GROUP */);
		// if (getSchedulerControlFacadeRemote().isScheduled((String)clct.getValue(E.JOBCONTROLLER.name.getUID()))) {
		if (getSchedulerControlFacadeRemote().isScheduled(jk)) {
			throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage(					
					"jobcontroller.error.validation.name", clct.getValue(E.JOBCONTROLLER.name.getUID())));
		}
	}

	protected void setupDetailsToolBar() {
		super.setupDetailsToolBar();
		// additional functionality in Details panel:
		//final JToolBar toolbar = UIUtils.createNonFloatableToolBar();

		final JButton btnStart = new JButton(this.actSchedule);
		btnStart.setName("btnStart");
		btnStart.setText(getSpringLocaleDelegate().getMessage("JobControlCollectController.1", "Aktivieren"));
		//toolbar.add(btnStart);
		this.getDetailsPanel().addToolBarComponent(btnStart);

		final JButton btnStop = new JButton(this.actUnschedule);
		btnStop.setName("btnStop");
		btnStop.setText(getSpringLocaleDelegate().getMessage("JobControlCollectController.3", "Deaktivieren"));
		//toolbar.add(btnStop);
		this.getDetailsPanel().addToolBarComponent(btnStop);

		final JButton btnStartNow = new JButton(this.actStartImmediately);
		btnStartNow.setName("btnExecuteNow");
		btnStartNow.setText(getSpringLocaleDelegate().getMessage("JobControlCollectController.2", "Ausf\u00fchren"));
		//toolbar.add(btnStartNow);
		this.getDetailsPanel().addToolBarComponent(btnStartNow);

		//this.getDetailsPanel().setCustomToolBarArea(toolbar);
	}

	private void enableParameterSubForm(String sType) {
		if (sType != null) {
			JobType type = org.nuclos.common2.KeyEnum.Utils.findEnum(JobType.class, sType);
			if (JobType.TIMELEIMIT.equals(type)) {
				this.setSubFormsVisibility(true, false);
			}
			else if (JobType.HEALTHCHECK.equals(type)) {
				this.setSubFormsVisibility(false, true);
			}
		}
		else {
			this.setSubFormsVisibility(true, true);
		}
	}

	private void setSubFormsVisibility(boolean bJobRuleSubForm, boolean bJobDBObjectSubForm) {
		if (getSubForm(E.SERVERCODEJOB) != null) {
			getSubForm(E.SERVERCODEJOB).setVisible(bJobRuleSubForm);
		}
		if (getSubForm(E.JOBDBOBJECT) != null) {
			getSubForm(E.JOBDBOBJECT).setVisible(bJobDBObjectSubForm);
		}
	}

	private SubForm getSubForm(EntityMeta<?> entity) {
		for (SubFormController subformctl : getSubFormControllersInDetails()) {
			if(entity.checkEntityUID(subformctl.getCollectableEntity().getUID()))
				return subformctl.getSubForm();
		}

		return null;
	}

	private void cmdScheduleJob() throws CommonBusinessException {
		if (CollectState.isDetailsModeChangesPending(getCollectStateModel().getDetailsMode())) {
			throw new NuclosUpdateException(getSpringLocaleDelegate().getMessage(
					"JobControlCollectController.4", "Bitte zuerst speichern"));
		}
		else {
			checkNameWithAlreadyScheduledJobs(this.getSelectedCollectable());
			delegate.scheduleJob(this.getSelectedCollectable().getMasterDataCVO().getId());
			UIUtils.runCommand(this.getTab(), new CommonRunnable() {
				@Override
				public void run() throws CommonBusinessException {
					refreshCurrentCollectable();
				}
			});
		}
	}

	private void cmdUnscheduleJob() throws CommonBusinessException {
		delegate.unscheduleJob(this.getSelectedCollectable().getMasterDataCVO().getId());
		UIUtils.runCommand(this.getTab(), new CommonRunnable() {
			@Override
			public void run() throws CommonBusinessException {
				refreshCurrentCollectable();
			}
		});
	}

	private void cmdStartJobImmediately() {
		CommonMultiThreader.getInstance().execute(new CommonClientWorker() {

			@Override
			public void work() throws CommonBusinessException {
				delegate.startJobImmediately(getSelectedCollectable().getMasterDataCVO().getId());
			}

			@Override
			public void paint() throws CommonBusinessException {
				UIUtils.runCommand(getTab(), new CommonRunnable() {
					@Override
					public void run() throws CommonBusinessException {
						try {
							Thread.sleep(3 * 1000); //wait for quartz to start job
						} catch (InterruptedException e) { }
						refreshCurrentCollectable();
					}
				});
			}

			@Override
			public void init() throws CommonBusinessException {
			}

			@Override
			public void handleError(Exception ex) {
				JOptionPane.showMessageDialog(JobControlCollectController.this.getDetailsPanel(), getSpringLocaleDelegate().getMessageFromResource(
						ex.getMessage()), "", JOptionPane.INFORMATION_MESSAGE);
			}

			@Override
			public JComponent getResultsComponent() {
				return getTab();
			}
		});
	}

	/**
	 *
	 */
	public void setResultLastRunInDetails() {
		final CollectableMasterDataWithDependants<UID> cltl = getSelectedCollectable();
		final String resultLastRun = cltl != null ?
				(cltl.getValue(E.JOBCONTROLLER.result.getUID()) != null 
				? (String) cltl.getValue(E.JOBCONTROLLER.result.getUID()) : null) : null;
		for (CollectableComponent clct : getDetailCollectableComponentsFor(E.JOBCONTROLLER.result.getUID())) {
			if (clct instanceof CollectableTextField) {
				final CollectableTextField ctf = (CollectableTextField) clct;
				ctf.getJTextField().setVisible(false);
				ctf.getJLabel().setText("");
				ctf.getJLabel().setIcon(getIconForJobResult(resultLastRun));
				clct.setToolTipText(resultLastRun);
			}
		}
	}

	private void setResultLastRunRendererInResultTable() {
		SwingUtilities.invokeLater( new Runnable() {
			@Override
			public void run() {
				setRendereInResultTable(E.JOBCONTROLLER.result.getUID(), createResultLastRunRenderer());
			}
		});
	}

	private void setRendereInResultTable(UID columnName, TableCellRenderer tableRenderer) {
		TableColumn column = null;
		final int idx_active = getResultTableModel().findColumnByFieldUid(columnName);
		if( idx_active >= 0 ) {
			column = getResultTable().getColumnModel().getColumn(idx_active);
			column.setCellRenderer(tableRenderer);
		}
	}

	private Icon getIconForJobResult(String resultLastRun) {
		Icon result;
		if (StringUtils.looksEmpty(resultLastRun)) {
			result = getIconForJobResult((JobResult) null);
		} else {
			result = getIconForJobResult(JobResult.valueOf(resultLastRun));
		}
		return result;
	}
	
	private Icon getIconForJobResult(JobResult resultLastRun) {
		Icon resultIcon = null;
		if( resultLastRun == null) {
			resultIcon = Icons.getInstance().getIconJobUnknown();
		} else if (resultLastRun == JobResult.INFO) {
			resultIcon = Icons.getInstance().getIconJobSuccessful();
		} else if (resultLastRun == JobResult.ERROR) {
			resultIcon = Icons.getInstance().getIconJobError();
		} else if (resultLastRun == JobResult.WARNING) {
			resultIcon = Icons.getInstance().getIconJobWarning();
		}
		return resultIcon;
	}

	private TableCellRenderer createResultLastRunRenderer() {

		class TrafficLightCellRenderer extends DefaultTableCellRenderer {

			public TrafficLightCellRenderer() {
				super();
			}

			@Override
			public Component getTableCellRendererComponent(JTable table, Object oValue, boolean bSelected, boolean bHasFocus,
					int iRow, int iColumn) {
				
	                final JLabel jLabel = (JLabel)super.getTableCellRendererComponent(table, oValue, bSelected, bHasFocus, iRow, iColumn);
	                final String sValue = (String)((CollectableField)oValue).getValue();
	                Icon icon = null;
	                /*
	                 * could happen when wrong column come through 
	                 * on IllegalArgumentException show component from super
	                 */
	                try {
	                	icon = getIconForJobResult(sValue);
	                }
	                catch(IllegalArgumentException iae) {
	                	return super.getTableCellRendererComponent(table, oValue, bSelected, bHasFocus, iRow, iColumn);
	                }
	                jLabel.setText("");
	                jLabel.setHorizontalAlignment(SwingConstants.CENTER);	                
	                
	                jLabel.setIcon(icon);
	                return jLabel;
			}
		}  // inner class CollectableBooleanTableCellRenderer

		return new TrafficLightCellRenderer();
	}
	
	@Override
    protected boolean isMultiEditAllowed() {
    	return false;
    }

}
