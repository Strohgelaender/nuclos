//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;

public class SystemMetaProvider implements IRigidMetaProvider {
	
	private final SysEntities sysEntities;
	private final Map<UID, EntityMeta<?>> entities = new HashMap<UID, EntityMeta<?>>();
	private final Map<UID, FieldMeta<?>> fields = new HashMap<UID, FieldMeta<?>>();

	public SystemMetaProvider(SysEntities sysEntities) {
		if (sysEntities == null) {
			throw new NuclosFatalException("sysEntities must not be null");
		}
		this.sysEntities = sysEntities;
		for (EntityMeta<?> mdMeta : sysEntities._getAllEntities()) {
			registerEntity(mdMeta);
		}
	}
	
	protected void registerEntity(EntityMeta<?> entityMeta) {
		entities.put(entityMeta.getUID(), entityMeta);
		for (FieldMeta<?> fMeta : entityMeta.getFields()) {
			fields.put(fMeta.getUID(), fMeta);
		}
	}

	@Override
	public Collection<EntityMeta<?>> getAllEntities() {
		Collection<EntityMeta<?>> result = new ArrayList<EntityMeta<?>>(entities.values());
		// HACK: spring is ready (server side)
		SpringApplicationContextHolder.setSpringReady();
		return result;
	}
	
	@Override
	public EntityMeta<?> getEntity(UID entityUID) {
		return entities.get(entityUID);
	}
	
	@Override
	public FieldMeta<?> getEntityField(UID fieldUID) {
		return fields.get(fieldUID);
	}

	@Override
	public Map<UID, FieldMeta<?>> getAllEntityFieldsByEntity(UID entityUID) {
		Map<UID, FieldMeta<?>> fields = new HashMap<UID, FieldMeta<?>>();
		for (FieldMeta<?> fMeta : getEntity(entityUID).getFields()) {
			fields.put(fMeta.getUID(), fMeta);
		}
		return fields;
	}

	@Override
	public EntityMeta<?> getByTablename(String sTableName) {
		for (EntityMeta<?> eMeta :  entities.values()) {
			if (sTableName.equals(eMeta.getDbTable())) {
				return eMeta;
			}
		}
		throw new NullPointerException("Entity for table " + sTableName + " does not exist.");
	}

	@Override
	public boolean isNuclosEntity(UID entityUID) {
		return sysEntities._isNuclosEntity(entityUID);
	}

}
