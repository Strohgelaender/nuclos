//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.query;

import org.nuclos.common.DbField;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;

/**
 * TODO: Is there really any difference between DbSelections and DbColumnExpressions?
 *   What does it mean a column and/or table alias here?
 */
public class DbColumnExpression<T> extends DbExpression<T> {

	private String tableAlias;

	private final DbField<T> column;

	DbColumnExpression(String tableAlias, DbFrom fromTable, DbField<T> column) {
		this(tableAlias, fromTable, column, false, false);
	}

	DbColumnExpression(String tableAlias, DbFrom fromTable, DbField<T> column, boolean caseSensitive) {
		this(tableAlias, fromTable, column, caseSensitive, true);
	}
	
	DbColumnExpression(String tableAlias, DbFrom fromTable, DbField<T> column, boolean caseSensitive, boolean quoteAliasOnNeed) {
		super(fromTable.getQuery().getBuilder(), column.getJavaClass(), mkQualifiedColumnName(tableAlias, column.getDbColumn(), caseSensitive));
		if (fromTable.getAlias() == null) {
			throw new IllegalArgumentException("Table alias in DbFrom must not be null on table " + fromTable.getEntityName());
		}
		this.tableAlias = tableAlias;
		this.column = column;
		alias(getDefaultDbFieldAlias(column, quoteAliasOnNeed));
	}

	DbColumnExpression(String tableAlias, DbFrom fromTable, FieldMeta<T> column, UID langUID, boolean caseSensitive, boolean quoteAliasOnNeed) {
		super(fromTable.getQuery().getBuilder(), column.getJavaClass(), mkQualifiedColumnName(tableAlias, column.getDbColumn(), caseSensitive));
		if (fromTable.getAlias() == null) {
			throw new IllegalArgumentException("Table alias in DbFrom must not be null on table " + fromTable.getEntityName());
		}
		this.tableAlias = tableAlias;
		this.column = column;
		alias(getDefaultDbFieldAlias(column, langUID, quoteAliasOnNeed));
	}
	
	/**
	 * @deprecated Use {@link #mkQualifiedColumnName(String, PreparedStringBuilder, boolean)} below if possible.
	 */
	static final PreparedStringBuilder mkQualifiedColumnName(String tableAlias, String columnName, boolean caseSensitive) {
		final PreparedStringBuilder result;
		if (tableAlias != null) {
			if (caseSensitive) {
				result = PreparedStringBuilder.concat(tableAlias, ".", "\"", columnName, "\"");
			}
			else {
				result = PreparedStringBuilder.concat(tableAlias, ".", columnName);
			}
		}
		else {
			if (caseSensitive) {
				result = PreparedStringBuilder.concat("\"", columnName, "\"");
			}
			else {
				result = PreparedStringBuilder.concat(columnName);
			}
		}
		return result;
	}

	static final PreparedStringBuilder mkQualifiedColumnName(String tableAlias, PreparedStringBuilder columnName, boolean caseSensitive) {
		if (columnName != null && columnName.isFunction()) {
			return columnName;
		}
		final PreparedStringBuilder result;
		if (tableAlias != null) {
			if (caseSensitive) {
				result = PreparedStringBuilder.concat(tableAlias, ".", "\"", columnName, "\"");
			}
			else {
				result = PreparedStringBuilder.concat(tableAlias, ".", columnName);
			}
		}
		else {
			if (caseSensitive) {
				result = PreparedStringBuilder.concat("\"", columnName, "\"");
			}
			else {
				result = PreparedStringBuilder.concat(columnName);
			}
		}
		return result;
	}

	public final DbExpression<T> tableAlias(String tableAlias) {
		if (this.tableAlias != null) throw new IllegalArgumentException(
				"Tried to alter table alias from " + this.tableAlias + " to " + tableAlias);
		this.tableAlias = tableAlias;
		return this;
	}

	public final String getTableAlias() {
		return tableAlias;
	}

	public final DbField<T> getColumn() {
		return column;
	}
	
	public static String getDefaultDbFieldAlias(DbField<?> dbf, boolean quoteAliasOnNeed) {
		String alias = dbf.getDbColumn();
		if ("NAME".equalsIgnoreCase(alias)) {
			return "C_NAME";
		}
		if (quoteAliasOnNeed && RigidUtils.needsToBeQuoted(alias)) {
			alias = "\"" + alias + "\"";
		}
		return alias;
	}

	public static String getDefaultDbFieldAlias(FieldMeta<?> dbf, UID langUID, boolean quoteAliasOnNeed) {
		String alias = dbf.getDbColumn();
		if ("NAME".equalsIgnoreCase(alias)) {
			return "C_NAME";
		}
		if (quoteAliasOnNeed && RigidUtils.needsToBeQuoted(alias)) {
			alias = "\"" + alias + "\"";
		}
		
		alias = langUID.toString() + "_" + alias;
		
		return alias;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("tableAlias=").append(getTableAlias());
		result.append(", Alias=").append(getAlias());
		result.append(", type=").append(getJavaType());
		result.append(", column=").append(column);
		final PreparedStringBuilder psb = getSqlString();
		if (psb != null) {
			result.append(", sql=").append(psb);
			result.append(", frozen=").append(psb.isFrozen());
		}
		result.append("]");
		return result.toString();
	}

}
