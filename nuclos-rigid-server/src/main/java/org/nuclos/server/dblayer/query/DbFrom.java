//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.server.dblayer.MetaDbHelper;

/**
 * @param <PK> Mapped Java type for db primary key column type. 
 */
public class DbFrom<PK> implements Serializable {

	private final static String USERNAME_PLACEHOLDER = "'$username'";
	private final static String USERNAME_PLACEHOLDER_QUOTED = Pattern.quote(USERNAME_PLACEHOLDER);

	private final DbQuery<?> query;
	private final EntityMeta<PK> entity;
	private final boolean forceFromTable;
	private final String selectStatement;
	private String tableAlias;
	private Set<DbJoin<?>> joins;
	private String username;
	
	// private final IMetaProvider mdProv = MetaProvider.getInstance();
	
	DbFrom(DbQuery<?> query, EntityMeta<PK> entity, boolean forceFromTable) {
		if (query == null || entity == null) {
			throw new NullPointerException();
		}
		this.query = query;
		this.entity = entity;
		this.forceFromTable = forceFromTable;
		this.selectStatement = null;
		this.joins = new LinkedHashSet<DbJoin<?>>();
	}

	DbFrom(DbQuery<?> query, String selectStatement) {
		if (query == null || selectStatement == null) {
			throw new NullPointerException();
		}
		this.query = query;
		this.entity = null;
		this.forceFromTable = false;
		this.selectStatement = selectStatement;
		this.joins = new LinkedHashSet<DbJoin<?>>();
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(entity);
		result.append(", selectStatement=").append(selectStatement);
		result.append(", alias=").append(tableAlias);
		result.append(", joins=").append(joins);
		result.append("]");
		return result.toString();
	}

	public String getEntityName() {
		return entity != null ? entity.getEntityName() : null;
	}
	
	public EntityMeta<PK> getEntity() {
		return entity;
	}
	
	public String getFrom() {
		if (selectStatement != null) {
			return "(" + selectStatement + ")";
		}
		if (forceFromTable) {
			return MetaDbHelper.getTableName(entity);
		} else {
			String dbSelect = entity.getDbSelect();
			if (entity.isDynamic() || entity.isDynamicTasklist()) {
				if (username != null && dbSelect.indexOf(USERNAME_PLACEHOLDER) >= 0) {
					dbSelect = dbSelect.replaceAll(USERNAME_PLACEHOLDER_QUOTED, "'" + username + "'");
				}
			}
			return dbSelect;
		}
	}
	
	public String getAlias() {
		return tableAlias;
	}
	
	protected DbFrom<PK> alias(String tableAlias) {
		query.registerAlias(this, tableAlias);
		this.tableAlias = tableAlias;
		return this;
	}
	
	public Set<DbJoin<?>> getJoins() {
		return joins;
	}

	public DbJoin<PK> join(EntityMeta<PK> entity, JoinType joinType, String alias) {
		DbJoin<PK> join = new DbJoin<PK>(query, this, joinType, entity, false);
		join.alias(alias);
		joins.add(join);
		return join;
	}
	
	public DbLanguageJoin<PK> join(EntityMeta<PK> entity, JoinType joinType, String alias, UID language) {
		DbLanguageJoin<PK> join = new DbLanguageJoin<PK>(query, this, joinType, entity, false, language);
		join.alias(alias);
		joins.add(join);
		return join;
	}
	
	public <T> DbJoin<T> join(EntityMeta<?> entity, JoinType joinType, Class<T> joinClass, String alias) {
		DbJoin<T> join = new DbJoin<T>(query, this, joinType, entity, false);
		join.alias(alias);
		joins.add(join);
		return join;
	}
	
	/**
	 * Join 'jEntity' on field 'fieldOfJEntity' == 'base.INTID'.
	 * 
	 */
	public DbJoin<PK> join(EntityMeta<PK> base, JoinType joinType, DbField<PK> fieldOfJEntity, String alias) {
		// ??? This must be new DbJoin<PK>(query, this, joinType, jEntity) (tp) ???
		DbJoin<PK> join = new DbJoin<PK>(query, this, joinType, base, false);
		join.alias(alias);
		joins.add(join);
		join.on(base.getPk(), fieldOfJEntity);
		return join;
	}

	public <T> DbJoin<T> join(String selectStatement, JoinType joinType, String alias) {
		DbJoin<T> join = new DbJoin<T>(query, this, joinType, selectStatement);
		join.alias(alias);
		joins.add(join);
		return join;
	}
	
	/**
	 * Join 'jEntity' on field 'fieldOfJEntity' == 'base.INTID'.
	 */
	public DbJoin<PK> joinOnBasePk(EntityMeta<PK> jEntiy, JoinType joinType, DbField<PK> fieldOfJEntity, String alias) {
		DbJoin<PK> join = new DbJoin<PK>(query, this, joinType, jEntiy, false);
		join.alias(alias);
		joins.add(join);
		join.on(entity.getPk(), fieldOfJEntity);
		return join;
	}
	
	/**
	 * Join 'jEntity' on 'jEntity.INTID' == 'base.fieldOfBase'.
	 */
	public <T> DbJoin<T> joinOnJoinedPk(EntityMeta<T> jEntity, JoinType joinType, DbField<T> fieldOfBase, String alias) {
		DbJoin<T> join = new DbJoin<T>(query, this, joinType, jEntity, false);
		join.alias(alias);
		joins.add(join);
		join.on(fieldOfBase, jEntity.getPk());
		return join;
	}
	
	/**
	 * An alternative to join().alias().on() for usability would be very nice...
	public DbJoin join(EntityMeta joinEntity, JoinType joinType) {
		DbJoin join = new DbJoin(query, this, joinType, joinEntity.getDbEntity());
		joins.add(join);

		final FieldMeta ref = mdProv.getRefField(entity.getEntity(), joinEntity.getEntity());
		String foreignEntityField = ref.getForeignEntityField();
		// TODO: ???
		if (foreignEntityField == null) {
			foreignEntityField = "INTID";
		}
		
		return join.alias(joinEntity.getEntity()).on(foreignEntityField, ref.getField());
	}
	 */
	
	public DbJoin<PK> innerJoin(EntityMeta<PK> entity, String alias) {
		return join(entity, JoinType.INNER, alias);
	}
	
	public <T> DbJoin<T> innerJoin(EntityMeta<?> entity, Class<T> joinClass, String alias) {
		return join(entity, JoinType.INNER, joinClass, alias);
	}
	
	public DbJoin<PK> leftJoin(EntityMeta<PK> entity, String alias) {
		return join(entity, JoinType.LEFT, alias);
	}
	
	public DbLanguageJoin<PK> leftJoin(EntityMeta<PK> entity, String alias, UID language) {
		return join(entity, JoinType.LEFT, alias, language);
	}
	
	public <T> DbJoin<T> leftJoin(EntityMeta<?> entity, Class<T> joinClass, String alias) {
		return join(entity, JoinType.LEFT, joinClass, alias);
	}
	
	/**
	 * Return a column from the <em>base</em> table.
	 * 
	 * @since Nuclos 4.0.00
	 */
	public <T> DbColumnExpression<T> baseColumn(EntityMeta<T> entity) {
		if (!containsAlias(tableAlias)) {
			throw new IllegalArgumentException();
		}
		return new DbColumnExpression<T>(tableAlias, this, entity.getPk());
	}
	
	@Deprecated
	/** @deprecated Only use case-sensitive columns if needed. */
	public <T> DbColumnExpression<T> baseColumnCaseSensitive(EntityMeta<T> entity, boolean isDynmicView) {
		if (!containsAlias(tableAlias)) {
			throw new IllegalArgumentException();
		}
		if(isDynmicView && entity.getPk().getFieldName().equalsIgnoreCase(entity.getPk().getDbColumn())) {
			return new DbColumnExpression<T>(tableAlias, this, entity.getPk(), false);			
		}
		return new DbColumnExpression<T>(tableAlias, this, entity.getPk(), true);
	}
	
	/**
	 * Return the primary key column from the <em>base</em> table.
	 * 
	 * @since Nuclos 4.0.00
	 */
	public DbColumnExpression<PK> basePk() {
		if (!containsAlias(tableAlias)) {
			throw new IllegalArgumentException();
		}
		return new DbColumnExpression<PK>(tableAlias, this, entity.getPk());
	}
	
	/**
	 * Return a column from the <em>base</em> table.
	 * 
	 * @since Nuclos 4.0.00
	 */
	public <T> DbColumnExpression<T> baseColumn(DbField<T> column) {
		if (!containsAlias(tableAlias)) {
			throw new IllegalArgumentException();
		}
		return new DbColumnExpression<T>(tableAlias, this, column);
	}
	
	public List<DbSelection<?>> baseColumns(EntityMeta<?> eMeta) {
		List<DbSelection<?>> result = new ArrayList<DbSelection<?>>();
		result.add(baseColumn(eMeta.getPk()));
		for (FieldMeta<?> fMeta : eMeta.getFields()) {
			result.add(baseColumn(fMeta));
		}
		return result;
	}
	
	public <T> DbColumnExpression<T> baseColumn(FieldMeta<T> column, UID langUID) {
		if (!containsAlias(tableAlias)) {
			throw new IllegalArgumentException();
		}
		return new DbColumnExpression<T>(tableAlias, this, column, langUID, false, false);
	}
	
	/**
	 * Return a column from the <em>base</em> table.
	 * 
	 * @since Nuclos 4.0.19
	 * @author Thomas Pasch
	 */
	public <T> DbColumnExpression<T> baseColumnRef(DbField<T> column, boolean isUid) {
		if (!containsAlias(tableAlias)) {
			throw new IllegalArgumentException();
		}
		final DbField<T> realRef = (DbField<T>) SimpleDbField.createRef(column.getDbColumn(), isUid);
		return new DbColumnExpression<T>(tableAlias, this, realRef);
	}
	
	public <T extends Object> DbQuery<T> getQuery() {
		return (DbQuery<T>) query;
	}
	
	public <T> DbColumnExpression<T> column(String tAlias, FieldMeta<T> fieldMeta) {
		return column(tAlias, (DbField<T>) fieldMeta);
	}
	
	/**
	 * Return a column.
	 * 
	 * @since Nuclos 3.1.01
	 */
	public <T> DbColumnExpression<T> column(String tAlias, DbField<T> column) {
		// This would be too early, as the unparser could add a join... (tp)
		// if (!containsAlias(alias)) throw new IllegalArgumentException("FROM clause " + this + " does not contain alias " + alias);
		return new DbColumnExpression<T>(tAlias, this, column, false);
	}
	
	/**
	 * Return the real reference column.
	 * 
	 * @since Nuclos 4.0.19
	 * @author Thomas Pasch
	 */
	public <T> DbColumnExpression<T> columnRef(String tAlias, DbField<T> column, boolean isUid) {
		final DbField<T> realRef = (DbField<T>) SimpleDbField.createRef(column.getDbColumn(), isUid);
		return new DbColumnExpression<T>(tAlias, this, realRef, false);
	}
	
	public <T> DbColumnExpression<T> columnCaseSensitive(String tableAlias, DbField<T> column) {
		if (!containsAlias(tableAlias)) {
			throw new IllegalArgumentException("No such table alias in FROM clause " + this + ": " + tableAlias);
		}
		return new DbColumnExpression<T>(tableAlias, this, column, true);
	}
	
	public <T> DbColumnExpression<T> baseColumnSensitiveOrInsensitive(DbField<T> column, boolean bCaseSensitive, boolean quoteAliasOnNeed) {
		if (!containsAlias(tableAlias)) {
			throw new IllegalArgumentException("No such table alias in FROM clause " + this + ": " + tableAlias);
		}
		
		return new DbColumnExpression<T>(tableAlias, this, column, bCaseSensitive, quoteAliasOnNeed);
	}

	public static boolean useCaseSensitiveConcerningUdGenericObject(String sql, String dbColumn) {
		if (!"INTID_T_UD_GENERICOBJECT".equalsIgnoreCase(dbColumn)) {
			return true;
		}
		return sql.toUpperCase().contains("\"INTID_T_UD_GENERICOBJECT\"");
	}
	
	public boolean containsAlias(String tAlias) {
		if (tAlias == null) {
			throw new NullPointerException();
		}
		if (tAlias.equals(tableAlias)) {
			return true;
		}
		for (DbJoin<?> j: joins) {
			if (tAlias.equals(j.getAlias())) {
				return true;
			}
		}
		return false;
	}
	
	public DbFrom<?> findFrom(UID en) {
		DbFrom<?> result = null;
		if (entity == null) {
			throw new NullPointerException();
		}
		if (entity.checkEntityUID(en)) {
			return this;
		}
		for (DbJoin<?> j: joins) {
			result = j.findFrom(en);
			if (result != null) {
				if (j instanceof DbLanguageJoin) {
					DbLanguageJoin langJoin = (DbLanguageJoin) j;
					if (langJoin.getAlias().equals("LANG_" + langJoin.getLanguage().toString())) break;
				} else {
					break;
				}
			}
		}
		return result;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}
}
