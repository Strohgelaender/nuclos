//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.impl.standard;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.json.JsonObject;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dbunit.database.DefaultMetadataHandler;
import org.dbunit.database.IMetadataHandler;
import org.nuclos.common.DbField;
import org.nuclos.common.JsonUtils;
import org.nuclos.common.NuclosDateTime;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.NuclosPassword;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.RigidFile;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.ChainedTransformer;
import org.nuclos.common.collection.IdentityTransformer;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.ResultSetMetaDataTransformer;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.XStreamSupport;
import org.nuclos.server.common.valueobject.DocumentFileBase;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbIdent;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.EBatchType;
import org.nuclos.server.dblayer.IBatch;
import org.nuclos.server.dblayer.IPreparedStringExecutor;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.impl.BatchImpl;
import org.nuclos.server.dblayer.impl.BatchOpenStatement;
import org.nuclos.server.dblayer.impl.LogOnlyPreparedStringExecutor;
import org.nuclos.server.dblayer.impl.SQLUtils2;
import org.nuclos.server.dblayer.impl.StandardPreparedStringExecutor;
import org.nuclos.server.dblayer.impl.util.DbTupleImpl;
import org.nuclos.server.dblayer.impl.util.DbTupleImpl.DbTupleElementImpl;
import org.nuclos.server.dblayer.impl.util.PreparedString;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.incubator.DbExecutor.ConnectionRunner;
import org.nuclos.server.dblayer.incubator.DbExecutor.LimitedResultSetRunner;
import org.nuclos.server.dblayer.incubator.DbExecutor.ResultSetRunner;
import org.nuclos.server.dblayer.incubator.SQLInfo;
import org.nuclos.server.dblayer.query.DbDelete;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbOrder;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.query.DbUpdate;
import org.nuclos.server.dblayer.query.IDbCompoundColumnExpression;
import org.nuclos.server.dblayer.statements.AbstractDbStatementVisitor;
import org.nuclos.server.dblayer.statements.DbBatchStatement;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbInsertWithSelectStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbPlainStatement;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.statements.DbStatementVisitor;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.nuclos.server.dblayer.structure.DbArtifact;
import org.nuclos.server.dblayer.structure.DbCallable;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbPrimaryKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbUniqueConstraint;
import org.nuclos.server.dblayer.structure.DbIndex;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbReference;
import org.nuclos.server.dblayer.structure.DbReferenceColumn;
import org.nuclos.server.dblayer.structure.DbSequence;
import org.nuclos.server.dblayer.structure.DbSimpleView;
import org.nuclos.server.dblayer.structure.DbSimpleView.DbSimpleViewColumn;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.structure.DbTableArtifact;
import org.nuclos.server.dblayer.structure.DbTableType;
import org.nuclos.server.dblayer.util.StatementToStringVisitor;

import com.thoughtworks.xstream.XStream;

public abstract class StandardSqlDBAccess extends AbstractDBAccess {

    private static final Logger LOG = Logger.getLogger(StandardSqlDBAccess.class);

    protected StandardSqlDBAccess() {
    }

	/**
	 * @deprecated Use an IBatch for executing structural DB changes.
	 */
	@Override
	public int execute(List<? extends DbBuildableStatement> statements) throws DbException {
		int result = 0;
		DbStatementVisitor<Integer> visitor = createCommandVisitor();
		for (DbBuildableStatement statement : statements) {
			try {
				result += statement.build().accept(visitor);
			} catch (SQLException e) {
				throw wrapSQLException(null, "execute DbBuildableStatement " + statement + " failed: " + e.toString(), e);
			}
		}
		return result;
	}
	
    @Override
    public int executePlainUpdate(final String sql) throws DbException {
	    if (LOG.isDebugEnabled()) {
			LOG.debug("Query to execute:\n\t" + sql);
		}
	    try {
			return executor.executeUpdate(sql);
		} catch (SQLException e) {
			LOG.error("SQL query failed with " + e.toString() + ":\n\t" + sql);
			throw wrapSQLException(null, "executeUpdate(" + sql + ") failed", e);
		}
    }

    @Override
    public <T> T executePlainQuery(String sql, int maxRows, ResultSetRunner<T> runner) throws DbException {
        Map<String, Integer> hints = null;
        if (maxRows != -1) {
            hints = Collections.<String, Integer>singletonMap(DbQuery.MAX_ROWS_HINT, maxRows);        	
        }
        T result = executeQueryImpl(sql, hints, runner);
        return result;
    }

    @Override
    public ResultVO executePlainQueryAsResultVO(final String sql, final int maxRows, final boolean DEPRECATED_2017_ALLOWED) throws DbException {
        return executePlainQuery(sql, maxRows, new LimitedResultSetRunner<ResultVO>() {
            @Override
            public ResultVO perform(ResultSet rs) throws SQLException {
                ResultVO result = new ResultVO();
                ResultSetMetaData metadata = rs.getMetaData();


                Class<?>[] javaTypes = new Class<?>[metadata.getColumnCount()];
                for (int i = 0; i < metadata.getColumnCount(); i++) {
                    ResultColumnVO column = new ResultColumnVO();
                    column.setColumnLabel(metadata.getColumnLabel(i + 1));

                    DbGenericType type = getDbGenericType(metadata.getColumnType(i + 1), metadata.getColumnTypeName(i + 1));
                    if (type != null) {
                    	Class<?> javaType = type.getPreferredJavaType();
                    	// @see NUCLOS-1631 / NUCLOS-1031
                    	// override java type here @todo this is not the right place.
                    	if (type == DbGenericType.NUMERIC) {
                    		if (metadata.getScale(i+1) == 0 && metadata.getPrecision(i+1) != 0) {
                    			// @see NUCLOS-5987
								if (DEPRECATED_2017_ALLOWED) {
									javaType = Integer.class;
								} else {
									javaType = Long.class;
								}
                    			if (getDbType().equals(DbType.ORACLE)) { // @see NUCLOS-2676
                            		if (metadata.getPrecision(i+1) == 1) {
                            			javaType = Boolean.class;
                            		}
                            	}
                    		} else {
                    			javaType = Double.class;                    			
                    		}
                    	}
                        column.setColumnClassName(javaType.getName());
                        javaTypes[i] = javaType;
                    } else {
                        column.setColumnClassName(metadata.getColumnClassName(i + 1));
                        javaTypes[i] = Object.class;
                    }
                    result.addColumn(column);
                }
                
                int counter = 0;
                while (rs.next()) {
               		if (maxRows >= 0 && counter++ >= maxRows) {
            			break;
            		}
     
                    final Object[] values = new Object[javaTypes.length];
                    for (int i = 0; i < values.length; i++) {
                        values[i] = getResultSetValue(rs, i + 1, javaTypes[i]);
                    }
                    result.addRow(values);
                }
                return result;
            }

        });
    }
    
    @Override
    public <T> List<T> executeQuery(DbQuery<? extends T> query) throws DbException {
        return executeQuery(query, IdentityTransformer.<T>id());
    }

    //TODO: Check the complete call history for calls that parse the query without maxResult-Parameter.
    @Override
    public <T, R> List<R> executeQuery(DbQuery<T> query, Transformer<? super T, R> transformer) throws DbException {
    	try {
            StandardQueryBuilder queryBuilder = (StandardQueryBuilder) query.getBuilder();
            PreparedString ps = queryBuilder.getPreparedString(query);
            
            Map<String, Integer> hints = query.getStatementHints();
            ResultSetRunner<List<R>> runner = queryBuilder.createListResultSetRunner(query, transformer);
            return executeQueryImpl(ps.toString(), hints, runner, ps.getParameters());    		
    	} catch (DbException e) {
    		if (wasCanceled(e.getSqlCause())) {
    			String msg;
    	   		if (query.getTimeout() != null && !canceledInitiatorIds.contains(query.getInitiatorId())) {
            		// NUCLOS-5224 1b) Query Timeout has happened
        			msg = "Timeout occured! The execution of the SQL Statement lasted more than " + query.getTimeout() + " seconds!\n"
        					+ "Your query has been cancelled and there is no result. Please change the parameter of your search!";
        		} else {
        			// NUCLOS-5224 2) User closed a tab
        			msg = "Canceling statement due to user request.";
        		}
    			e = new DbException(e.getPk(), msg, Collections.singletonList(e.getMessage()));
    			e.setMaxStackTraces(3);
    		}
    		throw e;
    	}
    }
    
    protected final <T> T executeQueryImpl(final String sql, final Map<String, Integer> hints, final ResultSetRunner<T> runner, final Object...parameters) throws DbException {
        try {
			return executor.execute(new ConnectionRunner<T>() {
			    @Override
			    public T perform(Connection conn) throws SQLException {
			        PreparedStatement stmt = conn.prepareStatement(sql);
		            setOrUnsetStatementHints(stmt, hints, true);
		            
			        boolean resetAutoCommitToTrue = false;
			        try {
			            executor.prepareStatementParameters(stmt, parameters);
			            resetAutoCommitToTrue = prepareDbForSelect(stmt, runner);
			            ResultSet rs = stmt.executeQuery();
			            try {
			            	T result = runner.perform(rs);
					    	return result;
			            } finally {
			                rs.close();
			            }
			        } finally {
			        	unprepareDbForSelect(stmt, resetAutoCommitToTrue);
			            setOrUnsetStatementHints(stmt, hints, false);
			            stmt.close();
			        }
			    }

				@Override
				public SQLInfo getInfo() {
					return new SQLInfo(sql, parameters);
				}
			});
		} catch (SQLException e) {
    		LOG.error("SQL query failed with " + e.toString() + ":\n\t" + sql + "\n\t" + Arrays.asList(parameters));
    		throw wrapSQLException(null, "executeQuery(" + sql + ") failed", e);
		}
    }

    private void setOrUnsetStatementHints(Statement stmt, Map<String, Integer> hints, boolean set) throws SQLException {
        if (hints != null) {
            for (Map.Entry<String, Integer> e : hints.entrySet()) {
            	if (set) {
                    setStatementHint(stmt, e.getKey(), e.getValue());            		
            	} else {
            		unsetStatementHint(stmt, e.getKey(), e.getValue());
            	}
            }
        }
    }

    private Map<Integer, Collection<Statement>> mpStatements = new HashMap<>();
    private Set<Integer> canceledInitiatorIds = new HashSet<>();

    protected boolean setStatementHint(Statement stmt, String hint, Integer value) throws SQLException {
        if (DbQuery.MAX_ROWS_HINT.equals(hint)) {
            stmt.setMaxRows(value);
        } else if (DbQuery.QUERY_TIMEOUT_HINT.equals(hint)) {
            stmt.setQueryTimeout(value);
        } else if (DbQuery.INITIATOR_ID.equals(hint)) {
        	synchronized (mpStatements) {
            	if (!mpStatements.containsKey(value)) {
            		mpStatements.put(value, new HashSet<Statement>());
            	}
            	mpStatements.get(value).add(stmt);				
			}
        	canceledInitiatorIds.remove(value);
        } else {
            return false;        	
        }
        return true;
    }
    
    protected boolean unsetStatementHint(Statement stmt, String hint, Integer id) throws SQLException {
    	if (DbQuery.INITIATOR_ID.equals(hint)) {
    		synchronized (mpStatements) {
            	if (mpStatements.containsKey(id)) {
            		Collection<Statement> stmts = mpStatements.get(id);
            		stmts.remove(stmt);
            		if (stmts.isEmpty()) {
            			mpStatements.remove(id);
            		}
            	}				
			}
        	return true;
        }
    	return false;
    }
    
    @Override
    public void cancelRunningStatements(Integer initiatorId) {
    	if (mpStatements.containsKey(initiatorId)) {
    		canceledInitiatorIds.add(initiatorId);
    		for (Statement stmt : mpStatements.get(initiatorId)) {
    			try {
        			stmt.cancel();    				
    			} catch (SQLException sql) {
    				LOG.warn(sql.getMessage(), sql);
    			}
    		}
    	}
    }

    protected boolean wasCanceled(SQLException e) {
    	return false;
    }

    @Override
    public <T> T executeQuerySingleResult(DbQuery<? extends T> query) throws DbException, DbInvalidResultSizeException {
        List<T> result = executeQuery(query.limit(2L));
        if (result.size() != 1) {
            throw new DbInvalidResultSizeException(null, "Invalid result set size", result.size());
        }
        return result.get(0);
    }
    
    @Override
    public int executeUpdate(DbUpdate update) throws DbException {
    	StandardQueryBuilder queryBuilder = (StandardQueryBuilder) update.getBuilder();
    	PreparedString ps = queryBuilder.getPreparedString(update);
   	 	return executeUpdate(ps.toString(), ps.getParameters());
    }
    
    @SuppressWarnings("unchecked")
	@Override
    protected IBatch getSqlForInsertWithSelect(DbInsertWithSelectStatement insertStmt) {
    	
    	PreparedString source;
    	Object[] paramsSource = null;
    	PreparedString target;
    	
    	if (insertStmt.getColumnConditionMap() == null || 
    			insertStmt.getColumnConditionMap().isEmpty()) {
    		source = new PreparedString(String.format("SELECT %s FROM %s",
    				join(", ", getColumnSourceMap(insertStmt.getColumnMap())),
    				insertStmt.getDbEntitySource()));
    	} else {
    		paramsSource = getColumnValueList(insertStmt.getColumnConditionMap());
    		
    		source = new PreparedString(String.format("SELECT %s FROM %s WHERE %s",
    				join(", ", getColumnSourceMap(insertStmt.getColumnMap())),
    				insertStmt.getDbEntitySource(),
    		        buildWhereString(insertStmt.getColumnConditionMap())), paramsSource);
    	}
    	
    	target = new PreparedString(String.format("INSERT INTO %s (%s) VALUES (%s)",
    			insertStmt.getDbEntity(),
				join(", ", getColumnMap(insertStmt.getColumnMap(), true)),
				join(", ", getColumnMap(insertStmt.getColumnMap(), false))));
    	
        return BatchOpenStatement.simpleBatch(source, target, insertStmt.getColumnMap());
	}
    
    private Object[] getColumnValueList(DbMap fieldMap) {
    	List<Object> result = new ArrayList<Object>();
    	for (DbField<?> field : fieldMap.keySet()) {
    		Object value = fieldMap.get(field);
    		if (value != null && !(value instanceof DbNull) && !(value instanceof DbField)) {
    			result.add(value);
    		}
    	}
    	return result.toArray(new Object[result.size()]);
    }

    private Iterable<?> getColumnSourceMap(DbMap fieldMap) {
    	List<String> result = new ArrayList<String>();
    	for (DbField<?> field : fieldMap.keySet()) {
    		Object value = fieldMap.get(field);
    		if (!(value instanceof DbNull) && value instanceof DbField) {
    			result.add(((DbField<?>) value).getDbColumn());
    		}
    	}
    	return result;
	}
    
    private Iterable<?> getColumnMap(DbMap fieldMap, boolean useAsSelect) {
    	List<String> result = new ArrayList<String>();
    	for (DbField<?> field : fieldMap.keySet()) {
    		if (useAsSelect) {
    			result.add(field.getDbColumn());    			
    		} else {
    			result.add("?");
    		}
    	}
    	return result;
	}
    
	protected int executeUpdate(final String sql, final Object...parameters) throws DbException {
        try {
			return executor.execute(new ConnectionRunner<Integer>() {
			    @Override
			    public Integer perform(Connection conn) throws SQLException {
			        PreparedStatement stmt = conn.prepareStatement(sql);
			        int result = 0;
			        try {
			            executor.prepareStatementParameters(stmt, parameters);
			            result = stmt.executeUpdate();
			        } finally {
			            stmt.close();
			        }
			        return result;
			    }

				@Override
				public SQLInfo getInfo() {
					return new SQLInfo(sql, parameters);
				}
			});
		} catch (SQLException e) {
    		LOG.error("SQL update failed with " + e.toString() + ":\n\t" + sql + "\n\t" + Arrays.asList(parameters));
    		throw wrapSQLException(null, "executeUpdate(" + sql + ") failed", e);
		}
    }
    
    @Override
    public int executeDelete(DbDelete delete) throws DbException {
    	StandardQueryBuilder queryBuilder = (StandardQueryBuilder) delete.getBuilder();
    	PreparedString ps = queryBuilder.getPreparedString(delete);
    	return executeDeleteImpl(ps.toString(), ps.getParameters());
    }
    
    protected Integer getDefaultFetchSize() {
    	return null;
    }
    
    protected boolean autoCommitToFalseBeforeSelect() {
    	return false;
    }
    
    protected final <T> boolean prepareDbForSelect(Statement st, ResultSetRunner<T> runner) throws SQLException {
    	boolean resetAutoCommitToTrue = false;
    	
    	if (autoCommitToFalseBeforeSelect() && st.getConnection().getAutoCommit()) {
    		st.getConnection().setAutoCommit(false);
    		resetAutoCommitToTrue = true;
    	}
    	
    	if (getDefaultFetchSize() != null) {
    		st.setFetchSize(getDefaultFetchSize());
    	}
    	
		return resetAutoCommitToTrue;
    }
    
    protected final void unprepareDbForSelect(Statement st, boolean resetAutoCommitToTrue) {
    	if (resetAutoCommitToTrue) {
    		try {
        		st.getConnection().setAutoCommit(true);
        	} catch (SQLException ex) {
        		LOG.error(ex.getMessage(), ex);
        	}
    	}
    }
    
    private int executeDeleteImpl(final String sql, final Object...parameters) throws DbException {
        try {
			return executor.execute(new ConnectionRunner<Integer>() {
			    @Override
			    public Integer perform(Connection conn) throws SQLException {
			        PreparedStatement stmt = conn.prepareStatement(sql);
			        int result = 0;
			        try {
			            executor.prepareStatementParameters(stmt, parameters);
			            result = stmt.executeUpdate();
			        } finally {
			            stmt.close();
			        }
			        return result;
			    }

				@Override
				public SQLInfo getInfo() {
					return new SQLInfo(sql, parameters);
				}
			});
		} catch (SQLException e) {
    		LOG.error("SQL delete failed with " + e.toString() + ":\n\t" + sql + "\n\t" + Arrays.asList(parameters));
    		throw wrapSQLException(null, "executeDelete(" + sql + ") failed", e);
		}
    }
    
    @Override
    public DalCallResult executeBatch(final IBatch batch, EBatchType type) {
    	return batch.process(getPreparedStringExecutor(), type);
    }

    @Override
    public List<String> getStatementsForLogging(final IBatch batch) {
    	final LogOnlyPreparedStringExecutor ex = new LogOnlyPreparedStringExecutor();
    	// Sometimes (e.g. for creating a virtual entity), there is no SQL to execute. (tp)
    	if (batch != null) {
    		batch.process(ex, EBatchType.FAIL_NEVER);    		
    	}
    	return ex.getStatements();
    }

    @Override
    public <T> T executeFunction(String name, boolean bSilent, Class<T> resultType, Object...args) throws DbException {
        return executeCallableImpl(name, bSilent, resultType, args);
    }

    @Override
    public void executeProcedure(String name, boolean bSilent, Object...args) throws DbException {
        executeCallableImpl(name, bSilent, Void.class, args);
    }

    private <T> T executeCallableImpl(final String name, final boolean bSilent, final Class<T> resultType, final Object...args) throws DbException {
        final boolean hasOut = resultType != Void.class;
        final String call = String.format("{%scall %s(%s)}",
                hasOut ? "? = " : "", name, join(", ", RigidUtils.replicate("?", args.length)));
        try {
			return executor.execute(new ConnectionRunner<T>() {
			    @Override
			    public T perform(Connection conn) throws SQLException {
			        final CallableStatement stmt = conn.prepareCall(call);
			        try {
			            executor.prepareStatementParameters(stmt, hasOut ? 2 : 1, Arrays.asList(args));
			            if (hasOut)
			                stmt.registerOutParameter(1, executor.getPreferredSqlTypeFor(resultType));
			            stmt.execute();
			            if (hasOut) {
			                return getCallableResultValue(stmt, 1, resultType);
			            } else {
			                return null;
			            }
			        } finally {
			            stmt.close();
			        }
			    }
			    
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo(call, args);
				}
			});
		} catch (SQLException e) {
			if (!bSilent)
				LOG.error("executeCallable failed with " + e.toString() + ":\n\t" + call + "\n\t" + Arrays.asList(args));
    		throw wrapSQLException(null, "executeCallable failed", e);
		}
    }

	protected List<String> getColumns(final String tableOrView) {
		try {
			return executor.execute(new ConnectionRunner<List<String>>() {
				@Override
				public List<String> perform(Connection conn) throws SQLException {
					final DatabaseMetaData meta = conn.getMetaData();
					// we NEED  toLowerCase() here, at least for PostgreSQL (tp)
					final ResultSet columns = meta.getColumns(catalog, schema, tableOrView.toLowerCase(), "%");
					final List<String> result = new ArrayList<String>();
					while (columns.next()) {
						result.add(columns.getString("COLUMN_NAME"));
					}
					if (result.size() == 0) {
						throw new IllegalArgumentException("Table " + tableOrView + " with no columns?");
					}
					return result;
				}
				
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA COLUMNS", catalog, schema, tableOrView.toLowerCase());
				}
			});
		} catch (SQLException e) {
			LOG.error("getColumns failed with " + e.toString());
			throw wrapSQLException(null, "getColumns failed", e);
		}
	}

    protected List<DbSimpleViewColumn> ensureNaturalSequence(DbSimpleView view, List<DbSimpleViewColumn> columns) {
    	final int size = columns.size();
    	final List<DbSimpleViewColumn> result = new ArrayList<DbSimpleView.DbSimpleViewColumn>(size);
    	final List<String> natural = getColumns(view.getViewName());

    	COLUMNS:
    	for (String n: natural) {
    		final Iterator<DbSimpleViewColumn> it = columns.iterator();
    		while (it.hasNext()) {
    			final DbSimpleViewColumn c = it.next();
    			if (c.getColumnName().equalsIgnoreCase(n)) {
    				// http://support.novabit.de/browse/NUCLOSINT-1356
    				// it.remove();
    				result.add(c);
    				continue COLUMNS;
    			}
    		}
    		// We only get here if we can't find the column
    		throw new IllegalStateException("Can't find column " + n + " of " + view + " in " + columns);
    	}
    	return result;
    }

	protected boolean existsTableOrView(final String tableOrView) {
		try {
			return executor.execute(new ConnectionRunner<Boolean>() {
				@Override
				public Boolean perform(Connection conn) throws SQLException {
					final DatabaseMetaData meta = conn.getMetaData();
					// we NEED  toLowerCase() here, at least for PostgreSQL (tp)
					final ResultSet columns = meta.getTables(catalog, schema, tableOrView.toLowerCase(), null);
					return Boolean.valueOf(columns.next());
				}
				
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA TABLES", catalog, schema, tableOrView.toLowerCase());
				}
			});
		} catch (SQLException e) {
			LOG.error("existsTableOrView failed with " + e.toString());
			throw wrapSQLException(null, "existsTableOrView failed", e);
		}
	}

    @Override
    public Set<String> getTableNames(final DbTableType tableType) throws DbException {
        try {
			return executor.execute(new ConnectionRunner<Set<String>>() {
			    @Override
			    public Set<String> perform(Connection conn) throws SQLException {
		    		return getMetaDataExtractor().setup(conn, catalog, schema).getNamesOfTablesOrViews(tableType);
			    }
			    
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA TABLENAMES", catalog, schema, tableType);
				}
			});
		} catch (SQLException e) {
    		LOG.error("getTableNames failed with " + e.toString() + ":\n\tcatalog: " + catalog + " schema: " + schema
    				+ " table: " + tableType);
    		throw wrapSQLException(null, "getTableNames failed", e);
		}
    }

    @Override
    public DbTable getTableMetaData(final String tableName) throws DbException {
        try {
			return executor.execute(new ConnectionRunner<DbTable>() {
			    @Override
			    public DbTable perform(Connection conn) throws SQLException {
		    		return getMetaDataExtractor().setup(conn, catalog, schema).getTableMetaData(tableName);
			    }
			    
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA TABLEMETADATA", catalog, schema, tableName);
				}
			});
		} catch (SQLException e) {
    		LOG.error("getTableMetaData failed with " + e.toString() + ":\n\tcatalog: " + catalog + " schema: " + schema
    				+ " table: " + tableName);
    		throw wrapSQLException(null, "getTableMetaData(" + tableName + ") failed", e);
		}
    }

    @Override
    public Set<String> getCallableNames() throws DbException {
        try {
			return executor.execute(new ConnectionRunner<Set<String>>() {
			    @Override
			    public Set<String> perform(Connection conn) throws SQLException {
		    		return getMetaDataExtractor().setup(conn, catalog, schema).getCallableNames();
			    }
			    
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA CALLABLENAMES", catalog, schema);
				}
			});
		} catch (SQLException e) {
    		LOG.error("getCallableNames failed with " + e.toString() + ":\n\tcatalog: " + catalog + " schema: " + schema);
    		throw wrapSQLException(null, "getCallableNames failed", e);
		}
    }

    @Override
    public Collection<DbArtifact> getAllMetaData() throws DbException {
        try {
			return executor.execute(new ConnectionRunner<Collection<DbArtifact>>() {
			    @Override
			    public Collection<DbArtifact> perform(Connection conn) throws SQLException {
		    		return getMetaDataExtractor().setup(conn, catalog, schema).getAllMetaData();
			    }
			    
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA ALLMETADATA", catalog, schema);
				}
			});
		} catch (SQLException e) {
    		LOG.error("getAllMetaData failed with " + e.toString() + ":\n\tcatalog: " + catalog + " schema: " + schema);
			throw wrapSQLException(null, "getAllMetaData fails", e);
		}
    }

    @Override
    public Map<String, Object> getMetaDataInfo() throws DbException {
        try {
			return executor.execute(new ConnectionRunner<Map<String, Object>>() {
			    @Override
			    public Map<String, Object> perform(Connection conn) throws SQLException {
			    	return getMetaDataExtractor().setup(conn, catalog, schema).getMetaDataInfo();
			    }
			    
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA METADATAINFO", catalog, schema);
				}
			});
		} catch (SQLException e) {
    		LOG.error("getMetaDataInfo failed with " + e.toString() + ":\n\tcatalog: " + catalog + " schema: " + schema);
			throw wrapSQLException(null, "getMetaDataInfo fails", e);
		}
    }

    @Override
    public Map<String, String> getDatabaseParameters() throws DbException {
        try {
			return executor.execute(new ConnectionRunner<Map<String, String>>() {
			    @Override
			    public Map<String, String> perform(Connection conn) throws SQLException {
			    	return getMetaDataExtractor().setup(conn, catalog, schema).getDatabaseParameters();
			    }
			    
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo("METADATA DATABASEPARAMETERS", catalog, schema);
				}
			});
		} catch (SQLException e) {
    		LOG.error("getDatabaseParameters failed with " + e.toString() + ":\n\tcatalog: " + catalog + " schema: " + schema);
			throw wrapSQLException(null, "getDatabaseParameters fails", e);
		}
    }

    public static <T> T getResultSetValue(ResultSet rs, int index, Class<T> javaType) throws SQLException {
        Object value;
        if (javaType == String.class) {
            value = rs.getString(index);
        } else if (javaType == Double.class){
            value = rs.getDouble(index);
        } else if (javaType == Long.class){
            value = rs.getLong(index);
        } else if (javaType == Integer.class){
            value = rs.getInt(index);
        } else if (javaType == UID.class){
            value = rs.getString(index);
            if (value != null) {
            	value = new UID((String) value);            	
            }
        } else if (javaType == Boolean.class) {
            value = rs.getBoolean(index);
        } else if (javaType == BigDecimal.class) {
            value = rs.getBigDecimal(index);
        } else if (javaType == java.util.Date.class || javaType == java.sql.Date.class) {
            value = rs.getDate(index);
        } else if (NuclosDateTime.class.isAssignableFrom(javaType)) {
            value = NuclosDateTime.toNuclosDateTime(rs.getTimestamp(index));
        } else if (InternalTimestamp.class.isAssignableFrom(javaType)) {
            value = InternalTimestamp.toInternalTimestamp(rs.getTimestamp(index));
        } else if (DateTime.class == javaType) {
            value = new DateTime(rs.getTimestamp(index));
        } else if (java.sql.Timestamp.class.isAssignableFrom(javaType)) {
        	value = rs.getTimestamp(index);
        } else if (javaType == byte[].class) {
            value = rs.getBytes(index);
        } else if (javaType == Object.class) {
            value = rs.getObject(index);
        } else if (javaType == NuclosScript.class) {
        	String xml = rs.getString(index);
        	if (StringUtils.isEmpty(xml)) {
        		value = null;
        	} else {
				final XStreamSupport xs = XStreamSupport.getInstance();
				try (CloseableXStream closeable = xs.getCloseableStream()) {
					final XStream xstream = closeable.getStream();
					value = xstream.fromXML(rs.getString(index));
				}
			}
        } else if (DocumentFileBase.class.isAssignableFrom(javaType)) {
        	if (rs.getString(index) == null) {
        		value = null;
        	} else {
	        	value = new RigidFile(rs.getString(index), null);
	        	if (javaType != RigidFile.class && javaType != DocumentFileBase.class) {
	        		try {
						value = javaType.getConstructor(RigidFile.class).newInstance(value);
					} catch (Exception e) {
						throw new IllegalArgumentException("Class " + javaType + " not constructable with RigidFile: " + e.getMessage());
					}
	        	}
        	}
        } else if (javaType == ByteArrayCarrier.class) {
			value = new ByteArrayCarrier(rs.getBytes(index));
		} else if (javaType == NuclosImage.class) {
			NuclosImage ni = new NuclosImage("", rs.getBytes(index));
			value = ni;
		} else if (javaType == JsonObject.class) {
			value = JsonUtils.stringToObject(rs.getString(index));
		} else {
            throw new IllegalArgumentException("Class " + javaType + " not supported by readField");
        }
        return rs.wasNull() ? null : javaType.cast(value);
    }

    protected <T> T getCallableResultValue(CallableStatement stmt, int index, Class<T> javaType) throws SQLException {
        Object value;
        if(javaType == String.class) {
            value = stmt.getString(index);
        } else if (javaType == NuclosPassword.class) {
            value = new NuclosPassword(stmt.getString(index));
        } else if(javaType == Double.class){
            value = stmt.getDouble(index);
        } else if(javaType == Long.class){
            value = stmt.getLong(index);
        } else if(javaType == Integer.class){
            value = stmt.getInt(index);
        } 	else if(javaType == UID.class){
        	value = stmt.getString(index);
            if (value != null) {
            	value = new UID((String) value);            	
            }
        } 	else if(javaType == Boolean.class) {
            value = stmt.getBoolean(index);
        } 	else if(javaType == BigDecimal.class) {
            value = stmt.getBigDecimal(index);
        } 	else if(javaType == java.util.Date.class) {
            value = stmt.getDate(index);
        } 	else if(javaType == byte[].class) {
            value = stmt.getBytes(index);
        } 	else if (javaType == NuclosScript.class) {
			final XStreamSupport xs = XStreamSupport.getInstance();
			try (CloseableXStream closeable = xs.getCloseableStream()) {
				final XStream xstream = closeable.getStream();
				value = xstream.fromXML(stmt.getString(index));
			}
        }   else if (javaType == JsonObject.class) {
        	value = JsonUtils.stringToObject(stmt.getString(index));
        }   else {
            throw new IllegalArgumentException("Class " + javaType + " not supported by readField");
        }
        return stmt.wasNull() ? null : javaType.cast(value);
    }

    protected static DbGenericType getDbGenericType(int sqlType, String typeName) {
        switch (sqlType) {
        case Types.VARCHAR:
        case Types.NVARCHAR:
        case Types.NCHAR:
        case Types.CHAR:
            return RigidUtils.equal(typeName, "text") ? DbGenericType.CLOB : DbGenericType.VARCHAR;
        case Types.NUMERIC:
        case Types.DECIMAL:
        case Types.INTEGER:
        case Types.BIGINT:
            return DbGenericType.NUMERIC;
        case Types.BIT:
        case Types.BOOLEAN:
            return DbGenericType.BOOLEAN;
        case Types.DATE:
            return DbGenericType.DATE;
        case Types.BLOB:
        case Types.VARBINARY:
        case Types.BINARY:
        case Types.LONGVARBINARY:
            return DbGenericType.BLOB;
        case Types.CLOB:
        case Types.LONGVARCHAR:
            return DbGenericType.CLOB;
        case Types.NCLOB:
        	return DbGenericType.NCLOB;
        case Types.TIMESTAMP:
            return DbGenericType.DATETIME;
        default:
            return null;
        }
    }

    protected String getName(String name) {
        return makeIdent(name);
    }

    protected String getQualifiedName(String name) {
        return makeIdent(getSchemaName()) + "." + makeIdent(name);
    }

    protected String makeIdent(String name) {
        return name;
    }

    @Override
    protected DbException wrapSQLException(Long id, String message, SQLException ex) {
        return new DbException(id, message, ex);
    }

    protected DbStatementVisitor<Integer> createCommandVisitor() {
        return new StatementVisitor();
    }

    protected IPreparedStringExecutor getPreparedStringExecutor() {
    	return new StandardPreparedStringExecutor(executor);
    }

    protected abstract MetaDataSchemaExtractor getMetaDataExtractor();

    @Override
    public <T> T checkSyntax(final String sql, final ResultSetMetaDataTransformer<T> transformer) {
    	T result = null;
        try {
        	result = executor.execute(new ConnectionRunner<T>() {
			    @Override
			    public T perform(Connection conn) throws SQLException {
			        final Statement stmt = conn.createStatement();
			        try {
			            setStatementHint(stmt, DbQuery.MAX_ROWS_HINT, 1);
			            setStatementHint(stmt, DbQuery.QUERY_TIMEOUT_HINT, 300);
			            if (stmt.execute(sql)) {
			            	if (transformer != null) {
			            		ResultSet rs = stmt.getResultSet();
				            	try {
				            		return transformer.transform(rs.getMetaData());
				            	} finally {
				            		rs.close();
				            	}
			            	}
			            }
			        } finally {
			            stmt.close();
			        }
			        return null;
			    }
			    
				@Override
				public SQLInfo getInfo() {
					return new SQLInfo(sql);
				}
			});
		} catch (SQLException e) {
    		LOG.error("checkSyntax failed with " + e.toString() + ":\n\t" + sql);
			throw wrapSQLException(null, "checkSyntax fails on '" + sql + "'", e);
		}
        return result;
    }

    @SuppressWarnings("serial")
	public static abstract class StandardQueryBuilder extends DbQueryBuilder {

    	private final StandardSqlDBAccess dbAccess;

    	protected StandardQueryBuilder(StandardSqlDBAccess dbAccess) {
    		if (dbAccess == null) {
    			throw new NullPointerException();
    		}
    		this.dbAccess = dbAccess;
    	}

    	@Override
    	public StandardSqlDBAccess getDBAccess() {
    		return dbAccess;
    	}

        protected <T, R> LimitedResultSetRunner<List<R>> createListResultSetRunner(final DbQuery<? extends T> query, Transformer<? super T, R> transformer)
        		throws DbException {
            final List<? extends DbSelection<? extends T>> selections = query.getSelections();
            final DbTupleElementImpl<T>[] elements = new DbTupleElementImpl[selections.size()];
            for (int i = 0; i < selections.size(); i++) {
                DbSelection<? extends T> selection = selections.get(i);
                elements[i] = new DbTupleElementImpl<T>((DbSelection<T>) selection);
            }
            Transformer<Object[], ? extends Object> internalTransformer;
            if (query.getResultType() == Object[].class) {
                internalTransformer = IdentityTransformer.id();
            } else if (query.getResultType() == DbTuple.class) {
                internalTransformer = new Transformer<Object[], DbTuple>() {
                    @Override
                    public DbTuple transform(Object[] values) {
                        return new DbTupleImpl(elements, values);
                    }
                };
            } else if (query.getResultType().isAssignableFrom(elements[0].getJavaType())) {
                internalTransformer = new Transformer<Object[], T>() {
                    @Override
                    public T transform(Object[] values) { return (T) values[0]; };
                };
            } else if (query.getResultType().isAssignableFrom(UID.class) && elements[0].getJavaType() == String.class ) {
                internalTransformer = new Transformer<Object[], T>() {
                    @Override
                    public T transform(Object[] values) { return (T) new UID((String) values[0]); };
                };
            } else {
                throw new DbException("Query does not return result of type " + query.getResultType());
            }
            
            Long maxResult = query.getLimit();
            long lmaxRows = maxResult != null ? maxResult.longValue() : -1L;
            
            if (transformer == IdentityTransformer.id()) {
                return new DefaultResultSetRunner(elements, internalTransformer, lmaxRows);
            } else {
                return new DefaultResultSetRunner<R>(elements, ChainedTransformer.chained(internalTransformer, (Transformer<Object, R>) transformer), lmaxRows);
            }
        }
        
        protected final PreparedString getPreparedString(DbUpdate update) {
    		return buildPreparedString(update).toPreparedStringWithDbMap(update.getValues(), null);
        }
        
        protected void updateHead(PreparedStringBuilder ps, DbUpdate update) {
            ps.append("UPDATE ");
            ps.append(update.getFrom().getDbTable());
            ps.append(" " + update.getAlias());
            ps.append(" SET " + buildUpdateString(update.getValues()));
        }

        @Override
        protected final PreparedStringBuilder buildPreparedString(DbUpdate update) {
            PreparedStringBuilder ps = new PreparedStringBuilder();
            if (update.getFrom() == null) {
                throw new RuntimeException("No from for update" + this);            	
            }
            
            updateHead(ps, update);
            
            if (update.getRestriction() != null) {
                ps.append(" WHERE ").append(getPreparedString(update.getRestriction()));
            }
            
            return ps;
        }
        
        protected final PreparedString getPreparedString(DbDelete delete) {
            return buildPreparedString(delete).toPreparedString(null);
        }
        
        protected void deleteHead(PreparedStringBuilder ps, DbDelete delete) {
            ps.append("DELETE FROM ");
            ps.append(delete.getFrom().getDbTable());
            ps.append(" " + delete.getAlias());       	
        }
        
        @Override
        protected final PreparedStringBuilder buildPreparedString(DbDelete delete) {
            PreparedStringBuilder ps = new PreparedStringBuilder();
            if (delete.getFrom() == null) {
                throw new RuntimeException("No from for delete" + this);            	
            }
            
            deleteHead(ps, delete);
            
            if (delete.getRestriction() != null) {
                ps.append(" WHERE ").append(getPreparedString(delete.getRestriction()));
            }
            return ps;
        }
        
        public final PreparedString getPreparedString(DbQuery<?> query) {
            return buildPreparedString(query).toPreparedString(null);
        }

        @Override
        protected PreparedStringBuilder buildPreparedString(DbQuery<?> query) {
            PreparedStringBuilder ps = new PreparedStringBuilder();
            prepareSelect(ps, query);
            appendSelection(ps, query);
            postprocessSelect(ps, query);
            ps.append(" FROM ");
            appendFrom(ps, query.getRoots());
            if (query.getRestriction() != null) {
                ps.append(" WHERE ").append(getPreparedString(query.getRestriction()));
            }
            if (!query.getGroupList().isEmpty()) {
                String sep = " GROUP BY ";
                for (DbExpression<?> grouping : query.getGroupList()) {
                    ps.append(sep);
                    ps.append(getPreparedString(grouping));
                    sep = ", ";
                }
            }
            if (query.getGroupRestriction() != null) {
                ps.append(" HAVING ").append(getPreparedString(query.getGroupRestriction()));
            }
            prepareOrderBy(ps, query);
            return ps;
        }

		@Override
		protected PreparedStringBuilder buildPreparedSubselect(DbQuery<?> query) {
			PreparedStringBuilder ps = buildPreparedString(query);
			ps.prepend("(");
			ps.append(")");
			return ps;
		}

         protected void prepareOrderBy(PreparedStringBuilder ps, DbQuery<?> query) {
        	if (!query.getOrderList().isEmpty()) {
                String sep = " ORDER BY ";
                for (DbOrder order : query.getOrderList()) {
                    ps.append(sep);
                    DbExpression<?> expression = order.getExpression();
                    boolean bString = expression.getJavaType() == String.class;
                    
                    if (expression instanceof IDbCompoundColumnExpression) {
                    	bString = false;
                    }
                    
                    if (bString) {
                    	ps.append("LOWER(");
                    }    
                    ps.append(getPreparedString(expression));
                    if (bString) {
                    	ps.append(")");
                    }    
                    
                    ps.append(order.isAscending() ? " ASC" : " DESC");
                    sep = ", ";
                }
            }
        }

        protected void prepareSelect(PreparedStringBuilder ps, DbQuery<?> query) {
            ps.append("SELECT ");
            if (query.isDistinct()) {
                ps.append("DISTINCT ");            	
            }
        }

        protected void postprocessSelect(PreparedStringBuilder ps, DbQuery<?> query) {	
        }
        
        protected List<? extends DbSelection<?>> getSqlSelections(DbQuery<?> query) {
            return query.getSelections();
        }
        
        private void appendSelection(PreparedStringBuilder ps, List<? extends DbSelection<?>> selections) {
        	for (Iterator<? extends DbSelection<?>> it = selections.iterator(); it.hasNext();) {
        		final DbSelection<?> s = it.next();
				if (s instanceof DbExpression) {
					ps.append(((DbExpression<?>)s).getSqlColumnExprAsPreparedStringBuilder());
				 } else {
					ps.append(s.getSqlColumnExpr());
				}
        		if (it.hasNext()) {
        			ps.append(", ");
        		}
			}
        }
        
        private void appendSelection(PreparedStringBuilder ps, DbQuery<?> query) {
            appendSelection(ps, getSqlSelections(query));
        }

		protected void appendFrom(PreparedStringBuilder ps, Set<DbFrom<?>> roots) {
            if (roots.isEmpty()) {
                throw new RuntimeException("No root for query" + this);            	
            }

            int index = 0;
            for (DbFrom root : roots) {
                if (index++ > 0) {
                    ps.append(", ");                	
                }
                if (SpringDataBaseHelper.getInstance().getDbAccess().getDbType() != DbType.ORACLE) {
                	ps.append(root.getFrom() + " " + " AS " + root.getAlias());
                } else {
                	ps.append(root.getFrom() + " " + /* Oracle don't like it " AS " + */ root.getAlias());
                }
                for (DbJoin join : getAllJoins(root)) {
                    switch (join.getJoinType()) {
                    case INNER:
                        ps.append(" INNER JOIN ");
                        break;
                    case LEFT:
                        ps.append(" LEFT OUTER JOIN ");
                        break;
                    case RIGHT:
                        ps.append(" RIGHT OUTER JOIN ");
                        break;
                    default:
                        throw new IllegalArgumentException("Join type " + join.getJoinType() + " not supported");
                    }
                    ps.append(join.getFrom() + " " + join.getAlias());
                    ps.append(" ON (");
                    /*
                    ps.append(join.getParent().getAlias() + "." + join.getOn().x);
                    ps.append(" = ");
                    ps.append(join.getAlias() + "." + join.getOn().y);
                    */
                    ps.append(join.getOn().getSqlString());
                    ps.append(")");
                }
            };
        }

        protected Set<DbJoin<?>> getAllJoins(DbFrom<?> root) {
            return collectAllJoinsImpl(root, new LinkedHashSet<DbJoin<?>>());
        }

        private Set<DbJoin<?>> collectAllJoinsImpl(DbFrom<?> parent, Set<DbJoin<?>> set) {
            for (DbJoin<?> join : parent.getJoins()) {
                if (set.add(join)) {
                    collectAllJoinsImpl(join, set);                	
                }
            }
            return set;
        }
    }

    public class StatementVisitor implements DbStatementVisitor<Integer> {

    	private final EBatchType type;

    	public StatementVisitor() {
    		type = EBatchType.FAIL_LATE;
    	}

        @Override
        public Integer visitInsert(final DbInsertStatement<?> insertStmt) throws SQLException {
            return executeBatch(getBatchFor(insertStmt), type).getNumberOfDbChanges();
        }

       
        @Override
        public Integer visitDelete(final DbDeleteStatement<?> deleteStmt) throws SQLException {
            return executeBatch(getBatchFor(deleteStmt), type).getNumberOfDbChanges();
        }

        @Override
        public Integer visitUpdate(final DbUpdateStatement<?> updateStmt) throws SQLException {
            return executeBatch(getBatchFor(updateStmt), type).getNumberOfDbChanges();
        }

        @Override
        public Integer visitStructureChange(DbStructureChange command) throws SQLException {
            int result = -1;
            String message = "Unknown error";
            final IBatch batch;
            try {
            	batch = getBatchFor(command);
            	// sometimes (i.e. for creating virtual entities, there is no SQL to execute, hence...
            	if (batch != null) {
            		result = executeBatch(batch, type).getNumberOfDbChanges();
            	}
        		message = "Success";
            } catch (SQLException e) {
                message = "Error: " + e;
                throw e;
            } finally {
                logStructureChange(command, message);
            }
            return result;
        }

        /**
         * @deprecated
         */
    	private void logStructureChange(DbStructureChange command, String result) {
    		if (structureChangeLogDir == null) {
    			return;    			
    		}
    		try {
    			Date date = new Date();
    			synchronized (DbAccess.class) {
    				if (!structureChangeLogDir.exists()) {
    					structureChangeLogDir.mkdirs();
    				}
    				PrintWriter w = new PrintWriter(new BufferedWriter(
    					new FileWriter(new File(structureChangeLogDir, String.format("dbchanges-%tF.log", date)), true)));
    				try {
    					w.println(String.format("---------- %1$tFT%1$tT ----------------------------", date));
    					w.println("-- " + command.accept(new StatementToStringVisitor()));
    					int i = 0;
    					final IBatch batch = getBatchFor(command);
    					for (String ps : getStatementsForLogging(batch)) {
    						w.println(ps);
    						if (i++ > 0)
    							w.println();
    					}
    					if (result != null)
    						w.println("-- => " + result);
    					w.println();
    					w.flush();
    				} finally {
    					w.close();
    				}
    			}
    		} catch (Exception e) {
    			LOG.debug(e);
    			LOG.error("Exception during structure change logging: " + e);
    		}
    	}

        @Override
        public Integer visitPlain(DbPlainStatement command) {
            try {
				return executeBatch(getBatchFor(command), type).getNumberOfDbChanges();
			} catch (SQLException e) {
				throw wrapSQLException(null, "visitPlain fails on " + command, e);
			}
        }

        @Override
        public Integer visitBatch(DbBatchStatement batch) throws SQLException {
            // TODO: reuse PreparedStatement if two consecutive statements are compatible
            int result = 0;
            final List<SQLException> exceptions = new ArrayList<SQLException>();
            for (DbStatement stmt : batch.getStatements()) {
                try {
                    result += stmt.accept(this);
                } catch (SQLException e) {
                	LOG.error("visitBatch failed on " + stmt, e);
                    if (batch.isFailFirst()) {
                        throw e;
                    } else {
                        exceptions.add(e);
                    }
                }
            }
            if (!exceptions.isEmpty()) {
                throw exceptions.get(0);
            }
            return result;
        }

        @Override
		public Integer visitInsertWithSelect(
				DbInsertWithSelectStatement insertWithSelect)
				throws SQLException {
        	 return executeBatch(getBatchFor(insertWithSelect), type).getNumberOfDbChanges();
		}

    }

    protected String getColumnSpecForAlterTableColumn(DbColumn column, DbColumn oldColumn) {
        return getColumnSpec(column, column.getNullable() != oldColumn.getNullable());
    }

    protected String getColumnSpec(DbColumn column, boolean withNullable) {
        if (withNullable) {
            return String.format("%s %s %s", column.getColumnName(), getDataType(column.getColumnType()), column.getNullable());
        } else {
            return String.format("%s %s", column.getColumnName(), getDataType(column.getColumnType()));
        }
    }

    protected String getColumnSpecNullable(DbColumn column) {
    	return String.format("%s %s %s", column.getColumnName(), getDataType(column.getColumnType()), DbNullable.NULL);
    }

    protected String getUsingIndex(DbConstraint constraint) {
        return "USING INDEX " + getTablespaceSuffix(constraint);
    }

    protected String getTablespaceSuffix(DbArtifact artifact) {
        return "";
    }

    @Override
    protected IBatch getSqlForInsert(DbInsertStatement<?> insertStmt) {
        String sql = String.format("INSERT INTO %s (%s) VALUES (%s)",
            insertStmt.getDbEntity(),
            join(", ", getColumns(insertStmt.getColumnValues().keySet())),
            join(", ", RigidUtils.replicate("?", insertStmt.getColumnValues().size())));
        Object[] params = insertStmt.getColumnValues().values().toArray();
        return BatchImpl.simpleBatch(new PreparedString(sql, params));
    }
    
    protected Iterable<String> getColumns(Iterable<DbField<?>> fields) {
    	List<String> result = new ArrayList<String>();
    	for (DbField<?> dbf : fields) {
    		result.add(dbf.getDbColumn());
    	}
    	return result;
    }
    
    @Override
    public IBatch getSqlForDelete(DbDeleteStatement<?> deleteStmt) {
        String sql = "DELETE FROM " + deleteStmt.getDbEntity();
        if (deleteStmt.getConditions() != null) {
            sql = sql + " WHERE " + buildWhereString(deleteStmt.getConditions());
        }
        Object[] params = deleteStmt.getConditions().values().toArray();
        return BatchImpl.simpleBatch(new PreparedString(sql, params));
    }

    @Override
    public IBatch getSqlForUpdate(DbUpdateStatement<?> updateStmt) {
        return BatchImpl.simpleBatch(getPreparedStringForUpdate(updateStmt));
    }

	protected PreparedString getPreparedStringForUpdate(DbUpdateStatement<?> updateStmt) {
		String sql = String.format("UPDATE %s SET %s WHERE %s", updateStmt.getDbEntity(),
				buildUpdateString(updateStmt.getColumnValues()), buildWhereString(updateStmt.getConditions()));

		Object[] params = RigidUtils.concat(updateStmt.getColumnValues().values(),
				updateStmt.getConditions().values()).toArray();
		return new PreparedString(sql, params);
	}

    private String buildWhereString(Collection<String> names) {
        if (names.isEmpty()) {
            return "1=1";        	
        }
        return join(" AND ", RigidUtils.transform(names, new Transformer<String, String>() {
            @Override
            public String transform(String c) { return c + " = ?"; }
        }));
    }

    private String buildWhereString(final DbMap mpConditions) {
        if (mpConditions.isEmpty()) {
            return "1=1";        	
        }

        return join(" AND ", RigidUtils.transform(new LinkedHashSet<DbField<?>>(mpConditions.keySet()), new Transformer<DbField<?>, String>() {
            @Override
            public String transform(DbField<?> c) {
            	Object value = mpConditions.get(c);
            	if(value == null || value instanceof DbNull<?>) {
            		mpConditions.remove(c);
            		return c.getDbColumn() + " is null ";
            	} else
            		return c.getDbColumn() + " = ?";
            	}
        }));
    }

    protected static String buildUpdateString(final DbMap columnValues) {
        return join(", ", RigidUtils.transform(new LinkedHashSet<Map.Entry<DbField<?>, Object>>(columnValues.entrySet()), 
        		new Transformer<Map.Entry<DbField<?>, Object>, String>() {
            @Override
            public String transform(Map.Entry<DbField<?>, Object> e) {
                String c = e.getKey().getDbColumn();
                if (e.getValue() == null || e.getValue() instanceof DbNull) {
                	columnValues.remove(e.getKey());
                	return c + " = null";
                } else if (e.getValue().getClass() == DbIncrement.class) {
                	columnValues.remove(e.getKey());
                    return c + " = " + c + " + 1";
                }
                return c + " = ?";
            }
        }));
    }

    @Override
    protected IBatch getSqlForCreateTable(DbTable table) {
        List<PreparedString> list = new ArrayList<PreparedString>();
        List<DbColumn> columns = table.getTableArtifacts(DbColumn.class);
		if (!table.isVirtual()) {
			list.add(PreparedString.format("CREATE TABLE %s(\n%s\n) %s", getQualifiedName(table.getTableName()),
					join(",\n", RigidUtils.transform(columns, new Transformer<DbColumn, String>() {
						@Override
						public String transform(DbColumn column) {
							return getColumnSpec(column, true);
						}
					})),
					getTablespaceSuffix(table)));
		}
		final IBatch result = BatchImpl.simpleBatch(list);
        for (DbTableArtifact tableArtifact : table.getTableArtifacts()) {
            if (!columns.contains(tableArtifact)) {
                result.append(getSqlForCreate(tableArtifact));            	
            }
        }
        return result;
    }

    @Override
	protected abstract IBatch getSqlForAlterTableNotNullColumn(DbColumn column);

    @Override
    protected IBatch getSqlForCreateColumn(DbColumn column) throws SQLException {
    	final PreparedString ps = PreparedString.format("ALTER TABLE %s ADD %s",
            getQualifiedName(column.getTable().getName()),
            getColumnSpec(column, true));

    	final IBatch result;
    	if(column.getDefaultValue() != null && column.getNullable().equals(DbNullable.NOT_NULL)) {
    		PreparedString ps2 = PreparedString.format("ALTER TABLE %s ADD %s",
                getQualifiedName(column.getTable().getName()),
                getColumnSpecNullable(column));

    		result = BatchImpl.simpleBatch(ps2);
    		result.append(getSqlForUpdateNotNullColumn(column));
    		result.append(getSqlForAlterTableNotNullColumn(column));
    	}
    	else {
    		result = BatchImpl.simpleBatch(ps);
    	}
    	return result;
    }

	protected IBatch getSqlForUpdateNotNullColumn(final DbColumn column) throws SQLException {
		final DbUpdateStatement<?> stmt = DbStatementUtils.getDbUpdateStatementWhereFieldIsNull(column.getTable().getName(), column, column.getDefaultValue());

		final PreparedString sPlainUpdate = stmt.build().accept(new AbstractDbStatementVisitor<PreparedString>() {
			@Override
			public PreparedString visitUpdate(DbUpdateStatement<?> update) {
				String sUpdate = getPreparedStringForUpdate(stmt).toString();
				for(Object obj : update.getColumnValues().values()) {
					sUpdate = org.apache.commons.lang.StringUtils.replace(sUpdate, "?", "'"+obj.toString()+"'");
				}
				return new PreparedString(sUpdate);
			}
		});
		return BatchImpl.simpleBatch(sPlainUpdate);
	}

	protected IBatch getSqlForUpdateSetNullColumn(final DbColumn column) throws SQLException {
		final DbMap columnValueMap = new DbMap();
		columnValueMap.putNull(column);
		final DbUpdateStatement stmt = new DbUpdateStatement(
				getQualifiedName(column.getTable().getName()),
				columnValueMap, new DbMap());

		final PreparedString sPlainUpdate = stmt.build().accept(new AbstractDbStatementVisitor<PreparedString>() {
			@Override
			public PreparedString visitUpdate(DbUpdateStatement<?> update) {
				String sUpdate = getPreparedStringForUpdate(stmt).toString();
				for (Object obj : update.getColumnValues().values()) {
					sUpdate = org.apache.commons.lang.StringUtils.replace(sUpdate, "?", "'" + obj.toString() + "'");
				}
				return new PreparedString(sUpdate);
			}
		});

		return BatchImpl.simpleBatch(sPlainUpdate);
	}

    @Override
    protected IBatch getSqlForCreatePrimaryKey(DbPrimaryKeyConstraint constraint) {
        return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s ADD CONSTRAINT %s PRIMARY KEY (%s) %s",
            getQualifiedName(constraint.getTable().getName()),
            constraint.getConstraintName(),
            join(",", constraint.getColumnNames()),
            getUsingIndex(constraint)));
    }

    @Override
    protected IBatch getSqlForCreateForeignKey(DbForeignKeyConstraint constraint) {
    	String sql = String.format("ALTER TABLE %s ADD CONSTRAINT %s FOREIGN KEY (%s) REFERENCES %s (%s)",
            getQualifiedName(constraint.getTable().getName()),
            constraint.getConstraintName(),
            join(",", constraint.getColumnNames()),
            getQualifiedName(constraint.getReferencedTable().getName()),
            join(",", constraint.getReferencedColumnNames()));
        if (constraint.isOnDeleteCascade()) {
        	sql += " ON DELETE CASCADE";
        }
        return BatchImpl.simpleBatch(new PreparedString(sql));
    }

    @Override
    protected IBatch getSqlForCreateUniqueConstraint(DbUniqueConstraint constraint) {
        return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s ADD CONSTRAINT %s UNIQUE (%s) %s",
            getQualifiedName(constraint.getTable().getName()),
            constraint.getConstraintName(),
            join(",", constraint.getColumnNames()),
            getUsingIndex(constraint)));
    }

    @Override
    protected abstract IBatch getSqlForCreateIndex(DbIndex index);

    /**
     * @deprecated Views has always been problematic (especially with PostgreSQL).
     * 	Avoid whenever possible.
     */
    @Override
    protected IBatch getSqlForCreateSimpleView(DbSimpleView view) throws DbException {
    	return BatchImpl.simpleBatch(_getSqlForCreateSimpleView("CREATE VIEW", view));
    }

    protected PreparedString _getSqlForCreateSimpleView(String prefix, DbSimpleView view) throws DbException {
    	final TableAliasForSimpleView aliasFactory = new TableAliasForSimpleView();
        final StringBuilder fromClause = new StringBuilder();
        fromClause.append(getName(view.getTable().getName()) + " " + SystemFields.BASE_ALIAS);

        int fkIndex = 1;
        for (DbSimpleViewColumn vc : view.getReferencingViewColumns()) {
        	final String tableAlias = aliasFactory.getTableAlias(vc);
            final DbReference fk = vc.getReference();
            fromClause.append(String.format("\nLEFT OUTER JOIN %s %s ON (%s)",
                getName(fk.getReferencedTable().getName()),
                tableAlias,
				join(" AND ", RigidUtils.transform(fk.getReferences(), new Transformer<Pair<String, String>, String>() {
					@Override
					public String transform(Pair<String, String> p) {
						return String.format("t.%s=%s.%s", p.x, tableAlias, p.y);
					}
				}))));
        }

		final StringBuilder whereClause = new StringBuilder();
        whereClause.append(join(",\n", RigidUtils.transform(view.getViewColumns(), new Transformer<DbSimpleViewColumn, String>() {
			@Override
			public String transform(DbSimpleViewColumn vc) {
				final String cond = vc.getSimpleCondition();
				if (cond == null) {
					return null;
				}
				switch (vc.getViewColumnType()) {
					case TABLE:
						return String.format("t.%s %s", vc.getColumnName(), cond);
					default:
						return null;
				}
			}
        }).parallelStream().filter(Objects::nonNull).collect(Collectors.toList())));
		if (whereClause.length() > 0) {
			whereClause.insert(0, "WHERE\n");
		}

        return PreparedString.format("%s %s(\n%s\n) AS SELECT\n%s\nFROM\n%s\n%s",
        	prefix,
            getQualifiedName(view.getViewName()),
            join(",\n", RigidUtils.transform(view.getViewColumns(), new Transformer<DbSimpleViewColumn, String>() {
				// int fkIndex = 1;
				@Override
				public String transform(DbSimpleViewColumn vc) {
					if (vc.isHidden()) {
						return null;
					}
					return vc.getColumnName();
				}
            }).parallelStream().filter(Objects::nonNull).collect(Collectors.toList())),
            join(",\n", RigidUtils.transform(view.getViewColumns(), new Transformer<DbSimpleViewColumn, String>() {
                // int fkIndex = 1;
                @Override
                public String transform(DbSimpleViewColumn vc) {
                	if (vc.isHidden()) {
                		return null;
					}
                	String sql;
                    switch (vc.getViewColumnType()) {
                    case TABLE:
                    	if (vc.isNull()) {
							return getSqlForCast("null", vc.getColumnType());
						} else {
							// ignore column type (must be the same as the original column)
							return String.format("t.%s", vc.getColumnName());
						}
                    case FOREIGN_REFERENCE:
                        // int fkN = fkIndex++;
                        sql = null;
                        for (Object obj : vc.getViewPattern()) {
                            String sql2;
                            if (obj instanceof DbIdent) {
                                sql2 = String.format("%s.%s", aliasFactory.getTableAlias(vc), ((DbIdent) obj).getName());
                            } else {
                                String s = (String) obj;
                                if (s.isEmpty())
                                    continue;
                                sql2 = "'" + SQLUtils2.escape(s) +"'";
                            }
                            sql = (sql != null) ? getSqlForConcat(sql, sql2) : sql2;
                        }
                        sql = getSqlForCast(sql, vc.getColumnType());
                        return getSqlForNullCheck(String.format("t.%s", RigidUtils.getFirst(vc.getReference().getReferences(), null).x), sql);
                    case FUNCTION:
                        sql = String.format("%s(%s)",
                            getFunctionNameForUseInView(vc.getFunctionName()),
                            join(",", RigidUtils.transform(vc.getArgColumns(),
                                new Transformer<String, String>() {
                                @Override public String transform(String c) { return "t." + c; }
                            })));
                        return getSqlForCast(sql, vc.getColumnType());
                    default:
                        throw new DbException("Unsupported view column type " + vc.getViewColumnType());
                    }
                }
            }).parallelStream().filter(Objects::nonNull).collect(Collectors.toList())),
            fromClause,
			whereClause);
    }

    protected String getFunctionNameForUseInView(String name) {
        return name;
    }

	@Override
	public String getSqlForConcat(String x, String y) {
		// or JDBC escape syntax? String.format("{fn concat(%s,%s)}", x, y);
		return String.format("CONCAT(%s,%s)", x, y);
	}

	@Override
	public String getSqlForConcat(List<String> l) {
		if (l == null || l.isEmpty()) {
			throw new IllegalArgumentException();
		}
		final Iterator<String> it = l.iterator();
		String result = it.next();
		while (it.hasNext()) {
			result = getSqlForConcat(result, it.next());
		}
		return result;
	}

    public String getSqlForCast(String x, DbColumnType type) {
    	return String.format("CAST(%s AS %s)", x, getDataType(type));
    }
    
    public abstract String getSqlForCastAsString(String x, DbColumnType type);

    public String getSqlForNullCheck(String x, String y) {
        return String.format("CASE WHEN %s IS NOT NULL THEN %s END", x, y);
    }

    public String getSqlForSubstituteNull(String expression, String substitute) {
        return String.format("CASE WHEN %s IS NOT NULL THEN %s ELSE %s END", expression, expression, substitute);
    }

    @Override
    protected IBatch getSqlForCreateSequence(DbSequence sequence) {
        return BatchImpl.simpleBatch(PreparedString.format(
            "CREATE SEQUENCE %s INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START WITH %d NO CYCLE",
            getQualifiedName(sequence.getSequenceName()), sequence.getStartWith()));
    }

    @Override
    protected IBatch getSqlForCreateCallable(DbCallable callable) throws DbException {
        String code = callable.getCode();
        if (code == null) {
            throw new DbException("No code for callable " + callable.getCallableName());
        }
        Pattern pattern = Pattern.compile(
            String.format("\\s*(CREATE\\s+(OR\\s+REPLACE\\s+)?)?%s\\s+%s\\s*(?=\\W)",
                callable.getType(), callable.getCallableName()),
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(code);
        boolean success = matcher.lookingAt();
        if (!success) {
            throw new DbException("Cannot interpret header for callable " + callable.getCallableName());
        }
        return BatchImpl.simpleBatch(PreparedString.format("CREATE %s %s %s",
            callable.getType(),
            getQualifiedName(callable.getCallableName()),
            code.substring(matcher.group().length())));
    }

    @Override
    protected abstract IBatch getSqlForAlterTableColumn(DbColumn column1, DbColumn column2) throws SQLException;

	protected IBatch getSqlForAlterTableColumnSetNull(DbColumn column1, DbColumn column2, IBatch result) throws SQLException {
		if (column1 instanceof DbReferenceColumn && column2 instanceof DbReferenceColumn) {
			DbReferenceColumn refcol1 = (DbReferenceColumn) column1;
			DbReferenceColumn refcol2 = (DbReferenceColumn) column2;

			if (refcol1.getReferencedTable() != null && !RigidUtils.equal(refcol1.getReferencedTable(), refcol2.getReferencedTable())) {
				// Statement for clearing the column, in case of change of referenced table (NUCLOS-5755 Nuclet Integrationpoints)
				if (result != null) {
					result.append(getSqlForUpdateSetNullColumn(column1));
				} else {
					result = getSqlForUpdateSetNullColumn(column1);
				}
			}
		}
		return result;
	}

    @Override
    protected IBatch getSqlForAlterSequence(DbSequence sequence1, DbSequence sequence2) {
        long restartWith = Math.max(sequence1.getStartWith(), sequence2.getStartWith());
        
        if(sequence1.getStartWith() !=sequence2.getStartWith())
        return BatchImpl.simpleBatch(PreparedString.format("ALTER SEQUENCE %s RESTART WITH %s",
            getQualifiedName(sequence2.getSequenceName()),
            restartWith));
        else   if(sequence1.isLongSequence() !=sequence2.isLongSequence())
            return BatchImpl.simpleBatch(PreparedString.format(" ALTER SEQUENCE %s  MAXVALUE 9223372036854775807",
                    getQualifiedName(sequence2.getSequenceName()),
                    restartWith));
        throw new NotImplementedException("Use case nicht implementiert"); 
}

    @Override
 	public void resetSequenceToValue(String sequenceName, long value) {
    	DbSequence sequence1 = new DbSequence(null, sequenceName, value);
    	DbSequence sequence2 = new DbSequence(null, sequenceName, value);
    	IBatch batch = getSqlForAlterSequence(sequence1, sequence2);
 		executeBatch(batch, EBatchType.FAIL_LATE);
 	}

    @Override
    protected IBatch getSqlForDropTable(DbTable table) {
    	if (!table.isVirtual()) {
	        return BatchImpl.simpleBatch(PreparedString.format("DROP TABLE %s",
	            getQualifiedName(table.getTableName())));
    	}
    	else {
    		return null;
    	}
    }

    @Override
    protected IBatch getSqlForDropColumn(DbColumn column) {
        return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s DROP COLUMN %s",
            getQualifiedName(column.getTable().getName()),
            column.getColumnName()));
    }

    @Override
    protected IBatch getSqlForDropPrimaryKey(DbPrimaryKeyConstraint constraint) {
        return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s DROP CONSTRAINT %s",
            getQualifiedName(constraint.getTable().getName()),
            constraint.getConstraintName()));
    }

    @Override
    protected IBatch getSqlForDropForeignKey(DbForeignKeyConstraint constraint) {
        return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s DROP CONSTRAINT %s",
            getQualifiedName(constraint.getTable().getName()),
            constraint.getConstraintName()));
    }

    @Override
    protected IBatch getSqlForDropUniqueConstraint(DbUniqueConstraint constraint) {
        return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s DROP CONSTRAINT %s",
            getQualifiedName(constraint.getTable().getName()),
            constraint.getConstraintName()));
    }

    @Override
    protected IBatch getSqlForDropIndex(DbIndex index) {
        return BatchImpl.simpleBatch(PreparedString.format("DROP INDEX %s",
            getQualifiedName(index.getIndexName())));
    }

    @Override
    protected IBatch getSqlForDropSimpleView(DbSimpleView view) {
        return BatchImpl.simpleBatch(PreparedString.format("DROP VIEW %s",
            getQualifiedName(view.getViewName())));
    }

    @Override
    protected IBatch getSqlForDropSequence(DbSequence sequence) {
        return BatchImpl.simpleBatch(PreparedString.format("DROP SEQUENCE %s",
            getQualifiedName(sequence.getSequenceName())));
    }

    @Override
    protected IBatch getSqlForDropCallable(DbCallable callable) {
        return BatchImpl.simpleBatch(PreparedString.format("DROP %s %s",
            callable.getType(),
            getQualifiedName(callable.getCallableName())));
    }
    
    //Most Databases will be able to handle booleans, so just return it the way it is.
    @Override
	public Object getBooleanRepresentationInSQL(Boolean b) {
    	return b;
    }

    public static class DefaultResultSetRunner<T> implements LimitedResultSetRunner<List<T>> {

        DbTupleElementImpl<?>[] elements;
        Transformer<Object[], T> transformer;
        private final long maxResults;

        public DefaultResultSetRunner(DbTupleElementImpl<?>[] elements, Transformer<Object[], T> transformer, long parMaxResults) {
            this.elements = elements;
            this.transformer = transformer;
            this.maxResults = parMaxResults;
        }

        @Override
        public List<T> perform(ResultSet rs) throws SQLException {
            List<T> result = new ArrayList<T>();
        	long counter = 0;
        	while (rs.next()) {
        		if (maxResults >= 0L && counter++ >= maxResults) {
        			break;
        		}
        		result.add(transform(extractRow(rs)));
        	}            	
            return result;
        }

        protected Object[] extractRow(ResultSet rs) throws SQLException {
            int columnCount = elements.length;
            Object[] values = new Object[columnCount];
            for (int i = 0; i < columnCount; i++) {
                values[i] = getResultSetValue(rs, i + 1, elements[i].getJavaType());
            }
            return values;
        }

        protected T transform(Object[] row) {
            return transformer.transform(row);
        }

    }
    
    public static String join(String sep, Iterable<?> iter) {
		  return org.apache.commons.lang.StringUtils.join(iter.iterator(), sep);
	}
    
    public static <T> String join(String sep, T...s) {
		  return org.apache.commons.lang.StringUtils.join(s, sep);
    }
    
    protected final SimpleDateFormat getDateFormat() {
    	try {
    		final Class<?> cls = StandardSqlDBAccess.class.getClassLoader().loadClass("org.nuclos.server.common.ServerLocaleDelegate");
    		final Object delegate = cls.getMethod("getInstance", new Class[0]).invoke(cls, new Object[0]);
    		return (SimpleDateFormat)cls.getMethod("getDateFormat", new Class[0]).invoke(delegate, new Object[0]);
		} catch (Exception e) {
			return (SimpleDateFormat)SimpleDateFormat.getDateInstance();
		}
	}
    
    @Override
    protected IMetadataHandler getMetadataHandler() {
    	return new DefaultMetadataHandler();
    }
    
    @Override
    public String getTableAliasing() {
    	return "AS";
    }
}
