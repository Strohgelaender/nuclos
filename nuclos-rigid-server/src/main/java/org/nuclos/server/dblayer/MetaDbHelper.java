//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import static org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType.DATETIME;
import static org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType.NUMERIC;
import static org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType.VARCHAR;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.dblayer.SchemaHelperVersion;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.structure.DbArtifact;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbConstraint.DbForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbLogicalUniqueConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbPrimaryKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbUniqueConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbUnreferencedForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbIndex;
import org.nuclos.server.dblayer.structure.DbNamedObject;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbReferenceColumn;
import org.nuclos.server.dblayer.structure.DbSimpleView;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.structure.DbTableArtifact;

public class MetaDbHelper {

	private static Map<String, DbColumnType> SYSTEM_COLUMNS = new LinkedHashMap<String, DbColumnType>();
	private static final Map<String, Integer> SYSTEM_COLUMN_ORDER = new LinkedHashMap<String, Integer>();

	public static DbColumnType ID_COLUMN_TYPE = new DbColumnType(NUMERIC, 20, 0);
	public static DbColumnType UID_COLUMN_TYPE = new DbColumnType(VARCHAR, SF.UID_SCALE);
	public static DbColumnType VERSION_COLUMN_TYPE = new DbColumnType(NUMERIC, SF.VERSION_SCALE, 0);

	static {
		SYSTEM_COLUMNS.put(SF.CREATEDAT.getDbColumn(), new DbColumnType(DATETIME));
		SYSTEM_COLUMNS.put(SF.CHANGEDAT.getDbColumn(), new DbColumnType(DATETIME));
		// TODO Warum nur 30 Zeichen? In den Metadaten SF.NAME_SCALE sind es 255.
		// Wenn hier SF.NAME_SCALE eingesetzt wird, so findet keine Aenderung vom Autosetup statt. Sind Systemspalten ausgeklammert?
		// Kann man keine NOT_NULL Spalten aendern wenn bereits Datensaetze existieren? Wird also eine Migration benoetigt?
		SYSTEM_COLUMNS.put(SF.CREATEDBY.getDbColumn(), new DbColumnType(VARCHAR, 30 /*Richtig waere: SF.NAME_SCALE*/));
		SYSTEM_COLUMNS.put(SF.CHANGEDBY.getDbColumn(), new DbColumnType(VARCHAR, 30 /*Richtig waere: SF.NAME_SCALE*/));
		SYSTEM_COLUMNS.put(SF.VERSION.getDbColumn(), VERSION_COLUMN_TYPE);
		for (SF<?> systemField : SF.getAllFields()) {
			SYSTEM_COLUMN_ORDER.put(systemField.getDbColumn(), systemField.getOrder());
		}
	}

	private final MetaDbProvider provider;
	private final PersistentDbAccess dbAccess;
//	private final boolean viewsEnabled;
	private final SchemaHelperVersion v;
	private final Map<UID, Boolean> overwriteNullable = new HashMap<UID, Boolean>();
	
	public MetaDbHelper(SchemaHelperVersion v, IRigidMetaProvider provider,
						Collection<MetaDbEntityWrapper> additionalEntityWrapper,
						Collection<MetaDbFieldWrapper> additionalFieldWrapper) {
		this(v, new MetaDbProvider(provider, additionalEntityWrapper, additionalFieldWrapper));
	}

	public MetaDbHelper(SchemaHelperVersion v, MetaDbProvider provider) {
		this(v, new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess()), provider);
	}
	
	public MetaDbHelper(SchemaHelperVersion v, DbAccess dbAccess, IRigidMetaProvider provider,
						Collection<MetaDbEntityWrapper> additionalEntityWrapper,
						Collection<MetaDbFieldWrapper> additionalFieldWrapper) {
		this(v, new PersistentDbAccess(dbAccess), new MetaDbProvider(provider, additionalEntityWrapper, additionalFieldWrapper));
	}
	
	public MetaDbHelper(SchemaHelperVersion v, PersistentDbAccess dbAccess, IRigidMetaProvider provider,
						Collection<MetaDbEntityWrapper> additionalEntityWrapper,
						Collection<MetaDbFieldWrapper> additionalFieldWrapper) {
		this(v, dbAccess, new MetaDbProvider(provider, additionalEntityWrapper, additionalFieldWrapper));
	}
	
	public MetaDbHelper(SchemaHelperVersion v, DbAccess dbAccess, MetaDbProvider provider) {
		this(v, new PersistentDbAccess(dbAccess), provider);
	}

	public MetaDbHelper(SchemaHelperVersion v, PersistentDbAccess dbAccess, MetaDbProvider provider) {
		this.v = v;
		this.dbAccess = dbAccess;
		this.provider = provider;
//		this.viewsEnabled = viewsEnabled;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("access=").append(dbAccess);
		result.append(", mdProv=").append(provider);
		result.append("]");
		return result.toString();
	}
	
	public Map<String, DbTable> getSchema() {
		return getSchema(null);
	}

	public Map<String, DbTable> getSchema(Predicate<DbArtifact> predicate) {
		Map<String, DbTable> tables = new LinkedHashMap<String, DbTable>();
		for (MetaDbEntityWrapper entityMeta : provider.getAllEntities()) {
			if (!entityMeta.isDatasourceBased() && !entityMeta.isProxy() && !entityMeta.isGeneric() && !entityMeta.isDataLanguageEntity()) {
				DbTable dbTable = getDbTable(entityMeta);
				if (dbTable == null) {
					continue;
				}
				if (predicate != null) {
					if (predicate.evaluate(dbTable)) {
						Iterator<DbTableArtifact> it = dbTable.getTableArtifacts().iterator();
						while (it.hasNext()) {
							DbTableArtifact dba = it.next();
							if (!predicate.evaluate(dba)) {
								it.remove();
							}
						}
						tables.put(dbTable.getTableName(), dbTable);
					}
				} else {
					tables.put(dbTable.getTableName(), dbTable);
				}
			}
		}
		return tables;
	}
	
	public DbTable getDbTable(EntityMeta<?> entityMeta) {
		return getDbTable(new MetaDbEntityWrapper(entityMeta), null);
	}

	public DbTable getDbTable(MetaDbEntityWrapper entityMeta) {
		return getDbTable(entityMeta, null);
	}

	public DbTable getDbTable(EntityMeta<?> entityMeta, Collection<FieldMeta<?>> fieldsMeta) {
		
		Collection<MetaDbFieldWrapper> lstWrappedFieldMeta = new ArrayList();
		
		for (FieldMeta fm : fieldsMeta) {
			lstWrappedFieldMeta.add(new MetaDbFieldWrapper(fm));
		}
		return getDbTable(new MetaDbEntityWrapper(entityMeta), lstWrappedFieldMeta);
	}

	public DbTable getDbTable(MetaDbEntityWrapper entityMeta, Collection<MetaDbFieldWrapper> fieldMetas) {
		if (!entityMeta.isTableMaster() || entityMeta.isProxy() || entityMeta.isGeneric()) {
			return null;
		}

		final ArtifactCollector ei = new ArtifactCollector(entityMeta);
		final List<DbTableArtifact> tableArtifacts;
		final String dbTableName;
		final UID dbTableUID;
		if (entityMeta.isIntegrationPoint()) {
			final DbTableArtifact ipView = getIpView(entityMeta, fieldMetas);
			tableArtifacts = Collections.singletonList(ipView);
			// dummy for map key
			dbTableName = ipView.getArtifactName()+"_"+ei.dbTableName;
			dbTableUID = new UID();
		} else {
			if (fieldMetas == null) {
				fieldMetas = provider.getAllEntityFieldsByEntity(entityMeta.getUID());
			}
			tableArtifacts = ei.collect(fieldMetas);
			dbTableName = ei.dbTableName;
			dbTableUID = entityMeta.getUID();
		}

		boolean bIsVirtual = entityMeta.getVirtualEntity() != null || entityMeta.isGeneric() || entityMeta.isIntegrationPoint();
		return new DbTable(dbTableUID, dbTableName, tableArtifacts, bIsVirtual);
	}

	private DbTableArtifact getIpView(MetaDbEntityWrapper entityMeta, Collection<MetaDbFieldWrapper> fieldMetas) {
		final List<DbSimpleView.DbSimpleViewColumn> viewColumns = new ArrayList<>();
		if (fieldMetas == null) {
			fieldMetas = provider.getAllEntityFieldsByEntity(entityMeta.getUID());
		}
		for (MetaDbFieldWrapper fieldMeta : fieldMetas) {
			final String columnName = fieldMeta.getDbColumn();
			if (fieldMeta.isCalculated()) {
				final DbColumnType columnType = DbUtils.getDbColumnType(fieldMeta);
				String functionName = fieldMeta.getCalcFunction();
				String[] argColumns = new String[]{SF.PK_ID.getDbColumn()};
				DbSimpleView.DbSimpleViewColumn vielcol = new DbSimpleView.DbSimpleViewColumn(
						columnName, columnType, functionName, argColumns
						);
				viewColumns.add(vielcol);
			} else {
				try {
					DbSimpleView.DbSimpleViewColumn viewcol = new DbSimpleView.DbSimpleViewColumn(
							fieldMeta.getDbColumn(), DbUtils.getDbColumnType(fieldMeta)
					);
					viewcol.setAlias(fieldMeta.getAlias());
					viewcol.setIsNull(fieldMeta.isNull());
					viewColumns.add(viewcol);
				} catch (Exception ex) {
					// ignore, not integrated
				}
			}
		}
		DbSimpleView view = new DbSimpleView(
				entityMeta.getUID(),
				new DbNamedObject(entityMeta.getUID(), entityMeta.getDbTable()),
				entityMeta.getAlias(),
				viewColumns);
		return view;
	}

	private class ArtifactCollector {

		private final MetaDbEntityWrapper entityMeta;
		private final UID uid;
		private final String tableName;
		private final String dbTableName;
		private final DbNamedObject dbTableObject;

		private final Map<String, DbColumn> dbColumns = new LinkedHashMap<String, DbColumn>();
		private final Map<UID, DbColumn> dbColumnsByField = new HashMap<UID, DbColumn>();
		private final Set<DbForeignKeyConstraint> fkConstraints = new LinkedHashSet<DbForeignKeyConstraint>();
		private final Set<DbUnreferencedForeignKeyConstraint> ufkConstraints = new LinkedHashSet<DbUnreferencedForeignKeyConstraint>();
		private final Map<String, DbIndex> indexes = new LinkedHashMap<String, DbIndex>();
		private final SortedMap<String, String> simpleUniqueColumns = new TreeMap<String, String>();

		private ArtifactCollector(MetaDbEntityWrapper entityMeta) {
			this.entityMeta = entityMeta;
			this.uid = entityMeta.getUID();
			this.tableName = getTableName(entityMeta);
			this.dbTableName = generateDbName(tableName);
			this.dbTableObject = new DbNamedObject(uid, dbTableName);
			// Dummy placeholder (LinkedHashMap guarantees that the order)
			this.dbColumns.put(entityMeta.isUidEntity()?"STRUID":"INTID", null);
		}

		private List<DbTableArtifact> collect(Collection<MetaDbFieldWrapper> fieldMap) {
			for (MetaDbFieldWrapper fieldMeta : fieldMap) {
				if (!fieldMeta.isColumnMaster() || fieldMeta.getDbColumn() == null || !fieldMeta.isIntegrationComplete()) {
					continue;
				}

				boolean isForeignReference = (fieldMeta.getForeignEntity() != null);

				DbColumn dbColumn = null;
				if (fieldMeta.isCalculated()) {
					//Do nothing
				} else if (isForeignReference) {
					dbColumn = getReferenceDbColumn(fieldMeta);

				} else {
					dbColumn = getNoRefenceDbColumn(fieldMeta);
				}

				if (dbColumn != null) {
					addDbColumn2Index(fieldMeta, dbColumn);
				}
			}

			overWriteColumnsIfNecessary();

			return getTableArtifacts();
		}

		private DbColumn getReferenceDbColumn(MetaDbFieldWrapper fieldMeta) {
			final MetaDbEntityWrapper foreignEntity = provider.getEntity(fieldMeta.getForeignEntity());
			if (foreignEntity == null) {
				throw new IllegalArgumentException("Entity " + entityMeta.getEntityName() + ": Foreign entity " + fieldMeta.getForeignEntity() + " does not exist");
			}

			final String dbForeignTableName = generateDbName(getTableName(foreignEntity));
			final String dbColumnName = generateDbName(DbUtils.getDbIdFieldName(fieldMeta, foreignEntity.isUidEntity()));
			final DbNamedObject referencedTable = new DbNamedObject(null, dbForeignTableName);

			DbReferenceColumn dbColumn = new DbReferenceColumn(fieldMeta.getUID(), dbTableObject, dbColumnName,
					foreignEntity.isUidEntity()?UID_COLUMN_TYPE:ID_COLUMN_TYPE, referencedTable,
					isNullable(fieldMeta), fieldMeta.getDefaultMandatory(), fieldMeta.getOrder());

			final boolean onDeleteCascade = fieldMeta.isOnDeleteCascade();

			// TODO: was XR_<ID> but this does not work consistently for system entities => ...
			final DbForeignKeyConstraint fkConstraint = new DbForeignKeyConstraint(null, dbTableObject,
					generateDbGenericName("XR", uid, tableName, dbColumnName),
					Arrays.asList(dbColumnName),new DbNamedObject(null, dbForeignTableName), null, Arrays.asList(foreignEntity.isUidEntity()?"STRUID":"INTID"), onDeleteCascade);

			if (!fieldMeta.isReadonly() && foreignEntity.getVirtualEntity() == null) {
				fkConstraints.add(fkConstraint);
			}

			return dbColumn;
		}

		private DbColumn getNoRefenceDbColumn(MetaDbFieldWrapper fieldMeta) {
			DbColumnType columnType = createDbColumnType(fieldMeta.getDataType(), fieldMeta.getScale(), fieldMeta.getPrecision(), fieldMeta.isLocalized());

			final String dbColumnName = StringUtils.upperCase(fieldMeta.getDbColumn());
			DbColumn dbColumn = new DbColumn(fieldMeta.getUID(), dbTableObject, dbColumnName,
					columnType, isNullable(fieldMeta), fieldMeta.getDefaultMandatory(), fieldMeta.getOrder());

			final boolean isUnreferencedForeignReference = (fieldMeta.getUnreferencedForeignEntity() != null);
			if (isUnreferencedForeignReference) {
				final MetaDbEntityWrapper unreferencedForeignEntity = provider.getEntity(fieldMeta.getUnreferencedForeignEntity());
				final String dbUnreferencedForeignTableName = generateDbName(getTableName(unreferencedForeignEntity));

				final DbUnreferencedForeignKeyConstraint ufkConstraint = new DbUnreferencedForeignKeyConstraint(null, dbTableObject,
						generateDbGenericName("XUR", uid, tableName, dbColumnName),
						Arrays.asList(dbColumnName), new DbNamedObject(null, dbUnreferencedForeignTableName), null, Arrays.asList(unreferencedForeignEntity.isUidEntity()?"STRUID":"INTID"), false);
				ufkConstraints.add(ufkConstraint);
			}

			return dbColumn;
		}

		private void addDbColumn2Index(MetaDbFieldWrapper fieldMeta, DbColumn dbColumn) {
			if (Boolean.TRUE.equals(fieldMeta.isUnique())) {
				simpleUniqueColumns.put(fieldMeta.getFieldName(), dbColumn.getColumnName());
			}
			if (Boolean.TRUE.equals(fieldMeta.isIndexed())) {
				final DbIndex index = new DbIndex(null, dbTableObject,
						generateDbGenericName("XIE", uid, tableName, dbColumn.getColumnName()),
						Arrays.asList(dbColumn.getColumnName()));
				indexes.put(dbColumn.getColumnName(), index);
			}

			dbColumns.put(dbColumn.getColumnName(), dbColumn);
			dbColumnsByField.put(fieldMeta.getUID(), dbColumn);
		}

		private void overWriteColumnsIfNecessary() {
			// TODO: check that the types matches if a user mapping exists
			for (Map.Entry<String, DbColumnType> e : SYSTEM_COLUMNS.entrySet()) {
				final String dbColumnName = e.getKey();
				dbColumns.put(dbColumnName, new DbColumn(null, dbTableObject, dbColumnName, e.getValue(), DbNullable.NOT_NULL, null,
						SYSTEM_COLUMN_ORDER.get(dbColumnName)));
			}
			if (entityMeta.isUidEntity()) {
				dbColumns.put("STRUID", new DbColumn(null, dbTableObject, "STRUID", UID_COLUMN_TYPE, DbNullable.NOT_NULL, null, -1));
				dbColumns.put(SF.IMPORTVERSION.getDbColumn(), new DbColumn(null, dbTableObject, SF.IMPORTVERSION.getDbColumn(), VERSION_COLUMN_TYPE, DbNullable.NULL, null, SF.IMPORTVERSION.getOrder()));
				dbColumns.put(SF.ORIGINUID.getDbColumn(), new DbColumn(null, dbTableObject, SF.ORIGINUID.getDbColumn(), UID_COLUMN_TYPE, DbNullable.NULL, null, SF.ORIGINUID.getOrder()));
			} else {
				dbColumns.put("INTID", new DbColumn(null, dbTableObject, "INTID", ID_COLUMN_TYPE, DbNullable.NOT_NULL, null, -1));
			}
		}

		private List<DbTableArtifact> getTableArtifacts() {
			List<DbTableArtifact> tableArtifacts = new ArrayList<DbTableArtifact>();

			if (entityMeta.getVirtualEntity() == null) {
				// Columns
				tableArtifacts.addAll(dbColumns.values());
				// Primary Key
				tableArtifacts.add(new DbPrimaryKeyConstraint(null, dbTableObject, generateDbGenericName("PK", uid, tableName), Arrays.asList(entityMeta.isUidEntity()?"STRUID":"INTID")));
				// Foreign Keys
				tableArtifacts.addAll(fkConstraints);
				// Unique Columns
				final List<List<String>> uniqueColumnsList = new ArrayList<List<String>>();
				// - System entities support multiple unique combinations
				final UID[][] uniqueFieldCombinations = entityMeta.getUniqueFieldCombinations();
				if (uniqueFieldCombinations != null) {
					for (UID[] uniqueFields : uniqueFieldCombinations) {
						uniqueColumnsList.add(mapFieldList(uniqueFields, dbColumnsByField));
					}
				}
				// add mandator column to simple unique if enabled / since v4.25 (NUCLOS-6678)
				if (entityMeta.isMandator() && entityMeta.isMandatorUnique() && simpleUniqueColumns.size() > 0) {
					simpleUniqueColumns.put(SF.MANDATOR_UID.getFieldName(), SF.MANDATOR_UID.getDbColumn());
				}
				// - Legacy behaviour: generate one combined unique key for all flagged columns
				if (uniqueColumnsList.isEmpty() && simpleUniqueColumns.size() > 0) {
					uniqueColumnsList.add(new ArrayList<String>(simpleUniqueColumns.values()));
				}
				// - Generate unique constraints (and remove index for the same columns)
				for (List<String> uniqueColumns : uniqueColumnsList) {
					tableArtifacts.add(new DbUniqueConstraint(null, dbTableObject, generateDbGenericName("XAK", uid, tableName, uniqueColumns), uniqueColumns));
					if (uniqueColumns.size() == 1) {
						indexes.remove(uniqueColumns.get(0));
					}
				}

				// Indexes
				tableArtifacts.addAll(indexes.values());
				// - System entities support multiple index combinations
				final UID[][] indexFieldCombinations = entityMeta.getIndexFieldCombinations();
				if (indexFieldCombinations != null) {
					for (UID[] indexFields : indexFieldCombinations) {
						final List<String> indexColumns = mapFieldList(indexFields, dbColumnsByField);
						final String indexName = generateDbGenericName("XIE", uid, tableName, indexColumns);
						DbTableArtifact indexArtifact = new DbIndex(null, dbTableObject, indexName, indexColumns);
						tableArtifacts.add(indexArtifact);
					}
				}
			}

			// Unreferenced Foreign Keys are not materialized in db, but we need them for validation and documentation
			tableArtifacts.addAll(ufkConstraints);

			// Logical Unique Constraints are not materialized in db, but we need them for validation and documentation
			final List<List<String>> logicalUniqueColumnsList = new ArrayList<List<String>>();
			final UID[][] logicalUniqueFieldCombinations = entityMeta.getLogicalUniqueFieldCombinations();
			if (logicalUniqueFieldCombinations != null) {
				for (UID[] uniqueFields : logicalUniqueFieldCombinations) {
					logicalUniqueColumnsList.add(mapFieldList(uniqueFields, dbColumnsByField));
				}
			}
			for (List<String> uniqueColumns : logicalUniqueColumnsList) {
				tableArtifacts.add(new DbLogicalUniqueConstraint(null, dbTableObject, generateDbGenericName("XLO", uid, tableName, uniqueColumns), uniqueColumns));
			}

			return tableArtifacts;
		}

	}

	public static String getDbRefColumn(EntityMeta entity, FieldMeta field) {
		return getDbRefColumn(new MetaDbEntityWrapper(entity), new MetaDbFieldWrapper(field));
	}

	public static String getDbRefColumn(MetaDbEntityWrapper entity, MetaDbFieldWrapper field) {
		if (field.getForeignEntity() == null) {
			throw new IllegalArgumentException();
		}
		return getDbRefColumn(field.getDbColumn(), entity.isUidEntity());
	}
	
	public static String getDbRefColumn(String column, boolean refsToUidEntity) {
		if (column == null) {
			throw new IllegalArgumentException();
		}
		final String dbColumn = column.toUpperCase();
		final String result;
		if (dbColumn.startsWith("STRVALUE_") || dbColumn.startsWith("INTVALUE_") || dbColumn.startsWith("OBJVALUE_")) {
			result = (refsToUidEntity?"STRUID_":"INTID_") + dbColumn.substring(9);
		} else if (dbColumn.startsWith("STRUID_")) {
			result = (refsToUidEntity?"STRUID_":"INTID_") + dbColumn.substring(7);
		} else if (dbColumn.startsWith("INTID_")) {
			result = (refsToUidEntity?"STRUID_":"INTID_") + dbColumn.substring(6);
		}
		else {
			throw new IllegalArgumentException(String.format("getDbRefColumn(\"%s\", %s)", column, refsToUidEntity));
		}
		return result;
	}

	/**
	 * @deprecated Stringified refs are now dereferenced by table joins. Hence the whole method is
	 * 		obsolete. (tp)
	 */
	private static <F extends FieldMeta> List<?> getViewPatternForField(MetaDbFieldWrapper fieldMeta, MetaDbProvider provider2) {
		List<Object> result = new ArrayList<Object>();
		UID refEntityUID = fieldMeta.getForeignEntity() != null ? fieldMeta.getForeignEntity() : fieldMeta.getLookupEntity();

		if (refEntityUID != null) {
			boolean isJoin = !StringUtils.upperCase(fieldMeta.getDbColumn()).startsWith("INTID_") && 
					!StringUtils.upperCase(fieldMeta.getDbColumn()).startsWith("STRUID_");
			if (isJoin) {

				// New:
				UID nameFieldUID = null;
				for (MetaDbFieldWrapper fWrapper : provider2.getAllEntityFieldsByEntity(refEntityUID)) {
					if ("name".equals(fWrapper.getFieldName())) {
						nameFieldUID = fWrapper.getUID();
					}
				}
				for (IFieldUIDRef r: new ForeignEntityFieldUIDParser(fieldMeta.getForeignEntityField(), fieldMeta.getLookupEntityField(), nameFieldUID)) {
					if (r.isUID()) {
						String foreignDbColumn = null;
						try {
							foreignDbColumn = provider2.getEntityField(r.getUID()).getDbColumn();
						} catch (CommonFatalException e) {
							if (DbUtils.isDbIdField(fieldMeta.getDbColumn())) {
								foreignDbColumn = fieldMeta.getDbColumn().toUpperCase().startsWith("INTID")?"INTID":"STRUID)";
							} else {
								throw e;
							}
						}
						result.add(DbIdent.makeIdent(foreignDbColumn));
					}
				}

			} else {
				result.add(DbIdent.makeIdent(fieldMeta.getDbColumn()));
			}
		} else {
			result.add(DbIdent.makeIdent(fieldMeta.getDbColumn()));
		}

		return result;
	}

	private static List<String> mapFieldList(UID[] fields, Map<UID, DbColumn> dbColumnsByField) {
		List<String> list = new ArrayList<String>();
		for (UID field : fields) {
			DbColumn dbColumn = dbColumnsByField.get(field);
			if (dbColumn == null)
				throw new IllegalArgumentException("Field " + field + " does not exist");
			list.add(dbColumn.getColumnName());
		}
		return list;
	}
	
	private DbNullable isNullable(MetaDbFieldWrapper field) {
		boolean nullable = field.isNullable();
		if (isOverwriteNullable(field.getUID())) {
			nullable = overwriteNullable.get(field.getUID());
		}
		return DbNullable.of(nullable);
	}
	
	public void setOverwriteNullable(UID fieldUID, boolean nullable) {
		overwriteNullable.put(fieldUID, nullable);
	}
	
	public void removeOverwriteNullable(UID fieldUID) {
		overwriteNullable.remove(fieldUID);
	}
	
	public boolean isOverwriteNullable(UID fieldUID) {
		return overwriteNullable.containsKey(fieldUID);
	}
	
	private String generateDbName(String name, String...affixes) {
		return dbAccess.generateName(null, name, affixes);
	}

	private String generateDbGenericName(String prefix, UID uid, String name, String...affixes) {
		switch (v) {
			case V_3_X:
				return dbAccess.generateName(prefix+"_", name, affixes);
			default:
				return dbAccess.generateName(prefix+"_", null, addUid(affixes, uid));
		}
	}
	
	private String generateDbGenericName(String prefix, UID uid, String name, List<String> affixes) {
		return generateDbGenericName(prefix, uid, name, affixes.toArray(new String[affixes.size()]));
	}
	
	public static String[] addUid(String[] affixes, UID uid) {
		String[] result = new String[affixes.length+1];
		result[0] = uid.getString();
		for (int i = 0; i < affixes.length; i++) {
			result[i+1] = affixes[i];
		}
		return result;
	}
	
	public static String getTableName(EntityMeta<?> entityMeta) {
		return getTableName(entityMeta.getVirtualEntity(), entityMeta.getDbTable());
	}

	public static String getTableName(MetaDbEntityWrapper entityMeta) {
		return getTableName(entityMeta.getVirtualEntity(), entityMeta.getDbTable());
	}
	
	public static String getTableName(String virtualEntity, String dbTable) {
		if (!RigidUtils.looksEmpty(virtualEntity)) {
			return virtualEntity;
		}
		String tableName = StringUtils.upperCase(dbTable);
		if (tableName.startsWith("V_")) {
			tableName = "T_" + tableName.substring(2);
		}
		else if (tableName.startsWith("T_")){
			// do nothing
		}
		return tableName;
	}

	/**
	 * @deprecated Consider that the auto-generated views of nuclos are deprecated.
	 * 		Hence there should be no need to use this method. (tp)
	 */
	public static String getViewName(MetaDbEntityWrapper entityMeta) {
		return getViewName(entityMeta.getDbTable());
	}
	
	/**
	 * @deprecated Consider that the auto-generated views of nuclos are deprecated.
	 * 		Hence there should be no need to use this method. (tp)
	 */
	public static String getViewName(EntityMeta<?> entityMeta) {
		return getViewName(entityMeta.getDbTable());
	}
	
	/**
	 * @deprecated Consider that the auto-generated views of nuclos are deprecated.
	 * 		Hence there should be no need to use this method. (tp)
	 */
	public static String getViewName(String dbTable) {
		String tableName = StringUtils.upperCase(dbTable);
		if (tableName.startsWith("V_")) {
			return tableName;
		}
		else if (tableName.startsWith("T_")) {
			return "V_" + tableName.substring(2);
		}
		return null;
	}

	public static DbColumnType createDbColumnType(FieldMeta fieldMeta) {
		return createDbColumnType(fieldMeta.getDataType(), fieldMeta.getScale(), fieldMeta.getPrecision(), fieldMeta.isLocalized());
	}

	public static DbColumnType createDbLangColumnType(FieldMeta fieldMeta) {
		return createDbColumnType(fieldMeta.getDataType(), fieldMeta.getScale(), fieldMeta.getPrecision(), true);
	}
	
	private static DbColumnType createDbColumnType(String javaClass, Integer oldScale, Integer oldPrecision, boolean IsLocalized) {
		try {
			return DbUtils.getDbColumnType(Class.forName(javaClass), oldScale, oldPrecision, IsLocalized);
		} catch(ClassNotFoundException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	public List<DbIndex> getIndexes(EntityMeta<?> entityMeta) {
		final List<DbIndex> result = new ArrayList<DbIndex>();
		final String tableName = getTableName(entityMeta);
		final String dbTableName = generateDbName(tableName);
		final DbNamedObject dbTableObject = new DbNamedObject(entityMeta.getUID(), dbTableName);
		
		for (FieldMeta<?> fieldMeta : entityMeta.getFields()) {
			if (!fieldMeta.isColumnMaster() || !fieldMeta.isIndexed() || fieldMeta.getForeignEntity() == null) {
				continue;
			}
			
			final MetaDbEntityWrapper foreignEntity = provider.getEntity(fieldMeta.getForeignEntity());
			if (foreignEntity == null) {
				throw new IllegalArgumentException("Entity " + entityMeta.getEntityName() + ": Foreign entity " + fieldMeta.getForeignEntity() + " does not exist");
			}
			
			final String dbColumnName = generateDbName(DbUtils.getDbIdFieldName(fieldMeta, foreignEntity.isUidEntity()));
			final DbColumn dbColumn = new DbColumn(fieldMeta.getUID(), dbTableObject, dbColumnName,
					foreignEntity.isUidEntity()?UID_COLUMN_TYPE:ID_COLUMN_TYPE,
				isNullable(new MetaDbFieldWrapper(fieldMeta)), fieldMeta.getDefaultMandatory(), fieldMeta.getOrder());		
			final DbIndex index = new DbIndex(null, dbTableObject,
					generateDbGenericName("XIE", entityMeta.getUID(), tableName, dbColumn.getColumnName()),
				Arrays.asList(dbColumn.getColumnName()));
			result.add(index);
		}
		return result;
	}
	
}
