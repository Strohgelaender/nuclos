//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.query;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.impl.SQLUtils2;
import org.nuclos.server.dblayer.impl.standard.StandardSqlDBAccess;
import org.nuclos.server.dblayer.impl.util.PreparedString;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder.Parameter;
import org.nuclos.server.dblayer.statements.DbMap;


public abstract class DbQueryBuilder implements Serializable {

	public static final String DATE_PATTERN_GERMAN = "dd.MM.yyyy";
	public static final String DATE_PATTERN_ENGLISH = "dd/MM/yyyy";
	public static final String DATE_PATTERN_USA1 = "MM/dd/yyyy";
	public static final String DATE_PATTERN_USA2 = "MM-dd-yyyy";
	public static final String DATE_PATTERN_OTHER = "dd-MM-yyyy";
	
	protected DbQueryBuilder() {
	}
	
	// 
	// Delete 
	//
	
	public DbDelete createDelete(EntityMeta<?> from) {
		return new DbDelete(this, from);
	}
	
	public DbUpdate createUpdate(EntityMeta<?> from, DbMap values) {
		return new DbUpdate(this, from, values);
	}

	//
	// Query
	//

	public <T> DbQuery<T> createQuery(Class<T> javaType, boolean bSortCalculatedAttributes) {
		return new DbQuery<T>(this, javaType, bSortCalculatedAttributes);
	}
	
	public <T> DbQuery<T> createQuery(Class<T> javaType) {
		return createQuery(javaType, true);
	}

	public DbQuery<DbTuple> createTupleQuery() {
		return createQuery(DbTuple.class);
	}

	//
	// Expressions
	//

	public <T> DbExpression<T> literal(T value) {
		Class<? extends T> javaType = (Class<? extends T>) value.getClass();
		return buildExpressionSql(javaType, literalImpl(value));
	}

	protected PreparedStringBuilder literalImpl(Object value) {
		Parameter param = new PreparedStringBuilder.Parameter().bind(value);
		return new PreparedStringBuilder().append(param);
	}

	public <T> DbExpression<T> nullLiteral(Class<T> javaType) {
		Parameter param = new PreparedStringBuilder.Parameter().bind(DbNull.forType(javaType));
		return buildExpressionSql(javaType, param);
	}

	public DbExpression<String> upper(DbExpression<String> x) {
		return buildExpressionSql(String.class, "UPPER(", x, ")");
	}
	
	public DbExpression<String> lower(DbExpression<String> x) {
		return buildExpressionSql(String.class, "LOWER(", x, ")");
	}
	
	public DbExpression<Date> truncDate(DbExpression<Date> x) {
		return x;
	}

	public abstract DbExpression<java.util.Date> currentDate();
	
	public abstract StandardSqlDBAccess getDBAccess();

	public DbExpression<String> convertDateToString(DbExpression<java.util.Date> x, String pattern) {
		return buildExpressionSql(String.class, "TO_CHAR(", x, ", '", SQLUtils2.escape(pattern), "')");
	}

	public DbExpression<java.util.Date> convertInternalTimestampToDate(DbExpression<InternalTimestamp> x) {
		return buildExpressionSql(Date.class, "CAST(", x, " AS DATE)");
	}

	public <T> DbExpression<T> subSelect(DbQuery<T> subQuery) {
		return buildExpressionSql(subQuery.getResultType(), buildPreparedSubselect(subQuery));
	}

	@Deprecated
	public <T> DbExpression<T> plainExpression(Class<T> javaType, String sql) {
		return buildExpressionSql(javaType, sql);
	}

	//
	// Count
	//

	public DbExpression<Long> count(DbExpression<?> x) {
		return buildExpressionSql(Long.class, "COUNT (", x, ")");
	}

	public DbExpression<Long> countDistinct(DbExpression<?> x) {
		return buildExpressionSql(Long.class, "COUNT (DISTINCT ", x, ")");
	}

	public DbExpression<Long> countRows() {
		return buildExpressionSql(Long.class, "COUNT (*)");
	}

	//
	// Aggregates
	//

	public <T> DbExpression<T> min(DbExpression<T> x) {
		return buildExpressionSql(x.getJavaType(), "MIN (", x, ")");
	}

	public <T> DbExpression<T> max(DbExpression<T> x) {
		return buildExpressionSql(x.getJavaType(), "MAX (", x, ")");
	}

	public <T> DbExpression<T> avg(DbExpression<T> x) {
		return buildExpressionSql(x.getJavaType(), "AVG (", x, ")");
	}

	public <T> DbExpression<T> sum(DbExpression<T> x) {
		return buildExpressionSql(x.getJavaType(), "SUM (", x, ")");
	}

	//
	// Conditions
	//

	public <T> DbCondition equal(DbExpression<? extends T> x, DbExpression<? extends T> y) {
		return buildConditionSql(x, " = ", y);
	}

	public <T> DbCondition equalValue(DbSelection<T> x, T y) {
		if (y == null) {
			// Note that "x = NULL" (which *is always false*) is the intended behavior here!!
			return buildConditionSql(x, " = NULL");
		} else {
			return buildConditionSql(x, " = ", literalImpl(y));
		}
	}

	public <T> DbCondition notEqual(DbExpression<? extends T> x, DbExpression<? extends T> y) {
		return buildConditionSql(x, " <> ", y);
	}

	public <C, S extends Comparable<? extends C>, T extends Comparable<? extends C>> DbCondition lessThan(DbExpression<S> x, DbExpression<T> y) {
		return buildConditionSql(x, " < ", y);
	}

	public <C, S extends Comparable<? extends C>, T extends Comparable<? extends C>> DbCondition lessThanOrEqualTo(DbExpression<S> x, DbExpression<T> y) {
		return buildConditionSql(x, " <= ", y);
	}

	public <C, S extends Comparable<? extends C>, T extends Comparable<? extends C>> DbCondition greaterThan(DbExpression<S> x, DbExpression<T> y) {
		return buildConditionSql(x, " > ", y);
	}

	public <C, S extends Comparable<? extends C>, T extends Comparable<? extends C>> DbCondition greaterThanOrEqualTo(DbExpression<S> x, DbExpression<T> y) {
		return buildConditionSql(x, " >= ", y);
	}
	
	public DbCondition isNull(DbField<?> field) {
		return buildConditionSql(field, " IS NULL");
	}

	public DbCondition isNull(DbExpression<?> expression) {
		return buildConditionSql(expression, " IS NULL");
	}
	
	public DbCondition isNotNull(DbField<?> field) {
		return buildConditionSql(field, " IS NOT NULL");
	}

	public DbCondition isNotNull(DbExpression<?> expression) {
		return buildConditionSql(expression, " IS NOT NULL");
	}

	public boolean supportsILike() {
		return false;
	}

	public DbCondition iLike(DbExpression<String> x, String pattern) {
		return buildConditionSql(x, " ILIKE '", SQLUtils2.escape(pattern), "'");
	}

	public DbCondition notILike(DbExpression<String> x, String pattern) {
		return buildConditionSql(x, " NOT ILIKE '", SQLUtils2.escape(pattern), "'");
	}

	public DbCondition like(DbExpression<String> x, String pattern) {
		return buildConditionSql(x, " LIKE '", SQLUtils2.escape(pattern), "'");
	}

	public DbCondition notLike(DbExpression<String> x, String pattern) {
		return buildConditionSql(x, " NOT LIKE '", SQLUtils2.escape(pattern), "'");
	}

	public DbCondition like(DbExpression<String> x, DbExpression<String> y) {
		return buildConditionSql(x, " LIKE ", y);
	}

	public DbCondition likeUnsafe(DbExpression<?> x, DbExpression<String> y) {
		return buildConditionSql(x, " LIKE ", y);
	}

	public DbCondition likeUnsafe(DbExpression<?> x, String pattern) {
		return buildConditionSql(x, " LIKE ", pattern);
	}

	public DbCondition notLike(DbExpression<String> x, DbExpression<String> y) {
		return buildConditionSql(x, " NOT LIKE ", x);
	}

	public <T, C extends Collection<T>> DbCondition isMember(DbExpression<T> e, DbExpression<T> coll) {
		return buildConditionSql(e, " IN ", coll);
	}
	
	public <T> DbCondition in(DbExpression<T> expression, Collection<T> values) {
		return in(expression, values, "IN", null);
	}
	
	public <T> DbCondition in(String alias, DbExpression<T> expression, Collection<T> values) {
		return in(expression, values, "IN", alias);
	}

	public <T> DbCondition notin(DbExpression<T> expression, Collection<T> values) {
		return in(expression, values, "NOT IN", null);
	}

	protected <T> DbCondition in(DbExpression<T> expression, Collection<T> values, String inrepr, String alias) {
		if (values.size() <= getInLimit()) {
			return inSimple(expression, values, inrepr, alias);
		} else {
			PreparedStringBuilder psRet = new PreparedStringBuilder();
			boolean first = true;
			for (List<T> split : RigidUtils.splitEvery(values, getInLimit())) {
				DbCondition dbCond = inSimple(expression, split, inrepr, alias);
				if (first) {
					first = false;
				} else {
					psRet.append(" OR ");
				}
				psRet.append("(");
				psRet.append(dbCond.getSqlString());
				psRet.append(")");
			}
			return new DbCondition(this, psRet);
		}
	}
	
	protected <T> DbCondition inSimple(DbExpression<T> expression, Collection<T> values, String inrepr, String alias) {
		PreparedStringBuilder ps = new PreparedStringBuilder();
		int index = 0;
		for (T value : values) {
			if (value != null) {
				if (index++ > 0) {
					ps.append(", ");					
				}
				ps.append(literalImpl(value));
			}
		}
		if (index == 0) {
			ps.append("NULL");			
		}
		if (alias != null && !alias.isEmpty()) {
			return buildConditionSql(alias + ".", expression, " " + inrepr + " (", ps, ")");			
		}
		return buildConditionSql(expression, " " + inrepr + " (", ps, ")");		
	}

	public <T> DbCondition in(DbExpression<T> expression, String sql) {
		return buildConditionSql(expression, " IN (", sql, ")");
	}

	public <T> DbCondition in(DbExpression<T> expression, DbQuery<T> subquery) {
		return buildConditionSql(expression, " IN (", buildPreparedString(subquery), ")");
	}
	
	public <T> DbCondition exists(DbQuery<T> subquery) {
		return buildConditionSql("EXISTS (", buildPreparedString(subquery), ")");
	}

	public <T> DbCondition notexists(DbQuery<T> subquery) {
		return buildConditionSql("NOT EXISTS (", buildPreparedString(subquery), ")");
	}

	public DbCondition not(DbCondition condition) {
		return buildConditionSql("NOT (", condition.getSqlString(), ")");
	}

	public DbCondition and(DbCondition...conditions) {
		if (conditions.length == 0) {
			return alwaysTrue(); // This is the same behavior as JPA's Criteria API			
		}
		PreparedStringBuilder sqlString = PreparedStringBuilder.valueOf("(");
		for (int i = 0; i < conditions.length; i++) {
			if (i > 0) {
				sqlString.append(" AND ");
			}
			sqlString.append(conditions[i].getSqlString());
		}
		return new DbCondition(this, sqlString.append(")"));
	}

	public DbCondition and2(DbCondition c, DbCondition...conditions) {
		if (conditions.length == 0) {
			return c;			
		}
		final PreparedStringBuilder sqlString = PreparedStringBuilder.valueOf("(").append(c.getSqlString());
		for (int i = 0; i < conditions.length; i++) {
			sqlString.append(" AND ").append(conditions[i].getSqlString());
		}
		return new DbCondition(this, sqlString.append(")"));
	}

	public DbCondition or(Collection<DbCondition> conditions) {
		DbCondition[] conditionArray;

		if (conditions == null || conditions.isEmpty()) {
			conditionArray = new DbCondition[0];
		} else {
			conditionArray = new DbCondition[conditions.size()];
			conditions.toArray(conditionArray);
		}

		return or(conditionArray);
	}

	public DbCondition or(DbCondition...conditions) {
		if (conditions.length == 0) {
			return alwaysFalse(); // This is the same behavior as JPA's Criteria API			
		}
		PreparedStringBuilder sqlString = PreparedStringBuilder.valueOf("(");
		for (int i = 0; i < conditions.length; i++) {
			if (i > 0) {
				sqlString.append(" OR ");
			}
			sqlString.append(conditions[i].getSqlString());
		}
		return new DbCondition(this, sqlString.append(")"));
	}

	public DbCondition or2(DbCondition c, DbCondition...conditions) {
		if (conditions.length == 0) {
			return c;			
		}
		final PreparedStringBuilder sqlString = PreparedStringBuilder.valueOf("(").append(c.getSqlString());
		for (int i = 0; i < conditions.length; i++) {
			sqlString.append(" OR ").append(conditions[i].getSqlString());
		}
		return new DbCondition(this, sqlString.append(")"));
	}

	public DbCondition alwaysTrue() {
		return buildConditionSql("1=1");
	}

	public DbCondition alwaysFalse() {
		return buildConditionSql("1<>1");
	}

	public int getInLimit() {
		// this limit is smaller than Oracle's
		return 255;
	}

	@Deprecated
	public DbCondition plainCondition(String sql) {
		return new DbCondition(this, PreparedStringBuilder.valueOf(sql));
	}

	//
	// Order
	//

	public DbOrder asc(DbExpression<?> expression) {
		return new DbOrder(expression, true);
	}

	public DbOrder desc(DbExpression<?> expression) {
		return new DbOrder(expression, false);
	}

	//
	//
	//

	protected DbCondition buildConditionSql(Object...args) {
		return new DbCondition(this, buildSql(args));
	}

	protected <T> DbExpression<T> buildExpressionSql(Class<? extends T> javaType, Object...args) {
		return new DbExpression<T>(this, javaType, buildSql(args));
	}

	protected abstract PreparedStringBuilder buildPreparedString(DbQuery<?> query);

	protected abstract PreparedStringBuilder buildPreparedSubselect(DbQuery<?> query);

	protected abstract PreparedStringBuilder buildPreparedString(DbDelete delete);
	
	protected abstract PreparedStringBuilder buildPreparedString(DbUpdate update);

	protected PreparedStringBuilder getPreparedString(DbField field) {
		return new PreparedStringBuilder(field.getDbColumn());
	}
	
	protected PreparedStringBuilder getPreparedString(DbCondition condition) {
		return condition.getSqlString();
	}

	protected PreparedStringBuilder getPreparedString(DbExpression<?> expression) {
		return expression.getSqlString();
	}

	protected PreparedStringBuilder buildSql(Object...args) {
		for (int i = 0; i < args.length; i++) {
			if (args[i] instanceof DbField) {
				args[i] = getPreparedString((DbField) args[i]);
			} else if (args[i] instanceof DbCondition) {
				args[i] = getPreparedString((DbCondition) args[i]);
			} else if (args[i] instanceof DbExpression<?>) {
				args[i] = getPreparedString((DbExpression<?>) args[i]);
			} else if (args[i] instanceof UID) {
				args[i] = ((UID) args[i]).getString();
			} else if (args[i] instanceof String) {
				// do nothing, no conversion needed
			} else if (args[i] instanceof PreparedStringBuilder) {
				// do nothing, no conversion needed
			} else if (args[i] == null) {
				throw new NullArgumentException("No null allowed at index " + i + " in " + Arrays.asList(args));
			} else {
				throw new IllegalArgumentException("Don't know how to handle " + args[i] 
						+ " of " + args[i].getClass().getName() + " in " + Arrays.asList(args));
			}
		}
		return PreparedStringBuilder.concat(args);
	}
	
	public abstract PreparedString getPreparedString(DbQuery<?> query);
	
}
