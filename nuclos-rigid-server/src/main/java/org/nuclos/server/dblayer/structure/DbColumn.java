//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.structure;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.DbField;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.server.dblayer.DbException;

public class DbColumn extends DbTableArtifact implements DbField {

	private final DbColumnType	columnType;
	private DbNullable nullable;
	private final Object defaultValue;
	private final int order;

	public DbColumn(UID uid, DbNamedObject table, String columnName, DbColumnType columnType, DbNullable nullable, Object defaultValue, Integer order) {
		super(uid, table, columnName);
		this.columnType = columnType;
		this.nullable = nullable;
		this.defaultValue = defaultValue;
		this.order = RigidUtils.defaultIfNull(order, Integer.MAX_VALUE);
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("columnType=").append(columnType);
		result.append(", name=").append(getSimpleName());
		result.append(", nullable=").append(nullable);
		result.append(", default=").append(defaultValue);
		result.append(", order=").append(order);
		result.append("]");
		return result.toString();
	}

	public String getColumnName() {
		return getSimpleName();
	}
	
	public Object getDefaultValue() {
		return this.defaultValue;
	}
	
	public DbColumnType getColumnType() {
		return columnType;
	}
	
	public DbNullable getNullable() {
		return nullable;
	}
	
	public void setNullable(DbNullable nullable) {
		this.nullable = nullable;
	}
	
	@Override
	public String getEqualsKey() {
		return getTable().getEqualsKey() + "." + super.getEqualsKey();
	}
	
	@Override
	public String getArtifactName() {
		return getTable().getName() + "." + getColumnName();
	}

	@Override
	protected boolean isUnchanged(DbArtifact a, boolean forceNames) {
		DbColumn other = (DbColumn) a;
		return ObjectUtils.equals(getColumnName(), other.getColumnName())
			&& ObjectUtils.equals(getColumnType(), other.getColumnType())
			&& ObjectUtils.equals(getNullable(), other.getNullable());
	}
	
	@Override
	public <T> T accept(DbArtifactVisitor<T> visitor) throws DbException {
		return visitor.visitColumn(this);
	}

	@Override
	public String getDbColumn() {
		return getColumnName();
	}

	@Override
	public Class getJavaClass() {
		return getColumnType().getGenericType().getPreferredJavaType();
	}
	
	@Override
	public boolean isVirtual() {
		return false;
	}
	
	public int getOrder() {
		return order;
	}
	
	// TODO: types
}
