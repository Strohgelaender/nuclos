//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.impl.postgresql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.dbunit.dataset.datatype.IDataTypeFactory;
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.DbObjectHelper;
import org.nuclos.server.dblayer.DbType;
import org.nuclos.server.dblayer.EBatchType;
import org.nuclos.server.dblayer.IBatch;
import org.nuclos.server.dblayer.IPart;
import org.nuclos.server.dblayer.IPart.NextPartHandling;
import org.nuclos.server.dblayer.IPreparedStringExecutor;
import org.nuclos.server.dblayer.impl.BatchImpl;
import org.nuclos.server.dblayer.impl.PartImpl;
import org.nuclos.server.dblayer.impl.SqlConditionalUnit;
import org.nuclos.server.dblayer.impl.SqlSequentialUnit;
import org.nuclos.server.dblayer.impl.SubPartImpl;
import org.nuclos.server.dblayer.impl.standard.MetaDataSchemaExtractor;
import org.nuclos.server.dblayer.impl.standard.StandardSqlDBAccess;
import org.nuclos.server.dblayer.impl.util.PreparedString;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.incubator.DbExecutor.ResultSetRunner;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.structure.DbArtifact;
import org.nuclos.server.dblayer.structure.DbCallable;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbPrimaryKeyConstraint;
import org.nuclos.server.dblayer.structure.DbIndex;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbSequence;
import org.nuclos.server.dblayer.structure.DbSimpleView;
import org.nuclos.server.dblayer.structure.DbSimpleView.DbSimpleViewColumn;
import org.nuclos.server.dblayer.structure.DbTable;

/**
 * @author Thomas Pasch
 * @since Nuclos 3.2.0
 */
public class PostgreSQLDBAccess extends StandardSqlDBAccess {

	public static final String AUTOSAVEPOINT = "autosavepoint";

    private static final Logger LOG = Logger.getLogger(PostgreSQLDBAccess.class);

	private boolean autoSavepoint = true;
	
	public PostgreSQLDBAccess() {
	}
		
	@Override
    protected Integer getDefaultFetchSize() {
    	return 1000;
    }
	
	@Override
    protected boolean autoCommitToFalseBeforeSelect() {
    	return true;
    }

	@Override
	protected boolean wasCanceled(SQLException e) {
		// 57014 is the postgresql-code for "ERROR: canceling statement due to user request"
		return "57014".equals(e.getSQLState());
	}
	   
	@Override
	public void init(DbType type, DataSource dataSource, Map<String, String> config) {
		if (config.containsKey(AUTOSAVEPOINT)) {
			autoSavepoint = Boolean.valueOf(config.get(AUTOSAVEPOINT));
		}
		if (autoSavepoint) {
			LOG.info("Auto savepoint activated");
		}
		this.executor = new PostgreSQLExecutor(dataSource, config.get(USERNAME), config.get(PASSWORD)); 
		super.init(type, dataSource, config);
	}
	
	@Override
	public <T> T executeFunction(String name, final Class<T> resultType, Object ... args) throws DbException {
		String sql = String.format("SELECT %s(%s)", name, join(",", RigidUtils.replicate("?", args.length)));
		return this.executeQueryImpl(sql, null, new ResultSetRunner<T>() {
			@Override
			public T perform(ResultSet rs) throws SQLException { return rs.next() ? getResultSetValue(rs, 1, resultType) : null; }
		}, args);
	}

	@Override
	public void runWithDisabledChecksAndTriggers(Runnable runnable) throws DbException {
		// TODO:
		throw new UnsupportedOperationException();
	}

	@Override
	public String generateName(String prefix, String base, String...affixes) {
		return generateName(prefix, base, 30, affixes);
	}

	@Override
	public DbQueryBuilder getQueryBuilder() {
		return new PostgreSQLQueryBuilder(this);
	}

	@Override
	protected String getDataType(DbColumnType columnType) throws DbException {
		if (columnType.getTypeName() != null) {
			return columnType.getTypeName() + columnType.getParametersString();
		} else {
			switch (columnType.getGenericType()) {
			case VARCHAR:
				return String.format("VARCHAR(%d)", columnType.getLength());
			case NVARCHAR:
				return String.format("VARCHAR(%d)", columnType.getLength());
			case NUMERIC:
				return String.format("NUMERIC(%d,%d)", columnType.getPrecision(), columnType.getScale());
			case BOOLEAN:
				return "BOOLEAN";
			case BLOB:
				return "BYTEA";
			case CLOB:
				return "TEXT";
			case NCLOB:
				return "TEXT";
			case DATE:
				return "DATE";
			case DATETIME:
				return "TIMESTAMP";
			default:
				throw new DbException("Unsupported column type " + columnType.getGenericType());
			}
		}
	}

	@Override
    public String getSqlForCastAsString(String x, DbColumnType fromType) {
    	if (fromType.getGenericType() == DbGenericType.DATE
				|| fromType.getGenericType() == DbGenericType.DATETIME) {
    		final String pattern = getDateFormat().toPattern();
    		return String.format("TO_CHAR(%s, '%s')", x, pattern);
    	}
		return getSqlForCast(x, new DbColumnType(DbGenericType.VARCHAR, 255));
	}

	@Override
	protected IBatch getSqlForCreateIndex(DbIndex index) {
		return BatchImpl.simpleBatch(PreparedString.format("CREATE INDEX %s ON %s (%s)",
			getName(index.getIndexName()),
			getQualifiedName(index.getTable().getName()),
			join(",", index.getColumnNames())));
	}

	protected IBatch getSqlForRenameColumn(DbColumn column1, DbColumn column2) throws SQLException {
		final PreparedString ps = PreparedString.format("ALTER TABLE %s RENAME %s TO %s",
				getQualifiedName(column2.getTable().getName()),
				column1.getColumnName(), column2.getColumnName());
		return BatchImpl.simpleBatch(ps);
	}

	@Override
	protected IBatch getSqlForAlterTableColumn(DbColumn column1, DbColumn column2) throws SQLException {
		IBatch result = null;
		if (!column1.getColumnName().equalsIgnoreCase(column2.getColumnName())) { 
			// rename table column
			result = getSqlForRenameColumn(column1, column2);
		}
		result = getSqlForAlterTableColumnSetNull(column1, column2, result);
		
		final String sColumnSpec = getColumnSpecForAlterTableColumn(column2, column1);
		if(sColumnSpec != null) {
			final PreparedString ps = PreparedString.format("ALTER TABLE %s %s",
				getQualifiedName(column2.getTable().getName()),
				sColumnSpec);
			if(column2.getDefaultValue() != null && column2.getNullable().equals(DbNullable.NOT_NULL)) {
				if (result != null) {
					result.append(getSqlForUpdateNotNullColumn(column2));
				} else {
					result = getSqlForUpdateNotNullColumn(column2);
				}
				result.append(new SqlSequentialUnit(ps));
			}
			else {
				if (result != null) {
					result.append(BatchImpl.simpleBatch(ps));
				} else {
					result = BatchImpl.simpleBatch(ps);
				}
			}
		}
		
		return result;
	}
	
	@Override
	protected IBatch getSqlForAlterTableNotNullColumn(DbColumn column) {
		String columnSpec = String.format("ALTER %s TYPE %s",
			column.getColumnName(),
			getDataType(column.getColumnType()));
		

			columnSpec += String.format(", ALTER %s %s NOT NULL",
				column.getColumnName(), "SET");
		
		PreparedString str = PreparedString.format("ALTER TABLE %s %s",
			getQualifiedName(column.getTable().getName()),
			columnSpec);
		
    	return BatchImpl.simpleBatch(str);
    }

	@Override
	protected String getColumnSpecForAlterTableColumn(DbColumn column, DbColumn oldColumn) {
		if(!getDataType(column.getColumnType()).equals(getDataType(oldColumn.getColumnType()))) {
			String columnSpec = String.format("ALTER %s TYPE %s",
				column.getColumnName(),
				getDataType(column.getColumnType()));
			if (column.getNullable() != oldColumn.getNullable()) {
				// if nullability is changed, add another 
				columnSpec += String.format(", ALTER %s %s NOT NULL",
					column.getColumnName(),
					column.getNullable() == DbNullable.NOT_NULL ? "SET" : "DROP");
			}
			return columnSpec;
		}
		else {			
			if (column.getNullable() != oldColumn.getNullable()) {
				// if nullability is changed, add another 
				return String.format("ALTER %s %s NOT NULL",
					column.getColumnName(),
					column.getNullable() == DbNullable.NOT_NULL ? "SET" : "DROP");
			}		
		}
		return null;
	}

	@Override
	protected IBatch getSqlForDropColumn(DbColumn column) {
		return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s DROP COLUMN %s CASCADE",
			getQualifiedName(column.getTable().getName()),
			column.getColumnName()));
	}

    /**
     * @deprecated Views has always been problematic (especially with PostgreSQL).
     * 	Avoid whenever possible.
     */
    @Override
    protected IBatch getSqlForCreateSimpleView(DbSimpleView view) throws DbException {
    	return BatchImpl.simpleBatch(_getSqlForCreateSimpleView("CREATE VIEW", view));
    }
    
    /**
     * With PostgreSQL views sometimes get deleted when one of the underlying tables
     * is alter. Because of this drop not always succeed. Hence we <em>ignore</em> any
     * problem when dropping a view.
     * 
     * @author Thomas Pasch
     * @since Nuclos 3.2.0
     */
    @Override
    protected IBatch getSqlForDropSimpleView(DbSimpleView view) {
    	return new BatchImpl(new SqlConditionalUnit(_getSqlForDropSimpleView(view)));
    }

    protected PartImpl _getSqlForDropSimpleView(DbSimpleView view) {
    	final PartImpl part = new PartImpl(PreparedString.format("DROP VIEW %s",
                getQualifiedName(view.getViewName())), 
                EBatchType.FAIL_NEVER_IGNORE_EXCEPTION, NextPartHandling.ALWAYS);
    	return part;
    }

	@Override
	protected IBatch getSqlForAlterSimpleView(DbSimpleView oldView, DbSimpleView newView) {
		if (!oldView.getViewName().equals(newView.getViewName())) {
			throw new IllegalArgumentException();
		}
		if (!existsTableOrView(newView.getViewName())) {
			// view does not exist any more (PostgreSQL sometimes drops views when underlying
			// tables have been modified.) (tp)
			return getSqlForAlterSimpleViewFallback(oldView, newView);
		}

		// http://www.postgresql.org/docs/9.1/static/sql-createview.html
		// Create or replace is very restricted...
		
		final List<DbSimpleViewColumn> oldColumns = oldView.getViewColumns();
		final List<DbSimpleViewColumn> newColumns = newView.getViewColumns();
		
		if (oldColumns.size() > newColumns.size()) {
			// deleting columns is not possible
			return getSqlForAlterSimpleViewFallback(oldView, newView);
		}
		final Set<DbSimpleViewColumn> addSet = new HashSet<DbSimpleViewColumn>();
		final Set<DbSimpleViewColumn> deleteSet = new HashSet<DbSimpleViewColumn>(oldColumns);
		for (DbSimpleViewColumn nc: newColumns) {
			if (!deleteSet.remove(nc)) {
				addSet.add(nc);
			}
		}
		
		if (!deleteSet.isEmpty()) {
			// deleting columns is not possible
			return getSqlForAlterSimpleViewFallback(oldView, newView);
		}
		// Modify new column list in place.
		newColumns.clear();
		newColumns.addAll(ensureNaturalSequence(newView, oldColumns));
		newColumns.addAll(addSet);
		
		// Sometimes 'CREATE OR REPLACE VIEW' fails (even) for unknown reason ...
		final PreparedString ps = _getSqlForCreateSimpleView("CREATE OR REPLACE VIEW", newView);
		final List<IPart> parts = new ArrayList<IPart>(2);
		parts.add(new PartImpl(
				ps, EBatchType.FAIL_NEVER_IGNORE_EXCEPTION, NextPartHandling.ONLY_IF_THIS_FAILS));
		
		// ... in this case we fall back to first DROP and then CREATE. (tp)
		final List<IPart> subParts = new ArrayList<IPart>(2);
		subParts.add(_getSqlForDropSimpleView(oldView));
		subParts.add(new PartImpl(_getSqlForCreateSimpleView("CREATE VIEW", newView),
				EBatchType.FAIL_LATE, NextPartHandling.ALWAYS));
		parts.add(new SubPartImpl(subParts, EBatchType.FAIL_LATE, NextPartHandling.ALWAYS));
		
		final SqlConditionalUnit unit = new SqlConditionalUnit(parts);
		final IBatch result = new BatchImpl(unit);
		return result;
	}

	private IBatch getSqlForAlterSimpleViewFallback(DbSimpleView oldView, DbSimpleView newView) {
		final IBatch result = getSqlForDropSimpleView(oldView);
		result.append(getSqlForCreateSimpleView(newView));
		return result;
	}
	
	@Override
	protected IBatch getSqlForDropCallable(DbCallable callable) throws DbException {
		String code = callable.getCode();
		if (code == null)
			throw new DbException("No code for callable " + callable.getCallableName());
		Pattern pattern = Pattern.compile(
			String.format("\\s*(?:CREATE\\s+(?:OR\\s+REPLACE\\s+)?)?(%s\\s+%s\\s*\\([^)]*\\))",
				callable.getType(), callable.getCallableName()),
				Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(code);
		boolean success = matcher.lookingAt();
		if (!success)
			throw new DbException("Cannot interpret header for callable " + callable.getCallableName());
		return BatchImpl.simpleBatch(PreparedString.format("DROP %s", matcher.group(1)));
	}

	@Override
	public String getSqlForConcat(String x, String y) {
		return String.format("COALESCE(CAST(%s as text), '') || COALESCE(CAST(%s as text), '')", x, y);
	}

	@Override
	public String getSqlForConcat(List<String> l) {
		if (l == null || l.isEmpty()) {
			throw new IllegalArgumentException();
		}
		final StringBuilder result = new StringBuilder("COALESCE(CAST(");
		for (Iterator<String> it = l.iterator(); it.hasNext();) {
			result.append(it.next()).append(" as text), '')");
			if (it.hasNext()) {
				result.append(" || COALESCE(CAST(");
			}
		}
		return result.toString();
	}
	
	@Override
    protected IBatch getSqlForRenameTable(DbTable table1, DbTable table2) {
    	return BatchImpl.simpleBatch(PreparedString.format("ALTER TABLE %s RENAME TO %s",
    			getQualifiedName(table1.getTableName()),
    			getName(table2.getTableName())));
    }

	@Override
	public boolean validateObjects() throws DbException {
		// AFAIK, PostgreSQL doesn't support a something like that...
		// (i.e. it "don't even store the original text of the view", cf. thread
		// http://archives.postgresql.org/pgsql-hackers/2010-04/msg01495.php)
		// ...so we can't do anything here and return false
		return false;
	}
	
	@Override
    protected IPreparedStringExecutor getPreparedStringExecutor() {
    	return new PostgreSQLPreparedStringExecutor(executor, autoSavepoint);
    }
    
    @Override
	protected MetaDataSchemaExtractor getMetaDataExtractor() {
		return new PostgreSQLMetaData();
	}

	@Override
	protected String getTablespaceSuffix(DbArtifact artifact) {
		String tablespace = getTablespace();
		if (tablespace != null) {
			if (artifact instanceof DbTable) {
				return "TABLESPACE " + tablespace;
			} else if (artifact instanceof DbPrimaryKeyConstraint) {
				return "TABLESPACE " + tablespace;
			}
		}
		return "";
	}

	@Override
	protected String getUsingIndex(DbConstraint constraint) {
		return "";
	}

	protected String getTablespace() {
		return RigidUtils.nullIfEmpty(config.get(TABLESPACE));
	}
	
	@Override
	protected IDataTypeFactory getDataTypeFactory() {
		return new PostgresqlDataTypeFactory();
	}

	static class PostgreSQLMetaData extends MetaDataSchemaExtractor {

		@Override
		protected void initMetaData() throws SQLException {
			supportsJDBC4getFunction = false;
			super.initMetaData();
		}

		@Override
		protected Collection<DbSequence> getSequences() throws SQLException {
			Collection<DbSequence> sequences = new ArrayList<DbSequence>();
			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM pg_class c, pg_namespace ns WHERE ns.oid = c.relnamespace AND relkind = 'S' AND nspname = ?");
			ResultSet rs = null;
			try {
				stmt.setString(1, schema);
				rs = stmt.executeQuery();
				while (rs.next()) {
					String sequenceName = rs.getString("relname");

					Statement stmt2 = connection.createStatement();
					ResultSet rs2 = null;
					try {
						rs2 = stmt2.executeQuery("SELECT * FROM " + sequenceName);
						if (rs2.next()) {
							sequences.add(new DbSequence(null, sequenceName, rs2.getLong("LAST_VALUE")));      				
						}
					} finally {
						if (rs2 != null) {
							rs2.close();							
						}
						stmt2.close();
					}
				}
			} finally {
				if (rs != null) {
					rs.close();					
				}
				stmt.close();
			}
			return sequences;
		}

		@Override
		public Map<String, String> getDatabaseParameters() throws SQLException {
			// TODO:
			return Collections.emptyMap();
		}

		@Override
		protected DbColumnType getColumnType(ResultSet rs) throws SQLException {
			int sqlType = rs.getInt("DATA_TYPE");
			String typeName = rs.getString("TYPE_NAME");
			Integer precision = rs.getInt("COLUMN_SIZE");
			Integer scale = rs.getInt("DECIMAL_DIGITS");
			// PostgreSQL does not return NULL for DECIMAL_DIGITS if it is a non-number type...
			DbGenericType genericType = getDbGenericType(sqlType, typeName);
			if (genericType != null) {
				return new DbColumnType(genericType, typeName, precision, precision, scale);
			} else {
				Integer length = 0;
				if (scale == 0) {
					length = precision;
					scale = precision = null;
				}
				return new DbColumnType(null, typeName, length, precision, scale);
			}
		}
		
		@Override
		protected DbGenericType getDbGenericType(int sqlType, String typeName) {
			return PostgreSQLDBAccess.getDbGenericType(sqlType, typeName);
		}
	}
	
	private Long estimateCountAll(String tableName) {
		String fullTableName = tableName.indexOf('.') >= 0 ? tableName : getSchemaName() + "." + tableName;
		
		String sql = "SELECT reltuples AS count FROM pg_class WHERE oid = '" + fullTableName + "'::regclass";
		ResultVO resultVO = executePlainQueryAsResultVO(sql, 1, false);
		Number result = (Number)resultVO.getRows().get(0)[0];
		
		return result.longValue();
	}
	
	private <PK> Long estimateCountQuery(DbQuery<PK> query) {
		try {
			PreparedString ps = query.getBuilder().getPreparedString(query);
			PreparedStatement stmt = executor.getPreparedStatement(ps);
			String sqlQuery = StringEscapeUtils.escapeSql(stmt.toString());
			String sqlEstimateCount = "SELECT count_estimate('" + sqlQuery + "');";
			ResultVO resultVO = executePlainQueryAsResultVO(sqlEstimateCount, 1, false);
			
			return ((Number)resultVO.getRows().get(0)[0]).longValue();
			
		} catch (Exception s) {
			LOG.warn(s.getMessage(), s);
		}
		return 0L;
	}
	
	public <PK> Long estimateCount(EntityMeta<PK> eMeta, DbQuery<PK> query) {
		String sourceTable;
		
		if (eMeta.getVirtualEntity() == null) {
			sourceTable = eMeta.getDbTable();
		} else {
			sourceTable = new DbObjectHelper(this).getSourceTableForSimpleViewIfAny(eMeta.getVirtualEntity());
		}
		
		if (sourceTable == null) {
			return null;
		}
		
		Long estimatedCountAll = estimateCountAll(sourceTable);
		
		if (query == null) {
			return estimatedCountAll >= 250000 ? estimatedCountAll : null;
		} 
		
		Long estimatedCountQuery = estimateCountQuery(query);
		
		if (estimatedCountQuery >= Math.max(50000, estimatedCountAll/10)) {
			return estimatedCountQuery;
		}
		
		return null;
	}
	
    @Override
    public boolean supportsSubqueryPaging() {
    	return true;
    }

	@SuppressWarnings("serial")
	public static class PostgreSQLQueryBuilder extends StandardQueryBuilder {
		
		public PostgreSQLQueryBuilder(StandardSqlDBAccess dbAccess) {
			super(dbAccess);
		}

		@Override
		public int getInLimit() {
			return 20000;
		}
		
		@Override
		public DbExpression<String> upper(DbExpression<String> x) {
			return buildExpressionSql(String.class, "UPPER(CAST(", x, " AS VARCHAR))");
		}

		@Override
		public DbExpression<Date> currentDate() {
			return buildExpressionSql(java.util.Date.class, PreparedStringBuilder.valueOf("CURRENT_DATE"));
		}
		
		@Override
		protected PreparedStringBuilder buildPreparedString(DbQuery<?> query) {
			PreparedStringBuilder ps = super.buildPreparedString(query);
			
			if (query.hasLimit()) {
				ps.appendf(" LIMIT %d", query.getLimit());
			}
			
			if (query.hasOffset()) {
				ps.appendf(" OFFSET %d", query.getOffset());
			}
			return ps;
		}

		@Override
		public boolean supportsILike() {
			return true;
		}
	}

	@Override
	public boolean withOffsetZero() {
		return false;
	}

	@Override
	public String escapeLiteral(final String s) throws SQLException {
		return org.postgresql.core.Utils.escapeLiteral(null, s, true).toString();
	}
}
