package org.nuclos.server.dblayer.query;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.JoinType;

public class DbLanguageJoin<T> extends DbJoin<T> {

	UID language;
	
	DbLanguageJoin(DbQuery<?> query, DbFrom<?> left, JoinType joinType,
			EntityMeta<?> entity, boolean forceTable, UID language) {
		super(query, left, joinType, entity, forceTable);
		this.language = language;
	}

	public UID getLanguage() {
		return language;
	}

	
}
