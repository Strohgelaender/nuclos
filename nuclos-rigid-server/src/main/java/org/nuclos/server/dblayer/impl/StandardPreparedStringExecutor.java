//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.DbField;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.IPreparedStringExecutor;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.impl.standard.StandardSqlDBAccess;
import org.nuclos.server.dblayer.impl.util.PreparedString;
import org.nuclos.server.dblayer.incubator.DbExecutor;
import org.nuclos.server.dblayer.incubator.DbExecutor.ConnectionRunner;
import org.nuclos.server.dblayer.incubator.DbExecutor.ResultSetRunner;
import org.nuclos.server.dblayer.incubator.SQLInfo;
import org.nuclos.server.dblayer.statements.DbMap;

/**
 * @author Thomas Pasch
 * @since Nuclos 3.2.0
 */
public class StandardPreparedStringExecutor implements IPreparedStringExecutor {
	
    private static final Logger LOG = Logger.getLogger(StandardPreparedStringExecutor.class);

	protected final DbExecutor dbExecutor;
	
	public StandardPreparedStringExecutor(DbExecutor dbExecutor) {
		if (dbExecutor == null) throw new NullPointerException();
		this.dbExecutor = dbExecutor;
	}

	/**
	 * @deprecated
	 */
	@Override
    public Integer executePreparedStatements(List<PreparedString> pss) throws SQLException {
        int result = 0;
        for (PreparedString ps : pss)
            result += executePreparedStatement(ps);
        return result;
    }

	
	public void executePreparedStatement(final PreparedString sourceQuery, final PreparedString targetQuery, final DbMap map) throws SQLException {
		  
      final String sql = sourceQuery.toString();
      final Object[] parameters = sourceQuery.getParameters();
      final ResultSetRunner<List<Object[]>> runner = new ResultSetRunner<List<Object[]>>() {
		@Override
		public List<Object[]> perform(ResultSet rs) throws SQLException {
		    List<Object[]> result = new ArrayList<Object[]>();
        	while (rs.next()) {
        		try {
        			result.add(extractRow(rs));					
				} catch (ClassNotFoundException e) {
					LOG.error(e);
				}
        	}            	
            return result;
        }

        protected Object[] extractRow(ResultSet rs) throws SQLException, ClassNotFoundException {
            int columnCount = rs.getMetaData().getColumnCount();
            Object[] values = new Object[columnCount];
            for (int i = 0; i < columnCount; i++) {
                values[i] = StandardSqlDBAccess.getResultSetValue(rs, i + 1, map.getColumnTypeSelect(i));
                if (values[i] == null) {
                	values[i] = DbNull.forType(map.getColumnTypeSelect(i));
                }
            }
            return values;
        }
		
      };
      
            logSql("execute SQL statement", sql, null);
            final List<Object[]> sources = dbExecutor.execute(new ConnectionRunner<List<Object[]>>() {
			    @Override
			    public List<Object[]> perform(Connection conn) throws SQLException {
			        PreparedStatement stmt = conn.prepareStatement(sql);
			        try {
			        	
			        	if (sourceQuery.hasParameters()) {
			        		dbExecutor.prepareStatementParameters(stmt, parameters);
			        	}
			            ResultSet rs = stmt.executeQuery();
			            try {
			            	return runner.perform(rs);
			            } finally {
			                rs.close();
			            }
			        } finally {
			            stmt.close();
			        }
			    }

				@Override
				public SQLInfo getInfo() {
					return new SQLInfo(sql, parameters);
				}
			});
            
            dbExecutor.execute(new ConnectionRunner<Integer>() {
                @Override
                public Integer perform(Connection conn) throws SQLException {
                  
                	final PreparedStatement stmt = conn.prepareStatement(targetQuery.toString());

                	try {
                		for (Object[] row : sources) {
                			final List<Object> params = new ArrayList<Object>();
                			for (DbField targetColumn : map.keySet()) {
	                			Object value = map.get(targetColumn);
	            	    		if (value != null) {
	            	    			if (value instanceof DbId) {
	            	    				params.add(dbExecutor.getNextId(SpringDataBaseHelper.DEFAULT_SEQUENCE));
	            	    			} else if(value instanceof DbField) {
            	    					// Map columns value into result param List
            	    					params.add(row[map.getColumnIndexSelect(targetColumn)]);
	            	    			}            	    				
	            	    			else {
	            	    				params.add(value);
	            	    			}
                				}
	            	    	}	                	    
                			dbExecutor.prepareStatementParameters(stmt, params.toArray());
                			
                			//stmt.executeUpdate();
                			// for logging...
                			dbExecutor.execute(new ConnectionRunner<Void>() {

								@Override
								public Void perform(Connection conn) throws SQLException {
									stmt.executeUpdate();
									return null;
								}

								@Override
								public SQLInfo getInfo() {
									return new SQLInfo(targetQuery.toString(), params);
								}
							});
                		}
                	} catch (Exception e) {
                		LOG.error("executePreparedStatement failed with " + e.toString() + ":\n\t" + targetQuery.toString() + "\n\t" + null);
                		throw e;
                	} finally {
                		stmt.close();
                		return 0;
                	}
                }

				@Override
				public SQLInfo getInfo() {
					return new SQLInfo(targetQuery.toString(), new ArrayList());
				}
            });
	}
	
	
	@Override
    public int executePreparedStatement(final PreparedString ps) throws SQLException {
        if (!ps.hasParameters()) {
            String sql = ps.toString();
            logSql("execute SQL statement", sql, null);
            return dbExecutor.executeUpdate(sql);
        } else {
            return dbExecutor.execute(new ConnectionRunner<Integer>() {
                @Override
                public Integer perform(Connection conn) throws SQLException {
                    String sql = ps.toString();
                    Object[] params = ps.getParameters();

                    logSql("execute SQL statement", sql, params);

                    PreparedStatement stmt = conn.prepareStatement(sql);
                    try {
                        dbExecutor.prepareStatementParameters(stmt, params);
                        return stmt.executeUpdate();
                    } catch (SQLException e) {
                		LOG.error("executePreparedStatement failed with " + e.toString() + ":\n\t" + sql + "\n\t" + Arrays.asList(params));
                    	throw e;
                    } finally {
                        stmt.close();
                    }
                }

				@Override
				public SQLInfo getInfo() {
					return new SQLInfo(ps.toString(), ps.getParameters());
				}
            });
        }
    }

    protected void logSql(String text, String sql, Object[] parameters) {
    	StringBuilder sb = new StringBuilder(text);
    	sb.append(" <[").append(sql.toString()).append("]>");
    	if (parameters != null && parameters.length > 0) {
    		sb.append(" with parameters [");
    		for (int i = 0, n = parameters.length; i < n; i++) {
    			if (i > 0) sb.append(", ");
    			Object param = parameters[i];
				sb.append(param);
				if (param != null) {
					Class<?> type = param.getClass();
					if (type != DbNull.class) {
						sb.append(" (").append(param.getClass().getName()).append(")");
					}
				}
    		}
    		sb.append("]");
    	}
        LOG.debug(sb.toString());
    }
}
