//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.UID;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.statements.DbBuildableStatement;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;

public class DbStatementUtils {

	public static interface SpecifiyCondition<F> {
		
		DbBuildableStatement where(final DbField<F> column1, F value1, Object...varargs);
		
	}
	
	// TODO: merge with SchemaUtils?
	
	private DbStatementUtils() {
		// Never invoked.
	}
	
	public static <PK,F> DbInsertStatement<PK> insertInto(final EntityMeta<PK> entity, final DbField<F> column1, F value1, Object...varargs) {
		return new DbInsertStatement(entity, makeMap(entity, column1, value1, varargs));
	}
	
	public static <PK> DbInsertStatement<PK> insertIntoUnsafe(final EntityMeta<PK> entity, final DbField<?> column1, Object value1, Object...varargs) {
		return new DbInsertStatement(entity, makeMap(entity, column1, value1, varargs));
	}
	
	public static <PK> DbDeleteStatement<PK> deleteAllFrom(final EntityMeta<PK> entity) {
		return new DbDeleteStatement(entity, new DbMap());
	}
	
	public static <PK,F> DbDeleteStatement<PK> deleteFrom(final EntityMeta<PK> entity, final DbField<F> column1, F value1, Object...varargs) {
		return deleteFromUnsafe(entity, column1, value1, varargs);
	}

	public static <PK> DbDeleteStatement<PK> deleteFromUnsafe(final EntityMeta<PK> entity, final DbField<?> column1, Object value1, Object...varargs) {
		return new DbDeleteStatement(entity, makeMap(entity, column1, value1, varargs));
	}
	
	public static <PK,F> SpecifiyCondition updateValues(final EntityMeta<PK> entity, final DbField<F> column1, F value1, Object...varargs) {
		return updateValuesUnsafe(entity, column1, value1, varargs);
	}
	
	public static <PK> SpecifiyCondition updateValuesUnsafe(final EntityMeta<PK> entity, final DbField<?> column1, Object value1, Object...varargs) {
		final DbMap values = makeMap(entity, column1, value1, varargs);
		return new SpecifiyCondition<Object>() {
			@Override
			public DbBuildableStatement where(final DbField<Object> column1, Object value1, Object ... varargs) {
				return new DbUpdateStatement(entity, values, makeMap(entity, column1, value1, varargs));
			}
		};
	}
	
	public static <PK,F> DbUpdateStatement<PK> getDbUpdateStatementWhereFieldIsNull(final String dbEntity, final DbField<F> field, F value) {
		final DbMap columnValueMap = new DbMap();
		final DbMap columnConditionMap = new DbMap();
		DbNull<F> dbNull = null;
		if(value instanceof String){
			dbNull = (DbNull<F>) new DbNull<String>(String.class);
		}
		else if(value instanceof Boolean) {
			dbNull = (DbNull<F>) new DbNull<Boolean>(Boolean.class);
		}
		else if(value instanceof Integer) {
			dbNull = (DbNull<F>) new DbNull<Integer>(Integer.class);
		}
		else if(value instanceof java.util.Date) {
			dbNull = (DbNull<F>) new DbNull<InternalTimestamp>(InternalTimestamp.class);
		}
		else if(value instanceof UID) {
			dbNull = (DbNull<F>) new DbNull<String>(String.class);
		}
		else {
			dbNull = (DbNull<F>) DbNull.forType(DbUtils.getDbType(value.getClass()));
		}
		
		columnConditionMap.put(field, dbNull);
		columnValueMap.put(field, value);
		DbUpdateStatement<PK> update = new DbUpdateStatement<PK>(dbEntity, columnValueMap, columnConditionMap);		
		return update;			
	}
	/*
	 * TODO MULTINUCLET: deprecated
	private static Map<String, Object> makeMap(String column1, Object value1, Object...varargs) {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		map.put(column1, value1);
		for (int i = 0; i < varargs.length; i += 2)
			map.put((String) varargs[i], varargs[i+1]);
		return map;
	}
	*/
	
	private static <F> DbMap makeMap(final EntityMeta<?> entity, DbField<F> entityField1, Object value1, Object...varargs) {
		final DbMap dbMap = new DbMap();
		dbMap.putUnsafe(entityField1, value1);
		for (int i = 0; i < varargs.length; i += 2) {
			DbField<?> dbField;
			if (varargs[i] instanceof DbField) {
				dbField = (DbField<?>) varargs[i];
			} else {
				dbField = SimpleDbField.create((String)varargs[i], varargs[i+1].getClass());
			}
			dbMap.putUnsafe(dbField, varargs[i+1]);
		}
		return dbMap;
	}
	
}
