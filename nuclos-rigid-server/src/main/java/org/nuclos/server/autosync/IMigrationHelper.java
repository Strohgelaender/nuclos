//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.Collection;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.server.dblayer.PersistentDbAccess;

public interface IMigrationHelper {

	public Collection<RigidEO> getAll(EntityMeta<?> meta);
	
	public Collection<Object> getAllPks(EntityMeta<?> meta);
	
	public RigidEO getByPrimaryKey(EntityMeta<?> meta, Object pk);
	
	public Collection<RigidEO> getByField(EntityMeta<?> meta, UID field, Object value);
	
	public Collection<RigidEO> getByField(EntityMeta<?> meta, FieldMeta<?> field, Object value);
	
	public Collection<RigidEO> getBySysField(FieldMeta<?> field, Object value);

	public void updateAll(EntityMeta<?> meta, Collection<RigidEO> values);
	
	public void update(EntityMeta<?> meta, RigidEO values); 
	
	public void delete(EntityMeta<?> meta, RigidEO value);
	
	public void insert(EntityMeta<?> meta, RigidEO value);
	
	/**
	 * Renames a db table
	 * 
	 * Only supported from helper for db migrations.
	 * Helper for nuclet migrations throws NotSupportedException.
	 * 
	 * 
	 * @param currentName
	 * @param newName
	 */
	public void renameTable(String currentName, String newName) throws NotSupportedException;

	public PersistentDbAccess getDbAccess();

}
