//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.package org.nuclos.server.statemodel.ejb3;
package org.nuclos.server.autosync;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Predicate;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.MetaDbEntityWrapper;
import org.nuclos.server.dblayer.MetaDbFieldWrapper;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.MetaDbProvider;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.impl.SchemaUtils;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.structure.DbArtifact;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbPrimaryKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbUniqueConstraint;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.util.StatementToStringVisitor;

public class ConstraintHelper {
	
	public static class ConstraintPredicate implements Predicate<DbArtifact> {

		private final boolean reverse;
		private final boolean primary;
		private final boolean unique;
		private final boolean foreign;
		
		/**
		 * 
		 * @param unique (select unique constraints?)
		 * @param foreign (select foreign key constraints?)
		 */
		public ConstraintPredicate(boolean unique,boolean foreign) {
			this(false, false, unique, foreign);
		}
		
		/**
		 * 
		 * @param reverse (true, if you want all others)
		 * @param primary (select primary constraints?)
		 * @param unique (select unique constraints?)
		 * @param foreign (select foreign key constraints?)
		 */
		public ConstraintPredicate(boolean reverse, boolean primary, boolean unique, boolean foreign) {
			super();
			this.reverse = reverse;
			this.primary = primary;
			this.unique = unique;
			this.foreign = foreign;
		}

		@Override
		public boolean evaluate(DbArtifact dba) {
			if (primary && dba instanceof DbPrimaryKeyConstraint) {
				return reverse? false: true;
			} else if (unique && dba instanceof DbUniqueConstraint) {
				return reverse? false: true;
			} else if (foreign && dba instanceof DbForeignKeyConstraint) {
				return reverse? false: true;
			}
			return reverse? true: false;
		}
		
	}
	
	public static synchronized void removeConstraints(
		boolean primary, boolean unique, boolean foreign,
		Collection<UID> involvedFields, Collection<MetaDbEntityWrapper> allEntities,
		Collection<MetaDbFieldWrapper> allFields, SysEntities sysEntities,
		Logger log) {
		PersistentDbAccess dbAccess = new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess());
		for (DbStructureChange constraint : SchemaUtils.drop(
				getConstraints(primary, unique, foreign, involvedFields, allEntities, allFields, sysEntities, dbAccess))) {
			try {
				removeConstraint("drop involved constraints", constraint, dbAccess, log);
			} catch (Exception ex) {
				log.warn("constraint {} not dropped: {}", constraint.getArtifact1().getSimpleName(), ex.getMessage());
			}
		}
	}
	
	private static void removeConstraint(
		String description,
		DbStructureChange constraint,
		PersistentDbAccess dbAccess,
		Logger log) throws Exception {
		StatementToStringVisitor toStringVisitor = new StatementToStringVisitor();
		log.debug("{}: {}", description, constraint.accept(toStringVisitor));
		dbAccess.execute(constraint);
	}
	
	public static synchronized void createConstraints(
		boolean primary,
		boolean unique,
		boolean foreign,
		Collection<UID> involvedFields,
		Collection<MetaDbEntityWrapper> allEntities,
		Collection<MetaDbFieldWrapper> allFields,
		SysEntities sysEntities,
		Logger log) {
		PersistentDbAccess dbAccess = new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess());
		for (DbStructureChange constraint : SchemaUtils.create(
				getConstraints(primary, unique, foreign, involvedFields, allEntities, allFields, sysEntities, dbAccess))) {
			try {
				createConstraint("create involved constraints", constraint, dbAccess, log);
			} catch (Exception ex) {
				StatementToStringVisitor toStringVisitor = new StatementToStringVisitor();
				String sql = "";
				try {
					sql = constraint.accept(toStringVisitor);
				} catch (SQLException e) {
					log.error(e.getMessage(), e);
				}
				log.warn("constraint {} not created: {} ({})", constraint.getArtifact2().getSimpleName(), ex.getMessage(), sql);
			}
		}
	}
	
	private static void createConstraint(
		String description,
		DbStructureChange constraint,
		PersistentDbAccess dbAccess,
		Logger log) throws Exception {
		StatementToStringVisitor toStringVisitor = new StatementToStringVisitor();
		log.debug("{}: {}", description, constraint.accept(toStringVisitor));
		dbAccess.execute(constraint);
	}
	
	private static List<DbConstraint> getConstraints(
		boolean primary,
		boolean unique,
		boolean foreign,
		Collection<UID> involvedFields,
		Collection<MetaDbEntityWrapper> allEntities,
		Collection<MetaDbFieldWrapper> allFields,
		SysEntities sysEntities,
		PersistentDbAccess dbAccess) {
		final MetaDbProvider provider = new MetaDbProvider(allEntities, allFields, sysEntities);
		final MetaDbHelper helper = new MetaDbHelper(sysEntities._getSchemaHelperVersion(), dbAccess, provider);
		
		final List<DbConstraint> result = new ArrayList<>();
		final Map<MetaDbEntityWrapper, Collection<MetaDbFieldWrapper>> involvedConstraints =
			new HashMap<>();
			
		for (UID ivf : involvedFields) {
			final MetaDbFieldWrapper fWrapper = provider.getEntityField(ivf);
			final MetaDbEntityWrapper eWrapper = provider.getEntity(fWrapper.getEntity());
			Collection<MetaDbFieldWrapper> fList = involvedConstraints.get(eWrapper);
			if (fList == null) {
				fList = new ArrayList<>();
				involvedConstraints.put(eWrapper, fList);
			}
			fList.add(fWrapper);
		}
		
		for (MetaDbEntityWrapper eWrapper : involvedConstraints.keySet()) {
			final Collection<MetaDbFieldWrapper> fList = involvedConstraints.get(eWrapper);
			final List<DbConstraint> temp = new ArrayList<DbConstraint>();
			final DbTable dbtable = helper.getDbTable(eWrapper);
			if (dbtable == null) {
				continue;
			}
			if (foreign) temp.addAll(dbtable.getTableArtifacts(DbForeignKeyConstraint.class));
			if (unique) temp.addAll(dbtable.getTableArtifacts(DbUniqueConstraint.class));
			for (DbConstraint constraint : temp) {
				boolean add = false;
				for (String concol : constraint.getColumnNames()) {
					for (MetaDbFieldWrapper fWrapper : fList) {
						final String dbField;
						if (fWrapper.getForeignEntity() != null) {
							MetaDbEntityWrapper foreignEntity = provider.getEntity(fWrapper.getForeignEntity());
							dbField = MetaDbHelper.getDbRefColumn(foreignEntity, fWrapper);
						} else {
							dbField = fWrapper.getDbColumn();
						}
						if (StringUtils.equalsIgnoreCase(concol, dbField)) {
							add = true;
						}
					}
				}
				if (add) {
					result.add(constraint);
				}
			}
			if (primary) {
				result.addAll(dbtable.getTableArtifacts(DbPrimaryKeyConstraint.class));
			}
		}
		
		return result;
	}
	
}
