package org.nuclos.server.autosync.migration.m_04_00_00;

import java.io.IOException;
import java.io.InputStream;
import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

class Prefs_DOMPreferencesFactory implements PreferencesFactory {
	
	private static final Logger LOG = Logger.getLogger(Prefs_DOMPreferencesFactory.class);
	
	private static final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	
	private static final TransformerFactory tf = TransformerFactory.newInstance();
	
	public Prefs_DOMPreferencesFactory() {
		dbf.setNamespaceAware(false);
		dbf.setValidating(false);
		dbf.setCoalescing(false);
		dbf.setIgnoringComments(true);
		dbf.setIgnoringElementContentWhitespace(true);
		dbf.setXIncludeAware(false);
/*		try {
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		}
		catch (ParserConfigurationException e) {
			throw new ExceptionInInitializerError(e);
		}*/
	}

	@Override
	public Preferences systemRoot() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Preferences userRoot() {
		final DocumentBuilder db = newDocumentBuilder();
		final Document document = db.newDocument();
		// http://stackoverflow.com/questions/1409708/how-can-produce-a-doctype-declaration-with-dom-level-3-serialization-api
		final DocumentType dt = db.getDOMImplementation().createDocumentType(
				"preferences", null, "http://java.sun.com/dtd/preferences.dtd");
		document.appendChild(dt);
		final Prefs_DOMPreferences result = new Prefs_DOMPreferences(document, true);
		return result;
	}
	
	public void write(Prefs_DOMPreferences prefs, Result result, boolean pretty) throws IOException {
		Transformer trans;
		try {
			trans = tf.newTransformer();
			// http://stackoverflow.com/questions/1264849/pretty-printing-output-from-javax-xml-transform-transformer-with-only-standard-j
			if (pretty) {
				trans.setOutputProperty(OutputKeys.INDENT, "yes");
				trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			}
			// http://stackoverflow.com/questions/3509860/how-can-i-retain-doctype-info-in-xml-documnet-during-read-write-process
			// http://www.java-forum.org/xml-co/97353-doctype-dom-nutzen.html#post618998
			final Document document = prefs.getDOMDocument();
			final String docType = document.getDoctype().getSystemId();
            final String entities = document.getDoctype().getInternalSubset();
 
            if (docType != null) {
            	trans.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, docType);
            }
            if (entities != null) {
            	trans.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, entities);
            }
            
			trans.transform(new DOMSource(prefs.getDOMDocument()), result);
		}
		catch (TransformerConfigurationException e) {
			throw new IllegalStateException(e);
		}
		catch (TransformerException e) {
			throw new IOException(e);
		}
	}
	
	public Prefs_DOMPreferences read(InputStream in) throws IOException {
		return new Prefs_DOMPreferences(newDocumentBuilder(), in, true);
	}
	
	private DocumentBuilder newDocumentBuilder() {
		DocumentBuilder result;
		try {
			result = dbf.newDocumentBuilder();
		}
		catch (ParserConfigurationException e) {
			throw new IllegalStateException(e);
		}
		result.setErrorHandler(new MyErrorHandler());
		return result;
	}
	
	private static class MyErrorHandler implements ErrorHandler {
		
		private MyErrorHandler() {
		}

		@Override
		public void warning(SAXParseException exception) throws SAXException {
			LOG.warn(exception.toString());
		}

		@Override
		public void error(SAXParseException exception) throws SAXException {
			LOG.warn(exception.toString(), exception);
		}

		@Override
		public void fatalError(SAXParseException exception) throws SAXException {
			LOG.error(exception.toString(), exception);
		}
		
	}
	
}
