//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import static org.nuclos.server.autosync.MigrationUtils.getSchemaVersions;
import static org.nuclos.server.autosync.MigrationUtils.getStaticsVersions;
import static org.nuclos.server.autosync.MigrationUtils.getSysEntities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidE;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SysEntities;
import org.nuclos.common.Version;
import org.nuclos.common.VersionNumber;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.server.autosync.migration.AbstractMigration;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SystemMetaProvider;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.MetaDbEntityWrapper;
import org.nuclos.server.dblayer.MetaDbFieldWrapper;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.expression.DbCurrentDate;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.impl.SchemaUtils;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbStatement;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.structure.DbArtifact;
import org.nuclos.server.dblayer.structure.DbColumn;
import org.nuclos.server.dblayer.structure.DbConstraint.DbPrimaryKeyConstraint;
import org.nuclos.server.dblayer.structure.DbNullable;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.structure.DbTableArtifact;
import org.nuclos.server.dblayer.structure.DbTableData;
import org.nuclos.server.dblayer.structure.DbTableType;
import org.nuclos.server.dblayer.util.DbArtifactXmlReader;
import org.nuclos.server.dblayer.util.StatementToStringVisitor;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * !Beware that all methods of this class call SQL-Statements that run in a single transaction and therefore cannot be roll-backed!
 *
 */
@Configurable
public class AutoDbSetup {
	
	private static final Logger LOG = Logger.getLogger(AutoDbSetup.class);

	private static final String NUCLOS = "NUCLOS";
	
	private final PersistentDbAccess dbAccess;
	
	private final SysEntities nuclosEntities;
	
	private final Version nuclosVersion;
	
	public final String nuclosStaticsVersion = "2.8";
	
	public static class Schema extends Pair<Collection<DbArtifact>, Collection<DbTableData>> {

		public Schema(Collection<DbArtifact> artifacts, Collection<DbTableData> dumps) {
			super(artifacts, dumps);
		}
		
		public Collection<DbArtifact> getArtifacts() {
			return x;
		}

		public Collection<DbTableData> getDumps() {
			return y;
		}
	}


	public static class ReleaseInfo {

		private String release;
		private Date releaseDate;
		private String schemaversion;
		private Schema schema;

		public ReleaseInfo(String release, Date releaseDate, String schemaversion, Schema schema) {
			this.release = release;
			this.releaseDate = releaseDate;
			this.schemaversion = schemaversion;
			this.schema = schema;
		}

		public String getReleaseString() {
			return release;
		}

		public Date getReleaseDate() {
			return releaseDate;
		}

		public String getSchemaVersion() {
			return schemaversion;
		}

		public Schema getSchema() {
			return schema;
		}
	}

	public AutoDbSetup(DbAccess dbAccess, SysEntities nuclosEntities, Version nuclosVersion) {
		this.dbAccess = new PersistentDbAccess(dbAccess);
		this.nuclosEntities = nuclosEntities;
		this.nuclosVersion = nuclosVersion;
	}
	
	public void run() throws Exception {
		Pair<String, Date> version = null;
		try {
			version = determineLastVersion();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		boolean installed = checkIsInstalled();
		if (version != null) {
			LOG.info(String.format("Nuclos version found in schema %s: %s (%s)", dbAccess.getSchemaName(), version.x, version.y));
			if (!installed) {
				LOG.warn("Cannot detect Nuclos schema via table existence");
			}
			installed = true;

			if (NuclosSystemParameters.is(NuclosSystemParameters.AUTOSETUP_VERSION_CHECK_ENABLED, true)) {
				// Check that the installed version not newer than this Nuclos server version
				String thisVersion = nuclosEntities._getSchemaVersion();
				if (VersionNumber.compare(version.x, thisVersion) > 0) {
					throw new NuclosFatalException(String.format("Found schema is newer than this Nuclos server: %s > %s", version.x, thisVersion));
				}
			} else {
				LOG.warn("WARNING <<<<<<<< AUTOSETUP VERSION CHECK IS    D I S A B L E D   >>>>>>>> WARNING");
				LOG.warn("WARNING <<<<<<<< Database downgrade is possible (with unknown side effects)!");
			}
		}

		if (!installed) {
			runInitial();
		} else {
			if (version != null) {
				runUpdate(version.x);
			}
		}
	}
	
	private void runInitial() throws Exception {
		LOG.info("No Nuclos installation found in schema " + dbAccess.getSchemaName());
		List<DbStatement> statements = getSetupStatements();
		executeSetupStatements("Auto-Init", statements, false);
	}
	
	private void runUpdate(String installedVersion) throws Exception {
		String migrationVersion = runMigrations(installedVersion);
		
		if (!RigidUtils.equal(migrationVersion, installedVersion)) {
			List<DbStatement> statements = getUpdateStatementsFromDB(StringUtils.defaultString(migrationVersion, installedVersion), nuclosEntities, nuclosStaticsVersion, nuclosEntities._getSchemaVersion(), null);
			if (statements != null) {
				executeSetupStatements("Auto-Update", statements, false);
			}
		}
	}
	
	private String runMigrations(String installedVersion) throws Exception {
		String result = null;
		
		if (NuclosSystemParameters.is(NuclosSystemParameters.MIGRATION_ENABLED, true)) {
			for (AbstractMigration migration : MigrationUtils.getMigrations(installedVersion)) {
				String migrationVersion = runMigration(migration, StringUtils.defaultString(result, installedVersion));
				if (migrationVersion != null) {
					result = migrationVersion;
				}
			}
		}
		
		return result;
	}
	
	private void initMigration(AbstractMigration migration) {
		final SysEntities sourceMeta = migration.getSourceMeta();
		final MigrationDbHelper helper = new MigrationDbHelper(sourceMeta, dbAccess);
		final MigrationContext context = new MigrationContext(MigrationContext.Type.DB, helper, dbAccess);
		migration.init(context);
	}
	
	private String runMigration(AbstractMigration migration, String currentDbVersion) throws Exception {
		
		initMigration(migration);
		
		final Pair<SysEntities, SysEntities> sysEntities = getSysEntities(migration);
		final Pair<String, String> schemaVersions = getSchemaVersions(sysEntities);
		final Pair<String, String> staticsVersions = getStaticsVersions(migration);
		
		int sourceCompare = VersionNumber.compare(MigrationUtils.getPureVersion(currentDbVersion), schemaVersions.x);
		if (sourceCompare <= 0) {
			runMigrationInitDDL(currentDbVersion, sysEntities.x, staticsVersions.x);
		} else if (sourceCompare > 0) {
			throw new NuclosFatalException(String.format("Only updates are allowed [source=%s, target=%s, class=%s", 
					currentDbVersion, schemaVersions.x, migration.getClass().getSimpleName()));
		}
		
		runMigration(migration, sysEntities.x);
		
		int targetCompare = VersionNumber.compare(schemaVersions.x, schemaVersions.y);
		if (targetCompare <= 0) {
			runMigrationCompleteDDL(migration.getMigrationName(), sysEntities.x, staticsVersions.x, sysEntities.y, staticsVersions.y);
		} else {
			throw new NuclosFatalException(String.format("Only updates are allowed [source=%s, target=%s, class=%s", 
					schemaVersions.x, schemaVersions.y, migration.getClass().getSimpleName()));
		}
		
		return schemaVersions.y;
	}
	
	/**
	 * fill in the initial database schema definition and data for Nuclos for the
	 * given database.
	 * The complete schema is assembled from different sources: a general schema
	 * definition, database-specific amendments (e.g. functions and procedures),
	 * schema definitions and data for Quartz (which are also database-specific),
	 * schema definitions derived from current metadata of the system entities,
	 * and the data for the initial user.
	 */
	private Pair<List<DbArtifact>, List<DbTableData>> getSchemaStatics(String version) {
		final List<DbArtifact> staticArtifacts = new ArrayList<DbArtifact>();		
		final List<DbTableData> staticDumps = new ArrayList<DbTableData>();
		
		String typeId = StringUtils.lowerCase(dbAccess.getDbType().name());
		String[] resources = {
			String.format("resources/db/v%1$s/nuclos-base.xml", version),
			String.format("resources/db/v%1$s/nuclos-user.xml", version),
			String.format("resources/db/v%1$s/%2$s/nuclos-%2$s.xml", version, typeId),
			String.format("resources/db/v%1$s/%2$s/quartz-%2$s.xml", version, typeId)
		};

		DbArtifactXmlReader reader = new DbArtifactXmlReader();
		for (String res : resources) {
			InputStream is = getClass().getClassLoader().getResourceAsStream(res);
			if (is == null) {
				throw new NuclosFatalException("Initial schema resource " + res + " not found");				
			}
			try {
				reader.read(is);
			} catch (IOException ex) {
				throw new NuclosFatalException("Error loading schema resource " + res, ex);
			}
		}
		
		staticArtifacts.addAll(reader.getArtifacts());
		staticDumps.addAll(reader.getDumps());
		
		Pair<List<DbArtifact>, List<DbTableData>> result = new Pair<List<DbArtifact>, List<DbTableData>>(staticArtifacts, staticDumps);
		return result;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	private void runMigration(AbstractMigration migration, SysEntities sysEntities) {
		migration.migrate();
	}
	
	public static abstract class UpdateStatementsPredicate implements Predicate<DbArtifact> {
		private Schema sourceSchema;
		private List<DbStructureChange> preview;
		public final void setSourceSchema(Schema schema) {
			this.sourceSchema = schema;
		}
		public final Schema getSourceSchema() {
			return this.sourceSchema;
		}
		public final List<DbStructureChange> getPreview() {
			return preview;
		}
		public final void setPreview(List<DbStructureChange> preview) {
			this.preview = preview;
		}
		public final DbStructureChange getDbStructureChangeFromPreview(DbArtifact dba, DbStructureChange.Type...types) {
			if (dba == null) {
				return null;
			}
			DbStructureChange result = null;
			boolean create = false;
			boolean modify = false;
			boolean drop = false;
			for (DbStructureChange.Type t : types) {
				switch (t) {
				case CREATE: create = true; break;
				case MODIFY: modify = true; break;
				case DROP: drop = true; break;
				}
			}
			for (DbStructureChange dbsc : preview) {
				DbArtifact check = null;
				if (dbsc.getType() == DbStructureChange.Type.CREATE && create) {
					check = dbsc.getArtifact2();
				}
				if ((dbsc.getType() == DbStructureChange.Type.MODIFY && modify) ||
					(dbsc.getType() == DbStructureChange.Type.DROP && drop)) {
					check = dbsc.getArtifact1();
				}
				if (check != null) {
					if (RigidUtils.equal(dba.getArtifactName(), check.getArtifactName()) &&
						dba.isSameType(check)) {
						if (result != null) {
							StatementToStringVisitor statement1 = new StatementToStringVisitor();
							String s1 = statement1.visitStructureChange(result);
							StatementToStringVisitor statement2 = new StatementToStringVisitor();
							String s2 = statement2.visitStructureChange(dbsc);
							
							throw new NuclosFatalException(String.format("DbArtifact %s is changed more than once:   \n%s   \n%s", dba.getArtifactName(), s1, s2));
						}
						result = dbsc;
					}
				}
			}
			return result;
		}
	}
	
	/**
	 * remove UNIQUE and 
	 * FOREIGN keys...
	 */
	private static final Predicate<DbArtifact> MIGRATION_INIT_PREDICATE = new ConstraintHelper.ConstraintPredicate(true, false, true, true);
	
	/**
	 * extends MIGRATION_INIT_PREDICATE
	 * 
	 * prevent new NOT NULL constraints and
	 * new PRIMARY keys...
	 */
	private static final UpdateStatementsPredicate PRE_VALIDATION_PREDICATE = new UpdateStatementsPredicate() {
		@Override
		public boolean evaluate(DbArtifact dba) {
			if (MIGRATION_INIT_PREDICATE.evaluate(dba)) {
				if (dba instanceof DbTable) {
					DbTable targetTable = (DbTable) dba;
					Iterator<DbTableArtifact> artifacts = targetTable.getTableArtifacts().iterator();
					while (artifacts.hasNext()) {
						DbTableArtifact dbta = artifacts.next();
						
						// prevent new PRIMARY keys
						if (dbta instanceof DbPrimaryKeyConstraint) {
							DbStructureChange previewChange = getDbStructureChangeFromPreview(dbta, DbStructureChange.Type.CREATE);
							if (previewChange != null) {
								artifacts.remove();
							}
						}
						
						// prevent new NOT NULL constraints
						else if (dbta instanceof DbColumn) {
							DbColumn column = (DbColumn) dbta;
							DbStructureChange previewChange = getDbStructureChangeFromPreview(column, DbStructureChange.Type.CREATE, DbStructureChange.Type.MODIFY);
							if (previewChange != null) {
								final DbColumn dbColumn2 = (DbColumn) previewChange.getArtifact2();
								if (dbColumn2.getNullable() == DbNullable.NOT_NULL) {
									boolean setNullable = false;
									if (previewChange.getType() == DbStructureChange.Type.CREATE) {
										setNullable = true;
									} else {
										final DbColumn dbColumn1 = (DbColumn) previewChange.getArtifact1();
										if (dbColumn1.getNullable() == DbNullable.NULL) {
											setNullable = true;
										}
									}
									if (setNullable) {
										column.setNullable(DbNullable.NULL);
									}
								}
							}
						}
					}
				}
				return true;
			}
			return false;
		}
		private DbTable getSourceTable(String tableName) {
			for (DbArtifact dba : getSourceSchema().getArtifacts()) {
				if (dba instanceof DbTable) {
					if (StringUtils.equalsIgnoreCase(((DbTable) dba).getTableName(), tableName)) {
						return (DbTable) dba;
					}
				}
			}
			return null;
		}
	};
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	private void runMigrationInitDDL(String currentDbVersion, SysEntities targetSystem, String targetStaticsVersion) throws Exception {
		String migVersion = targetSystem._getSchemaVersion();
		if (!migVersion.endsWith(MigrationUtils.VERSION_MIG_SUFFIX)) {
			migVersion += MigrationUtils.VERSION_MIG_SUFFIX; //"(mig)";
		}
		
		List<DbStatement> statements = getUpdateStatementsFromDB(
				currentDbVersion, 
				targetSystem, targetStaticsVersion, migVersion, MIGRATION_INIT_PREDICATE);
		if (statements != null) {
			LOG.info(String.format("Run pre migration update from version %s to version %s", currentDbVersion, migVersion));
			executeSetupStatements("Auto-Migration init " + targetSystem._getSchemaVersion(), statements, false);
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	private void runMigrationCompleteDDL(String migrationName, SysEntities sourceSystem, String sourceStaticsVersion, SysEntities targetSystem, String targetStaticsVersion) throws Exception {
		String validateVersion = targetSystem._getSchemaVersion()+MigrationUtils.VERSION_VAL_SUFFIX;//"(val)";
		String completeVersion = targetSystem._getSchemaVersion();
		LOG.info(String.format("Run migration update from version %s to final version %s", sourceSystem._getSchemaVersion(), targetSystem._getSchemaVersion()));
		
		Update update = getUpdate(
				sourceSystem, sourceStaticsVersion, sourceSystem._getSchemaVersion(), MIGRATION_INIT_PREDICATE, 
				targetSystem, targetStaticsVersion, validateVersion, PRE_VALIDATION_PREDICATE,
				true);
		executeSetupStatements("Auto-Migration validation " + validateVersion, update.statements, false);
		
		SchemaHelper helper = new SchemaHelper(getSchema(targetSystem, targetStaticsVersion, null), completeVersion, targetSystem, dbAccess);
		helper.validate(migrationName + "-schema-validation");
		
		update = getUpdate(
				update.targetSchema, 
				targetSystem, targetStaticsVersion, completeVersion, null, true);
		executeSetupStatements("Auto-Migration complete " + targetSystem._getSchemaVersion(), update.statements, false);
	}

	/**
	 * Checks whether Nuclos is already set-up on the given database.
	 */
	public boolean checkIsInstalled() {
		// we check for the T_AD_RELEASE (ignore case) table
		TreeSet<String> tableNames = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		tableNames.addAll(dbAccess.getTableNames(DbTableType.TABLE));
		return tableNames.contains(RigidE.RELEASEHISTORY.getDbTable());
	}

	public Pair<String, Date> determineLastVersion() {
		return RigidUtils.getFirst(getInstalledVersions(), null);
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, noRollbackFor= {Exception.class})
	public List<Pair<String, Date>> getInstalledVersions() {
		final DbQueryBuilder builder = dbAccess.getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		final DbFrom<Long> t = query.from(RigidE.RELEASEHISTORY);
		query.multiselect(
			t.baseColumn(RigidE.RELEASEHISTORY.release),
			t.baseColumn(RigidE.RELEASEHISTORY.delivered));
		query.where(builder.equalValue(t.baseColumn(RigidE.RELEASEHISTORY.application), NUCLOS));
		query.orderBy(
			//builder.desc(t.baseColumn("STRRELEASE", String.class))); // order by STRRELEASE does not respect natural sort oder @see NUCLOS-1321 
				builder.desc(t.baseColumn(SF.PK_ID))); 
		return dbAccess.executeQuery(query, new Transformer<DbTuple, Pair<String, Date>>() {
			@Override
			public Pair<String, Date> transform(DbTuple tuple) {
				String releaseString = tuple.get(0, String.class);
				Date releaseDate = tuple.get(1, Date.class);
				return Pair.makePair(releaseString, releaseDate);
			}
		});
	}

	public Schema getInstalledSchemaFor(String release) {
		final DbQueryBuilder builder = dbAccess.getQueryBuilder();
		final DbQuery<byte[]> query = builder.createQuery(byte[].class);
		final DbFrom<Long> t = query.from(RigidE.RELEASEHISTORY);
		query.select(t.baseColumn(RigidE.RELEASEHISTORY.data));
		query.where(builder.and(
			builder.equalValue(t.baseColumn(RigidE.RELEASEHISTORY.application), NUCLOS),
			builder.equalValue(t.baseColumn(RigidE.RELEASEHISTORY.release), release)));
		byte[] b = RigidUtils.getFirst(dbAccess.executeQuery(query), null);
		if (b != null && b.length > 0) {
			return AutoDbSetupUtils.uncompressSchema(b);
		}
		return null;
	}
	
	public void removeSysConstraints(boolean unique, boolean foreign, String reason) throws Exception {
		final Predicate<DbArtifact> predicateWith = new ConstraintHelper.ConstraintPredicate(true, false, false, false);
		final Predicate<DbArtifact> predicateWithout = new ConstraintHelper.ConstraintPredicate(true, false, unique, foreign);
		
		Update update = getUpdate(
				nuclosEntities, nuclosStaticsVersion, nuclosEntities._getSchemaVersion(), predicateWith, 
				nuclosEntities, nuclosStaticsVersion, nuclosEntities._getSchemaVersion(), new UpdateStatementsPredicate() {	
					@Override
					public boolean evaluate(DbArtifact t) {
						return predicateWithout.evaluate(t);
					}
				},
				false /*no release statement*/);
		executeSetupStatementsAndIgnoreErrors("Removing SYS constraints for " + reason, update.statements);
	}
	
	public void createSysConstraints(boolean unique, boolean foreign, String reason) throws Exception {
		final Predicate<DbArtifact> predicateWith = new ConstraintHelper.ConstraintPredicate(true, false, false, false);
		final Predicate<DbArtifact> predicateWithout = new ConstraintHelper.ConstraintPredicate(true, false, unique, foreign);
		
		Update update = getUpdate(
				nuclosEntities, nuclosStaticsVersion, nuclosEntities._getSchemaVersion(), predicateWithout, 
				nuclosEntities, nuclosStaticsVersion, nuclosEntities._getSchemaVersion(), new UpdateStatementsPredicate() {	
					@Override
					public boolean evaluate(DbArtifact t) {
						return predicateWith.evaluate(t);
					}
				},
				false /*no release statement*/);
		dbAccess.commit();
		executeSetupStatementsAndIgnoreErrors("Creating SYS constraints for " + reason, update.statements);
	}
		
	public Schema getSchema(SysEntities sysEntities, String staticsVersion, Predicate<DbArtifact> predicate) {
		Pair<List<DbArtifact>, List<DbTableData>> statics = getSchemaStatics(staticsVersion);
		List<DbArtifact> artifacts = new ArrayList<DbArtifact>();
		if (predicate == null) {
			artifacts.addAll(statics.x);
		} else {
			for (DbArtifact dba : statics.x) {
				if (predicate.evaluate(dba)) {
					artifacts.add(dba);
					if (dba instanceof DbTable) {
						Iterator<DbTableArtifact> it = ((DbTable) dba).getTableArtifacts().iterator();
						while (it.hasNext()) {
							if (!predicate.evaluate(it.next())) {
								it.remove();
							}
						}
					}
				}
			}
		}

		MetaDbHelper eoHelper = new MetaDbHelper(sysEntities._getSchemaHelperVersion(), dbAccess, new SystemMetaProvider(sysEntities), null, null);
		artifacts.addAll(eoHelper.getSchema(predicate).values());

		return new Schema(artifacts, statics.y);
	}

	private ReleaseInfo getRelease(SysEntities sysEntities, String staticsVersion, String version, Predicate<DbArtifact> predicate) {
		return new ReleaseInfo(nuclosVersion.getVersionNumber(), nuclosVersion.getVersionDate(), version, getSchema(sysEntities, staticsVersion, predicate));
	}

	private List<DbStatement> getSetupStatements() {
		List<DbStatement> statements = new ArrayList<DbStatement>();

		ReleaseInfo releaseInfo = getRelease(nuclosEntities, nuclosStaticsVersion, nuclosEntities._getSchemaVersion(), null);
		
		statements.addAll(SchemaUtils.create(releaseInfo.getSchema().getArtifacts()));
		for (DbTableData data : releaseInfo.getSchema().getDumps()) {
			statements.addAll(data.getStatements(false));
		}
		statements.add(getReleaseStatement(releaseInfo));
		return statements;
	}

	private List<DbStatement> getUpdateStatementsFromDB(String installedSchemaVersion, SysEntities sysEntities, String staticsVersion, String version, Predicate<DbArtifact> predicate) {		
		ReleaseInfo releaseInfo = getRelease(sysEntities, staticsVersion, version, predicate);
		
		if (releaseInfo.getSchemaVersion().equals(installedSchemaVersion)) {
			return null;			
		}

		// Auto-update only considers schema artifacts and _not_ initial data
		Schema installedSchema = getInstalledSchemaFor(installedSchemaVersion);
		List<DbStatement> statements = new ArrayList<DbStatement>();
		statements.addAll(SchemaUtils.modify(installedSchema.getArtifacts(), releaseInfo.schema.getArtifacts(), true));
		statements.add(getReleaseStatement(releaseInfo));
		return statements;
	}
	
	private static class Update {
		final List<DbStatement> statements;
		final Schema sourceSchema;
		final Schema targetSchema;
		public Update (List<DbStatement> statements, Schema sourceSchema, Schema targetSchema) {
			this.statements = statements;
			this.sourceSchema = sourceSchema;
			this.targetSchema = targetSchema;
		}
	}
	
	/**
	 * 
	 * @param sourceSystem
	 * @param sourceStaticsVersion
	 * @param sourcePredicate (handles source DbArtifacts)
	 * @param targetSystem
	 * @param targetStaticsVersion
	 * @param targetPredicate (handles target DbArtifacts)
	 * @return
	 */
	private Update getUpdate(
			SysEntities sourceSystem, 
			String sourceStaticsVersion, 
			String sourceVersion,
			Predicate<DbArtifact> sourcePredicate, 
			SysEntities targetSystem, 
			String targetStaticsVersion, 
			String targetVersion,
			UpdateStatementsPredicate targetPredicate,
			boolean withReleaseStatement) {
		ReleaseInfo releaseInfoSource = getRelease(sourceSystem, sourceStaticsVersion, sourceVersion, sourcePredicate);
		return getUpdate(releaseInfoSource.getSchema(), targetSystem, targetStaticsVersion, targetVersion, targetPredicate, withReleaseStatement);
	}
	
	/**
	 * 
	 * @param sourceSchema
	 * @param targetSystem
	 * @param targetStaticsVersion
	 * @param targetPredicate (handles target DbArtifacts)
	 * @return
	 */
	private Update getUpdate(
			Schema sourceSchema, 
			SysEntities targetSystem, 
			String targetStaticsVersion, 
			String targetVersion,
			UpdateStatementsPredicate targetPredicate,
			boolean withReleaseStatement) {
		if (targetPredicate != null) {
			ReleaseInfo releaseInfoTarget = getRelease(targetSystem, targetStaticsVersion, targetVersion, null);
			List<DbStructureChange> preview = SchemaUtils.modify(sourceSchema.getArtifacts(), releaseInfoTarget.schema.getArtifacts(), true);
			targetPredicate.setSourceSchema(sourceSchema);
			targetPredicate.setPreview(preview);
		}
		ReleaseInfo releaseInfoTarget = getRelease(targetSystem, targetStaticsVersion, targetVersion, targetPredicate);

		List<DbStatement> statements = new ArrayList<DbStatement>();
		statements.addAll(SchemaUtils.modify(sourceSchema.getArtifacts(), releaseInfoTarget.schema.getArtifacts(), true));
		if (withReleaseStatement) {
			statements.add(getReleaseStatement(releaseInfoTarget));
		}
		return new Update(statements, sourceSchema, releaseInfoTarget.schema);
	}

	private DbStatement getReleaseStatement(ReleaseInfo releaseInfo) {
		String releaseString = releaseInfo.getReleaseString();
		Date releaseDate = releaseInfo.getReleaseDate();
		if (releaseString == null) {
			throw new NuclosFatalException("releaseString must not be null");
		}
		return DbStatementUtils.insertIntoUnsafe(RigidE.RELEASEHISTORY,
			RigidE.RELEASEHISTORY.getPk(), new DbId(),
			RigidE.RELEASEHISTORY.application, NUCLOS,
			RigidE.RELEASEHISTORY.release, releaseInfo.getSchemaVersion(),
			RigidE.RELEASEHISTORY.description, "Nuclos " + releaseString,
			RigidE.RELEASEHISTORY.delivered, releaseDate != null ? releaseDate : DbCurrentDate.CURRENT_DATE,
			RigidE.RELEASEHISTORY.installed, DbCurrentDate.CURRENT_DATE,
			 SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME,
			 SF.CREATEDBY, "AUTOSETUP",
			 SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME,
			 SF.CHANGEDBY, "AUTOSETUP",
			 SF.VERSION, 1,
			 RigidE.RELEASEHISTORY.data, releaseInfo.getSchema() != null ? AutoDbSetupUtils.compressSchema(releaseInfo.getSchema()) : DbNull.forType(byte[].class));
	}
	
	private void executeSetupStatementsAndIgnoreErrors(String type, List<DbStatement> stmts) {
		try {
			executeSetupStatements(type, stmts, true);
		} catch (Exception ex) {
			// ignore here. logged already
		}
	}
	
	private void executeSetupStatements(String type, List<DbStatement> stmts, boolean ignoreErrors) throws Exception {
		LOG.info("Starting " + type);
		LOG.info(type + ": " + stmts.size() + " statement(s) to execute...");

		int stmtNo = 1;
		String stmtDescription = null;
		StatementToStringVisitor toStringVisitor = new StatementToStringVisitor();
		try {
			for (DbStatement stmt : stmts) {
				stmtDescription = type + " #" + stmtNo;
				// this is intentionally a separate assignment; if something went
				// wrong inside the visitor, the number is already set
				stmtDescription = stmtDescription + ": " + stmt.accept(toStringVisitor);
				LOG.info(stmtDescription + "...");
				try {
					dbAccess.execute(stmt);
				} catch (Exception e) {
					String message = "Error during " + stmtDescription + ": " + e.toString();
					LOG.fatal(message, e);
					if (!ignoreErrors) {
						throw e;
					}
				}
				stmtNo++;
			}
			LOG.info(type + " finished successfully");
		} catch (Exception e) {
			throw new NuclosFatalException(e);
		}
	}
}
