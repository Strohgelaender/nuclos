package org.nuclos.server.autosync.migration.m_04_00_00;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.Logger;
import org.nuclos.common.SF;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.server.autosync.ConstraintHelper;
import org.nuclos.server.autosync.IMigrationHelper;
import org.nuclos.server.autosync.IndexHelper;
import org.nuclos.server.autosync.RigidEO;
import org.nuclos.server.autosync.migration.AbstractMigration;
import org.nuclos.server.autosync.migration.AbstractMigration.Migration;
import org.nuclos.server.dblayer.MetaDbEntityWrapper;
import org.nuclos.server.dblayer.MetaDbFieldWrapper;

/**
 * We need existing primary keys on system tables for foreign keys. 
 * But the "main" migration creates the constraints on finish, so we need this extra migration.
 */
@Migration
public class Migration_04_00_0022_finish extends AbstractMigration {
	
	@Override
	public SysEntities getSourceMeta() {
		return E_04_00_0022_main.getThis();
	}

	@Override
	public String getSourceStatics() {
		return "2.0";
	}

	@Override
	public SysEntities getTargetMeta() {
		return E_04_00_0022_finish.getThis();
	}

	@Override
	public String getTargetStatics() {
		return "2.0";
	}
	
	@Override
	public String getNecessaryUntilSchemaVersion() {
		return "3.99.9999";
	}
	
	private Logger LOG;

	@Override
	public void migrate() {
		IMigrationHelper helper = getContext().getHelper();
		long start = System.currentTimeMillis();
		LOG = getLogger(Migration_04_00_0022_finish.class);
		LOG.info("Migration started");
		
//		DbAccess dbAccess = SpringDataBaseHelper.getInstance().getDbAccess();
		
		// user defined keys and indices
		Collection<MetaDbEntityWrapper> allEntities = new ArrayList<MetaDbEntityWrapper>();
		Collection<MetaDbFieldWrapper> allFields = new ArrayList<MetaDbFieldWrapper>();
		Collection<UID> involvedFields = new ArrayList<UID>();
		
		for (RigidEO entity : helper.getAll(E_04_00_0022_finish.ENTITY)) {
			allEntities.add(new MetaDbEntityWrapper(entity));
			final UID pkFieldUID = SF.PK_ID.getUID((UID)entity.getPrimaryKey());
			final MetaDbFieldWrapper pkFieldWrapper = new MetaDbFieldWrapper(SF.PK_ID.getMetaData((UID)entity.getPrimaryKey()));
			allFields.add(pkFieldWrapper);
			involvedFields.add(pkFieldUID);
			if (Boolean.TRUE.equals(entity.getValue(E_03_15_0004.ENTITY.usessatemodel))) {
				final UID stateFieldUID = SF.STATE_UID.getUID((UID)entity.getPrimaryKey());
				final UID processFieldUID = SF.PROCESS_UID.getUID((UID)entity.getPrimaryKey());
				final MetaDbFieldWrapper stateFieldWrapper = new MetaDbFieldWrapper(SF.STATE_UID.getMetaData((UID)entity.getPrimaryKey()));
				final MetaDbFieldWrapper processFieldWrapper = new MetaDbFieldWrapper(SF.PROCESS_UID.getMetaData((UID)entity.getPrimaryKey()));
				allFields.add(stateFieldWrapper);
				allFields.add(processFieldWrapper);
				involvedFields.add(stateFieldUID);
				involvedFields.add(processFieldUID);
			}
		}
		
		for (RigidEO field : helper.getAll(E_04_00_0022_finish.ENTITYFIELD)) {
			allFields.add(new MetaDbFieldWrapper(field));
			involvedFields.add((UID) field.getPrimaryKey());
		}
		
		LOG.info("Create constraints and indices");
		ConstraintHelper.createConstraints(true, true, true, involvedFields, allEntities, allFields, E_04_00_0022_finish.getThis(), LOG);
		IndexHelper.createIndices(involvedFields, allEntities, allFields, E_04_00_0022_finish.getThis(), LOG);
		LOG.info("Migration finished in " + new DecimalFormat("0.###").format((System.currentTimeMillis()-start)/1000d/60d) + " minutes");
	}

}
