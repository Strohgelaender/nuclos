//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.package org.nuclos.server.statemodel.ejb3;
package org.nuclos.server.autosync;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.MetaDbEntityWrapper;
import org.nuclos.server.dblayer.MetaDbFieldWrapper;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.MetaDbProvider;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.impl.SchemaUtils;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.structure.DbIndex;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.dblayer.util.StatementToStringVisitor;

public class IndexHelper {
	
	public static synchronized void removeIndices(
		Collection<UID> involvedFields,
		Collection<MetaDbEntityWrapper> allEntities,
		Collection<MetaDbFieldWrapper> allFields,
		SysEntities sysEntities,
		Logger log) {
		PersistentDbAccess dbAccess = new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess());
		for (DbStructureChange constraint : SchemaUtils.drop(
				getIndices(involvedFields, allEntities, allFields, sysEntities, dbAccess))) {
			try {
				removeIndex("drop involved index", constraint, dbAccess, log);
			} catch (Exception ex) {
				log.warn("index {} not dropped: {}", constraint.getArtifact1().getSimpleName(), ex.getMessage());
			}
		}
	}
	
	private static void removeIndex(
		String description,
		DbStructureChange constraint,
		PersistentDbAccess dbAccess,
		Logger log) throws Exception {
		StatementToStringVisitor toStringVisitor = new StatementToStringVisitor();
		log.debug("{}: {}", description, constraint.accept(toStringVisitor));
		dbAccess.execute(constraint);
	}
	
	public static synchronized void createIndices(
		Collection<UID> involvedFields,
		Collection<MetaDbEntityWrapper> allEntities,
		Collection<MetaDbFieldWrapper> allFields,
		SysEntities sysEntities,
		Logger log) {
		PersistentDbAccess dbAccess = new PersistentDbAccess(SpringDataBaseHelper.getInstance().getDbAccess());
		for (DbStructureChange constraint : SchemaUtils.create(
				getIndices(involvedFields, allEntities, allFields, sysEntities, dbAccess))) {
			try {
				createIndex("create involved index", constraint, dbAccess, log);
			} catch (Exception ex) {
				StatementToStringVisitor toStringVisitor = new StatementToStringVisitor();
				String sql = "";
				try {
					sql = constraint.accept(toStringVisitor);
				} catch (SQLException e) {
					log.error(e.getMessage(), e);
				}
				log.warn("index {} not created: {} ({})", constraint.getArtifact2().getSimpleName(), ex.getMessage(), sql);
			}
		}
	}
	
	private static void createIndex(
		String description,
		DbStructureChange constraint,
		PersistentDbAccess dbAccess,
		Logger log) throws Exception {
		StatementToStringVisitor toStringVisitor = new StatementToStringVisitor();
		log.debug("{}: {}", description, constraint.accept(toStringVisitor));
		dbAccess.execute(constraint);
	}
	
	private static List<DbIndex> getIndices(
		Collection<UID> involvedFields,
		Collection<MetaDbEntityWrapper> allEntities,
		Collection<MetaDbFieldWrapper> allFields,
		SysEntities sysEntities,
		PersistentDbAccess dbAccess) {
		final MetaDbProvider provider = new MetaDbProvider(allEntities, allFields, sysEntities);
		final MetaDbHelper helper = new MetaDbHelper(sysEntities._getSchemaHelperVersion(), dbAccess, provider);
		
		final List<DbIndex> result = new ArrayList<>();
		final Map<MetaDbEntityWrapper, Collection<MetaDbFieldWrapper>> involvedIndices =
			new HashMap<>();
		
		for (UID ivf : involvedFields) {
			final MetaDbFieldWrapper fWrapper = provider.getEntityField(ivf);
			final MetaDbEntityWrapper eWrapper = provider.getEntity(fWrapper.getEntity());
			Collection<MetaDbFieldWrapper> fList = involvedIndices.get(eWrapper);
			if (fList == null) {
				fList = new ArrayList<>();
				involvedIndices.put(eWrapper, fList);
			}
			fList.add(fWrapper);
		}
		
		for (MetaDbEntityWrapper eWrapper : involvedIndices.keySet()) {
			final Collection<MetaDbFieldWrapper> fList = involvedIndices.get(eWrapper);
			
			DbTable dbtable = helper.getDbTable(eWrapper);
			if (dbtable == null) {
				continue;
			}
			for (DbIndex index : dbtable.getTableArtifacts(DbIndex.class)) {
				boolean add = false;
				for (String indcol : index.getColumnNames()) {
					for (MetaDbFieldWrapper fWrapper : fList) {
						final String dbField;
						if (fWrapper.getForeignEntity() != null) {
							MetaDbEntityWrapper foreignEntity = provider.getEntity(fWrapper.getForeignEntity());
							dbField = MetaDbHelper.getDbRefColumn(foreignEntity, fWrapper);
						} else {
							dbField = fWrapper.getDbColumn();
						}
						if (StringUtils.equalsIgnoreCase(indcol, dbField)) {
							add = true;
						}
					}
				}
				if (add) {
					result.add(index);
				}
			}
		}
		
		return result;
	}
	
}
