#!/bin/bash

# set the following if needed
#KEYSTORE=""
#ALIAS=""

verifyjar() {
	local JAR1="$1"
	shift
	jarsigner -keystore "$KEYSTORE" -strict -verify "$JAR1" "$ALIAS"
	local RET1="$?"
	if [ $RET1 -ne 0 ]; then
		echo "$JAR1 failed to verify: $RET1"
	fi
}

while [ -n "$1" ]; do
	TARGET="$1"
	BASE=`basename $TARGET`
	shift
	if [[ "$TARGET" == *.jar ]]; then
		verifyjar "$TARGET"
		RET="$?"
		echo "$BASE: $RET"
		if [ $? -ne 0 ]; then
			echo "$BASE failed to verify"
		fi
	elif [[ "$TARGET" == *.jar.pack.gz ]]; then
		JAR=`mktemp --suffix=".jar"`
		unpack200 "$TARGET" "$JAR"
		RET="$?"
		echo "$BASE: $RET"
		verifyjar "$JAR"
		if [ $? -ne 0 ]; then
			echo "$BASE failed to verify"
		fi
		rm -f "$JAR"
	else 
		echo "$BASE: skipping (not *.jar nor *.jar.pack.gz)"
	fi
done
