package org.nuclos.layout.transformation.weblayout

import javax.xml.bind.JAXBElement

import org.nuclos.schema.layout.layoutml.Panel
import org.nuclos.schema.layout.layoutml.Tabbedpane
import org.nuclos.schema.layout.layoutml.TabbedpaneConstraints
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebTab
import org.nuclos.schema.layout.web.WebTabcontainer

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class TabbedpaneTransformer extends ElementTransformer<Tabbedpane, WebComponent> {

	TabbedpaneTransformer() {
		super(Tabbedpane.class)
	}

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Tabbedpane tabbedPane) {
		WebTabcontainer result = factory.createWebTabcontainer()

		tabbedPane.container.each { JAXBElement element ->
			layoutTransformer.getWebComponents(element).each {
				WebTab tab = factory.createWebTab()
				Panel panel = (Panel) element.value
				TabbedpaneConstraints constraints = (TabbedpaneConstraints) panel.layoutconstraints.value
				tab.title = constraints.title
				tab.content = it

				result.tabs << tab
			}
		}

		return result
	}
}
