package org.nuclos.layout.transformation

import org.nuclos.cache.IFqnCache
import org.nuclos.common.UID

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider

/**
 * A Jackson Serializer that searches for UIDs in String properties.
 *
 * If a string contains exactly 1 UID, the property is serialized to the fully qualified name
 * of the found UID.
 *
 * TODO: Move to nuclos-common?
 * TODO: This serializer should only serialize UID types, not Strings.
 * TODO: UID-FQN-Mapping should generally be done in a consistent way in all REST services.
 */
abstract class UidToFqnJacksonSerializer extends JsonSerializer<String> {

	abstract IFqnCache getFqnCache();

	@Override
	Class<String> handledType() {
		return String.class
	}

	@Override
	void serialize(
			final String value,
			final JsonGenerator gen,
			final SerializerProvider serializers
	) throws IOException, JsonProcessingException {
		List<UID> uids = UID.parseAllUIDs(value)
		if (uids.size() == 1) {
			UID uid = uids[0]
			String fqn = fqnCache.translateUid(uid)
			gen.writeString(fqn)
		} else {
			gen.writeString(value)
		}
	}
}