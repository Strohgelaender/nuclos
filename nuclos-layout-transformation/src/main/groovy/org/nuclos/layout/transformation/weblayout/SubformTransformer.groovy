package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Boolean
import org.nuclos.schema.layout.layoutml.Controltype
import org.nuclos.schema.layout.layoutml.Subform
import org.nuclos.schema.layout.web.SubformControlType
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebSubform
import org.nuclos.schema.layout.web.WebSubformColumn
import org.nuclos.schema.layout.web.WebToolbarOrientation
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class SubformTransformer extends ElementTransformer<Subform, WebComponent> {
	private static final Logger log = LoggerFactory.getLogger(SubformTransformer.class)

	SubformTransformer() {
		super(Subform.class)
	}

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Subform subform) {
		WebSubform result = factory.createWebSubform()

		result.name = subform.name

		result.autonumberSorting = subform.autonumbersorting == Boolean.YES

		// TODO
		result.controllerType = null

		addDynamicallyEnabledButtons(subform, result)

		result.dynamicCellHeightsDefault = subform.dynamicCellHeightsDefault == Boolean.YES
		result.entity = subform.entity
		result.foreignkeyfieldToParent = subform.foreignkeyfieldToParent
		result.ignoreSubLayout = subform.ignoreSubLayout == Boolean.YES
		result.multiEditable = subform.multieditable == Boolean.YES
		result.notCloneable = subform.notcloneable == Boolean.YES
		result.opaque = subform.opaque == Boolean.YES
		result.parentSubform = subform.parentSubform
		if (subform.toolbarorientation) {
			result.toolbarOrientation = WebToolbarOrientation.fromValue(subform.toolbarorientation.toLowerCase())
		}
		result.uniqueMastercolumn = subform.uniqueMastercolumn

		subform.subformColumn.each {
			WebSubformColumn column = factory.createWebSubformColumn()

			column.columns = it.columns
			column.controlType = getControlType(it.controltype)
			column.controlTypeClass = it.controltypeclass
			column.customUsageSearch = it.customusagesearch
			column.enabled = it.enabled == Boolean.YES
			column.insertable = it.insertable == Boolean.YES
			column.label = it.label
			column.name = it.name
			column.nextFocusComponent = it.nextfocuscomponent
			column.nextFocusField = it.nextfocusfield
			column.notCloneable = it.notcloneable == Boolean.YES
			column.resourceId = it.resourceId
			column.rows = it.rows
			column.width = it.width
			column.visible = it.visible == Boolean.YES

			transferValuelistProvider(column, it.valuelistProvider)

			result.subformColumns << column
		}

		return result
	}

	SubformControlType getControlType(final Controltype controltype) {
		try {
			return SubformControlType.fromValue(controltype.value()?.toUpperCase())
		} catch (IllegalArgumentException | NullPointerException e) {
			// Ignore
		}
	}

	private void addDynamicallyEnabledButtons(Subform subform, WebSubform result) {
		if (subform.newEnabled) {
			result.newEnabled = tryEval(subform.newEnabled.content) as boolean
		}
		if (subform.editEnabled) {
			result.editEnabled = tryEval(subform.editEnabled.content) as boolean
		}
		if (subform.cloneEnabled) {
			result.cloneEnabled = tryEval(subform.cloneEnabled.content) as boolean
		}
		if (subform.deleteEnabled) {
			result.deleteEnabled = tryEval(subform.deleteEnabled.content) as boolean
		}
	}

	private Object tryEval(String expression) {
		def result = null

		try {
			result = Eval.me(expression)
		} catch (Exception e) {
			log.warn('Failed to evaluate expression', e)
		}

		return result
	}
}
