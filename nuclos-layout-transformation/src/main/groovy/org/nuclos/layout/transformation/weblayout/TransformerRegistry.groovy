package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.*
import org.nuclos.schema.layout.web.WebComponent

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class TransformerRegistry {
	static Map<Class, ElementTransformer> map = new HashMap<>()

	static {
		register(Tabbedpane.class, new TabbedpaneTransformer())
		register(CollectableComponent.class, new CollectableComponentTransformer())
		register(Subform.class, new SubformTransformer())
		register(Button.class, new ButtonTransformer())
		register(Matrix.class, new MatrixTransformer())
		register(Label.class, new LabelTransformer())
		register(Separator.class, new SeparatorTransformer())
		register(TitledSeparator.class, new TitledSeparatorTransformer())
		register(Webaddon.class, new WebaddonTransformer());
	}

	static <T> void register(Class<T> clss, ElementTransformer<T, ? extends WebComponent> transformer) {
		this.map.put(clss, transformer)
	}

	static <T> ElementTransformer<T, ? extends WebComponent> lookup(Class<T> clss) {
		return map.get(clss)
	}
}
