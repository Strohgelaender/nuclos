package org.nuclos.layout.transformation.weblayout

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class AttributeIdFactory {

	/**
	 * Remembers the first assigned ID for every attribute name.
	 */
	private final Map<String, Integer> attributeIds = new HashMap<>()

	private int nextId = 1

	int getNewId(String attributeName) {
		synchronized(attributeIds) {
			int id = nextId++

			if (!attributeIds.containsKey(attributeName)) {
				attributeIds.put(attributeName, id)
			}

			return id
		}
	}

	Integer getExistingId(String attributeName) {
		synchronized(attributeIds) {
			attributeIds.get(attributeName)
		}
	}
}
