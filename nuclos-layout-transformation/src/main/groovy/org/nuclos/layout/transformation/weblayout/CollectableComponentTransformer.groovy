package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Boolean
import org.nuclos.schema.layout.layoutml.CollectableComponent
import org.nuclos.schema.layout.layoutml.Controltype
import org.nuclos.schema.layout.layoutml.Option
import org.nuclos.schema.layout.web.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class CollectableComponentTransformer extends ElementTransformer<CollectableComponent, WebComponent> {
	private static final Logger log = LoggerFactory.getLogger(LayoutmlToWeblayoutTransformer.class);

	CollectableComponentTransformer() {
		super(CollectableComponent.class)
	}

	WebComponent transform(
			LayoutmlToWeblayoutTransformer layoutTransformer,
			CollectableComponent collectableComponent
	) {
		if (collectableComponent.visible == Boolean.NO) {
			return null
		}

		WebComponent result = null

		if (collectableComponent.showOnly == 'label') {
			result = factory.createWebLabel()
		} else {
			switch (collectableComponent.controltype) {
				case Controltype.TEXTFIELD:
					result = factory.createWebTextfield()
					break
				case Controltype.PASSWORD:
					result = factory.createWebPassword()
					break
				case Controltype.TEXTAREA:
					result = factory.createWebTextarea()
					break;
				case Controltype.DATECHOOSER:
					result = factory.createWebDatechooser()
					break;
				case Controltype.LISTOFVALUES:
					result = factory.createWebListofvalues()
					break;
				case Controltype.COMBOBOX:
					result = factory.createWebCombobox()
					break;
				case Controltype.CHECKBOX:
					result = factory.createWebCheckbox()
					break;
				case Controltype.IMAGE:
					result = factory.createWebFile()
					break;
				case Controltype.EMAIL:
					result = factory.createWebEmail()
					break
				case Controltype.HYPERLINK:
					result = factory.createWebHyperlink()
					break
				case Controltype.PHONENUMBER:
					result = factory.createWebPhonenumber()
					break
				case Controltype.OPTIONGROUP:
					result = factory.createWebOptiongroup()
					break
				case null:
					// No type means "universal component". Could be a Filechooser.

					result = determineByDataType(layoutTransformer, collectableComponent, result)

					if (!result) {
						result = determineByControlTypeClass(collectableComponent, result)
					}

					break
				default:
					log.debug "Skipping control type: $collectableComponent.controltype"
					result = null
			}
		}

		if (result) {
			result.name = layoutTransformer.getFieldName(collectableComponent.name)
			result.fontSize = convertFontSize(collectableComponent.font?.size)
			setTextFormatInWebComponent(result, collectableComponent.textFormat)

			if (result instanceof WebInputComponent) {
				result.insertable = collectableComponent.insertable != Boolean.NO
				transferValuelistProvider(
						result as WebInputComponent,
						collectableComponent.valuelistProvider
				)
			}

			if (result instanceof WebOptiongroup) {
				WebOptiongroup wog = (WebOptiongroup) result
				WebOptions webOptions = new WebOptions()
				webOptions.setName(collectableComponent.options.getName())
				webOptions.setDefault(collectableComponent.options.getDefault())
				webOptions.setOrientation(collectableComponent.options.getOrientation())

				for (Option option : collectableComponent.options.getOption()) {
					WebOption webOption = new WebOption();
					webOption.setName(option.getName())
					webOption.setValue(option.getValue())
					webOption.setLabel(option.getLabel())
					webOptions.getOption().add(webOption)
				}

				wog.setOptions(webOptions)
			}
		}

		return result
	}

	private WebComponent determineByControlTypeClass(CollectableComponent collectableComponent, WebComponent result) {
		switch (collectableComponent.controltypeclass) {
			case 'org.nuclos.client.ui.collect.component.CollectableEditorPane':
				result = factory.createWebHtmlEditor();
				break;
		}
		result
	}

	private WebComponent determineByDataType(LayoutmlToWeblayoutTransformer layoutTransformer, CollectableComponent collectableComponent, WebComponent result) {
		LayoutmlToWeblayoutTransformer.DataType type = layoutTransformer.getFieldType(collectableComponent.name)
		switch (type) {
			case LayoutmlToWeblayoutTransformer.DataType.FILE:
				result = factory.createWebFile()
				break;
			default:
				log.warn "Unknown data type for field $collectableComponent.name"
		}
		result
	}
}
