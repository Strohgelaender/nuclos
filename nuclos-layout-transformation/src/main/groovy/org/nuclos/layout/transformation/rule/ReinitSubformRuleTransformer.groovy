package org.nuclos.layout.transformation.rule

import org.nuclos.schema.layout.layoutml.ReinitSubform
import org.nuclos.schema.layout.rule.RuleActionReinitSubform

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class ReinitSubformRuleTransformer extends ActionTransformer<ReinitSubform, RuleActionReinitSubform> {

	ReinitSubformRuleTransformer() {
		super(ReinitSubform.class)
	}

	@Override
	RuleActionReinitSubform transform(final ReinitSubform input) {
		RuleActionReinitSubform result = factory.createRuleActionReinitSubform()

		result.entity = input.entity
		result.targetcomponent = input.targetcomponent

		return result
	}
}
