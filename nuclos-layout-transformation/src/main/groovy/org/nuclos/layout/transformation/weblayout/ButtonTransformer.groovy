package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Boolean
import org.nuclos.schema.layout.layoutml.Button
import org.nuclos.schema.layout.web.WebButton
import org.nuclos.schema.layout.web.WebComponent

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class ButtonTransformer extends ElementTransformer<Button, WebComponent> {

	ButtonTransformer() {
		super(Button.class)
	}

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Button button) {
		WebButton result

		def action = button.actioncommand
		if (action.endsWith('ExecuteRuleButtonAction')) {
			result = createRuleButton(button)
		} else if (action.endsWith('ChangeStateButtonAction')) {
			result = createChangeStateButton(button)
		} else if (action.endsWith('GeneratorButtonAction')) {
			result = createObjectGeneratorButton(button)
		} else if (action.endsWith('HyperlinkButtonAction')) {
			result = createHyperlinkButton(button)
		} else {
			result = factory.createWebButtonDummy()

			// Disable unknown components
			if (!action.endsWith('DummyButtonAction')) {
				result.enabled = false
			}
		}

		result.name = button.name
		result.label = button.label
		result.icon = button.icon
		result.fontSize = convertFontSize(button.font?.size)

		if (button.disableDuringEdit == Boolean.YES) {
			result.disableDuringEdit = true
		}

		button.getIcon()

		return result
	}

	private WebButton createHyperlinkButton(Button button) {
		def result = factory.createWebButtonHyperlink()
		result.hyperlinkField = button.getProperty().find { it.name == 'hyperlinkField' }?.value
		result
	}

	private WebButton createRuleButton(Button button) {
		def result = factory.createWebButtonExecuteRule()
		result.rule = button.getProperty().find { it.name == 'ruletoexecute' }?.value
		result
	}

	private WebButton createChangeStateButton(Button button) {
		def result = factory.createWebButtonChangeState()
		result.targetState = button.getProperty().find { it.name == 'targetState' }?.value
		result
	}

	private WebButton createObjectGeneratorButton(Button button) {
		def result = factory.createWebButtonGenerateObject()
		result.objectGenerator = button.getProperty().find { it.name == 'generatortoexecute' }?.value
		result
	}

}
