package org.nuclos.layout.transformation.weblayout.responsive

import org.nuclos.schema.layout.layoutml.Panel
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebContainer
import org.nuclos.layout.transformation.weblayout.AbstractPanelTransformer
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class PanelTransformer extends AbstractPanelTransformer {

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Panel panel) {
		if (panel.visible == org.nuclos.schema.layout.layoutml.Boolean.NO) {
			return
		}

		WebContainer result = factory.createWebContainer()
		List<WebComponent> components = []

		result.grid = factory.createWebGrid()

		result.fontSize = convertFontSize(panel.font?.size)

		panel.containerOrLabelOrTextfield.each {
			components.addAll(layoutTransformer.getWebComponents(it))
		}

		layoutTransformer.populateGrid(result.grid, components)

		return result
	}
}
