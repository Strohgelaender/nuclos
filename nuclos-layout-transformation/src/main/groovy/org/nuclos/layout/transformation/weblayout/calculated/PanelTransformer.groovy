package org.nuclos.layout.transformation.weblayout.calculated

import java.math.MathContext
import java.math.RoundingMode

import org.nuclos.schema.layout.layoutml.Background
import org.nuclos.schema.layout.layoutml.Panel
import org.nuclos.schema.layout.layoutml.Tablelayout
import org.nuclos.schema.layout.web.WebComponent
import org.nuclos.schema.layout.web.WebContainer
import org.nuclos.layout.transformation.weblayout.AbstractPanelTransformer
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class PanelTransformer extends AbstractPanelTransformer {
	private static final Logger log = LoggerFactory.getLogger(PanelTransformer.class)

	WebComponent transform(LayoutmlToWeblayoutTransformer layoutTransformer, Panel panel) {
		if (panel.visible == org.nuclos.schema.layout.layoutml.Boolean.NO) {
			return
		}

		WebContainer result = createPanelOrContainer(panel)
		List<WebComponent> components = []

		result.calculated = factory.createWebGridCalculated()

		result.fontSize = convertFontSize(panel.font?.size)
		if (panel.background) {
			result.backgroundColor = convertBackground(panel.background)
		}

		result.opaque = panel.opaque == org.nuclos.schema.layout.layoutml.Boolean.YES

		panel.containerOrLabelOrTextfield.each {
			components.addAll(layoutTransformer.getWebComponents(it))
		}

		GridSizes gridSizes = getGridSizes(panel)
		result.calculated.minWidth = gridSizes.fixedColSize
		result.calculated.minHeight = gridSizes.fixedRowSize

		((LayoutmlToWeblayoutCalcTransformer) layoutTransformer).populateGridCalculated(
				result.calculated,
				components,
				gridSizes
		)

		return result
	}

	String convertBackground(final Background background) {
		try {
			String.format(
					'#%02X%02X%02X',
					background.red as Integer,
					background.green as Integer,
					background.blue as Integer
			)
		} catch (NumberFormatException) {
		}
	}

	private GridSizes getGridSizes(Panel panel) {
		GridSizes result = null
		def layoutManager = panel.layoutmanager.value
		if (layoutManager instanceof Tablelayout) {
			Tablelayout tablelayout = (Tablelayout) layoutManager
			List<Integer> rowSizes = tablelayout.rows.split('\\|').collect {
				new BigDecimal("$it").toInteger()
			}
			List<Integer> columnSizes = tablelayout.columns.split('\\|').collect {
				new BigDecimal("$it").toInteger()
			}
			result = new GridSizes(
					rowSizes,
					columnSizes
			)
		}
		return result
	}

	class GridSizes {
		List<Integer> rowSizes
		List<Integer> columnSizes

		int dynamicRowCount = 0
		BigInteger fixedRowSize = 0

		int dynamicColCount = 0
		BigInteger fixedColSize = 0

		MathContext mc = new MathContext(3, RoundingMode.HALF_UP)

		GridSizes(
				List<Integer> rowSizes,
				List<Integer> columnSizes
		) {
			this.rowSizes = rowSizes
			this.columnSizes = columnSizes

			rowSizes.each {
				if (it == -1) {
					dynamicRowCount++
				} else if (it > 0) {
					fixedRowSize += it.toBigInteger()
				}
			}
			columnSizes.each {
				if (it == -1) {
					dynamicColCount++
				} else if (it > 0) {
					fixedColSize += it.toBigInteger()
				}
			}
		}

		CalculatedPosition getCellPosition(WebComponent component) {
			CellSizeContext rowContext = new CellSizeContext(
					rowSizes,
					component.row,
					component.rowspan
			)
			CellSizeContext columnContext = new CellSizeContext(
					columnSizes,
					component.column,
					component.colspan
			)

			return new CalculatedPosition(
					getCellLeft(columnContext),
					getCellTop(rowContext),
					getCellWidth(columnContext),
					getCellHeight(rowContext)
			)
		}

		String getCellLeft(CellSizeContext context) {
			toOffset(context)
		}

		String getCellTop(CellSizeContext context) {
			toOffset(context)
		}

		String getCellWidth(CellSizeContext context) {
			toSize(context)
		}

		String getCellHeight(CellSizeContext context) {
			toSize(context)
		}

		String toOffset(CellSizeContext context) {
			String result = null

			if (context.previousDynamic == 0) {
				result = "${context.previousFixed}px"
			} else {
				int totalDynamic = context.previousDynamic + context.coveredDynamic + context.followingDynamic
				int totalFixed = context.previousFixed + context.coveredFixed + context.followingFixed
				result = "calc((100% - ${totalFixed}px) / $totalDynamic * $context.previousDynamic + ${context.previousFixed}px)"
			}

			return result
		}

		String toSize(CellSizeContext context) {
			String result = null
			if (context.coveredDynamic == 0) {
				result = "${context.coveredFixed}px"
			} else {
				int totalDynamic = context.previousDynamic + context.coveredDynamic + context.followingDynamic
				int uncoveredDynamic = context.previousDynamic + context.followingDynamic
				if (uncoveredDynamic == 0) {
					result = "calc(100% - ${context.previousFixed}px - ${context.followingFixed}px)"
				} else {    // Covered at least 1 dynamic, but there are also uncovered
					int totalFixed = context.previousFixed + context.coveredFixed + context.followingFixed
					BigDecimal shiftPixels = (new BigDecimal(totalFixed) / totalDynamic).round(mc)
					result = "calc((100% - ${totalFixed}px) / $totalDynamic * $context.coveredDynamic + ${context.coveredFixed}px)"
				}
			}

			return result
		}
	}

	/**
	 * Holds row/column size informations necessary to calculate the actual size of a component.
	 */
	class CellSizeContext {
		List<Integer> previousSizes = []
		List<Integer> coveredSizes = []
		List<Integer> followingSizes = []

		/**
		 * Count of dynamically sized ("use remaining space") row/column sizes before the current component.
		 */
		final int previousDynamic
		/**
		 * Sum of fixed pixel row/column sizes before the current component.
		 */
		final int previousFixed

		/**
		 * Count of dynamically sized ("use remaining space") row/column sizes covered by the current component.
		 */
		final int coveredDynamic
		/**
		 * Sum of fixed pixel row/column sizes covered by the current component.
		 */
		final int coveredFixed

		/**
		 * Count of dynamically sized ("use remaining space") row/column sizes after the current component.
		 */
		final int followingDynamic
		/**
		 * Sum of fixed pixel row/column sizes after the current component.
		 */
		final int followingFixed

		CellSizeContext(
				List<Integer> rowOrColumnSizes,
				BigInteger cellStart,
				BigInteger cellSpan
		) {
			rowOrColumnSizes.eachWithIndex { size, index ->
				if (index < cellStart) {
					previousSizes << size
				} else if (index < cellStart + (cellSpan ?: 1g)) {
					coveredSizes << size
				} else {
					followingSizes << size
				}
			}

			previousDynamic = countDynamic(previousSizes)
			previousFixed = sumFixed(previousSizes)

			coveredDynamic = countDynamic(coveredSizes)
			coveredFixed = sumFixed(coveredSizes)

			followingDynamic = countDynamic(followingSizes)
			followingFixed = sumFixed(followingSizes)
		}

		int countDynamic(List<Integer> sizes) {
			sizes.findAll { it == -1 }.size()
		}

		int sumFixed(List<Integer> sizes) {
			(Integer) (sizes.findAll { it > 0 }.sum() ?: 0g)
		}
	}

	class CalculatedPosition {
		String left
		String top
		String width
		String height

		CalculatedPosition(
				final String left,
				final String top,
				final String width,
				final String height
		) {
			this.left = left
			this.top = top
			this.width = width
			this.height = height
		}
	}
}
