package org.nuclos.installer.unpack;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.Constants;
import org.nuclos.installer.InstallException;
import org.nuclos.installer.database.PostgresService;
import org.nuclos.installer.mode.Installer;
import org.nuclos.installer.util.EnvironmentUtils;

import freemarker.template.TemplateException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class AbstractUnpackerTest {

	@Test
	public void testJDKLookup() {
		final TestUnpacker unpacker = new TestUnpacker();

		final String javaHome = unpacker.getDefaultValue(Constants.JAVA_HOME);

		assert StringUtils.isNotBlank(javaHome);
		assert EnvironmentUtils.isValidJDK(new File(javaHome));

	}

	@Test
	public void createIndexHtmlRichclientWebclientLauncher() throws IOException, TemplateException {
		ConfigContext.getCurrentConfig().clear();
		ConfigContext.setProperty(Constants.NUCLOS_HOME, "../nuclos-war");
		ConfigContext.setProperty(Constants.CLIENT_WEBCLIENT, "true");
		ConfigContext.setProperty(Constants.CLIENT_RICHCLIENT, "true");
		ConfigContext.setProperty(Constants.CLIENT_LAUNCHER, "true");

		String source = createIndexHtml();

		assert hasDesktopButton(source);
		assert hasLauncher(source);
		assert hasWebclientButton(source);
	}

	@Test
	public void createIndexHtmlRichclientWebclient() throws IOException, TemplateException {
		ConfigContext.getCurrentConfig().clear();
		ConfigContext.setProperty(Constants.NUCLOS_HOME, "../nuclos-war");
		ConfigContext.setProperty(Constants.CLIENT_WEBCLIENT, "true");
		ConfigContext.setProperty(Constants.CLIENT_RICHCLIENT, "true");

		String source = createIndexHtml();

		assert hasDesktopButton(source);
		assert !hasLauncher(source);
		assert hasWebclientButton(source);
	}

	@Test
	public void createIndexHtmlNoclient() throws IOException, TemplateException {
		ConfigContext.getCurrentConfig().clear();
		ConfigContext.setProperty(Constants.NUCLOS_HOME, "../nuclos-war");

		String source = createIndexHtml();

		assert !hasDesktopButton(source);
		assert !hasLauncher(source);
		assert !hasWebclientButton(source);
	}

	@Test
	public void createIndexHtmlWebclient() throws IOException, TemplateException {
		ConfigContext.getCurrentConfig().clear();
		ConfigContext.setProperty(Constants.NUCLOS_HOME, "../nuclos-war");
		ConfigContext.setProperty(Constants.CLIENT_WEBCLIENT, "true");

		String source = createIndexHtml();

		assert !hasDesktopButton(source);
		assert !hasLauncher(source);
		assert hasWebclientButton(source);
	}

	@Test
	public void createIndexHtmlRichclient() throws IOException, TemplateException {
		ConfigContext.getCurrentConfig().clear();
		ConfigContext.setProperty(Constants.NUCLOS_HOME, "../nuclos-war");
		ConfigContext.setProperty(Constants.CLIENT_RICHCLIENT, "true");

		String source = createIndexHtml();

		assert hasDesktopButton(source);
		assert !hasLauncher(source);
		assert !hasWebclientButton(source);
	}

	@Test
	public void createIndexHtmlRichtclientAndLauncher() throws IOException, TemplateException {
		ConfigContext.getCurrentConfig().clear();
		ConfigContext.setProperty(Constants.NUCLOS_HOME, "../nuclos-war");
		ConfigContext.setProperty(Constants.CLIENT_RICHCLIENT, "true");
		ConfigContext.setProperty(Constants.CLIENT_LAUNCHER, "true");

		String source = createIndexHtml();

		assert hasDesktopButton(source);
		assert hasLauncher(source);
		assert !hasWebclientButton(source);
	}

	@Test
	public void createIndexHtmlLauncherWithoutRichtclient() throws IOException, TemplateException {
		ConfigContext.getCurrentConfig().clear();
		ConfigContext.setProperty(Constants.NUCLOS_HOME, "../nuclos-war");
		ConfigContext.setProperty(Constants.CLIENT_LAUNCHER, "true");

		String source = createIndexHtml();

		assert !hasDesktopButton(source);
		assert !hasLauncher(source);
		assert !hasWebclientButton(source);
	}

	@Test
	public void tomcatConfigNotOverriden() {
		final TestUnpacker unpacker = new TestUnpacker();
		assert !unpacker.isTomcatConfigOverriddenByExtension();
	}

	@Test
	@Ignore
	public void tomcatConfigOverriden() {
		final TestUnpacker unpacker = new TestUnpacker();

		// TODO: Create extensions/tomcat/conf/server.xml
		assert unpacker.isTomcatConfigOverriddenByExtension();
	}

	private String createIndexHtml() throws IOException, TemplateException {
		TestUnpacker testUnpacker = new TestUnpacker();
		List<String> files = new ArrayList<>();

		File nuclosHome = new File("../nuclos-war/src/main");
		testUnpacker.createIndexHtml(nuclosHome, files);

		assert files.size() == 1;
		assert files.get(0).endsWith("index.html");
		assert new File(files.get(0)).exists();

		File indexHtml = new File(nuclosHome, "webapp/index.html");
		assert indexHtml.exists();

		return FileUtils.readFileToString(indexHtml);
	}

	private boolean hasDesktopButton(final String source) {
		return source.contains("DesktopClient.png");
	}

	private boolean hasWebclientButton(final String source) {
		return source.contains("WebClient.png");
	}

	private boolean hasLauncher(final String source) {
		return source.contains("<a id=\"launcherLink\"");
	}

	class TestUnpacker extends AbstractUnpacker {

		@Override
		public boolean isPrivileged() {
			return false;
		}

		@Override
		public boolean canInstall() {
			return false;
		}

		@Override
		public boolean isProductRegistered() {
			return false;
		}

		@Override
		public boolean isPostgresBundled() {
			return false;
		}

		@Override
		public List<PostgresService> getPostgresServices() {
			return null;
		}

		@Override
		public void shutdown(final Installer cb) {

		}

		@Override
		public void startup(final Installer cb) {

		}

		@Override
		public void installPostgres(final Installer cb) {

		}

		@Override
		public void register(final Installer cb, final boolean systemlaunch) {

		}

		@Override
		public void unregister(final Installer cb) {

		}
	}
}