package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class CommonExtensionInstallerTest extends ExtensionInstallerTest {
	@Test
	public void install() throws IOException {
		InstallationContext context = getInstallationContext();

		CommonExtensionInstaller extensionInstaller = new CommonExtensionInstaller(
				context,
				new ClientExtensionInstaller(context),
				new ServerExtensionInstaller(context)
		);

		File testFile = new File(extensionInstaller.getSourceDirectory(), "test.txt");
		FileUtils.writeStringToFile(testFile, getClass().getSimpleName());

		extensionInstaller.install();

		assertFileWithContent(
				new File(context.getWebappDir(), "app/extensions/" + testFile.getName()),
				getClass().getSimpleName()
		);
		assertFileWithContent(
				new File(context.getNuclosHome(), "client/" + testFile.getName()),
				getClass().getSimpleName()
		);
		assertFileWithContent(
				new File(context.getWebappDir(), "WEB-INF/lib/" + testFile.getName()),
				getClass().getSimpleName()
		);
		assert testFile.exists();
	}
}