package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class NuclosBinExtensionInstallerTest extends ExtensionInstallerTest {
	@Test
	public void install() throws IOException {
		InstallationContext context = getInstallationContext();

		NuclosBinExtensionInstaller extensionInstaller = new NuclosBinExtensionInstaller(context);

		File testFile = new File(extensionInstaller.getSourceDirectory(), "test.txt");
		FileUtils.writeStringToFile(testFile, getClass().getSimpleName());

		extensionInstaller.install();

		assertFileWithContent(
				new File(context.getNuclosHome(), "bin/" + testFile.getName()),
				getClass().getSimpleName()
		);
		assert testFile.exists();
	}
}