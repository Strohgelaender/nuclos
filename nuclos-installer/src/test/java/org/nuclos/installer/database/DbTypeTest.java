package org.nuclos.installer.database;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class DbTypeTest {
	@Test
	public void getAdaptersAsArray() {
		String[] array = DbType.getAdaptersAsArray();
		assert array.length >= 7;

		List<String> list = Arrays.asList(array);
		assert list.contains("postgresql");
		assert list.contains("oracle");
		assert list.contains("mssql");
		assert list.contains("db2");
		assert list.contains("h2");
	}
}