package org.nuclos.installer.unpack.extension;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class NuclosBinExtensionInstaller extends ExtensionInstaller {
	NuclosBinExtensionInstaller(
			final InstallationContext context
	) {
		super(context);
	}

	@Override
	File getSourceDirectory() {
		return new File(context.getExtensionsDir(), DirectoryName.BIN);
	}

	@Override
	List<File> getTargetDirectories() {
		return Arrays.asList(
				new File(context.getNuclosHome(), DirectoryName.BIN)
		);
	}
}
