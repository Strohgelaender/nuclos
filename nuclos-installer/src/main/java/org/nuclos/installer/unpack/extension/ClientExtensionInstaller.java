package org.nuclos.installer.unpack.extension;

import static org.nuclos.installer.unpack.extension.DirectoryName.CLIENT;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class ClientExtensionInstaller extends ExtensionInstaller {
	ClientExtensionInstaller(
			final InstallationContext context
	) {
		super(context);
	}

	@Override
	File getSourceDirectory() {
		return new File(context.getExtensionsDir(), DirectoryName.CLIENT);
	}

	@Override
	List<File> getTargetDirectories() {
		return Arrays.asList(
				new File(context.getWebappDir(), "app/extensions"),
				new File(context.getNuclosHome(), CLIENT)
		);
	}
}
