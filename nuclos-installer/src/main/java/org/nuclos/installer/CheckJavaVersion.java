package org.nuclos.installer;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nuclos.installer.util.FileUtils;

public final class CheckJavaVersion {
	private static final Logger LOG = Logger.getLogger(CheckJavaVersion.class);

	private CheckJavaVersion() {
		// Never invoked.
	}

	//Java 1.8.0_101 minimum Version
	public static final String minJavaVersion = "1.8.0_101";
	public static final String recommendedJavaVersion = "1.8.0_144";

	public static boolean checkVersion(String version) {
		return getVersionValue(version) >= getVersionValue(minJavaVersion);
	}

	private static boolean checkRecommendedVersion(String version) {
		return getVersionValue(version) >= getVersionValue(recommendedJavaVersion);
	}

	/**
	 * TODO: This checks only the version of the current JVM, i.e. this is only relevant for the installer itself!
	 * It does not check the version of the JDK which is selected during the installation process.
	 */
	public static boolean isRecommended() {
		return checkRecommendedVersion(System.getProperty("java.version"));
	}

	private static int getVersionValue(String version) {
		final int vi[] = parseVersion(version);

		// There must be at least a major version number, e.g. 9
		int sum = vi[0] * 10090709;

		// There might be minor, security and patch version numbers
		if (vi.length >= 2) {
			sum += vi[1] * 100907;
		}
		if (vi.length >= 3) {
			sum += vi[2] * 1009;
		}
		if (vi.length >= 4) {
			sum += vi[3];
		}

		return sum;
	}

	private static int[] parseVersion(String version) {
		if (StringUtils.isBlank(version)) {
			return new int[]{0};
		}

		final String v[] = version.split("[.\\-_]");
		final int vi[] = new int[v.length];
		for (int i = 0; i < v.length; ++i) {
			try {
				vi[i] = Integer.parseInt(v[i]);
			} catch (NumberFormatException e) {
				// ignore
			}
		}
		return vi;
	}

	static void checkVersion(Runnable run) {
		final String version = System.getProperty("java.version");
		if (!checkVersion(version)) {
			bailOut();
		}

		if (!checkRecommendedVersion(version)) {
			warn();
		}

		try {
			run.run();
		} catch (UnsupportedClassVersionError e) {
			handle(e);
		}
	}

	private static void handle(UnsupportedClassVersionError e) {
		final String msg = e.toString();
		final String minVersion;
		if (msg.contains("52.0")) {
			minVersion = "Java SE 8";
		} else if (msg.contains("51.0")) {
			minVersion = "Java SE 7";
		} else if (msg.contains("50.0")) {
			minVersion = "Java SE 6";
		} else if (msg.contains("49.0")) {
			minVersion = "Java SE 5 (Java 1.5)";
		} else {
			minVersion = "Java SE 6";
		}
		System.err.println("You must use at least " + minVersion + ".");
		System.err.println("Sie müssen mindestens " + minVersion + " verwenden.");
		FileUtils.copyLog();
		System.exit(-1);
	}

	private static void bailOut() {
		System.err.println("You are using an outdated version of java. " +
				"Please install a more recent version, at least " + minJavaVersion);
		System.err.println("Sie benutzen eine veraltete Java Version. " +
				"Bitte installieren Sie eine aktuelle Java Umgebung, mindestens " + minJavaVersion);
		FileUtils.copyLog();
		System.exit(-1);
	}

	private static void warn() {
		System.out.println("You are using a unsupported java version. " +
				"You should use the latest certified Java SE 8 for your platform. Recommended: " + recommendedJavaVersion);
		System.out.println("Sie benutzen eine nicht unterstützte Java Version. " +
				"Sie sollten die neuste zertifizierte Java SE 8 Version für Ihr Betriebssystem verwenden. Empfohlen: " + recommendedJavaVersion);
	}

	public static void main(String[] args) {
		checkVersion(() -> System.out.println("CheckJavaVersion passed."));
	}

	public static void checkVersion(final File javaHome) throws InstallException {
		final String version = getJavaVersion(javaHome);

		if (!checkVersion(version)) {
			throw new InstallException(L10n.getMessage("validation.javahome.invalidversion") + version);
		}
	}

	/**
	 * Tries to extract the java version from rt.jar (if it exists)
	 * or from the "release" properties file.
	 */
	private static String getJavaVersion(final File javaHome) {
		final File rtJar = new File(javaHome, "jre/lib/rt.jar");

		if (rtJar.exists()) {
			Manifest manifest;
			try {
				manifest = FileUtils.extractManifest(rtJar);
				return manifest.getMainAttributes().getValue(Attributes.Name.IMPLEMENTATION_VERSION);
			} catch (IOException e) {
				// Ignore, rt.jar exists only for Java version <= 8
			}
		}

		final File releaseFile = new File(javaHome, "release");
		final Properties props = new Properties();
		try {
			props.load(org.apache.commons.io.FileUtils.openInputStream(releaseFile));
		} catch (IOException e) {
			LOG.warn("Could not load release properties", e);
			return null;
		}

		return StringUtils.strip(props.getProperty("JAVA_VERSION"), "\"");
	}
}
