//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.util;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.installer.CheckJavaVersion;
import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.Constants;
import org.nuclos.installer.InstallException;
import org.nuclos.installer.L10n;

public class EnvironmentUtils {
	private static final Logger LOG = Logger.getLogger(EnvironmentUtils.class);

	public static void validateJavaHome(String javahome) throws InstallException {
		javahome = new File(javahome).getAbsolutePath();

		final boolean isValidJDK = isValidJDK(new File(javahome));
		if (!isValidJDK) {
			throw new InstallException(L10n.getMessage("validation.javahome.invalid") + javahome);
		}

		CheckJavaVersion.checkVersion(new File(javahome));
	}

	/**
	 * Determines if the given File is a valid JDK home.
	 */
	public static boolean isValidJDK(File javaHome) {
		return isValidJava8OrOlderJDK(javaHome) || isValidJava9OrNewerJDK(javaHome);
	}

	/**
	 * Checks only the existence of some files which are always present in
	 * JDKs up to version 8.
	 */
	private static boolean isValidJava8OrOlderJDK(final File javaHome) {
		final boolean hasRTJar = new File(javaHome, "jre/lib/rt.jar").exists();
		final boolean hasToolsJar = new File(javaHome, "lib/tools.jar").exists();

		return hasRTJar && hasToolsJar;
	}

	/**
	 * Checks the existence of certain files which are normally present in
	 * JDKs version 9 and newer.
	 */
	private static boolean isValidJava9OrNewerJDK(final File javaHome) {
		final File releaseFile = new File(javaHome, "release");

		final boolean hasJavac = new File(javaHome, "bin/javac").exists()
				|| new File(javaHome, "bin/javac.exe").exists();

		return releaseFile.exists() && hasJavac;
	}

	public static String getJvmDll(String javahome) {
		final List<String> possibleJvmDllPaths = Arrays.asList(
				"jre/bin/server/jvm.dll",
				"jre/bin/client/jvm.dll",
				// Newer JDK versions (9+) do not have a "jre" sub-directory
				"bin/server/jvm.dll",
				"bin/client/jvm.dll"
		);

		for (String path: possibleJvmDllPaths) {
			File file = new File(javahome, path);
			if (file.exists()) {
				return file.getAbsolutePath();
			}
		}

		return "auto";
	}

	public static boolean checkPortRange(int port) {
		return (port >= 0 && port <= 65535);
	}

	public static boolean isPortBlocked(int port) {
		boolean ok = true;
		ServerSocket socket = null;
		try {
			socket = new ServerSocket(port);
		} catch (IOException e) {
			ok = false;
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					// Ok! (tp)
					e.printStackTrace();
					LOG.error("checkPort failed: " + e, e);
				}
			}
		}
		return !ok;
	}

	//NUCLOS-5040
	public static int getNextOpenPort(int port) {
		if (ConfigContext.isUpdate()) {
			return port;
		} else {
			int def = port;
			while (isPortBlocked(port) || isInsecurePort(port) || isSelectedPort(port)) {
				if (port >= 65535) {
					LOG.warn("No open port found");
					return def;
				}
				port++;
			}
		}
		return port;
	}

	private static boolean isSelectedPort(int port) {
		String input = Integer.toString(port);
		String http = ConfigContext.getProperty(Constants.HTTP_PORT);
		String https = ConfigContext.getProperty(Constants.HTTPS_PORT);
		String shutdown = ConfigContext.getProperty(Constants.SHUTDOWN_PORT);
		String ajp = ConfigContext.getProperty(Constants.AJP_PORT);
		if (http != null && http.equals(input)) {
			return true;
		}
		if (https != null && https.equals(input)) {
			return true;
		}
		if (shutdown != null && shutdown.equals(input)) {
			return true;
		}
		return ajp != null && ajp.equals(input);
	}

	public static boolean isWindows() {
		String osName = System.getProperty("os.name").toLowerCase();
		return osName.contains("win");
	}

	public static boolean isMac() {
		String osName = System.getProperty("os.name").toLowerCase();
		return osName.contains("mac");
	}

	public static boolean isLinux() {
		String osName = System.getProperty("os.name").toLowerCase();
		return osName.contains("nux");
	}

	public static boolean isUnixoid() {
		String osName = System.getProperty("os.name").toLowerCase();
		return osName.contains("nix") || osName.contains("nux") || osName.contains("os x");
	}

	public static boolean isInsecurePort(int iport) {
		for (int insecurePort : insecurePorts) {
			if (iport < insecurePort)
				return false;
			if (iport == insecurePort)
				return true;
		}
		return false;
	}

	private static final int[] insecurePorts = {
			1,    // tcpmux
			7,    // echo
			9,    // discard
			11,   // systat
			13,   // daytime
			15,   // netstat
			17,   // qotd
			19,   // chargen
			20,   // ftp data
			21,   // ftp access
			22,   // ssh
			23,   // telnet
			25,   // smtp
			37,   // time
			42,   // name
			43,   // nicname
			53,   // domain
			77,   // priv-rjs
			79,   // finger
			87,   // ttylink
			95,   // supdup
			101,  // hostriame
			102,  // iso-tsap
			103,  // gppitnp
			104,  // acr-nema
			109,  // pop2
			110,  // pop3
			111,  // sunrpc
			113,  // auth
			115,  // sftp
			117,  // uucp-path
			119,  // nntp
			123,  // NTP
			135,  // loc-srv /epmap
			139,  // netbios
			143,  // imap2
			179,  // BGP
			389,  // ldap
			465,  // smtp+ssl
			512,  // print / exec
			513,  // login
			514,  // shell
			515,  // printer
			526,  // tempo
			530,  // courier
			531,  // chat
			532,  // netnews
			540,  // uucp
			556,  // remotefs
			563,  // nntp+ssl
			587,  // submission
			601,  // syslog
			636,  // ldap+ssl
			993,  // ldap+ssl
			995,  // pop3+ssl
			2049, // nfs
			3659, // apple-sasl / PasswordServer
			4045, // lockd
			6000, // X11
			6665, // Alternate IRC [Apple addition]
			6666, // Alternate IRC [Apple addition]
			6667, // Standard IRC [Apple addition]
			6668, // Alternate IRC [Apple addition]
			6669 // Alternate IRC [Apple addition]
	};
}
