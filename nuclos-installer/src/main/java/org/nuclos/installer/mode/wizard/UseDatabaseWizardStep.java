//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode.wizard;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyEvent;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.swing.*;

import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.Constants;
import org.nuclos.installer.L10n;
import org.nuclos.installer.database.DbType;
import org.pietschy.wizard.ButtonBar;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

import static org.nuclos.installer.database.DbType.*;

/**
 * Configuration of existing database connection.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 */
public class UseDatabaseWizardStep extends AbstractWizardStep implements ActionListener, Constants {

	private ButtonBar buttonBar;
	
	private final JButton btnCheckConnection = new JButton();
	
	private final JComboBox cmbAdapter = new JComboBox();
	private DatabaseComboBoxItem selectedAdapter;

	private final JTextField txtDriverJarPath = new JTextField();
	private final JButton btnDriverJarSelect = new JButton();

	private final JTextField txtHost = new JTextField();

	private final JTextField txtPort = new JTextField();

	private final JTextField txtDbName = new JTextField();

	private final JTextField txtDbUsername = new JTextField();

	private final JPasswordField txtDbPassword1 = new JPasswordField();
	private final JPasswordField txtDbPassword2 = new JPasswordField();

	private final JTextField txtSchema = new JTextField();

	private final JTextField txtTablespace = new JTextField();
	private final JTextField txtTablespaceIndex = new JTextField();
	
	private final JComboBox cmbMSSQLIsolation = new JComboBox();
	
	private final JTextArea txtConnectionInit = new JTextArea();

	private static final double[][] layout = {
        { 200.0, TableLayout.FILL, 100.0 }, // Columns
        { 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 50.0, TableLayout.FILL } };// Rows

	public UseDatabaseWizardStep() {
		super(L10n.getMessage("gui.wizard.usedb.title"), L10n.getMessage("gui.wizard.usedb.description"));

		TableLayout layout = new TableLayout(this.layout);
        layout.setVGap(5);
        layout.setHGap(5);
        this.setLayout(layout);

        JLabel label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.adapter.label"));
        this.add(label, "0,0");

        cmbAdapter.addItem(new DatabaseComboBoxItem(POSTGRESQL));
        cmbAdapter.addItem(new DatabaseComboBoxItem(ORACLE));
        cmbAdapter.addItem(new DatabaseComboBoxItem(MSSQL));
        cmbAdapter.addItem(new DatabaseComboBoxItem(SYBASE));
        cmbAdapter.addItem(new DatabaseComboBoxItem(DB2));
        cmbAdapter.addItem(new DatabaseComboBoxItem(DB2iSeries));
        cmbAdapter.addActionListener(this);
        this.add(cmbAdapter, "1,0");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.driverjar.label"));
        this.add(label, "0,1");

        this.txtDriverJarPath.getDocument().addDocumentListener(this);
		this.add(txtDriverJarPath, "1,1");

		btnDriverJarSelect.setText(L10n.getMessage("filechooser.browse"));
		btnDriverJarSelect.addActionListener(new SelectJarActionListener());
        this.add(btnDriverJarSelect, "2,1");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.host.label"));
        this.add(label, "0,2");

        txtHost.getDocument().addDocumentListener(this);
        this.add(txtHost, "1,2");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.port.label"));
        this.add(label, "0,3");

        txtPort.getDocument().addDocumentListener(this);
        this.add(txtPort, "1,3");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.dbname.label"));
        this.add(label, "0,4");

        txtDbName.getDocument().addDocumentListener(this);
        this.add(txtDbName, "1,4");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.dbuser.label"));
        this.add(label, "0,5");

        txtDbUsername.getDocument().addDocumentListener(this);
        this.add(txtDbUsername, "1,5");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.dbpassword1.label"));
        this.add(label, "0,6");

        txtDbPassword1.getDocument().addDocumentListener(this);
        this.add(txtDbPassword1, "1,6");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.dbpassword2.label"));
        this.add(label, "0,7");

        txtDbPassword1.getDocument().addDocumentListener(this);
        this.add(txtDbPassword2, "1,7");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.schema.label"));
        this.add(label, "0,8");

        txtSchema.getDocument().addDocumentListener(this);
        this.add(txtSchema, "1,8");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.tablespace.label"));
        this.add(label, "0,9");

        txtTablespace.getDocument().addDocumentListener(this);
        this.add(txtTablespace, "1,9");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.tablespaceindex.label"));
        this.add(label, "0,10");

        txtTablespaceIndex.getDocument().addDocumentListener(this);
        this.add(txtTablespaceIndex, "1,10");
        
        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.mssql.isolation.label"));
        this.add(label, "0,11");

        cmbMSSQLIsolation.addItem(new ComboBoxItem("", "Default"));
        cmbMSSQLIsolation.addItem(new ComboBoxItem("snapshot", "snapshot isolation"));
        cmbMSSQLIsolation.addActionListener(this);
        this.add(cmbMSSQLIsolation, "1,11");
        
        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.usedb.connection.init.label"));
        this.add(label, "0,12");
        txtConnectionInit.getDocument().addDocumentListener(this);
        this.add(new JScrollPane(txtConnectionInit), "1,12");
        
		btnCheckConnection.setText(L10n.getMessage("validation.db.connection"));
		btnCheckConnection.addActionListener(new CheckConnectionActionListener());

		addHierarchyListener(e -> {
			if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
				if (e.getChanged().isShowing()) {
					buttonBar = (ButtonBar)UseDatabaseWizardStep.this.findFirstJComponentInParent(
							(JComponent)UseDatabaseWizardStep.this.getParent(), ButtonBar.class);
					if (buttonBar != null) {
						buttonBar.add(btnCheckConnection, 0);
						buttonBar.revalidate();
						buttonBar.repaint();
					}
				}
				else {
					if (buttonBar != null) {
						buttonBar.remove(btnCheckConnection);
						buttonBar.revalidate();
						buttonBar.repaint();
					}
				}
			}
		});
	}

	@Override
	public void prepare() {
		modelToView(DATABASE_ADAPTER, cmbAdapter);
		modelToView(DATABASE_DRIVERJAR, txtDriverJarPath);
		modelToView(DATABASE_SERVER, txtHost);
		modelToView(DATABASE_PORT, txtPort);
		modelToView(DATABASE_NAME, txtDbName);
		modelToView(DATABASE_USERNAME, txtDbUsername);
		modelToView(DATABASE_PASSWORD, txtDbPassword1);
		modelToView(DATABASE_PASSWORD, txtDbPassword2);
		modelToView(DATABASE_SCHEMA, txtSchema);
		modelToView(DATABASE_TABLESPACE, txtTablespace);
		modelToView(DATABASE_TABLESPACEINDEX, txtTablespaceIndex);
		modelToView(DATABASE_MSSQL_ISOLATION, cmbMSSQLIsolation);
		modelToView(DATABASE_CONNECTION_INIT, txtConnectionInit);
		updateState();
	}

	@Override
	protected void updateState() {
		DatabaseComboBoxItem item = (DatabaseComboBoxItem) cmbAdapter.getSelectedItem();
		String adapter = item.value;
		if ("postgresql".equals(adapter)) {
			txtDriverJarPath.setEnabled(false);
			btnDriverJarSelect.setEnabled(false);
		}
		else {
			txtDriverJarPath.setEnabled(true);
			btnDriverJarSelect.setEnabled(true);
		}

		if (selectedAdapter != null) {
			//NUCLOS-3439
			if (!adapter.equals(selectedAdapter.value)
					&& txtPort.getText().equals(Integer.toString(selectedAdapter.type.getDefaultPort()))) {
				txtPort.setText(Integer.toString(item.type.getDefaultPort()));
			}
		}

		selectedAdapter = item;
		this.setComplete(true);
	}

	@Override
	public void applyState() throws InvalidStateException {
		viewToModel(DATABASE_ADAPTER, cmbAdapter);
		viewToModel(DATABASE_DRIVERJAR, txtDriverJarPath);
		viewToModel(DATABASE_SERVER, txtHost);
		viewToModel(DATABASE_PORT, txtPort);
		viewToModel(DATABASE_NAME, txtDbName);
		viewToModel(DATABASE_USERNAME, txtDbUsername);
		validatePasswordEquality(txtDbPassword1, txtDbPassword2, "gui.wizard.usedb.dbpassword1.label");
		viewToModel(DATABASE_PASSWORD, txtDbPassword1);
		viewToModel(DATABASE_PASSWORD, txtDbPassword2);
		viewToModel(DATABASE_SCHEMA, txtSchema);
		viewToModel(DATABASE_TABLESPACE, txtTablespace);
		viewToModel(DATABASE_TABLESPACEINDEX, txtTablespaceIndex);
		viewToModel(DATABASE_MSSQL_ISOLATION, cmbMSSQLIsolation);
		viewToModel(DATABASE_CONNECTION_INIT, txtConnectionInit);

		getRootPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		checkDbConnection();
		getRootPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public static void checkDbConnection() throws InvalidStateException {
		Connection con = null;
		Driver driver = null;

		try {
			String adapter = ConfigContext.getProperty(DATABASE_ADAPTER);
			String driverjar = ConfigContext.getProperty(DATABASE_DRIVERJAR);

			DbType dbType = DbType.findType(adapter);

			JarClassLoader loader = new JarClassLoader(
					((URLClassLoader)ClassLoader.getSystemClassLoader()).getURLs());
			if (dbType != POSTGRESQL) {
				loader.addURL(new URL("file:" + driverjar));

				driver = new DriverShim(
						(Driver)loader.loadClass(dbType.getDriverClassName()).newInstance());
				DriverManager.registerDriver(driver);
			}

			String username = ConfigContext.getProperty(DATABASE_USERNAME);
			String password = ConfigContext.getProperty(DATABASE_PASSWORD);
			con = DriverManager.getConnection(dbType.buildJdbcConnectionString(ConfigContext.getCurrentConfig()), username, password);
		}
		catch (Exception ex) {
			throw new InvalidStateException(L10n.getMessage("validation.invalid.db.connection") + "\n" + ex.getMessage());
		} finally {
			if (con != null) try { con.close(); } catch(Exception e) {}
			if (driver != null) try { DriverManager.deregisterDriver(driver); } catch(Exception e) {}
		}
	}

	private class SelectJarActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

	        int returnVal = chooser.showOpenDialog(UseDatabaseWizardStep.this);

	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	        	File file = chooser.getSelectedFile();
	        	txtDriverJarPath.setText(file.getAbsolutePath());
	        }
		}
	}
	
	private static class DriverShim implements Driver {
		private final Driver driver;
		DriverShim(Driver d) {
			this.driver = d;
		}
		public boolean acceptsURL(String u) throws SQLException {
			return this.driver.acceptsURL(u);
		}
		public Connection connect(String u, Properties p) throws SQLException {
			return this.driver.connect(u, p);
		}
		public int getMajorVersion() {
			return this.driver.getMajorVersion();
		}
		public int getMinorVersion() {
			return this.driver.getMinorVersion();
		}
		public DriverPropertyInfo[] getPropertyInfo(String u, Properties p) throws SQLException {
			return this.driver.getPropertyInfo(u, p);
		}
		public boolean jdbcCompliant() {
			return this.driver.jdbcCompliant();
		}
		public Logger getParentLogger() {
			try {
				Method m = Driver.class.getMethod("getParentLogger");
				if (m != null) return (Logger)m.invoke(this.driver);
			} catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException nsme) {
				//ignore
			}
			return null;
		}
	}
	
    private static class JarClassLoader extends URLClassLoader{
        JarClassLoader(URL[] urls) {
            super(urls);  
        }  
        
        @Override
        public void addURL(URL url) {  
            super.addURL(url);  
        }  
    }  
    
	private class CheckConnectionActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				UseDatabaseWizardStep.this.applyState();				
			} catch (InvalidStateException e2) {
				JOptionPane.showMessageDialog(null, e2.getMessage());
				return;
			}
			JOptionPane.showMessageDialog(null, L10n.getMessage("validation.valid.db.connection"));
		}
	}

	private class DatabaseComboBoxItem extends ComboBoxItem {

		final DbType type;

		DatabaseComboBoxItem(DbType type) {
			super(type.getAdapterName(), type.getDisplayName());
			this.type = type;
		}
	}
}
