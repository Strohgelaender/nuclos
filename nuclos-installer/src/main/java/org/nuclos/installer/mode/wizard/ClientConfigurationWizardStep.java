//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode.wizard;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.jar.JarFile;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.apache.log4j.Logger;
import org.nuclos.installer.CheckJavaVersion;
import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.InstallException;
import org.nuclos.installer.L10n;
import org.nuclos.installer.unpack.AbstractUnpacker;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;


public class ClientConfigurationWizardStep extends AbstractWizardStep implements CaretListener {

	private static final Logger LOG = Logger.getLogger(ClientConfigurationWizardStep.class);

	private final JCheckBox chkSingleInstance = new JCheckBox();
	private final JCheckBox chkRichClient = new JCheckBox();
	private final JCheckBox chkWebClient = new JCheckBox();
	private final JCheckBox chkLauncher = new JCheckBox();
	private final JLabel lblAboutWebClient = new JLabel();

	private final JTextField txtServerHost = new JTextField();

	private final JTextField txtJREPath = new JTextField();
	private final JButton btnSelectPath = new JButton();

	private static final double[][]	layout			= {
		{ 20.0, 240.0, TableLayout.FILL, 100.0 }, // Columns
		{ 20.0, 20.0, 20.0, 20.0, 20.0, 20.00, TableLayout.FILL } };		// Rows

	public ClientConfigurationWizardStep() {
		super(L10n.getMessage("gui.wizard.client.title"), L10n.getMessage("gui.wizard.client.description"));

		TableLayout layout = new TableLayout(this.layout);
		layout.setVGap(5);
		layout.setHGap(5);
		this.setLayout(layout);

		JLabel label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.client.singleinstance.label"));
		this.add(label, "1,0,3,0");

		this.chkSingleInstance.addActionListener(this);
		this.add(chkSingleInstance, "0,0");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.client.richclient.label"));
		this.add(label, "1,1,1,1");
		this.chkRichClient.addActionListener(this);
		this.add(chkRichClient, "0,1");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.client.launcher.label"));
		this.add(label, "1,2,3,2");
		this.chkLauncher.addActionListener(this);
		this.add(chkLauncher, "0,2");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.client.webclient.label"));
		this.add(label, "1,3,3,3");
		this.chkWebClient.addActionListener(this);
		this.add(chkWebClient, "0,3");
		this.add(lblAboutWebClient, "2,3,3,3");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.client.serverhost.label"));
		this.add(label, "1,4");
		this.txtServerHost.addCaretListener(this);
		this.add(txtServerHost, "2,4");
		
        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.client.jre.label"));
        this.add(label, "1,5");
		
        this.txtJREPath.getDocument().addDocumentListener(this);
		this.add(txtJREPath, "2,5");

		btnSelectPath.setText(L10n.getMessage("filechooser.browse"));
		btnSelectPath.addActionListener(this);
        this.add(btnSelectPath, "3,5");

	}

	@Override
	public void prepare() {
		modelToView(CLIENT_SINGLEINSTANCE, chkSingleInstance);
		modelToView(CLIENT_RICHCLIENT, chkRichClient);
		modelToView(CLIENT_WEBCLIENT, chkWebClient);
		modelToView(CLIENT_LAUNCHER, chkLauncher);
		modelToView(CLIENT_SERVERHOST, txtServerHost);
		modelToView(CLIENT_JRE, txtJREPath);
		updateState();
	}

	@Override
	protected void updateState() {
		setComplete(true);
		SwingUtilities.invokeLater(() -> {
			setLblAboutWebClientText(ConfigContext.getProperty(CLIENT_SERVERHOST));
			updateLauncherCheckbox();
		});
	}

	private void updateLauncherCheckbox() {
		if (chkRichClient.isSelected()) {
			chkLauncher.setEnabled(true);
		} else {
			chkLauncher.setSelected(false);
			chkLauncher.setEnabled(false);
		}
	}

	@Override
	public void applyState() throws InvalidStateException {
		viewToModel(CLIENT_SINGLEINSTANCE, chkSingleInstance);
		viewToModel(CLIENT_RICHCLIENT, chkRichClient);
		viewToModel(CLIENT_WEBCLIENT, chkWebClient);
		viewToModel(CLIENT_LAUNCHER, chkLauncher);
		viewToModel(CLIENT_SERVERHOST, txtServerHost);
		viewToModel(CLIENT_JRE, txtJREPath);
		try {
			ConfigContext.getCurrentConfig().setDerivedProperties();
			ConfigContext.getCurrentConfig().verify();
			setComplete(true);
		}
		catch(InstallException e) {
			throw new InvalidStateException(e.getLocalizedMessage());
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnSelectPath) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.jre.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

	        int returnVal = chooser.showOpenDialog(this);

	        //Process the results.
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File file = chooser.getSelectedFile();
	            
	            File rtJar = new File(file, "lib/rt.jar");
	            File rtExecUnix = new File(file,"bin/java");
	            File rtExecWin = new File(file,"bin/java.exe");
	            if (rtJar.exists() && (rtExecUnix.exists() || rtExecWin.exists())) {

					try (JarFile jarFile = new JarFile(rtJar)) {
						String version = jarFile.getManifest().getMainAttributes().getValue("Implementation-Version");

						if (CheckJavaVersion.checkVersion(version)) {
							txtJREPath.setText(file.getAbsolutePath());
							return;
						}

						getModel().getCallback().info("info.wrong.jre.version", version, CheckJavaVersion.minJavaVersion);
						return;

					} catch (IOException io) {
						LOG.info(io.getMessage(), io);
					}
	            		       
	            }
	            
    			getModel().getCallback().info("info.no.valid.jre", file.getName());
	            
	        }
	        
		} else {
			super.actionPerformed(e);
		}
 	}
	
	private void setLblAboutWebClientText(String serverHost) {
		if (chkWebClient.isSelected()) {
			String baseUrl = AbstractUnpacker.getBaseUrlforWebclient(false, serverHost);
			String htmlUrl = "<html>URL: <a href=\"" + baseUrl + "\">" + baseUrl + "</a>";
			lblAboutWebClient.setText(htmlUrl);
		} else {
			lblAboutWebClient.setText(null);
		}
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		setLblAboutWebClientText(txtServerHost.getText());
	}

}
