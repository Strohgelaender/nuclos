//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode;

import java.io.Console;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.InstallException;
import org.nuclos.installer.L10n;
import org.nuclos.installer.util.FileUtils;

/**
 * @author Thomas Pasch
 */
public class BatchInstaller extends CliInstaller {

	public BatchInstaller(Console console) {
		super(console);
	}

	/**
	 * Don't read anything from command line, just use default values.
	 */
	@Override
	void getProperty(String property, String...aValues) {
		final List<String> values = aValues == null ? new ArrayList<>() : Arrays.asList(aValues);
		while (true) {
			final String value = ConfigContext.containsKey(property) ? ConfigContext.getProperty(property) : "";
			if (values.size() > 0 && !values.contains(value)) {
				info("cli.enum.error", values.toString());
			}
			else {
				try {
					unpacker.validate(property, value);
					ConfigContext.setProperty(property, value);
					return;
				}
				catch (InstallException ex) {
					error(ex.getLocalizedMessage());
					FileUtils.copyLog();
					System.exit(1);
				}
			}
		}
	}

	/**
	 * Don't read anything from command line, just use default values.
	 */
	@Override
	void getPasswordProperty(String property) {
		while (true) {
			final String value = ConfigContext.containsKey(property) ? ConfigContext.getProperty(property) : "";
			try {
				unpacker.validate(property, value);
				ConfigContext.setProperty(property, value);
				return;
			}
			catch (InstallException ex) {
				error(ex.getLocalizedMessage());
				FileUtils.copyLog();
				System.exit(1);
			}
		}
	}

	/**
	 * Always return y(es) on questions.
	 */
	@Override
	public int askQuestion(String text, int questiontype, int defaultanswer, Object...args) {
		while (true) {
			writer.print(L10n.getMessage(text, args));
			if (QUESTION_YESNO == questiontype) {
				writer.print(" y\n");
				return ANSWER_YES;
			}
			if (QUESTION_OKCANCEL == questiontype) {
				writer.print(" 1\n");
				return ANSWER_OK;
			}
		}
	}

}
