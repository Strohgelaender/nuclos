//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode.wizard;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;

import javax.swing.*;

import org.nuclos.installer.Constants;
import org.nuclos.installer.L10n;
import org.nuclos.installer.database.DbType;
import org.pietschy.wizard.ButtonBar;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
 * Set target installation path
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 */
public class EmbeddedDatabaseWizardStep extends AbstractWizardStep implements ActionListener, Constants {
	
	private ButtonBar buttonBar;
	
	private final JButton btnCheckConnection = new JButton();

	private final JTextField txtDbUsername = new JTextField();

	private final JPasswordField txtDbPassword1 = new JPasswordField();
	private final JPasswordField txtDbPassword2 = new JPasswordField();

	private final JTextField txtSchema = new JTextField();

	private static final double[][] layout = {
        { 200.0, TableLayout.FILL, 100.0 }, // Columns
        { 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, TableLayout.FILL } };// Rows

	public EmbeddedDatabaseWizardStep() {
		super(L10n.getMessage("gui.wizard.embeddeddb.title"), L10n.getMessage("gui.wizard.embeddeddb.description"));

		TableLayout layout = new TableLayout(this.layout);
        layout.setVGap(5);
        layout.setHGap(5);
        this.setLayout(layout);

        JLabel label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.installdb.dbuser.label"));
        this.add(label, "0,0");

        txtDbUsername.getDocument().addDocumentListener(this);
        this.add(txtDbUsername, "1,0");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.installdb.dbpassword1.label"));
        this.add(label, "0,1");

        txtDbPassword1.getDocument().addDocumentListener(this);
        this.add(txtDbPassword1, "1,1");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.installdb.dbpassword2.label"));
        this.add(label, "0,2");

        txtDbPassword2.getDocument().addDocumentListener(this);
        this.add(txtDbPassword2, "1,2");

        label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.installdb.schema.label"));
        this.add(label, "0,3");

        txtSchema.getDocument().addDocumentListener(this);
        this.add(txtSchema, "1,3");
        
		btnCheckConnection.setText(L10n.getMessage("validation.db.connection"));
		btnCheckConnection.addActionListener(new CheckConnectionActionListener());
		btnCheckConnection.setEnabled(false);
		
		addHierarchyListener(e -> {
			if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
				if (e.getChanged().isShowing()) {
					buttonBar = (ButtonBar)EmbeddedDatabaseWizardStep.this.findFirstJComponentInParent(
							(JComponent)EmbeddedDatabaseWizardStep.this.getParent(), ButtonBar.class);
					if (buttonBar != null) {
						buttonBar.add(btnCheckConnection, 0);
						buttonBar.revalidate();
						buttonBar.repaint();
					}
				}
				else {
					if (buttonBar != null) {
						buttonBar.remove(btnCheckConnection);
						buttonBar.revalidate();
						buttonBar.repaint();
					}
				}
			}
		});
	}

	@Override
	public void prepare() {
		modelToView(DATABASE_USERNAME, txtDbUsername);
		modelToView(DATABASE_PASSWORD, txtDbPassword1);
		modelToView(DATABASE_PASSWORD, txtDbPassword2);
		modelToView(DATABASE_SCHEMA, txtSchema);
		this.setComplete(true);
	}
	
	@Override
	protected void updateState() {
		this.setComplete(true);
	}

	@Override
	public void applyState() throws InvalidStateException {
		viewToModel(DATABASE_USERNAME, txtDbUsername);
		validatePasswordEquality(txtDbPassword1, txtDbPassword2, "gui.wizard.installdb.dbpassword1.label");
		viewToModel(DATABASE_PASSWORD, txtDbPassword1);
		viewToModel(DATABASE_SCHEMA, txtSchema);
		viewToModel(DATABASE_ADAPTER, new JTextField(DbType.H2.getAdapterName()));

		getRootPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		try {
		}
		catch (Exception ex) {
			throw new InvalidStateException(L10n.getMessage("validation.invalid.db.connection") + "\n" + ex.getMessage());
		} finally {
			getRootPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));			
		}
	}
	
	private class CheckConnectionActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				EmbeddedDatabaseWizardStep.this.applyState();				
			} catch (InvalidStateException e2) {
				JOptionPane.showMessageDialog(null, e2.getMessage());
				return;
			}
			JOptionPane.showMessageDialog(null, L10n.getMessage("validation.valid.db.connection"));
		}
	}
}
