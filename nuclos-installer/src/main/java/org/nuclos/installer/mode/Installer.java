//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode;

import org.nuclos.installer.InstallException;
import org.nuclos.installer.unpack.Unpacker;
import org.nuclos.installer.util.FileUtils;

/**
 * Installer mode (collect information)
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 */
public interface Installer {

	int QUESTION_YESNO = 0;
	int QUESTION_OKCANCEL = 1;

	int ANSWER_YES = 0;
	int ANSWER_NO = 1;
	int ANSWER_OK = 2;
	int ANSWER_CANCEL = 3;

	void install(Unpacker os) throws InstallException;

	void uninstall(Unpacker os) throws InstallException;

	void info(String resid, Object... args);

	void warn(String resid, Object... args);

	void error(String resid, Object... args);

	void logException(Throwable e);

	int askQuestion(String text, int questiontype, int automatedAnswer, Object... args);

	default void close() {
		FileUtils.copyLog();
		System.exit(0);
	}

	default void cancel() {
		FileUtils.copyLog();
		System.exit(0);
	}
}
