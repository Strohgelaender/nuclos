//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;

import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.Constants;
import org.nuclos.installer.InstallException;
import org.nuclos.installer.L10n;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
 * Set target installation path
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 */
public class TargetPathWizardStep extends AbstractWizardStep implements ActionListener, Constants {

	private final JTextField txtTargetPath = new JTextField();
	private final JButton btnSelectPath = new JButton();

	private static final double[][] layout = {
        { TableLayout.FILL, 100.0 }, // Columns
        { 20.0, 20.0, TableLayout.FILL } };// Rows

	public TargetPathWizardStep() {
		super(L10n.getMessage("gui.wizard.path.title"), L10n.getMessage("gui.wizard.path.description"));

		TableLayout layout = new TableLayout(this.layout);
        layout.setVGap(5);
        layout.setHGap(5);
        this.setLayout(layout);

        JLabel label = new JLabel();
        label.setText(L10n.getMessage("gui.wizard.path.select.label"));
        this.add(label, "0,0, 1,0");

        this.txtTargetPath.getDocument().addDocumentListener(this);
		this.add(txtTargetPath, "0,1");

		btnSelectPath.setText(L10n.getMessage("filechooser.browse"));
		btnSelectPath.addActionListener(this);
        this.add(btnSelectPath, "1,1");
	}

	@Override
	public void prepare() {
		File nuclosHome;
		String nh = ConfigContext.getCurrentConfig().getProperty(NUCLOS_HOME);
		if (nh == null) {
			File f = new File(System.getProperty("user.home"));
			if (f.canWrite() && f.isDirectory()) {
				nuclosHome = new File(f, "nuclos");
			}
			else {
				nuclosHome = null;
			}
		}
		else {
			nuclosHome = new File(nh);
			final File parent = nuclosHome.getParentFile();
			if (!parent.canWrite() || !parent.isDirectory()) {
				File f = new File(System.getProperty("user.home"));
				if (f.canWrite() && f.isDirectory()) {
					nuclosHome = new File(f, "nuclos");
				}
			}
		}
		if (nuclosHome != null) {
			txtTargetPath.setText(nuclosHome.toString());
		}
		else {
			txtTargetPath.setText("");
		}
	}

	@Override
	protected void updateState() {
		if (txtTargetPath.getText() != null) {
			ConfigContext.getCurrentConfig().setProperty(NUCLOS_HOME, txtTargetPath.getText());
		
			setComplete(true);
		}
		else {
			setComplete(false);
		}
	}

	@Override
	public void applyState() throws InvalidStateException {
		validate(NUCLOS_HOME, txtTargetPath.getText());
		
		// if the document path is not set yet we change the default according to this nuclos home setting
		if (ConfigContext.getCurrentConfig().getProperty(DOCUMENT_PATH) == null) {
			ConfigContext.getCurrentConfig().setProperty(DOCUMENT_PATH, 
					ConfigContext.getCurrentConfig().getProperty(NUCLOS_HOME) + File.separator + 
					DIR_NAME_DATA + File.separator + DIR_NAME_DOCUMENTS);
		}
		// if the index path is not set yet we change the default according to this nuclos home setting
		if (ConfigContext.getCurrentConfig().getProperty(INDEX_PATH) == null) {
			ConfigContext.getCurrentConfig().setProperty(INDEX_PATH, 
					ConfigContext.getCurrentConfig().getProperty(NUCLOS_HOME) + File.separator + 
					DIR_NAME_DATA + File.separator + DIR_NAME_INDEX);
		}
		
		if (ConfigContext.isUpdate()) {
			getModel().getCallback().info("info.update.backup");
			try {
				getModel().getUnpacker().shutdown(getModel().getCallback());
			} catch (InstallException e) {
				throw new InvalidStateException(e.getLocalizedMessage());
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int returnVal = chooser.showOpenDialog(this);

        //Process the results.
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            txtTargetPath.setText(file.getAbsolutePath());
        }
	}
}
