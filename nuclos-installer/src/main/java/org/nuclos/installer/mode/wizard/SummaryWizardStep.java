//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode.wizard;

import javax.swing.*;
import javax.swing.text.html.HTMLEditorKit;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.log4j.Logger;
import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.Constants;
import org.nuclos.installer.L10n;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.WizardModel;

import info.clearthought.layout.TableLayout;

import java.io.*;
import java.net.URL;
import java.util.Collections;
import java.util.Locale;

/**
 * Show and accept Nuclos license.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 */
public class SummaryWizardStep extends AbstractWizardStep {

	private static final Logger LOG = Logger.getLogger(SummaryWizardStep.class);

	private final JEditorPane pane = new JEditorPane();

	private static final double[][] layout = {
        { TableLayout.FILL }, // Columns
        { TableLayout.FILL } // Rows
    };

	public SummaryWizardStep() {
		super(L10n.getMessage("gui.wizard.summary.title"), L10n.getMessage("gui.wizard.summary.description"));
	}

	@Override
	public void init(WizardModel model) {
		super.init(model);
		TableLayout layout = new TableLayout(this.layout);
        layout.setVGap(5);
        layout.setHGap(5);
        this.setLayout(layout);

        pane.setEditable(false);
        pane.setContentType("text/html");
        pane.setEditorKit(new HTMLEditorKit());

		final JScrollPane scrlpane = new JScrollPane();
		scrlpane.getViewport().add(pane, null);
		this.add(scrlpane, "0,0");
	}

	@Override
	public void prepare() {
		setComplete(true);
		SwingUtilities.invokeLater(() -> {
			try {
				pane.setText(createSummaryHtml());
			}
			catch(/* IO */ Exception e) {
				// throw new RuntimeException(e);
				LOG.error("prepare failed: " + e, e);
			}
		});
	}

	private String createSummaryHtml() throws IOException, TemplateException {
		//Freemarker
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
		URL directory = SummaryWizardStep.class.getClassLoader().getResource("org/nuclos/installer/resource/");
		if (directory != null) {
			cfg.setDirectoryForTemplateLoading(new File(directory.getFile()));
			Template template = cfg.getTemplate("summary.ftl", getLocale());
			StringWriter stringWriter = new StringWriter();
			template.process(ConfigContext.getCurrentConfig(), stringWriter);
			return stringWriter.toString();
		}
		return "";
	}

	@Override
	protected void updateState() { }

	@Override
	public void applyState() { }
}
