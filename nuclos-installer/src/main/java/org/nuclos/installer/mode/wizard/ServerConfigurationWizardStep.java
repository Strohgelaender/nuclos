// Copyright (C) 2010 Novabit Informationssysteme GmbH
//
// This file is part of Nuclos.
//
// Nuclos is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nuclos is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Nuclos. If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode.wizard;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.*;

import org.json.HTTP;
import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.Constants;
import org.nuclos.installer.L10n;
import org.nuclos.installer.mode.Installer;
import org.nuclos.installer.unpack.GenericUnpacker;
import org.nuclos.installer.unpack.Unpacker;
import org.nuclos.installer.unpack.WindowsUnpacker;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
 * Collect server configuration
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 */
public class ServerConfigurationWizardStep extends AbstractWizardStep implements Constants {

	private final JTextField txtJavaHomePath = new JTextField();
	private final JButton btnJavaHomeSelect = new JButton();

	private final JButton btnDocumentHomePathSelect = new JButton();
	private final JTextField txtDocumentHomePath = new JTextField();
	private final JButton btnIndexSelect = new JButton();
	private final JTextField txtIndexPath = new JTextField();

	private final JTextField txtInstance = new JTextField();
	private final JTextField txtPort = new JTextField();

	private final JLabel tomcatConfigOverridenWarning = new JLabel();
	private final ButtonGroup group = new ButtonGroup();
	private final JRadioButton optHttp = new JRadioButton();
	private final JRadioButton optHttps = new JRadioButton();

	private final JTextField txtHttpsPort = new JTextField();
	private final JTextField txtHttpsKeystoreFile = new JTextField();
	private final JButton btnHttpsKeystoreSelect = new JButton();
	private final JPasswordField txtKeystorePassword1 = new JPasswordField();
	private final JPasswordField txtKeystorePassword2 = new JPasswordField();
	private final JTextField txtShutdownPort = new JTextField();
	private final JTextField txtMemory = new JTextField();

	private final Set<Component> tomcatConfigurationComponents = new HashSet<>();
	private final ButtonGroup group2 = new ButtonGroup();
	private final JRadioButton optProduction = new JRadioButton();
	private final JRadioButton optDevelopment = new JRadioButton();

	private final JTextField txtDebugport = new JTextField();
	private final JTextField txtJmxport = new JTextField();

	private final JCheckBox chkLaunch = new JCheckBox();

	private final JCheckBox chkAjp = new JCheckBox();
	private final JTextField txtAjpport = new JTextField();

	private final JCheckBox chkClusterMode = new JCheckBox();

	private static final double[][] layout = {{20.0, TableLayout.PREFERRED, TableLayout.FILL, 100.0}, // Columns
			{20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, TableLayout.FILL}}; // Rows

	public ServerConfigurationWizardStep() {
		super(L10n.getMessage("gui.wizard.server.title"), L10n.getMessage("gui.wizard.server.description"));

		TableLayout layout = new TableLayout(this.layout);
		layout.setVGap(5);
		layout.setHGap(5);
		this.setLayout(layout);

		JLabel label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.javahomepath.label"));
		this.add(label, "0,0, 1,0");

		this.txtJavaHomePath.getDocument().addDocumentListener(this);
		this.add(txtJavaHomePath, "2,0");

		btnJavaHomeSelect.setText(L10n.getMessage("filechooser.browse"));
		btnJavaHomeSelect.addActionListener(new SelectJavaHomeActionListener());
		this.add(btnJavaHomeSelect, "3,0");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.instance.label"));
		this.add(label, "0,1, 1,1");

		this.txtInstance.getDocument().addDocumentListener(this);
		this.add(txtInstance, "2,1");

		JLabel lblDocPath = new JLabel();
		lblDocPath.setText(L10n.getMessage("gui.wizard.server.dochomepath.label"));
		this.add(lblDocPath, "0,2, 1,2");

		this.txtDocumentHomePath.getDocument().addDocumentListener(this);
		this.add(txtDocumentHomePath, "2,2");

		btnDocumentHomePathSelect.setText(L10n.getMessage("filechooser.browse"));
		btnDocumentHomePathSelect.addActionListener(new SelectDocumentHomeActionListener());
		this.add(btnDocumentHomePathSelect, "3,2");

		JLabel lblIndexPath = new JLabel();
		lblIndexPath.setText(L10n.getMessage("gui.wizard.server.indexpath.label"));
		this.add(lblIndexPath, "0,3, 1,3");

		this.txtIndexPath.getDocument().addDocumentListener(this);
		this.add(txtIndexPath, "2,3");

		btnIndexSelect.setText(L10n.getMessage("filechooser.browse"));
		btnIndexSelect.addActionListener(new SelectIndexActionListener());
		this.add(btnIndexSelect, "3,3");

		addTomcatConfigurationComponents();

		optProduction.addActionListener(this);
		this.add(optProduction, "0,12");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.production.label"));
		this.add(label, "1,12");

		optDevelopment.addActionListener(this);
		this.add(optDevelopment, "0,13");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.development.label"));
		this.add(label, "1,13");
		this.txtDebugport.getDocument().addDocumentListener(this);
		this.add(txtDebugport, "2,13");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.development.jmxport.label"));
		this.add(label, "1,14");
		this.txtJmxport.getDocument().addDocumentListener(this);
		this.add(txtJmxport, "2,14");

		chkLaunch.setText(L10n.getMessage("gui.wizard.server.launch.label"));
		this.chkLaunch.addActionListener(this);
		this.add(chkLaunch, "0,15 2,15");

		chkClusterMode.setText(L10n.getMessage("gui.wizard.server.cluster.label"));
		this.chkClusterMode.addActionListener(this);
		this.add(chkClusterMode, "0,16 2,16");

		group2.add(optProduction);
		group2.add(optDevelopment);
	}

	private void updateTomcatConfigState() {
		if (getUnpacker().isTomcatConfigOverriddenByExtension()) {
			tomcatConfigOverridenWarning.setVisible(true);
			for (Component comp: tomcatConfigurationComponents) {
				comp.setEnabled(false);
			}
		} else {
			tomcatConfigOverridenWarning.setVisible(false);
		}
	}

	private void addTomcatConfigurationComponents() {
		addTomcatConfigurationOverriddenWarning();

		optHttp.addActionListener(this);
		addTomcatConfigComponent(optHttp, "0,4");

		JLabel label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.port.label"));
		addTomcatConfigComponent(label, "1,4");

		this.txtPort.getDocument().addDocumentListener(this);
		addTomcatConfigComponent(txtPort, "2,4");

		optHttps.addActionListener(this);
		addTomcatConfigComponent(optHttps, "0,5");

		group.add(optHttp);
		group.add(optHttps);

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.httpsport.label"));
		addTomcatConfigComponent(label, "1,5");

		this.txtHttpsPort.getDocument().addDocumentListener(this);
		addTomcatConfigComponent(txtHttpsPort, "2,5");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.httpskeystorefile.label"));
		addTomcatConfigComponent(label, "1,6");

		this.txtHttpsKeystoreFile.getDocument().addDocumentListener(this);
		addTomcatConfigComponent(txtHttpsKeystoreFile, "2,6");

		btnHttpsKeystoreSelect.setText(L10n.getMessage("filechooser.browse"));
		btnHttpsKeystoreSelect.addActionListener(new SelectKeyStoreActionListener());
		addTomcatConfigComponent(btnHttpsKeystoreSelect, "3,6");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.httpskeystorepassword1.label"));
		addTomcatConfigComponent(label, "1,7");

		this.txtKeystorePassword1.getDocument().addDocumentListener(this);
		addTomcatConfigComponent(txtKeystorePassword1, "2,7");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.httpskeystorepassword2.label"));
		addTomcatConfigComponent(label, "1,8");

		this.txtKeystorePassword2.getDocument().addDocumentListener(this);
		addTomcatConfigComponent(txtKeystorePassword2, "2,8");

		chkAjp.setText("AJP Port");
		this.chkAjp.addActionListener(this);
		this.chkAjp.addActionListener(e -> txtAjpport.setEnabled(chkAjp.isEnabled()));
		addTomcatConfigComponent(chkAjp, "0,9 1,9");

		this.txtAjpport.getDocument().addDocumentListener(this);
		addTomcatConfigComponent(txtAjpport, "2,9");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.shutdownport.label"));
		addTomcatConfigComponent(label, "0,10, 1,10");

		this.txtShutdownPort.getDocument().addDocumentListener(this);
		addTomcatConfigComponent(txtShutdownPort, "2,10");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.memory.label"));
		addTomcatConfigComponent(label, "0,11, 1,11");

		this.txtMemory.getDocument().addDocumentListener(this);
		addTomcatConfigComponent(txtMemory, "2,11");
	}

	private void addTomcatConfigComponent(Component component, String constraints) {
		this.add(component, constraints);
		tomcatConfigurationComponents.add(component);
	}

	private void addTomcatConfigurationOverriddenWarning() {
		String tomcatWarningText = L10n.getMessage("gui.wizard.tomcat.overridden.by.extension");
		tomcatConfigOverridenWarning.setText("<html><pre>" + tomcatWarningText + "</pre></html>");

		Map<TextAttribute, Object> attributes = (Map<TextAttribute, Object>) tomcatConfigOverridenWarning.getFont().getAttributes();
		attributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
		tomcatConfigOverridenWarning.setFont(new Font(attributes));

		tomcatConfigOverridenWarning.setForeground(Color.WHITE);
		tomcatConfigOverridenWarning.setBackground(new Color(0, 0, 0, 200));
		tomcatConfigOverridenWarning.setOpaque(true);
		tomcatConfigOverridenWarning.setHorizontalTextPosition(JLabel.CENTER);
		tomcatConfigOverridenWarning.setHorizontalAlignment(JLabel.CENTER);

		// Prevent clicking the underlying components
		tomcatConfigOverridenWarning.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				e.consume();
			}

			@Override
			public void mousePressed(final MouseEvent e) {
				e.consume();
			}

			@Override
			public void mouseReleased(final MouseEvent e) {
				e.consume();
			}

			@Override
			public void mouseEntered(final MouseEvent e) {
				e.consume();
			}

			@Override
			public void mouseExited(final MouseEvent e) {
				e.consume();
			}
		});

		this.add(tomcatConfigOverridenWarning, "0,4,3,11");
	}

	@Override
	public void prepare() {
		this.txtInstance.setEnabled(!ConfigContext.isUpdate());
		modelToView(JAVA_HOME, txtJavaHomePath);
		modelToView(NUCLOS_INSTANCE, txtInstance);
		modelToView(DOCUMENT_PATH, txtDocumentHomePath);
		modelToView(INDEX_PATH, txtIndexPath);
		this.txtDocumentHomePath.setEnabled(!ConfigContext.isUpdate());
		this.btnDocumentHomePathSelect.setEnabled(!ConfigContext.isUpdate());

		optHttp.setSelected("true".equals(ConfigContext.getProperty(HTTP_ENABLED)));
		modelToView(HTTP_PORT, txtPort);
		optHttps.setSelected("true".equals(ConfigContext.getProperty(HTTPS_ENABLED)));
		modelToView(HTTPS_PORT, txtHttpsPort);
		modelToView(HTTPS_KEYSTORE_FILE, txtHttpsKeystoreFile);
		modelToView(HTTPS_KEYSTORE_PASSWORD, txtKeystorePassword1);
		modelToView(HTTPS_KEYSTORE_PASSWORD, txtKeystorePassword2);
		modelToView(SHUTDOWN_PORT, txtShutdownPort);
		modelToView(HEAP_SIZE, txtMemory);
		optProduction.setSelected("true".equals(ConfigContext.getProperty(PRODUCTION_ENABLED)));
		optDevelopment.setSelected("true".equals(ConfigContext.getProperty(DEVELOPMENT_ENABLED)));
		modelToView(DEBUG_PORT, txtDebugport);
		modelToView(JMX_PORT, txtJmxport);
		modelToView(LAUNCH_STARTUP, chkLaunch);
		modelToView(CLUSTER_MODE, chkClusterMode);
		modelToView(AJP_ENABLED, chkAjp);
		txtAjpport.setEnabled("true".equals(ConfigContext.getProperty(AJP_ENABLED)));
		modelToView(AJP_PORT, txtAjpport);

		this.chkLaunch.setEnabled(!(getModel().getUnpacker() instanceof GenericUnpacker));
	}

	@Override
	protected void updateState() {
		txtPort.setEnabled(optHttp.isSelected());
		txtHttpsPort.setEnabled(optHttps.isSelected());
		txtHttpsKeystoreFile.setEnabled(optHttps.isSelected());
		btnHttpsKeystoreSelect.setEnabled(optHttps.isSelected());
		txtKeystorePassword1.setEnabled(optHttps.isSelected());
		txtKeystorePassword2.setEnabled(optHttps.isSelected());
		txtDebugport.setEnabled(optDevelopment.isSelected());
		txtJmxport.setEnabled(optDevelopment.isSelected());
		setComplete(true);

		updateTomcatConfigState();
	}

	@Override
	public void applyState() throws InvalidStateException {
	    viewToModel(JAVA_HOME, txtJavaHomePath);
		viewToModel(NUCLOS_INSTANCE, txtInstance);
		viewToModel(DOCUMENT_PATH, txtDocumentHomePath);
		viewToModel(INDEX_PATH, txtIndexPath);
		ConfigContext.setProperty(HTTP_ENABLED, optHttp.isSelected() ? "true" : "false");
		try {
		    viewToModel(HTTP_PORT, txtPort);
        } catch (InvalidStateException e) {
            hadlePortException(e, txtPort.getText()); //NUCLOS-7210
            ConfigContext.setProperty(HTTP_PORT, txtPort.getText());
        }
        ConfigContext.setProperty(HTTPS_ENABLED, optHttps.isSelected() ? "true" : "false");
		try {
            viewToModel(HTTPS_PORT, txtHttpsPort);
        } catch (InvalidStateException e) {
		    hadlePortException(e, txtHttpsPort.getText()); //NUCLOS-7210
            ConfigContext.setProperty(HTTPS_PORT, txtHttpsPort.getText());
        }
		viewToModel(HTTPS_KEYSTORE_FILE, txtHttpsKeystoreFile);
		if (optHttps.isSelected()) {
			validatePasswordEquality(txtKeystorePassword1, txtKeystorePassword2, "gui.wizard.server.httpskeystorepassword1.label");
		}
		viewToModel(HTTPS_KEYSTORE_PASSWORD, txtKeystorePassword1);
		viewToModel(SHUTDOWN_PORT, txtShutdownPort);
		viewToModel(HEAP_SIZE, txtMemory);
		ConfigContext.setProperty(PRODUCTION_ENABLED, optProduction.isSelected() ? "true" : "false");
		ConfigContext.setProperty(DEVELOPMENT_ENABLED, optDevelopment.isSelected() ? "true" : "false");
		viewToModel(DEBUG_PORT, txtDebugport);
		viewToModel(JMX_PORT, txtJmxport);
		viewToModel(LAUNCH_STARTUP, chkLaunch);
		viewToModel(CLUSTER_MODE, chkClusterMode);
		viewToModel(AJP_ENABLED, chkAjp);
		viewToModel(AJP_PORT, txtAjpport);

		Unpacker unpacker = getModel().getUnpacker();
		if (unpacker instanceof WindowsUnpacker) {
			try {
				String java = ConfigContext.getProperty(JAVA_HOME) + "/bin/java.exe";

				if (((WindowsUnpacker) unpacker).isAmd64() && !is64BitJava(new File(java)))
					throw new InvalidStateException(L10n.getMessage("validation.javahome.use64"));
				else if (!((WindowsUnpacker) unpacker).isAmd64() && is64BitJava(new File(java)))
					throw new InvalidStateException(L10n.getMessage("validation.javahome.use32"));

			} catch (Exception e) {
				throw new InvalidStateException(e.getMessage());
			}
		}
	}

	private void hadlePortException(InvalidStateException e, String port) throws InvalidStateException {
        if (e.getMessage().equals(L10n.getMessage("validation.port.insecure", port))) {
                int answer = getModel().getCallback().askQuestion(L10n.getMessage("validation.port.insecure", port), Installer.QUESTION_YESNO, Installer.ANSWER_NO);
                if (answer == Installer.ANSWER_NO) {
                    throw new InvalidStateException();
                }
        } else {
            throw e;
        }
    }

	private class SelectDocumentHomeActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {

			File pathToUse = ConfigContext.getFileProperty(DOCUMENT_PATH);
			if (!pathToUse.exists()) {
				pathToUse = ConfigContext.getFileProperty(NUCLOS_HOME);
			}

			if (txtDocumentHomePath.getText() != null) {
				File docPathtemp = new File(txtDocumentHomePath.getText());
				if (pathToUse.exists() && docPathtemp.isDirectory()) {
					pathToUse = docPathtemp;
				}
			}

			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(pathToUse);
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			int returnVal = chooser.showOpenDialog(ServerConfigurationWizardStep.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				txtDocumentHomePath.setText(file.getAbsolutePath());
			}
		}
	}

	private class SelectIndexActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {

			File pathToUse = ConfigContext.getFileProperty(INDEX_PATH);
			if (!pathToUse.exists()) {
				pathToUse = ConfigContext.getFileProperty(NUCLOS_HOME);
			}

			if (txtIndexPath.getText() != null) {
				File docPathtemp = new File(txtIndexPath.getText());
				if (pathToUse.exists() && docPathtemp.isDirectory()) {
					pathToUse = docPathtemp;
				}
			}

			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(pathToUse);
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			int returnVal = chooser.showOpenDialog(ServerConfigurationWizardStep.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				txtIndexPath.setText(file.getAbsolutePath());
			}
		}
	}

	private class SelectJavaHomeActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			int returnVal = chooser.showOpenDialog(ServerConfigurationWizardStep.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				txtJavaHomePath.setText(file.getAbsolutePath());
			}
		}
	}

	private class SelectKeyStoreActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

			int returnVal = chooser.showOpenDialog(ServerConfigurationWizardStep.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				txtHttpsKeystoreFile.setText(file.getAbsolutePath());
			}
		}
	}

	private static boolean is64BitJava(File exe) throws IOException {
		InputStream is = new FileInputStream(exe);
		int magic = is.read() | is.read() << 8;
		if (magic != 0x5A4D)
			throw new IOException("Invalid Exe");
		for (int i = 0; i < 58; i++) is.read(); // skip until pe offset
		int address = is.read() | is.read() << 8 |
				is.read() << 16 | is.read() << 24;
		for (int i = 0; i < address - 60; i++) is.read(); // skip until pe header+4
		int machineType = is.read() | is.read() << 8;
		return machineType == 0x8664;
	}
}
