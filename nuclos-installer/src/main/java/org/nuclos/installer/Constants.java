//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.installer;

/**
 * Constants.
 */
public interface Constants {

	/**
	 * Nuclos home is the target installation path
	 */
	String NUCLOS_HOME = "server.home";

	/**
	 * Nuclos.xml filename
	 */
	String NUCLOS_XML = "nuclos.xml";

	/**
	 * JRE home
	 */
	String JAVA_HOME = "server.java.home";


	/**
	 * JVM DLL (Required for windows service)
	 */
	String JAVA_JVMDLL = "server.java.jvm";

	String NUCLOS_INSTANCE = "server.name";
	String WEBCLIENT_INSTANCE = "webclient.name";
	String DOCUMENT_PATH = "server.documentPath";
	String INDEX_PATH = "server.indexPath";
	String HTTP_ENABLED = "server.http.enabled";
	String HTTP_PORT = "server.http.port";
	String HTTPS_ENABLED = "server.https.enabled";
	String HTTPS_PORT = "server.https.port";
	String HTTPS_KEYSTORE_FILE = "server.https.keystore.file";
	String HTTPS_KEYSTORE_PASSWORD = "server.https.keystore.password";
	String PRODUCTION_ENABLED = "server.production.enabled";
	String DEVELOPMENT_ENABLED = "server.development.enabled";
	String DEBUG_PORT = "server.development.debugport";
	String JMX_PORT = "server.development.jmxport";
	String FORCE_FILE_ENCODING_UTF8 = "server.force.file.encoding.utf8";

	/**
	 * Shutdown Port
	 */
	String SHUTDOWN_PORT = "server.shutdown.port";

	/**
	 * Server jvm heap size (-Xmx setting)
	 */
	String HEAP_SIZE = "server.heap.size";

	/**
	 * Launch server on startup
	 */
	String LAUNCH_STARTUP = "server.launch.on.startup";


	/**
	 * Single instance (Webstart)
	 */
	String CLIENT_SINGLEINSTANCE = "client.singleinstance";
	String CLIENT_RICHCLIENT = "client.richclient";
	String CLIENT_WEBCLIENT = "client.webclient";
	String CLIENT_LAUNCHER = "client.launcher";
	String CLIENT_SERVERHOST = "client.serverhost";
	String CLIENT_JRE = "client.jre";

	String DATABASE_ADAPTER = "database.adapter";
	String DATABASE_DRIVERJAR = "database.driverjar";
	String DATABASE_SERVER = "database.server";
	String DATABASE_PORT = "database.port";
	String DATABASE_NAME = "database.name";
	String DATABASE_USERNAME = "database.username";
	String DATABASE_PASSWORD = "database.password";
	String DATABASE_SCHEMA = "database.schema";
	String DATABASE_TABLESPACE = "database.tablespace";
	String DATABASE_TABLESPACEINDEX = "database.tablespace.index";
	String DATABASE_SETUP = "database.setup";
	String DATABASE_MSSQL_ISOLATION = "database.mssql.isolation";
	String DATABASE_CONNECTION_INIT = "database.connection.init";

	String POSTGRES_PREFIX = "postgres.prefix";
	String POSTGRES_DATADIR = "postgres.datadir";
	String POSTGRES_SUPERUSER = "postgres.superuser";
	String POSTGRES_SUPERPWD = "postgres.superpw";
	String POSTGRES_TABLESPACEPATH = "postgres.tablespacepath";

	String SERVER_TOMCAT_DIR = "server.tomcat.dir";

	String DBOPTION_INSTALL = "install";
	String DBOPTION_EMBEDDED = "embedded";
	String DBOPTION_SETUP = "setup";
	String DBOPTION_USE = "use";

	String UNINSTALL_REMOVEDATAANDLOGS = "uninstall.removedataandlogs";

	// versions

	String POSTGRESQL_MAIN_VERSION = "9.5";
	String POSTGRESQL_FULL_VERSION = "9.5.3-1";

	String TOMCAT_FULL_VERSION = "9.0.8";
	String TOMCAT_VERSION = "apache-tomcat-" + TOMCAT_FULL_VERSION;

	/**
	 * Cluster Mode
	 */
	String CLUSTER_MODE = "server.cluster.mode";


	String DIR_NAME_DATA = "data";
	String DIR_NAME_CONF = "conf";
	String DIR_NAME_CLIENT = "client";
	String DIR_NAME_WEBAPP = "webapp";
	String DIR_NAME_TOMCAT = "tomcat";

	String DIR_NAME_DOCUMENTS = "documents";
	String DIR_NAME_DOCUMENTS_UPLOAD = "documents-upload";
	String DIR_NAME_INDEX = "index";
	String DIR_NAME_RESOURCE = "resource";
	String DIR_NAME_EXPIMP = "expimp";
	String DIR_NAME_CODEGENERATOR = "codegenerator";
	String DIR_NAME_COMPILED_REPORTS = "compiled-reports";
	String DIR_NAME_LOGS = "logs";

	/**
	 * AJP
	 */
	String AJP_ENABLED = "server.ajp.enabled";
	String AJP_PORT = "server.ajp.port";

}

