<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
	<title>Nuclos Webstart</title>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="start.css">
	<script src="start.js"></script>
</head>
<body alink="#0A5CA5" style="text-align:center;"
		onload="MM_preloadImages('pictures/BUTTON-START-aktiv.png', 'pictures/BUTTON-START-2.png')">
<div class="headerrund">
	<img src="pictures/Header-600x96sw.png" width="600" height="116"/>
	<div class="reiterleiste">
		<div class="homebutton">
			<img src="pictures/home.png"/>
		</div>
		<a href="http://wiki.nuclos.de">
			<div class="reiter">
				<strong>WIKI</strong>
			</div>
		</a>
		<a href="http://api.nuclos.de/">
			<div class="reiter">
				<strong>API</strong>
			</div>
		</a>
		<a href="http://www.nuclos.de/nuclos-forum">
			<div class="reiter">
				<strong>FORUM</strong>
			</div>
		</a>
		<a href="http://support.nuclos.de/">
			<div class="reiter">
				<strong>BUGS</strong>
			</div>
		</a>
		<a href="http://www.nuclos.de/download">
			<div class="reiter">
				<strong>DOWNLOAD</strong>
			</div>
		</a>
	</div>
	<br style="clear:left;"/>
	<div class="grauverlauf">
	<#if .data_model["client.richclient"]>
		<div class="client">
			<div>
				<img src="pictures/DesktopClient.png"/>
			</div>
			<a id="startButton" href="app/webstart.jnlp" onmouseout="MM_swapImgRestore()"
					onmouseover="MM_swapImage('Image2','','pictures/BUTTON-START-aktiv.png',1)">
				<img src="pictures/BUTTON-START-2.png" name="Image2" width="160" height="160" border="0"
						id="Image2"/>
			</a>
			<#if .data_model["client.launcher"]>
				<a id="launcherLink" href="#">Launcher</a>
			</#if>
		</div>
	</#if>
	<#if .data_model["client.webclient"]>
		<div class="client">
			<div>
				<img src="pictures/WebClient.png"/>
			</div>
			<a href="../webclient/" onmouseout="MM_swapImgRestore()"
					onmouseover="MM_swapImage('Image3','','pictures/BUTTON-START-aktiv.png',1)">
				<img src="pictures/BUTTON-START-2.png" name="Webstart" width="160" height="160" border="0"
						id="Image3"/>
			</a>
		</div>
	</#if>
	</div>
</div>


<script language="JavaScript">
	(function() {
		var useLauncherLinkOnly = false;
		var link = document.getElementById('launcherLink');
		if (link) {
			link.href = getNuclosLauncherUrl();

			var startlink = document.getElementById('startButton');
			if (useLauncherLinkOnly && startlink) {
				startlink.href = getNuclosLauncherUrl();
				link.parentNode.removeChild(link);
			}
		}
	})();
</script>

</body>
</html>