package org.nuclos.server.eventsupport.valueobject;

import java.util.Date;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class ProcessVO extends NuclosValueObject<UID> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9092001637148016442L;
	private UID nuclet;
	private UID module;
	private Date	dValidUntil;
	private Date	dValidFrom;
	private String	sDescription;
	private String  sName;
	
	public ProcessVO(NuclosValueObject<UID> nvo, UID nuclet, UID module, Date pValidUntil, Date pValidFrom, String pDescription, String pName) {
		super(nvo);
		
		this.nuclet = nuclet;
		this.module = module;
		
		this.dValidFrom = pValidFrom;
		this.dValidUntil = pValidUntil;
		this.sDescription = pDescription;
		this.sName = pName;	
	}

	
	public String getName() {
		return sName;
	}

	public UID getNuclet() {
		return nuclet;
	}

	public UID getModule() {
		return module;
	}

	public Date getValidUntil() {
		return dValidUntil;
	}

	public Date getValidFrom() {
		return dValidFrom;
	}

	public String getDescription() {
		return sDescription;
	}
}
