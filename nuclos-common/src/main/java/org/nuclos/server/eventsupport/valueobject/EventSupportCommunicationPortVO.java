//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.eventsupport.valueobject;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class EventSupportCommunicationPortVO extends EventSupportVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7704461345060505258L;
	
	private UID portUID;
	
	
	public EventSupportCommunicationPortVO(NuclosValueObject<UID> nvo, Integer pOrder, UID pPortUID, String pEventSupportClass, String pEventSupportType) {
		super(nvo, pOrder, pEventSupportClass, pEventSupportType);
	
		this.portUID = pPortUID;
	}
	
	public EventSupportCommunicationPortVO(EventSupportVO eseVO, UID pPortUID) {
		super(eseVO, eseVO.getOrder(), eseVO.getEventSupportClass(), eseVO.getEventSupportClassType());
		this.portUID = pPortUID;
	}
	
	public EventSupportCommunicationPortVO(Integer pOrder, UID pPortUID, String pEventSupportClass, String pEventSupportType) {
		super(pOrder, pEventSupportClass, pEventSupportType);
	
		this.portUID = pPortUID;
	}
	
	public UID getPort() {
		return portUID;
	}

	public void setPort(UID pPortUID) {
		this.portUID = pPortUID;
	}

	/**
	 * validity checker
	 */
	@Override
	public void validate() throws CommonValidationException {
		
		super.validate();
		
		if (getPort() == null) {
			throw new CommonValidationException("ruleengine.error.validation.rule.name");
		}	
	}
	
	public EventSupportCommunicationPortVO clone() {
		return new EventSupportCommunicationPortVO(super.clone(), this.getPort());
	}
	
}
