package org.nuclos.server.eventsupport.valueobject;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class EventSupportJobVO extends EventSupportVO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8387728074285607456L;
	private String  sDescription;
	private UID 	jobControllerUID;
	
	public EventSupportJobVO(NuclosValueObject<UID> nvo, String pDescription, String pEventSupportClass, String pEventSupportType, Integer pOrder, UID pJobControllerUID) {
		super(nvo, pOrder, pEventSupportClass,pEventSupportType);
		this.sDescription = pDescription;
		this.jobControllerUID = pJobControllerUID;
	}
	
	public EventSupportJobVO(EventSupportVO eseVO, String pDescription, UID pJobControllerUID) {
		super(eseVO, eseVO.getOrder(), eseVO.getEventSupportClass(), eseVO.getEventSupportClassType());
		this.sDescription = pDescription;
		this.jobControllerUID = pJobControllerUID;
	}
	
	public EventSupportJobVO(String pDescription, String pEventSupportClass, String pEventSupportType, Integer pOrder, UID pJobControllerUID) {
		super(pOrder, pEventSupportClass, pEventSupportType);
		this.sDescription = pDescription;
		this.jobControllerUID = pJobControllerUID;
	}

	public String getDescription() {
		return sDescription;
	}

	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}

	public UID getJobControllerUID() {
		return jobControllerUID;
	}

	public void setJobControllerUID(UID pJobControllerUID) {
		this.jobControllerUID = pJobControllerUID;
	}
	

	/**
	 * validity checker
	 */
	@Override
	public void validate() throws CommonValidationException {
		
		super.validate();
		
		if (getJobControllerUID() == null) {
			throw new CommonValidationException("ruleengine.error.validation.rule.name");
		}
	}	

	public EventSupportJobVO clone() {
		return new EventSupportJobVO(super.clone(), this.getDescription(), this.getJobControllerUID());
	}
	
}
