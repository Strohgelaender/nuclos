package org.nuclos.server.eventsupport.ejb3;

import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;
import org.nuclos.server.eventsupport.valueobject.CommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportCommunicationPortVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportEventVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportJobVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportTypeVO;
import org.nuclos.server.eventsupport.valueobject.ProcessVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.job.valueobject.JobVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.valueobject.StateModelVO;

/**
 * @author reichama
 *
 */
// @Remote
public interface EventSupportFacadeRemote {

	/**
	 * This Method returns all available Classes that can be used a EventSupport-Files
	 * due to the given interface(s)
	 */
	@RolesAllowed("Login")
	List<EventSupportSourceVO> getAllEventSupportsRemote()
			throws NuclosFatalException, CommonPermissionException;
	
	@RolesAllowed("Login")
	List<EventSupportTypeVO> getAllEventSupportTypes() 
			throws CommonPermissionException;

	@RolesAllowed("Login")
	List<EventSupportEventVO> getEventSupportEntities() 
			throws CommonPermissionException;
	
	@RolesAllowed("Login")
	EventSupportJobVO createEventSupportJob(EventSupportJobVO esjVOToInsert) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException;

	@RolesAllowed("Login")
	EventSupportJobVO modifyEventSupportJob(EventSupportJobVO esjVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	void deleteEventSupportJob(EventSupportJobVO esjVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	EventSupportEventVO createEventSupportEvent(EventSupportEventVO eseVOToInsert) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException;

	@RolesAllowed("Login")
	EventSupportEventVO modifyEventSupportEvent(EventSupportEventVO eseVOToUpdate, UID oldState, UID oldProcess) 
			throws CommonPermissionException, CommonValidationException, CommonFinderException, NuclosBusinessRuleException, 
			CommonCreateException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	void deleteEventSupportEvent(EventSupportEventVO eseVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	EventSupportTransitionVO createEventSupportTransition(EventSupportTransitionVO eseVOToInsert) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException;

	@RolesAllowed("Login")
	EventSupportTransitionVO modifyEventSupportTransition(EventSupportTransitionVO eseVOToUpdate, UID oldTransaction) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	void deleteEventSupportTransition(EventSupportTransitionVO eseVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	List<ProcessVO> getProcessesByModule(UID moduleId);
	
	@RolesAllowed("Login")
	List<EventSupportTransitionVO> getEventSupportTransitions() 
			throws CommonFinderException, CommonPermissionException  ;

	@RolesAllowed("Login")
	List<EventSupportGenerationVO> getEventSupportGenerations() 
			throws CommonPermissionException;

	@RolesAllowed("Login")
	List<EventSupportCommunicationPortVO> getEventSupportCommunicationPorts() 
			throws CommonPermissionException;
	
	@RolesAllowed("Login")
	List<EventSupportJobVO> getEventSupportJobs() 
			throws CommonPermissionException;
	
	@RolesAllowed("Login")
	EventSupportGenerationVO createEventSupportGeneration(EventSupportGenerationVO eseVOToInsert) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException;
	
	@RolesAllowed("Login")
	EventSupportCommunicationPortVO createEventSupportCommunicationPort(EventSupportCommunicationPortVO eseVOToInsert) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException;

	@RolesAllowed("Login")
	EventSupportGenerationVO modifyEventSupportGeneration(EventSupportGenerationVO eseVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	EventSupportCommunicationPortVO modifyEventSupportCommunicationPort(EventSupportCommunicationPortVO eseVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonCreateException, 
			CommonFinderException, CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	void deleteEventSupportGeneration(EventSupportGenerationVO eseVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	void deleteEventSupportCommunicationPort(EventSupportCommunicationPortVO eseVOToUpdate) 
			throws CommonPermissionException, CommonValidationException, NuclosBusinessRuleException, CommonFinderException, 
			CommonRemoveException, CommonStaleVersionException, NuclosCompileException;
	
	@RolesAllowed("Login")
	List<EventSupportEventVO> getEventSupportEntitiesByClassname(String classname);
	
	@RolesAllowed("Login")
	List<StateModelVO> getStateModelsByEventSupportClassname(String classname) 
			throws CommonFinderException, CommonPermissionException, NuclosBusinessException;
	
	@RolesAllowed("Login")
	List<JobVO> getJobsByClassname(String classname) 
			throws CommonFinderException, CommonPermissionException;
	
	@RolesAllowed("Login")
	List<GeneratorActionVO> getGenerationsByClassname(String classname) 
			throws CommonFinderException, CommonPermissionException;
	
	@RolesAllowed("Login")
	List<CommunicationPortVO> getCommunicationPortByClassname(String classname)
			throws CommonFinderException, CommonPermissionException;

	@RolesAllowed("Login")
	void forceEventSupportCompilation() 
			throws NuclosCompileException;
	
	@RolesAllowed("Login")
	List<EventSupportSourceVO> findEventSupportsByUsageAndEntity(String sEventclass, UsageCriteria usagecriteria);

	/**
	 * Creates source files and JARs of BOs and other components.
	 */
	@RolesAllowed("Login")
	void createBusinessObjects(boolean reloadClassPath)
			throws NuclosCompileException, InterruptedException;
	
	@RolesAllowed("Login")
	void deleteEventSupport(EventSupportSourceVO eseVOToDelete) 
			throws CommonPermissionException, CommonValidationException, NuclosCompileException;

	@RolesAllowed("Login")
	Map<String, ErrorMessage> getCompileExceptionMessages();

	@RolesAllowed("Login")
	void invalidateCaches(EntityMeta<UID> entity);
	
	@RolesAllowed("Login")
	void invalidateCaches();
	
	@RolesAllowed("Login")
	boolean getUsesEventSupportForEntity(UID entityUID, String eventType);
	
	@RolesAllowed("Login")
	List<EventSupportJobVO> getEventSupportsByJobUid(UID genId) throws CommonPermissionException;

	@RolesAllowed("Login")
	String getEntityProxyInterfaceQualifiedName(UID entityUID);

	@RolesAllowed("Login")
	String getEntityProxyInterfaceQualifiedName(String entity, UID nucletUID);
	
	@RolesAllowed("Login")
	boolean isSourceCodeScannerScheduled();

	@RolesAllowed("Login")
	void setSourceCodeScannerScheduled(boolean schedule) throws CommonPermissionException;

	@RolesAllowed("Login")
	void readSourcesAndCompile() throws CommonPermissionException;
}
