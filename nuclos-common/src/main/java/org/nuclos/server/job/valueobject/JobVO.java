//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.job.valueobject;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.job.JobResult;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value object representing a job.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 00.01.000
 */
public class JobVO extends NuclosValueObject<UID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5451544463043598299L;
	private String sName;
	private String sType;
	private String sDescription;
	private Date dStartdate;
	private String sStarttime;
	private Integer iInterval;
	private String sUnit;
	private Boolean bUseCronExpression;
	private String sCronExpression;
	
	/**
	 * @deprecated
	 */
	private UID user;
	
	/**
	 * @deprecated
	 */
	private String sLevel;
	
	private Integer iDeleteInDays;
	private String sLastState;
	private Boolean bRunning;
	private String dNextFireTime;
	private String dLastFireTime;
	private String sResult;
	private String sResultDetails;
	private Integer iSessionId;
	private UID nuclet;

	// map for dependant child subform data
	private IDependentDataMap mpDependants = new DependentDataMap();

	public JobVO(MasterDataVO<UID> mdVO, IDependentDataMap mpDependants) {
		super(mdVO.getId(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion());
		this.sName = mdVO.getFieldValue(E.JOBCONTROLLER.name);
		this.sType = mdVO.getFieldValue(E.JOBCONTROLLER.type);
		this.sDescription = mdVO.getFieldValue(E.JOBCONTROLLER.description);
		this.dStartdate = mdVO.getFieldValue(E.JOBCONTROLLER.startdate);
		this.sStarttime = mdVO.getFieldValue(E.JOBCONTROLLER.starttime);
		this.iInterval = mdVO.getFieldValue(E.JOBCONTROLLER.interval);
		this.sUnit = mdVO.getFieldValue(E.JOBCONTROLLER.unit);
		this.user = mdVO.getFieldUid(E.JOBCONTROLLER.user);
		this.bUseCronExpression = mdVO.getFieldValue(E.JOBCONTROLLER.usecronexpression);
		this.sCronExpression = mdVO.getFieldValue(E.JOBCONTROLLER.cronexpression);
		this.sLevel = mdVO.getFieldValue(E.JOBCONTROLLER.level);
		this.iDeleteInDays = mdVO.getFieldValue(E.JOBCONTROLLER.days);
		this.sLastState = mdVO.getFieldValue(E.JOBCONTROLLER.laststate);
		this.bRunning = mdVO.getFieldValue(E.JOBCONTROLLER.running);
		this.dNextFireTime = mdVO.getFieldValue(E.JOBCONTROLLER.nextfiretime);
		this.dLastFireTime = mdVO.getFieldValue(E.JOBCONTROLLER.lastfiretime);
		this.sResult = mdVO.getFieldValue(E.JOBCONTROLLER.result);
		this.sResultDetails = mdVO.getFieldValue(E.JOBCONTROLLER.resultdetails);
		this.nuclet = mdVO.getFieldUid(E.JOBCONTROLLER.nuclet);
		this.mpDependants = mpDependants;
	}

	/**
	 * constructor to be called by server only
	 * @param sName name of underlying database record
	 * @param sType type of underlying database record
	 */
	public JobVO(String sName, String sType, String sDescription, Date dStartdate, String sStarttime, Integer iInterval,
			String sUnit, UID user, String sLevel, Integer iDays) {
		this.sName = sName;
		this.sType = sType;
		this.sDescription = sDescription;
		this.dStartdate = dStartdate;
		this.sStarttime = sStarttime;
		this.iInterval = iInterval;
		this.sUnit = sUnit;
		this.user = user;
		this.sLevel = sLevel;
		this.iDeleteInDays = iDays;
		this.sLastState = null;
		this.bRunning = false;
		this.dLastFireTime = null;
		this.dNextFireTime = null;
		this.sResult = null;
		this.sResultDetails = null;
	}


	public JobVO(String sName, String sType, String sDescription, Date dStartdate, String sStarttime, Integer iInterval,
			String sUnit) {
		this.sName = sName;
		this.sType = sType;
		this.sDescription = sDescription;
		this.dStartdate = dStartdate;
		this.sStarttime = sStarttime;
		this.iInterval = iInterval;
		this.sUnit = sUnit;
		this.user = null;
		this.sLevel = null;
		this.iDeleteInDays = null;
		this.sLastState = null;
		this.bRunning = false;
		this.dLastFireTime = null;
		this.dNextFireTime = null;
		this.sResult = null;
		this.sResultDetails = null;
	}

	/**
	 * get job name of underlying database record
	 * @return job name of underlying database record
	 */
	public String getName() {
		return this.sName;
	}

	/**
	 * set job name of underlying database record
	 * @param sName job name of underlying database record
	 */
	public void setName(String sName) {
		this.sName = sName;
	}

	/**
	 * get job type of underlying database record
	 * @return job type of underlying database record
	 */
	public String getType() {
		return this.sType;
	}

	/**
	 * set job type.
	 * @param sType
	 */
	public void setType(String sType) {
		this.sType = sType;
	}

	/**
	 * get job description of underlying database record
	 * @return job description of underlying database record
	 */
	public String getDescription() {
		return this.sDescription;
	}

	/**
	 * set job description of underlying database record
	 * @param sDescription job description of underlying database record
	 */
	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}

	/**
	 * get job start date of underlying database record
	 * @return job start date of underlying database record
	 */
	public Date getStartdate() {
		return this.dStartdate;
	}

	/**
	 * set job start date of underlying database record
	 * @param dStartdate job start date of underlying database record
	 */
	public void setStartdate(Date dStartdate) {
		this.dStartdate = dStartdate;
	}

	/**
	 * get job start time of underlying database record
	 * @return job start time of underlying database record
	 */
	public String getStarttime() {
		return this.sStarttime;
	}

	/**
	 * set job start time of underlying database record
	 * @param sStarttime job start time of underlying database record
	 */
	public void setStarttime(String sStarttime) {
		this.sStarttime = sStarttime;
	}

	/**
	 * get job interval start time of underlying database record
	 * @return job interval start time of underlying database record
	 */
	public Integer getInterval() {
		return this.iInterval;
	}

	/**
	 * set job interval start time of underlying database record
	 * @param iInterval job interval start time of underlying database record
	 */
	public void setInterval(Integer iInterval) {
		this.iInterval = iInterval;
	}

	/**
	 * get job unit interval start time of underlying database record
	 * @return job unit interval start time of underlying database record
	 */
	public String getUnit() {
		return this.sUnit;
	}

	/**
	 * set job unit interval start time of underlying database record
	 * @param sUnit job unit interval start time of underlying database record
	 */
	public void setUnit(String sUnit) {
		this.sUnit = sUnit;
	}

	/**
	 * @deprecated
	 */
	public UID getUser() {
		return this.user;
	}

	/**
	 * @deprecated
	 */
	public void setUser(UID user) {
		this.user = user;
	}

	/**
	 * @deprecated
	 */
	public String getLevel() {
		return this.sLevel;
	}

	/**
	 * @deprecated
	 */
	public void setLevel(String sLevel) {
		this.sLevel = sLevel;
	}

	public Integer getDeleteInDays() {
		return this.iDeleteInDays;
	}

	public void setDeleteInDays(Integer iDeleteInDays) {
		this.iDeleteInDays = iDeleteInDays;
	}

	public String getLastState() {
		return this.sLastState;
	}

	public void setLastState(String sLastState) {
		this.sLastState = sLastState;
	}

	public Boolean isRunning() {
		return this.bRunning;
	}

	public void setRunning(Boolean bRunning) {
		this.bRunning = bRunning;
	}

	public String getLastFireTime() {
		return this.dLastFireTime;
	}

	public void setLastFireTime(String dLastFireTime) {
		this.dLastFireTime = dLastFireTime;
	}

	public String getNextFireTime() {
		return this.dNextFireTime;
	}

	public void setNextFireTime(String dNextFireTime) {
		this.dNextFireTime = dNextFireTime;
	}

	private String getResult() {
		return this.sResult;
	}

	private void setResult(String sResult) {
		this.sResult = sResult;
	}
	
	public JobResult getResultAsEnum() {
		if (StringUtils.isBlank(sResult)) {
			return null;
		}
		return JobResult.valueOf(sResult.toUpperCase());
	}
	
	public void setResultAsEnum(JobResult jobResult) {
		if (jobResult == null) {
			this.sResult = null;
		} else {
			this.sResult = jobResult.name();
		}
	}

	public String getResultDetails() {
		return this.sResultDetails;
	}


	public void setResultDetails(String sResultDetails) {
		this.sResultDetails = sResultDetails;
	}

	public Integer getSessionId() {
		return this.iSessionId;
	}

	public void setSessionId(Integer iSessionId) {
		this.iSessionId = iSessionId;
	}

	public Boolean getUseCronExpression() {
    	return bUseCronExpression;
    }

	public void setUseCronExpression(Boolean bUseCronExpression) {
    	this.bUseCronExpression = bUseCronExpression;
    }

	public String getCronExpression() {
    	return sCronExpression;
    }

	public void setCronExpression(String sCronExpression) {
    	this.sCronExpression = sCronExpression;
    }

	public UID getNuclet() {
		return nuclet;
	}

	public void setNuclet(UID nuclet) {
		this.nuclet = nuclet;
	}

	@Override
	public int hashCode() {
		return (getName() != null ? getName().hashCode() : 0);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof JobVO) {
			final JobVO that = (JobVO) o;
			// resources are equal if there names are equal
			return this.getName().equals(that.getName());
		}
		return false;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public MasterDataVO<UID> toMasterDataVO() {
		MasterDataVO<UID> mdvo = new MasterDataVO(E.JOBCONTROLLER.getUID(), getId(), 
				getCreatedAt(), getCreatedBy(), getChangedAt(), getChangedBy(), getVersion(), null, null, null, false);
		
		mdvo.setFieldValue(E.JOBCONTROLLER.name, getName());
		mdvo.setFieldValue(E.JOBCONTROLLER.type, getType());
		mdvo.setFieldValue(E.JOBCONTROLLER.description, getDescription());
		mdvo.setFieldValue(E.JOBCONTROLLER.startdate, getStartdate());
		mdvo.setFieldValue(E.JOBCONTROLLER.starttime, getStarttime());
		mdvo.setFieldValue(E.JOBCONTROLLER.interval, getInterval());
		mdvo.setFieldValue(E.JOBCONTROLLER.unit, getUnit());
		mdvo.setFieldValue(E.JOBCONTROLLER.usecronexpression, getUseCronExpression());
		mdvo.setFieldValue(E.JOBCONTROLLER.cronexpression, getCronExpression());
		mdvo.setFieldUid(E.JOBCONTROLLER.user, getUser());
		mdvo.setFieldValue(E.JOBCONTROLLER.level, getLevel());
		mdvo.setFieldValue(E.JOBCONTROLLER.days, getDeleteInDays());
		mdvo.setFieldValue(E.JOBCONTROLLER.laststate, getLastState());
		mdvo.setFieldValue(E.JOBCONTROLLER.running, isRunning());
		mdvo.setFieldValue(E.JOBCONTROLLER.nextfiretime, getNextFireTime());
		mdvo.setFieldValue(E.JOBCONTROLLER.lastfiretime, getLastFireTime());
		mdvo.setFieldValue(E.JOBCONTROLLER.result, getResult());
		mdvo.setFieldValue(E.JOBCONTROLLER.resultdetails, getResultDetails());
		mdvo.setFieldUid(E.JOBCONTROLLER.nuclet, getNuclet());
		
		return mdvo;
	}

	public IDependentDataMap getDependants() {
		return this.mpDependants;
	}

	public void setDependants(IDependentDataMap mpDependants) {
		this.mpDependants = mpDependants;
	}

}
