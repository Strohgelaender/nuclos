package org.nuclos.server.navigation.treenode;

import java.io.Serializable;
import java.util.Date;

import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.RelationDirection;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.SystemRelationType;

public class GenericObjectTreeNodeParameters implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6439053405334768661L;
	private final Long id;
	private final UsageCriteria			usagecriteria;
	private final String				sIdentifier;
	private final Long				relationId;
	private final SystemRelationType	relationtype;
	private final RelationDirection		direction;
	private final String						sUserName;
	private final UID 					uidNode;
	private final Long idRoot;
	
	private final String label;
	private final Date changedAt;
	private final String description;
	
	public GenericObjectTreeNodeParameters(Long id,
			UsageCriteria usagecriteria, String sIdentifier, Long relationId,
			SystemRelationType relationtype, RelationDirection direction,
			String sUserName, UID uidNode, Long idRoot, String label,
			Date changedAt, String description) {
		this.id = id;
		this.usagecriteria = usagecriteria;
		this.sIdentifier = sIdentifier;
		this.relationId = relationId;
		this.relationtype = relationtype;
		this.direction = direction;
		this.sUserName = sUserName;
		this.uidNode = uidNode;
		this.idRoot = idRoot;
		this.label = label;
		this.changedAt = changedAt;
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public UsageCriteria getUsagecriteria() {
		return usagecriteria;
	}

	public String getsIdentifier() {
		return sIdentifier;
	}

	public Long getRelationId() {
		return relationId;
	}

	public SystemRelationType getRelationtype() {
		return relationtype;
	}

	public RelationDirection getDirection() {
		return direction;
	}

	public String getsUserName() {
		return sUserName;
	}

	public UID getUidNode() {
		return uidNode;
	}

	public Long getIdRoot() {
		return idRoot;
	}

	public String getLabel() {
		return label;
	}

	public Date getChangedAt() {
		return changedAt;
	}

	public String getDescription() {
		return description;
	}
	
	
	//Sector B: Additional Parameters for GenericObjectTreeNodeWithCondition
	private CollectableSearchCondition cond;	
	private String nodeType;
	
	public CollectableSearchCondition getCondition() {
		return cond;
	}

	public void setCondition(CollectableSearchCondition cond) {
		this.cond = cond;
	}
	
	public String getNodeType() {
		return nodeType;
	}
	
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GenericObjectTreeNodeParameters) {
			GenericObjectTreeNodeParameters that = (GenericObjectTreeNodeParameters) obj;
			return LangUtils.equal(id, that.id) && LangUtils.equal(usagecriteria, that.usagecriteria)
					&& LangUtils.equal(relationId, that.relationId) && LangUtils.equal(relationtype, that.relationtype)
					&& LangUtils.equal(direction, that.direction) && LangUtils.equal(sUserName, that.sUserName)
					&& LangUtils.equal(uidNode, that.uidNode) && LangUtils.equal(idRoot, that.idRoot)
					&& LangUtils.equal(label, that.label)
					&& LangUtils.equal(cond, that.cond) && LangUtils.equal(nodeType, that.nodeType);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(id, usagecriteria, relationId, relationtype, direction, sUserName, uidNode, idRoot, label, cond, nodeType);
	}
	
}
