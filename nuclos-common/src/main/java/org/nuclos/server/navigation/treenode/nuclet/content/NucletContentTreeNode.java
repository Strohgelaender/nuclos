//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode.nuclet.content;

import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.ServiceLocator;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.navigation.ejb3.TreeNodeFacadeRemote;
import org.nuclos.server.navigation.treenode.TreeNode;

/**
 * Tree node implementation representing a nuclet content.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 *
 * @author	<a href="mailto:maik.stueker@nuclos.de">maik.stueker</a>
 * @version 00.01.000
 */
public class NucletContentTreeNode implements TreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2792946310575747688L;
	
	private final UID nuclet;
	private final EntityMeta<UID> content;

	public NucletContentTreeNode(UID nuclet, EntityMeta<UID> content) {
		this.nuclet = nuclet;
		this.content = content;
	}

	public UID getNuclet() {
		return nuclet;
	}

	public EntityMeta<UID> getEntity() {
		return content;
	}

	@Override
	public Object getId() {
		return content;
	}

	@Override
	public String getIdentifier() {
		return content.getEntityName();
	}

	@Override
	public List<? extends TreeNode> getSubNodes() {
		return getTreeNodeFacade().getSubNodes(this);
	}

	@Override
	public Boolean hasSubNodes() {
		return false;
	}

	@Override
	public void removeSubNodes() {
	}

	@Override
	public void refresh() {
	}

	@Override
	public boolean implementsNewRefreshMethod() {
		return false;
	}

	@Override
	public boolean needsParent() {
		return true;
	}

	@Override
	public String getLabel() {
		return SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(getMetaData());
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		return this;
	}

	private EntityMeta getMetaData() {
		return (EntityMeta) SpringApplicationContextHolder.getBean(IMetaProvider.class).getEntity(content.getUID());
	}

	private TreeNodeFacadeRemote getTreeNodeFacade() throws NuclosFatalException {
		return ServiceLocator.getInstance().getFacade(TreeNodeFacadeRemote.class);
	}

	@Override
	public UID getEntityUID() {
		return content.getUID();
	}

	@Override
	public UID getNodeId() {
		throw new NotImplementedException("getNodeId not implemented for " + this);
	}
	
	@Override
	public Long getRootId() {
		throw new NotImplementedException("getRootId not implemented for " + this);
	}
}	// class NucletContentTreeNode
