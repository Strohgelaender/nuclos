//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.rmi.RemoteException;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common.Utils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Tree node implementation representing a subform.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: Create a constructor that takes EntityTreeViewVO instead of MasterDataVO.
 * This way, we could get rid of 
 * org.nuclos.server.genericobject.Modules.getSubnodesMD(String).
 * </p>
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 */
public class SubFormTreeNode<Id> extends DynamicTreeNode<Id>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7512526859295066172L;

	/**
	 * @deprecated See class javadoc for details. (Thomas Pasch)
	 */
	public SubFormTreeNode(Id id, TreeNode node, MasterDataVO<Id> mdVO) {
	    super(id, node, mdVO);
    }
	
	@Override
	public UID getEntityUID() {
		if (getTreeNodeObject() instanceof GenericObjectTreeNode)
			return ((GenericObjectTreeNode)getTreeNodeObject()).getEntityUID();
		else if (getTreeNodeObject() instanceof MasterDataTreeNode)
			return ((MasterDataTreeNode)getTreeNodeObject()).getEntityUID();
		throw new IllegalStateException("treenodeobject not instance of MasterData- or GenericObjectTreeNode.");
	}

	public TreeNode getTreeNodeObject(){
		return super.getTreeNode();
	}

	@Override
    public MasterDataVO<Id> getMasterDataVO() {
	    return super.getMasterDataVO();
    }

	@Override
    protected List<? extends TreeNode> getSubNodesImpl() throws RemoteException {
	    return Utils.getTreeNodeFacade().getSubNodesForSubFormTreeNode(getTreeNode(), getMasterDataVO());
    }

	@Override
    public TreeNode refreshed() {
	    return Utils.getTreeNodeFacade().getSubFormTreeNode(super.getTreeNode(), getMasterDataVO());
    }
	
}
