//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.Utils;
import org.nuclos.common2.IdUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoteException;
import org.nuclos.server.masterdata.ejb3.EntityFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * TreeNode for MasterDataRecords
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version	01.00.00
 */
public class SubFormEntryTreeNode<Id> extends DefaultMasterDataTreeNode<Id> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8669666417590422265L;
	private transient EntityFacadeRemote entityFacadeRemote;

	public SubFormEntryTreeNode(MasterDataVO<Id> mdvo, final UID uidNode, final Long idRoot, final String label, final String description) {
		super(new DefaultMasterDataTreeNodeParameters<Id>(mdvo.getEntityObject().getDalEntity(), mdvo.getPrimaryKey(), 
				uidNode, idRoot, label, description));
	}
	
	private EntityFacadeRemote getEntityFacadeRemote() {
		if (this.entityFacadeRemote == null) {
			this.entityFacadeRemote = (EntityFacadeRemote) SpringApplicationContextHolder.getBean("entityService");
		}
		return this.entityFacadeRemote;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
    try {
    	  return Utils.getTreeNodeFacade().getSubFormEntryTreeNode(this.getId(), this.getEntityUID(), getNodeId(), IdUtils.toLongId(getRootId()), false);
		}
		catch (RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
		catch (CommonPermissionException ex) {
			throw new CommonRemoteException(ex);
		}
	}
}
