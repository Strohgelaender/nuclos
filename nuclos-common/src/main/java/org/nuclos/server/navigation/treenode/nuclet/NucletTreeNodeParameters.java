package org.nuclos.server.navigation.treenode.nuclet;

import java.io.Serializable;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;

public class NucletTreeNodeParameters implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2602668495953041511L;
	private final UID nuclet;
	private final String label;
	private final String description;
	private final boolean showDependencies;

	public NucletTreeNodeParameters(UID nuclet, String label,
			String description, boolean showDependencies) {
		this.nuclet = nuclet;
		this.label = label;
		this.description = description;
		this.showDependencies = showDependencies;
	}
	
	public NucletTreeNodeParameters(EntityObjectVO<UID> eovo, boolean showDependencies) {
		this(eovo.getPrimaryKey(), 
			eovo.getFieldValue(E.NUCLET.name), 
			eovo.getFieldValue(E.NUCLET.description),
			showDependencies);
	}

	public UID getNuclet() {
		return nuclet;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}

	public boolean isShowDependencies() {
		return showDependencies;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof NucletTreeNodeParameters) {
			NucletTreeNodeParameters that = (NucletTreeNodeParameters) obj;
			return LangUtils.equal(nuclet, that.nuclet) && LangUtils.equal(label, that.label) 
					&& LangUtils.equal(description, that.description) && LangUtils.equal(showDependencies, that.showDependencies);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(nuclet, label, description, showDependencies);
	}
	
}
