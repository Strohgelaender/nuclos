package org.nuclos.server.navigation.treenode;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;

/**
 *  NON LAZY TREE NODE 
 *
 */
public class SimpleTreeNode extends AbstractTreeNode{

    private static final long serialVersionUID = 1L;
    
    private ArrayList<TreeNode> list = new ArrayList<TreeNode>();
    private Object parentId;
    private UID entityUID;
    
    public SimpleTreeNode(Object id, String sLabel, String sDescription, Object parentId, UID entityUID) {
        super(id, sLabel, sDescription);
        this.parentId = parentId;
        this.entityUID = entityUID;
    }
    
    @Override
    public TreeNode refreshed() throws CommonFinderException { 
        return this; 
    }
    
    @Override
    protected List<? extends TreeNode> getSubNodesImpl() throws RemoteException {
        return list;
    }
    
    public Object getParentId(){
        return parentId;
    }
    
    public void setParentId(Object parentId){
        this.parentId = parentId;
    }
    
    @Override
    public UID getEntityUID() {
        return entityUID;
    }
    
    public void sortSubNodesByLabel(){
        Collections.sort(list, new Comparator<TreeNode>() {
            @Override
            public int compare(TreeNode o1, TreeNode o2) {
                if(o1.getLabel()!=null && o2.getLabel()!=null){
                    return  o1.getLabel().compareTo(o2.getLabel());
                } else {
                    return 0;
                }
            }
        });
    }
    
}
