package org.nuclos.server.navigation.treenode.nuclet.content;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.SpringLocaleDelegate;

public class ReportNucletContentTreeNode extends NucletContentTreeNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2868651000987418829L;

	public ReportNucletContentTreeNode(UID nuclet) {
		super(nuclet, E.REPORT);
	}

	@Override
	public String getLabel() {
		return SpringLocaleDelegate.getInstance().getMessage("report.and.form", "Report & Formulare");
	}

	
}
