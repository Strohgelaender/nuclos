//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.rmi.RemoteException;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common.Utils;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * Tree node representing a masterdata search result.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Lars.Rueckemann@novabit.de">Lars Rueckemann</a>
 * @version	01.00.00
 */
public class MasterDataSearchResultTreeNode<PK> extends AbstractSearchResultTreeNode<PK> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5969008928383621910L;
	private final UID sEntity;
	//This is obsolete stuff, but serialized in old workspaces
	private UID uidNode;
	private Long idRoot;
	
	public MasterDataSearchResultTreeNode(MasterDataSearchResultNodeParameters mdsrParam) {
		super(mdsrParam.getCond(), mdsrParam.getsFilterName());
		this.sEntity = mdsrParam.getsEntity();
	}

	@Override
	protected List<? extends TreeNode> getSubNodesImpl() throws RemoteException, CommonPermissionException {
		return Utils.getTreeNodeFacade().getSubNodes(this);
	}
	
	@Override
	public Object getRootId() {
		return idRoot;
	}
	
	@Override
	public UID getEntityUID() {
		return sEntity;
	}
	
	public MasterDataSearchResultNodeParameters getMasterDataSearchResultNodeParameters() {
		return new MasterDataSearchResultNodeParameters(sEntity, getSearchCondition(), getFilterName());
	}
	
	@Override
	public UID getNodeId() {
		// TODO Auto-generated method stub
		return super.getNodeId();
	}

}
