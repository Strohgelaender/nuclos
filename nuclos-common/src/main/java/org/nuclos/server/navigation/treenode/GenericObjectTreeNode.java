// Copyright (C) 2010 Novabit Informationssysteme GmbH
//
// This file is part of Nuclos.
//
// Nuclos is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nuclos is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Nuclos. If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.rmi.RemoteException;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.ServiceLocator;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.SecurityFacadeRemote;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeRemote;
import org.nuclos.server.navigation.ejb3.TreeNodeFacadeRemote;

/**
 * Abstract tree node implementation representing a generic object. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:christoph.radig@novabit.de">christoph.radig</a>
 * @version 01.00.00
 */
public class GenericObjectTreeNode extends AbstractTreeNodeConfigured<Long> implements ITreeNodeWithGenericObjectTreeNodeParameters {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4172286224228417043L;

	private static final Logger LOG = Logger.getLogger(GenericObjectTreeNode.class);

	private SecurityFacadeRemote		facade;
	private GenericObjectFacadeRemote	gofacade;

	public static enum SystemRelationType implements KeyEnum<String> {

		/** relation type: "is predecessor of" */
		PREDECESSOR_OF("PredecessorOf"),
		/** relation type: "is part of" */
		PART_OF("PartOf");

		private String	name;

		private SystemRelationType(String name) {
			this.name = name;
		}

		@Override
		public String getValue() {
			return name;
		}

		public static SystemRelationType findSystemRelationType(
			String relationType) {
			return KeyEnum.Utils.findEnum(SystemRelationType.class,
				relationType);
		}
	} // enum SystemRelationType

	public static enum RelationDirection {

		/**
		 * relation direction: forward, eg. (lo1, lo2, part-of, forward) means:
		 * "lo1 is-part-of lo2"
		 */
		FORWARD,
		/**
		 * relation direction: reverse, eg. (lo1, lo2, part-of, reverse) means:
		 * "lo2 is-part-of lo1"
		 */
		REVERSE;

		public boolean isForward() {
			return this == FORWARD;
		}

		public boolean isReverse() {
			return this == REVERSE;
		}

	} // enum RelationDirection

	private final UsageCriteria			usagecriteria;
	private final String				sIdentifier;
	private final Long				relationId;
	private final SystemRelationType	relationtype;
	private final RelationDirection		direction;
	private String						sUserName;
	private UID 					state;

	private final Long idRoot;

	public GenericObjectTreeNode(GenericObjectTreeNodeParameters params) {

		super(params.getId(), params.getUidNode());

		this.idRoot = params.getIdRoot();
		this.usagecriteria = params.getUsagecriteria();
		this.sIdentifier = params.getsIdentifier();
		this.relationId = params.getRelationId();
		this.relationtype = params.getRelationtype();
		this.direction = params.getDirection();
		this.sUserName = params.getsUserName();
		
		this.setLabel(params.getLabel());
		this.setChangedAt(params.getChangedAt());
		this.setDescription(params.getDescription());

		this.state = usagecriteria.getStatusUID();

	}

	/**
	 * @return process uid of leased object tree node
	 */
	public UID getProcessUID() {
		return this.usagecriteria.getProcessUID();
	}

	/**
	 * @return identifier of generic object tree node
	 */
	@Override
	public String getIdentifier() {
		return this.sIdentifier;
	}

	/**
	 * @return Is this node related to another?
	 */
	public boolean isRelated() {
		return this.getRelationId() != null;
	}

	/**
	 * @return the id of the relation, if any.
	 */
	public Long getRelationId() {
		return this.relationId;
	}

	/**
	 * @return the type of the relation, if any.
	 */
	public SystemRelationType getRelationType() {
		return this.relationtype;
	}

	/**
	 * @return the direction of the relation, if any.
	 */
	public RelationDirection getRelationDirection() {
		return this.direction;
	}

	/**
	 * §postcondition result != null
	 */
	public UsageCriteria getUsageCriteria() {
		return this.usagecriteria;
	}

	/**
	 * @return the status id.
	 */
	public UID getStateUID() {
		return this.state;
	}

	@Override
	public boolean implementsNewRefreshMethod() {
		return true;
	}

	/**
	 * §postcondition result != null
	 * 
	 * @throws CommonFinderException
	 */
	@Override
	public GenericObjectTreeNode refreshed() throws CommonFinderException, CommonPermissionException {
		return getTreeNodeFacade().getGenericObjectTreeNode(this.getId(), usagecriteria.getEntityUID());
	}

	@Override
	protected List<? extends TreeNode> getSubNodesImpl() throws RemoteException, CommonPermissionException {
		return getTreeNodeFacade().getSubNodesForGenericObjectTreeNode(this);
	}

	@Override
	public UID getEntityUID() {
		return this.usagecriteria.getEntityUID();
	}

	/**
	 * orders <code>GenericObjectTreeNode</code>s by relation type, direction,
	 * module id and label.
	 */
	@Override
	public int compareTo(AbstractTreeNodeConfigured<Long> parThat) {
		if (parThat instanceof GenericObjectTreeNode) {
			GenericObjectTreeNode that = (GenericObjectTreeNode) parThat;
			// 1. order by relation type
			// Note that getRelationtype() may be null:
			int result = LangUtils.compare(this.getRelationType(), that.getRelationType());

			// 2. FORWARD < REVERSE so forward relationships are shown before
			// reverse relationships.
			// Note that getDirection() may be null:
			if(result == 0) {
				result = LangUtils.compare(this.getRelationDirection(), that.getRelationDirection());
			}

			// 3. order by module id
			if(result == 0) {
				result = this.getEntityUID().compareTo(that.getEntityUID());
			}

			// 4. order by label:
			if(result == 0) {
				result = this.getLabel().compareTo(that.getLabel());
			}

			return result;			
		}
		return 0;
	}

	private static TreeNodeFacadeRemote getTreeNodeFacade() {
		try {
			return ServiceLocator.getInstance().getFacade(
				TreeNodeFacadeRemote.class);
		}
		catch(RuntimeException ex) {
			throw new CommonFatalException(ex);
		}
	}

	private String getUserName() {
		return this.sUserName;
	}

	/**
	 * Don't use @Override as there is nothing to override seen by the
	 * (maven) java compiler.
	 * 
	 * Must be public and must not throw ObjectStreamException
	 * {@link org.springframework.beans.factory.aspectj.AbstractInterfaceDrivenDependencyInjectionAspect}.
	 */
	public Object readResolve() {
		if(this.getUserName() == null) {
			try {
				this.sUserName = getSecurityFacade().getSessionContextAsString();
			}
			catch(NuclosFatalException e) {
				throw new IllegalStateException("Username could not be set.");
			}
			catch(RuntimeException ex) {
				throw new IllegalStateException("Username could not be set.");
			}
		}

		if (this.getStateUID() == null) {
			try {
				this.state = getGenericObjectFacade().getStateByGenericObject(this.getId());
			}
			catch(NuclosFatalException e) {
				throw new IllegalStateException("StatusId could not be set.");
			}
			catch(RuntimeException ex) {
				throw new IllegalStateException("Username could not be set.");
			}
			catch(CommonFinderException e) {
				throw new IllegalStateException("StatusId could not be set.");
			}
		}
		return this;
	}

	/**
	 * gets the facade once for this object and stores it in a member variable.
	 */
	private SecurityFacadeRemote getSecurityFacade()
		throws NuclosFatalException {
		if(this.facade == null) {
			try {
				this.facade = ServiceLocator.getInstance().getFacade(
					SecurityFacadeRemote.class);
			}
			catch(RuntimeException ex) {
				throw new CommonFatalException(ex);
			}
		}
		return this.facade;
	}

	/**
	 * gets the facade once for this object and stores it in a member variable.
	 */
	private GenericObjectFacadeRemote getGenericObjectFacade()
		throws NuclosFatalException {
		if(this.gofacade == null) {
			try {
				this.gofacade = ServiceLocator.getInstance().getFacade(
					GenericObjectFacadeRemote.class);
			}
			catch(RuntimeException ex) {
				throw new CommonFatalException(ex);
			}
		}
		return this.gofacade;
	}
	
	@Override
	public Object getRootId() {
		return idRoot; 
	}

	@Override
	public GenericObjectTreeNodeParameters getGenericObjectTreeNodeParameters() {
		return new GenericObjectTreeNodeParameters(getId(), getUsageCriteria(), getIdentifier(), getRelationId(), getRelationType(), 
				getRelationDirection(), getUserName(), getNodeId(), idRoot, getLabel(), getChangedAt(), getDescription());		
	}
} // class GenericObjectTreeNode
