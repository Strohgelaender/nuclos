//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.eventsupport.valueobject.EventSupportTransitionVO;

/**
 * Value object representing a state transition.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.00
 */
public class StateTransitionVO extends NuclosValueObject<UID> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3383006265210621520L;
	private final UID clientUid;
	private UID stateSourceUid;
	private UID stateTargetUid;
	private String sDescription;
	private boolean bAutomatic;
	private boolean bDefault;
	private boolean bNonstop;
	private List<EventSupportTransitionVO> lstRules = new ArrayList<EventSupportTransitionVO>();
	
	private List<UID> lstRoleUids = new ArrayList<UID>();

	/**
	 * constructor to be called by server only
	 * @param uid primary key of underlying database record
	 * @param stateSourceUid id of state source for transition
	 * @param stateTargetUid id of state target for transition
	 * @param sDescription description of transition
	 * @param bAutomatic automatic transition only?
	 * @param dCreated creation date of underlying database record
	 * @param sCreated creator of underlying database record
	 * @param dChanged last changed date of underlying database record
	 * @param sChanged last changer of underlying database record
	 */
	public StateTransitionVO(UID uid, UID stateSourceUid, UID stateTargetUid, String sDescription,
			boolean bAutomatic, boolean bDefault, boolean bNonstop, Date dCreated, String sCreated, Date dChanged, String sChanged,
			Integer iVersion) {
		super(uid, dCreated, sCreated, dChanged, sChanged, iVersion);
		this.clientUid = uid;
		this.populateFields(stateSourceUid, stateTargetUid, sDescription, bAutomatic, bDefault, bNonstop);
	}

	/**
	 * constructor to be called by client only
	 * @param stateSourceUid id of state source for transition
	 * @param stateTargetUid id of state target for transition
	 * @param bAutomatic automatic transition only?
	 */
	public StateTransitionVO(UID uid, UID stateSourceUid, UID stateTargetUid, String sDescription,
			boolean bAutomatic, boolean bDefault, boolean bNonstop) {
		super();
		this.clientUid = uid;
		this.populateFields(stateSourceUid, stateTargetUid, sDescription, bAutomatic, bDefault, bNonstop);
	}
	
	private void populateFields(UID stateSourceUid, UID stateTargetUid, String sDescription,
			boolean bAutomatic, boolean bDefault, boolean bNonstop) {
		this.stateSourceUid = stateSourceUid;
		this.stateTargetUid = stateTargetUid;
		this.sDescription = sDescription;
		this.bAutomatic = bAutomatic;
		this.bDefault = bDefault;
		this.bNonstop = bNonstop;		
	}

	/**
	 * get copy of primary key of underlying database record (or id of new transition inserted by client)
	 * @return primary key of underlying database record
	 */
	public UID getClientUID() {
		return clientUid;
	}

	/**
	 * get state source of underlying database record
	 * @return state source of underlying database record
	 */
	public UID getStateSourceUID() {
		return stateSourceUid;
	}

	/**
	 * set state source of underlying database record
	 * @param stateSourceUid state source of underlying database record
	 */
	public void setStateSource(UID stateSourceUid) {
		this.stateSourceUid = stateSourceUid;
	}

	/**
	 * get state target of underlying database record
	 * @return state target of underlying database record
	 */
	public UID getStateTargetUID() {
		return stateTargetUid;
	}

	/**
	 * set state target of underlying database record
	 * @param stateTargetUid state target of underlying database record
	 */
	public void setStateTarget(UID stateTargetUid) {
		this.stateTargetUid = stateTargetUid;
	}

	/**
	 * get automatic flag of underlying database record
	 * @return automatic flag of underlying database record
	 */
	public boolean isAutomatic() {
		return bAutomatic;
	}

	/**
	 * set automatic flag of underlying database record
	 * @param bAutomatic automatic flag of underlying database record
	 */
	public void setAutomatic(boolean bAutomatic) {
		this.bAutomatic = bAutomatic;
	}

	/**
	 * get default flag of underlying database record
	 * @return default flag of underlying database record
	 */
	public boolean isDefault() {
		return bDefault;
	}

	/**
	 * set default flag of underlying database record
	 * @param bDefault default flag of underlying database record
	 */
	public void setDefault(boolean bDefault) {
		this.bDefault = bDefault;
	}

	/**
	 * get nonstop flag of underlying database record
	 * @return nonstop flag of underlying database record
	 */
	public boolean isNonstop() {
		return bNonstop;
	}

	/**
	 * set nonstop flag of underlying database record
	 * @param bNonstop default flag of underlying database record
	 */
	public void setNonstop(boolean bNonstop) {
		this.bNonstop = bNonstop;
	}
	
	/**
	 * get description of underlying database record
	 * @return description of underlying database record
	 */
	public String getDescription() {
		return sDescription;
	}

	/**
	 * set description of underlying database record
	 * @param sDescription description of underlying database record
	 */
	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}
	
	public void removeRule(UID ruleUID) {
		for (EventSupportTransitionVO eseVO : lstRules) {
			if (eseVO.getId().equals(ruleUID)) {
				lstRules.remove(eseVO);
				break;
			}
		}
	}
	
	/*public List<UID> getRuleUIDs(String classType) {
		List<UID> result = new ArrayList<UID>();
		for (EventSupportTransitionVO eseVO : lstRules) {
			if (eseVO.getEventSupportClassType().equals(classType)) {
				result.add(eseVO.getId());				
			}
		}
		return result;
	}*/
	
	public List<EventSupportTransitionVO> getRules() {
		return lstRules;
	}
	
	public void setRules(List<EventSupportTransitionVO> r) {
		lstRules = r;
	}
	
	/**
	 * get list of roles attached to transition
	 * @return list of roles attached to transition
	 */
	public List<UID> getRoleUIDs() {
		return lstRoleUids;
	}

	/**
	 * attach list of roles to transition
	 * @param lstRoles list of roles to attach to transition
	 */
	public void setRoleUIDs(List<UID> lstRoles) {
		this.lstRoleUids = lstRoles;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("id=").append(getId());
		result.append(",stateSrc=").append(getStateSourceUID());
		result.append(",stateTarget=").append(getStateTargetUID());
		result.append(",clientId=").append(getClientUID());
		result.append("]");
		return result.toString(); 
	}

	public boolean equals(Object obj) {
		boolean retVal = false;
		
		if (obj instanceof StateTransitionVO) {
			StateTransitionVO trans = (StateTransitionVO) obj;
			
			if (trans.getStateTargetUID().equals(this.stateTargetUid)){
				if (trans.getStateSourceUID() == null && this.stateSourceUid == null) {
					retVal = true;
				}
				else if ( (trans.getStateSourceUID() != null && this.stateSourceUid != null) && 
						   trans.getStateSourceUID().equals(this.stateSourceUid)) {
					retVal = true;
				}
			}
		}
		
		return retVal;
	}
	 
}	// class StateTransitionVO
