//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.UID;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.resource.valueobject.ResourceVO;

/**
 * Value object representing a state.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.00
 */
public class StateVO extends NuclosValueObject<UID> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8388051145762242214L;
	private UID clientUid;
	private Integer iNumeral;
	private String sStatenameDE;
	private String sStatenameEN;
	private String sDescriptionDE;
	private String sDescriptionEN;
	private UID modelUID;
	private NuclosImage bIcon;
	private String sButtonLabelDE;
	private String sButtonLabelEN;
	private ResourceVO resButtonIcon;
	private String sColor;
	private String sTab;
	private boolean bAuto;
	private UserRights userrights = new UserRights();
	private UserSubformRights usersubformrights = new UserSubformRights();
	private Set<MandatoryFieldVO> mandatoryFields = new HashSet<MandatoryFieldVO>();
	private Set<MandatoryColumnVO> mandatoryColumns = new HashSet<MandatoryColumnVO>();
	private Set<UID> mpNonStopSourceStates;

	/**
	 * constructor to be called by server only
	 *
	 * @param iNumeral				state mnemonic of underlying database record
	 * @param sStatenameDE			state name of underlying database record in German
	 * @param sStatenameEN			state name of underlying database record in English
	 * @param sDescriptionDE		model description of underlying database record in German
	 * @param sDescriptionEN		model description of underlying database record in English
	 */
	public StateVO(final NuclosValueObject<UID> nvo, final Integer iNumeral, final String sStatenameDE, final String sStatenameEN, 
			final String sDescriptionDE, final String sDescriptionEN, final NuclosImage bIcon, final UID pModelUID) {
		super(nvo);
		this.clientUid = nvo.getPrimaryKey();
		this.iNumeral = iNumeral;
		this.bIcon = bIcon;
		this.sStatenameDE = sStatenameDE;
		this.sStatenameEN = sStatenameEN;
		this.sDescriptionDE = sDescriptionDE;
		this.sDescriptionEN = sDescriptionEN;
		this.modelUID = pModelUID;
	}

	/**
	 * constructor to be called by client only
	 *
	 * @param iClientUid primary key of underlying database record
	 * @param iNumeral state mnemonic of underlying database record
	 * @param sStatenameDE			state name of underlying database record in German
	 * @param sStatenameEN			state name of underlying database record in English
	 * @param sDescriptionDE		model description of underlying database record in German
	 * @param sDescriptionEN		model description of underlying database record in English
	 */
	public StateVO(UID iClientUid, Integer iNumeral,  final String sStatenameDE, final String sStatenameEN, 
			final String sDescriptionDE, final String sDescriptionEN, NuclosImage bIcon, UID pModelUID) {
		super();
		this.clientUid = iClientUid;
		this.iNumeral = iNumeral;
		this.bIcon = bIcon;
		this.sStatenameDE = sStatenameDE;
		this.sStatenameEN = sStatenameEN;
		this.sDescriptionDE = sDescriptionDE;
		this.sDescriptionEN = sDescriptionEN;
		this.modelUID = pModelUID;
	}

	/**
	 * get copy of primary key of underlying database record (or id of new states inserted by client)
	 *
	 * @return primary key of underlying database record
	 */
	public UID getClientUID() {
		return clientUid;
	}

	/**
	 * get mnemonic of underlying database record
	 *
	 * @return mnemonic of underlying database record
	 */
	public Integer getNumeral() {
		return iNumeral;
	}

	/**
	 * set mnemonic of underlying database record
	 *
	 * @param iNumeral mnemonic of underlying database record
	 */
	public void setNumeral(Integer iNumeral) {
		this.iNumeral = iNumeral;
	}

	/**
	 * get icon of underlying database record
	 *
	 * @return icon of underlying database record
	 */
	public NuclosImage getIcon() {
		return bIcon;
	}

	/**
	 * set icon of underlying database record
	 *
	 * @param bIcon icon of underlying database record
	 */
	public void setIcon(NuclosImage bIcon) {
		this.bIcon = bIcon;
	}
	
	/**
	 * get button icon of underlying database record
	 *
	 * @return button icon of underlying database record
	 */
	public ResourceVO getButtonIcon() {
		return resButtonIcon;
	}

	/**
	 * set button icon of underlying database record
	 *
	 * @param resButtonIcon button icon of underlying database record
	 */
	public void setButtonIcon(ResourceVO resButtonIcon) {
		this.resButtonIcon = resButtonIcon;
	}

	/**
	 * get color of underlying database record
	 *
	 * @return color of underlying database record
	 */
	public String getColor() {
		return sColor;
	}

	/**
	 * set color of underlying database record
	 *
	 * @param sColor color of underlying database record
	 */
	public void setColor(String sColor) {
		this.sColor = sColor;
	}

	/**
	 * get state name of underlying database record
	 *
	 * @return state name of underlying database record
	 */
	public String getStatename(Locale locale) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			return sStatenameDE;
		} else {
			return sStatenameEN;			
		}
	}

	/**
	 * set state name of underlying database record
	 *
	 * @param sStatename state name of underlying database record
	 */
	public void setStatename(Locale locale, String sStatename) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			this.sStatenameDE = sStatename;
		} else {
			this.sStatenameEN = sStatename;		
		}
	}

	public String getButtonLabel(Locale locale) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			return sButtonLabelDE;
		} else {
			return sButtonLabelEN;
		}
	}

	public void setButtonLabel(Locale locale, String sButtonLabel) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			this.sButtonLabelDE = sButtonLabel;
		} else {
			this.sButtonLabelEN = sButtonLabel;
		}
	}

	/**
	 * get state description of underlying database record
	 *
	 * @return state description of underlying database record
	 */
	public String getDescription(Locale locale) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			return sDescriptionDE;			
		} else {
			return sDescriptionEN;			
		}
	}

	/**
	 * set state description of underlying database record
	 *
	 * @param sDescription state description of underlying database record
	 */
	public void setDescription(Locale locale, String sDescription) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			this.sDescriptionDE = sDescription;
		} else {
			this.sDescriptionEN = sDescription;			
		}
	}

	public UserRights getUserRights() {
		return userrights;
	}

	/**
	 * get id of the related state model
	 *
	 * @return id of the related state model
	 */
	public UID getModelUID() {
		return modelUID;
	}

	/**
	 * set id of the related state model
	 *
	 * @param pModelUID id of the related state model
	 */
	public void setModelId(UID pModelUID) {
		this.modelUID = pModelUID;
	}

	/**
	 * §precondition userrights != null
	 */
	public void setUserRights(UserRights userrights) {
		if (userrights == null) {
			throw new NullArgumentException("userrights");
		}
		this.userrights = userrights;
	}

	public UserSubformRights getUserSubformRights() {
		return usersubformrights;
	}

	/**
	 * §precondition usersubformrights != null
	 */
	public void setUserSubformRights(UserSubformRights usersubformrights) {
		if (usersubformrights == null) {
			throw new NullArgumentException("usersubformrights");
		}
		this.usersubformrights = usersubformrights;
	}

	@Override
	public String toString() {
		return this.getNumeral() == null ? "" : this.getNumeral() + " " + this.getStatename(Locale.GERMAN);
	}
	
	public void setTabbedPaneName(String sTab) {
		this.sTab = sTab;
	}
	
	public String getTabbedPaneName() {
		return sTab;
	}
	
	public void setFromAutomatic(boolean bAuto) {
		this.bAuto = bAuto;
	}
	
	public boolean isFromAutomatic() {
		return bAuto;
	}
	
	public void addNonstopSource(UID sourceStateUid) {
		if (mpNonStopSourceStates == null) {
			mpNonStopSourceStates = new HashSet<UID>();
		}
		mpNonStopSourceStates.add(sourceStateUid);
	}
	
	public boolean isTransitionNonstop(UID sourceStateUid) {
		return mpNonStopSourceStates != null && mpNonStopSourceStates.contains(sourceStateUid);
	}
	
	public Set<UID> getNonStopSourceStates() {
		return mpNonStopSourceStates != null ? Collections.unmodifiableSet(mpNonStopSourceStates) : null;
	}
	
	/**
     * @param mandatoryFields the mandatoryFields to set
     */
    public void setMandatoryFields(Set<MandatoryFieldVO> mandatoryFields) {
    	if (mandatoryFields == null) {
			throw new NullArgumentException("mandatoryFields");
		}
	    this.mandatoryFields = mandatoryFields;
    }

	/**
     * @return the mandatoryFields
     */
    public Set<MandatoryFieldVO> getMandatoryFields() {
	    return mandatoryFields;
    }

	/**
     * @param mandatoryColumns the mandatoryColumns to set
     */
    public void setMandatoryColumns(Set<MandatoryColumnVO> mandatoryColumns) {
    	if (mandatoryColumns == null) {
			throw new NullArgumentException("mandatoryColumns");
		}
	    this.mandatoryColumns = mandatoryColumns;
    }

	/**
     * @return the mandatoryColumns
     */
    public Set<MandatoryColumnVO> getMandatoryColumns() {
	    return mandatoryColumns;
    }

	/**
	 * maps a role id to a collection of <code>AttributegroupPermissionVO</code>.
	 */
	public static class UserRights extends MultiListHashMap<UID, AttributegroupPermissionVO> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8384014986527551508L;
	}
	
	/**
	 * maps a role id to a collection of <code>SubformPermissionVO</code>.
	 */
	public static class UserSubformRights extends MultiListHashMap<UID, SubformPermissionVO> {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3745435483246237032L;
	}

}	// class StateVO
