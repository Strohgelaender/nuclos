//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.AtomicCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;

import com.google.common.primitives.Ints;

/**
 * Abstract <em>un-synchronized</em> implementation of a <code>ProxyList</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author Christoph Radig, Oliver Brausch, Thomas Pasch
 * @version 01.00.00
 */
public abstract class AbstractProxyList<T, E extends HasPrimaryKey<? extends T>> implements ProxyList<T,E>, Serializable {
	
	private static final Logger LOG = Logger.getLogger(AbstractProxyList.class);
	private static final long serialVersionUID = 7985887L;

	public static final int HUGELISTSIZE = 250000;
	public static final int LARGELISTSIZE = 65536;
	private static final int MAXPAGESIZE = 10000;
	private static final int DEFAULTPAGESIZE = 100;
	private int pageSize = DEFAULTPAGESIZE;
	private int jumpLimit = HUGELISTSIZE;
	protected final ProxyListProvider proxyListProvider;
	//

	/**
	 * the index of the last element that has already bean read.
	 */
	private volatile int iLastIndexRead = 0;

	/**
	 * The real data containers: index -&gt; values/entities
	 */
	protected final BidiMap<Integer, E> index2Loaded;
	
	/**
	 * Ids/primary keys of loaded values/entities.
	 */
	protected final BidiMap<T,Integer> id2Index;

	/**
	 * returns the size of the data set (rowcount).
	 */
	private int mdsize = -1;
	private boolean hugeList = false;
	private boolean countListOff = false;
	private boolean endOfData = false;

	protected final UID entity;
	protected final CollectableSearchExpression clctexpr;
	protected final Collection<UID> fields;

	//
	
	public AbstractProxyList(UID entity, CollectableSearchExpression clctexpr, Collection<UID> fields, ProxyListProvider plProvider) {
		this.entity = entity;
		this.clctexpr = clctexpr;
		this.fields = fields;
		this.proxyListProvider = plProvider;
		id2Index = new DualHashBidiMap<T,Integer>();
		index2Loaded = new DualHashBidiMap<Integer, E>();
	}

	protected abstract Collection<E> fetchChunk(ResultParams resultParams) throws RemoteException, CommonPermissionException;
	
	protected abstract Integer countMasterDataRows() throws RemoteException, CommonPermissionException;
	
	protected abstract boolean blockSorting();
	
	protected abstract Object getValue(E obj, UID field);

	/**
	 * reads the first chunk if necessary. Must be called as the last statement of the ctor in subclasses.
	 */
	protected void initialize() {
		pageSize = proxyListProvider.overrideChunksize(DEFAULTPAGESIZE);
		jumpLimit = proxyListProvider.overrideJumpLimit(HUGELISTSIZE);
		countListOff = proxyListProvider.isCountResultListOff() && !org.nuclos.common.E.isNuclosEntity(entity);
		
		if (size() > 0) get(0);
	}

	public int size(boolean forceCount) {
		if (forceCount || mdsize == -1) {
			try {
				if (countListOff) {
					mdsize = pageSize;
				} else {
					mdsize = countMasterDataRows();
				}
			}
			catch (RemoteException | CommonPermissionException nfe) {
				LOG.warn("size() failed", nfe);
			}
		}
		hugeList = mdsize >= jumpLimit && jumpLimit > 0;
		return mdsize;
	}
	
	@Override
	public int size() {
		return size(false);
	}


	@Override
	public int totalSize(boolean forceCount) {
		if (forceCount) {
			countListOff = false;
		}
		size(forceCount);
		return !countListOff || endOfData ? mdsize : 0x7FFFFFFF;
	}
	
	@Override
	public boolean isOnlySequentialPaging() {
		return countListOff || hugeList;
	}
	
	@Override
	public boolean isHugeList() {
		return hugeList;
	}
	
	//NUCLOS-5161 For fetching a big bulk, bigger chunk-sizes are preferred. But never bigger than requested though.
	@Override
	public Collection<E> getBulk(int[] indizes) {
		Collection<E> res = new ArrayList<E>();
		if (indizes.length == 0) {
			return res;
		}
		
		int max = Ints.max(indizes);
		int min = Ints.min(indizes);
		
		int savedPageSize = pageSize;
		pageSize = Math.min(MAXPAGESIZE, max - min);
		
		get(min); //Fetch the chunk from the lowest index
		
		for (int i : indizes) {
			res.add(get(i));
		}
		
		pageSize = savedPageSize;
		return res;
	}

	/**
	 * @param iIndex
	 * @return
	 */
	@Override
	public E get(int iIndex) {
		if (iIndex >= size()) {
			throw new IndexOutOfBoundsException(Integer.toString(iIndex));
		}
		fetchDataIfNecessary(iIndex, null);

        E value = index2Loaded.get(iIndex);
        return value;
	}
	
	public E getRaw(int index) {
		return index2Loaded.get(index);
	}
	
	@Override
	public E getRawById(T id) {
		E result = null;
		int index = getIndexById(id);
		if (index >= 0) {
			result = getRaw(index);
		}
		return result;
	}
	
	@Override
	public int indexOf(E element) {
		Integer result = index2Loaded.getKey(element);
		if (result == null) {
			result = Integer.valueOf(-1);
		}
		return result.intValue();
	}

	@Override
	public T getIdByIndex(int index) {
		return id2Index.getKey(index);
	}
	
	@Override
	public Collection<T> getAllLoadedIds() {
		return id2Index.keySet();
	}

	/**
	 * gets the data for all entries at least up to (including) the given index.
	 * @param iIndex
	 * @param changelistener Optional <code>ChangeListener</code> that gets notified when the last index was increased.
	 */
	@Override
	public void fetchDataIfNecessary(int iIndex, ChangeListener changelistener) {
		int iSize = size();
		if (iIndex >= iSize) {
			throw new IndexOutOfBoundsException(Integer.toString(iIndex));
		}
		if (!hasObjectBeenReadForIndex(iIndex)) {
			try {
				int rowsloaded = fetchDataChunk(iIndex, changelistener);
				if (rowsloaded < 0) {
					// count changed or count is wrong
					if (iSize >= iIndex) {
						mdsize = iIndex;
						if (changelistener != null) {
							changelistener.stateChanged(new ChangeEvent(this));
						}
					}
				}
			} catch (CommonPermissionException cpe) {
				throw new NuclosFatalException(cpe);
			}
		}
	}

	/**
	 * fetchChunkAndMap: Method in order to read a data chunk 
	 * @param istart Integer: Starting Rownumber (exclusive)
	 * @param iend Integer: Ending Rownumber (inclusive)
	 * @return Boolean: True when at least on row was read and inserted in the list
	 * @throws RemoteException
	 */
	private int fetchChunkAndMap(Integer istart, Integer iend) throws RemoteException, CommonPermissionException {
		if (blockSorting()) {
			this.clctexpr.setSortingOrder(new ArrayList<CollectableSorting>());
		}
		Long rowcount = iend.longValue() - istart.longValue() + 1L;
		ResultParams resultParams = new ResultParams(fields, istart.longValue(), rowcount, true);
		
		//NUCLOS-5302, if there is an anchor from where the next block can start, add it to the result params.
		if (istart > 0 && index2Loaded.containsKey(istart - 1)) {
			E obj = index2Loaded.get(istart - 1);
			
			for (CollectableSorting sorting : clctexpr.getSortingOrder()) {
				if (sorting.isBaseEntity()) {
					Object oValue = getValue(obj, sorting.getField());
					AtomicCollectableSearchCondition cc;
					if (oValue != null) {
						ComparisonOperator compop = sorting.isAscending() ? ComparisonOperator.GREATER : ComparisonOperator.LESS;
						cc = SearchConditionUtils.newComparison(sorting.getField(), compop, oValue);						
					} else {
						//NUCLOS-5437 This just a placeholder to avoid to error within the ComparisonOperator.
						//It works for now because it is no used yet. See EntityObjectProcessor.java line 541.
						cc = SearchConditionUtils.newIsNotNullCondition(sorting.getField());	
					}
					
					resultParams.getAnchor().add(cc);
				}
			}
			
			resultParams.getAnchor().add(new CollectableIdCondition(obj.getPrimaryKey()));
		}

		Collection<E> mdwv = fetchChunk(resultParams);
		if (mdwv.isEmpty()) {
			return -1;
		}
		int icount = 0;
		
		for (Iterator<E> iter = mdwv.iterator(); iter.hasNext();) {
			E eRow = iter.next();
			T tId = eRow.getPrimaryKey();
			if (id2Index.containsKey(tId) && index2Loaded.containsKey(istart + icount)) {
				continue;
			}
			
			final int index = istart + (icount++);
			index2Loaded.put(index, eRow);
			id2Index.put(tId, index);
			if (index > iLastIndexRead) {
				iLastIndexRead = index;
			}
		}
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("FDC2: " + istart + "+" + rowcount + " IMPORTED:" + icount + " (" + getClass() + ")");
		}
		return icount;
	}
	
	/**
	 * fetchDataChunk: Method for checking and reading datachunks
	 * @param iIndex Integer: The current index of displayed row in the tablemodel.
	 * @param changelistener ChangeListener: The ChangeListener to the JTable, can be null.
	 * @return int: Number of records that has been loaded from server AND added to the result list
	 * 		 		rowsLoaded == 0: there are still records at iIndex, but they have already been loaded
	 * 		 	    rowsLoaded == -1: there are no more records
	 */
	private int fetchDataChunk(int iIndex, ChangeListener changelistener) throws CommonPermissionException {
		try {
			int rowsLoaded = 0;
			
			if (isOnlySequentialPaging()) {
				rowsLoaded = fetchChunkAndMap(iIndex, iIndex + pageSize);
				endOfData = rowsLoaded <= pageSize;
				if (countListOff) {
					mdsize = index2Loaded.size();
					if (!endOfData) {
						index2Loaded.remove(iIndex + pageSize);
					}
				}
				LOG.debug("Index=" + iIndex + " PageSize=" + pageSize + " rowsLoaded=" + rowsLoaded + " endOfData=" + endOfData + " size=" + mdsize);
				
			} else {
				
				int iIndexStart = Math.max(0, iIndex - pageSize);
				int iIndexEnd = Math.min(size(), iIndex + pageSize);
				
				int chunkstart = -1;
				for (int i = iIndexStart; i < iIndexEnd; i++) {
					if (!index2Loaded.containsKey(i)) {
						if (chunkstart == -1) {
							chunkstart = i;
						}
						continue;
					}
					if (chunkstart == -1) {
						continue;
					}
					rowsLoaded += fetchChunkAndMap(chunkstart, i);
					chunkstart = -1;					
				}
				
	            if (chunkstart != -1 && (iIndexEnd - 1 >= chunkstart || (iIndex == 0 && mdsize == 1))) {
	            	rowsLoaded += fetchChunkAndMap(chunkstart, iIndexEnd - 1);
	            }
            
			}
				
			if (rowsLoaded > 0 && changelistener != null) {
				changelistener.stateChanged(new ChangeEvent(this));
			}

			return rowsLoaded;
		} catch (RemoteException ex) {
			throw new NuclosFatalException(ex);
		}
	}

	@Override
	public boolean hasObjectBeenReadForIndex(int index) {
		return index2Loaded.containsKey(index);
	}

	@Override
	public boolean add(E element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int index, E element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends E> coll) {
		boolean result = false;
		for(E element : coll) {
			result = result || add(element);
		}
		return result;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean contains(Object o) {
		return index2Loaded.containsValue(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		for (Object o : c) {
			if (!contains(o)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int indexOf(Object o) {
		return index2Loaded.getKey(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		// ???
		return indexOf(o);
	}

	@Override
	public ListIterator<E> listIterator() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		boolean result = false;
		int index = indexOf(o);
		if (index >= 0) {
			result = index2Loaded.remove(index) != null;
			id2Index.removeValue(index);
		}
		return result;
	}

	@Override
	public E remove(int iIndex) {
		final E result = index2Loaded.remove(iIndex);
		id2Index.removeValue(iIndex);
		if (result != null) {
			mdsize--;
			for (int i = iIndex; i < size() - 1; i++) {
				E next = index2Loaded.get(i + 1);
				index2Loaded.put(i, next);
				if (next != null) {
					id2Index.put(next.getPrimaryKey(), i);
				}
			}
		}
		else {
			LOG.info("APL:ERROR:NULL ON ROW:" + iIndex);
		}
		return result;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public E set(int index, E element) {
		final E oldValue = index2Loaded.remove(index);
		index2Loaded.put(index, element);
		id2Index.put(element.getPrimaryKey(), index);
		return oldValue;
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	@Override
	public <X> X[] toArray(X[] a) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		index2Loaded.clear();
		id2Index.clear();
	}

	@Override
	public boolean isEmpty() {
		return index2Loaded.isEmpty();
	}

	@Override
	public int getLastIndexRead() {
		return iLastIndexRead;
	}

	protected void setLastIndexRead(int iIndex) {
		this.iLastIndexRead = iIndex;
	}

	/**
	 * get index by id of object in list
	 * @param oId the object's id
	 * @return index of object in list; -1 if not found
	 */
	@Override
	public int getIndexById(T oId) {
		Integer result = id2Index.get(oId);
		if (result == null) {
			result = Integer.valueOf(-1);
		}
		return result.intValue();
	}

	/**
	 * @return an <code>Iterator</code> that doesn't check for concurrent co-modification.
	 */
	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private int iIndex = -1;

			@Override
			public boolean hasNext() {
				return iIndex + 1 < AbstractProxyList.this.size();
			}

			@Override
			public E next() {
				return AbstractProxyList.this.get(++iIndex);
			}

		    // default implementation in java8 @Override 
			public void remove() {
				AbstractProxyList.this.remove(iIndex);
			}
		};
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		final int size = size();
		result.append(getClass().getName()).append("[");
		result.append("size=").append(size);
		mapDescription(result, index2Loaded, 5);
		result.append("]");
		return result.toString();
	}
	
	protected void mapDescription(StringBuilder result, Map<Integer,E> map, int max) {
		int i = 0;
		for (Integer index: map.keySet()) {
			final E vo = map.get(index);
			result.append(", id:").append(index).append(" -> ").append(vo);
			if (++i > max) break; 
		}
	}

}  // class AbstractProxyList
