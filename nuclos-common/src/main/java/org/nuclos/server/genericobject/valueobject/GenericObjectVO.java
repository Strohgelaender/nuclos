//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.valueobject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nuclos.common.GenericObjectMetaDataProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.StateClientDelegate;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.DataLanguageMap;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.attribute.valueobject.AttributeCVO.DataType;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Value object representing a generic object with all its attributes.
 * <code>GenericObjectVO</code>s may be incomplete (partially loaded), meaning they contain only a selected set of attributes.
 * This allows us to efficiently load a large number of generic objects when only some attributes need to be displayed.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.000
 */
public class GenericObjectVO extends NuclosValueObject<Long> implements Cloneable
//TODO MULTINUCLET: Enable for backwards compatibility after refactoring... 
//, IGenericObjectVODeprecated
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6832161192594070061L;

	/**
	 * name of the attribute, if any, that contains the name.
	 */
	public static final String ATTRIBUTENAME_NAME = "name";

	/**
	 * the id of the module this generic object belongs to.
	 */
	private UID module;

	/**
	 * the attribute values.
	 */
	private Map<UID, DynamicAttributeVO> mpattrvo;

	/**
	 * Has this object been (logically) deleted?
	 */
	private boolean bDeleted;
	
	/**
	 * Has this object been completely loaded?
	 */
	private boolean bComplete = false;

	private UID stateIconAttribute;
	
	private Object canWrite;
	private Object canStateChange;
	private Object canDelete;

	// map for dataLanguages
	private IDataLanguageMap mpDataLanguages = new DataLanguageMap();
	
	
	/**
	 * Just called by GenericObjectCollectController.updateCollectable to handle update where isCollectiveProcessing=true
	 * @param paramdataLanguageMap
	 */
	public void updateDataLanguageMap(IDataLanguageMap paramdataLanguageMap) {
		if(paramdataLanguageMap==null || paramdataLanguageMap.getLanguageMap()==null)
			return;
		
		for(UID language:paramdataLanguageMap.getLanguageMap().keySet())
		{			
			DataLanguageLocalizedEntityEntry paramlan=paramdataLanguageMap.getDataLanguage(language);
			if(paramlan==null || paramlan.getValueMap()==null)
				continue;
			for(UID field:paramlan.getValueMap().keySet())
			{
				 if(paramlan==null || paramlan.getValueMap()==null || paramlan.isFlagUnchanged())
					continue;
				if(!field.toString().startsWith("DL_") || field.toString().endsWith("MOD"))
					continue;
				if(paramlan.getValue(field) instanceof String)
				{
					String targetValue=paramlan.getValue(field);
					if(mpDataLanguages.getDataLanguage(language)!=null)
					{
						try{mpDataLanguages.getDataLanguage(language).setValue(field, targetValue);}catch(Exception e){}
					
					}
				}
			}
		}
	}
	
	public void setDataLanguageMap(IDataLanguageMap map) {
		if (map == null) {
			this.mpDataLanguages.clear();
		} else {
			this.mpDataLanguages = map;
		}
	}	

	public IDataLanguageMap getDataLanguageMap() {
		return this.mpDataLanguages;
	}
	
	/**
	 * constructor to be called by client only
	 * 
	 * §postcondition this.getId() == null
	 * 
	 * @param module uid of underlying database record
	 * @param lometadataprovider used for setting default values, if != null
	 */
	public GenericObjectVO(UID module, GenericObjectMetaDataProvider lometadataprovider) {
		super();
		this.module = module;
		this.mpattrvo = CollectionUtils.newHashMap();
		this.bDeleted = false;

		if (lometadataprovider != null) {
			for (AttributeCVO attrcvo : lometadataprovider.getAttributeCVOsByModule(module, false)) {
				
				if (attrcvo.getDataType().equals(DataType.BOOLEAN)) {
					// boolean attribute should not be null
					this.setAttribute(new DynamicAttributeVO(attrcvo.getPrimaryKey(), attrcvo.getDefaultValueId(), attrcvo.getDefaultValueUid(), false));
				}
				
				if (attrcvo.getDefaultValue() != null) {
					this.setAttribute(new DynamicAttributeVO(attrcvo.getPrimaryKey(), attrcvo.getDefaultValueId(), attrcvo.getDefaultValueUid(), attrcvo.getDefaultValue()));
				}
			}
		}
		stateIconAttribute = SF.STATEICON.getUID(module);
		assert this.getId() == null;
	}

	/**
	 * §precondition permission != null
	 * 
	 * @param nvo contains the common fields.
	 * @param module uid of underlying database record
	 * @param bDeleted Deletion flag
	 */
	public GenericObjectVO(NuclosValueObject<Long> nvo, UID module, Boolean bDeleted) {
		super(nvo);
		this.module = module;
		this.mpattrvo = CollectionUtils.newHashMap();
		this.bDeleted = bDeleted == null ? false : bDeleted;
		stateIconAttribute = SF.STATEICON.getUID(module);
	}

	/**
	 * copy constructor
	 * 
	 * §postcondition this.getId() == that.getId()
	 * 
	 * @param that
	 */
	protected GenericObjectVO(GenericObjectVO that) {
		this(that, that.getModule(), that.isDeleted());
		setComplete(that.isComplete());
		this.setCanWrite(that.getCanWrite());
		this.setCanStateChange(that.getCanStateChange());
		this.setCanDelete(that.getCanDelete());
		this.stateIconAttribute = that.stateIconAttribute;
		assert this.mpattrvo.isEmpty();
		this.mpattrvo.putAll(that.mpattrvo);
		assert this.getId() == that.getId();
	}

	/**
	 * §postcondition result.getId() == this.getId()
	 * 
	 * @return an exact clone of this object.
	 */
	@Override
	public GenericObjectVO clone() {
		final GenericObjectVO result = (GenericObjectVO) super.clone();
		// deepen shallow copy:
		result.mpattrvo = new HashMap<UID, DynamicAttributeVO>(this.mpattrvo);
		result.stateIconAttribute = this.stateIconAttribute;
		result.setDataLanguageMap(getDataLanguageMap().copy());
		assert result.getId() == this.getId();
		return result;
	}

	/**
	 * §precondition this.isComplete()
	 * §postcondition result.getId() == null
	 * §postcondition result.getModuleId() == this.getModuleId()
	 * §postcondition result.getParentId() == this.getParentId()
	 * 
	 * @return a copy of this object that can be inserted into the database.
	 */
	public GenericObjectVO copy() {
		if (!this.isComplete()) {
			throw new IllegalStateException("This object is not complete.");
		}
		final GenericObjectVO result = new GenericObjectVO(new NuclosValueObject<Long>(), this.getModule(), this.bDeleted);
		result.setComplete(isComplete());
		result.stateIconAttribute = this.stateIconAttribute;
		result.setDataLanguageMap(getDataLanguageMap().copy());
		assert result.mpattrvo.isEmpty();
		result.mpattrvo.putAll(this.mpattrvo);

		assert result.getId() == null;
		assert result.getModule() == this.getModule();
		return result;
	}

	public boolean isComplete() {
		return bComplete;
	}

	public void setComplete(boolean bComplete) {
		this.bComplete = bComplete;
	}

	/**
	 * @return the uid of the module this generic object belongs to.
	 */
	public UID getModule() {
		return this.module;
	}

	/**
	 * sets the module uid that this generic object belongs to.
	 */
	public void setModule(UID module) {
		this.module = module;
	}

	/**
	 * get generic object attribute
	 * @param attribute uid
	 * @return generic object attribute for attribute id
	 */
	public DynamicAttributeVO getAttribute(UID attribute) {
		if (LangUtils.equal(attribute, stateIconAttribute)) {
			DynamicAttributeVO dynAttrStateIcon = this.mpattrvo.get(attribute);
			if (dynAttrStateIcon == null || dynAttrStateIcon.getValue() == null) {
				// get state icon from client cache only ...
				try {
					StateClientDelegate stateDelegate = SpringApplicationContextHolder.getBean(StateClientDelegate.class);
					if (stateDelegate != null) {
						StateVO state = stateDelegate.getState(getModule(), getStatus());
						if (dynAttrStateIcon == null) {
							dynAttrStateIcon = new DynamicAttributeVO(attribute, null, null, state.getIcon());
							setAttribute(dynAttrStateIcon);
						} else {
							dynAttrStateIcon.setValue(state.getIcon());
						}
					}
				} catch (Exception ex) {
					// ignore: server call
				}
			}
		}
		return this.mpattrvo.get(attribute);
	}

	/**
	 * get generic object attribute for the tree view, don't need to check if the attribute was loaded
	 * @param attribute uid
	 * @return generic object attribute for attribute uid
	 */
	public DynamicAttributeVO getAttributeForTreeView(UID attribute) {
		return getAttribute(attribute);
	}

	

	/**
	 * set generic object attribute
	 * @param attrvo generic object attribute
	 */
	public void setAttribute(DynamicAttributeVO attrvo) {
		final UID attribute = attrvo.getAttributeUID();

		this.mpattrvo.put(attribute, attrvo);
	}

	/**
	 * get all attributes for generic object
	 * @return Collection&lt;DynamicAttributeVO&gt; attributes for generic object
	 */
	public Collection<DynamicAttributeVO> getAttributes() {
		return this.mpattrvo.values();
	}

	/**
	 * set all attributes for generic object
	 * @param collattrvo attributes for generic object
	 */
	public void setAttributes(Collection<DynamicAttributeVO> collattrvo) {
		this.mpattrvo = new HashMap<UID, DynamicAttributeVO>(collattrvo.size(), 1.0f);

		for (DynamicAttributeVO attrvo : collattrvo) {
			this.setAttribute(attrvo);
		}
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param attrprovider
	 * @return the usage criteria of this generic object.
	 */
	public UsageCriteria getUsageCriteria(String sCustom) {
		return new UsageCriteria(this.getModule(),
				this.getSystemAttributeValue(SF.PROCESS.getUID(this.getModule())),
				this.getSystemAttributeValue(SF.STATE.getUID(this.getModule())),
				sCustom);
	}

	/**
	 * @return this object's system identifier, if any.
	 */
	public String getSystemIdentifier() {
		final DynamicAttributeVO attrvo = this.getAttribute(SF.SYSTEMIDENTIFIER.getUID(this.getModule()));
		return (attrvo == null) ? null : (String) attrvo.getValue();
	}

	private UID getSystemAttributeValue(UID attribute) {
		final DynamicAttributeVO attrvo = this.getAttribute(attribute);
		return (attrvo == null) ? null : attrvo.getValueUid();
	}

	/**
	 * @return Has this object been (logically) deleted?
	 */
	public boolean isDeleted() {
		return this.bDeleted;
	}

	/**
	 * marks the generic object as deleted (logical)
	 */
	public void setDeleted(boolean bDeleted) {
		this.bDeleted = bDeleted;
	}

	/**
	 * get the status uid of this object
	 * @return status uid
	 */
	public UID getStatus() {
		DynamicAttributeVO attributeVO = getAttribute(SF.STATE.getUID(this.getModule()));
		return (attributeVO != null) ? attributeVO.getValueUid() : null;
	}

	/**
	 * get the process uid of this object
	 * @return process uid
	 */
	public UID getProcess() {
		DynamicAttributeVO attributeVO = getAttribute(SF.PROCESS.getUID(this.getModule()));
		return (attributeVO != null) ? attributeVO.getValueUid() : null;
	}

	/**
	 * first add attribute if it wasn't loaded and then set the given attribute
	 * @param attrvo
	 */
	public void addAndSetAttribute(DynamicAttributeVO attrvo) {
		setAttribute(attrvo);
	}

	/**
	 * first add attribute if it wasn't loaded and then set the given attribute
	 * @param collattrvo
	 */
	public void addAndSetAttribute(Collection<DynamicAttributeVO> collattrvo) {
		for (DynamicAttributeVO attrvo : collattrvo) {
			addAndSetAttribute(attrvo);
		}
	}
	
	public boolean canWrite() {
		return (canWrite == null ? Boolean.TRUE.booleanValue() : isTrue(canWrite));
	}
	public Object getCanWrite() {
		return canWrite;
	}
	public void setCanWrite(Object canWrite) {
		this.canWrite = canWrite;
	}
	
	/**
	 * true in canStateChange overwrites a false in canWrite in RecardGrant
	 * @return
	 */
	public boolean canStateChange() {
		return (canStateChange == null ? canWrite() : isTrue(canStateChange));
	}
	public Object getCanStateChange() {
		return canStateChange;
	}
	public void setCanStateChange(Object canStateChange) {
		this.canStateChange = canStateChange;
	}

	public boolean canDelete() {
		return (canDelete == null ? Boolean.TRUE.booleanValue() : isTrue(canDelete));
	}
	public Object getCanDelete() {
		return canDelete;
	}
	public void setCanDelete(Object canDelete) {
		this.canDelete = canDelete;
	}
	
	private boolean isTrue(Object o) {
		if (o instanceof Boolean) {
			return (Boolean)o;
		} else if (o instanceof String) {
			if ("false".equalsIgnoreCase((String)o) || "0".equals(o)) {
				return false;
			}
		} else if (o instanceof Number) {
			if (((Number)o).longValue() == 0) {
				return false;
			}
		}
		return true;
	}
	
	public String toDescription() {
		final StringBuilder result = new StringBuilder();
		result.append("GoVO[id=").append(getId());
		result.append(",module=").append(module);
		if (bDeleted) {
			result.append(",deleted=").append(bDeleted);
		}
		result.append(",fields=").append(mpattrvo);
		result.append("]");
		return result.toString();
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("GenericObjectVO[");
		result.append("id=").append(getId());
		result.append(",module=").append(getModule());
		result.append(",status=").append(getStatus());
		result.append("]");
		return result.toString();
	}
	
//	TODO MULTINUCLET: Enable for backwards compatibility after refactoring... 
	
//	/**
//	 * get generic object attribute
//	 * @param sAttribute attribute name
//	 * @param attrprovider
//	 * @return generic object attribute for attribute name
//	 */
//	public DynamicAttributeVO getAttribute(String sAttribute, AttributeProvider attrprovider) {
//		return this.getAttribute(attrprovider.getAttribute(this.module, sAttribute).getId());
//	}
	
}	// class GenericObjectVO
