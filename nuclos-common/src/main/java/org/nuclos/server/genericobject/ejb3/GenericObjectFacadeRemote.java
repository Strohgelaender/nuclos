//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.GenericObjectMetaDataVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.genericobject.valueobject.LogbookVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

// @Remote
public interface GenericObjectFacadeRemote {

	/**
	 * @deprecated
	 */
	@RolesAllowed("Login")
	GenericObjectMetaDataVO getMetaData();

	@RolesAllowed("Login")
	Map<UID, UID> getResourceMap();

	/**
	 * §postcondition result != null
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param iGenericObjectId
	 * @return the generic object with the given id
	 * @throws CommonFinderException if there is no object with the given id.
	 * @throws CommonPermissionException if the user doesn't have the permission to view the generic object with the given id.
	 */
	@RolesAllowed("Login")
	GenericObjectVO get(UID entityUID, Long iGenericObjectId)
		throws CommonFinderException, CommonPermissionException;

	/**
	 * §postcondition result != null
	 * §postcondition result.isComplete()
	 *
	 * @param entity
	 * @param iGenericObjectId
	 * @return the complete generic object with the given id, along with its required dependants.
	 * @throws CommonFinderException if no such object was found.
	 * @throws CommonPermissionException if the user doesn't have the permission to view the generic object.
	 * @throws IllegalArgumentException if the given module id doesn't match the generic object's module id.
	 */

	@RolesAllowed("Login")
	GenericObjectWithDependantsVO getWithDependants(UID entity, Long iGenericObjectId, String customUsage)
		throws CommonPermissionException, CommonFinderException;

	/**
	 * @return reload the dependant data of the genericobject, if bAll is false, only the direct
	 *         dependants (highest hierarchie of subforms) will be reloaded
	 * @throws CommonFinderException if no such object was found.
	 */
	@RolesAllowed("Login")
	IDependentDataMap reloadDependants(
		GenericObjectVO govo, IDependentDataMap mpDependants, boolean bAll, String customUsage)
		throws CommonFinderException;

	/**
	 * gets generic object with dependants vo for a given generic object id (historical, readonly view)
	 * 
	 * §precondition dateHistorical != null
	 * §postcondition result != null
	 * §nucleus.permission mayRead(module)
	 * 
	 * @param iGenericObjectId id of generic object to show historical information for
	 * @param dateHistorical date to show historical information for
	 * @return generic object with dependants vo at the given point in time
	 * @throws CommonFinderException if the given object didn't exist at the given point in time.
	 */
	@RolesAllowed("Login")
	GenericObjectWithDependantsVO getHistorical(
		Long iGenericObjectId, Date dateHistorical, String customUsage) throws CommonFinderException,
		CommonPermissionException;

	/**
	 * gets all generic objects along with its dependants, that match a given search condition
	 * 
	 * §precondition stRequiredSubEntityNames != null
	 * §todo rename to getGenericObjectProxyList?
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr value object containing search expression
	 * @param stRequiredAttributeIds may be <code>null</code>, which means all attributes are required
	 * @param stRequiredSubEntityNames
	 * @return list of generic object value objects
	 */
	@RolesAllowed("Login")
	ProxyList<Long,GenericObjectWithDependantsVO> getGenericObjectsWithDependants(
		UID iModuleId, CollectableSearchExpression clctexpr,
		Set<UID> stRequiredAttributeIds,
		Set<UID> stRequiredSubEntityNames, 
		String customUsage) throws CommonPermissionException;

	
	/**
	 * gets all generic objects along with its dependants, that match a given search condition, but
	 * clears the values of all attributes on which the current user has no read permission
	 * NOTE: use only within report mechanism
	 * 
	 * §precondition stRequiredSubEntityNames != null
	 * §todo rename to getGenericObjectProxyList?
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr value object containing search expression
	 * @param stRequiredAttributeIds may be <code>null</code>, which means all attributes are required
	 * @param stRequiredSubEntityNames
	 * @return list of generic object value objects
	 */
	@RolesAllowed("Login")
	@Deprecated
	Collection<GenericObjectWithDependantsVO> getPrintableGenericObjectsWithDependants(
		UID iModuleId, CollectableSearchExpression clctexpr,
		Set<UID> stRequiredAttributeIds,
		Set<UID> stRequiredSubEntityNames, String custom) throws CommonPermissionException;
	

	/**
	 * gets all generic objects that match a given search condition
	 * 
	 * §precondition stRequiredSubEntityNames != null
	 * §precondition iMaxRowCount &gt; 0
	 * §postcondition result.size() &lt;= iMaxRowCount
	 * 
	 * @param iModuleId id of module to search for generic objects in
	 * @param clctexpr
	 * @param stRequiredSubEntityNames
	 * @param iMaxRowCount the maximum number of rows
	 * @return list of generic object value objects
	 * 
	 */

	@RolesAllowed("Login")
	TruncatableCollection<GenericObjectWithDependantsVO> getRestrictedNumberOfGenericObjects(
		UID iModuleId, CollectableSearchExpression clctexpr,
		Set<UID> stRequiredSubEntityNames, String customUsage,
		Long iMaxRowCount) throws CommonPermissionException;

	
	/**
	 * gets the ids of all leased objects that match a given search expression (ordered, when necessary)
	 * @param iModuleId id of module to search for leased objects in
	 * @param cse condition that the leased objects to be found must satisfy
	 * @return List&lt;Long&gt; list of leased object ids
	 */
	List<Long> getGenericObjectIds(UID iModuleId,
		CollectableSearchExpression cse) throws CommonPermissionException;

	/**
	 * gets more leased objects that match a given search condition
	 * 
	 * §precondition stRequiredSubEntityNames != null
	 * 
	 * @param iModuleId id of module to search for leased objects in
	 * @param stRequiredSubEntityNames
	 * @return list of leased object value objects
	 * 
	 */

	@RolesAllowed("Login")
	Collection<GenericObjectWithDependantsVO> getGenericObjectsMore(
		UID iModuleId, List<Long> lstIds,
		Set<UID> stRequiredSubEntityNames, String customUsage) throws CommonPermissionException;

	@RolesAllowed("Login")
	Collection<GenericObjectWithDependantsVO> getGenericObjectsChunk(
			UID iModuleId, Set<UID> stRequiredSubEntityNames, 
			CollectableSearchExpression clctexpr, ResultParams resultParams,
			String customUsage) throws CommonPermissionException ;

	
    @RolesAllowed("Login")
	Long countGenericObjectRows(UID iModuleId, final CollectableSearchExpression clctexpr) throws CommonPermissionException;

	/**
	 * creates a new generic object, along with its dependants.
	 * 
	 * §precondition gowdvo.getId() == null
	 * §precondition Modules.getInstance().isSubModule(iModuleId.intValue()) --&gt; gowdvo.getParentId() != null
	 * §precondition (gowdvo.getDependants() != null) -&gt; gowdvo.getDependants().dependantsAreNew()
	 * §precondition stRequiredSubEntityNames != null
	 *
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param gowdvo containing the generic object data
	 * @param stRequiredSubEntityNames Set&lt;UID&gt;
	 * @return the new generic object, containing the dependants for the specified sub entities.
	 *
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	GenericObjectWithDependantsVO create(
		GenericObjectWithDependantsVO gowdvo, Set<UID> stRequiredSubEntityNames)
		throws CommonPermissionException, NuclosBusinessRuleException,
		CommonCreateException, CommonFinderException;
	
	@RolesAllowed("Login")
	GenericObjectWithDependantsVO create(
		GenericObjectWithDependantsVO gowdvo, Set<UID> stRequiredSubEntityNames, String customUsage)
		throws CommonPermissionException, NuclosBusinessRuleException,
		CommonCreateException, CommonFinderException;

	/**
	 * creates a new generic object, along with its dependants.
	 * 
	 * §precondition gowdvo.getId() == null
	 * §precondition Modules.getInstance().isSubModule(gowdvo.getModuleId()) --&gt; gowdvo.getParentId() != null
	 * §precondition (gowdvo.getDependants() != null) --&gt; gowdvo.getDependants().areAllDependantsNew()
	 *
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param gowdvo the generic object, along with its dependants.
	 * @return the created object witout dependants.
	 * @throws CommonPermissionException if the current user doesn't have the right to create such an object.
	 * @throws NuclosBusinessRuleException if the object could not be created because a business rule failed.
	 * @throws CommonCreateException if the object could not be created for other reasons, eg. because of a database constraint violation.
	 *
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	GenericObjectVO create(GenericObjectWithDependantsVO gowdvo)
		throws CommonPermissionException, NuclosBusinessRuleException,
		CommonCreateException;
	
	@RolesAllowed("Login")
	GenericObjectVO create(GenericObjectWithDependantsVO gowdvo, String customUsage)
			throws CommonPermissionException, NuclosBusinessRuleException,
			CommonCreateException;

	/**
	 * updates an existing generic object in the database and returns the written object along with its dependants.
	 * 
	 * §precondition iModuleId != null
	 * §precondition lowdcvo.getModuleId() == iModuleId
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param iModuleId module id for plausibility check
	 * @param lowdcvo containing the generic object data
	 * @return same generic object as value object
	 * 
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	GenericObjectWithDependantsVO modify(UID iModuleId,
		GenericObjectWithDependantsVO lowdcvo) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException;
	
	@RolesAllowed("Login")
	GenericObjectWithDependantsVO modify(UID iModuleId,
		GenericObjectWithDependantsVO lowdcvo, String customUsage) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException;
	
	/**
	 * updates an existing generic object in the database and returns the written object along with its dependants.
	 * 
	 * §precondition iModuleId != null
	 * §precondition lowdcvo.getModuleId() == iModuleId
	 * §nucleus.permission mayWrite(module)
	 * 
	 * @param iModuleId module id for plausibility check
	 * @param lowdcvo containing the generic object data
	 * @return same generic object as value object
	 * 
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	GenericObjectWithDependantsVO modify(UID iModuleId,
		GenericObjectWithDependantsVO lowdcvo, boolean isCollectiveProcessing) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException;
	
	@RolesAllowed("Login")
	GenericObjectWithDependantsVO modify(UID iModuleId,
		GenericObjectWithDependantsVO lowdcvo, String customUsage, boolean isCollectiveProcessing) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonPermissionException, CommonStaleVersionException,
		NuclosBusinessException, CommonValidationException;

	/**
	 * delete generic object from database
	 * 
	 * §nucleus.permission mayDelete(module, bDeletePhysically)
	 * 
	 * @param bDeletePhysically remove from db?
	 * @deprecated use with customUsage
	 */
	@RolesAllowed("Login")
	@Deprecated
	void remove(UID entity, Long id,
		boolean bDeletePhysically) throws NuclosBusinessException,
		CommonFinderException,
		CommonRemoveException, CommonPermissionException,
		CommonStaleVersionException,
		CommonCreateException;
	
	@RolesAllowed("Login")
	void remove(UID entity, Long id,
		boolean bDeletePhysically, String customUsage) throws NuclosBusinessException,
		CommonFinderException,
		CommonRemoveException, CommonPermissionException,
		CommonStaleVersionException,
		CommonCreateException;

	/**
	 * restore GO marked as deleted
	 */
	void restore(UID entity, Long iId, String customUsage) throws CommonBusinessException;

	/**
	 * method to get logbook entries for a generic object
	 *
	 * §precondition Modules.getInstance().isLogbookTracking(this.getModuleContainingGenericObject(iGenericObjectId))
	 * §nucleus.permission mayRead(module)
	 *
	 * @param iGenericObjectId id of generic object to get logbook entries for
	 * @param iAttributeId		id of attribute to get logbook entries for
	 * @return collection of logbook entry value objects
	 */
	@RolesAllowed("Login")
	Collection<LogbookVO> getLogbook(Long iGenericObjectId,
		UID iAttributeId) throws CommonFinderException,
		CommonPermissionException;

	/**
	 * relates a generic object to another generic object.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget.intValue())
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iModuleIdTarget module id of target generic object to be related
	 * @param iGenericObjectIdTarget id of target generic object to be related
	 * @param iGenericObjectIdSource id of source generic object to be related to
	 * @param relationType relation type
	 */
	@RolesAllowed("Login")
	void relate(UID iModuleIdTarget,
		Long iGenericObjectIdTarget, Long iGenericObjectIdSource,
		String relationType) throws CommonFinderException, CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * relates a generic object to another generic object.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget.intValue())
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iModuleIdTarget module id of target generic object to be related (i.e. of request)
	 * @param iGenericObjectIdTarget id of target generic object to be related (i.e. request)
	 * @param iGenericObjectIdSource id of source generic object to be related to (i.e. demand)
	 * @param relationType relation type
	 * @param dateValidFrom Must be <code>null</code> for system defined relation types.
	 * @param dateValidUntil Must be <code>null</code> for system defined relation types.
	 * @param sDescription Must be <code>null</code> for system defined relation types.
	 */
	@RolesAllowed("Login")
	void relate(UID iModuleIdTarget,
		Long iGenericObjectIdTarget, Long iGenericObjectIdSource,
		String relationType, Date dateValidFrom, Date dateValidUntil,
		String sDescription) throws CommonFinderException,
		CommonCreateException, CommonPermissionException,
		NuclosBusinessRuleException;

	/**
	 * removes the relation with the given id.
	 * 
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param mpGOTreeNodeRelation
	 */
	@RolesAllowed("Login")
	void removeRelation(
		Map<Long, GenericObjectTreeNode> mpGOTreeNodeRelation)
		throws CommonBusinessException, CommonRemoveException,
		CommonFinderException;

	/**
	 * removes the relation with the given id.
	 * Note that only the relation id is really needed, the other arguments are for security only.
	 * 
	 * §precondition isGenericObjectInModule(iModuleIdTarget, iGenericObjectIdTarget)
	 * §precondition Modules.getInstance().isMainModule(iModuleIdTarget)
	 * §nucleus.permission mayWrite(targetModule)
	 * 
	 * @param iRelationId the id of the relation
	 * @param iGenericObjectIdTarget the id of the target object
	 * @param iModuleIdTarget the module id of the target object
	 */
	@RolesAllowed("Login")
	void removeRelation(Long iRelationId,
		Long iGenericObjectIdTarget, UID iModuleIdTarget)
		throws CommonRemoveException, CommonFinderException,
		CommonBusinessException;

	/**
	 * @param iGenericObjectId
	 * @return the id of the module containing the generic object with the given id.
	 * @throws CommonFinderException if there is no generic object with the given id.
	 */
	@RolesAllowed("Login")
	UID getModuleContainingGenericObject(Long iGenericObjectId)
		throws CommonFinderException;

	/**
	 * @param genericObjectId
	 * @return the id of the state of the generic object with the given id
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	UID getStateByGenericObject(Long genericObjectId)
		throws CommonFinderException;

	/**
	 * attaches document to any generic object
	 * 
	 * §todo restrict permission - check module id! requires the right to modify documents
	 * 
	 * @param mdvoDocument master data value object with fields for table of type t_ud_go_document, has to be filled
	 * with following fields: comment, createdDate, createdUser, file and genericObjectId.
	 * @param parentAttribute referencing attribute to parent object
	 */
	@RolesAllowed("Login")
	void attachDocumentToObject(MasterDataVO<Long> mdvoDocument, UID parentAttribute);


	@RolesAllowed("Login")
	public UsageCriteria getGOMeta(Long id, UID entity, String customUsage);
}
