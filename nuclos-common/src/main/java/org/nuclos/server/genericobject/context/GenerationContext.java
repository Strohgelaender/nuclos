package org.nuclos.server.genericobject.context;

import java.io.Serializable;
import java.util.Collection;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class GenerationContext<PK> implements Serializable {

	private static final long serialVersionUID = 2109948025202259939L;

	private final Collection<EntityObjectVO<PK>> sourceEos;
	private final GeneratorActionVO generator;
	private final Long parameterObjectId;
	private final boolean cloningAction;
	private final String customUsage;

	GenerationContext(
			final Collection<EntityObjectVO<PK>> sourceEos,
			final GeneratorActionVO generator,
			final Long parameterObjectId,
			final boolean cloningAction,
			final String customUsage
	) {
		this.sourceEos = sourceEos;
		this.generator = generator;
		this.parameterObjectId = parameterObjectId;
		this.cloningAction = cloningAction;
		this.customUsage = customUsage;
	}

	public Collection<EntityObjectVO<PK>> getSourceEos() {
		return sourceEos;
	}

	public GeneratorActionVO getGenerator() {
		return generator;
	}

	public Long getParameterObjectId() {
		return parameterObjectId;
	}

	public boolean isCloningAction() {
		return cloningAction;
	}

	public String getCustomUsage() {
		return customUsage;
	}
}
