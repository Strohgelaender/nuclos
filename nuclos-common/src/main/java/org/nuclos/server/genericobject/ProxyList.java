//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.swing.event.ChangeListener;

import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * Proxy list that loads its elements chunkwise. Can be used for huge search results that are displayed partially.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author Christoph Radig, Thomas Pasch
 * @version 01.00.00
 */
public interface ProxyList<T, E extends HasPrimaryKey<? extends T>> extends List<E>, Serializable {


	/**
	 * fetches the data for all entries at least up to (including) the given index.
	 * @param iIndex
	 * @param changelistener Optional <code>ChangeListener</code> that gets notified when the last index was increased.
	 */
	void fetchDataIfNecessary(int iIndex, ChangeListener changelistener);

	/**
	 * @return the index of the last element that has already been read.
	 */
	int getLastIndexRead();

	/**
	 * Return true if the object for the given index has already been loaded.
	 * 
	 * @see #getRaw(int)
	 */
	boolean hasObjectBeenReadForIndex(int index);

	/**
	 * get index by id of object in list
	 * @param oId the object's id
	 * @return index of object in list; -1 if not found
	 */
	int getIndexById(T oId);

	/**
	 * Return the object at the given index <em>if</em> the object has 
	 * already been loaded.
	 * <p>
	 * This method is necessary to support vaadin lazy loading containers.
	 * </p>
	 * @param  index of the object in the result list.
	 * @return The object or <code>null</code> if the object has not been loaded yet.
	 * @author Thomas Pasch
	 * @since  Nuclos 3.10
	 */
	E getRaw(int index);
	
	/**
	 * Return the object at the given id <em>if</em> the object has 
	 * already been loaded.
	 * <p>
	 * This method is necessary to support vaadin lazy loading containers.
	 * </p>
	 * @param  id of the object in the result list.
	 * @return The object or <code>null</code> if the object has not been loaded yet.
	 * @author Thomas Pasch
	 * @since  Nuclos 3.10
	 */
	E getRawById(T id);
	
	/**
	 * Return the index of the given object <em>if</em> the object has 
	 * already been loaded.
	 * <p>
	 * This method is necessary to support vaadin lazy loading containers.
	 * </p>
	 * @param element the object to look up the index for.
	 * @return The index or <code>-1</code> if the object has not been loaded yet. 
	 *         The method also returns <code>-1</code> if the object is not contained
	 *         in the result list.
	 * @author Thomas Pasch
	 * @since  Nuclos 3.10
	 */
	int indexOf(E element);

	/**
	 * Return the id of the object w.r.t. the given index <em>if</em> the object has 
	 * already been loaded.
	 * <p>
	 * This method is necessary to support vaadin lazy loading containers.
	 * </p>
	 * @param  index of the object in the result list.
	 * @return The id of the object or <code>null</code> if the object has not been
	 *         loaded yet.
	 * @author Thomas Pasch
	 * @since  Nuclos 3.10
	 */
	T getIdByIndex(int index);
	
	/**
	 * Return a collection of the ids of all objects loaded.
	 * <p>
	 * The behaviour of Nuclos is <em>undefined</em> if the returned collection is modified.
	 * If you want to make changes to the returned collection, you need to make a defensive copy first!
	 * </p><p>
	 * This method is necessary to support vaadin lazy loading containers.
	 * </p>
	 * @return A collection of the ids of all objects loaded.
	 * @author Thomas Pasch
	 * @since  Nuclos 3.10
	 */
	Collection<T> getAllLoadedIds();
	
	boolean isHugeList();
	
	int totalSize(boolean forceCount);
	
	boolean isOnlySequentialPaging();
	
	/**
	 * Return a bigger bulk of data-rows. This avoids the bulk being parted in a lot of small chunks which 
	 * can be very slow
	 * 
	 * @param indizes
	 * @return
	 */
	Collection<E> getBulk(int[] indizes);
	
}	// interface ProxyList
