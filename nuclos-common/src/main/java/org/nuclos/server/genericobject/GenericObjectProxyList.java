//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;

/**
 * Proxy list for leased object search results.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph Radig</a>
 * @version 00.01.000
 */
public class GenericObjectProxyList extends AbstractProxyList<Long, GenericObjectWithDependantsVO> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7335922363495428751L;

	private static GenericObjectFacadeRemote FACADE;

	private final Set<UID> stRequiredSubEntities;
	
	private final String customUsage;

	/**
	 * §precondition stRequiredSubEntityNames != null
	 * 
	 * @param iModuleId
	 * @param clctexpr
	 * @param stRequiredAttributeIds Set&lt;UID&gt; may be <code>null</code>, which means all attributes are required
	 * @param stRequiredSubEntityNames Set&lt;String&gt;
	 */
	public GenericObjectProxyList(UID iModuleId, CollectableSearchExpression clctexpr, Set<UID> stRequiredAttributeIds, 
			Set<UID> stRequiredSubEntityNames, String customUsage, ProxyListProvider plProvider) {
		super(iModuleId, clctexpr, stRequiredAttributeIds, plProvider);
		this.stRequiredSubEntities = stRequiredSubEntityNames;
		this.customUsage = customUsage;

		this.initialize();
	}

	@Override
	protected Collection<GenericObjectWithDependantsVO> fetchChunk(ResultParams resultParams) throws RemoteException, CommonPermissionException {
		return this.getGenericObjectFacade().getGenericObjectsChunk(this.entity, this.stRequiredSubEntities, 
				this.clctexpr, resultParams, this.customUsage);
	}

	@Override
	protected Integer countMasterDataRows() throws RemoteException, CommonPermissionException {
    	return this.getGenericObjectFacade().countGenericObjectRows(this.entity, this.clctexpr).intValue();
	}
	
	@Override
	protected Object getValue(GenericObjectWithDependantsVO obj, UID field) {
		return obj.getAttribute(field).getValue();
	}
	
	private GenericObjectFacadeRemote getGenericObjectFacade() {
		if (FACADE == null) {
			try {
				FACADE = SpringApplicationContextHolder.getBean(GenericObjectFacadeRemote.class);
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return FACADE;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		final int size = size();
		result.append("GenericObjectProxyList[");
		result.append("size=").append(size);
		result.append(",reqFields=").append(fields);
		result.append(",reqSubforms=").append(stRequiredSubEntities);
		result.append(",search=").append(clctexpr);
		mapDescription(result, index2Loaded, 5);
		result.append("]");
		return result.toString();
	}

	@Override
	protected boolean blockSorting() {
		return false;
	}
	
}	// class GenericObjectProxyList
