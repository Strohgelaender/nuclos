//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.valueobject;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;

/**
 * Value object representing a leased object generator.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.000
 */
public class GeneratorVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5296698471521480967L;
	private final List<GeneratorActionVO> lstActions;

	/**
	 * constructor to be called by server only
	 * @param lstActions list of generator actions
	 */
	public GeneratorVO(List<GeneratorActionVO> lstActions) {
		this.lstActions = lstActions;
	}

	/**
	 * gets a list of generator actions by module, process and state.
	 * If the source process uid is null in the spec, any given source process uid matches.
	 * If the state uid is null in the spec, any given state matches.
	 * 
	 * §postcondition result != null
	 * 
	 * @param moduleUid source module uid
	 * @param state source state uid
	 * @param process source process uid
	 * @return List&lt;GeneratorActionVO&gt; list of generator actions
	 */
	public List<GeneratorActionVO> getGeneratorActions(UID moduleUid, UID state, UID process, UID mandatorUid) {
		final List<GeneratorActionVO> result = new LinkedList<GeneratorActionVO>();
		final IMetaProvider mdProv = SpringApplicationContextHolder.getBean(IMetaProvider.class);
		for (GeneratorActionVO generatoractionvo : lstActions) {
			if (moduleUid.equals(generatoractionvo.getSourceModule())) {
				if (mandatorUid != null && generatoractionvo.getSourceMandator() != null && 
						!mandatorUid.equals(generatoractionvo.getSourceMandator())) {
					continue;
				}
				if (mdProv.getEntity(moduleUid).isStateModel()) {
					if (generatoractionvo.getUsages().isEmpty())
						result.add(generatoractionvo); // no usages... add all.
					else {
						for (GeneratorUsageVO guvo : generatoractionvo.getUsages()) {
							if ((guvo.getState() == null || guvo.getState().equals(state)) &&
									(guvo.getProcess() == null || guvo.getProcess().equals(process))) {
								result.add(generatoractionvo);
							}
						}
					}
				}
				else {
					result.add(generatoractionvo);
				}
			}
		}
		assert result != null;
		return result;
	}

	/**
	 * gets a list of generator actions by module.
	 * 
	 * §postcondition result != null
	 * 
	 * @param moduleUid source module id
	 * @return List&lt;GeneratorActionVO&gt; list of generator actions
	 */
	public List<GeneratorActionVO> getGeneratorActions(UID moduleUid) {
		final List<GeneratorActionVO> result = new LinkedList<GeneratorActionVO>();
		for (GeneratorActionVO generatoractionvo : lstActions) {
			if (moduleUid.equals(generatoractionvo.getSourceModule())) {
				result.add(generatoractionvo);
			}
		}
		//assert result != null;
		return result;
	}

	/**
	 * gets a generator action by id.
	 * 
	 * §postcondition result != null
	 * 
	 * @param generatorActionUid the generator action id
	 * @return GeneratorActionVO the generator action
	 */
	public GeneratorActionVO getGeneratorAction(UID generatorActionUid) {
		GeneratorActionVO result = null;
		for (GeneratorActionVO generatoractionvo : lstActions) {
			if (generatoractionvo.getId().equals(generatorActionUid)) {
				result = generatoractionvo;
			}
		}
		//assert result != null;
		return result;
	}

	/**
	 * gets a generator action by name.
	 * 
	 * §postcondition result != null
	 * 
	 * @param sGeneratorName the generator action name
	 * @return GeneratorActionVO the generator action
	 */
	public GeneratorActionVO getGeneratorAction(String sGeneratorName) {
		GeneratorActionVO result = null;
		for (GeneratorActionVO generatoractionvo : lstActions) {
			if (generatoractionvo.getName().equals(sGeneratorName)) {
				result = generatoractionvo;
			}
		}
		//assert result != null;
		return result;
	}

}	// class GeneratorVO
