//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.valueobject;

import java.util.Collection;

import org.nuclos.common.PropertiesMap;
import org.nuclos.common.UID;
import org.nuclos.server.resource.valueobject.ResourceVO;

/**
 * Value object representing a leased object generator action.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 01.00.000
 */
public class GeneratorActionVO implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -183299207423286282L;
	private UID uid;
	private String sName;
	private String sLabel;
	private UID nuclet;
	private ResourceVO resButtonIcon;
	private UID sourceModule;
	private UID sourceMandator;
	private UID targetModule;
	private UID targetProcess;
	private UID targetMandator;
	private UID parameterEntity;
	private boolean blnGroupAttributes;
	private boolean blnShowObject;
	private boolean blnRefreshSrcObject;
	private boolean blnRuleOnly; // TODO rename - this action will be available for buttons but not shown in toolbar dropdown if set to true
	private boolean blnCreateRelationBetweenObjects;
	private boolean blnCreateRelationToParameterObject;
	private boolean blnNonstop;
	private boolean blnCloseOnException;

	private UID valuelistProvider;

	private PropertiesMap mpProperties = null;

	private Collection<GeneratorUsageVO> collUsages;

	/**
	 * constructor to be called by server only
	 * @param uid for generator action
	 * @param sName name for generator action
	 * @param sLabel label for generator action
	 * @param sourceModule source module for generator action
	 * @param targetModule target module for generator action
	 * @param targetProcess target process for generator action
	 */
	public GeneratorActionVO(
			UID uid,
			String sName,
			String sLabel,
			ResourceVO resButtonIcon,
			UID sourceModule,
			UID sourceMandator,
			UID targetModule,
			UID targetProcess,
			UID targetMandator,
			UID parameterEntity,
			UID nuclet,
			Collection<GeneratorUsageVO> collUsages) {
		this.uid = uid;
		this.sName = sName;
		this.sLabel = sLabel;
		this.resButtonIcon = resButtonIcon;
		this.sourceModule = sourceModule;
		this.sourceMandator = sourceMandator;
		this.targetModule = targetModule;
		this.targetProcess = targetProcess;
		this.targetMandator = targetMandator;
		this.parameterEntity = parameterEntity;
		this.collUsages = collUsages;
		this.nuclet = nuclet;
	}

	public UID getNuclet() {
		return nuclet;
	}

	public boolean isGroupAttributes() {
		return blnGroupAttributes;
	}

	public void setGroupAttributes(boolean blnGroupAttributes) {
		this.blnGroupAttributes = blnGroupAttributes;
	}

	public boolean isShowObject() {
		return blnShowObject;
	}

	public void setShowObject(boolean blnShowObject) {
		this.blnShowObject = blnShowObject;
	}

	public boolean isRefreshSrcObject() {
		return blnRefreshSrcObject;
	}

	public void setRefreshSrcObject(boolean blnRefreshSrcObject) {
		this.blnRefreshSrcObject = blnRefreshSrcObject;
	}

	public boolean isRuleOnly() {
		return blnRuleOnly;
	}

	public void setRuleOnly(boolean blnRuleOnly) {
		this.blnRuleOnly = blnRuleOnly;
	}
	
	public boolean isNonstop() {
		return blnNonstop;
	}

	public void setNonstop(boolean blnNonstop) {
		this.blnNonstop = blnNonstop;
	}

	public boolean isCreateRelationBetweenObjects() {
		return blnCreateRelationBetweenObjects;
	}

	public void setCreateRelationBetweenObjects(boolean blnMakeRelationBetweenObjects) {
		this.blnCreateRelationBetweenObjects = blnMakeRelationBetweenObjects;
	}

	public boolean isCreateRelationToParameterObject() {
		return blnCreateRelationToParameterObject;
	}

	public void setCreateRelationToParameterObject(boolean blnMakeRelationToParameterObject) {
		this.blnCreateRelationToParameterObject = blnMakeRelationToParameterObject;
	}

	public boolean isCloseOnException() {
		return blnCloseOnException;
	}

	public void setCloseOnException(boolean blnCloseOnException) {
		this.blnCloseOnException = blnCloseOnException;
	}

	/**
	 * get uid for generator action
	 * @return uid for generator action
	 */
	public UID getId() {
		return uid;
	}

	/**
	 * get name for generator action
	 * @return name for generator action
	 */
	public String getName() {
		return sName;
	}

	/**
	 * get label for generator action
	 * @return label for generator action
	 */
	public String getLabel() {
		return sLabel;
	}
	
	/**
	 * get button icon for generator action
	 * @return button icon for generator action
	 */
	public ResourceVO getButtonIcon() {
		return resButtonIcon;
	}

	/**
	 * get source module for generator action
	 * @return source module for generator action
	 */
	public UID getSourceModule() {
		return sourceModule;
	}
	
	public UID getSourceMandator() {
		return sourceMandator;
	}

	/**
	 * get target module for generator action
	 * @return target module for generator action
	 */
	public UID getTargetModule() {
		return targetModule;
	}

	/**
	 * get target process for generator action
	 * @return target process for generator action
	 */
	public UID getTargetProcess() {
		return targetProcess;
	}
	
	public UID getTargetMandator() {
		return targetMandator;
	}

	public UID getParameterEntity() {
		return parameterEntity;
	}

	public Collection<GeneratorUsageVO> getUsages() {
		return collUsages;
	}

	@Override
	public String toString() {
		return (this.getLabel() == null? this.getName() : this.getLabel());
	}

	public void setProperties(PropertiesMap mpProperties) {
		this.mpProperties = mpProperties;
	}

	public PropertiesMap getProperties() {
		return this.mpProperties;
	}

	public UID getValuelistProvider() {
		return valuelistProvider;
	}

	public void setValuelistProvider(UID valuelistProvider) {
		this.valuelistProvider = valuelistProvider;
	}
}
