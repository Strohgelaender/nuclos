//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.valueobject;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.RigidFile;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.server.documentfile.DocumentFileFacadeRemote;

/**
 * Specific file class used for leased object documents.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 00.01.000
 */
public class GenericObjectDocumentFile extends RigidFile {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8826611783110916496L;

	public GenericObjectDocumentFile(RigidFile file) {
		super(file);
	}

	public GenericObjectDocumentFile(String sFileName, Object documentFilePk) {
		super(sFileName, documentFilePk);
	}

	public GenericObjectDocumentFile(String sFileName, byte[] abContents) {
		super(sFileName, null, abContents);
	}
	
	public GenericObjectDocumentFile(String sFileName, Object documentFilePk, byte[] abContents) {
		super(sFileName, documentFilePk, abContents);
	}
	
	public GenericObjectDocumentFile(NuclosFile file) {
		super(file.getName(), file.getId(), file.getContent());
	}

	public org.nuclos.common.NuclosFile getNuclosFile() {
		return new NuclosFile((UID) getDocumentFilePk(), getFilename(), getContents());
	}
	
	@Override
	protected byte[] getStoredContents() {
		try {
			DocumentFileFacadeRemote documentFileFacade = (DocumentFileFacadeRemote) SpringApplicationContextHolder.getBean("documentFileService");
			return documentFileFacade.loadContent((UID)this.getDocumentFilePk(), this.getFilename());
		}
		catch (Exception ex) {
			throw new NuclosFatalException(ex);
		}
	}

	
}	// class GenericObjectDocumentFile
