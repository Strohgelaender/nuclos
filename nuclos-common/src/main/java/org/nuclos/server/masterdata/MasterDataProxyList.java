//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata;

import java.rmi.RemoteException;
import java.util.Collection;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.AbstractProxyList;
import org.nuclos.server.genericobject.ProxyListProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Proxy list for master data search results.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph Radig</a>
 * @version 00.01.000
 */
public class MasterDataProxyList<PK> extends AbstractProxyList<PK, MasterDataVO<PK>> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3881227628592274603L;
	private static MasterDataFacadeRemote FACADE;

	public MasterDataProxyList(UID entity, Collection<UID> fields, CollectableSearchExpression clctexpr, ProxyListProvider plProvider) {
		super(entity, clctexpr, fields, plProvider);
		this.initialize();
	}

	@Override
	protected Collection<MasterDataVO<PK>> fetchChunk(ResultParams resultParams) throws RemoteException, CommonPermissionException {
		return this.getMasterDataFacade().getMasterDataChunk(this.entity, this.clctexpr, resultParams);
	}
	
	@Override
	protected Integer countMasterDataRows() throws  CommonPermissionException {
    	return this.getMasterDataFacade().countMasterDataRows(this.entity, this.clctexpr).intValue();
	}
	
	@Override
	protected Object getValue(MasterDataVO<PK> obj, UID fieldUID) {
		return obj.getFieldValue(fieldUID);
	}

	private MasterDataFacadeRemote getMasterDataFacade() {
		if (FACADE == null) {
			try {
				FACADE = (MasterDataFacadeRemote) SpringApplicationContextHolder.getBean("masterDataService");
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return FACADE;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		final int size = size();
		result.append("MasterDataProxyList[");
		result.append("size=").append(size);
		result.append(",entity=").append(entity);
		result.append(",search=").append(clctexpr);
		mapDescription(result, index2Loaded, 5);
		result.append("]");
		return result.toString();
	}

	@Override
	protected boolean blockSorting() {
		return isHugeList();
	}
	
}	// class MasterDataProxyList
