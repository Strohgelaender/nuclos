//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.valueobject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDependentDataMap;

public class MasterDataVOWrapper<PK> extends MasterDataVO<PK> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1280496260517453254L;
	//only wrapper data exist
	public final static int IS_WRAPPED = 0;
	//both wrapper and native data exist
	public final static int IS_MAPPED = 1;
	//only native data exist
	public final static int IS_NATIVE = 2;

	private Map<UID, Object> wrapperFields;
	private List<UID> wrapperFieldUids;
	private int type = -1;

	public MasterDataVOWrapper(MasterDataVO<PK> mdvo, List<UID> pWrapperFieldUids) {
		super(mdvo);
		this.wrapperFields = new HashMap<UID, Object>();
		this.wrapperFieldUids = pWrapperFieldUids;
	}

	public MasterDataVOWrapper(MasterDataVO<PK> mdvo, List<UID> wrapperFieldUids,  Map<UID, Object> wrapperFields) {
		this(mdvo, wrapperFieldUids);
		this.wrapperFields = wrapperFields;
	}

	public Map<UID, Object> getWrapperFields() {
		return wrapperFields;
	}

	public void addWrapperField(UID key, Object value){
		this.wrapperFields.put(key, value);
	}

	public boolean hasEqualWrapperFields(){
		boolean equal = true;
		Object wrapperField = null;
		Object nativeField = null;
		for(UID field : wrapperFieldUids){
			wrapperField = wrapperFields.get(field);
			nativeField = getFieldValue(field);
			if(wrapperField == null || nativeField == null || !wrapperField.equals(nativeField)){
				return false;
			}
		}
		return equal;
	}

	public void replaceNativeFields(){
		Object wrapperField = null;
		for(UID field : wrapperFieldUids){
			wrapperField = wrapperFields.get(field);
			if(wrapperField != null){
				setFieldValue(field, wrapperField);
			}
		}
	}

	public void setWrapperFields(Map<UID, Object> wrapperFields) {
		this.wrapperFields = wrapperFields;
	}

	public boolean isWrapped() {
		return (getType() == IS_WRAPPED);
	}

	public boolean isMapped(){
		return (getType() == IS_MAPPED);
	}

	public boolean isNative(){
		return (getType() == IS_NATIVE);
	}

	public int getType() {
		return type;
	}

	private void setType(int type) {
		this.type = type;
	}

	public void setIsMapped(){
		setType(IS_MAPPED);
	}

	public void setIsWrapped(){
		setType(IS_WRAPPED);
	}

	public void setIsNative(){
		setType(IS_NATIVE);
	}

	public List<UID> getMappedFields() {
		return wrapperFieldUids;
	}
	
	public String toDescription() {
		final StringBuilder result = new StringBuilder();
		result.append("MdwdVOWrapped[id=").append(getId());
		if (isChanged()) {
			result.append(",changed=").append(isChanged());
		}
		if (isSystemRecord()) {
			result.append(",sr=").append(isSystemRecord());
		}
		result.append(",fields=").append(getFieldValues());
		result.append(",type").append(type);
		result.append(",wrapped=").append(wrapperFields);
		final IDependentDataMap deps = getDependents();
		if (deps != null && !deps.isEmpty()) {
			result.append(",deps=").append(deps);
		}
		final IDependentDataMap deps2 = getDependents();
		if (deps2 != null && !deps2.isEmpty()) {
			result.append(",deps2=").append(deps2);
		}
		result.append("]");
		return result.toString();
	}
}
