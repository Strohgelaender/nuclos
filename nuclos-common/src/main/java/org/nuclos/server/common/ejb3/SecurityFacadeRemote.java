//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.security.auth.login.LoginException;

import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.common.LoginResult;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.UID;
import org.nuclos.common.Version;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.security.Permission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.MasterDataPermissions;
import org.nuclos.server.common.ModulePermissions;

// @Remote
public interface SecurityFacadeRemote {
	
	String USERNAME = "USERNAME";
	String USERUID = "USERUID";
	String IS_SUPER_USER = "IS_SUPER_USER";
	String ALLOWED_ACTIONS = "ALLOWED_ACTIONS";
	String ALLOWED_CUSTOM_ACTIONS = "ALLOWED_CUSTOM_ACTIONS";
	String MODULE_PERMISSIONS = "MODULE_PERMISSIONS";
	String MASTERDATA_PERMISSIONS = "MASTERDATA_PERMISSIONS";
	String DYNAMIC_TASKLISTS = "DYNAMIC_TASKLISTS";
	String MANDATORS_BY_LEVEL = "MANDATORS_BY_LEVEL";
	String HAS_COMMUNICATION_ACCOUNT_PHONE = "HAS_COMMUNICATION_ACCOUNT_PHONE";
	String USER_ROLES = "USER_ROLES";

	/**
	 * logs the current user in.
	 * @return session id for the current user
	 */
	@RolesAllowed("Login")
	LoginResult login();

	/**
	 * logs the current user out.
	 * @param sessionId session id for the current user
	 */
	@RolesAllowed("Login")
	void logout(Long sessionId) throws LoginException;

	/**
	 * get the date of password expiration (or null if password never expires)
	 */
	@RolesAllowed("Login")
	Date getPasswordExpiration();

	/**
	 * @return information about the current version of the application installed on the server.
	 */
	@RolesAllowed("Login")
	Version getCurrentApplicationVersionOnServer();

	/**
	 * @return information about the current version of the application installed on the server.
	 */
	@RolesAllowed("Login")
	String getUserName();

	/**
	 * Get all actions that are allowed for the current user.
	 * @return set that contains the Actions objects (no duplicates).
	 */
	@RolesAllowed("Login")
	Set<String> getAllowedActions();

	/**
	 * @return the module permissions for the current user.
	 */
	@RolesAllowed("Login")
	ModulePermissions getModulePermissions();

	/**
	 * @return the masterdata permissions for the current user.
	 */
	@RolesAllowed("Login")
	MasterDataPermissions getMasterDataPermissions();

	/**
	 * get a String representation of the users session context
	 */
	@RolesAllowed("Login")
	String getSessionContextAsString();

	/**
	 * @return the readable subforms
	 */
	Map<UID, SubformPermission> getSubFormPermission(UID enityUid);

	/**
	 * determine the permission of an attribute regarding the state of a module for the current user
	 */
	Permission getAttributePermission(UID entityUid, UID attributeUid, UID stateUid);


	@RolesAllowed("Login")
	Map<UID, Permission> getAttributePermissionsByEntity(UID entityUid, UID stateUid);

	@RolesAllowed("Login")
	void invalidateCache();

	@RolesAllowed("Login")
	Boolean isSuperUser();

	@RolesAllowed("Login")
	Map<String, Object> getInitialSecurityData();

	@RolesAllowed("Login")
	UID getUserUid(final String sUserName) throws CommonFatalException;

	Boolean isLdapAuthenticationActive();

	Boolean isLdapSynchronizationActive();
	
    String getCurrentApplicationInfoOnServer();
    
	@RolesAllowed("Login")
    void clientWaitingForLogin();
    
	@RolesAllowed("Login")
    void clientReady();

    @RolesAllowed("Login")
	List<MandatorVO> getMandators();

    @RolesAllowed("Login")
	void checkMandatorPermission() throws CommonPermissionException;

    @RolesAllowed("Login")
	List<MandatorVO> getMandatorsByLevel(UID levelUID);
    
    @RolesAllowed("Login")
	List<CollectableField> getUserCommunicationAccounts(UID userUID, Class<? extends RequestContext<?>> requestContextClass);

}
