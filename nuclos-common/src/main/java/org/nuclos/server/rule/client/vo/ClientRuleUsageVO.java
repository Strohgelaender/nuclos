package org.nuclos.server.rule.client.vo;

import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class ClientRuleUsageVO extends NuclosValueObject<UID> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -218695925461399523L;
	private UID boId;
	private UID fieldId;
	private UID processId;
	private UID stateId;
	private String customusage;
	/**
	 * @param boId
	 * @param fieldId
	 * @param processId
	 * @param stateId
	 * @param customusage
	 */
	public ClientRuleUsageVO(NuclosValueObject<UID> nvo, UID boId, UID fieldId, UID processId,
			UID stateId, String customusage) {
		super(nvo);
		this.boId = boId;
		this.fieldId = fieldId;
		this.processId = processId;
		this.stateId = stateId;
		this.customusage = customusage;
	}
	
	public UID getBusinessObjectId() {
		return boId;
	}
	public UID getFieldId() {
		return fieldId;
	}
	public UID getProcessId() {
		return processId;
	}
	public UID getStateId() {
		return stateId;
	}
	public String getCustomusage() {
		return customusage;
	}
	
}
