//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.i18n.language.data;

import java.util.HashMap;
import java.util.List;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityMetaVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.IDataLanguageMap;

public class DataLanguageUtils {

	public static NucletFieldMeta extractFieldValue(FieldMeta fm,
			EntityMetaVO<?> boMeta) {
		NucletFieldMeta voField = new NucletFieldMeta();
		
		String languageFieldDBColumName = NucletFieldMeta.getLanguageFieldDBColumName(fm);
		
	    buildValueListField(fm.getFieldName(), languageFieldDBColumName, fm.getDataType(), fm.getScale(), voField);
	    voField.setUID(extractFieldUID(fm.getUID()));
	    voField.setEntity(NucletEntityMeta.getEntityLanguageUID(boMeta));
	    voField.setForeignEntityField(fm.getForeignEntityField());
	    voField.setForeignEntity(fm.getForeignEntity());
	    //voField.setIndexed(true);
	    return voField;
	}

	public static NucletFieldMeta extractFlaggedFieldValue(NucletFieldMeta fmt, EntityMetaVO<?> boMeta) {
		
		NucletFieldMeta newFlagField = new NucletFieldMeta();
		
		String languageFieldDBColumName = 
				NucletFieldMeta.getLanguageFieldDBColumName(fmt, true);
		
	    buildValueListField(fmt.getFieldName() + "_modified", languageFieldDBColumName, "java.lang.Boolean", 5, newFlagField);
	    newFlagField.setUID(extractFieldUID(fmt.getUID(), true));
	    newFlagField.setEntity(NucletEntityMeta.getEntityLanguageUID(boMeta));
	    newFlagField.setPrimaryKey(extractFieldUID(fmt.getPrimaryKey(), true));
	    if (fmt.isFlagNew()) newFlagField.flagNew();
	    if (fmt.isFlagUpdated()) newFlagField.flagUpdate();
	    if (fmt.isFlagRemoved()) newFlagField.flagRemove();
	    if (fmt.isFlagUnchanged()) newFlagField.reset();
	    
	   return newFlagField;
	}
	
	public static NucletFieldMeta extractFieldValue(NucletFieldMeta fm, EntityMetaVO<?> boMeta) {
		NucletFieldMeta voField = new NucletFieldMeta();
		
		String languageFieldDBColumName = NucletFieldMeta.getLanguageFieldDBColumName(fm);
		
	    buildValueListField(fm.getFieldName(), languageFieldDBColumName, fm.getDataType(), fm.getScale(), voField);
	    voField.setUID(extractFieldUID(fm.getUID()));
	    voField.setEntity(NucletEntityMeta.getEntityLanguageUID(boMeta));
	    voField.setPrimaryKey(extractFieldUID(fm.getPrimaryKey()));
	    voField.setForeignEntityField(fm.getForeignEntityField());
	    voField.setForeignEntity(fm.getForeignEntity());
	    if (fm.isFlagNew()) voField.flagNew();
	    if (fm.isFlagUpdated()) voField.flagUpdate();
	    if (fm.isFlagRemoved()) voField.flagRemove();
	    if (fm.isFlagUnchanged()) voField.reset();
	    return voField;
	}
	
	protected static void buildValueListField(String fieldname, String dbField, String javaType, Integer iScale, NucletFieldMeta<?> voField) {
	    voField.flagNew();
	    voField.setUnique(false);
	    voField.setLogBookTracking(false);
	    voField.setModifiable(false);
	    voField.setSearchable(false);
	    voField.setInsertable(false);
	    voField.setUnique(false);
	    voField.setShowMnemonic(false);
	    voField.setNullable(true);
	    voField.setReadonly(false);
	    voField.setDataType(javaType);
	    voField.setPrecision(null);
	    voField.setScale(iScale);
	    voField.setFieldName(fieldname);
	    voField.setDbColumn(dbField);
    }
	
	public static UID extractForeignEntityReference(UID uid) {
		return new UID("_RE"+ uid.getString());
	}
	
	public static UID extractDataLanguageReference(UID uid) {
		return new UID(uid.getString() + "_RDL");
	}
	
	public static UID extractFieldUID(UID uid) {
		return extractFieldUID(uid, false);
	}
	
	public static UID extractFieldUID(UID uid, boolean createModificationFlag) {
		if (createModificationFlag) {
			return new UID("DL_" + uid.getString() + "MOD");
		} else {
			return new UID("DL_" + uid.getString());
		}
	}
	
	public static UID extractFlaggedFieldFromLangField(UID uid) {
		return new UID(uid.getString() + "MOD");
	}
	
	public static UID extractOriginalFieldUID(UID langUID) {
		return new UID(langUID.getString().replace("DL_", ""));
	}
	
	public static void setLocalizedValueAndValidateMap(IDataLanguageMap mp, Long primKey, EntityMeta<?> meta, UID selectedLanguage, 
			UID primLanguage, UID userLanguage, UID field, List<UID> assignedLanguages, String value) {
		
		UID fieldLangUID = DataLanguageUtils.extractFieldUID(field);
		UID fieldLangFlaggedUID = DataLanguageUtils.extractFlaggedFieldFromLangField(fieldLangUID);
		
		if (selectedLanguage.equals(primLanguage)) {			
			if (mp.getDataLanguage(primLanguage) == null) {
				mp.setDataLanguage(primLanguage, new DataLanguageLocalizedEntityEntry(
						new EntityMetaVO(meta,  true), primKey, primLanguage, new HashMap<UID, Object>()));
			}
			
			DataLanguageLocalizedEntityEntry dlPrimLanguage = 
					mp.getDataLanguage(primLanguage);
			
			dlPrimLanguage.setFieldValue(fieldLangUID, value);
			dlPrimLanguage.setFieldValue(fieldLangFlaggedUID, Boolean.FALSE);
			
			if (dlPrimLanguage.getPrimaryKey() == null) {
				dlPrimLanguage.flagNew();
			} else {
				dlPrimLanguage.flagUpdate();
			}
			
			transferLocalizedValues(assignedLanguages, mp, primKey, meta, field, value);
			
		} else {
			if (mp.getDataLanguage(selectedLanguage) == null) {
				mp.setDataLanguage(selectedLanguage, new DataLanguageLocalizedEntityEntry(
						new EntityMetaVO(meta,  true), primKey, selectedLanguage, new HashMap<UID, Object>()));
			}
			
			DataLanguageLocalizedEntityEntry dlSelectedLanguage = 
					mp.getDataLanguage(selectedLanguage);
			dlSelectedLanguage.setFieldValue(fieldLangUID, value);
			dlSelectedLanguage.setFieldValue(fieldLangFlaggedUID, Boolean.FALSE);
			
			if (dlSelectedLanguage.getPrimaryKey() == null) {
				dlSelectedLanguage.flagNew();
			} else {
				dlSelectedLanguage.flagUpdate();
			}
			
			if (mp.getDataLanguage(primLanguage) == null) {
				mp.setDataLanguage(primLanguage, new DataLanguageLocalizedEntityEntry(
						new EntityMetaVO(meta,  true), primKey, primLanguage, new HashMap<UID, Object>()));
			}
			
			DataLanguageLocalizedEntityEntry dlPrimLanguage = 
					mp.getDataLanguage(primLanguage);
			
			if (dlPrimLanguage.getFieldValue(fieldLangUID) == null || 
					(dlPrimLanguage.getFieldValue(fieldLangUID) != null && 
					(!Boolean.FALSE.equals(dlPrimLanguage.getFieldValue(fieldLangFlaggedUID))))) {
				
				dlPrimLanguage.setFieldValue(fieldLangUID, value);
				dlPrimLanguage.setFieldValue(fieldLangFlaggedUID, Boolean.TRUE);
				
				if (dlPrimLanguage.getPrimaryKey() == null) {
					dlPrimLanguage.flagNew();
				} else {
					dlPrimLanguage.flagUpdate();
				}
				
				transferLocalizedValues(assignedLanguages, mp, primKey, meta, field, value);
			}
			
		}
	}
	
	private static void transferLocalizedValues(List<UID> assignedLanguages, IDataLanguageMap mp, Long primKey, EntityMeta<?> meta, UID field, String value) {
		for (UID curLangUID : assignedLanguages) {
			UID curFieldLangUID = DataLanguageUtils.extractFieldUID(field);
			UID curFieldLangFlaggedUID = DataLanguageUtils.extractFlaggedFieldFromLangField(curFieldLangUID);
			
			if (mp.getDataLanguage(curLangUID) == null) {
				mp.setDataLanguage(curLangUID, new DataLanguageLocalizedEntityEntry(
						new EntityMetaVO(meta,  true), primKey, curLangUID, new HashMap<UID, Object>()));
			}
			
			DataLanguageLocalizedEntityEntry dlCurLanguage = mp.getDataLanguage(curLangUID);			
			if (!Boolean.FALSE.equals(dlCurLanguage.getFieldValue(curFieldLangFlaggedUID))) {
				dlCurLanguage.setFieldValue(curFieldLangUID, value);
				dlCurLanguage.setFieldValue(curFieldLangFlaggedUID, Boolean.TRUE);
				if (dlCurLanguage.getPrimaryKey() == null) {
					dlCurLanguage.flagNew();
				} else {
					dlCurLanguage.flagUpdate();
				}
			}
		}
	}
}
