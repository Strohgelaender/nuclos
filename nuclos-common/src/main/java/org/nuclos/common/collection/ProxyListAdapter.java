package org.nuclos.common.collection;

import java.util.Collection;

import javax.swing.event.ChangeListener;

import org.nuclos.common.dal.vo.HasPrimaryKey;
import org.nuclos.server.genericobject.ProxyList;

public abstract class ProxyListAdapter<T,W  extends HasPrimaryKey<T>,E extends HasPrimaryKey<T>> extends ListAdapter<W, E> implements ProxyList<T,E> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4084340165391334943L;

	protected ProxyListAdapter(ProxyList<T,W> lstAdaptee) {
		super(lstAdaptee);
	}
	
	protected ProxyList<T,W> plAdaptee() {
		return (ProxyList<T,W>) adaptee();
	}

	@Override
	public void fetchDataIfNecessary(int iIndex, ChangeListener changelistener) {
		plAdaptee().fetchDataIfNecessary(iIndex, changelistener);
	}

	@Override
	public int getLastIndexRead() {
		return plAdaptee().getLastIndexRead();
	}

	@Override
	public boolean hasObjectBeenReadForIndex(int index) {
		return plAdaptee().hasObjectBeenReadForIndex(index);
	}

	@Override
	public int getIndexById(T oId) {
		return plAdaptee().getIndexById(oId);
	}

	@Override
	public E getRaw(int index) {
		return wrap(plAdaptee().getRaw(index));
	}

	@Override
	public E getRawById(T id) {
		return wrap(plAdaptee().getRawById(id));
	}

	@Override
	public int indexOf(E element) {
		return plAdaptee().indexOf(unwrap(element));
	}

	@Override
	public T getIdByIndex(int index) {
		return plAdaptee().getIdByIndex(index);
	}

	@Override
	public Collection<T> getAllLoadedIds() {
		return plAdaptee().getAllLoadedIds();
	}

}
