//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.http;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpClientConnection;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultRoutePlanner;
import org.apache.http.impl.conn.DefaultSchemePortResolver;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.log4j.Logger;
import org.nuclos.common.tls.CustomSecureProtocolSocketFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;

/**
 * A Spring BeanFactory that creates a customized (apache httpcomponents) HttpClient instance.
 * <p>
 * The created instance uses the {@link CustomSecureProtocolSocketFactory} for SSL/TLS connections.
 * In addition the http(s) pool settings could be tweaked.
 * </p><p>
 * This instance is used for both remote and JMS calls.
 * </p>
 *
 * @author Thomas Pasch
 */
public class NuclosHttpClientFactory implements FactoryBean<HttpClient>, DisposableBean, Closeable {

	private static final Logger LOG = Logger.getLogger(NuclosHttpClientFactory.class);

	private static final int DEFAULT_MAX_TOTAL_CONNECTIONS = 300;
	private static final int DEFAULT_MAX_CONNECTIONS_PER_ROUTE = 150;
	private static final int RETRY_HTTP_REQUESTS = 90;

	private static NuclosHttpClientFactory INSTANCE;

	private final CloseableHttpClient httpClient;

	private final HttpClientConnectionManager connectionManager;
	private final IdleConnectionMonitorThread idleConnectionMonitorThread;

	public NuclosHttpClientFactory() {
		if (INSTANCE == null) {
			INSTANCE = this;
		}

		LOG.info("Register CustomSecureProtocolSocketFactory for HTTPS (modern apache http component)");
		Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
				.register("http", PlainConnectionSocketFactory.getSocketFactory())
				.register("https", new CustomSecureProtocolSocketFactory())
				.build();

		connectionManager = new PoolingHttpClientConnectionManager(registry);

		// A custom RequestExecutor that always uses the current RequestConfig from RequestConfigHolder.
		final HttpRequestExecutor customRequestExecutor = new HttpRequestExecutor() {
			@Override
			public HttpResponse execute(final HttpRequest request, final HttpClientConnection conn, final HttpContext context) throws IOException, HttpException {
				final RequestConfig requestConfig = RequestConfigHolder.getInstance().getCurrentConfig();
				final HttpUriRequest requestWithConfig = RequestBuilder.copy(request).setConfig(requestConfig).build();

				return super.execute(requestWithConfig, conn, context);
			}
		};

		httpClient = HttpClientBuilder
				.create()
				.setRequestExecutor(customRequestExecutor)
				.setConnectionManager(connectionManager)
				.setRetryHandler((exception, executionCount, context) -> {
					if (executionCount > RETRY_HTTP_REQUESTS) {
						LOG.warn("Maximum tries reached for client http pool ");
						return false;
					}
					if (exception instanceof org.apache.http.NoHttpResponseException) {
						LOG.warn("No response from server on " + executionCount + " call");
						return true;
					}
					return false;
				})
				.setMaxConnTotal(DEFAULT_MAX_TOTAL_CONNECTIONS)
				.setMaxConnPerRoute(DEFAULT_MAX_CONNECTIONS_PER_ROUTE)
				.setRoutePlanner(new DefaultRoutePlanner(DefaultSchemePortResolver.INSTANCE) {
					@Override
					public HttpRoute determineRoute(final HttpHost host, final HttpRequest request, final HttpContext context) throws HttpException {
						URI location;
						try {
							location = new URI(host.getSchemeName(), host.getHostName(), null, null);
							System.setProperty("java.net.useSystemProxies", "true");
							List<Proxy> proxies = ProxySelector.getDefault().select(location);
							for (Proxy proxy : proxies) {
								if (proxy.type() == Type.HTTP) {
									InetSocketAddress addr = (InetSocketAddress) proxy.address();

									if (addr == null) {
										LOG.warn("HTTP Proxy address is null");
									} else {
										HttpHost proxyHost = new HttpHost(addr.getHostName(), addr.getPort());

										LOG.debug("Using HTTP proxy " + proxyHost + " for target " + location);

										final boolean secure = "https".equalsIgnoreCase(location.getScheme());
										return new HttpRoute(host, null, proxyHost, secure);
									}
								}
							}
						} catch (URISyntaxException e) {
							LOG.warn(e.getMessage(), e);
						}

						return super.determineRoute(host, request, context);
					}
				})
				.build();

		idleConnectionMonitorThread = new IdleConnectionMonitorThread(this);
	}

	HttpClientConnectionManager getConnectionManager() {
		return connectionManager;
	}

	@Deprecated
	public static NuclosHttpClientFactory getInstance() {
		return INSTANCE;
	}

	@Override
	public HttpClient getObject() {
		if (!idleConnectionMonitorThread.isAlive()) {
			idleConnectionMonitorThread.start();
		}

		return httpClient;
	}

	@Override
	public Class<?> getObjectType() {
		return HttpClient.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void destroy() {
		INSTANCE = null;
		close();
	}

	@Override
	public void close() {
		idleConnectionMonitorThread.shutdown();

		try {
			httpClient.close();
		} catch (IOException e) {
			LOG.warn(e.getMessage(), e);
		}
	}
}
