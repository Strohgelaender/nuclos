//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.common;

import java.awt.Rectangle;

import javax.swing.JFrame;

import org.nuclos.common.WorkspaceDescription2.Desktop;
import org.nuclos.common.WorkspaceDescription2.EntityPreferences;
import org.nuclos.common.WorkspaceDescription2.MenuButton;
import org.nuclos.common.WorkspaceDescription2.MenuItem;
import org.nuclos.common.preferences.ColumnPreferences;
import org.nuclos.common.preferences.ColumnSortingPreferences;

public class WorkspaceDescriptionDefaultsFactory {
	
	public static WorkspaceDescription2 createOldMdiStyle() {
		WorkspaceDescription2 result = new WorkspaceDescription2();
		
		/**
		 * TABBED
		 */
		WorkspaceDescription2.Tabbed wdTabbedExplorer = new WorkspaceDescription2.Tabbed();
		WorkspaceDescription2.Tabbed wdTabbedTask = new WorkspaceDescription2.Tabbed();
		WorkspaceDescription2.Tabbed wdTabbedHome = new WorkspaceDescription2.Tabbed();
		wdTabbedExplorer.setHomeTree(true);
		wdTabbedExplorer.setAlwaysHideHistory(true);
		wdTabbedExplorer.setAlwaysHideBookmark(true);
		wdTabbedTask.setAlwaysHideStartmenu(true);
		wdTabbedTask.setAlwaysHideHistory(true);
		wdTabbedTask.setAlwaysHideBookmark(true);
		wdTabbedHome.setHome(true);
		wdTabbedHome.setAlwaysHideStartmenu(true);
		
		/**
		 * SPLIT
		 */
		WorkspaceDescription2.Split wdSplitTaskHome = new WorkspaceDescription2.Split();
		WorkspaceDescription2.Split wdSplitExplorerOther = new WorkspaceDescription2.Split();
		wdSplitTaskHome.getContentA().setContent(wdTabbedTask);
		wdSplitTaskHome.getContentB().setContent(wdTabbedHome);
		wdSplitTaskHome.setHorizontal(false);
		wdSplitTaskHome.setPosition(160);
		wdSplitExplorerOther.getContentA().setContent(wdTabbedExplorer);
		wdSplitExplorerOther.getContentB().setContent(wdSplitTaskHome);
		wdSplitExplorerOther.setHorizontal(true);
		wdSplitExplorerOther.setPosition(200);
		
		/**
		 * FRAME
		 */
		WorkspaceDescription2.Frame wdFrame = new WorkspaceDescription2.Frame();
		wdFrame.getContent().setContent(wdSplitExplorerOther);
		wdFrame.setMainFrame(true);
		wdFrame.setNormalBounds(new Rectangle(40, 40, 960, 700));
		wdFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		result.addFrame(wdFrame);

		return result;
	}
	
	public static WorkspaceDescription2 createMaintenanceModeStyle() {
		WorkspaceDescription2 result = new WorkspaceDescription2();
		
		/**
		 * TABBED
		 */
		WorkspaceDescription2.Tabbed wdTabbedExplorer = new WorkspaceDescription2.Tabbed();
		WorkspaceDescription2.Tabbed wdTabbedTask = new WorkspaceDescription2.Tabbed();
		WorkspaceDescription2.Tabbed wdTabbedHome = new WorkspaceDescription2.Tabbed();
		wdTabbedExplorer.setHomeTree(true);
		wdTabbedExplorer.setAlwaysHideHistory(true);
		wdTabbedExplorer.setAlwaysHideBookmark(true);
		wdTabbedExplorer.setDesktop(new WorkspaceDescription2.Desktop());
		wdTabbedExplorer.setDesktopActive(true);
		wdTabbedExplorer.getDesktop().setHideToolBar(true);
		wdTabbedTask.setAlwaysHideStartmenu(true);
		wdTabbedTask.setAlwaysHideHistory(true);
		wdTabbedTask.setAlwaysHideBookmark(true);
		wdTabbedHome.setHome(true);
		wdTabbedHome.setAlwaysHideStartmenu(true);
		
		MenuButton menuButton = new MenuButton();
		menuButton.setNuclosResource("org.nuclos.client.resource.icon.main-blue.nuclet.png");
		
		WorkspaceDescription2.Action menuAction = new WorkspaceDescription2.Action();
		menuAction.setAction("nuclosGenericEntityAction");
		menuAction.putStringParameter("entity", RigidE._Nuclet.UID.getString());
		
		menuButton.setMenuAction(menuAction);
		
		MenuItem menuItem = new MenuItem();
		menuItem.setMenuAction(menuAction);
		menuButton.addMenuItem(menuItem);
		
		WorkspaceDescription2.Desktop desktop = new WorkspaceDescription2.Desktop();
		desktop.setLayout(Desktop.LAYOUT_WRAP);
		desktop.setHorizontalGap(10);
		desktop.setVerticalGap(10);
		desktop.setMenuItemTextHorizontalAlignment(WorkspaceDescription2.Desktop.HORIZONTAL_ALIGNMENT_LEFT);
		desktop.setStaticMenu(true);
		desktop.addDesktopItem(menuButton);

		wdTabbedHome.setDesktop(desktop);
		wdTabbedHome.setDesktopActive(true);
		
		
		/**
		 * SPLIT
		 */
		WorkspaceDescription2.Split wdSplitExplorerOther = new WorkspaceDescription2.Split();
		wdSplitExplorerOther.getContentA().setContent(wdTabbedExplorer);
		wdSplitExplorerOther.getContentB().setContent(wdTabbedHome);
		wdSplitExplorerOther.setHorizontal(true);
		wdSplitExplorerOther.setPosition(200);
		
		/**
		 * FRAME
		 */
		WorkspaceDescription2.Frame wdFrame = new WorkspaceDescription2.Frame();
		wdFrame.getContent().setContent(wdSplitExplorerOther);
		wdFrame.setMainFrame(true);
		wdFrame.setNormalBounds(new Rectangle(40, 40, 960, 700));
		wdFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		result.addFrame(wdFrame);
		
		return result;
	}
	
	
	private static EntityPreferences getEntity(WorkspaceDescription2 wd, EntityMeta<?> entity) {
		EntityPreferences result = wd.getEntityPreferences(entity.getUID());
		return result;
	}

	private static ColumnPreferences getColumn(UID column, int width) {
		return getColumn(column, width, false);
	}
	
	private static ColumnPreferences getFixedColumn(UID column, int width) {
		return getColumn(column, width, true);
	}
	
	private static ColumnPreferences getColumn(UID column, int width, boolean fixed) {
		ColumnPreferences result = new ColumnPreferences();
		result.setColumn(column);
		result.setWidth(width);
		result.setFixed(fixed);
		return result;
	}
	
	private static ColumnSortingPreferences getSorting(UID column) {
		ColumnSortingPreferences result = new ColumnSortingPreferences();
		//TODO MULTINUCLET
		//result.setColumn("name");
		result.setColumn(E.USER.name.getUID());
		result.setAsc(true);
		return result;
	}
	
}
