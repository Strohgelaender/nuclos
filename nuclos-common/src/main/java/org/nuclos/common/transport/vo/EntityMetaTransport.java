//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.transport.vo;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.TranslationVO;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;


/**
 * Entity meta data vo
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@novabit.de">Maik.Stueker</a>
 * @version 01.00.00
 */
public class EntityMetaTransport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5764801470477931134L;

	private NucletEntityMeta voEntityMetaVO;
	
	private UID mandatorInitialFill;

	private List<TranslationVO> lstTranslation;
	private List<EntityTreeViewVO> lstTreeView;

	private Collection<EntityObjectVO<UID>> processes;

	/**
	 * Key = menu entry
	 * Value = localeresources (Key=locale, Value=translation)
	 */
	private Map<EntityObjectVO<UID>, Map<String, String>> menus;

	public NucletEntityMeta getEntityMetaVO() {
		return voEntityMetaVO;
	}

	public void setEntityMetaVO(NucletEntityMeta voEntityMetaVO) {
		this.voEntityMetaVO = voEntityMetaVO;
	}

	public List<TranslationVO> getTranslation() {
		return lstTranslation;
	}

	public void setTranslation(List<TranslationVO> lstTranslation) {
		this.lstTranslation = lstTranslation;
	}

	public List<EntityTreeViewVO> getTreeView() {
		return lstTreeView;
	}

	public void setTreeView(List<EntityTreeViewVO> lstTreeView) {
		this.lstTreeView = lstTreeView;
	}

	public Collection<EntityObjectVO<UID>> getProcesses() {
		return processes;
	}

	public void setProcesses(Collection<EntityObjectVO<UID>> processes) {
		this.processes = processes;
	}

	public Map<EntityObjectVO<UID>, Map<String, String>> getMenus() {
		return menus;
	}

	public void setMenus(Map<EntityObjectVO<UID>, Map<String, String>> menus) {
		this.menus = menus;
	}

	public final UID getMandatorInitialFill() {
		return mandatorInitialFill;
	}

	public final void setMandatorInitialFill(UID mandatorInitialFill) {
		this.mandatorInitialFill = mandatorInitialFill;
	}
	
	
}
