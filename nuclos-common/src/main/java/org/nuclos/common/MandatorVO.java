package org.nuclos.common;

import java.io.Serializable;

import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;


public class MandatorVO extends NuclosValueObject<UID> implements Serializable, Comparable<MandatorVO> {
	/**
	 *
	 */
	private static final long serialVersionUID = 3131200026335127052L;

	public static final String PATH_SEPARATOR = "/";

	private UID uid;
	private String name;
	private String path;
	private UID parent;
	private UID level;
	private String color;
	private MandatorVO parentVO;

	public MandatorVO(String name)
	{
		this.name=name;
		this.path=name;
	}
	public MandatorVO(String name,  MandatorVO parent)
	{
		this.name=name;
		this.parent=parent.getUID();
		this.parentVO=parent;

	}
	public MandatorVO(UID uid, String name, String path, UID parent, UID level, String color) {
		super();
		this.uid = uid;
		this.name = name;
		this.parent = parent;
		this.path = path;
		this.level = level;
		this.color = color;
	}

	public MandatorVO(MasterDataVO<UID> mdvo) {
		super(mdvo.getPrimaryKey(), mdvo.getCreatedAt(), mdvo.getCreatedBy(), mdvo.getChangedAt(), mdvo.getChangedBy(), mdvo.getVersion());
		this.name = mdvo.getFieldValue(E.MANDATOR.name);
		this.parent = mdvo.getFieldUid(E.MANDATOR.parentMandator);
		this.level= mdvo.getFieldUid(E.MANDATOR.level);
		this.path= mdvo.getFieldValue(E.MANDATOR.path);
		this.uid=E.MANDATOR.getUID();
		this.color= mdvo.getFieldValue(E.MANDATOR.color);
	}

	public UID getUID() {
		return uid;
	}
	public String getName() {
		return name;
	}
	public String getPath() {
		return path;
	}
	public UID getParentUID() {
		return parent;
	}
	public UID getLevelUID() {
		return level;
	}
	public String getColor() {
		return color;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj instanceof MandatorVO && uid != null) {
			return uid.equals(((MandatorVO)obj).getUID());
		}
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		if (uid != null) {
			return uid.hashCode();
		}
		return super.hashCode();
	}

	@Override
	public String toString() {
		return path==null?(name==null?uid.toString():name):path;
	}

	@Override
	public int compareTo(MandatorVO o) {
		return StringUtils.compareIgnoreCase(path, o.path);
	}

	public static class ToUid implements Transformer<MandatorVO, UID> {
		@Override
		public UID transform(MandatorVO i) {
			return i.getUID();
		}
	}

	public MasterDataVO<UID> toMasterDataVO() {
		MasterDataVO<UID> mdvo = new MasterDataVO<UID>(E.MANDATOR.getUID(), getId(),
				getCreatedAt(), getCreatedBy(), getChangedAt(), getChangedBy(), getVersion(), null, null, null, false);
		mdvo.getEntityObject().setComplete(true);

		mdvo.setFieldValue(E.MANDATOR.name, name);
		if(parentVO!=null)
			mdvo.setFieldUid(E.MANDATOR.parentMandator,parentVO.getPrimaryKey());

		String sPath = "";
		if (mdvo.getFieldUid(E.MANDATOR.parentMandator) != null) {
			sPath = parentVO.getPath() + MandatorVO.PATH_SEPARATOR;
		}
		sPath = sPath + mdvo.getFieldValue(E.MANDATOR.name);
		mdvo.setFieldValue(E.MANDATOR.path, sPath);

		mdvo.setFieldUid(E.MANDATOR.level, level);
		mdvo.setFieldValue(E.MANDATOR.color, color);

		return mdvo;
	}

}
