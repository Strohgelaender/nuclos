//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.mail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.common.NuclosFile;

public class NuclosMail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7379242483537376850L;
	private String from;
	private String subject;
	private String message;
	private String type;
	private String charset;
	
	private List<String> to = new ArrayList<String>();
	private List<String> toCC = new ArrayList<String>();
	private List<String> toBCC = new ArrayList<String>();
	
	private String replyTo;
	
	private Map<String, String> headers = new HashMap<String, String>();
	
	private final Collection<NuclosFile> lstAttachment = new ArrayList<NuclosFile>(1);

	public NuclosMail() {
		this.charset = "UTF-8";
		this.type = "text/plain";
	}

	public NuclosMail(org.nuclos.api.mail.NuclosMail apiMail) {
		this(apiMail.getFrom(), apiMail.getRecipients(), apiMail.getSubject(), apiMail.getMessage(), (Collection<NuclosFile>)(Object)apiMail.getAttachments());

		this.type = apiMail.getMailType();
		this.charset = apiMail.getMailCharset();
		this.toCC = apiMail.getRecipientsCC();
		this.toBCC = apiMail.getRecipientsBCC();

		this.replyTo = apiMail.getReplyTo();
		this.headers = apiMail.getHeaders();
	}

	public NuclosMail(String to, String subject, String message) {
		this(null, to, subject, message);
	}

	public NuclosMail(String from, String to, String subject, String message) {
		this(from, to, subject, message, (NuclosFile)null);
	}

	public NuclosMail(String to, String subject, String message, Collection<NuclosFile> attachments) {
		this(null, Collections.singletonList(to), subject, message, attachments);
	}

	public NuclosMail(String to, String subject, String message, NuclosFile attachment) {
		this(null, to, subject, message, attachment);
	}

	public NuclosMail(String from, String to, String subject, String message, NuclosFile attachment) {
		this(from, Collections.singletonList(to), subject, message, attachment != null ? Collections.singleton(attachment) : null);
	}

	private NuclosMail(String from, List<String> tos, String subject, String message, Collection<NuclosFile> attachments) {
		this.from = from;
		this.addRecipients(tos);
		this.subject = subject;
		this.message = message;
		this.addAttachments(attachments);
	}
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
	
	public String getHeader(final String header) {
		return headers.get(header);
	}

	public void setHeader(final String header, final String value) {
		headers.put(header, value);
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Collection<NuclosFile> getAttachments() {
		return lstAttachment;
	}

	public String getTo() {
		return this.getRecipients().toArray(new String[this.getRecipients().size()]).toString();
	}

	public void setTo(String to) {
		this.addRecipients(Collections.singletonList(to));
	}

	public void addAttachments(Collection<NuclosFile> files) {
		if (files != null) {
			this.lstAttachment.addAll(files);			
		}
	}

	public void addRecipients(List<String> emailRecipients) {
		if (emailRecipients != null) {
			for (String emailRecipient : emailRecipients) {
				if (emailRecipient != null) {
					String[] split = emailRecipient.split(";");
					for (String add : split) {
						this.to.add(add.trim());				
					}
				}
			}
		}
	}

	public List<String> getRecipients() {
		return this.to;
	}
	
	
	public void addRecipientCC(String emailRecipientCC) {
		if (emailRecipientCC != null) {
			String[] split = emailRecipientCC.split(";");
			for (String add : split) {
				this.toCC.add(add.trim());				
			}
		}
	}

	public List<String> getRecipientsCC() {
		return this.toCC;
	}

	public void addRecipientBCC(String emailRecipientBCC) {
		if (emailRecipientBCC != null) {
			String[] split = emailRecipientBCC.split(";");
			for (String add : split) {
				this.toBCC.add(add.trim());				
			}
		}
	}

	public List<String> getRecipientsBCC() {
		return this.toBCC;
	}
	
	public void setReplyTo(String emailReplyTo) {
		this.replyTo = emailReplyTo;
	}
	
	public String getReplyTo() {
		return this.replyTo;
	}

	public String getType() {
		return type;
	}

	public String getCharset() {
		return charset;
	}
	
	
}
