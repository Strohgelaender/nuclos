//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.statemodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.TransformerUtils;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.statemodel.valueobject.StateTransitionVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * Simple transfer class representing a state model for a module and process
 * (thus: usage criteria), bound to the current user.
 * 
 * This includes all information needed for one state-based process, i.e. the
 * complete state model, the resource ids, the total transitions and a set
 * of possible user transitions.
 * <p>
 * TODO: This should be cached and NOT be transfered between server and client.
 * Instead, it should be re-constructed on the client side from information
 * available from (client) caches. (tp)
 *
 * TODO: Confusingly, this can contain the statuses of different status models.
 */
public class Statemodel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1701690512875791316L;
	private UsageCriteria	                            usageCriteria;
	private Set<StateVO>	                            allStates;
	private UID	                                    initialState;
	private Map<UID, Collection<StateTransitionVO>>	stateTransitions;
	private Map<UID, String>	                    labelResourceSIDs;
	private Map<UID, String>	                    desriptionResourceSIDs;
	private Set<UID>              			        userTransitions;

	private Map<UID, StateVO>	                    _stateLookup;

	public Statemodel(UsageCriteria usageCriteria) {
		this.usageCriteria = usageCriteria;
		stateTransitions = new HashMap<UID, Collection<StateTransitionVO>>();
		labelResourceSIDs = new HashMap<UID, String>();
		desriptionResourceSIDs = new HashMap<UID, String>();
	}

	public boolean isComplete() {
		return usageCriteria != null
				&& allStates != null
				&& initialState != null
				&& stateTransitions.size() == allStates.size()
				&& labelResourceSIDs.size() == allStates.size()
				&& desriptionResourceSIDs.size() == allStates.size()
				&& userTransitions != null;
	}

	public UsageCriteria getUsageCriteria() {
		return usageCriteria;
	}

	public void setUsageCriteria(UsageCriteria usageCriteria) {
		this.usageCriteria = usageCriteria;
	}

	public Set<StateVO> getAllStates() {
		return allStates;
	}

	public void setAllStates(Collection<StateVO> allStates) {
		this.allStates = new HashSet<StateVO>(allStates);
	}

	public Set<UID> getStateUIDs() {
		return CollectionUtils.transformIntoSet(allStates, new NuclosValueObject.GetId());
	}

	public UID getInitialState() {
		return initialState;
	}

	public void setInitialState(UID initialState) {
		this.initialState = initialState;
	}

	public void setTransitionsForState(UID state, Collection<StateTransitionVO> transitions) {
		stateTransitions.put(state, transitions);
	}

	public void setResourceSIDsForState(UID state, String labelResourceSId, String descriptionResourceSId) {
		labelResourceSIDs.put(state, labelResourceSId);
		desriptionResourceSIDs.put(state, descriptionResourceSId);
	}

	public Map<UID, String> getLabelResourceSIDs() {
		return labelResourceSIDs;
	}

	public Map<UID, String> getDesriptionResourceSIDs() {
		return desriptionResourceSIDs;
	}

	public void setUserTransitionIDs(Collection<UID> userTransitions) {
		this.userTransitions = new HashSet<UID>(userTransitions);
	}

	public StateVO getState(UID state) {
		return getStateLookup().get(state);
	}

	private Map<UID, StateVO> getStateLookup() {
		if(_stateLookup == null)
			_stateLookup = CollectionUtils.generateLookupMap(allStates, TransformerUtils.<UID, StateVO>getPk(UID.class));
		return _stateLookup;
	}

	public List<StateVO> getSubsequentStates(UID state, final boolean includeAutomatic) {
		ArrayList<StateVO> res = new ArrayList<StateVO>();
		if (state == null) {
			res.add(getStateLookup().get(initialState));
		}
		else {
			Collection<StateTransitionVO> outgoingTransitions = stateTransitions.get(state);
			if (outgoingTransitions != null) {
				Collection<StateTransitionVO> filteredTransitions = CollectionUtils.applyFilter(outgoingTransitions,
					new Predicate<StateTransitionVO>() {
						@Override
						public boolean evaluate(StateTransitionVO t) {
							return includeAutomatic || !t.isAutomatic();
						}
					});
				
				for (StateTransitionVO trans : filteredTransitions) {
					if (userTransitions.contains(trans.getId()) || (includeAutomatic && trans.isAutomatic())) {
						StateVO stateVO = getStateLookup().get(trans.getStateTargetUID());
						if (trans.isNonstop()) {
							stateVO.addNonstopSource(state);
						}
						res.add(stateVO);						
					}
				}
			}
		}

		Collections.sort(res, new Comparator<StateVO>() {
			@Override
			public int compare(StateVO statevo1, StateVO statevo2) {
				return statevo1.getStatename(Locale.ENGLISH).compareTo(statevo2.getStatename(Locale.ENGLISH));
			}
		});
		return res;
	}

	public List<StateVO> getDefaultStatePath() {
		List<StateVO> result = new LinkedList<StateVO>();

		// validate if there is an valid path from an start to an end state.
		List<StateTransitionVO> transitionVOs = new LinkedList<StateTransitionVO>();
		for (Collection<StateTransitionVO> transitions : stateTransitions.values()) {
			transitionVOs.addAll(transitions);
		}
		if (CollectionUtils.indexOfFirst(transitionVOs, new Predicate<StateTransitionVO>() {
			@Override public boolean evaluate(StateTransitionVO t) { return t.isDefault(); }
		}) != -1) {
			// find start transition - there has to be one because of the checks before.
			StateTransitionVO startTransition = CollectionUtils.findFirst(transitionVOs, new Predicate<StateTransitionVO>() {
				@Override public boolean evaluate(StateTransitionVO t) {
					return t.getStateSourceUID().equals(initialState) && t.isDefault();
				}
			});
			result.add(getStateLookup().get(initialState));
			if (startTransition != null) {
				result.add(getStateLookup().get(startTransition.getStateTargetUID()));
			}

			List<UID> checkedStates = new LinkedList<UID>();

			// finde alle trans die als source den end der letzten haben.
			if (startTransition != null) {
				UID subsequentState = startTransition.getStateTargetUID();
				while (subsequentState != null) {
					if (checkedStates.contains(subsequentState)) {
						break;
					}
					final UID subsequentStateSource = subsequentState;
					StateTransitionVO subsequentTransition = CollectionUtils.findFirst(transitionVOs, new Predicate<StateTransitionVO>() {
						@Override public boolean evaluate(StateTransitionVO t) { return t.getStateSourceUID().equals(subsequentStateSource) && t.isDefault() == true; }
					});

					if (subsequentTransition == null) {
						break;
					}

					// iterate next.
					checkedStates.add(subsequentState);
					subsequentState = subsequentTransition.getStateTargetUID();
					StateVO state = getStateLookup().get(subsequentTransition.getStateTargetUID());
					state.setFromAutomatic(subsequentTransition.isAutomatic());
					if (subsequentTransition.isNonstop()) {
						state.addNonstopSource(subsequentStateSource);
					}
					result.add(state);
				}
			}
		}
		return result;
	}

	public UID getStateUIDFromNumeral(Integer numeral) {
		for (Iterator iterator = allStates.iterator(); iterator.hasNext();) {
			StateVO state = (StateVO) iterator.next();
			if (state.getNumeral().equals(numeral))
				return state.getId();
		}
		return null;
	}
	
	public List<UID> isStateReachableInDefaultPathByNumeral(final Integer stateCurrent, StateVO stateToReach) {
		return isStateReachableInDefaultPath(getStateUIDFromNumeral(stateCurrent), stateToReach);
	}   
	
	public List<UID> isStateReachableInDefaultPath(final UID stateCurrent, StateVO stateToReach) {
		List<UID> result = new LinkedList<UID>();

		// state to reach is not in a default path
		List<StateVO> statesDefaultPath = getDefaultStatePath();
		if (!statesDefaultPath.contains(stateToReach)) {
			return result;
		}

		List<StateTransitionVO> transitionVOs = new LinkedList<StateTransitionVO>();
		for (Collection<StateTransitionVO> transitions : stateTransitions.values()) {
			transitionVOs.addAll(transitions);
		}

		// we are not at defaultpath, but we can reach default path within one step.
		for (StateVO stateVO : getSubsequentStates(stateCurrent, false)) {
			if (stateVO.getId().equals(stateToReach.getId())) {
				result.add(stateToReach.getId());
				return result;
			}
		}

		// find start transition
		StateTransitionVO startTransition = CollectionUtils.findFirst(transitionVOs, new Predicate<StateTransitionVO>() {
			@Override public boolean evaluate(StateTransitionVO t) {
				return t.isDefault() && !t.isAutomatic() && t.getStateSourceUID().equals(stateCurrent);
			}
		});

		if (startTransition == null)
			return result;

		List<UID> checkedStates = new LinkedList<UID>();
		UID subsequentState = startTransition.getStateTargetUID();
		if (!startTransition.isAutomatic() && !result.contains(subsequentState)
				&& userTransitions.contains(startTransition.getId())) {
			result.add(subsequentState);
		}
		while (subsequentState != null) {			
			if (subsequentState.equals(stateToReach.getId())) {
				return result;
			}

			final UID subsequentStateSource = subsequentState;
			StateTransitionVO subsequentTransition = CollectionUtils.findFirst(transitionVOs, new Predicate<StateTransitionVO>() {
				@Override public boolean evaluate(StateTransitionVO t) { return t.isDefault() && !t.isAutomatic() && t.getStateSourceUID().equals(subsequentStateSource); }
			});

			if (subsequentTransition == null) {
				break;
			}

			// add path.
			if (!subsequentTransition.isAutomatic() && !result.contains(subsequentState)
					&& userTransitions.contains(subsequentTransition.getId())) {
				result.add(subsequentState);
			}

			if (checkedStates.contains(subsequentTransition.getId())) {
				break;
			}

			if (subsequentState.equals(stateToReach.getId())) {
				return result;
			}

			// iterate next.
			checkedStates.add(subsequentTransition.getId());
			subsequentState = subsequentTransition.getStateTargetUID();
			if (subsequentState.equals(stateToReach.getId())) {
				if (!result.contains(subsequentState)
						&& userTransitions.contains(subsequentTransition.getId()))
					result.add(subsequentState);
				return result;
			}
		}

		return new LinkedList<UID>();
	}
	
	public Map<UID, Collection<StateTransitionVO>> getStateTransitions() {
		return stateTransitions;
	}
}
