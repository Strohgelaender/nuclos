package org.nuclos.common.valueobject;

import java.io.Serializable;

import org.nuclos.common.E;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.security.UserVO;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class MandatorUserVO extends NuclosValueObject<UID> implements Serializable, Comparable<MandatorUserVO> {

    /**
     *
     */
    private static final long serialVersionUID = 3131200026335127052L;


    private MandatorVO mandatorVO;
    private UserVO userVO;
    private UID uid;

    public MandatorUserVO(MandatorVO mandatorVO, UserVO userVO) {
        this.mandatorVO = mandatorVO;
        this.userVO = userVO;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj instanceof MandatorUserVO && uid != null) {
            return uid.equals(((MandatorUserVO) obj).getUID());
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (uid != null) {
            return uid.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public int compareTo(MandatorUserVO o) {
        return o.compareTo(this);
    }

    public UID getUID() {
        return uid;
    }


    public static class ToUid implements Transformer<MandatorUserVO, UID> {
        @Override
        public UID transform(MandatorUserVO i) {
            return i.getUID();
        }
    }

    public MasterDataVO<UID> toMasterDataVO() {
        MasterDataVO<UID> mdvo = new MasterDataVO<UID>(E.MANDATOR_USER.getUID(), getId(),
                getCreatedAt(), getCreatedBy(), getChangedAt(), getChangedBy(), getVersion(), null, null, null, false);
        mdvo.getEntityObject().setComplete(true);

        mdvo.setFieldUid(E.MANDATOR_USER.mandator, mandatorVO.getPrimaryKey());
        mdvo.setFieldUid(E.MANDATOR_USER.user, userVO.getPrimaryKey());

        return mdvo;
    }
}