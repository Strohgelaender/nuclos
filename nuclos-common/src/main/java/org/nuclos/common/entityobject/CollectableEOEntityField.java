//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.entityobject;

import java.util.prefs.Preferences;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.masterdata.EnumeratedDefaultValueProvider;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * <p>
 * TODO: Consider org.nuclos.client.common.CollectableEntityFieldPreferencesUtil
 * to write to {@link Preferences}.
 */
public class CollectableEOEntityField extends AbstractCollectableEntityField {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2140165688879812230L;


	private static final Logger LOG = Logger.getLogger(CollectableEOEntityField.class);


	private final FieldMeta<?> efMeta;

	private String label;

	public CollectableEOEntityField(FieldMeta<?> efMeta) {
		if (efMeta == null) {
			throw new NullArgumentException("efMeta");
		}
		this.efMeta = efMeta;
	}

	@Override
	public CollectableField getDefault() {
		try {
			final String sDefault = this.efMeta.getDefaultValue();
			if(sDefault == null || sDefault.length() == 0) {
				return super.getDefault();
			}
			else {
				Object o = SpringApplicationContextHolder.getBean("enumeratedDefaultValueProvider");
				if (o != null && o instanceof EnumeratedDefaultValueProvider) {
					return ((EnumeratedDefaultValueProvider)o).getDefaultValue(this.efMeta);
				}
			}
		}
		catch(Exception e) {
			// on exception return super.getDefault()
			LOG.info("getDefault: " + e);
			return super.getDefault();
		}

		return super.getDefault();
	}

	@Override
	public String getDescription() {
		return SpringLocaleDelegate.getInstance().getTextFallback(
				efMeta.getLocaleResourceIdForDescription(), efMeta.getFallbackLabel());
	}

	@Override
	public int getFieldType() {
		return isReferencing() ? CollectableEntityField.TYPE_VALUEIDFIELD: CollectableEntityField.TYPE_VALUEFIELD;
	}

	@Override
	public String getFormatInput() {
		return efMeta.getFormatInput();
	}

	@Override
	public String getFormatOutput() {
		return efMeta.getFormatOutput();
	}

	@Override
	public Class<?> getJavaClass() {
		try {
			return LangUtils.getClassLoaderThatWorksForWebStart().loadClass(efMeta.getDataType());
		}
		catch(ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	@Override
	public String getLabel() {
		if (label == null) {
			label = SpringLocaleDelegate.getInstance().getTextFallback(
					efMeta.getLocaleResourceIdForLabel(), efMeta.getFallbackLabel());
			label = label != null ? label : (getUID() != null ? getUID().getString() : "");
		}
		return label;
	}

	@Override
	public Integer getMaxLength() {
		// @TODO bei Dates nicht auch \u00fcber locale gesteuert?
		if (java.util.Date.class.getName().equals(efMeta.getDataType())) {
			return 10;
		}
		return efMeta.getScale();
	}

	@Override
	public Integer getPrecision() {
		return efMeta.getPrecision();
	}

	@Override
	public UID getReferencedEntityUID() {
		if (efMeta.getForeignEntity() != null)
			return efMeta.getForeignEntity();
		if (efMeta.getLookupEntity() != null)
			return efMeta.getLookupEntity();
		if (efMeta.getUnreferencedForeignEntity() != null)
			return efMeta.getUnreferencedForeignEntity();
		return null;
	}

	@Override
	public String getReferencedEntityFieldName() {
		if (efMeta.getForeignEntityField() != null)
			return efMeta.getForeignEntityField();
		if (efMeta.getLookupEntityField() != null)
			return efMeta.getLookupEntityField();
		if (efMeta.getUnreferencedForeignEntityField() != null)
			return efMeta.getUnreferencedForeignEntityField();
		return null;
	}

	@Override
	public boolean isNullable() {
		return efMeta.isNullable();
	}

	@Override
	public boolean isReferencing() {
		return efMeta.getForeignEntity() != null || efMeta.getLookupEntity() != null || efMeta.getUnreferencedForeignEntity() != null;
	}

	@Override
	public UID getEntityUID() {
		return efMeta.getEntity();
	}

	public FieldMeta<?> getMeta() {
		return efMeta;
	}
	
	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (!(other instanceof CollectableEOEntityField)) return false;
		final CollectableEOEntityField o = (CollectableEOEntityField) other;
		return efMeta.equals(o.efMeta);
	}
	
	@Override
	public int hashCode() {
		int result = efMeta.hashCode();
		return result + 3971;
	}

	@Override
	public String getDefaultComponentType() {
		return efMeta.getDefaultComponentType();
	}

	@Override
	public UID getUID() {
		return efMeta.getUID();
	}

	@Override
	public boolean isCalculated() {
		return efMeta.isCalculated();
	}

	@Override
	public boolean isCalcOndemand() {
		return efMeta.isCalcOndemand();
	}

	@Override
	public boolean isCalcAttributeAllowCustomization() {
		return efMeta.isCalcAttributeAllowCustomization();
	}

	@Override
	public String getName() {
		return efMeta.getFieldName();
	}
	
	public boolean isLocalized() {
		return efMeta.isLocalized();
	}

	@Override
	public boolean isHidden() {
		return efMeta.isHidden();
	}

	@Override
	public boolean isModifiable() {
		return efMeta.isModifiable();
	}
}
