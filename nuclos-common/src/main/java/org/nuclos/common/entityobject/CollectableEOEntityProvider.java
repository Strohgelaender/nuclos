//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.entityobject;

import java.util.NoSuchElementException;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityProvider;

/**
 * An abstract {@link CollectableEntityProvider} implementation which uses a given
 * {@link IMetaProvider} as back end.
 */
public abstract class CollectableEOEntityProvider implements CollectableEntityProvider {

	private final IMetaProvider metaProvider;

	protected CollectableEOEntityProvider(IMetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}

	/**
	 * This implementation is guaranteed to return a CollectableEOEntity.
	 */
	@Override
	public CollectableEntity getCollectableEntity(UID entityUID) throws NoSuchElementException {
		try {
			EntityMeta<?> entity = metaProvider.getEntity(entityUID);
			return new CollectableEOEntity(entity);
		} catch (Exception ex) {
			throw new NoSuchElementException(ex.getMessage());
		}
	}

}
