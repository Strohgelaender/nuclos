//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.metadata;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

public class NotifyObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6894479830160054363L;

	/**
	 * Maybe null.
	 */
	private final UID entity;
	
	private final boolean refreshMenus;
	
	public NotifyObject(UID entity, boolean refreshMenus) {
		this.entity = entity;
		this.refreshMenus = refreshMenus;
	}
	
	public UID getEntity() {
		return entity;
	}
	
	public boolean getRefreshMenus() {
		return refreshMenus;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof NotifyObject)) return false;
		final NotifyObject other = (NotifyObject) o;
		return LangUtils.equal(entity, other.entity) && (refreshMenus == other.refreshMenus);
	}
	
	@Override
	public int hashCode() {
		int result = 3911;
		if (entity != null) {
			result += 7 * entity.hashCode();
		}
		if (refreshMenus) {
			result += 11;
		}
		return result;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append(entity).append(",");
		result.append(refreshMenus).append("]");
		return result.toString();
	}
}
