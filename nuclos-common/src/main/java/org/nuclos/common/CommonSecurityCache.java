package org.nuclos.common;

public interface CommonSecurityCache {

	UID getUserUid(final String sUserName);

	boolean isReadAllowedForEntity(String user, UID entityUID, UID mandator);

	boolean isActionAllowed(final String user, final String action, UID mandator);

}
