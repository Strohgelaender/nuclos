package org.nuclos.common;

import java.io.Serializable;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LoginResult implements Serializable {

	private static final long serialVersionUID = 4355860982893558377L;

	/**
	 * The DB record ID of this session (from T_AD_SESSION).
	 * Always available.
	 */
	private final Long sessionId;

	/**
	 * The HTTP session ID.
	 * Only available for remote clients.
	 */
	private final String jSessionId;

	public LoginResult(final Long sessionId, final String jSessionId) {
		this.sessionId = sessionId;
		this.jSessionId = jSessionId;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public String getjSessionId() {
		return jSessionId;
	}
}
