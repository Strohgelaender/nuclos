//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * VO object used for entity object tree view representation persistence.
 * <p>
 * There is one instance of this class for every subform of a (base) entity.
 * This means that this class is used to customize subform entities only.
 * </p><p>
 * On the server side persistence will be performed by 
 * org.nuclos.server.masterdata.ejb3.MetaDataFacadeBean#createOrModifyEntity.
 * DB table used is 't_md_entity_subnodes'.
 * </p><p>
 * On the client side, the configuration is done with help of
 * org.nuclos.client.wizard.steps.NuclosEntityTreeValueStep.
 * </p><p>
 * TODO: This representation does not contain the Nuclos standard fields like
 * NuclosEOField.CHANGEDBY. 
 * </p>
 * @author Thomas Pasch (javadocs)
 */
public class EntityTreeViewVO implements IDalVO<UID>, Serializable, Comparable<EntityTreeViewVO> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4140509963733507276L;

	/**
	 * Primary key (uid) in DB table t_md_entity.
	 * 
	 * @author Thomas Pasch
	 * @since Nuclos 3.2.0 
	 */
	private UID uid;
	
	/**
	 * Reference to (user defined) entity meta data in DB table t_md_entity of the 
	 * (base) entity this subform tree view representation is defined for.
	 */
	private UID originentity;
	
	/**
	 * Name of the subform entity the (base) entity is linked to.
	 */
	private UID entity;
	
	/**
	 * Name of the field in the <em>subform</em> entity that denotes the reference to
	 * the base entity.
	 */
	private UID field;
	
	/**
	 * Display name ('Ordnername') in the <em>base</em> entity that
	 * is displayed as parent node for all subform entities. If null, this node is 
	 * omitted (and the subform entities are displayed as direct subnodes of the base
	 * entity). 
	 * <p>
	 * At present this field cannot be localized, i.e. it is always displayed in
	 * verbatim (no translation)!
	 * </p>
	 */
	@Deprecated
	private String foldername;
	
	/**
	 * Flag indicating that this subform entities are displayed in the tree view.
	 * 
	 * @author Thomas Pasch
	 * @since 3.1.01, NUCLOSINT-1176
	 */
	private Boolean active;
	
	/**
	 * Sort order between different subforms. 
	 * 
	 * @author Thomas Pasch
	 * @since 3.1.01, NUCLOSINT-1176
	 */
	private Integer sortOrder;

	/**
	 * Field for root reference (optional)
	 */
	private UID uidFieldRoot;

	/**
	 * Parent subnode (optional)
	 */
	private UID uidSubnode;

	/**
	 * Node label
	 */
	private String node;

	/**
	 * Node tooltip
	 */
	private String nodeTooltip;
	
	private String groupBy;

	private Boolean inheritNodes;

	private int state = -1;

	private String processor;
	
	private List<EntityTreeViewVO> lstChildNodes;

	private Boolean showEntityFolder;
	
	public EntityTreeViewVO(final UID uid, final UID uidOriginentity, final UID uidEntity, final UID uidField, 
			final String foldername, final Boolean active, final Integer sortOrder,final  Boolean inheritNodes, final  UID uidFieldRoot, final UID uidSubnode,
			final String node, final String nodeTooltip, final String groupBy, final Boolean showEntityFolder) {
			this(uid, uidOriginentity, uidEntity, uidField, foldername, active, sortOrder);
			this.uidFieldRoot = uidFieldRoot;
			this.uidSubnode = uidSubnode;
			this.node = node;
			this.nodeTooltip = nodeTooltip;
			this.inheritNodes = inheritNodes;
			this.groupBy = groupBy;
			this.showEntityFolder = showEntityFolder;
			
			makeConsistent();
	}
	
	@Deprecated
	private EntityTreeViewVO(final UID uid, final UID originentity, final UID entity, final UID field,
			final String foldername, final Boolean active, final Integer sortOrder) {
		this.uid = (null == uid) ? UID.UID_NULL : uid;
		this.originentity = originentity;
		this.entity = entity;
		this.field = field;
		this.foldername = LangUtils.nullIfBlank(foldername);		
		this.active = active;
		this.sortOrder = sortOrder;
		this.lstChildNodes = new ArrayList<EntityTreeViewVO>();
		makeConsistent();
	}

	/**
	 * @deprecated For serialization only.
	 */
	public EntityTreeViewVO() {
	}
	
	public final void makeConsistent() {
		// active and sortOrder are new fields. 
		// Hence we need a way to default them. (Thomas Pasch)
		if (active == null) {
			active = Boolean.FALSE;
		}
		
		// ensure that only sensible instances are active
		//if (StringUtils.isBlank(entity) || StringUtils.isBlank(field)) {
		if (null == entity) {
			active = Boolean.FALSE;
		}
		
		if (uid == null && !isFlagRemoved()) {
			flagNew();
		} else {
			if (-1 == this.state) {
				this.state = STATE_UNCHANGED;
			}
		}
		
		if (null == inheritNodes)  {
			inheritNodes = Boolean.FALSE;
		}
		
		if (null == showEntityFolder)  {
			showEntityFolder = Boolean.FALSE;
		}
		
		if (null == sortOrder)  {
			sortOrder = 0;
		}
	}
	
	public UID getPrimaryKey() {
		return uid;
	}
	
	public void setPrimaryKey(UID uid) {
		this.uid = uid;
	}
	
	public UID getOriginEntity() {
		return originentity;
	}
	
	public void setOriginEntity(UID originentity) {
		this.originentity = originentity;
	}
	
	public UID getEntity() {
		return entity;
	}
	
	public void setEntity(UID entity) {
		this.entity = entity;
	}
	
	public UID getField() {
		return field;
	}
	
	public void setField(UID field) {
		this.field = field;
	}
	@Deprecated
	public String getFoldername() {
		return foldername;
	}
	
	public void setFoldername(String foldername) {
		this.foldername = LangUtils.nullIfBlank(foldername);
	}
	
	public Boolean getIsShowEntityFolder() {
		return showEntityFolder;
	}

	public void setIsShowEntityFolder(Boolean showEntityFolder) {
		this.showEntityFolder = showEntityFolder;
	}
	
	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	
	public UID getFieldRoot() {
		return uidFieldRoot;
	}

	public void setFieldRoot(final UID uidFieldRoot) {
		this.uidFieldRoot = uidFieldRoot;
	}

	public UID getParentSubnodeId() {
		return uidSubnode;
	}

	public void setParentSubnodeId(final UID uidSubnode) {
		this.uidSubnode = uidSubnode;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getNodeTooltip() {
		return nodeTooltip;
	}

	public void setNodeTooltip(String nodeTooltip) {
		this.nodeTooltip = nodeTooltip;
	}
	
	public String getGroupBy() {
		return groupBy;
	}
	
	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}
	
	public Boolean getIsInheritNodes() {
		return inheritNodes;
	}

	public void setIsInheritNodes(Boolean inheritNodes) {
		this.inheritNodes = inheritNodes;
	}

	public void removeChildNode(final EntityTreeViewVO vo) {
		if (lstChildNodes.contains(vo)) {
			lstChildNodes.get(lstChildNodes.indexOf(vo)).flagRemove();
		}
	}

	public void addChildNode(final EntityTreeViewVO vo) {
		if (!lstChildNodes.contains(vo)) {
			this.lstChildNodes.add(vo);
			if (isFlagUnchanged()) {
				flagUpdate();
			}
		}
	}
	
	public List<EntityTreeViewVO> getChildNodes() {
		return lstChildNodes;
	}
	@Override
	public String toString() {
		final ToStringBuilder b = new ToStringBuilder(this);
		b.append(uid).append(originentity).append(entity).append(foldername);
		b.append(active).append(sortOrder);
		return b.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof EntityTreeViewVO)) return false;
		final EntityTreeViewVO other = (EntityTreeViewVO) o;
		return originentity.equals(other.originentity) 
			&& ObjectUtils.equals(entity, other.entity)
			&& ObjectUtils.equals(uidSubnode, other.uidSubnode)
			&& ObjectUtils.equals(field, other.field);
	}
	
	@Override 
	public int hashCode() {
		int result = 6261;
		result += 7 * originentity.hashCode();
		if (uidSubnode != null) result += 17 * uidSubnode.hashCode();
		if (entity != null) result += 11 * entity.hashCode();
		if (field != null) result += 13 * field.hashCode();
		return result;
	}

	/**
	 * This implementation is <em>not</em> consistent with equals.
	 * You have been warned!
	 */
	@Override
	public int compareTo(EntityTreeViewVO o) {
		int result = 0;
		result = LangUtils.compareComparables(sortOrder, o.sortOrder);
		if (result == 0) {
			result = LangUtils.compareComparables(originentity, o.originentity);
			if (result == 0) {
				result = LangUtils.compareComparables(uidSubnode, o.uidSubnode);	
				if (result == 0) {
					result = LangUtils.compareComparables(entity, o.entity);
					if (result == 0) {
						result = LangUtils.compareComparables(field, o.field);
					}
				}
			}
		}
		return result;
	}

	@Override
	public final void flagNew() {
		this.state = STATE_NEW;
	}
	
	@Override
	public final void flagUpdate() {
		this.state = STATE_UPDATED;
	}
	
	@Override
	public final void flagRemove() {
		this.state = STATE_REMOVED;
	}

	@Override
	public final boolean isFlagNew() {
		return this.state == STATE_NEW;
	}

	@Override
	public final boolean isFlagUpdated() {
		return this.state == STATE_UPDATED;
	}

	@Override
	public final boolean isFlagUnchanged() {
		return this.state == STATE_UNCHANGED;
	}
	
	@Override
	public final boolean isFlagRemoved() {
		return this.state == STATE_REMOVED;
	}
	

	@Override
	public String processor() {
		return processor;
	}

	@Override
	public void processor(String p) {
		this.processor = p;
		
	}
	
	@Override
	public UID getDalEntity() {
		// FIXME is this correct?
		return E.ENTITYSUBNODES.getUID();
	}
		
	public static EntityTreeViewVO wrapMasterData(final MasterDataVO<UID> vo, final UID uidOriginEntity) {
		final UID uidEntity = vo.getFieldUid(E.ENTITYSUBNODES.entity);
		final UID uidField = vo.getFieldUid(E.ENTITYSUBNODES.field);;
		final String foldername = vo.getFieldValue(E.ENTITYSUBNODES.foldername);
		final Boolean isActive = vo.getFieldValue(E.ENTITYSUBNODES.active);
		final Boolean isInheritSubNodes = vo.getFieldValue(E.ENTITYSUBNODES.inheritSubNodes);
		final UID uidFieldRoot = vo.getFieldUid(E.ENTITYSUBNODES.fieldRoot);
		final UID uidFieldSubnode = vo.getFieldUid(E.ENTITYSUBNODES.parentSubNode);
		final String node = vo.getFieldValue(E.ENTITYSUBNODES.node);
		final String nodeTooltip = vo.getFieldValue(E.ENTITYSUBNODES.nodeTooltip);
		final Integer sortOrder = vo.getFieldValue(E.ENTITYSUBNODES.sortOrder);
		final String groupBy = vo.getFieldValue(E.ENTITYSUBNODES.groupBy);
		final Boolean isShowEntityFolder = vo.getFieldValue(E.ENTITYSUBNODES.isShowEntityFolder);
		
		return new EntityTreeViewVO(vo.getId(), uidOriginEntity, uidEntity, uidField, foldername, isActive, 
				sortOrder, isInheritSubNodes, uidFieldRoot, uidFieldSubnode, node, nodeTooltip, groupBy, isShowEntityFolder);
	}
	
}
