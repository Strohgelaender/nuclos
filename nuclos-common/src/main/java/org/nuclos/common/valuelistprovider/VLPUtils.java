package org.nuclos.common.valuelistprovider;

import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

public final class VLPUtils {
	private VLPUtils () {
	}
	
	public static String getNameField(Object oValue, Map<String, Object> mpParameters) {
		String sValueFieldName = extractFieldName((String) oValue);
		mpParameters.put(ValuelistProviderVO.DATASOURCE_NAMEFIELD, sValueFieldName);
		return sValueFieldName;
		
	}
	
	public static String getIdField(Object oValue, Map<String, Object> mpParameters, UID valuelistProviderUid, IDataSource dataSource) {
		String sIdFieldName = extractFieldName((String) oValue);
		mpParameters.put(ValuelistProviderVO.DATASOURCE_IDFIELD, sIdFieldName);
		
		try {
			if (StringUtils.isNullOrEmpty(sIdFieldName) && valuelistProviderUid != null) {
				for (String sColumn : DatasourceUtils.getColumnsWithoutQuotes(
						dataSource.getColumnsFromVLP(valuelistProviderUid, Number.class))) {
					
					final String sColumnTmp = extractFieldName(sColumn);
					if ("intid".equalsIgnoreCase(sColumnTmp)) {
						sIdFieldName = sColumnTmp;
						mpParameters.put(ValuelistProviderVO.DATASOURCE_IDFIELD, sIdFieldName);
						break;
						
					}
				}
			}
		} catch (CommonBusinessException e) {
			throw new CommonFatalException(
					SpringLocaleDelegate.getInstance().getMessage(
							"datasource.collectable.fieldsprovider", "Fehler beim Laden der Datenquelle ''{0}'':\n", oValue), e);
		}
		
		return sIdFieldName;
	}
	
	public static String extractFieldName(String value) {
		// extract label if no alias is set. we strip something like T1."strname" @see NUCLOS-645
		if (value != null) {
			int idxDot = value.indexOf('.');
			if (idxDot != -1) {
				value = value.substring(idxDot + 1);
			}
			value = value.replaceAll("\"", "");				
		}
		return value;
	}

}
