//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable.searchcondition;

import java.util.*;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.IRigidMetaProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collect.collectable.searchcondition.visit.*;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.PredicateUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dblayer.IFieldRef;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

/**
 * Helper class for <code>CollectableSearchCondition</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class SearchConditionUtils {

	private static final String ALWAYS_FALSE_CONDITION = "AlwaysFalseCondition";

	protected SearchConditionUtils() {
	}

	/**
	 * §postcondition result != null
	 * §postcondition (cond != null) --&gt; result == cond
	 * §postcondition (cond == null) --&gt; result == TrueCondition.TRUE
	 * 
	 * @param cond May be <code>null</code>.
	 */
	public static CollectableSearchCondition trueIfNull(CollectableSearchCondition cond) {
		return (cond == null) ? TrueCondition.TRUE : cond;
	}

	/**
	 * §postcondition result.getLogicalOperator() == LogicalOperator.AND
	 * 
	 * @param acond
	 * @return a new conjunction containing the given operands.
	 */
	public static CompositeCollectableSearchCondition and(CollectableSearchCondition... acond) {
		final CompositeCollectableSearchCondition result = new CompositeCollectableSearchCondition(LogicalOperator.AND);
		for (CollectableSearchCondition cond : acond) {
			result.addOperand(cond);
		}
		assert result.getLogicalOperator() == LogicalOperator.AND;
		return result;
	}

	/**
	 * §postcondition result.getLogicalOperator() == LogicalOperator.OR
	 * 
	 * @param acond
	 * @return a new disjunction containing the given operands.
	 */
	public static CompositeCollectableSearchCondition or(CollectableSearchCondition... acond) {
		final CompositeCollectableSearchCondition result = new CompositeCollectableSearchCondition(LogicalOperator.OR);
		for (CollectableSearchCondition cond : acond) {
			result.addOperand(cond);
		}
		assert result.getLogicalOperator() == LogicalOperator.OR;
		return result;
	}

	/**
	 * §postcondition result.getLogicalOperator() == LogicalOperator.NOT
	 * 
	 * @param cond
	 * @return a new negation containing the given operand.
	 */
	public static CompositeCollectableSearchCondition not(CollectableSearchCondition cond) {
		final CompositeCollectableSearchCondition result = new CompositeCollectableSearchCondition(LogicalOperator.NOT);
		result.addOperand(cond);
		assert result.getLogicalOperator() == LogicalOperator.NOT;
		return result;
	}

	/**
	 * @param cond
	 * @return true if this condition is ALWAYS_FALSE_CONDITION -- constructed as not(TrueCondition).
	 */
	public static boolean isAlwaysFalseCondition(CollectableSearchCondition cond){
		return cond.getConditionName() != null && cond.getConditionName().equals(ALWAYS_FALSE_CONDITION);
	}
	
	/**
	 * §precondition !CollectionUtils.isNullOrEmpty(collIds)
	 * 
	 * @param collIds Collection&lt;Object&gt; collection of ids
	 * @return a <code>CollectableSearchCondition</code> like "(id=id1) OR (id=id2) OR ..." that finds all objects
	 * with the given ids.
	 */
	public static CollectableSearchCondition getCollectableSearchConditionForIds(Collection<?> collIds) {
		if (CollectionUtils.isNullOrEmpty(collIds)) {
			CompositeCollectableSearchCondition alwaysFalseCondition = SearchConditionUtils.not(TrueCondition.TRUE);
			alwaysFalseCondition.setConditionName(ALWAYS_FALSE_CONDITION);
			return alwaysFalseCondition;
			//throw new IllegalArgumentException("collIds");
		}

		final CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.OR);

		for (Object oId : collIds) {
			cond.addOperand(new CollectableIdCondition(oId));
		}
		return simplified(cond);
	}

	/**
	 * @param cond
	 * @return a condition that is syntactically simpler (if possible) but semantically equivalent to the given condition.
	 */
	public static CollectableSearchCondition simplified(CollectableSearchCondition cond) {
		return (cond == null) ? null : cond.accept(new SimplifiedVisitor());
		// @todo
		// return trueIfNull(cond).accept(new SimplifiedVisitor());
	}

	/**
	 * §postcondition result == null &lt;--&gt; cond == null
	 * 
	 * @param cond May be <code>null</code>.
	 * @return a semantically equivalent search condition in which the nodes in each level are sorted by this order:
	 * atomic nodes come first, ordered by their labels, composite nodes come last (unordered).
	 * @see #sorted(CollectableSearchCondition, boolean)
	 */
	public static CollectableSearchCondition sortedByLabels(CollectableSearchCondition cond) {
		return sorted(cond, true);
	}

	/**
	 * §postcondition result == null &lt;--&gt; cond == null
	 * 
	 * @param cond May be <code>null</code>.
	 * @param bSortByLabels Sort by field labels? Otherwise sort by field names.
	 * @return a semantically equivalent search condition in which the nodes in each level are sorted by this order:
	 * atomic nodes come first, ordered by their field names/labels, composite nodes come last (unordered).
	 */
	public static CollectableSearchCondition sorted(CollectableSearchCondition cond, boolean bSortByLabels) {
		return (cond == null) ? cond : cond.accept(new SortedVisitor(bSortByLabels));
	}

	/**
	 * §precondition cond != null
	 * 
	 * @param cond
	 * @param predicate
	 * @param bTraverseSubConditions true: traverse subconditions/referencing conditions - false: stop in subconditions/referencing conditions
	 * @return Does the given search condition contain a node that matches the given predicate?
	 */
	public static boolean contains(CollectableSearchCondition cond, Predicate<CollectableSearchCondition> predicate,
			boolean bTraverseSubConditions) {
		return cond.accept(new ContainsVisitor(predicate, bTraverseSubConditions));
	}

	/**
	 * §precondition atomiccond != null
	 * 
	 * @param clctefPeer
	 * @param cond
	 * @return the equivalent search condition for the given peer
	 */
	public static AtomicCollectableSearchCondition getConditionForPeer(CollectableEntityField clctefPeer, AtomicCollectableSearchCondition cond) {
		return cond.accept(new GetAtomicConditionForPeerVisitor(clctefPeer));
	}

	/**
	 * §todo generalize and make public
	 * 
	 * @param atomiccond
	 * @return the negation of the the given condition.
	 */
	public static AtomicCollectableSearchCondition negationOf(AtomicCollectableSearchCondition atomiccond) {
		return atomiccond.accept(new NegationOfAtomicSearchConditionVisitor());
	}

	/**
	 * @param prefs
	 * @return the search condition stored in prefs, if any.
	 */
	public static CollectableSearchCondition getSearchCondition(Preferences prefs, UID entity) throws PreferencesException {
		return PutSearchConditionToPrefsVisitor.getSearchCondition(prefs, entity, DefaultCollectableEntityProvider.getInstance());
	}
	
	/**
	 * @param collOperands Collection&lt;CollectableSearchCondition&gt;
	 * @return Is it true that there is not more than one atomic condition for a fieldname?
	 */
	public static boolean areAtomicConditionsUnique(Collection<CollectableSearchCondition> collOperands) {
		final Collection<AtomicCollectableSearchCondition> collAtomicOperands = CollectionUtils.selectInstancesOf(collOperands, AtomicCollectableSearchCondition.class);
		return CollectionUtils.forall(collAtomicOperands, PredicateUtils.transformedInputPredicate(new GetFieldUID(), PredicateUtils.<UID>isUnique()));
	}

	/**
	 * gets a map of all <code>CollectableField</code>s in <code>CollectableComparison</code>s contained in <code>cond</code>.
	 * 
	 * §postcondition result != null
	 * 
	 * @param cond
	 * @return Map&lt;String sFieldName, CollectableField clctfValue&gt;
	 */
	public static Map<UID, CollectableField> getAtomicFieldsMap(CollectableSearchCondition cond) {
		return SearchConditionUtils.trueIfNull(cond).accept(new GetAtomicFieldsMapVisitor());
	}

	/**
	 * Predicate: Does a given <code>CollectableSearchCondition</code> have the given type?
	 */
	public static class HasType implements Predicate<CollectableSearchCondition> {
		private final int iType;

		public HasType(int iType) {
			this.iType = iType;
		}

		@Override
		public boolean evaluate(CollectableSearchCondition cond) {
			return cond.getType() == this.iType;
		}
	}	// inner class HasType

	/**
	 * Transformer: Gets the field name of a given <code>AtomicCollectableSearchCondition</code>.
	 */
	public static class GetFieldUID implements Transformer<AtomicCollectableSearchCondition, UID> {
		@Override
		public UID transform(AtomicCollectableSearchCondition o) {
			return o.getFieldUID();
		}
	}	// inner class GetFieldUID

	/**
	 * inner class NegationOfAtomicSearchConditionVisitor
	 */
	private static class NegationOfAtomicSearchConditionVisitor implements AtomicVisitor<AtomicCollectableSearchCondition, RuntimeException> {

		@Override
		public AtomicCollectableSearchCondition visitComparison(CollectableComparison comparison) {
			CollectableComparison res = new CollectableComparison(comparison.getEntityField(), getComplementalOperator(comparison), comparison.getComparand());
			res.setConditionName(comparison.getConditionName());
			return res;
		}

		@Override
		public AtomicCollectableSearchCondition visitComparisonWithParameter(CollectableComparisonWithParameter comparisonwp) {
			CollectableComparisonWithParameter res = new CollectableComparisonWithParameter(comparisonwp.getEntityField(), getComplementalOperator(comparisonwp), comparisonwp.getParameter());
			res.setConditionName(comparisonwp.getConditionName());
			return res;
		}
		
		@Override
		public AtomicCollectableSearchCondition visitComparisonDateValues(CollectableComparisonDateValues comparisondv) throws RuntimeException {
			CollectableComparisonDateValues res = new CollectableComparisonDateValues(comparisondv.getEntityField(), getComplementalOperator(comparisondv), comparisondv.getDateValues());
			res.setConditionName(comparisondv.getConditionName());
			return res;
		}

		@Override
		public AtomicCollectableSearchCondition visitComparisonWithOtherField(CollectableComparisonWithOtherField comparisonwf) {
			CollectableComparisonWithOtherField res = new CollectableComparisonWithOtherField(comparisonwf.getEntityField(), getComplementalOperator(comparisonwf), comparisonwf.getOtherField());
			res.setConditionName(comparisonwf.getConditionName());
			return res;
		}

		@Override
		public AtomicCollectableSearchCondition visitIsNullCondition(CollectableIsNullCondition isnullcond) {
			CollectableIsNullCondition res = new CollectableIsNullCondition(isnullcond.getEntityField(), getComplementalOperator(isnullcond));
			res.setConditionName(isnullcond.getConditionName());
			return res;
		}

		@Override
		public AtomicCollectableSearchCondition visitLikeCondition(CollectableLikeCondition likecond) {
			CollectableLikeCondition res = new CollectableLikeCondition(likecond.getEntityField(), getComplementalOperator(likecond), likecond.getLikeComparand());
			res.setConditionName(likecond.getConditionName());
			return res;
		}
		
		@Override
		public <T> AtomicCollectableSearchCondition visitInCondition(CollectableInCondition<T> incond) {
			CollectableInCondition<T> res = new CollectableInCondition<T>(incond.getEntityField(), getComplementalOperator(incond), incond.getInComparands(), incond.getKeyMap());
			res.setConditionName(incond.getConditionName());
			return res;
		}

		private static ComparisonOperator getComplementalOperator(AtomicCollectableSearchCondition atomiccond) {
			return ComparisonOperator.complement(atomiccond.getComparisonOperator());
		}

	}	// NegationOfAtomicSearchConditionVisitor

	/**
	 * inner class SortedVisitor
	 */
	private static class SortedVisitor implements Visitor<CollectableSearchCondition, RuntimeException>, CompositeVisitor<CollectableSearchCondition, RuntimeException> {

		private final boolean bSortByLabels;

		SortedVisitor(boolean bSortByLabels) {
			this.bSortByLabels = bSortByLabels;
		}

		@Override
		public CollectableSearchCondition visitTrueCondition(TrueCondition truecond) throws RuntimeException {
			// there is nothing to sort. We can return the condition itself as it is immutable.
			return truecond;
		}

		@Override
		public CollectableSearchCondition visitAtomicCondition(AtomicCollectableSearchCondition atomiccond) {
			// there is nothing to sort. We can return the condition itself as it is immutable.
			return atomiccond;
		}

		@Override
		public CollectableSearchCondition visitCompositeCondition(CompositeCollectableSearchCondition compositecond) {
			CompositeCollectableSearchCondition res = new CompositeCollectableSearchCondition(compositecond.getLogicalOperator(),
					CollectionUtils.sorted(
							CollectionUtils.transform(compositecond.getOperands(), new Sorted(bSortByLabels)),
							new CompareByFieldNameOrLabel(bSortByLabels)));
			res.setConditionName(compositecond.getConditionName());
			return res;
		}

		@Override
		public CollectableSearchCondition visitIdCondition(CollectableIdCondition idcond) {
			// there is nothing to sort. We can return the condition itself as it is immutable.
			return idcond;
		}

		@Override
		public CollectableSearchCondition visitSubCondition(CollectableSubCondition subcond) {
			CollectableSubCondition res = new CollectableSubCondition(subcond.getSubEntityUID(), subcond.getForeignKeyFieldUID(),
					sorted(subcond.getSubCondition(), bSortByLabels));
			res.setConditionName(subcond.getConditionName());
			return res;
		}

		@Override
		public CollectableSearchCondition visitRefJoinCondition(RefJoinCondition joincond) {
			// do noting
			return joincond;
		}

		@Override
		public CollectableSearchCondition visitGeneralJoinCondition(GeneralJoinCondition joincond) {
			// do noting
			return joincond;
		}

		@Override
		public CollectableSearchCondition visitSelfSubCondition(CollectableSelfSubCondition subcond) {
			CollectableSelfSubCondition res = new CollectableSelfSubCondition(subcond.getForeignKeyFieldName(),
					sorted(subcond.getSubCondition(), bSortByLabels), subcond.getSubEntityName());
			res.setConditionName(subcond.getConditionName());
			return res;
		}

		@Override
		public CollectableSearchCondition visitPlainSubCondition(PlainSubCondition subcond) {
			//return new PlainSubCondition(subcond.getPlainSQL(), subcond.getConditionName());
			return subcond;
		}

		@Override
		public CollectableSearchCondition visitReferencingCondition(ReferencingCollectableSearchCondition refcond) {
			ReferencingCollectableSearchCondition res = new ReferencingCollectableSearchCondition(refcond.getReferencingField(),
					sorted(refcond.getSubCondition(), bSortByLabels));
			res.setConditionName(refcond.getConditionName());
			return res;
		}

		@Override
        public CollectableSearchCondition visitIdListCondition(CollectableIdListCondition collectableIdListCondition) throws RuntimeException {
	        return collectableIdListCondition;
        }

		@Override
		public <T> CollectableSearchCondition visitInCondition(CollectableInCondition<T> collectableInCondition) throws RuntimeException {
			return collectableInCondition;
		}

	}	 // inner class SortedVisitor

	/**
	 * Comparator: CompareByFieldNameOrLabel
	 */
	private static class CompareByFieldNameOrLabel implements Comparator<CollectableSearchCondition> {

		private final boolean bSortByLabels;

		CompareByFieldNameOrLabel(boolean bSortByLabels) {
			this.bSortByLabels = bSortByLabels;
		}

		@Override
		public int compare(CollectableSearchCondition cond1, final CollectableSearchCondition cond2) {
			final int result;
			final int iTypeDiff = cond1.getType() - cond2.getType();

			if (iTypeDiff != 0) {
				result = iTypeDiff;
			}
			else {
				// NUCLOS-7383 Somebody did not take care for the mix of PlainSubCondition with CollectabelSubCondition
				// as they have the same type, unfortunately
				if (cond2 instanceof PlainSubCondition) {
					return 0;
				}

				result = cond1.accept(new CompareByFieldNameOrLabelAtomicVisitor(cond2, bSortByLabels));
			}
			return result;
		}

	}	// inner class CompareByFieldNameOrLabel

	/**
	 * Transformer: Sorted
	 */
	private static class Sorted implements Transformer<CollectableSearchCondition, CollectableSearchCondition> {

		private final boolean bSortByLabels;

		Sorted(boolean bSortByLabels) {
			this.bSortByLabels = bSortByLabels;
		}

		@Override
		public CollectableSearchCondition transform(CollectableSearchCondition cond) {
			return sorted(cond, bSortByLabels);
		}

	} // inner class Sorted

	/**
	 * Visitor returning an equivalent atomic condition for a given entity field.
	 */
	private static class GetAtomicConditionForPeerVisitor implements AtomicVisitor<AtomicCollectableSearchCondition, RuntimeException> {

		private final CollectableEntityField clctefPeer;

		GetAtomicConditionForPeerVisitor(CollectableEntityField clctefPeer) {
			this.clctefPeer = clctefPeer;
		}

		@Override
		public AtomicCollectableSearchCondition visitComparison(CollectableComparison comparison) {
			return new CollectableComparison(clctefPeer, comparison.getComparisonOperator(), comparison.getComparand());
		}

		@Override
		public AtomicCollectableSearchCondition visitComparisonWithParameter(CollectableComparisonWithParameter comparisonwp) {
			return new CollectableComparisonWithParameter(clctefPeer, comparisonwp.getComparisonOperator(), comparisonwp.getParameter());
		}
		
		@Override
		public AtomicCollectableSearchCondition visitComparisonDateValues(CollectableComparisonDateValues comparisondv) throws RuntimeException {
			return new CollectableComparisonDateValues(clctefPeer, comparisondv.getComparisonOperator(), comparisondv.getDateValues());
			}
		
		@Override
		public AtomicCollectableSearchCondition visitComparisonWithOtherField(CollectableComparisonWithOtherField comparisonwf) {
			return new CollectableComparisonWithOtherField(clctefPeer, comparisonwf.getComparisonOperator(), comparisonwf.getOtherField());
		}

		@Override
		public AtomicCollectableSearchCondition visitIsNullCondition(CollectableIsNullCondition isnullcond) {
			return new CollectableIsNullCondition(clctefPeer, isnullcond.getComparisonOperator());
		}

		@Override
		public AtomicCollectableSearchCondition visitLikeCondition(CollectableLikeCondition likecond) {
			return new CollectableLikeCondition(clctefPeer, likecond.getComparisonOperator(), likecond.getLikeComparand());
		}
		
		@Override
		public <T> AtomicCollectableSearchCondition visitInCondition(CollectableInCondition<T> incond) {
			return new CollectableInCondition<T>(clctefPeer, incond.getComparisonOperator(), incond.getInComparands(), incond.getKeyMap());
		}
	}	// inner class GetAtomicConditionForPeerVisitor
	
	public static Object getObjectValueForStringAndClass(String s, Class<?> _numberClass) {
		Object value = null;
		try {
			if (_numberClass == Integer.class) {
				value = Integer.parseInt(s);
			} else if (_numberClass == Long.class) {
				value = Long.parseLong(s);
			} else if (_numberClass == Double.class) {
				value = Double.parseDouble(s.replace(',', '.'));
			}
		} catch (NumberFormatException nfe) {
			//ignore
		}
		return value;
	}
	
	public static CollectableSearchCondition getSearchExpressionForNumberSearch(String s, Class<?> _numberClass, CollectableEntityField c) {
		CollectableSearchCondition csd = null;
		boolean bEquals = (s.indexOf('=') == 1);	
		Pattern p = Pattern.compile("\\d{1}\\-");
		Matcher m = p.matcher(s);
		if (m.find()) {
		    int n = m.start();
			Object value1 = getObjectValueForStringAndClass(s.substring(0, n + 1), _numberClass);
			Object value2 = getObjectValueForStringAndClass(s.substring(n + 2), _numberClass);
			if (value1 != null && value2 != null) {
				CollectableComparison cc1 = new CollectableComparison(c, ComparisonOperator.GREATER_OR_EQUAL, new CollectableValueField(value1));					
				CollectableComparison cc2 = new CollectableComparison(c, ComparisonOperator.LESS_OR_EQUAL, new CollectableValueField(value2));
				CompositeCollectableSearchCondition ccsc = new CompositeCollectableSearchCondition(LogicalOperator.AND);
				ccsc.addOperand(cc1);
				ccsc.addOperand(cc2);
				csd = ccsc;
			}
		} else if (s.startsWith(">")) {
			Object value = getObjectValueForStringAndClass(s.substring(bEquals ? 2 : 1), _numberClass);
			if (value != null) {
				csd = new CollectableComparison(c, bEquals ? ComparisonOperator.GREATER_OR_EQUAL 
						: ComparisonOperator.GREATER, new CollectableValueField(value));					
			}			
		} else if (s.startsWith("<")) {
			Object value = getObjectValueForStringAndClass(s.substring(bEquals ? 2 : 1), _numberClass);
			if (value != null) {
				csd = new CollectableComparison(c, bEquals ? ComparisonOperator.LESS_OR_EQUAL 
						: ComparisonOperator.LESS, new CollectableValueField(value));					
			}			
		} else {
			Object value = getObjectValueForStringAndClass(s, _numberClass);
			if (value != null) {
				csd = new CollectableComparison(c, ComparisonOperator.EQUAL, new CollectableValueField(value));					
			}
		}
		return csd;
	}

	public static void addTextSearchToSearchExpression(String search, List<CollectableEntityField> cefs,
													   CollectableSearchExpression clctexpr, UID sEntity, IRigidMetaProvider mdProv) {
		if (search == null || cefs == null) {
			return;
		}

		List<CollectableSearchCondition> lstAnd = new ArrayList<>();

		String[] spatterns;
		int slength = search.length();
		if (slength >= 3 && search.startsWith("\"") && search.endsWith("\"")) {
			// NUCLOS-6256 Expressions in quotes do not use split for whitespaces
			spatterns = new String[]{search.substring(1, slength - 1)};
		} else {
			spatterns = search.split("\\s");
		}

		if (spatterns.length == 0) {
			return;
		}

		for (String s : spatterns) {
			String spattern = "*" + s + "*";

			List<CollectableSearchCondition> lstOr = new ArrayList<>();
			for (CollectableEntityField c : cefs) {
				if (!c.getEntityUID().equals(sEntity)) continue;

				Class<?> _class = c.getJavaClass();

				// NUCLOS-6375: For References get the real java class of the Stringified Reference
				if (c.isReferencing() && mdProv != null) {
					String refFieldName = c.getReferencedEntityFieldName();
					if (refFieldName != null) {
						ForeignEntityFieldUIDParser parser = new ForeignEntityFieldUIDParser(refFieldName, null, null);
						_class = parser.getReferencingClass(mdProv);
						// NUCLOS-7119 Skip Search on references to pure date, because Postgres can't handle it
						if (_class == Date.class) {
							continue;
						}
					}
				}

				if (Number.class.isAssignableFrom(_class)) {
					CollectableSearchCondition csd = getSearchExpressionForNumberSearch(search, _class, c);
					if (csd != null) lstOr.add(csd);
				}

				if (_class != String.class && _class != Date.class) continue;
				CollectableLikeCondition clc = new CollectableLikeCondition(c, spattern);
				lstOr.add(clc);
			}

			if (lstOr.isEmpty()) {
				continue;
			}

			if (lstOr.size() > 1) {
				CollectableSearchCondition[] cscs = new CollectableSearchCondition[lstOr.size()];
				for (int i = 0; i < cscs.length; i++) cscs[i] = lstOr.get(i);
				CompositeCollectableSearchCondition ccsc = SearchConditionUtils.or(cscs);
				lstAnd.add(ccsc);
			} else {
				lstAnd.add(lstOr.get(0));
			}

		}


		CollectableSearchCondition c = clctexpr.getSearchCondition();
		if (c != null) lstAnd.add(0, c);
		if (lstAnd.size() > 1) {
			CollectableSearchCondition[] cscs = new CollectableSearchCondition[lstAnd.size()];
			for (int i = 0; i < cscs.length; i++) cscs[i] = lstAnd.get(i);
			clctexpr.setSearchCondition(SearchConditionUtils.and(cscs));
		} else {
			clctexpr.setSearchCondition(lstAnd.get(0));
		}
	}

}	// class SearchConditionUtils
