//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable;

import org.nuclos.common.UID;

public class VirtualCollectableEntityField extends AbstractCollectableEntityField {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7339858066006880714L;

	private final UID entity;
	
	private final UID field;
	
	private final UID referencedEntity;
	
	public VirtualCollectableEntityField(UID entity, UID field, UID refEntity) {
		this.entity = entity;
		this.field = field;
		this.referencedEntity = refEntity;
	}

	@Override
	public String getDescription() {
		return "Verweis auf \u00fcbergeordneten Datensatz";
	}

	@Override
	public int getFieldType() {
		return CollectableEntityField.TYPE_VALUEIDFIELD;
	}

	@Override
	public Class<?> getJavaClass() {
		return String.class;
	}

	@Override
	public String getLabel() {
		return "Referenz auf Vaterobjekt";
	}

	@Override
	public Integer getMaxLength() {
		return null;
	}
	
	@Override
	public Integer getPrecision() {
		return null;
	}

	@Override
	public UID getUID() {
		return field;
	}
	
	@Override
	public String getFormatInput() {
		return null;
	}
	
	@Override
	public String getFormatOutput() {
		return null;
	}

	@Override
	public UID getReferencedEntityUID() {
		return referencedEntity;
	}

	@Override
	public boolean isNullable() {
		return false;
	}

	@Override
	public boolean isReferencing() {
		return true;
	}

	@Override
	public CollectableEntity getCollectableEntity() {
		return null;
	}

	@Override
	public void setCollectableEntity(CollectableEntity clent) {
	}

	@Override
	public UID getEntityUID() {
		return entity;
	}

	@Override
	public String getDefaultComponentType() {
		return null;
	}

	@Override
	public boolean isCalculated() {
		return false;
	}

	@Override
	public boolean isCalcOndemand() {
		return false;
	}

	@Override
	public boolean isCalcAttributeAllowCustomization() {
		return false;
	}

	@Override
	public String getName() {
		return null;
	}
}
