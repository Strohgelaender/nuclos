//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.util.List;


public class NuclosResultDetailsSplitToolBarConfiguration extends ToolBarConfiguration {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2973177037845995459L;
	public static final NuclosResultDetailsSplitToolBarConfiguration DEFAULT = new NuclosResultDetailsSplitToolBarConfiguration(
			new ToolBarItemGroup(MAIN_TOOL_BAR_KEY)
				.addItem(NuclosToolBarItems.SAVE)
				.addItem(NuclosToolBarItems.REFRESH_LIST)
				.addItem(NuclosToolBarItems.NEW)
				.addItem(NuclosToolBarItems.CONFIG_COLUMNS)
			);
	
	@Override
	public ToolBarConfiguration getDefaultConfiguration() {
		return DEFAULT;
	}

	@Override
	public List<ToolBarItem> getConfigurableItems(Context context) {
		List<ToolBarItem> result = super.getConfigurableItems(context);
		return result;
	}
	
	public NuclosResultDetailsSplitToolBarConfiguration(ToolBarItemGroup mainToolBar) {
		super(mainToolBar);
	}

	public NuclosResultDetailsSplitToolBarConfiguration(String value) {
		super(value);
	}
	
	public NuclosResultDetailsSplitToolBarConfiguration() {
	}

}
