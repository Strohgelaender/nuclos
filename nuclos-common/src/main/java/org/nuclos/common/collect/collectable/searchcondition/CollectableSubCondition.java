//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable.searchcondition;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityProvider;
import org.nuclos.common.collect.collectable.searchcondition.visit.Visitor;
import org.nuclos.common2.LangUtils;

/**
 * A collectable subcondition which represents an expression like this: "Does a Collectable in the given dependant subentity
 * exist that is joined to the main entity by the given foreign key field and that matches the criteria given in the subcondition?".
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public final class CollectableSubCondition extends AbstractCollectableSearchCondition {
	/**
	 *
	 */
	private static final long serialVersionUID = 3875234220537361491L;

	private UID field;

	private final UID subEntity;
	private final UID foreignKeyField;
	private final CollectableSearchCondition condSub;

	public CollectableSubCondition(
			UID field,
			UID subEntity,
			UID foreignKeyField,
			CollectableSearchCondition condSub
	) {
		this(subEntity, foreignKeyField, condSub);
		if (field == null) {
			throw new NullArgumentException("field");
		}
		this.field = field;
	}

	/**
	 * §precondition sSubEntityName != null
	 * §precondition sForeignKeyFieldName != null
	 */
	public CollectableSubCondition(
			UID subEntity,
			UID foreignKeyField,
			CollectableSearchCondition condSub
	) {
		if (subEntity == null) {
			throw new NullArgumentException("subEntity");
		}
		this.subEntity = subEntity;
		this.foreignKeyField = foreignKeyField;
		this.field = null;
		this.condSub = condSub;
	}

	/**
	 * §postcondition result != null
	 */
	public CollectableEntity getSubEntity() {
		return DefaultCollectableEntityProvider.getInstance().getCollectableEntity(this.getSubEntityUID());
	}

	public UID getSubEntityUID() {
		return this.subEntity;
	}

	/**
	 * §postcondition result != null
	 */
	public UID getForeignKeyFieldUID() {
		return this.foreignKeyField;
	}

	public UID getFieldUID() {
		return this.field;
	}

	/**
	 * @return the subcondition, if any.
	 */
	public CollectableSearchCondition getSubCondition() {
		return this.condSub;
	}

	/**
	 * @deprecated Don't use this constant in new applications.
	 */
	@Override
	public int getType() {
		return TYPE_SUB;
	}

	@Override
	public boolean isSyntacticallyCorrect() {
		final CollectableSearchCondition condSub = this.getSubCondition();
		return condSub != null && condSub.isSyntacticallyCorrect();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CollectableSubCondition)) {
			return false;
		}
		final CollectableSubCondition that = (CollectableSubCondition) o;

		return this.subEntity.equals(that.subEntity) &&
				this.foreignKeyField.equals(that.foreignKeyField) &&
				LangUtils.equal(this.condSub, that.condSub);
	}

	@Override
	public int hashCode() {
		return this.subEntity.hashCode() ^ this.foreignKeyField.hashCode() ^ LangUtils.hashCode(this.condSub);
	}

	@Override
	public <O, Ex extends Exception> O accept(Visitor<O, Ex> visitor) throws Ex {
		return visitor.visitSubCondition(this);
	}

	@Override
	public String toString() {
		return getClass().getName() + ":" + getConditionName() + ":" + condSub + ":" +
				subEntity + ":" + foreignKeyField;
	}

}  // class CollectableSubCondition
