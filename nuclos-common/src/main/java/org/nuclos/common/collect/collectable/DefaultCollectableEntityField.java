//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable;

import java.io.Serializable;
import java.util.Date;
import java.util.prefs.Preferences;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.exception.CollectableFieldValidationException;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;



/**
 * Default implementation of a <code>CollectableEntityField</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: Why we have such a strange implementation? Can we get rid of it? If this is mainly
 * to write to {@link Preferences}, consider todo below.
 * </p><p>
 * TODO: Consider org.nuclos.client.common.CollectableEntityFieldPreferencesUtil
 * to write to {@link Preferences}.
 * </p>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class DefaultCollectableEntityField extends AbstractCollectableEntityField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -620983730311455804L;
	private final UID entity;
	private final UID field;
	private final Class<?> cls;
	private final String sLabel;
	private final String sDescription;
	private final Integer iMaxLength;
	private final Integer iPrecision;
	private final boolean bNullable;
	private final int iFieldType;
	private final CollectableField clctfDefault;
	private final UID referencedEntity;
	private final String sFormatInput;
	private final String sFormatOutput;
	private final String sDefaultComponentType;
	private final boolean bCalculated;
	private final boolean bCalcOndemand;
	private final boolean bCalcAttributeAllowCustomization;
	private final boolean isLocalized;
	private final boolean isHidden;
	private final boolean isModifiable;

	public DefaultCollectableEntityField(UID field, Class<?> cls, String sLabel, String sDescription,
			Integer iMaxLength, Integer iPrecision, boolean bNullable, int iFieldType, String sFormatInput,
			String sFormatOutput, UID entity, String sDefaultComponentType) {
		this(field, cls, sLabel, sDescription, iMaxLength, iPrecision, bNullable, iFieldType, null,
				CollectableUtils.getNullField(iFieldType, false), sFormatInput, sFormatOutput, entity, sDefaultComponentType, false, false, true);
		assert field != null;
		assert entity != null;
	}

	public DefaultCollectableEntityField(UID field, Class<?> cls, String sLabel, String sDescription,
										 Integer iMaxLength, Integer iPrecision, boolean bNullable, int iFieldType, String sFormatInput,
										 String sFormatOutput, UID entity, String sDefaultComponentType, boolean isLocalized, boolean isHidden, final boolean isModifiable) {
		this(field, cls, sLabel, sDescription, iMaxLength, iPrecision, bNullable, iFieldType, null,
				CollectableUtils.getNullField(iFieldType, isLocalized), sFormatInput, sFormatOutput, entity, sDefaultComponentType, isLocalized, isHidden, true);
		assert field != null;
		assert entity != null;
	}

	/**
	 * §precondition clctfDefault != null
	 * §todo add precondition (referencedEntity != null) --&gt; (iFieldType == IDFIELD)
	 */
	public DefaultCollectableEntityField(UID field, Class<?> cls, String sLabel, String sDescription, Integer iMaxLength,
										 Integer iPrecision, boolean bNullable, int iFieldType, UID referencedEntity, CollectableField clctfDefault,
										 String sFormatInput, String sFormatOutput, UID entity, String sDefaultComponentType, boolean isLocalized, boolean isHidden, final boolean isModifiable) {
		this(field, cls, sLabel, sDescription, iMaxLength, iPrecision, bNullable, iFieldType, referencedEntity, clctfDefault, sFormatInput, sFormatOutput, entity, sDefaultComponentType, false, false, false, isLocalized, isHidden, isModifiable);
	}
	
	public DefaultCollectableEntityField(UID field, Class<?> cls, String sLabel, String sDescription, Integer iMaxLength,
										 Integer iPrecision, boolean bNullable, int iFieldType, UID referencedEntity, CollectableField clctfDefault,
										 String sFormatInput, String sFormatOutput, UID entity, String sDefaultComponentType,
										 boolean bCalculated, boolean bCalcOndemand, boolean bCalcAttributeAllowCustomization, boolean isLocalized, boolean isHidden, final boolean isModifiable) {
	
		if (clctfDefault == null) {
			throw new NullArgumentException("clctfDefault");
		}
		
		this.entity = entity;
		this.field = field;
		this.cls = cls;
		this.sLabel = sLabel;
		this.sDescription = sDescription;
		this.iMaxLength = iMaxLength;
		this.iPrecision = iPrecision;
		this.bNullable = bNullable;
		this.iFieldType = iFieldType;
		this.clctfDefault = clctfDefault;
		this.referencedEntity = referencedEntity;
		this.sFormatInput = sFormatInput;
		this.sFormatOutput = sFormatOutput;
		this.sDefaultComponentType = sDefaultComponentType;
		this.bCalculated = bCalculated;
		this.bCalcOndemand = bCalcOndemand;
		this.bCalcAttributeAllowCustomization = bCalcAttributeAllowCustomization;
		this.isLocalized = isLocalized;
		this.isHidden = isHidden;
		this.isModifiable = isModifiable;
		
		try {
			CollectableUtils.validateFieldType(clctfDefault, this);
			if (this.getJavaClass().equals(Date.class) && clctfDefault.getValue() != null &&
				clctfDefault.getValue().equals(RelativeDate.today().toString())) {
				//ok
			}
			else {
				CollectableUtils.validateValueClass(clctfDefault, this);
			}
		}
		catch (CollectableFieldValidationException ex) {
			throw new IllegalArgumentException(SpringLocaleDelegate.getInstance().getMessage(
					"DefaultCollectableEntityField.1","Der angegebene Defaultwert passt nicht zum Datentyp."));
		}
		
		assert field != null;
		assert entity != null;
	}

	/**
	 * §postcondition this.equals(clctef)
	 */
	public DefaultCollectableEntityField(CollectableEntityField clctef, UID entity) {
		this(clctef, entity, null);
	}
	
	/**
	 * §postcondition this.equals(clctef)
	 */
	public DefaultCollectableEntityField(CollectableEntityField clctef, UID entity, String sDefaultComponentType) {
		this(clctef.getUID(), clctef.getJavaClass(), clctef.getLabel(), clctef.getDescription(), clctef.getMaxLength(),
				clctef.getPrecision(), clctef.isNullable(), clctef.getFieldType(), clctef.getReferencedEntityUID(),
				clctef.getDefault(), clctef.getFormatInput(), clctef.getFormatOutput(), entity, sDefaultComponentType, clctef.isCalculated(), clctef.isCalcOndemand(), clctef.isCalcAttributeAllowCustomization(), clctef.isLocalized(),
				clctef.isHidden(), clctef.isModifiable());
		
		assert field != null;
		assert entity != null;
	}
	
	@Override
	public UID getUID() {
		return this.field;
	}

	@Override
	public UID getEntityUID() {
		return this.entity;
	}

	@Override
	public String getFormatInput() {
		return this.sFormatInput;
	}

	@Override
	public String getFormatOutput() {
		return this.sFormatOutput;
	}

	@Override
	public int getFieldType() {
		return this.iFieldType;
	}

	@Override
	public Class<?> getJavaClass() {
		return this.cls;
	}

	@Override
	public String getLabel() {
		return this.sLabel;
	}

	@Override
	public String getDescription() {
		return this.sDescription;
	}

	@Override
	public Integer getMaxLength() {
		return this.iMaxLength;
	}

	@Override
	public Integer getPrecision() {
		return this.iPrecision;
	}

	@Override
	public UID getReferencedEntityUID() {
		return this.referencedEntity;
	}

	@Override
	public boolean isReferencing() {
		return this.getReferencedEntityUID() != null;
	}

	@Override
	public boolean isNullable() {
		return this.bNullable;
	}

	@Override
	public boolean isCalculated() {
		return bCalculated;
	}

	@Override
	public boolean isCalcOndemand() {
		return bCalcOndemand;
	}

	@Override
	public boolean isCalcAttributeAllowCustomization() {
		return bCalcAttributeAllowCustomization;
	}

	@Override
	public CollectableField getDefault() {
		final CollectableField result = this.clctfDefault;

		assert result != null;
		assert result.getFieldType() == this.getFieldType();
		if (!(this.getJavaClass() == Date.class && result.getValue() != null && result.getValue().toString().equalsIgnoreCase(RelativeDate.today().toString())))
			assert LangUtils.isInstanceOf(result.getValue(), this.getJavaClass());
		return result;
	}

	@Override
	public String getDefaultComponentType() {
		return sDefaultComponentType;
	}

	@Override
	public String getName() {
		return null;
	}
	
	public boolean isLocalized() {
		return this.isLocalized;
	}

	@Override
	public boolean isHidden() {
		return isHidden;
	}

	@Override
	public boolean isModifiable() {
		return isModifiable;
	}
}	// class DefaultCollectableEntityField
