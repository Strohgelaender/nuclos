//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable;

import java.io.Serializable;
import java.text.Collator;
import java.util.Comparator;
import java.util.prefs.Preferences;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.access.CefSecurityAgent;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.LangUtils;

/**
 * Provides structural (meta) information about a <code>CollectableField</code>.
 * This corresponds to a column in a relational database table schema.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: Consider org.nuclos.client.common.CollectableEntityFieldPreferencesUtil
 * to write to {@link Preferences}.
 * </p>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public interface CollectableEntityField extends Serializable, Comparable<CollectableEntityField> {

	/**
	 * Transformer: GetEntityUID
	 */
	public static class GetEntityUID implements Transformer<CollectableEntityField, UID> {
		@Override
		public UID transform(CollectableEntityField clctefwe) {
			return clctefwe.getEntityUID();
		}
	}
	
	public static class GetEntityUIDAsString implements Transformer<CollectableEntityField, String> {
		@Override
		public String transform(CollectableEntityField clctefwe) {
			return clctefwe.getEntityUID().getString();
		}
	}

	/**
	 * Transformer: GetQualifiedEntityField
	 */
	public static class GetQualifiedEntityField implements Transformer<CollectableEntityField, String> {
		@Override
		public String transform(CollectableEntityField clctefwe) {
			return clctefwe.getEntityUID().getString() + "." + clctefwe.getUID().getString();
		}
	}

	/**
	 * Predicate: HasEntity
	 */
	public static class HasEntity implements Predicate<CollectableEntityField> {
		private final UID entity;

		public HasEntity(CollectableEntity clcte) {
			this(clcte.getUID());
		}

		public HasEntity(UID entity) {
			this.entity = entity;
		}

		@Override
		public boolean evaluate(CollectableEntityField clctefwe) {
			return this.entity.equals(clctefwe.getEntityUID());
		}
	}
	
	/**
	 * inner class <code>LabelComparator</code>. Compares <code>CollectableEntityField</code>s by their labels.
	 */
	public static class LabelComparator implements Comparator<CollectableEntityField> {
		private final Collator collator = LangUtils.getDefaultCollator();

		@Override
        public int compare(CollectableEntityField clctef1, CollectableEntityField clctef2) {
			return this.collator.compare(clctef1.getLabel(), clctef2.getLabel());
		}

	}	// inner class LabelComparator

	/**
	 * inner class <code>GetUID</code>: transforms a <code>CollectableEntityField</code> into that field's UID.
	 */
	public static class GetUID implements Transformer<CollectableEntityField, UID> {

		@Override
        public UID transform(CollectableEntityField clctef) {
			return clctef.getUID();
		}

	}	// inner class GetUID

	/**
	 * field type: undefined. This is needed for reading from the preferences.
	 */
	public static final int TYPE_UNDEFINED = -1;

	/**
	 * field type: value field. A value field has a value, but no id.
	 */
	public static final int TYPE_VALUEFIELD = 1;

	/**
	 * field type: id field. An id field has an id and a value.
	 */
	public static final int TYPE_VALUEIDFIELD = 2;

	/**
	 * @return the UID of this field
	 * formerly known as: String getName();
	 */
	UID getUID();

	/**
	 * @return the UID of the entity this field belongs to.
	 *
	 * @author Thomas Pasch
	 * @since Nuclos 3.1.01
	 * formerly known as: String getEntityUID();
	 */
	UID getEntityUID();

	/**
	 * @return the input format of this field
	 */
	String getFormatInput();

	/**
	 * @return the output format of this field
	 */
	String getFormatOutput();

	/**
	 * @return the type of this field (value or id field)
	 */
	int getFieldType();

	/**
	 * (Often needed) shortcut for this.getFieldType() == TYPE_VALUEIDFIELD.
	 * @return result &lt;--&gt; (this.getFieldType() == TYPE_VALUEIDFIELD)
	 */
	boolean isIdField();

	/**
	 * Two <code>CollectableEntityField</code>s are considered equal iff their names are equal
	 * @param o
	 */
	// @Override
    boolean equals(Object o);

	/**
	 * §postcondition result != null
	 * @return the type of this field, as Java class
	 */
	public Class<?> getJavaClass();
	
	/**
	 * Internal name of the field.
	 * 
	 * @return
	 */
	String getName();

	/**
	 * @return the (default) label that is presented to the user.
	 * It is shown in dialogs, column headers, search conditions etc.
	 * <p>
	 * Warning: Never return Html here!
	 * </p>
	 */
	String getLabel();

	/**
	 * This bean property is used for the tool tip text in Nuclos.
	 * 
	 * @return a description of this field, if any
	 */
	String getDescription();

	/**
	 * @return the maximum length of this field in characters, if any
	 */
	Integer getMaxLength();

	/**
	 * @return the precision of this field, if any
	 */
	Integer getPrecision();

	/**
	 * §precondition isIdField()
	 * @return Is this field restricted to a value list? If no, values not contained in the value list can be entered
	 * also. A typical component for this is a combobox, as opposed to a dropdown.
	 */
	boolean isRestrictedToValueList();

	/**
	 * @return Is this field nullable?
	 */
	boolean isNullable();

	/**
	 * @return Is this field referencing another <code>Collectable</code>?
	 * In a database application, this would be <code>true</code> for foreign key fields.
	 */
	boolean isReferencing();

	/**
	 * @return the parent <code>CollectableEntity</code>, if any.
	 *
	 * @deprecated Not always present.
	 */
	CollectableEntity getCollectableEntity();

	/**
	 * sets the parent <code>CollectableEntity</code>
	 * @param clent the parent <code>CollectableEntity</code>
	 *
	 * @deprecated Not always present.
	 */
	void setCollectableEntity(CollectableEntity clent);

	/**
	 * @return the name of the referenced <code>CollectableEntity</code>, if any.
	 */
	UID getReferencedEntityUID();

	/**
	 * @return the name of the field in the referenced <code>CollectableEntity</code>, if any. That is the field
	 * that this field is a foreign key of. Defaults to <code>"name"</code>.
	 * The field name returned by this method is used for lookups and for displaying the identifier of a referenced Collectable (entity).
	 * In lookups, the field to transfer must be specified as it isn't always the same. For displaying the identifier,
	 * org.nuclos.common.collect.collectable.Collectable#getIdentifierLabel() should be used.
	 *
	 * @deprecated There is no such thing like a "referenced field" - only a whole Collectable can be referenced.
	 */
	@Deprecated
	String getReferencedEntityFieldName();

	/**
	 * §precondition isReferencing()
	 * @return Can the referenced entity be displayed (and possibly edited)? This defaults to <code>true</code>,
	 * but may be <code>false</code> for some instances that are referenced externally.
	 * @deprecated This doesn't belong here! It's not the entity field's responsibility to know about that.
	 */
	@Deprecated
	boolean isReferencedEntityDisplayable();
	
	String getDefaultComponentType();

	/**
	 * @return the default CollectableComponent type for this field.
	 * @see CollectableComponentTypes
	 */
	int getDefaultCollectableComponentType();

	/**
	 * §postcondition result != null
	 * §postcondition result.isNull()
	 * §postcondition result.getFieldType() == this.getFieldType()
	 * @return a null value appropriate for this field.
	 */
	CollectableField getNullField();

	/**
	 * §postcondition result != null
	 * §postcondition result.getFieldType() == this.getFieldType()
	 * §postcondition LangUtils.isInstanceOf(result.getValue(), this.getJavaClass())
	 * @return the default value for this field. The default value is set when an instance of this
	 * field is created. Note that this only applies to situations where a new Collectable is about to
	 * be entered by the user. When specifying a search condition,  <code>getNullField()</code> should be used.
	 */
	CollectableField getDefault();

	/**
	 * @return Some implementation return {@link #getLabel()} <em>but</em> other return
	 * HTML suited for coloring JLabels.
	 */
	// @Override
    String toString();

	/**
	 * sets the security agent for this <code>CollectableEntityField</code>
	 * @param sa
	 */
	<PK> void setSecurityAgent(CefSecurityAgent<PK> sa);

	/**
	 * get the security agent for this <code>CollectableEntityField</code>
	 * @return CollectableEntityFieldSecurityAgent
	 */
	<PK> CefSecurityAgent<PK> getSecurityAgent();

	/**
	 * checks whether read permission is granted to this field or not
	 */
	boolean isReadable();

	/**
	 * checks whether write permission is granted to this field or not
	 */
	boolean isWritable();

	/**
	 * checks whether delete permission is granted to this field or not
	 */
	boolean isRemovable();

	boolean isCalculated();

	boolean isCalcOndemand();

	boolean isCalcAttributeAllowCustomization();

	boolean isLocalized();

	boolean isHidden();

	boolean isModifiable();

	String getExportLabel();

}	// interface CollectableEntityField
