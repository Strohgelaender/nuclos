//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable;

import org.nuclos.common.UID;

/**
 * Factory that creates <code>CollectableFieldProvider</code>s.
 * 
 * §todo merge the three methods to one.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */

public interface CollectableFieldsProviderFactory {

	/**
	 * called for provider type="default"
	 * 
	 * §todo add precondition sEntityName != null
	 * 
	 * @param fieldUid
	 */
	CollectableFieldsProvider newDefaultCollectableFieldsProvider(UID fieldUid);

	/**
	 * called for provider type="dependant"
	 * 
	 * §todo add precondition sEntityName != null
	 * 
	 * @param fieldUid
	 */
	CollectableFieldsProvider newDependantCollectableFieldsProvider(UID fieldUid);

	/**
	 * called for provider type="custom"
	 * 
	 * §todo add precondition sEntityName != null
	 * 
	 * @param sCustomType
	 * @param fieldUid
	 * 
	 * @deprecated Use {@link #newCustomCollectableFieldsProvider(ValueListProviderType, UID)} instead.
	 */
	CollectableFieldsProvider newCustomCollectableFieldsProvider(String sCustomType, UID fieldUid);

	CollectableFieldsProvider newCustomCollectableFieldsProvider(ValueListProviderType vlpt, UID fieldUid);

}  // interface CollectableFieldsProviderFactory
