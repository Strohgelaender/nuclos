//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.util.List;



public class NuclosDetailToolBarConfiguration extends ToolBarConfiguration {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -708748031203879792L;
	public static final NuclosDetailToolBarConfiguration DEFAULT = new NuclosDetailToolBarConfiguration(
			new ToolBarItemGroup(MAIN_TOOL_BAR_KEY)
				.addItem(NuclosToolBarItems.COLLECT_INDICATOR)
				.addItem(NuclosToolBarItems.SAVE)
				.addItem(NuclosToolBarItems.REFRESH)
				.addItem(NuclosToolBarItems.NEW)
				.addItem(NuclosToolBarItems.DELETE)
				.addItem(NuclosToolBarItems.newExtras()
						.addItem(NuclosToolBarItems.CLONE)
						.addItem(NuclosToolBarItems.DELETE_REAL)
						.addItem(NuclosToolBarItems.MENU_SEPARATOR)
						.addItem(NuclosToolBarItems.BOOKMARK)
						.addItem(NuclosToolBarItems.OPEN_IN_NEW_TAB)
						.addItem(NuclosToolBarItems.SHOW_IN_EXPLORER)
						.addItem(NuclosToolBarItems.MENU_SEPARATOR)
						.addItem(NuclosToolBarItems.EXECUTE_RULE)
						.addItem(NuclosToolBarItems.SHOW_STATE_HISTORY)
						.addItem(NuclosToolBarItems.SHOW_HISTORY)
						) 
				.addItem(NuclosToolBarItems.PRINT_DETAILS)
				.addItem(NuclosToolBarItems.CHANGE_STATE)
				.addItem(NuclosToolBarItems.GENERATE)
				.addItem(NuclosToolBarItems.LOCALIZATION)
				.addItem(NuclosToolBarItems.LOCK)
				.addItem(NuclosToolBarItems.INFO)
				.addItem(NuclosToolBarItems.HELP)
			);
	
	@Override
	public ToolBarConfiguration getDefaultConfiguration() {
		return DEFAULT;
	}

	@Override
	public List<ToolBarItem> getConfigurableItems(Context context) {
		List<ToolBarItem> result = super.getConfigurableItems(context);
		result.add(NuclosToolBarItems.COLLECT_INDICATOR);
		result.add(NuclosToolBarItems.EXTRAS);
		result.add(NuclosToolBarItems.MENU_SEPARATOR);
		result.add(NuclosToolBarItems.SAVE);
		result.add(NuclosToolBarItems.REFRESH);
		result.add(NuclosToolBarItems.NEW);
		result.add(NuclosToolBarItems.DELETE);
		result.add(NuclosToolBarItems.CLONE);
		result.add(NuclosToolBarItems.BOOKMARK);
		result.add(NuclosToolBarItems.OPEN_IN_NEW_TAB);
		result.add(NuclosToolBarItems.SHOW_IN_EXPLORER);
		result.add(NuclosToolBarItems.GENERATE);
		result.add(NuclosToolBarItems.INFO);
		result.add(NuclosToolBarItems.EXECUTE_RULE);
		result.add(NuclosToolBarItems.SHOW_HISTORY);
		result.add(NuclosToolBarItems.PRINT_DETAILS);
		result.add(NuclosToolBarItems.RECORDNAV_FIRST);
		result.add(NuclosToolBarItems.RECORDNAV_PREVIOUS);
		result.add(NuclosToolBarItems.RECORDNAV_NEXT);
		result.add(NuclosToolBarItems.RECORDNAV_LAST);
		result.add(NuclosToolBarItems.LOCALIZATION);
		result.add(NuclosToolBarItems.LOCK);
		if (context == Context.UNSPECIFIED || context == Context.ENTITY_MODULE) {
			result.add(NuclosToolBarItems.CHANGE_STATE);
			result.add(NuclosToolBarItems.DELETE_REAL);
			result.add(NuclosToolBarItems.SHOW_STATE_HISTORY);
		}
		if (context == Context.UNSPECIFIED || context == Context.ENTITY_MASTERDATA) {
			
		}
		return result;
	}
	
	public NuclosDetailToolBarConfiguration(ToolBarItemGroup mainToolBar) {
		super(mainToolBar);
	}

	public NuclosDetailToolBarConfiguration(String value) {
		super(value);
	}
	
	public NuclosDetailToolBarConfiguration() {
	}

}
