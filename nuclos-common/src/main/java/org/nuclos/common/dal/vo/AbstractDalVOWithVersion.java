//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import org.nuclos.common.UID;
import org.nuclos.common2.InternalTimestamp;

public abstract class AbstractDalVOWithVersion<PK> extends AbstractDalVOBasic<PK> implements IVersionVO<PK> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4939556001344573244L;
	private InternalTimestamp createdAt;
	private String createdBy;
	private InternalTimestamp changedAt;
	private String changedBy;
	private int version = -1;
	private boolean systemFieldsOnRequ = false;
	
	public AbstractDalVOWithVersion(UID entityUID) {
		super(entityUID);
	}
	
	public AbstractDalVOWithVersion(AbstractDalVOWithVersion<PK> dalVO) {
		super(dalVO);
		this.setChangedAt(dalVO.getChangedAt());
		this.setChangedBy(dalVO.getChangedBy());
		this.setCreatedAt(dalVO.getCreatedAt());
		this.setCreatedBy(dalVO.getCreatedBy());
		this.setVersion(dalVO.getVersion());
	}
	
	@Override
	public InternalTimestamp getCreatedAt() {
		return createdAt;
	}

	@Override
	public void setCreatedAt(InternalTimestamp createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public InternalTimestamp getChangedAt() {
		return changedAt;
	}

	@Override
	public void setChangedAt(InternalTimestamp changedAt) {
		this.changedAt = changedAt;
	}

	@Override
	public String getChangedBy() {
		return changedBy;
	}

	@Override
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}

	@Override
	public int getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		if (version == null) {
			version = Integer.valueOf(-1);
		}
		this.version = version;
	}
	
	public boolean systemFieldsOnRequest() {
		return systemFieldsOnRequ;
	}
	
	public void setSystemFieldsOnRequest(boolean b) {
		systemFieldsOnRequ = b;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("pk=").append(getPrimaryKey());
		result.append(",version=").append(getVersion());
		result.append("]");
		return result.toString();
	}
	
}
