//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.PredicateUtils;

public class DependentDataMap implements IDependentDataMap {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2345951827436922516L;

//	private static final Logger LOG = Logger.getLogger(DependentDataMap.class);
	
	//
	
	public static class DependentKey implements IDependentKey, Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -3956345989614275706L;
		
		private final UID dependentRefFieldUID;

		public DependentKey(UID dependentRefFieldUID) {
			super();
			if (dependentRefFieldUID == null) {
				throw new IllegalArgumentException("dependentRefFieldUID of DependentKey must not be null");
			}
			this.dependentRefFieldUID = dependentRefFieldUID;
		}

		@Override
		public final UID getDependentRefFieldUID() {
			return dependentRefFieldUID;
		}

		@Override
		public String toString() {
			return "DependentKey [dependentRefFieldUID=" + dependentRefFieldUID + "]";
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((dependentRefFieldUID == null) ? 0 : dependentRefFieldUID.hashCode());
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj instanceof DependentKey) {
				DependentKey other = (DependentKey) obj;
				return RigidUtils.equal(dependentRefFieldUID, other.dependentRefFieldUID);
			}
			return false;
		}
		
	}
	
	protected final DepMap dependents = new DepMap();
	
	public DependentDataMap() {
	}
	
	public static IDependentKey createDependentKey(FieldMeta<?> f) {
		return new DependentKey(f.getUID());
	}
	
	public static IDependentKey createDependentKey(UID fieldUID) {
		return new DependentKey(fieldUID);
	}

	@Override
	public boolean hasData(FieldMeta<?> referencingField) {
		return dependents.containsKey(createDependentKey(referencingField));
	}
	
	@Override
	public boolean hasData(IDependentKey dependentKey) {
		return dependents.containsKey(dependentKey);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EntityObjectVO<?>> getData(FieldMeta<?> referencingField) {
		return dependents.getValues(createDependentKey(referencingField));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EntityObjectVO<?>> getData(IDependentKey dependentKey, Flag... flags) {
		final List<EntityObjectVO<?>> allData = dependents.getValues(dependentKey);

		if (ArrayUtils.isEmpty(flags)) {
			return allData;
		}

		final List<EntityObjectVO<?>> result = new ArrayList<>();
		for (EntityObjectVO<?> eo : allData) {
			if (eo.matches(flags)) {
				result.add(eo);
			}
		}

		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <PK> List<EntityObjectVO<PK>> getDataPk(FieldMeta<?> referencingField) {
		return dependents.getValues(createDependentKey(referencingField));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <PK> List<EntityObjectVO<PK>> getDataPk(IDependentKey dependentKey) {
		return dependents.getValues(dependentKey);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <PK> void addData(FieldMeta<?> referencingField, EntityObjectVO<PK> eoDependent) {
		dependents.addValue(createDependentKey(referencingField), eoDependent);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <PK> void addData(IDependentKey dependentKey, EntityObjectVO<PK> eoDependent) {
		dependents.addValue(dependentKey, eoDependent);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <PK> void addAllData(FieldMeta<?> referencingField, Collection<EntityObjectVO<PK>> colleoDependents) {
		dependents.addAllValues(createDependentKey(referencingField), colleoDependents);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <PK> void addAllData(IDependentKey dependentKey, Collection<EntityObjectVO<PK>> colleoDependents) {
		dependents.addAllValues(dependentKey, colleoDependents);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <PK> void setData(FieldMeta<?> referencingField, Collection<EntityObjectVO<PK>> colleoDependents) {
		final IDependentKey key = createDependentKey(referencingField);
		dependents.removeKey(key);
		dependents.addAllValues(key, colleoDependents);
		
		assert getData(key).size() == colleoDependents.size();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <PK> void setData(IDependentKey dependentKey, Collection<EntityObjectVO<PK>> colleoDependents) {
		this.dependents.removeKey(dependentKey);
		this.dependents.addAllValues(dependentKey, colleoDependents);
		
		assert getData(dependentKey).size() == colleoDependents.size();
	}
	
	@Override
	public void removeKey(FieldMeta<?> referencingField) {
		this.dependents.removeKey(createDependentKey(referencingField));
	}
	
	@Override
	public void removeKey(IDependentKey dependentKey) {
		this.dependents.removeKey(dependentKey);
	}
	
	@SuppressWarnings("unchecked")
	public Map<IDependentKey, List<EntityObjectVO<?>>> getRoDataMap() {
		return dependents.getRoMap();
	}

	@Override
	public boolean isEmpty() {
		return dependents.isEmpty();
	}
	
	@SuppressWarnings("unchecked")
	@Override
		public Set<IDependentKey> getKeySet() {
			return dependents.keySet();
		}

	@Override
	public Set<UID> getReferencingFieldUids() {
		Set<UID> result = new HashSet<UID>();
		for (Object key : dependents.keySet()) {
			result.add(((IDependentKey)key).getDependentRefFieldUID());
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean areAllDependentsNew() {
		return CollectionUtils.forall(dependents.getAllValues(), 
				PredicateUtils.transformedInputIsNull(new EntityObjectVO.GetId()));
	}

	@Override
	public boolean getPendingChanges() {
		for (Object o : dependents.getAllValues()) {
			EntityObjectVO<?> eo = (EntityObjectVO<?>) o;
			if (eo.isFlagNew() || eo.isFlagRemoved() || eo.isFlagUpdated())
				return true;
			if (eo.getDependents().getPendingChanges())
				return true;
		}
		return false;
	}
	
	@Override
	public void clear() {
		dependents.clear();
	}
	
	/**
	 * 
	 * Dependent Map for primary key type PK compatibility
	 *
	 * @param <PK>
	 */
	private static class DepMap<PK> extends HashMap<IDependentKey, List<EntityObjectVO<PK>>> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2729522562145593139L;

		private List<EntityObjectVO<PK>> getOrCreateValues(IDependentKey key) {
			List<EntityObjectVO<PK>> result = get(key);
			if (result == null) {
				result = new ArrayList<EntityObjectVO<PK>>();
				put(key, result);
			}
			return result;
		}

		public List<EntityObjectVO<PK>> getValues(IDependentKey key) {
			return CollectionUtils.emptyListIfNull(get(key));
		}
		
		public Map<IDependentKey, List<EntityObjectVO<PK>>> getRoMap() {
			return Collections.unmodifiableMap(this);
		}
		
		public void addValue(IDependentKey key, EntityObjectVO<PK> value) {
			this.getOrCreateValues(key).add(value);
		}
		
		public void addAllValues(IDependentKey key, Collection<? extends EntityObjectVO<PK>> values) {
			if (!values.isEmpty()) {
				this.getOrCreateValues(key).addAll(values);
			}
		}
		
		public Set<EntityObjectVO<PK>> getAllValues() {
			return CollectionUtils.unionAll(values());
		}
		
		public void removeKey(IDependentKey key) {
			remove(key);
		}
		
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if  (obj instanceof DependentDataMap) {
			DependentDataMap other = (DependentDataMap)obj;
			return RigidUtils.equal(dependents, other.dependents);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return RigidUtils.hashCode(dependents);
	}

	@Override
	public String toString() {
		return "DependentDataMap:hashCode=" + hashCode();
	}
}
