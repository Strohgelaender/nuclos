//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal;

import org.apache.log4j.Logger;
import org.nuclos.common.E;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class DalSupportForMD {

	private static final Logger LOG = Logger.getLogger(DalSupportForMD.class);

	private DalSupportForMD() {
		// Never invoked.
	}

	public static <PK> MasterDataVO<PK> wrapEntityObjectVO(EntityObjectVO<PK> eo) {
		return new MasterDataVO<PK>(eo, false);
	}

	public static EntityTreeViewVO wrapEntityObjectVOAsSubNode(EntityObjectVO<UID> eo) {
		EntityTreeViewVO vo = new EntityTreeViewVO(eo.getPrimaryKey(),
			eo.getFieldUid(E.ENTITYSUBNODES.originentityid),
			eo.getFieldUid(E.ENTITYSUBNODES.entity),
			eo.getFieldUid(E.ENTITYSUBNODES.field),
			eo.getFieldValue(E.ENTITYSUBNODES.foldername),
			eo.getFieldValue(E.ENTITYSUBNODES.active),
			eo.getFieldValue(E.ENTITYSUBNODES.sortOrder),
			eo.getFieldValue(E.ENTITYSUBNODES.inheritSubNodes),
			eo.getFieldUid(E.ENTITYSUBNODES.fieldRoot),
			eo.getFieldUid(E.ENTITYSUBNODES.parentSubNode),
			eo.getFieldValue(E.ENTITYSUBNODES.node),
			eo.getFieldValue(E.ENTITYSUBNODES.nodeTooltip),
			eo.getFieldValue(E.ENTITYSUBNODES.groupBy),
			eo.getFieldValue(E.ENTITYSUBNODES.isShowEntityFolder)
		);

		return vo;
	}

	/**
	 *
	 * @return
	 */
	public static <PK> Transformer<MasterDataVO<PK>, EntityObjectVO<PK>> getTransformerToEntityObjectVO() {
		return new Transformer<MasterDataVO<PK>, EntityObjectVO<PK>>() {
			@Override
            public EntityObjectVO<PK> transform(MasterDataVO<PK> md) {
                return md.getEntityObject();
            }
		};
	}

}
