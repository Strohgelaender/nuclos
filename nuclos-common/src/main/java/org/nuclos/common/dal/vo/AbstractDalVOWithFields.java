//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.JsonObject;

import org.apache.log4j.Logger;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.JsonUtils;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.InternalTimestamp;

public abstract class AbstractDalVOWithFields<T, PK> extends AbstractDalVOWithVersion<PK> implements IDalWithFieldsVO<T, PK>
//	TODO MULTINUCLET: Enable for backwards compatibility after refactoring... 
//	, IDalWithFieldsVODeprecated<T, PK>
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1097274371180207160L;

	private static final Logger LOG = Logger.getLogger(AbstractDalVOWithFields.class);
	
	/**
	 * Fill only when needed...
	 */
	private static transient IMetaProvider MDP;	

	protected Map<UID, UID> mapFieldUid = new HashMap<UID, UID>();
	
	protected Map<UID, Long> mapFieldId = new HashMap<UID, Long>();

	protected Map<UID, T> mapField = new HashMap<UID, T>();
	
	private boolean bComplete = false;
	
	private boolean bSkipLoadingFromDB = false;
	
	private boolean bIsInsertWoId = false;

	private UID createdAtUID;
	private UID createdByUID;
	private UID changedAtUID;
	private UID changedByUID;
	private UID versionUID;
	
	// NOTICE: Custom serialization, don't forget to extend!

	public AbstractDalVOWithFields(UID dalEntity) {
		super(dalEntity);
		this.createdAtUID = SF.CREATEDAT.getUID(dalEntity);
		this.createdByUID = SF.CREATEDBY.getUID(dalEntity);
		this.changedAtUID = SF.CHANGEDAT.getUID(dalEntity);
		this.changedByUID = SF.CHANGEDBY.getUID(dalEntity);
		this.versionUID = SF.VERSION.getUID(dalEntity);
	}

	@Override
	public final boolean isComplete() {
		return bComplete;
	}

	//FIXME: NUCLOS-4773 There is huge confusion about the double use of "complete".
	//In EntityObjectProcess this is set, if the EO has all columns used ("completeColumns")
	//In other case there are other purposes (e.g. ResPlanController). 
	//Either way it is not need in REST/webclient
	@Override
	public final void setComplete(boolean bComplete) {
		this.bComplete = bComplete;
	}

	@Override
	public void setCreatedAt(InternalTimestamp createdAt) {
		super.setCreatedAt(createdAt);
		if (mapField != null /*&& getFields().containsKey("createdAt")*/)
			mapField.put(createdAtUID, (T)createdAt);
	}

	@Override
	public void setCreatedBy(String createdBy) {
		super.setCreatedBy(createdBy);
		if (mapField != null /*&& getFields().containsKey("createdBy")*/)
			mapField.put(createdByUID, (T)createdBy);
	}

	@Override
	public void setChangedAt(InternalTimestamp changedAt) {
		super.setChangedAt(changedAt);
		if (mapField != null /*&& getFields().containsKey("changedAt")*/)
			mapField.put(changedAtUID, (T)changedAt);
	}

	@Override
	public void setChangedBy(String changedBy) {
		super.setChangedBy(changedBy);
		if (mapField != null /*&& getFields().containsKey("changedBy")*/)
			mapField.put(changedByUID, (T)changedBy);
	}

	@Override
	public void setVersion(Integer version) {
		if (version == null) {
			version = -1;
		}
		super.setVersion(version);
		if (mapField != null /*&& getFields().containsKey("version")*/)
			mapField.put(versionUID, (T)version);
	}

	@Override
	public boolean hasFields() {
		return true;
	}

	@Override
	public Map<UID, Long> getFieldIds() {
		return Collections.unmodifiableMap(this.mapFieldId);
	}

	public boolean isSkipLoadingFromDB() {
		return bSkipLoadingFromDB;
	}
	
	public boolean isbInsertWoId() {
		return bIsInsertWoId;
	}
	
	public boolean setInsertWoId(boolean b) {
		return bIsInsertWoId = b;
	}
	
	@Override
	public Map<UID, UID> getFieldUids() {
		return Collections.unmodifiableMap(this.mapFieldUid);
	}

	@Override
	public Map<UID, T> getFieldValues() {
		return Collections.unmodifiableMap(this.mapField);
	}
	
	@Override
	public Long getFieldId(UID field) {
		return mapFieldId.get(field);
	}

	public void setSkipLoadingFromDB(boolean b) {
		bSkipLoadingFromDB = b;
	}
	
	@Override
	public void setFieldId(UID field, Long id) {
		mapFieldId.put(field, id);
	}
	
	@Override
	public void removeFieldId(UID field) {
		mapFieldId.remove(field);
	}
	
	@Override
	public UID getFieldUid(UID field) {
		return mapFieldUid.get(field);
	}
	
	@Override
	public void setFieldUid(UID field, UID uid) {
		mapFieldUid.put(field, uid);
	}
	
	@Override
	public void removeFieldUid(UID field) {
		mapFieldUid.remove(field);
	}
	
	@Override
	public boolean exist(UID field) {
		if (mapField.containsKey(field)) {
			return true;
		}
		if (mapFieldId.containsKey(field)) {
			return true;
		}
		if (mapFieldUid.containsKey(field)) {
			return true;
		}
		return false;
	}

	protected static IMetaProvider getMetaProvider() {
		if (MDP == null && SpringApplicationContextHolder.isSpringReady()) {
			MDP =  (IMetaProvider) SpringApplicationContextHolder.getBean("metaDataProvider");
		}
		return MDP;
	}
	
	@Override
	public <S> S getFieldValue(UID field, Class<S> cls) {
		final Object value = getFieldValue(field);
		final S result;
		// Enum support
		if (Enum.class.isAssignableFrom(cls)) {
			if (value == null) {
				result = null;
			} else if (value instanceof String) {
				result = (S) Enum.valueOf((Class) cls, (String) value);
			} else {
				result = (S) value;
			}
		} else {
			if (value == null) {
				result = null;
			} else {
				final Class<?> vclazz = value.getClass();
				if (/* Enum.class.isAssignableFrom(vclazz) && */ String.class.equals(cls)) {
					// general toString support
								
					result = (S) value.toString();
					
				} else {
					try {
						result = cls.cast(value);
					} catch (ClassCastException e) {
						LOG.error("On " + this + " field " + field + " value " + value + " expected type " + cls
								+ " real type " + value.getClass(), e);
						throw e;
					}
				}
			}
		}
		return result;
	}
	
	@Override
	public void setFieldValue(UID field, T obj) {
		mapField.put(field, obj);
		if (versionUID.equals(field)) {
			super.setVersion((Integer) obj);
		} else
		if (createdAtUID.equals(field)) {
			InternalTimestamp timestamp;
			if (obj instanceof Date) {
				timestamp = InternalTimestamp.toInternalTimestamp((Date) obj);
			} else {
				timestamp = (InternalTimestamp) obj;
			}
			super.setCreatedAt(timestamp);
		} else
		if (createdByUID.equals(field)) {
			super.setCreatedBy((String) obj);
		} else
		if (changedAtUID.equals(field)) {
			InternalTimestamp timestamp;
			if (obj instanceof Date) {
				timestamp = InternalTimestamp.toInternalTimestamp((Date) obj);
			} else {
				timestamp = (InternalTimestamp) obj;
			}
			super.setChangedAt(timestamp);
		} else
		if (changedByUID.equals(field)) {
			super.setChangedBy((String) obj);
		}
	}
	
	@Override
	public void removeFieldValue(UID field) {
		mapField.remove(field);
	}

	@Override
	public T getFieldValue(UID field) {
		return mapField.get(field);
	}
	
	private static class SerializableJsonObject implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8763140627594315916L;
		
		final String jsonObjectString;

		public SerializableJsonObject(String jsonObjectString) {
			super();
			this.jsonObjectString = jsonObjectString;
		}
		
	}
	
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeBoolean(false); // This was the removed flag "thin". Use this slot for a new flag
		out.writeBoolean(bComplete);
		out.writeBoolean(bSkipLoadingFromDB);
		out.writeBoolean(bIsInsertWoId);
		out.writeObject(createdAtUID);
		out.writeObject(createdByUID);
		out.writeObject(changedAtUID);
		out.writeObject(changedByUID);
		out.writeObject(versionUID);
		out.writeObject(mapFieldUid);
		out.writeObject(mapFieldId);
		Map<UID, T> mapSerializableField = new HashMap<UID, T>(mapField.size());
		Iterator<Entry<UID, T>> it = mapField.entrySet().iterator();
		while (it.hasNext()) {
			Entry<UID, T> entry = it.next();
			if (entry.getValue() instanceof JsonObject) {
				SerializableJsonObject sjo = new SerializableJsonObject(JsonUtils.objectToString((JsonObject) entry.getValue()));
				mapSerializableField.put(entry.getKey(), (T) sjo);
			}
			if (entry.getValue() instanceof Serializable) {
				mapSerializableField.put(entry.getKey(), entry.getValue());
			}
		}
		out.writeObject(mapSerializableField);
	}
	
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.readBoolean(); // This was for the removed flag "thin". It is "false" in most cases; Use it for a new flag!
		bComplete = in.readBoolean();
		bSkipLoadingFromDB = in.readBoolean();
		bIsInsertWoId = in.readBoolean();
		createdAtUID = (UID) in.readObject();
		createdByUID = (UID) in.readObject();
		changedAtUID = (UID) in.readObject();
		changedByUID = (UID) in.readObject();
		versionUID = (UID) in.readObject();
		mapFieldUid = (Map<UID, UID>) in.readObject();
		mapFieldId = (Map<UID, Long>) in.readObject();
		Map<UID, T> mapSerializableField = (Map<UID, T>) in.readObject();
		mapField = new HashMap<UID, T>(mapSerializableField.size());
		Iterator<Entry<UID, T>> it = mapSerializableField.entrySet().iterator();
		while (it.hasNext()) {
			Entry<UID, T> entry = it.next();
			if (entry.getValue() instanceof SerializableJsonObject) {
				SerializableJsonObject sjo = (SerializableJsonObject)entry.getValue();
				mapField.put(entry.getKey(), (T) JsonUtils.stringToObject(sjo.jsonObjectString));
			} else {
				mapField.put(entry.getKey(), entry.getValue());
			}
		}
	}

	public <PK> Collection<UID> getFieldsWithDifferentValue(AbstractDalVOWithFields<?, PK> other) {
		Collection<UID> differentFields = new ArrayList<>();
		for (FieldMeta<?> fMeta : getMetaProvider().getAllEntityFieldsByEntity(getDalEntity()).values()) {
			if (Boolean.FALSE.equals(isFieldValueEqual(fMeta, other))) {
				differentFields.add(fMeta.getUID());
			}
		}
		return differentFields;
	}

	protected final <PK> Boolean isFieldValueEqual(FieldMeta<?> fMeta, AbstractDalVOWithFields<?, PK> other) {
		// No equality check for calculated attributes, see NUCLOS-5099 / ThinObjects and for VersionFields (Version, CreatedAt)
		if (fMeta.isCalculated() || fMeta.isVersionField()) {
			return null;
		}

		UID fuid = fMeta.getUID();
		if ((fMeta.getForeignEntity() != null || fMeta.getUnreferencedForeignEntity() != null) && !fMeta.isFileDataType()) {
			if (!RigidUtils.equal(getFieldId(fuid), other.getFieldId(fuid))
					|| !RigidUtils.equal(getFieldUid(fuid), other.getFieldUid(fuid))) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Not equalFields: E=" + getMetaProvider().getEntity(getDalEntity()).getEntityName()
							+ ", F=" + fMeta.getFieldName() + ", (U)ID=[" + getFieldId(fuid) + ", " + other.getFieldId(fuid)
							+ ", " + getFieldUid(fuid) + ", " + other.getFieldUid(fuid) + "]");
				}
				return false;
			}
		} else {
			if (fMeta.getJavaClass().equals(ByteArrayCarrier.class) || fMeta.getJavaClass().equals(byte[].class) ||
					fMeta.getJavaClass().equals(NuclosImage.class) || fMeta.getJavaClass().equals(org.nuclos.api.NuclosImage.class)) {
				byte[] barry1 = null;
				byte[] barry2 = null;
				if (fMeta.getJavaClass().equals(ByteArrayCarrier.class)) {
					ByteArrayCarrier bac = getFieldValue(fuid, ByteArrayCarrier.class);
					if (bac != null) {
						barry1 = bac.getData();
					}
					bac = other.getFieldValue(fuid, ByteArrayCarrier.class);
					if (bac != null) {
						barry2 = bac.getData();
					}
				} else if (fMeta.getJavaClass().equals(NuclosImage.class) ||
						fMeta.getJavaClass().equals(org.nuclos.api.NuclosImage.class)) {
					org.nuclos.api.NuclosImage nuclosImage = getFieldValue(fuid, org.nuclos.api.NuclosImage.class);
					if (nuclosImage != null) {
						barry1 = nuclosImage.getContent();
					}
					nuclosImage = other.getFieldValue(fuid, org.nuclos.api.NuclosImage.class);
					if (nuclosImage != null) {
						barry2 = nuclosImage.getContent();
					}
				} else {
					barry1 = getFieldValue(fuid, byte[].class);
					barry2 = other.getFieldValue(fuid, byte[].class);
				}
				if (!Arrays.equals(barry1, barry2)) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Not equalFields: E=" + getMetaProvider().getEntity(getDalEntity()).getEntityName()
								+ ", F=" + fMeta.getFieldName() + ", value=[" + barry1 + ", " + barry2 + "]");
					}
					return false;
				}
			} else {
				if (!RigidUtils.equal(getFieldValue(fuid), other.getFieldValue(fuid))) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Not equalFields: E=" + getMetaProvider().getEntity(getDalEntity()).getEntityName()
								+ ", F=" + fMeta.getFieldName() + ", value=[" + getFieldValue(fuid) + ", " + other.getFieldValue(fuid) + "]");
					}
					return false;
				}
			}
		}
		return true;
	}

}
