//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.util.Comparator;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

public class EntityObjectValueComparator<PK> implements Comparator<EntityObjectVO<PK>> {
	
	private final UID valueField;
	
	public EntityObjectValueComparator(UID valueField) {
		this.valueField = valueField;
	}

	@Override
	public int compare(EntityObjectVO<PK> o1, EntityObjectVO<PK> o2) {
		int result = compNull(o1, o2);
		if (result == 0) {
			final Object v1 = o1.getFieldValue(valueField);
			final Object v2 = o2.getFieldValue(valueField);
			result = compNull(v1, v2);
			if (v1 != null && Comparable.class.isAssignableFrom(v1.getClass())) {
				final Comparable c1 = (Comparable) v1;
				final Comparable c2 = (Comparable) v2;
				result = LangUtils.compare(c1, c2);
			}
		}
		if (result == 0) {
			final Object pk1 = o1.getPrimaryKey();
			final Object pk2 = o2.getPrimaryKey();
			result = compNull(pk1, pk2);
			if (pk1 != null && Comparable.class.isAssignableFrom(pk1.getClass())) {
				final Comparable c1 = (Comparable) pk1;
				final Comparable c2 = (Comparable) pk2;
				result = LangUtils.compare(c1, c2);
			}
		}
		return result;
	}
	
	private int compNull(Object o1, Object o2) {
		int result = 0;
		if (o1 == null) {
			if (o2 != null) {
				result = 1;
			}
		} else {
			if (o2 == null) {
				result = -1;
			}
		}
		return result;
	}

}
