package org.nuclos.common.dal.vo;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;

public class DataLanguageMap implements IDataLanguageMap {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6939548668431799515L;
	private Map<UID, DataLanguageLocalizedEntityEntry> dataLanguageMap;
	
	public DataLanguageMap() {
		this.dataLanguageMap = new HashMap<UID, DataLanguageLocalizedEntityEntry>();
	}
	
	@Override
	public Map<UID, DataLanguageLocalizedEntityEntry> getLanguageMap() {
		return dataLanguageMap;
	}

	@Override
	public void setLanguageMap(Map<UID, DataLanguageLocalizedEntityEntry> dataLanguageMap) {
		this.dataLanguageMap = dataLanguageMap;
	}

	@Override
	public void setDataLanguage(UID DataLanguage,
			DataLanguageLocalizedEntityEntry entity) {
		this.dataLanguageMap.put(DataLanguage, entity);
	}

	@Override
	public DataLanguageLocalizedEntityEntry getDataLanguage(UID DataLanguage) {
		return this.dataLanguageMap.get(DataLanguage);
	}

	@Override
	public void clear() {
		if(this.dataLanguageMap != null) {
			this.dataLanguageMap.clear();
		}
	}

	@Override
	public IDataLanguageMap clone() {
		return this.copy(true);
	}
	
	@Override
	public IDataLanguageMap copy() {
		return this.copy(false);
	}
	
	private IDataLanguageMap copy(boolean isCloneWithKeys) {
		IDataLanguageMap retVal = new DataLanguageMap();
		
		for (UID language : this.getLanguageMap().keySet()) {
			DataLanguageLocalizedEntityEntry dataLanguageLocalizedEntityEntry = this.getLanguageMap().get(language);
			if(dataLanguageLocalizedEntityEntry != null) {
				retVal.getLanguageMap().put(language, 
						isCloneWithKeys ? dataLanguageLocalizedEntityEntry.clone() : dataLanguageLocalizedEntityEntry.copy());										
			} else {
				retVal.getLanguageMap().put(language, null);
			}
		}
		
		return retVal;
	}

	 public boolean equals(Object obj) {
		 boolean retVal = true;
				 
		 if (obj != null && obj instanceof DataLanguageMap) {
			 DataLanguageMap toCompare = (DataLanguageMap) obj;
			 
			 for (UID lang : toCompare.getLanguageMap().keySet()) {
				 DataLanguageLocalizedEntityEntry entry = toCompare.getLanguageMap().get(lang);
				 if (this.getDataLanguage(lang) == null) {
					 return false;
				 }
				 
				 for (Object fieldObj : entry.getFieldValues().keySet()) {
					 if (!this.getDataLanguage(lang).getFieldValue((UID) fieldObj).equals(
							 entry.getFieldValue((UID) fieldObj))) {
						 return false;
					 }
				 }
			 }
		 } else {
			 return false;
		 }
		 
		 return retVal;
	 }
	
}
