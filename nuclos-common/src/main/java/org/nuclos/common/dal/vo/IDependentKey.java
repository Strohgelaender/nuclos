package org.nuclos.common.dal.vo;

import org.nuclos.common.UID;

/**
 * Interface to {@link org.nuclos.common.dal.vo.DependentDataMap.DependenceKey}.
 * 
 * @since Nuclos 4.11
 */
public interface IDependentKey {

	UID getDependentRefFieldUID();

}
