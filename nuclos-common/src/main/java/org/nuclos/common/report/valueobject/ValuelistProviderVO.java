//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.util.Locale;

import org.nuclos.common.UID;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.common.valueobject.NuclosValueObject;

public class ValuelistProviderVO extends DatasourceVO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1506187964301279989L;
	public static final String DATASOURCE_IDFIELD = "id-fieldname";
	public static final String DATASOURCE_NAMEFIELD = "fieldname";
	public static final String DATASOURCE_DEFAULTMARKERFIELD = "default-fieldname";
	public static final String DATASOURCE_VALUELISTPROVIDER = "valuelistProvider";
	public static final String DATASOURCE_SEARCHMODE = "searchmode";
	
	private String sDetailsearchdescriptionDe;
	private String sDetailsearchdescriptionEn;

	public static enum Type implements KeyEnum<String> {

		DEFAULT("default"),DEPENDANT("dependant"),NAMED("named"), DS("ds");
		
		private final String type;
		
		private Type(String type) {
			this.type = type;
		}

		@Override
		public String getValue() {
			return getType();
		}
		
		public String getType() {
			return type;
		}
		
		@Override
		public String toString() {
			return getType();
		}		
	}
	
	public ValuelistProviderVO(final NuclosValueObject<UID> evo, final String name,
		final String sDescription, final String sDetailsearchdescriptionDE, final String sDetailsearchdescriptionEN, final Boolean bValid, final String sDatasourceXML, final UID nucletUID) {
		super(evo, name, sDescription, bValid, sDatasourceXML, nucletUID, PERMISSION_NONE);
		this.sDetailsearchdescriptionDe = sDetailsearchdescriptionDE;
		this.sDetailsearchdescriptionEn = sDetailsearchdescriptionEN;
	}

	public ValuelistProviderVO(final String name, final String sDescription, final String sDetailsearchdescriptionDE, final String sDetailsearchdescriptionEN,
		final String sDatasourceXML, final Boolean bValid, final UID nucletUID) {
		super(name, sDescription, sDatasourceXML, bValid, nucletUID);
		this.sDetailsearchdescriptionDe = sDetailsearchdescriptionDE;
		this.sDetailsearchdescriptionEn = sDetailsearchdescriptionEN;
	}
	
	public String getDetailsearchdescription(Locale locale) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			return sDetailsearchdescriptionDe;
		} else {
			return sDetailsearchdescriptionEn;
		}
	}
	
	public void setDetailsearchdescription(String sDetailssearchdescription, Locale locale) {
		if (Locale.GERMAN.equals(locale) || Locale.GERMANY.equals(locale)) {
			this.sDetailsearchdescriptionDe = sDetailssearchdescription;
		} else {
			this.sDetailsearchdescriptionEn = sDetailssearchdescription;
		}
	}
	
	public String getDetailsearchdescription() {
		return getDetailsearchdescription(SpringLocaleDelegate.getInstance().getLocale());
	}
}
