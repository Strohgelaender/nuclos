package org.nuclos.common.report.valueobject;

import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;

/**
 * 
 * ReportOutput value object modifiable
 * 
 * 
 * FIXME systematically refactored former ReportOutputVO class without any improvment
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface ModifiableReportOutputVO extends ReportOutputVO {
	public void setReportUID(UID reportUID);

	public void setDestination(Destination destination);

	public void setFormat(Format format);

	public void setPageOrientation(PageOrientation pageOrientation);

	public void setColumnScaled(boolean bColumnScaled);

	public void setParameter(String sParameter);

	public void setFilename(String sFilename);

	public void setSourceFile(String sSourceFile);

	public void setReportCLS(ByteArrayCarrier oReportCLS);

	public void setDatasourceUID(UID dataSourceUID);

	public void setSheetname(String sSheetname);

	public void setSourceFileContent(
			ByteArrayCarrier oSourceFileContent);

	public void setIsFirstOfMany(boolean bIsLastOfMany);

	public void setIsLastOfMany(boolean bIsLastOfMany);

	public void setDescription(String sDescription);

	public void setLocale(String locale);

	public boolean getAttachDocument();

	public void setAttachDocument(boolean bAttach);
}