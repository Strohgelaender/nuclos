//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;

/**
 * print service transport object
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintServiceTO implements Serializable, Comparable<PrintServiceTO> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4585791977210641775L;
	private final UID idPrintService;
	private final String name;
	private final String description;
	private final Tray defaultTray;
	private final boolean useNativePdfSupport;
	private final List<Tray> trays = new ArrayList<Tray>();
	
	public PrintServiceTO(final UID idPrintService, final String name, final String description, final boolean useNativePdfSupport, final Tray defaultTray) {
		if (idPrintService == null) {
			throw new IllegalArgumentException("idPrintService must not be null");
		}
		if (name == null) {
			throw new IllegalArgumentException("name must not be null");
		}
		this.idPrintService = idPrintService;
		this.name = name;
		this.description = description;
		this.defaultTray = defaultTray;
		this.useNativePdfSupport = useNativePdfSupport;
	}
	
	/**
	 * use for client printers only!
	 * @param name
	 */
	public PrintServiceTO(final String name) {
		this.idPrintService = null;
		this.name = name;
		this.description = null;
		this.defaultTray = null;
		this.useNativePdfSupport = false;
	}

	public UID getId() {
		return idPrintService;
	}

	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Tray getDefaultTray() {
		return defaultTray;
	}

	public boolean isUseNativePdfSupport() {
		return useNativePdfSupport;
	}

	public void addTray(Tray tray) {
		trays.add(tray);
		Collections.sort(trays);
	}
	
	public List<Tray> getTrays() {
		return trays;
	}
	
	public String toString() {
		return getName();
	}
	
	public String getPresentation() {
		if (StringUtils.looksEmpty(description)) {
			return getName();
		}
		return getDescription();
	}
	
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that instanceof PrintServiceTO) {
			if (idPrintService != null && ((PrintServiceTO) that).getId() != null) {
				return idPrintService.equals(((PrintServiceTO) that).getId());
			} else {
				return name.equals(((PrintServiceTO) that).getName());
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (idPrintService != null) {
			return idPrintService.hashCode();
		} else {
			return name.hashCode();
		}
	}

	@Override
	public int compareTo(PrintServiceTO other) {
		return StringUtils.compareIgnoreCase(this.getPresentation(), other.getPresentation());
	}
	
	public static class Tray implements Serializable, Comparable<Tray> {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1210734878190304781L;
		private final UID idPrintService;
		private final UID idTray;
		private final int number;
		private final String description;
		
		// use for client side printing only!
		private final String printServiceName;
		
		public Tray(final UID idTray, final UID idPrintService, final int number, final String description) {
			if (idPrintService == null) {
				throw new IllegalArgumentException("idPrintService must not be null");
			}
			if (idTray == null) {
				throw new IllegalArgumentException("idTray must not be null");
			}
			if (description == null) {
				throw new IllegalArgumentException("description must not be null");
			}
			this.idPrintService = idPrintService;
			this.idTray = idTray;
			this.number = number;
			this.description = description;
			this.printServiceName = null;
		}
		
		/**
		 * use for client printers only!
		 * @param number
		 * @param trayName
		 * @param printServiceName
		 */
		public Tray(final int number, final String description, final String printServiceName) {
			if (description == null) {
				throw new IllegalArgumentException("description must not be null");
			}
			if (printServiceName == null) {
				throw new IllegalArgumentException("printServiceName must not be null");
			}
			this.idPrintService = null;
			this.idTray = null;
			this.number = number;
			this.description = description;
			this.printServiceName = printServiceName;
		}

		public UID getId() {
			return idTray;
		}
		
		public UID getPrintServiceId() {
			return idPrintService;
		}
		
		public int getNumber() {
			return number;
		}

		public String getDescription() {
			return description;
		}
		
		public String toString() {
			return getDescription();
		}
		
		public String getPrintServiceName() {
			return printServiceName;
		}

		public String getPresentation() {
			if (Integer.toString(number).equals(description)) {
				return description;
			}
			return description + " (" + number + ")";
		}
		
		@Override
		public boolean equals(Object that) {
			if (this == that) {
				return true;
			}
			if (that instanceof Tray) {
				if (idTray != null && ((Tray) that).getId() != null) {
					return idTray.equals(((Tray) that).getId());
				} else {
					return description.equals(((Tray) that).getDescription());
				}
			}
			return false;
		}

		@Override
		public int hashCode() {
			if (idTray != null) {
				return idTray.hashCode();
			} else {
				return description.hashCode();
			}
		}

		@Override
		public int compareTo(Tray other) {
			return new Integer(number).compareTo(other.number);
		}
	}


}
