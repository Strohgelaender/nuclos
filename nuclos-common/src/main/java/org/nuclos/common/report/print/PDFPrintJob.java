//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.print;

import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.JobName;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.codehaus.groovy.tools.shell.util.Logger;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.printservice.PrintJobWatcher;
import org.nuclos.common.report.NuclosReportPrintJob;

/**
 * Exporter which prints Adobe PDF-files. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de </a>
 * 
 * §todo don't extend AbstractReportExporter
 *
 * @author <a href="mailto:uwe.allner@novabit.de">Uwe.Allner</a>
 * @version 01.00.00
 */
public class PDFPrintJob extends NuclosReportPrintJob {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6380496260138087399L;

	private final static Logger LOG = Logger.create(PDFPrintJob.class);
	
	private final boolean forceToPdfbox;
	
	public PDFPrintJob() {
		this(true);
	}
	
	public PDFPrintJob(boolean forceToPdfbox) {
		super();
		this.forceToPdfbox = forceToPdfbox;
	}

	@Override
	public void print(final PrintService prserv, final String sFilename,
			final PrintRequestAttributeSet aset) throws PrintException {
		
		// Workaraound for RSWORGA-343
		// Attribute Copies is ignored under windows (and macOS) systems
		// so we have to call print multiple times
		int loops = 1;
		boolean needsLooping = false;
		Copies checkCopies = (Copies)aset.get(Copies.class);
		if (checkCopies != null && checkCopies.getValue() > 1) {
			loops = checkCopies.getValue();
			aset.remove(checkCopies);
			needsLooping = true;
		}
		
		for (int i=0; i< loops; i++) {
		
			final File file = new File(sFilename);
			if (!aset.containsKey(JobName.class)) {
				aset.add(new JobName(file.getName(), Locale.getDefault()));
			}
			
			InputStream is = null;
			try {
				is = new FileInputStream(file);
				if (forceToPdfbox) {
					printViaPdfbox(prserv, is, aset);
				} else {
					PrintJobWatcher watcher = print(prserv, is, aset);
					if (needsLooping) {
						watcher.waitForDone();
					}
				}
			} catch (FileNotFoundException e) {
				throw new PrintException(e);
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						LOG.error(e.getMessage(), e);
					}
				}
			}
		}
	}
	
	public void printViaPdfbox(final PrintService prserv, final InputStream is,
			final PrintRequestAttributeSet aset) throws PrintException {
		if (!aset.containsKey(JobName.class)) {
			throw new NuclosFatalException("jobname must be set");
		}
		PDDocument document = null;
		try {
			JobName jobname = (JobName) aset.get(JobName.class);
			try {
				
				document = PDDocument.load(is);
				if( document.isEncrypted() )
					throw new PrintException ("decrypted pdfs are not allowed");
				
				PrinterJob printJob = PrinterJob.getPrinterJob();
				printJob.setJobName(jobname.getValue());
				printJob.setPrintService(prserv);
				printJob.setPageable(new PDFPageable(document));
				printJob.print(aset);
				//Silent print does not use PrintRequestAttributes (ARCD-1052)
				//document.silentPrint(printJob);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new PrintException(e.getMessage());
			}	
			finally
			{
				if( document != null )
				{
					document.close();
				}
			}			
		} catch (Exception e) {
			throw new PrintException(e);
		}
	}
}	// class PDFExport
