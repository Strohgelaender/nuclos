//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * Value object representing a datasource parameter.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Boris.Sander@novabit.de">Boris Sander</a>
 * @version 01.00.00
 */
public class DatasourceParameterVO extends NuclosValueObject<Void> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6097565410177639263L;
	private UID dataSourceUID;
	private String sParameter;
	private String sDescription;
	private String sDatatype;
	private boolean bMandatory;
	private DatasourceParameterValuelistproviderVO valuelistprovider;

	public DatasourceParameterVO(final UID dataSourceUID, String sParameter, String sDatatype, String sDescription, boolean bMandatory) {
		super();
		setDatasourceId(dataSourceUID);
		setParameter(sParameter);
		setDescription(sDescription);
		setDatatype(sDatatype);
		setMandatory(bMandatory);
	}

	public UID getDatasourceUID() {
		return dataSourceUID;
	}

	public void setDatasourceId(final UID dataSourceUID) {
		this.dataSourceUID = dataSourceUID;
	}

	public String getParameter() {
		return sParameter;
	}

	public void setParameter(String sParameter) {
		this.sParameter = sParameter;
	}

	public String getDatatype() {
		return sDatatype;
	}

	public void setDatatype(String sDatatype) {
		this.sDatatype = sDatatype;
	}

	public String getDescription() {
		return sDescription;
	}

	public void setDescription(String sDescription) {
		this.sDescription = sDescription;
	}

	public boolean getMandatory() {
		return bMandatory;
	}

	public void setMandatory(boolean bMandatory) {
		this.bMandatory = bMandatory;
	}

	public DatasourceParameterValuelistproviderVO getValueListProvider() {
		return this.valuelistprovider;
	}

	public void setValueListProvider(DatasourceParameterValuelistproviderVO valuelistprovider) {
		this.valuelistprovider = valuelistprovider;
	}

	@Override
	public void validate() throws CommonValidationException {
		if (getDescription() != null && getDescription().trim().equals("")) {
			setDescription(null);
		}
		if (getParameter() == null || getParameter().trim().equals("")) {
			throw new CommonValidationException("datasource.parameter.error.validation.name");//"Ung\u00fcltige Parameter Definition\nDas Feld \"Parameter Name\" darf nicht leer sein.");
		}

		if (getParameter().contains("'") || getParameter().contains("\"") ||
				getParameter().contains(" ") || getParameter().contains("$") ||
				getParameter().contains("!") || getParameter().contains("%")) {
			throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("datasource.parameter.error.validation.value", getParameter()));
				//"Der Parameter " + getParameter() + " enth\u00e4lt ung\u00fcltige Zeichen.");
		}

		if (getDatatype() == null || getDatatype().trim().equals("")) {
			throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("datasource.parameter.error.validation.definition", getParameter()));
				//"Ung\u00fcltige Definition des Parameters " + getParameter() + "\nDas Feld \"Datentyp\" darf nicht leer sein.");
		}

		if ("SQL".equals(getDatatype())) {
			if (getValueListProvider() == null) {
				throw new CommonValidationException(StringUtils.getParameterizedExceptionMessage("datasource.parameter.error.validation.sql", getParameter()));
			}
		}
	}
	
	@Override
	public String toString() {
		return  StringUtils.emptyIfNull(this.sParameter);
	}
	
	public String getLabel() {
		return StringUtils.looksEmpty(getDescription()) ? getParameter() : getDescription();
	}

}
