//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.io.Serializable;

import org.nuclos.api.print.PrintProperties;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common.report.valueobject.ReportOutputVO.Destination;
import org.nuclos.common.report.valueobject.ReportOutputVO.Format;
import org.nuclos.common2.KeyEnum;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * create {@link ReportOutputVOContext} from {@link MasterDataVO}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class DefaultReportOutputVOContext implements ReportOutputVOContext, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3774364866295161919L;
	private static final Boolean DEFAULT_ISMANDATORY = Boolean.FALSE;
	private final MasterDataVO<UID> mdVO;
	private final EntityMeta<UID> meta;


	public <T extends UID> DefaultReportOutputVOContext(final MasterDataVO<T> mdVO, final EntityMeta<T> meta) {
		this.mdVO = (MasterDataVO<UID>) mdVO;
		this.meta = (EntityMeta<UID>) meta;
	}


	@Override
	public ByteArrayCarrier getSourceFileContent() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.sourceFileContent);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.sourceFileContent);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public String getSourceFile() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.sourceFile);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.sourceFile);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public String getSheetName() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.sheetname);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.sheetname);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public UID getReportId() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.FORMOUTPUT.parent);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.REPORTOUTPUT.parent);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public ByteArrayCarrier getReportCLS() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.reportCLS);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.reportCLS);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public String getParameter() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.parameter);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.parameter);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public NuclosValueObject<UID> getNuclosVO() {
		return new NuclosValueObject<UID>(mdVO.getPrimaryKey(), mdVO.getCreatedAt(), mdVO.getCreatedBy(), mdVO.getChangedAt(), mdVO.getChangedBy(), mdVO.getVersion());
	}

	@Override
	public String getLocale() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.locale);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.locale);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public Format getFormat() {
		if (E.FORMOUTPUT.equals(meta)) {
			return KeyEnum.Utils.findEnum(ReportOutputVO.Format.class, mdVO.getFieldValue(E.FORMOUTPUT.format));
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return KeyEnum.Utils.findEnum(ReportOutputVO.Format.class, mdVO.getFieldValue(E.REPORTOUTPUT.format));
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public String getFilename() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.filename);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.filename);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public Destination getDestination() {
		if (E.FORMOUTPUT.equals(meta)) {
			return KeyEnum.Utils.findEnum(ReportOutputVO.Destination.class, mdVO.getFieldValue(E.FORMOUTPUT.destination));
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return KeyEnum.Utils.findEnum(ReportOutputVO.Destination.class, mdVO.getFieldValue(E.REPORTOUTPUT.destination));
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public String getDescription() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.description);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.description);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public UID getDataSourceId() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.FORMOUTPUT.datasource);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.REPORTOUTPUT.datasource);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public Boolean getAttach() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.attach);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.attach);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}


	@Override
	public PrintProperties getProperties() {
		return new DefaultPrintProperties(mdVO, meta);
	}

	@Override
	public String getCustomParameter() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.FORMOUTPUT.custom);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldValue(E.REPORTOUTPUT.custom);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public UID getRoleId() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.FORMOUTPUT.role);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.REPORTOUTPUT.role);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public UID getUserId() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.FORMOUTPUT.user);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.REPORTOUTPUT.user);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public boolean getIsMandatory() {
		if (E.FORMOUTPUT.equals(meta)) {
			return defaultIfNull(mdVO.getFieldValue(E.FORMOUTPUT.isMandatory),DEFAULT_ISMANDATORY);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return defaultIfNull(mdVO.getFieldValue(E.REPORTOUTPUT.isMandatory),DEFAULT_ISMANDATORY);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}
	
	private <T> T defaultIfNull(T value, T defaultValue) {
		if (null == value) {
			return (T) defaultValue;
		}
		return (T)value;
	}
}