//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.awt.*;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A Format class for {@link Color} objects. String representation is in line with modern CSS style.
 * 
 * @author Thomas Pasch
 */
public class ColorFormat extends Format {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7036679129214188843L;

	private static final Pattern HEX_RGB_PAT = Pattern.compile("^\\s*#\\s*([0-9a-f]{1,6})", 
			Pattern.CASE_INSENSITIVE);
	
	private static final Pattern RGB_PAT = Pattern.compile("^\\s*rgb\\(\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*\\)", 
			Pattern.CASE_INSENSITIVE);
	
	private static final Pattern RGBA_PAT = Pattern.compile("^\\s*rgba\\(\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*,\\s*([0-9\\+\\.]+)\\s*\\)", 
			Pattern.CASE_INSENSITIVE);
		
	private ColorFormat() {
	}
	
	public static ColorFormat getInstance() {
		return new ColorFormat();
	}

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition fieldPosition) {
		final Color color = (Color) obj;
		final int alpha = color.getAlpha();
		if (alpha == 255) {
			formatRgb(color, toAppendTo, fieldPosition);
		} else {
			formatRgba(color, toAppendTo, fieldPosition);
		}
		return toAppendTo;
	}
	
	private StringBuffer formatRgb(Color color, StringBuffer toAppendTo, FieldPosition fieldPosition) {
		toAppendTo.append("rgb(");
		toAppendTo.append(color.getRed()).append(", ");
		toAppendTo.append(color.getGreen()).append(", ");
		toAppendTo.append(color.getBlue()).append(") ");
		return toAppendTo;
	}

	private StringBuffer formatRgba(Color color, StringBuffer toAppendTo, FieldPosition fieldPosition) {
		toAppendTo.append("rgba(");
		toAppendTo.append(color.getRed()).append(", ");
		toAppendTo.append(color.getGreen()).append(", ");
		toAppendTo.append(color.getBlue()).append(", ");
		toAppendTo.append((float) color.getAlpha()/256.0).append(") ");
		return toAppendTo;
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		final CharSequence str = source.subSequence(pos.getIndex(), source.length());
		Matcher match = HEX_RGB_PAT.matcher(str);
		final Color result;
		if (match.find()) {
			result = parseHexRgb(match);
		} else {
			match = RGB_PAT.matcher(str);
			if (match.find()) {
				result = parseRgb(match);
			} else {
				match = RGBA_PAT.matcher(str);
				if (match.find()) {
					result = parseRgba(match);
				} else {
					// error
					pos.setErrorIndex(pos.getIndex());
					return null;
				}
			}
		}
		// update ParsePosition
		pos.setIndex(pos.getIndex() + match.end());
		return result;
	}
	
	private Color parseHexRgb(Matcher m) {
		String hex = m.group(1);
		while (hex.length() < 6) {
			hex = "0" + hex;
		}
		if (hex.length() > 6) {
			throw new IllegalArgumentException();
		}
		final int r = intFromHexString(hex.substring(0, 2));
		final int g = intFromHexString(hex.substring(2, 4));
		final int b = intFromHexString(hex.substring(4, 6));
		return new Color(r, g, b);
	}
	
	private Color parseRgb(Matcher m) {
		final int r = intFromString(m.group(1));
		final int g = intFromString(m.group(2));
		final int b = intFromString(m.group(3));
		return new Color(r, g, b);
	}
	
	private Color parseRgba(Matcher m) {
		final int r = intFromString(m.group(1));
		final int g = intFromString(m.group(2));
		final int b = intFromString(m.group(3));
		final int a = (int) (doubleFromString(m.group(4)) * 256.0);
		return new Color(r, g, b, a);
	}
	
	private static int intFromHexString(String s) {
		final int result = Integer.parseInt(s, 16);
		if (result < 0 || result > 255) {
			throw new IllegalArgumentException();
		}
		return result;
	}
	
	private static int intFromString(String s) {
		final int result = Integer.parseInt(s);
		if (result < 0 || result > 255) {
			throw new IllegalArgumentException();
		}
		return result;
	}
	
	private static double doubleFromString(String s) {
		final double result = Double.parseDouble(s);
		if (result < 0 || result > 1) {
			throw new IllegalArgumentException();
		}
		return result;
	}
	
	public static void main(String[] args) throws ParseException {
		final ColorFormat dut = ColorFormat.getInstance();
		Color c1 = new Color(30, 155, 255, 255);
		String sc = dut.format(c1);
		sc += sc;
		final ParsePosition pos = new ParsePosition(0);
		Color c2 = (Color) dut.parseObject(sc, pos);
		Color c3 = (Color) dut.parseObject(sc, pos);
		System.out.println("c1=" + c1 + " sc=" + sc + " c2=" + c2 + " c3=" + c3 + " pos=" + pos);
	}
	
}
