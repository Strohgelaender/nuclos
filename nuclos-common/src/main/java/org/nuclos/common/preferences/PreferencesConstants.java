//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

public class PreferencesConstants {
	
	private PreferencesConstants() {
		// Never invoked.
	}
	
	/**
	 * Node for storing the last seen (Integer) version <em>release notes</em>
	 * of a certain nuclet.
	 * <p>
	 * Syntax: nuclet/releasenote/<uid_of_nuclet> -> (Integer) value of last seen version
	 * 
	 * @author Thomas Pasch
	 * @since Nuclos 4.3.0
	 */
	public static final String RELEASENOTE_PREF_PATH = "nuclet/releasenote";
	

}
