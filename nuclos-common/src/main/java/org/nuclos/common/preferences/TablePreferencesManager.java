//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.RowSorter;
import javax.swing.SortOrder;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.Actions;
import org.nuclos.common.CommonSecurityCache;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.ProfileUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.profile.PublishType;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * Replacement for the WorkspaceDescription.ProfileManager
 */
public class TablePreferencesManager {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(TablePreferencesManager.class);

	protected final UID entity;
	protected final UID layout;
	private final UID taskList;
	protected final UID searchFilter;

	private final NuclosPreferenceType prefType;

	protected List<TablePreferences> tablePrefs = new ArrayList<>();

	protected final IPreferencesProvider prefProv;

	protected final IMetaProvider metaProv;

	protected final CommonSecurityCache securityCache;

	protected final String username;

	protected final UID mandator;

	public TablePreferencesManager(final UID entity, final UID layout,
								   final UID taskList, final NuclosPreferenceType prefType,
								   final IPreferencesProvider prefProv,
								   final IMetaProvider metaProv,
								   final CommonSecurityCache securityCache,
								   final String username,
								   final UID mandator
	) {
		this(entity, layout, taskList, null, prefType, prefProv, metaProv, securityCache, username, mandator);
	}

	protected TablePreferencesManager(final UID entity, final UID layout,
								   final UID taskList, final UID searchFilter,
								   final NuclosPreferenceType prefType,
								   final IPreferencesProvider prefProv,
								   final IMetaProvider metaProv,
								   final CommonSecurityCache securityCache,
								   final String username,
								   final UID mandator
	) {
		this.entity = entity;
		this.layout = layout;
		this.taskList = taskList;
		this.searchFilter = searchFilter;
		this.prefType = prefType;
		this.prefProv = prefProv;
		this.metaProv = metaProv;
		this.securityCache = securityCache;
		this.username = username;
		this.mandator = mandator;

		final List<TablePreferences> tablePreferencesFromServer = prefProv.getTablePreferences(null, prefType, entity, layout);
		if (taskList != null) {
			// no preselection. server returns all tasklist prefs.
			for (TablePreferences tp : tablePreferencesFromServer) {
				if (taskList.equals(tp.getTaskList())) {
					if (!isServerSupportsSelection(tp)) {
						// no support for selection, but we need a working "getSelected()"...
						tp.setSelected(true);
					}
					tablePrefs.add(tp);
				}
			}
		} else if (searchFilter != null) {
			// no preselection. server returns all searchfilter prefs for one entity.
			for (TablePreferences tp : tablePreferencesFromServer) {
				if (searchFilter.equals(tp.getSearchFilter())) {
					if (!isServerSupportsSelection(tp)) {
						// no support for selection, but we need a working "getSelected()"...
						tp.setSelected(true);
					}
					tablePrefs.add(tp);
				}
			}
		} else {
			// the preferences are preselected
			tablePrefs.addAll(tablePreferencesFromServer);
		}
	}

	public static boolean isServerSupportsSelection(TablePreferences tp) {
		return isServerSupportsSelection(tp.getType());
	}

	public static boolean isServerSupportsSelection(NuclosPreferenceType prefType) {
		return prefType == NuclosPreferenceType.TABLE ||
				prefType == NuclosPreferenceType.SUBFORMTABLE;
	}

	public NuclosPreferenceType getType() {
		return prefType;
	}

	public interface IColumnFieldUidResolver {
		UID getColumnFieldUid(int iColumn);
	}

	public interface IColumnIndexResolver {
		int getColumnIndex(UID fieldUid);
	}

	public void share(final TablePreferences tp, final PublishType publishType) {
		throw new NotImplementedException("Preferences sharing not implemented yet");
	}

	public void reset(final TablePreferences tp) {
		if (tp != null) {
			try {
				if (tp.isShared()) {
					if (tp.isCustomized()) {
						final boolean bSelected = tp.isSelected();
						prefProv.delete(tp);
						final TablePreferences tpShared = prefProv.getTablePreference(tp.getUID());
						tp.clearAndImport(tpShared);
						tp.setSelected(bSelected);
					} // else ... nothing to do;
				} else {
					final UID id = tp.getUID();
					tp.clearAndImport(createDefault());
					tp.setUID(id);
					prefProv.update(tp, getEntity());
				}
			} catch (CommonFinderException e) {
				LOG.warn("Reset ot table preference " + tp.getUID() + " failed. Not found!");
			} catch (CommonPermissionException e) {
				LOG.warn("Reset of table preference " + tp.getUID() + " is not allowed.");
			}
		}
	}

	public UID getEntity() {
		return entity;
	}

	public UID getLayout() {
		return layout;
	}

	public UID getTaskList() {
		return taskList;
	}

	public UID getSearchFilter() {
		return searchFilter;
	}

	public TablePreferencesManager validate(IMetaProvider metaProvider) {
		for (final TablePreferences tp : tablePrefs) {
			validate(tp, entity, metaProvider);
		}
		return this;
	}

	private void validate(TablePreferences tp, UID entity, IMetaProvider metaProvider) {
		// special entities with custom collect controller

		if (E.getByUID(entity) != null) {
			if (entity.equals(E.SERVERCODE.getUID())) {
				return;
			}
		}

		for (ColumnPreferences cp : tp.getSelectedColumnPreferences()) {
			try {
				metaProvider.getEntityField(cp.getColumn());
			} catch (Exception e) {
				tp.removeSelectedColumnPreferences(cp);
				removeColumnSorting(tp, cp.getColumn());
			}
		}
	}

	public void removeColumnSorting(TablePreferences tp, UID column) {
		for (ColumnSortingPreferences cs : tp.getColumnSortings()) {
			if (LangUtils.equal(cs.getColumn(), column)) {
				tp.removeColumnSorting(cs);
			}
		}
	}

	public TablePreferences createDefault() {
		final TablePreferences result = new TablePreferences(prefType, new UID());
		result.setLayout(layout);
		result.setTaskList(taskList);
		result.setSearchFilter(searchFilter);
		final Map<UID, FieldMeta<?>> mapAllFields = metaProv.getAllEntityFieldsByEntity(getEntity());
		final List<UID> initialTableColumnFields = ProfileUtils.getInitialTableColumnFields(mapAllFields.values());
		for (UID fieldUID : initialTableColumnFields) {
			ColumnPreferences cp = new ColumnPreferences();
			cp.setColumn(fieldUID);
			cp.setWidth(ProfileUtils.getMinimumColumnWidth(mapAllFields.get(fieldUID).getJavaClass()));
			result.addSelectedColumnPreferences(cp);
		}
		return result;
	}

	public TablePreferences getSelected() {
		TablePreferences result = null;
		for (TablePreferences tp : tablePrefs) {
			if (result == null) {
				result = tp;
			}
			if (tp.isSelected()) {
				result = tp;
				break;
			}
		}
		if (result == null) {
			result = createDefault();
			insert(result);
			tablePrefs.add(result);
		}
		if (!result.isSelected()) {
			select(result);
		}
		return result;
	}

	public List<TablePreferences> getShared() {
		return CollectionUtils.select(tablePrefs,
				new Predicate<TablePreferences>() {
					@Override
					public boolean evaluate(TablePreferences t) {
						return t.isShared();
					}
				});
	}

	public List<TablePreferences> getPrivate() {
		return CollectionUtils.select(tablePrefs,
				new Predicate<TablePreferences>() {
					@Override
					public boolean evaluate(TablePreferences t) {
						return !t.isShared();
					}
				});
	}

	public void insert(final TablePreferences tp) {
		if (tp.getUID() == null) {
			throw new IllegalArgumentException("UID of TablePreference must not be null");
		}
		final boolean bSelected = tp.isSelected();
		if (!isServerSupportsSelection(tp)) {
			// remove selection before sending to server...
			tp.setSelected(false);
		}
		// all new TablePreferences should be layout dependent
		if (getType() == NuclosPreferenceType.SUBFORMTABLE) {
			tp.setLayout(layout);
		}
		try {
			prefProv.insert(tp, getEntity());
			tablePrefs.add(prefProv.getTablePreference(tp.getUID()));
		} catch (CommonPermissionException e) {
			LOG.warn("Insert of table preference " + tp.getUID() + " is not allowed.");
		} catch (CommonFinderException e) {
			LOG.warn("Insert of table preference " + tp.getUID() + " failed. Not found!");
		} finally {
			tp.setSelected(bSelected);
		}
	}

	public void update(TablePreferences tp) {
		final boolean bSelected = tp.isSelected();
		if (!isServerSupportsSelection(tp)) {
			// remove selection before sending to server...
			tp.setSelected(false);
		}
		try {
			prefProv.update(tp, getEntity());
			if (tp.isShared() && !tp.isCustomized()) {
				tp.setCustomized(true);
			}
		} catch (CommonPermissionException e) {
			LOG.warn("Update of table preference " + tp.getUID() + " is not allowed.");
		} catch (CommonFinderException e) {
			LOG.warn("Update ot table preference " + tp.getUID() + " failed. Not found!");
		} finally {
			tp.setSelected(bSelected);
		}
	}

	public boolean delete(final TablePreferences tp) {
		try {
			if (tp.isShared()) {
				throw new IllegalArgumentException("Preference could not be delete. Un-share it first.");
			}
			prefProv.delete(tp);
			tablePrefs.remove(tp);
			return true;
		} catch (CommonPermissionException e) {
			LOG.warn("Delete of table preference " + tp.getUID() + " is not allowed.");
		} catch (CommonFinderException e) {
			LOG.warn("Delete ot table preference " + tp.getUID() + " failed. Not found!");
		}
		return false;
	}

	public void select(final TablePreferences tp) {
		tp.setSelected(true);
		for (TablePreferences other : tablePrefs) {
			if (other.getUID().equals(tp.getUID())) {
				continue;
			}
			other.setSelected(false);
		}
		if (isServerSupportsSelection(tp)) {
			// selection only for entity or subform prefs...
			prefProv.select(tp, layout);
		}
	}

	public List<ColumnPreferences> getSelectedColumnPreferences() {
		return getSelected().getSelectedColumnPreferences();
	}

	public List<ColumnSortingPreferences> getColumnSortings() {
		return getSelected().getColumnSortings();
	}

	/**
	 * @return selected columns with fixed
	 */
	public List<UID> getSelectedColumns() {
		// Our webclient does not handle hidden columns.
		// So we have to disable the 'addNewColumns' feature
		return
				//addNewColumns(
				CollectionUtils.transform(getSelectedColumnPreferences(),
				new Transformer<ColumnPreferences, UID>() {
					@Override
					public UID transform(ColumnPreferences i) {
						return i.getColumn();
					}
				})
				//, getSelected(), getEntity(), false)
				;
	}

	public List<Integer> getFixedWidths() {
		return CollectionUtils.transform(
				CollectionUtils.select(getSelectedColumnPreferences(),
						new Predicate<ColumnPreferences>() {
							@Override
							public boolean evaluate(ColumnPreferences t) {
								return t.isFixed();
							}
						}),
				new Transformer<ColumnPreferences, Integer>(){
					@Override
					public Integer transform(ColumnPreferences i) {
						return i.getWidth();
					}
				});
	}

	public List<UID> getSelectedWithoutFixedColumns() {
		return CollectionUtils.transform(
				CollectionUtils.select(getSelectedColumnPreferences(),
						new Predicate<ColumnPreferences>() {
							@Override
							public boolean evaluate(ColumnPreferences t) {
								return !t.isFixed();
							}
						}),
				new Transformer<ColumnPreferences, UID>(){
					@Override
					public UID transform(ColumnPreferences i) {
						return i.getColumn();
					}
				});
	}

	public List<UID> getFixedColumns() {
		return CollectionUtils.transform(
				CollectionUtils.select(getSelectedColumnPreferences(),
						new Predicate<ColumnPreferences>() {
							@Override
							public boolean evaluate(ColumnPreferences t) {
								return t.isFixed();
							}
						}),
				new Transformer<ColumnPreferences, UID>(){
					@Override
					public UID transform(ColumnPreferences i) {
						return i.getColumn();
					}
				});
	}

	public List<Integer> getColumnWidths() {
		return CollectionUtils.transform(getSelectedColumnPreferences(),
				new Transformer<ColumnPreferences, Integer>() {
					@Override
					public Integer transform(ColumnPreferences i) {
						return i.getWidth();
					}
				});
	}

	public List<Integer> getColumnWidthsWithoutFixed() {
		return CollectionUtils.transform(CollectionUtils.select(getSelectedColumnPreferences(),
				new Predicate<ColumnPreferences>() {
					@Override
					public boolean evaluate(ColumnPreferences t) {
						return !t.isFixed();
					}
				}),
				new Transformer<ColumnPreferences, Integer>() {
					@Override
					public Integer transform(ColumnPreferences i) {
						return i.getWidth();
					}
				});
	}

	public Map<UID, Integer> getColumnWidthsMap() {
		Map<UID, Integer> result = new HashMap<UID, Integer>();
		for (ColumnPreferences cp : getSelectedColumnPreferences()) {
			result.put(cp.getColumn(), cp.getWidth());
		}
		return result;
	}

	public List<RowSorter.SortKey> getSortKeys(final IColumnIndexResolver ciResolver) {
		return CollectionUtils.transform(getColumnSortings(),
				new Transformer<ColumnSortingPreferences, RowSorter.SortKey>(){
					@Override
					public RowSorter.SortKey transform(ColumnSortingPreferences i) {
						return new RowSorter.SortKey(ciResolver.getColumnIndex(i.getColumn()),
								i.isAsc()? SortOrder.ASCENDING:SortOrder.DESCENDING);
					}
				});
	}

	public static List<CollectableEntityField> getSelectedFields(TablePreferences tp, List<CollectableEntityField> fields) {
		List<CollectableEntityField> result = new ArrayList<>();
		Set<UID> preventDuplicates = new HashSet<>();
		Set<UID> preventDuplicates2 = new HashSet<>();
		for (ColumnPreferences cp : tp.getSelectedColumnPreferences()) {
			if (preventDuplicates.contains(cp.getColumn())) {
				continue;
			} else {
				preventDuplicates.add(cp.getColumn());
			}
			for (CollectableEntityField clctef : fields) {
				if (LangUtils.equal(cp.getColumn(), clctef.getUID())) {
					if (preventDuplicates2.contains(clctef.getUID())) {
						continue;
					} else {
						preventDuplicates2.add(clctef.getUID());
					}
					if (cp.getEntity() == null || LangUtils.equal(cp.getEntity(), clctef.getEntityUID())) {
						result.add(clctef);
					}
				}
			}
		}
		return result;
	}

	public List<CollectableEntityField> getSelectedFields(List<CollectableEntityField> fields) {
		return getSelectedFields(getSelected(), fields);
	}

	private List<CollectableEntityField> getSelectedWithoutFixedFields(List<CollectableEntityField> fields) {
		List<CollectableEntityField> result = new ArrayList<CollectableEntityField>();
		for (ColumnPreferences cp : CollectionUtils.select(getSelectedColumnPreferences(),
				new Predicate<ColumnPreferences>() {
					@Override
					public boolean evaluate(ColumnPreferences t) {
						return !t.isFixed();
					}
				})) {
			for (CollectableEntityField clctef : fields) {
				if (LangUtils.equal(cp.getColumn(), clctef.getUID())) {
					result.add(clctef);
				}
			}
		}
		return result;
	}

	public Set<CollectableEntityField> getFixedFields(List<CollectableEntityField> selectedFields) {
		Set<CollectableEntityField> result = new HashSet<CollectableEntityField>();
		for (ColumnPreferences cp : getSelectedColumnPreferences()) {
			for (CollectableEntityField clctef : selectedFields) {
				if (LangUtils.equal(cp.getColumn(), clctef.getUID())
						&& cp.isFixed()) {
					result.add(clctef);
				}
			}
		}
		return result;
	}

	public void addFixedColumns(List<UID> fields, List<Integer> fieldWidths) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("addFixedColumns for subform " + getEntity().debugString() + " fields=" + StringUtils.debugUIDs(fields) + ", widths=" + fieldWidths);
		}
		if (securityCache.isActionAllowed(username, Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS, mandator)) {
			final TablePreferences tp = getSelected();
			fields = new ArrayList<UID>(fields);
			fieldWidths = new ArrayList<Integer>(fieldWidths);
			Collections.reverse(fields);
			Collections.reverse(fieldWidths);
			for (int i = 0; i < fields.size(); i++) {
				if (fieldWidths.size() > i) {
					ColumnPreferences cp = new ColumnPreferences();
					cp.setFixed(true);
					cp.setColumn(fields.get(i));
					cp.setWidth(fieldWidths.get(i));
					tp.addSelectedColumnPreferencesInFront(cp);
					LOG.debug(StringUtils.logFormat("addFixedColumns",cp.getColumn(),cp.getWidth()));
				}
			}
			update(tp);
		}
	}

	public void setColumnPreferences(List<UID> fields, List<Integer> fieldWidths, final List<UID> fixedFields) {
		if (securityCache.isActionAllowed(username, Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS, mandator)) {
			final TablePreferences tp = getSelected();
			for (ColumnPreferences tpcp : tp.getSelectedColumnPreferences()) {
				if (!fields.contains(tpcp.getColumn())) {
					tp.removeSelectedColumnPreferences(tpcp);
				}
			}
			List<ColumnPreferences> backup = tp.getSelectedColumnPreferences();
			tp.removeAllSelectedColumnPreferences();
			Set<UID> preventDuplicates = new HashSet<>();
			int totalWidth = 0;
			for (int i = 0; i < fields.size(); i++) {
				UID fieldUID = fields.get(i);
				if (preventDuplicates.contains(fieldUID)) {
					continue;
				} else {
					preventDuplicates.add(fieldUID);
				}
				ColumnPreferences cp = null;
				for (ColumnPreferences tpcp : backup) {
					if (LangUtils.equal(tpcp.getColumn(), fieldUID)) {
						cp = tpcp;
						cp.setFixed(false);
						break;
					}
				}
				if (cp == null) {
					cp = new ColumnPreferences();
					cp.setColumn(fieldUID);
				}

				Integer width;

				if (fieldWidths.size()>i) {
					width = fieldWidths.get(i);
				} else {
					width = 75;
				}

				if (width != null && totalWidth + width > 30000) {
					width = 75; // NUCLOS-6553
				}

				totalWidth += cp.setWidth(width);

				if (fixedFields != null && fixedFields.contains(cp.getColumn())) {
					cp.setFixed(true);
				}

				tp.addSelectedColumnPreferences(cp);

				LOG.debug(StringUtils.logFormat("setColumnPreference",cp.getColumn(),cp.getWidth()));
			}
			update(tp);
		}
	}

	public void setSortKeys(List<? extends RowSorter.SortKey> sortKeys, IColumnFieldUidResolver cnResolver) {
		if (securityCache.isActionAllowed(username, Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS, mandator)) {
			TablePreferences tp = getSelected();
			tp.removeAllColumnSortings();
			for (RowSorter.SortKey sortKey : sortKeys) {
				if (sortKey.getSortOrder() == SortOrder.UNSORTED)
					continue;
				if (sortKey.getColumn() == -1)
					continue;
				ColumnSortingPreferences cs = new ColumnSortingPreferences();
				cs.setColumn(cnResolver.getColumnFieldUid(sortKey.getColumn()));
				cs.setAsc(sortKey.getSortOrder() == SortOrder.ASCENDING);
				tp.addColumnSorting(cs);
				LOG.debug(StringUtils.logFormat("setSortKeys",cs.getColumn(),(cs.isAsc()?"ASC":"DESC")));
			}
			update(tp);
		}
	}

	// Our webclient does not handle hidden columns.
	// So we have to disable the 'addNewColumns' feature
//	private List<UID> addNewColumns(final List<UID> selectedFields, final TablePreferences tp, final UID entity, final boolean ignoreFixed) {
//		if (securityCache.isActionAllowed(username, Actions.ACTION_CUSTOMIZE_ENTITY_AND_SUBFORM_COLUMNS, mandator)) {
//
//			// do not add columns first time...
//			if (selectedFields.isEmpty() && tp.getHiddenColumns().isEmpty()) {
//				return selectedFields;
//			}
//
//			org.nuclos.common.preferences.WorkspaceUtils.addNewColumns(selectedFields, tp, entity, ignoreFixed);
//		}
//		return selectedFields;
//	}

	public void setDynamicRowHeight(boolean dynamicRowHeight) {
		final TablePreferences tp = getSelected();
		tp.setDynamicRowHeight(dynamicRowHeight);
		update(tp);
	}

}
