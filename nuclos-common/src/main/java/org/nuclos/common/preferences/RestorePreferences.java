package org.nuclos.common.preferences;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common2.CloseableXStream;
import org.nuclos.common2.XStreamSupport;

import com.thoughtworks.xstream.XStream;

public class RestorePreferences implements Serializable {
	private static final long serialVersionUID = 6637996725938917463L;

	public static final int GENERIC = -1;
	public static final int PERSONAL = 1;
	public static final int TIMELIMIT = 2;
	public static final int DYNAMIC = 3;

	/**
	 * use GENERIC, PERSONAL or TIMELIMIT
	 */
	public Integer type;
	public Integer refreshInterval;
	/**
	 * only for type GENERIC
	 */
	public UID searchFilterId;
	
	/**
	 * only for type DYNAMIC
	 */
	public UID tasklistId;
	public String tasklistName;
	
	public static RestorePreferences fromXML(String xml) {
		return (RestorePreferences)objectFromXML(xml);
	}

	public static Object objectFromXML(String xml) {
		xml = xml.replaceAll("org\\.nuclos\\.client\\.task\\.TaskController_-RestorePreferences",
				"org\\.nuclos\\.common\\.preferences\\.RestorePreferences");

		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.fromXML(xml);
		}
	}


	public static String toXML(RestorePreferences rp) {
		final XStreamSupport xs = XStreamSupport.getInstance();
		try (CloseableXStream closeable = xs.getCloseableStream()) {
			final XStream xstream = closeable.getStream();
			return xstream.toXML(rp);
		}
	}
}
