//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.util.List;

import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

public interface IPreferencesProvider {

	TablePreferencesManager getTablePreferencesManagerForEntity(UID entityUID, final String userName, final UID mandatorUID);

	TablePreferencesManager getTablePreferencesManagerForSubformEntity(UID entityUID, final UID layoutUID, final String userName, final UID mandatorUID);

	TablePreferencesManager getTablePreferencesManagerForTaskList(UID taskListUID, final String userName, final UID mandatorUID);

	SearchFilterTaskListTablePreferencesManager getTablePreferencesManagerForSearchFilterTaskList(SearchFilterVO searchFilterVO, final String userName, final UID mandatorUID);

	PlanningTablePreferencesManager getPlanningTablePreferencesManager(UID planningTable, final String userName);

	void insert(TablePreferences tp, final UID entityUID) throws CommonPermissionException;

	void select(TablePreferences result, final UID layoutUID);

	void update(TablePreferences tp, final UID entityUID) throws CommonFinderException, CommonPermissionException;

	void delete(TablePreferences tp) throws CommonFinderException, CommonPermissionException;

	List<TablePreferences> getTablePreferences(final String app, final NuclosPreferenceType type, UID entityUID, UID layoutUID);

	TablePreferences getTablePreference(UID uid) throws CommonFinderException, CommonPermissionException;

	void resetAllCustomizedTablePreferences();

	void insert(PlanningTablePreferences p, UID planningTableUID) throws CommonPermissionException;

	void select(PlanningTablePreferences p);

	void update(PlanningTablePreferences p, UID planningTableUID) throws CommonFinderException, CommonPermissionException;

	void delete(PlanningTablePreferences p) throws CommonFinderException, CommonPermissionException;

	List<PlanningTablePreferences> getPlanningTablePreferences(final String app, UID planningTableUID);

	PlanningTablePreferences getPlanningTablePreference(UID uid) throws CommonFinderException, CommonPermissionException;
}
