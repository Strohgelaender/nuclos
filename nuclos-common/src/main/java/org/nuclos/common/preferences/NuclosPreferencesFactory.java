//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.util.prefs.Preferences;
import java.util.prefs.PreferencesFactory;

import org.apache.log4j.Logger;

/**
 * Just a wrapper for {@link NuclosPreferencesSingleton} (that resides in spring context).
 *
 * @author Thomas Pasch
 * @since Nuclos 3.13.8
 */
public class NuclosPreferencesFactory implements PreferencesFactory {
	
	private static final Logger LOG = Logger.getLogger(NuclosPreferencesFactory.class);
	
	private NuclosPreferencesSingleton wrapped;
	
	/**
	 * Attention: MUST be public. (tp)
	 */
	public NuclosPreferencesFactory() {
		wrapped = NuclosPreferencesSingleton.getInstance();
	}

	@Override
	public Preferences systemRoot() {
		return wrapped.systemRoot();
	}

	@Override
	public Preferences userRoot() {
		return wrapped.userRoot();
	}

}	// class NuclosPreferencesFactory
