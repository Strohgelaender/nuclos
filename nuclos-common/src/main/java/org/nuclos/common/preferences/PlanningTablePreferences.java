package org.nuclos.common.preferences;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.UID;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.interval.GranularityType;

/**
 * Created by Sebastian Debring on 2/14/2018.
 */
public class PlanningTablePreferences implements Serializable {

    private static final long serialVersionUID = 1590474694728798660L;

    final NuclosPreferenceType preftype = NuclosPreferenceType.PLANNINGTABLE;

    private UID id;
    private boolean shared;
    private boolean selected;
    private GranularityType granularity;
    private int orientation;
    private int resourceCellExtentH;
    private int timelineCellExtentH;
    private int resourceCellExtentV;
    private int timelineCellExtentV;
    private int resourceHeaderExtent;
    private int timelineHeaderExtent;
    private String searchFilter;
    private String searchcondition;
    private String resourceSortOrder;
    private Map<String, String> headerLabel;
    private boolean ownBooking;

    public UID getUID() {
        return id;
    }

    public void setUID(UID id) {
        this.id = id;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public GranularityType getGranularity() {
        return granularity;
    }

    public void setGranularity(GranularityType granularity) {
        this.granularity = granularity;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int getResourceCellExtentH() {
        return resourceCellExtentH;
    }

    public void setResourceCellExtentH(int resourceCellExtentH) {
        this.resourceCellExtentH = resourceCellExtentH;
    }

    public int getTimelineCellExtentH() {
        return timelineCellExtentH;
    }

    public void setTimelineCellExtentH(int timelineCellExtentH) {
        this.timelineCellExtentH = timelineCellExtentH;
    }

    public int getResourceCellExtentV() {
        return resourceCellExtentV;
    }

    public void setResourceCellExtentV(int resourceCellExtentV) {
        this.resourceCellExtentV = resourceCellExtentV;
    }

    public int getTimelineCellExtentV() {
        return timelineCellExtentV;
    }

    public void setTimelineCellExtentV(int timelineCellExtentV) {
        this.timelineCellExtentV = timelineCellExtentV;
    }

    public int getResourceHeaderExtent() {
        return resourceHeaderExtent;
    }

    public void setResourceHeaderExtent(int resourceHeaderExtent) {
        this.resourceHeaderExtent = resourceHeaderExtent;
    }

    public int getTimelineHeaderExtent() {
        return timelineHeaderExtent;
    }

    public void setTimelineHeaderExtent(int timelineHeaderExtent) {
        this.timelineHeaderExtent = timelineHeaderExtent;
    }

    public String getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(String searchFilter) {
        this.searchFilter = searchFilter;
    }

    public String getSearchcondition() {
        return searchcondition;
    }

    public void setSearchcondition(String searchcondition) {
        this.searchcondition = searchcondition;
    }

    public NuclosPreferenceType getPrefType() {
        return preftype;
    }

    public String getResourceSortOrder() {
        return resourceSortOrder;
    }

    public void setResourceSortOrder(String resourceSortOrder) {
        this.resourceSortOrder = resourceSortOrder;
    }

    public Map<String, String> getHeaderLabel() {
        return headerLabel;
    }

    public void setHeaderLabel(Map<String, String> headerLabel) {
        this.headerLabel = headerLabel;
    }

    public boolean isOwnBooking() {
        return ownBooking;
    }

    public void setOwnBooking(boolean ownBooking) {
        this.ownBooking = ownBooking;
    }
}
