package org.nuclos.common.preferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.nuclos.common.CommonSecurityCache;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.UID;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.interval.GranularityType;

/**
 * Created by Sebastian Debring on 2/14/2018.
 */
public class PlanningTablePreferencesManager {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(PlanningTablePreferencesManager.class);

    protected final UID planningTable;

    private final NuclosPreferenceType prefType = NuclosPreferenceType.PLANNINGTABLE;

    protected List<PlanningTablePreferences> prefs = new ArrayList<>();

    protected final IPreferencesProvider preferencesProvider;

    protected final IMetaProvider metaProv;

    protected final CommonSecurityCache securityCache;

    protected final String userName;

    public PlanningTablePreferencesManager(UID planningTable, PreferencesProvider preferencesProvider, IMetaProvider metaProv, CommonSecurityCache securityCache, String userName) {
        this.planningTable = planningTable;
        this.preferencesProvider = preferencesProvider;
        this.metaProv = metaProv;
        this.securityCache = securityCache;
        this.userName = userName;

        prefs.addAll(preferencesProvider.getPlanningTablePreferences(null, planningTable));
    }

    public void insert(final PlanningTablePreferences p) {
        if (p.getUID() == null) {
            throw new IllegalArgumentException("UID of PlanningTablePreference must not be null");
        }
        p.setSelected(true);
        try {
            preferencesProvider.insert(p, planningTable);
            prefs.add(preferencesProvider.getPlanningTablePreference(p.getUID()));
        } catch (CommonPermissionException e) {
            LOG.warn("Insert of planning table preference " + p.getUID() + " is not allowed.");
        } catch (CommonFinderException e) {
            LOG.warn("Insert of planning table preference " + p.getUID() + " failed. Not found!");
        }
    }

    public void update(PlanningTablePreferences p) {
        try {
            preferencesProvider.update(p, planningTable);
        } catch (CommonPermissionException e) {
            LOG.warn("Update of planning table preference " + p.getUID() + " is not allowed.");
        } catch (CommonFinderException e) {
            LOG.warn("Update ot planning table preference " + p.getUID() + " failed. Not found!");
        }
    }

    public boolean delete(final PlanningTablePreferences p) {
        try {
            if (p.isShared()) {
                throw new IllegalArgumentException("Preference could not be delete. Un-share it first.");
            }
            preferencesProvider.delete(p);
            prefs.remove(p);
            return true;
        } catch (CommonPermissionException e) {
            LOG.warn("Delete of planning table preference " + p.getUID() + " is not allowed.");
        } catch (CommonFinderException e) {
            LOG.warn("Delete ot planning table preference " + p.getUID() + " failed. Not found!");
        }
        return false;
    }

    public PlanningTablePreferences getSelected(boolean migrate) {
        PlanningTablePreferences result = null;
        for (PlanningTablePreferences p : prefs) {
            if (result == null) {
                result = p;
            }
            if (p.isSelected()) {
                result = p;
                break;
            }
        }
        if (result == null && !migrate) {
            result = createDefault();
            insert(result);
            prefs.add(result);
        }
        if (result != null && !result.isSelected()) {
            select(result);
        }
        return result;
    }

    public void select(final PlanningTablePreferences p) {
        p.setSelected(true);
        for (PlanningTablePreferences other : prefs) {
            if (other.getUID().equals(p.getUID())) {
                continue;
            }
            other.setSelected(false);
        }
            // selection only for entity or subform prefs...
        preferencesProvider.select(p);
    }

    public PlanningTablePreferences migrate(Preferences p, PlanningTablePreferences newPrefs) throws BackingStoreException {
        PlanningTablePreferences result = newPrefs == null ? new PlanningTablePreferences() : newPrefs;
        if (result.getUID() == null) {
            result.setUID(new UID());
        }
        result.setShared(false);
        result.setGranularity(GranularityType.valueOf(p.get("granularity", "day").toUpperCase()));
        result.setOrientation(p.getInt("orientation", 0));
        result.setResourceCellExtentH(p.getInt("resourceCellExtentH", -1));
        result.setTimelineCellExtentH(p.getInt("timelineCellExtentH", -1));
        result.setResourceCellExtentV(p.getInt("resourceCellExtentV", -1));
        result.setTimelineCellExtentV(p.getInt("timelineCellExtentV", -1));
        result.setResourceHeaderExtent(p.getInt("resourceHeaderExtent", -1));
        result.setTimelineHeaderExtent(p.getInt("timelineHeaderExtent", -1));

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < p.getInt("subsearchfiltercount" ,0); i++) {
            sb.append(p.get("subsearchfilter" + i, ""));
        }
        result.setSearchFilter(StringUtils.isNullOrEmpty(sb.toString()) ? null : sb.toString());
        sb.setLength(0);
        for (int i = 0; i < p.getInt("subsearchconditioncount" ,0); i++) {
            sb.append(p.get("subsearchcondition" + i, ""));
        }
        result.setSearchcondition(StringUtils.isNullOrEmpty(sb.toString()) ? null : sb.toString());

        result.setResourceSortOrder(p.get("resourcesortoder", null));

        Map<String, String> localeHeaderlabel = result.getHeaderLabel();
        if (localeHeaderlabel == null) {
            localeHeaderlabel = new HashMap<>();
            result.setHeaderLabel(localeHeaderlabel);
        }
        for (String key : p.keys()) {
            try {
                if ("headerlabel".equals(key.substring(0, 11))) {
                    localeHeaderlabel.put(key.substring(11), p.get(key, null));
                }
            } catch (StringIndexOutOfBoundsException e) {
                //key.length < 11, do nothing
            }
        }

        result.setOwnBooking(p.getBoolean("ownbooking", false));

        return result;
    }

    private PlanningTablePreferences createDefault() {
        PlanningTablePreferences planningTablePreferences= new PlanningTablePreferences();
        planningTablePreferences.setUID(new UID());
        planningTablePreferences.setShared(false);
        planningTablePreferences.setSelected(false);
        planningTablePreferences.setGranularity(GranularityType.DAY);
        planningTablePreferences.setOrientation(0);
        planningTablePreferences.setResourceCellExtentH(-1);
        planningTablePreferences.setTimelineCellExtentH(-1);
        planningTablePreferences.setResourceCellExtentV(-1);
        planningTablePreferences.setTimelineCellExtentV(-1);
        planningTablePreferences.setResourceHeaderExtent(-1);
        planningTablePreferences.setTimelineHeaderExtent(-1);
        planningTablePreferences.setSearchFilter(null);
        planningTablePreferences.setSearchcondition(null);
        planningTablePreferences.setHeaderLabel(new HashMap<>());
        planningTablePreferences.setResourceSortOrder(null);
        planningTablePreferences.setOwnBooking(false);
        return planningTablePreferences;
    }
}
