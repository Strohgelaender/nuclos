//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.common.customcomp.resplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.time.LocalTime;
import org.nuclos.common2.StringUtils;

//Version
@XmlTransient
public abstract class AbstractResPlanConfigVO<PK,R,C extends Collectable<PK>,PLE extends PlanElement<R>> implements Serializable, ResPlanConstants {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4810604592930011989L;

	@XmlAttribute(name="version", required=true)
	static final String VERSION = "0.2";

	//Group 1: Single Fields
	private UID resourceEntity;
	private UID resourceSortField;
	
	private UID holidaysEntity;
	private UID holidaysDateField;
	private UID holidaysNameField;
	private UID holidaysTipField;
	private boolean withHolidays;
	
	private String resourceLabelText;
	private String resourceToolTipText;
	private String cornerLabelText;
	private String cornerToolTipText;

	private String defaultViewFrom;
	private String defaultViewUntil;
	
	private UID specialFiewFromField;
	private UID specialViewUntilField;
	private String specialViewFrom;
	private String specialViewUntil;
	
	private boolean dayOfWeek;
	
	private List<ResourceLocaleVO> resourceLocales;
	private List<PLE> planElements;
	
	//Group 2: Entry Entity Fields (1:n to to resource Entity)
	private UID entryEntity;
	private UID referenceField;
	private UID bookerField;
	private UID milestoneField;

	private UID dateFromField;
	private UID dateUntilField;
	private String timePeriodsString;
	private UID timeFromField;
	private UID timeUntilField;

	private String entryLabelText;
	private String entryToolTipText;

	//Group 3: Relation Entity Fields (1:n to Resource Entity)
	private UID relationEntity;
	private UID relationFromField;
	private UID relationToField;
	
	private int relationPresentation;
	private int relationFromPresentation;
	private int relationToPresentation;
	private boolean newRelationFromController;
	//missing: Icons and Color

	//Group 4: Scripting
	//Group 4a: Single Scripting Fields
	private boolean scriptingActivated;
	private String scriptingCode;
	private String backgroundPaintMethod;
	private String scriptingResourceCellMethod;
	//Group 4b: Entry Entity Scripting Fields (1:n to Resource Entity)
	private String scriptingEntryCellMethod;
	
	//Group 5: Old Resources
	private List<ResPlanResourceVO> resources;

	public AbstractResPlanConfigVO() {
	}

	@XmlElement(name="resourceEntity")
	public UID getResourceEntity() {
		return resourceEntity;
	}

	public void setResourceEntity(UID resourceEntity) {
		this.resourceEntity = resourceEntity;
	}
	
	public void setResourceSortField(UID resourceSortField) {
		this.resourceSortField = resourceSortField;
	}

	public UID getResourceSortField() {
		return resourceSortField;
	}
	
	private PLE createPlanElementFromOldData(int type) {
		final PLE elem = newPlanElementInstance();
		elem.setType(type);
		if (type == PlanElement.ENTRY || type == PlanElement.MILESTONE) {
			if (entryEntity == null) return null;
			if (referenceField == null) return null;
			elem.setEntity(entryEntity);
			elem.setPrimaryField(referenceField);
			if (type == PlanElement.ENTRY) {
				elem.setBookerField(bookerField);
				elem.setDateFromField(dateFromField);
				elem.setDateUntilField(dateUntilField);
				elem.setTimePeriodsString(timePeriodsString);
				elem.setTimeFromField(timeFromField);
				elem.setTimeUntilField(timeUntilField);
				elem.setLabelText(entryLabelText);
				elem.setToolTipText(entryToolTipText);
				elem.setScriptingEntryCellMethod(scriptingEntryCellMethod);
				elem.transferResources(resources);
			}
		} else if (type == PlanElement.RELATION) {
			if (relationEntity == null) return null;
			if (relationFromField == null) return null;
			if (relationToField == null) return null;
			elem.setEntity(relationEntity);
			elem.setPrimaryField(relationFromField);
			elem.setSecondaryField(relationToField);
			elem.setPresentation(relationPresentation);
			elem.setFromPresentation(relationFromPresentation);
			elem.setToPresentation(relationToPresentation);
			elem.setNewRelationFromController(newRelationFromController);			
		}
		return elem;
	}
	
	//This is a migration-method.
	public List<PLE> getOrMigratePlanElements() {
		if (planElements == null) {
			planElements = new ArrayList<PLE>();
			PLE pElement = createPlanElementFromOldData(PlanElement.ENTRY);
			if (pElement != null) {
				planElements.add(pElement);				
			}
			pElement = createPlanElementFromOldData(PlanElement.RELATION);
			if (pElement != null) {
				planElements.add(pElement);				
			}
		}
		return planElements;
	}
	
	//This, too
	public List<ResourceLocaleVO> getOrMigrateResourceLocales() {
		if (resourceLocales == null) {
			resourceLocales = new ArrayList<ResourceLocaleVO>();
			if (resources != null) for (ResPlanResourceVO r : resources) {
				ResourceLocaleVO rlvo = new ResourceLocaleVO();
				rlvo.setLocaleId(r.getLocaleId());
				rlvo.setLocaleLabel(r.getLocaleLabel());
				rlvo.setResourceLabel(r.getResourceLabel());
				rlvo.setResourceTooltip(r.getResourceTooltip());
				rlvo.setLegendLabel(r.getLegendLabel());
				rlvo.setLegendTooltip(r.getLegendTooltip());
				resourceLocales.add(rlvo);
			}
		}
		return resourceLocales;
	}
	
	private List<PLE> getListOfDedicatedPlanElements(int type) {
		List<PLE> lstElem = new ArrayList<PLE>();
		for (PLE pe : getOrMigratePlanElements()) {
			if (pe.getType() == type) {
				lstElem.add(pe);
			}
		}
		return lstElem;
	}
	
	public List<PLE> getListEntriesOrMileStones(boolean withMileStone) {
		List<PLE> lstPlanElements = getListOfDedicatedPlanElements(PlanElement.ENTRY);
		if (withMileStone) {
			lstPlanElements.addAll(getListOfDedicatedPlanElements(PlanElement.MILESTONE));
		}
		return lstPlanElements;
	}

	public List<PLE> getListOfRelations() {
		return getListOfDedicatedPlanElements(PlanElement.RELATION);
	}

	@Deprecated
	public PLE getFirstEntry() {
		List<PLE> lstElem = getListEntriesOrMileStones(false);
		return lstElem.size() == 0 ? null : lstElem.get(0);
	}
	
	@Deprecated
	public PLE getFirstRelation() {
		List<PLE> lstElem = getListOfRelations();
		return lstElem.size() == 0 ? null : lstElem.get(0);
	}

	@XmlElement(name="planElements")
	public List<PLE> getPlanElements() {
		if (planElements == null) {
			planElements = new ArrayList<>();
		}
		return planElements;
	}

	public void setPlanElements(List<PLE> entries) {
		this.planElements = entries;
	}

	@XmlElement(name="entryEntity")
	private UID getEntryEntity() {
		return entryEntity;
	}

	private void setEntryEntity(UID entryEntity) {
		this.entryEntity = entryEntity;
	}

	@XmlElement(name="referenceField")
	private UID getReferenceField() {
		return referenceField;
	}

	private void setReferenceField(UID referenceField) {
		this.referenceField = referenceField;
	}
	
	@XmlElement(name="bookerField")
	private UID getBookerField() {
		return bookerField;
	}

	private void setBookerField(UID bookerField) {
		this.bookerField = bookerField;
	}
	
	@XmlElement(name="HolidaysEntity")
	public UID getHolidaysEntity() {
		return holidaysEntity;
	}

	public void setHolidaysEntity(UID holidaysEntity) {
		this.holidaysEntity = holidaysEntity;
	}
	
	@XmlElement(name="HolidaysDate")
	public UID getHolidaysDate() {
		return holidaysDateField;
	}

	public void setHolidaysDate(UID holidaysDate) {
		this.holidaysDateField = holidaysDate;
	}
	
	@XmlElement(name="HolidaysName")
	public UID getHolidaysName() {
		return holidaysNameField;
	}

	public void setHolidaysName(UID holidaysName) {
		this.holidaysNameField = holidaysName;
	}
	
	@XmlElement(name="HolidaysTip")
	public UID getHolidaysTip() {
		return holidaysTipField;
	}

	public void setHolidaysTip(UID holidaysTip) {
		this.holidaysTipField = holidaysTip;
	}
	
	@XmlElement(name="WithHolidays")
	public boolean getWithHolidays() {
		return withHolidays;
	}

	public void setWithHolidays(boolean withHolidays) {
		this.withHolidays = withHolidays;
	}

	@XmlElement(name="dateFromField")
	private UID getDateFromField() {
		return dateFromField;
	}

	private void setDateFromField(UID dateFromField) {
		this.dateFromField = dateFromField;
	}

	@XmlElement(name="dateToField")
	private UID getDateUntilField() {
		return dateUntilField;
	}

	private void setDateUntilField(UID dateUntilField) {
		this.dateUntilField = dateUntilField;
	}

	@XmlElement(name="timeFromField")
	private UID getTimeFromField() {
		return timeFromField;
	}

	private void setTimeFromField(UID timeFromField) {
		this.timeFromField = timeFromField;
	}

	@XmlElement(name="timeToField")
	private UID getTimeUntilField() {
		return timeUntilField;
	}

	private void setTimeUntilField(UID timeUntilField) {
		this.timeUntilField = timeUntilField;
	}

	@XmlElement(name="timePerods")
	private String getTimePeriodsString() {
		return timePeriodsString;
	}

	private void setTimePeriodsString(String timePeriodsString) {
		this.timePeriodsString = timePeriodsString;
	}

	@XmlElement(name="resourceLabelText")
	public String getResourceLabelText() {
		return resourceLabelText;
	}

	public void setResourceLabelText(String resourceLabelText) {
		this.resourceLabelText = resourceLabelText;
	}

	@XmlElement(name="resourceToolTipText")
	public String getResourceToolTipText() {
		return resourceToolTipText;
	}

	public void setResourceToolTipText(String resourceToolTipText) {
		this.resourceToolTipText = resourceToolTipText;
	}

	@XmlElement(name="entryLabelText")
	protected String getEntryLabelText() {
		return entryLabelText;
	}

	private void setEntryLabelText(String entryLabelText) {
		this.entryLabelText = entryLabelText;
	}

	@XmlElement(name="entryToolTipText")
	protected String getEntryToolTipText() {
		return entryToolTipText;
	}

	private void setEntryToolTipText(String entryToolTipText) {
		this.entryToolTipText = entryToolTipText;
	}

	@XmlElement(name="cornerLabelText")
	public String getCornerLabelText() {
		return cornerLabelText;
	}

	public void setCornerLabelText(String cornerLabelText) {
		this.cornerLabelText = cornerLabelText;
	}

	@XmlElement(name="cornerToolTipText")
	public void setCornerToolTipText(String cornerToolTipText) {
		this.cornerToolTipText = cornerToolTipText;
	}

	public String getCornerToolTipText() {
		return cornerToolTipText;
	}

	@XmlElement(name="scripting")
	public boolean isScriptingActivated() {
		return scriptingActivated;
	}

	public void setScriptingActivated(boolean scriptingActivated) {
		this.scriptingActivated = scriptingActivated;
	}

	@XmlElement(name="code")
	public String getScriptingCode() {
		return scriptingCode;
	}

	public void setScriptingCode(String scriptingCode) {
		this.scriptingCode = scriptingCode;
	}

	@XmlElement(name="backgroundPaintMethod")
	public String getScriptingBackgroundPaintMethod() {
		return backgroundPaintMethod;
	}

	public void setScriptingBackgroundPaintMethod(String backgroundPaintCellMethod) {
		this.backgroundPaintMethod = backgroundPaintCellMethod;
	}

	@XmlElement(name="resourceCellMethod")
	public String getScriptingResourceCellMethod() {
		return scriptingResourceCellMethod;
	}

	public void setScriptingResourceCellMethod(String scriptingResourceCellMethod) {
		this.scriptingResourceCellMethod = scriptingResourceCellMethod;
	}

	@XmlElement(name="entryCellMethod")
	public String getScriptingEntryCellMethod() {
		return scriptingEntryCellMethod;
	}

	public void setScriptingEntryCellMethod(String scriptingEntryCellMethod) {
		this.scriptingEntryCellMethod = scriptingEntryCellMethod;
	}
	
	@XmlElement(name="resourceLocales")
	private List<ResourceLocaleVO> getResourceLocales() {
		return resourceLocales;
	}

	public void setResourceLocales(List<ResourceLocaleVO> resourceLocales) {
		this.resourceLocales = resourceLocales;
	}
	
	@XmlElement(name="resources")
	protected List<ResPlanResourceVO> getResources() {
		return resources;
	}

	protected void setResources(List<ResPlanResourceVO> resources) {
		this.resources = resources;
	}
	
	@XmlElement(name="defaultViewFrom")
	public String getDefaultViewFrom() {
		return defaultViewFrom;
	}

	public void setDefaultViewFrom(String defaultViewFrom) {
		this.defaultViewFrom = defaultViewFrom;
	}

	@XmlElement(name="defaultViewUntil")
	public String getDefaultViewUntil() {
		return defaultViewUntil;
	}

	public void setDefaultViewUntil(String defaultViewUntil) {
		this.defaultViewUntil = defaultViewUntil;
	}
	
	@XmlElement(name="specialViewFromField")
	public UID getSpecialViewFromField() {
		return specialFiewFromField;
	}

	public void setSpecialViewFromField(UID specialFiewFromField) {
		this.specialFiewFromField = specialFiewFromField;
	}
	
	@XmlElement(name="specialViewUntilField")
	public UID getSpecialViewUntilField() {
		return specialViewUntilField;
	}

	public void setSpecialViewUntilField(UID specialFiewUntilField) {
		this.specialViewUntilField = specialFiewUntilField;
	}
	
	@XmlElement(name="specialViewFrom")
	public String getSpecialViewFrom() {
		return specialViewFrom;
	}

	public void setSpecialViewFrom(String specialViewFrom) {
		this.specialViewFrom = specialViewFrom;
	}
	
	@XmlElement(name="specialViewUntil")
	public String getSpecialViewUntil() {
		return specialViewUntil;
	}

	public void setSpecialViewUntil(String specialViewUntil) {
		this.specialViewUntil = specialViewUntil;
	}
	
	@XmlElement(name="dayOfWeek")
	public boolean getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(boolean dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	@XmlElement(name="milestoneField")
	private UID getMilestoneField() {
		return milestoneField;
	}

	private void setMilestoneField(UID milestoneField) {
		this.milestoneField = milestoneField;
	}

	@XmlElement(name="relationEntity")
	private UID getRelationEntity() {
		return relationEntity;
	}

	private void setRelationEntity(UID relationEntity) {
		this.relationEntity = relationEntity;
	}

	@XmlElement(name="relationFromField")
	private UID getRelationFromField() {
		return relationFromField;
	}

	private void setRelationFromField(UID relationFromField) {
		this.relationFromField = relationFromField;
	}

	@XmlElement(name="relationToField")
	private UID getRelationToField() {
		return relationToField;
	}

	private void setRelationToField(UID relationToField) {
		this.relationToField = relationToField;
	}

	@XmlElement(name="relationPresentation")
	private int getRelationPresentation() {
		return relationPresentation;
	}

	private void setRelationPresentation(int relationPresentation) {
		this.relationPresentation = relationPresentation;
	}

	@XmlElement(name="relationFromPresentation")
	private int getRelationFromPresentation() {
		return relationFromPresentation;
	}

	private void setRelationFromPresentation(int relationFromPresentation) {
		this.relationFromPresentation = relationFromPresentation;
	}

	@XmlElement(name="relationToPresentation")
	private int getRelationToPresentation() {
		return relationToPresentation;
	}

	private void setRelationToPresentation(int relationToPresentation) {
		this.relationToPresentation = relationToPresentation;
	}
	
	@XmlElement(name="newRelationFromController")
	private boolean isNewRelationFromController() {
		return newRelationFromController;
	}

	private void setNewRelationFromController(boolean newRelationFromController) {
		this.newRelationFromController = newRelationFromController;
	}

	public List<Pair<LocalTime, LocalTime>> getParsedTimePeriods() {
		PLE rpEntry = getFirstEntry();
		String timePeriodsString = rpEntry.getTimePeriodsString();
		if (StringUtils.looksEmpty(timePeriodsString))
			return null;
		return parseTimePeriodsString(timePeriodsString);
	}

	/*
	public byte[] toBytes() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		JAXB.marshal(this, out);
		return out.toByteArray();
	}
	 */

	public static List<Pair<LocalTime, LocalTime>> parseTimePeriodsString(String period) {
		List<Pair<LocalTime, LocalTime>> list = new ArrayList<Pair<LocalTime, LocalTime>>();
		if (period == null || period.trim().isEmpty())
			return list;
		for (String s : period.split("\\s*[ ,;]\\s*")) {
			String[] s2 = s.split("\\s*-\\s*", 2);
			if (s2.length != 2) {
				throw new IllegalArgumentException("Invalid time period string " + s);
			}
			list.add(Pair.makePair(LocalTime.parse(s2[0]), LocalTime.parse(s2[1])));
		}
		if (list.isEmpty()) {
			throw new IllegalArgumentException("Empty time period string");
		}
		return list;
	}
	
	public abstract PLE newPlanElementInstance();
}

