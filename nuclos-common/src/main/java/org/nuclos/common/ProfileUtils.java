package org.nuclos.common;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.preferences.TablePreferences;
import org.nuclos.common.preferences.TablePreferencesManager;

public class ProfileUtils {

	private static final Logger LOG = Logger.getLogger(ProfileUtils.class);

	public static TablePreferences findInProfiles(final List<TablePreferences> lstProfiles, final UID uid) {
		final Predicate<TablePreferences> predicate = new Predicate<TablePreferences>() {

			@Override
			public boolean evaluate(TablePreferences t) {
				return RigidUtils.equal(t.getUID(), uid);
			}
		};

		final TablePreferences profile = CollectionUtils.findFirst(lstProfiles, predicate);
		return profile;
	}

	public static TablePreferences findInAllProfiles(final TablePreferencesManager tblprefManager, final UID uid) {
		final Predicate<TablePreferences> predicate = new Predicate<TablePreferences>() {

			@Override
			public boolean evaluate(TablePreferences t) {
				return RigidUtils.equal(t.getUID(), uid);
			}
		};

		TablePreferences profile = CollectionUtils.findFirst(tblprefManager.getShared(), predicate);
		if (null == profile) {
			profile = CollectionUtils.findFirst(tblprefManager.getPrivate(), predicate);
		}
		return profile;
	}

	public static TablePreferences findActiveProfile(final List<TablePreferences> lstProfiles) {
		final Predicate<TablePreferences> predicate = new Predicate<TablePreferences>() {

			@Override
			public boolean evaluate(TablePreferences t) {
				return t.isSelected();
			}
		};

		final TablePreferences profile = CollectionUtils.findFirst(lstProfiles,predicate);
		return profile;
	}

	public static TablePreferences findActiveProfile(final TablePreferencesManager tblprefManager) {
		final Predicate<TablePreferences> predicate = new Predicate<TablePreferences>() {

			@Override
			public boolean evaluate(TablePreferences t) {
				return t.isSelected();
			}
		};

		TablePreferences profile = CollectionUtils.findFirst(tblprefManager.getShared(),predicate);
		if (null == profile) {
			profile = CollectionUtils.findFirst(tblprefManager.getPrivate(),predicate);
		}
		return profile;
	}

	/**
	 * @return minimum column width based on class
	 */
	public static int getMinimumColumnWidth(Class<?> c) {
		if (String.class == c)
			return 120;
		if (Integer.class == c)
			return 80;
		if (Double.class == c)
			return 80;
		if (Date.class == c)
			return 80;
		if (Boolean.class == c)
			return 40;
		if (org.nuclos.common.NuclosImage.class == c)
			return 40;
		
		return 120;
	}
	
	
	public static List<UID> getInitialTableColumnFields(Collection<FieldMeta<?>> entityFields) {

		final List<UID> fieldNames = new ArrayList<UID>();
		try {
			//FieldMeta<?> is already Comparable
			List<FieldMeta<?>> fields = new ArrayList<FieldMeta<?>>(entityFields);
			Collections.sort(fields);

			for (FieldMeta<?> efMeta : fields) {
				if (efMeta.isCalculated()) {
					continue;
				}

				if (efMeta.isHidden()) {
					continue;
				}

				if (SF.isEOField(efMeta.getFieldName())
						&& !SF.STATE.checkField(efMeta.getFieldName())
						&& !SF.STATEICON.checkField(efMeta.getFieldName())) {
					continue;
				}
				
				// NUCLOS-1798
				if (String.class.getCanonicalName().equals(efMeta.getDataType())) {
					if (efMeta.getScale() == null || efMeta.getScale() > 4000) {
						continue;
					}
					if (efMeta.getScale() > 255 && !E.isNuclosEntity(efMeta.getEntity())) { // NUCLOS-6553
						continue;
					}
				}

				// The number of columns should not exceed 30 to avoid performance problems of entities with many attributes
				boolean add = fieldNames.size() <= 30;

				if (SF.STATEICON.checkField(efMeta.getFieldName())) {
					// state icon always first
					fieldNames.add(0, efMeta.getUID());
					add = false;
				}

				if (add) {
					fieldNames.add(efMeta.getUID());
				}
			}
		} catch (Exception ex) {
			LOG.warn("Unable to get initial field order.", ex);
		}
		return fieldNames;
	}
	
}
