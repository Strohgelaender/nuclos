//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

/**
 * Preference types for use in org.nuclos.common.E._Preference.type in case of 'app = nuclos'
 */
public enum NuclosPreferenceType {

	CHART("chart", "http://wiki.nuclos.de/display/Entwicklung/Charts"),
	SEARCHTEMPLATE("searchtemplate", "http://wiki.nuclos.de/display/Entwicklung/Searchtemplates"),
	TABLE("table"),
	SUBFORMTABLE("subform-table"),
	TASKLIST_TABLE("tasklist-table"),
	SEARCHFILTER_TASKLIST_TABLE("searchfilter-tasklist-table"),
	// SUBFORMSEARCHTEMPLATE("subform-searchtemplate"),
	MENU("menu"),
	PERSPECTIVE("perspective"),
	PLANNINGTABLE("planning-table"),
	DASHBOARD("dashboard");

	private final String type;
	private final String description;

	NuclosPreferenceType(String type) {
		this(type, null);
	}

	NuclosPreferenceType(String type, String description) {
		this.type = type;
		this.description = description;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return type + " (" + description + ")";
	}

	public static NuclosPreferenceType get(String type) {
		if (type != null) {
			for (NuclosPreferenceType result : values()) {
				if (type.equals(result.getType())) {
					return result;
				}
			}
		}
		throw new IllegalArgumentException(NuclosPreferenceType.class.getCanonicalName() + ": Type '" + type + "' is unknown!");
	}

}
