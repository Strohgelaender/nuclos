//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

public class ConsoleConstants {
	public static final String CMD_SEND_MESSAGE = "-sendMessage";
	public static final String CMD_KILL_SESSION = "-killSession";
	public static final String CMD_CLEAR_USER_PREFERENCES = "-clearUserPreferences";
	public static final String CMD_INVALIDATE_ALL_CACHES = "-invalidateAllCaches";
	public static final String CMD_REBUILD_CLASSES = "-rebuildClasses";
	public static final String CMD_REBUILD_CONSTRAINTS = "-rebuildConstraints";
	public static final String CMD_REBUILD_INDEXES = "-rebuildIndexes";
	public static final String CMD_REBUILD_CONSTRAINTS_AND_INDEXES = "-rebuildConstraintsAndIndexes";
	public static final String CMD_COMPILE_DB_OBJECTS = "-compileDbObjects";
    public static final String CMD_CHECK_DOCUMENT_FILES = "-checkDocumentFiles";
    public static final String CMD_CLEANUP_DUPLICATE_DOCUMENTS = "-cleanupDuplicateDocuments";
    public static final String CMD_SET_MANDATOR_LEVEL = "-setMandatorLevel";
	public static final String CMD_SET_DATA_LANGUAGE = "-setDataLanguage";
    public static final String CMD_GENERATE_BO_UID_LIST = "-generateBoUidList";
    public static final String CMD_RESET_COMPLETE_MANDATOR_SETUP = "-resetCompleteMandatorSetup";
	public static final String CMD_ENABLE_INDEXER = "-enableIndexer";
	public static final String CMD_ENABLE_INDEXER_SYNCHRONOUSLY = "-enableIndexerSynchronously";
	public static final String CMD_DISABLE_INDEXER = "-disableIndexer";
	public static final String CMD_REBUILD_LUCENE_INDEX = "-rebuildLuceneIndex";
	public static final String CMD_SYNC_ACTIVE_WEBADDONS = "-syncActiveWebAddons";
}
