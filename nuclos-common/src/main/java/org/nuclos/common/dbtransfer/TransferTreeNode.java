//Copyright (C) 2018  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.mutable.MutableInt;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.navigation.treenode.nuclet.content.AbstractNucletContentEntryTreeNode;

public class TransferTreeNode extends DefaultMutableTreeNode {

	private static final long serialVersionUID = 1856460263158185793L;

	public enum CHANGE_TYPE_IMPORT {INSERT, UPDATE, DELETE}

	public enum CHANGE_TYPE_LOCAL {LOCAL_CHANGES, LOCAL_ADDED}

	private final String label;

	private CHANGE_TYPE_IMPORT changeTypeImport;

	private CHANGE_TYPE_LOCAL changeTypeLocal;

	private boolean ignoreChange = false;

	public TransferTreeNode(final TransferEO teo, final String label) {
		super(teo);
		this.label = label;
	}

	public TransferTreeNode() {
		super();
		this.label = "ROOT";
	}

	private static final Map<UID, Integer> ENTITY_ORDER_MAP = new HashMap<>();
	static {
		final List<EntityMeta<UID>> nucletContentEntities = AbstractNucletContentEntryTreeNode.getNucletContentEntities();
		for (int i = 0; i < nucletContentEntities.size(); i++) {
			final EntityMeta<UID> entityMeta = nucletContentEntities.get(i);
			ENTITY_ORDER_MAP.put(entityMeta.getUID(), i);
		}
	}

	public boolean isIgnoreChange() {
		return ignoreChange;
	}

	public void setIgnoreChange(final boolean ignoreChange) {
		this.ignoreChange = ignoreChange;
		for (int i = 0; i < getChildCount(); i++) {
			final TransferTreeNode child = getChildAt(i);
			child.setIgnoreChange(ignoreChange);
		}
	}

	public void setChangeType(final CHANGE_TYPE_IMPORT type) {
		changeTypeImport = type;
	}

	public void setChangeType(final CHANGE_TYPE_LOCAL type) {
		changeTypeLocal = type;
	}

	public String getLabel() {
		return label;
	}

	public CHANGE_TYPE_IMPORT getChangeTypeImport() {
		return changeTypeImport;
	}

	public CHANGE_TYPE_LOCAL getChangeTypeLocal() {
		return changeTypeLocal;
	}

	public boolean isChangeType(CHANGE_TYPE_IMPORT type) {
		return RigidUtils.equal(changeTypeImport, type);
	}

	public boolean isChangeType(CHANGE_TYPE_LOCAL type) {
		return RigidUtils.equal(changeTypeLocal, type);
	}

	public CHANGE_TYPE_IMPORT getChangeTypeImportWithChildren() {
		if (changeTypeImport != null) {
			return changeTypeImport;
		}
		CHANGE_TYPE_IMPORT result = null;
		for (int i = 0; i < getChildCount(); i++) {
			final TransferTreeNode child = getChildAt(i);
			final CHANGE_TYPE_IMPORT changeTypeWithChildren = child.getChangeTypeImportWithChildren();
			if (changeTypeWithChildren != null) {
				return CHANGE_TYPE_IMPORT.UPDATE;
			}
		}
		return result;
	}

	public CHANGE_TYPE_LOCAL getChangeTypeLocalWithChildren() {
		if (changeTypeLocal != null) {
			return changeTypeLocal;
		}
		CHANGE_TYPE_LOCAL result = null;
		for (int i = 0; i < getChildCount(); i++) {
			final TransferTreeNode child = getChildAt(i);
			final CHANGE_TYPE_LOCAL changeTypeWithChildren = child.getChangeTypeLocalWithChildren();
			if (changeTypeWithChildren != null) {
				return CHANGE_TYPE_LOCAL.LOCAL_CHANGES;
			}
		}
		return result;
	}

	public void countChanges(MutableInt countImportChanges, MutableInt countLocalChanges) {
		if (getChangeTypeImport() != null) {
			countImportChanges.increment();
		}
		if (getChangeTypeLocal() != null) {
			countLocalChanges.increment();
		}
		for (int i = 0; i < getChildCount(); i++) {
			final TransferTreeNode child = getChildAt(i);
			child.countChanges(countImportChanges, countLocalChanges);
		}
	}

	public void countIgnoreChangesWithChildren(MutableInt countChangedIgnores, MutableInt countChangedDo, MutableInt countChanged) {
		if (changeTypeImport != null || changeTypeLocal != null) {
			countChanged.increment();
			if (isIgnoreChange()) {
				countChangedIgnores.increment();
			} else {
				countChangedDo.increment();
			}
		}
		for (int i = 0; i < getChildCount(); i++) {
			final TransferTreeNode child = getChildAt(i);
			child.countIgnoreChangesWithChildren(countChangedIgnores, countChangedDo, countChanged);
		}
	}

	public TransferEO getTransferEO() {
		return (TransferEO) getUserObject();
	}

	@Override
	public TransferTreeNode getChildAt(final int index) {
		return (TransferTreeNode) super.getChildAt(index);
	}

	public TransferTreeNode getChild(final TransferEO teo) {
		return getChild(teo.eo.getDalEntity(), teo.getUID());
	}

	public TransferTreeNode getChild(final UID entityUID, final UID pk) {
		final TransferEO thisTeo = getTransferEO();
		if (thisTeo != null && new EqualsBuilder()
				.append(thisTeo.getUID(), pk)
				.append(thisTeo.eo.getDalEntity(), entityUID)
				.isEquals()) {
			return this;
		}
		for (int j = 0; j < getChildCount(); j++) {
			final TransferTreeNode child = (TransferTreeNode) getChildAt(j);
			final TransferTreeNode foundChild = child.getChild(entityUID, pk);
			if (foundChild != null) {
				return foundChild;
			}
		}
		return null;
	}

	public TransferTreeNode mergeWith(final TransferTreeNode treeToAdd) {
		for (int i = 0; i < treeToAdd.getChildCount(); i++) {
			final TransferTreeNode addingChild = treeToAdd.getChildAt(i);
			TransferTreeNode targetChildFound = null;
			for (int j = 0; j < getChildCount(); j++) {
				final TransferTreeNode targetChild = getChildAt(j);
				if (RigidUtils.equal(targetChild, addingChild)) {
					targetChildFound = targetChild;
					break;
				}
			}
			if (targetChildFound != null) {
				targetChildFound.mergeWith(addingChild);
			} else {
				add(addingChild);
				// we edit the child list as we read it, we have to adjust the loop index
				i--;
			}
		}
		return this;
	}

	private static int getEntityOrder(UID entityUID) {
		return RigidUtils.defaultIfNull(ENTITY_ORDER_MAP.get(entityUID), 255).intValue();
	}

	/**
	 * add newChild ordered!
	 *
	 * @param newChild
	 */
	@Override
	public void add(final MutableTreeNode newChild) {
		if (getTransferEO() == null) { // ROOT
			super.add(newChild);
			return;
		}
		final TransferTreeNode ttn = (TransferTreeNode) newChild;
		final int newChildEntityOrder = getEntityOrder(ttn.getTransferEO().eo.getDalEntity());
		Integer insertPoint = null;
		Integer lastLabelCompare = null;
		for (int j = 0; j < getChildCount(); j++) {
			final TransferTreeNode child = (TransferTreeNode) getChildAt(j);
			final TransferEO teo = child.getTransferEO();
			final int currentChildEntityOrder = getEntityOrder(teo.eo.getDalEntity());
			if (newChildEntityOrder < currentChildEntityOrder) {
				if (insertPoint == null) {
					insertPoint = j;
				}
				break; // stop compare
			} else if (newChildEntityOrder == currentChildEntityOrder) {
				final int iLabelCompare = StringUtils.compareIgnoreCase(ttn.label, child.label);
				if (lastLabelCompare != null && lastLabelCompare.intValue() < 0 && iLabelCompare < 0) {
					break;
				}
				if (iLabelCompare <= 0) {
					insertPoint = j;
				} else {
					insertPoint = j+1;
				}
				if (lastLabelCompare != null && lastLabelCompare.intValue() < 0 && iLabelCompare > 0) {
					break;
				}
				lastLabelCompare = iLabelCompare;
			}
		}
		if (insertPoint == null || insertPoint.intValue() >= getChildCount()) {
			super.add(newChild);
		} else {
			super.insert(newChild, insertPoint.intValue() < 0 ? 0 : insertPoint.intValue());
		}
	}

	/**
	 * new view of import "keep local changes where possible - but in case of conflict: incoming change > local change"
	 */
	public void setDefaultIgnoreToKeepLocalChanges() {
		// set default ignores (only overwrite local changes in case of an importing update)
		if (getChangeTypeLocal() != null && getChangeTypeImport() == null) {
			setIgnoreChange(true);
		}
		for (int j = 0; j < getChildCount(); j++) {
			final TransferTreeNode child = getChildAt(j);
			child.setDefaultIgnoreToKeepLocalChanges();
		}
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) return true;

		if (!(o instanceof TransferTreeNode)) return false;

		final TransferTreeNode that = (TransferTreeNode) o;

		return new EqualsBuilder()
				.append(getUserObject(), that.getUserObject())
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.append(getUserObject())
				.toHashCode();
	}

	@Override
	public String toString() {
		return (getTransferEO() != null ? getTransferEO().eo.getDalEntity().debugString() + ": " : "") +
			   (label == null ? super.toString() : label) +
			   (changeTypeLocal != null ? (" changeTypeLocal="+changeTypeLocal) : "") +
			   (changeTypeImport != null ? (" changeTypeImport="+changeTypeImport) : "");
	}
}
