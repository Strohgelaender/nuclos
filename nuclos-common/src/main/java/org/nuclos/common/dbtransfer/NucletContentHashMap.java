//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dbtransfer;

import java.io.Serializable;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collection.multimap.MultiListHashMap;

public class NucletContentHashMap extends MultiListHashMap<EntityMeta<UID>, TransferEO> implements NucletContentMap, Serializable {
	
	private static final long serialVersionUID = 941535288701043416L;

	@Override
	public void addAll(NucletContentMap map) {
		for (EntityMeta<UID> entity : map.keySet()) {
			this.addAllValues(entity, map.getValues(entity));
		}
	}

	@Override
	public void add(TransferEO teo) {
		EntityMeta<UID> entity = E.getByUID(teo.eo.getDalEntity());
		if (entity != null) {
			this.addValue(entity, teo);
		}
	}

}
