//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.security;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

/**
 * defines a unique key to get the permission for an attribute used in a module within a status numeral
 */
public class AttributePermissionKey {
	private UID entityUID;
	private UID attributeUID;
	private UID stateUID;

	public AttributePermissionKey(UID entityUID, UID attributeUID, UID stateUID) {
		this.entityUID = entityUID;
		this.attributeUID = attributeUID;
		this.stateUID = stateUID;
	}

	public UID getEntityUID() {
		return this.entityUID;
	}

	public UID getAttributeUID() {
		return this.attributeUID;
	}

	public UID getStateUID() {
		return this.stateUID;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || (this.getClass() != o.getClass())) {
			return false;
		}
		final AttributePermissionKey that = (AttributePermissionKey) o;
		return LangUtils.equal(this.attributeUID, that.attributeUID) &&
		LangUtils.equal(this.entityUID, that.entityUID) &&
		LangUtils.equal(this.stateUID, that.stateUID);
	}

	@Override
	public int hashCode() {
		return LangUtils.hashCode(this.entityUID) ^ LangUtils.hashCode(this.attributeUID) ^ LangUtils.hashCode(this.stateUID);
	}
}	// class AttributePermissionKey
