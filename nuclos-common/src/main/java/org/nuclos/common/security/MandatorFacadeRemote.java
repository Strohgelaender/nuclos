package org.nuclos.common.security;

import org.nuclos.common.MandatorVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.valueobject.MandatorUserVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

public interface MandatorFacadeRemote {

	public MandatorVO create(MandatorVO vo) throws CommonBusinessException;

	public MandatorVO modify(MandatorVO vo, IDependentDataMap mpDependants, String customUsage) throws CommonBusinessException;

	public void remove(MandatorVO vo) throws CommonBusinessException;


}

