//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.security;

import java.util.Date;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.server.common.valueobject.NuclosValueObject;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Value object representing a user for administration purposes (exclude preferences).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class UserVO extends NuclosValueObject<UID> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -878255780140323253L;
	private String name;
	private String email;
	private String lastname;
	private String firstname;
	private Boolean superuser;
	private Boolean locked;
	private Date passwordChanged;
	private Date expirationDate;
	private Boolean requirePasswordChange;
	private UID communicationAccountPhone;
	private String activationcode;
	private String passwordresetcode;

	// non-persistent attributes
	private Boolean setPassword;
	private String newPassword;
	private Boolean notifyUser;
	private Boolean privacyconsent;

	private Map<UID, Object> originalFields;

	public UserVO(MasterDataVO<UID> mdvo) {
		super(mdvo.getId(), mdvo.getCreatedAt(), mdvo.getCreatedBy(), mdvo.getChangedAt(), mdvo.getChangedBy(), mdvo.getVersion());
		this.name = mdvo.getFieldValue(E.USER.name);
		this.email = mdvo.getFieldValue(E.USER.email);
		this.lastname = mdvo.getFieldValue(E.USER.lastname);
		this.firstname = mdvo.getFieldValue(E.USER.firstname);
		this.superuser = mdvo.getFieldValue(E.USER.superuser);
		this.locked = mdvo.getFieldValue(E.USER.locked);
		this.passwordChanged = mdvo.getFieldValue(E.USER.passwordchanged);
		this.expirationDate = mdvo.getFieldValue(E.USER.expirationdate);
		this.requirePasswordChange = mdvo.getFieldValue(E.USER.requirepasswordchange);
		this.originalFields = mdvo.getFieldValues();
		this.communicationAccountPhone = mdvo.getFieldUid(E.USER.communicationAccountPhone); 
		this.activationcode = mdvo.getFieldValue(E.USER.activationcode);
		this.privacyconsent = mdvo.getFieldValue(E.USER.privacyConsent);
		this.passwordresetcode = mdvo.getFieldValue(E.USER.passwordresetcode);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Boolean getSuperuser() {
		return superuser != null ? superuser : Boolean.FALSE;
	}

	public void setSuperuser(Boolean superuser) {
		this.superuser = superuser;
	}

	public boolean getLocked() {
		return locked != null ? locked : Boolean.FALSE;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Date getPasswordChanged() {
		return passwordChanged;
	}

	public void setPasswordChanged(Date passwordChanged) {
		this.passwordChanged = passwordChanged;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Boolean getRequirePasswordChange() {
		return requirePasswordChange != null ? requirePasswordChange : Boolean.FALSE;
	}

	public void setRequirePasswordChange(Boolean requirePasswordChange) {
		this.requirePasswordChange = requirePasswordChange;
	}

	public Boolean getSetPassword() {
		return setPassword != null ? setPassword : Boolean.FALSE;
	}

	public void setSetPassword(Boolean setPassword) {
		this.setPassword = setPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public Boolean getNotifyUser() {
		return notifyUser == null ? Boolean.FALSE : notifyUser;
	}

	public void setNotifyUser(Boolean notifyUser) {
		this.notifyUser = notifyUser;
	}

	public UID getCommunicationAccountPhone() {
		return communicationAccountPhone;
	}

	public void setCommunicationAccountPhone(UID communicationAccountPhone) {
		this.communicationAccountPhone = communicationAccountPhone;
	}

	public String getActivationcode() {
		return activationcode;
	}

	public void setActivationcode(String activationcode) {
		this.activationcode = activationcode;
	}
	
	public Boolean isPrivacyConsent() {
		return privacyconsent == null ? Boolean.FALSE : privacyconsent;
	}
	
	public void setPrivacyConsent(Boolean privacyconsent) {
		this.privacyconsent = privacyconsent;
	}

	public String getPasswordresetcode() {
		return passwordresetcode;
	}

	public void setPasswordresetcode(String passwordresetcode) {
		this.passwordresetcode = passwordresetcode;
	}

	public MasterDataVO<UID> toMasterDataVO() {
		MasterDataVO<UID> mdvo = new MasterDataVO<UID>(E.USER.getUID(), getId(), 
				getCreatedAt(), getCreatedBy(), getChangedAt(), getChangedBy(), getVersion(), null, null, null, false);
		mdvo.getEntityObject().setComplete(true);
		
		mdvo.setFieldValue(E.USER.name, name);
		mdvo.setFieldValue(E.USER.email, email);
		mdvo.setFieldValue(E.USER.lastname, lastname);
		mdvo.setFieldValue(E.USER.firstname, firstname);
		mdvo.setFieldValue(E.USER.superuser, superuser);
		mdvo.setFieldValue(E.USER.locked, locked);
		mdvo.setFieldValue(E.USER.passwordchanged, passwordChanged);
		mdvo.setFieldValue(E.USER.expirationdate, expirationDate);
		mdvo.setFieldValue(E.USER.requirepasswordchange, requirePasswordChange);
		mdvo.setFieldUid(E.USER.communicationAccountPhone, communicationAccountPhone);
		mdvo.setFieldValue(E.USER.activationcode, activationcode);
		mdvo.setFieldValue(E.USER.privacyConsent, privacyconsent);
		mdvo.setFieldValue(E.USER.passwordresetcode, passwordresetcode);

		return mdvo;
	}

	@Override
	public int hashCode() {
		return (getName() != null ? getName().hashCode() : 0);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof UserVO) {
			final UserVO that = (UserVO) o;
			// rules are equal if there names are equal
			return this.getName().equals(that.getName());
		}
		return false;
	}

	@Override
	public String toString() {
		return this.getName();
	}

}	// class UserVO
