//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.CaseFormat;

/**
 * TODO: I18n for description
 * TODO: Proper type for arguments with "optional" flag, instead of simple Strings
 */
public enum ConsoleCommand {
	CHECK_DOCUMENT_FILES("[-deleteUnusedFiles] [-deleteDatabaseRecords <full qualified business object name 1> <fqbon 2> ...]", "Checks the validity of all document attachments."),
	CLEANUP_DUPLICATE_DOCUMENTS(null, "Generates a hash value per file and remove duplicates."),
	CLEAR_USER_PREFERENCES("<user name>", "Resets the preferences for the user with the given name."),
	COMPILE_DB_OBJECTS(null, "Compile all invalid db functins and views."),
	DISABLE_INDEXER(null, null),
	ENABLE_INDEXER(null, null),
	ENABLE_INDEXER_SYNCHRONOUSLY(null, null),
	GENERATE_BO_UID_LIST(null, null),
	INVALIDATE_ALL_CACHES(null, "Invalidates all system caches (Like Nuclet import)"),
	KILL_SESSION("[-user <username>]", "Kill the client of the specified user (or all users if no one is specified)."),
	REBUILD_CLASSES(null, "Rebuilds all java classes (Like Nuclet import)"),
	REBUILD_CONSTRAINTS(null, "Rebuilds unique and foreign constraints that are defined via entity wizard."),
	REBUILD_CONSTRAINTS_AND_INDEXES(null, "Rebuilds both at once."),
	REBUILD_INDEXES(null, "Rebuilds all indexes that are defined via entity wizard."),
	REBUILD_LUCENE_INDEX(null, null),
	RESET_COMPLETE_MANDATOR_SETUP(null, null),
	SEND_MESSAGE("-message <message text> [-user <username>] [-priority <high, normal, low>] [-author <author>]", "Send a message to the specified user (or all users if no one is specified)."),
	SET_DATA_LANGUAGE(null, null),
	SET_MANDATOR_LEVEL("-package <nuclet package> -bo <name of the business object> -level <mandator level (1, 2, ...)> [-initial <path of the initial mandator>]", "Set mandator level in multiple business objects.\nDo not change order of the arguments. Separate multiple updates with a blank or a new line."),
	SHOW_MAINTENANCE(null, "Shows the status of the maintenance mode."),
	START_MAINTENANCE(null, "Starts the maintenance mode."),
	STOP_MAINTENANCE(null, "Ends the maintenance mode."),
	SYNC_ACTIVE_WEBADDONS(null, "Sync all active web addons files from file system to database (Nuclet) for development.");

	private final String command;
	private final String arguments;
	private final String[] descriptionLines;

	ConsoleCommand(
			final String arguments,
			final String description
	) {
		this.command = "-" + CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, this.name());
		this.arguments = arguments;
		this.descriptionLines = StringUtils.split(description, "\n");
	}

	public String getCommand() {
		return command;
	}

	public String[] getDescriptionLines() {
		return descriptionLines;
	}

	public String getArguments() {
		return arguments;
	}

	/**
	 * Usage = commands + arguments
	 */
	public String getUsage() {
		String usage = command;

		if (StringUtils.isNotBlank(arguments)) {
			usage += " " + arguments;
		}

		return usage;
	}

	static ConsoleCommand fromValue(String command) {
		for (ConsoleCommand constant : values()) {
			if (StringUtils.equalsIgnoreCase(constant.getCommand(), command)) {
				return constant;
			}
		}
		throw new IllegalArgumentException("Unknown command: " + command);
	}
}
