//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.masterdata;

import java.util.prefs.Preferences;

import org.apache.log4j.Logger;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.RigidFile;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableComponentTypes;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;

/**
 * Structure (meta data) of a foreign key field in a masterdata table.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: Consider org.nuclos.client.common.CollectableEntityFieldPreferencesUtil
 * to write to {@link Preferences}.
 * 
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class CollectableMasterDataForeignKeyEntityField<PK> extends AbstractCollectableEntityField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4365462411773695403L;

	private static final Logger LOG = Logger.getLogger(CollectableMasterDataForeignKeyEntityField.class);

	private final FieldMeta<PK> fieldMeta;

	/**
	 * creates a <code>CollectableEntityField</code> representing the foreign key field <code>fieldMeta</code>.
	 * The collection of enumerated fields is built lazily.
	 * @param fieldMeta
	 */
	public CollectableMasterDataForeignKeyEntityField(FieldMeta<PK> fieldMeta) {
		this.fieldMeta = fieldMeta;
		final UID foreignEntity = LangUtils.firstNonNull(fieldMeta.getForeignEntity(), fieldMeta.getUnreferencedForeignEntity());
		if (foreignEntity == null) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("clctmdfkef.missing.foreignentity.error", fieldMeta.getUID()));
		}
	}

	/**
	 * §postcondition result != null
	 */
	@Override
	public UID getReferencedEntityUID() {
		final UID result = LangUtils.firstNonNull(this.fieldMeta.getForeignEntity(), this.fieldMeta.getUnreferencedForeignEntity());
		assert result != null;
		return result;
	}

	@Override
	public String getReferencedEntityFieldName() {
		return LangUtils.firstNonNull(this.fieldMeta.getForeignEntityField(), this.fieldMeta.getUnreferencedForeignEntityField());
	}

	/**
	 * §postcondition result
	 * 
	 * @return always <code>true</code>
	 */
	@Override
	public boolean isReferencing() {
		return true;
	}

	@Override
	public UID getUID() {
		return this.fieldMeta.getUID();
	}

	@Override
	public String getFormatInput() {
		return fieldMeta.getFormatInput();
	}

	@Override
	public String getFormatOutput() {
		return fieldMeta.getFormatOutput();
	}

	@Override
	public int getFieldType() {
		return CollectableField.TYPE_VALUEIDFIELD;
	}

	@Override
	public Class<?> getJavaClass() {
		try {
			if (this.fieldMeta.getJavaClass() == UID.class
					&& !GenericObjectDocumentFile.class.getCanonicalName().equals(this.fieldMeta.getDataType())
					) {
				return String.class;
			}
			return Class.forName(this.fieldMeta.getDataType());
		} catch (ClassNotFoundException e) {
			throw new CommonFatalException(e);
		}
	}

	@Override
	public String getDescription() {
		return SpringLocaleDelegate.getInstance().getTextFallback(fieldMeta.getLocaleResourceIdForDescription(), "");
	}

	@Override
	public String getLabel() {
		return SpringLocaleDelegate.getInstance().getTextFallback(fieldMeta.getLocaleResourceIdForLabel(), fieldMeta.getFallbackLabel());
	}

	@Override
	public Integer getMaxLength() {
		return this.fieldMeta.getScale();
	}

	@Override
	public Integer getPrecision() {
		return this.fieldMeta.getPrecision();
	}

	@Override
	public boolean isNullable() {
		return this.fieldMeta.isNullable();
	}

	@Override
	public int getDefaultCollectableComponentType() {
		if (this.fieldMeta.isFileDataType())
			return CollectableComponentTypes.TYPE_FILECHOOSER;
		return (this.fieldMeta.isSearchable()) ? CollectableComponentTypes.TYPE_LISTOFVALUES : super.getDefaultCollectableComponentType();
	}

	@Override
	public boolean isRestrictedToValueList() {
	// see NUCLOSINT-442
		return true;
	}

	@Override
	public CollectableField getDefault() {
		try {
			final String sDefault = this.fieldMeta.getDefaultValue();
			if(sDefault == null || sDefault.length() == 0) {
				return super.getDefault();
			}
			else if(this.fieldMeta.getForeignEntity() == null || this.fieldMeta.getForeignEntityField() == null) {
				return super.getDefault();
			}
			else {
				Object o = SpringApplicationContextHolder.getBean("enumeratedDefaultValueProvider");
				if (o != null && o instanceof EnumeratedDefaultValueProvider) {
					return ((EnumeratedDefaultValueProvider)o).getDefaultValue(this.fieldMeta);
				}
			}
		}
		catch(Exception e) {
			// on exception return super.getDefault()
			LOG.info("getDefault: " + e);
			return super.getDefault();
		}
		return super.getDefault();
	}

	@Override
	public UID getEntityUID() {
		return this.fieldMeta.getEntity();
	}

	@Override
	public String getDefaultComponentType() {
		return this.fieldMeta.getDefaultComponentType();
	}

	@Override
	public boolean isCalculated() {
		return fieldMeta.isCalculated();
	}

	@Override
	public boolean isCalcOndemand() {
		return fieldMeta.isCalcOndemand();
	}

	@Override
	public boolean isCalcAttributeAllowCustomization() {
		return fieldMeta.isCalcAttributeAllowCustomization();
	}

	@Override
	public boolean isHidden() {
		return fieldMeta.isHidden();
	}

	@Override
	public boolean isModifiable() {
		return fieldMeta.isModifiable();
	}

	@Override
	public String getName() {
		return fieldMeta.getFieldName();
	}

}	// class CollectableMasterDataForeignKeyEntityField
