//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.textmodule;

import java.io.Serializable;

import org.nuclos.common.UID;

public class TextModuleSettings implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2652695740827460088L;
	private final UID uidEntity;
	private final UID uidEntityField;
	private final UID uidEntityFieldName;
	private final UID uidEntityFieldSort;
	
	
	public TextModuleSettings(UID uidEntity, UID uidEntityFieldName, UID uidEntityField, UID uidEntityFieldSort) {
		super();
		if (null == uidEntity || null == uidEntityField) {
			throw new IllegalArgumentException("entity, entityField must not be null.");
		}
		this.uidEntity = uidEntity;
		this.uidEntityField = uidEntityField;
		this.uidEntityFieldName = uidEntityFieldName;
		this.uidEntityFieldSort = uidEntityFieldSort;
	}
	public UID getEntity() {
		return uidEntity;
	}
	
	public UID getEntityFieldName() {
		return uidEntityFieldName;
	}
	
	public UID getEntityField() {
		return uidEntityField;
	}
	public UID getEntityFieldSort() {
		return uidEntityFieldSort;
	}
}
