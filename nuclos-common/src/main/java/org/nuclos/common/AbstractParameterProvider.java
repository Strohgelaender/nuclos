//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.security.ProtectionDomain;

import org.nuclos.common2.LangUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract implementation for <code>ParameterProvider</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public abstract class AbstractParameterProvider implements ParameterProvider {
	
	private static final Logger LOG = LoggerFactory.getLogger(AbstractParameterProvider.class);

	@Override
	public int getIntValue(String sParameterName, int iDefaultValue) {
		int result;
		try {
			result = Integer.parseInt(this.getValue(sParameterName));
		}
		catch (Exception ex) {
			LOG.debug("Parameter \"{}\" cannot be retrieved from the parameter table.",
			          sParameterName);
			result = iDefaultValue;
		}
		return result;
	}

	/**
	 * @param sParameterName
	 * @return the boolean value from the parameters or the default [false] if parameter not set
	 */
	@Override
	public boolean isEnabled(String sParameterName) {
		return isEnabled(sParameterName, false);
	}

	/**
	 *
	 * @param sParameterName
	 * @param bDefaultValue default value for parameter if not set
	 * @return the boolean value from the parameters or the default if parameter not set
	 */
	@Override
	public boolean isEnabled(String sParameterName, boolean bDefaultValue) {
		String sValue = getValue(sParameterName);
		if (sValue != null) {
			sValue = sValue.trim();
			if ("1".equals(sValue) ||
					"true".equalsIgnoreCase(sValue) ||
					"y".equalsIgnoreCase(sValue) ||
					"yes".equalsIgnoreCase(sValue) ||
					"on".equalsIgnoreCase(sValue) ||
					"enable".equals(sValue) ||
					"enabled".equals(sValue)) {
				return true;
			} else {
				return false;
			}
		}
		
		return bDefaultValue;
	}
	
	/**
	 * Default value of {@link #JASPER_REPORTS_COMPILE_CPJAR_BYCLASSES} is no value is given.
	 *
	 * @since Nuclos 3.2.9
	 */
	private final String JASPER_REPORTS_DEFAULT_COMPILE_CPJAR_BYCLASSES_DEFAULT_VALUE
			= "net.sf.jasperreports.engine.JasperReport org.nuclos.server.report.api.JRNuclosDataSource";

	@Override
	public String getJRClassPath() {
		String value = getValue(JASPER_REPORTS_COMPILE_CPJAR_BYCLASSES);
		if (value == null) {
			// default value (if parameter is not set)
			value = JASPER_REPORTS_DEFAULT_COMPILE_CPJAR_BYCLASSES_DEFAULT_VALUE;
		}
		try {
			return getClassPathFor(value);
		} catch (ClassNotFoundException e) {
			LOG.warn("Unable to preload reports because of JasperReports class path: ", e);
			// default value (if classes in value could not be found)
			value = JASPER_REPORTS_DEFAULT_COMPILE_CPJAR_BYCLASSES_DEFAULT_VALUE;
			try {
				return getClassPathFor(value);
			} catch (ClassNotFoundException e1) {
				// bail out if even the default could not be found in classpath
				throw new IllegalArgumentException(e1);
			}
		}
	}

	private static String getClassPathFor(String classes) throws ClassNotFoundException {
		if (classes == null) return null;
		final String[] klass = classes.split("[ \t\n\r]+");
		final int len = klass.length;
		if (len < 1) return null;
		final ClassLoader cl = LangUtils.getClassLoaderThatWorksForWebStart();
		final Class<?>[] cs = new Class<?>[len];
		for (int i = 0; i < len; ++i) {
			cs[i] = cl.loadClass(klass[i]);
		}
		return getClassPathFor(cs);
	}
	
	private static String getClassPathFor(Class<?>... classes) {
		StringBuilder sb = new StringBuilder();
		for (Class<?> clazz : classes) {
			if (sb.length() > 0) {
				sb.append(File.pathSeparator);
			}
			try {
				ProtectionDomain protectionDomain = clazz.getProtectionDomain();
				CodeSource codeSource = protectionDomain.getCodeSource();
				URL location = codeSource.getLocation();
				String path = location.getFile();
				// still some inner JAR URL stuff? (tp)
				final int end = path.indexOf("!/");
				if (end >= 0) {
					// get rid of the junk (tp)
					path = new URL(path.substring(0, end)).getFile();
				}
				path = URLDecoder.decode(path, "UTF-8");
				sb.append(path);
			}
			catch (Exception e) {
				throw new NuclosFatalException("Cannot configure JasperReports classpath ", e);
			}
		}
		return sb.toString();
	}

}	// class AbstractParameterProvider
