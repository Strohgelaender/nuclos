//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

/**
 * Marker to indicate that the implementing class contains a crypto hash
 * of a major part of its content.
 * <p>
 * Presently used for java code compiled by Nuclos (rules, business objects, 
 * business support objects).
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.14.18, 3.15.18, 4.0.15
 */
public interface HasCryptoHash {
	
	/**
	 * Returns a String representation of calculated crypto hash. No contract 
	 * about the representation (e.g. binary, hex, base64) is made. However, 
	 * the same object is guaranteed to use only <em>one</em> representation. Per
	 * consequence the representation could be used with 
	 * {@link Object#equals(Object)}.
	 */
	String getHashValue();

}
