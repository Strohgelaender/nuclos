//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Created on 26.08.2009, adapted 13.11.2013
 */
package org.nuclos.common.caching;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.nuclos.common.collection.Pair;

/**
 * Deprecated: use GuavaCache
 */
@Deprecated
public class TimedCache<K, V> implements NBCache<K, V> {
	
	private static final Logger LOG = Logger.getLogger(TimedCache.class);
		
	private LookupProvider<K, V> look;
	private long maxAgeMillis;
	private Map<K, Pair<V, Long>> map;
	
	public TimedCache(LookupProvider<K, V> lookupProvider, int maxAgeSeconds) {
		this(lookupProvider, maxAgeSeconds * 1000L);
	}

	public TimedCache(LookupProvider<K, V> lookupProvider, long maxAgeMilliSeconds) {
		this.look = lookupProvider;
		this.maxAgeMillis = maxAgeMilliSeconds;
		this.map = new ConcurrentHashMap<>();
	}
	
	@Override
	public void clear() {
		map.clear();
	}
	
	@Override
	public V get(K key) {		
		V v;
		synchronized (map) {
			long ct = System.currentTimeMillis();
			if(map.containsKey(key)) {
				Pair<V, Long> p = map.get(key);
				if(p != null && (ct - p.y) < maxAgeMillis) {
					log("cache hit for key " + key);
					return p.x;
				} else {
					if(p != null) {
						log("cache expired for key " + key + " (" + (ct - p.y) + " >= " + maxAgeMillis +  ")");
					}
					else {
						log("no time comparison possible");
					}
				}
			} else {
				log("cache miss for key " + key);
			}
			// reached this point: either the key is not present, or the value is too old
			v = look.lookup(key);
			map.put(key, new Pair<V, Long>(v, ct));			
		}
		return v;
	}
	
	public void remove(K key) {
		map.remove(key);
	}
	
	private static void log(String msg) {
		if (LOG.isDebugEnabled()) {
			LOG.debug(msg);
		}
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getSimpleName()).append("[");
		result.append("prov=").append(look);
		result.append(",mapsize=").append(map.size());
		result.append(",maxAgeMillis=").append(maxAgeMillis);
		result.append("]");
		return result.toString();
	}
}
