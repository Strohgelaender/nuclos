//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.format;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.LangUtils;

public abstract class FormattingUIDTransformer implements Transformer<UID, String> {
	
	private final boolean bFormat;
	private final IMetaProvider metaProvider;
	
	public FormattingUIDTransformer() {
		this(true);
	}
	public FormattingUIDTransformer(boolean bFormat) {
		this(bFormat, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}
	public FormattingUIDTransformer(boolean bFormat, IMetaProvider metaProvider) {
		this.bFormat = bFormat;
		this.metaProvider = metaProvider;
	}

	@Override
	public String transform(UID fieldUID) {
		
		Object val = getValue(fieldUID);
		if (val == null) {
			return "";
		}
		FieldMeta<?> meta = metaProvider.getEntityField(fieldUID);
		try {
			if (!bFormat)
				return "" + val;
			return CollectableFieldFormat.getInstance(
					LangUtils.getClassLoaderThatWorksForWebStart().loadClass(meta.getDataType()))
					.format(meta.getFormatOutput(), val);
		} catch (ClassNotFoundException e) {
			return val != null ? val.toString() : "";
		}
	}

	protected abstract Object getValue(UID field);

}
