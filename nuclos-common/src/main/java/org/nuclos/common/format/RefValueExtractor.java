//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.format;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.i18n.language.data.DataLanguageUtils;

public class RefValueExtractor {
	
	private static final Logger LOG = Logger.getLogger(RefValueExtractor.class);
	
	public static Collection<UID> getAttributes(final String refAttributeStringifiedDefinition) {
		final Collection<UID> result = new ArrayList<UID>();
		final Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(refAttributeStringifiedDefinition, null, null).iterator();
		while (it.hasNext()) {
			IFieldUIDRef refPart = it.next();
			if (refPart.isUID()) {
				result.add(refPart.getUID());
			}
		}
		return result;
	}
	
	public static Collection<UID> getAttributes(final UID refAttribute) {
		return getAttributes(refAttribute, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}
	
	public static Collection<UID> getAttributes(final UID refAttribute, final IMetaProvider metaProvider) {
		final Collection<UID> result = new ArrayList<UID>();
		final Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(metaProvider.getEntityField(refAttribute), metaProvider).iterator();
		while (it.hasNext()) {
			IFieldUIDRef refPart = it.next();
			if (refPart.isUID()) {
				result.add(refPart.getUID());
			}
		}
		return result;
	}
	
	public static String get(EntityObjectVO<?> referencedObject, UID refAttribute, final UID dataLanguage) {
		return get(referencedObject, refAttribute, dataLanguage, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}
	
	public static String get(EntityObjectVO<?> referencedObject, UID refAttribute, final UID dataLanguage, IMetaProvider metaProvider) {
		return get(referencedObject, refAttribute, dataLanguage, true, metaProvider);
	}
	
	public static String get(EntityObjectVO<?> referencedObject, UID refAttribute, final UID dataLanguage, boolean bFormat) {
		return get(referencedObject, refAttribute, dataLanguage, bFormat, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}

	public static String get(final EntityObjectVO<?> referencedObject, final UID refAttribute, final UID dataLanguage, final boolean bFormat, final IMetaProvider metaProvider) {
		
		final StringBuffer result = new StringBuffer();
		final FormattingUIDTransformer formatter = new FormattingUIDTransformer(bFormat, metaProvider) {
			@Override
			protected Object getValue(UID field) {
				if (dataLanguage != null && metaProvider.getEntityField(field).isLocalized() && metaProvider.getEntityField(field).getCalcFunction() == null) {
					UID fieldLocalized = DataLanguageUtils.extractFieldUID(field);
					if (referencedObject.getDataLanguageMap() != null && 
							referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage) != null && 
							referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage).getFieldValue(fieldLocalized) != null) {
						return referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage).getFieldValue(fieldLocalized);
					}
				}
				return referencedObject.getFieldValue(field);
			}
		};
		
		
		final Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(metaProvider.getEntityField(refAttribute), metaProvider).iterator();
		while (it.hasNext()) {
			try {
				IFieldUIDRef refPart = it.next();
				if (refPart.isConstant()) {
					result.append(refPart.getConstant());
				}
				if (refPart.isUID()) {
					result.append(formatter.transform(refPart.getUID()));
				}
			} catch (Exception ex) {
				LOG.warn(String.format("Stringified value for attribute %s and Object (%s) not complete", refAttribute, referencedObject), ex);
			}
		}
		return result.toString();
	}
	
	public static String get(EntityObjectVO<?> referencedObject, String refAttributeStringifiedDefinition,UID dataLanguage) {
		return get(referencedObject, refAttributeStringifiedDefinition, dataLanguage, true);
	}

	public static String get(EntityObjectVO<?> referencedObject, String refAttributeStringifiedDefinition,UID dataLanguage, IMetaProvider metaProvider) {
		return get(referencedObject, refAttributeStringifiedDefinition, dataLanguage, true, metaProvider);
	}

	public static String get(EntityObjectVO<?> referencedObject, String refAttributeStringifiedDefinition, UID dataLanguage,boolean bFormat) {
		return get(referencedObject, refAttributeStringifiedDefinition, dataLanguage, bFormat, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}
	
	public static String get(final EntityObjectVO<?> referencedObject, final String refAttributeStringifiedDefinition, final UID dataLanguage, final boolean bFormat, final IMetaProvider metaProvider) {
		final StringBuffer result = new StringBuffer();
		final FormattingUIDTransformer formatter = new FormattingUIDTransformer(bFormat, metaProvider) {
			@Override
			protected Object getValue(UID field) {
				final FieldMeta<?> fieldMeta = metaProvider.getEntityField(field);
				if (dataLanguage != null && fieldMeta.getCalcFunction() == null) {
					boolean isLocalized = fieldMeta.isLocalized();
					if (!isLocalized && fieldMeta.getForeignEntity() != null) {
						final UID foreignEntityLangField = DataLanguageUtils.extractForeignEntityReference(fieldMeta.getForeignEntity());
						final Object fieldValue = referencedObject.getFieldValue(foreignEntityLangField);
						if (fieldValue != null) {
							return fieldValue;
						}
					}
					if (isLocalized) {
						UID fieldLocalized = DataLanguageUtils.extractFieldUID(field);
						if (referencedObject.getDataLanguageMap() != null &&
								referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage) != null &&
								referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage).getFieldValue(fieldLocalized) != null) {
							return referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage).getFieldValue(fieldLocalized);
						}
					}
				}
				return referencedObject.getFieldValue(field);
			}
		};
		UID nameFieldIfExist = null;
		if (refAttributeStringifiedDefinition == null && referencedObject.getDalEntity() != null) {
			for (FieldMeta<?> fMeta : metaProvider.getAllEntityFieldsByEntity(referencedObject.getDalEntity()).values()) {
				if ("name".equals(fMeta.getFieldName())) {
					nameFieldIfExist = fMeta.getUID();
					break;
				}
			}
		}
		final Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(refAttributeStringifiedDefinition, null, nameFieldIfExist).iterator();
		while (it.hasNext()) {
			try {
				IFieldUIDRef refPart = it.next();
				if (refPart.isConstant()) {
					result.append(refPart.getConstant());
				}
				if (refPart.isUID()) {
					result.append(formatter.transform(refPart.getUID()));
				}
			} catch (Exception ex) {
				LOG.warn(String.format("Stringified value for attribute %s.%s and Object (%s) not complete", 
						referencedObject.getDalEntity(), refAttributeStringifiedDefinition, referencedObject), ex);
			}
		}
		return result.toString();
	}
	
	public static String get(Collectable<?> referencedObject, UID refAttribute) {
		return get(referencedObject, refAttribute, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}
	
	public static String get(Collectable<?> referencedObject, UID refAttribute, IMetaProvider metaProvider) {
		return get(referencedObject, refAttribute, true, metaProvider);
	}
	
	public static String get(Collectable<?> referencedObject, UID refAttribute, boolean bFormat) {
		return get(referencedObject, refAttribute, bFormat, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}
	
	public static String get(final Collectable<?> referencedObject, final UID refAttribute, final boolean bFormat, final IMetaProvider metaProvider) {
		final StringBuffer result = new StringBuffer();
		final FormattingUIDTransformer formatter = new FormattingUIDTransformer(bFormat, metaProvider) {
			@Override
			protected Object getValue(UID field) {
				return referencedObject.getValue(field);
			}
		};
		final Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(metaProvider.getEntityField(refAttribute), metaProvider).iterator();
		while (it.hasNext()) {
			try {
				IFieldUIDRef refPart = it.next();
				if (refPart.isConstant()) {
					result.append(refPart.getConstant());
				}
				if (refPart.isUID()) {
					result.append(formatter.transform(refPart.getUID()));
				}
			} catch (Exception ex) {
				LOG.warn(String.format("Stringified value for attribute %s and Object (%s) not complete", refAttribute, referencedObject), ex);
			}
		}
		return result.toString();
	}
	
	public static String get(Collectable<?> referencedObject, String refAttributeStringifiedDefinition) {
		return get(referencedObject, refAttributeStringifiedDefinition, true);
	}

	public static String get(Collectable<?> referencedObject, String refAttributeStringifiedDefinition, IMetaProvider metaProvider) {
		return get(referencedObject, refAttributeStringifiedDefinition, true, metaProvider);
	}

	public static String get(Collectable<?> referencedObject, String refAttributeStringifiedDefinition, boolean bFormat) {
		return get(referencedObject, refAttributeStringifiedDefinition, bFormat, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}
	
	public static String get(final Collectable<?> referencedObject, final String refAttributeStringifiedDefinition, final boolean bFormat, final IMetaProvider metaProvider) {
		final StringBuffer result = new StringBuffer();
		final FormattingUIDTransformer formatter = new FormattingUIDTransformer(bFormat, metaProvider) {
			@Override
			protected Object getValue(UID field) {
				return referencedObject.getValue(field);
			}
		};
		final Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(refAttributeStringifiedDefinition, null, null).iterator();
		while (it.hasNext()) {
			try {
				IFieldUIDRef refPart = it.next();
				if (refPart.isConstant()) {
					result.append(refPart.getConstant());
				}
				if (refPart.isUID()) {
					result.append(formatter.transform(refPart.getUID()));
				}
			} catch (Exception ex) {
				LOG.warn(String.format("Stringified value for attribute %s.%s and Object (%s) not complete", 
						referencedObject.getEntityUID(), refAttributeStringifiedDefinition, referencedObject), ex);
			}
		}
		return result.toString();
	}
	
	public static String get(final GenericObjectVO referencedObject, final String refAttributeStringifiedDefinition, final UID dataLanguage, final boolean bFormat) {
		return get(referencedObject, refAttributeStringifiedDefinition, dataLanguage, bFormat, SpringApplicationContextHolder.getBean(IMetaProvider.class));
	}
	
	public static String get(final GenericObjectVO referencedObject, final String refAttributeStringifiedDefinition, final UID dataLanguage, final boolean bFormat, final IMetaProvider metaProvider) {
		final StringBuffer result = new StringBuffer();
		final FormattingUIDTransformer formatter = new FormattingUIDTransformer(bFormat, metaProvider) {
			@Override
			protected Object getValue(UID field) {
				DynamicAttributeVO attribute = referencedObject.getAttribute(field);
				if (attribute != null) {
					
					if (dataLanguage != null && metaProvider.getEntityField(field).isLocalized() && metaProvider.getEntityField(field).getCalcFunction() == null) {
						UID fieldLocalized = DataLanguageUtils.extractFieldUID(field);
						if (referencedObject.getDataLanguageMap() != null && 
								referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage) != null && 
								referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage).getFieldValue(fieldLocalized) != null) {
							return referencedObject.getDataLanguageMap().getDataLanguage(dataLanguage).getFieldValue(fieldLocalized);
						}
					}
					
					attribute.getValue();
				}
				return null;
			}
		};
		final Iterator<IFieldUIDRef> it = new ForeignEntityFieldUIDParser(refAttributeStringifiedDefinition, null, null).iterator();
		while (it.hasNext()) {
			try {
				IFieldUIDRef refPart = it.next();
				if (refPart.isConstant()) {
					result.append(refPart.getConstant());
				}
				if (refPart.isUID()) {
					result.append(formatter.transform(refPart.getUID()));
				}
			} catch (Exception ex) {
				LOG.warn(String.format("Stringified value for attribute %s.%s and Object (%s) not complete", 
						referencedObject.getModule(), refAttributeStringifiedDefinition, referencedObject), ex);
			}
		}
		return result.toString();
	}
	
}
