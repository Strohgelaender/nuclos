package org.nuclos.common2.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.nuclos.common2.SpringLocaleDelegate;

public final class NuclosExceptions {
	private NuclosExceptions() {}
	
	public static Throwable getCause(Throwable t) {
		if (t.getCause() == null) {
			return t;
		}
		else {
			return getCause(t.getCause());
		}
	}
	
	public static <T> T getCause(Throwable t, Class<T> clazz) {
		if (t == null) {
			return null;
		}
		else if (clazz.isAssignableFrom(t.getClass())) {
			return (T) t;
		}
		else {
			return getCause(t.getCause(), clazz);
		}
	}

	public static String getReasonableMessage(Throwable t) {
		String result = null;

		if (t != null) {
			String resMessage = SpringLocaleDelegate.getInstance().getMessageFromResource(getRealDetailMessage(t));
			if (isReasonableException(t)) {
				result = resMessage != null ? resMessage : t.getLocalizedMessage();
			}
			else if (t.getCause() != null && t.getCause() instanceof Exception) {
				result = resMessage != null ? resMessage : getReasonableMessage(t.getCause());
			}
		}
		return result;
	}

	private static String getRealDetailMessage(Throwable t) {
		Throwable cause = t;
		while (cause.getCause() != null && !isReasonableException(cause))
			cause = cause.getCause();
		return cause.getMessage();
	}
	
	private static boolean isReasonableException(Throwable t) {
		return (t instanceof CommonBusinessException) || ((t instanceof CommonFatalException) && (!(t instanceof CommonRemoteException)));
	}
	
	public static void shortenStackTrace(Throwable t, int maxLength) {
		StackTraceElement[] those = t.getStackTrace();
		List<StackTraceElement> these = new ArrayList<StackTraceElement>();
		for (StackTraceElement stElem : those) {
			these.add(stElem);
			if (these.size() >= maxLength) {
				break;
			}
		}
		t.setStackTrace(these.toArray(new StackTraceElement[these.size()]));
	}

	public static Throwable findRootCause(Throwable t, Predicate<Throwable> predicate) {
		if (t.getCause() != null) {
			Throwable rootCause = findRootCause(t.getCause(), predicate);
			if (rootCause != null) {
				return rootCause;
			}
		}

		if (predicate.test(t)) {
			return t;
		}

		return null;
	}
}
