//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.exception;

import org.nuclos.api.printout.Printout;
import org.nuclos.api.report.OutputFormat;

/**
 * Wrapper exception for printout exception.
 *
 */
public class PrintoutException extends CommonPrintException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4801479564624936769L;
	private final Printout printout;
	private final OutputFormat outputFormat;

	/**
	 * @param printout the printout in which the error occurred
	 * @param outputFormat the outputFormat in which the error occurred
	 */
	public PrintoutException(Printout printout, OutputFormat outputFormat) {
		super(CommonBusinessException.CREATE);
		this.printout = printout;
		this.outputFormat = outputFormat;
	}

	/**
	 * @param printout the printout in which the error occurred
	 * @param outputFormat the outputFormat in which the error occurred
	 * @param tCause wrapped exception
	 */
	public PrintoutException(Printout printout, OutputFormat outputFormat, Throwable tCause) {
		super(tCause);
		this.printout = printout;
		this.outputFormat = outputFormat;
	}

	/**
	 * @param printout the printout in which the error occurred
	 * @param outputFormat the outputFormat in which the error occurred
	 * @param sMessage exception message
	 */
	public PrintoutException(Printout printout, OutputFormat outputFormat, String sMessage) {
		super(sMessage);
		this.printout = printout;
		this.outputFormat = outputFormat;
	}

	/**
	 * @param printout the printout in which the error occurred
	 * @param outputFormat the outputFormat in which the error occurred
	 * @param sMessage exception message
	 * @param tCause wrapped exception
	 */
	public PrintoutException(Printout printout, OutputFormat outputFormat, String sMessage, Throwable tCause) {
		super(sMessage, tCause);
		this.printout = printout;
		this.outputFormat = outputFormat;
	}

	public Printout getPrintout() {
		return printout;
	}

	public OutputFormat getOutputFormat() {
		return outputFormat;
	}
	
}
