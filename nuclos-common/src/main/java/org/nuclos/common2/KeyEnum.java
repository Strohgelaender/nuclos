//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2;

/**
 * @author Thomas Pasch (javadoc, deprecation)
 *
 * @param <T> type of underlying enum
 * 
 * @deprecated Can't see any improvement over an 'normal' enum here.
 * 		Especially, using the {@link #getValue()} in DB persistence 
 * 		instead of the name suffers from problems of non-enum types
 * 		(e.g. you can't use '==' but must use {@link #equals(Object)};
 * 		plus there is no built-in measure again the same value in
 * 		two different KeyEnum instances).
 * 		<p>
 * 		ATTENTION: Implementations of this interface trashes the
 * 		enum contract of {@link Enum#valueOf(Class, String)}, '==',
 * 		<code>switch</code> statement <em>on purpose</em>.
 */
public interface KeyEnum<T> {

	public T getValue();
	
	/**
	 * @author Thomas Pasch (javadoc, deprecation)
	 *
	 * @deprecated See deprecation of KeyEnum above.
	 */
	public static class Utils {

		/**
		 * @deprecated This implementation uses {@link #equals(Object)} for comparison and
		 * 		thus breaks normal contract of {@link Enum}s.
		 */
		public static <V, T extends Enum<T> & KeyEnum<V>> T findEnum(Class<T> clazz, V value) {
			for (T e : clazz.getEnumConstants()) {
				if (e.getValue().equals(value)) {
					return e; 
				}
			}
			return null;
		}
		
		public static <V> V unwrap(KeyEnum<V> e) {
			return (e != null) ? e.getValue() : null;  
		}
	}
}
