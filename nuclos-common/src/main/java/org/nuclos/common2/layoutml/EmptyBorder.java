package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class EmptyBorder extends CompBorder {
	private final int top;
	private final int left;
	private final int bottom;
	private final int right;
	
	public EmptyBorder(Attributes attributes) {
		this.top = MyLayoutConstants.getIntValue(attributes, ATTRIBUTE_TOP, 0);
		this.left = MyLayoutConstants.getIntValue(attributes, ATTRIBUTE_LEFT, 0);
		this.bottom = MyLayoutConstants.getIntValue(attributes, ATTRIBUTE_BOTTOM, 0);
		this.right = MyLayoutConstants.getIntValue(attributes, ATTRIBUTE_RIGHT, 0);
	}
	
	@Override
	public String getType() {
		return null;
	}
	
	@Override
	public String toString() {
		return "Empty: top=" + top + ", left=" + left + ", bottom =" + bottom + ", right=" + right;
	}

}
