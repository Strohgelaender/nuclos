//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.layoutml;

import java.awt.Dimension;

/**
 * Constants for the LayoutML.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @version 01.00.00
 * @author    <a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 */

public interface LayoutMLConstants {
	/**
	 * the system identifier (URI) for the LayoutML DTD.
	 */
	String LAYOUTML_DTD_SYSTEMIDENTIFIER = "http://www.novabit.de/technologies/layoutml/layoutml.dtd";

	/**
	 * the resource path of the bundled LayoutML DTD
	 */
	String LAYOUTML_DTD_RESSOURCEPATH = "org/nuclos/schema/layout/layoutml/layoutml.dtd";

	// constants for XML elements used in the LayoutML:
	String ELEMENT_PANEL = "panel";
	String ELEMENT_EMPTYPANEL = "empty-panel";
	String ELEMENT_LABEL = "label";
	String ELEMENT_TEXTFIELD = "textfield";
	//NUCLEUSINT-1142
	String ELEMENT_PASSWORD = "password";
	String ELEMENT_TEXTAREA = "textarea";
	String ELEMENT_COMBOBOX = "combobox";
	String ELEMENT_CHECKBOX = "checkbox";
	String ELEMENT_BUTTON = "button";
	String ELEMENT_COLLECTABLECOMPONENT = "collectable-component";
	String ELEMENT_TABBEDPANE = "tabbedpane";
	String ELEMENT_SCROLLPANE = "scrollpane";
	String ELEMENT_SPLITPANE = "splitpane";
	String ELEMENT_SUBFORM = "subform";
	String ELEMENT_MATRIX = "matrix";
	String ELEMENT_MATRIXCOLUMN = "matrix-column";
	String ELEMENT_SUBFORMCOLUMN = "subform-column";
	String ELEMENT_CHART = "chart";
	String ELEMENT_DESCRIPTION = "description";
	String ELEMENT_BORDERLAYOUT = "borderlayout";
	String ELEMENT_FLOWLAYOUT = "flowlayout";
	String ELEMENT_GRIDLAYOUT = "gridlayout";
	String ELEMENT_GRIDBAGLAYOUT = "gridbaglayout";
	String ELEMENT_TABLELAYOUT = "tablelayout";
	String ELEMENT_BOXLAYOUT = "boxlayout";
	String ELEMENT_ROWLAYOUT = "rowlayout";
	String ELEMENT_COLUMNLAYOUT = "columnlayout";
	String ELEMENT_BORDERLAYOUTCONSTRAINTS = "borderlayout-constraints";
	String ELEMENT_GRIDBAGCONSTRAINTS = "gridbag-constraints";
	String ELEMENT_TABLELAYOUTCONSTRAINTS = "tablelayout-constraints";
	String ELEMENT_TABBEDPANECONSTRAINTS = "tabbedpane-constraints";
	String ELEMENT_SPLITPANECONSTRAINTS = "splitpane-constraints";
	String ELEMENT_BACKGROUND = "background";
	String ELEMENT_MINIMUMSIZE = "minimum-size";
	String ELEMENT_PREFERREDSIZE = "preferred-size";
	String ELEMENT_STRICTSIZE = "strict-size";
	String ELEMENT_CLEARBORDER = "clear-border";
	String ELEMENT_EMPTYBORDER = "empty-border";
	String ELEMENT_ETCHEDBORDER = "etched-border";
	String ELEMENT_TITLEDBORDER = "titled-border";
	String ELEMENT_BEVELBORDER = "bevel-border";
	String ELEMENT_LINEBORDER = "line-border";
	String ELEMENT_SEPARATOR = "separator";
	String ELEMENT_TITLEDSEPARATOR = "titled-separator";
	String ELEMENT_IMAGE = "image";
	String ELEMENT_OPTIONS = "options";
	String ELEMENT_OPTION = "option";
	String ELEMENT_OPTIONGROUP = "optiongroup";
	String ELEMENT_RULES = "rules";
	String ELEMENT_RULE = "rule";
	String ELEMENT_EVENT = "event";
	String ELEMENT_CONDITION = "condition";
	String ELEMENT_ACTIONS = "actions";
	String ELEMENT_TRANSFERLOOKEDUPVALUE = "transfer-lookedup-value";
	String ELEMENT_CLEAR = "clear";
	String ELEMENT_ENABLE = "enable";
	String ELEMENT_REFRESHVALUELIST = "refresh-valuelist";
	String ELEMENT_REINIT_SUBFORM = "reinit-subform";
	String ELEMENT_FONT = "font";
	String ELEMENT_COLLECTABLEFIELD = "collectable-field";
	String ELEMENT_DEPENDENCY = "dependency";
	String ELEMENT_DEPENDENCIES = "dependencies";
	String ELEMENT_VALUELISTPROVIDER = "valuelist-provider";
	String ELEMENT_PARAMETER = "parameter";
	String ELEMENT_TEXTFORMAT = "text-format";
	String ELEMENT_TEXTCOLOR = "text-color";

	String ELEMENT_PROPERTY = "property";
	String ELEMENT_PROPERTY_SIZE = "property-size";
	String ELEMENT_PROPERTY_COLOR = "property-color";
	String ELEMENT_PROPERTY_FONT = "property-font";
	String ELEMENT_PROPERTY_SCRIPT = "property-script";
	String ELEMENT_PROPERTY_TRANSLATIONS = "property-translations";
	String ELEMENT_PROPERTY_VALUELIST_PROVIDER = "property-valuelist-provider";

	String ELEMENT_INITIALFOCUSCOMPONENT = "initial-focus-component";
	String ELEMENT_INITIALSORTINGORDER = "initial-sorting-order";
	String ELEMENT_TRANSLATIONS = "translations";
	String ELEMENT_TRANSLATION = "translation";
	String ELEMENT_LAYOUTCOMPONENT = "layoutcomponent";
	String ELEMENT_WEBADDON = "webaddon";

	String ELEMENT_ENABLED = "enabled";
	String ELEMENT_NEW_ENABLED = "new-enabled";
	String ELEMENT_EDIT_ENABLED = "edit-enabled";
	String ELEMENT_DELETE_ENABLED = "delete-enabled";
	String ELEMENT_CLONE_ENABLED = "clone-enabled";

	String ELEMENT_DYNAMIC_ROW_COLOR = "dynamic-row-color";

	String ELEMENT_TEXTMODULE = "text-module";

	// constants for XML attributes used in the LayoutML:
	String ATTRIBUTE_NAME = "name";
	String ATTRIBUTE_PROP_NAME = "prop-name";
	String ATTRIBUTE_TEXT = "text";
	String ATTRIBUTE_ROWS = "rows";
	String ATTRIBUTE_COLUMNS = "columns";

	String ATTRIBUTE_TITLE = "title";
	String ATTRIBUTE_POSITION = "position";
	String ATTRIBUTE_RED = "red";
	String ATTRIBUTE_GREEN = "green";
	String ATTRIBUTE_BLUE = "blue";
	String ATTRIBUTE_ALIGN = "align";
	String ATTRIBUTE_WIDTH = "width";
	String ATTRIBUTE_HEIGHT = "height";
	String ATTRIBUTE_TOP = "top";
	String ATTRIBUTE_LEFT = "left";
	String ATTRIBUTE_BOTTOM = "bottom";
	String ATTRIBUTE_RIGHT = "right";
	String ATTRIBUTE_TYPE = "type";
	String ATTRIBUTE_HGAP = "hgap";
	String ATTRIBUTE_VGAP = "vgap";
	String ATTRIBUTE_GAP = "gap";
	String ATTRIBUTE_ENABLED = "enabled";
	String ATTRIBUTE_BOLD = "bold";
	String ATTRIBUTE_ITALIC = "italic";
	String ATTRIBUTE_UNDERLINE = "underline";
	String ATTRIBUTE_NOT_CLONEABLE = "notcloneable";
	String ATTRIBUTE_MULTISELECT_SEARCHFILTER = "multiselectsearchfilter";
	String ATTRIBUTE_MAX_ENTRIES = "max-entries";
	String ATTRIBUTE_MULTIEDITABLE = "multieditable";
	String ATTRIBUTE_INVERTABLE = "invertable";
	String ATTRIBUTE_EDITABLE = "editable";
	String ATTRIBUTE_INSERTABLE = "insertable";
	String ATTRIBUTE_MULTISELECT = "multiselect";
	String ATTRIBUTE_LOV_BUTTON = "lovbtn";
	String ATTRIBUTE_LOV_SEARCHMASK = "lovsearch";
	String ATTRIBUTE_DROPDOWN_BUTTON = "dropdownbtn";
	String ATTRIBUTE_CUSTOM_USAGE_SEARCH = "customusagesearch";
	String ATTRIBUTE_SCALABLE = "scalable";
	String ATTRIBUTE_ASPECTRATIO = "aspectratio";
	String ATTRIBUTE_TABLAYOUTPOLICY = "tablayoutpolicy";
	String ATTRIBUTE_TABPLACEMENT = "tabplacement";
	String ATTRIBUTE_GRIDX = "gridx";
	String ATTRIBUTE_GRIDY = "gridy";
	String ATTRIBUTE_GRIDWIDTH = "gridwidth";
	String ATTRIBUTE_GRIDHEIGHT = "gridheight";
	String ATTRIBUTE_WEIGHTX = "weightx";
	String ATTRIBUTE_WEIGHTY = "weighty";
	String ATTRIBUTE_ANCHOR = "anchor";
	String ATTRIBUTE_FILL = "fill";
	String ATTRIBUTE_FILLHORIZONTALLY = "fill-horizontally";
	String ATTRIBUTE_FILLVERTICALLY = "fill-vertically";
	String ATTRIBUTE_PADX = "padx";
	String ATTRIBUTE_PADY = "pady";
	String ATTRIBUTE_INSETTOP = "insettop";
	String ATTRIBUTE_INSETLEFT = "insetleft";
	String ATTRIBUTE_INSETBOTTOM = "insetbottom";
	String ATTRIBUTE_INSETRIGHT = "insetright";
	String ATTRIBUTE_LABEL = "label";
	String ATTRIBUTE_TOOLTIP = "tooltip";
	String ATTRIBUTE_HORIZONTALSCROLLBARPOLICY = "horizontalscrollbar";
	String ATTRIBUTE_VERTICALSCROLLBARPOLICY = "verticalscrollbar";
	String ATTRIBUTE_CONTROLTYPE = "controltype";
	String ATTRIBUTE_CONTROLTYPECLASS = "controltypeclass";
	String ATTRIBUTE_MNEMONIC = "mnemonic";
	String ATTRIBUTE_ORIENTATION = "orientation";
	String ATTRIBUTE_DIVIDERSIZE = "dividersize";
	String ATTRIBUTE_EXPANDABLE = "expandable";
	String ATTRIBUTE_CONTINUOUSLAYOUT = "continuous-layout";
	String ATTRIBUTE_RESIZEWEIGHT = "resizeweight";
	String ATTRIBUTE_AXIS = "axis";
	String ATTRIBUTE_ID = "id";
	String ATTRIBUTE_OPAQUE = "opaque";
	String ATTRIBUTE_CONSTRAINTS = "constraints";
	String ATTRIBUTE_ENTITY = "entity";
	String ATTRIBUTE_ENTITY_X = "entity_x";
	String ATTRIBUTE_ENTITY_Y = "entity_y";
	String ATTRIBUTE_ENTITY_Y_PARENT_FIELD = "entity_y_parent_field";
	String ATTRIBUTE_ENTITY_MATRIX = "entity_matrix";
	String ATTRIBUTE_ENTITY_FIELD_MATRIX_PREFERENCES = "matrix_preferences_field";
	String ATTRIBUTE_MATRIX_PARENT_FIELD = "entity_field_matrix_parent";
	String ATTRIBUTE_MATRIX_VALUE_TYPE = "entity_matrix_value_type";
	String ATTRIBUTE_MATRIX_VALUE_FIELD = "entity_matrix_value_field";
	String ATTRIBUTE_MATRIX_NUMBER_STATE = "entity_matrix_number_state";
	String ATTRIBUTE_MATRIX_X_REF_FIELD = "entity_field_matrix_x_ref_field";
	String ATTRIBUTE_ENTITY_FIELD_X = "entity_field_x";
	String ATTRIBUTE_ENTITY_FIELD_CATEGORIE = "entity_field_categorie";
	String ATTRIBUTE_ENTITY_FIELD_Y = "entity_field_y";
	String ATTRIBUTE_ENTITY_X_SORTING_FIELDS = "entity_x_sorting_fields";
	String ATTRIBUTE_ENTITY_Y_SORTING_FIELDS = "entity_y_sorting_fields";
	String ATTRIBUTE_ENTITY_X_HEADER = "entity_x_header";
	String ATTRIBUTE_ENTITY_Y_HEADER = "entity_y_header";
	String ATTRIBUTE_ENTITY_MATRIX_REFERENCE_FIELD = "entity_matrix_reference_field";
	String ATTRIBUTE_ENTITY_X_VLP_ID = "entity_x_vlp_id";
	String ATTRIBUTE_ENTITY_X_VLP_IDFIELDNAME = "entity_x_vlp_idfieldname";
	String ATTRIBUTE_ENTITY_X_VLP_FIELDNAME = "entity_x_vlp_fieldname";
	String ATTRIBUTE_ENTITY_X_VLP_REFERENCE_PARAM_NAME = "entity_x_vlp_reference_param_name";
	String ATTRIBUTE_CELL_INPUT_TYPE = "cell_input_type";
	String ATTRIBUTE_CELL_INPUT_TYPE_TEXTFIELD = "textfield";
	String ATTRIBUTE_CELL_INPUT_TYPE_COMBOBOX = "combobox";
	String ATTRIBUTE_CELL_INPUT_TYPE_CHECKICON = "checkicon";
	// RSWORGA-207 (Spaltensortierung mit Autonummer)
	String ATTRIBUTE_AUTONUMBER_SORTING = "autonumbersorting";

	String ATTRIBUTE_FIELD = "field";
	String ATTRIBUTE_ACTIONCOMMAND = "actioncommand";
	String ATTRIBUTE_ACTIONKEYSTROKE = "actionkeystroke";
	String ATTRIBUTE_VALUE = "value";
	String ATTRIBUTE_DEFAULT = "default";
	String ATTRIBUTE_SHOWONLY = "show-only";
	String ATTRIBUTE_TOOLBARORIENTATION = "toolbarorientation";
	String ATTRIBUTE_STATUSBAR = "statusbar";
	String ATTRIBUTE_OPEN_DETAILS_WITH_TAB_RECYCLING = "open-details-with-tab-recycling";
	String ATTRIBUTE_SCROLLPANE = "scrollpane";
	String ATTRIBUTE_THICKNESS = "thickness";
	String ATTRIBUTE_SOURCEFIELD = "sourcefield";
	String ATTRIBUTE_SOURCECOMPONENT = "sourcecomponent";
	String ATTRIBUTE_TARGETCOMPONENT = "targetcomponent";
	String ATTRIBUTE_PARENTCOMPONENT = "parentcomponent";
	String ATTRIBUTE_VISIBLE = "visible";
	String ATTRIBUTE_FILLCONTROLHORIZONTALLY = "fill-control-horizontally";
	String ATTRIBUTE_SIZE = "size";
	String ATTRIBUTE_DEPENDANTFIELD = "dependant-field";
	String ATTRIBUTE_DEPENDSONFIELD = "depends-on";
	String ATTRIBUTE_PARAMETER_FOR_SOURCECOMPONENT = "parameter-for-sourcecomponent";
	String ATTRIBUTE_UNIQUEMASTERCOLUMN = "unique-mastercolumn";
	String ATTRIBUTE_CONTROLLERTYPE = "controllertype";
	String ATTRIBUTE_FOREIGNKEYFIELDTOPARENT = "foreignkeyfield-to-parent";
	String ATTRIBUTE_SORTINGORDER = "sorting-order";
	String ATTRIBUTE_PARENTSUBFORM = "parent-subform";
	String ATTRIBUTE_COL1 = "col1";
	String ATTRIBUTE_COL2 = "col2";
	String ATTRIBUTE_ROW1 = "row1";
	String ATTRIBUTE_ROW2 = "row2";
	String ATTRIBUTE_HALIGN = "hAlign";
	String ATTRIBUTE_VALIGN = "vAlign";
	@Deprecated /** @deprecated resourceId is replaced with nested translation elements; use this attribute only for migration. */
			String ATTRIBUTE_LOCALERESOURCEID = "resourceId";
	String ATTRIBUTE_LANG = "lang";
	String ATTRIBUTE_INTERNALNAME = "internalname";
	String ATTRIBUTE_COLUMNWIDTH = "width";
	String ATTRIBUTE_NEXTFOCUSFIELD = "nextfocusfield";
	String ATTRIBUTE_NEXTFOCUSCOMPONENT = "nextfocuscomponent";
	String ATTRIBUTE_NEXTFOCUSONACTION = "nextfocusonaction";
	String ATTRIBUTE_ICON = "icon";
	String ATTRIBUTE_LANGUAGE = "language";
	String ATTRIBUTE_CLASS = "class";
	String ATTRIBUTE_DYNAMIC_CELL_HEIGHTS_DEFAULT = "dynamic-cell-heights-default";
	String ATTRIBUTE_IGNORE_SUB_LAYOUT = "ignore-sub-layout";
	String ATTRIBUTE_DISABLE_DURING_EDIT = "disable-during-edit";
	String ATTRIBUTE_TEXTMODULE_ENTITY = "entity";
	String ATTRIBUTE_TEXTMODULE_ENTITY_FIELD = "entityfield";
	String ATTRIBUTE_TEXTMODULE_ENTITY_FIELD_NAME = "entityfield-name";
	String ATTRIBUTE_TEXTMODULE_ENTITY_FIELD_SORT = "entityfield-sort";


	// constants for XML attribute values used in the LayoutML:
	String ATTRIBUTEVALUE_LOWERED = "lowered";
	String ATTRIBUTEVALUE_RAISED = "raised";
	String ATTRIBUTEVALUE_YES = "yes";
	String ATTRIBUTEVALUE_NO = "no";
	String ATTRIBUTEVALUE_TEXTFIELD = ELEMENT_TEXTFIELD;
	String ATTRIBUTEVALUE_IDTEXTFIELD = "id-textfield";
	String ATTRIBUTEVALUE_COMBOBOX = ELEMENT_COMBOBOX;
	String ATTRIBUTEVALUE_CHECKBOX = ELEMENT_CHECKBOX;
	String ATTRIBUTEVALUE_TEXTAREA = ELEMENT_TEXTAREA;
	String ATTRIBUTEVALUE_LISTOFVALUES = "listofvalues";
	String ATTRIBUTEVALUE_DATECHOOSER = "datechooser";
	String ATTRIBUTEVALUE_HYPERLINK = "hyperlink";
	String ATTRIBUTEVALUE_EMAIL = "email";
	String ATTRIBUTEVALUE_PHONENUMBER = "phonenumber";
	String ATTRIBUTEVALUE_OPTIONGROUP = ELEMENT_OPTIONGROUP;
	String ATTRIBUTEVALUE_FILECHOOSER = "filechooser";
	String ATTRIBUTEVALUE_IMAGE = "image";
	//NUCLEUSINT-1142
	String ATTRIBUTEVALUE_PASSWORDFIELD = ELEMENT_PASSWORD;
	String ATTRIBUTEVALUE_X = "x";
	String ATTRIBUTEVALUE_Y = "y";
	String ATTRIBUTEVALUE_LABEL = "label";
	String ATTRIBUTEVALUE_CONTROL = "control";
	String ATTRIBUTEVALUE_BROWSEBUTTON = "browsebutton";
	String ATTRIBUTEVALUE_LOOKUP = "lookup";
	String ATTRIBUTEVALUE_VALUECHANGED = "value-changed";
	String ATTRIBUTEVALUE_DEFAULT = "default";
	String ATTRIBUTEVALUE_DEPENDANT = "dependant";
	String ATTRIBUTEVALUE_HORIZONTAL = "horizontal";
	String ATTRIBUTEVALUE_VERTICAL = "vertical";
	String ATTRIBUTEVALUE_LEFT = "left";
	String ATTRIBUTEVALUE_RIGHT = "right";
	String ATTRIBUTEVALUE_TOP = "top";
	String ATTRIBUTEVALUE_BOTTOM = "bottom";
	String ATTRIBUTEVALUE_SCROLL = "scroll";
	String ATTRIBUTEVALUE_WRAP = "wrap";
	String ATTRIBUTEVALUE_ASCENDING = "ascending";
	String ATTRIBUTEVALUE_DESCENDING = "descending";
	String ATTRIBUTEVALUE_ASNEEDED = "asneeded";
	String ATTRIBUTEVALUE_ALWAYS = "always";
	String aTTRIBUTEVALUE_NEVER = "never";
	String ATTRIBUTEVALUE_HIDE = "hide";
	String ATTRIBUTEVALUE_NONE = "none";
	String ATTRIBUTEVALUE_BOTH = "both";

	// controltypes
	String CONTROLTYPE_COMBOBOX = "combobox";
	String CONTROLTYPE_CHECKBOX = "checkbox";
	String CONTROLTYPE_TEXTFIELD = "textfield";
	//NUCLEUSINT-1142
	String CONTROLTYPE_PASSWORDFIELD = "password";
	String CONTROLTYPE_IMAGE = "image";
	String CONTROLTYPE_FILECHOOSER = "filechooser";
	String CONTROLTYPE_DATECHOOSER = "datechooser";
	String CONTROLTYPE_LISTOFVALUES = "listofvalues";
	String CONTROLTYPE_TEXTAREA = "textarea";
	String CONTROLTYPE_OPTIONGROUP = "optiongroup";
	String CONTROLTYPE_LABEL = "label";
	String CONTROLTYPE_EMAIL = "email";
	String CONTROLTYPE_PHONENUMBER = "phonenumber";
	String CONTROLTYPE_HYPERLINK = "hyperlink";

	// default element attribute values //NUCLEUSINT-485
	Dimension DEFAULTVALUE_BUTTON_MINIMUMSIZE = new Dimension(15, 22);
	Dimension DEFAULTVALUE_BUTTON_PREFERREDSIZE = new Dimension(50, 22);
	Dimension DEFAULTVALUE_LABEL_MINIMUMSIZE = new Dimension(15, 22);
	Dimension DEFAULTVALUE_LABEL_PREFERREDSIZE = new Dimension(50, 22);
	Dimension DEFAULTVALUE_CHECKBOX_MINIMUMSIZE = new Dimension(19, 22);
	Dimension DEFAULTVALUE_CHECKBOX_PREFERREDSIZE = new Dimension(20, 22);
	Dimension DEFAULTVALUE_TEXTFIELD_MINIMUMSIZE = new Dimension(35, 22);
	Dimension DEFAULTVALUE_TEXTFIELD_PREFERREDSIZE = new Dimension(70, 22);
	Dimension DEFAULTVALUE_COMBOBOX_MINIMUMSIZE = new Dimension(35, 20);
	Dimension DEFAULTVALUE_COMBOBOX_PREFERREDSIZE = new Dimension(70, 20);
	Dimension DEFAULTVALUE_COMBOBOX_STRICTSIZE = new Dimension(70, 20);
	Dimension DEFAULTVALUE_DATECHOOSER_MINIMUMSIZE = new Dimension(35, 22);
	Dimension DEFAULTVALUE_DATECHOOSER_PREFERREDSIZE = new Dimension(90, 22);
	Dimension DEFAULTVALUE_OPTIONGROUP_MINIMUMSIZE = new Dimension(35, 22);
	Dimension DEFAULTVALUE_OPTIONGROUP_PREFERREDSIZE = new Dimension(70, 22);
	Dimension DEFAULTVALUE_LISTOFVALUES_MINIMUMSIZE = new Dimension(35, 22);
	Dimension DEFAULTVALUE_LISTOFVALUES_PREFERREDSIZE = new Dimension(70, 22);
	Dimension DEFAULTVALUE_TEXTAREA_MINIMUMSIZE = new Dimension(35, 22);
	Dimension DEFAULTVALUE_TEXTAREA_PREFERREDSIZE = new Dimension(70, 22);
	Dimension DEFAULTVALUE_LAYOUTCOMPONENT_MINIMUMSIZE = new Dimension(20, 20);

	Dimension DEFAULTVALUE_TABBEDPANE_MINIMUMSIZE = new Dimension(180, 40);
	Dimension DEFAULTVALUE_TABBEDPANE_PREFERREDSIZE = new Dimension(300, 100);
	Dimension DEFAULTVALUE_SUBFORM_MINIMUMSIZE = new Dimension(180, 40);
	Dimension DEFAULTVALUE_MATRIX_MINIMUMSIZE = new Dimension(180, 40);
	Dimension DEFAULTVALUE_SUBFORM_PREFERREDSIZE = new Dimension(300, 100);
	Dimension DEFAULTVALUE_CHART_MINIMUMSIZE = new Dimension(180, 40);
	Dimension DEFAULTVALUE_CHART_PREFERREDSIZE = new Dimension(300, 100);
	Dimension DEFAULTVALUE_SCROLLPANE_MINIMUMSIZE = new Dimension(180, 40);
	Dimension DEFAULTVALUE_SCROLLPANE_PREFERREDSIZE = new Dimension(300, 100);
	Dimension DEFAULTVALUE_SPLITPANE_MINIMUMSIZE = new Dimension(180, 40);
	Dimension DEFAULTVALUE_SPLITPANE_PREFERREDSIZE = new Dimension(300, 100);

	int DEFAULTVALUE_TEXTAREA_COLUMNS = 30;
	int DEFAULTVALUE_TEXTAREA_ROWS = 3;

	int DEFAULTVALUE_TEXTFIELD_COLUMNS = 30;

	int DEFAULTVALUE_SPLITPANE_DIVIDERSIZE = 5;

	String DEFAULTVALUE_SEPARATOR_ORIENTATION = ATTRIBUTEVALUE_HORIZONTAL;
}
