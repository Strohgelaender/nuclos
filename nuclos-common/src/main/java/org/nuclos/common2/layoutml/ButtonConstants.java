package org.nuclos.common2.layoutml;

public interface ButtonConstants {
	
	String ACTION_EXECUTERULEBUTTON = "ExecuteRuleButtonAction";
	String ACTION_CHANGESTATEBUTTON = "ChangeStateButtonAction";
	String ACTION_GENERATORBUTTON = "GeneratorButtonAction";
	String ACTION_HYPERLINKBUTTON = "HyperlinkButtonAction";
	String ACTION_DUMMYBUTTON = "DummyButtonAction";

	String ACTION_EXECUTE_CUSTOM_RULE = "executeCustomRule";
	
	String[] SUPPORTEDACTIONSINWEB = {
		ACTION_EXECUTERULEBUTTON,
		ACTION_CHANGESTATEBUTTON,
		ACTION_GENERATORBUTTON
	};
	
}
