package org.nuclos.common2.layoutml;

import java.util.Map;

public interface IWebComponent {

	boolean isVisible();
	
	boolean isEnabled();
	
	boolean isMemo();
	
	IWebContainer getContainer();
	
	IWebConstraints getConstraints();
	
	Map<String, String> getProperties();
	
	Integer getTabIndex();
	
	public String getBackground();
}
