package org.nuclos.common2.layoutml;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public class WebMatrix extends AbstractWebDependents {

	private final String name;
	private final UID entityX;
	private final UID entityY;
	private final UID entityMatrix;
	private final UID entityFieldMatrixParent;
	private final UID entityMatrixValueType;
	private final UID entityMatrixValueField;
	private final UID entityFieldMatrixXRefField;
	private final UID entityFieldCategorie;
	private final UID entityFieldX;
	private final UID entityYParentField;
	private final Boolean enabled;
	private final Boolean editable;
	private final Boolean dynamicCellHeightsDefault;
	private final Integer entityMatrixNumberState;
	private final String entityXSortingFields;
	private final String entityYSortingFields;
	private final String entityXHeader;
	private final String entityYHeader;
	private final UID entityMatrixReferenceField;

	private final UID entityXVlpId; // uid of VLP to limit columns
	private final String entityXVlpIdfieldname; // id field name, usually INTID
	private final String entityXVlpFieldname;
	private final String entityXVlpReferenceParamName; // vlp parameter name, value will be set to boId of master bo

	private final String cellInputType; // input type: "checkicon", "textfield"


	private final UID entity;

	WebMatrix(Attributes attributes) {
		super(attributes);

		name = attributes.getValue(ATTRIBUTE_NAME);
		entityX = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_X));
		entityY = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_Y));
		entityMatrix = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_MATRIX));
		entityFieldMatrixParent = UID.parseUID(attributes.getValue(ATTRIBUTE_MATRIX_PARENT_FIELD));
		entityMatrixValueType = UID.parseUID(attributes.getValue(ATTRIBUTE_MATRIX_VALUE_TYPE));
		entityMatrixValueField = UID.parseUID(attributes.getValue(ATTRIBUTE_MATRIX_VALUE_FIELD));
		entityFieldMatrixXRefField = UID.parseUID(attributes.getValue(ATTRIBUTE_MATRIX_X_REF_FIELD));
		entityFieldCategorie = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_FIELD_CATEGORIE));
		entityFieldX = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_FIELD_X));
		entityYParentField = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_Y_PARENT_FIELD));
		enabled = "yes".equals(attributes.getValue(ATTRIBUTE_ENABLED));
		editable = "yes".equals(attributes.getValue(ATTRIBUTE_EDITABLE));
		dynamicCellHeightsDefault = "yes".equals(attributes.getValue(ATTRIBUTE_DYNAMIC_CELL_HEIGHTS_DEFAULT));
		
		String attributeMatrixNumberState = attributes.getValue(ATTRIBUTE_MATRIX_NUMBER_STATE);
		entityMatrixNumberState = (attributeMatrixNumberState != null && attributeMatrixNumberState.length() != 0) ? Integer.parseInt(attributeMatrixNumberState) : null;

		cellInputType = attributes.getValue(ATTRIBUTE_CELL_INPUT_TYPE);
		
		entity = new UID(); 
		
		entityXSortingFields = attributes.getValue(ATTRIBUTE_ENTITY_X_SORTING_FIELDS);
		entityYSortingFields = attributes.getValue(ATTRIBUTE_ENTITY_Y_SORTING_FIELDS);
		entityXHeader = attributes.getValue(ATTRIBUTE_ENTITY_X_HEADER);
		entityYHeader = attributes.getValue(ATTRIBUTE_ENTITY_Y_HEADER);

		entityMatrixReferenceField = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_MATRIX_REFERENCE_FIELD));
		
		entityXVlpId = UID.parseUID(attributes.getValue(ATTRIBUTE_ENTITY_X_VLP_ID));
		entityXVlpIdfieldname = attributes.getValue(ATTRIBUTE_ENTITY_X_VLP_IDFIELDNAME);
		entityXVlpFieldname = attributes.getValue(ATTRIBUTE_ENTITY_X_VLP_FIELDNAME);
		entityXVlpReferenceParamName = attributes.getValue(ATTRIBUTE_ENTITY_X_VLP_REFERENCE_PARAM_NAME);
	}

	public UID getEntity() {
		return entity;
	}

	@Override
	public UID getUID() {
		return entity;
	}

	public String getName() {
		return name;
	}

	public UID getEntityX() {
		return entityX;
	}

	public UID getEntityY() {
		return entityY;
	}

	public UID getEntityMatrix() {
		return entityMatrix;
	}

	//This is the reference-field from the SubSubForm to the SubForm
	public UID getEntityFieldMatrixParent() {
		return entityFieldMatrixParent;
	}

	public UID getEntityMatrixValueType() {
		return entityMatrixValueType;
	}

	public UID getEntityMatrixValueField() {
		return entityMatrixValueField;
	}

	public UID getEntityFieldMatrixXRefField() {
		return entityFieldMatrixXRefField;
	}

	public UID getEntityFieldCategorie() {
		return entityFieldCategorie;
	}

	public UID getEntityFieldX() {
		return entityFieldX;
	}

	//This is the reference-field from the SubForm to the MainForm
	public UID getEntityYParentField() {
		return entityYParentField;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public boolean getEditable() {
		return editable;
	}

	public Boolean getDynamicCellHeightsDefault() {
		return dynamicCellHeightsDefault;
	}

	@Override
	public UID getForeignkeyfield() {
		return entityYParentField; // OK!
	}

	@Override
	public String getServiceIdentifier() {
		return "dependence_list";
	}

	public Integer getEntityMatrixNumberState() {
		return entityMatrixNumberState;
	}

	public UID getEntityMatrixReferenceField() {
		return entityMatrixReferenceField;
	}

	public String getEntityXSortingFields() {
		return entityXSortingFields;
	}

	public String getEntityYSortingFields() {
		return entityYSortingFields;
	}

	public String getEntityXHeader() {
		return entityXHeader;
	}

	public String getEntityYHeader() {
		return entityYHeader;
	}

	public UID getEntityXVlpId() {
		return entityXVlpId;
	}

	public String getEntityXVlpIdfieldname() {
		return entityXVlpIdfieldname;
	}

	public String getEntityXVlpFieldname() {
		return entityXVlpFieldname;
	}

	public String getEntityXVlpReferenceParamName() {
		return entityXVlpReferenceParamName;
	}

	public String getCellInputType() {
		return cellInputType;
	}

}
