package org.nuclos.common2.layoutml;

public abstract class CompBorder implements LayoutMLConstants {
	
	public abstract String getType();
	
	@Override
	public String toString() {
		return "Type:" + getType();
	}

}
