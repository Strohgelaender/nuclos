package org.nuclos.common2.searchfilter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.log4j.Logger;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionUtils;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.entityobject.CollectableEOEntityProvider;
import org.nuclos.common.preferences.DOMPreferencesFactory;
import org.nuclos.common.preferences.PreferencesSupport;
import org.nuclos.common.preferences.PreferencesUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.PreferencesException;
import org.nuclos.common2.resource.ResourceResolver;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.springframework.beans.factory.annotation.Autowired;

public class EntitySearchFilter2Support {
	
	private static final Logger LOG = Logger.getLogger(EntitySearchFilter2Support.class);
	
	//

	public static final String PREFS_NODE_SEARCHFILTERS = "searchFilters";

	public static final String PREFS_NODE_SEARCHCONDITION = "searchCondition";
	public static final String PREFS_NODE_VISIBLECOLUMNS = "visibleColumns";
	public static final String PREFS_NODE_VISIBLECOLUMNENTITIES = "visibleColumnEntities";

	/**
	 * New way to save/load sorting column prefs: As {@link org.nuclos.common.collect.collectable.CollectableSorting}.
	 */
	public static final String PREFS_NODE_COLLECTABLESORTING = "collectableSorting";
	
	/**
	 * @deprecated Old way to save/load sorting column prefs: only sorting columns *names* (String).
	 */
	private static final String PREFS_NODE_SORTINGCOLUMNS = "sortingColumns";
	
	// Spring injection

	@Autowired
	private PreferencesSupport preferencesSupport;
	
	@Autowired
	private SpringLocaleDelegate localeDelegate;
	
	@Autowired
	private DOMPreferencesFactory prefsFactory;
	
	@Autowired
	private CollectableEOEntityProvider collectableEntityProvider;
	
	@Autowired
	private ResourceResolver resourceResolver;

	// end of Spring injection

	EntitySearchFilter2Support() {
	}

	/**
	 * Copied from SearchFilterDelegate.makeSearchFilter 
	 * and modified to return EntitySearchFilter2
	 * 
	 * transforms a SearchFilterVO to a SearchFilter
	 * @param searchFilterVO
	 * @return SearchFilter
	 */
	public EntitySearchFilter2 createSearchFilter(SearchFilterVO searchFilterVO) {
		EntitySearchFilter2 result = null;
		
		CollectableSearchCondition cond;
		final List<? extends CollectableEntityField> visibleFields;
		List<CollectableSorting> sortingOrder = null;
		
		try {
			final ByteArrayInputStream is = new ByteArrayInputStream(searchFilterVO.getFilterPrefs().getBytes("UTF-8"));
			Preferences prefs = prefsFactory.read(is, false);
	
			final UID entity = searchFilterVO.getEntity();
			// SearchFilter properties
			cond = SearchConditionUtils.getSearchCondition(
					prefs.node(PREFS_NODE_SEARCHCONDITION), entity);
			
			//This is support for some old stuff with the complete dom tree.
			if (cond == null) {
				try {
					Preferences prefs2 = prefs.node("org/nuclos/client");
					final Preferences prefsSearchfilter = prefs2.node(PREFS_NODE_SEARCHFILTERS);
					
					if (prefsSearchfilter.nodeExists(searchFilterVO.getFilterName())) {
						prefs = prefsSearchfilter.node(searchFilterVO.getFilterName());
						cond = SearchConditionUtils.getSearchCondition(
								prefs.node(PREFS_NODE_SEARCHCONDITION), entity);
					}
				} catch (BackingStoreException bse) {
					LOG.warn("Can't load search filter from preferences: " + bse);
					return result;
				}
			}
			
			visibleFields = preferencesSupport.readCollectableEntityFieldsFromPreferences(prefs,
					collectableEntityProvider.getCollectableEntity(entity),
					PREFS_NODE_VISIBLECOLUMNS, PREFS_NODE_VISIBLECOLUMNENTITIES);
	
			if (PreferencesUtils.nodeExists(prefs, PREFS_NODE_COLLECTABLESORTING)) {
				sortingOrder = (List<CollectableSorting>) PreferencesUtils.getSerializableListXML(prefs,
						PREFS_NODE_COLLECTABLESORTING, true);
			}
			// backward compatibility
			else {
				sortingOrder = new ArrayList<CollectableSorting>();
				for (UID n : PreferencesUtils.getUidList(prefs, PREFS_NODE_SORTINGCOLUMNS)) {
					sortingOrder.add(new CollectableSorting(SystemFields.BASE_ALIAS, entity, true, n, true));
				}
			}
		}
		catch (IOException e) {
			LOG.warn("Can't load search filter from preferences: " + e);
			return result;
		}
		catch (PreferencesException e) {
			LOG.warn("Can't load search filter from preferences: " + e);
			return result;
		}
	
		if (searchFilterVO.getFastSelectInResult() == null || !searchFilterVO.getFastSelectInResult()) {
			result = new EntitySearchFilter2(searchFilterVO, 
					new CollectableSearchExpression(cond, sortingOrder), visibleFields);
		} else {
			final FastSelectSearchFilter2 r = new FastSelectSearchFilter2(searchFilterVO, 
					new CollectableSearchExpression(cond, sortingOrder), visibleFields);
			try {
				if (searchFilterVO.getFastSelectIcon() != null) {
					r.setIcon(resourceResolver.resolveDbResourceIcon(searchFilterVO.getFastSelectIcon()));					
				}
			}
			catch (IOException e) {
				LOG.warn("Can't load icon with resourceId " + searchFilterVO.getFastSelectNuclosIcon());
			}
			result = r;
		}		
		return result;
	}

}
