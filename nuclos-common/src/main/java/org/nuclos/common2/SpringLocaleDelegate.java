//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
/*
 * Created on 26.05.2009
 */
package org.nuclos.common2;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldFormat;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

/**
 * Static utility class in the "common" hierarchy, which holds the selected
 * locale data for the user vm.
 *
 * 2011-06-08, tsc: As this class is located in the common part, it must not hold the locale or the
 * resources as static variables. These fields need to be located in the LookupService implementation that
 * can hold the user's locale (client) or perform a locale-specific lookup (server).
 *
 * @author marc.jackisch
 */
// @Component
public class SpringLocaleDelegate {

	private static final Logger LOG = Logger.getLogger(SpringLocaleDelegate.class);

	public interface LookupService {

		Locale getLocale();

		LocaleInfo getLocaleInfo();

		boolean isResource(String key);

		/**
		 * getResource always returns a value.
		 * If there is not resource for the given key, the resource is marked with [key].
		 * 
		 * @param key the resource id
		 * @return resource
		 */
		String getResource(String key);

		/**
		 * get resource from database.
		 * @param li the target locale info
		 * @param key the resource id
		 * @return
		 */
		String getResourceById(LocaleInfo li, String key);

		NumberFormat getNumberFormat();

		DateFormat getDateFormat();

		DateFormat getTimeFormat();

		DateFormat getDateTimeFormat();

		/**
		 * Gets the complete (ordered) chain of candidate locales for the given locale.
		 */
		List<LocaleInfo> getParentChain();

		Date getLastChange();
		
		UID getUserDataLanguage();
		
		UID getSystemDataLanguage();
	};

	private static SpringLocaleDelegate INSTANCE;

	//

	private LookupService keyLookup;
	
	SpringLocaleDelegate() {
		INSTANCE = this;
	}

	// @Autowired
	public void setLookupService(LookupService lookupService) {
		this.keyLookup = lookupService;
	}

	
	
	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static SpringLocaleDelegate getInstance() {
		return INSTANCE;
	}

	private List<LocaleInfo> getLocaleCandidates() {
		return getKeyLookup().getParentChain();
	}

	public Locale getLocale() {
		return getKeyLookup().getLocale();
	}

	private LookupService getKeyLookup() {
		return keyLookup;
	}

	
	public LocaleInfo getUserLocaleInfo()  {
		return getKeyLookup().getLocaleInfo();
	}

	public NumberFormat getIntegerFormat() {
		NumberFormat nf = NumberFormat.getIntegerInstance(getLocale());
		nf.setGroupingUsed(false);
		return nf;
	}

	public NumberFormat getLongFormat() {
		NumberFormat nf = NumberFormat.getNumberInstance(getLocale());
		nf.setGroupingUsed(false);
		return nf;
	}

	public NumberFormat getNumberFormat() {
		return getKeyLookup().getNumberFormat();
	}

	public DateFormat getDateFormat() {
		return getKeyLookup().getDateFormat();
	}

	public DateFormat getTimeFormat() {
		return getKeyLookup().getTimeFormat();
	}

	public DateFormat getDateTimeFormat() {
		return getKeyLookup().getDateTimeFormat();
	}

	//==========================================================================


	public String getResourceById(LocaleInfo li, String id) {
		return getKeyLookup().getResourceById(li, id);
	}

	/**
	 * GetMessage returns a fully formatted message with parameters extended and
	 * eventual recursive resolution of ressource strings.
	 * This method should only be used for message texts, i.e. texts which are
	 * written with {@link java.text.MessageFormat} in mind.
	 *
	 * To get custom texts for fields (like labels, descriptions, menu paths),
	 * which are not properly escaped for {@link java.text.MessageFormat},
	 * use {@link #getText(String, String)} instead.
	 *
	 * @param rid     the resource id
	 * @param otext   original resource text, only used for informational purposes
	 *                in the call and by the build-process examining program
	 * @param params  parameters for the message format
	 * @return the formatted string
	 * @exception java.lang.RuntimeException if a reference recursion occurs, or
	 *            a reference does not exist
	 * @see java.text.MessageFormat
	 */
	public String getMessage(String rid, String otext, Object ... params) {
		if (rid != null) {
			return getMessageInternal(rid, new ResourceBundleResolverUtils.RecursiveResolver(keyLookup, rid), otext, params);
		}
		return otext != null ? "[" + otext + "]" : "[Missing translation for locale " + getKeyLookup().getLocaleInfo().getTag() + "]";
	}

	/**
	 * A more sane version of {@link #getMessage(String, String, Object...)}.
	 * 
	 * @author Thomas Pasch
	 * @since Nuclos 4.1
	 */
	public String getMsg(String rid, Object ... params) {
		return getMessageInternal(rid, new ResourceBundleResolverUtils.RecursiveResolver(keyLookup, rid), rid, params);
	}

	/**
	 * Gets the plain-vanilla text with the given resource id.
	 * 
	 * @param rid the resource id
	 */
	public String getText(String rid) {
		if (rid == null) {
			return null;
		}
		return getKeyLookup().getResource(rid);
	}
	/**
	 * Gets the plain-vanilla text with the given resource id.
	 * Deprecated. Use <code>getText(String rid)</code>.
	 * @param rid     the resource id
	 * @param otext   original resource text (optional, just for debugging purposes)
	 */
	@Deprecated
	public String getText(String rid, String otext) {
		return getText(rid);
	}

	public String getTextFallback(String rid, String fallback) {
		try {
			if (rid != null && getKeyLookup().isResource(rid)) {
				return getText(rid);
			}
		} catch (MissingResourceException e) {
		}
		return fallback;
	}

	public String getText(Localizable localizable) {
		String resId = localizable.getResourceId();
		return getText(resId);
	}

	private String getMessageInternal(String rid, Transformer<String, String> resolver, String otext, Object ... params) {
		try {
			return ResourceBundleResolverUtils.getMessageInternal(keyLookup, resolver, rid, params);
		}
		catch(RuntimeException e) {
			LOG.info("getMessageInternal failed: rid=" + rid + " params=" + (params == null ? "null" : Arrays.asList(params)) + " otext=" + otext + ": " + e);
			return getTextFallback(rid, otext);
		}
	}

	/**
	 * Shortcut to
	 * @param date
	 * @return
	 */
	public String formatDate(Date date) {
		return getKeyLookup().getDateFormat().format(date);
	}

	public String formatDate(Object date) {
		return getKeyLookup().getDateFormat().format(date);
	}

	public Date parseDate(String text) throws ParseException {
		return parseDate(text, true);
	}
	
	public Date parseDate(String text, boolean lenient) throws ParseException {
		DateFormat df = getKeyLookup().getDateFormat();
		if(df instanceof SimpleDateFormat) {
			final SimpleDateFormat sdf = (SimpleDateFormat) df;
			return DateUtils.parse(sdf, text, lenient);
		}
		if (RelativeDate.today().toString().equals(text)) {
			return DateUtils.today();
		}
		df.setLenient(lenient);
		return df.parse(text);
	}

	public String formatTime(Date time) {
		return getKeyLookup().getTimeFormat().format(time);
	}

	public String formatTime(Object time) {
		return getKeyLookup().getTimeFormat().format(time);
	}

	public String formatDateTime(Date dt) {
		return getKeyLookup().getDateTimeFormat().format(dt);
	}

	public String formatDateTime(Object dt) {
		return getKeyLookup().getDateTimeFormat().format(dt);
	}

	public String getLabelFromAttributeCVO(AttributeCVO a) {
		if(a.getResourceSIdForLabel() != null && a.getResourceSIdForLabel().length() > 0) {
			return getTextFallback(a.getResourceSIdForLabel(), a.getLabel());
		}
		return a.getLabel();
	}

	public String getDescriptionFromAttributeCVO(AttributeCVO a) {
		if(a.getResourceSIdForDescription() != null && a.getResourceSIdForDescription().length() > 0)
			return getText(a.getResourceSIdForDescription(), a.getDescription());
		return a.getDescription();
	}

	public String getTextForStaticLabel(String resourceId) {
		try {
			if (resourceId != null) {
				return getText(resourceId, null);
			}
		}
		catch (MissingResourceException ex) {
		}
		return "[Missing resource id=" + resourceId + ", locale " + getKeyLookup().getLocaleInfo().getTag() + "]";
	}

	public String getLabelFromMetaDataVO(EntityMeta<?> entitymetavo) {
		String result = null;
		if(entitymetavo.getLocaleResourceIdForLabel() != null && keyLookup != null)
			result = getText(entitymetavo.getLocaleResourceIdForLabel(), entitymetavo.getEntityName());

		if (result == null)
			result = entitymetavo.getEntityName();
		return result;
	}

	public String getLabelFromMetaFieldDataVO(FieldMeta<?> fieldmetavo) {
		String result = null;
		if(fieldmetavo.getLocaleResourceIdForLabel() != null && keyLookup != null) {
			result = getTextFallback(fieldmetavo.getLocaleResourceIdForLabel(), fieldmetavo.getFallbackLabel());
		}

		if (result == null)
			result = fieldmetavo.getFieldName();
		return result;
	}

	public String getTreeViewFromMetaDataVO(EntityMeta<?> entitymetavo) {
		String result = null;
		if(entitymetavo.getLocaleResourceIdForTreeView() != null && keyLookup != null) {
			try {
				result = getResourceById(getUserLocaleInfo(), entitymetavo.getLocaleResourceIdForTreeView());
			} catch (MissingResourceException mse) {
				if (UID.isStringifiedDefinitionWithEntity(entitymetavo.getLocaleResourceIdForTreeView())) {
					UID titleUID = UID.parseUID(entitymetavo.getLocaleResourceIdForTreeView());
					result = titleUID.getStringifiedDefinition();
				}
			}
		}

		return result;
	}

	public String getTreeViewLabel(EntityObjectVO<?> eovo, IMetaProvider metaDataProvider, UID language) {
		return getTreeViewLabel(eovo.getFieldValues(), eovo.getDalEntity(), metaDataProvider, language);
	}

	public String getTreeViewLabel(Collectable<?> clct, UID entityUID, IMetaProvider metaDataProvider, UID language) {
		if (clct == null) {
			return null;
		}
		final Map<UID, Object> values = new HashMap<>();
		for (FieldMeta<?> field : metaDataProvider.getAllEntityFieldsByEntity(entityUID).values()) {
			if (field.isLocalized() && field.getCalcFunction() == null) {
				values.put(field.getUID(), clct.getLocalizedValue(field.getUID(), language));
			} else {
				final CollectableField value = clct.getField(field.getUID());
				if (value != null && value.getValue() != null) {
					values.put(field.getUID(), value.getValue());
				}
				else {
					values.put(field.getUID(), null);
				}				
			}
		}
		return getTreeViewLabel(values, entityUID, metaDataProvider, language);
	}
	
	public String getIdentifierLabel(Collectable<?> clct, UID entityUID, IMetaProvider metaDataProvider) {
		return getIdentifierLabel(clct, entityUID, metaDataProvider, null);
	}
	
	public String getIdentifierLabel(Collectable<?> clct, UID entityUID, IMetaProvider metaDataProvider, UID language) {
		String result = getTreeViewLabel(clct, entityUID, metaDataProvider, language);
		if (result != null) { // remove image fields. @see NUCLOS-1773
			if (result.indexOf("[$") != -1 && result.indexOf("$]") != -1) {
				result = result.substring(0, result.indexOf("[$")) + result.substring(result.indexOf("$]") + 2);
			}	
		}
		return result;
	}

	public String getTreeViewLabel(Map<UID, Object> values, UID entityUID, IMetaProvider metaDataProvider, UID language) {
		return getTreeViewLabel(values, entityUID, metaDataProvider, true, new HashSet<UID>(),language);
	}
	
	public String getTreeViewLabel(Map<UID, Object> values, UID entityUID, IMetaProvider metaDataProvider, boolean format, UID language) {
		return getTreeViewLabel(values, entityUID, metaDataProvider, format, new HashSet<UID>(),language);
	}

	public String getTreeViewLabel(Map<UID, Object> values, UID entityUID, IMetaProvider metaDataProvider, Set<UID> usedFields, UID language) {
		return getTreeViewLabel(values, entityUID, metaDataProvider, true, usedFields, language);
	}
	
	public String getTreeViewLabel(Map<UID, Object> values, UID entityUID, IMetaProvider metaDataProvider, boolean format, Set<UID> usedFields, UID language) {
		final EntityMeta<?> meta = metaDataProvider.getEntity(entityUID);
		String result = getTreeViewFromMetaDataVO(meta);
		if (result != null) {
			result = replace(result, values, metaDataProvider, format, usedFields);
		} else {
			UID systemidentifier = SF.SYSTEMIDENTIFIER.getUID(entityUID);
			if (values.containsKey(systemidentifier)) {
				usedFields.add(systemidentifier);
				result = (String) values.get(systemidentifier);
			} else {
				Collection<UID> fields = CollectionUtils.transform(meta.getFields(), (FieldMeta<?> field) -> field.getUID());
				for (UID fieldUid : values.keySet()) {
					if (!fields.contains(fieldUid)) {
						continue;
					}
					final FieldMeta<?> field = metaDataProvider.getEntityField(fieldUid);
					if (field.getFieldName().equals("name") && values.get(fieldUid) != null) {
						usedFields.add(fieldUid);
						result = values.get(fieldUid).toString();
						break;
					}
				}
			}
		}
		if (result == null) {
			result = metaDataProvider.getEntity(entityUID).getEntityName();
		}
		return result;
	}
	
	public String getTreeViewLabel(Map<UID, Object> values, UID uidEntity, String formattedString, IMetaProvider metaProvider, UID language) {
		
		if (formattedString != null) {
			return replace(formattedString, values, metaProvider, true, new HashSet<>());
		}
		else {
			UID systemidentifier = SF.SYSTEMIDENTIFIER.getUID(uidEntity);
			if (values.containsKey(systemidentifier)) {
				return (String)values.get(systemidentifier);
			}
			else {
				//return "<unknown>";	
				return metaProvider.getEntity(uidEntity).getEntityName();
			}
		}
	}

	public String getTreeViewDescriptionFromMetaDataVO(EntityMeta metavo) {
		String result = null;
		if(metavo.getLocaleResourceIdForTreeViewDescription() != null && keyLookup != null) {
			try {
				result = getResourceById(getUserLocaleInfo(), metavo.getLocaleResourceIdForTreeViewDescription());
			} catch (MissingResourceException mse) {
				if (UID.isStringifiedDefinitionWithEntity(metavo.getLocaleResourceIdForTreeViewDescription())) {
					UID titleUID = UID.parseUID(metavo.getLocaleResourceIdForTreeViewDescription());
					result = titleUID.getStringifiedDefinition();
				}
			}
		}
		return result;
	}
	
	public String getTreeViewDescription(EntityObjectVO<?> eovo, IMetaProvider metaDataProvider, UID language) {
		return getTreeViewDescription(eovo.getFieldValues(), eovo.getDalEntity(), metaDataProvider, language);
	}

	public String getTreeViewDescription(Collectable<?> clct, UID entityUID, IMetaProvider metaDataProvider, UID language) {
		Map<UID, Object> values = new HashMap<>();
		for (FieldMeta<?> field : metaDataProvider.getAllEntityFieldsByEntity(entityUID).values()) {
			CollectableField value = clct.getField(field.getUID());
			if (value != null && value.getValue() != null) {
				values.put(field.getUID(), value.getValue());
			}
			else {
				values.put(field.getUID(), null);
			}
		}
		return getTreeViewLabel(values, entityUID, metaDataProvider, language);
	}

	public String getTreeViewDescription(Map<UID, Object> values, UID entityUID, IMetaProvider metaDataProvider, UID language) {
		return getTreeViewDescription(values,  entityUID, metaDataProvider, true, language);
	}
	
	public String getTreeViewDescription(Map<UID, Object> values, UID entityUID, IMetaProvider metaDataProvider, boolean format, UID language) {
		String result = getTreeViewDescriptionFromMetaDataVO(metaDataProvider.getEntity(entityUID));
		if (result != null) {
			return replace(result, values, metaDataProvider, format, new HashSet<>());
		}
		return "";
	}

	public String getTreeViewDescription(Map<UID, Object> values, EntityTreeViewVO etvVO, IMetaProvider metaProvider, UID language) {
		String result = etvVO.getNodeTooltip();
		if (result != null) {
			return replace(result, values, metaProvider, true, new HashSet<>());
		}
		return "";
		
	}

	public static Collection<UID> getUsedFieldsInInputString(String input) {
		Collection<UID> usedFields = new HashSet<>();
		replace(input, null, null, false, usedFields);
		return usedFields;
	}
	
	private static String replace(String input, Map<UID, Object> values, IMetaProvider metaDataProvider, boolean format, Collection<UID> usedFields) {
		int sidx = 0;
		while ((sidx = input.indexOf("uid{", sidx)) >= 0) {
			final int eidx = input.indexOf("}", sidx);
			// NUCLOS-2054
			if (eidx <= sidx) {
				break;
			}
			String key = input.substring(sidx + 4, eidx);
			String flags = null;
			final int ci = key.indexOf(':');
			if (ci >= 0) {
				flags = key.substring(ci + 1);
				key = key.substring(0, ci);
			}
			final UID fieldUID = new UID(key);
			usedFields.add(fieldUID);
			
			if (values != null) {
				String rep = "";
				rep = findReplacement(fieldUID, flags, values, metaDataProvider, format);
				input = input.substring(0, sidx) + rep + input.substring(eidx + 1);
				sidx += rep.length();
				
			} else {
				sidx += key.length() + 4;
				
			}
		}
		return input;
	}

	private static String findReplacement(UID sKey, String sFlag, Map<UID, Object> values, IMetaProvider metaDataProvider, boolean format) {
		String sResIfNull = "";
		if(sFlag != null) {
			for(StringTokenizer st = new StringTokenizer(sFlag, ":"); st.hasMoreElements(); ) {
				String flag = st.nextToken();
				if(flag.startsWith("ifnull=")) {
					sResIfNull = flag.substring(7);
				}
			}
		}

		if (values.containsKey(sKey) && values.get(sKey) != null) {
			Object value = values.get(sKey);
			if (format) {
				FieldMeta<?> fieldmeta = metaDataProvider.getEntityField(sKey);
				try {
					final CollectableFieldFormat formatter = CollectableFieldFormat.getInstance(Class.forName(fieldmeta.getDataType()));
					return formatter.format(fieldmeta.getFormatOutput(), value);
				}
				catch (Exception ex) {
					return sResIfNull;
				}
			} else {
				return value==null?"":value.toString();
			}
		}
		else {
			return sResIfNull;
		}
	}

	public String getResource(String resId, String sText) {
		final String result;
		if (resId != null && keyLookup != null) {
			result = getMessage(resId, sText);
		}
		else {
			result = sText;
		}
		return result;
	}

	/**
	 * Given a map from locale tags to translations, this method returns
	 * the best match; or null.
	 */
	public String selectBestTranslation(Map<String, String> map) {
		for (LocaleInfo li : getLocaleCandidates()) {
			String tag = li.getTag();
			String text = map.get(tag);
			if (text != null) {
				return text;
			}
		}
		return null;
	}

	public boolean isResourceId(String id) {
		return keyLookup.isResource(id);
	}

	public String getMessageFromResource(String text) {
		String resText = null;

		try {
			if (text != null && text.indexOf("{") > -1) {
				resText = getText(text.substring(0, text.indexOf("{")).trim());
			}
			else {
				resText = getText(text);
			}
		}
		catch (RuntimeException e) {
			// text seems to be no resourceId
		}

		if (resText != null && text != null) {
			Pattern REF_PATTERN = Pattern.compile("\\{([^\\}]*?)\\}");
			List<String> paramList = new ArrayList<>();
			StringBuffer rb = new StringBuffer();

			Matcher m = REF_PATTERN.matcher(text);
			while(m.find()) {
				String param = m.group(1);

				try {
					if (param.length() > 0 && isResourceId(param.trim()))
						param = getText(param.trim(),null);
				}
				catch (RuntimeException e) {
					// param seems to be no resourceId
				}
				paramList.add(param);
			}

			m = REF_PATTERN.matcher(resText);
			try {
			while(m.find())
				m.appendReplacement(rb, paramList.get(Integer.valueOf(m.group(1))).replace("$", "\\$"));
			} catch (IndexOutOfBoundsException ex) {
				// wrong number of params...
			}

			m.appendTail(rb);

			resText = rb.toString();
		}
		else {
			return text;
		}

		return resText;
	}
}
